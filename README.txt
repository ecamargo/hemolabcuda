# $Id$
Este é o diretório principal do projeto e contém sua última versão.
	=============================================================================================================
OBS: 
    1) Para compilar o projeto execute os seguintes comandos (começando do zero):
       mkdir build/HeMoLab  ---> Cria o diretorio dos binarios
       cd build/HeMoLab     ---> Entra neste diretorio
       cmake ../../			---> Povoa as variaveis do cmake
       cmake ../../			---> Segunda passada ira configurar o ParaView para ser compilado junto ao HeMoLab
							---> A primeira vez que este passo é feito, deve ser setada a variável ParaView_DIR,
							---> Esta deve apontar para o diretorio onde se encontra o arquivo "ParaViewConfig.cmake"
							---> (geralmente a raiz da arvore de compilação do ParaView). Sugestão: Usar o ccmake (em Linux)
							---> ou o CMakeSetup (em Windows).
							---> Depois da primeria compilada completa, existem duas alternativas: 
							--->			a) compilar o ParaView junto ao HeMoLab (incluindo no primeiro os sources de interface)
							--->			b) compilar o HeMoLab só (para ser utilizada quando o codigo de interface do HeMoLab não foi alterado)
       make					---> Copilar (e rezar... :)).
    2) Deve-se configurar as seguintes variáveis de ambiente.
       export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:Diretorio_Raiz_HeMoLab/build/HeMoLab/bin
       
    5)  Após a compilação do Paraview: TCL_LIBRARY="....HeMoLab/build/HeMoLab/utilities/paraview-2.4.2/lib/tcl8.4/"


======================================================================================================


- Geração do pacote no linux

Executar o script "generate_pack.sh".

Será gerado um pacote com o nome HeMoLab.tgz


Principais variáveis que serão utilizadas no projeto:
================================

1) HeMoLab_SOURCE_DIR - Variável que contém o caminho para o diretório source raiz do projeto.

2) HeMoLab_BINARY_DIR - Variável que contém o caminho para o diretório binário raiz do projeto.
(Onde o cmake gera seus arquivos de saída).

3) HeMoLab_INCLUDE_DIRS - Variável que contém uma [lista de caminhos] para diretórios que possuem,
arquivos de cabeçalho.

4) HeMoLab_LIBRARY_DIRS - Variável que contém uma [lista de caminhos] para diretórios que possuem,
bibliotecas compiladas do projeto.

5) HeMoLab_SOURCE_FILE_DIRS - Variável que contém uma [lista de caminhos] para diretórios onde existem 
arquivos fontes dos tipos (.c .cpp .f .f90 .cxx).

6) HeMoLab_USE_FILE - Variável que carrega todas as configurações do projeto.
(Já está sendo utilizada pelo CMakerLists.txt raix do projeto).

7) VTK_DIR - Variável contendo o caminho para o diretório dos binários da biblioteca VTK.

8) VTK_USE_FILE - Variável que possui todas as configurações do VTK.
(A variável já está sendo carregada no arquivo de configuração do projeto não precisa ser inclusa).

9)CMAKE_CXX_FLAGS | CMAKE_CXX_FLAGS_DEBUG - Variáveis que armazenam as flags CXX para compilação de códigos.

10)CMAKE_Fortran_FLAGS | CMAKE_Fortran_FLAGS_DEBUG - Variáveis para flags Fortran.

11)CMAKE_C_FLAGS | CMAKE_C_FLAGS_DEBUG - Variáveis para flags C.

12)CMAKE_EXE_FLAGS | CMAKE_EXE_FLAGS_DEBUG - Variáveis para flags executáveis.

13)DART_START - Variável utilizada para configurar testes.
(Já está configurada no arquivo UseHeMoLab.cmake).


=============================================================================================================
Segue abaixo a estrutura de diretórios inicial do projeto:
=============================================================================================================

HeMoLab(raiz)
    |
    |---build(Onde serão gerados os arquivos do CMake OBS: Deve ser criado um diretório chamado HeMoLab dentro de build)
    |
    |---cmake(?)
    |
    |---doc(Documentação do projeto)
    |    |
    |    |---generac_malhas
    |    -
    |	 
    |---src(Fontes)
    |    |
    |    |---common
    |    |      |
    |    |      |---testing
    |    |      |
    |    |       
    |    |---graphics
    |           
    |---utilities
    |	 |
    |	 |---Gemesis3D
    |    |
    | 	 |---paraview-2.4.2(Paraview e VTK)
    |    |
    |    |---skullydoo
    |	 |
    |  
    |---.externalToolBuilders(Configuração de ferramentas externas e builders) **Somente após configurado.
    |
    |---CMakeLists.txt(Raiz)
    |---ConfigParaviewInstall.cmake
    |---README.txt
    |---readme (do eclipse)
    
