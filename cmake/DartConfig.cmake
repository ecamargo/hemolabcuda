# $Id: DartConfig.cmake 54 2006-03-07 14:42:45Z nacho $
ADD_TEST(test2 ${HeMoLab_BINARY_DIR}/src/outro)

SET (NIGHTLY_START_TIME "1:00:00 EST")
#
# Dart server to submit results (used by client)
#
SET (DROP_METHOD "ftp")
SET (DROP_SITE "hemo01a.lncc.br")
SET (DROP_LOCATION "/incoming")
SET (DROP_SITE_USER "anonymous")
SET (DROP_SITE_PASSWORD "")
SET (TRIGGER_SITE "http://${DROP_SITE}/cgi-bin/TriggerDart.cgi")

SET (UPDATE_TYPE "svn")
#
# Dart server configuration 
#
#SET (CVS_WEB_URL "file://${DROP_SITE}/cvsweb.cgi/dt/")
#SET (CVS_WEB_CVSROOT "teste2")
#SET (CVS_WEB_URL "http://${DROP_SITE}/")
#SET (DOXYGEN_URL "file://${DROP_SITE}/" )
#SET (GNATS_WEB_URL "file://${DROP_SITE}/")

#
# Copy over the testing logo
#
#CONFIGURE_FILE(${TESTE_SOURCE_DIR}/TestingLogo.gif ${TESTE_BINARY_DIR}/Testing/HTML/TestingResults/Icons/Logodt.gif COPYONLY)
