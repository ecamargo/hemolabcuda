# $Id$
#Neste arquivo devemos configurar todas as variáveis globais do projeto.
#Este arquivo será incuido desde src/CMakeLists.txt

FIND_PACKAGE(ParaView REQUIRED)
INCLUDE(${PARAVIEW_USE_FILE})

# Se estou compilando o ParaView, incluo os XML do cliente.
IF (PARAVIEW_INCLUDE_GUI_RESOURCES)
	PARAVIEW_INCLUDE_GUI_RESOURCES("${HeMoLab_BINARY_DIR}/modules/client/hm1DModuleClient.xml")
	PARAVIEW_INCLUDE_GUI_RESOURCES("${HeMoLab_BINARY_DIR}/modules/client/hm3DModuleClient.xml")
	PARAVIEW_INCLUDE_GUI_RESOURCES("${HeMoLab_BINARY_DIR}/modules/client/hmImagingClient.xml")
ENDIF (PARAVIEW_INCLUDE_GUI_RESOURCES)


#Diretório include do CUDA.
INCLUDE_DIRECTORIES(
	${CUDA_INCLUDE_DIRS}
#	${HeMoLab_SOURCE_DIR}/src/cuda
)

SET(HeMoLabSrc_CONFIGURE_DIR ${HeMoLabSrc_SOURCE_DIR}/configure)

#Diretório include do projeto HeMoLab.
INCLUDE_DIRECTORIES(
${HeMoLabSrc_SOURCE_DIR}/configure
${HeMoLabSrc_SOURCE_DIR}/common
${HeMoLabSrc_SOURCE_DIR}/cuda
${HeMoLabSrc_SOURCE_DIR}/io
${HeMoLabSrc_SOURCE_DIR}/gui/client
${HeMoLabSrc_SOURCE_DIR}/gui/widgets
${HeMoLabSrc_SOURCE_DIR}/filtering
${HeMoLabSrc_SOURCE_DIR}/graphics
${HeMoLabSrc_SOURCE_DIR}/servermanager
${HeMoLabSrc_SOURCE_DIR}/widgets
${HeMoLabSrc_SOURCE_DIR}/imaging

${HeMoLabSrc_BINARY_DIR}/common
${HeMoLabSrc_BINARY_DIR}/cuda
${HeMoLabSrc_BINARY_DIR}/filtering
${HeMoLabSrc_BINARY_DIR}/graphics
${HeMoLabSrc_BINARY_DIR}/servermanager
${HeMoLabSrc_BINARY_DIR}/io
${HeMoLabSrc_BINARY_DIR}/resources
${HeMoLabSrc_BINARY_DIR}/imaging
${HeMoLabSrc_BINARY_DIR}/widgets
${HeMoLabSrc_BINARY_DIR}/gui/client
${HeMoLabSrc_BINARY_DIR}/gui/widgets

${ParaView_BINARY_DIR}/GUI/Widgets/Templates
)

# Em Windows, utilizo o TCL interno do ParaView
IF (WIN32)
	INCLUDE_DIRECTORIES(
		${ParaView_SOURCE_DIR}/Utilities/TclTk/TclTk8.4.5Win/include
	)
ELSE(WIN32)
	INCLUDE_DIRECTORIES(
		${ParaView_SOURCE_DIR}/Utilities/TclTk/tk8.4.5/generic
		${ParaView_SOURCE_DIR}/Utilities/TclTk/tcl8.4.5/generic
	)
ENDIF(WIN32)
