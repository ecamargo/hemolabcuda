# Variáveis que serão utilizadas dentro do CMakeList do Paraview (GUI/Client)

SET(HEMOLAB_TO_KW_SRCS
	${HeMoLab_SOURCE_DIR}/src/gui/widgets/vtkKWHM1DXYPlotWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/widgets/vtkKWHM1DXYAnimatedPlotWidget.cxx
)

SET(HEMOLAB_TO_KW_INCLUDE_DIRS
	${HeMoLab_SOURCE_DIR}/src/graphics
	${HeMoLab_SOURCE_DIR}/src/gui/widgets     
)
	
SET(HEMOLAB_TO_KW_LIBS
	vtkhmCommon
	vtkhmIO
	vtkhmGraphics
	)
