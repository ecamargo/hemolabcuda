# Variáveis que serão utilizadas dentro do CMakeList do Paraview (GUI/Client)

SET(HEMOLAB_TO_PV_SRCS
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMObjectWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMElementWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMSegmentWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMNodeWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMTerminalWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMHeartWidget.cxx    
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMPlotWidget.cxx  
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMLineSourceWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMPointWidget.cxx  
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMPointSourceWidget.cxx 
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMStraightModelWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMStraightModelReaderModule.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMGeometryWidget.cxx  
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DTrisurfFilter.cxx    
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DDelaFilter.cxx    
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DQoptFilter.cxx 	
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMImageWorkspaceWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMBoxWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMDICOMImageReader.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMVoxelGrow.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMTopologicalDerivative.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMExtractGrid.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMImageFilter.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMMultipleSegmentWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMMultipleTerminalWidget.cxx   
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DFullModelFilter.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMCouplingFilter.cxx   
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMCoupledModelConfigurationWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM1DSolverFilesConfigurationWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DSolverFilesConfigurationWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMWavePropagationFilter.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMSphereWidget3D.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMSphereSourceWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMInterventionsWidget.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMSolverToEnSight.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DModelReader.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DParticleTracingFilter.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM1DResultsVisFilter.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHMSubTreeMesh.cxx
	${HeMoLab_SOURCE_DIR}/src/gui/client/vtkPVHM3DCenterlines.cxx
)

SET(HEMOLAB_TO_PV_INCLUDE_DIRS
#	${WORKSPACE_PATH}/HeMoLab/utilities/paraview-2.4.2/Utilities/VTKClientServer/
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/bin/	
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/graphics
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/common	
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/widgets		
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/gui
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/gui/client  
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/io  
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/filtering
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/imaging
#	${WORKSPACE_PATH}/HeMoLab/build/HeMoLab/src/servermanager    
#	${WORKSPACE_PATH}/HeMoLab/src/configure
	${HeMoLab_SOURCE_DIR}/src/common  
	${HeMoLab_SOURCE_DIR}/src/filtering  
	${HeMoLab_SOURCE_DIR}/src/graphics
	${HeMoLab_SOURCE_DIR}/src/io      
	${HeMoLab_SOURCE_DIR}/src/widgets   
	${HeMoLab_SOURCE_DIR}/src/gui/widgets     
	${HeMoLab_SOURCE_DIR}/src/gui/client
	${HeMoLab_SOURCE_DIR}/src/servermanager
	)
	
SET(HEMOLAB_TO_PV_LIBS
	vtkhmCommon
	vtkhmFiltering
    vtkhmImaging
	vtkhmIO
	vtkhmWidgets
	vtkhmGraphics
	vtkhmServerManager
	)
