*Param Write Swicht
1
*Renumbering
0
*StepContinuationControl
F 100 0.8 1
1 1 1 30000 0.1 0.1 0.1 
0.01 0.01 0.01 0.01 0.01 0.01 0.01 
VX	 VY	 VZ	 P	 UX	 UY	 UZ	 
0 0 0 
*TimeStep ( DelT, Tini, Tmax )
0.04 0 4
*OutputControl
1 1
*NodeOutputControl (#nodes=0 > All >> OutputNodesList )
0 

*ElementLibraryControl  (MaxElemLib,MaxLCommonPar)
5 13

*SubStep
F T 100 1 -1 
100 1e-10 60 20 1e-10
5245 0 0 -1 0 
5705 1 0 0 0 
540 1 0 -2 0 
540 1 0 -3 0 
540 1 0 -4 0 
1 -1 0 1 0 0 0 1 0.038 0.1 1 0 0 
1 -1 0 1 0 0 0 0 0 0 1 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
1 1 1 30000 0.1 0.1 0.1 
0.01 0.01 0.01 0.01 0.01 0.01 0.01 
VX	 VY	 VZ	 P	 UX	 UY	 UZ	 
0 0 0 

*SubStep
T F 100 1 0 
525 0 0 -5 0 
57 1 0 0 0 
540 1 0 0 0 
540 1 0 0 0 
540 1 0 0 0 
1 -1 0 1 0 0 0 1 0.038 0.1 1 0 0 
1 -1 0 1 0 0 0 0 0 0 1 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
0 1 -1 0 0 0 0 0 0 0 0 0 0 
1 1 1 30000 0.1 0.1 0.1 
0.01 0.01 0.01 0.01 0.01 0.01 0.01 
VX	 VY	 VZ	 P	 UX	 UY	 UZ	 
0 0 0 

*LogFile
Log.txt

*Initial Time
0
