*Renumbering
10
*StepContinuationControl
T 100 1.000000e+000 1 
1.000000e-001  1.000000e-001  1.000000e-001  
1.000000e-003  1.000000e-003  1.000000e-003  
'Caudal' 'Area' 'Presion' 
0 0 0 
*TimeStep ( DelT, Tini, Tmax )
1.000000e-005 0.000000e+000 3.000000e-003 
*OutputControl
1 10 
*NodeOutputControl (#nodes=0 > All >> OutputNodesList )
0 
*ElementLibraryControl  (MaxElemLib,MaxLCommonPar)
2 14
*SubStep
F F 100 1.000000e+000 0
1 0 0 -1 0
3 1 0 -2 0
1.000000 0.000000 1 2 0.500000 2 0.000000 0.000000 0.000000 0 0 0 0 0 
1.000000 2 1.000000 2 0 0 0 0 0 0 0 0 0 0 
1.000000e-001  1.000000e-001  1.000000e-001  
1.000000e-003  1.000000e-003  1.000000e-003  
'Caudal' 'Area' 'Presion' 
0 0 0 


*LogFile
Saida.log