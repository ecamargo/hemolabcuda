\documentclass[times, 10pt,twocolumn]{article}
\usepackage{latex8}
\usepackage{times}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{amssymb,amsmath}

\usepackage[portuguese,english]{babel}

%package para reconhecimento de acentua��o em portugu�s
\usepackage[latin1]{inputenc}

%package para reconhecimento de portugu�s
\usepackage{babel}

%\documentstyle[times,art10,twocolumn,latex8]{article}

%-------------------------------------------------------------------------
% take the % away on next line to produce the final camera-ready version
\pagestyle{empty}

%-------------------------------------------------------------------------
\begin{document}

\title{Virtual Modeling and Numerical Simulation of \\Aneurysms and Stenoses}

\author{ \\
\\
\\
\\
\\
}

\maketitle
\thispagestyle{empty}

\begin{abstract}
   This paper presents a first approach to model and simulate hemodynamic
   pathologies, based on geometrical singularities, such as Aneurysms and Stenoses. The techniques
   described in this work allow the specialist to add these
   pathologies on a parameterized geometry or in a real artery geometry, usually obtained via segmentation from medical images. The
   relative position of the pathology on the artery can be interactively changed
   with the aim of obtaining a better understanding of the disease.
\end{abstract}

\DeclareGraphicsExtensions{.jpg, .pdf, .mps, .png}

%-------------------------------------------------------------------------
\Section{Introduction}

The increase of cardiovascular diseases in the last years has
motivated the development of specific techniques for their
treatment. The complexity of these surgical interventions is notably
growing in the last years due the fast advances in modern medicine.
Some cardiovascular diseases, such as arterioscleroses and cerebral
aneurysms, are reported to depend on hemodynamic factors,
particularly on the characteristics of the wall shear stress induced
by blood flow \cite{citeulike:416747}. With the intention of joining
tools to aid the treatment of these diseases a computational
environment called HeMoLab was created. HeMoLab is a computer system
that allows the creation of patient-oriented models of the human
cardiovascular system.

The 3D modeling of the human cardiovascular system (HCS) aims to
study with a high level of detail the hemodynamic features of the
blood flow and how those features are modified when, for instance, a
surgical planning is devised over those systems. Any part of the
arterial system can be represented in details, by using
patient-specific data (like medical images) of the arterial district
of interest. Particular cases, like the presence of geometric
singularities (aneurysm, stenoses, bifurcations and so on) can be
simulated in order to study local changes in the blood flow
structure and used to retrieve a more complete quantitative
information.

A complete simulation is composed by the reconstruction of the
geometry data through medical imaging systems (Magnetic Resonance
Imaging and Computed Tomography), determination of boundary
conditions and mechanical properties of the blood and arterial walls
of the selected area and finally the computation of an approximate
solution of the problem.

This paper presents an interactive system to create and simulate
virtual aneurysms and stenoses over parameterized geometry and
patient-based arteries. This interactivity allows the user to
simulate several conditions and analyze the numerical results
graphically using visualization techniques such as stream-lines,
stream-tubes, warping techniques for the velocity field, etc

The tool developed in this paper was integrated into the HeMoLab
system that, in turn, is supported by the ParaView \cite{paraview}.
ParaView is a point-and-click 3D scientific visualization system
that allows for most of the common visualization techniques
(isocontouring, volume rendering) on structured and unstructured
grids. Its implementation uses distributed memory parallelism, and
is focused on visual data analysis of large scientific datasets. It
is an open-source, multi-platform visualization application, and
supports distributed computation models to process large datasets.
It has an open, flexible, and intuitive user interface and an
extensible architecture based on the Visualization Toolkit (VTK)
\cite{vtk}.

The remainder of this work is organized as follows: Section 2
presents a brief survey of some works related to computer modelling
of the cardiovascular system. In section 3, the algorithm for the
generation of aneurysms and stenoses is detailed, whereas
mathematical modelling and numerical approximation of the physical
phenomena involved are described in sections 4 and 5, respectively.
Results obtained after simulating the flow inside a patient-specific
artery modified by the tools presented here are shown in section 6,
and some concluding remarks are listed in section 7.

%-------------------------------------------------------------------------
\Section{Related Works}

Cardiovascular diseases are one of the leading causes of death,
morbidity, and invalidity in the world and several researches have
conducted numerous works in order to prevent, treat and diagnose
those pathologies.  Some work in the field of treatment,
visualization, simulation and diagnosis of stenoses and aneurysms
can be viewed in \cite{harreld96, marie04, sato98viewpoint,
stergiopulos92}.

For instance, in \cite{lebarbajec:2003b} a training system developed
with medical purposes was proposed. The system enables the
instructor to generate specific cases for analysis, allowing to
teach not only the basic feature of searching and stenosis
evaluation processes, but also the importance of the correct
viewpoint of acquisition within the environment.

The authors of \cite{daren01} propose an algorithm that decomposes
the patterns of 3D, unsteady blood flow into behavioral components
to reduce the visual complexity while retaining the structure and
information of the original data. The key point of the algorithm is
to enable the visualization of large simulated data sets avoiding
visual clutter and ambiguity.

This paper differs from these previous approaches by adding a
friendly interface to enable the interactive positioning of the
aneurysm or the selection of the cut area in the case of a stenosis.
Also, algorithms to join two or more meshes with a different
resolution were implemented. This feature allows an user to simulate
several cases of these hemodynamic pathologies on an interactive and
intuitive manner.

%-------------------------------------------------------------------------
\Section{Modeling Aneurysms and Stenoses}

The main feature of the system proposed in this paper is the ability
to easily join two meshes to create new geometries maintaining the
correct topology. A friendly interface was designed to enable an
interactive positioning of the portion to be added or removed in the
case of aneurysm or stenosis, respectively.

Basically, the system provides a sphere widget (Figure
\ref{fig:sinteticAneurysm}.a) to be added on an arterial section to
create an aneurysm, or use this sphere to remove a portion of the
arterial section to create a stenosis.

In Figure \ref{fig:sinteticAneurysm}.b, the geometry of the widget
was added to the parameterized geometry artery to create a virtual
aneurysm. The same widget can be translated to another position to
simulate a virtual stenosis as in Figure
\ref{fig:sinteticAneurysm}.c.


\begin{figure}[htb]
    \centering
    \begin{minipage}[b]{4.8cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/spherewidget.png}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{4.8cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/aneurysm01.png}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{4.8cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/stenosis01.png}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{In (a), sphere widget used to model
aneurysms and stenoses. In (b) a virtual aneurysm and in (c) a
virtual stenosis. Both examples without geometry smoothing.}
\label{fig:sinteticAneurysm}
\end{figure}

Finally, Figure \ref{fig:smoothStenosis} shows the result of the
geometry presented in Figure \ref{fig:sinteticAneurysm}.c after some
geometry smoothing.

\begin{figure}[hbt]
 \centering
 \includegraphics[width=5.0cm]{../figs/stenosis02.png}
 \caption{Filtered representation of the Figure \ref{fig:sinteticAneurysm}.b .}
\label{fig:smoothStenosis}
\end{figure}

\subsection{Merging surfaces}

One of the major problems of creating the pathologies was to merge
different meshes while preserving the consistency of the topology,
connectivity and point sharing. To solve this problem the following
steps were adopted:

\begin{itemize}
    \item set the sphere widget in the right position;
    \item clip the main surface with the widget;
    \item find boundary triangles on the clipped area;
    \item find the intersection between the clipped surface and the
    sphere widget;
    \item merge the two surfaces discarding the unused sphere
    triangles;
\end{itemize}

The resolution used in the geometrical representation of the sphere
is also an issue. The ideal case is when the resolution of the
sphere is the same as the resolution of the main surface. In this
situation, joining the two meshes is easier, because the number of
the points to be joined in the clipped surface and the sphere
intersected area is similar. Even when the resolution is similar,
the number of the points in both surfaces generally are not the
same. To solve this problem, an algorithm to sew the two meshes was
developed. Basically, two points of the border area selected to be
used as seeds. Based on these two points, a direction is selected
and the sewing process starts, as illustrated in Figure
\ref{fig:sewing}.a.  As the two surfaces have a different numbers of
border points, one single point of one surface can be linked by
edges with many others points of the other surface (Figure
\ref{fig:sewing}.b). A criterion of proximity was used to link these
points.

\begin{figure}[htb]
    \centering
    \begin{minipage}[b]{5.2cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/sewpoints.png}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.2cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/sewpoints2.png}
        \\(b)
    \end{minipage}
\vspace{-0.3cm} \caption{ In (a) the points of the surface (yellow)
and the points of the sphere (gray).  The white arrow is over the
edge linking the two seed points in (b) where the started sewing
process is illustrated. The white arrow also shows the sewing
direction.} \label{fig:sewing}
\end{figure}

\subsection{Removing malformed elements}

The generation of high quality meshes is a fundamental step towards
the resolution of a problem via an approximate technique such as the
finite element method. The techniques described above generate a
surface with the right topology but do not guarantee the quality of
the mesh. Some special algorithms were developed to handle malformed
triangles, e.g., needle-like triangles (Figure
\ref{fig:malformed}.a), or tiny triangles, which have a much smaller
area than the average element area in the mesh. (Figure
\ref{fig:malformed}.b).

Basically a good algorithm should remove those malformed elements
maintaining the right neighborhood association of the surrounding
elements. A needle triangle has always one neighbor that must be
also removed. This neighbor shares the smaller edge with the needle
triangle as illustrated in Figure \ref{fig:malformed}.c. A small
triangle, when removed, forces the removal of all surrounding
triangles (usually needles) as pictured in \ref{fig:malformed}.d.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{4.1cm}
        \centering
        \includegraphics[width=1.0\linewidth]{../figs/agulhas_model.png}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{4.1cm}
        \centering
        \includegraphics[width=1.0\linewidth]{../figs/small_model.png}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{4.1cm}
        \centering
        \includegraphics[width=1.0\linewidth]{../figs/agulhas_remocao_01.png}
        \\(c)
    \end{minipage}
    \begin{minipage}[b]{4.1cm}
        \centering
        \includegraphics[width=1.0\linewidth]{../figs/small_remocao_01.png}
        \\(d)
    \end{minipage}

\vspace{-0.3cm} \caption{ Red Triangles represent malformed elements
like needle (a) and small triangles (b). In (b) and (c) the
neighbors in yellow are triangles marked to be removed.}
\label{fig:malformed}
\end{figure}

These algorithms were implemented in ParaView and can be
interactively used through an user-friendly interface. Needles are
identified by a user-set threshold angle, which defines the minimum
angle allowed in an element. All the elements that have angles
bellow the threshold are then highlighted, and the user can
eliminate them from the mesh with just one click. Tiny elements are
also defined by a threshold value, which represents now a percentage
of the average area in the mesh. Elements bellow the threshold are
highlighted and, again, the user can erase them with one click.
Details and examples about the algorithms described above can be
found in (suppressed).

\section{The Model}

The governing equations were derived based on a variational
formulation for the coupling of kinematically incompatible models,
in this case 3D-1D flow models in compliant vessels
 (suppressed). The associated Euler equations for a Newtonian
fluid when coupling a 1D domain $\Omega_{1D}$ with a 3D region
$\Omega_{3D}$ through a coupling interface $\Gamma_c$, and
considering the ALE formulation over $\Omega_{3D}$, are the
following:
\begin{align}
\rho A \dfrac{\partial{\bar{u}}}{\partial{t}} + \rho A
\bar{u}\dfrac{\partial{\bar{u}}}{\partial{z}} = -A
\dfrac{\partial{\bar{p}}}{\partial{z}} - 8 \pi \mu \bar{u} + f^{z}&
\nonumber\\ \text{ in } \Omega_{1D} \times \left( 0,T \right)&
\label{eq1}
\\
\rho \dfrac{\partial{\textbf{u}}}{\partial{t}}\Bigg{|}_{\textbf{Y}}
+ \rho \nabla{{\textbf{u}}} \left({\textbf{u}} - \textbf{w} \right)
= - \nabla{p} + \mu \Delta {\textbf{u}} + \textbf{f} & \nonumber\\
\text{ in } \Omega_{3D} \times \left( 0,T \right)&
\label{eq2}
\\
\dfrac{\partial A}{\partial t} + \dfrac{\partial \left( A
\bar{u}\right)}{\partial z} = 0 & \nonumber\\ \text{ in }
\Omega_{1D} \times \left( 0,T \right)& \label{eq3}
\\
\nabla \cdot \textbf{u} = 0 & \nonumber\\ \text{  in  } \Omega_{3D} \times \left( 0,T \right) \label{eq4}\\
\left( -p \textbf{I} + 2 \mu \boldsymbol{\varepsilon}(\mathbf
u)\right)\textbf{n}_1 = -\bar{p}
\textbf{n}_1 & \nonumber\\ \text{ on } \Gamma_c \times \left( 0,T \right) \label{eq5}\\
A_c \bar{u} = \int_{\Gamma_c} \textbf{u} \cdot \textbf{n}_1 \text{ }
d\Gamma  & \nonumber\\ \text{ on } \Gamma_c \times \left( 0,T
\right) \label{eq6}
\end{align}

Where $\mathbf{n}_1$ is the unit outward normal to domain
$\Omega_{1D}$ over the coupling interface $\Gamma_c$. In equations
\ref{eq1} and \ref{eq3}, which represent the 1D model, $\bar{u}$ ,
$\bar{p}$ are the mean velocity and pressure values, $\rho$ is the
blood density, $\mu$ is the dynamic viscosity, $A$ denotes the cross
sectional area, $A \bar{u}$ is the flow rate and $z$ is the axial
coordinate. Equations \ref{eq2} and \ref{eq4} represent the 3D
model, $\textbf{u}$ is the blood velocity, $\textbf{w}$ is the
domain velocity of change consistent with the ALE framework and $p$
is the blood pressure. Equation \ref{eq5} stands for the continuity
of the traction vector at $\Gamma_{c}$ (the coupling interface
between the 3D and 1D models), while expression \ref{eq6} is the
counterpart of the mass conservation.

The wall movement is modelled according to the independent ring
model \cite{Kivity1974b}, and its equations are stated bellow:
\begin{align}
\bar{p} = {\bar{p}}_{0} + \dfrac{E \pi R_{0} h_{0}}{A}\left(
\sqrt{\dfrac{A}{A_{0}}}-1 \right) + &\nonumber\\ + \dfrac{k \pi
R_{0} h_{0}}{A}\dfrac{1}{2 \sqrt{A_{0}
A}}\dfrac{\text{d}A}{\text{d}t} \text{ in }  \Omega_{1D} \times
\left( 0,T \right)& \label{eq7}
\\
p = p_{0} + \dfrac{E h}{R_{0}^{2}}\zeta + \dfrac{k h}{R_{0}^{2}}
\dfrac{\text{d} \zeta}{\text{d}t} &\nonumber\\ \text{in } \Gamma_{w}
\times \left( 0,T \right) \label{eq8}
\end{align}
The deformation of the domain $\Omega_{3D}$ is accounted for through
a laplacian problem, as stated bellow:
\begin{align}
\nabla^{2} \mathbf{d} &= 0 \text{\hspace{10mm}in\hspace{10mm}}
\Omega_{3D}\times(0,T) \label{eq9}
\end{align}
Since it is a small amplitude movement, no remeshing is performed.
Instead, equation \ref{eq9} is used in order to extend the wall
movement to the interior of $\Omega_{3D}$, and
${\textbf{d}}{|}_{\Gamma_\text{w}} =\zeta \textbf{n}$ is the wall
displacement, where $\zeta$ is the scalar field that denotes the
displacement of the wall in the normal direction, given by $\mathbf
n$, that is obtained from equation \ref{eq8}.  Finally, it is
$\mathbf{w}=\frac{\partial\mathbf d}{\partial t}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Numerical Approximation}

In this section the numerical aspects of this work are briefly
described. In this sense, details concerning the numerical
techniques applied and the setting of the model regarding boundary
conditions as well as the 3D geometry and the 1D arterial network
topology are given.

The time discretization is performed by means of a single step
finite difference method corresponding to a classical $\theta$
scheme for both 1D and 3D parts. The spatial discretization is
carried out through the Finite Element Method. Variables $Q$, $A$,
$\bar{p}$ of the 1D model are discretized with $\mathbb{P}_1$ finite
elements, while $\textbf{u}$, $p$ in the 3D model are discretized
with $\mathbb{P}_1^B - \mathbb{P}_1$ finite elements. The index $B$
stands for bubble functions for the velocity field according to the
mini element formulation \cite{Arnold1984}. The domain displacement,
$\textbf{d}$, is also approximated with $\mathbb{P}_1$ finite
elements, and the reference velocity $\textbf{w}$ is computed from
the displacement by a backward Euler difference scheme. For both 1D
and 3D parts, stabilization terms must be included in order to avoid
the non--physical oscillating solutions present in standard Galerkin
approximations. For the 1D model these terms are incorporated along
the characteristics lines and correspond to a Galerkin Least Squares
formulation (suppressed). For the 3D model the stabilization terms
correspond to the Streamline Upwind Petrov Galerkin technique with a
suitable stabilization parameter \cite{Hughes1987}. In all cases,
nonlinearities are treated with Picard iterations.

%-------------------------------------------------------------------------
\section{Results}

In order to show the usability of the tool described above, we
present the results of the flow simulation inside a segment of the
right interior carotid artery in two situations: (i): when an
aneurysm was added to it with the tool described before and (ii)
when the healthy artery was used, for comparison purposes.

Figure \ref{geometry}.a shows the geometry for the two cases, along
with the position within the 1D model where the coupling is
performed (Figure \ref{model1D}). Figure \ref{geometry}.b shows
details of the mesh in the region of the aneurysm, where it can be
seen the smoothness in the transition between the two merged
geometries, both regarding their shapes and their triangular meshes.

\begin{figure}[htb]
    \centering
    \begin{minipage}[b]{5.2cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/geometry.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.2cm}
        \centering
        \includegraphics[width=0.95\linewidth]{../figs/mesh_detail.jpg}
        \\(b)
    \end{minipage}
\vspace{-0.3cm} \caption{ In (a) Artery geometry, with and without
aneurysm and in (b) mesh details on the region affected by the
aneurysm.} \label{geometry}
\end{figure}

\begin{figure}[hbt]
 \centering
 \includegraphics[width=5.8cm]{../figs/coupling_site.jpg}
\caption{Positioning and coupling points of the arterial district
considered within the 1D model.} \label{model1D}
\end{figure}


The simulation was carried out within a cardiac period of
$T=0.8$\text{sec} starting from the at-rest situation. Although some
time-varying transition should be expected before reaching the
time-periodic regime, the main characteristics of the flow patterns
are present even when starting the simulation from the at-rest
state. The blood flow characteristics are here put into evidence by
visualizing the velocity profiles and also some well-known
indicators such as the OSI (measures the oscillatory behavior of
stresses along a cardiac cycle) and the WSS (measures the mean value
of stresses along a cardiac cycle) indexes.

Figures \ref{isovelocity}.a and \ref{isovelocity}.d show the flow
pattern along the artery for each case, with details in the region
of the aneurysm. Notice that in the region before the aneurysm, both
cases led to similar flow patterns and velocity magnitudes. However,
after the aneurysm, the velocity is such that higher strain rate
deformations occur. Also, a steep decrease in the velocity magnitude
can be seen inside the aneurysm (see Figures \ref{isovelocity}.b and
\ref{isovelocity}.e), where complex flow patterns evolved. This
region within the aneurysm is also the place where higher OSI values
occurred (Figure Figures \ref{isovelocity}.c and
\ref{isovelocity}.f): these values were about two times higher in
case 1 when compared to case 2, while the peak in WSS took place at
the inlet and outlet of the aneurysm.

Finally, figure \ref{isovelocity2} shows the evolution profile of an
isosurface of velocity magnitude 20 cm/sec. Here it is possible to
appreciate the recirculation pattern of the flow when entering in
the affected part of the artery.

\onecolumn

\begin{figure}[h!]
\centering
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/warp_full.jpg}
     \\(a)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/streamlines.jpg}
     \\(b)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/OSI4.jpg}
     \\(c)
  \end{minipage}
\par
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/warp_aneurisma3.jpg}
     \\(d)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/streamlines_warp_aneurisma6.jpg}
     \\(e)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/WSS4.jpg}
     \\(f)
  \end{minipage}
\par
\vspace{-0.3cm} \caption{(a) and (d) Velocity profiles and in (b)
and (e) stream lines along the vessels with T=0.105s. OSI (c) and
WSS (f) indexes distribution over the arteries with and without
aneurysm.} \label{isovelocity}
\end{figure}

\begin{figure}[h!]
\centering
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/isocontour_vel0020.jpg}
     \\(a)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/isocontour_vel0025.jpg}
     \\(b)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/isocontour_vel0037.jpg}
     \\(c)
  \end{minipage}
\par
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/isocontour_vel0039.jpg}
     \\(d)
  \end{minipage}
\begin{minipage}[b]{5.0cm}
  \centering
     \includegraphics[width=0.95\linewidth]{../figs/isocontour_vel0041.jpg}
     \\(e)
  \end{minipage}
\par
\vspace{-0.3cm} \caption{Time evolution of an iso surface of
velocity magnitude equal to 20 cm/s.  From left to right, top to
bottom: $t=0.05\text{ sec}$, $t=0.0625\text{ sec}$, $t=0.0925\text{
sec}$,  $t=0.0995\text{ sec}$ and $t=0.105\text{ sec}$.}
\label{isovelocity2}
\end{figure}

\twocolumn

%-------------------------------------------------------------------------
\Section{Conclusions}

This paper presents an approach to model and simulate pathologies
arising in cardiovascular modeling such as Aneurysms and Stenoses.
The geometrical models can be interactively constructed through the
manipulation of spherical widgets over the main geometry. The
generated surface preserves the topology of the original mesh.

After the reconstruction of the geometrical singularity that
represents the pathology, the system allows the computation of an
approximate solution through the numerical simulation. The result of
the simulation can be analyzed using several visualization
techniques such as stream lines, isosurfaces, arbitrary slices,
warping techniques, etc.

Although the present model allows only spherical widgets, this is
the first step to model more general and real situations such as
elliptic-like geometries or even free-form geometries.

%-------------------------------------------------------------------------
\bibliographystyle{latex8}
\bibliography{latex8}

\end{document}
