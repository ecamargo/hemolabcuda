\documentclass[times, 10pt]{article}
\usepackage{latex8_mod}
\usepackage{times}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{amssymb,amsmath}

\usepackage[portuguese,english]{babel}

%package para reconhecimento de acentua��o em portugu�s
\usepackage[latin1]{inputenc}

%package para reconhecimento de portugu�s
\usepackage{babel}

%-------------------------------------------------------------------------
% take the % away on next line to produce the final camera-ready version
\pagestyle{empty}

%-------------------------------------------------------------------------
\begin{document}

\title{An Interactive Tool to Model Virtual Abdominal and Cerebral Aneurysms}

\author{--\\
--\\
-- \\
--\\
--\\
}

\maketitle
\thispagestyle{empty}

\begin{abstract}
   This paper presents a modeling tool to create hemodynamic pathologies, based on geometrical singularities, such as Aneurysms and Stenoses.
   An interactive tool that allows the positioning of virtual aneurysms will be explained and some simulation results will be presented.
   These pathologies can be added based on parameterized geometries or over real arterial geometries, usually obtained via segmentation
   techniques from medical images. This tool allows modeling cerebral and
   abdominal aneurysms and stenoses interactively.
\end{abstract}

\DeclareGraphicsExtensions{.jpg, .pdf, .mps, .png}

%-------------------------------------------------------------------------
\Section{Introduction}

\noindent Recently, computational tools to treat cardiovascular
diseases are being developed stimulated by the increase of these
pathologies. The complexity of these surgical interventions is
notably growing in the last years due the fast advances in modern
medicine. Some cardiovascular diseases, such as arterioscleroses and
cerebral aneurysms, are reported to depend on hemodynamic factors,
particularly on the characteristics of the wall shear stress induced
by blood flow \cite{citeulike:416747}. With the purpose of joining
tools to aid the treatment of these diseases a computational
environment called $<<$\textit{name suppressed by blind review}$>>$
was created \cite{techlncc132006}. $<<$\textit{name suppressed by
blind review}$>>$ is a computer system that allows the creation of
patient-oriented models of the human cardiovascular system and also
the numerical simulation within a unified framework.

To allow a better study of the cardiovascular diseases, it is
necessary to model a 3D representation of the human cardiovascular
system (HCS). This modeling aims to study with a high level of
detail the hemodynamic features of the blood flow and how those
features are modified when, for instance, a surgical planning is
devised over such system. Patient-specific data can be used to
represent in details any part of the arterial network by using
information like medical images of the arterial district of
interest. Particular cases, like the presence of geometric
singularities (aneurysm, stenoses, bifurcations and so on) can be
simulated in order to study local changes in the blood flow
structure and used to retrieve a more complete quantitative
information.

A complete simulation is composed by the reconstruction of the
geometrical data through medical imaging systems (Magnetic Resonance
Imaging and Computed Tomography), determination of boundary
conditions and mechanical properties of the blood and arterial walls
of the selected area and finally the computation of an approximate
solution of the problem. Due to the high complexity of the
simulation of a full 3D HCS, just the arterial district of interest
is represented in 3D. The rest of the HCS is represented using a
simplified 1D model \cite{Blanco2007a}.

This paper presents an interactive system to create and simulate
virtual abdominal aneurysms over parameterized geometrical
structures and patient-based arteries. This interactivity allows the
user to simulate aneurysms with different sizes and different
positions. Numerical results can be graphically presented using
visualization techniques such as stream-lines, stream-tubes, warping
techniques for the velocity field, etc.

The tool developed in this paper was integrated into the
$<<$\textit{name suppressed by blind review}$>>$ system that, in
turn, is supported by the ParaView architecture \cite{paraview}.
ParaView is a point-and-click 3D scientific visualization system
that allows for most of the common visualization techniques
(isocontouring, volume rendering) on structured and unstructured
grids. Its implementation uses distributed memory parallelism, and
is focused on visual data analysis of large scientific datasets. It
is an open-source, multi-platform visualization application, and
supports distributed computation models to process large datasets.
It has an open, flexible, and intuitive user interface and an
extensible architecture based on the Visualization Toolkit (VTK)
\cite{vtk}.

The remainder of this work is organized as follows: Section 2
presents a brief survey of some works related to computer modelling
of the cardiovascular system. In Section 3 the algorithm for the
generation of aneurysms and stenoses is detailed, whereas
mathematical modelling and numerical approximation
 of the physical phenomena involved are described in Sections 4. Results obtained after simulating the flow inside a
 patient-specific artery modified by the tools presented here are
 shown in Section 5, and some concluding remarks are listed in
 Section 6.

%-------------------------------------------------------------------------
\Section{Related Works}

\noindent Some work oriented to the treatment, visualization,
simulation and diagnosis of stenoses and aneurysms can be viewed in
\cite{harreld96, marie04, sato98viewpoint, stergiopulos92}. A
training system developed with medical purposes was proposed in
\cite{lebarbajec:2003b}. The system enables the instructor to
generate specific cases for analysis, allowing to gain insight not
only in the basic feature of searching and stenosis evaluation
processes, but also about the importance of the correct viewpoint of
acquisition within the environment.

In \cite{daren01} the authors propose an algorithm that decomposes
the patterns of 3D unsteady blood flow into behavioral components to
reduce the visual complexity while retaining the structure and
information of the original data. The key point of the algorithm is
to enable the visualization of large simulated data sets avoiding
visual clutter and ambiguity.

Previous versions of the system proposed in this article were able
to model cerebral aneurysms and stenoses as presented in
\cite{svr2008}. Also some preliminary numerical simulation results
were presented.  This paper differs from these works approaches by
extending the usability of the system presented in \cite{svr2008} to
model not only cerebral aneurysms but also abdominal. These
extensions were accomplished by the generalization of the algorithms
that allows the addiction of spherical meshes over synthetic and
real arterial districts.  This paper also presents some comparisons
between the simulation of real and synthetics aneurysms.

%-------------------------------------------------------------------------
\Section{Modeling Abdominal Aneurysms}

\noindent The main feature of the tool presented in this paper is
the ability to easily merge two meshes so as to create new
geometries maintaining the correct topology. Basically one or more
sphere meshes are merged over synthetic and real arterial districts.

Creating abdominal aneurysms require the following steps:
\begin{itemize}
    \item set the sphere widget in the right position;
    \item clip the main surface with the widget;
    \item find boundary triangles on the clipped area;
    \item find the intersection between the clipped surface and the
    sphere widget;
    \item merge the two (or more) surfaces discarding the unused triangles on the sphere and the ones over the original geometry;
\end{itemize}

The system provides a sphere widget to be added on a given region to
create an aneurysm.  The user can change the position of the widget
by clicking over it in the render interface or change that position
numerically in the parameter interface. The radius and the
resolution of the widget can also be changed (Figure
\ref{fig:interface01}).

\begin{figure}[hbt]
 \centering
 \includegraphics[width=18.0cm]{newfigs/interface01.png}
 \caption{System interface. The sphere widget is represented in wireframe mode.}
\label{fig:interface01}
\end{figure}

\subsection{Merging surfaces}

\noindent One of the major problems of creating the pathologies
mentioned above was to merge different meshes while preserving the
consistency of the topology, connectivity and point sharing. To
address these issues, the resolution of the spherical widget and the
resolution of the main mesh should be as similar as possible. In
this situation, joining the two meshes is easier, because the number
of points to be joined in the clipped surface and the sphere is
similar. The initial resolution of the sphere widget is
automatically selected based on the average size of the input
triangle mesh. Although, the user can change this resolution to
refine the sphere or make it more rough. Figure \ref{fig:resolution}
shows several resolution values applied to the same widget to
illustrate this feature.

\begin{figure}[htb]
    \centering
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_SphereResolution01.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_SphereResolution02.jpg}
        \\(b)
    \end{minipage}\\
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_SphereResolution03.jpg}
        \\(c)
    \end{minipage}
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_SphereResolution04.jpg}
        \\(d)
    \end{minipage}
\vspace{-0.3cm} \caption{Abdominal aneurysms with different
resolutions. The resolution modifier was set to (a) 50\%, (b) 100\%,
(c) 150\% and (d) 220\%.} \label{fig:resolution}
\end{figure}


Depending on the position of the widget, the system will produce an
aneurysm or a stenoses. Also, when the widget cuts the main mesh in
two pieces, abdominal aneurysms can be created. If the widget do not
split the main mesh, cerebral aneurysms can be modeled.  Figures
\ref{fig:types}.a, \ref{fig:types}.b e \ref{fig:types}.c presents
examples of stenoses, cerebral aneurysms and abdominal aneurysms.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/stenosis.png}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneu01.png}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneu02.png}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Example of (a) Stenoses, (b) Cerebral
aneurysm and (c) abdominal aneurysms.} \label{fig:types}
\end{figure}

Figure \ref{fig:radius} illustrate the use of the tool to model
several sizes of virtual abdominal aneurysms. An user can change the
size of the sphere widget by right-clicking on the render interface
or by changing the radius entry on the parameter interface.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_size01.png}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_size02.png}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_size03.png}
        \\(c)
    \end{minipage}
    \begin{minipage}[b]{7.5cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aneurisma_size04.png}
        \\(d)
    \end{minipage}
\vspace{-0.3cm} \caption{Abdominal aneurysms with different sizes.}
\label{fig:radius}
\end{figure}

% \section{The Model}
\section{Mathematical Model of the coupled 3D -- 1D blood flow simulation}

\noindent Blood flow simulation is performed with a coupled 3D -- 1D
model of the arterial system, which consists in embedding a 3D model
of an artery in a simplified 1D model of the arterial system. Inlets
and outlets of the 3D model are coupled with their respective
counterparts in the 1D model, such that no 3D boundary condition is
necessary. The main advantage of using 3D--1D coupled models is that
they behave as a unique system, avoiding the need for measurements
that would be required when employing standalone 3D models. The
boundary conditions over the inlets-outlets of the 3D region are
accounted for the own solution of the entire problem. In other
words, it can be understood in the following way: the 1D model feeds
the 3D model with certain conditions and vice-versa. On the other
hand, the boundary conditions needed are the heart inflow boundary
condition that represents the heart ejection and the Windkessel
terminals, which are used to simulate the peripheral beds not
included in the model. Therefore, once the 1D model is calibrated
and validated, any artery which is present in the model may be
replaced by a detailed 3D counterpart that is able to yield richer
qualitative and quantitative results about the flow patterns in that
region. This is particularly useful when studying pathologies such
as aneurysm, stenoses, etc. which are phenomena based on local
factors.

The governing equations were derived based on a variational
formulation for the coupling of kinematically incompatible models,
in this case 3D-1D flow models in compliant vessels
\cite{Blanco2007a}. The associated Euler equations for a Newtonian
fluid when coupling a 1D domain $\Omega_{1D}$ with a 3D region
$\Omega_{3D}$ through a coupling interface $\Gamma_c$, and
conside\-ring the ALE (Arbitrary Lagrangian Eulerian) formulation
over $\Omega_{3D}$, are the following:
\begin{align}
\rho A \dfrac{\partial{\bar{u}}}{\partial{t}} + \rho A
\bar{u}\dfrac{\partial{\bar{u}}}{\partial{z}} = -A
\dfrac{\partial{\bar{p}}}{\partial{z}} - 8 \pi \mu \bar{u} + f^{z}&
\nonumber\\ \text{ in } \Omega_{1D} \times \left( 0,T \right)&
\label{eq1}
\\
\rho \dfrac{\partial{\textbf{u}}}{\partial{t}}\Bigg{|}_{\textbf{Y}}
+ \rho \nabla{{\textbf{u}}} \left({\textbf{u}} - \textbf{w} \right)
= - \nabla{p} + \mu \Delta {\textbf{u}} + \textbf{f} & \nonumber\\
\text{ in } \Omega_{3D} \times \left( 0,T \right)&
\label{eq2}
\\
\dfrac{\partial A}{\partial t} + \dfrac{\partial \left( A
\bar{u}\right)}{\partial z} = 0 & \nonumber\\ \text{ in }
\Omega_{1D} \times \left( 0,T \right)& \label{eq3}
\\
\nabla \cdot \textbf{u} = 0 & \nonumber\\ \text{  in  } \Omega_{3D} \times \left( 0,T \right) \label{eq4}\\
\left( -p \textbf{I} + 2 \mu \boldsymbol{\varepsilon}(\mathbf
u)\right)\textbf{n}_1 = -\bar{p}
\textbf{n}_1 & \nonumber\\ \text{ on } \Gamma_c \times \left( 0,T \right) \label{eq5}\\
A_c \bar{u} = \int_{\Gamma_c} \textbf{u} \cdot \textbf{n}_1 \text{ }
d\Gamma  & \nonumber\\ \text{ on } \Gamma_c \times \left( 0,T
\right) \label{eq6}
\end{align}

Where $\mathbf{n}_1$ is the unit outward normal to domain
$\Omega_{1D}$ over the coupling interface $\Gamma_c$. In equations
\ref{eq1} and \ref{eq3}, which represent the 1D model, $\bar{u}$ ,
$\bar{p}$ are the mean velocity and pressure values, $\rho$ is the
blood density, $\mu$ is the dynamic viscosity, $A$ denotes the cross
sectional area, $Q = A \bar{u}$ is the flow rate and $z$ is the
axial coordinate. Equations \ref{eq2} and \ref{eq4} represent the 3D
model, $\textbf{u}$ is the blood velocity, $\textbf{w}$ is the
domain velocity of change consistent with the ALE framework, while
$p$ is the blood pressure. Equation \ref{eq5} stands for the
continuity of the traction vector at $\Gamma_{c}$ (the coupling
interface between the 3D and 1D models), while expression \ref{eq6}
is the counterpart of the mass conservation.

The wall movement is modelled according to the independent ring
model \cite{Kivity1974b}, and its equations are stated bellow:
\begin{align}
\bar{p} = {\bar{p}}_{0} + \dfrac{E \pi R_{0} h_{0}}{A}\left(
\sqrt{\dfrac{A}{A_{0}}}-1 \right) + &\nonumber\\ + \dfrac{k \pi
R_{0} h_{0}}{A}\dfrac{1}{2 \sqrt{A_{0}
A}}\dfrac{\text{d}A}{\text{d}t} \text{ in }  \Omega_{1D} \times
\left( 0,T \right)& \label{eq7}
\\
p = p_{0} + \dfrac{E h}{R_{0}^{2}}\zeta + \dfrac{k h}{R_{0}^{2}}
\dfrac{\text{d} \zeta}{\text{d}t} &\nonumber\\ \text{in } \Gamma_{w}
\times \left( 0,T \right) \label{eq8}
\end{align}
This is a rather simplified model of the structural behavior of the
arterial wall, but serves to take into account the arterial
compliance. The deformation of the domain $\Omega_{3D}$ is accounted
for through a Laplacian problem, as stated be\-llow:
\begin{align}
\nabla^{2} \mathbf{d} &= 0 \text{\hspace{10mm}in\hspace{10mm}}
\Omega_{3D}\times(0,T) \label{eq9}
\end{align}
Since it is a small amplitude movement, no remeshing is performed.
Instead, equation \ref{eq9} is used in order to extend the wall
movement to the interior of $\Omega_{3D}$, and
${\textbf{d}}{|}_{\Gamma_\text{w}} =\zeta \textbf{n}$ is the wall
displacement, where $\zeta$ is the scalar field that denotes the
displacement of the wall in the normal direction, given by $\mathbf
n$, that is obtained from equation \ref{eq8}.  Finally, it is
$\mathbf{w}=\frac{\partial\mathbf d}{\partial t}$. Refer to
\cite{Blanco2007a} for a further theoretical account about the
coupling of 3D-1D blood flow models.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}

\noindent In order to show the usability of the tool described
above, we present the results of the flow simulation inside a
segment of the abdominal aorta in two situations: (i): when an
aneurysm was added to it with the tool described before and (ii)
when the healthy artery was used, for comparison purposes. Another
case is presented, in which the flow inside an arterial segment of
the pericallosal artery with a natural aneurism is compared to the
flow when an artificial aneurism is included in the same artery.

Figure \ref{fig:fig01} shows a comparison between a segment of a
healthy abdominal aorta (Figure \ref{fig:fig01}.a), and the same
segment after the insertion of an aneurism with the tool described
in this work. In Figure \ref{fig:fig01}.c it is possible to see both
geometries overlayed.


\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/fig01a.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/fig01b.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/fig01c.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{A segment of the abdominal aorta without
aneurism (a), with an aneurism generated artificially (b) and both
cases overlayed (c).} \label{fig:fig01}
\end{figure}

Figure \ref{fig:fig02} shows some characteristics of the flow in the
healthy aorta segment at some time instant in the systolic phase of
a heart beat. Notice that the streamlines (Figure
\ref{fig:fig02}.a), which represent lines tangent to the velocity
field at a given time, show a laminar flow, without recirculation.
This is confirmed by Figure \ref{fig:fig02}.b, which shows the
profile of the modulus of the velocity at some planes. The
overlaying of both can be seen in Figure \ref{fig:fig02}.c.


\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aorta_sem_aneurisma1.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aorta_sem_aneurisma3.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/aorta_sem_aneurisma2.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Characteristics of the flow in the healthy
aorta: Streamlines (a), velocity profiles (b) and both overlayed
(c).} \label{fig:fig02}
\end{figure}

Comparing Figure \ref{fig:fig02} and Figure \ref{fig:fig03} it is
possible to see the differences caused by the presence of the
aneurism. Notice that, after entering that region, there is an
important variation in the velocity field, due to both a change in
the sectional area and an increase in the geometric complexity of
the domain. These figures also show results during the systolic
phase.


\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/AAA_artificial1.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/AAA_artificial2.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/AAA_artificial3.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Details of the flow after inserting of the
aneurism. Streamlines (a), streamlines and modulus of the velocity
profile and  a zoom in the region of the aneurism (c).}
\label{fig:fig03}
\end{figure}

Figure \ref{fig:fig04} shows two segments of the pericallosal artery
belonging to the same individual:  namely a natural aneurism (Figure
\ref{fig:fig04}.a), an artificial aneurism (Figure
\ref{fig:fig04}.b) and a comparison between both (Figure
\ref{fig:fig04}.c). The natural aneurism is located in the right
pericallosal artery and the artificial one is on the left artery.
The goal here is to assess differences in the flow inside the
aneurism in both cases. These comparisons could justify the uses of
this class of tools to represent geometric singularities.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_natural0.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_artificial0.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/geo_pericalosas.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Comparison between a natural aneurism in
the pericallosal artery and an artificial aneurism generated in the
same artery.} \label{fig:fig04}
\end{figure}

Figure \ref{fig:fig05} shows some preliminary results of the flow in
the segment with the natural aneurism. It is easy to see that both
velocity profiles and streamlines are highly complex inside the
aneurism.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_natural3.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_natural5.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_natural7.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Details of the flow in the arterial segment
with a natural aneurism: streamlines (a), velocity profile (b) and a
zoom inside the aneurism, with streamlines and velocity profiles
overlayed.} \label{fig:fig05}
\end{figure}

Figure \ref{fig:fig06} shows the flow in the arterial segment with
an artificial aneurism. Some characteristics present in the natural
aneurism may be identified here, such like the decrease of the
velocity and the greater complexity of the flow inside the aneurism.
Both results, from Figure \ref{fig:fig05} and Figure
\ref{fig:fig06}, correspond to the systolic phase of the heart beat.
We expect the results in the diastolic phase to present more
significant differences and similitudes.

\begin{figure}[!htb]
    \centering
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_artificial7.jpg}
        \\(a)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_artificial8.jpg}
        \\(b)
    \end{minipage}
    \begin{minipage}[b]{5.75cm}
        \centering
        \includegraphics[width=0.95\linewidth]{newfigs/pericalosa_A_artificial9.jpg}
        \\(c)
    \end{minipage}
\vspace{-0.3cm} \caption{Details of the flow in the arterial segment
with an artificial aneurism: streamlines (a), velocity profile (b)
and streamlines and velocity profiles overlayed.} \label{fig:fig06}
\end{figure}

%-------------------------------------------------------------------------
\Section{Conclusions}

\noindent This work presents a tool to model and simulate
pathologies arising in cardiovascular modeling such as Aneurysms and
Stenoses. The geometrical models can be interactively constructed
through the manipulation of spherical widgets over a given geometry
representing an arterial district of interest.

After the reconstruction of the geometrical singularity that
represents the pathology, the system allows in a straightforward way
the computation of an approximate solution through the numerical
simulation. The numerical results have been then analyzed using
several visualization techniques such as stream lines, isosurfaces,
arbitrary slices, warping techniques, etc. From the obtained
numerical evidences it can be said that this kind of tool permits
the characterization, through diverse numerical indicators, of the
altered physical phenomena that occur when different anomalies are
present in the cardiovascular system.

%-------------------------------------------------------------------------
\bibliographystyle{latex8}
\bibliography{references_BLIND}

\end{document}
