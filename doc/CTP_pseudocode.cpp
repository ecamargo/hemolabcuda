/// Estrutura do ponto
typedef struct spoint
{
  /// Identificador do ponto
  int pointId;
  /// Coordenadas
  float x, y, z;
  /// Vetor Velocidade
  float velocityX;
  float velocityY;
  float velocityZ;
  /// Press�o
  float pressure;
  /// Tempo. Usado para armazenar quanto tempo uma part�cula permanece no interior de uma estrutura
  float time;
} point;


/// Estrutura do c�lula (tetraedro)
typedef struct scell
{
  /// Identificador da c�lula
  int cellId;
  /// �ndice dos v�rtices. Cada posi��o ir� armazenar um pointId da struct point
  int v[4];
  /// Centro da c�lula
  point center;
} cell;


/// Estrutura auxiliar para ajudar na ordena��o de uma lista de c�lulas
typedef struct ssort
{
  /// Identificador de uma c�lula
  int cellId;
  /// Coordenada x, y ou z de um v�rtice
  float vertex;
} HMsort;

/// Estrutura da meta-c�lula
typedef struct sgpuMetaCell
{
  /// N�mero de pontos que comp�e a meta-c�lula
  int numPoints;
  /// N�mero de c�lulas que comp�e a meta-c�lula
  int numCells;
  /// Limites da meta-c�lula (Xmin, Xmax, Ymin, Ymax, Zmin, Zmax)
  float bounds[6];
} gpuMetaCell;



//--------------------------------------------------------------
//Pseudo c�digo da main
int main()
{
  VolumeGrid; // malha de volume (tetraedros) contendo valores para velocidade e press�o em todos os instantes de tempo
  TimeStepsDelta; // dura��o de cada instante de tempo
  OutputPath; // diret�rio de sa�da para os arquivos
  NumberOfParticles; // quantidade de part�culas que ter�o as trajet�rias calculadas
  InitialPositionOfParticles; // coordenadas iniciais de cada part�cula
  float distMeta = 0; // Distancia da maior aresta. Auxilia na busca dos tetraedros de interesse no momento de calcular a trajet�ria de uma part�cula
  point *vecListSearch; // vecListSearch � um vetor global que armazena as velocidades, press�o e tempo de cada ponto em 1 instante de tempo. Indexado pelo �ndice do ponto
  gpuMetaCell *Host_metaCells; // Representa cada meta-c�lula alocada em CPU.



  particleWriterCUDA->SetInput( VolumeGrid );
  particleWriterCUDA->SetDelta( TimeStepsDelta );
  particleWriterCUDA->SetPath( OutputPath );
  particleWriterCUDA->SetNumberOfParticles( NumberOfParticles );
  particleWriterCUDA->SetParticles( InitialPositionOfParticles );


  Host_metaCells = new gpuMetaCell[ 8 ];
  vecListSearch = new point[ VolumeGrid->GetNumberOfPoints ];



  // Divis�o em meta-c�lulas
  particleWriterCUDA->WriteMetaCells();

  // C�lculo das trajet�rias, velocidade e press�o das part�culas em todos os instantes de tempo
  particleWriterCUDA->WriteParticles();

  // Recuperando o vetor com as trajet�rias, velocidade e press�o das part�culas
  point *vec;
  particleWriterCUDA->GetPositionOfParticlesInAllTimeSteps( &vec );

 // Escrita dos arquivos EnSight
  WriteEnSightFiles(vec);


  delete [] vecListSearch;
  delete [] Host_metaCells;
}



//--------------------------------------------------------------
// Divis�o da malha de entrada em meta-c�lulas
void vtkHM3DParticleTracingWriterCUDA::WriteMetaCells()
{
  int numPoints = malhaDeEntrada->GetNumberOfPoints();
  int numCells = malhaDeEntrada->GetNumberOfCells();


  point *pointList;   // Lista de pontos
  cell  *cellList;    // Lista de c�lulas
  HMsort  *sortList;  // Lista auxiliar para ajudar na ordena��o dos dados

  pointList = new point[numPoints];

  //Popula uma lista de pontos com os pontos da malha de entrada
  for(i=0; i < numPoints; i++)
    {
    pointList[i] = malhaDeEntrada->GetPoint(i);
    }


  // Popula uma lista de c�lulas com as c�lulas da malha de entrada
  // Encontra a medida da maior aresta (ser� usada na busca)
  float aux.
  for(i=0; i < numCells; i++)
    {
    cellList[i] = malhaDeEntrada->GetCell(i);


    // Encontra tamanho da maior aresta da c�lula corrente.
    // Ao final do loop teremos o valor da maior aresta
    aux = findLongestEdge(cellList[i]);
    if( aux > distMeta )
      distMeta = aux;


    // C�lcula e armazena na pr�pria c�lula seu ponto central.
    computingPointCenter( cellList[i] );
    }


  //Ordena a lista de c�lulas usando o componente x dos centros de cada c�lula
  quicksort(sortList);

  //Divide ao meio a lista de c�lulas (sortList), gerando duas sublistas (l1 e l2)
  divide(sorList, l1, l2);


  // Ordena as 2 sublistas de c�lulas usando o componente y dos centros de cada c�lula
  quicksort(l1);
  quicksort(l2);

  //Divide ao meio a lista de c�lulas, gerando quatro sublistas
  divide(l1, l3, l4);
  divide(l2, l5, l6);


  // Ordena as 4 sublistas de c�lulas usando o componente y dos centros de cada c�lula
  quicksort(l3);
  quicksort(l4);
  quicksort(l5);
  quicksort(l6);


  //Divide ao meio as listas de c�lulas, gerando oito sublistas
  divide(l3, metaCell1, metaCell2);
  divide(l4, metaCell3, metaCell4);
  divide(l5, metaCell5, metaCell6);
  divide(l6, metaCell7, metaCell8);


  //Adiciona os vetores de velocidade e press�o, em cada instante de tempo, �s metac�lulas
  for(i=0; i < timeStep; i++)
    {
    adicionaVetores(metaCell1, i);
    adicionaVetores(metaCell2, i);
    adicionaVetores(metaCell3, i);
    adicionaVetores(metaCell4, i);
    adicionaVetores(metaCell5, i);
    adicionaVetores(metaCell6, i);
    adicionaVetores(metaCell7, i);
    adicionaVetores(metaCell8, i);
    }

  // Escreve as meta-c�lulas em disco
  write(metaCell1);
  write(metaCell2);
  write(metaCell3);
  write(metaCell4);
  write(metaCell5);
  write(metaCell6);
  write(metaCell7);
  write(metaCell8);
}


//----------------------------------------------------------------------------
// Calculo das trajet�rias
int vtkHM3DParticleTracingWriterCUDA::WriteParticles()
{
  int totalPoints   = VolumeGrid->GetNumberOfPoints();  // qtd de pontos da malha
  int timeSteps     = VolumeGrid->GetNumberOfBlocks();  // qtd de instantes de tempo
  int totalCells    = VolumeGrid->GetNumberOfCells();   // qtd de c�lulas (tetraedros)


  // Vari�veis auxiliares
  int size, tam, tmp;


  double bounds[6];   // limites da malha de volume (Xmin, Xmax, Ymin, Ymax, Zmin, ZMax)
  VolumeGrid->GetBounds( bounds );


  // Aloca o vetor de trajet�rias
  size = NumberOfParticles * timeSteps * NumberOfRepeatTimeSteps;
  Host_ParticlePath = new point[ size ];

  // Inicializa o vetor de trajet�rias
  // Todas as posi��es com coordeandas fora da bound da malha de entrada
  InitializeOutOfBounds( bounds );

  //Copia as posi��es iniciais para o vetor Host_ParticlePath
  Copy( InitialPositionOfParticles, Host_ParticlePath );


  // Aloca copia o vetor de trajet�ria para a GPU.
  tam = NumberOfParticles * timeSteps * NumberOfRepeatTimeSteps;
  size = sizeof(point) * tam;
  cudaMalloc((void**)&Device_ParticlePath, size);
  CHECK_CUDA_ERROR();
  // Envia dados para a GPU
  cudaMemcpy( Device_ParticlePath, Host_ParticlePath, size, cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();


  //L� a geometria das meta c�lulas
  readMetaCellsGeometry();


  // Aloca e copia o vetor de pontos para GPU.
  tam = totalPoints;
  size = sizeof(point) * tam;
  cudaMalloc((void**)&Device_fullPoints, size);
  CHECK_CUDA_ERROR();
  // Envia dados para a GPU
  cudaMemcpy( Device_fullPoints, Host_fullPoints, size, cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();
  //------------------------------------------



  // Aloca e copia o vetor de c�lulas (tetraedros) para GPU.
  tam = totalCells;
  size = sizeof(cell) * tam;
  cudaMalloc((void**)&Device_fullCells, size);
  CHECK_CUDA_ERROR();
  // Envia dados para a GPU
  cudaMemcpy( Device_fullCells, Host_fullCells, size, cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();
  //------------------------------------------



  // Aloca e copia o vetor de meta-c�lulas para GPU.
  size = sizeof(gpuMetaCell) * 9;
  cudaMalloc((void**)&Device_metaCells, size);
  CHECK_CUDA_ERROR();
  // Envia dados para a GPU
  cudaMemcpy(Device_metaCells, Host_metaCells, size, cudaMemcpyHostToDevice);
  CHECK_CUDA_ERROR();
  //------------------------------------------



  // Aloca o vetor
  tam = totalPoints;
  size = sizeof(point) * tam;
  cudaMalloc((void**)&Device_vecListSearch, size);
  CHECK_CUDA_ERROR();
  //------------------------------------------



  //Vetor que informa se o passo de tempo referente a um de seus �ndice foi carregados na GPU.
  bool *StatusLoadVelocityVectorForSteps = new bool[ timeSteps * NumberOfRepeatTimeSteps ];

  // Indica qual repeti��o corrente do ciclo (todos os instantes de tempo)
  int startCycle = 0;

  for(int r=1; r <= NumberOfRepeatTimeSteps; r++)
    {
    for(int t=0; t < (timeSteps-1); t++)
      {
      StatusLoadVelocityVectorForSteps[t] = false;


      // L� e armazena na estrutura os valores de velocidade e press�o de todas as meta-c�lulas para um dado instante de tempo
      loadMetaCellsVectors(t);

      // Envia dados lidos para a GPU
      cudaMemcpy( Device_vecListSearch, vecListSearch, size, cudaMemcpyHostToDevice );
      CHECK_CUDA_ERROR();


      StatusLoadVelocityVectorForSteps[t] = true;


      // Calcula a trajet�ria das part�culas (simultaneamente e em GPU) para o instante t.
      // Deve levar em considera��o se existem repeti��es do ciclo (vari�vel tmp)
      tmp = t + ( (r-1) * timeSteps );
      ExecuteKernel(Device_ParticlePath,
                    Device_metaCells,
                    Device_vecListSearch,
                    NumberOfParticles,
                    tmp,
                    distPart,
                    Delta,
                    Device_fullCells,
                    Device_fullPoints,
                    r,
                    startCycle);
      CHECK_CUDA_ERROR();

      startCycle = 0;
      }
    startCycle = 1;
    }


  // Copiando os resultados da GPU para a CPU
  tam = NumberOfParticles * timeSteps * NumberOfRepeatTimeSteps;
  size = sizeof(point) * tam;
  cudaMemcpy(Host_ParticlePath, Device_ParticlePath, size, cudaMemcpyDeviceToHost);
  CHECK_CUDA_ERROR();



  // Desaloca os vetores
  cudaFree(Device_ParticlePath);
  CHECK_CUDA_ERROR();

  cudaFree((void*)Device_metaCells);
  CHECK_CUDA_ERROR();

  cudaFree((void*)Device_vecListSearch);
  CHECK_CUDA_ERROR();

  cudaFree((void*)Device_fullPoints);
  CHECK_CUDA_ERROR();

  cudaFree((void*)Device_fullCells);
  CHECK_CUDA_ERROR();
}


//----------------------------------------------------------------------------
// L� a geometria de todas as meta-c�lulas
void vtkHM3DParticleTracingWriterCUDA::readMetaCellsGeometry()
{
  char metaName[256];


  Host_fullPoints = new point[ totalPoints ]; // To allow direct search (any index).
  Host_fullCells = new cell[ totalCells ]; // To allow direct search (any index).


  for(int i = 0; i < 8; i++)
    {
    // L� do arquivo auxiliar quantos pontos, quantas c�lulas e as coordenadas limites (bounds) da meta-c�lula corrente
    getNumberOfPointsAndCells(i, &numPoints, &numCells);
    getMetaCellsBounds(i, xmin, xmax, ymin, ymax, zmin, zmax);


    // Armazena os valores lidos
    Host_metaCells[i].numPoints = numPoints;
    Host_metaCells[i].numCells  = numCells;

    Host_metaCells[i].bounds[0] = xmin;
    Host_metaCells[i].bounds[1] = xmax;
    Host_metaCells[i].bounds[2] = ymin;
    Host_metaCells[i].bounds[3] = ymax;
    Host_metaCells[i].bounds[4] = zmin;
    Host_metaCells[i].bounds[5] = zmax;



    metaName = strconcat( "metacell", i).
    FILE *fin = fopen(metaName,  "r");

    for(int j=0; j < numPoints; j++)
      {
      // L� os pontos da meta-c�lula i e armazena na struct Host_fullPoints
      }

    for(int j=0; j < numCells; j++)
      {
      // L� as c�lulas da meta-c�lula i e armazena na struct Host_fullCells
      }

    fclose(fin);
  }
}


//----------------------------------------------------------------------------
// L� a velocidade e press�o de todas as meta-c�lulas para um dado instante de tempo
bool vtkHM3DParticleTracingWriterCUDA::loadMetaCellsVectors(int time )
{
  int i, j, k, pointId, numCells, initialCell, finalCell;
  double vector[7];


  if( (time < 0) || (time > this->timeSteps) )
    {
    return false;
    }


  for(i=1; i < 8; i++ )
    {
    numCells = Host_metaCells[ i ].numCells;

    initialCell = (i-1) * numCells;
    finalCell = initialCell + numCells;

    for(j = initialCell; j < finalCell; j++)
      {
      for(k=0; k <= 3; k++)
        {
        pointId = Host_fullCells[ j ].v[ k ];

        VolumeGrid->GetVectors(pointId, vector);

        vecListSearch[ pointId ].pointId = 1;
        vecListSearch[ pointId ].velocityX = vector[0];
        vecListSearch[ pointId ].velocityY = vector[1];
        vecListSearch[ pointId ].velocityZ = vector[2];
        vecListSearch[ pointId ].pressure  = vector[3];
        vecListSearch[ pointId ].time = time;
        }
      }
    }

  return true;
}

// Calcula a trajet�ria das part�culas (simultaneamente e em GPU) para o instante currentStep.
//----------------------------------------------------------------------------
void  ExecuteKernel(Device_ParticlePath,
                    Device_metaCells,
                    Device_vecListSearch,
                    NumberOfParticles,
                    tmp,
                    distPart,
                    Delta,
                    Device_fullCells,
                    Device_fullPoints,
                    r,
                    startCycle);
{
  uint max_threads_por_bloco, blocos;

  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, 0);


  // Esta fun��o retorna 9999 para ambos os campos "major" e "minor", caso n�o exista um device que suporte CUDA
  if (deviceProp.major == 9999 && deviceProp.minor == 9999)
    max_threads_por_bloco = 64;
  else
    max_threads_por_bloco = 192;



  // Se o resto for diferente de zero tenho part�culas n�o alocadas em um bloco ent�o
  // devo adicionar mais 1 bloco para processar as part�culas restantes.
  blocos = nParticles / max_threads_por_bloco + (nParticles % max_threads_por_bloco == 0 ? 0 : 1);

  // Configura as quantidade de threads em cada bloco e a quantidade de blocos
  dim3 blocksize(max_threads_por_bloco, 1, 1);
  dim3 gridsize(blocos, 1, 1);

  // Executa o c�lculo das trajet�rias de todas as part�culas para o instante de tempo currentStep
  computeTraceCUDA <<< gridsize, blocksize >>> (Device_ParticlePath, Device_metaCells, Device_vecListSearch,
                                                nParticles, currentStep, distance,
                                                d, Device_fullCells, Device_fullPoints, cycle_Id, startCycle);
}


//----------------------------------------------------------------------------
// Inicia o c�lculo de trajet�rias encontrando qual meta-c�lula contem a part�cula
__global__ void computeTraceCUDA(point *Device_ParticlePath,
                                 gpuMetaCell *Device_metaCells,
                                 point *Device_vecListSearch,
                                 uint nParticles,
                                 uint currentStep,
                                 float distance,
                                 float d,
                                 cell *Device_fullCells,
                                 point *Device_fullPoints,
                                 uint cycle_Id,
                                 uint startCycle)
{
  int inside = -1;
  float bounds[6];


  // Thread (Part�cula) que ser� calculada
  uint partId = getCurrentPartId();
  point p = ParticlePath[ partId ];


  for( uint metaNumber = 1; metaNumber <= 8; metaNumber++)
    {
    // Recupera os limites da meta-c�lula
    getBounds( metaNumber, bounds );

    // Verfica se a part�cula p esta dentro da meta-c�lula
    inside = isInside( p, bounds );

    if(inside)
      {
      findCloserCells( metaNumber );
      }
    }
}

//----------------------------------------------------------------------------
// Encontra c�lulas pr�ximas da part�cula usando dist�ncia Euclidiana
__device__ void findCloserCells( uint metaNumber )
{
  // Alfas globais para habilitar a interpola��o
  float a1, a2, a3, a4;

  uint numCells = MetaCells[ metaNumber ].numCells;


  for(uint i = 0; i < numCells; i++)
    {
    // � uma c�lula pr�xima?
    if( getCloserCells(i) )
      {
      // A part�cula esta dentro da c�lula?
      if(isInsideCell( i, &a1, &a2, &a3, &a4))
        {
        // Aplica a equa��o de deslocamento
        computeVelocity( i, a1, a2, a3, a4);
        break; // Se a c�lula foi encontrada, pare busca
        }
      }
    }
}


//----------------------------------------------------------------------------
// Verifica se a part�cula esta pr�xima da c�lula usando dist�ncia Euclidiana
__device__ int getCloserCells( uint cellId )
{
  //Resultado de cada v�rtice
  int res[4] = {-1, -1, -1, -1};
  int dist;

  for(uint i = 0; i <=3; i++)
    {
    //Dist�ncia Euclidiana entre a part�cula e o v�rtice
    dist = computeEuclidianDistance( fullCells[ cellId ].v[i], p );


    //Se a dist for menor ou igual a dist�ncia da maior aresta (calculada na divis�o das meta-c�lulas), a part�cula esta pr�xima do c�lula.
    if( dist <= distMeta ) res[i] = 1;
    }

  if( res[0] && res[1] && res[2] && res[3])
    return 1;
  else
    return 0;
}


//----------------------------------------------------------------------------
// Verifica se a part�cula esta dentro de uma dada c�lula
// Retorna os valores a1, a2, a3 e a4 que s�o usados para o c�lculo deslocamento, velocidade, press�o e tempo da part�cula no interior da c�lula.
__device__ int isInsideCell( uint currentCell, float *a1, float *a2, float *a3, float *a4)
{
  computeVectorOperation(uint currentCell, float *a1, float *a2, float *a3, float *a4);

  *a1 = 1 - *a2 - *a3 - *a4;

  if((*a1>=0  && *a1<=1) && (*a2>=0  && *a2<=1) && (*a3>=0  && *a3<=1) && (*a4>=0  && *a4<=1))
    return 1; // Part�cula esta dentro da c�lula 'c'
  else
    return 0; // Part�cula esta fora da c�lula 'c'
}


//----------------------------------------------------------------------------
// Calcula deslocamento, velocidade, press�o e tempo da part�cula no interior da c�lula.
__device__ void computeVelocity(uint currentCell, float a1, float a2, float a3, float a4)
{
uint actual_Id = getCurrentPartId();
uint next_Id = getPartIdInNextStep();

point actual_part = ParticlePath[ actual_Id ];

uint v0 = fullCells[ currentCell ].v[0];
uint v1 = fullCells[ currentCell ].v[1];
uint v2 = fullCells[ currentCell ].v[2];
uint v3 = fullCells[ currentCell ].v[3];



ParticlePath[ next_Id ].pointId = 1;

ParticlePath[ next_Id ].x = actual_part.x +
                            delta * (
                            a1 * VecListSearch[ v0 ].velocityX +
                            a2 * VecListSearch[ v1 ].velocityX +
                            a3 * VecListSearch[ v2 ].velocityX +
                            a4 * VecListSearch[ v3 ].velocityX);

ParticlePath[ next_Id ].y = actual_part.y +
                            delta * (
                            a1 * VecListSearch[ v0 ].velocityY +
                            a2 * VecListSearch[ v1 ].velocityY +
                            a3 * VecListSearch[ v2 ].velocityY +
                            a4 * VecListSearch[ v3 ].velocityY);

ParticlePath[ next_Id ].z = actual_part.z +
                            delta * (
                            a1 * VecListSearch[ v0 ].velocityZ +
                            a2 * VecListSearch[ v1 ].velocityZ +
                            a3 * VecListSearch[ v2 ].velocityZ +
                            a4 * VecListSearch[ v3 ].velocityZ);

}

