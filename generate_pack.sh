#!/bin/bash
# parametro 1: path em que projeto foi compilado (exemplo: build/HeMoLab)
# parametro 2: path do SolverGP.x (exemplo: utilities/SolverSerial/binary/)
# parametro 3: especifica se esta versao é 32 ou 64 bits - desabilitado

# exe:  ./generate_pack .sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ 




if [ "x${1}" = "x" ] ; then 
    echo "./generate_pack.sh <caminho em que projeto foi compilado > <caminho executavel SolverGP>"
    echo "exemplo: ./generate_pack.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ "
    exit
fi


if [ "x${2}" = "x" ] ; then 
    echo "./generate_pack.sh <caminho em que projeto foi compilado > <caminho executavel SolverGP> "
    echo "exemplo: ./generate_pack.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ "
    exit
fi


#if [ "x${3}" = "x" ] ; then 
#    echo "./generate_pack.sh <caminho em que projeto foi compilado > <caminho executavel SolverGP> "
#    echo "exemplo: ./generate_pack.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ "
#    exit
#fi



tmpdir="/tmp"
package="HeMoLab"
outpath=`/bin/pwd`
version="1.0"
arch=$(uname -m)

echo "Removendo arquivos de scripts antigos que foram interrompidos durante execucao ..."

rm /tmp/HeMoLab - rf

echo
echo
echo "Gerando pacote para arquitetura $arch"

echo
echo "Criando diretorios"
mkdir $tmpdir/$package
mkdir $tmpdir/$package/bin
mkdir $tmpdir/$package/lib

echo
echo "Copiando arquivos"
#cp $1/bin/paraview-real $tmpdir/$package/bin
cp $1/bin/paraview $tmpdir/$package/bin



if [ "${arch}" = "i686" ] ; then 
   echo "Copiando SolverGP versao 32 bits"
	cp $2/SolverGP_32.x $tmpdir/$package/bin/SolverGP.x
elif [ "${arch}" = "i686" ] ; then
   echo "Copiando SolverGP versao 32 bits"
 	cp $2/SolverGP_32.x $tmpdir/$package/bin/SolverGP.x

else
   echo "Copiando SolverGP versao 64 bits"
	cp $2/SolverGP_64.x $tmpdir/$package/bin/SolverGP.x
fi


cp HeMoLab.sh $tmpdir/$package/

cp -d $1/bin/*.so* $tmpdir/$package/lib


cp -dr $1/lib/tcl8.4 $tmpdir/$package/lib
cp -dr $1/lib/tk8.4 $tmpdir/$package/lib

cp -dr $1/modules $tmpdir/$package/
cp HeMoLab.sh $tmpdir/$package/


echo
cd $tmpdir
tar cvzfl $outpath/$package-$version-$arch.tar.gz $package

echo "Gerando pacote com nome $package-$version-$arch.tar.gz"

echo
echo "Removendo arquivos"
rm -rf $tmpdir/$package

