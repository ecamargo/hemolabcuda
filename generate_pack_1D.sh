#!/bin/bash
# parametro 1: path em que projeto foi compilado (exemplo: build/HeMoLab)
# parametro 2: path do SolverGP.x (exemplo: utilities/SolverSerial/binary/)
# exe:  ./generate_pack_1D.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/  



if [ "x${1}" = "x" ] ; then 
    echo "./generate_pack.sh <caminho em que projeto foi compilado > <caminho executavel SolverGP>"
    echo "exemplo: ./generate_pack.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ "
    exit
fi


if [ "x${2}" = "x" ] ; then 
    echo "./generate_pack.sh <caminho em que projeto foi compilado > <caminho executavel SolverGP>"
    echo "exemplo: ./generate_pack.sh build/HeMoLab_minSizeRelease/ utilities/SolverSerial/binary/ "
    exit
fi




tmpdir="/tmp"
package="HeMoLab_1D"
outpath=`/bin/pwd`
version="1.0"
arch=$(uname -m)

echo
echo "Gerando pacote para arquitetura $arch"


echo
echo "Criando diretorios"
mkdir $tmpdir/$package
mkdir $tmpdir/$package/bin
mkdir $tmpdir/$package/lib
mkdir $tmpdir/$package/modules
mkdir $tmpdir/$package/modules/client
mkdir $tmpdir/$package/modules/server

mkdir $tmpdir/$package/data
mkdir $tmpdir/$package/data/1d_model_data1
mkdir $tmpdir/$package/data/1d_model_data2


echo
echo "Copiando arquivos"
#cp $1/bin/paraview-real $tmpdir/$package/bin
cp $1/bin/paraview $tmpdir/$package/bin


if [ "${arch}" = "i686" ] ; then 
   echo "Copiando SolverGP versao 32 bits"
	cp $2/SolverGP_32.x $tmpdir/$package/bin/SolverGP.x
elif [ "${arch}" = "i686" ] ; then
   echo "Copiando SolverGP versao 32 bits"
 	cp $2/SolverGP_32.x $tmpdir/$package/bin/SolverGP.x

else
   echo "Copiando SolverGP versao 64 bits"
	cp $2/SolverGP_64.x $tmpdir/$package/bin/SolverGP.x
fi



cp HeMoLab1D.sh $tmpdir/$package/


cp -d $1/bin/*.so* $tmpdir/$package/lib


cp -dr $1/lib/tcl8.4 $tmpdir/$package/lib
cp -dr $1/lib/tk8.4 $tmpdir/$package/lib

cp -dr $1/modules/client/hm1DModuleClient.xml $tmpdir/$package/modules/client
cp -dr $1/modules/server/hm1DModuleServer.xml $tmpdir/$package/modules/server

cp -dr data/padrao_eixoY_invertido/Mesh.txt $tmpdir/$package/data/1d_model_data1
cp -dr data/padrao_eixoY_invertido/Basparam.txt $tmpdir/$package/data/1d_model_data1
cp -dr data/padrao_eixoY_invertido/IniFile.txt $tmpdir/$package/data/1d_model_data1
cp -dr data/padrao_eixoY_invertido/Param.txt $tmpdir/$package/data/1d_model_data1

cp -dr data/New_1d_tree/*.txt $tmpdir/$package/data/1d_model_data2/ 
 
 
 
#cp configure.sh $tmpdir/$package
#cp Install.txt $tmpdir/$package


echo
echo "Gerando pacote"
cd $tmpdir
tar cvzfl $outpath/$package-$version-$arch.tar.gz $package

echo "Gerando pacote com nome $package-$version-$arch.tar.gz"


echo
echo "Removendo arquivos"
rm -rf $tmpdir/$package

