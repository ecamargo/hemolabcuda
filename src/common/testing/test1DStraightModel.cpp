/*
 * $Id:test1DStraightModel.cpp 286 2006-05-05 19:43:09Z nacho $
 */
#include "vtkHM1DTerminal.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DStraightModel.h"
//
#include "vtkPVProcessModule.h"
//
#include "vtkClientServerInterpreter.h"
#include "vtkClientServerStream.h"
//

#include <stdio.h>

vtkClientServerID GetUniqueID()
{
  static vtkClientServerID id = {3};
  ++id.ID;
  return id;
}


// ClientServer wrapper initialization functions.
extern "C" void vtkhmCommon_Initialize(vtkClientServerInterpreter*);

int main( int argc, char *argv[ ])
{
	
//  vtkPVProcessModule* pm = vtkPVProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());
  vtkPVProcessModule* pm = vtkPVProcessModule::New();
  pm->Initialize();
  std::cout << "Loading module... "<< *(pm)<< endl;
  pm->LoadModule("vtkhmCommon","/home/nacho/workspace/HeMoLab/build/HeMoLab/bin"); 
  pm->LoadModule("vtkPVServerCommon","/home/nacho/workspace/HeMoLab/build/paraview-dbg/bin"); 
  std::cout << "Module loaded!" << endl;

  vtkClientServerInterpreter* interp = vtkClientServerInterpreter::New();
  interp->SetLogStream(&cout);

  vtkHM1DTerminal *root;
  vtkHM1DTerminal *tmpTerm;
  vtkHM1DSegment *tmpSegm;
  
  vtkHM1DStraightModel *straightModel = vtkHM1DStraightModel::New();
  
  root = (vtkHM1DTerminal *)straightModel->Get1DTreeRoot();

  straightModel->AddSegment(root->GetId());
  cout << "Valor id raiz: " << root->GetId()<< endl;
  
  cout<< "Recuperando segmento inexistente." << endl;
  tmpSegm =  straightModel->GetSegment(1);
  cout << "^---->Warming message should have been deslayed." << endl;
  cout<< "Recuperando segmento existente." << endl;
  tmpSegm =  straightModel->GetSegment(2);

  cout<< "Recuperando termnal inexistente." << endl;
  tmpTerm =  straightModel->GetTerminal(2);
  if (!tmpTerm)
    cout << "^---->Warming message should have been deslayed." << endl;
  cout<< "Recuperando termnal existente." << endl;
  tmpTerm =  straightModel->GetTerminal(3);

  tmpTerm = (vtkHM1DTerminal *)tmpSegm->GetFirstChild();

  cout << "-> Eliminando segmento inexistente con id: 1" << endl;
  straightModel->RemoveSegment(1,0);
  cout << "^---->Warming message should have been deslayed." << endl;
  cout << "-> Eliminando segmento con id: 2" << endl;
  if (straightModel->RemoveSegment(2,0))
    cout << "--> Terminal eliminado!" << endl;
  else
    cout << "--> Ocorreu algunm erro ao eliminar o terminal." <<  endl;

  cout << "-> Recuperando terminal eliminado, id:" << tmpTerm->GetId() << endl;
  tmpTerm = straightModel->GetTerminal(3);
//  cout << "-> Recuperando terminal eliminado, id:" << tmpTerm->GetId() << endl;

  straightModel->AddSegment(root->GetId());
  
//  tmpTerm = straightModel->GetTerminal(3);
//  cout << "Recuperado termianl com id "  << tmpTerm->GetId() << endl;
  tmpSegm = straightModel->GetSegment(4);
  cout << "-> Recuperado segmento com id "  << tmpSegm->GetId()  << " filho de id " << tmpSegm->GetFirstParent()->GetId()<< endl;
  tmpTerm = straightModel->GetTerminal(5);
  cout << "-> Recuperado terminal com id "  << tmpTerm->GetId()  << " filho de id " << tmpTerm->GetFirstParent()->GetId() << endl;
//  tmpTerm = straightModel->GetTerminal(3);
//  tmpSegm = straightModel->GetSegment(3);

//  cout << "Setando os parametros do segundo terminal por segunda vez" << endl;

  cout << "-> Recuperando filhos da raiz: " << endl;
  tmpSegm = (vtkHM1DSegment *)root->GetFirstChild();
  cout << "--> Primeiro filho: " << tmpSegm->GetId() << endl;
  tmpSegm = (vtkHM1DSegment *)tmpTerm->GetFirstParent();
  cout << "--> Parent de terminal com id 5 :" << tmpSegm->GetId() << endl;
  tmpTerm = (vtkHM1DTerminal *)tmpSegm->GetFirstChild();
  cout << "--> Primeiro filho do segmento id: " << tmpTerm->GetId() << endl;

  cout << "-> Eliminando recursivamente raiz con id 1" << endl;
  straightModel->ClearTree();
  // falta verificar que el arbol este vacio
  
  straightModel->Delete();  
}
