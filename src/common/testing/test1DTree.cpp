/*
 * $Id: test1DTree.cpp 286 2006-05-05 19:43:09Z nacho $
 */
#include "vtkHM1DTreeElement.h"
#include <stdio.h>


int main( int argc, char *argv[ ])
{
  int id = 1;
  vtkHM1DTreeElement *elem = vtkHM1DTreeElement::New();
  vtkHM1DTreeElement *elemChild1 = vtkHM1DTreeElement::New();
  vtkHM1DTreeElement *elemChild2 = vtkHM1DTreeElement::New();
  vtkHM1DTreeElement *tmpElem;
  cout << "Recuperando o id antes de seta-lo " <<  endl;
  cout << " id : " << elem->GetId() << endl;
  cout << "Setando id para " << id << endl;
  elem->SetId(id);
  cout << "Novo valor id : " << elem->GetId() << endl;
  id = 20;
  cout << "Setando id para " << id << endl;
  elem->SetId(id);
  cout << "Valor id : " << elem->GetId() << endl;
  elemChild1->SetId(2);
  elemChild2->SetId(3);
  cout << "Agregando o primeiro filho, id : " << elemChild1->GetId() << endl;
  elem->AddChild(elemChild1);
  cout << "Agregando o segundo filho, id : " << elemChild2->GetId() << endl;
  elem->AddChild(elemChild2);
  
  cout << "Recuperando o filho, id : " << elemChild1->GetId() << endl;
  tmpElem = elem->GetChild(elemChild1->GetId());
  cout << "Id do filho recuperado, id : " << tmpElem->GetId() << endl;
  
  cout << "Recuperando o filho, id : " << elemChild2->GetId() << endl;
  tmpElem = elem->GetChild(elemChild2->GetId());
  cout << "Id do filho recuperado, id : " << tmpElem->GetId() << endl;
  
  tmpElem = elem->GetChild(elemChild2->GetId());
  cout << "Removendo o filho, id : " << tmpElem->GetId() << endl;
  elem->RemoveChild(tmpElem);

  if (elemChild2->IsLeaf())
    {
    cout << "O elemento id " << elemChild2->GetId() << " é folha" << endl;
    }
  else
    {
    cout << "O elemento id " << elemChild2->GetId() << " NÃO é folha" << endl;
    }
  if (elem->IsLeaf())
    {
    cout << "O elemento id " << elem->GetId() << " é folha" << endl;
    }
  else
    {
    cout << "O elemento id " << elem->GetId() << " NÃO é folha" << endl;
    }
  tmpElem = elem->GetChild(elemChild1->GetId());
  cout << "Removendo o filho, id : " << tmpElem->GetId() << endl;
  elem->RemoveChild(tmpElem);
  if (elem->IsLeaf())
    {
    cout << "O elemento id " << elem->GetId() << " é folha" << endl;
    }
  else
    {
    cout << "O elemento id " << elem->GetId() << " NÃO é folha" << endl;
    }

  elem->Delete();
  elemChild1->Delete();
  elemChild2->Delete();
  return 0;
}
