/*
 * $Id: testSegmentTerminal.cpp 286 2006-05-05 19:43:09Z nacho $
 */
#include "vtkHM1DTerminal.h"
#include "vtkHM1DSegment.h"
#include <stdio.h>

int main( int argc, char *argv[ ])
{
  
  vtkHM1DTerminal *term1 =  vtkHM1DTerminal::New();
  vtkHM1DTerminal *term2 =  vtkHM1DTerminal::New();
  vtkHM1DTerminal *tmpTerm;
  
  vtkHM1DSegment *segm1 = vtkHM1DSegment::New();
  vtkHM1DSegment *tmpSegm;
  
  cout << "Setando ids..." << endl;
  term1->SetId(1);
  term2->SetId(2);
  segm1->SetId(3);

  cout << "Agregando segmento como filho do primeiro terminal" << endl;
  term1->AddChild(segm1);  
  cout << "Agregando terminal como filho do segmento" << endl;
  segm1->AddChild(term2);

  cout << "Recuperando o filho do primeiro terminal, id : " << term1->GetId() << endl;
  tmpSegm = (vtkHM1DSegment *)term1->GetChild(segm1->GetId());
  cout << "Id do filho recuperado, id : " << tmpSegm->GetId() << endl;
  
  vtkHM1DTerminal::ResistanceValuesList *rlist1 = new vtkHM1DTerminal::ResistanceValuesList;
  vtkHM1DTerminal::ResistanceValuesList *rlist2 = new vtkHM1DTerminal::ResistanceValuesList;
  vtkHM1DTerminal::CapacitanceValuesList *clist = new vtkHM1DTerminal::CapacitanceValuesList;
  vtkHM1DTerminal::PressureValuesList *plist = new vtkHM1DTerminal::PressureValuesList;

  rlist1->push_back(1.0);
  rlist1->push_back(1.1);
  rlist1->push_back(1.2);
  
  vtkHM1DTerminal::ResistanceValuesList::iterator it = rlist1->begin();
  cout << "Os valores na lista são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  
  rlist2->push_back(2.0);
  rlist2->push_back(2.1);
  rlist2->push_back(2.2);

  clist->push_back(10.0);  
  clist->push_back(11.0);  
  clist->push_back(12.0);  

  plist->push_back(10000.0);
  plist->push_back(11000.0);
  plist->push_back(12000.0);
  
  cout << "Setando os parametros do primeiro terminal" << endl;
  term1->SetR1(rlist1);
  term1->SetR2(rlist2);
  term1->SetC(clist);
  term1->SetPt(plist);

  rlist1->clear();
  rlist2->clear();
  clist->clear();
  plist->clear();
  
  rlist1->push_front(3.0);
  rlist1->push_front(3.1);
  rlist1->push_front(3.2);

  rlist2->push_front(4.0);
  rlist2->push_front(4.1);
  rlist2->push_front(4.2);

  clist->push_front(40.0);  
  clist->push_front(41.0);  
  clist->push_front(42.0);  

  plist->push_front(50000.0);
  plist->push_front(51000.0);
  plist->push_front(52000.0);

  cout << "Setando os parametros do segundo terminal" << endl;
  term2->SetR1(rlist1);
  term2->SetR2(rlist2);
  term2->SetC(clist);
  term2->SetPt(plist);

  rlist1->clear();
  rlist2->clear();
  clist->clear();
  plist->clear();
  
  cout << "Recuperando os parametros do primeiro terminal" << endl;
  rlist1 = term1->GetR1();
  rlist2 = term1->GetR2();
  clist = term1->GetC();
  plist = term1->GetPt();
  
  it = rlist1->begin();
  cout << "Os valores na lista r1 para o primeiro terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = rlist2->begin();
  cout << "Os valores na lista r2 para o primeiro terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = clist->begin();
  cout << "Os valores na lista c para o primeiro terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = plist->begin();
  cout << "Os valores na lista p para o primeiro terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  
  cout << "Recuperando os parametros do segundo terminal" << endl;
  rlist1 = term2->GetR1();
  rlist2 = term2->GetR2();
  clist = term2->GetC();
  plist = term2->GetPt();

  it = rlist1->begin();
  cout << "Os valores na lista r1 para o segundo terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = rlist2->begin();
  cout << "Os valores na lista r2 para o segundo terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = clist->begin();
  cout << "Os valores na lista c para o segundo terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  it = plist->begin();
  cout << "Os valores na lista p para o segundo terminal são:" << *it++ << ", "<< *it++ << ", "<< *it++ << endl;
  
  rlist1->clear();
  rlist2->clear();
  clist->clear();
  plist->clear();

  term2->SetR1(rlist1);
  term2->SetR2(rlist2);
  term2->SetC(clist);
  term2->SetPt(plist);

  cout << "Recuperando os parametros do segundo terminal depois deles serem vaciados" << endl;
  rlist1 = term2->GetR1();
  rlist2 = term2->GetR2();
  clist = term2->GetC();
  plist = term2->GetPt();

  if (rlist1 || rlist2 || clist || plist )
    return 1;
  /*
   * Falta testear la clase vtkHM1DSegment!!!!
   */
  vtkHMNodeData *node1 = new vtkHMNodeData();
  vtkHMNodeData *node2 = new vtkHMNodeData();
  vtkHMNodeData *node3 = new vtkHMNodeData();
  vtkHMNodeData *tmpNode;
  
  node1->coords[0]    = 1.0;
  node1->coords[1]    = 1.0;
  node1->area         = 1.0;
  node1->radius       = 1.2;
  node1->elastin      = 1.3;
  node1->collagen     = 1.4;
  node1->wallThickness= 1.5;
  node1->pressure     = 1.6; 

  node2->coords[0]    = 2.0;
  node2->coords[1]    = 2.0;
  node2->area         = 2.0;
  node2->radius       = 2.2;
  node2->elastin      = 2.3;
  node2->collagen     = 2.4;
  node2->wallThickness= 2.5;
  node2->pressure     = 2.6; 

  node3->coords[0]    = 3.0;
  node3->coords[1]    = 3.0;
  node3->area         = 3.0;
  node3->radius       = 3.2;
  node3->elastin      = 3.3;
  node3->collagen     = 3.4;
  node3->wallThickness= 3.5;
  node3->pressure     = 3.6;
  
  cout << "Setando o comprimento e o numero de elementos em esgmento 1" << endl;
  segm1->SetLength(2.0);
  segm1->SetNumberOfElements(3);
  
  cout << "Setando os valores dos nôs no segmento 1" << endl;
  segm1->SetNodeData(1,*node1);
  segm1->SetNodeData(2,*node2);
  segm1->SetNodeData(3,*node3);

  cout << "Recuperando os valores dos nôs no segmento 1" << endl;
  tmpNode = segm1->GetNodeData(1);
  cout << " Primeiro nó:" << endl;
  cout << " node1->coords[0]:     " <<  tmpNode->coords[0] << endl;
  cout << " node1->coords[1]:     " <<  tmpNode->coords[1] << endl;
  cout << " node1->area:          " <<  tmpNode->area << endl;
  cout << " node1->radius:        " <<  tmpNode->radius << endl;
  cout << " node1->elastin:       " <<  tmpNode->elastin << endl;
  cout << " node1->collagen:      " <<  tmpNode->collagen << endl;
  cout << " node1->wallThickness: " <<  tmpNode->wallThickness << endl;
  cout << " node1->pressure:      " <<  tmpNode->pressure << endl;
  
  tmpNode = segm1->GetNodeData(2);
  cout << " Segundo nó:" << endl;
  cout << " node2->coords[0]:     " <<  tmpNode->coords[0] << endl;
  cout << " node2->coords[1]:     " <<  tmpNode->coords[1] << endl;
  cout << " node2->area:          " <<  tmpNode->area << endl;
  cout << " node2->radius:        " <<  tmpNode->radius << endl;
  cout << " node2->elastin:       " <<  tmpNode->elastin << endl;
  cout << " node2->collagen:      " <<  tmpNode->collagen << endl;
  cout << " node2->wallThickness: " <<  tmpNode->wallThickness << endl;
  cout << " node2->pressure:      " <<  tmpNode->pressure << endl;

  tmpNode = segm1->GetNodeData(3);
  cout << " Terceiro nó:" << endl;
  cout << " node3->coords[0]:     " <<  tmpNode->coords[0] << endl;
  cout << " node3->coords[1]:     " <<  tmpNode->coords[1] << endl;
  cout << " node3->area:          " <<  tmpNode->area << endl;
  cout << " node3->radius:        " <<  tmpNode->radius << endl;
  cout << " node3->elastin:       " <<  tmpNode->elastin << endl;
  cout << " node3->collagen:      " <<  tmpNode->collagen << endl;
  cout << " node3->wallThickness: " <<  tmpNode->wallThickness << endl;
  cout << " node3->pressure:      " <<  tmpNode->pressure << endl;

  tmpNode = segm1->GetNodeData(4);

  term1->Delete();
  term2->Delete();
  segm1->Delete();
}
