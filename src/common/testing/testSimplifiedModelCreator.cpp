/*
 * $Id: testSimplifiedModelCreator.cpp 286 2006-05-05 19:43:09Z nacho $
 */
#include "vtkHM1DStraightModelCreator.h"
#include "vtkHM1DTreeElement.h"
//#include "vtkHM1DSegment.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHMSimplifiedModel.h"

#include <stdio.h>


int main( int argc, char *argv[ ])
{
  vtkHM1DTreeElement *root;
  vtkHM1DStraightModel *model;
  vtkHM1DStraightModelCreator *modelCreator = vtkHM1DStraightModelCreator::New();
  
  model = (vtkHM1DStraightModel *) modelCreator->MakeNewSimplifiedModel();
  
  root = model->Get1DTreeRoot();

  cout<< "-> Agregando segmento à arvore." << endl;
  if (model->AddSegment(root->GetId()))
    cout<< "-> Segmento agregado com sucesso!." << endl;
  else
    cout<< "-> Ocorreu algum erro no agregado do segmento!." << endl;
 
  return 0;
}
