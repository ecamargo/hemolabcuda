/*
 * $Id: vtkHM1DBasParam.cxx 370 2006-05-16 18:26:05Z igor $
 */

#include "vtkHM1DBasParam.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <string>

vtkCxxRevisionMacro(vtkHM1DBasParam, "$Revision: 370 $");
vtkStandardNewMacro(vtkHM1DBasParam);

vtkHM1DBasParam::vtkHM1DBasParam()
{
	vtkDebugMacro(<<"Reading BasParam data...");
	this->ParamWriteSwicht = 0;
	this->Renumbering = 0;
	
	this->ConfigProcessExternal	= false;
	this->MaxIteration				= 0;
	this->Ralaxacao						= 0.0;
	this->QuantityStep				= 0;
	this->Norma								= NULL;
	this->Tolerance						= NULL;
	this->NameDegreeOfFreedom	= NULL;
	
	for (int i=0; i<3; i++)
		this->Nums[i] = 0;
	
	this->DelT							= 0.0;
	this->Tini							= 0.0;
	this->Tmax							= 0.0;
	this->TimeQuantityScreen		= 0;
	this->TimeQuantityFile			= 0;
	this->WriterOutput					= 0;
	this->OutputList					= NULL;
	this->QuantityOfDifferentElements	= 0;
	this->MaximumQuantityOfParameters	= 0;
	this->ResultantMatrixIsSymmetrical	= false;
	this->CalculationStepIsLinear		= false;
	this->MaxIterationSubStep			= 0;
	this->RalaxacaoSubStep				= 0.0;
	this->ResolveLibrary					= 0;
	this->MaxIterationOfResolver	= 0;
	this->ToleranceOfConvergence	= 0.0;
	this->SpacesOfKrylov					= 0;
	this->NumberOfCoefficients 		= 0;
	this->ToleranceOfExcuse 			= 0.0;
	this->ElementType 							= NULL;
	this->ElementTypeSubStepBubble	= NULL;
	this->ParamElementType 					= NULL;
	this->NormaSubStep 						= NULL;
	this->ToleranceSubStep 				= NULL;
	this->NameDegreeOfFreedomSubStep 	= NULL;
	
	for (int i=0; i<3; i++)
		this->NumsSubStep[i] = 0;
	
	this->InitialTime = 0;	
	
	//this->LogFilename = NULL;
	
	
	this->CoupledModel = 0;
	
	//this->SegregationType = 0;
	
	this->SegregatedSubStepOrder = 0;
	
	
	this->SuperSegregatedSubstepList = NULL;
	
	
	// BasParam como default serial
	this->SolverType = 0;

	this->ElementTypeSubStep3D = NULL;
	this->ToleranceSubStep1D = NULL;
	this->ToleranceSubStep1D = NULL;
	this->NormaSubStep1D = NULL;
	this->NormaSubStep3D = NULL;
	this->ToleranceSubStep3D = NULL;
	
	
}


//----------------------------------------------------------------------------

vtkHM1DBasParam::~vtkHM1DBasParam()
{
	// apagar a lista de vetores int dinamicamente alocados
	// this->SuperSegregatedSubstepList
	if (this->SuperSegregatedSubstepList)
		{
		ListOfIntArray::iterator it = this->SuperSegregatedSubstepList->begin();
		
		for (; it != this->SuperSegregatedSubstepList->end(); ++it)
			delete [] (*it);
		
		this->SuperSegregatedSubstepList = NULL;		
				
		}
	
	
//	if (this->NameDegreeOfFreedom)
//		{
//		delete this->NameDegreeOfFreedom;
//		this->NameDegreeOfFreedom = NULL;
//		}



	if (this->Norma)	
		{
		delete [] this->Norma;
		this->Norma = NULL;
		}
	
	if (this->Tolerance)
		{
		delete [] this->Tolerance;
		this->Tolerance = NULL;
		}
	
	if (this->OutputList)
		{
		delete [] this->OutputList;
		this->OutputList = NULL;
		}
		
	if (this->ElementTypeSubStepBubble)
		{		
		delete [] this->ElementTypeSubStepBubble;
		this->ElementTypeSubStepBubble = NULL;
		}
	
	if (this->ElementTypeSubStep3D)
		{
		delete []this->ElementTypeSubStep3D;
		this->ElementTypeSubStep3D = NULL;
		}
	
	if (this->ElementType)
		{
		delete [] this->ElementType;
		this->ElementType = NULL;
		}
	
	
	if (this->ParamElementType)
		{
		delete [] this->ParamElementType;
		this->ParamElementType = NULL;
		}
	
//	if (this->NormaSubStep)
//		{
//		delete [] this->NormaSubStep;
//		this->NormaSubStep = NULL;
//		}
	
//	if (this->ToleranceSubStep)
//		{
//		delete [] this->ToleranceSubStep;
//		this->ToleranceSubStep = NULL;
//		}
	
	if (this->NormaSubStep1D)
		{
		delete [] this->NormaSubStep1D;
		this->NormaSubStep1D = NULL;
		}
	
	if (this->ToleranceSubStep1D)
		{
		delete [] this->ToleranceSubStep1D;
		this->ToleranceSubStep1D = NULL;
		}
	
	if (this->NormaSubStep3D)
		{
		delete [] this->NormaSubStep3D;	
		this->NormaSubStep3D = NULL;
		}
	
	if (this->ToleranceSubStep3D)
		{
		delete [] this->ToleranceSubStep3D;
		this->ToleranceSubStep3D = NULL;
		}


}

//----------------------------------------------------------------------------

void vtkHM1DBasParam::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------

void vtkHM1DBasParam::DeepCopy(vtkHM1DBasParam *orig)
{
	this->ParamWriteSwicht					= orig->GetParamWriteSwicht();
	this->Renumbering 							= orig->GetRenumbering();
	this->ConfigProcessExternal			= orig->GetConfigProcessExternal();
	this->MaxIteration							= orig->GetMaxIteration();
	this->Ralaxacao									= orig->GetRalaxacao();
	this->QuantityStep							= orig->GetQuantityStep();
	this->Norma											= orig->GetNorma();
	this->Tolerance									= orig->GetTolerance();
	this->NameDegreeOfFreedom				= orig->GetNameDegreeOfFreedom();
	
	int *n = orig->GetNums();
	for (int i=0; i<3; i++)
		this->Nums[i] = n[i];
	this->DelT									= orig->GetDelT();
	this->Tini									= orig->GetTini();
	this->Tmax									= orig->GetTmax();
	this->TimeQuantityScreen		= orig->GetTimeQuantityScreen();
	this->TimeQuantityFile			= orig->GetTimeQuantityFile();
	this->WriterOutput					= orig->GetWriterOutput();
	this->OutputList						= orig->GetOutputList();
	this->QuantityOfDifferentElements		= orig->GetQuantityOfDifferentElements();
	this->MaximumQuantityOfParameters		= orig->GetMaximumQuantityOfParameters();
	this->ResultantMatrixIsSymmetrical	= orig->GetResultantMatrixIsSymmetrical();
	this->CalculationStepIsLinear		= orig->GetCalculationStepIsLinear();
	this->MaxIterationSubStep				= orig->GetMaxIterationSubStep();
	this->RalaxacaoSubStep					= orig->GetRalaxacaoSubStep();
	this->ResolveLibrary						= orig->GetResolveLibrary();
	this->MaxIterationOfResolver		= orig->GetMaxIterationOfResolver();
	this->ToleranceOfConvergence		= orig->GetToleranceOfConvergence();
	this->SpacesOfKrylov						= orig->GetSpacesOfKrylov();
	this->NumberOfCoefficients 			= orig->GetNumberOfCoefficients();
	this->ToleranceOfExcuse 				= orig->GetToleranceOfExcuse();
	this->ElementType 							= orig->GetElementType();
	this->ElementTypeSubStepBubble 	= orig->GetElementTypeSubStepBubble();
	
	this->ParamElementType 					= orig->GetParamElementType();
	this->NormaSubStep 							= orig->GetNormaSubStep();
	this->ToleranceSubStep 					= orig->GetToleranceSubStep();
	this->NameDegreeOfFreedomSubStep 	= orig->GetNameDegreeOfFreedomSubStep();
	
	n = orig->GetNumsSubStep();
	for (int i=0; i<3; i++)
		this->NumsSubStep[i] = n[i];
	
}

/*
//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetRenumbering()
{
	return this->Renumbering;
}

//----------------------------------------------------------------------------
bool vtkHM1DBasParam::GetConfigProcessExternal()
{
	return this->ConfigProcessExternal;
}

//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetMaxIteration()
{
	return this->MaxIteration;
}

//----------------------------------------------------------------------------
double vtkHM1DBasParam::GetRalaxacao()
{
	return this->Ralaxacao;
}

//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetQuantityStep()
{
	return this->QuantityStep;
}
*/
//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetNorma()
{
	return this->Norma;
}

//----------------------------------------------------------------------------
void vtkHM1DBasParam::SetNorma(double *n)
{
	this->Norma = n;
}

//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetTolerance()
{
	return this->Tolerance;
}

//----------------------------------------------------------------------------
void vtkHM1DBasParam::SetTolerance(double *t)
{
	this->Tolerance = t;
}

//----------------------------------------------------------------------------
string *vtkHM1DBasParam::GetNameDegreeOfFreedom()
{
	return this->NameDegreeOfFreedom;
}

//----------------------------------------------------------------------------
void vtkHM1DBasParam::SetNameDegreeOfFreedom(string *n)
{
	this->NameDegreeOfFreedom = n;
}

//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetNums()
{
	return this->Nums;
}

//----------------------------------------------------------------------------
void vtkHM1DBasParam::SetNums(int n[3])
{
	this->Nums[0] = n[0];
	this->Nums[1] = n[1];
	this->Nums[2] = n[2];
}
/*
//----------------------------------------------------------------------------
double vtkHM1DBasParam::GetDelT()
{
	return this->DelT;
}

//----------------------------------------------------------------------------
double vtkHM1DBasParam::GetTini()
{
	return this->Tini;
}

//----------------------------------------------------------------------------
double vtkHM1DBasParam::GetTmax()
{
	return this->Tmax;
}

//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetTimeQuantityScreen()
{
	return this->TimeQuantityScreen;
}

//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetTimeQuantityFile()
{
	return this->TimeQuantityFile;
}

//----------------------------------------------------------------------------
int vtkHM1DBasParam::GetWriterOutput()
{
	return this->WriterOutput;
}
*/
//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetOutputList()
{
	return this->OutputList;
}

void vtkHM1DBasParam::SetOutputList(int *o)
{
	this->OutputList = o;
}

//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetElementType()
{
	return this->ElementType;
}
//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetElementType(int *e)
{
	this->ElementType = e;
}

//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetElementTypeSubStepBubble()
{
	return this->ElementTypeSubStepBubble;
}
//----------------------------------------------------------------------------

void vtkHM1DBasParam::SetElementTypeSubStepBubble(int *e)
{
	this->ElementTypeSubStepBubble = e;
}

//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetElementTypeSubStep3D()
{
	return this->ElementTypeSubStep3D;
}
//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetElementTypeSubStep3D(int *e)
{
	this->ElementTypeSubStep3D = e;
}

//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetParamElementType()
{
	return this->ParamElementType;
}

//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetParamElementType(double *p)
{
	this->ParamElementType = p;
}

//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetNormaSubStep()
{
	return this->NormaSubStep;
}

void vtkHM1DBasParam::SetNormaSubStep(double *n)
{
	this->NormaSubStep = n;
}
//----------------------------------------------------------------------------


double *vtkHM1DBasParam::GetNormaSubStep1D()
{
	return this->NormaSubStep1D;
}

//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetNormaSubStep1D(double *n)
{
	this->NormaSubStep1D = n;
}

//----------------------------------------------------------------------------

double *vtkHM1DBasParam::GetNormaSubStep3D()
{
	return this->NormaSubStep3D;
}

//----------------------------------------------------------------------------

void vtkHM1DBasParam::SetNormaSubStep3D(double *n)
{
	this->NormaSubStep3D = n;
}


//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetToleranceSubStep1D()
{
	return this->ToleranceSubStep1D;
}

//----------------------------------------------------------------------------

void vtkHM1DBasParam::SetToleranceSubStep1D(double *t)
{
	this->ToleranceSubStep1D = t;
}

//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetToleranceSubStep3D()
{
	return this->ToleranceSubStep3D;
}

//----------------------------------------------------------------------------

void vtkHM1DBasParam::SetToleranceSubStep3D(double *t)
{
	this->ToleranceSubStep3D = t;
}


//----------------------------------------------------------------------------
double *vtkHM1DBasParam::GetToleranceSubStep()
{
	return this->ToleranceSubStep;
}

//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetToleranceSubStep(double *t)
{
	this->ToleranceSubStep = t;
}

//----------------------------------------------------------------------------
string *vtkHM1DBasParam::GetNameDegreeOfFreedomSubStep()
{
	return this->NameDegreeOfFreedomSubStep;
}

void vtkHM1DBasParam::SetNameDegreeOfFreedomSubStep(string *n)
{
	this->NameDegreeOfFreedomSubStep = n;
}

//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetNumsSubStep()
{
	return this->NumsSubStep;
}

void vtkHM1DBasParam::SetNumsSubStep(int n[3])
{
	this->NumsSubStep[0] = n[0];
	this->NumsSubStep[1] = n[1];
	this->NumsSubStep[2] = n[2];
}

//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetLogFilename(const char *Filename)
{
	strcpy(LogFilename, Filename);
}

//----------------------------------------------------------------------------

	
const char *vtkHM1DBasParam::GetLogFilename()
{
	return 	this->LogFilename;
}
//----------------------------------------------------------------------------
int *vtkHM1DBasParam::GetSubStepSuperSegregated(int Substep3dOrder)
{
	
	ListOfIntArray::iterator it = this->SuperSegregatedSubstepList->begin();
	//ListOfInt::iterator it = GroupFromPoint.begin();
	int counter = 0;
	
	for (; it != this->SuperSegregatedSubstepList->end(); ++it)
		{
		
		if (Substep3dOrder == counter)
			return (*it);	
		counter++;
		}
	
	
	//return this->SuperSegregatedSubstepList;
}

//----------------------------------------------------------------------------


void vtkHM1DBasParam::SetSubStepSuperSegregated(ListOfIntArray *List)
{
	this->SuperSegregatedSubstepList = List;
}

//----------------------------------------------------------------------------
