// .NAME vtkHM1DBasParam - BasParam data file
// .SECTION Description
// vtkHM1DBasParam is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also

#ifndef BASPARAM_H_
#define BASPARAM_H_

#include "vtkDataObject.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

   
#include <string>
#include <list>
using namespace std;
	
class VTK_EXPORT vtkHM1DBasParam : public vtkDataObject
{
public:

	//BTX
	typedef std::list<int *> ListOfIntArray;
	//ETX

	static vtkHM1DBasParam *New();
	vtkTypeRevisionMacro(vtkHM1DBasParam,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHM1DBasParam *orig);
	
	// Description:
	// Set/Get Param Write Swicht	
	vtkSetMacro(ParamWriteSwicht,int);
	vtkGetMacro(ParamWriteSwicht,int);
	
	// Description:
	// Set/Get Renumbering	
	vtkSetMacro(Renumbering,int);
	vtkGetMacro(Renumbering,int);
	
	// Description:
	// Set/Get ConfigProcessExternal
	vtkSetMacro(ConfigProcessExternal,bool);
	vtkGetMacro(ConfigProcessExternal,bool);
	
	// Description:
	// Set/Get MaxIteration
	vtkSetMacro(MaxIteration,int);
	vtkGetMacro(MaxIteration,int);
	
	// Description:
	// Set/Get Ralaxacao
	vtkSetMacro(Ralaxacao,double);
	vtkGetMacro(Ralaxacao,double);
	
	// Description:
	// Set/Get QuantityStep
	vtkSetMacro(QuantityStep,int);
	vtkGetMacro(QuantityStep,int);
	
	// Description:
	// Set/Get Norma
	double *GetNorma();
	void SetNorma(double *n);
	
	// Description:
	// Set/Get Tolerance
	double *GetTolerance();
	void SetTolerance(double *t);
	
	// Description:
	// Set/Get NameDegreeOfFreedom
	string *GetNameDegreeOfFreedom();
	void SetNameDegreeOfFreedom(string *n);
	
	// Description:
	// Set/Get Nums
	int *GetNums();
	void SetNums(int n[3]);
	
	// Description:
	// Set/Get DelT, Tini, Tmax
	vtkSetMacro(DelT,double);
	vtkGetMacro(DelT,double);
	
	vtkSetMacro(Tini,double);
	vtkGetMacro(Tini,double);
	
	vtkSetMacro(Tmax,double);
	vtkGetMacro(Tmax,double);
	
	// Description:
	// Set/Get TimeQuantityScreen
	vtkSetMacro(TimeQuantityScreen,int);
	vtkGetMacro(TimeQuantityScreen,int);
	
	// Description:
	// Set/Get TimeQuantityFile
	vtkSetMacro(TimeQuantityFile,int);
	vtkGetMacro(TimeQuantityFile,int);
	
	// Description:
	// Set/Get WriterOutput
	vtkSetMacro(WriterOutput,int);
	vtkGetMacro(WriterOutput,int);
	
	// Description:
	// Set/Get OutputList
	int *GetOutputList();
	void SetOutputList(int *o);
	
	// Description:
	// Set/Get QuantityOfDifferentElements
	vtkSetMacro(QuantityOfDifferentElements,int);
	vtkGetMacro(QuantityOfDifferentElements,int);
	
	// Description:
	// Set/Get MaximumQuantityOfParameters
	vtkSetMacro(MaximumQuantityOfParameters,int);
	vtkGetMacro(MaximumQuantityOfParameters,int);
	
	// Description:
	// Set/Get ResultantMatrixIsSymmetrical
	vtkSetMacro(ResultantMatrixIsSymmetrical,bool);
	vtkGetMacro(ResultantMatrixIsSymmetrical,bool);
	
	// Description:
	// Set/Get CalculationStepIsLinear
	vtkSetMacro(CalculationStepIsLinear,bool);
	vtkGetMacro(CalculationStepIsLinear,bool);
	
		// Description:
	// Set/Get ResultantMatrixIsSymmetrical
	vtkSetMacro(ResultantMatrixIsSymmetricalSubStepBubble,bool);
	vtkGetMacro(ResultantMatrixIsSymmetricalSubStepBubble,bool);
	
	// Description:
	// Set/Get CalculationStepIsLinear
	vtkSetMacro(CalculationStepIsLinearSubStepBubble,bool);
	vtkGetMacro(CalculationStepIsLinearSubStepBubble,bool);
	
	
	// Description:
	// Set/Get CalculationStepIsLinear
	vtkSetMacro(ResolveLibrarySubStepBubble,int);
	vtkGetMacro(ResolveLibrarySubStepBubble,int);
	

	// Description:
	//
	int *GetElementTypeSubStepBubble();

	// Description:
	//
	void 	SetElementTypeSubStepBubble(int *e);


	// Description:
	//
	vtkSetMacro(MaxIterationSubStep3D,int);
	vtkGetMacro(MaxIterationSubStep3D,int);

	// Description:
	//
	vtkSetMacro(RalaxacaoSubStep3D,double);
	vtkGetMacro(RalaxacaoSubStep3D,double);

	// Description:
	// Set/Get ResultantMatrixIsSymmetrical
	vtkSetMacro(ResultantMatrixIsSymmetricalSubStep3D,bool);
	vtkGetMacro(ResultantMatrixIsSymmetricalSubStep3D,bool);
	
	// Description:
	// Set/Get CalculationStepIsLinear
	vtkSetMacro(CalculationStepIsLinearSubStep3D,bool);
	vtkGetMacro(CalculationStepIsLinearSubStep3D,bool);
	
	
	// Description:
	// Set/Get CalculationStepIsLinear
	vtkSetMacro(ResolveLibrarySubStep3D,int);
	vtkGetMacro(ResolveLibrarySubStep3D,int);
	

	// Description:
	//
	int *GetElementTypeSubStep3D();

	// Description:
	//
	void 	SetElementTypeSubStep3D(int *e);

	// Description:
	// Set/Get MaxIterationSubStep
	vtkSetMacro(MaxIterationSubStep,int);
	vtkGetMacro(MaxIterationSubStep,int);
	
	// Description:
	// Set/Get RalaxacaoSubStep
	vtkSetMacro(RalaxacaoSubStep,double);
	vtkGetMacro(RalaxacaoSubStep,double);
	
	// Description:
	// Set/Get ResolveLibrary
	vtkSetMacro(ResolveLibrary,int);
	vtkGetMacro(ResolveLibrary,int);
	
	// Description:
	// Set/Get MaxIterationOfResolver
	vtkSetMacro(MaxIterationOfResolver,int);
	vtkGetMacro(MaxIterationOfResolver,int);
	
	// Description:
	// Set/Get ToleranceOfConvergence
	vtkSetMacro(ToleranceOfConvergence,double);
	vtkGetMacro(ToleranceOfConvergence,double);
	
	// Description:
	// Set/Get SpacesOfKrylov
	vtkSetMacro(SpacesOfKrylov,int);
	vtkGetMacro(SpacesOfKrylov,int);
	
	// Description:
	// Set/Get NumberOfCoefficients
	vtkSetMacro(NumberOfCoefficients,int);
	vtkGetMacro(NumberOfCoefficients,int);
	
	// Description:
	// Set/Get ToleranceOfExcuse
	vtkSetMacro(ToleranceOfExcuse,double);
	vtkGetMacro(ToleranceOfExcuse,double);
	
	// Description:
	// Set/Get ElementType
	int *GetElementType();
	void SetElementType(int *e);
	
	
	// Description:
	// Set/Get ParamElementType
	double *GetParamElementType();
	void SetParamElementType(double *p);
	
	// Description:
	// Set/Get NormaSubStep
	double *GetNormaSubStep();
	void SetNormaSubStep(double *n);
	
	// Description:
	//
	void SetNormaSubStep1D(double *n);

	// Description:
	//
	double *GetNormaSubStep1D();
	
	// Description:
	//
	void SetNormaSubStep3D(double *n);

	// Description:
	//
	double *GetNormaSubStep3D();


	// Description:
	//
	void SetToleranceSubStep1D(double *n);

	// Description:
	//
	double *GetToleranceSubStep1D();


	// Description:
	//
	void SetToleranceSubStep3D(double *n);

	// Description:
	//
	double *GetToleranceSubStep3D();
	
	
	// Description:
	// Set/Get ToleranceSubStep
	double *GetToleranceSubStep();
	void SetToleranceSubStep(double *t);
	
	// Description:
	// Set/Get NameDegreeOfFreedomSubStep
	string *GetNameDegreeOfFreedomSubStep();
	void SetNameDegreeOfFreedomSubStep(string *n);
	
	// Description:
	// Set/Get NumsSubStep
	int *GetNumsSubStep();
	void SetNumsSubStep(int n[3]);
	
	// Description:
	//
	// Set/Get InitialTime
	vtkSetMacro(InitialTime,int);
	vtkGetMacro(InitialTime,int);
	
	// Description:
	//
	void SetLogFilename(const char *Filename);

	// Description:
	//
	const char *GetLogFilename();
	
	// Description:
	//
	vtkSetMacro(CoupledModel,int);
	vtkGetMacro(CoupledModel,int);
	
	
	// Description:
	//
	vtkSetMacro(CouplingScheme,int);
	vtkGetMacro(CouplingScheme,int);

	// Description:
	//
	vtkSetMacro(SegregatedSubStepOrder,int);
	vtkGetMacro(SegregatedSubStepOrder,int);
	
	// Description:
	//
	vtkSetMacro(MaxIterationOfResolver3D,int);
	vtkGetMacro(MaxIterationOfResolver3D,int);

	// Description:
	//
	vtkSetMacro(ToleranceOfConvergence3D,double);
	vtkGetMacro(ToleranceOfConvergence3D,double);

	// Description:
	//
	vtkSetMacro(SpacesOfKrylov3D,int);
	vtkGetMacro(SpacesOfKrylov3D,int);

	// Description:
	//
	vtkSetMacro(NumberOfCoefficients3D,int);
	vtkGetMacro(NumberOfCoefficients3D,int);
		
	// Description:
	//
	vtkSetMacro(ToleranceOfExcuse3D,double);
	vtkGetMacro(ToleranceOfExcuse3D,double);
		
	// Description:
	//
	vtkSetMacro(NumberOfSubsteps,int);
	vtkGetMacro(NumberOfSubsteps,int);
	

	// Description:
	//
	int *GetSubStepSuperSegregated(int Substep3dOrder);

	// Description:
	//
	void SetSubStepSuperSegregated(ListOfIntArray *List);
	
	
	
	// Description:
	//
	vtkSetMacro(SolverType,int);
	vtkGetMacro(SolverType,int);
	
	
	// Description:
	//
	vtkSetMacro(ParallelSolverNumberOfProcessors,int);
	vtkGetMacro(ParallelSolverNumberOfProcessors,int);
	

	// Description:
	//
	vtkSetMacro(ParallelSolverRLS,int);
	vtkGetMacro(ParallelSolverRLS,int);


	// Description:
	//
	vtkSetMacro(ParallelSolverGMRESIterations,int);
	vtkGetMacro(ParallelSolverGMRESIterations,int);


	// Description:
	//
	vtkSetMacro(ParallelSolverMaxIterations,int);
	vtkGetMacro(ParallelSolverMaxIterations,int);
	

	// Description:
	//
	vtkSetMacro(ParallelSolverPreConditioning,int);
	vtkGetMacro(ParallelSolverPreConditioning,int);
	

	// Description:
	//
	vtkSetMacro(ParallelSolverRelativeConvError,double);
	vtkGetMacro(ParallelSolverRelativeConvError,double);


	// Description:
	//
	vtkSetMacro(ParallelSolverAbsConvError,double);
	vtkGetMacro(ParallelSolverAbsConvError,double);
	
	
	// Description:
	//
	vtkSetMacro(DoFNumber,int);
	vtkGetMacro(DoFNumber,int);
	
	
//**************************************************************************************************	
	
	
protected:
	vtkHM1DBasParam();
	~vtkHM1DBasParam();
	
	// Description:
	//
	//Indicates if write or not the calculated parameters inside the file Param.txt
	int ParamWriteSwicht;
	
	// Description:
	//
	//Indicates if it carries through renumeration
	//or not of the nodo of the matrix 
	int Renumbering;
	
	// Description:
	//
	//Detemine if the process most exteral is linear or not
	bool ConfigProcessExternal;
	
	// Description:
	//
	//Max number of iterations
	int MaxIteration;
	
	// Description:
	//
	//Indicate the parameter of ralaxacao
	//for the solution between iteration
	double  Ralaxacao;
	
	// Description:
	//
	//Indicate the quantity of steps of time
	//takes in account in the calculation
	int QuantityStep;
	
	// Description:
	//
	//Values of reference front which goes to be
	//calculated the error with relation the
	//norms established for the case where the
	//process are iterative. 
	double *Norma;
	
	// Description:
	//
	//Values of the tolerances that will be used to
	//check the convergence of the iterative process. 
	double *Tolerance;
	
	// Description:
	//
	//Names of the degree of liberty of the problem
	string *NameDegreeOfFreedom;
	
	// Description:
	//
	int Nums[3];
	
	// Description:
	//
	//Indicate the parameters that characterize
	//the transient problem, being respectively: 
	double DelT;  //Step of time
	double Tini;  //Time of beginning of the round
	double Tmax;  //Time of finishing of the round
	
	// Description:
	//
	//Quantity of steps of time between out in the screem
	int TimeQuantityScreen; 
	
	// Description:
	//
	//Quantity of steps of time between
	//out in the file DataOut.txt.
	int TimeQuantityFile;
	
	// Description:
	//
	//Indicates for which nodes goes to be carried
	//through the writing of the results 
	int WriterOutput;
	
	// Description:
	//
	//List of nodes that they are asked for in the exit
	//of the program case WriterOutput is greater that zero 
	int *OutputList;
	
	// Description:
	//
	//Indicate the quantity of different elements to be used
	int QuantityOfDifferentElements;
	
	// Description:
	//
	//Indicate the maximum quatity of parameters
	//that an element possess
	int MaximumQuantityOfParameters;
	
	// Description:
	//
	//Indicate if the resultant matrix is symmetrical or not
	bool ResultantMatrixIsSymmetrical;
	
	// Description:
	//
	//Indicate if the step of calculate is linear or not
	bool CalculationStepIsLinear;
	

	// Description:
	//
  // parameters used in substep Bubble

	// Description:
	//
	// used when to write the bubble substeps
	//Indicate if the resultant matrix is symmetrical or not
	bool ResultantMatrixIsSymmetricalSubStepBubble;
	
	// Description:
	//
	// used when to write the bubble substeps
	//Indicate if the step of calculate is linear or not
	bool CalculationStepIsLinearSubStepBubble;

	// Description:
	//
	// used when to write the bubble substeps
	//Indicate the resolver of the library of resolvers
	int ResolveLibrarySubStepBubble;

	// Description:
	//
	// used when to write the bubble substeps
	int *ElementTypeSubStepBubble;
  
  // end 


  // parameters used in substep 3d - used only in the coupled model

	// Description:
	//
	int MaxIterationSubStep3D;

	// Description:
	//
	double RalaxacaoSubStep3D;
	
	
	// Description:
	//
	// used when to write the bubble substeps
	//Indicate if the resultant matrix is symmetrical or not
	bool ResultantMatrixIsSymmetricalSubStep3D;
	
	// Description:
	//
	// used when to write the bubble substeps
	//Indicate if the step of calculate is linear or not
	bool CalculationStepIsLinearSubStep3D;

	// Description:
	//
	// used when to write the bubble substeps
	//Indicate the resolver of the library of resolvers
	int ResolveLibrarySubStep3D;

	// Description:
	//
	// used when to write the bubble substeps
	int *ElementTypeSubStep3D;
  
  // end 


	// Description:
	//
	//Number maximum of iterations SubStep
	int MaxIterationSubStep;
	
	// Description:
	//
	//Indicate the parameter of ralaxacao
	//for the solution between iteration SubStep
	double  RalaxacaoSubStep;
	
	// Description:
	//
	//Indicate the resolver of the library of resolvers
	int ResolveLibrary;
	
	// Description:
	//
	//Indicate the quantity of maximum iterations
	//to be carried through for the iterative resolver 
	int MaxIterationOfResolver;
	
	// Description:
	//
	//Indicate the tolerance of the convergence
	//of the iterative resolver
	double ToleranceOfConvergence;
	
	// Description:
	//
	//Indicate a parameter for characterize
	//the spaces of Krylov
	int SpacesOfKrylov;
	
	// Description:
	//
	//Indicate the number of coefficients of the matrix
	//taken to get a preconditioning matrix 
	int NumberOfCoefficients;
	
	// Description:
	//
	//Indicate a tolerance to excuse
	//coefficients of the matrix
	double ToleranceOfExcuse;
	
	// Description:
	//
	//Types of elements and parameters
	int *ElementType;
	
	
	// Description:
	//
	//Parameters of the types of elements
	double *ParamElementType;
	
	// Description:
	//
	//Values of reference front which goes to be calculated
	//the errors with relation the norms established
	//for the case where the process are iterative.
	double *NormaSubStep;
	
	// Description:
	//
	//Values of the tolerances that will be used for
	//check the convergence of the iterative process.
	double *ToleranceSubStep;
	
	
	// Description:
	//
	double *NormaSubStep1D;
	
	// Description:
	//
	//Values of the tolerances that will be used for
	//check the convergence of the iterative process.
	double *ToleranceSubStep1D;
	
	
	// Description:
	//
	double *NormaSubStep3D;
	
	// Description:
	//
	//Values of the tolerances that will be used for
	//check the convergence of the iterative process.
	double *ToleranceSubStep3D;
	
	// Description:
	//
	//Names of the degree of liberty of the problem SubStep
	string *NameDegreeOfFreedomSubStep;

	// Description:
	//
	int NumsSubStep[3];
	
	// Description:
	//
	// Indica se retoma uma rodada a partir do tempo indicado no IniFile.txt 
	int InitialTime;
	
	
	// Description:
	//
	char LogFilename[20];
	
	// Description:
	//
	// 0: 3D stand alone model e 1: CoupledModel
	int CoupledModel;
	
	// Description:
	//
	// 0 -- monolitico e 1 - segregated
	int CouplingScheme;
	
	
	// Description:
	//
	// 0: 1d-3d e 1: 3d-1d
	int SegregatedSubStepOrder;
	
	// Description:
	//
	int MaxIterationOfResolver3D;
		
	// Description:
	//
	double ToleranceOfConvergence3D; 
		
	// Description:
	//
	int SpacesOfKrylov3D;
		
	// Description:
	//
	int NumberOfCoefficients3D;
		
	// Description:
	//
	double ToleranceOfExcuse3D; 
	
	// Description:
	//
	int NumberOfSubsteps;
	
	//BTX
	ListOfIntArray *SuperSegregatedSubstepList;
	//ETX
	
	// Description:
	//
	int SolverType; // 0: Sequential 1: Parallel
	
	// Description:
	// Atributes used only when the solver type is parallel
	int ParallelSolverNumberOfProcessors;
	int ParallelSolverRLS;
	int ParallelSolverGMRESIterations;
	int ParallelSolverMaxIterations;
	int ParallelSolverPreConditioning;
	double ParallelSolverRelativeConvError;
	double ParallelSolverAbsConvError;
	//
	
	// Description:
	// store the number of degrees of freedom from current model	
	int DoFNumber;
	
	
}; //End class


#endif /*BASPARAM_H_*/
