#include "vtkHM1DStraightModel.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFile.h"
#include "vtkHMDataOut.h"
#include "vtkHM1DData.h"

#include "vtkDataObject.h"
#include "vtkCellArray.h"
#include "vtkFieldData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPolyData.h"
#include "vtkStreamingDemandDrivenPipeline.h"

vtkCxxRevisionMacro(vtkHM1DData, "$Revision: 444 $");
vtkStandardNewMacro(vtkHM1DData);

vtkHM1DData::vtkHM1DData()
{
	this->StraightModel = NULL;
	this->Mesh = NULL;
	this->BasParam = NULL;
	this->Param = NULL;
	this->IniFile = NULL;
	this->DataOut = NULL;

	this->FullModel = NULL;

	//default as 1d model
	this->ModelType = 0;

	this->ElementTypeArray = NULL;
	this->ElementMatArray = NULL;
}

vtkHM1DData::~vtkHM1DData()
{
	if (this->ElementTypeArray)
		this->ElementTypeArray->Delete();
	this->ElementTypeArray = NULL;

	if (this->ElementMatArray)
		this->ElementMatArray->Delete();
	this->ElementMatArray = NULL;
}

void vtkHM1DData::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetMesh(vtkHM1DMesh *mesh)
{
	this->Mesh = mesh;
}

//----------------------------------------------------------------------------
vtkHM1DMesh *vtkHM1DData::GetMesh()
{
	return this->Mesh;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetBasParam(vtkHM1DBasParam *BP)
{
	this->BasParam = BP;
}

//----------------------------------------------------------------------------
vtkHM1DBasParam *vtkHM1DData::GetBasParam()
{
	return this->BasParam;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetParam(vtkHM1DParam *p)
{
	this->Param =p;
}

//----------------------------------------------------------------------------
vtkHM1DParam *vtkHM1DData::GetParam()
{
	return this->Param;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetIniFile(vtkHM1DIniFile *IF)
{
	this->IniFile = IF;
}

//----------------------------------------------------------------------------
vtkHM1DIniFile *vtkHM1DData::GetIniFile()
{
	return this->IniFile;
}

//----------------------------------------------------------------------------
vtkHM1DStraightModel *vtkHM1DData::GetStraightModel()
{
	return this->StraightModel;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetDataOut(vtkHMDataOut *DO)
{
	this->DataOut = DO;
}

//----------------------------------------------------------------------------
vtkHMDataOut *vtkHM1DData::GetDataOut()
{
	return this->DataOut;
}

//----------------------------------------------------------------------------

void vtkHM1DData::Set3DFullModel(vtkHM3DFullModel *FullModel)
{
	this->FullModel = FullModel;
}

//----------------------------------------------------------------------------

vtkHM3DFullModel *vtkHM1DData::Get3DFullModel()
{
	return this->FullModel;
}
//----------------------------------------------------------------------------
void vtkHM1DData::SetModelInfo(vtkIdList *list)
{
	this->ModelInfo = list;
}

//----------------------------------------------------------------------------


vtkIdList *vtkHM1DData::GetModelInfo()
{
	return this->ModelInfo;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetElementTypeArray(vtkIntArray *array)
{
	if ( !this->ElementTypeArray)
		this->ElementTypeArray = vtkIntArray::New();
	this->ElementTypeArray->DeepCopy(array);
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DData::GetElementTypeArray()
{
	return this->ElementTypeArray;
}

//----------------------------------------------------------------------------
void vtkHM1DData::SetElementMatArray(vtkIntArray *array)
{
	if (array)
		{
		if ( !this->ElementMatArray)
			this->ElementMatArray = vtkIntArray::New();
		this->ElementMatArray->DeepCopy(array);
		} else
		{
		if (this->ElementMatArray)
			this->ElementMatArray->Delete();
		this->ElementMatArray = NULL;
		}
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DData::GetElementMatArray()
{
	return this->ElementMatArray;
}
