#ifndef VTKHM1DDATA_H_
#define VTKHM1DDATA_H_

#include "vtkDataObject.h"
#include "vtkIdList.h"
#include "vtkIntArray.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM1DIniFile;
class vtkHMDataOut;
class vtkHM1DStraightModel;
class vtkHM3DFullModel;

class VTK_EXPORT vtkHM1DData : public vtkDataObject
{
public:
	static vtkHM1DData *New();
	vtkTypeRevisionMacro(vtkHM1DData,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description :
	// Set/Get MeshReader
	void SetMesh(vtkHM1DMesh *mesh);	
	vtkHM1DMesh *GetMesh();
	
	// Description :
	// Set/Get BasParamReader
	void SetBasParam(vtkHM1DBasParam *BP);	
	vtkHM1DBasParam *GetBasParam();
	
	// Description :
	// Set/Get ParamReader
	void SetParam(vtkHM1DParam *p);	
	vtkHM1DParam *GetParam();
	
	// Description :
	// Set/Get IniFileReader
	void SetIniFile(vtkHM1DIniFile *IF);	
	vtkHM1DIniFile *GetIniFile();
	
	// Description :
	// Set/Get DataOutReader
	void SetDataOut(vtkHMDataOut *DO);	
	vtkHMDataOut *GetDataOut();
	
	// Description :
	// Get StraightModel
	vtkHM1DStraightModel *GetStraightModel();
	
	
	// Description :
	// Set/Get 3DFullModel
	void Set3DFullModel(vtkHM3DFullModel *FullModel);	
	vtkHM3DFullModel *Get3DFullModel();
	
	// Description :
	// Set/Get ModeType
	vtkSetMacro(ModelType, int);
	vtkGetMacro(ModelType, int);
	
	// Description :
	// Set/Get ModelInfo
	void SetModelInfo(vtkIdList *list);
	vtkIdList *GetModelInfo();
	
	// Description:
	// Set/Get element type array.
	void SetElementTypeArray(vtkIntArray *array);
	vtkIntArray *GetElementTypeArray();
	
	// Description:
	// Set/Get element mat array.
	void SetElementMatArray(vtkIntArray *array);
	vtkIntArray *GetElementMatArray();
	
protected:
	vtkHM1DData();
	~vtkHM1DData();

	// Description:
	//Object of the tree 1D
	vtkHM1DStraightModel *StraightModel;
	
	// Description:
	//Object for read data of the Mesh.txt file
	vtkHM1DMesh *Mesh;
	
	// Description:
	//Object for read data of the BasParam.txt file
	vtkHM1DBasParam *BasParam;
	
	// Description:
	//Object for read data of the Param.txt file
	vtkHM1DParam *Param;
	
	// Description:
	//Object for read data of the IniFile.txt file
	vtkHM1DIniFile *IniFile;
	
	// Description:
	//Object for read data of the DataOut.txt file
	vtkHMDataOut *DataOut;
	
	// object used to write generalinformation about stand alone 3d model
	vtkHM3DFullModel *FullModel;
	
	// Description:
	// 0  1d model
	// 1  3d model
	// 2  CoupledModel
	int ModelType;
	
	// Description:
	// data used to write model (3d or coupled) general information 
	vtkIdList *ModelInfo;
  
	// Description:
	// Array with element type of the 1D model coupled to 3D model.
	// Positive value, indicate that the element isn't inside of the 3D model.
	// Negative value, indicate that the element is inside of the 3D model consequently, in mesh.txt, the element is null(99).
	// Possible values: 1, 3, 4 for element and 2 for terminal and your respective negative values.
	vtkIntArray *ElementTypeArray;
	
	// Description:
	// Array with element mat of the 1D model coupled to 3D model.
	vtkIntArray *ElementMatArray;
	
};

#endif /*VTKHM1DDATA_H_*/
