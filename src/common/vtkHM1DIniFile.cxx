#include "vtkHM1DIniFile.h"
#include "vtkHM1DMesh.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkDoubleArray.h"

#include <fstream>

#include <sys/stat.h>


vtkCxxRevisionMacro(vtkHM1DIniFile, "$Revision: 370 $");
vtkStandardNewMacro(vtkHM1DIniFile);

vtkHM1DIniFile::vtkHM1DIniFile()
{
	vtkDebugMacro(<<"Reading IniFile data...");
	this->InitialConditions = vtkDoubleArray::New();
	this->CurrentTimeStep = 0.0;
	this->DeltaT = 0.0;
}

//----------------------------------------------------------------------------
vtkHM1DIniFile::~vtkHM1DIniFile()
{
	if ( this->InitialConditions )
		this->InitialConditions->Delete();
}

//----------------------------------------------------------------------------
void vtkHM1DIniFile::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

void vtkHM1DIniFile::DeepCopy(vtkHM1DIniFile *orig)
{
	if ( this->InitialConditions )
	{
		this->InitialConditions->Reset();
		this->InitialConditions->DeepCopy(orig->GetInitialConditions());
	}
	else
	{
		this->InitialConditions = vtkDoubleArray::New();
		this->InitialConditions->DeepCopy(orig->GetInitialConditions());
	}
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHM1DIniFile::GetInitialConditions()
{
	return this->InitialConditions;
}

void vtkHM1DIniFile::SetInitialConditions(vtkDoubleArray *i)
{
	this->InitialConditions->DeepCopy(i);
}
