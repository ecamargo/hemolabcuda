// .NAME vtkHM1DIniFile - IniFile data file
// .SECTION Description
// vtkHM1DIniFile is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also

#ifndef VTKHM1DINIFILE_H_
#define VTKHM1DINIFILE_H_

#include "vtkDataObject.h"

class vtkDoubleArray;

class VTK_EXPORT vtkHM1DIniFile : public vtkDataObject
{
public:
	static vtkHM1DIniFile *New();
	vtkTypeRevisionMacro(vtkHM1DIniFile,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHM1DIniFile *orig);
	
	// Description:
	// Set/Get InitialConditions
	vtkDoubleArray *GetInitialConditions();
	void SetInitialConditions(vtkDoubleArray *i);
	
	
	// Description:
	// Set/Get macro for CurrentTime and DeltaTime
	vtkSetMacro(CurrentTimeStep, double);
	vtkGetMacro(CurrentTimeStep, double);
	
	vtkSetMacro(DeltaT, double);
	vtkGetMacro(DeltaT, double);
	
	
	
protected:
	vtkHM1DIniFile();
	~vtkHM1DIniFile();
	
	// Description:
	// Initial condition of the round
	vtkDoubleArray *InitialConditions;
	
	
	//Description:
	// inifile data correspondent time 
	double CurrentTimeStep;
	
	//Description:
	// last time step used
	double DeltaT;
	
	
	
	
}; //End class

#endif /*VTKHM1DINIFILE_H_*/
