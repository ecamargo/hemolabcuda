#include "vtkHM1DMesh.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPointSet.h"
#include "vtkObject.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"

vtkCxxRevisionMacro(vtkHM1DMesh, "$Revision: 311 $");
vtkStandardNewMacro(vtkHM1DMesh);

vtkHM1DMesh::vtkHM1DMesh()
{
	vtkDebugMacro(<<"Mesh...");
	
	this->points = vtkPoints::New();
	this->DirichletsTag = vtkIntArray::New();
	this->Dirichlets = vtkDoubleArray::New();
	this->CoupledModel = 0;
	
	this->NullElementList = NULL;
	this->NullTerminalList = NULL;
	
	this->nElementForGroup = NULL;
	this->ElementType = NULL;
	this->ElementMat = NULL;
	
}

//----------------------------------------------------------------------------
vtkHM1DMesh::~vtkHM1DMesh()
{	
	this->points->Delete();
	this->DirichletsTag->Delete();
	this->Dirichlets->Delete();
	
	if ( this->NullElementList )
		{
		this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
	
	if ( this->NullTerminalList )
		{
		this->NullTerminalList->Delete();
		this->NullTerminalList = NULL;
		}
	
	if (this->nElementForGroup)
		{
		delete [] this->nElementForGroup;
		this->nElementForGroup = NULL;
		}
	
	if (this->ElementType)
		{
		delete [] this->ElementType;
		this->ElementType	 = NULL;
		}
	
	if (this->ElementMat)
		{
		delete [] this->ElementMat;
		this->ElementMat = NULL;
		}
	
}

//----------------------------------------------------------------------------
void vtkHM1DMesh::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
	os << indent << "Dimension: " << this->Dimension << "\n";
	os << indent << "DegreeOfFreedom: " << this->DegreeOfFreedom << "\n";
	os << indent << "Number of points: " << this->NumberOfPoints << "\n";
	os << indent << "Number of element groups: " << this->NumberOfElementGroups << "\n";
}

void vtkHM1DMesh::DeepCopy(vtkHM1DMesh *orig)
{
	this->DegreeOfFreedom 		= orig->GetDegreeOfFreedom();
	this->NumberOfPoints 		= orig->GetNumberOfPoints();
	this->NumberOfElementGroups = orig->GetNumberOfElementGroups();
	this->Dimension 			= orig->GetDimension();
	this->ElementType 			= orig->GetElementType();
	this->ElementMat 			= orig->GetElementMat();
	this->nElementForGroup 		= orig->GetElementForGroup();
	
	this->DirichletsTag->DeepCopy(orig->GetDirichletsTag());
	
	this->Dirichlets->DeepCopy(orig->GetDirichlets());

	double *p;
	for( int i=0; i<this->NumberOfPoints; i++ )
	{
		p = orig->GetPoints1D(i);
		this->SetPoints1D(p[0], p[1], p[2]);
	}
	
	this->GroupElements.clear();
  
  VectorOfIntList *v = orig->GetGroupElements();
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<v->size(); i++ )
		{
		lst = v->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->GroupElements.push_back(lst2);
		lst2.clear();
		}
  
	if ( orig->GetNullElementList() )
		{
		if ( !this->NullElementList )
			this->NullElementList = vtkIntArray::New();
		this->NullElementList->DeepCopy(orig->GetNullElementList());
		}
	else
		{
		if ( this->NullElementList )
			this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
	
	if ( orig->GetNullTerminalList() )
		{
		if ( !this->NullTerminalList )
			this->NullTerminalList = vtkIntArray::New();
		this->NullTerminalList->DeepCopy(orig->GetNullTerminalList());
		}
	else
		{
		if ( this->NullTerminalList )
			this->NullTerminalList->Delete();
		this->NullTerminalList = NULL;
		}
}

//----------------------------------------------------------------------------
int vtkHM1DMesh::GetDimension()
{
	return this->Dimension;
}

//----------------------------------------------------------------------------
void vtkHM1DMesh::SetDimension(int d)
{
	this->Dimension = d;
}

//----------------------------------------------------------------------------
int vtkHM1DMesh::GetDegreeOfFreedom()
{
	return this->DegreeOfFreedom;
}

void vtkHM1DMesh::SetDegreeOfFreedom(int df)
{
	this->DegreeOfFreedom = df;
}

//----------------------------------------------------------------------------
double *vtkHM1DMesh::GetPoints1D(int i)
{
	return this->points->GetPoint(i);
}

//----------------------------------------------------------------------------
void vtkHM1DMesh::SetPoints(vtkPoints *points)
{
	this->points->DeepCopy(points);
}
//----------------------------------------------------------------------------

void vtkHM1DMesh::SetPoints1D(double x, double y, double z)
{
	this->points->InsertNextPoint(x,y,z);
}

vtkPoints *vtkHM1DMesh::GetPoints()
{
	return this->points;
}

//----------------------------------------------------------------------------
vtkHM1DMesh::VectorOfIntList *vtkHM1DMesh::GetGroupElements()
{
	return &this->GroupElements;
}

void vtkHM1DMesh::SetGroupElements(VectorOfIntList *groupElem)
{
	this->GroupElements.clear();
  
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<groupElem->size(); i++ )
		{
		lst = groupElem->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->GroupElements.push_back(lst2);
		lst2.clear();
		}
		
}

//----------------------------------------------------------------------------
int vtkHM1DMesh::GetNumberOfElementGroups()
{
	return this->NumberOfElementGroups;
}

void vtkHM1DMesh::SetNumberOfElementGroups(int n)
{
	this->NumberOfElementGroups = n;
}

//----------------------------------------------------------------------------
int vtkHM1DMesh::GetNumberOfPoints()
{
	return this->NumberOfPoints;
}

void vtkHM1DMesh::SetNumberOfPoints(int n)
{
	this->NumberOfPoints = n;
}

//----------------------------------------------------------------------------
int *vtkHM1DMesh::GetElementType()
{
	return this->ElementType;
}

void vtkHM1DMesh::SetElementType(int *elemType)
{
	this->ElementType = elemType;
}

//----------------------------------------------------------------------------
int *vtkHM1DMesh::GetElementMat()
{
	return this->ElementMat;
}

void vtkHM1DMesh::SetElementMat(int *elemMat)
{
	this->ElementMat = elemMat;
}

//----------------------------------------------------------------------------
int *vtkHM1DMesh::GetElementForGroup()
{
	return this->nElementForGroup;
}

void vtkHM1DMesh::SetElementForGroup(int *elemForGroup)
{
	this->nElementForGroup = elemForGroup;
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DMesh::GetDirichletsTag()
{
	return this->DirichletsTag;
}

void vtkHM1DMesh::SetDirichletsTag(vtkIntArray *d)
{
	this->DirichletsTag->DeepCopy(d);
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHM1DMesh::GetDirichlets()
{
	return this->Dirichlets;
}

void vtkHM1DMesh::SetDirichlets(vtkDoubleArray *d)
{
	this->Dirichlets->DeepCopy(d);
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DMesh::GetNullElementList()
{
	return this->NullElementList;
}

//----------------------------------------------------------------------------
void vtkHM1DMesh::SetNullElementList(vtkIntArray *list)
{
	if ( list )
		{
		if ( !this->NullElementList )
			this->NullElementList = vtkIntArray::New();
		this->NullElementList->DeepCopy(list);
		}
	else
		{
		if ( this->NullElementList )
			this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DMesh::GetNullTerminalList()
{
	return this->NullTerminalList;
}

//----------------------------------------------------------------------------
void vtkHM1DMesh::SetNullTerminalList(vtkIntArray *list)
{
	if ( list )
		{
		if ( !this->NullTerminalList )
			this->NullTerminalList = vtkIntArray::New();
		this->NullTerminalList->DeepCopy(list);
		}
	else
		{
		if ( this->NullElementList )
			this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
}
