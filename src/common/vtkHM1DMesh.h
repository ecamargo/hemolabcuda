// .NAME vtkHM1DMesh - Mesh data file
// .SECTION Description
// vtkHM1DMesh is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also

#ifndef VTKHM1DMESH_H_
#define VTKHM1DMESH_H_

#include "vtkPoints.h"
#include "vtkPointSet.h"
#include "vtkDataObject.h"

class vtkIntArray;
class vtkDoubleArray;
class vtkIdList;

#include <iostream>
#include <list>
#include <vector>
	
class VTK_EXPORT vtkHM1DMesh : public vtkDataObject
{
public:

	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX

	static vtkHM1DMesh *New();
	vtkTypeRevisionMacro(vtkHM1DMesh,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHM1DMesh *orig);
	
	// Description:
	// Set/Get DegreeOfFreedom
	int GetDegreeOfFreedom();
	void SetDegreeOfFreedom(int df);
	
	// Description:
	// Set/Get Dimension
	int GetDimension();
	void SetDimension(int d);
	
	// Description:
	// Set/Get NumberOfElementGroups
	int GetNumberOfElementGroups();
	void SetNumberOfElementGroups(int n);

	// Description:
	// Set/Get NumberOfPoints
	int GetNumberOfPoints();
	void SetNumberOfPoints(int n);
	
	// Description:
	// Set/Get ElementType
	int *GetElementType();
	void SetElementType(int *elemType);
	
	// Description:
	// Set/Get ElementMat
	int *GetElementMat();
	void SetElementMat(int *elemMat);
	
	// Description:
	// Set/Get nElementForGroup
	int *GetElementForGroup();
	void SetElementForGroup(int *elemForGroup);
	
	// Description:
	// Set/Get Points1D
	double *GetPoints1D(int i);
	void SetPoints1D(double x, double y, double z);
	
	vtkPoints *GetPoints();
	void SetPoints(vtkPoints *points);
	
	//BTX
	// Description:
	// Set/Get GroupElements
	VectorOfIntList *GetGroupElements();
	void SetGroupElements(VectorOfIntList *groupElem);
	//ETX
	
	// Description:
	// Set/Get DirichletsTag
	vtkIntArray *GetDirichletsTag();
	void SetDirichletsTag(vtkIntArray *d);
	
	// Description:
	// Set/Get Dirichlets
	vtkDoubleArray *GetDirichlets();
	void SetDirichlets(vtkDoubleArray *d);
	
	vtkSetMacro(CoupledModel, int);
	vtkGetMacro(CoupledModel, int);
	
	vtkSetMacro(SubStepNumber, int);
	vtkGetMacro(SubStepNumber, int);
	
	// Description:
	// Set/Get null element list.
	void SetNullElementList(vtkIntArray *list);
	vtkIntArray *GetNullElementList();

	// Description:
	// Set/Get null terminal list.
	void SetNullTerminalList(vtkIntArray *list);
	vtkIntArray *GetNullTerminalList();
	
protected:
	vtkHM1DMesh();
	~vtkHM1DMesh();
	
	// Description:
	// Number of points
	int NumberOfPoints;
	
	// Description:
	// Number of element group
	int NumberOfElementGroups;
	
	// Description:
	// Maximum number of degrees of liberty of th problem  	
	int DegreeOfFreedom;
	
	// Description:
	// Number of dimensions
	int Dimension;
	
	// Description:
	// Types of elements of the 1D tree (terminal, segment, etc...)
	int *ElementType;
	
	// Description:
	// Element mat of the 1D tree
	int *ElementMat;
	
	// Description:
	// Number of elements for group
	int *nElementForGroup;
	
	// Description:
	// Dirichlets tag
	vtkIntArray *DirichletsTag;
	
	// Description:
	// Dirichlets
	vtkDoubleArray *Dirichlets;
	
	// Description:
	// Points 
	vtkPoints* 		points;
	
	//BTX
	// Description:
	// Vector with an list of the element groups
	VectorOfIntList GroupElements;
	//ETX
	
	int CoupledModel;
	
	int SubStepNumber;
	
	// Descriptin:
	// List with id of the null elements 1D.
	vtkIntArray *NullElementList;

	// Descriptin:
	// List with id of the null terminals 1D.
	vtkIntArray *NullTerminalList;
	
}; //End class

#endif /*VTKHM1DMESH_H_*/
