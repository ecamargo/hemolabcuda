
#include "vtkHM1DParam.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>
#include <sys/stat.h>


vtkCxxRevisionMacro(vtkHM1DParam, "$Revision: 370 $");
vtkStandardNewMacro(vtkHM1DParam);

vtkHM1DParam::vtkHM1DParam()
{
	vtkDebugMacro(<<"Reading Param data...");	
	
	this->QuantityOfRealParameters = NULL;
	
	this->QuantityOfIntegerParameters = NULL;
	
	
}

//----------------------------------------------------------------------------
vtkHM1DParam::~vtkHM1DParam()
{

	if (this->QuantityOfRealParameters)
		{
		delete [] this->QuantityOfRealParameters;
		this->QuantityOfRealParameters = NULL;
		}
	
	if (this->QuantityOfIntegerParameters)
		{
		delete [] this->QuantityOfIntegerParameters;
		this->QuantityOfIntegerParameters = NULL;
		}


}

//----------------------------------------------------------------------------
void vtkHM1DParam::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}
//----------------------------------------------------------------------------

void vtkHM1DParam::DeepCopy(vtkHM1DParam *orig)
{
	this->NumberOfGroups 			  = orig->GetNumberOfGroups();
	this->QuantityOfRealParameters 	  = orig->GetQuantityOfRealParameters();
	this->QuantityOfIntegerParameters = orig->GetQuantityOfIntegerParameters();
	
	this->RealParameters.clear();
  
  VectorOfDoubleList *v = orig->GetRealParameters();
  ListOfDouble lstD, lst2D;
	ListOfDouble::iterator irD;
	
	for (unsigned int i=0; i<v->size(); i++ )
		{
		lstD = v->at(i);
		
		for (irD = lstD.begin() ;irD!=lstD.end();irD++)
			{
			lst2D.push_back(*irD);
			}
		this->RealParameters.push_back(lst2D);
		lst2D.clear();
		}
  
	this->IntegerParameters.clear();
    
  VectorOfIntList *vInt = orig->GetIntegerParameters();
	
	ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<vInt->size(); i++ )
		{
		lst = vInt->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->IntegerParameters.push_back(lst2);
		lst2.clear();
		}
}

//----------------------------------------------------------------------------
int *vtkHM1DParam::GetQuantityOfRealParameters()
{
	return this->QuantityOfRealParameters;
}
//----------------------------------------------------------------------------

void vtkHM1DParam::SetQuantityOfRealParameters(int *q)
{
	this->QuantityOfRealParameters = q;
}

//----------------------------------------------------------------------------
vtkHM1DParam::VectorOfDoubleList *vtkHM1DParam::GetRealParameters()
{
	return &this->RealParameters;
}
//----------------------------------------------------------------------------

void vtkHM1DParam::SetRealParameters(vtkHM1DParam::VectorOfDoubleList *v)
{
	this->RealParameters.clear();
  
  ListOfDouble lst, lst2;
	ListOfDouble::iterator ir;
	
	for (unsigned int i=0; i<v->size(); i++ )
		{
		lst = v->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->RealParameters.push_back(lst2);
		lst2.clear();
		}
}
//----------------------------------------------------------------------------
int *vtkHM1DParam::GetQuantityOfIntegerParameters()
{
	return this->QuantityOfIntegerParameters;
}
//----------------------------------------------------------------------------

void vtkHM1DParam::SetQuantityOfIntegerParameters(int *q)
{
	this->QuantityOfIntegerParameters = q;
}

//----------------------------------------------------------------------------
vtkHM1DParam::VectorOfIntList *vtkHM1DParam::GetIntegerParameters()
{
	return &this->IntegerParameters;
}
//----------------------------------------------------------------------------

void vtkHM1DParam::SetIntegerParameters(vtkHM1DParam::VectorOfIntList *v)
{
	this->IntegerParameters.clear();
  
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<v->size(); i++ )
		{
		lst = v->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->IntegerParameters.push_back(lst2);
		lst2.clear();
		}
}
