// .NAME vtkHM1DParam - Param data file
// .SECTION Description
// vtkHM1DParam is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also

#ifndef VTKHM1DPARAM_H_
#define VTKHM1DPARAM_H_

#include "vtkDataObject.h"

#include <list>
#include <vector>
	
class VTK_EXPORT vtkHM1DParam : public vtkDataObject
{
public:

	//BTX
	typedef std::list<int> ListOfInt;
	typedef std::list<double> ListOfDouble;
	
	typedef std::vector<ListOfInt> VectorOfIntList;
	typedef std::vector<ListOfDouble> VectorOfDoubleList;
	//ETX


	static vtkHM1DParam *New();
	vtkTypeRevisionMacro(vtkHM1DParam,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHM1DParam *orig);
	
	// Description:
	// Set/Get NumberOfGroups
	//int GetNumberOfGroups();
	vtkSetMacro(NumberOfGroups,int);
	vtkGetMacro(NumberOfGroups,int);
	
	// Description:
	// Set/Get QuantityOfReadlParameters
	int *GetQuantityOfRealParameters();
	void SetQuantityOfRealParameters(int *q);
	
	//BTX
	// Description:
	// Get RealParameters
	vtkHM1DParam::VectorOfDoubleList *GetRealParameters();
	void SetRealParameters(vtkHM1DParam::VectorOfDoubleList *v);
	//ETX
	
	// Description:
	// Set/Get QuantityOfIntegerParameters
	int *GetQuantityOfIntegerParameters();
	void SetQuantityOfIntegerParameters(int *q);
	
	//BTX
	// Description:
	// Get IntegerParameters
	VectorOfIntList *GetIntegerParameters();
	void SetIntegerParameters(vtkHM1DParam::VectorOfIntList *v);
	//ETX
	
protected:
	vtkHM1DParam();
	~vtkHM1DParam();
	
	// Description:
	// Indicate the number the element groups whose
	//parameters are differents
	int NumberOfGroups;
	
	// Description:
	// Quantity of real parameters that each group possess
	int *QuantityOfRealParameters;
	
	//BTX
	// Description:
	// List of real parameters of each group
	 VectorOfDoubleList RealParameters;
	//ETX
	
	// Description:
	// Quantity of integer parameters that each group possess
	int *QuantityOfIntegerParameters;
	
	//BTX
	// Description:
	// List of integer parameters of each group
	VectorOfIntList IntegerParameters;
	//ETX
	
}; //End class

#endif /*VTKHM1DPARAM_H_*/
