/*
 * $Id:vtkHM1DSegment.cxx 286 2006-05-05 19:43:09Z nacho $
 */

#include "vtkHM1DSegment.h"
#include "vtkHMUtils.h"
#include<math.h>
#include <stdio.h>

#define M_PI 3.14159265358979323846

vtkCxxRevisionMacro(vtkHM1DSegment, "$Rev:286 $");
vtkStandardNewMacro(vtkHM1DSegment);
//----------------------------------------------------------------------------


vtkHM1DSegment::vtkHM1DSegment():vtkHM1DTreeElement()
{
  Length = -1;
  NumberOfElements = -1;
  dx = -1;
  isViscoelastic = false;
  A,B=0;
//  this->SegmentName = NULL;
	this->Result.clear();
	this->interpolationType = 0;
}
//----------------------------------------------------------------------------


vtkHM1DSegment::~vtkHM1DSegment()
{
  // todo: Probablemente tenga que eliminar los nodeData para cada nodo.
  NodeDataMap::iterator it;
  for (it = nodeData.begin();it!=nodeData.end();it++)
    {
      if(it->second->ResolutionArray)
      	it->second->ResolutionArray->Delete();
      delete it->second;
    }
  nodeData.erase(nodeData.begin(), nodeData.end());

  ElementDataMap::iterator ite;
  for (ite = ElementData.begin();ite!=ElementData.end();ite++)
    {
      delete ite->second;
    }
  ElementData.erase(ElementData.begin(), ElementData.end());

	this->Result.clear();
}
//----------------------------------------------------------------------------


void vtkHM1DSegment::DeepCopy(vtkHM1DSegment *orig)
{
	this->Superclass::DeepCopy(orig);

	this->SetSegmentName(orig->GetSegmentName());

	Length = orig->GetLength();
	NumberOfElements = orig->GetElementNumber();
	isViscoelastic = orig->GetisViscoelastic();
	dx = orig->GetDx();

	for (int i = 0; i<NumberOfElements;i++)
		{
		//Coping node data
		vtkHMNodeData *origData = orig->GetNodeData(i);
  	vtkHMNodeData *data = new vtkHMNodeData();

    data->coords[0]     = origData->coords[0];
  	data->coords[1]     = origData->coords[1];
  	data->coords[2]     = origData->coords[2];
    data->area          = origData->area;
  	data->radius        = origData->radius;
		data->alfa      		= origData->alfa;
		data->meshID     		= origData->meshID;
		data->PercentageToCalculateStenosis     = origData->PercentageToCalculateStenosis;
		data->PercentageToCalculateAneurysm     = origData->PercentageToCalculateAneurysm;
	  data->ResolutionArray = origData->ResolutionArray;

    this->SetNodeData(i,*data);

    //Coping element data
  	vtkHMElementData *ElemData = orig->GetElementData(i);
  	vtkHMElementData *Elem = new vtkHMElementData();

  	if (ElemData)
  		{
    	Elem->Elastin					= ElemData->Elastin;
	   	Elem->Collagen					= ElemData->Collagen;
  	 	Elem->CoefficientA				= ElemData->CoefficientA;
  		Elem->CoefficientB				= ElemData->CoefficientB;
  		Elem->Viscoelasticity			= ElemData->Viscoelasticity;
  		Elem->ViscoelasticityExponent	= ElemData->ViscoelasticityExponent;
  		Elem->InfiltrationPressure		= ElemData->InfiltrationPressure;
  		Elem->ReferencePressure			= ElemData->ReferencePressure;
  		Elem->Permeability				= ElemData->Permeability;
  		Elem->WallThickness				= ElemData->WallThickness;
  		Elem->idNode[0]					= ElemData->idNode[0];
  		Elem->idNode[1]					= ElemData->idNode[1];
  		Elem->meshID     		= ElemData->meshID;
  		Elem->ElastinStentFactor				= ElemData->ElastinStentFactor;
  		Elem->ViscoelasticityStentFactor				= ElemData->ViscoelasticityStentFactor;
  		}

  	this->SetElementData(i,*Elem);
  	delete data;
  	delete Elem;
		}

	//Coping node data
	vtkHMNodeData *origData = orig->GetNodeData(NumberOfElements);
	vtkHMNodeData *data = new vtkHMNodeData();

  data->coords[0]     = origData->coords[0];
	data->coords[1]     = origData->coords[1];
	data->coords[2]     = origData->coords[2];
  data->area          = origData->area;
	data->radius        = origData->radius;
	data->alfa     			= origData->alfa;
	data->meshID      	= origData->meshID;
	data->PercentageToCalculateStenosis      = origData->PercentageToCalculateStenosis;
	data->PercentageToCalculateAneurysm      = origData->PercentageToCalculateAneurysm;
	data->ResolutionArray = origData->ResolutionArray;

  this->SetNodeData(NumberOfElements,*data);
  delete data;
}

//----------------------------------------------------------------------------

int vtkHM1DSegment::GetNodeNumber(void)
{
 return nodeData.size();
}
//----------------------------------------------------------------------------


int vtkHM1DSegment::GetElementNumber(void)
{
  	return ElementData.size();
}
//----------------------------------------------------------------------------


const char *vtkHM1DSegment::GetElementArray(int elem)
{
  if ( elem == -1 )
    return NULL;

  vtkHMElementData *Elem = this->GetElementData(elem);

  Result.clear();

  char Temp[30];

	sprintf(Temp, "%f;", Elem->Elastin);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->Collagen);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->CoefficientA);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->CoefficientB);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->Viscoelasticity);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->ViscoelasticityExponent);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->InfiltrationPressure);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->ReferencePressure);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->Permeability);
	Result.append(Temp);

	sprintf(Temp, "%f;", 0);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->ElastinStentFactor);
	Result.append(Temp);

	sprintf(Temp, "%f;", Elem->ViscoelasticityStentFactor);
	Result.append(Temp);

	sprintf(Temp, "%d;", Elem->idNode[0]);
	Result.append(Temp);

	sprintf(Temp, "%d;", Elem->idNode[1]);
	Result.append(Temp);

	sprintf(Temp, "%d;", Elem->meshID);
	Result.append(Temp);

	return Result.c_str();
}



//----------------------------------------------------------------------------


const char *vtkHM1DSegment::GetNodeArray(int NodeOrder)
{
  if ( NodeOrder == -1 )
    return NULL;

	vtkHMNodeData *Node = this->GetNodeData(NodeOrder);


  //char Result[2000] ={0};

  Result.clear();

  char Temp[20];

	sprintf(Temp, "%f;", Node->coords[0]);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%f;", Node->coords[1]);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%f;", Node->coords[2]);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%f;", Node->area);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%f;", Node->radius);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%f;", Node->alfa);
//	strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%i;", Node->meshID);
	//strcat(Result, Temp);
	Result.append(Temp);

	sprintf(Temp, "%i;", Node->PercentageToCalculateStenosis);
	Result.append(Temp);

	sprintf(Temp, "%i;", Node->PercentageToCalculateAneurysm);
	Result.append(Temp);

	//return &Result[0];
	return Result.c_str();

}

//----------------------------------------------------------------------------

vtkDoubleArray *vtkHM1DSegment::GetResolutionArray(int NodeOrder)
{
  	vtkHMNodeData *Node = this->GetNodeData(NodeOrder);
    if (Node)
    {
    	return Node->ResolutionArray;
    }
    else
    {
    	vtkWarningMacro(<< "Can not find the selected Node in the segment");
   		return NULL;
    }
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetResolutionArray(int NodeOrder, vtkDoubleArray *array)
{
  vtkHMNodeData *Node = this->GetNodeData(NodeOrder);
  if (Node)
    {
    if ( array )
      {
      if ( !Node->ResolutionArray )
        Node->ResolutionArray = vtkDoubleArray::New();
      Node->ResolutionArray->DeepCopy(array);
      }
    else
      {
      if ( Node->ResolutionArray )
        Node->ResolutionArray->Delete();
      Node->ResolutionArray = NULL;
      }
    }
  else
    {
    vtkWarningMacro(<< "Can not find the selected Node in the segment");
    }
}

//----------------------------------------------------------------------------


int vtkHM1DSegment::GetResolutionNumberOfTuples(int NodeOrder)
{
  vtkHMNodeData *Node = this->GetNodeData(NodeOrder);
  if (Node->ResolutionArray)
  	return Node->ResolutionArray->GetNumberOfTuples();
  else
  	{
   	vtkErrorMacro("Couldn't Get ResolutionArray Tuples Number");
   	return 0;
  	}
}

//----------------------------------------------------------------------------

int vtkHM1DSegment::GetResolutionArrayNumberOfComponents(int NodeOrder)
{
  vtkHMNodeData *Node = this->GetNodeData(NodeOrder);
  if (Node->ResolutionArray)
  	{
  	return Node->ResolutionArray->GetNumberOfComponents();
  	}
  else
  	{
    vtkErrorMacro("Couldn't Get ResolutionArray Components Number");
    return 0;
    }
}

//----------------------------------------------------------------------------


const char *vtkHM1DSegment::GetResolutionArrayAsChar(int NodeOrder, char Type)
{
	vtkHMNodeData *Node;
	vtkHMNodeData *Node2;

	Result.clear();

  if ( Type == 'N' ) //Objeto selecionado e um node
	  {
	  Node = this->GetNodeData(NodeOrder);
	  if (Node->ResolutionArray)
	  	{
	  	string temp = vtkHMUtils::ConvertDoubleArrayToChar(Node->ResolutionArray);
	  	Result.append(temp.c_str());
	  	return Result.c_str();
	  	}
	  else
	  	{
	  	vtkErrorMacro("vtkHM1DSegment::GetResolutionArrayAsChar:: Could not get Node Element Data  ");
	  	return NULL;
	  	//	  	return Result.c_str();
	  	}
	  }
	else if ( Type == 'E' ) //Objeto selecionado e um elemento
		{
		vtkHMElementData *Element = this->GetElementData(NodeOrder);
		Node = this->GetNodeData(Element->idNode[0]);
		Node2 = this->GetNodeData(Element->idNode[1]);

	  if (Node->ResolutionArray)
	  	{
	  	vtkDoubleArray *Array = vtkDoubleArray::New();
	    Array->SetNumberOfComponents(Node->ResolutionArray->GetNumberOfComponents());//Array para armazenar a media dos pontos. Recebe o Array auxiliar
	    double* ArrayAux= new double[Node->ResolutionArray->GetNumberOfComponents()];//Array auxiliar para trabalhar dentro do For
		  int NumberOfTuples = Node->ResolutionArray->GetNumberOfTuples();

		  for (int tuple=0; tuple < NumberOfTuples; tuple++)
		  	{
		    for (int z=0; z < Node->ResolutionArray->GetNumberOfComponents(); z++)
	      	{
	      	//pegar os nodes da esquerda e da direita
	      	//enchendo o array auxiliar com a media dos pontos
	      	ArrayAux[z] = (Node->ResolutionArray->GetTuple(tuple)[z] + Node2->ResolutionArray->GetTuple(tuple)[z]) / 2;
		    	}
	 	  	Array->InsertNextTuple( ArrayAux );//Enchendo o Array com o Array auxiliar
		    }
	  	string temp = vtkHMUtils::ConvertDoubleArrayToChar(Array);
	  	Result.append(temp.c_str());
		  Array->Delete();
		  return Result.c_str();
	  	}
	  else
	  	{
	  	vtkErrorMacro("vtkHM1DSegment::GetResolutionArrayAsChar:: Could not get Element Data  ");
	  	return NULL;
	  	//	  	return Result.c_str();
	  	}
		}

}
//----------------------------------------------------------------------------


void vtkHM1DSegment::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  const char* name = this->GetName();
  if (name)
    {
    os << indent << "Name: " << name << "\n";
    }
  else
    {
    os << indent << "Name: (none)\n";
    }
  os << indent << "Lenght: " << Length << "\n";
  os << indent << "NumberOfElements: " << NumberOfElements << "\n";
  os << indent << "dx: " << dx << "\n";
  os << indent << "isViscoelastic: " << isViscoelastic << endl;
  os << indent << "interpolationType: " << interpolationType << endl;
}
//----------------------------------------------------------------------------
vtkHMNodeData *vtkHM1DSegment::GetNodeData(int node)
{
  NodeDataMap::iterator it;
  it = nodeData.find(node);
  if (it != nodeData.end())
    {
    return it->second;
    }
  else
    {
    vtkWarningMacro(<< "Segm: " << this->id << "\tThe node does not exist!! " << node);
    return NULL;
    }
}
//----------------------------------------------------------------------------

void vtkHM1DSegment::SetNodeData(int node, vtkHMNodeData newData)
{
  NodeDataMap::iterator it = nodeData.find(node);
  if (node > NumberOfElements)
    {
    vtkWarningMacro(<< "Trying to add information to a node that does not exist! >> node:" << node << ", NumberOfElements: " << NumberOfElements);
    return;
    }
  if (it == nodeData.end())
    {
    vtkHMNodeData *data = new vtkHMNodeData();

    data->coords[0]     	= newData.coords[0];
    data->coords[1]     	= newData.coords[1];
    data->coords[2]     	= newData.coords[2];
    data->area          	= newData.area;
    data->radius        	= newData.radius;
    data->alfa      			= newData.alfa;
//    data->ResolutionArray = newData.ResolutionArray;
    data->meshID      = newData.meshID;
    data->PercentageToCalculateStenosis      = newData.PercentageToCalculateStenosis;
    data->PercentageToCalculateAneurysm      = newData.PercentageToCalculateAneurysm;

    if(newData.ResolutionArray)
      {
      if ( !data->ResolutionArray )
        data->ResolutionArray = vtkDoubleArray::New();

      data->ResolutionArray->DeepCopy(newData.ResolutionArray);
      }
    else
      {
      if ( data->ResolutionArray )
        data->ResolutionArray->Delete();
      data->ResolutionArray = NULL;
      }

    nodeData.insert(NodeDataPair(node,data));
    }
  else
    {
    it->second->coords[0]  	  	= newData.coords[0];
    it->second->coords[1]   	  = newData.coords[1];
    it->second->coords[2]    		= newData.coords[2];
    it->second->area          	= newData.area;
    it->second->radius        	= newData.radius;
    it->second->alfa      	  	= newData.alfa;
//    it->second->ResolutionArray = newData.ResolutionArray;
    it->second->meshID      = newData.meshID;
    it->second->PercentageToCalculateStenosis      = newData.PercentageToCalculateStenosis;
    it->second->PercentageToCalculateAneurysm      = newData.PercentageToCalculateAneurysm;

    if(newData.ResolutionArray)
      {
      if ( !it->second->ResolutionArray )
        it->second->ResolutionArray = vtkDoubleArray::New();

      it->second->ResolutionArray->DeepCopy(newData.ResolutionArray);
      }
    else
      {
      if ( it->second->ResolutionArray )
        it->second->ResolutionArray->Delete();
      it->second->ResolutionArray = NULL;
      }
    }
}


//----------------------------------------------------------------------------
// Set element data
void vtkHM1DSegment::SetElementData(int elem, vtkHMElementData newData)
{

	 ElementDataMap::iterator it = ElementData.find(elem);
  if (elem > NumberOfElements)
    {
    vtkWarningMacro(<< "Trying to add information to a element that does not exist! >> node:"
    				<< elem << ", NumberOfElements: " << NumberOfElements);
    return;
    }
  if (it == ElementData.end())
    {
    vtkHMElementData *data = new vtkHMElementData();

    data->Elastin       					= newData.Elastin;
    data->Collagen      					= newData.Collagen;
    data->CoefficientA      			= newData.CoefficientA;
    data->CoefficientB      			= newData.CoefficientB;
    data->Viscoelasticity      		= newData.Viscoelasticity;
    data->ViscoelasticityExponent	= newData.ViscoelasticityExponent;
    data->InfiltrationPressure		= newData.InfiltrationPressure;
    data->ReferencePressure     	= newData.ReferencePressure;
    data->Permeability						= newData.Permeability;
    data->WallThickness 					= newData.WallThickness;
    data->idNode[0]								= newData.idNode[0];
    data->idNode[1]								= newData.idNode[1];
    data->ElastinStentFactor 					= newData.ElastinStentFactor;
    data->ViscoelasticityStentFactor 					= newData.ViscoelasticityStentFactor;
    data->meshID      = newData.meshID;

    ElementData.insert(ElementDataPair(elem,data));
    }
  else
    {
    it->second->Elastin       					= newData.Elastin;
    it->second->Collagen      					= newData.Collagen;
    it->second->CoefficientA      			= newData.CoefficientA;
    it->second->CoefficientB      			= newData.CoefficientB;
    it->second->Viscoelasticity      		= newData.Viscoelasticity;
    it->second->ViscoelasticityExponent	= newData.ViscoelasticityExponent;
    it->second->InfiltrationPressure		= newData.InfiltrationPressure;
    it->second->ReferencePressure     	= newData.ReferencePressure;
    it->second->Permeability						= newData.Permeability;
    it->second->WallThickness 					= newData.WallThickness;
    it->second->idNode[0]								= newData.idNode[0];
    it->second->idNode[1]								= newData.idNode[1];
    it->second->ElastinStentFactor 					= newData.ElastinStentFactor;
    it->second->ViscoelasticityStentFactor 					= newData.ViscoelasticityStentFactor;
    it->second->meshID      = newData.meshID;
    }
}

//----------------------------------------------------------------------------
// Get element data
vtkHMElementData *vtkHM1DSegment::GetElementData(int elem)
{
	ElementDataMap::iterator it;
  it = ElementData.find(elem);
  if (it != ElementData.end())
    {
    return it->second;
    }
  else
    {
    vtkWarningMacro(<< "The element does not exist!! " << elem);
    return NULL;
    }
}
//----------------------------------------------------------------------------
void vtkHM1DSegment::SetDx(double dx)
{
  // que pasa con los nodeData??, con el map?, tengo que eliminar los elementos que estan de mas?
  if (Length != -1 && NumberOfElements!=-1)
    {
    NumberOfElements = (int)(Length / dx);
    }
  else
    {
    vtkWarningMacro(<< "Cannot SetDx, Length or NumberOfElements have not been set!.");
    }
}
//----------------------------------------------------------------------------
double vtkHM1DSegment::GetDx()
{
  //todo: calcular dx en funcion de length y numberOfElements
  // que pasa con los nodeData??, con el map?, tengo que eliminar los elementos que estan de mas?
  if (Length != -1 && dx!=-1)
    {
    dx = Length / NumberOfElements;
    return dx;
    }
  else
    {
//    vtkWarningMacro(<< "Cannot GetDx, Length or NumberOfElements have not been set!.");
    return 0;
    }
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetNodesProps(double nodeArray[7])
{
//cout << "vtkHM1DSegment::SetNodesProps -> interpolacao: " << nodeArray[2] << endl;
  double pi = M_PI;
  double ProximalRadius 	= nodeArray[0];
  double DistalRadius 		= nodeArray[1];
  int InterpolationType 	= int(nodeArray[2]);
  double ProximalWall 		= nodeArray[3];
  double DistalWall 			= nodeArray[4];
  double Lenght 					= nodeArray[5];
  double DX 							= nodeArray[6];
  double radius=0.0, area =0.0, wall = 0.0;

  this->SetinterpolationType(InterpolationType);

  if (InterpolationType) // interpolacao linear é igual a 1
  	{
    //  this->SetEquationCoefficient(ProximalRadius, DistalRadius, Lenght);
    int i=0;
	  NodeDataMap::iterator it = nodeData.find(0);
	  while (it != nodeData.end())
	  	{
	    this->SetEquationCoefficient(ProximalRadius, DistalRadius, Lenght);
      radius = this->GetY(i*DX);
	   	area = (pi*(radius*radius));

	   	this->SetEquationCoefficient(ProximalWall, DistalWall, Lenght);
      wall = this->GetY(i*DX);
      it->second->area  = area;
	  	it->second->alfa  = wall*pi*radius;
	  	it->second->radius  = radius;
	  	it++;
	    i++;
	  	}
  	}
  else // tipo de interpolacao constante
  	{
//  	cout << "Segm: " << this->GetId() << "\tInterpolação constante" << endl;
	  int i=0;
	  NodeDataMap::iterator it = nodeData.find(0);
	  while (it != nodeData.end())
	  	{
	  	area = (pi*(ProximalRadius*ProximalRadius));
	  	it->second->area  = area;
	   	it->second->alfa  = ProximalWall*pi*ProximalRadius;
	   	it->second->radius  = ProximalRadius;
	  	it++;
	    i++;
	  	}
  	}
}

//----------------------------------------------------------------------------

void vtkHM1DSegment::SetSingleNodeProp( double radius, double wall, int NodeOrder)
{
	double pi = M_PI;
 	int i=0;
	NodeDataMap::iterator it = nodeData.find(0);
	while (it != nodeData.end())
		{
	  if (i==NodeOrder)
	  	{
	   	// calculo da area
	   	// Area = Pi * Radius^2
	   	it->second->area  = (pi*(radius*radius));

	    // calculo do alfa
	    // Alfa = WallThickness*Pi*Radius
	    it->second->alfa  = wall*pi*radius;

	    it->second->radius = radius;

	    break;
	   	}
	  it++;
	  i++;
	  }
}

//----------------------------------------------------------------------------

void vtkHM1DSegment::SetEquationCoefficient(double v1, double v2, double SegmentSize)
{
	// y=ax+b

	this->A = ((v2-v1)/SegmentSize);
	this->B = v1;

}

//----------------------------------------------------------------------------

double vtkHM1DSegment::GetY(double x)
{
   // y=ax+b eq. da reta
   double y = ((this->A*x) + (this->B));
   return y;
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetSingleElem(double ElemArray[11])
{
  int ElemOrder = int(ElemArray[10]);
  int i=0;
  ElementDataMap::iterator it = ElementData.find(0);
  while (it != ElementData.end())
  	{
  	if (i==ElemOrder)
  		{
	  	it->second->Elastin =ElemArray[0];
		  it->second->Collagen =ElemArray[1];
		  it->second->CoefficientA=ElemArray[2];
		  it->second->CoefficientB=ElemArray[3];
		  it->second->Viscoelasticity=ElemArray[4];
		  it->second->ViscoelasticityExponent=ElemArray[5];
		  it->second->InfiltrationPressure=ElemArray[6];
		  it->second->ReferencePressure=ElemArray[7];
		  it->second->Permeability=ElemArray[8];
		  it->second->WallThickness=ElemArray[9];
		  break;
	   	}
  	it++;
    i++;
  	}
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetProxElem(double proxElem[10])
{
	for (int i=0;i<10;i++)
	  this->proxElem[i]=proxElem[i];

}
//----------------------------------------------------------------------------
void vtkHM1DSegment::SetDistalElem(double distalElem[10])
{
	for (int i=0;i<10;i++)
	  this->distalElem[i]=distalElem[i];

}
//----------------------------------------------------------------------------

void vtkHM1DSegment::SetElementsFromSegment(int InterpolationType) //, double Lenght, double DX)
{
  // valores dos parametros proximais
  double proxElastin 										=proxElem[0];
	double proxPermeability								=proxElem[1];
  double proxInfiltrationPressure				=proxElem[2];
	double proxReferencePressure					=proxElem[3];
	double proxCollagen 									=proxElem[4];
	double proxCoefficientA								=proxElem[5];
	double proxCoefficientB								=proxElem[6];
	double proxViscoelasticity						=proxElem[7];
	double proxViscoelasticityExponent		=proxElem[8];
	double proxWallThickness							=proxElem[9];

	// valores dos parametros distais
	double distalElastin 									=distalElem[0];
	double distalPermeability							=distalElem[1];
  double distalInfiltrationPressure			=distalElem[2];
	double distalReferencePressure				=distalElem[3];
	double distalCollagen 								=distalElem[4];
	double distalCoefficientA							=distalElem[5];
	double distalCoefficientB							=distalElem[6];
	double distalViscoelasticity					=distalElem[7];
	double distalViscoelasticityExponent	=distalElem[8];
	double distalWallThickness						=distalElem[9];

  this->SetinterpolationType(InterpolationType);

  if (InterpolationType) // interpolacao linear é igual a 1
  	{
    double i=0;

    int ElementNumber = this->GetElementNumber() -1;

    //acessa elemento 0 - primeiro elemento do segmento
    ElementDataMap::iterator it = ElementData.find(0);


    // enquanto nao chega ao fim da lista dos elementos no atual segmento
	  while (it != ElementData.end())
	  	{
			this->SetEquationCoefficient(proxElastin, distalElastin, ElementNumber);
		  it->second->Elastin = this->GetY(i);

      this->SetEquationCoefficient(proxCollagen, distalCollagen, ElementNumber);
      it->second->Collagen = this->GetY(i);

	   	this->SetEquationCoefficient(proxCoefficientA, distalCoefficientA, ElementNumber);
      it->second->CoefficientA = this->GetY(i);

      this->SetEquationCoefficient(proxCoefficientB, distalCoefficientB, ElementNumber);
      it->second->CoefficientB = this->GetY(i);

      this->SetEquationCoefficient(proxViscoelasticity, distalViscoelasticity, ElementNumber);
      it->second->Viscoelasticity = this->GetY(i);

      this->SetEquationCoefficient(proxViscoelasticityExponent, distalViscoelasticityExponent, ElementNumber);
      it->second->ViscoelasticityExponent = this->GetY(i);

      this->SetEquationCoefficient(proxInfiltrationPressure, distalInfiltrationPressure, ElementNumber);
      it->second->InfiltrationPressure = this->GetY(i);

      this->SetEquationCoefficient(proxReferencePressure, distalReferencePressure, ElementNumber);
      it->second->ReferencePressure = this->GetY(i);

      this->SetEquationCoefficient(proxPermeability, distalPermeability, ElementNumber);
      it->second->Permeability = this->GetY(i);

      this->SetEquationCoefficient(proxWallThickness, distalWallThickness, ElementNumber);
      it->second->WallThickness = this->GetY(i);
      it++;
	    i++;
		  }
   }
  else // tipo de interpolacao constante
  	{
		ElementDataMap::iterator it = ElementData.begin();
	  while (it != ElementData.end())
	  	{
		  it->second->Elastin = proxElastin;
			it->second->Collagen = proxCollagen;
		  it->second->CoefficientA = proxCoefficientA;
	    it->second->CoefficientB = proxCoefficientB;
	    it->second->Viscoelasticity = proxViscoelasticity;
	    it->second->ViscoelasticityExponent = proxViscoelasticityExponent;
	    it->second->InfiltrationPressure = proxInfiltrationPressure;
	    it->second->ReferencePressure = proxReferencePressure;
	    it->second->Permeability = proxPermeability;
	    it->second->WallThickness =proxWallThickness;
		  it++;
	  	}
	  }
}

//----------------------------------------------------------------------------
// Remove um node data
int vtkHM1DSegment::RemoveNodeData(int node)
{
	NodeDataMap::iterator it, it2;
  it = nodeData.find(node);
  it2 = it;
  it2++;
  if (it != nodeData.end())
    {
    if (it->second->ResolutionArray)
    	{
    	it->second->ResolutionArray->Delete();
    	it->second->ResolutionArray = NULL;
    	}
    nodeData.erase(it);
    while (it2 != nodeData.end() )
	    {
	    it2++;
	    }
    return 1;
    }
  else
    {
    vtkWarningMacro(<< "The node does not exist!! " << node);
    return 0;
    }
}

//----------------------------------------------------------------------------
// Remove node data entre os indices passados como parametro
void vtkHM1DSegment::RemoveNodeData(int beginNode, int endNode)
{
	NodeDataMap::iterator it, it2;
  it = nodeData.find(beginNode);
  it2 = nodeData.find(endNode);

  nodeData.erase(it, it2);
}

//----------------------------------------------------------------------------
// Remove um element data
int vtkHM1DSegment::RemoveElementData(int element)
{
	ElementDataMap::iterator it, it2;
  it = ElementData.find(element);
  it2 = it;
  it2++;
  if (it != ElementData.end())
    {
    ElementData.erase(it);
    while (it2 != ElementData.end() )
	    {
	    it2++;
	    }
    return 1;
    }
  else
    {
    vtkWarningMacro(<< "The element does not exist!! " << element);
    return 0;
    }
}

//----------------------------------------------------------------------------
// Remove element data entre os indices passados como parametro
void vtkHM1DSegment::RemoveElementData(int beginElement, int endElement)
{
	ElementDataMap::iterator it, it2;
  it = ElementData.find(beginElement);
  it2 = ElementData.find(endElement);

  ElementData.erase(it, it2);
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetId(vtkIdType id)
{
	if (this->id == -1)
		{
		int i = id;
		sprintf(this->SegmentName, "s%d", i);
		}

	this->Superclass::SetId(id);
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::SetSegmentName(char *n)
{
	sprintf(this->SegmentName, "%s", n);
}

//----------------------------------------------------------------------------
char *vtkHM1DSegment::GetSegmentName()
{
	return this->SegmentName;
}

//----------------------------------------------------------------------------
int vtkHM1DSegment::InvertFlowDirection()
{
	if ( this->GetFirstParent()->GetId() == 1 )
		{
		vtkWarningMacro(<< "Parent is heart, cannot invert flow of the segment");
		return 0;
		}

	vtkHM1DSegment *segm = vtkHM1DSegment::New();

	/////////////////////////////////////////////////////////////
	//Copiar os dados para outro segmento, invertendo a posição dos elementos e nodes
	segm->DeepCopy(this);

	segm->SetLength(this->GetLength());
	segm->SetNumberOfElements(this->GetElementNumber());
	segm->SetisViscoelastic(this->GetisViscoelastic());
//	segm->SetDx(this->GetDx());
	int j = 0;

	for (int i=this->NumberOfElements; i>0 ;i--)
		{
		//Coping node data
		vtkHMNodeData *origData = this->GetNodeData(i);
  	vtkHMNodeData *data = new vtkHMNodeData();

    data->coords[0]     = origData->coords[0];
  	data->coords[1]     = origData->coords[1];
  	data->coords[2]     = origData->coords[2];
    data->area          = origData->area;
  	data->radius        = origData->radius;
		data->alfa      		= origData->alfa;
		data->meshID     		= origData->meshID;
		data->PercentageToCalculateStenosis     		= origData->PercentageToCalculateStenosis;
		data->PercentageToCalculateAneurysm     		= origData->PercentageToCalculateAneurysm;
		data->ResolutionArray = origData->ResolutionArray;

    segm->SetNodeData(j,*data);

    //Coping element data
  	vtkHMElementData *ElemData = this->GetElementData(i-1);
  	vtkHMElementData *Elem = new vtkHMElementData();

  	if (ElemData)
  		{
    	Elem->Elastin					= ElemData->Elastin;
	   	Elem->Collagen					= ElemData->Collagen;
  	 	Elem->CoefficientA				= ElemData->CoefficientA;
  		Elem->CoefficientB				= ElemData->CoefficientB;
  		Elem->Viscoelasticity			= ElemData->Viscoelasticity;
  		Elem->ViscoelasticityExponent	= ElemData->ViscoelasticityExponent;
  		Elem->InfiltrationPressure		= ElemData->InfiltrationPressure;
  		Elem->ReferencePressure			= ElemData->ReferencePressure;
  		Elem->Permeability				= ElemData->Permeability;
  		Elem->WallThickness				= ElemData->WallThickness;
  		Elem->idNode[0]					= ElemData->idNode[0];
  		Elem->idNode[1]					= ElemData->idNode[1];
  		Elem->ElastinStentFactor				= ElemData->ElastinStentFactor;
  		Elem->ViscoelasticityStentFactor				= ElemData->ViscoelasticityStentFactor;
  		Elem->meshID     		= ElemData->meshID;
  		}

  	segm->SetElementData(j,*Elem);

  	delete data;
  	delete Elem;
  	j++;
		}

	//Coping node data
	vtkHMNodeData *origData = this->GetNodeData(0);
	vtkHMNodeData *data = new vtkHMNodeData();

  data->coords[0]     = origData->coords[0];
	data->coords[1]     = origData->coords[1];
	data->coords[2]     = origData->coords[2];
  data->area          = origData->area;
	data->radius        = origData->radius;
	data->alfa      = origData->alfa;
	data->meshID      = origData->meshID;
	data->PercentageToCalculateStenosis      = origData->PercentageToCalculateStenosis;
	data->PercentageToCalculateAneurysm      = origData->PercentageToCalculateAneurysm;
	data->ResolutionArray = origData->ResolutionArray;

  segm->SetNodeData(NumberOfElements,*data);

  delete data;

  this->DeepCopy(segm);

  segm->Delete();
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::AddStent(vtkIdType idElement, double elastinFactor, double viscoElasticityFactor)
{
	ElementDataMap::iterator it = this->ElementData.find(idElement);

	if ( it != this->ElementData.end() )
		{
		it->second->ElastinStentFactor = elastinFactor;
		it->second->ViscoelasticityStentFactor = viscoElasticityFactor;

		it->second->Elastin *= elastinFactor;
		it->second->Viscoelasticity *= viscoElasticityFactor;
		}
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::RemoveStent(vtkIdType idElement)
{
	vtkHMElementData *elem = this->GetElementData(idElement);

	elem->Elastin /= elem->ElastinStentFactor;
	elem->Viscoelasticity /= elem->ViscoelasticityStentFactor;

	elem->ElastinStentFactor = 1;
	elem->ViscoelasticityStentFactor = 1;
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::AddStenosis(vtkIdType idNode, double percentage)
{
	vtkHMNodeData *node = this->GetNodeData(idNode);
	if ( node )
		{
		node->PercentageToCalculateStenosis = percentage;
		node->area -= node->area*node->PercentageToCalculateStenosis/100;
		node->radius = sqrt(node->area/vtkMath::Pi());
		}
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::RemoveStenosis(vtkIdType idNode)
{
	vtkHMNodeData *node = this->GetNodeData(idNode);
	if ( node )
		{
		node->area += (node->area*node->PercentageToCalculateStenosis)/(100-node->PercentageToCalculateStenosis);
		node->PercentageToCalculateStenosis = 0;
		node->radius = sqrt(node->area/vtkMath::Pi());
		}
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::AddAneurysm(vtkIdType idNode, double percentage)
{
	vtkHMNodeData *node = this->GetNodeData(idNode);
	if ( node )
		{
		node->PercentageToCalculateAneurysm = percentage;
		node->area += node->area*node->PercentageToCalculateAneurysm/100;
		node->radius = sqrt(node->area/vtkMath::Pi());
		}
}

//----------------------------------------------------------------------------
void vtkHM1DSegment::RemoveAneurysm(vtkIdType idNode)
{
	vtkHMNodeData *node = this->GetNodeData(idNode);
	if ( node )
		{
		node->area -= (node->area*node->PercentageToCalculateAneurysm)/(100-node->PercentageToCalculateAneurysm);
		node->PercentageToCalculateAneurysm = 0;
		node->radius = sqrt(node->area/vtkMath::Pi());
		}
}

//----------------------------------------------------------------------------
double vtkHM1DSegment::GetVolume()
{
	double volume = 0;
	double dx = this->GetLength()/this->GetElementNumber();

	for (int i=0; i<this->GetElementNumber(); i++)
		{
		vtkHMNodeData *node = this->GetNodeData(i);
		vtkHMNodeData *node2 = this->GetNodeData(i+1);

		volume += ((node->area + node2->area) * dx)/ 2;
		}

	return volume;
}

//----------------------------------------------------------------------------
double vtkHM1DSegment::GetLength()
{
	vtkHMNodeData *node;
	vtkHMNodeData *node2;

	double length=0;
	double cat1, cat2, cat3;

	for (int i=0; i<this->GetElementNumber(); i++)
		{
		vtkHMNodeData *node = this->GetNodeData(i);
		vtkHMNodeData *node2 = this->GetNodeData(i+1);
		cat1 = node->coords[0] - node2->coords[0];
		cat2 = node->coords[1] - node2->coords[1];
		cat3 = node->coords[2] - node2->coords[2];

		length += sqrt((cat1*cat1) + (cat2*cat2) + (cat3*cat3));  // calculo da distancia entre os pontos

		}

	this->SetLength(length);

	return length;
}
