/*
 * $Id:vtkHM1DSegment.h 109 2006-03-27 15:04:14Z diego $
 */

 // .NAME vtkHM1DSegment - Class that represents a segment of the 1D
// straigt tubes representation of the arterial tree.
// .SECTION Description
// vtkHM1DSegment is the representation of the 1D Tree Elements section for a
// strait tubes one dimensional representation of the arterial tree. Its responsible
// for handling the information of a particular tree segment.

#ifndef _vtkHM1DSegment_h_
#define _vtkHM1DSegment_h_

#include "vtkHM1DTreeElement.h"

#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkMath.h"

#include <list> // List of node data.
#include <map> // hash of node data.
#include <string>
using namespace std;

class vtkHMNodeData;
class vtkHMElementData;

class VTK_EXPORT vtkHM1DSegment:public vtkHM1DTreeElement
{
public:
//BTX
  typedef std::map<int,vtkHMNodeData *> NodeDataMap;
  typedef std::pair<int,vtkHMNodeData *> NodeDataPair;

  typedef std::map<int,vtkHMElementData *> ElementDataMap;
  typedef std::pair<int,vtkHMElementData *> ElementDataPair;
//ETX
protected:

  // Description:
  // Lenght of the segment.
  double Length;

  // Description:
  // Number of elements in the segment.
  int NumberOfElements;

  // Description:
  // Size of every elemen inside a segment.
  double dx;

  // Description:
  // Is the wall viscoelastic.
  bool isViscoelastic;

  // Description:
  // Interpolation type: 1 is equal linear and zero is equal constant
  int interpolationType;

//BTX
  // Description:
  // node information data list.
  NodeDataMap nodeData;
//ETX

//BTX
  // Description:
  // Element information data list.
  ElementDataMap ElementData;
//ETX



  // Description:
  // parameters used to create the line equation (used to interpolate values in the segment)
  double A;
  double B;

  // Descripition:
  // these two double arrays store the properties values from distal
  // and proximal elements from current segment
  double proxElem[10];
  double distalElem[10];

  // Description:
  // Name of the segment
  char SegmentName[100];

  // Description:
  // String that will be returned when the values of resolution array or node/ segment info have to be sent to the client
  string Result;

public:
/*
 * Esta macro cria os metodos de revisão e de typo usados pelo vtkObject.
 */
  vtkTypeRevisionMacro(vtkHM1DSegment,vtkHM1DTreeElement);
  void PrintSelf(ostream& os, vtkIndent indent);
  static vtkHM1DSegment *New();

  // Description:
  // Set/Get the Length of the segment.
  vtkSetMacro(Length,double);
  double GetLength();

  // Description:
  // Set/Get the number of elements of the segment.
  vtkSetMacro(NumberOfElements,int);
  vtkGetMacro(NumberOfElements,int);

  // Description:
  // Set/Get the size of the element.
  void SetDx(double);
  double GetDx();

//BTX
  // Description:
  // Set/Get the data of a specific node.
  void SetNodeData(int,vtkHMNodeData);
  vtkHMNodeData *GetNodeData(int);
//ETX

//BTX
  // Description:
  // Set/Get the data of a specific element.
  void SetElementData(int,vtkHMElementData);
  vtkHMElementData *GetElementData(int);
//ETX

  // Description:
  // To turn On/Off the viscoelasticity bit.
  vtkBooleanMacro(isViscoelastic,bool);

	// Description:
  // Set/Get the viscoelasticity bit.
  vtkSetMacro(isViscoelastic,bool);
  vtkGetMacro(isViscoelastic,bool);

	vtkSetMacro(interpolationType,int);
	vtkGetMacro(interpolationType,int);

	// Description:
	// Deep copy
	void DeepCopy(vtkHM1DSegment *);

	// Description:
	// returns the number of nodes from the segment
	int GetNodeNumber(void);

	// Description:
	// returns the number of elements from the segment
	int GetElementNumber(void);

	// Description:
	// return basic element info (same info from vtkHMElementData) as char array
	const char *GetElementArray(int elem);

	// Description:
	// return node basic info (vtkHMNodeData) as char array
	const char *GetNodeArray(int NodeOrder);

	// Description:
	// return Resolution Array from some node especified by NodeOrder in the segment
	vtkDoubleArray *GetResolutionArray(int NodeOrder);
  void SetResolutionArray(int NodeOrder, vtkDoubleArray *array);

	// Description:
	// Set the properties (by linear interpolation or constant values) from every node in the segment
	void SetNodesProps(double nodeArray[7]);

	// Description:
	// Set the values from some single node in the segment
  void SetSingleNodeProp( double radius, double wall, int NodeOrder);

  // Description:
	// Set the values from some single element in the segment
  void SetSingleElem(double ElemArray[11]);

  // Description:
	// Set the values from all elements in the segment.
	// Two update methods can be used: Liner interpolation or Constant values
	// The ivar InterpolationType with value 1 specifies that Linear Interpolation should be used - O means every element
	// should have the same value
	void SetElementsFromSegment(int InterpolationType);

	// Description:
	// used to establish the coefficients from a line equation - when properties are
	// linear interpolated.
	void SetEquationCoefficient(double v1, double v2, double SegmentSize);

	// Description:
	// Return the value from some property (when a linear interpolation is being used)
	double GetY(double x);

	// Description:
	// set the values of segment's proximal element. This is used when all elements from segment are updated.
	void SetProxElem(double proxElem[10]);

	// Description:
	// set the values of segment's distal element. This is used when all elements from segment are updated.
  void SetDistalElem(double distalElem[10]);

  // Description:
	// Return the number of tuples in the Resolution array
	int GetResolutionNumberOfTuples(int NodeOrder);

  // Description:
	// Return the Resolution array as string
	//Type 'E' Element or 'N' Node.
	const char *GetResolutionArrayAsChar(int NodeOrder, char Type);

  // Description:
	// Return the number of components from Resolution array tuple
	int GetResolutionArrayNumberOfComponents(int NodeOrder);

	// Description:
	// Remove one node data
	int RemoveNodeData(int node);

	// Description:
	// Remove node data between begin and end node
	void RemoveNodeData(int beginNode, int endNode);

	// Description:
	// Remove one element data
	int RemoveElementData(int element);

	// Description:
	// Remove element data between begin and end element
	void RemoveElementData(int beginElement, int endElement);

	// Description:
	// Set/Get Name
//	vtkSetMacro(SegmentName, *char);
//	vtkGetMacro(SegmentName, *char);
	void SetSegmentName(char *n);
	char *GetSegmentName();

	void SetId(vtkIdType id);

	// Description:
	// Invert flow direction.
	int InvertFlowDirection();

	// Description:
	// Add stent, multiply the elastin and visco-elasticity for the respective factors.
	void AddStent(vtkIdType idElement, double elastinFactor, double viscoElasticityFactor);

	// Description:
	// Remove stent, it divides the elastin and visco-elasticity for
	// the respective factors of the element.
	void RemoveStent(vtkIdType idElement);

	// Description:
	// Add stenosis, percentage of reduction of area
	void AddStenosis(vtkIdType idNode, double percentage);

	// Description:
	// Remove stenosis, set the percentage in zero and return
	// value of area with default value
	void RemoveStenosis(vtkIdType idNode);

	// Description:
	// Add aneurysm, percentage of increase of area
	void AddAneurysm(vtkIdType idNode, double percentage);

	// Description:
	// Remove aneurysm, set the percentage in zero and return
	// value of area with default value
	void RemoveAneurysm(vtkIdType idNode);

  // Description:
  // Return volume of the segment.
  double GetVolume();

protected:
	vtkHM1DSegment();
	~vtkHM1DSegment();
};
//BTX
class vtkHMNodeData{
// .NAME vtkHMNodeData - Class holding the information of the node data.
// .SECTION Description
// vtkHMNodeData is a source object that reads ASCII or binary
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Biblioteca_de_Elementos
//  for format details).
public:
	vtkHMNodeData()
		{
		area 						= 3.141593;
		alfa 						= 0.3141593;
		radius 					= sqrt(area/vtkMath::Pi());
		ResolutionArray = NULL;
		meshID = -1;
		PercentageToCalculateStenosis = 0;
		PercentageToCalculateAneurysm = 0;
		}

  // Description:
  // Node coordinates, only the first and the last point coordinates are really used.
  // The other points coordinates are a linear interpolation of the first and last coordinate.
  double coords[3];
  // Description:
  // Area of the cross section.
  double area;
  // Description:
  // Radius at the cross section.
  double radius;

  // Description:
  // Value of alfa of the node
  double alfa;

  int meshID;

  // Description:
  // Percentage of reduction of the area
  double PercentageToCalculateStenosis;

  // Description:
  // Percentage of increase of the area
  double PercentageToCalculateAneurysm;

  //Description:
  //Resolution of the data submit when solver
  vtkDoubleArray *ResolutionArray;
};
//ETX


//BTX
class vtkHMElementData{

// .NAME vtkHMElementData - Class holding the information of the element data.
// .SECTION Description
// vtkHMElementData is a source object that reads ASCII or binary
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Biblioteca_de_Elementos
//  for format details).
public:

	vtkHMElementData()
		{
		Elastin = 8000000;
		Collagen = 0;
		CoefficientA = 1;
		CoefficientB = 1;
		Viscoelasticity = 44400;
		ViscoelasticityExponent = 1;
		InfiltrationPressure = 0;
		ReferencePressure = 100000;
		Permeability = 0;
		WallThickness = 0;

		ElastinStentFactor = 1;
		ViscoelasticityStentFactor = 1;
		meshID = -1;
		}

	int meshID;

  // Description:
  // Elastin content in the arterial wall.
  double Elastin;

  // Description:
  // Collagen content at the arterial wall.
  double Collagen;

  // Description:
  // Value of coefficient A in the constituent model of the arterial wall
  double CoefficientA;

  // Description:
  // Value of coefficient B in the constituent model of the arterial wall
  double CoefficientB;

  // Description:
  // Value of viscoelasticity of the element
  double Viscoelasticity;

  // Description:
  // Value of the exponent of the model of viscoelasticity of the element
  double ViscoelasticityExponent;

  // Description:
  // Value of infiltration pressure
  double InfiltrationPressure;

  // Description:
  // Value of reference pressure
  double ReferencePressure;

  // Description:
  // Value of permeability of the arterial wall
  double Permeability;

  // Description:
  // Identifier the left and right node of the element
  vtkIdType idNode[2];

  // Description:
  // Wall thikness.
  double WallThickness;

  // Description:
  // Multiplicative factor of the elastin for stent
  double ElastinStentFactor;

  // Description:
  // Multiplicative factor of the viscoelasticity for stent
  double ViscoelasticityStentFactor;

//  // Description:
//  // Multiplicative factor of the elastin for aging
//  double ElastinAgingFactor;
//
//  // Description:
//  // Multiplicative factor of the viscoelasticity for aging
//  double ViscoelasticityAgingFactor;

};
//ETX

#endif /*_vtkHM1DSegment_h_*/
