/*
 *$Id: vtkHM1DStraightModel.cxx 2884 2010-05-11 12:37:55Z igor $
 */

#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkClientServerID.h"
#include "vtkClientServerStream.h"
#include "vtkProcessModule.h"
//
#include "vtkPVProcessModule.h"
#include "vtkIdList.h"
//
#include <map>

vtkCxxRevisionMacro(vtkHM1DStraightModel, "$Rev: 2884 $");
vtkStandardNewMacro(vtkHM1DStraightModel);

typedef std::map<vtkIdType,vtkHM1DTreeElement *>  TreeNodeDataMap;
typedef std::pair<vtkIdType,vtkHM1DTreeElement *> TreeNodeDataPair;

//Informações do nome do segmento e o id. Auxilia na busca do segmento pelo nome
typedef std::map<char *, vtkIdType> SegmentInformationMap;
typedef std::pair<char *, vtkIdType> SegmentInformationPair;

//Informações de segmento e os elementos com intervenções ou patologias
typedef std::map<vtkIdType, vtkIdList*> InterventionsMap;
typedef std::pair<vtkIdType, vtkIdList*> InterventionsPair;

//Informações de relação de intervenções ou patologias com segmento.
typedef std::multimap<vtkIdType,vtkIdType> InterventionsInformationMultimap;
typedef std::pair<vtkIdType,vtkIdType> InterventionsInformationPair;

//Informações de segmento e os elementos e os terminais nulos no
//modelo acoplado.
typedef std::map<vtkIdType, vtkIdList*> NullElementsMap;
typedef std::pair<vtkIdType, vtkIdList*> NullElementsPair;

vtkHM1DStraightModel::vtkHM1DStraightModel():vtkHMSimplifiedModel()
{
//  this->DebugOn();
  treeRoot = NULL;
  treeMesh = NULL;
  modelName = "1DStraightModel";
  MaxId = 0;
  this->InstantOfTime = vtkDoubleArray::New();

  this->ElementVisited = vtkIdList::New();

  this->AgingElastinFactor = 1;
  this->AgingViscoelasticityFactor = 1;

  this->ClipSegments = vtkIntArray::New();
  this->ClipSegments->SetNumberOfComponents(2);

  this->FinalTime = 0;
}

vtkHM1DStraightModel::~vtkHM1DStraightModel()
{
//	if (treeRoot)
//		this->ClearTree();

	TreeNodeDataMap::iterator it;
  it = segmentsMap.begin();
  while ( it != segmentsMap.end() )
	  {
//	  segmentsMap.erase(remId);
	  this->RemoveElementInternal(it->first);
	  it++;
	  }
	segmentsMap.clear();

	it = terminalsMap.begin();
  while ( it != terminalsMap.end() )
	  {
//	  segmentsMap.erase(remId);
	  this->RemoveElementInternal(it->first);
	  it++;
	  }
	terminalsMap.clear();

	if ( this->InstantOfTime )
		this->InstantOfTime->Delete();

	this->ElementVisited->Delete();

	this->SegmentNamesMap.clear();

	this->ClipSegments->Delete();

	InterventionsMap::iterator itIntervention = this->StentMap.begin();
	while ( itIntervention != this->StentMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	itIntervention = this->StenosisMap.begin();
	while ( itIntervention != this->StenosisMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	itIntervention = this->AneurysmMap.begin();
	while ( itIntervention != this->AneurysmMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	this->ClearNullElements1D3DMap();
}

void vtkHM1DStraightModel::DeepCopy(vtkHM1DStraightModel *orig)
{
  this->ClearTree();

	if ( !orig )
    return;
	this->InstantOfTime->DeepCopy(orig->GetInstantOfTime());
	// tengo que copiar toda la estructra del arbol
	// cuidado que tengo que crear los objetos a travez del ProcMod!
	// Recupero a raiz da arvore original
	vtkHM1DTerminal *origRoot = vtkHM1DTerminal::SafeDownCast(orig->Get1DTreeRoot());
	if ( !origRoot )
    return;

  //Reseta a lista de elementos visitados.
  this->ElementVisited->Reset();

	// Crio a raiz da arvore
	vtkHM1DTerminal *Term;
	Term = vtkHM1DTerminal::SafeDownCast(this->Get1DTreeRoot());
	Term->DeepCopy(origRoot);
	this->treeRoot = Term;

	this->ElementVisited->InsertNextId(origRoot->GetId());

	vtkHM1DTreeElement *origChild;
  //acessa a instancia unica do ProcessModule
  origChild = origRoot->GetFirstChild();

  while (origChild)
  	{
	  if (orig->IsSegment(origChild->GetId()))
    	{
    	CloneSegment(vtkHM1DStraightModel::SafeDownCast(orig),
    							 vtkHM1DTreeElement::SafeDownCast(orig->Get1DTreeRoot()),
    							 vtkHM1DTreeElement::SafeDownCast(treeRoot),
    							 vtkHM1DSegment::SafeDownCast(origChild));
    	}
 		else if (orig->IsTerminal(origChild->GetId()))
    	{
    	CloneTerminal(vtkHM1DStraightModel::SafeDownCast(orig),
    								vtkHM1DTreeElement::SafeDownCast(orig->Get1DTreeRoot()),
    							  vtkHM1DTreeElement::SafeDownCast(treeRoot),
    								vtkHM1DTerminal::SafeDownCast(origChild));
    	}
    origChild = origRoot->GetNextChild();
  	}

  //copiar os fatores de envelhecimento
  this->SetAgingFactor(orig->GetAgingElastinFactor(), orig->GetAgingViscoelasticityFactor());

  //copiar os clips
  vtkIdType clipId[2];
  for ( int i=0; i<orig->GetNumberOfClips(); i++ )
  	{
  	orig->GetClip(i, clipId);
  	this->AddClip(orig->GetSegment(clipId[0])->GetSegmentName(), orig->GetSegment(clipId[1])->GetSegmentName());
  	}

  //copiar os stent
  InterventionsInformationMultimap multiMap = orig->GetStentsInformation();
  InterventionsInformationMultimap::iterator itMulti = multiMap.begin();
  while ( itMulti != multiMap.end() )
  	{
  	//pega a lista de elementos com stent
  	vtkIdList *l = orig->GetStent(itMulti->second);
  	//pega o nome do segmento
  	char *name = orig->GetSegment(itMulti->first)->GetSegmentName();
  	//busca o segmento pelo nome e adiciona o stent com o seu id no straight model.
  	//Isso é feito devido ser criado um novo straight model, podendo assim,
  	//os elementos da árvore ficarem com ids diferentes, sendo assim, o segmento é
  	//buscado pelo nome e não pelo id.
  	this->AddStent(this->GetSegment(name)->GetId(), itMulti->second, l);
  	itMulti++;
  	}

  //copiar as stenosis
  multiMap = orig->GetStenosisInformation();
  itMulti = multiMap.begin();
  while ( itMulti != multiMap.end() )
  	{
  	//pega a lista de elementos com stenosis
  	vtkIdList *l = orig->GetStenosis(itMulti->second);
  	//pega o nome do segmento
  	char *name = orig->GetSegment(itMulti->first)->GetSegmentName();
  	//busca o segmento pelo nome e adiciona a stenosis com o seu id no straight model.
  	//Isso é feito devido ser criado um novo straight model, podendo assim,
  	//os elementos da árvore ficarem com ids diferentes, sendo assim, o segmento é
  	//buscado pelo nome e não pelo id.
  	this->AddStenosis(this->GetSegment(name)->GetId(), itMulti->second, l);
  	itMulti++;
  	}

  //copiar os aneurismas
  multiMap = orig->GetAneurysmInformation();
  itMulti = multiMap.begin();
  while ( itMulti != multiMap.end() )
  	{
  	//pega a lista de elementos com aneurisma
  	vtkIdList *l = orig->GetAneurysm(itMulti->second);
  	//pega o nome do segmento
  	char *name = orig->GetSegment(itMulti->first)->GetSegmentName();
  	//busca o segmento pelo nome e adiciona o aneurisma com o seu id no straight model.
  	//Isso é feito devido ser criado um novo straight model, podendo assim,
  	//os elementos da árvore ficarem com ids diferentes, sendo assim, o segmento é
  	//buscado pelo nome e não pelo id.
  	this->AddAneurysm(this->GetSegment(name)->GetId(), itMulti->second, l);
  	itMulti++;
  	}

  //Reseta a lista de elementos visitados.
  this->ElementVisited->Reset();

  this->FinalTime = orig->GetFinalTime();
}

void vtkHM1DStraightModel::CloneTerminal(vtkHM1DStraightModel *treeOrig, vtkHM1DTreeElement * parent, vtkHM1DTreeElement * cloneParent, vtkHM1DTerminal *orig)
{
	if ( this->ElementVisited->IsId(orig->GetId()) != -1 )
		return;

	// ojo con esta linea!!!!
	vtkIdType id = AddTerminal(cloneParent->GetId());
	vtkHM1DTreeElement *origChild;
	vtkHM1DTerminal *elem = GetTerminal(id);

  // soy una hoja!
  if (!orig)
  	return;

	elem->DeepCopy(orig);

	if ( treeOrig->IsNullElements1D3D(orig->GetId()))
		{
		vtkIdList * list = treeOrig->GetNullElements1D3DList(orig->GetId());
		this->AddNullElements1D3D(id, list);
		}

	this->ElementVisited->InsertNextId(orig->GetId());

//	vtkHM1DTreeElement *child;
  //acessa a instancia unica do ProcessModule
  origChild = orig->GetFirstChild();

  while (origChild)
  	{
	  if (treeOrig->IsSegment(origChild->GetId()))
    	{
    	CloneSegment(vtkHM1DStraightModel::SafeDownCast(treeOrig),
    							 vtkHM1DTreeElement::SafeDownCast(orig),
    							 vtkHM1DTreeElement::SafeDownCast(elem),
    							 vtkHM1DSegment::SafeDownCast(origChild));
    	}
 		else if (treeOrig->IsTerminal(origChild->GetId()))
    	{
    	CloneTerminal(vtkHM1DStraightModel::SafeDownCast(treeOrig),
    								vtkHM1DTreeElement::SafeDownCast(orig),
 	   							  vtkHM1DTreeElement::SafeDownCast(elem),
    								vtkHM1DTerminal::SafeDownCast(origChild));
    	}
    origChild = orig->GetNextChild();
  	}
}

//
// Talvez tenga que crear un metodo addsegment internal que no me cree los
// terminales, para que no me de problemas de crear elementos repetidos!!
//
void vtkHM1DStraightModel::CloneSegment(vtkHM1DStraightModel *treeOrig, vtkHM1DTreeElement * parent, vtkHM1DTreeElement * cloneParent, vtkHM1DSegment *orig)
{
	if ( this->ElementVisited->IsId(orig->GetId()) != -1 )
		return;

	//
	vtkIdType id = AddSegmentInternal(cloneParent->GetId(), orig->GetSegmentName());

	vtkHM1DTreeElement *origChild;
	vtkHM1DSegment *elem = GetSegment(id);
	  // soy una hoja!
  if (!orig)
    return;

	// copio todos los datos de este objeto
	elem->DeepCopy(orig);

	if ( treeOrig->IsNullElements1D3D(orig->GetId()))
		{
		vtkIdList * list = treeOrig->GetNullElements1D3DList(orig->GetId());
		this->AddNullElements1D3D(id, list);
		}

	this->ElementVisited->InsertNextId(orig->GetId());

//	vtkHM1DTreeElement *child;
  //acessa a instancia unica do ProcessModule
  origChild = orig->GetFirstChild();
  // soy una hoja!
  if (!origChild)
    return;
  while (origChild)
  	{
	  if (treeOrig->IsSegment(origChild->GetId()))
    	{
    	CloneSegment(vtkHM1DStraightModel::SafeDownCast(treeOrig),
    							 vtkHM1DTreeElement::SafeDownCast(orig),
    							 vtkHM1DTreeElement::SafeDownCast(elem),
    							 vtkHM1DSegment::SafeDownCast(origChild));
    	}
 		else if (treeOrig->IsTerminal(origChild->GetId()))
    	{
    	CloneTerminal(vtkHM1DStraightModel::SafeDownCast(treeOrig),
    								vtkHM1DTreeElement::SafeDownCast(orig),
 	   							  vtkHM1DTreeElement::SafeDownCast(elem),
    								vtkHM1DTerminal::SafeDownCast(origChild));
    	}
      origChild = orig->GetNextChild();
  	}
}

vtkHM1DTreeElement *vtkHM1DStraightModel::Get1DTreeRoot()
{
  if (!treeRoot)
    {
   	vtkDebugMacro( << "Creating 1D Tree root.");
    vtkPVProcessModule* pm = vtkPVProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());
    if (!pm)
      {
      vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
      return NULL;
      }

    // requisicao ao ProcessModule de um ClientServerID
    vtkClientServerID terminalCSId = pm->GetUniqueID();
    // criacao de stream para criacao de um objeto do tipo vtkHM1DTerminal
    vtkClientServerStream initStream;
    initStream << vtkClientServerStream::New
             << "vtkHM1DTerminal" << terminalCSId
             << vtkClientServerStream::End;
    // envia stream para o DATA_SERVER
    pm->SendStream(vtkProcessModule::DATA_SERVER, initStream);
    // realiza associacao com o obejto do tipo terminal recem criado
    vtkHM1DTerminal *root = vtkHM1DTerminal::SafeDownCast(pm->GetObjectFromID(terminalCSId));
    // grava o ClientSeverID no novo terminal
    root->SetClientServerID(terminalCSId);
    //  vtkHM1DTerminal *root = vtkHM1DTerminal::New();
    MaxId++;
    root->SetId(MaxId);
    terminalsMap.insert(TreeNodeDataPair(MaxId,root));
    treeRoot = root;
//    this->treeRoot->SetIsInMainTree(true);
   	vtkDebugMacro( << "1D Tree root created succesfully");
    }
  return treeRoot;
}

vtkIdType vtkHM1DStraightModel::AddSegmentInternal(vtkIdType parentId)
{
  vtkHM1DTreeElement *parent = NULL;
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(parentId);

  //Se nao encontrou o parent no map de segmentos, procura no map de terminal
  if ( it == segmentsMap.end() )
  	{
  	it = terminalsMap.find(parentId);
  	}

  if (parentId == treeRoot->GetId())
    {
    parent = treeRoot;
    }
  else if (it!=segmentsMap.end())
    {
    parent = it->second;
    }
  else
    {
    vtkWarningMacro(<< "Given segment (parent) does not exist!. id: " << parentId);
    return 0;
    }
  // criação do objeto atraves do ProcessModule
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
    {
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return 0;
    }
  // Requisicao ao ProcessModule de um ClientServerID
  vtkClientServerID segmentCSId = pm->GetUniqueID();
  // Criacao de stream para criacao de um objeto remoto do tipo vtkHM1DSegment
  vtkClientServerStream initStream;
  initStream << vtkClientServerStream::New
             << "vtkHM1DSegment" << segmentCSId
             << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER, initStream);
  // Objeto criado localmente
  vtkHM1DSegment *newSegm = vtkHM1DSegment::SafeDownCast(pm->GetObjectFromID(segmentCSId));
  // Assigna o ClientSeverID no novo segmento
  newSegm->SetClientServerID(segmentCSId);
  MaxId++;
  newSegm->SetId(MaxId);

  segmentsMap.insert(TreeNodeDataPair(MaxId,newSegm));
  this->SegmentNamesMap.insert(SegmentInformationPair(newSegm->GetSegmentName(),MaxId));
  parent->AddChild(newSegm);

  return newSegm->GetId();
}

vtkIdType vtkHM1DStraightModel::AddSegmentInternal(vtkIdType parentId, char *segmentName)
{
  vtkHM1DTreeElement *parent = NULL;
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(parentId);

  //Se nao encontrou o parent no map de segmentos, procura no map de terminal
  if ( it == segmentsMap.end() )
  	{
  	it = terminalsMap.find(parentId);
  	}

  if (parentId == treeRoot->GetId())
    {
    parent = treeRoot;
    }
  else if (it!=segmentsMap.end())
    {
    parent = it->second;
    }
  else
    {
    vtkWarningMacro(<< "Given segment (parent) does not exist!. id: " << parentId);
    return 0;
    }
  // criação do objeto atraves do ProcessModule
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
    {
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return 0;
    }
  // Requisicao ao ProcessModule de um ClientServerID
  vtkClientServerID segmentCSId = pm->GetUniqueID();
  // Criacao de stream para criacao de um objeto remoto do tipo vtkHM1DSegment
  vtkClientServerStream initStream;
  initStream << vtkClientServerStream::New
             << "vtkHM1DSegment" << segmentCSId
             << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER, initStream);
  // Objeto criado localmente
  vtkHM1DSegment *newSegm = vtkHM1DSegment::SafeDownCast(pm->GetObjectFromID(segmentCSId));
  // Assigna o ClientSeverID no novo segmento
  newSegm->SetClientServerID(segmentCSId);
  MaxId++;
  newSegm->SetId(MaxId);
  newSegm->SetSegmentName(segmentName);

  segmentsMap.insert(TreeNodeDataPair(MaxId,newSegm));
  this->SegmentNamesMap.insert(SegmentInformationPair(newSegm->GetSegmentName(),MaxId));
  parent->AddChild(newSegm);

  return newSegm->GetId();
}

vtkIdType vtkHM1DStraightModel::AddSegmentWithoutChild(vtkIdType parentId)
{
	return this->AddSegmentInternal(parentId);
}

vtkIdType vtkHM1DStraightModel::AddSegment(vtkIdType parentId)
{
  vtkHM1DTreeElement *parent = NULL;
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(parentId);

  //Se nao encontrou o parent no map de segmentos, procura no map de terminal
  if ( it == segmentsMap.end() )
  	{
  	it = terminalsMap.find(parentId);
  	}

  //Se parent eh igual a coracao
  if (parentId == treeRoot->GetId())
    {
    parent = treeRoot;
    }
  //Se encontrou o parent
  else if (it!=segmentsMap.end())
    {
    parent = it->second;
    }
  else
    {
    vtkWarningMacro(<< "Given segment (parent) does not exist!. id: " << parentId);
    return 0;
    }

  // Se somente tenho um filho e ele é um terminal
  /*if (parent->GetChildCount()==1)
    {
    if (IsTerminal(parent->GetFirstChild()->GetId()))
      {
      RemoveTerminal(parent->GetFirstChild()->GetId(),0);
      }
    else if (IsSegment(parent->GetFirstChild()->GetId()))
      {
      AddTerminal(parent->GetId());
      }
    }*/
  // criação do objeto atraves do ProcessModule
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
    {
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return 0;
    }
  // Requisicao ao ProcessModule de um ClientServerID
  vtkClientServerID segmentCSId = pm->GetUniqueID();
  // Criacao de stream para criacao de um objeto remoto do tipo vtkHM1DSegment
  vtkClientServerStream initStream;
  initStream << vtkClientServerStream::New
             << "vtkHM1DSegment" << segmentCSId
             << vtkClientServerStream::End;
  // Como vamos liberar esta memoria.
  // enviar para o DATA_SERVER???
  pm->SendStream(vtkProcessModule::DATA_SERVER, initStream);
  // Objeto criado localmente
  // vtkHM1DSegment *newSegm = vtkHM1DSegment::New();
  // cria referencia ao objeto recem criado
  vtkHM1DSegment *newSegm = vtkHM1DSegment::SafeDownCast(pm->GetObjectFromID(segmentCSId));
  // Assigna o ClientSeverID no novo segmento
  newSegm->SetClientServerID(segmentCSId);

  MaxId++;
  newSegm->SetId(MaxId);

  segmentsMap.insert(TreeNodeDataPair(MaxId,newSegm));
  this->SegmentNamesMap.insert(SegmentInformationPair(newSegm->GetSegmentName(),MaxId));

  AddTerminal(newSegm->GetId());

  parent->AddChild(newSegm);

  return newSegm->GetId();
}

vtkIdType vtkHM1DStraightModel::AddSegment(vtkHM1DTreeElement *parent)
{
  if (parent)
    {
    return AddSegment(parent->GetId());
    }
  else
    {
    vtkWarningMacro(<< "Given element (parent) does not exist!.");
    return 0;
    }
}

int vtkHM1DStraightModel::ClearTree()
{
  if ( !this->treeRoot )
    return 1;

  this->RemoveElementChildsRecursively(treeRoot->GetId());
  terminalsMap.erase(treeRoot->GetId());
  this->RemoveElementInternal(treeRoot->GetId());

  this->MaxId = 0;
  this->treeRoot = NULL;
  return 1;
}

void vtkHM1DStraightModel::RemoveElementChildsRecursively(int id)
{
  vtkHM1DTreeElement *elem;
  vtkHM1DTreeElement *child;
  int remId;
  //acessa a instancia unica do ProcessModule
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (IsSegment(id))
    {
    elem = GetSegment(id);
    child = elem->GetFirstChild();
    }
  else if (IsTerminal(id))
    {
    elem = GetTerminal(id);
    child = elem->GetFirstChild();
    }
// Si o meu filho é NULL, sou uma folha-> Saida!
  if (!child)
  	return;

  vtkClientServerStream deleteStream;
  while (child)
    {
    remId = child->GetId();
    RemoveElementChildsRecursively(remId);
    elem->RemoveChild(child);
    child = elem->GetFirstChild();
    RemoveElementInternal(remId);

    if(IsSegment(remId))
      {
//      deleteStream 	<< vtkClientServerStream::Delete
//      				  	 	<< GetSegment(id)->GetClientServerID()
//		      				  << vtkClientServerStream::End;
 // 		deleta a referencia do ProcessoModule a um objeto do tipo Segment
 //     pm->DeleteStreamObject(GetSegment(id)->GetClientServerID());
//      GetSegment(remId)->Delete();
      this->SegmentNamesMap.erase(this->GetSegment(remId)->GetSegmentName());
      segmentsMap.erase(remId);
      }
    else if(IsTerminal(remId))
      {
//      deleta a referencia do ProcessoModule de um objeto do tipo Terminal
//      deleteStream  << vtkClientServerStream::Delete
//      				  		<< GetTerminal(id)->GetClientServerID()
//      				  		<< vtkClientServerStream::End;
//      pm->DeleteStreamObject(GetTerminal(id)->GetClientServerID());
//      GetTerminal(remId)->Delete();
      terminalsMap.erase(remId);
      }
	  pm->SendStream(vtkProcessModule::DATA_SERVER, deleteStream);
    }

// Este filho pode ser um segmento ou um terminal, vou remover de ambas as listas
// numa delas não existe.
}

int vtkHM1DStraightModel::RemoveElementInternal(vtkIdType id)
{
	vtkClientServerStream deleteStream;
	if(IsSegment(id))
	{
		vtkHM1DSegment *elem = GetSegment(id);

//		vtkClientServerStream deleteStream;
	  deleteStream 	<< vtkClientServerStream::Delete
				  	 				<< elem->GetClientServerID()
	   				  			<< vtkClientServerStream::End;

	}
	else if(IsTerminal(id))
	{
		vtkHM1DTerminal *elem = GetTerminal(id);
//		vtkClientServerStream deleteStream;
	  deleteStream 	<< vtkClientServerStream::Delete
				  	 				<< elem->GetClientServerID()
	   				  			<< vtkClientServerStream::End;

	}
//	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
//	pm->SendStream(vtkProcessModule::DATA_SERVER, deleteStream);


	/*vtkHM1DTreeElement *elem = GetSegment(id);
	if (elem)
	  {
	  vtkClientServerStream deleteStream;
	  deleteStream 	<< vtkClientServerStream::Delete
				  	 				<< elem->GetClientServerID()
	   				  			<< vtkClientServerStream::End;
	  if (elem)
	  	{
	  	GetSegment(id)->Delete();
	  	}
	  else
	  	{
	  	GetTerminal(id)->Delete();
	  	}
	  }
	  */
	return 1;
}

int vtkHM1DStraightModel::RemoveSegment(vtkIdType id, int recurse)
{
  vtkHM1DTreeElement *child;
  vtkHM1DTreeElement *elem = GetSegment(id);
  vtkHM1DTreeElement *term;

  if (elem)
    {
    if (recurse)
      {
      child = elem->GetFirstChild();
      while (child)
        {
        RemoveElementChildsRecursively(child->GetId());
        //tengo que borrar cada uno de los elementos del map
        child = elem->GetNextChild();
        }
      elem->RemoveChild(child);
      }
    else
      {
      child = elem->GetFirstChild();
      while ( !this->IsTerminal(child->GetId() ))
				child = elem->GetNextChild();
			term = child;

			if ( (term->IsLeaf()) && (term->GetNumberOfParents() <= 1) )
				this->RemoveTerminal(term->GetId(), 0);
			else
				{
				term->RemoveParent(elem);
				elem->RemoveChild(term);
				}

      vtkHM1DTreeElement *parent = elem->GetFirstParent();
      while ( parent )
	      {
      	parent->RemoveChild(elem);

	      if ( (this->IsTerminal(parent->GetId())) && (parent->GetChildCount() <= 0) && (parent != this->treeRoot) )
	      	{
	        vtkHM1DTerminal::ResistanceValuesList r1;
	        vtkHM1DTerminal::ResistanceTimeList r1Time;
	        vtkHM1DTerminal::ResistanceValuesList r2;
	        vtkHM1DTerminal::ResistanceTimeList r2Time;
	        vtkHM1DTerminal::CapacitanceValuesList c;
	        vtkHM1DTerminal::CapacitanceTimeList cTime;
	        vtkHM1DTerminal::PressureValuesList p;
	        vtkHM1DTerminal::PressureValuesList pTime;

	        //Adicionar zero as listas
	        r1.push_back(0);
	        r2.push_back(0);
	        c.push_back(0);
	        p.push_back(0);
	        r1Time.push_back(0);
	        r2Time.push_back(0);
	        cTime.push_back(0);
	        pTime.push_back(0);

	        //Setar as listas
	        vtkHM1DTerminal::SafeDownCast(parent)->SetR1(&r1);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetR1Time(&r1Time);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetR2(&r2);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetR2Time(&r2Time);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetC(&c);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetCTime(&cTime);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetPt(&p);
	        vtkHM1DTerminal::SafeDownCast(parent)->SetPtTime(&pTime);

          if ( parent->GetNumberOfParents() <= 0 )
            this->RemoveTerminal(parent->GetId(), 0);
	      	}

	      parent = elem->GetNextParent();
	      }
      RemoveElementInternal(elem->GetId());
      }
    this->SegmentNamesMap.erase(vtkHM1DSegment::SafeDownCast(elem)->GetSegmentName());
    segmentsMap.erase(elem->GetId());

    return 1;
    }
  else
    return 0;
}

int vtkHM1DStraightModel::RemoveSegmentsInSubTree(vtkIdList *list, int recurse)
{
	if(list)
		{
		for(int i=0; i<list->GetNumberOfIds(); i++)
			this->RemoveSegment(list->GetId(i), recurse);
		return 1;
		}
	else
		return 0;
}


int vtkHM1DStraightModel::RemoveSegment(vtkHM1DTreeElement *elem, int recurse)
{
  if (elem)
    {
    RemoveSegment(elem->GetId(),recurse);
    return 1;
    }
  else
    {
    vtkWarningMacro(<< "Element does not exist!. id: " << elem);
    return 0;
    }
}

//-------------------------------------------------------------------
int vtkHM1DStraightModel::RemoveSegment(vtkIdType id, vtkIdType parentId, int recurse)
{
  vtkHM1DTreeElement *child;
  vtkHM1DTreeElement *elem = GetSegment(id);
  vtkHM1DTreeElement *term;
  if (elem)
    {
    vtkHM1DTreeElement *parent;
    if ( this->IsTerminal(parentId) )
    	parent = this->GetTerminal(parentId);
    else
    	parent = this->GetSegment(parentId);

    child = elem->GetFirstChild();

    while ( !this->IsTerminal(child->GetId() ))
			child = elem->GetNextChild();

		term = child;
		child = term->GetFirstChild();

    while (child)
      {
      child = term->PopFirstChild();
      parent->AddChild(child);
//      child->SetIsInMainTree(parent->GetIsInMainTree());

      child = term->GetFirstChild();
      }

    this->RemoveTerminal(term->GetId(), 0);

    parent = elem->GetFirstParent();
    while ( parent )
	    {
	    parent->RemoveChild(elem);

			if ( (this->IsTerminal(parent->GetId())) && (parent->GetNumberOfParents() > 0) )
	     	this->RemoveTerminal(parent->GetId(), 0);

	    parent = elem->GetNextParent();
	    }

	  RemoveElementInternal(elem->GetId());
	  this->SegmentNamesMap.erase(vtkHM1DSegment::SafeDownCast(elem)->GetSegmentName());
    segmentsMap.erase(elem->GetId());
    return 1;
    }
  else
    return 0;
}

vtkIdType vtkHM1DStraightModel::AddTerminal(vtkIdType parentId)
{
  // en realidad tengo que fijarme si existe en alguna de las listas,
  // Si existe el padre entonces puedo agregar el hijo.
  vtkHM1DTreeElement *parent = NULL;
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(parentId);
  if (it!=segmentsMap.end())
    {
    parent = it->second;
    }

  // criacao de um terminal atraves do ProcessModule
  // cria nova instancia do PM???
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
    {
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return 0;
    }
  // requisicao ao ProcessModule de um ClientServerID
  vtkClientServerID terminalCSId = pm->GetUniqueID();
  // Criacao de stream para criacao de um objeto remoto do tipo vtkHM1DTerminal
  vtkClientServerStream initStream;
  initStream << vtkClientServerStream::New
             << "vtkHM1DTerminal" << terminalCSId
             << vtkClientServerStream::End;
// Como vamos liberar esta memoria.
// enviar para o DATA_SERVER???
  pm->SendStream(vtkProcessModule::DATA_SERVER, initStream);
  vtkHM1DTerminal *newTerminal = vtkHM1DTerminal::SafeDownCast(pm->GetObjectFromID(terminalCSId));
  // Assigna o ClientSeverID no novo terminal
  newTerminal->SetClientServerID(terminalCSId);
  // Objeto criado localmente
  // vtkHM1DTerminal *newTerminal = vtkHM1DTerminal::New();

  MaxId++;
  newTerminal->SetId(MaxId);

  terminalsMap.insert(TreeNodeDataPair(MaxId,newTerminal));

	//Declaracao de listas de parametros do terminal
	vtkHM1DTerminal::ResistanceValuesList r1;
	vtkHM1DTerminal::ResistanceTimeList r1Time;
	vtkHM1DTerminal::ResistanceValuesList r2;
	vtkHM1DTerminal::ResistanceTimeList r2Time;
	vtkHM1DTerminal::CapacitanceValuesList c;
	vtkHM1DTerminal::CapacitanceTimeList cTime;
	vtkHM1DTerminal::PressureValuesList p;
	vtkHM1DTerminal::PressureValuesList pTime;

	//Adicionar zero as listas
	r1.push_back(0);
	r2.push_back(0);
	c.push_back(0);
	p.push_back(0);
	r1Time.push_back(0);
	r2Time.push_back(0);
	cTime.push_back(0);
	pTime.push_back(0);

	//Setar as listas
	newTerminal->SetR1(&r1);
	newTerminal->SetR1Time(&r1Time);
	newTerminal->SetR2(&r2);
	newTerminal->SetR2Time(&r2Time);
	newTerminal->SetC(&c);
	newTerminal->SetCTime(&cTime);
	newTerminal->SetPt(&p);
	newTerminal->SetPtTime(&pTime);

  //Se parent existe adicionar o novo terminal como filho
  if ( it!=segmentsMap.end() )
  	{
  	it->second->AddChild(newTerminal);
//  	newTerminal->SetIsInMainTree(it->second->GetIsInMainTree());
  	}

  /*
  // ***********************************area de teste
  vtkClientServerStream testeStream;
  vtkClientServerID testeCSId;
  testeCSId.ID=526;
  //testeStream << vtkClientServerStream::Invoke
    //         << testeCSId << "SetDx" << 23
      //       << vtkClientServerStream::End;

  testeStream << vtkClientServerStream::Invoke
             << testeCSId << "GetClientServerID"
             << vtkClientServerStream::End;

 // testeStream << vtkClientServerStream::Invoke
   //          << testeCSId.ID << "GetTerminalCSID"
     //        << vtkClientServerStream::End;


  pm->SendStream(vtkProcessModule::DATA_SERVER, testeStream);
  const vtkClientServerStream& res=pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  vtkClientServerID CSID;
  if(res.GetArgument(0,0,&CSID))
  //*********************************************************
  */

  return newTerminal->GetId();
}

vtkIdType vtkHM1DStraightModel::AddTerminal(vtkHM1DTreeElement *parent)
{
  if (parent)
    {
    return AddTerminal(parent->GetId());
    }
  else
    {
    vtkWarningMacro(<< "Given segment (parent) does not exist!.");
    return 0;
    }
}

int vtkHM1DStraightModel::RemoveTerminal(vtkIdType id, int recurse)
{
  vtkHM1DTreeElement *elem = this->GetTerminal(id);
  vtkHM1DTreeElement *child;
  if (elem)
    {
    if (recurse)
      {
      vtkHM1DTreeElement *child = elem->GetFirstChild();
      while (child)
        {
        RemoveElementChildsRecursively(child->GetId());
        //tengo que borra a cada uno de los elementos del map
        child = elem->GetNextChild();
        }
      elem->RemoveChild(child);
      }
    else
      {
      vtkHM1DTreeElement *parent = elem->GetFirstParent();
      if (parent == this->Get1DTreeRoot())
        {
        vtkWarningMacro(<< "Atempting to remove tree root, operation not allowed.");
        return 0;
        }

      child = elem->GetFirstChild();
      while (child)
        {
        child = elem->PopFirstChild();
        child = elem->GetFirstChild();
        }

      while ( parent )
      	{
      	parent->RemoveChild(elem);
      	parent = elem->GetNextParent();
      	}

      RemoveElementInternal(elem->GetId());
      }

    terminalsMap.erase(elem->GetId());
    return 1;
    }
  else
    return 0;
}

int vtkHM1DStraightModel::RemoveTerminal(vtkHM1DTreeElement *elem, int recurse)
{
  if (elem)
    {
    RemoveTerminal(elem->GetId(),recurse);
    return 1;
    }
  else
    {
    vtkDebugMacro(<< "Given segment (parent) does not exist!.");
    return 0;
    }
}


vtkHM1DSegment *vtkHM1DStraightModel::GetSegment(vtkIdType id)
{
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(id);
  if (it!=segmentsMap.end())
    {
    return (vtkHM1DSegment *)it->second;
    }
  else
    {
    vtkDebugMacro(<< "Segment does not exist!. id:" << id);
    return NULL;
    }
}

vtkHM1DSegment *vtkHM1DStraightModel::GetSegment(char *name)
{
  SegmentInformationMap::iterator it = this->SegmentNamesMap.begin();

  while ( it != this->SegmentNamesMap.end() )
    {
    if ( strcmp(it->first, name) == 0 )
    	{
    	return this->GetSegment(it->second);
    	}
    it++;
    }
  vtkDebugMacro(<< "Segment does not exist!. Name: " << name);
  return NULL;
}

vtkHM1DTerminal *vtkHM1DStraightModel::GetTerminal(vtkIdType id)
{
  TreeNodeDataMap::iterator it;
  it = terminalsMap.find(id);
  if (it!=terminalsMap.end())
    {
    return (vtkHM1DTerminal *) it->second;
    }
  else
    {
    vtkDebugMacro(<< "Terminal does not exist!. id:" << id);
    return NULL;
    }
}

bool vtkHM1DStraightModel::IsSegment(vtkIdType id)
{
  TreeNodeDataMap::iterator it;
  it = segmentsMap.find(id);
  return it!=segmentsMap.end();
}

bool vtkHM1DStraightModel::IsTerminal(vtkIdType id)
{
  TreeNodeDataMap::iterator it;
  it = terminalsMap.find(id);
  return it!=terminalsMap.end();
}

// --------------------------------------------------------------------------------------------

//vtkIntArray *vtkHM1DStraightModel::GetTerminalCSID(vtkIdType id)
vtkIdType vtkHM1DStraightModel::GetTerminalCSID(vtkIdType id)
{
  // pesquisa se o id é de um terminal ou segmento
  if (this->IsTerminal(id))
  	return this->GetTerminal(id)->GetClientServerID_int();
//  	{
//  	// busca os ids dos segmentos que estao relacionados com o ator do terminal selecionado
//  	// ver http://hemo01a.lncc.br/cgi-bin/bugzilla/show_bug.cgi?id=37#c4
//  	vtkHM1DTerminal *ter = GetTerminal(id);
//  	vtkHM1DTreeElement *tree=ter->GetFirstParent();
//
//  	if (tree)   // se o terminal selecionado tem pai
//	  	{
//		  	int sons=tree->GetChildCount();
//			// criar um vtkIntArray que informe os CSIDs do terminal e segmentos adjcentes.
//			// primeiro elemento do array é o CSID do pai; segundo é o CSId do terminal e os demais
//			// correspondem aos segmentos. Zero marca como final do vetor
//			vtkIntArray* CSIDArray = vtkIntArray::New();
//
//			// insere o csid do segmento pai do terminal
//			CSIDArray->InsertNextValue(tree->GetClientServerID_int());
//
//			// insere o csid do terminal
////		  CSIDArray->InsertNextValue(tree->GetFirstChild()->GetClientServerID_int());
//			CSIDArray->InsertNextValue(ter->GetClientServerID_int());
//
//			//insere os csids dos segmentos "filhos" do terminal
//			int i;
//			for (i=1;i<sons;i++)
//				{
//			  CSIDArray->InsertNextValue(tree->GetNextChild()->GetClientServerID_int());
//				}
//
//		  	// zero indicates the end of the array -- we can not have CSID equals zero
//		  	CSIDArray->InsertNextValue(0);
//		  	return CSIDArray;
//	  	}
//  	else // o terminal nao tem pai - é o coracao
//	  	{
//		  	// criar um vtkIntArray que informe os CSIDs do terminal e segmentos adjcentes.
//			// primeiro elemento do array é o ZERO (indicando que o vetor é referente a informacao
//			// do coracao);
//			// segundo é o CSId do terminal (coracao) e os demais correspondem aos segmentos filhos
//			// zero marca o fim do array
//
//			vtkIntArray* CSIDArray = vtkIntArray::New();
//			// csid do terminal
//	  	int sons=ter->GetChildCount();
//
//	  	// csdi do Pai --como nao tem pai é ZERO
//	  	CSIDArray->InsertNextValue(0);
//
//	  	// insercao do csis do terminal
//	  	CSIDArray->InsertNextValue(GetTerminal(id)->GetClientServerID_int());
//
//	  	int i=0;
//			CSIDArray->InsertNextValue(ter->GetFirstChild()->GetClientServerID_int());
//			for (i=1;i<sons;i++)
//				{
//			    	CSIDArray->InsertNextValue(ter->GetNextChild()->GetClientServerID_int());
//				}
//			CSIDArray->InsertNextValue(0); // marca final do array
//			return CSIDArray;
//	  	}
//  	}
  return 0;
}

// --------------------------------------------------------------------------------------------

vtkIntArray *vtkHM1DStraightModel::GetSegmentCSID(vtkIdType id)
{
  if (this->IsSegment(id))
  	{
  	vtkIntArray* array = vtkIntArray::New();
  	array->SetNumberOfValues(3);
  	array->InsertValue(0, this->GetSegment(id)->GetClientServerID_int());
  	array->InsertValue(1, this->GetSegment(id)->GetNodeNumber());
  	array->InsertValue(2, this->GetSegment(id)->GetElementNumber());
  	return array;
  	}
  else
  	{
  	vtkErrorMacro(<< "Can not get CSID of given segment ID: " << id);
	  return NULL;
		}
}

// --------------------------------------------------------------------------------------------

int vtkHM1DStraightModel::GetNumberOfSegments()
{
	return this->segmentsMap.size();
}

// --------------------------------------------------------------------------------------------

int vtkHM1DStraightModel::GetNumberOfTerminals()
{
	return this->terminalsMap.size();
}

// --------------------------------------------------------------------------------------------

int vtkHM1DStraightModel::GetNumberOfNodes()
{
	TreeNodeDataMap::iterator it;
	int sumNodes = 0;
	for (it = this->segmentsMap.begin(); it!= this->segmentsMap.end(); it++)
	{
		vtkHM1DSegment* s = (vtkHM1DSegment*)(*it).second;
		sumNodes += s->GetElementNumber()+1;
	}
	return sumNodes;
}

// --------------------------------------------------------------------------------------------

int vtkHM1DStraightModel::CheckForNullViscoElasticElements()
{
	int i=0;
	int id=1; // tree root id
	int elem;
	while (i < (this->GetNumberOfSegments()+ this->GetNumberOfTerminals()))
	{
		if (this->IsSegment(id))
  		{
    	elem=0;
    	while (elem < this->GetSegment(id)->GetElementNumber()) // percorre cada elemento do segmento atual
    		{
    		vtkHMElementData *CurrentElement = this->GetSegment(id)->GetElementData(elem);
    		if (!CurrentElement->Viscoelasticity)
    			{
    			return 0;
    			break;
    			}
    		elem++;
    		}
    	id++;
  		}
  	else if (this->IsTerminal(id)) // terminal
  		{
  		id++;
  		}
	i++;
	}
	return 1;

}

// --------------------------------------------------------------------------------------------


void vtkHM1DStraightModel::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  const char* name = this->GetName();
  if (name)
    {
    os << indent << "Name: " << name << "\n";
    }
  else
    {
    os << indent << "Name: (none)\n";
    }
  os << indent << "MaxId: " << this->MaxId << "\n";
  os << indent << "NumberOfSegments: " << this->segmentsMap.size() << "\n";
  os << indent << "NumberOfTerminals: " << this->terminalsMap.size() << "\n";
}

TreeNodeDataMap vtkHM1DStraightModel::GetSegmentsMap()
{
	return segmentsMap;
}

TreeNodeDataMap vtkHM1DStraightModel::GetTerminalsMap()
{
	return this->terminalsMap;
}


SegmentInformationMap vtkHM1DStraightModel::GetSegmentNamesMap()
{
	return this->SegmentNamesMap;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::SetInstantOfTime(vtkDoubleArray *array)
{
	if ( array )
		this->InstantOfTime->DeepCopy(array);
}

// --------------------------------------------------------------------------------------------
vtkDoubleArray *vtkHM1DStraightModel::GetInstantOfTime()
{
	return this->InstantOfTime;
}

// --------------------------------------------------------------------------------------------


void vtkHM1DStraightModel::SetTreePressure(int pressure, double value)
{
	// pressure =0 Reference Pressure
	// pressure =1 Infiltration Pressure
	int i=0;
	int id=1; // tree root id
	int elem;
	while (i < (this->GetNumberOfSegments()+ this->GetNumberOfTerminals()))
		{
		if (this->IsSegment(id))
  		{
    	elem=0;
    	while (elem < this->GetSegment(id)->GetElementNumber()) // percorre cada elemento do segmento atual
    		{
    		vtkHMElementData *CurrentElement = this->GetSegment(id)->GetElementData(elem);
    		if (!pressure)
    			CurrentElement->ReferencePressure = value;
    		else
    			CurrentElement->InfiltrationPressure = value;

    		elem++;
    		}
    	id++;
  		}
  	else if (this->IsTerminal(id)) // terminal
  		{
  		id++;
  		}
		i++;
		}
	//return 1;

}

// --------------------------------------------------------------------------------------------

char *vtkHM1DStraightModel::GetSegmentsID()
{
  char Temp[20];
  this->IdCharList.clear();

	TreeNodeDataMap::iterator it;
	for (it = this->segmentsMap.begin(); it!= this->segmentsMap.end(); it++)
		{
		vtkHM1DSegment* s = (vtkHM1DSegment*)(*it).second;
		sprintf(Temp, "%i;", s->GetId());
		this->IdCharList.append(Temp);
		//strcat(this->IdCharList, Temp);
		for (int i = 0; i < 20; ++i)
			Temp[i] = 0;
		}

	this->IdCharList2 = new char[this->IdCharList.length()];
	strcpy(this->IdCharList2, this->IdCharList.c_str());
	return this->IdCharList2;
}

// --------------------------------------------------------------------------------------------

void vtkHM1DStraightModel::SetNonLeafTerminalsDefaultValues(double R1, double R2, double Cap, double Pressure)
{
	TreeNodeDataMap::iterator it;
	for (it = this->terminalsMap.begin(); it!= this->terminalsMap.end(); it++)
		{
		vtkHM1DTerminal* ter = (vtkHM1DTerminal*)(*it).second;

		//para nao atualizar o terminal coracao entao o terminal deve ter pais -
		// se nao tiver pais isto indicara que o terminal é o coracao

		if (ter->GetTerminalNumberOfSons() != 0 && (ter->GetNumberOfParents()))
 			ter->SetTerminalParam(R1, R2, Cap, Pressure);
		}
}
// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::SetLeafTerminalsDefaultValues(double R1, double R2, double Cap, double Pressure)
{
	TreeNodeDataMap::iterator it;
	for (it = this->terminalsMap.begin(); it!= this->terminalsMap.end(); it++)
		{
		vtkHM1DTerminal* ter = (vtkHM1DTerminal*)(*it).second;
		if (!ter->GetTerminalNumberOfSons()) // se nao tem filhos o terminal é folha
 			ter->SetTerminalParam(R1, R2, Cap, Pressure);
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::SetAgingFactor(double elastinFactor, double viscoelasticityFactor)
{
	this->AgingElastinFactor = elastinFactor;
	this->AgingViscoelasticityFactor = viscoelasticityFactor;

	TreeNodeDataMap::iterator it;
	for (it = this->segmentsMap.begin(); it!= this->segmentsMap.end(); it++)
		{
		vtkHM1DSegment *segm = vtkHM1DSegment::SafeDownCast(it->second);
		for ( int i=0; i<segm->GetElementNumber(); i++ )
			{
			segm->GetElementData(i)->Elastin *= this->AgingElastinFactor;
			segm->GetElementData(i)->Viscoelasticity *= this->AgingViscoelasticityFactor;
			}
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::GetAgingFactor(double factor[2])
{
	factor[0] = this->AgingElastinFactor;
	factor[1] = this->AgingViscoelasticityFactor;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveAging()
{
	TreeNodeDataMap::iterator it;
	for (it = this->segmentsMap.begin(); it!= this->segmentsMap.end(); it++)
		{
		vtkHM1DSegment *segm = vtkHM1DSegment::SafeDownCast(it->second);
		for ( int i=0; i<segm->GetElementNumber(); i++ )
			{
			segm->GetElementData(i)->Elastin /= this->AgingElastinFactor;
			segm->GetElementData(i)->Viscoelasticity /= this->AgingViscoelasticityFactor;
			}
		}

	this->AgingElastinFactor = 1;
	this->AgingViscoelasticityFactor = 1;
}

// --------------------------------------------------------------------------------------------
int vtkHM1DStraightModel::AddClip(char *seg1, char *seg2)
{
	vtkHM1DSegment *s1 = this->GetSegment(seg1);
	vtkHM1DSegment *s2 = this->GetSegment(seg2);

	vtkIdType find = this->FindClipIds(s1->GetId());
	vtkIdType find2 = this->FindClipIds(s2->GetId());

	if ( (find == -1) && (find2 == -1) )
		if ( s1 && s2 )
			{
			this->ClipSegments->InsertNextTuple2(s1->GetId(), s2->GetId());
			return 1;
			}

	return 0;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddClip(vtkIdType seg1, vtkIdType seg2)
{
	vtkIdType find = this->FindClipIds(seg1);
	vtkIdType find2 = this->FindClipIds(seg2);

	if ( (find == -1) && (find2 == -1) )
		this->ClipSegments->InsertNextTuple2(seg1, seg2);
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveClip(vtkIdType idClip)
{
	this->ClipSegments->RemoveTuple(idClip);
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::GetClip(vtkIdType idClip, vtkIdType idSegments[2])
{
	double *ids;

	ids = this->ClipSegments->GetTuple2(idClip);

	idSegments[0] = (vtkIdType)ids[0];
	idSegments[1] = (vtkIdType)ids[1];
}

//------------------------------------------------------------------------
//Procura se o segment está relacionado com algum
//clip e retorna a posição no array, caso não encontre retorna -1
vtkIdType vtkHM1DStraightModel::FindClipIds(vtkIdType idSegment)
{
	double *tuple;
	for ( int i=0; i<this->ClipSegments->GetNumberOfTuples(); i++ )
		{
		tuple = this->ClipSegments->GetTuple(i);
		if ( (tuple[0] == idSegment) || (tuple[1] == idSegment) )
			return i;
		}
	return -1;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddStent(vtkIdType idSegment, vtkIdType idStent, vtkIdList *stentList)
{
	InterventionsMap::iterator it = this->StentMap.find(idStent);

	if ( it != this->StentMap.end() )
		{
		it->second->DeepCopy(stentList);
		}
	else
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(stentList);
		this->StentMap.insert(InterventionsPair(idStent, l));
		this->StentMultimap.insert(InterventionsInformationPair(idSegment, idStent));
		}

}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveStent(vtkIdType idSegment, vtkIdType idStent)
{
	InterventionsMap::iterator it = this->StentMap.find(idStent);
	InterventionsInformationMultimap::iterator itMultimap1 = this->StentMultimap.lower_bound(idSegment);
	InterventionsInformationMultimap::iterator itMultimap2 = this->StentMultimap.upper_bound(idSegment);

	if ( it != this->StentMap.end() )
		{
		InterventionsMap map;
		InterventionsMap::iterator it2 = this->StentMap.begin();

		vtkIdType i=0;
		//Deleta a lista de id de elementos e inseri o restante em um outro map
		//com id do stent atualizado.
		while ( it2 != this->StentMap.end() )
			{
			if ( idStent != it2->first )
				{
				map.insert(InterventionsPair(i, it2->second));
				i++;
				}
			else
				{
				it2->second->Delete();
				}

			it2++;
			}

		this->StentMap.clear();

		it2 = map.begin();
		//copia de um map para o outro
		while ( it2 != map.end() )
			{
			this->StentMap.insert(InterventionsPair(it2->first, it2->second));

			it2++;
			}

		//apaga associação do stent com o segmento
		while ( (itMultimap1 != itMultimap2) || (itMultimap1 != this->StentMultimap.end()) )
			{
			if ( itMultimap1->second == idStent )
				{
				this->StentMultimap.erase(itMultimap1);
				break;
				}
			itMultimap1++;
			}

		//atualiza o map de associação de stent e segmento com os novos ids do stent.
		itMultimap1 = this->StentMultimap.begin();
		while ( itMultimap1 != this->StentMultimap.end() )
			{
			if ( itMultimap1->second > idStent )
				itMultimap1->second--;
			itMultimap1++;
			}
		}

}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveAllStents()
{
	InterventionsMap::iterator itIntervention = this->StentMap.begin();
	while ( itIntervention != this->StentMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	this->StentMultimap.clear();
}

// --------------------------------------------------------------------------------------------
InterventionsInformationMultimap vtkHM1DStraightModel::GetStentsInformation()
{
	return this->StentMultimap;
}

// --------------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStraightModel::GetStent(vtkIdType idStent)
{
	InterventionsMap::iterator it = this->StentMap.find(idStent);

	if ( it != this->StentMap.end() )
		return it->second;

	return NULL;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddStenosis(vtkIdType idSegment, vtkIdType idStenosis, vtkIdList *stenosisList)
{
	InterventionsMap::iterator it = this->StenosisMap.find(idStenosis);

	if ( it != this->StenosisMap.end() )
		{
		it->second->DeepCopy(stenosisList);
		}
	else
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(stenosisList);
		this->StenosisMap.insert(InterventionsPair(idStenosis, l));
		this->StenosisMultimap.insert(InterventionsInformationPair(idSegment, idStenosis));
		}

}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveStenosis(vtkIdType idSegment, vtkIdType idStenosis)
{
	InterventionsMap::iterator it = this->StenosisMap.find(idStenosis);
	InterventionsInformationMultimap::iterator itMultimap1 = this->StenosisMultimap.lower_bound(idSegment);
	InterventionsInformationMultimap::iterator itMultimap2 = this->StenosisMultimap.upper_bound(idSegment);

	if ( it != this->StenosisMap.end() )
		{
		InterventionsMap map;
		InterventionsMap::iterator it2 = this->StenosisMap.begin();

		vtkIdType i=0;
		//Deleta a lista de id de elementos e inseri o restante em um outro map
		//com id do stenosis atualizado.
		while ( it2 != this->StenosisMap.end() )
			{
			if ( idStenosis != it2->first )
				{
				map.insert(InterventionsPair(i, it2->second));
				i++;
				}
			else
				{
				it2->second->Delete();
				}

			it2++;
			}

		this->StenosisMap.clear();

		it2 = map.begin();
		//copia de um map para o outro
		while ( it2 != map.end() )
			{
			this->StenosisMap.insert(InterventionsPair(it2->first, it2->second));

			it2++;
			}

		//apaga associação do stenosis com o segmento
		while ( (itMultimap1 != itMultimap2) || (itMultimap1 != this->StenosisMultimap.end()) )
			{
			if ( itMultimap1->second == idStenosis )
				{
				this->StenosisMultimap.erase(itMultimap1);
				break;
				}
			itMultimap1++;
			}

		//atualiza o map de associação de stenosis e segmento com os novos ids do stenosis.
		itMultimap1 = this->StenosisMultimap.begin();
		while ( itMultimap1 != this->StenosisMultimap.end() )
			{
			if ( itMultimap1->second > idStenosis )
				itMultimap1->second--;
			itMultimap1++;
			}
		}
}

// --------------------------------------------------------------------------------------------
InterventionsInformationMultimap vtkHM1DStraightModel::GetStenosisInformation()
{
	return this->StenosisMultimap;
}

// --------------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStraightModel::GetStenosis(vtkIdType idStenosis)
{
	InterventionsMap::iterator it = this->StenosisMap.find(idStenosis);

	if ( it != this->StenosisMap.end() )
		return it->second;

	return NULL;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddAneurysm(vtkIdType idSegment, vtkIdType idAneurysm, vtkIdList *aneurysmList)
{
	InterventionsMap::iterator it = this->AneurysmMap.find(idAneurysm);

	if ( it != this->AneurysmMap.end() )
		{
		it->second->DeepCopy(aneurysmList);
		}
	else
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(aneurysmList);
		this->AneurysmMap.insert(InterventionsPair(idAneurysm, l));
		this->AneurysmMultimap.insert(InterventionsInformationPair(idSegment, idAneurysm));
		}

}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveAneurysm(vtkIdType idSegment, vtkIdType idAneurysm)
{
	InterventionsMap::iterator it = this->AneurysmMap.find(idAneurysm);
	InterventionsInformationMultimap::iterator itMultimap1 = this->AneurysmMultimap.lower_bound(idSegment);
	InterventionsInformationMultimap::iterator itMultimap2 = this->AneurysmMultimap.upper_bound(idSegment);

	if ( it != this->AneurysmMap.end() )
		{
		InterventionsMap map;
		InterventionsMap::iterator it2 = this->AneurysmMap.begin();

		vtkIdType i=0;
		//Deleta a lista de id de elementos e inseri o restante em um outro map
		//com id do aneurisma atualizado.
		while ( it2 != this->AneurysmMap.end() )
			{
			if ( idAneurysm != it2->first )
				{
				map.insert(InterventionsPair(i, it2->second));
				i++;
				}
			else
				{
				it2->second->Delete();
				}

			it2++;
			}

		this->AneurysmMap.clear();

		it2 = map.begin();
		//copia de um map para o outro
		while ( it2 != map.end() )
			{
			this->AneurysmMap.insert(InterventionsPair(it2->first, it2->second));

			it2++;
			}

		//apaga associação do aneurysm com o segmento
		while ( (itMultimap1 != itMultimap2) || (itMultimap1 != this->AneurysmMultimap.end()) )
			{
			if ( itMultimap1->second == idAneurysm )
				{
				this->AneurysmMultimap.erase(itMultimap1);
				break;
				}
			itMultimap1++;
			}

		//atualiza o map de associação de aneurysm e segmento com os novos ids do aneurysm.
		itMultimap1 = this->AneurysmMultimap.begin();
		while ( itMultimap1 != this->AneurysmMultimap.end() )
			{
			if ( itMultimap1->second > idAneurysm )
				itMultimap1->second--;
			itMultimap1++;
			}

		}
}

// --------------------------------------------------------------------------------------------
InterventionsInformationMultimap vtkHM1DStraightModel::GetAneurysmInformation()
{
	return this->AneurysmMultimap;
}

// --------------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStraightModel::GetAneurysm(vtkIdType idAneurysm)
{
	InterventionsMap::iterator it = this->AneurysmMap.find(idAneurysm);

	if ( it != this->AneurysmMap.end() )
		return it->second;

	return NULL;
}

// --------------------------------------------------------------------------------------------
double vtkHM1DStraightModel::CalculateTreeVolume()
{
	double volume = 0;

	TreeNodeDataMap::iterator it = this->segmentsMap.begin();

	while ( it != this->segmentsMap.end() )
		{
		volume += vtkHM1DSegment::SafeDownCast(it->second)->GetVolume();
		it++;
		}

	return volume;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::MoveChildren(vtkIdType rootId, double coord[3])
{
	if ( this->ElementVisited->IsId(rootId) != -1 )
		return;

	vtkHM1DTreeElement *root;
	vtkHM1DTreeElement *child;

	if ( this->IsTerminal(rootId))
		{
		root = this->GetTerminal(rootId);
		}
	else
		{
		vtkHM1DSegment *seg = this->GetSegment(rootId);
		root = seg;

		int n = seg->GetElementNumber()+1;

		for ( int i=0; i<n; i++ )
			{
			seg->GetNodeData(i)->coords[0] += coord[0];
			seg->GetNodeData(i)->coords[1] += coord[1];
			seg->GetNodeData(i)->coords[2] += coord[2];
			}
		}

	this->ElementVisited->InsertUniqueId(rootId);

	child = root->GetFirstChild();

	while ( child )
		{
		this->MoveChildren(child->GetId(), coord);
		child = root->GetNextChild();
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::ResetElementVisitedList()
{
	this->ElementVisited->Reset();
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddNullElements1D3D(vtkIdType idElement, vtkIdList *elementList)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		{
		if ( elementList && it->second )
			{
			for ( int i=0; i<elementList->GetNumberOfIds(); i++ )
				it->second->InsertUniqueId(elementList->GetId(i));
			}
		else if ( elementList )
			{
			vtkIdList *l = vtkIdList::New();
			l->DeepCopy(elementList);
			this->NullElements1D3DMap.insert(NullElementsPair(idElement, l));
			}

		}
	else
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(elementList);
		this->NullElements1D3DMap.insert(NullElementsPair(idElement, l));
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::AddNullElements1D3D(vtkIdType idElement, vtkIdType element)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		{
		if ( it->second )
			{
			it->second->InsertUniqueId(element);
			}
		else
			{
			vtkIdList *l = vtkIdList::New();
			l->InsertUniqueId(element);
			this->NullElements1D3DMap.insert(NullElementsPair(idElement, l));
			}
		}
	else
		{
		vtkIdList *l = vtkIdList::New();
		l->InsertUniqueId(element);
		this->NullElements1D3DMap.insert(NullElementsPair(idElement, l));
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveNullElements1D3D(vtkIdType idElement)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		{
		if ( it->second )
			it->second->Delete();
		this->NullElements1D3DMap.erase(idElement);
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RemoveNullElements1D3D(vtkIdType idElement, vtkIdList *elementList)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		{
		if ( elementList && it->second )
			{
			for ( int i=0; i<elementList->GetNumberOfIds(); i++ )
				it->second->DeleteId(elementList->GetId(i));

			if ( it->second->GetNumberOfIds() == 0 )
				{
				it->second->Delete();
				it->second = NULL;
				this->NullElements1D3DMap.erase(idElement);
				}
			}
		else if ( !it->second )
			this->NullElements1D3DMap.erase(idElement);
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::ClearNullElements1D3DMap()
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.begin();

	while ( it != this->NullElements1D3DMap.end() )
		{
		if ( it->second )
			it->second->Delete();
		it->second = NULL;
		this->NullElements1D3DMap.erase(it->first);

		it++;
		}

	this->NullElements1D3DMap.clear();
}

// --------------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStraightModel::GetNullElements1D3DList(vtkIdType idElement)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		return it->second;

	return NULL;
}

// --------------------------------------------------------------------------------------------
NullElementsMap vtkHM1DStraightModel::GetNullElements1D3DMap()
{
	return this->NullElements1D3DMap;
}

// --------------------------------------------------------------------------------------------
bool vtkHM1DStraightModel::IsNullElements1D3D(vtkIdType idElement)
{
	NullElementsMap::iterator it = this->NullElements1D3DMap.find(idElement);

	if ( it != this->NullElements1D3DMap.end() )
		return true;

	return false;
}

// --------------------------------------------------------------------------------------------
bool vtkHM1DStraightModel::HasNullElements1D3D()
{
	if ( this->NullElements1D3DMap.size() > 0 )
		return true;
	else
		return false;
}

// --------------------------------------------------------------------------------------------
vtkIdType vtkHM1DStraightModel::GetSmallerSegment()
{
	vtkIdType idSegment = 0;
	double length;

	TreeNodeDataMap::iterator it = this->segmentsMap.begin();
	//pega o comprimento do primeiro segmento.
	if ( this->segmentsMap.size() > 0 )
		{
		idSegment = it->first;
		length = vtkHM1DSegment::SafeDownCast(it->second)->GetLength();
		it++;
		}

	while ( it != this->segmentsMap.end() )
		{
		if ( vtkHM1DSegment::SafeDownCast(it->second)->GetLength() < length )
			{
			idSegment = it->first;
			length = vtkHM1DSegment::SafeDownCast(it->second)->GetLength();
			}
		it++;
		}

	return idSegment;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::ConfigureDefaultLengthOfElements(double elementLength)
{
	/*
	 * Metodo para configurar todos os elementos de todos os segmentos
	 * baseados em um tamanho fornecido pelo usuário ou baseado no
	 * tamanho do elemento do menor segmento da árvore.
	 * Para isso, pega-se o tamanho do segmento e divide pelo tamanho
	 * do elemento "parametro (elementLength)", encontrando o número
	 * de elementos que o segmento terá.
	 */

	TreeNodeDataMap::iterator it = this->segmentsMap.begin();
	double n, length;
	int numberOfElements, numberOfElementsCeil, numberOfElementsFloor;
	vtkHM1DSegment *segm;
	double diff;

	double x[3], df[3];
	double pt1[3], pt2[3], p1[3], p2[3];

	double nodeArray[7]; //propriedades proximais e distais dos nodes.

	elementLength = fabs(elementLength);

	while ( it != this->segmentsMap.end() )
		{
		///////////////////////////////////////////////////////////////////////
		//Calcular o novo numero de elementos

		segm = vtkHM1DSegment::SafeDownCast(it->second);
		length = segm->GetLength();
		n = length / elementLength;
		//arredonda o numero de elementos para cima
		numberOfElementsCeil = (int)ceil(n);
		//arredonda o numero de elementos para baixo
		numberOfElementsFloor = (int)floor(n);
		//calcula a diferenca entre o numero de elementos e o arredondado para cima
		diff = numberOfElementsCeil - n;
		//se a diferenca for maior que 0.5, devo atribuir o numero arredondado para baixo
		if ( diff > 0.5 )
			{
			numberOfElements = numberOfElementsFloor;
			if ( numberOfElements <= 0 )
				numberOfElements = 1;
			}
		//se a diferenca for menor que 0.5, devo atribuir o numero arredondado para cima
		else
			{
			numberOfElements = numberOfElementsCeil;
			}

		//pegar as propriedades proximais e distais antes que seja removido ou adicionado
		//novos nodes.
		nodeArray[0] = segm->GetNodeData(0)->radius; //raio proximal
		nodeArray[1] = segm->GetNodeData(segm->GetElementNumber())->radius; //raio distal
		nodeArray[2] = segm->GetinterpolationType();
		nodeArray[3] = segm->GetNodeData(0)->alfa / (segm->GetNodeData(0)->radius*vtkMath::Pi()); //wallThickness proximal
		nodeArray[4] = segm->GetNodeData(segm->GetElementNumber())->alfa / (segm->GetNodeData(segm->GetElementNumber())->radius*vtkMath::Pi()); //wallThickness distal

		///////////////////////////////////////////////////////////////////////
		//Calcular as novas coordenadas dos nodes do segmento.

		//pegar o primeiro ponto do segmento
		p1[0] = segm->GetNodeData(0)->coords[0];
		p1[1] = segm->GetNodeData(0)->coords[1];
		p1[2] = segm->GetNodeData(0)->coords[2];

		//pegar o ultimo ponto do segmento
		p2[0] = segm->GetNodeData(segm->GetElementNumber())->coords[0];
		p2[1] = segm->GetNodeData(segm->GetElementNumber())->coords[1];
		p2[2] = segm->GetNodeData(segm->GetElementNumber())->coords[2];

		//calcula a diferenca entre o primeiro e o ultimo ponto
		df[0] = p2[0] - p1[0];
		df[1] = p2[1] - p1[1];
		df[2] = p2[2] - p1[2];

		//calcula a distancia entre cada nó do segmento
		x[0] = df[0] / numberOfElements;
		x[1] = df[1] / numberOfElements;
		x[2] = df[2] / numberOfElements;

		pt1[0] = p1[0];
		pt1[1] = p1[1];
		pt1[2] = p1[2];

	  // valores dos parametros proximais
		double proxElem[10];
	  proxElem[0] = segm->GetElementData(0)->Elastin;
		proxElem[1] = segm->GetElementData(0)->Permeability;
	  proxElem[2] = segm->GetElementData(0)->InfiltrationPressure;
		proxElem[3] = segm->GetElementData(0)->ReferencePressure;
		proxElem[4] = segm->GetElementData(0)->Collagen;
		proxElem[5] = segm->GetElementData(0)->CoefficientA;
		proxElem[6] = segm->GetElementData(0)->CoefficientB;
		proxElem[7] = segm->GetElementData(0)->Viscoelasticity;
		proxElem[8] = segm->GetElementData(0)->ViscoelasticityExponent;
		proxElem[9] = segm->GetElementData(0)->WallThickness;

		segm->SetProxElem(proxElem);

		// valores dos parametros distais
		double distalElem[10];
		distalElem[0] = segm->GetElementData(segm->GetElementNumber()-1)->Elastin;
		distalElem[1] = segm->GetElementData(segm->GetElementNumber()-1)->Permeability;
	  distalElem[2] = segm->GetElementData(segm->GetElementNumber()-1)->InfiltrationPressure;
		distalElem[3] = segm->GetElementData(segm->GetElementNumber()-1)->ReferencePressure;
		distalElem[4] = segm->GetElementData(segm->GetElementNumber()-1)->Collagen;
		distalElem[5] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientA;
		distalElem[6] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientB;
		distalElem[7] = segm->GetElementData(segm->GetElementNumber()-1)->Viscoelasticity;
		distalElem[8] = segm->GetElementData(segm->GetElementNumber()-1)->ViscoelasticityExponent;
		distalElem[9] = segm->GetElementData(segm->GetElementNumber()-1)->WallThickness;

		segm->SetDistalElem(distalElem);

		if ( segm->GetElementNumber() > numberOfElements )
			{
			//remover os nodes e elementos

			segm->RemoveNodeData(numberOfElements+1, segm->GetNodeNumber());
			segm->RemoveElementData(numberOfElements, segm->GetElementNumber());
			segm->SetNumberOfElements(numberOfElements);

			//setar as coordenadas dos nodes
			for ( int i=0; i<numberOfElements+1; i++ )
				{
				//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
				pt2[0] = pt1[0] + x[0];
				pt2[1] = pt1[1] + x[1];
				pt2[2] = pt1[2] + x[2];

		  	//Adicionar novo node
				segm->GetNodeData(i)->coords[0] = pt1[0];
				segm->GetNodeData(i)->coords[1] = pt1[1];
				segm->GetNodeData(i)->coords[2] = pt1[2];

				pt1[0] = pt2[0];
				pt1[1] = pt2[1];
				pt1[2] = pt2[2];
				}

			//calcula as novas propriedades dos elementos considerando o tipo de interpolação
			segm->SetElementsFromSegment(segm->GetinterpolationType());
			//calcula as novas propriedades dos nodes considerando o tipo de interpolação
			nodeArray[5] = segm->GetLength();
			nodeArray[6] = nodeArray[5]/numberOfElements; //dx

			segm->SetNodesProps(nodeArray);

			}  //fim if ( segm->GetElementNumber() > numberOfElements )
		else if ( segm->GetElementNumber() < numberOfElements )
			{
			int nElements = segm->GetElementNumber();

			//setar as coordenadas dos nodes existentes no segmento
			for ( int j=0; j<nElements; j++ )
				{
				//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
				pt2[0] = pt1[0] + x[0];
				pt2[1] = pt1[1] + x[1];
				pt2[2] = pt1[2] + x[2];

		  	//Adicionar novo node
				segm->GetNodeData(j)->coords[0] = pt1[0];
				segm->GetNodeData(j)->coords[1] = pt1[1];
				segm->GetNodeData(j)->coords[2] = pt1[2];

				pt1[0] = pt2[0];
				pt1[1] = pt2[1];
				pt1[2] = pt2[2];
				}

			//setar novo numero de elementos
			segm->SetNumberOfElements(numberOfElements);
			int i=nElements;
			//Adicionar novos elementos e nodes
			for ( i=nElements; i<numberOfElements; i++ )
				{
				//criar novo elemento
				vtkHMElementData *elem = new vtkHMElementData();

				elem->idNode[0] = i;
				elem->idNode[1] = i+1;

				segm->SetElementData(i, *elem);

				//criar novo node
				vtkHMNodeData *node = new vtkHMNodeData();

				node->coords[0] = pt1[0];
				node->coords[1] = pt1[1];
				node->coords[2] = pt1[2];

				segm->SetNodeData(i, *node);

				//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
				pt2[0] = pt1[0] + x[0];
				pt2[1] = pt1[1] + x[1];
				pt2[2] = pt1[2] + x[2];

				pt1[0] = pt2[0];
				pt1[1] = pt2[1];
				pt1[2] = pt2[2];

		    delete node;
		    delete elem;
				}
			//configurar ultimo node
			vtkHMNodeData *node = new vtkHMNodeData();

			node->coords[0] = pt2[0];
			node->coords[1] = pt2[1];
			node->coords[2] = pt2[2];

			segm->SetNodeData(i, *node);

      delete node;

			segm->SetNumberOfElements(numberOfElements);

			//calcula as novas propriedades dos elementos considerando o tipo de interpolação
			segm->SetElementsFromSegment(segm->GetinterpolationType());
			//calcula as novas propriedades dos nodes considerando o tipo de interpolação
			nodeArray[5] = segm->GetLength();
			nodeArray[6] = nodeArray[5]/numberOfElements; //dx

			segm->SetNodesProps(nodeArray);

			}  //fim else if ( segm->GetElementNumber() < numberOfElements )

		it++;
		}  //fim while ( it != this->segmentsMap.end() )
}

// --------------------------------------------------------------------------------------------
vtkIdList* vtkHM1DStraightModel::CopySegments(vtkIdList *list)
{
	vtkIdList *newSegmentsList = NULL;
	if ( list )
		{
		if ( list->GetNumberOfIds() > 0 )
			{
			newSegmentsList = vtkIdList::New();

			vtkIdType idTerminal, idSegment;
			vtkHM1DSegment *segm;
			vtkHM1DTerminal *term;
			for ( int i=0; i<list->GetNumberOfIds(); i++ )
				{
				idTerminal = this->AddTerminal(0);
				term = this->GetTerminal(idTerminal);

				idSegment = this->AddSegment(idTerminal);
				newSegmentsList->InsertUniqueId(idSegment);
				segm = this->GetSegment(idSegment);
				char n[256];
				sprintf(n, "%s", segm->GetSegmentName());
				segm->DeepCopy(this->GetSegment(list->GetId(i)));
				segm->SetSegmentName(n);
				}
			}
		}

	return newSegmentsList;
}

// --------------------------------------------------------------------------------------------
char* vtkHM1DStraightModel::GetSegmentName(vtkIdType *id)
{
	char* segmentName;
	vtkIdType* ID = id;
  segmentName = this->GetSegment(*ID)->GetSegmentName();
  return segmentName;
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::FlipSegments(vtkIdList *list)
{
	if ( list )
		{
		if ( list->GetNumberOfIds() > 0 )
			{
			vtkHM1DSegment *segm;
			double max = -10000000;
			double min = 10000000;
			double x;
			//achar o x maximo e minimo
			for ( int i=0; i<list->GetNumberOfIds(); i++ )
				{
				segm = this->GetSegment(list->GetId(i));

				for ( int j=0; j<segm->GetNodeNumber(); j++ )
					{
					if ( segm->GetNodeData(j)->coords[0] < min )
						min = segm->GetNodeData(j)->coords[0];
					if ( segm->GetNodeData(j)->coords[0] > max )
						max = segm->GetNodeData(j)->coords[0];
					}
				}
			x = (min+max)/2;
			//inverter o eixo x
			for ( int i=0; i<list->GetNumberOfIds(); i++ )
				{
				segm = this->GetSegment(list->GetId(i));
				for ( int j=0; j<segm->GetNodeNumber(); j++ )
					{
					segm->GetNodeData(j)->coords[0] *= -1;
					segm->GetNodeData(j)->coords[0] += (2*x);
					}
				}
			}
		}
}

// --------------------------------------------------------------------------------------------
void vtkHM1DStraightModel::RefineSegment(vtkIdList *list, int method, int factor)
{
  vtkHM1DSegment *segm;
  int numElements;
  double length, elementLength;

  if ( factor > 0 )
    {
    // se metodo igual a "Refine", novos nodes serão adicionados, multiplicando
    // "factor" pelo numero atual de elementos, diminuindo assim o tamanho dos elementos.
    if ( method == 1 )
      {
      for ( int i=0; i<list->GetNumberOfIds(); i++ )
        {
        segm = this->GetSegment(list->GetId(i));

        numElements = segm->GetElementNumber();
        length = segm->GetLength();

        numElements = factor * numElements;

        double diff;

        double x[3], df[3];
        double pt1[3], pt2[3], p1[3], p2[3];

        double nodeArray[7]; //propriedades proximais e distais dos nodes.

        //pegar as propriedades proximais e distais antes que seja adicionado
        //novos nodes.
        nodeArray[0] = segm->GetNodeData(0)->radius; //raio proximal
        nodeArray[1] = segm->GetNodeData(segm->GetElementNumber())->radius; //raio distal
        nodeArray[2] = segm->GetinterpolationType();
        nodeArray[3] = segm->GetNodeData(0)->alfa / (segm->GetNodeData(0)->radius*vtkMath::Pi()); //wallThickness proximal
        nodeArray[4] = segm->GetNodeData(segm->GetElementNumber())->alfa / (segm->GetNodeData(segm->GetElementNumber())->radius*vtkMath::Pi()); //wallThickness distal

        ///////////////////////////////////////////////////////////////////////
        //Calcular as novas coordenadas dos nodes do segmento.

        //pegar o primeiro ponto do segmento
        p1[0] = segm->GetNodeData(0)->coords[0];
        p1[1] = segm->GetNodeData(0)->coords[1];
        p1[2] = segm->GetNodeData(0)->coords[2];

        //pegar o ultimo ponto do segmento
        p2[0] = segm->GetNodeData(segm->GetElementNumber())->coords[0];
        p2[1] = segm->GetNodeData(segm->GetElementNumber())->coords[1];
        p2[2] = segm->GetNodeData(segm->GetElementNumber())->coords[2];

        //calcula a diferenca entre o primeiro e o ultimo ponto
        df[0] = p2[0] - p1[0];
        df[1] = p2[1] - p1[1];
        df[2] = p2[2] - p1[2];

        //calcula a distancia entre cada nó do segmento
        x[0] = df[0] / numElements;
        x[1] = df[1] / numElements;
        x[2] = df[2] / numElements;

        pt1[0] = p1[0];
        pt1[1] = p1[1];
        pt1[2] = p1[2];

        // valores dos parametros proximais
        double proxElem[10];
        proxElem[0] = segm->GetElementData(0)->Elastin;
        proxElem[1] = segm->GetElementData(0)->Permeability;
        proxElem[2] = segm->GetElementData(0)->InfiltrationPressure;
        proxElem[3] = segm->GetElementData(0)->ReferencePressure;
        proxElem[4] = segm->GetElementData(0)->Collagen;
        proxElem[5] = segm->GetElementData(0)->CoefficientA;
        proxElem[6] = segm->GetElementData(0)->CoefficientB;
        proxElem[7] = segm->GetElementData(0)->Viscoelasticity;
        proxElem[8] = segm->GetElementData(0)->ViscoelasticityExponent;
        proxElem[9] = segm->GetElementData(0)->WallThickness;

        segm->SetProxElem(proxElem);

        // valores dos parametros distais
        double distalElem[10];
        distalElem[0] = segm->GetElementData(segm->GetElementNumber()-1)->Elastin;
        distalElem[1] = segm->GetElementData(segm->GetElementNumber()-1)->Permeability;
        distalElem[2] = segm->GetElementData(segm->GetElementNumber()-1)->InfiltrationPressure;
        distalElem[3] = segm->GetElementData(segm->GetElementNumber()-1)->ReferencePressure;
        distalElem[4] = segm->GetElementData(segm->GetElementNumber()-1)->Collagen;
        distalElem[5] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientA;
        distalElem[6] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientB;
        distalElem[7] = segm->GetElementData(segm->GetElementNumber()-1)->Viscoelasticity;
        distalElem[8] = segm->GetElementData(segm->GetElementNumber()-1)->ViscoelasticityExponent;
        distalElem[9] = segm->GetElementData(segm->GetElementNumber()-1)->WallThickness;

        segm->SetDistalElem(distalElem);

        int nElements = segm->GetElementNumber();

        //setar as coordenadas dos nodes existentes no segmento
        for ( int j=0; j<nElements; j++ )
          {
          //Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
          pt2[0] = pt1[0] + x[0];
          pt2[1] = pt1[1] + x[1];
          pt2[2] = pt1[2] + x[2];

          segm->GetNodeData(j)->coords[0] = pt1[0];
          segm->GetNodeData(j)->coords[1] = pt1[1];
          segm->GetNodeData(j)->coords[2] = pt1[2];

          pt1[0] = pt2[0];
          pt1[1] = pt2[1];
          pt1[2] = pt2[2];
          }

        //setar novo numero de elementos
        segm->SetNumberOfElements(numElements);
        int j=nElements;
        //Adicionar novos elementos e nodes
        for ( j=nElements; j<numElements; j++ )
          {
          //criar novo elemento
          vtkHMElementData *elem = new vtkHMElementData();

          elem->idNode[0] = j;
          elem->idNode[1] = j+1;

          segm->SetElementData(j, *elem);

          //criar novo node
          vtkHMNodeData *node = new vtkHMNodeData();

          node->coords[0] = pt1[0];
          node->coords[1] = pt1[1];
          node->coords[2] = pt1[2];

          segm->SetNodeData(j, *node);

          //Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
          pt2[0] = pt1[0] + x[0];
          pt2[1] = pt1[1] + x[1];
          pt2[2] = pt1[2] + x[2];

          pt1[0] = pt2[0];
          pt1[1] = pt2[1];
          pt1[2] = pt2[2];

          delete node;
          delete elem;
          }
        //configurar ultimo node
        vtkHMNodeData *node = new vtkHMNodeData();

        node->coords[0] = pt2[0];
        node->coords[1] = pt2[1];
        node->coords[2] = pt2[2];

        segm->SetNodeData(j, *node);

        delete node;

        segm->SetNumberOfElements(numElements);

        //calcula as novas propriedades dos elementos considerando o tipo de interpolação
        segm->SetElementsFromSegment(segm->GetinterpolationType());
        //calcula as novas propriedades dos nodes considerando o tipo de interpolação
        nodeArray[5] = segm->GetLength();
        nodeArray[6] = nodeArray[5]/numElements; //dx

        segm->SetNodesProps(nodeArray);

        }  //fim  for ( int i=0; i<list->GetNumberOfIds(); i++ )
      }  //fim  if ( method == 1 )

    // se metodo igual a "De-Refine", divido o numero atual de elementos por "factor",
    // aumentando assim o tamanho dos elementos.
    else if ( method == 2 )
      {
      for ( int i=0; i<list->GetNumberOfIds(); i++ )
        {
        segm = this->GetSegment(list->GetId(i));

        numElements = segm->GetElementNumber();
        if ( numElements < 4 )
          {
          vtkWarningMacro(<< "Number of elements is less than 4, no operation performed in the segment: " << segm->GetId());
          continue;
          }

        length = segm->GetLength();

        numElements = numElements / factor;
        if ( numElements < 4 )
          {
          vtkWarningMacro(<< "New number of elements is less than 4, the number of elements is set to 4: " << segm->GetId());
          numElements = 4;
          }

        double diff;

        double x[3], df[3];
        double pt1[3], pt2[3], p1[3], p2[3];

        double nodeArray[7]; //propriedades proximais e distais dos nodes.

        //pegar as propriedades proximais e distais antes que seja adicionado
        //novos nodes.
        nodeArray[0] = segm->GetNodeData(0)->radius; //raio proximal
        nodeArray[1] = segm->GetNodeData(segm->GetElementNumber())->radius; //raio distal
        nodeArray[2] = segm->GetinterpolationType();
        nodeArray[3] = segm->GetNodeData(0)->alfa / (segm->GetNodeData(0)->radius*vtkMath::Pi()); //wallThickness proximal
        nodeArray[4] = segm->GetNodeData(segm->GetElementNumber())->alfa / (segm->GetNodeData(segm->GetElementNumber())->radius*vtkMath::Pi()); //wallThickness distal

        ///////////////////////////////////////////////////////////////////////
        //Calcular as novas coordenadas dos nodes do segmento.

        //pegar o primeiro ponto do segmento
        p1[0] = segm->GetNodeData(0)->coords[0];
        p1[1] = segm->GetNodeData(0)->coords[1];
        p1[2] = segm->GetNodeData(0)->coords[2];

        //pegar o ultimo ponto do segmento
        p2[0] = segm->GetNodeData(segm->GetElementNumber())->coords[0];
        p2[1] = segm->GetNodeData(segm->GetElementNumber())->coords[1];
        p2[2] = segm->GetNodeData(segm->GetElementNumber())->coords[2];

        //calcula a diferenca entre o primeiro e o ultimo ponto
        df[0] = p2[0] - p1[0];
        df[1] = p2[1] - p1[1];
        df[2] = p2[2] - p1[2];

        //calcula a distancia entre cada nó do segmento
        x[0] = df[0] / numElements;
        x[1] = df[1] / numElements;
        x[2] = df[2] / numElements;

        pt1[0] = p1[0];
        pt1[1] = p1[1];
        pt1[2] = p1[2];

        // valores dos parametros proximais
        double proxElem[10];
        proxElem[0] = segm->GetElementData(0)->Elastin;
        proxElem[1] = segm->GetElementData(0)->Permeability;
        proxElem[2] = segm->GetElementData(0)->InfiltrationPressure;
        proxElem[3] = segm->GetElementData(0)->ReferencePressure;
        proxElem[4] = segm->GetElementData(0)->Collagen;
        proxElem[5] = segm->GetElementData(0)->CoefficientA;
        proxElem[6] = segm->GetElementData(0)->CoefficientB;
        proxElem[7] = segm->GetElementData(0)->Viscoelasticity;
        proxElem[8] = segm->GetElementData(0)->ViscoelasticityExponent;
        proxElem[9] = segm->GetElementData(0)->WallThickness;

        segm->SetProxElem(proxElem);

        // valores dos parametros distais
        double distalElem[10];
        distalElem[0] = segm->GetElementData(segm->GetElementNumber()-1)->Elastin;
        distalElem[1] = segm->GetElementData(segm->GetElementNumber()-1)->Permeability;
        distalElem[2] = segm->GetElementData(segm->GetElementNumber()-1)->InfiltrationPressure;
        distalElem[3] = segm->GetElementData(segm->GetElementNumber()-1)->ReferencePressure;
        distalElem[4] = segm->GetElementData(segm->GetElementNumber()-1)->Collagen;
        distalElem[5] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientA;
        distalElem[6] = segm->GetElementData(segm->GetElementNumber()-1)->CoefficientB;
        distalElem[7] = segm->GetElementData(segm->GetElementNumber()-1)->Viscoelasticity;
        distalElem[8] = segm->GetElementData(segm->GetElementNumber()-1)->ViscoelasticityExponent;
        distalElem[9] = segm->GetElementData(segm->GetElementNumber()-1)->WallThickness;

        segm->SetDistalElem(distalElem);

        //remover os nodes e elementos

        segm->RemoveNodeData(numElements+1, segm->GetNodeNumber());
        segm->RemoveElementData(numElements, segm->GetElementNumber());
        segm->SetNumberOfElements(numElements);

        //setar as coordenadas dos nodes
        for ( int i=0; i<numElements+1; i++ )
          {
          //Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
          pt2[0] = pt1[0] + x[0];
          pt2[1] = pt1[1] + x[1];
          pt2[2] = pt1[2] + x[2];

          //Adicionar novo node
          segm->GetNodeData(i)->coords[0] = pt1[0];
          segm->GetNodeData(i)->coords[1] = pt1[1];
          segm->GetNodeData(i)->coords[2] = pt1[2];

          pt1[0] = pt2[0];
          pt1[1] = pt2[1];
          pt1[2] = pt2[2];
          }

        //calcula as novas propriedades dos elementos considerando o tipo de interpolação
        segm->SetElementsFromSegment(segm->GetinterpolationType());
        //calcula as novas propriedades dos nodes considerando o tipo de interpolação
        nodeArray[5] = segm->GetLength();
        nodeArray[6] = nodeArray[5]/numElements; //dx

        segm->SetNodesProps(nodeArray);
        }
      }
    }
}
