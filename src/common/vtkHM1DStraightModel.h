/*
 * $Id:vtkHM1DStraightModel.h 109 2006-03-27 15:04:14Z ziemer $
 */
// .NAME vtkHM1DStraightModel - Class that holds and manages the representation of a 1Dtree.
// .SECTION Description
// vtkHM1DStraightModel is the class that holds and manages the way a 1D tree is created and modified
// by its users.

#ifndef _vtkHM1DStraightModel_h_
#define _vtkHM1DStraightModel_h_

#include "vtkHMSimplifiedModel.h"
#include <map> // hash of node data.
#include <string>
using namespace std;
#include "vtkIntArray.h"

class vtkHM1DTreeMesh;
class vtkHM1DTerminal;
class vtkHM1DSegment;
class vtkHM1DTreeElement;
struct vtkClientServerID;


// Na declaração da classe também deve entrar o nome do projeto
class VTK_EXPORT vtkHM1DStraightModel : public vtkHMSimplifiedModel
{
public:
  vtkTypeRevisionMacro(vtkHM1DStraightModel,vtkHMSimplifiedModel);
  void PrintSelf(ostream& os, vtkIndent indent);
  static vtkHM1DStraightModel* New();

  //BTX
  typedef std::map<vtkIdType,vtkHM1DTreeElement *>  TreeNodeDataMap;
  typedef std::pair<vtkIdType,vtkHM1DTreeElement *> TreeNodeDataPair;

  //Informações do nome do segmento e o id. Auxilia na busca do segmento pelo nome
  typedef std::map<char *, vtkIdType> SegmentInformationMap;
  typedef std::pair<char *, vtkIdType> SegmentInformationPair;

  //Informações de segmento e os elementos com intervenções ou patologias
  typedef std::map<vtkIdType, vtkIdList*> InterventionsMap;
  typedef std::pair<vtkIdType, vtkIdList*> InterventionsPair;

  //Informações de relação de intervenções ou patologias com segmento.
  typedef std::multimap<vtkIdType,vtkIdType> InterventionsInformationMultimap;
  typedef std::pair<vtkIdType,vtkIdType> InterventionsInformationPair;

  //Informações de segmento e os elementos e os terminais nulos no
  //modelo acoplado.
  typedef std::map<vtkIdType, vtkIdList*> NullElementsMap;
  typedef std::pair<vtkIdType, vtkIdList*> NullElementsPair;
  //ETX

  // Description:
  // Returns a reference to the root of the 1D tree.
  vtkHM1DTreeElement *Get1DTreeRoot();

  vtkGetMacro(MaxId,vtkIdType);


  // Description:
  // Adds a segment as child of a specific tree element by its Id in a way that
  // the tree stays consistent.
  vtkIdType AddSegment(vtkIdType);

  // Description:
  // Adds a segment as child of a specific tree element using the element pointer
  // in a way that the tree stays consistent.
  vtkIdType AddSegment(vtkHM1DTreeElement *);

  TreeNodeDataMap GetSegmentsMap();
  TreeNodeDataMap GetTerminalsMap();

  // Description:
  // Removes a terminal in a consistent way. If second parameter is 1 recurses over the tree,
  // other ways, the childs are assigned to the parent of the removed element.
  int RemoveSegment(vtkIdType,int);
  int RemoveSegmentsInSubTree(vtkIdList *list, int recurse);
  int RemoveSegment(vtkIdType id, vtkIdType parentId, int recurse);
  int RemoveSegment(vtkHM1DTreeElement *,int);

  // Description:
  // Recursivelly clears the tree.
  int ClearTree();

  // Description:
  // Returns the reference to a Segment.
  vtkHM1DSegment *GetSegment(vtkIdType);
  vtkHM1DSegment *GetSegment(char *name);

  // Description:
  // Returns the reference to a terminal.
  vtkHM1DTerminal *GetTerminal(vtkIdType);

  // Description:
  // Returns true if the id corresponds to a segment.
  bool IsSegment(vtkIdType);

  // Description:
  // Returns true if the id corresponds to a terminal.
  bool IsTerminal(vtkIdType);

  // Description:
  // Given a vtkidtype this method searches for the desired element and returns a vtkIntArray
  // with the CSId of the terminal (and its related segments) or the CSID of the segment
  //vtkClientServerID GetTerminalCSID(vtkIdType);
//  vtkIntArray *GetTerminalCSID(vtkIdType id);
  vtkIdType GetTerminalCSID(vtkIdType id);

  // Description:
  // serches for csid of one given segment
  // returns a array:
  // 0: csid of segment
  // 1: number of nodes of segment
  // 2: number of elements of a segment
  vtkIntArray *GetSegmentCSID(vtkIdType id);

  void SetName(const char* name){this->name = name;return;}
  const char* GetName(){return name;};

  void SetModelName(const char* name){this->modelName = modelName;return;}
  const char* GetModelName(){return modelName;};

  // Description:
  // Deep copy of the object.
  virtual void DeepCopy(vtkHM1DStraightModel *);

	int GetNumberOfSegments();
	int GetNumberOfTerminals();
	int GetNumberOfNodes();

	// Description:
  // runs the StraightModel 1D tree searching for elements with null viscoelasticity, returning 0 if such element is found. Otherwise returns 1;
	int CheckForNullViscoElasticElements();


	// Description:
  // Adds a terminal as child of a specific tree element by its Id in a consistent way.
  vtkIdType AddTerminal(vtkIdType);

	// Description:
  // Removes a terminal in a consistent way. If second parameter is 1 recurses over the tree,
  // other ways, the childs are assigned to the parent of the removed element.
  int RemoveTerminal(vtkIdType,int);
  int RemoveTerminal(vtkHM1DTreeElement *,int);

  void SetInstantOfTime(vtkDoubleArray *array);
  vtkDoubleArray *GetInstantOfTime();

  // Description:
  // this method upodate the value of Reference or Infiltration pressure of every elements
  // from the StraightModel tree
  void SetTreePressure(int pressure, double value);

	// Description:
	// return a Id list stored in a char array for whole segments from the StraightModel tree
	char *GetSegmentsID();


	// Description:
	// Update the parameters from tree's internal terminals
	void SetNonLeafTerminalsDefaultValues(double R1, double R2, double Cap, double Pressure);

	// Description:
	// Update the parameters from tree's leafs terminals
	void SetLeafTerminalsDefaultValues(double R1, double R2, double Cap, double Pressure);

	// Description:
	// Get AgingElastinFactor
  vtkGetMacro(AgingElastinFactor,double);
  // Description:
  // Get AgingViscoelasticityFactor
  vtkGetMacro(AgingViscoelasticityFactor,double);

  // Description:
  // Set factor of elastin and viscoelasticity and calculate aging.
  void SetAgingFactor(double elastinFactor, double viscoelasticityFactor);

  // Description:
  // Get factor of elastin and viscoelasticity of the aging.
  void GetAgingFactor(double factor[2]);

  // Description:
  // Remove aging and set default values for elastin and viscoelasticity factors.
  void RemoveAging();

  // Description:
  // Adds a segment as child of a specific tree element using the element pointer
  // in a way that the tree stays consistent. Does not create a terminal child on the end.
  vtkIdType AddSegmentWithoutChild(vtkIdType parentId);

  // Description:
  // Adds information about clip. Name of first and second segments
  // flow direction -->
  //
  //     1º segment		  clip		 2º segment
  // t__________________t| |t____________________t
  //
  // t = terminal
  int AddClip(char *seg1, char *seg2);

  // Description:
  // Add clip.
  void AddClip(vtkIdType seg1, vtkIdType seg2);

  // Description:
  // Remove clip.
  void RemoveClip(vtkIdType idClip);

  // Description:
  // Return number of clips.
  int GetNumberOfClips(){return ClipSegments->GetNumberOfTuples();}

  // Description:
  // Return ids of the segments associated with clip.
  void GetClip(vtkIdType idClip, vtkIdType idSegments[2]);

	// Description:
	// Find ids of the segments associated with clip
	// and return position in array
	vtkIdType FindClipIds(vtkIdType idSegment);

  // Description:
  // Add stent information.
  void AddStent(vtkIdType idSegment, vtkIdType idStent, vtkIdList *stentList);

  // Description:
  // Remove stent information.
  void RemoveStent(vtkIdType idSegment, vtkIdType idStent);

  // Description:
  // Remove all stent information.
  void RemoveAllStents();

  // Description:
  // Return number of stents.
  int GetNumberOfStents() { return StentMap.size(); }

  // Description:
  // Return multimap with informations about stent and segment association.
  InterventionsInformationMultimap GetStentsInformation();

  // Description:
  // Return id list of the elements with stent.
  vtkIdList *GetStent(vtkIdType idStent);

  // Description:
  // Add stenosis information.
  void AddStenosis(vtkIdType idSegment, vtkIdType idStenosis, vtkIdList *stenosisList);

  // Description:
  // Remove stenosis information.
  void RemoveStenosis(vtkIdType idSegment, vtkIdType idStenosis);

  // Description:
  // Return number of stenosis
  int GetNumberOfStenosis() { return StenosisMap.size(); }

  // Description:
  // Return multimap with informations about stenosis and segment association.
  InterventionsInformationMultimap GetStenosisInformation();

  // Description:
  // Return id list of the elements with stenosis.
  vtkIdList *GetStenosis(vtkIdType idStenosis);

  // Description:
  // Add aneurysm information.
  void AddAneurysm(vtkIdType idSegment, vtkIdType idAneurysm, vtkIdList *aneurysmList);

  // Description:
  // Remove aneurysm information.
  void RemoveAneurysm(vtkIdType idSegment, vtkIdType idAneurysm);

  // Description:
  // Return number of aneurysm
  int GetNumberOfAneurysm() { return AneurysmMap.size(); }

  // Description:
  // Return multimap with informations about aneurysm and segment association.
  InterventionsInformationMultimap GetAneurysmInformation();

  // Description:
  // Return id list of the elements with aneurysm.
  vtkIdList *GetAneurysm(vtkIdType idAneurysm);

  // Description:
  // Set/Get volume of the tree.
  vtkSetMacro(TreeVolume,double);
  vtkGetMacro(TreeVolume,double);

  // Description:
  // Calculate volume of the tree
  double CalculateTreeVolume();

  // Description:
  // Move children coordinates
  void MoveChildren(vtkIdType rootId, double coord[3]);

  // Description:
  // Reset list of visited element.
  void ResetElementVisitedList();

  // Description:
  // Add null elements in map.
  // Ex.: idElement is a segment or terminal and elementList are the elements of the segment.
  void AddNullElements1D3D(vtkIdType idElement, vtkIdList *elementList);

  // Description:
  // Add null elements in map.
  // Ex.: idElement is a segment or terminal and element is the element of the segment.
  void AddNullElements1D3D(vtkIdType idElement, vtkIdType element);

  // Description:
  // Remove all null elements with element id of the map.
  // Ex.: idElement is a segment or terminal.
  void RemoveNullElements1D3D(vtkIdType idElement);

  // Description:
  // Remove null elements
  // Ex.: idElement is a segment or terminal and elementList are the elements of the segment.
  void RemoveNullElements1D3D(vtkIdType idElement, vtkIdList *elementList);

  // Description:
  // Remove all null elements of the map.
  void ClearNullElements1D3DMap();

  // Description;
  // Get list with null elements.
  vtkIdList *GetNullElements1D3DList(vtkIdType idElement);

  // Description:
  // Get null elements 1D 3D map.
	NullElementsMap GetNullElements1D3DMap();

	// Description:
	// Verify if element is in null elements 1D 3D map.
	bool IsNullElements1D3D(vtkIdType idElement);

	// Description:
	// Verify if has null elements of the coupled model.
	bool HasNullElements1D3D();

	// Description:
	// Find smaller segment of the tree and return your id.
	vtkIdType GetSmallerSegment();

	// Description:
	// Configure all elements of all segment of the tree with lenght
	void ConfigureDefaultLengthOfElements(double elementLength);

	// Description:
	// Get map with name of the segments.
	SegmentInformationMap GetSegmentNamesMap();

	// Description:
	// Copy segments:
	vtkIdList* CopySegments(vtkIdList *list);

	// Description:
	// Returns Segment name for the past ID :
	char* GetSegmentName(vtkIdType *id);

	// Description:
	// Flip segments:
	void FlipSegments(vtkIdList *list);

	// Description:
	// Refine segments.
	void RefineSegment(vtkIdList *list, int method, int factor);

	// Description:
	// Set/Get FinalTime.
  vtkSetMacro(FinalTime, float);
  vtkGetMacro(FinalTime, float);

protected:
	vtkHM1DStraightModel();
	virtual ~vtkHM1DStraightModel();

  // Description:
  // Mesh that represents the tree to be shown in the screen (in vtk format).
  vtkHM1DTreeMesh *treeMesh;

  // Description:
  // Maximum Id already asigned to tree elements.
  vtkIdType MaxId;

  // Description:
  // 1D tree root.
  vtkHM1DTreeElement *treeRoot;

  // Description:
  // Ids corresponding to the segments.
  //vtkArrayMap<int,vtkHM1DTreeElement *> segmentsMap;
  TreeNodeDataMap segmentsMap;

  // Description:
  // Ids corresponding to the terminals.
  TreeNodeDataMap terminalsMap;

  // Description:
  // Recursivelly remove an element by its id.
  void RemoveElementChildsRecursively(vtkIdType);

  // Description:
  // Class/object name.
  const char* name;

  // Description:
  // Model name.
  const char* modelName;

  // Description:
	// Instant of time of the resolution of the problem
	vtkDoubleArray *InstantOfTime;

  // Description:
  // Adds a terminal as child of a specific tree element using the element pointer
  // in a consistent way.
  vtkIdType AddTerminal(vtkHM1DTreeElement *);
	//Description:
	// Removes an element thru the ProcessModule. Should only be used internally.
  int RemoveElementInternal(vtkIdType id);

	// Description:
	// Clones a given segment/terminal.
	void CloneSegment(vtkHM1DStraightModel *, vtkHM1DTreeElement *, vtkHM1DTreeElement *, vtkHM1DSegment *);
	void CloneTerminal(vtkHM1DStraightModel *, vtkHM1DTreeElement *, vtkHM1DTreeElement *, vtkHM1DTerminal *);

  // Description:
  // Adds a segment as child of a specific tree element using the element pointer
  // in a way that the tree stays consistent. Does not create a terminal on the end,
  // as is intended to be userd ONLY by the Deepopy() method!!!.
  vtkIdType AddSegmentInternal(vtkIdType);

  vtkIdType AddSegmentInternal(vtkIdType, char*);

	// Description:
	//List with visited elements
	vtkIdList *ElementVisited;

  char *IdCharList2;
  string IdCharList;

  // Description:
  // Factors of elastin and viscoelasticity for aging
  double AgingElastinFactor;
  double AgingViscoelasticityFactor;

  // Description:
  // Map with name of segment like key and segment id like data.
  SegmentInformationMap SegmentNamesMap;

	// Description:
	// Id of the segments associated with clip, first is segment before the clip
	// and second is segment after the clip
  vtkIntArray *ClipSegments;

  // Description:
  // Map with stent id like key and id list of the elements with stent like data.
  InterventionsMap StentMap;

  // Description:
  // Multimap with segment id like key and stent id like data.
  // Relation between stent and segment
  InterventionsInformationMultimap StentMultimap;

  // Description:
  // Map with stenosis id like key and id list of the elements with stenosis like data.
  InterventionsMap StenosisMap;

  // Description:
  // Multimap with segment id like key and stenosis id like data.
  // Relation between stenosis and segment
  InterventionsInformationMultimap StenosisMultimap;

  // Description:
  // Map with aneurysm id like key and id list of the elements with aneurysm like data.
  InterventionsMap AneurysmMap;

  // Description:
  // Multimap with segment id like key and aneurysm id like data.
  // Relation between aneurysm and segment
  InterventionsInformationMultimap AneurysmMultimap;

  // Description:
  // Volume of the tree.
  double TreeVolume;

  // Description:
  // Null elements of the coupled model. Null elements can be terminals and elements.
  NullElementsMap NullElements1D3DMap;

  // Description:
  // Final time configured in BasParam file.
  float FinalTime;

};

#endif /*_vtkHM1DStraightModel_h_*/
