/*
 * $Id:vtkHM1DTerminal.cxx 286 2006-05-05 19:43:09Z nacho $
 */

#include "vtkHM1DTerminal.h"
#include "vtkHM1DSegment.h"
#include "vtkHMUtils.h"

#include <string>
using namespace std;

vtkCxxRevisionMacro(vtkHM1DTerminal, "$Rev:286 $");
vtkStandardNewMacro(vtkHM1DTerminal);

//-----------------------------------------------------------------

vtkHM1DTerminal::vtkHM1DTerminal():vtkHM1DTreeElement(){
  r1.clear();
  r1Time.clear();
  r2.clear();
  r2Time.clear();
  c.clear();
  cTime.clear();
  pt.clear();
  ptTime.clear();
  this->ResolutionArray = NULL;
  this->Result.clear();
  this->meshID = -1;
  this->AssNodes.clear();

  // resetando array
//  for (int i = 0; i < 7500; ++i)
//   {
//   ResultArray[i] = 0;
//   TimeArray[i] = 0;
//   ResolutionData[i] = 0;
//   }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::~vtkHM1DTerminal()
{
  r1.clear();
  r1Time.clear();
  r2.clear();
  r2Time.clear();
  c.clear();
  cTime.clear();
  pt.clear();
  ptTime.clear();
  if(this->ResolutionArray)
  this->ResolutionArray->Delete();
  this->Result.clear();
  this->AssNodes.clear();
}

//-----------------------------------------------------------------
char *vtkHM1DTerminal::GetArray(char elem, int option)
{

	for (int i = 0; i < 7500; ++i)
    ResultArray[i] = 0;

	ResistanceValuesList::iterator it;
	int list_size, i = 0;
	char Data[25] ={0};

	switch (elem)
		{
		case 'R':
			{
			if (option==1)
				{
				it=r1.begin();
				list_size=r1.size();
				break;
				}
			if (option==2)
				{
				it=r2.begin();
				list_size=r2.size();
				break;
				}
			}
		case 'C':
			{
			it=c.begin();
			list_size=c.size();
			break;
			}
		case 'P':
			{
			it=pt.begin();
			list_size=pt.size();
			break;
			}
		}

	for (int i=0; i<list_size+1; i++)
		{

			if (!i)
				{
				sprintf(Data, "%u;", list_size);

		    strcat(ResultArray, Data);
		    //**************************
		    for (int i=0; i<25; i++)
		    	Data[i]=0;
		    //*************************

				}
			else
				{
				// insert the values of list
				sprintf(Data, "%e;", *it);
		    strcat(ResultArray, Data);
		    //**************************
		    for (int i=0; i<25; i++)
		    	Data[i]=0;
		    //*************************

				//Array->InsertNextValue(*it);
				it++;
				}
		}
	return ResultArray;
}

//-----------------------------------------------------------------
char *vtkHM1DTerminal::GetTimeArray(char elem, int option)
{

	for (int i = 0; i < 7500; ++i)
   TimeArray[i] = 0;


	ResistanceTimeList::iterator it;
	int list_size, i = 0;
	char Data[25] ={0};

	switch (elem)
		{
		case 'R':
			{
			if (option==1)
				{
				it=this->r1Time.begin();
				list_size=this->r1Time.size();
				break;
				}
			if (option==2)
				{
				it=this->r2Time.begin();
				list_size=this->r2Time.size();
				break;
				}
			}
		case 'C':
			{
			it=this->cTime.begin();
			list_size=this->cTime.size();
			break;
			}
		case 'P':
			{
			it=this->ptTime.begin();
			list_size=this->ptTime.size();
			break;
			}
		}

	for (int i=0; i<list_size+1; i++)
		{

			if (!i)
				{
				sprintf(Data, "%u;", list_size);

		    strcat(TimeArray, Data);
		    //**************************
		    for (int i=0; i<25; i++)
		    	Data[i]=0;
		    //*************************

				}
			else
				{
				// insert the values of list
				sprintf(Data, "%e;", *it);
		    strcat(TimeArray, Data);
		    //**************************
		    for (int i=0; i<25; i++)
		    	Data[i]=0;
		    //*************************

				//Array->InsertNextValue(*it);
				it++;
				}
		}
	return TimeArray;
}

//-----------------------------------------------------------------

void vtkHM1DTerminal::SetArray(vtkDoubleArray *array, char elem, int option)
{
	ResistanceValuesList aux;
	aux.clear();
	unsigned int i=0;

	switch (elem)
	{
		case 'R':
		{
			if (option==1)
			{
				for (int i=0; i < array->GetNumberOfTuples(); i++)
				{
					aux.push_back(array->GetValue(i));
				}
				this->SetR1(&aux);
				break;
			}
			if (option==2)
			{
				for (int i=0; i < array->GetNumberOfTuples(); i++)
				{
					aux.push_back(array->GetValue(i));
				}
				this->SetR2(&aux);
				break;
			}
		}
		case 'C':
		{
			for (int i=0; i < array->GetNumberOfTuples(); i++)
			{
				aux.push_back(array->GetValue(i));
			}
			this->SetC(&aux);
			break;
		}
		case 'P':
		{
			for (int i=0; i < array->GetNumberOfTuples(); i++)
			{
				aux.push_back(array->GetValue(i));
			}
			this->SetPt(&aux);
			break;
		}
	}
}

//-----------------------------------------------------------------

void vtkHM1DTerminal::SetTerminalParam(double R1, double R2, double Cap, double Pr)
{
	ResistanceValuesList aux;
	aux.clear();

	aux.push_back(R1);
	this->SetR1(&aux);
	aux.clear();


	aux.push_back(R2);
	this->SetR2(&aux);
	aux.clear();


	aux.push_back(Cap);
	this->SetC(&aux);
	aux.clear();


	aux.push_back(Pr);
	this->SetPt(&aux);
	aux.clear();
}

//-----------------------------------------------------------------

void vtkHM1DTerminal::SetTimeArray(vtkDoubleArray *array, char elem, int option)
{
	ResistanceValuesList aux;
	aux.clear();
	unsigned int i=0;

	switch (elem)
	{
		case 'R':
		{
			if (option==1)
			{
				for (int i=0; i < array->GetNumberOfTuples(); i++)
				{
					aux.push_back(array->GetValue(i));
				}
				this->SetR1Time(&aux);
				break;
			}
			if (option==2)
			{
				for (int i=0; i < array->GetNumberOfTuples(); i++)
				{
					aux.push_back(array->GetValue(i));
				}
				this->SetR2Time(&aux);
				break;
			}
		}
		case 'C':
		{
			for (int i=0; i < array->GetNumberOfTuples(); i++)
			{
				aux.push_back(array->GetValue(i));
			}
			this->SetCTime(&aux);
			break;
		}
		case 'P':
		{
			for (int i=0; i < array->GetNumberOfTuples(); i++)
			{
				aux.push_back(array->GetValue(i));
			}
			this->SetPtTime(&aux);
			break;
		}
	}
}

//-----------------------------------------------------------------


void vtkHM1DTerminal::DeepCopy(vtkHM1DTerminal *orig)
{
	this->Superclass::DeepCopy(orig);

  r1.clear();

	ResistanceValuesList::iterator ir = orig->GetR1()->begin();
	for (;ir!=orig->GetR1()->end();ir++)
		{
		r1.push_back(*ir);
		}

	r1Time.clear();

	ResistanceTimeList::iterator irTime = orig->GetR1Time()->begin();
	for (;irTime!=orig->GetR1Time()->end();irTime++)
		{
		r1Time.push_back(*irTime);
		}

  r2.clear();
	ir = orig->GetR2()->begin();
	for (;ir!=orig->GetR2()->end();ir++)
		{
		r2.push_back(*ir);
		}

	r2Time.clear();

  irTime = orig->GetR2Time()->begin();
	for (;irTime!=orig->GetR2Time()->end();irTime++)
		{
		r2Time.push_back(*irTime);
		}

	c.clear();
	CapacitanceValuesList::iterator ic = orig->GetC()->begin();
	for (;ic!=orig->GetC()->end();ic++)
		{
		c.push_back(*ic);
		}

	cTime.clear();
	CapacitanceTimeList::iterator icTime = orig->GetCTime()->begin();
	for (;icTime!=orig->GetCTime()->end();icTime++)
		{
		cTime.push_back(*icTime);
		}

	pt.clear();
  ResistanceValuesList::iterator ip = orig->GetPt()->begin();
  for (;ip!=orig->GetPt()->end();ip++)
    {
    pt.push_back(*ip);
    }

	ptTime.clear();
  ResistanceTimeList::iterator ipTime = orig->GetPtTime()->begin();
  for (;ipTime!=orig->GetPtTime()->end();ipTime++)
    {
    ptTime.push_back(*ipTime);
    }

  this->meshID = orig->GetmeshID();

	if(orig->GetResolutionArray())
	{
		this->ResolutionArray = vtkDoubleArray::New();
	  this->ResolutionArray->DeepCopy(orig->GetResolutionArray());
	}
	else
		this->ResolutionArray = NULL;
}

//-----------------------------------------------------------------

void vtkHM1DTerminal::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  const char* name = this->GetName();
  if (name)
    {
    os << indent << "Name: " << name << "\n";
    }
  else
    {
    os << indent << "Name: (none)\n";
    }
  os << indent << "r1.size(): " << r1.size() << "\n";
  os << indent << "r1Time.size(): " << r1Time.size() << "\n";
  os << indent << "r2.size(): " << r2.size() << "\n";
  os << indent << "r2Time.size(): " << r2Time.size() << "\n";
  os << indent << "c.size():  " << c.size() << "\n";
  os << indent << "cTime.size(): " << cTime.size() << "\n";
  os << indent << "pt.size(): " << pt.size() << endl;
  os << indent << "ptTime.size(): " << ptTime.size() << "\n";
}

//-----------------------------------------------------------------

vtkHM1DTerminal::ResistanceValuesList *vtkHM1DTerminal::GetR1()
{
  if (r1.empty())
    {
    //vtkWarningMacro(<< "Cannot GetR1, no values!.");
    return &r1;
    }
  else
    {
    return &r1;
    }
}

//-----------------------------------------------------------------

void vtkHM1DTerminal::SetR1(vtkHM1DTerminal::ResistanceValuesList *newR1)
{
  r1.clear();
  ResistanceValuesList::iterator it = newR1->begin();
  for (it = newR1->begin();it!=newR1->end();it++)
    {
    r1.push_back(*it);
    }
}

//-----------------------------------------------------------------

vtkHM1DTerminal::ResistanceTimeList *vtkHM1DTerminal::GetR1Time()
{
  if (r1Time.empty())
    {
    //vtkWarningMacro(<< "Cannot GetR1, no values!.");
    return &r1Time;
    }
  else
    {
    return &r1Time;
    }
}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetR1Time(vtkHM1DTerminal::ResistanceTimeList *newR1)
{
  r1Time.clear();
  ResistanceTimeList::iterator it = newR1->begin();
  for (it = newR1->begin();it!=newR1->end();it++)
    {
    r1Time.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::ResistanceValuesList *vtkHM1DTerminal::GetR2()
{
  if (r2.empty())
    {
    //vtkWarningMacro(<< "Cannot GetR2, no values!.");
    return &r2;
    }
  else
    {
    return &r2;
    }
}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetR2(ResistanceValuesList *newR2)
{
  r2.clear();
  ResistanceValuesList::iterator it = newR2->begin();
  for (;it!=newR2->end();it++)
    {
    r2.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::ResistanceTimeList *vtkHM1DTerminal::GetR2Time()
{
  if (r2Time.empty())
    {
    //vtkWarningMacro(<< "Cannot GetR1, no values!.");
    return &r2Time;
    }
  else
    {
    return &r2Time;
    }
}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetR2Time(vtkHM1DTerminal::ResistanceTimeList *newR2)
{
  r2Time.clear();
  ResistanceTimeList::iterator it = newR2->begin();
  for (it = newR2->begin();it!=newR2->end();it++)
    {
    r2Time.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::CapacitanceValuesList *vtkHM1DTerminal::GetC()
{
  if (c.empty())
    {
    //vtkWarningMacro(<< "Cannot GetC, no values!.");
    return &c;
    }
  else
    {
    return &c;
    }
}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetC(CapacitanceValuesList *newC)
{
  c.clear();
  CapacitanceValuesList::iterator it = newC->begin();
  for (;it!=newC->end();it++)
    {
    c.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::CapacitanceTimeList *vtkHM1DTerminal::GetCTime()
{
  if (cTime.empty())
    {
    //vtkWarningMacro(<< "Cannot GetC, no values!.");
    return &cTime;
    }
  else
    {
    return &cTime;
    }
}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetCTime(CapacitanceTimeList *newC)
{
  cTime.clear();
  CapacitanceTimeList::iterator it = newC->begin();
  for (;it!=newC->end();it++)
    {
    cTime.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::PressureValuesList *vtkHM1DTerminal::GetPt()
{
  if (pt.empty())
    {
    //vtkWarningMacro(<< "Cannot GetPt, no values!.");
    return &pt;
    }
  else
    {
    return &pt;
    }

}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetPt(PressureValuesList *newPt)
{
  pt.clear();
  PressureValuesList::iterator it = newPt->begin();
  for (;it!=newPt->end();it++)
    {
    pt.push_back(*it);
    }
}
//-----------------------------------------------------------------

vtkHM1DTerminal::PressureTimeList *vtkHM1DTerminal::GetPtTime()
{
  if (ptTime.empty())
    {
    //vtkWarningMacro(<< "Cannot GetPt, no values!.");
    return &ptTime;
    }
  else
    {
    return &ptTime;
    }

}
//-----------------------------------------------------------------

void vtkHM1DTerminal::SetPtTime(PressureTimeList *newPt)
{
  ptTime.clear();
  PressureTimeList::iterator it = newPt->begin();
  for (;it!=newPt->end();it++)
    {
    ptTime.push_back(*it);
    }
}

//-----------------------------------------------------------------
//GetResolutionArray
vtkDoubleArray *vtkHM1DTerminal::GetResolutionArray()
{
	if(this->ResolutionArray)
		return this->ResolutionArray;
	else
		return NULL;
}

//-----------------------------------------------------------------
//SetResolutionArray
void vtkHM1DTerminal::SetResolutionArray(vtkDoubleArray *resolution)
{
	if(resolution)
    {
    if ( !this->ResolutionArray )
      this->ResolutionArray = vtkDoubleArray::New();
    this->ResolutionArray->DeepCopy(resolution);
    }
	else
	  {
	  if ( this->ResolutionArray )
	    this->ResolutionArray->Delete();
		this->ResolutionArray = NULL;
	  }
}

//-----------------------------------------------------------------

int vtkHM1DTerminal::GetTerminalNumberOfSons(void)
{
 	return this->GetChildCount();
}

//-----------------------------------------------------------------

double vtkHM1DTerminal::GetHeartFinalTime(void)
{
	ResistanceTimeList::iterator it = r1Time.end();
	it--;
	return *it;
}

//-----------------------------------------------------------------


int vtkHM1DTerminal::GetResolutionNumberOfTuples()
{
	if (this->ResolutionArray)
  	return this->ResolutionArray->GetNumberOfTuples();
  else
  	{
   	vtkErrorMacro("Couldn't Get ResolutionArray Tuples Number from terminal");
   	return 0;
  	}
}

//-----------------------------------------------------------------

int vtkHM1DTerminal::GetResolutionArrayNumberOfComponents()
{
  if (this->ResolutionArray)
  	return this->ResolutionArray->GetNumberOfComponents();
  else
    {
   	vtkErrorMacro("Couldn't Get ResolutionArray Components Number from Terminal");
   	return 0;
  	}
}

//-----------------------------------------------------------------

const char *vtkHM1DTerminal::GetResolutionArrayAsChar()
{
	Result.clear();
 	if (this->ResolutionArray) // se tenho resolution array neste terminal
 		{
  	string temp = vtkHMUtils::ConvertDoubleArrayToChar(this->ResolutionArray);
  	Result.append(temp.c_str());
  	return Result.c_str();
		}

  else
  {
  	vtkErrorMacro("Could not get Terminal Resolution Array  ");
  	//return a reference to a empty string
 		return NULL;

  }
}

//-----------------------------------------------------------------
vtkIdType vtkHM1DTerminal::GetFirstChildId()
{
	if ( this->IsLeaf() )
		return 0;
	else
		return this->GetFirstChild()->GetId();
}

const char *vtkHM1DTerminal::GetAssNodesMeshID()
{
	vtkHM1DTreeElement *parent = NULL;
	vtkHM1DTreeElement *child = NULL;
	vtkHM1DSegment *assNodes;
	vtkIdList *List = vtkIdList::New();

//	Se tiver parents adiciona na lista
	parent = this->GetFirstParent();
	while ( parent )
		{
		assNodes = vtkHM1DSegment::SafeDownCast(parent);
		if(assNodes)
			List->InsertNextId(assNodes->GetNodeData(assNodes->GetNodeNumber()-1)->meshID);

		parent = this->GetNextParent();
		}

//	Se tiver childs adiciona na lista
	child = this->GetFirstChild();
	while ( child )
		{
		assNodes = vtkHM1DSegment::SafeDownCast(child);
		if(assNodes)
			List->InsertNextId(assNodes->GetNodeData(0)->meshID);

		child = this->GetNextChild();
		}

	this->AssNodes = vtkHMUtils::ConvertIdListToChar(List);
	//char ch[20];
	//strcpy (ch,listChar.c_str());


	List->Delete();
	return AssNodes.c_str();
	//return ch;
}
