/*
 * $Id:vtkHM1DTerminal.h 109 2006-03-27 15:04:14Z diego $
 */
// .NAME vtkHM1DTerminal - Class that represents a terminal element in of the 1D 
// straigt tubes representation of the arterial tree.
// .SECTION Description
// vtkHM1DTerminal is the representation of the 1D Tree Elements terminals for a 
// strait tubes one dimensional representation of the arterial tree. Its responsible 
// for handling the information of a particular tree terminal. It may hold a sigle 
// value for it's different parameter or a list of them.
 
#ifndef _vtkHM1DTerminal_h_
#define _vtkHM1DTerminal_h_

#include "vtkHM1DTreeElement.h"

#include "vtkDoubleArray.h"
#include "vtkType.h"

#include <string>
using namespace std;

#include <list> // List of values for the two resistances, capacitance and pressure(in time)


class VTK_EXPORT vtkHM1DTerminal:public vtkHM1DTreeElement
{
public:
//BTX
  typedef std::list<double> ResistanceValuesList;
  typedef std::list<double> ResistanceTimeList;
  typedef std::list<double> CapacitanceValuesList;
  typedef std::list<double> CapacitanceTimeList;
  typedef std::list<double> PressureValuesList;
  typedef std::list<double> PressureTimeList;
//ETX
protected:

//BTX
// Description:
// Values for the resistance R1.
  ResistanceValuesList r1;

// Description:
// Time for the resistance R1.
  ResistanceTimeList r1Time;
  
// Description:
// Values for the resistance R2.
  ResistanceValuesList r2;

// Description:
// Time for the resistance R2.
  ResistanceTimeList r2Time;
  
// Description:
// Values for the capacitance C.
  CapacitanceValuesList c;
  
// Description:
// Time for the capacitance C.
  CapacitanceTimeList cTime;
  
// Description:
// Values for the pressure pt.
  PressureValuesList pt;

// Description:
// Time for the pressure pt.
  PressureTimeList ptTime;
    
  //Description:
  //Resolution of the data submited when solver
  vtkDoubleArray *ResolutionArray;
  
  
  char ResultArray[7500];
  
  char TimeArray[7500];
  
  char ResolutionData[7500];
  
  string Result;
  
  int meshID;

  string AssNodes;
  
//ETX
public:
/*
 * Esta macro cria os metodos de revisão e de typo usados pelo vtkObject.
 */
  vtkTypeRevisionMacro(vtkHM1DTerminal,vtkHM1DTreeElement);
  void PrintSelf(ostream& os, vtkIndent indent);
  static vtkHM1DTerminal *New();

 // Description:
  char *GetArray(char elem, int option);
  
  // Description:
  char *GetTimeArray(char elem, int option);

  // Description:
  // this method receives a pointer to a vtkDoubleArray (created in Widget Client) and update the element 
  // (type selected by the char var elem: 'R' -->resistance, 'C' --> capacitance, 'P' --> Pressure 
  // the values of R1 or R2 (selected by ivar option) list.
  // ** here is assumed that the size of the array is of the same size of the 
  // "local" list.
  void SetArray(vtkDoubleArray *array, char elem,  int option);
  
  
  
  // Description:
  // 
  void SetTerminalParam(double R1, double R2, double Cap, double Pr);
  
  // Description:
  // this method receives a pointer to a vtkDoubleArray (created in Widget Client) and update the element 
  // (type selected by the char var elem: 'R' -->resistance, 'C' --> capacitance, 'P' --> Pressure 
  // the values of the time of R1 or R2 (selected by ivar option) list.
  // ** here is assumed that the size of the array is of the same size of the 
  // "local" list.
  void SetTimeArray(vtkDoubleArray *array, char elem,  int option);
  
  // Description:
  // returns the number of sons from Terminal 
  // if this is leaf terminal then the value returned will be Zero 
  int GetTerminalNumberOfSons(void); 
  
  // Description:
	// retuns the number of tuples found in the terminal resolution array
	int GetResolutionNumberOfTuples();
  
  // Description:
	// returns the number of components present in each resolution array tuple
	int GetResolutionArrayNumberOfComponents();

	// Description:
	// returns the resolution array as char
	const char *GetResolutionArrayAsChar();


//BTX
	// Description:
	// Set/Get de value(s) for r1.
  ResistanceValuesList *GetR1();
  void SetR1(ResistanceValuesList *);
  
  // Description:
	// Set/Get de time(s) for r1.
  ResistanceTimeList *GetR1Time();
  void SetR1Time(ResistanceTimeList *);
  
	// Description:
	// Set/Get de value(s) for r2.
  ResistanceValuesList *GetR2();
  void SetR2(ResistanceValuesList *);

	// Description:
	// Set/Get de time(s) for r2.
  ResistanceTimeList *GetR2Time();
  void SetR2Time(ResistanceTimeList *);
  
	// Description:
	// Set/Get de value(s) for c.
  CapacitanceValuesList *GetC();
  void SetC(CapacitanceValuesList*);
  
  // Description:
	// Set/Get de time(s) for c.
  CapacitanceTimeList *GetCTime();
  void SetCTime(CapacitanceTimeList*);
  
	// Description:
	// Set/Get de value(s) for pt.
  PressureValuesList *GetPt();
  void SetPt(PressureValuesList *);
  
  // Description:
	// Set/Get de time(s) for pt.
  PressureTimeList *GetPtTime();
  void SetPtTime(PressureTimeList *);
  
//ETX
  
  //Description:
  //Set/Get of values for ResolutionArray
  vtkDoubleArray *GetResolutionArray();
  void SetResolutionArray(vtkDoubleArray *resolution);

  // Description:
  // Deep copy
  void DeepCopy(vtkHM1DTerminal *);
  
  // Description
  // Get the meshIDs of the Nodes associated with the terminal clicked
  const char *GetAssNodesMeshID();
	
  // Description:
  // returns the final time of the first heart beat
  double GetHeartFinalTime(void);
	
  vtkIdType GetFirstChildId();

	vtkSetMacro(meshID,int);
	vtkGetMacro(meshID,int);
  
protected:
	vtkHM1DTerminal();
	~vtkHM1DTerminal();
};


#endif /*_vtkHM1DTerminal_h_*/
