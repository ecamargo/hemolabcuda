/*
 * $Id: vtkHM1DTreeElement.cxx 2422 2007-11-22 18:18:01Z igor $
 */

#include "vtkHM1DTreeElement.h"
#include <algorithm>
using namespace std;

vtkCxxRevisionMacro(vtkHM1DTreeElement, "$Rev: 2422 $");
vtkStandardNewMacro(vtkHM1DTreeElement);

//vtkHM1DTreeElement::vtkHM1DTreeElement(vtkIdType id)
vtkHM1DTreeElement::vtkHM1DTreeElement()
{
  this->id = -1;
//  this->IsInMainTree = false;
  
  this->childIt = this->childs.begin();
  this->parentIt = this->Parents.begin();
//  std::cout << "criando vtkHM1DTreeElement" << endl;
/*
 *  OJOJOJOJOJOJOJOJOJOJOJOJOJOJOJOJOJO!!!!!!!!!
 *
 * Hay que inicializar el valor del Id apra algo que nosea valido para 
 * poder verificar si el id ya fue creado.
 * 
 */
//  this->clientserverId.ID = 0;
}

vtkHM1DTreeElement::~vtkHM1DTreeElement()
{
  childs.clear();
}

void vtkHM1DTreeElement::DeepCopy(vtkHM1DTreeElement *orig)
{
//	this->SetName(orig->GetName());
}

void vtkHM1DTreeElement::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  const char* name = this->GetName();
  if (name)
    {
    os << indent << "Name: " << name << "\n";
    }
  else
    {
    os << indent << "Name: (none)\n";
    }
  os << indent << "Id: " << this->id << "\n";
  os << indent << "NumberOfChilds: " << this->childs.size() << endl;
}

bool vtkHM1DTreeElement::IsLeaf()
{
  if (childs.size() == 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

vtkClientServerID vtkHM1DTreeElement::GetClientServerID()
{
//  std::cout << "Retornando CSID do tree element " << this->clientserverId << endl;
  return this->clientserverId;
}

int vtkHM1DTreeElement::GetClientServerID_int()
{
  // returns the integer register of the vtkClientSerevreID object
//  std::cout << "Retornando int CSID do tree element " << this->clientserverId << endl;
  return this->clientserverId.ID;
}


void vtkHM1DTreeElement::SetClientServerID(vtkClientServerID id)
{
  //se clientserverId ainda não foi definido
//  if (!this->clientserverId)
//  {
  	this->clientserverId = id;
//  }
//  else
//  {
//  	vtkErrorMacro(<< "Object already has a Client/Server Id!.");
//  }
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::PopFirstChild()
{
  TreeChildList::iterator popChildIt = childs.begin();
  vtkHM1DTreeElement *child;
//  popChildIt = find (childs.begin(), childs.end(), child);
//  vtkWarningMacro(<< "About to remove child!, id:" << child->GetId())
  if (popChildIt != childs.end())
    {
    child = *popChildIt;
    childs.remove(*popChildIt);
    return child;
    }
  else
    {
    return NULL;
    }
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::PopFirstParent()
{
  TreeParentList::iterator popParentIt = this->Parents.begin();
  vtkHM1DTreeElement *p;
//  popChildIt = find (childs.begin(), childs.end(), child);
  if (popParentIt != this->Parents.end())
    {
    p = *popParentIt;
    this->Parents.remove(*popParentIt);
    return p;
    }
  else
    {
    return NULL;
    }
}

//-------------------------------------------------------------------------------------------
bool vtkHM1DTreeElement::RemoveChild(vtkHM1DTreeElement *child)
{
  bool ret = false;
  TreeChildList::iterator it = childs.begin();
  it = find(childs.begin(), childs.end(), child);
//  vtkWarningMacro(<< "About to remove child!, id:" << child->GetId())
  if (it != childs.end())
  {
//    if ((*it)->IsLeaf())
//    {
      childs.remove(*it);
      ret = true;
//    }
//    else
//    {
//      vtkWarningMacro(<< "Cannot remove child with id " << child->GetId() << ", is NOT leaf!");
//      ret = false;
//    }
  }
  else
  {
    for (it = childs.begin(); it != childs.end(); it++)
    {
      if (!(*it)->RemoveChild(child))
        ret = false;
      else
        return true;        
    }
  }
  return ret;
}

//-------------------------------------------------------------------------------------------
bool vtkHM1DTreeElement::RemoveParent(vtkHM1DTreeElement *p)
{
  bool ret = false;
  TreeParentList::iterator it = this->Parents.begin();
  it = find(this->Parents.begin(), this->Parents.end(), p);
  if (it != this->Parents.end())
	  {
	  this->Parents.remove(*it);
	  ret = true;
	  }
//  else
//  {
//    for (it = this->Parents.begin(); it != this->Parents.end(); it++)
//    {
//      if (!(*it)->RemoveChild(p))
//        ret = false;
//      else
//        return true;        
//    }
//  }
  return ret;
}

//-------------------------------------------------------------------------------------------
void vtkHM1DTreeElement::AddChild(vtkHM1DTreeElement *newChild)
{
  TreeChildList::iterator it;
  it = find (childs.begin(), childs.end(), newChild);
  if (it != childs.end())
  {
    vtkWarningMacro(<< "Element with id " << this->GetId() << " already has a child with id " << newChild->GetId() << ".");
  }
  else
  {
    newChild->AddParent(this);
    it = childs.insert(it, newChild);    
  }
  
  return;
}

//-------------------------------------------------------------------------------------------
void vtkHM1DTreeElement::AddParent(vtkHM1DTreeElement *newParent)
{
  TreeParentList::iterator it;
  it = find (this->Parents.begin(), this->Parents.end(), newParent);
  
  if (it != this->Parents.end())
  {
    vtkWarningMacro(<< "Element with id " << this->GetId() << " already has a parent with id " << newParent->GetId() << ".");
  }
  else
  {
//    newParent->SetParent(this);
    it = this->Parents.insert(it, newParent);    
  }
  
  return;
}

//-------------------------------------------------------------------------------------------
int vtkHM1DTreeElement::GetChildCount()
{
  return (int)childs.size();
}

//-------------------------------------------------------------------------------------------
int vtkHM1DTreeElement::GetNumberOfParents()
{
  return (int)this->Parents.size();
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetFirstChild()
{
  childIt = childs.begin();
  
  if (childIt!=childs.end())
    {
//    vtkWarningMacro(<< " Returning first child with id: " << (*childIt)->GetId())
    return (*childIt);
    }
  else
    {
    return NULL;
    }
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetFirstParent()
{
  this->parentIt = this->Parents.begin();
  
  if (this->parentIt != this->Parents.end())
    {
    return (*this->parentIt);
    }
  else
    {
    return NULL;
    }
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetNextChild()
{
	if ( (this->childIt == this->childs.end()) && (this->childs.size() > 0)  )
		return this->GetFirstChild();
	
  if (childIt++!=childs.end() && childIt!=childs.end())
    {
    //vtkWarningMacro(<< " Returning next child with id: " << (*childIt)->GetId())
    return (*childIt);
    }
  else
    {
    return NULL;
    }
    
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetNextParent()
{
	if ( (this->parentIt == this->Parents.end()) && (this->Parents.size() > 0) )
		this->GetFirstParent();
	
  if (this->parentIt++ != this->Parents.end() && this->parentIt != this->Parents.end())
    {
    return (*this->parentIt);
    }
  else
    {
    return NULL;
    }
    
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetChild(vtkIdType childId)
{
  vtkHM1DTreeElement *ret;
  TreeChildList::iterator it;
//  it = find (childs.begin(), childs.end(), childId);
  for (it = childs.begin(); it != childs.end(); it++)
  {
  	
    if ((*it)->GetId()==childId)
    {
      return (*it);
    }
    else
    {
      ret = (*it)->GetChild(childId);
      if (ret)
        return ret;
    }
  }
  return NULL;
}

//-------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkHM1DTreeElement::GetParent(vtkIdType parentId)
{
//  vtkHM1DTreeElement *ret;
  TreeParentList::iterator it;
//  it = find (childs.begin(), childs.end(), childId);
  for (it = this->Parents.begin(); it != this->Parents.end(); it++)
  {
  	
    if ((*it)->GetId()==parentId)
    {
      return (*it);
    }
//    else
//    {
//      ret = (*it)->GetChild(childId);
//      if (ret)
//        return ret;
//    }
  }
  return NULL;
}

int vtkHM1DTreeElement::GetId()
{
  if (this->id != -1)
    {
//    vtkDebugMacro(<< "Returning element id value to: " << id << ".");
    return id;
    }
  else
    {
    vtkWarningMacro(<< "Element does not have been assigned an id!, id: " << id << ".");
//    vtkDebugMacro(<< "Element does not have been assigned an id!, id: " << id << ".");
    return -1;
    }
}

void vtkHM1DTreeElement::SetId(vtkIdType id)
{
  if (this->id == -1)
    this->id = id;
  else
    {
    vtkWarningMacro(<< "Element already has id!!, id: " << this->GetId() << ".");
    return;
    }
}

