/*
 * $Id:vtkHM1DTreeElement.h 109 2006-03-27 15:04:14Z diego $
 */
// .NAME vtkHM1DTreeElement - Class that represents an element in an 1D tree straight tubes tree.
// .SECTION Description
// vtkHM1DTreeElement is the abstract representation of the 1D tree Elements for a 
// strait tubes one dimensional representation of the arterial tree. Its responsible 
// for handling the information of a prticular tree element. It only provides 
// an interface for its child classes.

#ifndef _vtkHM1DTreeElement_h_
#define _vtkHM1DTreeElement_h_

#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <list> // List of child 1D tree elements

#include "vtkClientServerID.h"

class VTK_EXPORT vtkHM1DTreeElement:public vtkObject
{
public:
//BTX
  typedef std::list<vtkHM1DTreeElement *> TreeChildList;
  typedef std::list<vtkHM1DTreeElement *> TreeParentList;
//ETX
  
protected:
// Description:
// Indicates the ClientServerID from this object
  vtkClientServerID clientserverId;

  // Description:
  // Child list iterator.
//BTX
  TreeChildList::iterator childIt;
  TreeParentList::iterator parentIt;
//ETX
//  int childItId;

  // Description:
  // 1D tree element identifier.
  vtkIdType id;
  
//BTX
  // Description:
  // Child list.
  TreeChildList childs;
  TreeParentList Parents;
//ETX

  // Description:
  // Class/object name.
  const char* name;
	
	// Description:
	// Test if element is in the main tree or in a tree not connected to the
	// tree with heart
//	bool IsInMainTree;
	
public:
  vtkTypeRevisionMacro(vtkHM1DTreeElement,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  static vtkHM1DTreeElement *New();
	
	// Description:
  // Deep copy
  void DeepCopy(vtkHM1DTreeElement *);
	
  // Description:
  // Return the pointer to a child an removes it from the child list of the element.
  vtkHM1DTreeElement *PopFirstChild();
  
  // Description:
  // Return the pointer to a parent an removes it from the parent list of the element.
  vtkHM1DTreeElement *PopFirstParent();

  // Description:
  // Elimina um filho do no atual. Returns false if there was a problem.
  bool RemoveChild(vtkHM1DTreeElement *);
	
	// Description:
  // Elimina um parent do no atual. Returns false if there was a problem.
  bool RemoveParent(vtkHM1DTreeElement *);
	
  // Description:
  // Adiciona um novo filho.
  void AddChild(vtkHM1DTreeElement *);
  
  // Description:
  // Adiciona um novo parent.
  void AddParent(vtkHM1DTreeElement *);
  
  // Description:
  // Returns true if the element is leaf, meening, it has no childs.
  bool IsLeaf();
  
  // Description:
  // Returns the pointer to the child by it's id.
  vtkHM1DTreeElement *GetChild(vtkIdType);
  
  // Description:
  // Returns the pointer to the parent by it's id.
  vtkHM1DTreeElement *GetParent(vtkIdType);
  
  // Description:
  // Returns child count for this node.
  int GetChildCount();
  
  // Description:
  // Returns parent count for this node.
  int GetNumberOfParents();

  // Description:
  // Returns the pointer to the first child in its internal list and sets an 
  // interator pointing to it.
  vtkHM1DTreeElement *GetFirstChild();
  
  // Description:
  // Returns the pointer to the first parent in its internal list and sets an 
  // interator pointing to it.
  vtkHM1DTreeElement *GetFirstParent();

  // Description:
  // Returns the pointer to the next child in its internal list and advances the 
  // internal pointer, GetFirstChild() must be called first.
  vtkHM1DTreeElement *GetNextChild();
  
  // Description:
  // Returns the pointer to the next parent in its internal list and advances the 
  // internal pointer, GetFirstParent() must be called first.
  vtkHM1DTreeElement *GetNextParent();

  // Description:
  // Returns the id of this element;
  vtkIdType GetId();
  void SetId(vtkIdType);
  
  void SetName(const char* name){this->name = name;return;}
  const char* GetName(){return name;};
  
  // Description:
  // Get/Set the reference to the parent of the actual node.

  // Description:
  // This method returns the ClientServerID from the current object  
  vtkClientServerID GetClientServerID();

  // Description:
  // This method returns the integer value of the register that forms the ClientServerID object
  int GetClientServerID_int();
//BTX  
  // Description:
  // Set the value of the ClientSeverID attribute from this object
  void SetClientServerID(vtkClientServerID);
//ETX
	
	// Description:
	// Set/Get IsInMainTree
//	vtkSetMacro(IsInMainTree, bool);
//	vtkGetMacro(IsInMainTree, bool);
	
protected:

//  vtkHM1DTreeElement(vtkIdType id = -1);
  vtkHM1DTreeElement();
  ~vtkHM1DTreeElement();
};

#endif /*_vtkHM1DTreeElement_h_*/
