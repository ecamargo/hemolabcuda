
#include "vtkHM3DFullModel.h"
#include "vtkMesh3d.h"
#include "vtkSurfaceGen.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkDataArrayCollection.h"

vtkCxxRevisionMacro(vtkHM3DFullModel, "$Rev: $");
vtkStandardNewMacro(vtkHM3DFullModel);

//----------------------------------------------------------------------------
vtkHM3DFullModel::vtkHM3DFullModel()
{
}

//----------------------------------------------------------------------------
vtkHM3DFullModel::~vtkHM3DFullModel()
{ 	
}

//----------------------------------------------------------------------------

void vtkHM3DFullModel::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}


//----------------------------------------------------------------------------

void vtkHM3DFullModel::SetCoversConstantPressure(vtkDoubleArray *ConstantPressureArray)
{
	this->ConstantCoverPressureArray = ConstantPressureArray; 
}

//----------------------------------------------------------------------------

double vtkHM3DFullModel::GetCoverConstantPressure(int CoverNumber)
{
	return this->ConstantCoverPressureArray->GetValue(CoverNumber);
}



//----------------------------------------------------------------------------
void vtkHM3DFullModel::SetCoverElementVelocity(vtkDoubleArray *CoverVelocity)
{
 this->CoverVelocity = CoverVelocity;	
	
}

//----------------------------------------------------------------------------

double *vtkHM3DFullModel::GetCoverElementVelocity(int CoverNumber)
{
 return this->CoverVelocity->GetTuple3(CoverNumber);
}

//----------------------------------------------------------------------------

void vtkHM3DFullModel::SetCoverPressionSetArray(vtkIntArray *CoverPressionSetArray)
{
	this->CoverPressionSetArray = CoverPressionSetArray;
}

//----------------------------------------------------------------------------


int vtkHM3DFullModel::CoverConstantPressureIsSet(int CoverNumber)
{
	return this->CoverPressionSetArray->GetValue(CoverNumber);	
}

//----------------------------------------------------------------------------

void vtkHM3DFullModel::SetCoverElementsBCType(vtkIntArray *BCType)
{
	this->CoverElementBCType = BCType;	
}

//----------------------------------------------------------------------------

vtkIntArray *vtkHM3DFullModel::GetCoverElementsBCType()
{
	return this->CoverElementBCType;	
}
//----------------------------------------------------------------------------

void vtkHM3DFullModel::SetPressureArrayCollection(vtkDataArrayCollection *collection)
{
	this->PressureCurveCollection = collection;
}
//----------------------------------------------------------------------------

vtkDoubleArray *vtkHM3DFullModel::GetPressureCurve(int CoverNumber)
{
	return vtkDoubleArray::SafeDownCast(this->PressureCurveCollection->GetItem(CoverNumber));
}	
//----------------------------------------------------------------------------


void vtkHM3DFullModel::SetWallParamCollection(vtkDataArrayCollection *collection)
{
	this->WallParamCollection = collection;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHM3DFullModel::GetWallParameters(int group)
{
	return vtkDoubleArray::SafeDownCast(this->WallParamCollection->GetItem(group));
}	


//----------------------------------------------------------------------------

int vtkHM3DFullModel::GetNumberOfCoverGroups()
{
	return this->GetSurface()->GetNumGroups() - this->GetSurface()->GetNumberOfShells(); 
}

//----------------------------------------------------------------------------

int vtkHM3DFullModel::GetNumberOfShells()
{
	return this->GetSurface()->GetNumberOfShells();

}
//----------------------------------------------------------------------------


