// .NAME vtkHM3DFullModel - Data structure for appending mechanical and boundary conditions (used in simulation) to 3D geometric information.
// .SECTION Description
// vtkHM3DFullModel 

#ifndef _vtkHM3DFullModel_
#define _vtkHM3DFullModel_

#include "vtkPolyData.h"
#include "vtkMeshData.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"


class vtkMesh3d;
class vtkSurfaceGen;
class vtkParamMesh;
class vtkUnstructuredGrid;
class vtkDataArrayCollection;

class VTK_EXPORT vtkHM3DFullModel : public vtkMeshData
{
public:

	static vtkHM3DFullModel *New();
	vtkTypeRevisionMacro(vtkHM3DFullModel, vtkMeshData);
	
	void PrintSelf(ostream& os, vtkIndent indent);
	

	// Description:
	// Set the value of constant pressure from covers
	void SetCoversConstantPressure(vtkDoubleArray *ConstantPressureArray);

	// Description:
	// Returns the constant pressure value from some cover	(Covers number begins at ZERO)
	double GetCoverConstantPressure(int CoverNumber);
	
	// Description:
	// Set the array that describe the velocity in each cover 
	void SetCoverElementVelocity(vtkDoubleArray *CoverVelocity);

	// Description:
	// return a double array (size 3) with the values of velocity vector
	double *GetCoverElementVelocity(int CoverNumber);

	// Description:
  // just an ordinary set method
	void SetCoverPressionSetArray(vtkIntArray *CoverPressionSetArray);

	// Description:
	// returns if constant pressure is defined in a cover given by CoverNumber
	int CoverConstantPressureIsSet(int CoverNumber);
	
	// Description:
	// set the vtkIntArray object that describe the type of Boundary Condition from each cover of model
	void SetCoverElementsBCType(vtkIntArray *BCType);

	// Description:
	// array with the size of cover groups which indicates the type of boundary condition 
	// defined by the user for each cover
	// 1 - constant pressure
	// 2 - p=f(t)
	// 3 - velocity
	vtkIntArray *GetCoverElementsBCType();

	
	// Description:
	// set the collection object that describe the curve pressure associated to each cover
	void SetPressureArrayCollection(vtkDataArrayCollection *collection);
	
	// Description:
	// return a vtkDoubleArray object with the- pressure X time curve for some Cover	
	vtkDoubleArray *GetPressureCurve(int CoverNumber);
	
	// Description:
	// set the collection of wall groups 
	void SetWallParamCollection(vtkDataArrayCollection *collection);	
	
	// Description:
	// return a vtkDoubleArray object (size 5) with the wall parameters from some wall group.
	// 0 - Elastin
	// 1 - Curve Radius
	// 2 - Wall thickness
	// 2 - Reference Pressure
	// 3 - ViscoElasticity
	// Note:  first wall group starts with Zero
	vtkDoubleArray *GetWallParameters(int group);
	
	// Description:
	// return the number of covers from FullModel
	int GetNumberOfCoverGroups();
	
	// Description:
	//	return the number of wall shells from FullModel
	int GetNumberOfShells();
	
	
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

protected:
	vtkHM3DFullModel();
	virtual ~vtkHM3DFullModel();

  
 	// Description:
	// cover velocity array	
 	vtkDoubleArray *CoverVelocity;
	
	// Description:
	//	array that store the value of constant pressure for each cover
	vtkIntArray *CoverPressionSetArray;
	
	// Description:
	// array with size of number of covers that indicates the type of BC defined for each cover
	// 1 - constante pressure
	// 2 - p=f(t)
	// 3 - velocity
	vtkIntArray *CoverElementBCType;
 	
  // Description:
	// collection of vtkDoubleArrays where each one indicates the values of (Time X Pressure)
	// for the curve pressure from each cover 
	vtkDataArrayCollection *PressureCurveCollection; 
	
	// Description:
	// collection of vtkDoubleArrays that store the 5 wall parameters for the wall groups from model
	vtkDataArrayCollection *WallParamCollection;
	
	// Description:
	// vtkDoubleArrays that store the value of constant pressure for each cover
	vtkDoubleArray *ConstantCoverPressureArray;
	
	
   
private:
	vtkHM3DFullModel(const vtkHM3DFullModel&); 		// Not Implemented
	void operator=(const vtkHM3DFullModel&);	// Not Implemented
};

#endif // _vtkHM3DFullModel_
