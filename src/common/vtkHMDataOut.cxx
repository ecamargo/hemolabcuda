#include "vtkHMDataOut.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>

vtkCxxRevisionMacro(vtkHMDataOut, "$Revision: 370 $");
vtkStandardNewMacro(vtkHMDataOut);

vtkHMDataOut::vtkHMDataOut()
{
	vtkDebugMacro(<<"DataOut data...");
	this->DegreeOfFreedomArray = NULL;
	this->TimeStep = NULL;
	this->InstantOfTime = NULL;
	this->TimeStepUsed = NULL;
	this->NumberOfBlock = 0;
	this->Max = NULL;
	this->Min = NULL;
}

//----------------------------------------------------------------------------
vtkHMDataOut::~vtkHMDataOut()
{
	if (this->DegreeOfFreedomArray)
		this->DegreeOfFreedomArray->Delete();
	if (this->TimeStep)
		this->TimeStep->Delete();
	if (this->InstantOfTime)
		this->InstantOfTime->Delete();
	if (this->TimeStepUsed)
		this->TimeStepUsed->Delete();
		
	if (this->Max)
		{
		delete [] this->Max;	
		this->Max = NULL;	
		}
			
	if (this->Min)
		{
		delete [] this->Min;	
		this->Min = NULL;	
		}
}

//----------------------------------------------------------------------------
void vtkHMDataOut::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

void vtkHMDataOut::DeepCopy(vtkHMDataOut *orig)
{
	this->TimeStep = vtkIntArray::New();
	this->InstantOfTime = vtkDoubleArray::New();
	this->TimeStepUsed = vtkDoubleArray::New();

	this->TimeStep->DeepCopy(orig->GetTimeStep());
	this->InstantOfTime->DeepCopy(orig->GetInstantOfTime());
	this->TimeStepUsed->DeepCopy(orig->GetTimeStepUsed());
	this->NumberOfBlock = orig->GetNumberOfBlock();

	this->DegreeOfFreedomArray = vtkDoubleArray::New();
	this->DegreeOfFreedomArray->DeepCopy(orig->GetDegreeOfFreedomArray());
}

//----------------------------------------------------------------------------
//GetDegreeOfFreedomArray
vtkDoubleArray *vtkHMDataOut::GetDegreeOfFreedomArray()
{
	if (this->DegreeOfFreedomArray)
		return this->DegreeOfFreedomArray;
	else
		return NULL;
}

void vtkHMDataOut::SetDegreeOfFreedomArray(vtkDoubleArray *array)
{
	this->DegreeOfFreedomArray = array;
}

//----------------------------------------------------------------------------
//GetTimeStep
vtkIntArray *vtkHMDataOut::GetTimeStep()
{
	return this->TimeStep;
}

void vtkHMDataOut::SetTimeStep(vtkIntArray *t)
{
	if (t != this->TimeStep)
		{
		if (this->TimeStep)
			{
			this->TimeStep->UnRegister(this);
			}
		this->TimeStep = t;
		if (this->TimeStep)
			{
			this->TimeStep->Register(this);
			}
		this->Modified();
		}
}

//----------------------------------------------------------------------------
//GetInstantOfTime
vtkDoubleArray *vtkHMDataOut::GetInstantOfTime()
{
	return this->InstantOfTime;
}

void vtkHMDataOut::SetInstantOfTime(vtkDoubleArray *i)
{
	if (!this->InstantOfTime)
		this->InstantOfTime = vtkDoubleArray::New();
	if (i)
		this->InstantOfTime->DeepCopy(i);

	//	if ( i != this->InstantOfTime)
	//	  {
	//	  if (this->InstantOfTime)
	//	    {
	//	    this->InstantOfTime->UnRegister(this);
	//	    }
	//	  this->InstantOfTime = i;
	//	  if (this->InstantOfTime)
	//	    {
	//	    this->InstantOfTime->Register(this);
	//	    }
	//	  this->Modified();
	//	  }
}

//----------------------------------------------------------------------------
//GetTimeStepUsed
vtkDoubleArray *vtkHMDataOut::GetTimeStepUsed()
{
	return this->TimeStepUsed;
}

void vtkHMDataOut::SetTimeStepUsed(vtkDoubleArray *t)
{
	if (t != this->TimeStepUsed)
		{
		if (this->TimeStepUsed)
			{
			this->TimeStepUsed->UnRegister(this);
			}
		this->TimeStepUsed = t;
		if (this->TimeStepUsed)
			{
			this->TimeStepUsed->Register(this);
			}
		this->Modified();
		}
}

//----------------------------------------------------------------------------
//GetNumberOfBlock
int vtkHMDataOut::GetNumberOfBlock()
{
	return this->NumberOfBlock;
}

void vtkHMDataOut::SetNumberOfBlock(int n)
{
	this->NumberOfBlock = n;
}

//---------------------------------------------------------------------
void vtkHMDataOut::SetMax(double *m)
{
	this->Max = new double[this->NumberOfBlock];
	for (int i=0; i<this->NumberOfBlock; i++)
		this->Max[i] = m[i];
}

double *vtkHMDataOut::GetMax()
{
	return this->Max;
}

//---------------------------------------------------------------------
void vtkHMDataOut::SetMin(double *m)
{
	this->Min = new double[this->NumberOfBlock];
	for (int i=0; i<this->NumberOfBlock; i++)
		this->Min[i] = m[i];

}

double *vtkHMDataOut::GetMin()
{
	return this->Min;
}
