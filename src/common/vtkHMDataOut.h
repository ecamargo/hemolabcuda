// .NAME vtkHMDataOut - DataOut data file
// .SECTION Description
// vtkHMDataOut is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also

#ifndef vtkHMDataOut_H_
#define vtkHMDataOut_H_

#include "vtkDataObject.h"

#include "vtkDoubleArray.h"
#include "vtkIntArray.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;

#include <string>
	
class VTK_EXPORT vtkHMDataOut : public vtkDataObject
{
public:
	static vtkHMDataOut *New();
	vtkTypeRevisionMacro(vtkHMDataOut,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHMDataOut *orig);
	
	// Description:
	// Set/Get DegreeOfFreedomArray
	vtkDoubleArray *GetDegreeOfFreedomArray();
	void SetDegreeOfFreedomArray(vtkDoubleArray *array);
	
	// Description:
	// Set/Get TimeStep
	vtkIntArray *GetTimeStep();
	void SetTimeStep(vtkIntArray *t);
	
	// Description:
	// Set/Get InstantOfTime
	vtkDoubleArray *GetInstantOfTime();
	void SetInstantOfTime(vtkDoubleArray *i);
	
	// Description:
	// Set/Get TimeStepUsed
	vtkDoubleArray *GetTimeStepUsed();
	void SetTimeStepUsed(vtkDoubleArray *t);
	
	// Description:
	// Set/Get NumberOfBlock
	int GetNumberOfBlock();
	void SetNumberOfBlock(int n);
		
	void SetMax(double *m);
	double *GetMax();
	
	void SetMin(double *m);
	double *GetMin();
	
protected:
	vtkHMDataOut();
	~vtkHMDataOut();
	
	double *Max;
	double *Min;
	
	//Indicate the list with the results of the problem
	//for each step of calculated time
	vtkIntArray *TimeStep;  //Time step
	vtkDoubleArray *InstantOfTime;  //Instant of time
	vtkDoubleArray *TimeStepUsed;  //Time step used for the calculation
	
	//Array with data of the degrees of liberty
	//of each point of the tree
	vtkDoubleArray *DegreeOfFreedomArray;
	
	//Number of blocks of data in the DataOut.txt file
	int NumberOfBlock;
	
}; //End class

#endif /*vtkHMDataOut_H_*/
