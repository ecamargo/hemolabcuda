#include "vtkHMGeneralInfo.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkObject.h"
#include "vtkIntArray.h"
#include "vtkIdList.h"


vtkCxxRevisionMacro(vtkHMGeneralInfo, "$Revision: 311 $");
vtkStandardNewMacro(vtkHMGeneralInfo);

//----------------------------------------------------------------------------
vtkHMGeneralInfo::vtkHMGeneralInfo()
{
	vtkDebugMacro(<<"GeneralInfo...");
	this->NumberOfVolumeCells = vtkIntArray::New();
	
	this->NullElementList = NULL;
	this->NullTerminalList = NULL;
}

//----------------------------------------------------------------------------
vtkHMGeneralInfo::~vtkHMGeneralInfo()
{	
	this->NumberOfVolumeCells->Delete();
	
	if ( this->NullElementList )
		this->NullElementList->Delete();
	this->NullElementList = NULL;
	
	if ( this->NullTerminalList )
		this->NullTerminalList->Delete();
	this->NullTerminalList = NULL;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
	os << indent << "Number of 1D Cells: " << this->NumberOf1DCells << "\n";
	os << indent << "Number of element type of 1D model: " << this->NumberOfElementType << "\n";
	os << indent << "Number of 3D Objects: " << this->NumberOf3DObjects << "\n";
	os << indent << "Number of SubSteps: " << this->NumberOfSubSteps << "\n";
	os << indent << "Number of Volume Cells: " << *this->NumberOfVolumeCells << "\n";
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::DeepCopy(vtkHMGeneralInfo *orig)
{
	this->NumberOf1DCells 		= orig->GetNumberOf1DCells();
	this->NumberOfElementType = orig->GetNumberOfElementType();
	
	if ( orig->GetNullElementList() )
		{
		if ( !this->NullElementList )
			this->NullElementList = vtkIntArray::New();
		this->NullElementList->DeepCopy(orig->GetNullElementList());
		}
	else
		{
		if ( this->NullElementList )
			this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
	
	if ( orig->GetNullTerminalList() )
		{
		if ( !this->NullTerminalList )
			this->NullTerminalList = vtkIntArray::New();
		this->NullTerminalList->DeepCopy(orig->GetNullTerminalList());
		}
	else
		{
		if ( this->NullTerminalList )
			this->NullTerminalList->Delete();
		this->NullTerminalList = NULL;
		}
	
	this->NumberOf3DObjects 	= orig->GetNumberOf3DObjects();
	this->NumberOfVolumeCells->DeepCopy( orig->GetNumberOfVolumeCells() );
	this->NumberOfSubSteps 		= orig->GetNumberOfSubSteps();
	
	this->NumberOfShellCells.clear();
	  
  VectorOfIntList *v = orig->GetNumberOfShellCells();
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i < v->size(); i++ )
		{
		lst = v->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->NumberOfShellCells.push_back(lst2);
		lst2.clear();
		}
	
	this->NumberOfCapsCells.clear();
	v->clear();
	lst.clear();
	lst2.clear();
	
  v = orig->GetNumberOfCapsCells();
  	
	for (unsigned int i=0; i < v->size(); i++ )
		{
		lst = v->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->NumberOfCapsCells.push_back(lst2);
		lst2.clear();
		}	
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOf1DCells(int i)
{
	this->NumberOf1DCells = i;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOf1DCells()
{
	return this->NumberOf1DCells;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOf3DObjects(int i)
{
	this->NumberOf3DObjects = i;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOf3DObjects()
{
	return this->NumberOf3DObjects;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOfSubSteps(int i)
{
	this->NumberOfSubSteps = i;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOfSubSteps()
{
	return this->NumberOfSubSteps;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOfVolumeCells(vtkIntArray *array)
{
	this->NumberOfVolumeCells->DeepCopy(array);
}

//----------------------------------------------------------------------------
vtkIntArray* vtkHMGeneralInfo::GetNumberOfVolumeCells()
{
	return this->NumberOfVolumeCells;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOfVolumeCells(int position)
{
	return this->NumberOfVolumeCells->GetValue(position);
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOfShellCells(VectorOfIntList *array)
{
	this->NumberOfShellCells.clear();
  
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<array->size(); i++ )
		{
		lst = array->at(i);
		
		for (ir = lst.begin() ;ir!=lst.end();ir++)
			{
			lst2.push_back(*ir);
			}
		this->NumberOfShellCells.push_back(lst2);
		lst2.clear();
		}
}

//----------------------------------------------------------------------------
vtkHMGeneralInfo::VectorOfIntList* vtkHMGeneralInfo::GetNumberOfShellCells()
{
	return &this->NumberOfShellCells;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOfShellCells(int position)
{
  ListOfInt lst;
	ListOfInt::iterator ir;
	
	VectorOfIntList *v = &this->NumberOfShellCells;
	lst = v->at(position);
	int result = 0;
	
	for (ir = lst.begin(); ir != lst.end(); ir++)
		result += *ir;	

	ir = lst.begin();
	result -= *ir;
	
	return result;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNumberOfCapsCells(VectorOfIntList *array)
{
	this->NumberOfCapsCells.clear();
  
  ListOfInt lst, lst2;
	ListOfInt::iterator ir;
	
	for (unsigned int i=0; i<array->size(); i++ )
		{
		lst = array->at(i);
		
		for (ir = lst.begin(); ir != lst.end(); ir++)
			{
			lst2.push_back(*ir);
			}
		this->NumberOfCapsCells.push_back(lst2);
		lst2.clear();
		}
}

//----------------------------------------------------------------------------
vtkHMGeneralInfo::VectorOfIntList* vtkHMGeneralInfo::GetNumberOfCapsCells()
{
	return &this->NumberOfCapsCells;
}

//----------------------------------------------------------------------------
int vtkHMGeneralInfo::GetNumberOfCapsCells(int position)
{
  ListOfInt lst;
	ListOfInt::iterator ir;
	
	VectorOfIntList *v = &this->NumberOfCapsCells;
	lst = v->at(position);
	int result = 0;
	
	for (ir = lst.begin(); ir != lst.end(); ir++)
		result += *ir;	

	ir = lst.begin();
	result -= *ir;
	
	return result;
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHMGeneralInfo::GetNullElementList()
{
	return this->NullElementList;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNullElementList(vtkIntArray *list)
{
	if ( list )
		{
		if ( !this->NullElementList )
			this->NullElementList = vtkIntArray::New();
		this->NullElementList->DeepCopy(list);
		}
	else
		{
		if ( this->NullElementList )
			this->NullElementList->Delete();
		this->NullElementList = NULL;
		}
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHMGeneralInfo::GetNullTerminalList()
{
	return this->NullTerminalList;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfo::SetNullTerminalList(vtkIntArray *list)
{
	if ( list )
		{
		if ( !this->NullTerminalList )
			this->NullTerminalList = vtkIntArray::New();
		this->NullTerminalList->DeepCopy(list);
		}
	else
		{
		if ( this->NullTerminalList )
			this->NullTerminalList->Delete();
		this->NullTerminalList = NULL;
		}
}
