// .NAME vtkHMGeneralInfo - GeneralInfo data file
// .SECTION Description
// vtkHMGeneralInfo is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataObject, 
// .SECTION See Also


#ifndef VTKHMGENERALINFO_H_
#define VTKHMGENERALINFO_H_


#include "vtkDataObject.h"

class vtkIntArray;
class vtkIdList;

#include <iostream>
#include <list>
#include <vector>
	
class VTK_EXPORT vtkHMGeneralInfo : public vtkDataObject
{
public:

	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX

	static vtkHMGeneralInfo *New();
	vtkTypeRevisionMacro(vtkHMGeneralInfo,vtkDataObject);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkHMGeneralInfo *orig);
	
	// Description:
	// Set/Get Number of 1D Cells
	void SetNumberOf1DCells(int NumberOf1DCells);
	int GetNumberOf1DCells();
	
	// Description:
	//Set/Get number of element type of the 1D model.
	vtkSetMacro(NumberOfElementType, int);
	vtkGetMacro(NumberOfElementType, int);
	
	// Description:
	// Set/Get Number of 3D Structures
	void SetNumberOf3DObjects(int NumberOf3DObjects);
	int GetNumberOf3DObjects();
	
	// Description:
	// Set/Get Number of SubSteps
	void SetNumberOfSubSteps(int NumberOfSubSteps);
	int GetNumberOfSubSteps();
	
	// Description:
	// Set/Get Number of Volume cells for each object
	int GetNumberOfVolumeCells(int object);
	void SetNumberOfVolumeCells(vtkIntArray *NumberOfVolumeCells);
	vtkIntArray* GetNumberOfVolumeCells();

	//BTX
	// Description:
	// Set/Get Number of Shell cells for each object
	void SetNumberOfShellCells(VectorOfIntList *array);
	VectorOfIntList* GetNumberOfShellCells();
		
	// Description:
	// Return the number of Shell cells for one object
	int GetNumberOfShellCells(int object);
	
	// Description:
	// Set/Get Number of Caps cells for each object
	void SetNumberOfCapsCells(VectorOfIntList *array);
	VectorOfIntList* GetNumberOfCapsCells();
	
	// Description:
	// Return the number of Shell cells for one object
	int GetNumberOfCapsCells(int object);
	//ETX	
	
	// Description:
	// Set/Get null element list.
	void SetNullElementList(vtkIntArray *list);
	vtkIntArray *GetNullElementList();

	// Description:
	// Set/Get null terminal list.
	void SetNullTerminalList(vtkIntArray *list);
	vtkIntArray *GetNullTerminalList();
	
protected:
	vtkHMGeneralInfo();
	~vtkHMGeneralInfo();
	
	// Description:
	//Number of 1D cells
	int NumberOf1DCells;
	
	// Description:
	//Number of element types of the 1D model.
	int NumberOfElementType;
	
	// Descriptin:
	// List with id of the null elements 1D.
	vtkIntArray *NullElementList;

	// Descriptin:
	// List with id of the null terminals 1D.
	vtkIntArray *NullTerminalList;
	
	// Description:
	//Number of 3D objects
	int NumberOf3DObjects;
	
	// Description:
	//Number of volume elements for each 3D object
	vtkIntArray *NumberOfVolumeCells;
	
	//BTX
	// Description:
	// Vector with an list of groups number and shell elements for each 3D object
	VectorOfIntList NumberOfShellCells;
	//ETX
	
	//BTX
	// Description:
	//Number of caps e cell-caps for each 3D object
	VectorOfIntList NumberOfCapsCells;
	//ETX
	
	// Description:
	//Number of SubSteps in BasParam.txt
	int NumberOfSubSteps;
	
}; //End class

#endif /*VTKHMGENERALINFO_H_*/
