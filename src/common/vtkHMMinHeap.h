/*
 * vtkHMMinHeap.h
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */

#ifndef VTKHMMINHEAP_H_
#define VTKHMMINHEAP_H_

// .NAME vtkHMMinHeap - Implementation of the min heap data structure.
// .SECTION Description
// This class is an implementation of the min heap data structure, used to handle a set of
// values in such a way that the retrieval of the minimum element takes constant time.
// A min heap is a complete binary tree where the value at each node is equal or less
// than the value at its children, and it is represented as an array where the children
// of a node stored at location k are at location 2k and 2k+1 (so that the parent of k is located at k/2).
// Keeping the min heap ordered after a value is updated or an id is inserted in teh heap takes O(log N).
//
// In the present implementation, values are provided in a vtkDoubleArray, and element ids are
// inserted in the heap. Backpointers are used to access the heap by id. This class is optimized
// for working in conjunction with vtkNonManifoldFastMarching.
//
// For more insight see J.A. Sethian, Level Set Methods and Fast Marching Methods, Cambridge University Press, 2nd Edition, 1999.
// .SECTION Caveats
// Be sure to call Initialize() after defining MinHeapScalars.


#include "vtkObject.h"
#include "vtkDoubleArray.h"
//#include "vtkvmtkComputationalGeometryWin32Header.h"
//#include "vtkvmtkWin32Header.h"

#ifndef DOUBLE_TOL
#define DOUBLE_TOL 1.0E-12
#endif

class VTK_EXPORT vtkHMMinHeap : public vtkObject
{
  public:
  vtkTypeRevisionMacro(vtkHMMinHeap,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMMinHeap *New();

  // Description:
  // Initializes the heap to an empty state and prepares back pointers. Calls this method before using the min heap once MinHeapScalars have been defined.
  void Initialize();

  // Description:
  // Set/Get the array containing the values indexed by the min heap.
  vtkSetObjectMacro(MinHeapScalars,vtkDoubleArray);
  vtkGetObjectMacro(MinHeapScalars,vtkDoubleArray);

  // Description:
  // Get heap size.
  int GetSize();

  // Description:
  // Insert an index to a value in HeapScalars in the min heap.
  void InsertNextId(vtkIdType id);

  // Description:
  // Tells the min heap that the value indexed by id has changed in MinHeapScalars array.
  void UpdateId(vtkIdType id);

  // Description:
  // Gets the id of the minimum value in the min heap.
  vtkIdType GetMin();

  // Description:
  // Gets the id of the minimum value in the min heap and removes it from the min heap.
  vtkIdType RemoveMin();

  protected:
  vtkHMMinHeap();
  ~vtkHMMinHeap();

  void Swap(vtkIdType loc0, vtkIdType loc1);
  int IsLeaf(vtkIdType loc);
  vtkIdType GetLeftChild(vtkIdType loc);
  vtkIdType GetRightChild(vtkIdType loc);
  vtkIdType GetParent(vtkIdType loc);
  void SiftUp(vtkIdType loc);
  void SiftDown(vtkIdType loc);
  vtkIdType RemoveAt(vtkIdType loc);

  vtkIdList* Heap;
  vtkIdList* BackPointers;

  vtkDoubleArray* MinHeapScalars;

  private:
  vtkHMMinHeap(const vtkHMMinHeap&);  // Not implemented.
  void operator=(const vtkHMMinHeap&);  // Not implemented.
};

#endif /* VTKHMMINHEAP_H_ */
