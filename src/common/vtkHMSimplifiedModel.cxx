/*
 * $Id:vtkHMSimplifiedModel.cxx 109 2006-03-27 15:04:14Z diego $
 */

 
#include "vtkHMSimplifiedModel.h"

vtkCxxRevisionMacro(vtkHMSimplifiedModel, "$Rev: 286 $");
vtkStandardNewMacro(vtkHMSimplifiedModel);

void vtkHMSimplifiedModel::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  const char* name = this->GetName();
  if (name)
    {
    os << indent << "Name: " << name << "\n";
    }
  else
    {
    os << indent << "Name: (none)\n";
    }
}

vtkHMSimplifiedModel::vtkHMSimplifiedModel()
{
}

vtkHMSimplifiedModel::~vtkHMSimplifiedModel()
{
}
