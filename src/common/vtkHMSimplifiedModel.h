/*
 * $Id:vtkHMSimplifiedModel.h 109 2006-03-27 15:04:14Z diego $
 */
// .NAME vtkHMSimplifiedModel - Abstract class that represents simplified models.
// .SECTION Description
// vtkHMSimplifiedModel is the abstract class that represents different simplified models of the HCVS.

#ifndef _vtkHMSimplifiedModel_h_
#define _vtkHMSimplifiedModel_h_


#include <vtkUnstructuredGridSource.h>
#include <vtkObjectFactory.h>

class VTK_EXPORT vtkHMSimplifiedModel:public vtkUnstructuredGridSource
{
public:
  static vtkHMSimplifiedModel *New();
  vtkTypeRevisionMacro(vtkHMSimplifiedModel,vtkSource);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void SetName(const char* name){this->name = name;return;}
  const char* GetName(){return name;};

protected:
	vtkHMSimplifiedModel();
	virtual ~vtkHMSimplifiedModel();

  // Description:
  // Class/object name.
  const char* name;
};

#endif /*_vtkHMSimplifiedModel_h_*/
