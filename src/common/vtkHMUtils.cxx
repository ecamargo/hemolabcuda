#include "vtkHMUtils.h"
#include "vtkObject.h"
#include "vtkObjectFactory.h"

//#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

vtkCxxRevisionMacro(vtkHMUtils, "$Revision: 1.97 $");
vtkStandardNewMacro(vtkHMUtils);

/////////////////////////ConvertIdListToChar////////////////////////////////////

std::string vtkHMUtils::ConvertIdListToChar(vtkIdList *IdList)
{
	if ( !IdList )
		return NULL;

	//const char *listChar = new char[IdList->GetNumberOfIds()*2];
	
	string str;
	for ( int i=0; i<IdList->GetNumberOfIds(); i++ )
		{
		char temp[20];
		sprintf(temp, "%d;", IdList->GetId(i));
		str.append(temp);
		}
	return str;
}

///////////////////////ConvertCharToIdList//////////////////////////////////////

vtkIdList* vtkHMUtils::ConvertCharToIdList(char *String)
{
	if ( !String )
		return NULL;

	int LastPosition =0, CurrentPosition=0;
	char temp[20];
	// resetando a string auxiliar ************
  for (int i=0; i < 20 ;i++)
  	  temp[i]=0;
  //*****************************************
	vtkIdList *elemList = vtkIdList::New();
  while (String[CurrentPosition])
 		{
 	  if (String[CurrentPosition] == ';')
 	  	{
 	  	memcpy(temp, String+LastPosition, CurrentPosition-LastPosition);
 	  	
 	  	elemList->InsertNextId(atoi (temp));
 	  	LastPosition =CurrentPosition+1;
 	  	
 	  	//***************************
 	  	for (int i=0; i < 20 ;i++)
 				temp[i]=0;
 			//***************************
 	  	}
 	  CurrentPosition++;
 		}
  return elemList;  
}

////////////////////////ConvertDoubleArrayToChar////////////////////////////////

std::string vtkHMUtils::ConvertDoubleArrayToChar(vtkDoubleArray *Array)
{
//A primeira posição da string armazena o número de componentes do DoubleArray
	if ( !Array )
		return NULL;
	
	int components = Array->GetNumberOfComponents();
	int tuples = Array->GetNumberOfTuples();
	
	string str;

	char t[5];
	sprintf(t, "%d;", components);
	str.append(t);
	for ( int i=0; i<tuples; i++ )
		{
		for ( int j=0; j<components; j++ )
			{
			char temp[20];
			sprintf(temp, "%f;", Array->GetComponent(i,j));
			str.append(temp);
			}
		}
	return str;
}

////////////////////////////ConvertCharToDoubleArray////////////////////////////

vtkDoubleArray* vtkHMUtils::ConvertCharToDoubleArray(char *String)
{
//A primeira posição da string armazena o número de componentes do DoubleArray
	if ( !String  || !strlen (String))
		return NULL;
	
	int components;
		
	int LastPosition =0, CurrentPosition=0;
	char temp[20];

	// resetando a string auxiliar ************
  for (int i=0; i < 20 ;i++)
  	  temp[i]=0;
  //*****************************************

  while (String[CurrentPosition] != ';')
 		{
  	temp[CurrentPosition]=String[CurrentPosition];
  	CurrentPosition++;
  	}
  components=atoi(temp);
  LastPosition=++CurrentPosition;

	// resetando a string auxiliar ************
  for (int i=0; i < 20 ;i++)
  	  temp[i]=0;
  //*****************************************

  vtkDoubleArray *Array = vtkDoubleArray::New();
  Array->SetNumberOfComponents(components);
  double* ArrayAux= new double[components];
  
	int cont=0;
  while (String[CurrentPosition])
 		{
 	  if (String[CurrentPosition] == ';')
 	  	{
 	  	memcpy(temp, String+LastPosition, CurrentPosition-LastPosition);
 	  	
 	  	ArrayAux[cont] = atof(temp);
 	  	LastPosition =CurrentPosition+1;
 	  	
 	  	//***************************
 	  	for (int i=0; i < 20 ;i++)
 				temp[i]=0;
 			//***************************
 	  	cont++;
 	  	}
 	  
 	  if ( cont == components )
 	  	{
 	  	Array->InsertNextTuple( ArrayAux );
 	  	cont = 0;
 	  	}
 	  CurrentPosition++;
 		}
  return Array;
}

////////////////////////////////////////////////////////////////////////////////

std::string vtkHMUtils::ConvertFloatArrayToChar(vtkFloatArray *Array)
{
//A primeira posição da string armazena o número de componentes do FloatArray
	if ( !Array )
		return NULL;
	
	int components = Array->GetNumberOfComponents();
	int tuples = Array->GetNumberOfTuples();
	
	string str;

	char t[5];
	sprintf(t, "%d;", components);
	str.append(t);
	for ( int i=0; i<tuples; i++ )
		{
		for ( int j=0; j<components; j++ )
			{
			char temp[20];
			sprintf(temp, "%f;", Array->GetComponent(i,j));
			str.append(temp);
			}
		}
	return str;
}

//////////////////////////////////////////////////////////////////////////////////

vtkFloatArray* vtkHMUtils::ConvertCharToFloatArray(char *String)
{
//A primeira posição da string armazena o número de componentes do FloatArray
	if ( !String )
		return NULL;
	
	int components;
		
	int LastPosition =0, CurrentPosition=0;
	char temp[20];

	// resetando a string auxiliar ************
  for (int i=0; i < 20 ;i++)
  	  temp[i]=0;
  //*****************************************

  while (String[CurrentPosition] != ';')
 		{
  	temp[CurrentPosition]=String[CurrentPosition];
  	CurrentPosition++;
  	}
  components=atoi(temp);
  LastPosition=++CurrentPosition;

	// resetando a string auxiliar ************
  for (int i=0; i < 20 ;i++)
  	  temp[i]=0;
  //*****************************************

  vtkFloatArray *Array = vtkFloatArray::New();
  Array->SetNumberOfComponents(components);
  float* ArrayAux= new float[components];
  
	int cont=0;
  while (String[CurrentPosition])
 		{
 	  if (String[CurrentPosition] == ';')
 	  	{
 	  	memcpy(temp, String+LastPosition, CurrentPosition-LastPosition);
 	  	
 	  	ArrayAux[cont] = atof(temp);
 	  	LastPosition =CurrentPosition+1;
 	  	
 	  	//***************************
 	  	for (int i=0; i < 20 ;i++)
 				temp[i]=0;
 			//***************************
 	  	cont++;
 	  	}
 	  
 	  if ( cont == components )
 	  	{
 	  	Array->InsertNextTuple( ArrayAux );
 	  	cont = 0;
 	  	}
 	  CurrentPosition++;
 		}
  return Array;
}

////////////////////////////////////////////////////////////////////////////////

std::string vtkHMUtils::ConvertIntArrayToChar(vtkIntArray *Array)
{
//A primeira posição da string armazena o número de componentes do IntArray
if ( !Array )
	return NULL;

int components = Array->GetNumberOfComponents();
int tuples = Array->GetNumberOfTuples();

string str;

char t[20];
sprintf(t, "%d;", components);
str.append(t);
for ( int i=0; i<tuples; i++ )
	{
	for ( int j=0; j<components; j++ )
		{
		char temp[20];
		sprintf(temp, "%d;", Array->GetComponent(i,j));
		str.append(temp);
		}
	}
return str;
}

////////////////////////////////////////////////////////////////////////////////

vtkIntArray* vtkHMUtils::ConvertCharToIntArray(char *String)
{
//A primeira posição da string armazena o número de componentes do IntArray
if ( !String )
	return NULL;

int components;
	
int LastPosition =0, CurrentPosition=0;
char temp[20];

// resetando a string auxiliar ************
for (int i=0; i < 20 ;i++)
	  temp[i]=0;
//*****************************************

while (String[CurrentPosition] != ';')
		{
	temp[CurrentPosition]=String[CurrentPosition];
	CurrentPosition++;
	}
components=atoi(temp);
LastPosition=++CurrentPosition;

// resetando a string auxiliar ************
for (int i=0; i < 20 ;i++)
	  temp[i]=0;
//*****************************************

vtkIntArray *Array = vtkIntArray::New();
Array->SetNumberOfComponents(components);
int* ArrayAux= new int[components];

int cont=0;
while (String[CurrentPosition])
		{
	  if (String[CurrentPosition] == ';')
	  	{
	  	memcpy(temp, String+LastPosition, CurrentPosition-LastPosition);
	  	
	  	ArrayAux[cont] = atoi(temp);
	  	LastPosition =CurrentPosition+1;
	  	
	  	//***************************
	  	for (int i=0; i < 20 ;i++)
				temp[i]=0;
			//***************************
	  	cont++;
	  	}
	  
	  if ( cont == components )
	  	{
	  	Array->InsertNextTupleValue( ArrayAux );
	  	cont = 0;
	  	}
	  CurrentPosition++;
		}
return Array;
}
