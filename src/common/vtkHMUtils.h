// .NAME vtkHMUtils - performs utils operations
// .SECTION Description
// VtkVtkHMUtils provides methods for the conversion of the types vtkDoubleArray, vtkIntArray vtkFloatArray,
// for the char type.
// Similarly, converts the char type for the vtkDoubleArray, vtkIntArray and vtkFloatArray types.

#ifndef VTKHMUTILS_
#define VTKHMUTILS_

#include <string>

using namespace std;

#include "vtkIdList.h"
#include "vtkObject.h"
#include "vtkDataArray.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkFloatArray.h"




/**************************************************************************************/
/* Structs - METACELLS*/
/**************************************************************************************/

/// Estrutura do ponto
typedef struct spoint
{
  /// Identificador do ponto
  int pointId;
  /// Coordenadas
  float x, y, z;
  /// Vetor Velocidade
  float velocityX;
  float velocityY;
  float velocityZ;
  /// Pressão
  float pressure;
  /// Tempo. Usado para armazenar quanto tempo uma partícula permance no interior de uma estrutura
  float time;
} point;



/// Estrutura do célula (tetraedro)
typedef struct scell
{
  /// Identificador da célula
  int cellId;
  /// Índice dos vértices. Cada posição irá armazenar um pointId da struct spoint
  int v[4];
  /// Centro da célula
  point center;
} cell;



/// Estrutura auxiliar para ajudar na ordenação de uma lista de células
typedef struct ssort
{
  /// Identificador de uma célula
  int cellId;
  /// Coordenda x, y ou z de um vértice
  float vertex;
} HMsort;



/**************************************************************************************/
/* Structs - PARTICLES*/
/**************************************************************************************/
typedef struct sMetaCell
{
    int numPoints;
    int numCells;
    float bounds[6];
    point *pointList;   // Point List
    cell  *cellList;    // Cell List

    point *vecList;
    point *vecListSearch;
} MetaCell;


/// Estrutura da meta-célula
typedef struct sgpuMetaCell
{
  /// Número de pontos que compõe a meta-célula
  int numPoints;
  /// Número de células que compõe a meta-célula
  int numCells;
  /// Limites da meta-célula (Xmin, Xmax, Ymin, Ymax, Zmin, Zmax)
  float bounds[6];
} gpuMetaCell;

/**************************************************************************************/





class VTK_EXPORT vtkHMUtils : public vtkObject
{
public:
  static vtkHMUtils *New();
  vtkTypeRevisionMacro(vtkHMUtils,vtkObject);
//  void PrintSelf(ostream& os, vtkIndent indent);

  ////////////////////////////IdList////////////////////////////
  
  //Description:
  //Convert vtkIdList to char
  static string ConvertIdListToChar(vtkIdList *IdList);
  
  //Description:
  //Convert char to vtkIdList
  static vtkIdList* ConvertCharToIdList(char *String);
 
  ////////////////////////////DoubleArray////////////////////////////
  
  //Description:
  //Convert vtkDoubleArray to char. In the first position the String, is stored the number of components the DoubleArray
  static string ConvertDoubleArrayToChar(vtkDoubleArray *Array);
  
  //Description:
  //Convert char to vtkDoubleArray. In the first position the String, is stored the number of components the DoubleArray
  static vtkDoubleArray* ConvertCharToDoubleArray(char *String);
 
//  ////////////////////////////FloatArray////////////////////////////
  
  //Description:
  //Convert vtkFloatArray to char. In the first position the String, is stored the number of components the FloatArray
  static string ConvertFloatArrayToChar(vtkFloatArray *Array);
  
  //Description:
  //Convert char to vtkFloatArray. In the first position the String, is stored the number of components the FloatArray
  static vtkFloatArray* ConvertCharToFloatArray(char *String);
  
  ////////////////////////////IntArray////////////////////////////  
  
  //Description:
  //Convert vtkIntArray to char. In the first position the String, is stored the number of components the IntArray
  static string ConvertIntArrayToChar(vtkIntArray *Array);
  
  //Description:
  //Convert char to vtkIntArray. In the first position the String, is stored the number of components the IntArray
  static vtkIntArray* ConvertCharToIntArray(char *String);

protected:
  vtkHMUtils() {};
  ~vtkHMUtils() {};

private:
  vtkHMUtils(const vtkHMUtils&);  // Not implemented.
  void operator=(const vtkHMUtils&);  // Not implemented.
};

#endif /*VTKHMUTILS_*/
