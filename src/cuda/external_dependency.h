#ifndef EXTERNDEPENDENCY__H
#define EXTERNDEPENDENCY__H

typedef unsigned int Size; 


/// Informa o erro, se houver, da chamada CUDA imediatamente anterior
#define CHECK_CUDA_ERROR() \
  { \
    cudaThreadSynchronize(); \
    cudaError_t error = cudaGetLastError(); \
    if(error != cudaSuccess) { \
      printf("error (%s: line %d): %s\n", __FILE__, __LINE__, cudaGetErrorString(error)); \
      return 1; \
    } \
  }
#endif


