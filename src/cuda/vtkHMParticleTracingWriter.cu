/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMParticleTracingWriter.cu
  Created on: 07/06/2010

  Author: Eduardo Camargo

=========================================================================*/
/// Foram reunidos neste arquivo todos os métodos necessários para o cálculo de trajetória de partículas usando GPU (CUDA)


#include "vtkHMUtils.h" // Adiciona as estruturas usadas
#include <external_dependency.h>
#include <iostream>



// -----------------------------------------------------------------------------
#if vtkHMParticleTracingWriter_EXPORTS
#  if defined( _WIN32 ) || defined( _WIN64 )
#    define vtkHMParticleTracingWriter_API __declspec(dllexport)
#  else
#    define vtkHMParticleTracingWriter_API
#  endif
#else
#  define vtkHMParticleTracingWriter_API
#endif



/// Vetor que contém as trajetórias de todas as partículas
__device__ point *ParticlePath;

/// Vetor que contém os valores de velocidade, pressão e tempo para o passo corrente
__device__ point *VecListSearch;

/// Meta-células
__device__ gpuMetaCell *MetaCells;

/// Quantidade de partículas
__device__ uint numParticles;

/// Passo de tempo corrente
__device__ uint actualtStep;

/// Maior distância entre 2 pontos da estrutura.
__device__ float maxDistance;

/// Duração de cada passo (instante de tempo) da simulação
__device__ float delta;

/// Vetor que contém todas as células
__device__ cell *fullCells;

/// Vetor que contém todos os pontos
__device__ point *fullPoints;

///
__device__ uint cycleId;





/// Encontra células próximas da partícula
__device__ void findCloserCells(uint metaNumber);

/// Usa a distância Euclidiana para verificar se a partícula esta próxima da célula informada.
__device__ int getCloserCells(uint currentCell);

/// Usa a distância Euclidiana para verificar se a distância entre as coordenadas informadas e a partícula corrente é menor do que "distPart".
__device__ int checkMinimumDistance(float x, float y,float z);

/// Verifica se a partícula esta dentro da célula informada
__device__ int isInsideCell(uint currentCell, float *a1, float *a2, float *a3, float *a4);

/// Configura os valores dos vértices da célula informada
__device__ void setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, uint currentCell);

/// Configura vetores
__device__ void setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4);

/// Calcula a diferença (subtração) entre dois pontos (pa e pb).
__device__ void computeVectorOperationSub(point *v, point pa, point pb);

/// Calcula os pontos 'n' para verificar se a partícula esta dentro ou fora de uma célula
__device__ void setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4);

/// Calcula o produto vetorial entre va e vb
__device__ void computeCrossProd(point *v, point va, point vb);

/// Calcula o produto escalar entre va e vb
__device__ float computeScalarProd(point va, point vb);

/// Calcula os valores alpha para verificar se a partícula esta dentro ou fora de uma célula
__device__ void setAlphaValues(float *alpha, point n, point va, point vb);

/// Calcula a próxima coordenada, velocidad, pressão e tempo da partícula.
__device__ void computeVelocity(uint currentCell, float a1, float a2, float a3, float a4);

/// Índice (vetor de trajetórias) da partícula no passo corrente. Leva em consideração as estruturas de dados mais os conceitos de blocos e threads.
__device__ uint getCurrentPartId();

/// Índice (vetor de trajetórias) da partícula no próximo passo. Leva em consideração as estruturas de dados mais os conceitos de blocos e threads.
__device__ uint getPartIdInNextStep();

/// Índice (vetor de trajetórias) da partícula no passo anterior. Leva em consideração as estruturas de dados mais os conceitos de blocos e threads.
__device__ uint getPartIdInLastStep();

/// Id da célula onde a meta-célula inicia
__device__ uint getInitialCell(uint metaNumber);

/// Inicializa as posições das partículas com as últimas posições do ciclo anterior
__device__ void initializeNextCycle();

/// Calcula a trajetória da partícula
__global__ void computeTraceCUDA(point *Device_ParticlePath, gpuMetaCell *Device_metaCells, point *Device_vecListSearch,
                                 uint nParticles, uint currentStep, float maxDistance, float delta, cell *Device_fullCells,
                                 point *Device_fullPoints, uint cycle_Id, uint startCycle);


/// Configura e dispara as threads em GPU
//----------------------------------------------------------------------------
void ExecuteKernel_DispatcherThreadsGPU(point *Device_ParticlePath,
                                        gpuMetaCell *Device_metaCells,
                                        point *Device_vecListSearch,
                                        uint nParticles,
                                        uint currentStep,
                                        float distance,
                                        float d,
                                        cell *Device_fullCells,
                                        point *Device_fullPoints,
                                        uint cycle_Id,
                                        uint startCycle,
                                        uint nblocks)
{
  uint max_threads_por_bloco, blocos;

  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, 0);


  // Esta função retorna 9999 para ambos os campos "major" e "minor", caso não exista um device que suporte CUDA
  if (deviceProp.major == 9999 && deviceProp.minor == 9999)
    max_threads_por_bloco = 64;
  else
    max_threads_por_bloco = 192;



  // Se o resto for diferente de zero tenho partículas não alocadas em um bloco então
  // devo adicionar mais 1 bloco para processar as partículas restantes.
  blocos = nParticles / max_threads_por_bloco + (nParticles % max_threads_por_bloco == 0 ? 0 : 1);


  dim3 blocksize(max_threads_por_bloco, 1, 1);
  dim3 gridsize(blocos, 1, 1);


  computeTraceCUDA <<< gridsize, blocksize >>> (Device_ParticlePath, Device_metaCells, Device_vecListSearch,
                                                nParticles, currentStep, distance,
                                                d, Device_fullCells, Device_fullPoints, cycle_Id, startCycle);

}


//----------------------------------------------------------------------------
__global__ void computeTraceCUDA(point *Device_ParticlePath,
                                 gpuMetaCell *Device_metaCells,
                                 point *Device_vecListSearch,
                                 uint nParticles,
                                 uint currentStep,
                                 float distance,
                                 float d,
                                 cell *Device_fullCells,
                                 point *Device_fullPoints,
                                 uint cycle_Id,
                                 uint startCycle)
{
  int inside = -1;
  float xmin, xmax, ymin, ymax, zmin, zmax;


  // Setando variáveis globais
  ParticlePath = Device_ParticlePath;
  VecListSearch = Device_vecListSearch;
  MetaCells = Device_metaCells;
  numParticles = nParticles;
  actualtStep = currentStep;
  maxDistance = distance;
  delta = d;
  fullCells = Device_fullCells;
  fullPoints = Device_fullPoints;
  cycleId = cycle_Id;



  // Evita que uma thread inválida inicie a execução
  // Caso a dimensão do bloco em x (threadIdx.x) seja maior que o número de partículas, podem existir threads inválidas.
  if( threadIdx.x < nParticles )
    {
    // Se for o início de um novo ciclo as últimas posições das partículas serão usadas para inicializar o novo ciclo.
    if( startCycle == 1 )
      initializeNextCycle();


    uint partId = getCurrentPartId();
    point p = ParticlePath[ partId ];


    for( uint metaNumber = 1; metaNumber <= 8; metaNumber++)
      {
      xmin = MetaCells[ metaNumber ].bounds[0];
      xmax = MetaCells[ metaNumber ].bounds[1];
      ymin = MetaCells[ metaNumber ].bounds[2];
      ymax = MetaCells[ metaNumber ].bounds[3];
      zmin = MetaCells[ metaNumber ].bounds[4];
      zmax = MetaCells[ metaNumber ].bounds[5];


  //    printf("Metacell %d >>>> %d %d\n", i, Device_metaCells[i].numPoints, Device_metaCells[i].numCells);
  //    printf("Metacell %d > %f %f %f %f %f %f\n", i, xmin, xmax, ymin, ymax, zmin, zmax);
  //    printf("Coordinates > %f %f %f\n", p.x, p.y, p.z);



      if((p.x >= xmin && p.x <= xmax) && (p.y >= ymin && p.y <= ymax) && (p.z >= zmin && p.z <= zmax))
        inside = 1; // Particle is inside of metacell
      else
        inside = 0; // Particle is outside of metacell


      if(inside)
        {
  //      printf("Particle %d is inside of metacell %d.\n", partId, i);
        findCloserCells( metaNumber );
        }
      }
    }
}


//----------------------------------------------------------------------------
__device__ void findCloserCells( uint metaNumber )
{
  float a1, a2, a3, a4; // Alfas globais para habilitar a interpolação


  uint numCells = MetaCells[ metaNumber ].numCells;


  uint initialCell = getInitialCell( metaNumber );
  uint finalCell = initialCell + numCells;



  // Finding an inside point
  for(uint i = initialCell; i < finalCell; i++)
    {
    if( getCloserCells(i) )
      {
      if(isInsideCell( i, &a1, &a2, &a3, &a4))
        {
        computeVelocity( i, a1, a2, a3, a4);
        break; // If a cell was found, end the search
        }
      }
    }
}


//----------------------------------------------------------------------------
__device__ int getCloserCells( uint cellId )
{

//  printf("getCloserCells.\n");


  float x = 0.0;
  float y = 0.0;
  float z = 0.0;

  int res[4] = {-1, -1, -1, -1};   // load the result for each vertex


  for(uint i = 0; i <=3; i++)
    {
    if( fullPoints[ fullCells[ cellId ].v[i] ].pointId )
      {
      x = fullPoints[ fullCells[ cellId ].v[i] ].x;
      y = fullPoints[ fullCells[ cellId ].v[i] ].y;
      z = fullPoints[ fullCells[ cellId ].v[i] ].z;

      res[i] = checkMinimumDistance( x, y, z );
      }
    else
      {
//      printf("\n\nCheck getCloserCells Function!!!\n\n");
//      system("PAUSE");
//      exit(1);
      }
    }
  if( res[0] && res[1] && res[2] && res[3])
    return 1;
  else
    return 0;
}


//----------------------------------------------------------------------------
__device__ int checkMinimumDistance(float x, float y,float z )
{
//  printf("checkMinimumDistance.\n");


  uint partId = getCurrentPartId();
  point currentParticle = ParticlePath[ partId ];

  float d;

  d = float(pow(x - currentParticle.x,2) + pow(y - currentParticle.y,2) + pow(z - currentParticle.z,2) - pow(maxDistance,2));
  if(d <= 0.0)
    return 1;
  else
    return 0;
}


//----------------------------------------------------------------------------
__device__ int isInsideCell( uint currentCell, float *a1, float *a2, float *a3, float *a4)
{
//  printf("isInsideCell.\n");


  point v1, v2, v3, v4;
  point n1, n2, n3;
  point p0, p1, p2, p3, p4;

  setPoints(&p0, &p1, &p2, &p3, &p4, currentCell);
  setVector(&v1, &v2, &v3, &v4, p0, p1, p2, p3, p4);
  setNValues(&n1, &n2, &n3, v2, v3, v4);

  setAlphaValues(a4, n1, v1, v4);
  setAlphaValues(a3, n2, v1, v3);
  setAlphaValues(a2, n3, v1, v2);

  *a1 = 1 - *a2 - *a3 - *a4;

  if((*a1>=0  && *a1<=1) && (*a2>=0  && *a2<=1) && (*a3>=0  && *a3<=1) && (*a4>=0  && *a4<=1))
    return 1; // Point is inside the cell 'c'
  else
    return 0; // Point is outside the cell 'c'
}


//----------------------------------------------------------------------------
__device__ void setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, uint currentCell)
{
//  printf("setPoints.\n");

  uint partId = getCurrentPartId();

  *p0 = ParticlePath[ partId ];;


  *p1 = fullPoints[ fullCells[ currentCell ].v[0] ];
  *p2 = fullPoints[ fullCells[ currentCell ].v[1] ];
  *p3 = fullPoints[ fullCells[ currentCell ].v[2] ];
  *p4 = fullPoints[ fullCells[ currentCell ].v[3] ];
}

//----------------------------------------------------------------------------
__device__ void setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4)
{
//  printf("setVector.\n");


  computeVectorOperationSub(v1, p0, p1);
  computeVectorOperationSub(v2, p2, p1);
  computeVectorOperationSub(v3, p3, p1);
  computeVectorOperationSub(v4, p4, p1);
}


//----------------------------------------------------------------------------
__device__ void computeVectorOperationSub(point *v, point pa, point pb)
{
//  printf("setVector.\n");


  (*v).x = pa.x - pb.x;
  (*v).y = pa.y - pb.y;
  (*v).z = pa.z - pb.z;
}


//----------------------------------------------------------------------------
__device__ void computeCrossProd(point *v, point va, point vb)
{
//  printf("computeCrossProd.\n");


  (*v).x = (va.y * vb.z) - (va.z * vb.y);
  (*v).y = (va.z * vb.x) - (va.x * vb.z);
  (*v).z = (va.x * vb.y) - (va.y * vb.x);
}


//----------------------------------------------------------------------------
__device__ void setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4)
{
//  printf("setNValues.\n");


  computeCrossProd(n1, v2, v3);
  computeCrossProd(n2, v2, v4);
  computeCrossProd(n3, v3, v4);
}


//----------------------------------------------------------------------------
__device__ float computeScalarProd(point va, point vb)
{
//  printf("computeScalarProd.\n");


  return float(va.x * vb.x + va.y * vb.y + va.z * vb.z);
}


//----------------------------------------------------------------------------
__device__ void setAlphaValues(float *alpha, point n, point va, point vb)
{
//  printf("setAlphaValues.\n");


  float val1, val2;
  val1 = float(computeScalarProd(n, va));
  val2 = float(computeScalarProd(n, vb));

  if(val2)
    *alpha = float(val1 / val2);
  else
    {
//    printf("\nCheck 'setAlphaValues' function!!!\n");
//    system("PAUSE");
    *alpha = 0;
    }

}


//----------------------------------------------------------------------------
__device__ void computeVelocity(uint currentCell, float a1, float a2, float a3, float a4)
{
//  printf("computeVelocity.\n");


  uint actual_Id = getCurrentPartId();
  uint next_Id = getPartIdInNextStep();

  point actual_part = ParticlePath[ actual_Id ];

  uint v0 = fullCells[ currentCell ].v[0];
  uint v1 = fullCells[ currentCell ].v[1];
  uint v2 = fullCells[ currentCell ].v[2];
  uint v3 = fullCells[ currentCell ].v[3];


  //-----------------------------------------------------------------------------------
  // Computes the new position
  ParticlePath[ next_Id ].pointId = 1; // Setting computed particle

  ParticlePath[ next_Id ].x = actual_part.x + delta * (a1 * VecListSearch[ v0 ].velocityX +
                                                       a2 * VecListSearch[ v1 ].velocityX +
                                                       a3 * VecListSearch[ v2 ].velocityX +
                                                       a4 * VecListSearch[ v3 ].velocityX);

  ParticlePath[ next_Id ].y = actual_part.y + delta * (a1 * VecListSearch[ v0 ].velocityY +
                                                       a2 * VecListSearch[ v1 ].velocityY +
                                                       a3 * VecListSearch[ v2 ].velocityY +
                                                       a4 * VecListSearch[ v3 ].velocityY);

  ParticlePath[ next_Id ].z = actual_part.z + delta * (a1 * VecListSearch[ v0 ].velocityZ +
                                                       a2 * VecListSearch[ v1 ].velocityZ +
                                                       a3 * VecListSearch[ v2 ].velocityZ +
                                                       a4 * VecListSearch[ v3 ].velocityZ);


  //-----------------------------------------------------------------------------------
  // Computes the new velocity
  ParticlePath[ next_Id ].velocityX = (a1 * VecListSearch[ v0 ].velocityX +
                                       a2 * VecListSearch[ v1 ].velocityX +
                                       a3 * VecListSearch[ v2 ].velocityX +
                                       a4 * VecListSearch[ v3 ].velocityX);

  ParticlePath[ next_Id ].velocityY = (a1 * VecListSearch[ v0 ].velocityY +
                                       a2 * VecListSearch[ v1 ].velocityY +
                                       a3 * VecListSearch[ v2 ].velocityY +
                                       a4 * VecListSearch[ v3 ].velocityY);

  ParticlePath[ next_Id ].velocityZ = (a1 * VecListSearch[ v0 ].velocityZ +
                                       a2 * VecListSearch[ v1 ].velocityZ +
                                       a3 * VecListSearch[ v2 ].velocityZ +
                                       a4 * VecListSearch[ v3 ].velocityZ);



  //-----------------------------------------------------------------------------------
  // Computes the new pressure and time
  ParticlePath[ next_Id ].pressure = ( VecListSearch[ v0 ].pressure +
                                       VecListSearch[ v1 ].pressure +
                                       VecListSearch[ v2 ].pressure +
                                       VecListSearch[ v3 ].pressure) / 4 ;


  ParticlePath[ next_Id ].time = actual_part.time + delta;
}


//----------------------------------------------------------------------------
__device__ uint getCurrentPartId()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + (actualtStep * numParticles);

  return partId;
}


//----------------------------------------------------------------------------
__device__ uint getPartIdInNextStep()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + ((actualtStep+1) * numParticles);

  return partId;
}

//----------------------------------------------------------------------------
__device__ uint getPartIdInLastStep()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + ((actualtStep-1) * numParticles);

  return partId;
}

//----------------------------------------------------------------------------
__device__ uint getInitialCell(uint metaNumber)
{
  uint cellId = 0;

  for(uint i=1; i<metaNumber; i++)
    cellId += MetaCells[ metaNumber ].numCells;


//  uint cellId = (metaNumber -1) * MetaCells[ metaNumber ].numCells;

  return cellId;
}

//----------------------------------------------------------------------------
__device__ void initializeNextCycle()
{
//  printf("\n initializeNextCycle.\n");

  uint actual_Id = getCurrentPartId();
  uint last_Id = getPartIdInLastStep();


  //-----------------------------------------------------------------------------------
  // Coordinates
  ParticlePath[ actual_Id ].pointId = 1; // Setting computed particle

  ParticlePath[ actual_Id ].x = ParticlePath[ last_Id ].x;
  ParticlePath[ actual_Id ].y = ParticlePath[ last_Id ].y;
  ParticlePath[ actual_Id ].z = ParticlePath[ last_Id ].z;


  //-----------------------------------------------------------------------------------
  // Velocity
  ParticlePath[ actual_Id ].velocityX = ParticlePath[ last_Id ].velocityX;
  ParticlePath[ actual_Id ].velocityY = ParticlePath[ last_Id ].velocityY;
  ParticlePath[ actual_Id ].velocityZ = ParticlePath[ last_Id ].velocityZ;


  //-----------------------------------------------------------------------------------
  // Pressure and time
  ParticlePath[ actual_Id ].pressure = ParticlePath[ last_Id ].pressure;

  ParticlePath[ actual_Id ].time = ParticlePath[ last_Id ].time;
}

