/*
 * vtkHMParticleTracingWriter.cu
 *
 *  Created on: 07/06/2010
 *      Author: eduardo
 */

#include "vtkHMUtils.h" // Adiciona as estruturas usadas
#include <external_dependency.h>
#include <iostream>


__device__ point *ParticlePath;
__device__ point *VecListSearch;
__device__ gpuMetaCell *MetaCells;
__device__ uint numParticles;
__device__ uint actualtStep;
__device__ float maxDistance;
__device__ float delta;
__device__ cell *fullCells;
__device__ point *fullPoints;
__device__ uint cycleId;




__device__ void findCloserCells(uint metaNumber);
__device__ int getCloserCells(uint currentCell);
__device__ int checkMinimumDistance(float x, float y,float z);
__device__ int isInsideCell(uint currentCell, float *a1, float *a2, float *a3, float *a4);
__device__ void setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, uint currentCell);
__device__ void setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4);
__device__ void computeVectorOperationSub(point *v, point pa, point pb);
__device__ void setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4);
__device__ void computeCrossProd(point *v, point va, point vb);
__device__ float computeScalarProd(point va, point vb);
__device__ void setAlphaValues(float *alpha, point n, point va, point vb);
__device__ void computeVelocity(uint currentCell, float a1, float a2, float a3, float a4);
__device__ uint getCurrentPartId();
__device__ uint getPartIdInNextStep();
__device__ uint getPartIdInLastStep();
__device__ uint getInitialCell(uint metaNumber);
__device__ void initializeNextCycle();


__global__ void computeTraceCUDA(point *Device_ParticlePath, gpuMetaCell *Device_metaCells, point *Device_vecListSearch,
                                 uint nParticles, uint currentStep, float maxDistance, float delta, cell *Device_fullCells,
                                 point *Device_fullPoints, uint cycle_Id, uint startCycle);



//----------------------------------------------------------------------------
void ExecuteKernel_DispatcherThreadsGPU(point *Device_ParticlePath,
                                        gpuMetaCell *Device_metaCells,
                                        point *Device_vecListSearch,
                                        uint nParticles,
                                        uint currentStep,
                                        float distance,
                                        float d,
                                        cell *Device_fullCells,
                                        point *Device_fullPoints,
                                        uint cycle_Id,
                                        uint startCycle)
{
  int max_threads_por_bloco, blocos;

  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, 0);


  // Esta função retorna 9999 para ambos os campos "major" e "minor", caso não exista um device que suporte CUDA
  if (deviceProp.major == 9999 && deviceProp.minor == 9999)
    max_threads_por_bloco = 64;
  else
    max_threads_por_bloco = 256;



  // Se o resto for diferente de zero tenho partículas não alocadas em um bloco então
  // devo adicionar mais 1 bloco para processar as partículas restantes.
  blocos = nParticles / max_threads_por_bloco + (nParticles % max_threads_por_bloco == 0 ? 0 : 1);



//  printf( "\n\t nParticles: %d\n", nParticles );
//  printf( "\t max_threads_por_bloco: %d\n", max_threads_por_bloco );
//  printf( "\t módulo: %d\n", (nParticles % max_threads_por_bloco) );
//  printf( "\t blocos: %d\n", blocos );


  dim3 blocksize(max_threads_por_bloco, 1, 1);
  dim3 gridsize(blocos, 1, 1);

  computeTraceCUDA <<< gridsize, blocksize >>> (Device_ParticlePath, Device_metaCells, Device_vecListSearch,
                                                          nParticles, currentStep, distance,
                                                          d, Device_fullCells, Device_fullPoints, cycle_Id, startCycle);




//  // Setup kernel problem size
//  dim3 blocksize(nParticles, 1, 1);
//  dim3 gridsize(1, 1, 1);
//
//
//  // Call kernel
//  computeTraceCUDA <<< gridsize, blocksize >>> (Device_ParticlePath, Device_metaCells, Device_vecListSearch,
//                                                nParticles, currentStep, distance,
//                                                d, Device_fullCells, Device_fullPoints, cycle_Id, startCycle);
}


//----------------------------------------------------------------------------
// Start Computing trace by finding the possible Metacells that contains the particle
__global__ void computeTraceCUDA(point *Device_ParticlePath,
                                 gpuMetaCell *Device_metaCells,
                                 point *Device_vecListSearch,
                                 uint nParticles,
                                 uint currentStep,
                                 float distance,
                                 float d,
                                 cell *Device_fullCells,
                                 point *Device_fullPoints,
                                 uint cycle_Id,
                                 uint startCycle)
{
  int inside = -1;
  float xmin, xmax, ymin, ymax, zmin, zmax;


  // Setando variáveis globais
  ParticlePath = Device_ParticlePath;
  VecListSearch = Device_vecListSearch;
  MetaCells = Device_metaCells;
  numParticles = nParticles;
  actualtStep = currentStep;
  maxDistance = distance;
  delta = d;
  fullCells = Device_fullCells;
  fullPoints = Device_fullPoints;
  cycleId = cycle_Id;


  // Evita que uma thread inválida inicie a execução
  // Caso a dimensão do bloco em x (threadIdx.x) seja maior que o número de partículas, podem existir threads inválidas.
  if( threadIdx.x < nParticles )
    {
    // Se for o início de um novo ciclo as últimas posições das partículas serão usadas para inicializar o novo ciclo.
    if( startCycle == 1 )
      initializeNextCycle();


    uint partId = getCurrentPartId();
    point p = ParticlePath[ partId ];


    for( uint metaNumber = 1; metaNumber <= 8; metaNumber++)
      {
      xmin = MetaCells[ metaNumber ].bounds[0];
      xmax = MetaCells[ metaNumber ].bounds[1];
      ymin = MetaCells[ metaNumber ].bounds[2];
      ymax = MetaCells[ metaNumber ].bounds[3];
      zmin = MetaCells[ metaNumber ].bounds[4];
      zmax = MetaCells[ metaNumber ].bounds[5];


  //    printf("Metacell %d >>>> %d %d\n", i, Device_metaCells[i].numPoints, Device_metaCells[i].numCells);
  //    printf("Metacell %d > %f %f %f %f %f %f\n", i, xmin, xmax, ymin, ymax, zmin, zmax);
  //    printf("Coordinates > %f %f %f\n", p.x, p.y, p.z);



      if((p.x >= xmin && p.x <= xmax) && (p.y >= ymin && p.y <= ymax) && (p.z >= zmin && p.z <= zmax))
        inside = 1; // Particle is inside
      else
        inside = 0; // Particle is outside


      if(inside)
        {
  //      printf("Particle %d is inside of metacell %d.\n", partId, i);
        findCloserCells( metaNumber );
        }
      }
    }
}


//----------------------------------------------------------------------------
// Find Cells by the Euclidean Distance of the Particle coordinate
__device__ void findCloserCells( uint metaNumber )
{
//  char metaName[256], aux[256];
  uint numCells;
  float a1, a2, a3, a4; // Alfas globais para habilitar a interpolação
//  uint numPoints;
//  setFileName(metaName, metaNumber, 1);
//  FILE *fin = fopen(metaName,  "r");

//  getNumberOfPointsAndCells(metaNumber, &numPoints, &numCells);
//  numPoints = Device_metaCells[ metaNumber ].numPoints;
  numCells = MetaCells[ metaNumber ].numCells;

//  point *pointList = MetaCells[ metaNumber ].pointList;   // Point List
//  cell  *cellList = MetaCells[ metaNumber ].cellList;    // Cell List


//  printf("Metacell >>>> %d %d\n",  Device_metaCells[ metaNumber ].numPoints, Device_metaCells[ metaNumber ].numCells);

  uint initialCell = getInitialCell( metaNumber );
  uint finalCell = initialCell + numCells;

  // Finding an inside point
  for(uint i = initialCell; i < finalCell; i++)
    {
    if( getCloserCells(i) )
      {
      if(isInsideCell( i, &a1, &a2, &a3, &a4))
        {
        computeVelocity( i, a1, a2, a3, a4);
        break; // If a cell was found, end the search
        }
      }




//    if(getCloserCells( pointList, cellList[i] ))
//      {
//      if(isInsideCell(pointList, cellList[i], &a1, &a2, &a3, &a4))
//        {
//        computeVelocity(cellList[i], a1, a2, a3, a4);
//        break; // If a cell was found, end the search
//        }
//      }
    }

}


//----------------------------------------------------------------------------
// Get Cells by the Euclidean Distance of the Particle coordinate
__device__ int getCloserCells( uint cellId )
{

//  printf("getCloserCells.\n");


  float x = 0.0;
  float y = 0.0;
  float z = 0.0;

  int res[4] = {-1, -1, -1, -1};   // load the result for each vertex


  for(uint i = 0; i <=3; i++)
    {
    if( fullPoints[ fullCells[ cellId ].v[i] ].pointId )
      {
      x = fullPoints[ fullCells[ cellId ].v[i] ].x;
      y = fullPoints[ fullCells[ cellId ].v[i] ].y;
      z = fullPoints[ fullCells[ cellId ].v[i] ].z;

      res[i] = checkMinimumDistance( x, y, z );
      }
    else
      {
//      printf("\n\nCheck getCloserCells Function!!!\n\n");
//      system("PAUSE");
//      exit(1);
      }
    }
  if( res[0] && res[1] && res[2] && res[3])
    return 1;
  else
    return 0;
}


//----------------------------------------------------------------------------
// Compute Euclidean Distance between two points and check if
// that value is lower than the minimum 'distance'
__device__ int checkMinimumDistance(float x, float y,float z )
{
//  printf("checkMinimumDistance.\n");


  uint partId = getCurrentPartId();
  point currentParticle = ParticlePath[ partId ];

  float d;

  d = float(pow(x - currentParticle.x,2) + pow(y - currentParticle.y,2) + pow(z - currentParticle.z,2) - pow(maxDistance,2));
  if(d <= 0.0)
    return 1;
  else
    return 0;
}


//----------------------------------------------------------------------------
// Check if the particle is inside a given cell (tetraedro).
__device__ int isInsideCell( uint currentCell, float *a1, float *a2, float *a3, float *a4)
{
//  printf("isInsideCell.\n");


  point v1, v2, v3, v4;
  point n1, n2, n3;
  point p0, p1, p2, p3, p4;

  setPoints(&p0, &p1, &p2, &p3, &p4, currentCell);
  setVector(&v1, &v2, &v3, &v4, p0, p1, p2, p3, p4);
  setNValues(&n1, &n2, &n3, v2, v3, v4);

  setAlphaValues(a4, n1, v1, v4);
  setAlphaValues(a3, n2, v1, v3);
  setAlphaValues(a2, n3, v1, v2);

  *a1 = 1 - *a2 - *a3 - *a4;

  if((*a1>=0  && *a1<=1) && (*a2>=0  && *a2<=1) && (*a3>=0  && *a3<=1) && (*a4>=0  && *a4<=1))
    return 1; // Point defined by global variable 'currentParticle' is inside the cell 'c'
  else
    return 0; // Point is outside the cell 'c'



// COMO ALPHAS ORIGINAIS ERAM GLOBAIS, NAS FUNÇÕES ABAIXO ERA PASSADO APENAS O ENDEREÇO DELES.

//  setAlphaValues(&a4, n1, v1, v4);
//  setAlphaValues(&a3, n2, v1, v3);
//  setAlphaValues(&a2, n3, v1, v2);
//
//  a1 = 1 - a2 - a3 - a4;
//
//  if((a1>=0  && a1<=1) && (a2>=0  && a2<=1) && (a3>=0  && a3<=1) && (a4>=0  && a4<=1))
//    return 1; // Point defined by global variable 'currentParticle' is inside the cell 'c'
//  else
//    return 0; // Point is outside the cell 'c'
}


//----------------------------------------------------------------------------
// Set Point Values
__device__ void setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, uint currentCell)
{
//  printf("setPoints.\n");

  uint partId = getCurrentPartId();

  *p0 = ParticlePath[ partId ];;


  *p1 = fullPoints[ fullCells[ currentCell ].v[0] ];
  *p2 = fullPoints[ fullCells[ currentCell ].v[1] ];
  *p3 = fullPoints[ fullCells[ currentCell ].v[2] ];
  *p4 = fullPoints[ fullCells[ currentCell ].v[3] ];
}

//----------------------------------------------------------------------------
// Set Vector Values
__device__ void setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4)
{
//  printf("setVector.\n");


  computeVectorOperationSub(v1, p0, p1);
  computeVectorOperationSub(v2, p2, p1);
  computeVectorOperationSub(v3, p3, p1);
  computeVectorOperationSub(v4, p4, p1);
}


//----------------------------------------------------------------------------
// Compute subtration operation for points
__device__ void computeVectorOperationSub(point *v, point pa, point pb)
{
//  printf("setVector.\n");


  (*v).x = pa.x - pb.x;
  (*v).y = pa.y - pb.y;
  (*v).z = pa.z - pb.z;
}


//----------------------------------------------------------------------------
// Calculates Cross Product between two vector v = pa x pb
__device__ void computeCrossProd(point *v, point va, point vb)
{
//  printf("computeCrossProd.\n");


  (*v).x = (va.y * vb.z) - (va.z * vb.y);
  (*v).y = (va.z * vb.x) - (va.x * vb.z);
  (*v).z = (va.x * vb.y) - (va.y * vb.x);
}


//----------------------------------------------------------------------------
// Set the 'n' Values to compute inside/outside points
__device__ void setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4)
{
//  printf("setNValues.\n");


  computeCrossProd(n1, v2, v3);
  computeCrossProd(n2, v2, v4);
  computeCrossProd(n3, v3, v4);
}


//----------------------------------------------------------------------------
// Calculates Scalar Product between two vector r = v1. v2
__device__ float computeScalarProd(point va, point vb)
{
//  printf("computeScalarProd.\n");


  return float(va.x * vb.x + va.y * vb.y + va.z * vb.z);
}


//----------------------------------------------------------------------------
// Set the alpha values to compute inside/outside points
__device__ void setAlphaValues(float *alpha, point n, point va, point vb)
{
//  printf("setAlphaValues.\n");


  float val1, val2;
  val1 = float(computeScalarProd(n, va));
  val2 = float(computeScalarProd(n, vb));

  if(val2)
    *alpha = float(val1 / val2);
  else
    {
//    printf("\nCheck 'setAlphaValues' function!!!\n");
//    system("PAUSE");
    *alpha = 0;
    }

}


//----------------------------------------------------------------------------
// Procedures that computes the velocity of a particle inside a cell. Metacells are read here.
__device__ void computeVelocity(uint currentCell, float a1, float a2, float a3, float a4)
{
//  printf("computeVelocity.\n");


  uint actual_Id = getCurrentPartId();
  uint next_Id = getPartIdInNextStep();

  point actual_part = ParticlePath[ actual_Id ];


//  printf("Computting particle %d\n", actual_Id);
//  printf("next_Id  %d\n", next_Id);
//  printf("threadIdx  %d\n", threadIdx.x);
//  printf("Cell:  %d %d %d %d\n", c.v[0], c.v[1], c.v[2], c.v[3]);
//  printf("delta  %f\n", delta);
//  printf("alphas  %f %f %f %f\n", alphas[1], alphas[2], alphas[3], alphas[4]);

  uint v0 = fullCells[ currentCell ].v[0];
  uint v1 = fullCells[ currentCell ].v[1];
  uint v2 = fullCells[ currentCell ].v[2];
  uint v3 = fullCells[ currentCell ].v[3];


  //-----------------------------------------------------------------------------------
  // Computes the new position
  ParticlePath[ next_Id ].pointId = 1; // Setting computed particle

  ParticlePath[ next_Id ].x = actual_part.x + delta * (a1 * VecListSearch[ v0 ].velocityX +
                                                       a2 * VecListSearch[ v1 ].velocityX +
                                                       a3 * VecListSearch[ v2 ].velocityX +
                                                       a4 * VecListSearch[ v3 ].velocityX);

  ParticlePath[ next_Id ].y = actual_part.y + delta * (a1 * VecListSearch[ v0 ].velocityY +
                                                       a2 * VecListSearch[ v1 ].velocityY +
                                                       a3 * VecListSearch[ v2 ].velocityY +
                                                       a4 * VecListSearch[ v3 ].velocityY);

  ParticlePath[ next_Id ].z = actual_part.z + delta * (a1 * VecListSearch[ v0 ].velocityZ +
                                                       a2 * VecListSearch[ v1 ].velocityZ +
                                                       a3 * VecListSearch[ v2 ].velocityZ +
                                                       a4 * VecListSearch[ v3 ].velocityZ);

//  printf("\tOld position %f %f %f.....\n", actual_part.x,
//                                           actual_part.y,
//                                           actual_part.z);
//  printf("\tNew position %d:  %f %f %f.....\n", next_Id,
//                                                ParticlePath[ next_Id ].x,
//                                                ParticlePath[ next_Id ].y,
//                                                ParticlePath[ next_Id ].z);




  //-----------------------------------------------------------------------------------
  // Computes the new velocity
  ParticlePath[ next_Id ].velocityX = (a1 * VecListSearch[ v0 ].velocityX +
                                       a2 * VecListSearch[ v1 ].velocityX +
                                       a3 * VecListSearch[ v2 ].velocityX +
                                       a4 * VecListSearch[ v3 ].velocityX);

  ParticlePath[ next_Id ].velocityY = (a1 * VecListSearch[ v0 ].velocityY +
                                       a2 * VecListSearch[ v1 ].velocityY +
                                       a3 * VecListSearch[ v2 ].velocityY +
                                       a4 * VecListSearch[ v3 ].velocityY);

  ParticlePath[ next_Id ].velocityZ = (a1 * VecListSearch[ v0 ].velocityZ +
                                       a2 * VecListSearch[ v1 ].velocityZ +
                                       a3 * VecListSearch[ v2 ].velocityZ +
                                       a4 * VecListSearch[ v3 ].velocityZ);



  //-----------------------------------------------------------------------------------
  // Computes the new pressure and time
  ParticlePath[ next_Id ].pressure = ( VecListSearch[ v0 ].pressure +
                                       VecListSearch[ v1 ].pressure +
                                       VecListSearch[ v2 ].pressure +
                                       VecListSearch[ v3 ].pressure) / 4 ;



  //-----------------------------------------------------------------------------------
  // Computes the new time
  ParticlePath[ next_Id ].time = actual_part.time + delta;











//  FILE *fpar;
//  //  float delta = 0.005f;
//  float delta = this->Delta;
//  char particleName[256];
//  float vec[4];
//
//  // Se a meta célula carregada em memória for diferente da meta célula corrente, carregar a metacélula e velocidade
//  // Se não carregar somente a velocidade para o timestep
//  if(metaNumber != currentMetacell)
//    this->loadVelocityVectorFromFile(metaNumber, numPoints);
//  else
//    this->loadVelocityVectorFromMemory(numPoints);
//
//  this->setFileName(particleName, partNumber, 2);
//
//  // New
//  fpar = fopen(particleName, "a+");
//
//  double tmpPart[4];
//  tmpPart[0] = this->part.x;
//  tmpPart[1] = this->part.y;
//  tmpPart[2] = this->part.z;
//  tmpPart[3] = this->part.time;
//
//  // Computes the vector
//  this->part.x = this->part.x + delta * (a1 * this->vecListSearch[ c.v[0] ].x + a2 * this->vecListSearch[ c.v[1] ].x +
//      a3 * this->vecListSearch[ c.v[2] ].x + a4 * this->vecListSearch[ c.v[3] ].x);
//  this->part.y = this->part.y + delta * (a1 * this->vecListSearch[ c.v[0] ].y + a2 * this->vecListSearch[ c.v[1] ].y +
//      a3 * this->vecListSearch[ c.v[2] ].y + a4 * this->vecListSearch[ c.v[3] ].y);
//  this->part.z = this->part.z + delta * (a1 * this->vecListSearch[ c.v[0] ].z + a2 * this->vecListSearch[ c.v[1] ].z +
//      a3 * this->vecListSearch[ c.v[2] ].z + a4 * this->vecListSearch[ c.v[3] ].z);
//
//  this->part.velocityX = (a1 * this->vecListSearch[ c.v[0] ].x + a2 * this->vecListSearch[ c.v[1] ].x +
//      a3 * this->vecListSearch[ c.v[2] ].x + a4 * this->vecListSearch[ c.v[3] ].x);
//  this->part.velocityY = (a1 * this->vecListSearch[ c.v[0] ].y + a2 * this->vecListSearch[ c.v[1] ].y +
//      a3 * this->vecListSearch[ c.v[2] ].y + a4 * this->vecListSearch[ c.v[3] ].y);
//  this->part.velocityZ = (a1 * this->vecListSearch[ c.v[0] ].z + a2 * this->vecListSearch[ c.v[1] ].z +
//      a3 * this->vecListSearch[ c.v[2] ].z + a4 * this->vecListSearch[ c.v[3] ].z);
//
//
//  this->part.pressure = ( this->vecListSearch[ c.v[0] ].pressure + this->vecListSearch[ c.v[1] ].pressure +
//      this->vecListSearch[ c.v[2] ].pressure + this->vecListSearch[ c.v[3] ].pressure) / 4 ;
//
//  this->part.time = this->part.time + delta;
//
//
//
//  //armazena as coordenadas do tempo zero mas com velocidade e pressão do passo 1 ..........o tempo será o do passo zero.
//  if( !this->IsRepeatPulse && this->currentTime == 0 )
//    {
//    //fecha o arquivo
//    fclose(fpar);
//
//    //apaga conteudo o abre o arquivo para leitura e escrita
//    fpar = fopen(particleName, "w+");
//
//    fprintf(fpar, "TIME %d\n", this->currentTime);
//
//    //coordenadas do passo zero com velocidade, pressão e tempo do passo 1
//    fprintf(fpar, "%f %f %f %f %f %f %f %f\n", tmpPart[0], tmpPart[1], tmpPart[2],
//        this->part.velocityX, this->part.velocityY, this->part.velocityZ, this->part.pressure, tmpPart[3]);
//
//    //fecha o arquivo
//    fclose(fpar);
//
//    //abre o arquivo para leitura e adição
//    fpar = fopen(particleName, "a+");
//    }
//
//
//
//
//  //Se as coordenadas de x, y e z forem diferentes ao passo anterior, houve movimento
//  if( (this->part.x != this->LastPositions[partNumber-1].x) &&
//      (this->part.y != this->LastPositions[partNumber-1].y) &&
//      (this->part.z != this->LastPositions[partNumber-1].z) )
//    {
//
//    //    if( this->currentTime > 0)
//    //      {
//    //armazena as coordenadas, velocidade, pressão e tempo do passo
//    fprintf(fpar, "%f %f %f %f %f %f %f %f\n", this->part.x, this->part.y, this->part.z, this->part.velocityX, this->part.velocityY,
//        this->part.velocityZ, this->part.pressure, this->part.time);
//    //      }
//
//
//    // Armazenando a última posição calculada para a partícula corrente
//    this->LastPositions[partNumber-1].x = this->part.x;
//    this->LastPositions[partNumber-1].y = this->part.y;
//    this->LastPositions[partNumber-1].z = this->part.z;
//    this->LastPositions[partNumber-1].velocityX = this->part.velocityX;
//    this->LastPositions[partNumber-1].velocityY = this->part.velocityY;
//    this->LastPositions[partNumber-1].velocityZ = this->part.velocityZ;
//    this->LastPositions[partNumber-1].pressure = this->part.pressure;
//    this->LastPositions[partNumber-1].time = this->part.time;
//    }
//
//
//  this->currentTime++;
//  fclose(fpar);
}


//----------------------------------------------------------------------------
__device__ uint getCurrentPartId()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + (actualtStep * numParticles);
//  uint partId = threadIdx.x + ( actualtStep * numParticles );

  return partId;
}


//----------------------------------------------------------------------------
__device__ uint getPartIdInNextStep()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + ((actualtStep+1) * numParticles);
//  uint partId = threadIdx.x + ( (actualtStep+1) * numParticles );

  return partId;
}

//----------------------------------------------------------------------------
__device__ uint getPartIdInLastStep()
{
  uint partId = (blockIdx.x * blockDim.x + threadIdx.x) + ((actualtStep-1) * numParticles);
//  uint partId = threadIdx.x + ( (actualtStep-1) * numParticles );

  return partId;
}


//----------------------------------------------------------------------------
__device__ uint getInitialCell(uint metaNumber)
{
  uint cellId = (metaNumber -1) * MetaCells[ metaNumber ].numCells;

  return cellId;
}

//----------------------------------------------------------------------------
__device__ void initializeNextCycle()
{
//  printf("\n initializeNextCycle.\n");

  uint actual_Id = getCurrentPartId();
  uint last_Id = getPartIdInLastStep();


//  printf("actualtStep: %d\n", actualtStep);
//  printf("actual_Id: %d\n", actual_Id);
//  printf("last_Id: %d\n", last_Id);



  //-----------------------------------------------------------------------------------
  // Computes the new position
  ParticlePath[ actual_Id ].pointId = 1; // Setting computed particle

  ParticlePath[ actual_Id ].x = ParticlePath[ last_Id ].x;
  ParticlePath[ actual_Id ].y = ParticlePath[ last_Id ].y;
  ParticlePath[ actual_Id ].z = ParticlePath[ last_Id ].z;


//  printf("\tPosition in %d:  %f %f %f.....\n", last_Id-2,
//                                               ParticlePath[ last_Id-2 ].x,
//                                               ParticlePath[ last_Id-2 ].y,
//                                               ParticlePath[ last_Id-2 ].z);
//
//  printf("\tPosition in %d:  %f %f %f.....\n", last_Id-1,
//                                               ParticlePath[ last_Id-1 ].x,
//                                               ParticlePath[ last_Id-1 ].y,
//                                               ParticlePath[ last_Id-1 ].z);
//
//  printf("\tPosition in %d:  %f %f %f.....\n", last_Id,
//                                               ParticlePath[ last_Id ].x,
//                                               ParticlePath[ last_Id ].y,
//                                               ParticlePath[ last_Id ].z);
//
//  printf("\tPosition in %d:  %f %f %f.....\n", actual_Id,
//                                               ParticlePath[ actual_Id ].x,
//                                               ParticlePath[ actual_Id ].y,
//                                               ParticlePath[ actual_Id ].z);

  //-----------------------------------------------------------------------------------
  // Computes the new velocity
  ParticlePath[ actual_Id ].velocityX = ParticlePath[ last_Id ].velocityX;
  ParticlePath[ actual_Id ].velocityY = ParticlePath[ last_Id ].velocityY;
  ParticlePath[ actual_Id ].velocityZ = ParticlePath[ last_Id ].velocityZ;


  //-----------------------------------------------------------------------------------
  // Computes the new pressure and time
  ParticlePath[ actual_Id ].pressure = ParticlePath[ last_Id ].pressure;


  //-----------------------------------------------------------------------------------
  // Computes the new time
  ParticlePath[ actual_Id ].time = ParticlePath[ last_Id ].time;
}















































//// -----------------------------------------------------------------------------
//__global__ void particle_kernel_Teste(point * in)
//{
//  printf("\nparticle_kernel_Teste\n" );
//
////  printf("blockDim.y: %d\n", blockDim.y);
//
//
//   /*const*/ unsigned int thread = threadIdx.y;
//  in[thread].pointId = in[thread].pointId;
//  printf("in[thread].pointId: %d\n", in[thread].pointId);
//
////  in[thread].x = in[thread].x;
//  printf("in[thread].x: %d\n", in[thread].x );
//
////  in[thread].y = in[thread].y;
//  printf("in[thread].y: %d\n", in[thread].y );
//
////  in[thread].z = in[thread].z;
//  printf("in[thread].z: %d\n", in[thread].z );
//
////  in[thread].velocityX = 2 * in[thread].velocityX;
//  printf("in[thread].velocityX: %d\n", in[thread].velocityX );
//
////  in[thread].velocityY = 2 * in[thread].velocityY;
//  printf("in[thread].velocityY: %d\n", in[thread].velocityY );
//
////  in[thread].velocityZ = 2 * in[thread].velocityZ;
//  printf("in[thread].velocityZ: %d\n", in[thread].velocityZ );
//
////  in[thread].pressure  = 2 * in[thread].pressure;
//  printf("in[thread].pressure: %d\n", in[thread].pressure );
//
////  in[thread].time      = 2 * in[thread].time;
//  printf("in[thread].time: %d\n", in[thread].time );
//
//
//
//
//
////  for (unsigned int i=0;i<blockDim.x;++i)
////    {
////    // /*const*/ unsigned int thread = threadIdx.x;
////    in[threadIdx.x].pointId = in[threadIdx.x].pointId;
////    printf("in[threadIdx.x].pointId: %d\n", in[threadIdx.x].pointId);
////
////    in[threadIdx.y].x = in[threadIdx.y].x;
////    printf("in[threadIdx.y].x: %d\n", in[threadIdx.y].x );
////
////    in[threadIdx.y].y = in[threadIdx.y].y;
////    printf("in[threadIdx.y].y: %d\n", in[threadIdx.y].y );
////
////    in[threadIdx.y].z = in[threadIdx.y].z;
////    printf("in[threadIdx.y].z: %d\n", in[threadIdx.y].z );
////
////
////    in[threadIdx.y].velocityX = 2 * in[threadIdx.y].velocityX;
////    printf("in[threadIdx.y].velocityX: %d\n", in[threadIdx.y].velocityX );
////
////    in[threadIdx.y].velocityY = 2 * in[threadIdx.y].velocityY;
////    printf("in[threadIdx.y].velocityY: %d\n", in[threadIdx.y].velocityY );
////
////    in[threadIdx.y].velocityZ = 2 * in[threadIdx.y].velocityZ;
////    printf("in[threadIdx.y].velocityZ: %d\n", in[threadIdx.y].velocityZ );
////
////
////    in[threadIdx.y].pressure  = 2 * in[threadIdx.y].pressure;
////    printf("in[threadIdx.y].pressure: %d\n", in[threadIdx.y].pressure );
////
////
////    in[threadIdx.y].time      = 2 * in[threadIdx.y].time;
////    printf("in[threadIdx.y].time: %d\n", in[threadIdx.y].time );
////    }
//}
//
//// -----------------------------------------------------------------------------
//void ExecuteKernel_Teste(point * in, int lin, int col)
//{
//  // Setup kernel problem size
//  dim3 blocksize(lin,col,1);
//  dim3 gridsize(1,1,1);
//
//  // Call kernel
//  particle_kernel_Teste<<<gridsize, blocksize>>>(in);
//}


// -----------------------------------------------------------------------------
#if vtkHMParticleTracingWriter_EXPORTS
#  if defined( _WIN32 ) || defined( _WIN64 )
#    define vtkHMParticleTracingWriter_API __declspec(dllexport)
#  else
#    define vtkHMParticleTracingWriter_API
#  endif
#else
#  define vtkHMParticleTracingWriter_API
#endif


//
//// -----------------------------------------------------------------------------
//vtkHMParticleTracingWriter_API int CopyVectorFieldToGPU(MetaCell *metaCells)
//{
//  // Allocate device memory
//  unsigned int size = sizeof(MetaCell *) * 9; // São alocadas 9 metacells em vtkHM3DParticleTracingWriterCUDA.h e usadas de 1 até 8.
//  MetaCell *d_metaCells;
//  cudaMalloc((void**)&d_metaCells, size);
//  CHECK_CUDA_ERROR();
//
////  ExecuteKernel(d_metaCells);
//
//
//  return 0; // Ok
//}
//
//// -----------------------------------------------------------------------------
//vtkHMParticleTracingWriter_API int CopyGeometryToGPU(MetaCell *host_metaCells, MetaCell *device_metaCells)
//{
//  // Allocate device memory
//  MetaCell *d_metaCells;
//  unsigned int size = sizeof(MetaCell *) * 9; // São alocadas 9 metacells em vtkHM3DParticleTracingWriterCUDA.h e usadas de 1 até 8.
//  cudaMalloc((void**)&d_metaCells, size);
//  CHECK_CUDA_ERROR();
//
//  int numCells, numPoints;
//
//  for(int i=1; i <= 8; i++)
//    {
//    numCells  = host_metaCells[i].numCells;
//    numPoints = host_metaCells[i].numPoints;
//
//    if(i==0)
//      numCells = numPoints = 0;
//
//    cudaMalloc((void**)&d_metaCells[i].cellList, sizeof(cell) * numCells);
//    cudaMalloc((void**)&d_metaCells[i].pointList, sizeof(point) * numPoints);
//    CHECK_CUDA_ERROR();
//    }
//
//
//  // Send input to device
//  cudaMemcpy(d_metaCells, host_metaCells, sizeof(MetaCell) * 9, cudaMemcpyHostToDevice);
//  CHECK_CUDA_ERROR();
//
//  device_metaCells = d_metaCells;
//
//
////  ExecuteKernel(d_metaCells);
//
//
//  return 0; // Ok
//}
//
//// -----------------------------------------------------------------------------
//vtkHMParticleTracingWriter_API int DeleteGeometryAndVectorsFromGPU( MetaCell *device_metaCells )
//{
//  printf("\nDeleteGeometryAndVectorsFromGPU\n");
//
//
//  for(int i=1; i <= 8; i++)
//    {
//    cudaFree((void*)device_metaCells[i].cellList);
//    cudaFree((void*)device_metaCells[i].pointList);
//    CHECK_CUDA_ERROR();
//    }
//
//  cudaFree((void*)device_metaCells);
//  CHECK_CUDA_ERROR();
//
//
//  //TODO: fazer a parte q desaloca os vetores
//
//return 0; // Ok
//}
//
//// -----------------------------------------------------------------------------
//vtkHMParticleTracingWriter_API int CopyParticlePathToGPU(point * host_ParticlePath, point * device_ParticlePath, int n_part, int n_steps, int n_repetitions)
//{
//
//  point * d_ParticlePath;
//
//  point * h_ParticlePath;
//  h_ParticlePath = host_ParticlePath;
//
//
//  int tam = n_part * n_steps * n_repetitions;
//
//
//  // Allocate device memory
//  unsigned int size = sizeof(point *) * tam;
//
//  cudaMalloc((void**)&d_ParticlePath, size);
//  CHECK_CUDA_ERROR();
//
//
//  // Envia dados para a GPU
//  cudaMemcpy(d_ParticlePath, h_ParticlePath, size, cudaMemcpyHostToDevice);
//  CHECK_CUDA_ERROR();
//
//
////  // Call the kernel
////  ExecuteKernel_Teste(d_ParticlePath, 1, n_part );
////  CHECK_CUDA_ERROR();
//
//
//  device_ParticlePath = d_ParticlePath ;
//
//  return 0; // sem erros
//}
//
//// -----------------------------------------------------------------------------
//vtkHMParticleTracingWriter_API int CopyParticlePathToCPU(point * host_ParticlePath, point * device_ParticlePath, int n_part, int n_steps, int n_repetitions)
//{
//  int tam = n_part * n_steps * n_repetitions;
//
//  unsigned int size = sizeof(point *) * tam;
//
//  point * h_ParticlePath;
//  point * d_ParticlePath;
//
//
//  // Get back results
//  cudaMemcpy(h_ParticlePath, device_ParticlePath, size, cudaMemcpyDeviceToHost);
//  CHECK_CUDA_ERROR();
//
//
//  device_ParticlePath = d_ParticlePath;
//  host_ParticlePath = h_ParticlePath;
//
//
//  cudaFree(d_ParticlePath);
//
//  return 0; // sem erros
//}
