#include "vtkPolyData.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkUnstructuredGridWriter.h"

int main(int argc, char* argv[])
{
	cout << "Break: 1" << endl;
  vtkUnstructuredGridReader *reader = vtkUnstructuredGridReader::New();
  reader -> SetFileName("/home/Data/DelaTest/carotida_furada.vtk");
  reader -> Update();

	cout << "Break: 2" << endl;
 
  vtkUnstructuredGridWriter *writer = vtkUnstructuredGridWriter::New();
  writer -> SetInput( (vtkDataObject *) reader->GetOutput());
  writer -> SetFileName("/home/Data/DelaTest/carotida_furada_poly.vtk");
  writer -> SetFileTypeToASCII();

	cout << "Break: 3" << endl;  
  
  writer -> Update();
  writer->Write();
  
	cout << "Break: 4" << endl;  

  return 1;
}

