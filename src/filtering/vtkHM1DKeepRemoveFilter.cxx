/*
 * vtkHM1DKeepRemoveFilter.cxx
 *
 *  Created on: Sep 3, 2009
 *      Author: igor
 */


#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkDoubleArray.h"

#include "vtkHM1DKeepRemoveFilter.h"

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHMUtils.h"

vtkCxxRevisionMacro(vtkHM1DKeepRemoveFilter, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHM1DKeepRemoveFilter);

//----------------------------------------------------------------------------
vtkHM1DKeepRemoveFilter::vtkHM1DKeepRemoveFilter()
{
  //Configurando numero de entradas.
  this->SetNumberOfInputPorts(1);

  this->StraightModel = vtkHM1DStraightModel::New();

  this->SegementRemoveList = vtkIdList::New();

  this->KeepRemoveMode = REMOVE_MODE;

  this->TimeArray = NULL;
}

//----------------------------------------------------------------------------
vtkHM1DKeepRemoveFilter::~vtkHM1DKeepRemoveFilter()
{
  if (this->StraightModel)
    this->StraightModel->Delete();

  this->SegementRemoveList->Delete();

  if ( this->TimeArray )
    this->TimeArray->Delete();
}

//----------------------------------------------------------------------------
void vtkHM1DKeepRemoveFilter::SetInput(vtkHM1DStraightModelGrid *input)
{
  vtkHM1DStraightModelGrid *grid = vtkHM1DStraightModelGrid::New();
  grid->DeepCopy(input);
  this->Superclass::SetInput(grid);
  grid->Delete();
}


//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DKeepRemoveFilter::GetOutput(int port)
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(port));
}

//----------------------------------------------------------------------------
int vtkHM1DKeepRemoveFilter::RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **inputVector,
    vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkHM1DStraightModelGrid
      *input =
      vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkHM1DStraightModelGrid
      *output =
          vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // Testa se input e output são válidos
  if (!input)
    {
    vtkErrorMacro("Input is NULL.");
    return 0;
    }

  if (!output)
    {
    vtkErrorMacro("Output is NULL.");
    return 0;
    }

  if ( this->KeepRemoveMode == REMOVE_MODE )
    {
    this->StraightModel->DeepCopy(input->GetStraightModel());

    vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegementRemoveList->GetId(0));

    //pego o terminal parent do segmento clicado
    vtkHM1DTerminal *termParent = vtkHM1DTerminal::SafeDownCast(segm->GetFirstParent());

    std::list<double> *r1List = termParent->GetR1();
    std::list<double> *r2List = termParent->GetR2();
    std::list<double> *cList = termParent->GetC();

    double r1Terminal = *r1List->begin();
    double r2Terminal = *r2List->begin();
    double cTerminal = *cList->begin();

    this->StraightModel->RemoveSegmentsInSubTree(this->SegementRemoveList,  0);

    //coloco o valor da capacitancia em um DoubleArray para poder setar na funcao
    vtkDoubleArray *Cap = vtkDoubleArray::New();
    Cap->InsertValue(0, this->EquivalentCapacitance+cTerminal);
    //seto o valor da capacitancia no terminal selecionado
    termParent->SetArray(Cap, 'C', 0);
    Cap->Delete();



    double newResistance = 1 / ( (1/(r1Terminal+r2Terminal)) + (1/this->EquivalentResistance) );

    //pego o valor de resistencia equivalente
    //coloco 20% desse valor para r1 e 80% para r2
    double r1 = newResistance * 0.2;
    double r2 = newResistance * 0.8;

    //coloco os valores de Resistencia em dois DoubleArrays para poder setar nas funcoes
    vtkDoubleArray *R1 = vtkDoubleArray::New();
    vtkDoubleArray *R2 = vtkDoubleArray::New();
    R1->InsertValue(0, r1);
    R2->InsertValue(0, r2);

    //seto os valores de R1 e R2 no terminal selecionado
    termParent->SetArray(R1, 'R', 1);
    termParent->SetArray(R2, 'R', 2);

    R1->Delete();
    R2->Delete();

    output->SetStraightModel(this->StraightModel);

    output->CreateStraightModelGrid();
    }
  else if ( this->KeepRemoveMode == KEEP_MODE )
    {
    this->StraightModel->DeepCopy(input->GetStraightModel());

    //pega todos os segmentos da arvore
    char *str = this->StraightModel->GetSegmentsID();

    vtkIdList *AllSegmentsIds = vtkHMUtils::ConvertCharToIdList(str);

    // se lista de segmentos selecionados estiver vazia,
    // deixo somente o segmento filho do coração.
    if ( this->SegementRemoveList->GetNumberOfIds() == 0 )
      {
      vtkWarningMacro("List of segments is empty.");
      AllSegmentsIds->DeleteId(2);

      this->StraightModel->RemoveSegmentsInSubTree(AllSegmentsIds,  0);

      vtkHM1DStraightModel *newStraightModel = vtkHM1DStraightModel::New();
      newStraightModel->DeepCopy(this->StraightModel);

      output->SetStraightModel(newStraightModel);

      output->CreateStraightModelGrid();

      AllSegmentsIds->Delete();
      newStraightModel->Delete();
      return 1;
      }

    //deleta da lista de todos os segmentos os selecionados
    for ( int i=0; i<this->SegementRemoveList->GetNumberOfIds(); i++ )
      {
      AllSegmentsIds->DeleteId(this->SegementRemoveList->GetId(i));
      }

    //remove da arvore os segmentos que não estão selecionados
    this->StraightModel->RemoveSegmentsInSubTree(AllSegmentsIds,  0);

    vtkHM1DTerminal *heart = vtkHM1DTerminal::SafeDownCast(this->StraightModel->Get1DTreeRoot());
    vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegementRemoveList->GetId(0));

    vtkHM1DTreeElement *parent = segm->GetFirstParent();
    vtkHM1DTreeElement *child = parent->GetFirstChild();

    // pegar o array de dados lido do DataOut do primeiro node do segmento.
    vtkDoubleArray *array = segm->GetResolutionArray(0);

    // se tenho dados lidos do DataOut, setar a curva do coração com
    // os dados lidos. Esses dados são do primeiro node do segmento
    // filho do terminal que ficará como coração.
    if ( array )
      {
      //seta o tempo e a curva do coração
      vtkDoubleArray *timeArray = this->StraightModel->GetInstantOfTime();
      vtkDoubleArray *newTimeArray = vtkDoubleArray::New();

      // tempo total da simulação
      double totalTime = timeArray->GetValue(timeArray->GetNumberOfTuples()-1);

      // numero de ciclos processados completamente
      int numberOfCardiacCycle = (float)totalTime / (float)this->CardiacCycleTime;

      // numero de pontos de cada ciclo cardiaco
      int nPoints = (float)this->CardiacCycleTime / ((float)timeArray->GetValue(1) - (float)timeArray->GetValue(0));

      // ponto inicial na curva.
      int initial = (numberOfCardiacCycle-1) * nPoints;

      vtkDoubleArray *p = vtkDoubleArray::New();

      std::list<double> *R1 = heart->GetR1();
      std::list<double> *R2 = heart->GetR2();

      std::list<double>::iterator it = R1->begin();

      double r1Value = *it;

      it = R2->begin();
      double r2Value = *it;

      double sumResistance = r1Value+r2Value;

      int n = array->GetNumberOfTuples();
      p->SetNumberOfValues(nPoints+1);
      newTimeArray->SetNumberOfValues(nPoints+1);

      // se a soma das resistencias for maior que zero, calculo o valor da pressão
      // multiplicando o fluxo pela soma das resistencias
      if ( sumResistance > 0 )
        {
        int j=0;
        for ( int i=initial; i<initial+nPoints+1; i++ )
          {
          double *tuple = array->GetTuple(i);
          p->SetValue(j, tuple[0]*sumResistance);
          newTimeArray->SetValue(j, timeArray->GetValue(j));

          j++;
          }
        }
      else
        {
        int j=0;
        for ( int i=initial; i<initial+nPoints+1; i++ )
          {
          double *tuple = array->GetTuple(i);
          p->SetValue(j, tuple[0]);
          newTimeArray->SetValue(j, timeArray->GetValue(j));

          j++;
          }
        }



      heart->SetTimeArray(newTimeArray, 'P', 1);
      heart->SetArray(p, 'P', 1);
      p->Delete();
      newTimeArray->Delete();
      }

    //coloca o coração na posição correta
    while (child)
      {
      heart->AddChild(child);
      child->RemoveParent(parent);
      parent->RemoveChild(child);
      child = parent->GetFirstChild();
      }

    vtkHM1DStraightModel *newStraightModel = vtkHM1DStraightModel::New();
    newStraightModel->DeepCopy(this->StraightModel);

    output->SetStraightModel(newStraightModel);

    output->CreateStraightModelGrid();

    AllSegmentsIds->Delete();
    newStraightModel->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkHM1DKeepRemoveFilter::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHM1DKeepRemoveFilter::FillOutputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DKeepRemoveFilter::FillInputPortInformation(int port,
    vtkInformation* info)
{
  // Seta tipo de entrada
  if (!this->Superclass::FillInputPortInformation(port, info))
    return 0;

  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkHM1DStraightModelGrid");
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM1DKeepRemoveFilter::SetSegmentsSelected(vtkIdList *list)
{
  this->SegementRemoveList->DeepCopy(list);
}

//----------------------------------------------------------------------------
void vtkHM1DKeepRemoveFilter::SetOutput(vtkHM1DStraightModelGrid *output)
{
  vtkHM1DStraightModelGrid *o = this->GetOutput(0);

  o->DeepCopy(output);
}

//----------------------------------------------------------------------------
void vtkHM1DKeepRemoveFilter::SetDataOutTimeArray(const char *array)
{
  string Result;
  char *str;

  if ( !array )
    {
    if ( this->TimeArray )
      {
      this->TimeArray->Delete();
      this->TimeArray = NULL;
      }
    vtkWarningMacro("vtkHM1DKeepRemoveFilter::SetDataOutTimeArray array is NULL");
    return;
    }

  if ( !this->TimeArray )
    this->TimeArray = vtkDoubleArray::New();

  vtkDoubleArray *darray = vtkHMUtils::ConvertCharToDoubleArray((char*)array);
  this->TimeArray->DeepCopy(darray);
  darray->Delete();
}

//----------------------------------------------------------------------------
const char *vtkHM1DKeepRemoveFilter::GetDataOutTimeArray()
{
  string Result;
  char *str;

  if (this->TimeArray)
    {
    Result = vtkHMUtils::ConvertDoubleArrayToChar(this->TimeArray);
    str = new char[Result.size()+2];
    strcpy(str, Result.c_str());
    return (const char*)str;
    }
  else
    vtkErrorMacro("vtkHM1DKeepRemoveFilter::GetDataOutTimeArray Unable to get InstantOfTime array ");
  return Result.c_str(); // retorna string vazia

}
