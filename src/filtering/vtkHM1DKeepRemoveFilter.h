/*
 * vtkHM1DKeepRemoveFilter.h
 *
 *  Created on: Sep 3, 2009
 *      Author: igor
 */

#ifndef VTKHM1DKEEPREMOVEFILTER_H_
#define VTKHM1DKEEPREMOVEFILTER_H_


// .NAME vtkHM1DKeepRemoveFilter
// .SECTION Description
// This filter give a input(vtkHM1DStraightModelGrid), and make a new 1DModel,
// generating in your output an vtkHM1DStraightModelGrid.


#define REMOVE_MODE 0
#define KEEP_MODE   1

#include "vtkDataSetAlgorithm.h"

class vtkHM1DStraightModel;
class vtkHM1DStraightModelGrid;

class vtkIdList;
class vtkDoubleArray;

class VTK_EXPORT vtkHM1DKeepRemoveFilter : public vtkDataSetAlgorithm
{
public:

  static vtkHM1DKeepRemoveFilter *New();
  vtkTypeRevisionMacro(vtkHM1DKeepRemoveFilter,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Return a vtkHM1DStraightModelGrid...
  vtkHM1DStraightModelGrid* GetOutput(int port);

  void SetInput(vtkHM1DStraightModelGrid *input);

  // Description:
  // Set list of segments selected.
  void SetSegmentsSelected(vtkIdList *list);

  vtkSetMacro(KeepRemoveMode, int);
  vtkGetMacro(KeepRemoveMode, int);

  vtkSetMacro(CardiacCycleTime, float);
  vtkGetMacro(CardiacCycleTime, float);

  vtkSetMacro(EquivalentResistance, double);
  vtkGetMacro(EquivalentResistance, double);

  vtkSetMacro(EquivalentCapacitance, double);
  vtkGetMacro(EquivalentCapacitance, double);

  void SetOutput(vtkHM1DStraightModelGrid *output);

  void SetDataOutTimeArray(const char *array);
  const char *GetDataOutTimeArray();

protected:

  vtkHM1DKeepRemoveFilter();
  ~vtkHM1DKeepRemoveFilter();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);

  // Description:
  // Sets output object type
  int FillOutputPortInformation(int, vtkInformation*);

  vtkHM1DStraightModel* StraightModel;

  // Description:
  // Ids of segments to remove.
  vtkIdList *SegementRemoveList;

  // Description:
  // Mode selected.
  // 0 is remove and 1 is keep.
  int KeepRemoveMode;

  // Description:
  // Cardiac cycle time configured in heart curve.
  float CardiacCycleTime;

  // Description:
  // Equivalent resistance to configurate terminal when is used REMOVE_MODE.
  double EquivalentResistance;

  // Description:
  // Equivalent capacitance to configurate terminal when is used REMOVE_MODE.
  double EquivalentCapacitance;

  vtkDoubleArray *TimeArray;

private:
  vtkHM1DKeepRemoveFilter(const vtkHM1DKeepRemoveFilter&);  // Not implemented.
  void operator=(const vtkHM1DKeepRemoveFilter&);  // Not implemented.
};

#endif /* VTKHM1DKEEPREMOVEFILTER_H_ */
