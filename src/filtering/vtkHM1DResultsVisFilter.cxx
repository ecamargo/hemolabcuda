/*
 * $Id$
 */

#include "vtkHM1DResultsVisFilter.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DSegment.h"
#include "vtkCallbackCommand.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkCellArray.h"
#include "vtkCommand.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkDataSet.h"
#include "vtkMath.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"
#include "vtkTriangleStrip.h"
#include "vtkPriorityQueue.h"
#include "vtkProcessModule.h"
#include "vtkClientServerID.h"
#include "vtkClientServerStream.h" 
#include "vtkExecutive.h"
#include "vtkCompositeDataSet.h"
#include "vtkMultiGroupDataSet.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkHM1DStraightModel.h"
#include "vtkIntArray.h"
#include "vtkCollection.h"
#include "vtkHMUtils.h"
#include "vtkIdList.h"
#include "vtkPVConnectivityFilter.h"
#include "vtkHM1DTerminal.h"


#include "vtkRectilinearGrid.h"


#include <string>
#include <iostream>

vtkCxxRevisionMacro(vtkHM1DResultsVisFilter, "$Revision: 1.67 $");
vtkStandardNewMacro(vtkHM1DResultsVisFilter);

//-------------------------------------------------------------------------
vtkHM1DResultsVisFilter::vtkHM1DResultsVisFilter()
{
  //this->DebugOn();
  
  this->InputEnabled = -1;
  this->portIndex = 0;
  //this->SetNumberOfOutputPorts(1);
  this->SetNumberOfInputPorts(10);  
  //this->SelectedGroup = 1; 
  //this->CouplingArrayInfo = NULL;
  this->NumberOfMeshData = 0;   
  
  // marcadas com -1 para indicar que as mesmas nao foram atualizadas
//  this->StraightModelInputNumber = -1;
//  this->MeshDataInputNumber = -1;
  
  this->GenerationStatus = 0 ;
  
  this->StraightModelInputNumber = 0;
  
  this->StraightModelGrid = NULL;
  this->numInputs = 0;
  
  
  
  this->auxSegments = NULL;
  this->segmentsList = NULL;
  this->segmentsCollection = NULL;
 // this->Initialpoint = 0;
  
  this->terminalsCollection = NULL;

  
}

//-------------------------------------------------------------------------
vtkHM1DResultsVisFilter::~vtkHM1DResultsVisFilter()
{
  if (this->auxSegments)
    this->auxSegments->Delete();
  
  if (this->segmentsList)
    this->segmentsList->Delete();
  
  if (this->segmentsCollection)
    this->segmentsCollection->Delete();
  
  
  if (this->terminalsCollection)
    this->terminalsCollection->Delete();

}

//----------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::SetInputEnabled(int value)
{
  this->InputEnabled=value;
  this->Modified();
  this->Update();
}

//-------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::SetNumberOfInputConnections(int port, int n)
{
}

//-------------------------------------------------------------------------
int vtkHM1DResultsVisFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{ 
  vtkDebugMacro(<<"*********** vtkHM1DResultsVisFilter::RequestData ***********");

  vtkInformation *inInfo;
  
//  this->NumberOf3DValidInputs = 0;
  
  vtkInformation* info = outputVector->GetInformationObject(0);
  vtkMultiGroupDataSet *output = vtkMultiGroupDataSet::SafeDownCast(
    info->Get(vtkCompositeDataSet::COMPOSITE_DATA_SET()));
  if (!output) return 0;

  unsigned int updatePiece = static_cast<unsigned int>(
    info->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER()));
//  unsigned int updateNumPieces =  static_cast<unsigned int>(
//    info->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES()));

  int numInputs = inputVector[0]->GetNumberOfInformationObjects();
  
  // se nao existem entradas aborta a passagem pelo RequestData
  if (!numInputs)
    return 0;
  
  
  int NumberOf1DModels = 0; // esta variavel é usada para contar o numero de modelos 1D - se tiver mais de um, há erro nos elementos de entrada do filtro


  // Once the number of 1D models is ok; i have to serach for this one
  // this search is done just once
  if (!this->StraightModelGrid)
    {
    //for (int i = 0; i < numInputs; ++i) 
      //{
      inInfo = inputVector[0]->GetInformationObject(0);   
      
      if (vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())))
        {
        this->StraightModelGrid = vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
        //break;
        }
      //}
    }
 
  
  if (!this->StraightModelGrid)
    {
    vtkErrorMacro("1D model not found in the vtkHM1DResultsVisFilter input list.");
    return 0;
    }
  

  output->SetNumberOfGroups(numInputs);
  
  if(this->InputEnabled == 0)
    {
    //seta na saida do filtro o modelo 1D
    output->SetDataSet(0, updatePiece, this->StraightModelGrid);
    }
  
  if (!this->numInputs)
    this->numInputs = numInputs;
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::Set1DInputNumber(int val)
{
  this->StraightModelInputNumber = val;
}

//----------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::Set3DInputNumber(int val)
{
  this->MeshDataInputNumber = val;  
}

//----------------------------------------------------------------------------  
void vtkHM1DResultsVisFilter::AddInput(vtkDataObject* input)
{
  this->AddInput(0, input);
}

//----------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::AddInput(int index, vtkDataObject* input)
{
  if(input) this->AddInputConnection(index, input->GetProducerPort());
}

//----------------------------------------------------------------------------
int vtkHM1DResultsVisFilter::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  info->Set(vtkAlgorithm::INPUT_IS_REPEATABLE(), 1);
  info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return 1;
}

//----------------------------------------------------------------------------

//-------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::SetReleaseDataFlag(int i)
{
  (i == 1) ? this->ReleaseDataFlagOn() : this->ReleaseDataFlagOff();
}

//-------------------------------------------------------------------------
void vtkHM1DResultsVisFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------


void vtkHM1DResultsVisFilter::GenerateFile(char *path)
{
 
  // inicia o metodo recurssivo para gerar lista dos Ids de um caminho que acaba no terminal final
  // ARMAZENA OS IDS NA PILHA Ids
  this->GenerateIDList(EndTerminal);
  
  //cout << "gerando arquivo com grid retangular do segmento --> " << BeginTerminal << endl;
 
  vtkHM1DStraightModel::TreeNodeDataMap segmentsMap;
  vtkHM1DStraightModel::TreeNodeDataMap::iterator iteratorMap;
  segmentsMap = StraightModelGrid->GetStraightModel()->GetSegmentsMap();
  vtkHM1DSegment* segment;
  vtkIdType* idType;
     
  vtkDoubleArray* ResolutionArray;
  int NumberOfComponents;
  double* scalars;

  char filename[512];
  char intermediaryResult[256];
  
  if (!this->segmentsList)
    {
    this->segmentsList = vtkIdList::New(); 
    segmentsList->SetNumberOfIds(Segment_stack.size());
    }
  this->segmentsList->Reset();
  
  if (!this->segmentsCollection)
    this->segmentsCollection = vtkCollection::New();

  this->segmentsCollection->RemoveAllItems();
  
  
  if (!this->terminalsCollection)
    this->terminalsCollection= vtkCollection::New();
  
  this->terminalsCollection->RemoveAllItems();
  
  
  

  
  
  int x=0;
  int SegmentNumberOfNode; // numero de nodes do segmento atual
  int SegmentNumber = Segment_stack.size();
  
  int TimeStepNumber = 0; 
  int dofs = 0;
  
  
  
  if (!this->auxSegments)
    {
    this->auxSegments = vtkIdList::New();
    this->auxSegments->SetNumberOfIds(SegmentNumber);
    }
  this->auxSegments->Reset();
  
  
  
  
  for (int i=0; Segment_stack.size(); ++i) 
    {
    segmentsCollection->AddItem(Segment_stack.top());
    
    
    if (i)
      {
      this->auxSegments->InsertId(i, auxSegments->GetId(i-1) + Segment_stack.top()->GetNodeNumber()-1);
      //cout << "aux Seg "<< auxSegments->GetId(i-1) + Segment_stack.top()->GetNodeNumber()-1 << endl;
      }
    else
      {
      //cout << "Aux seg " << Segment_stack.top()->GetNodeNumber()-1 << endl;
      this->auxSegments->InsertId(i, Segment_stack.top()->GetNodeNumber()-1);
      }
    
    if (!TimeStepNumber)
      {
      if (!Segment_stack.top()->GetResolutionArray(0))
        {
        // vtkErrorMacro("Sem dados de saida - verifique seu dataout.txt");
        this->GenerationStatus = 0;
        return;
        }
    
      TimeStepNumber = Segment_stack.top()->GetResolutionArray(0)->GetNumberOfTuples();
      }
    if (!dofs)
      dofs = Segment_stack.top()->GetResolutionArray(0)->GetNumberOfComponents();
    
    
    SegmentNumberOfNode = Segment_stack.top()->GetNodeNumber() - 2; // desprezando o primeiro e ultimo nó 
    x= (x + SegmentNumberOfNode);
    
    segmentsList->InsertId(i, Segment_stack.top()->GetId());  
    Segment_stack.pop();
    }
  
  
  x = x +Terminal_stack.size();
  
  for (int i=0; Terminal_stack.size(); ++i) 
    {
    this->terminalsCollection->AddItem(Terminal_stack.top());
    //segmentsList->InsertId(i, Segment_stack.top().top());  
    Terminal_stack.pop();
    }
  
  
  
  
  

  
  

  

  
// 
//  
//  // percorre cada id da pilha e insere na ordem correta (proximal para distal) na id list segmentsList
//  // insere tambem o objeto do tipo vtkHM1DSegmento na collection segmentsCollection
//  for (int i=0; Ids.size(); ++i) 
//    {
//    //cout <<" segmentos precorridos " << Ids.top() << endl;
//  
//    segmentsList->InsertId(i, Ids.top());  
//    Ids.pop();
//    
//    // para cada segmento id que acha na pilha, procura ele na lista de segmento e obtem numero de tuplas (1 vez só),
//    // numero de DOFs (operacao unica tambem) e numero de nodes do segmento - a variavel x irá dar o numero total de nodes do conjunto
//    // de segmentos
//  
//    for(iteratorMap = segmentsMap.begin(); iteratorMap != segmentsMap.end(); iteratorMap++ )
//       {
//       segment = vtkHM1DSegment::SafeDownCast(iteratorMap->second);
//       
//       if (!TimeStepNumber)
//         {
//         if (!segment->GetResolutionArray(0))
//           {
//           // vtkErrorMacro("Sem dados de saida - verifique seu dataout.txt");
//            this->GenerationStatus = 0;
//            return;
//           
//           }
//         
//         TimeStepNumber = segment->GetResolutionArray(0)->GetNumberOfTuples();
//         }
//       
//       if (!dofs)
//         dofs = segment->GetResolutionArray(0)->GetNumberOfComponents();
//      
//       if (segment->GetId() == segmentsList->GetId(i))
//         {
//         segmentsCollection->AddItem(segment);
//         //cout << "Inserindo Segmento na collection "<< segment->GetId() << endl;
//         
//         SegmentNumberOfNode = segment->GetNodeNumber() - 2; // desprezando o primeiro e ultimo nó 
//         
//         
//         
//         x= (x + SegmentNumberOfNode); // pega o numero de nodes de um segmento, com exceção do último
//         
//         if (i)
//           {
//           /*
//           supondo que temos tres segmentos: s1 com 7 nos, s2 com 9 e s3 com 3.
//           A lista tera o conteudo: 
//           lista (7, 16, 19)
//           
//           Essa lista é usada na hora de identificar qual segmento corresponde a uma dada coluna
//           do grid. (o metodo responsavel é o vtkDoubleArray *vtkHM1DResultsVisFilter::GetRightResolutionArray(int x))
//           que já retorna o ResolutionArray do segmento. 
//           
//           Exemplo: a coluna 6 corresponde ao s1; a coluna 21 corresponde ao s2 
//           */
//           this->auxSegments->InsertId(i, auxSegments->GetId(i-1) + SegmentNumberOfNode);
//           cout << "Inserindo nodenumebr total " <<  this->auxSegments->GetId(i) << endl;
//           
//           }
//         else // para i = 0
//           {
//           this->auxSegments->InsertId(i, SegmentNumberOfNode);
//           cout << "Inserindo nodenumebr total " <<  this->auxSegments->GetId(i) << endl;
//           }
//         }
//       }
//  
//  
//  }
//  cout << "Numero de nodes totais " << x << endl;



  
  double dx; 
  int temp2 = 0;
  double *coords;
     
  // Create a rectilinear grid by defining three arrays specifying the
  // coordinates in the x-y-z directions.
  vtkFloatArray *xCoords = vtkFloatArray::New();
  xCoords->SetNumberOfValues(x); 
   
  for (int j=0; j< SegmentNumber; j++) // de 0 ate o numero de segmentos do conjunto
    {
    segment = vtkHM1DSegment::SafeDownCast(segmentsCollection->GetItemAsObject(j));
    //
    dx = (segment->GetLength() / segment->GetNumberOfElements());
    
    if (!j)
      SegmentNumberOfNode = segment->GetNodeNumber();
    else
      SegmentNumberOfNode = segment->GetNodeNumber() -1;
      
    
//    cout << "numero de nodes Segment " << SegmentNumberOfNode << endl;
//    cout << "Varia no loop " << "de " << temp2 << " até "<< SegmentNumberOfNode+temp2 << endl;
            
    for (int i=temp2; i<SegmentNumberOfNode+temp2; i++) 
      {
      if (i)
        {
//        cout << "Setando valor X[" <<i << "]: "<< xCoords->GetValue(i-1) + dx << endl;
        xCoords->SetValue(i, (xCoords->GetValue(i-1) + dx ));
        }
      else
        {
//        cout << "Setando valor X[ "<<i <<"]: "<< 0 << endl;
        xCoords->SetValue(i,0.0);
        }
      }
    temp2 = temp2 + SegmentNumberOfNode;
    }
   

  
  vtkFloatArray *yCoords = vtkFloatArray::New();
  yCoords->SetNumberOfValues(TimeStepNumber);
  
  for (int i=0; i<TimeStepNumber; i++) 
     yCoords->SetValue(i,i);
   
  vtkFloatArray *zCoords = vtkFloatArray::New();
  
  for (int i=0; i<1; i++) zCoords->InsertNextValue(0);
   
   vtkRectilinearGrid *rgrid = vtkRectilinearGrid::New();
   rgrid->SetDimensions(x,TimeStepNumber,1);
   rgrid->SetXCoordinates(xCoords);
   rgrid->SetYCoordinates(yCoords);
   rgrid->SetZCoordinates(zCoords);

 
   //BTX
   std::string result2 = "";
   //ETX
   
   sprintf(filename, "%s%s%s%d_%d%s", path, "/", "segments_", BeginTerminal, EndTerminal, ".vtk");
   
   std::ofstream out(filename);
   
   
   out << "# vtk DataFile Version 3.0\n";
   out << "vtk output\n";
   out << "ASCII\n";
   out << "DATASET STRUCTURED_GRID\n";
   
   out << "DIMENSIONS "<< x << " "<< TimeStepNumber << " " << 1 << "\n";
   out << "POINTS "<< (x * TimeStepNumber) << " float\n";
   
   //Gravando coordenadas.
   for(int j=0; j < rgrid->GetNumberOfPoints(); j++)
     {
     coords = rgrid->GetPoint(j);
     sprintf(intermediaryResult, "%f %f %f \n", coords[0], coords[1], coords[2]);
     result2.append(intermediaryResult);
     }
 
   int pontos = (x * TimeStepNumber);
     
   out << result2; 
     
   vtkFloatArray *temp = vtkFloatArray::New();
   
   temp->SetNumberOfTuples(pontos);
   
  //for(int i=0; i < rgrid->GetNumberOfPoints(); i++)
   //   temp->SetValue(i, 0.0);
   temp->Reset();

   
   int TimeSteps = segment->GetResolutionArray(0)->GetNumberOfTuples();
        
          
   out << "POINT_DATA " <<  pontos << "\n";
   //out << "FIELD FieldData " << dofs << "\n";
   out << "FIELD FieldData " << 3 << "\n"; // sempre 3 dofs ;
   
   bool tuple3;
     
   int value = 0;  

   
   
   int Valid4DOF[3] = {0,2,3};
   int Valid3DOF[3] = {0,1,2};
   //int DOFCounter = 0;
   
   
   
   
   for (int z=0; z<3; z++)
     {

     
     for (int j = 0; j < x; j++)
       {
       //cout << "*********** PEGANDO ResolutionArray "<<j << endl;
       vtkDoubleArray *Resolution = NULL;
       Resolution = GetRightResolutionArray(j);
        
       if (!Resolution)
         {
         vtkErrorMacro("Sem dados de saida - verifique seu dataout.txt");
         this->GenerationStatus = 0;
         return;
         }
         
       if (Resolution->GetNumberOfComponents()==3)
         tuple3 = 1;
       else
         tuple3 = 0;

       value = 0;
       
       if (tuple3)       
         { 
         for(int i= j; i < pontos; i= i + x)
           {
             temp->SetValue(i, Resolution->GetTuple3(value)[Valid3DOF[z]]);
             //temp->SetValue(i,0); 
             //cout << "Setando na posicao " << i  << " Resolution->GetTuple3("<< value << ")"<< "["<<z<<"]::: "<< Resolution->GetTuple3(value)[z] << endl; 
             value++;
           }
         }
       else // para 4 dofs // só deve acessar DOfs 0(flow) 2(area) e 2(pressure) 
         {
         

         for(int i= j; i < pontos; i=i+x)
           {
           temp->SetValue(i, Resolution->GetTuple4(value)[Valid4DOF[z]]);
           //cout << "Setando na posicao " << i  << " "<< Resolution->GetTuple4(value)[z] << endl; 
           value++;
           }
         }  
       //cout << "**********************************************************" << endl;
       }     
     
     
     
     if (!z)
       out << "Flow_rate 1 "<< pontos << " float\n";
     
     if (tuple3) // 0 1 2
       {
       if (z==1)
         out << "\nArea 1 "<< pontos << " float\n";
  
       if (z==2)
          out << "\nPressure 1 "<< pontos << " float\n";
       
       }
     else
       {
       if (Valid4DOF[z]==2)
         out << "\nArea 1 "<< pontos << " float\n";
       if (Valid4DOF[z]==3)
         out << "\nPressure 1 "<< pontos << " float\n";
       
       }
       
     
     for(int j=0; j < rgrid->GetNumberOfPoints(); j++)
         out << temp->GetValue(j) <<" " ;
     
     result2 = "";

     //     for(int i=0; i < rgrid->GetNumberOfPoints(); i++)
//        temp->SetValue(i, 0.0);
     //temp->Reset();
     }
     
   
   out.close();  
 
   temp->Delete();
   xCoords->Delete();
   yCoords->Delete();
   zCoords->Delete();
   rgrid->Delete();
   
   
   this->GenerationStatus = 1; // gerou com sucesso
}

//----------------------------------------------------------------------------


int vtkHM1DResultsVisFilter::GetGenerationStatus()
{
  return this->GenerationStatus;
}
//----------------------------------------------------------------------------

void vtkHM1DResultsVisFilter::GenerateIDList(int id)
{
  //cout << "Visitando ID "<< id << endl;
  
  vtkHM1DTreeElement *elem = NULL;
//  vtkHM1DTreeElement *child = NULL;
  vtkHM1DTreeElement *parent = NULL;
  
  if (StraightModelGrid->GetStraightModel()->IsSegment(id)) // se atual elemento da arvore é um segmento
    {
    //Ids.push(id); // só insere na pilha somente os ids de segmentos ******************

    
    
//    if (id == BeginTerminal )
//      return;

    elem = StraightModelGrid->GetStraightModel()->GetSegment(id);
    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
    
    Segment_stack.push(Seg);

    
    
    this->GenerateIDList(Seg->GetFirstParent()->GetId());
    }
  else if (StraightModelGrid->GetStraightModel()->IsTerminal(id)) // se atual elemento da arvore é um terminal
    {
    vtkHM1DTerminal *term = StraightModelGrid->GetStraightModel()->GetTerminal(id);
    
    if (id == BeginTerminal )
      {
      Terminal_stack.push(term);
      return;
      }
    
//    if (!term->GetFirstParent())  // se é o coracao
//      {
//      // então o ID que iniciou o metodo recursivo é o ponto 1
//      return;
//      }
//    else
    if (term->GetFirstParent())
      {// se o terminal tem pai
      Terminal_stack.push(term);
      this->GenerateIDList(term->GetFirstParent()->GetId());
      }
    } // fim else
}


//----------------------------------------------------------------------------

vtkDoubleArray *vtkHM1DResultsVisFilter::GetRightResolutionArray(int x)
{
//  cout << "GetRightResolutionArray(int x) " << x << endl;
  int SegmentsNumber = this->auxSegments->GetNumberOfIds();
  
  
  if (this->auxSegments->GetId(SegmentsNumber -1) == x)
    {
    int i;
    
//    cout << "Este é o ultimo no " << x << endl;
    
    i = SegmentsNumber -1;
    vtkHM1DSegment *segment = vtkHM1DSegment::SafeDownCast(segmentsCollection->GetItemAsObject(i));
    return segment->GetResolutionArray(x-this->auxSegments->GetId(i-1));
    }
  
    for (int i=0; i < this->auxSegments->GetNumberOfIds(); i++)
      {
      if (x < this->auxSegments->GetId(i) )
        {
        vtkHM1DSegment *segment = vtkHM1DSegment::SafeDownCast(segmentsCollection->GetItemAsObject(i));
      
        if (i)
          {
//          cout << "ResolutionArray de Segmento do auxSegment " << i << " node "<< x-this->auxSegments->GetId(i-1)  << endl;
          return segment->GetResolutionArray(x-this->auxSegments->GetId(i-1));
          }
        else
          {
//          cout << "ResolutionArray de Segmento do auxSegment " << i << " node "<< x << endl;
          return segment->GetResolutionArray(x);
          }
        }
      }
//cout << "*******************" << endl;
}
//----------------------------------------------------------------------------
