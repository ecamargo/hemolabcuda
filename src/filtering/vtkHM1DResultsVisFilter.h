/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM1DResultsVisFilter

=========================================================================*/
// .NAME vtkHM1DResultsVisFilter
// .SECTION Description
// Filter that receives two or more inputs: One StraightModel and one or more 3D Fullmodels. These will be used to create
// the Solver Input Files from the Coupled Model.

#ifndef __vtkHM1DResultsVisFilter_h
#define __vtkHM1DResultsVisFilter_h

#include "vtkHM1DStraightModelGrid.h"
#include "vtkMultiGroupDataSetAlgorithm.h"
#include "vtkPolyData.h"
#include <string>
#include <iostream>
#include <stack>
using namespace std;
class vtkIntArray;
class vtkHM3DFullModel;
class vtkCollection;

typedef stack<vtkHM1DSegment*> SegmentStack;
typedef stack<vtkHM1DTerminal*> TerminalStack;


class VTK_EXPORT vtkHM1DResultsVisFilter : public vtkMultiGroupDataSetAlgorithm 
{
public:
  
  vtkTypeRevisionMacro(vtkHM1DResultsVisFilter,vtkMultiGroupDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkHM1DResultsVisFilter *New();
  
  // Release Data Flag  
  void SetReleaseDataFlag(int i);

  // Description:
  // Add an input of this algorithm.  Note that these methods support
  // old-style pipeline connections.  When writing new code you should
  // use the more general vtkAlgorithm::AddInputConnection().  See
  // SetInput() for details.
  void AddInput(vtkDataObject *);
  void AddInput(int, vtkDataObject*);

  // Description:
  // Controls which input was enabled on the interface (PV)
  void SetInputEnabled(int value);
  
  // Description:
  
  // Description:
  // Description:
  // Set Selected Group
  //void  SelectGroup(int gr);

  // Description:
  // Callback for "1DInputNumber" and "3DInputNumber" properties
  void Set1DInputNumber(int );
  void Set3DInputNumber(int );
  

  // Description:
  // creates the preprocessor which will run the 1D and 3D models and create the solver input files
  void GenerateFile(char *path);
  
  
  // Description:
  // returns if the generation of solver input files were (1) or not (0) successfull
  int GetGenerationStatus();
  
  
  vtkSetMacro(BeginTerminal, int);
  vtkGetMacro(BeginTerminal, int);
  

  vtkSetMacro(EndTerminal, int);
  vtkGetMacro(EndTerminal, int);
  
    
  
  // Description:
  // anda para cima na arvore até encontrar o terminal inicial ou até encontrar o coração
  void GenerateIDList(int id);
  
  //Description:
  // retorna o resolution array respectivo a uma detrminada coluna do grid
  vtkDoubleArray *GetRightResolutionArray(int x);

//***************************************************************************************** 
//***************************************************************************************** 
//***************************************************************************************** 

protected:
  vtkHM1DResultsVisFilter();
  ~vtkHM1DResultsVisFilter();

  // Description:
   // Set where the Straight model and Meshdata are located (input 0 or input 1)
  int StraightModelInputNumber; // TO BE DELETED
  int MeshDataInputNumber;    // TO BE DELETED
  int NumberOfMeshData;

  // Description:  
  // Fill Input Information.
  int FillInputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
    
  // Description:    
  // Set Number of Input Connections
  void SetNumberOfInputConnections(int port, int n);

  // Description:    
  // Variable to control current input
  int portIndex;
  

  // Description:
  // Current input enabled
  int InputEnabled;
  
  // Description:
  // controls if generation of solver files was ok or not
  int GenerationStatus;
  
  // Description:
  // reference to 1D model
  vtkHM1DStraightModelGrid *StraightModelGrid;
  
  // Description:
  // control the total number of inputs from this filter
  int numInputs; 
  
  
int BeginTerminal;
int EndTerminal;
  


private:
  vtkHM1DResultsVisFilter(const vtkHM1DResultsVisFilter&);  // Not implemented.
  void operator=(const vtkHM1DResultsVisFilter&);  // Not implemented.

  string listChar; 
 
  SegmentStack Segment_stack;
  TerminalStack Terminal_stack;
  
  vtkIdList *auxSegments;
  vtkIdList *segmentsList;
  vtkCollection *segmentsCollection;
  vtkCollection *terminalsCollection;

};

#endif
