/*
 * $Id$
 */

#include "vtkHM1DStraightModelEditor.h"

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"

#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDataSet.h"
#include "vtkDemandDrivenPipeline.h"
#include "vtkExecutive.h"
#include "vtkFloatArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkStreamingDemandDrivenPipeline.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelEditor, "$Rev$");
vtkStandardNewMacro(vtkHM1DStraightModelEditor);

//----------------------------------------------------------------------------
// Constructor
vtkHM1DStraightModelEditor::vtkHM1DStraightModelEditor()
{
	this->StraightModel = vtkHM1DStraightModel::New();
	this->StraightModel->SetName("StraightModelEditor");


	// Criando o StraightModelGrid (output)
	vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::New();
	this->SetOutput(output);
	// Releasing data for pipeline parallism.
	// Filters will know it is empty.
	output->ReleaseData();
	output->Delete();

	//  Criando vtkPoints
	this->Points = vtkPoints::New();

	//  Criando vtkCellArray para Linhas
	this->Segments = vtkCellArray::New();

	// Criando vtkCellArray para Vertices
	this->Nodes = vtkCellArray::New();

}

//----------------------------------------------------------------------------
// Destructor
vtkHM1DStraightModelEditor::~vtkHM1DStraightModelEditor()
{
	this->Points->Delete();
	this->Segments->Delete();
	this->Nodes->Delete();
	this->StraightModel->Delete();
}


//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelEditor::GetOutput()
{
  return this->GetOutput(0);
}
//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelEditor::GetOutput(int port)
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(port));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelEditor::SetOutput(vtkDataObject* d)
{
  this->GetExecutive()->SetOutputData(0, d);
}

/*
//----------------------------------------------------------------------------
vtkDataObject* vtkHM1DStraightModelEditor::GetInput()
{
  return this->GetInput(0);
}
//----------------------------------------------------------------------------
vtkDataObject* vtkHM1DStraightModelEditor::GetInput(int port)
{
  return this->GetExecutive()->GetInputData(port, 0);
}



//----------------------------------------------------------------------------
void vtkHM1DStraightModelEditor::AddInput(vtkHM1DStraightModelGrid* input)
{
  this->AddInput(0, static_cast<vtkHM1DStraightModelGrid*>(input));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelEditor::AddInput(int index, vtkHM1DStraightModelGrid* input)
{
  this->AddInput(index, static_cast<vtkHM1DStraightModelGrid*>(input));
}

*/

int vtkHM1DStraightModelEditor::ProcessRequest(
  vtkInformation* request,
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  // generate the data
  if(request->Has(vtkDemandDrivenPipeline::REQUEST_DATA()))
    {

    return this->RequestData(request, inputVector, outputVector);
    }

  // create the output
  if(request->Has(vtkDemandDrivenPipeline::REQUEST_DATA_OBJECT()))
    {

    return this->RequestDataObject(request, inputVector, outputVector);
    }

  // execute information
  if(request->Has(vtkDemandDrivenPipeline::REQUEST_INFORMATION()))
    {

    return this->RequestInformation(request, inputVector, outputVector);
    }

  // set update extent
 if(request->Has(vtkStreamingDemandDrivenPipeline::REQUEST_UPDATE_EXTENT()))
    {

    return this->RequestUpdateExtent(request, inputVector, outputVector);
    }


  return this->Superclass::ProcessRequest(request, inputVector, outputVector);

}

/*
//----------------------------------------------------------------------------
void vtkHM1DStraightModelEditor::SetInput(vtkDataObject* input)
{
  this->SetInput(0, static_cast<vtkHM1DStraightModelGrid*>(input));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelEditor::SetInput(int index, vtkDataObject* input)
{
  this->SetInput(index, static_cast<vtkHM1DStraightModelGrid*>(input));
}
*/





/*
void vtkHM1DStraightModelEditor::SetSource(vtkHM1DStraightModelGrid *source)
{
  this->SetInput(1, source);
}

vtkHM1DStraightModelGrid *vtkHM1DStraightModelEditor::GetSource()
{
  if (this->GetNumberOfInputConnections(1) < 1)
    {
    return NULL;
    }
  return vtkHM1DStraightModelGrid::SafeDownCast(
    this->GetExecutive()->GetInputData(1, 0));
}
*/



//----------------------------------------------------------------------------
int vtkHM1DStraightModelEditor::FillInputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataSetAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  return 1;
}
/*
//----------------------------------------------------------------------------
int vtkHM1DStraightModelEditor::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  // now add our info
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
  return 1;
}
*/

/*
//----------------------------------------------------------------------------
int vtkHM1DStraightModelEditor::RequestInformation(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **vtkNotUsed(inputVector),
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  if ( outInfo->Has(vtkStreamingDemandDrivenPipeline::TIME_STEPS()) )
    {
    this->NumberOfTimeSteps =
      outInfo->Length( vtkStreamingDemandDrivenPipeline::TIME_STEPS() );
    }
  else
    {
    this->NumberOfTimeSteps = 0;
    }


  return 1;
}
*/

int vtkHM1DStraightModelEditor::RequestUpdateExtent(vtkInformation* vtkNotUsed(request),
	vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{

	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkHM1DStraightModelGrid *input = vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if ( this->StraightModel->GetMaxId() == 0)
    this->StraightModel->DeepCopy( input->GetStraightModel() );

  output->SetStraightModel( this->StraightModel );
  output->CopyStructure( input );
  // Update self
  output->GetPointData()->PassData(input->GetPointData());
  output->GetCellData()->PassData(input->GetCellData());


	return 1;
}


//----------------------------------------------------------------------------
//
// Convert position along ray into scalar value.  Example use includes
// coloring terrain by elevation.
//
int vtkHM1DStraightModelEditor::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
	 // get the info objects


  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkHM1DStraightModelGrid *input = vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));


	if ( !input )
	{
		vtkErrorMacro(<<"Invalid input!");
		return 1;
	}
	if ( !output )
	{
		vtkErrorMacro(<<"Invalid output!");
		return 1;
	}

	if ( input->GetNumberOfPoints() < 1 )
	{
		vtkDebugMacro(<< "Input no contains data!");
		return 1;
	}


	// Initialize
  //
  vtkDebugMacro(<<"Copying input to output");

  // First, copy the input to the output as a starting point
	output->CopyStructure( input );
  //output->SetStraightModel( this->StraightModel );

  // Update self
  output->GetPointData()->PassData(input->GetPointData());
  output->GetCellData()->PassData(input->GetCellData());

/*
//========================================================================
	int rootId = 1;
	this->WalkStraightModelTree(rootId);
	output->SetPoints(this->Points);
	output->SetLines(this->Segments);
	int firstId = 0;
	this->Nodes->InsertNextCell(1, &firstId);
	output->SetVerts(this->Nodes);
//========================================================================
*/


  return 1;
}


//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeDataEditor(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] = -0.5;
	node0->coords[1] =  0.0;

	tmpSegm->SetNodeData(0,*node0);
	delete node0;

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = -0.5;
	node1->coords[1] =  0.25;

	tmpSegm->SetNodeData(1,*node1);
	delete node1;

	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = -0.5;
	node2->coords[1] =  0.5;

	tmpSegm->SetNodeData(2,*node2);
	delete node2;
}

void vtkHM1DStraightModelEditor::AddSegment(int parent)
{
	if ( parent < 1 )	return;



	vtkHM1DTerminal *root = (vtkHM1DTerminal *)this->StraightModel->Get1DTreeRoot();

	this->StraightModel->AddSegment(root->GetId());

	vtkHM1DSegment *tmpSegm =  this->StraightModel->GetSegment(this->StraightModel->Get1DTreeRoot()->GetFirstChild()->GetId());
	tmpSegm->SetNumberOfElements(2);
	AddNodeDataEditor(tmpSegm);

	this->Modified();
}




//----------------------------------------------------------------------------
// Adicionar segmentos
void vtkHM1DStraightModelEditor::AddSegment(vtkHM1DSegment* s)
{
	vtkIdType idPts[2];
	int nPoints = s->GetElementNumber()+1; // Number of nodes
	vtkIdType idNode;

	if ( nPoints > 0 )
	{
		double p[3];
		p[0] = s->GetNodeData(0)->coords[0];
		p[1] = s->GetNodeData(0)->coords[1];
		p[2] =    0;
		idPts[0] = this->Points->InsertNextPoint(p);

		for (int i=1; i<nPoints; i++)
		{
			double pt[3];
			pt[0] = s->GetNodeData(i)->coords[0];
			pt[1] = s->GetNodeData(i)->coords[1];
			pt[2] =    0;
			idPts[1] = this->Points->InsertNextPoint(pt);

			this->Segments->InsertNextCell(2,idPts);

			idPts[0] = idPts[1];
		}
	}

	idNode = idPts[1];
	this->Nodes->InsertNextCell(1, &idNode);
}



//----------------------------------------------------------------------------
// Percorrer o StraightModel para gerar segmentos
void vtkHM1DStraightModelEditor::WalkStraightModelTree( int &id )
{
	vtkHM1DTreeElement *elem;
	vtkHM1DTreeElement *child;

	if (this->StraightModel->IsSegment(id))
	{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		this->AddSegment( vtkHM1DSegment::SafeDownCast( elem ) );
	}
	else if (this->StraightModel->IsTerminal(id))
	{
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
	}

	if (!child)
	{
		// É uma folha
		return;
	}
	else
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
		{
			id = child->GetId();
			child = elem->GetNextChild();
			this->WalkStraightModelTree(id);
		}
}


void vtkHM1DStraightModelEditor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
