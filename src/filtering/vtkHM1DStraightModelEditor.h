/*
 * $Id$
 */


// .NAME vtkHM1DStraightModelEditor - 
// .SECTION Description
// vtkHM1DStraightModelEdtor is .... tenho que colocar algum conetário aqui.


#ifndef _vtkHM1DStraightModelEditor_h_
#define _vtkHM1DStraightModelEditor_h_

#include "vtkDataSetAlgorithm.h"

class vtkHM1DStraightModelGrid;
class vtkHM1DSegment;
class vtkPoints;
class vtkCellArray;
class vtkHM1DStraightModel;

class VTK_EXPORT vtkHM1DStraightModelEditor : public vtkDataSetAlgorithm 
{
public:
  static vtkHM1DStraightModelEditor *New();
  vtkTypeRevisionMacro(vtkHM1DStraightModelEditor,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Get the output data object for a port on this algorithm.
	vtkHM1DStraightModelGrid* GetOutput();
	vtkHM1DStraightModelGrid* GetOutput(int);
	
	void SetOutput(vtkDataObject* d);
	
	// Description:
  // Get the input data object. This method is not recommended for use, but
  // lots of old style filters use it.
//  vtkDataObject* GetInput();
  

/*  
  // Description:
  // Set an input of this algorithm. You should not override these
  // methods because they are not the only way to connect a pipeline.
  // Note that these methods support old-style pipeline connections.
  // When writing new code you should use the more general
  // vtkAlgorithm::SetInputConnection().  These methods transform the
  // input index to the input port index, not an index of a connection
  // within a single port.
  void SetInput(vtkDataObject*);
  void SetInput(int, vtkDataObject*);


	// Description:
  // Add an input of this algorithm.  Note that these methods support
  // old-style pipeline connections.  When writing new code you should
  // use the more general vtkAlgorithm::AddInputConnection().  See
  // SetInput() for details.
  void AddInput(vtkHM1DStraightModelGrid *);
  void AddInput(int, vtkHM1DStraightModelGrid*);
*/
	
	// Description:
  // see vtkAlgorithm for details
  virtual int ProcessRequest(vtkInformation* request, 
                             vtkInformationVector** inputVector,
                             vtkInformationVector* outputVector);
	
	
  /*
  // Description:
  // Specify the source object which is used to editor process. The 
  // source defines a Grid that the input
  void SetSource(vtkHM1DStraightModelGrid *source);
  vtkHM1DStraightModelGrid *GetSource();
  */
  
  void AddSegment(int parent);
   
   
  
protected:
  vtkHM1DStraightModelEditor();
  ~vtkHM1DStraightModelEditor();

  // Description:
  // This is called by the superclass.
  // This is the method you should override.
//  virtual int RequestDataObject(vtkInformation* request, 
//                                vtkInformationVector** inputVector, 
//                                vtkInformationVector* outputVector) {return 1;};
  
  // Description:
  // This is called by the superclass.
  // This is the method you should override.
//  virtual int RequestInformation(vtkInformation*, 
//                                 vtkInformationVector**, 
//                                 vtkInformationVector*);
  
  // Description:
  // This is called by the superclass.
  // This is the method you should override.
  virtual int RequestData(vtkInformation*, 
                          vtkInformationVector**, 
                          vtkInformationVector*);
  

  // Description:
  // This is called by the superclass.
  // This is the method you should override.
  virtual int RequestUpdateExtent(vtkInformation*,
                                  vtkInformationVector**,
                                  vtkInformationVector*); 
  
	
	//virtual int FillOutputPortInformation(int port, vtkInformation* info);
	virtual int FillInputPortInformation(int port, vtkInformation* info);

	// Description:
  // Get the input data object. This method is not recommended for use, but
  // lots of old style filters use it.
	//vtkDataObject *GetInput(int port);

	// Description :
	// Add segments to output vtkHM1DStraightModelGrid
	void AddSegment(vtkHM1DSegment* s);
	
	// Description :
	// Walk throught StraightModel to get data
	void WalkStraightModelTree( int &id );


  vtkPoints*						Points;
	vtkCellArray*					Segments;
	vtkCellArray*					Nodes;
	vtkHM1DStraightModel* StraightModel;

private:
  vtkHM1DStraightModelEditor(const vtkHM1DStraightModelEditor&);  // Not implemented.
  void operator=(const vtkHM1DStraightModelEditor&);  // Not implemented.
};

#endif


