/*
 * $Id$
 */

#include "vtkHM3DDelaFilter.h"
#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"
#include "vtkDela3d.h"

#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkPlane.h"
#include "vtkClipDataSet.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkCallbackCommand.h"
#include "vtkObjectFactory.h"
#include "vtkUnstructuredGrid.h"
#include "vtkProcessModule.h"
#include "vtkExecutive.h"
#include "vtkKWProgressGauge.h"

vtkCxxRevisionMacro(vtkHM3DDelaFilter, "$Revision: 1.67 $");
vtkStandardNewMacro(vtkHM3DDelaFilter);

static void DelaFilterUpdateProgress(vtkObject *_surface, unsigned long, void *_delaFilter, void *)
{
  vtkDela3d *dela 							= reinterpret_cast<vtkDela3d *>(_surface);  
  vtkHM3DDelaFilter *delaFilter = reinterpret_cast<vtkHM3DDelaFilter *>(_delaFilter);
  delaFilter->SetProgress(dela->GetProgress());
  delaFilter->SetProgressText(dela->GetProgressText());
  delaFilter->UpdateProgress(delaFilter->GetProgress());
}


//----------------------------------------------------------------------------
vtkHM3DDelaFilter::vtkHM3DDelaFilter()
{ 	
	vtkDebugMacro(<<"vtkHM3DDelaFilter::vtkHM3DDelaFilter()");
	this->ApplyFilter = false;
	this->RetrieveGeometricInformation = false;
	strcpy(this->cfgFileName, "None");
  	this->IsManualMode = true; // default
  	this->IsFixOrientationOn = true;
}

//----------------------------------------------------------------------------
vtkHM3DDelaFilter::~vtkHM3DDelaFilter()
{
	vtkDebugMacro(<<"vtkHM3DDelaFilter::~vtkHM3DDelaFilter()");	

}

//-------------------------------------------------------------------------
char* vtkHM3DDelaFilter::GetResponseOfFilter()
{
	this->Update();
	return this->ResponseFilterInfo;
}

//----------------------------------------------------------------------------
int vtkHM3DDelaFilter::FillInputPortInformation(int port,vtkInformation *info)
{
  if (!this->Superclass::FillInputPortInformation(port, info))
    return 0; 
  if (port == 1)
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DDelaFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkHM3DDelaFilter::GetGeometricInformation(int flag)
{
	this->Modified();	
	this->RetrieveGeometricInformation  = (flag) ? true : false;
}

//----------------------------------------------------------------------------
void vtkHM3DDelaFilter::ApplyFixOrientation(int op)
{
	vtkDebugMacro(<<"vtkHM3DDelaFilter::ApplyFixOrientation()");	
	this->IsFixOrientationOn = (op) ? true : false;
	this->Modified();	
}

//----------------------------------------------------------------------------
void vtkHM3DDelaFilter::ApplyDelaFilter(char *file)
{
  	vtkDebugMacro(<<"vtkHM3DDelaFilter::ApplyDelaFilter()");		
	this->Modified();	
	strcpy(this->cfgFileName, file);
	
  	this->IsManualMode = false;
	
	// Esta flag é usada para indicar que o filtro pode ser aplicado corretamente
	this->ApplyFilter = true;	
}

//----------------------------------------------------------------------------
void vtkHM3DDelaFilter::ApplyAutoParFilter(double edgesSize, double sizeElements, double renumbering)
{
  	vtkDebugMacro(<<"vtkHM3DDelaFilter::ApplyAutoParFilter()");			
	this->Modified();	
	
	this->edgesSize			= edgesSize;
	this->sizeElements	= sizeElements;
	this->renumbering		= renumbering;

	this->IsManualMode = true;
		
	// Esta flag é usada para indicar que o filtro pode ser aplicado corretamente
	this->ApplyFilter = true;	
}

void vtkHM3DDelaFilter::FixOrientation(vtkMeshData *mesh)
{
	cout << "\n==> Checking Input Surface Normal Vectors" << endl;
	cout << "--> Creating temporary surface..." << endl;	
	
	/* OLD METHOD REPLACED!
	// Cria superfície temporária
	vtkSurfaceGen *tempSurf = vtkSurfaceGen::New();
	
	// Copia dados do MeshData em memória
	tempSurf->Copy(mesh->GetMesh3d(), mesh->GetParamMesh());

	cout << "--> Checking temporary surface groups..." << endl;			
	// Este método remove o menor grupo da superfície (supondo que ela esteja fechada)
	// e calcula a direção do vetor normal desta borda.
	// Retorna 1 se os vetores estiverem apontando para fora (devem apontar para dentro para o Dela3d).
	int orientationProblem = tempSurf->CheckOrientation();	
	
	// Remove superfície temporária
	tempSurf->Delete();

	if(!orientationProblem ) 					// 0 - Input Surface is ok
	*/	
	// OLD METHOD REPLACED!
	//int orientationProblem = mesh->CheckOrientation();
	 
	int orientationProblem = mesh->GetSurface()->CheckOrientation(mesh->GetMesh3d(), mesh->GetParamMesh());	
	
	if( orientationProblem == 0 )
		cout << "--> Input Surface is ok and ready to be processed!\n" << endl;	
	else // 1 - Normals are inverted. Swap All!
		{
		if(orientationProblem == 1)		
			{
			// 1 - Normals are inverted. Swap All!
			cout << "--> Input surface Normals have problems. Fixing..." << endl;			
			}
	  else 
			{
			// 2 - Surface is closed with 1 group. Create a border and try to swap (normally the normals are pointed out)
			cout << "--> Warning: Input is a close surface and has only one group. Can't be checked!\n"  
					 << "             Dela3D might work incorrectly.\n" << endl;	
			}
			
		// Swap Groups to fix orientation
		for(int i = 0; i < mesh->GetSurface()->GetNumGroups(); i++)
			{
			cout << "--> Swapping group " << i << "." << endl;
			mesh->GetSurface()->SwapGroup(i);		
			//mesh->GetSurface()->SwapGroup(i+1); // Incrementa pois internamente o índice é decrementado.
			}
		cout << "--> All groups were swapped!" << endl;
		cout << "--> Updating internal structures...";
		
		// Update internal mesh3d with update MeshData's SurfaceGen		
		mesh->GetMesh3d()->Update(mesh);		
		
	  // Atualiza Grid interno com dados da superfície. As informações do grid interno
	  // são utilizadas no método de leitura da t_surface
	  mesh->SetOutputAsSurface();			
		cout << "done!\n" << endl;
		}		
}

void vtkHM3DDelaFilter::PrintError(int returnValue)
{
	char message[512];
	switch(returnValue)
		{
		case 1: strcpy(message, "  Malformed input MeshData. Input must be rebuilt. ");
		break;
		case 2: strcpy(message, "  Insufficient Memory! ");
		break;		
		case 3: strcpy(message, "  Can't open configuration file. ");
		break;				
		case 4: strcpy(message, "  Error in pt_mesh generation. ");
		break;		
		case 5: strcpy(message, "  Error when trying to recovery boundary. Malformed Elements. ");
		break;
		case 6: strcpy(message, "  There are NULL triangles. Input must be rebuilt.\n(See \"Warning Window\" for more information) ");
		break;
		case 7: strcpy(message, "  Error when trying to delete external tetrahedra. ");
		break;
		case 8: strcpy(message, "  Inconsistent mesh. Increase parameters\n or review the input mesh. ");
		break;		
		}			
	strcpy(this->ResponseFilterInfo, message);		
	vtkErrorMacro(<<message);
}

//----------------------------------------------------------------------------
int vtkHM3DDelaFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
	// get the info objects
  	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// Pega input e output corrente
  	vtkMeshData *input = vtkMeshData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  	vtkMeshData *output = vtkMeshData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

	// Inicia teste da malha (áreas e arestas)
	double areaAverage = 0, areaSmaller, areaGreater,
				 edgeAverage = 0, edgeSmaller, edgeGreater;

	// Pega informações sobre as arestas dos triângulos da malha
	input->GetSurface()->GetTriangleEdgeGlobalInformation(&edgeAverage, &edgeSmaller, &edgeGreater);

	// Pega informações sobre as areas dos triângulos da malha
	input->GetSurface()->GetTriangleAreaGlobalInformation(&areaAverage, &areaSmaller, &areaGreater);					 

	if(this->RetrieveGeometricInformation)
		{		
		sprintf(ResponseFilterInfo, "Triangle Area:\n\tSmaller:\t%.14lf\n\tAverage:\t%.14lf\n\tGreater:\t%.14lf\n"
																"Triangle Edge:\n\tSmaller:\t%.14lf\n\tAverage:\t%.14lf\n\tGreater:\t%.14lf", 
																areaSmaller, areaAverage, areaGreater, 
																edgeSmaller, edgeAverage, edgeGreater);
		this->RetrieveGeometricInformation = false;
		
		// Copia informações do input para o output para que saída não suma
		output->CopyStructure(input);
		//this->Modified();			
		return 1;		
		}

	// Se a área de um triangulo for menor que 1% da média, não processa
	if(areaSmaller < areaAverage/100)
		{
		this->PrintError(6);	
		// Pega número de triângulos menores do que 1% da média
		// Imprime na janela de warnings do HeMoLab quem são esses triângulos
		input->GetSurface()->GetTrianglesSmallerThanValue(areaAverage/100);
		return 1;	
		}	

	// Filtro não está pronto para ser aplicado.
	// Isso ocorre se um dos arquivos de configuração não tiver sido configurado corretamente.
	if(!this->ApplyFilter)
		{
		vtkDebugMacro(<<"vtkHM3DDelaFilter::Execute() :: Filter is not ready to be applied");
		return 1;		
		}

	// Testa se orientação está para dentro ou para fora. Se estiver para fora,
  	// corrige apontando no sentido contrário
  	if(this->IsFixOrientationOn)
  		this->FixOrientation(input);

	// Copia informações do input para o output
	output->CopyStructure(input);	
	
	// Cria objeto que irá processar malha
  	vtkDela3d* dela = vtkDela3d::New();	
  
  	// Passa input e output correntes para o filtro
  	dela->SetInput ( input );
  	dela->SetOutput( output);
  
	// Callback para gerenciar barra de progresso
  	vtkCallbackCommand *cbc = vtkCallbackCommand::New();
		cbc->SetClientData(this);
		cbc->SetCallback(DelaFilterUpdateProgress);
  	dela->AddObserver(vtkCommand::ProgressEvent, cbc);

	int returnValue =   ( this->IsManualMode ) ?
		dela->ProcessFilter(input->GetBounds(), this->edgesSize, this->sizeElements, this->renumbering) :
		dela->ProcessFilter(this->cfgFileName);

	// Processa malha passando o arquivo de configuração como parâmetro
	// Resultados são armazenados no "output" passado anteriormente  
	// Retorna != 0 se a malha tiver algum problema (acusado pelo Dela)
	if(!returnValue)
		{
		output->SetOutputAsVolume();		
		cout << *output;	
		strcpy(this->ResponseFilterInfo,"Processed Successfully!");					
		}
	else
		{
		output->Reset();			
		this->PrintError(returnValue);
		}
	
	// Remove filtro e a Callback após o uso
	cbc->Delete();
	dela->Delete();		
	
	return 1;
}

