/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM3DDelaFilter

  Author: Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHM3DDelaFilter
// .SECTION Description
// Apply Dela3d filter to a surface.  The class vtkDela3d receives a vtkMeshData
// (surface) as input and produces an vtkMeshData (volume) as output.

#ifndef __vtkHM3DDelaFilter_h
#define __vtkHM3DDelaFilter_h

#include "vtkDataSetAlgorithm.h"

class vtkMeshData;

class VTK_EXPORT vtkHM3DDelaFilter : public vtkDataSetAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkHM3DDelaFilter,vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

  	// Description:
  	// Constructor
  	static vtkHM3DDelaFilter *New();

  	// Description:
  	// Apply Filters 
	void ApplyDelaFilter(char *);

  	// Description:
  	// Apply Manual Configuration Settings
	void ApplyAutoParFilter(double edgesSize, double sizeElements, double renumbering);

  	// Description:
  	// Apply Automatic normal orientation fixer
	void ApplyFixOrientation(int );

  	// Description:
  	// Get Geometric Information
	void GetGeometricInformation(int );

  	// Description:
  	// Print Dela3d Errors
	void PrintError(int);	
	
	// Used to update GUI
	char* GetResponseOfFilter();	
    
protected:
  vtkHM3DDelaFilter();
  ~vtkHM3DDelaFilter();

	// Check face normal orientation is fix it if necessary
  	void FixOrientation(vtkMeshData *);

  	// Usual data generation method
  	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);	

  	//FillInputInformation.
  	int FillInputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  	// Description:
  	// CFG File name 
  	char cfgFileName[256];
  
  	// Description:
  	// Controls if filter is ready to be applied 
  	bool ApplyFilter;  

  	// Description:
  	// Controls if fix orientation button is pressed   	
  	bool IsFixOrientationOn;	

  	// Description:
  	// Verify if instead of processing the filter, just retrieve the geometric information
	bool RetrieveGeometricInformation;

  	// Description:
  	// Manual Configuration Parameters
	double edgesSize;
	double sizeElements;
	double renumbering;
	
  	char ResponseFilterInfo[2048];	
  
  	bool IsManualMode;

private:
  vtkHM3DDelaFilter(const vtkHM3DDelaFilter&);  // Not implemented.
  void operator=(const vtkHM3DDelaFilter&);  // Not implemented.
};

#endif
