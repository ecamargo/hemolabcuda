#include "vtkHM3DFullModelFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkMeshData.h"
#include "vtkCell.h"
#include "vtkPoints.h"
#include "vtkDataSet.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkSurfaceGen.h"
#include "vtkHM3DFullModelWriterPreProcessor.h"
#include "vtkDataArrayCollection.h"

vtkCxxRevisionMacro(vtkHM3DFullModelFilter, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHM3DFullModelFilter);
//----------------------------------------------------------------------------
vtkHM3DFullModelFilter::vtkHM3DFullModelFilter()
{
	this->CoverGroups =-1;
  this->SelectedGroup = 0;	
  this->CoverPressionArray = NULL;
  this->CoverVelocityArray = NULL;
 	this->CoverElementBCType = vtkIntArray::New();
 	
 	this->PressureArrayCollection = vtkDataArrayCollection::New();
 	this->PressureArrayCollection->RemoveAllItems();
 	
 	this->WallParamCollection = vtkDataArrayCollection::New();
 	this->WallParamCollection->RemoveAllItems();
 	
 	this->WallGroups = 0;
 }

//----------------------------------------------------------------------------
vtkHM3DFullModelFilter::~vtkHM3DFullModelFilter()
{
	if (this->CoverPressionArray)
		  this->CoverPressionArray->Delete();
	
	if (this->CoverVelocityArray)
		  this->CoverVelocityArray->Delete();
		  
	if (this->CoverPressionSetArray)
		this->CoverPressionSetArray->Delete();	  
	
	
	this->CoverElementBCType->Delete();

	for (int i=0; i< this->PressureArrayCollection->GetNumberOfItems(); i++)
		this->PressureArrayCollection->GetItem(i)->Delete();
	
	for (int i=0; i< this->WallParamCollection->GetNumberOfItems(); i++)
		this->WallParamCollection->GetItem(i)->Delete();
	
	
	this->PressureArrayCollection->RemoveAllItems();
	this->WallParamCollection->RemoveAllItems();	
	
	this->PressureArrayCollection->Delete();
	this->WallParamCollection->Delete();	
	
}

//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetSurfaceElementProp(char *SurfaceProp)
{
	char temp[100];
 	int LastPosition =0, CurrentPosition=0, z=0, x=0, Array=0; 
 	// resetar array **********
	for (int i=0; i < 100 ;i++)
		temp[i]=0;
	//*************************	
    
   
  int i=0;
  
  if (!this->WallParamCollection->GetNumberOfItems()) // entra aqui somente se collection estiver vazio
  	{
  	for (int z = 0; z < this->WallGroups; ++z)
  		{
			vtkDoubleArray *TempArray = vtkDoubleArray::New();
  		TempArray->SetNumberOfValues(5);
			this->WallParamCollection->AddItem(TempArray);
			//cout << "Inserindo vetor no Collection "<< TempArray << endl;
			}
  	}
  
  
  
  vtkDoubleArray *TempArray = vtkDoubleArray::SafeDownCast(this->WallParamCollection->GetItem(Array));	
    
  while (SurfaceProp[CurrentPosition])
  	{
   	if (SurfaceProp[CurrentPosition] == ';')
   		{
   	  memcpy(temp, SurfaceProp+LastPosition, CurrentPosition-LastPosition);
   		TempArray->SetValue(i, atof(temp));
   		//cout << "Inserindo valor "<< atof(temp) << " na posicao " << i << " no vetor "<< TempArray << endl;
   		i++;
   		
   		if (i==5)
   			{
   			Array++;
   			//cout << "Adcionando VtkDoubleArray to the collection "<< endl;
   			//this->WallParamCollection->AddItem(TempArray);
   			TempArray = vtkDoubleArray::SafeDownCast(this->WallParamCollection->GetItem(Array));
   			i = 0;
   			}
   		LastPosition =CurrentPosition+1;
   		}
  	
 	  // resetar array **********
		for (int i=0; i < 100 ;i++)
			temp[i]=0;
 		//*************************	
  	
  	CurrentPosition++;
  	}
  		
   		
this->Modified();	

}

//----------------------------------------------------------------------------
void vtkHM3DFullModelFilter::SetCoverElementPressure(char  *CoverProp)
{
	/*
	 string que chega aqui tem o seguinte formato:
	
	[valor pressao tampa1; valor pressao tampa2; ...................... valor pressao tampaN; 
	Existe Pressao tampa1; Existe pressao tampa2; ......................existe pressao tampaN;  ]   
	
	*/
	
	//cout << "Cover Properties " << CoverProp << endl;
	
	
	if (!this->CoverPressionArray)
		{
		this->CoverPressionArray = vtkDoubleArray::New();
		this->CoverPressionArray->SetNumberOfComponents(this->CoverGroups);
		
		this->CoverPressionSetArray = vtkIntArray::New();
		this->CoverPressionSetArray->SetNumberOfComponents(this->CoverGroups);
		}
	
	  int LastPosition =0, CurrentPosition=0, z=0, x=0; 

	  char temp[50];
   	// resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************	
	    
	    
	  while (CoverProp[CurrentPosition])
	  	{
	   	if (CoverProp[CurrentPosition] == ';')
	   		{
	   	  memcpy(temp, CoverProp+LastPosition, CurrentPosition-LastPosition);
	   		if (z < this->CoverGroups)
	   			this->CoverPressionArray->InsertValue(z,  atof(temp));
	   		else
	   			{
	   			this->CoverPressionSetArray->InsertValue(x,  atoi(temp));
	   			x++;
	   			}
	   	  LastPosition =CurrentPosition+1;
	   	  z++;
	   		}
	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 				//*************************	
	   	CurrentPosition++;
	  
	    }
	this->Modified();	
}

//----------------------------------------------------------------------------


void vtkHM3DFullModelFilter::SetCoverElementVelocity(char  *CoverProp)
{
	/*
	 string que chega aqui tem o seguinte formato:
	 
	 [ vel X, vel Y, velZ; velX, velY, velZ; ......................velX, velY, velZ]   
	  ----tampa 1--------  ----tampa 2------                       ---- tampa n-----    

	*/
	
	if (!this->CoverVelocityArray)
		{
		this->CoverVelocityArray = vtkDoubleArray::New();
		// componentes em cada tupla
		this->CoverVelocityArray->SetNumberOfComponents(3);
		this->CoverVelocityArray->SetNumberOfTuples(this->CoverGroups);
		}

	double velArray[3];
  int LastPosition =0, CurrentPosition=0, z=0;
  int tuplePosition=0; 
  char temp[50];
   	// resetar array **********
  for (int i=0; i < 50 ;i++)
 		temp[i]=0;
 		//*************************	
	    
	    
	while (CoverProp[CurrentPosition])
		{
	  if (CoverProp[CurrentPosition] == ' ')
	  	{
	   	memcpy(temp, CoverProp+LastPosition, CurrentPosition-LastPosition);
   		velArray[z] = atof(temp);
   	  LastPosition =CurrentPosition+1;
   	  if (z==2)
	   		{
   	  	this->CoverVelocityArray->SetTupleValue(tuplePosition, velArray);
   	  	tuplePosition++;
   	  	z=0;
   	  	}
  	  else	
	   	  	z++;
	   	}
	  // resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************	
	  CurrentPosition++;
	  
	  }
	this->Modified();
}
//----------------------------------------------------------------------------

vtkHM3DFullModel* vtkHM3DFullModelFilter::GetOutput(int port)
{
  return vtkHM3DFullModel::SafeDownCast(this->GetOutputDataObject(port));
}

//----------------------------------------------------------------------------
void	vtkHM3DFullModelFilter::SelectGroup(int gr)
{
	this->SelectedGroup = gr;
	this->Modified();		
	this->Update();
}

//----------------------------------------------------------------------------
int vtkHM3DFullModelFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkMeshData *input = vtkMeshData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkHM3DFullModel *output = vtkHM3DFullModel::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
    
  // Testa se input e output são válidos
  if(!input) 	vtkErrorMacro("Input is NULL.");
 	if(!output) vtkErrorMacro("Output is NULL.");
	
	// setando o numero de grupos "Tampas". É assumido aqui que exista somente um grupo da superficie
	//output->SetNumberOfCoverGroups(input->GetSurface()->GetNumGroups()-1);
	//output->SetNumberOfCoverGroups(input->GetSurface()->GetNumGroups() - input->GetSurface()->GetNumberOfShells()); 
	
  // cria lista de vetores correspondentes aos parametros dos cover groups
//	if (!output->GetCoverParamListCreated())
//		output->CreateCoverParamList(); 

	
	//setando parametros do elemento de superficie
//	output->SetSurfaceElementParam(this->SurfaceElemProp);
	
	//setando valor de pressao constante dos elementos das "tampas"
	output->SetCoversConstantPressure(this->CoverPressionArray);
	output->SetCoverPressionSetArray(this->CoverPressionSetArray);

	// setando parametros dos elementos velocidades das tampas   
	output->SetCoverElementVelocity(this->CoverVelocityArray);

  // seta FullModel com vetor que indica o tipo de BC definido para cada tampa
	output->SetCoverElementsBCType(this->CoverElementBCType);

	output->SetPressureArrayCollection(this->PressureArrayCollection);

	// seta os parametros dos diversos elementos de parede
	output->SetWallParamCollection(this->WallParamCollection);	

	// Copia informações do input para o output
	output->CopyStructure(input);
	
//	if(this->SelectedGroup == input->GetSurface()->GetNumGroups() )
//		{
//		output->SetOutputAsVolume();
//		output->UpdateGroupProperties(1);		
//		}
//	else
//		{
		output->SetOutputAsSurface();	
		output->UpdateGroupProperties(0);		
//		}

	// Colore a superfície se o modo "SelectedGroup" foi selecionado.
	output->SetSelectedGroup(this->SelectedGroup);
	output->GetCellData()->SetActiveScalars("SelectedGroup");   

  return 1;
}


//----------------------------------------------------	------------------------
void vtkHM3DFullModelFilter::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHM3DFullModelFilter::FillOutputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM3DFullModel");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHM3DFullModelFilter::FillInputPortInformation(
  int port,
  vtkInformation* info)
{
	
	// Seta tipo de entrada
  if(!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMeshData");
  
  vtkMeshData *mesh = vtkMeshData::SafeDownCast(this->GetInput(0));
  this->CoverGroups = mesh->GetSurface()->GetNumGroups() - mesh->GetSurface()->GetNumberOfShells() ; 
  this->WallGroups =  mesh->GetSurface()->GetNumberOfShells(); // 1 corresponde a 1 grupo do volume
  
  
  if (!this->PressureArrayCollection->GetNumberOfItems())
  	{
  	for (int i=0; i< this->CoverGroups; i++)
  		{
  		vtkDoubleArray *temp = vtkDoubleArray::New();
  		temp->SetNumberOfComponents(2); 
  		//this->CoverBCType->SetValue(i,0);
			this->PressureArrayCollection->AddItem(temp);
		 	}
  	}
  
	return 1;
}
//----------------------------------------------------------------------------
void vtkHM3DFullModelFilter::GenerateSolverFiles(char *path)
{
	vtkHM3DFullModelWriterPreProcessor *FullModelPreProcessor = vtkHM3DFullModelWriterPreProcessor::New();

	vtkHM3DFullModel *FullModel = vtkHM3DFullModel::SafeDownCast(this->GetOutput(0));
	
	FullModelPreProcessor->SetWhichFile(this->WhichFile);
	
	FullModelPreProcessor->SetIntegerParam(this->ConfigIntegerParam);
	
	FullModelPreProcessor->SetTimeParam(this->TimeParam);

	FullModelPreProcessor->SetConvergenceParam(this->ConvParam);
	
	FullModelPreProcessor->SetModelParam(this->ModelConfigParam);
	
	FullModelPreProcessor->SetSolverParam(this->SolverConfigParam);
	
	FullModelPreProcessor->SetFullModel(FullModel);
	
	FullModelPreProcessor->SetLogfilename(this->LogFileName);

	FullModelPreProcessor->SetScaleFactor3D(this->ScaleFactor3D);
	
	
	//FullModelPreProcessor->ConfigureBasparam();

	FullModelPreProcessor->InitializeWriteProcess(path);

	FullModelPreProcessor->Delete();
}
//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetIntParam(int IntParam[18])
{
	for (int i=0; i < 18; i++)
		this->ConfigIntegerParam[i]	=	IntParam[i];
}

//----------------------------------------------------------------------------


void vtkHM3DFullModelFilter::SetTimeParam(double TimeParam[3])
{
	for (int i=0; i < 3; i++)
		this->TimeParam[i]	=	TimeParam[i];
}

//----------------------------------------------------------------------------


void vtkHM3DFullModelFilter::SetConvergenceParam(double ConvParam[15])
{
	for (int i=0; i < 15; i++)
		this->ConvParam[i]	=	ConvParam[i];
}


//----------------------------------------------------------------------------


void vtkHM3DFullModelFilter::SetModelConfigParam(double ModelConfigParam[5])
{
	for (int i=0; i < 5; i++)
		this->ModelConfigParam[i]	=	ModelConfigParam[i];
}
		
//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetLogFileName(const char *logfilename)
{
	strcpy(this->LogFileName, logfilename);
}

//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetSolverConfigParam(double SolverConfigParam[6])
{
	for (int i=0; i < 6; i++)
		this->SolverConfigParam[i]	=	SolverConfigParam[i];
}


//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetCoverElementTimeVariantPressure(char  *CoverProp)
{
	/*
	a string que chega aqui apresenta o seguinte formato:
	valores curva tampa 1; valores curva 2 ; valores curva n = sendo n o numero de tampas com pressao variante definida como BC
	por exemplo: 1;1 2;2 3;4 5;6 *2;4 5;6 8;9 *
	* -- indica o final de uma curva
	*/

	double velArray[3];
  int LastPosition =0, CurrentPosition=0, z=0;
  double valor1, valor2;
  int tuplePosition=0;
  int ArraySize =0; 
  
  char temp[50];
   	// resetar array **********
  for (int i=0; i < 50 ;i++)
 		temp[i]=0;
 		//*************************	
	
	if (strlen(CoverProp))
		{
		for (int i=0; i< this->CoverGroups; i++)
  		{
  		
  		// resetando vtkDoubleArray para evitar que conteudo aqui antes adquirirdo seja incluido no DoubleArray
  		this->PressureArrayCollection->GetItem(i)->Reset();
  		
  		
  		if (this->CoverElementBCType->GetValue(i)==2)		
				{
				while (CoverProp[CurrentPosition] ) //!= '*')
					{
			  	if (CoverProp[CurrentPosition] == ';')
			  		{
			   		memcpy(temp, CoverProp+LastPosition, CurrentPosition-LastPosition);
		   			//cout << temp << endl;
		   			valor1 = atof(temp);
		   	  	LastPosition =CurrentPosition+1;
			   		}
			  	if (CoverProp[CurrentPosition] == ' ')
			  		{
			   		memcpy(temp, CoverProp+LastPosition, CurrentPosition-LastPosition);
		   			//cout << temp << endl;
		   			valor2 = atof(temp);
		   	  	LastPosition =CurrentPosition+1;
		   	  	this->PressureArrayCollection->GetItem(i)->InsertNextTuple2(valor1, valor2);
		   	  	
				   	ArraySize++;
				   	}
			  	if (CoverProp[CurrentPosition] == '*' && (CurrentPosition < strlen(CoverProp)))
			  		{
				   	CurrentPosition++;
				   	this->PressureArrayCollection->GetItem(i)->SetNumberOfTuples(ArraySize);
				   	ArraySize=0;
				   	break;
			  		}
		 	  
				 	// resetar array **********
				  for (int i=0; i < 50 ;i++)
				 		temp[i]=0;
				 	//*************************	
					
					CurrentPosition++; 	
					}  	
				}	   	
  		}	
		}  	

	this->Modified();	

}

//----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetCoverElementsBCType(char *CoverProp)
{
	double velArray[3];
  int LastPosition =0, CurrentPosition=0, z=0;
  double valor1, valor2;
  int tuplePosition=0; 
  
  char temp[50];
   	// resetar array **********
  for (int i=0; i < 50 ;i++)
 		temp[i]=0;
 		//*************************	
	this->CoverElementBCType->SetNumberOfTuples(this->CoverGroups);
	int i=0;
		
	while (CoverProp[CurrentPosition])
		{
	  if (CoverProp[CurrentPosition] == ' ')
	  	{
	   	memcpy(temp, CoverProp+LastPosition, CurrentPosition-LastPosition);
   	  LastPosition =CurrentPosition+1;
   	  this->CoverElementBCType->SetValue(i,atoi(temp));
   	  i++;	
	   	}

	  // resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************	
	  CurrentPosition++;
	  }
	this->Modified();
}

//----------------------------------------------------------------------------

int vtkHM3DFullModelFilter::GetNumberOfCoversWithBC(int BCType)
{
	int cover=0;
	for (int i=0; i< this->CoverGroups; i++)
  	{
  	 if (this->CoverElementBCType->GetValue(i)==BCType)
  	 	cover++;
	  }

	return cover;
}

// ----------------------------------------------------------------------------

void vtkHM3DFullModelFilter::SetSelectedSolverFiles(int WhichFile[4])
{
	for (int i=0; i < 4; i++)
		this->WhichFile[i]	=	WhichFile[i];
	
}

// ----------------------------------------------------------------------------
