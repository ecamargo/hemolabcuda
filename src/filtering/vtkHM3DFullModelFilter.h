
#ifndef __vtkHM3DFullModelFilter_h
#define __vtkHM3DFullModelFilter_h

#include "vtkDataSetAlgorithm.h"
#include "vtkHM3DFullModel.h"
#include <vector>

class vtkDataSet;
class vtkHM3DFullModel;
class vtkDataArrayCollection;

class VTK_EXPORT vtkHM3DFullModelFilter : public vtkDataSetAlgorithm
{
public:
	  static vtkHM3DFullModelFilter *New();
  vtkTypeRevisionMacro(vtkHM3DFullModelFilter,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return an vtkMeshData Object as an output of this filter
	vtkHM3DFullModel* GetOutput(int port);
	
	// Description:
  // 
	void SetSurfaceElementProp(char *SurfaceProp);
	
	// Description:
  // 
	void SetCoverElementPressure(char  *CoverProp);

	// Description:
  // 
	void SetCoverElementVelocity(char  *CoverProp);


  // Description:
  // 
	vtkGetMacro(CoverGroups,int);
	
	
	vtkGetMacro(WallGroups,int);

  // Description:
  // Set a group to be highlighted
	void SelectGroup(int gr);
	
	// Description:
	//
	void GenerateSolverFiles(char *path);
	
	// Description:
  // 
	void SetIntParam(int IntParam[18]);
	
	
	// Description:
  // 
	void SetTimeParam(double TimeParam[3]);
	
	
	// Description:
  // 
	void SetConvergenceParam(double ConvParam[15]);
	
	
	// Description:
  // 
	void SetModelConfigParam(double ModelConfigParam[5]);
	
	
	// Description:
  // 
	void SetLogFileName(const char *logfilename);


	// Description:
  // 
	void SetSolverConfigParam(double SolverConfigParam[6]);

	// Description:
  // 
	void SetCoverElementTimeVariantPressure(char  *CoverProp);

	// Description:
  // 
	void SetCoverElementsBCType(char *CoverProp);
	
	// Description:
  // 
	int GetNumberOfCoversWithBC(int BCType);
	
	
	void SetSelectedSolverFiles(int WhichFile[4]);
	
	// Description:
  // Set ang get method for the var that control the convertion between 1D and 3D scales
	vtkSetMacro(ScaleFactor3D, double);
	vtkGetMacro(ScaleFactor3D, double); 
	


// ******************************************************************************************************
// ******************************************************************************************************
// ******************************************************************************************************
protected:
  vtkHM3DFullModelFilter();
  ~vtkHM3DFullModelFilter();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
	int FillOutputPortInformation(int, vtkInformation*);
	
	// Description:
	//	
	int CoverGroups;
  
  // Description:
  // Variables to control which Group was selected
  int SelectedGroup; 
  
  // Description:
  // store the interger parameters used during the solver files generation
  int ConfigIntegerParam[18];	
  
  // Description:
  // store the time parameters used during the solver files generation
  double TimeParam[3];
  
  // Description:
  // store the value of the convergence parameters 
  double ConvParam[15];
  
  // Description:
  //
 	double ModelConfigParam[5];
 
 	// Description:
  //
  double SolverConfigParam[6];
 
 	// Description:
  //
  char LogFileName[50];
  
  // Description:
  //
  vtkDoubleArray *CoverPressionArray;
  
  // Description:
  //
  vtkIntArray *CoverPressionSetArray;
  
  // Description:SolverConfigParam
  //
  vtkDoubleArray *CoverVelocityArray;
  
  // Description:
  // vetor com tamanho do numero de tampas que indica o tipo de BC selecionado
	// 0 -pressao constante
	// 1 - velocidade
	// 2 - p=f(t)
	vtkIntArray *CoverElementBCType;
	
  // Description:
  // representa uma lista (correspondente ao numero de tampas) de vtkDoubleArrays
	// onde cada dupla de cada vetor, indica os valores de tempo; pressao  
	vtkDataArrayCollection *PressureArrayCollection;
	
  // Description:
  //	
	// collection de vtkDoubleArrays que guardam os parametros de cada grupo de parede
	vtkDataArrayCollection *WallParamCollection;
	
  // Description:
  //
	int WallGroups;
	
	// Description:
	// int array which describes which solver files will be generated
	int WhichFile[4];
	
	double ScaleFactor3D;
	
	
private:
  vtkHM3DFullModelFilter(const vtkHM3DFullModelFilter&);  // Not implemented.
  void operator=(const vtkHM3DFullModelFilter&);  // Not implemented.
};

#endif
