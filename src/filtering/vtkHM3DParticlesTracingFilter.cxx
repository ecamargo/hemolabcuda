/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM3DParticlesTracingFilter

  Author: Eduardo Camargo

=========================================================================*/
// .NAME vtkHM3DParticlesTracingFilter
// .SECTION Description
// Build files for Visualization particles

#include "vtkHM3DParticlesTracingFilter.h"
#include "vtkHM3DParticleTracingWriter.h"
#include "vtkHM3DParticleTracingWriterCUDA.h"
#include "vtkHM3DModelGrid.h"

#include "vtkSphereSource.h"
#include "vtkAppendFilter.h"
#include "vtkHMEnSightWriter.h"
#include "vtkCollection.h"

#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkCommand.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"
#include "vtkPoints.h"
#include "vtkCellType.h"
#include "vtkMath.h"
#include "vtkExecutive.h"

#include "sys/time.h"
#include <algorithm>
#include <string>



#ifdef _WIN32
	#include "windows.h"
#endif

vtkCxxRevisionMacro(vtkHM3DParticlesTracingFilter, "$Rev$");
vtkStandardNewMacro(vtkHM3DParticlesTracingFilter);


//----------------------------------------------------------------------------
vtkHM3DParticlesTracingFilter::vtkHM3DParticlesTracingFilter()
{
	this->SetNumberOfInputPorts(2);

	this->InitialPositionOfParticles = vtkPoints::New();
	this->internalGrid = NULL;

	this->NumberOfParticles = 1;
	this->RadiusOfParticles = 0.005;
	this->CurrentTimeStep = 0;
	this->NumberOfTimeSteps = 1;
	this->TimeStepsDelta = 0.005;
	strcpy(this->OutputPath, "");
	this->NumberOfRepeatTimeSteps = 1;
	this->NumberOfInsertTimeSteps = 1;

	//Variável usada na geração de coordenadas aleatórias
	this->cell = 0;

	// Processamento em GPU desabilitado pois deve ser solicitado pelo usuário.
	this->GPUProcess = true;
}

//----------------------------------------------------------------------------
vtkHM3DParticlesTracingFilter::~vtkHM3DParticlesTracingFilter()
{
	if( this->InitialPositionOfParticles )
		this->InitialPositionOfParticles->Delete();

	if( this->internalGrid )
			this->internalGrid->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::Modified()
{
	this->Superclass::Modified();
	this->InvokeEvent(vtkCommand::ModifiedEvent, this);
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
int vtkHM3DParticlesTracingFilter::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkDataSet");
	return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::SetCurrentTimeStep(int current)
{
	this->CurrentTimeStep = current;
	this->Modified();
}

//----------------------------------------------------------------------------
int vtkHM3DParticlesTracingFilter::GetCurrentTimeStep()
{
	return this->CurrentTimeStep;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::SetNumberOfTimeSteps(int steps)
{
	this->NumberOfTimeSteps = steps;
}

//----------------------------------------------------------------------------
int vtkHM3DParticlesTracingFilter::GetNumberOfTimeSteps()
{
	return this->NumberOfTimeSteps;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::Build3DModelGrid()
{
	vtkDoubleArray* degreeOfFreedom;
	double degreeOfFreedomTuple[7] = {-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0};
	double VelocityTuple[3] = {-1.0, -1.0, -1.0};
	double PressureTuple = -1.0;
	double DisplacementTuple[3] = {-1.0, -1.0, -1.0};

	vtkDataSet *Input = vtkDataSet::SafeDownCast( this->GetInput(0) );

//	cout << "Input->GetNumberOfPoints(): " << Input->GetNumberOfPoints() <<endl;


	// Se o grid interno não existir, deve ser criado
	if( !this->internalGrid )
		{
		this->internalGrid = vtkHM3DModelGrid::New();

		// Copia pontos e células
		this->internalGrid->CopyStructure(Input);

		// Configura e seta os Mesh Id`s
		vtkIdList* meshList = vtkIdList::New();
		for(int i=0; i < this->internalGrid->GetNumberOfPoints(); i++)
			meshList->InsertNextId(i);

		this->internalGrid->SetMeshId(meshList);
		meshList->Delete();

		// Seta o número de TimeSteps
		this->internalGrid->SetNumberOfBlocks(this->GetNumberOfTimeSteps());

		//Seta o número do componentes do array que irá conter os graus de liberdade
		this->internalGrid->GetDegreeOfFreedom()->SetNumberOfComponents(7);

		int numTuples = this->GetNumberOfTimeSteps() * this->internalGrid->GetNumberOfPoints();
		this->internalGrid->GetDegreeOfFreedom()->SetNumberOfTuples( numTuples );
		}

	// Pega referência do array para os graus de liberdade
	degreeOfFreedom = this->internalGrid->GetDegreeOfFreedom();


	// Redimenciona o array para evitar consumo desnecessário de memória
	int nTuples = degreeOfFreedom->GetNumberOfTuples();
	int id = this->CurrentTimeStep * this->internalGrid->GetNumberOfPoints();
	nTuples += Input->GetNumberOfPoints();


	int numberOfArrays = Input->GetPointData()->GetNumberOfArrays();
	vtkDataArray *velocity, *pressure, *displacement;
	std::string arrayName;

	velocity = NULL;
	pressure = NULL;
	displacement = NULL;


	for(int i=0; i < numberOfArrays; i++)
	{
		arrayName = Input->GetPointData()->GetArrayName(i);

		//converte a string para minúsculo
		std::transform (arrayName.begin(), arrayName.end(), arrayName.begin(),(int(*)(int)) tolower );

		if(arrayName.compare(0, 8, "velocity") == 0)
			{
			velocity = Input->GetPointData()->GetArray(i);
			}
		else if(arrayName.compare(0, 8, "pressure") == 0)
			{
			pressure = Input->GetPointData()->GetArray(i);
			}
		else if(arrayName.compare(0, 12, "displacement") == 0)
			{
			displacement = Input->GetPointData()->GetArray(i);
			}
		else
			{
			vtkErrorMacro( << "Invalid array name " << arrayName.c_str() );
			}
	}

	// Seta o array com os valores para velocidade, pressão e deslocamento
	for(int i=0; i < Input->GetNumberOfPoints(); i++)
		{
		velocity->GetTuple(i, VelocityTuple);
		pressure->GetTuple(i, &PressureTuple);

		DisplacementTuple[0] = 0.0;
		DisplacementTuple[1] = 0.0;
		DisplacementTuple[2] = 0.0;
		if ( displacement != NULL )
			{
			displacement->GetTuple(i, DisplacementTuple);
			}

		degreeOfFreedomTuple[0] = VelocityTuple[0];
		degreeOfFreedomTuple[1] = VelocityTuple[1];
		degreeOfFreedomTuple[2] = VelocityTuple[2];
		degreeOfFreedomTuple[3] = PressureTuple;
		degreeOfFreedomTuple[4] = DisplacementTuple[0];
		degreeOfFreedomTuple[5] = DisplacementTuple[1];
		degreeOfFreedomTuple[6] = DisplacementTuple[2];

		degreeOfFreedom->SetTuple(id+i, degreeOfFreedomTuple);
		}

	degreeOfFreedom->Squeeze();
	this->CurrentTimeStep++;
}

//-----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::AddInput(vtkDataSet* input)
{
  this->Superclass::AddInput(input);
}

//----------------------------------------------------------------------------
int vtkHM3DParticlesTracingFilter::FillInputPortInformation(int port, vtkInformation* info)
{
  if(!this->Superclass::FillInputPortInformation(port, info))
    {
    return 0;
    }
  if(port==1)
    {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(),1);
    }
  else
    {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    }
  return 1;
}

//-----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::SetSource(vtkDataSet *source)
{
  this->SetInputConnection(1, source->GetProducerPort());
}

//-----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::printHour()
{
  time_t     now;
  struct tm  *ts;
  char       buf[80];

  // Obtem o tempo corrente
  time(&now);

  // Formata e imprime o tempo, "ddd yyyy-mm-dd hh:mm:ss zzz"
  ts = localtime(&now);
  strftime (buf, 80,"%H:%M:%S",ts);
  printf("%s ", buf);
}

//-----------------------------------------------------------------------------
vtkDataSet *vtkHM3DParticlesTracingFilter::GetSource()
{
  if (this->GetNumberOfInputConnections(1) < 1) // because the port is optional
    {
    return 0;
    }
  return static_cast<vtkDataSet *>(this->GetExecutive()->GetInputData(1, 0));
}

//-----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::InitializeParticles()
{
  // Inicializando partículas para teste
  vtkPoints* initialPosition = vtkPoints::New();

  FILE *fp;
  char fileName[256];
  strcpy(fileName, this->GetOutputPath() );
  strcat(fileName, "ini_particles.txt");
  fp = fopen(fileName, "r");

	if(!fp)
	  printf("Initialize reading error.\n");

	int nParticles;
	float x, y, z;
	fscanf( fp, "%d", &nParticles );

	for(int i=0; i<nParticles; i++)
	  {
	  fscanf( fp, "%f %f %f", &x, &y, &z );
	  initialPosition->InsertNextPoint( x, y, z );
	  }
  fclose(fp);

  this->InitialPositionOfParticles->DeepCopy(initialPosition);
  this->NumberOfParticles = this->InitialPositionOfParticles->GetNumberOfPoints();
  initialPosition->Delete();
  //----------------------------------------------------------------------------






/*
  double point[3], point1[3], center[3];
  double radius, ranX, ranY, ranZ;;
  int i;
  int index = 0 ;

  vtkPoints* initialPosition = vtkPoints::New();
  vtkDataSet *dataSet = vtkDataSet::SafeDownCast(this->GetInput(1));


	// A spherewidget foi configurada manualmente na classe vtkPVHMSphereSourceWidget (método Create)
	// para possuir 362 pontos. Logo, se sua resolução for alterada na classe citada deve ser tb aqui.
	if( dataSet->GetNumberOfPoints() == 362 )
		{
		// Compute radius
		dataSet->GetPoint(0, point);
		dataSet->GetPoint(1, point1);
		radius = sqrt( vtkMath::Distance2BetweenPoints(point, point1) ) / 2;

		//Compute center
		center[0] = (point[0]+point1[0])/2;
		center[1] = (point[1]+point1[1])/2;
		center[2] = (point[2]+point1[2])/2;

		// Build cloud of random particles
		for(i=0; i<this->NumberOfSphereParticles; i++)
			{
			this->RandomCoordinates(center, radius, point);

			initialPosition->InsertNextPoint(point);
			}

		this->InitialPositionOfParticles->DeepCopy(initialPosition);
		this->NumberOfParticles = this->InitialPositionOfParticles->GetNumberOfPoints();
		initialPosition->Delete();
		}
	else
		{
		for(i=0; i < dataSet->GetNumberOfPoints(); i++)
			{
			dataSet->GetPoint(i, point);
			initialPosition->InsertNextPoint(point);
			}
		this->InitialPositionOfParticles->DeepCopy(initialPosition);
		this->NumberOfParticles = this->InitialPositionOfParticles->GetNumberOfPoints();
		initialPosition->Delete();
		}




	// Escreve as coordenadas iniciais - para teste
  //----------------------------------------------------------------------------
  FILE *fp;
  char fileName[256];
  strcpy(fileName, this->GetOutputPath() );
  strcat(fileName, "ini_particles.txt");
  fp = fopen(fileName, "w");

  if(!fp)
    printf("Initialize writing error.\n");

  fprintf(fp, "%d\n", this->NumberOfParticles);

  for(int i=0; i<this->NumberOfParticles; i++)
    {
    this->InitialPositionOfParticles->GetPoint(i, point);
    fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
    }
  fclose(fp);
	//-------------------------------------------------------------
*/
}

// Usado nos testes de desempenho
//-----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::InitializeParticles(int nParticles)
{
  // Inicializando partículas para teste
  vtkPoints* initialPosition = vtkPoints::New();

  FILE *fp;
  char fileName[256];
  strcpy(fileName, this->GetOutputPath() );
  strcat(fileName, "ini_particles.txt");
  fp = fopen(fileName, "r");

  if(!fp)
    printf("Initialize reading error.\n");

  int tmp;
  float x, y, z;
  fscanf( fp, "%d", &tmp );

  for(int i=0; i<nParticles; i++)
    {
    fscanf( fp, "%f %f %f", &x, &y, &z );
    initialPosition->InsertNextPoint( x, y, z );
    }
  fclose(fp);

  if(this->InitialPositionOfParticles)
    {
    this->InitialPositionOfParticles->Delete();
    this->InitialPositionOfParticles = vtkPoints::New();
    }

  this->InitialPositionOfParticles->DeepCopy(initialPosition);
  this->NumberOfParticles = this->InitialPositionOfParticles->GetNumberOfPoints();
  initialPosition->Delete();
  //----------------------------------------------------------------------------
}

//----------------------------------------------------------------------------
int vtkHM3DParticlesTracingFilter::RequestData(vtkInformation *vtkNotUsed(request),
								 vtkInformationVector **inputVector,
								 vtkInformationVector *outputVector)
{
	// Getting objects information
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// Getting input and output filter references
	vtkDataSet *Input = vtkDataSet::SafeDownCast( inInfo->Get(vtkDataSet::DATA_OBJECT()));
	vtkDataSet *Output = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataSet::DATA_OBJECT()));


	if(this->CurrentTimeStep == 1)
		{
//		cout << "\n\nInício da conversão para vtk3DModelGrid : ";
//		this->printHour();
		}

	//Usado para percorrer todos os passos de tempo e montar um objeto do tipo vtk3DModelGrid
	if( this->CurrentTimeStep < this->GetNumberOfTimeSteps())
		{
		this->Build3DModelGrid();
		}

	//Após montado o objeto vtk3DModelGrid, chama o gerador de partículas e EnSightWriter
	if( this->CurrentTimeStep == this->GetNumberOfTimeSteps() )
		{
//		cout << "\n\nFim da conversão para vtk3DModelGrid : "; 		this->printHour();

		// Removendo triângulos
		vtkHM3DModelGrid *VolumeGrid = vtkHM3DModelGrid::New();
		this->ExtractVolume(this->internalGrid, VolumeGrid);
		VolumeGrid->Update();


	// Inserção de passos de tempo artificiais
	if( this->NumberOfInsertTimeSteps > 1 )
		this->InsertTimeSteps(VolumeGrid, this->NumberOfInsertTimeSteps);


  // vetor para receber as coordenadas das partículas em todos os timeSteps
  // é alocado dentro do método GetPositionOfParticlesInAllTimeSteps
  point *vec;


  //---------------------------------------------------------------------------------------------
  // Usado para avaliação de desempenho - deve ser substituído pelo bloco comentado abaixo.
  char OriginalPath[512];
  strcpy(OriginalPath, this->OutputPath);
  //int nblocks[5] = {12, 24, 48, 96, 192};
  int nPart[15] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500};

  cout << "Path: " << this->OutputPath << endl;

  cout << "0-  GPU = 1; CPU = 0 (Medição em milisegundos)"<< endl;
  cout << "1-  Número de partículas:"<< endl;
  cout << "2-  Início do particle tracing:"<< endl;
  cout << "3-  Inicialização e carga da geometria na memória da CPU:"<< endl;
  cout << "4-  Cópia de dados para a GPU (dados iniciais):"<< endl;
  cout << "5-  Cópia de dados para a GPU (steps):"<< endl;
  cout << "6-  Blocos de processamento na GPU:"<< endl;
  cout << "7-  Total de processamento dos steps:"<< endl;
  cout << "8-  Cópia de dados para a CPU (trajetórias):"<< endl;
  cout << "9-  Liberação de memória GPU e CPU:"<< endl;
  cout << "10- Total de processamento (3+4+5+7+8+9):"<< endl;
  cout << "11- Fim do particle tracing:"<< endl;
  cout << "12- CPU - 0 (Medição em milisegundos)"<< endl;
  cout << "13- Número de partículas:"<< endl;
  cout << "14- Início do particle tracing:"<< endl;
  cout << "15- Total de processamento:"<< endl;
  cout << "16- Fim do particle tracing:"<< endl;
  cout << "#########################################"<< endl;

  for(int j=0; j < 15; j++)
    {
    this->InitializeParticles( nPart[j] );

    for(int i=0; i < 5; i++)
      {
      strcat(this->OutputPath, "GPU/");

      // Removendo arquivos desatualizados
      this->DeleteOldFile("_index.txt");
      this->DeleteOldFile("metacell*.txt");
      this->DeleteOldFile("particle*.txt");
      this->DeleteOldFile("InformationFile.par");
      this->DeleteOldFile("HMParticleTracing*");

      printf("1 ");
      printf("%d ", this->NumberOfParticles);
      vtkHM3DParticleTracingWriterCUDA *particleWriterCUDA = vtkHM3DParticleTracingWriterCUDA::New();
      particleWriterCUDA->SetInput(VolumeGrid);
//      particleWriterCUDA->SetNumBlocks( 8 );
      particleWriterCUDA->SetDelta( this->TimeStepsDelta );
      particleWriterCUDA->SetPath( this->OutputPath );
      particleWriterCUDA->SetNumberOfParticles( this->NumberOfParticles );
      particleWriterCUDA->SetParticles( this->InitialPositionOfParticles );
      particleWriterCUDA->WriteMetaCells();
      particleWriterCUDA->SetNumberOfRepeatTimeSteps( this->NumberOfRepeatTimeSteps );
  //    cout << "Início do particle tracing : ";
      this->printHour();
      particleWriterCUDA->WriteParticles();
  //    cout << "\nFim do particle tracing : ";
      this->printHour();
//    particleWriterCUDA->GetPositionOfParticlesInAllTimeSteps(&vec);
      particleWriterCUDA->Delete();

//      this->WriteEnSightFiles(vec);
//      delete [] vec;

      // removendo arquivos intermediários
      this->DeleteOldFile("_index.txt");
      this->DeleteOldFile("metacell*.txt");
      this->DeleteOldFile("InformationFile.par");




      strcpy(this->OutputPath, OriginalPath);
      strcat(this->OutputPath, "CPU/");

      // Removendo arquivos desatualizados
      this->DeleteOldFile("_index.txt");
      this->DeleteOldFile("metacell*.txt");
      this->DeleteOldFile("particle*.txt");
      this->DeleteOldFile("InformationFile.par");
      this->DeleteOldFile("HMParticleTracing*");

      printf("\n0 ");
      printf("%d ", this->NumberOfParticles);
      vtkHM3DParticleTracingWriter *particleWriter = vtkHM3DParticleTracingWriter::New();
      particleWriter->SetInput(VolumeGrid);
      particleWriter->SetDelta( this->TimeStepsDelta );
      particleWriter->SetPath( this->OutputPath );
      particleWriter->SetNumberOfParticles( this->NumberOfParticles );
      particleWriter->SetParticles( this->InitialPositionOfParticles );
//      particleWriter->WriteMetaCells();
      particleWriter->SetNumberOfRepeatTimeSteps( this->NumberOfRepeatTimeSteps );
  //    cout << "\n\nInício do particle tracing : ";
      this->printHour();
//      particleWriter->WriteParticles();
  //    cout << "\nFim do particle tracing : ";
      this->printHour();
//      particleWriter->GetPositionOfParticlesInAllTimeSteps(&vec);
      particleWriter->Delete();

//      this->WriteEnSightFiles(vec);
//      delete [] vec;

      // removendo arquivos intermediários
      this->DeleteOldFile("_index.txt");
      this->DeleteOldFile("metacell*.txt");
      this->DeleteOldFile("particle*.txt");
      this->DeleteOldFile("InformationFile.par");

      strcpy(this->OutputPath, OriginalPath);
      printf("\n");
      }
    }

  VolumeGrid->Delete();
  //---------------------------------------------------------------------------------------------







/*
   // Inicializando as partículas
  this->InitializeParticles();



  // Removendo arquivos desatualizados
  this->DeleteOldFile("_index.txt");
  this->DeleteOldFile("metacell*.txt");
  this->DeleteOldFile("particle*.txt");
  this->DeleteOldFile("InformationFile.par");
  this->DeleteOldFile("HMParticleTracing*"


  vtkHM3DParticleTracingWriterCUDA *particleWriterCUDA = vtkHM3DParticleTracingWriterCUDA::New();


	// Verifica se existe uma placa de vídeo com suporte a CUDA e se o usuário quer utilizá-la.
	//if( this->GPUProcess && particleWriterCUDA->HostHasGPUDevice() )
	if(1)
	  {
	  particleWriterCUDA->SetInput(VolumeGrid);
	  particleWriterCUDA->SetDelta( this->TimeStepsDelta );
	  particleWriterCUDA->SetPath(this->OutputPath);
	  particleWriterCUDA->SetNumberOfParticles(this->NumberOfParticles);
	  particleWriterCUDA->SetParticles(this->InitialPositionOfParticles);
//	  cout << "\n\nInício da divisão das meta celulas : ";  this->printHour();
	  particleWriterCUDA->WriteMetaCells();
//	  cout << "\n\nFim da divisão das meta celulas : "; this->printHour();
	  particleWriterCUDA->SetNumberOfRepeatTimeSteps(this->NumberOfRepeatTimeSteps);
	  cout << "\n\nInício do particle tracing : ";  this->printHour();
	  particleWriterCUDA->WriteParticles();
	  cout << "\n\nFim do particle tracing : ";   this->printHour();
	  particleWriterCUDA->GetPositionOfParticlesInAllTimeSteps(&vec);
	  particleWriterCUDA->Delete();
	  }
	else
	  {
	  particleWriterCUDA->Delete();

    vtkHM3DParticleTracingWriter *particleWriter = vtkHM3DParticleTracingWriter::New();

    particleWriter->SetInput(VolumeGrid);
    particleWriter->SetDelta( this->TimeStepsDelta );
    particleWriter->SetPath(this->OutputPath);
    particleWriter->SetNumberOfParticles(this->NumberOfParticles);
    particleWriter->SetParticles(this->InitialPositionOfParticles);
//    cout << "\n\nInício da divisão das meta celulas : ";  this->printHour();
    particleWriter->WriteMetaCells();
//    cout << "\n\nFim da divisão das meta celulas : "; this->printHour();
    particleWriter->SetNumberOfRepeatTimeSteps(this->NumberOfRepeatTimeSteps);
    cout << "\n\nInício do particle tracing : ";  this->printHour();
    particleWriter->WriteParticles();
    cout << "\n\nFim do particle tracing : ";   this->printHour();
    particleWriter->GetPositionOfParticlesInAllTimeSteps(&vec);
    particleWriter->Delete();
	  }


//		cout << "\n\nInício da escrita do arquivos EnSight : ";		this->printHour();
		this->WriteEnSightFiles(vec);
//		cout << "\n\nFim da escrita do arquivos EnSight : ";		this->printHour();

		VolumeGrid->Delete();
		delete [] vec;

		// removendo arquivos intermediários
		this->DeleteOldFile("_index.txt");
		this->DeleteOldFile("metacell*.txt");
		this->DeleteOldFile("particle*.txt");
		this->DeleteOldFile("InformationFile.par");
*/







		}




//  cout << "this->CurrentTimeStep: " << this->CurrentTimeStep <<endl;
//  cout << "this->GetNumberOfTimeSteps(): " << this->GetNumberOfTimeSteps() <<endl;

	if( this->internalGrid )
		{
	  // terminou o processamento
		if( this->CurrentTimeStep == this->GetNumberOfTimeSteps() )
			{
		  Output->DeepCopy(Input);

		  if( this->NumberOfInsertTimeSteps > 1 )
		    this->NumberOfTimeSteps = this->NumberOfTimeSteps / this->NumberOfInsertTimeSteps;

		  this->CurrentTimeStep = 0;
      this->internalGrid->Delete();
      this->internalGrid = NULL;
			}

		if( this->CurrentTimeStep == 1 )
			Output->DeepCopy(Input);
		}
	else
		{
		vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
		return 0;
		}

	return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::WriteEnSightFiles(point *vec)
{
//	printf("\nBuilding EnSight files...");

	double NewPoint[3], point[3], bounds[6];
	char name[256];
	int id, i, j;
	float velocityTuple[3], pressureTuple, timeTuple;
	int totalTimeSteps = this->GetNumberOfTimeSteps() * this->NumberOfRepeatTimeSteps;

	// Grid vazio. Usando quando não existem partículas à exibir para que não dê erro no escritor EnSight
	vtkUnstructuredGrid *emptyGrid = vtkUnstructuredGrid::New();

	// Configurando arrays de velocidade e pressão e adicionando ao emptyGrid
	vtkDoubleArray *VelArrayEmpty = vtkDoubleArray::New();
		VelArrayEmpty->SetName("Velocity");
		VelArrayEmpty->SetNumberOfComponents(3);

	vtkDoubleArray *PressArrayEmpty = vtkDoubleArray::New();
		PressArrayEmpty->SetName("Pressure");
		PressArrayEmpty->SetNumberOfComponents(1);

	vtkDoubleArray *TimeArrayEmpty = vtkDoubleArray::New();
		TimeArrayEmpty->SetName("Time");
		TimeArrayEmpty->SetNumberOfComponents(1);

	emptyGrid->GetPointData()->AddArray(VelArrayEmpty);
	emptyGrid->GetPointData()->AddArray(PressArrayEmpty);
	emptyGrid->GetPointData()->AddArray(TimeArrayEmpty);




	//Linhas de rastro
	vtkUnstructuredGrid *gridLines = vtkUnstructuredGrid::New();

	// Configurando arrays de velocidade e pressão e adicionando ao grid de linhas de rastro
	vtkDoubleArray *VelArray = vtkDoubleArray::New();
		VelArray->SetName("Velocity");
		VelArray->SetNumberOfComponents(3);

	vtkDoubleArray *PressArray = vtkDoubleArray::New();
		PressArray->SetName("Pressure");
		PressArray->SetNumberOfComponents(1);

	vtkDoubleArray *TimeArray = vtkDoubleArray::New();
		TimeArray->SetName("Time");
		TimeArray->SetNumberOfComponents(1);

	gridLines->GetPointData()->AddArray(VelArray);
	gridLines->GetPointData()->AddArray(PressArray);
	gridLines->GetPointData()->AddArray(TimeArray);




	// Configurando arrays para serem usados pela partículas
	vtkDoubleArray *Velocity = vtkDoubleArray::New();
		Velocity->SetName("Velocity");
		Velocity->SetNumberOfComponents(3);

	vtkDoubleArray *Pressure = vtkDoubleArray::New();
		Pressure->SetName("Pressure");
		Pressure->SetNumberOfComponents(1);

	vtkDoubleArray *Time = vtkDoubleArray::New();
		Time->SetName("Time");
		Time->SetNumberOfComponents(1);




	this->internalGrid->GetBounds(bounds);

	vtkAppendFilter* SphApp = vtkAppendFilter::New();
	vtkAppendFilter* app = vtkAppendFilter::New();
	vtkCollection* SphCollection = vtkCollection::New();

	sprintf(name, "%s%s", this->GetOutputPath(), "HMParticleTracing.case");
	vtkHMEnSightWriter* EnSightWriter = vtkHMEnSightWriter::New();
		EnSightWriter->SetFileName(name);
		EnSightWriter->SetTransientGeometry(true);
		EnSightWriter->SetInput(app->GetOutput());


//	cout << "\n\nBuilding Spheres Collection..." <<endl;
	for(i=0; i < this->NumberOfParticles; i++)
		{
		SphCollection->AddItem(vtkSphereSource::New());

		vtkSphereSource* sph = vtkSphereSource::SafeDownCast(SphCollection->GetItemAsObject(i));
			sph->SetThetaResolution(20);
			sph->SetPhiResolution(20);
			sph->SetRadius(this->RadiusOfParticles);
		}




  // Salva as partículas. Foi usado para validação da solução em GPU. TUDO OK!!!!
  //---------------------------
  FILE *fpar;
  char fileName[256];
  strcpy(fileName, this->GetOutputPath() );
  strcat(fileName, "a_Particles_Path_Validation.txt");
  fpar = fopen(fileName, "w");
  //---------------------------




	for(i=0; i < totalTimeSteps; i++)
		{
		// Teste da solução em GPU
    //---------------------------
		fprintf(fpar, "\nTIME %d\n", i);
		//---------------------------



		for(j=0; j < this->NumberOfParticles; j++)
			{
			id = i + (j * totalTimeSteps);


			if( (vec[id].x > bounds[1]) || (vec[id].x < bounds[0]) || // vec[id].x > xMax or vec[id].x < xMin
					(vec[id].y > bounds[3]) || (vec[id].y < bounds[2]) || // vec[id].y > yMax or vec[id].y < yMin
					(vec[id].z > bounds[5]) || (vec[id].z < bounds[4]) )  // vec[id].z > zMax or vec[id].z < zMin
				{
				continue;
				}

			vtkSphereSource* sph = vtkSphereSource::SafeDownCast(SphCollection->GetItemAsObject(j));

			point[0] = vec[id].x;
			point[1] = vec[id].y;
			point[2] = vec[id].z;

			velocityTuple[0] = vec[id].velocityX;
			velocityTuple[1] = vec[id].velocityY;
			velocityTuple[2] = vec[id].velocityZ;
			pressureTuple    = vec[id].pressure;
			timeTuple        = vec[id].time;

			sph->SetCenter(point);
			sph->Update();





		  // Salva as partículas. Usado para validação da solução em GPU. TUDO OK!!!!
		  //---------------------------
		  fprintf(fpar, "%f %f %f %f %f %f %f %f\n",
		          point[0], point[1], point[2],
		          velocityTuple[0], velocityTuple[1], velocityTuple[2],
		          pressureTuple, timeTuple);
      //---------------------------




			vtkPolyData *TempPoly = vtkPolyData::New();
				TempPoly->DeepCopy( sph->GetOutput() );


			for(int k = 0; k < TempPoly->GetNumberOfPoints(); k++)
				{
				Velocity->InsertNextTuple( velocityTuple );
				Pressure->InsertNextTuple( &pressureTuple );
				Time->InsertNextTuple( &timeTuple );
				}


			SphApp->AddInput(TempPoly);
			SphApp->Update();

			TempPoly->Delete();
			}



		if( (i > 0) && (i < totalTimeSteps) )
			this->BuildGridLines(gridLines, vec, i, bounds);


		// Appends all particles, for one time step
		if(SphApp->GetNumberOfInputConnections(0) == 0) // Se não existirem partículas no passo de tempo adicionar um grid vazio
			{
			app->AddInput(emptyGrid);
			}
		else
			{
			SphApp->Update();

			SphApp->GetOutput()->GetPointData()->RemoveArray("Normals");
			SphApp->GetOutput()->GetPointData()->AddArray(Velocity);
			SphApp->GetOutput()->GetPointData()->AddArray(Pressure);
			SphApp->GetOutput()->GetPointData()->AddArray(Time);
			SphApp->GetOutput()->Update();

			app->AddInput(SphApp->GetOutput());

			Velocity->Reset();
			Pressure->Reset();
			Time->Reset();
			}

		app->AddInput( gridLines );
		app->Update();

		EnSightWriter->SetTimeStep(i);
		EnSightWriter->Write();

		app->RemoveAllInputs();
		SphApp->RemoveAllInputs();
		}



  // Teste da solução em GPU
  //---------------------------
  fclose(fpar);
  //---------------------------



	EnSightWriter->WriteCaseFile(totalTimeSteps);

	for(i=0; i < SphCollection->GetNumberOfItems(); i++)
		{
		vtkSphereSource* sph = vtkSphereSource::SafeDownCast(SphCollection->GetItemAsObject(i));
			sph->Delete();
		}

	gridLines->Delete();
	emptyGrid->Delete();

	EnSightWriter->Delete();
	app->Delete();
	SphApp->Delete();
	SphCollection->Delete();

	VelArrayEmpty->Delete();
	PressArrayEmpty->Delete();
	TimeArrayEmpty->Delete();
	VelArray->Delete();
	PressArray->Delete();
	TimeArray->Delete();
	Velocity->Delete();
	Pressure->Delete();
	Time->Delete();

//	printf(" [OK]\n\n");
}

// método auxiliar para debug
//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::PrintArray(vtkDataArray *array)
{
	double tuple3[3], tuple;

	cout << array->GetName() <<endl;
	cout << array->GetNumberOfTuples() <<endl;

	if (array->GetNumberOfComponents() == 3)
	{
		for(int i=0; i<array->GetNumberOfTuples(); i++)
			{
			array->GetTuple(i, tuple3);

			cout << "i: " << i << "\t" << tuple3[0] << "\t" << tuple3[1] << "\t" << tuple3[2] << endl;
			}
	}


	if (array->GetNumberOfComponents() == 1)
	{
		for(int i=0; i<array->GetNumberOfTuples(); i++)
			{
			array->GetTuple(i, &tuple);

			cout << "i: " << i << "\t" << tuple << endl;
			}
	}



}

// método auxiliar para debug
//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::TestPart(vtkPolyData *poly1)
{
	vtkDataArray *velocity1;
	vtkDataArray *pressure1;
	vtkDataArray *time1;


	velocity1 = poly1->GetPointData()->GetArray("Velocity");
	pressure1 = poly1->GetPointData()->GetArray("Pressure");
	time1     = poly1->GetPointData()->GetArray("Time");



	double velocity[3];

	velocity1->GetTuple(0, velocity);

	cout << "\tVELOCIDADE: " << velocity[0] << "\t" << velocity[1] << "\t" << velocity[2] << endl;




	double pressure;

	pressure1->GetTuple(0, &pressure);

	cout << "\tPRESSÃO: " << pressure << endl;




	double time;

	time1->GetTuple(0, &time);

	cout << "\tTIME: " << time << endl;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::BuildGridLines(vtkUnstructuredGrid *gridLines, point *vec, int step, double *bounds)
{
	int j, ActualId, LastId, pointId;
	double p1[3], p2[3], velocityTuple[3], pressureTuple, timeTuple;
	int totalTimeSteps = this->GetNumberOfTimeSteps() * this->NumberOfRepeatTimeSteps;

	vtkIdList* pointList = vtkIdList::New();
	vtkPoints* points = vtkPoints::New();
	vtkDataArray* velocity = gridLines->GetPointData()->GetArray("Velocity");
	vtkDataArray* pressure = gridLines->GetPointData()->GetArray("Pressure");
	vtkDataArray* time = gridLines->GetPointData()->GetArray("Time");

	if( !gridLines->GetPoints() )
		gridLines->SetPoints(points);


	for(j = 0; j < this->NumberOfParticles; j++)
		{
		ActualId = step + j * totalTimeSteps;
		LastId = step-1 + j * totalTimeSteps;


		// se o ponto esta FORA da bounding box (min > vec[ActualId] > max)
		if( ! ((vec[ActualId].x > bounds[0]) && (vec[ActualId].x < bounds[1]) &&
				   (vec[ActualId].y > bounds[2]) && (vec[ActualId].y < bounds[3]) &&
				   (vec[ActualId].z > bounds[4]) && (vec[ActualId].z < bounds[5])) )
			{
			continue;
			}


		// se o ponto esta FORA da bounding box (min > vec[LastId] > max)
		if( ! ((vec[LastId].x > bounds[0]) && (vec[LastId].x < bounds[1]) &&
					 (vec[LastId].y > bounds[2]) && (vec[LastId].y < bounds[3]) &&
					 (vec[LastId].z > bounds[4]) && (vec[LastId].z < bounds[5])) )
			{
			continue;
			}


		p1[0] = vec[ActualId].x;
		p1[1] = vec[ActualId].y;
		p1[2] = vec[ActualId].z;

		p2[0] = vec[LastId].x;
		p2[1] = vec[LastId].y;
		p2[2] = vec[LastId].z;

		pointId = pointList->InsertNextId( gridLines->GetPoints()->InsertNextPoint(p1) );

		velocityTuple[0] = vec[ActualId].velocityX;
		velocityTuple[1] = vec[ActualId].velocityY;
		velocityTuple[2] = vec[ActualId].velocityZ;
		velocity->InsertNextTuple3(velocityTuple[0], velocityTuple[1], velocityTuple[2]);

		pressureTuple = vec[ActualId].pressure;
		pressure->InsertNextTuple(&pressureTuple);

		timeTuple = vec[ActualId].time;
		time->InsertNextTuple(&timeTuple);

		pointId = pointList->InsertNextId( gridLines->GetPoints()->InsertNextPoint(p2) );



//		cout << "Linha: " << vec[ActualId].x << "\t" << vec[ActualId].y << "\t" << vec[ActualId].z
//		                      << "\t" << vec[ActualId].velocityX << "\t" << vec[ActualId].velocityY << "\t" << vec[ActualId].velocityZ
//		                      << "\t" << vec[ActualId].pressure << "\t" << vec[ActualId].time << magnitudeTuple << endl;



		velocityTuple[0] = vec[LastId].velocityX;
		velocityTuple[1] = vec[LastId].velocityY;
		velocityTuple[2] = vec[LastId].velocityZ;
		velocity->InsertNextTuple3(velocityTuple[0], velocityTuple[1], velocityTuple[2]);

		pressureTuple = vec[LastId].pressure;
		pressure->InsertNextTuple(&pressureTuple);

		timeTuple = vec[LastId].time;
		time->InsertNextTuple(&timeTuple);

		gridLines->InsertNextCell(VTK_LINE, pointList);
		pointList->Reset();
		}

	points->Delete();
	pointList->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::SetOutputPath(char* c)
{
	sprintf(this->OutputPath, "%s%s", c, "/");
	this->Modified();
}

//----------------------------------------------------------------------------
char* vtkHM3DParticlesTracingFilter::GetOutputPath()
{
	return this->OutputPath;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::DeleteOldFile(string str)
{
	char del_command[512];
	char mask[30];
	int length;
	length = str.copy(mask,30,0);
	mask[length]='\0';

	//Apaga arquivos anteriormente criados (Windows e Linux)
	#ifdef _WIN32

		char tmp[512];
		char *path = this->GetOutputPath();
		int j = 0;

		for(int i = 0; i < 512; i++)
			{
			tmp[j] = path[i];

			if( path[i] == '/' )
				tmp[j] = '\\';

			j++;
			}
		sprintf(del_command, "%s%s%s", "/c del ", tmp, mask);

		//Este método pertence a API do Windows "windows.h"
		ShellExecute(NULL, NULL, "cmd.exe", del_command, NULL, SW_HIDE);

	#else
		char tmp[512];
		char path[512];
		sprintf(path, "%s", this->GetOutputPath());
		int j = 0;

		// verifica se o path contém espaços nos nomes das pastas
		// e insere uma barra invertida antes de cada espaço
		for(int i = 0; i < 512; i++)
			{
			tmp[j] = path[i];

			if( path[i] == ' ' )
				{
				tmp[j] = '\\';
				tmp[j+1] = ' ';
				j++;
				}
			j++;
			}
		sprintf(del_command, "%s%s%s", "/bin/rm -f ", tmp, mask);

		if( system(del_command) )
			{
		  cout << "del_command: " << del_command <<endl;
			 vtkErrorMacro(<< "Error deleting files: ");
//			 return 1;
			}

	#endif
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::ExtractVolume(vtkHM3DModelGrid *in, vtkHM3DModelGrid *out)
{
	vtkIdList *OldPointList = vtkIdList::New();
	vtkIdList *NewPointList = vtkIdList::New();

	vtkIdList *meshList = vtkIdList::New();
		meshList->SetNumberOfIds(in->GetNumberOfPoints());

	vtkPoints *points = vtkPoints::New();
		out->SetPoints(points);

	double p[3];
	int OldPointId, OldMeshId, NewPointId;
	int *wasVisited = new int[in->GetNumberOfPoints()];

	for(int i=0; i < in->GetNumberOfPoints(); i++)
		wasVisited[i] = -1;

	for(int i=0; i < in->GetNumberOfCells(); i++)
		{
		if(in->GetCellType(i) == VTK_TETRA)
			{
			in->GetCellPoints(i, OldPointList);
			for(int j=0; j < 4; j++)
				{
				NewPointList->SetNumberOfIds(4);
				OldPointId = OldPointList->GetId(j);
				if( wasVisited[OldPointId] == -1 )
					{
					OldMeshId = in->GetMeshId()->GetId( OldPointId );
					in->GetPoints()->GetPoint( OldPointId, p );
					NewPointId = out->GetPoints()->InsertNextPoint(p);
					NewPointList->SetId( j, NewPointId );
					wasVisited[OldPointId] = NewPointId;
					meshList->SetId( NewPointId, OldMeshId );
					}
				else
					{
					NewPointId = wasVisited[OldPointId];
					NewPointList->SetId( j, NewPointId );
					}
				}
			out->InsertNextCell(VTK_TETRA, NewPointList);
			OldPointList->Reset();
			NewPointList->Reset();
			}
		}

	out->SetMeshId(meshList);
	out->SetNumberOfBlocks(in->GetNumberOfBlocks());
	out->SetVirtualPointsOfCaps(in->GetVirtualPointsOfCaps());
	out->SetInstantOfTime(in->GetInstantOfTime());
	out->SetDegreeOfFreedom(in->GetDegreeOfFreedom());

	NewPointList->Delete();
	OldPointList->Delete();

	meshList->Delete();
	points->Delete();
	delete [] wasVisited;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::RandomCoordinates(double sphereCenter[3], double sphereRadius, double randomPoint[3])
{
	//Divide-se a esfera em oito pedaços e, a cada iteração, um ponto aleatório dentro de um dos 8 pedaços é escolhido.

	double Xmin, Xmax, Ymin, Ymax, Zmin, Zmax, tmp;

	Xmin = sphereCenter[0] - sphereRadius;
	Xmax = sphereCenter[0] + sphereRadius;

	Ymin = sphereCenter[1] - sphereRadius;
	Ymax = sphereCenter[1] + sphereRadius;

	Zmin = sphereCenter[2] - sphereRadius;
	Zmax = sphereCenter[2] + sphereRadius;


	if ( this->cell == 0 )
		{
		srand ( time(NULL) );
		this->cell = 1;
		}

	if(this->cell > 8)
		this->cell = 1;

	switch( this->cell )
		{
	// gerar um double tal que sphereCenter[i] <= randomPoint[i] <= max
	//	double X = XMin + rand() * (XMax - XMin) / RAND_MAX;

		case 1:
			{
			randomPoint[0] = sphereCenter[0] + rand() * (Xmax - sphereCenter[0]) / RAND_MAX;
			randomPoint[1] = sphereCenter[1] + rand() * (Ymax - sphereCenter[1]) / RAND_MAX;
			randomPoint[2] = sphereCenter[2] + rand() * (Zmax - sphereCenter[2]) / RAND_MAX;
			break;
			}

		case 2:
			{
			randomPoint[0] = Xmin + rand() * (sphereCenter[0] - Xmin) / RAND_MAX;
			randomPoint[1] = sphereCenter[1] + rand() * (Ymax - sphereCenter[1]) / RAND_MAX;
			randomPoint[2] = sphereCenter[2] + rand() * (Zmax - sphereCenter[2]) / RAND_MAX;
			break;
			}

		case 3:
			{
			randomPoint[0] = Xmin + rand() * (sphereCenter[0] - Xmin) / RAND_MAX;
			randomPoint[1] = sphereCenter[1] + rand() * (Ymax - sphereCenter[1]) / RAND_MAX;
			randomPoint[2] = Zmin + rand() * (sphereCenter[2] - Zmin) / RAND_MAX;
			break;
			}

		case 4:
			{
			randomPoint[0] = sphereCenter[0] + rand() * (Xmax - sphereCenter[0]) / RAND_MAX;
			randomPoint[1] = sphereCenter[1] + rand() * (Ymax - sphereCenter[1]) / RAND_MAX;
			randomPoint[2] = Zmin + rand() * (sphereCenter[2] - Zmin) / RAND_MAX;
			break;
			}

		case 5:
			{
			randomPoint[0] = sphereCenter[0] + rand() * (Xmax - sphereCenter[0]) / RAND_MAX;
			randomPoint[1] = Ymin + rand() * (sphereCenter[1] - Ymin) / RAND_MAX;
			randomPoint[2] = sphereCenter[2] + rand() * (Zmax - sphereCenter[2]) / RAND_MAX;
			break;
			}

		case 6:
			{
			randomPoint[0] = Xmin + rand() * (sphereCenter[0] - Xmin) / RAND_MAX;
			randomPoint[1] = Ymin + rand() * (sphereCenter[1] - Ymin) / RAND_MAX;
			randomPoint[2] = sphereCenter[2] + rand() * (Zmax - sphereCenter[2]) / RAND_MAX;
			break;
			}

		case 7:
			{
			randomPoint[0] = Xmin + rand() * (sphereCenter[0] - Xmin) / RAND_MAX;
			randomPoint[1] = Ymin + rand() * (sphereCenter[1] - Ymin) / RAND_MAX;
			randomPoint[2] = Zmin + rand() * (sphereCenter[2] - Zmin) / RAND_MAX;
			break;
			}

		case 8:
			{
			randomPoint[0] = sphereCenter[0] + rand() * (Xmax - sphereCenter[0]) / RAND_MAX;
			randomPoint[1] = Ymin + rand() * (sphereCenter[1] - Ymin) / RAND_MAX;
			randomPoint[2] = Zmin + rand() * (sphereCenter[2] - Zmin) / RAND_MAX;
			break;
			}
	}


	double euclidian_distance = sqrt(pow(randomPoint[0]-sphereCenter[0], 2) +
																	 pow(randomPoint[1]-sphereCenter[1], 2) +
																	 pow(randomPoint[2]-sphereCenter[2], 2));

	// teste para determinar se o ponto esta dentro da esfera
	if( euclidian_distance > sphereRadius )
		this->RandomCoordinates(sphereCenter, sphereRadius, randomPoint);

	this->cell++;
}

//----------------------------------------------------------------------------
void vtkHM3DParticlesTracingFilter::InsertTimeSteps(vtkHM3DModelGrid *in, int times)
{
	// Método que insere novos step entre dois já existentes.
	// Entre os steps existe um intervalo, a divisão deste em intervalos menores é a inserção de novos steps.
 	// Por exemplo, o intervalo entre os steps 0 e 1 será divido em 2 sub-intervalos, isso implica que deve
	// ser encontrado um ponto médio entre eles, este ponto divide o intervalo.


	double tuple_ini[7], new_Tuple[7], actual_Tuple[7], next_Tuple[7];
	int beginOfActualStep, beginOfNextStep, move, newIndexTuple, newTotalTuple, indexTmp_actual, indexTmp_next;
	int z, k, i, j;

	i=j=k=z=0;
	int numPoints = in->GetNumberOfPoints();
	int steps = this->NumberOfTimeSteps;
	this->TimeStepsDelta = 0.005/times; // configurando novo valor de delta

  vtkDoubleArray *degreeOfFreedom = in->GetDegreeOfFreedom();

	 // Novo total de tuplas
	newTotalTuple = times * degreeOfFreedom->GetNumberOfTuples();

	 // Configurando novo array
	vtkDoubleArray *array = vtkDoubleArray::New();
		array->SetNumberOfComponents(7);
		array->SetNumberOfTuples( newTotalTuple );

	// Inicializando o novo array
	tuple_ini[0] = 0.0;
	tuple_ini[1] = 0.0;
	tuple_ini[2] = 0.0;
	tuple_ini[3] = 0.0;
	tuple_ini[4] = 0.0;
	tuple_ini[5] = 0.0;
	tuple_ini[6] = 0.0;

	for(i=0; i < newTotalTuple; i++)
		{
		array->InsertTuple(i, tuple_ini);
		}

  move = (numPoints*(times-1)); // calcula a qtd de posições (tuplas) será usada como deslocamento.

	for(j=0; j < (steps -1); j++)
	 	{
		cout << "Configurando novo step: "<< j << endl;
	  beginOfActualStep = numPoints * j; // primeiro índice do step corrente
	  beginOfNextStep = numPoints * (j+1); // primeiro índice do próximo step

		for(z=0; z<numPoints; z++)
			{
	  	indexTmp_actual = beginOfActualStep + z;
			indexTmp_next = beginOfNextStep + z;

			degreeOfFreedom->GetTuple(indexTmp_actual, actual_Tuple);


			if(j==0)
		  	{
				array->SetTuple(indexTmp_actual, actual_Tuple);
		  	}
		  else
		  	{
		  	indexTmp_actual = indexTmp_actual + (move*j);
				array->SetTuple(indexTmp_actual, actual_Tuple);
		  	}

				degreeOfFreedom->GetTuple(indexTmp_next, next_Tuple);

	 			for(i=1; i<times; i++)
	 				{
	 				for(k=0; k<7; k++)
	 					{
	 					// cáculo do ponto médio
	 					new_Tuple[k] =  (double)i/times * next_Tuple[k] + (1 - (double)i/times ) * actual_Tuple[k];
	 					}
	 				newIndexTuple = (i*numPoints) + indexTmp_actual;
	 				array->SetTuple(newIndexTuple, new_Tuple);
	 				}

			}

		array->Squeeze();
	 	}

	degreeOfFreedom->Reset();
	degreeOfFreedom->Squeeze();



  in->SetDegreeOfFreedom(array);
  array->Delete();

  this->NumberOfTimeSteps = this->NumberOfTimeSteps * times;
  this->CurrentTimeStep = this->CurrentTimeStep * times;
  in->SetNumberOfBlocks(this->NumberOfTimeSteps);
}

//----------------------------------------------------------------------------
//void vtkHM3DParticlesTracingFilter::GetGridOfInstantOfTime(int TimeStep, vtkHM3DModelGrid *grid, vtkUnstructuredGrid *out)
//{
//	vtkDoubleArray *DegreeOfFreedomArray = vtkDoubleArray::New();
//	DegreeOfFreedomArray->SetNumberOfComponents( 7 );
//	DegreeOfFreedomArray->SetNumberOfTuples( grid->GetMeshId()->GetNumberOfIds() );
//
//	vtkDoubleArray *VelocityArray = vtkDoubleArray::New();
//		VelocityArray->SetNumberOfComponents(3);
//		VelocityArray->SetName("Velocity");
//
//	vtkDoubleArray *PressureArray = vtkDoubleArray::New();
//		PressureArray->SetNumberOfComponents(1);
//		PressureArray->SetName("Pressure");
//
//	int p1 = (TimeStep) * grid->GetMeshId()->GetNumberOfIds(); // primeira tupla do passo de tempo
//	int p2 = p1 + grid->GetMeshId()->GetNumberOfIds() - 1;	// última tupla do passo de tempo
//
//	// constrói um array com os valores dos graus de liberdade para 1 passo de tempo.
//	grid->GetDegreeOfFreedom()->GetTuples(p1, p2, DegreeOfFreedomArray);
//
//	double tuple[7];
//
//	//constrói e seta no grid os array de pressão e velocidade
//	for(int i = 0; i < grid->GetNumberOfPoints() ; i++)
//		{
//		DegreeOfFreedomArray->GetTuple(grid->GetMeshId()->GetId(i), tuple);
//
//		for(int k=0; k<3; k++)
//			VelocityArray->InsertNextValue( tuple[k] );
//
//		PressureArray->InsertNextValue( tuple[3] );
//		}
//
//	out->DeepCopy(this->internalGrid);
//	out->GetPointData()->AddArray(PressureArray);
//	out->GetPointData()->AddArray(VelocityArray);
//
//	DegreeOfFreedomArray->Delete();
//	PressureArray->Delete();
//	VelocityArray->Delete();
//}
