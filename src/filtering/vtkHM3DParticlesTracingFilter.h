#ifndef VTKHM3DPARTICLESTRACINGFILTER_H_
#define VTKHM3DPARTICLESTRACINGFILTER_H_


#include "vtkDataSetAlgorithm.h"
#include "vtkPoints.h"
#include "vtkHM3DModelGrid.h"
#include <string>

#include "vtkHM3DParticleTracingWriter.h"

using namespace std;

/// O cálculo de trajetória de partículas foi implementado no HeMoLab como um filtro.
/// Recebe dados de entrada (malha de volume+vetores no formato EnSight), configura opções de processamento,
/// executa e descarrega em disco arquivos no formato EnSight para visualização do resultados.
/// O resultado esperado é o conjunto de todas as partículas e suas trajetórias.
class VTK_EXPORT vtkHM3DParticlesTracingFilter : public vtkDataSetAlgorithm
{
public:
	static vtkHM3DParticlesTracingFilter *New();
	vtkTypeRevisionMacro(vtkHM3DParticlesTracingFilter, vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);
	virtual void Modified();
	int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);
  void AddInput(vtkDataSet* input);
  int FillInputPortInformation(int port, vtkInformation* info);

  /// Configura o diretório de saída dos arquivos
	void SetOutputPath(char* c);

	/// Recupera o diretório de saída dos arquivos
	char* GetOutputPath();

	/// Apaga arquivos no diretório informado em vtkHM3DParticlesTracingFilter::SetOutputPath usando uma máscara (str).
	/// Os caracteres * e ? são aceitos na máscara.
	void DeleteOldFile(string str);

	/// Configura o passo de tempo corrente
	void SetCurrentTimeStep(int current);

	/// Recupera o passo de tempo corrente
	int GetCurrentTimeStep();

	/// Configura a quantidade de passos de tempo
	void SetNumberOfTimeSteps(int steps);

	/// Recupera a quantidade de passos de tempo
	int GetNumberOfTimeSteps();

  /// Configura a duração do passo (instantes) de tempo.
	vtkSetMacro(TimeStepsDelta, double);

  /// Recupera a duração do passo (instantes) de tempo.
	vtkGetMacro(TimeStepsDelta, double);

  /// Configura o número de repetições do pulso cardíaco.
	vtkSetMacro(NumberOfRepeatTimeSteps, int);

  /// Recupera o número de repetições do pulso cardíaco.
	vtkGetMacro(NumberOfRepeatTimeSteps, int);

  /// Configura quantos passos de tempo serão interpolados entre dois passos originais.
	/// Serve para suavizar os deslocamentos das partículas aumentando a quantidade de passos de tempo.
	vtkSetMacro(NumberOfInsertTimeSteps, int);

  /// Configura quantos passos de tempo serão interpolados entre dois passos originais.
	/// Veja vtkHM3DParticlesTracingFilter::SetNumberOfInsertTimeSteps
	vtkGetMacro(NumberOfInsertTimeSteps, int);

	/// Configura o raio das partículas
	vtkSetMacro(RadiusOfParticles, double);

	/// Recupera o raio das partículas
	vtkGetMacro(RadiusOfParticles, double);

	///
	vtkSetMacro(NumberOfSphereParticles, int);
	vtkGetMacro(NumberOfSphereParticles, int);

	/// Habilita o processamento em GPU.
	/// Se o método vtkHM3DParticleTracingWriterCUDA::HostHasGPUDevice retornar false o procesamento ocorrerá em CPU.
  vtkSetMacro(GPUProcess, int);

  /// Habilita o processamento em GPU.
  /// Veja vtkHM3DParticlesTracingFilter::SetGPUProcess
  vtkGetMacro(GPUProcess, int);

	// Configura as partículas de iniciais
  void SetSource(vtkDataSet *source);

  // Recupera as partículas de iniciais
  vtkDataSet *GetSource();

  /// Imprime a hora do sistema
  void printHour();

protected:
	vtkHM3DParticlesTracingFilter();
	~vtkHM3DParticlesTracingFilter();

	/// Métodos auxiliares para debug
	void TestPart(vtkPolyData *poly1);

	/// Métodos auxiliares para debug
	void PrintArray(vtkDataArray *array);

	/// Extrai o volume e informações adicionais da malha de entrada.
	void ExtractVolume(vtkHM3DModelGrid *in, vtkHM3DModelGrid *out);

	/// Inicializa o vetor de trajetórias com as coordenada iniciais da partículas
	void InitializeParticles();

  /// Inicializa o vetor de trajetórias com as coordenada iniciais da partículas (apenas para testes de desemenho)
  void InitializeParticles(int nParticles);

	/// Método para geração dos resultados
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

	/// Escreve os arquivos no format EnSight. Somente partículas (*.case)
	void WriteEnSightFiles(point *vec);

	/// Constrói a geometria das trajetórias
	void BuildGridLines(vtkUnstructuredGrid *gridLine, point *vec, int step, double *bounds);

	/// Mapeia os dados de entrada para a estrutura interna usada pela classe.
	void Build3DModelGrid();

	/// Insere passos de tempo artificiais entre dois outros.
  void InsertTimeSteps(vtkHM3DModelGrid *in, int times);

  /// Gera coordenadas randômicas dentro da esfera defina pelos parâmetros centro e raio
	void RandomCoordinates(double centro[3], double raio, double random[3]);

	/// Quantidade de partículas usadas
	int NumberOfParticles;

	///
	int NumberOfSphereParticles;

	// Raio das partículas
	double RadiusOfParticles;

  /// Número de passos da simulação
	int NumberOfTimeSteps;

  /// Duração de cada passo (instante de tempo) da simulação
	double TimeStepsDelta;

  /// Instante de tempo corrente em cálculo.
	int CurrentTimeStep;

	/// Grid interno
	vtkHM3DModelGrid* internalGrid;

  /// Diretório de saída dos arquivos
	char OutputPath[256];

  /// Coordenadas iniciais das partículas
	vtkPoints* InitialPositionOfParticles;

  /// Número de repetições do pulso cardíaco.
	int NumberOfRepeatTimeSteps;

  /// Número de passos de tempo artificiais entre dois passos originais
	int NumberOfInsertTimeSteps;

	//Variável usada na geração de coordenadas aleatórias
	int cell;

	/// Define se a GPU será ou não utilizada
  bool GPUProcess;


private:
	vtkHM3DParticlesTracingFilter(const vtkHM3DParticlesTracingFilter&);  // Not implemented.
	void operator=(const vtkHM3DParticlesTracingFilter&);  // Not implemented.
};
#endif /*VTKHM3DPARTICLESTRACINGFILTER_H_*/
