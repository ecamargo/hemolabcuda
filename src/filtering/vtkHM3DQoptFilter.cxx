/*
 * $Id$
 */

#include "vtkHM3DQoptFilter.h"
#include "vtkMeshData.h"
#include "vtkQopt.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkCallbackCommand.h"
#include "vtkObjectFactory.h"
#include "vtkUnstructuredGrid.h"
#include "vtkProcessModule.h"
#include "vtkExecutive.h"
#include "vtkKWProgressGauge.h"

vtkCxxRevisionMacro(vtkHM3DQoptFilter, "$Revision: 1.67 $");
vtkStandardNewMacro(vtkHM3DQoptFilter);

static void QoptFilterUpdateProgress(vtkObject *_surface, unsigned long, void *_qoptFilter, void *)
{
  vtkQopt *qopt 							= reinterpret_cast<vtkQopt *>(_surface);  
  vtkHM3DQoptFilter *qoptFilter = reinterpret_cast<vtkHM3DQoptFilter *>(_qoptFilter);
  qoptFilter->SetProgress(qopt->GetProgress());
  qoptFilter->SetProgressText(qopt->GetProgressText());
  qoptFilter->UpdateProgress(qopt->GetProgress());
}

//----------------------------------------------------------------------------
vtkHM3DQoptFilter::vtkHM3DQoptFilter()
{
	vtkDebugMacro(<<"vtkHM3DQoptFilter::vtkHM3DQoptFilter()");
	this->ApplyFilter = 0;
	this->progress		= NULL;	
}

//----------------------------------------------------------------------------
vtkHM3DQoptFilter::~vtkHM3DQoptFilter()
{
	vtkDebugMacro(<<"vtkHM3DQoptFilter::~vtkHM3DQoptFilter()");		
}

//----------------------------------------------------------------------------
void vtkHM3DQoptFilter::SetProgressBar(vtkKWProgressGauge *p)
{
	this->progress = p;
}

//----------------------------------------------------------------------------
int vtkHM3DQoptFilter::FillInputPortInformation(int port,vtkInformation *info)
{
  if (!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  if (port == 1)
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DQoptFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkHM3DQoptFilter::ApplyQoptFilter(char *file)
{
  vtkDebugMacro(<<"vtkHM3DQoptFilter::ApplyQoptFilter()");		
	this->Modified();	
	strcpy(this->cfgFileName, file);
	
	// Esta flag é usada para indicar que o filtro pode ser aplicado corretamente
	this->ApplyFilter = 1;	
}

//----------------------------------------------------------------------------
void vtkHM3DQoptFilter::ApplyManualConfigFilter(int maxit1, int maxit2,  int maxit3, int maxit4,
																		int maxclu, int addnod, int antiele, int quali,  int seeall)
{
  vtkDebugMacro(<<"vtkHM3DQoptFilter::ApplyManualConfigFilter()");			
	
	this->MaxIter[0] 		= maxit1;
	this->MaxIter[1] 		= maxit2;
	this->MaxIter[2] 		= maxit3;
	this->MaxIter[3] 		= maxit4;			
	this->MaxCluster 		= maxclu;
	this->AddNodeFlag 	= addnod;
	this->AntiElemFlag 	= antiele;
	this->QualityFlag		= quali;
	this->SeeAllFlag		= seeall;
	
	this->Modified();	

	// Esta flag é usada para indicar que o filtro pode ser aplicado corretamente
	this->ApplyFilter = 2;	
}

//----------------------------------------------------------------------------
int vtkHM3DQoptFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
	// get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// Pega input e output corrente  
  vtkMeshData *input = vtkMeshData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkMeshData *output = vtkMeshData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

	// Certifica que a saída esteja no forma de volume (e não superfície)
	input->SetOutputAsVolume();

	// Copia informações do input para o output
	output->CopyStructure(input);	
	
	// Filtro não está pronto para ser aplicado.
	// Isso ocorre se um dos arquivos de configuração não tiver sido configurado corretamente.
	if(!this->ApplyFilter)
		{
		vtkDebugMacro(<<"vtkHM3DQoptFilter::RequestData() :: Filter is not ready to be applied");
		return 1;		
		}

	// Cria objeto que irá processar malha
  vtkQopt* qopt = vtkQopt::New();	
  
  // Passa input e output correntes para o filtro
  qopt->SetInput (input);
  qopt->SetOutput(output);
  
	// Callback para gerenciar barra de progresso
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
	  cbc->SetClientData(this);
  	cbc->SetCallback(QoptFilterUpdateProgress);
  qopt->AddObserver(vtkCommand::ProgressEvent, cbc);  

  // Processa malha passando o arquivo de configuração como parâmetro ou parâmetros manuais
  // Resultados são armazenados no "output" passado anteriormente  
	if(this->ApplyFilter == 1)
		qopt->ProcessFilter(this->cfgFileName);
	else  // ApplyFilter == 2
		qopt->ProcessFilter(this->MaxIter, 			this->MaxCluster,  this->AddNodeFlag,
												this->AntiElemFlag,	this->QualityFlag, this->SeeAllFlag);
  
	// Set the volume information to be visualized	
	output->SetOutputAsVolume();	
	
	// Remove filtro e a Callback após o uso
	cbc->Delete();	
	qopt->Delete();

	cout << *output;
  return 1;
}
