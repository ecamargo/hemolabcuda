/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM3DQoptFilter

  Author: Rodrigo L. S. Silva, Jan Palach

=========================================================================*/
// .NAME vtkHM3DQoptFilter
// .SECTION Description
// Apply Qopt filter over a vtkUnstructuredGrid object. The output of this
// filter is also a vtkUnstructuredGrid

#ifndef __vtkHM3DQoptFilter_h
#define __vtkHM3DQoptFilter_h

#include "vtkDataSetAlgorithm.h"
#include "vtkKWProgressGauge.h"

class VTK_EXPORT vtkHM3DQoptFilter : public vtkDataSetAlgorithm
{
public:
  vtkTypeRevisionMacro(vtkHM3DQoptFilter,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkHM3DQoptFilter *New();

  // Description:
  // Apply Filter using a configuration file
	void ApplyQoptFilter(char *);

	// Description:
  // Apply Manual Configuration Settings
	void ApplyManualConfigFilter(int maxit1, int maxit2, int maxit3, int maxit4,
															 int maxclu, int addnod, int antiele, int quali, int seeall);

  // Description:
  // Set the Pointer to the main Progress Bar.
	void SetProgressBar(vtkKWProgressGauge*p);
	
protected:
  vtkHM3DQoptFilter();
  ~vtkHM3DQoptFilter();

  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  
  //FillInputInformation.
  int FillInputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Description:
  // CFG File name 
  char cfgFileName[256];

  // Description:
  // Pointer to the main Progress Bar is passed to allow this filter to update it.
  vtkKWProgressGauge* progress;
    
  // Description:
  // Controls if filter is ready to be applied 
  // 0 - Not Ready
  // 1 - File input
  // 2 - Manual Parameters
  int ApplyFilter; 
  
	// Description:
	// Manual parameters
	int MaxIter[4];
	int MaxCluster;
	int AddNodeFlag;
	int AntiElemFlag;
	int QualityFlag;
	int SeeAllFlag;	

private:
  vtkHM3DQoptFilter(const vtkHM3DQoptFilter&);  // Not implemented.
  void operator=(const vtkHM3DQoptFilter&);  // Not implemented.
};

#endif

