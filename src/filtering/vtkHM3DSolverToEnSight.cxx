/*
 * $Id$
 */
#include "vtkHM3DSolverToEnSight.h"
#include "vtkHM3DModelGrid.h"
#include "vtkHMEnSightWriter.h"

#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkCommand.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

#ifdef _WIN32
	#include "windows.h"
#endif

#include <string>

vtkCxxRevisionMacro(vtkHM3DSolverToEnSight, "$Rev$");
vtkStandardNewMacro(vtkHM3DSolverToEnSight);


//----------------------------------------------------------------------------
vtkHM3DSolverToEnSight::vtkHM3DSolverToEnSight()
{
	strcpy(this->OutputPath, "");
	this->TransientGeometryFactor = 0.0;
	this->TransientGeometry = false;
  this->ByteOrder = 1; //Default is big endian
}

//----------------------------------------------------------------------------
vtkHM3DSolverToEnSight::~vtkHM3DSolverToEnSight()
{

}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM3DModelGrid");
	return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::Modified()
{
	this->Superclass::Modified();
	this->InvokeEvent(vtkCommand::ModifiedEvent, this);
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetOutputPath(char* path)
{	
	sprintf(this->OutputPath, "%s%s", path, "/"); 
	
	this->Modified();
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetLowLimit(int limit)
{
	this->LowLimit = limit;
}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::GetLowLimit()
{
	return this->LowLimit;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetUpLimit(int limit)
{
	this->UpLimit = limit;
}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::GetUpLimit()
{
	return this->UpLimit;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetInput(vtkDataObject *input)
{
	this->Superclass::SetInput(input);
	
//	setar intervalo de passos de tempo
	vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(this->GetInput());	
	this->SetLowLimit(0);
	this->SetUpLimit( grid->GetNumberOfBlocks() );
}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::GetInformationNumberOfTimeSteps()
{
	if( this->GetInput() )
		{	
		vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(this->GetInput());
		return grid->GetNumberOfBlocks();
		}
	return 0;
}

//----------------------------------------------------------------------------
double vtkHM3DSolverToEnSight::GetInformationInitialTimeOfTimeSteps()
{
	if( this->GetInput() )
		{	
		vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(this->GetInput());
		return grid->GetInstantOfTime()->GetValue(0);
		}
	return 0;
}

//----------------------------------------------------------------------------
double vtkHM3DSolverToEnSight::GetInformationFinalTimeOfTimeSteps()
{
	if( this->GetInput() )
		{	
		vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(this->GetInput());
		return grid->GetInstantOfTime()->GetValue( grid->GetNumberOfBlocks()-1 );
		}
	return 0;
}

//----------------------------------------------------------------------------
double vtkHM3DSolverToEnSight::GetInformationDurationOfTimeSteps()
{
	if( this->GetInput() )
		{	
		vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(this->GetInput());
		return grid->GetInstantOfTime()->GetValue(1) - grid->GetInstantOfTime()->GetValue(0);
		}
	return 0;
}

//----------------------------------------------------------------------------
char* vtkHM3DSolverToEnSight::GetOutputPath()
{
	return this->OutputPath;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::GetGridOfInstantOfTime(int TimeStep, vtkHM3DModelGrid *grid)
{
	vtkDoubleArray *DegreeOfFreedomArray = vtkDoubleArray::New();
	DegreeOfFreedomArray->SetNumberOfComponents( 7 );
	DegreeOfFreedomArray->SetNumberOfTuples( grid->GetMeshId()->GetNumberOfIds() );
	
	vtkDoubleArray *VelocityArray = vtkDoubleArray::New();
	VelocityArray->SetNumberOfComponents(3);
	VelocityArray->SetName("Velocity");
	
	vtkDoubleArray *DisplacementArray = vtkDoubleArray::New();
	DisplacementArray->SetNumberOfComponents(3);
	DisplacementArray->SetName("Displacement");	
	
	vtkDoubleArray *PressureArray = vtkDoubleArray::New();
	PressureArray->SetNumberOfComponents(1);
	PressureArray->SetName("Pressure");
	
	int p1 = (TimeStep) * grid->GetMeshId()->GetNumberOfIds(); // primeira tupla do passo de tempo
	int p2 = p1 + grid->GetMeshId()->GetNumberOfIds() - 1;	// última tupla do passo de tempo
	
	// constrói um array com os valores dos graus de liberdade para 1 passo de tempo.
	grid->GetDegreeOfFreedom()->GetTuples(p1, p2, DegreeOfFreedomArray);
		
	double tuple[7];
		
	//constrói e seta no grid os array de pressão, velocidade e deslocamento
	for(int i = 0; i < grid->GetNumberOfPoints() ; i++)
		{	
		DegreeOfFreedomArray->GetTuple(grid->GetMeshId()->GetId(i), tuple);
		
		for(int k=0; k<3; k++)
			VelocityArray->InsertNextValue( tuple[k] );
			
		PressureArray->InsertNextValue( tuple[3] );
		
		for(int k=4; k<7; k++)
			DisplacementArray->InsertNextValue( tuple[k] );
	
			if( this->TransientGeometry)
				{			
				double *point = grid->GetPoints()->GetPoint(i);
				
				point[0] = (this->TransientGeometryFactor * tuple[4]) + point[0];
				point[1] = (this->TransientGeometryFactor * tuple[5]) + point[1];
				point[2] = (this->TransientGeometryFactor * tuple[6]) + point[2];
						
				grid->GetPoints()->SetPoint(i, point);			
				grid->Update();
				}						
		}
	
	grid->GetPointData()->AddArray(DisplacementArray);
	grid->GetPointData()->AddArray(PressureArray);
	grid->GetPointData()->AddArray(VelocityArray);
	
	DegreeOfFreedomArray->Delete();
	PressureArray->Delete();
	DisplacementArray->Delete();
	VelocityArray->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetTransientGeometryFactor(double factor)
{
	this->TransientGeometryFactor = factor;
	
	this->Modified();
}

//----------------------------------------------------------------------------
double vtkHM3DSolverToEnSight::GetTransientGeometryFactor()
{
	return this->TransientGeometryFactor;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::SetTransientGeometry(int var)
{
	this->TransientGeometry = var; 
	
	this->Modified();
}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::GetTransientGeometry()
{
	return this->TransientGeometry;
}

//----------------------------------------------------------------------------
void vtkHM3DSolverToEnSight::GenerateCaseFiles(vtkHM3DModelGrid *grid)
{
	char name[256];	
	sprintf(name, "%s%s", this->GetOutputPath(), "HemoOut.case");
	
	vtkHMEnSightWriter *writer = vtkHMEnSightWriter::New();
		writer->SetFileName(name);
		writer->SetByteOrder( this->GetByteOrder() );
		writer->SetTransientGeometry(this->TransientGeometry);
		
    int limite_inf = this->GetLowLimit();
    int limite_sup = this->GetUpLimit();
		
//    for(int i=0; i < grid->GetNumberOfBlocks(); i++)
    for(int i=limite_inf; i < limite_sup; i++)
			{			
			this->GetGridOfInstantOfTime(i, grid);			
			writer->SetInput(grid);
			writer->SetTimeStep(i-limite_inf);
//			writer->SetTimeStep(i);
			writer->Update();
			writer->Write();
			}
        
//		writer->WriteCaseFile(grid->GetNumberOfBlocks());
		writer->WriteCaseFile(limite_sup - limite_inf);
				
		writer->Delete();
}

//----------------------------------------------------------------------------
int vtkHM3DSolverToEnSight::RequestData(vtkInformation *vtkNotUsed(request),
								 vtkInformationVector **inputVector,
								 vtkInformationVector *outputVector)
{
	// Getting objects information
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);
	
	// Getting input and output filter references
	vtkHM3DModelGrid *Input = vtkHM3DModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkHM3DModelGrid *Output = vtkHM3DModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	
	Output->DeepCopy(Input);
	
	// Verfica se o grid possui os dados do DataOut.txt
	if( !Input->GetDegreeOfFreedom() )
		{
		vtkErrorMacro(<<"This grid doesn`t have valid DataOut.txt");
		return 1;
		}
	
	char del_command[512];

	//Apaga arquivos anteriormente criados (Windows e Linux)
	#ifdef _WIN32	

		char tmp[512];
		char *path = this->GetOutputPath();
		int j = 0;

		for(int i = 0; i < 512; i++)
			{
			tmp[j] = path[i];

			if( path[i] == '/' )
				tmp[j] = '\\';

			j++;
			}
		sprintf(del_command, "%s%s%s", "/c del ", tmp, "HemoOut*.*");

		//Este método pertence a API do Windows "windows.h"
		ShellExecute(NULL, NULL, "cmd.exe", del_command, NULL, SW_HIDE);
		
	#else
		char tmp[512];
		char path[512];
		sprintf(path, "%s", this->GetOutputPath());
		int j = 0;
		
		// verifica se o path contém espaços nos nomes das pastas
		// e insere uma barra invertida antes de cada espaço
		for(int i = 0; i < 512; i++)
			{
			tmp[j] = path[i];

			if( path[i] == ' ' )
				{
				tmp[j] = '\\';
				tmp[j+1] = ' ';
				j++;
				}
			j++;
			}
		sprintf(del_command, "%s%s%s", "rm -rf ", tmp, "HemoOut*.*");

		if( system(del_command) )
			{
			 vtkErrorMacro("Error deleting files: ");
			 return 1;
			}
		
	#endif
	
		this->GenerateCaseFiles(Input);
		
	return 1;
}
