/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM3DSolverToEnSight

  Author: Eduardo Camargo

=========================================================================*/
// .NAME vtkHM3DSolverToEnSight
// .SECTION Description
// Convert one object vtkHM3DModelGrid that created from SolverGP files in EnSight files format using vtkHMEnSightWriter class

#ifndef VTKHM3DSOLVERTOENSIGHT_H_
#define VTKHM3DSOLVERTOENSIGHT_H_


#include "vtkUnstructuredGridAlgorithm.h"

class vtkHM3DModelGrid;


class VTK_EXPORT vtkHM3DSolverToEnSight : public vtkUnstructuredGridAlgorithm
{
public:
	static vtkHM3DSolverToEnSight *New();
	vtkTypeRevisionMacro(vtkHM3DSolverToEnSight, vtkUnstructuredGridAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);
	int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);
	virtual void Modified();
	
	// Description:
	// Set input DataSet
	void SetInput(vtkDataObject *input);
	
	// Description:
	// Get Number of time steps (used to set vtkPVHMSolverToEnSight)
	int GetInformationNumberOfTimeSteps();
	
	// Description:
	// Get initial time of time steps (used to set vtkPVHMSolverToEnSight)
	double GetInformationInitialTimeOfTimeSteps();
	
	// Description:
	// Get final time of time steps (used to set vtkPVHMSolverToEnSight)
	double GetInformationFinalTimeOfTimeSteps();
	
	// Description:
	// Get duration of each time steps (used to set vtkPVHMSolverToEnSight)
	double GetInformationDurationOfTimeSteps();
	
	// Description:
	// Return Grid with scalars associated (Pressure, Velocity and Displacement) in Instant of time
	void GetGridOfInstantOfTime(int TimeStep, vtkHM3DModelGrid *grid);
	
	// Description:
	// Build output files
	void GenerateCaseFiles(vtkHM3DModelGrid *grid);

	// Description:
	// Set/Get Transient Geometry (on/off)
	void SetTransientGeometry(int var);
	int GetTransientGeometry();
	
	// Description:
	// Set/Get Transient Geometry factor
	void SetTransientGeometryFactor(double factor);
	double GetTransientGeometryFactor();
	
	// Description:
	// Set/Get path for output .case files
	void SetOutputPath(char* path);
	char* GetOutputPath();
	
	// Description:
	// Set/Get lower limit
	void SetLowLimit(int limit);
	int GetLowLimit();

	// Description:
	// Set/Get upper limit
	void SetUpLimit(int limit);
	int GetUpLimit();
	
	// Description:
	// Set/Get byte order for files
  vtkSetMacro(ByteOrder, int);
	vtkGetMacro(ByteOrder, int);
  
protected:
	vtkHM3DSolverToEnSight();
	~vtkHM3DSolverToEnSight();

	// Description:
	// Upper and lower limits for writing the files
	int LowLimit;
	int UpLimit;	
	
	// Description:
	// Output path for files
	char OutputPath[256];
	
	// Description:
	// Transient Geometry factor
	double TransientGeometryFactor;
	
	// Description:
	// Transient Geometry (on/off)
	int TransientGeometry;
	
  // Description:
  // Define byte order for output files. Little Endian = 0 and Big Endian = 1, default value is 1.
  int ByteOrder;
	
	// Description:
	// Usual data generation method
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
	vtkHM3DSolverToEnSight(const vtkHM3DSolverToEnSight&);  // Not implemented.
	void operator=(const vtkHM3DSolverToEnSight&);  // Not implemented.
};
#endif /*VTKHM3DSOLVERTOENSIGHT_H_*/
