/*
 * $Id$
 */

#include "vtkHM3DTrisurfFilter.h"
#include "vtkTrisurf.h"
#include "vtkMeshData.h"
#include "vtkParamMesh.h"
#include "vtkSurfaceGen.h"
#include "vtkCallbackCommand.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkCellArray.h"
#include "vtkCommand.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkMath.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"
#include "vtkTriangleStrip.h"
#include "vtkPriorityQueue.h"
#include "vtkProcessModule.h"
#include "vtkClientServerID.h"
#include "vtkClientServerStream.h"
#include "vtkExecutive.h"
#include <string>
#include <iostream>

//-------------------------------------------------------------------------
vtkCxxRevisionMacro(vtkHM3DTrisurfFilter, "$Revision: 1.67 $");
vtkStandardNewMacro(vtkHM3DTrisurfFilter);

//-------------------------------------------------------------------------
static void TrisurfFilterUpdateProgress(vtkObject *_surface, unsigned long,
                             void *_trisurfFilter, void *)
{
  vtkTrisurf *surface = reinterpret_cast<vtkTrisurf *>(_surface);  
  vtkHM3DTrisurfFilter *trisurfFilter = reinterpret_cast<vtkHM3DTrisurfFilter *>(_trisurfFilter);
  trisurfFilter->UpdateProgress(surface->GetProgress());
}

//-------------------------------------------------------------------------
// Função conta o número de inteiros presentes em uma string
int CountNumberOfIntegers(char *inputString)
{
	int numberOfIntegers = 0;
	for(int i = 0, k = 0, lastDigit = 0; (unsigned) i < strlen(inputString); i++)
		{
   	if( !lastDigit && isdigit(inputString[i])) numberOfIntegers++;
   	lastDigit = isdigit(inputString[i]);
   	}
	return numberOfIntegers;
}

//-------------------------------------------------------------------------
// Conta e lê o número de inteiros em uma string
void readIntegers (char * inputString, int *v)
{
	for(int i = 0, k = 0, lastDigit = 0; (unsigned) i < strlen(inputString); i++)
   	{
		if ( !lastDigit && ( isdigit(inputString[i]) || inputString[i] == '-') )
			sscanf(&inputString[i], "%d", &v[k++]);
    lastDigit = isdigit(inputString[i]);
    }
}

//-------------------------------------------------------------------------
// Função para ordenar grupos. É utilizada para remover e fundir grupos
void ordenaGruposDecrescente(int *g, int start, int num)
{
	int k1, k2, a;
  for (k1=start; k1<num; k1++)
    for (k2=k1+1; k2<num; k2++)
      if (g[k1]<g[k2])
        {
        a = g[k1];
        g[k1]=g[k2];
        g[k2]=a;
        }
}

//-------------------------------------------------------------------------
// Função para ordenar grupos. É utilizada para remover e fundir grupos
void ordenaGruposCrescente(int *g, int start, int num)
{
	int k1, k2, a;
  for (k1=start; k1<num; k1++)
    for (k2=k1+1; k2<num; k2++)
      if (g[k1]>g[k2])
        {
        a = g[k1];
        g[k1]=g[k2];
        g[k2]=a;
        }
}


//-------------------------------------------------------------------------
vtkHM3DTrisurfFilter::vtkHM3DTrisurfFilter()
{
	this->currentFilter = 0;
	this->currentParameter= vtkParamMesh::New();
	this->ParameterLoaded = 0;
	sprintf(this->ResponseFilterInfo,"%s", "");
	
	this->SelectedGroup = -1;
}

//-------------------------------------------------------------------------
vtkHM3DTrisurfFilter::~vtkHM3DTrisurfFilter()
{
	if(this->currentParameter)
		this->currentParameter->Delete();
  this->currentParameter = NULL;
}

//-------------------------------------------------------------------------
int vtkHM3DTrisurfFilter::FillInputPortInformation(int port,vtkInformation *info)
{
  if (!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  if (port == 1)
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return 1;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::SetNumberOfInputConnections(int port, int n)
{
}

//-------------------------------------------------------------------------
int vtkHM3DTrisurfFilter::GetParameterLoaded()
{
	return this->ParameterLoaded;
}

//-------------------------------------------------------------------------
char* vtkHM3DTrisurfFilter::GetResponseOfFilter()
{
	this->Update();
	return this->ResponseFilterInfo;
}

//-------------------------------------------------------------------------
int vtkHM3DTrisurfFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
	int result, i, error = 0, resultVet[10];	
	double value;
	bool flag = false;
	FILE *fp;
	char aux[100];	

	// Para todos as funções que utilizam mais de um grupo
	int *gr = NULL, numGroups;
	
	// get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  
  vtkMeshData *input = vtkMeshData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkMeshData *output = vtkMeshData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
	
	// Copia informações do input para o output
	output->CopyStructure(input);
	
	vtkTrisurf *surf = vtkTrisurf::New();
	surf->SetSurfaceGen(output->GetSurface());

	// Se no input já foi carregado um arquivo de parâmetro, copia dados para o filtro corrente
	if(input->IsParamMeshLoaded())
		{
		this->ParameterLoaded = true;
		this->currentParameter->Copy(input->GetParamMesh());
		}		

	if(this->ParameterLoaded)
		{
		// Copia dados do parâmetro carregado para a variável de parâmetro da saída
		output->GetParamMesh()->Copy(this->currentParameter);

		// Define internamente no MeshData de saída que um parametro foi carregado		
		output->SetParamMeshLoaded(true);		

		// Define no SurfaceGen interno ao MeshData a varíavel de parâmetro
		output->GetSurface()->SetPar(output->GetParamMesh());

		// Constroi a Octree
		output->GetParamMesh()->BuildOctree();
		}

  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetClientData(this);
  cbc->SetCallback(TrisurfFilterUpdateProgress);
  surf->AddObserver(vtkCommand::ProgressEvent, cbc);

	if(this->currentFilter)
		{
		switch(this->currentFilter)
			{
			case TRISURF_SMOOTH:
				this->UpdateProgress(0.01);
				surf->Smooth((int)par[0], par[1], (int)par[2], (int)par[3], (int) par[4]);
			break;
			case TRISURF_SWAP_DIAGONALS:
				this->UpdateProgress(0.01);
				surf->SetCotaCopla(par[0]);
				result = surf->SwapAll();			
				sprintf(this->ResponseFilterInfo, "%d", result);			  
			break;
			case TRISURF_DIVIDE_OBTUSE:
				this->UpdateProgress(0.01);
				value  = cos(par[0]*3.141592/180.0);
				result = surf->DivideObtusos(value);
				sprintf(this->ResponseFilterInfo, "%d", result);			  
			break;
			case TRISURF_INSERT_NODES:
				this->UpdateProgress(0.01);
				surf->SetFactorH(par[0]);
				result = surf->DivideTodos();			
				sprintf(this->ResponseFilterInfo, "%d", result);			  				
			break;			
			case TRISURF_COLLAPSE:
				// Armazena colapseCote original em caso de erro
				double colapse;
				colapse = output->GetColapseCote();

		    this->UpdateProgress(0.01);
				value = par[0] * par[0];
				result = surf->ColapsaTodos(value);
				
				if(result == -1)
					{				
			    sprintf(this->ResponseFilterInfo, "Error: Aborted!");
					output->GetSurface()->Copy(output->GetMesh3d(), output->GetParamMesh() );
					output->SetColapseCote(colapse); 		    
					}			    
				else
			    sprintf(this->ResponseFilterInfo, "%d", result);
		    
			break;					
			case TRISURF_PAR:
				// Le o arquivo 'par' utilizando atributo da classe corrente (this->currentParameter)
			  fp = fopen(this->ParString, "r");
			  if (!fp) vtkErrorMacro("Unable to open file " << this->ParString);
				this->currentParameter->Read(fp);
				
				// Indica para a classe que um parametro foi carregada
				// Essa variavel será acessada via propriedade pela interface
				this->ParameterLoaded = 1;    		
				
				// Copia parametro para a saida, para que se uma outra instancia do trisurf
				// for chamada, o parametro não tenha que ser lido novamente
				output->GetParamMesh()->Copy(this->currentParameter);
				
				// Define internamente no MeshData de saída que um parametro foi carregado
				output->SetParamMeshLoaded(true);
			break;
			case TRISURF_AUTOPAR:
				// Cria as coordenadas a partir da bounding box do objeto de entrada
				this->currentParameter->ComputeParameters(output->GetBounds(), par[0]);
			
				// Indica para a classe que um parametro foi carregada
				// Essa variavel será acessada via propriedade pela interface
				this->ParameterLoaded = 1;    		
				
				// Copia parametro para a saida, para que se uma outra instancia do trisurf
				// for chamada, o parametro não tenha que ser lido novamente
				output->GetParamMesh()->Copy(this->currentParameter);
				
				// Define internamente no MeshData de saída que um parametro foi carregado
				output->SetParamMeshLoaded(true);
			break;			
			case TRISURF_REMOVE_TRES_TRI:
			  this->UpdateProgress(0.01);
			  surf->RemoveTresTri();			  											  
			break;
			case TRISURF_REMOVE_FREE_NODES:
			  this->UpdateProgress(0.01);
			  result = surf->RemoveFreeNodes();				
			  sprintf(this->ResponseFilterInfo, "%d", result);			  											
			break;
			case TRISURF_TRANSFORMATION:
		    this->UpdateProgress(0.01);
				surf->Transform(par[0], par[1], par[2], par[3], par[4],  
											  par[5], par[6], par[7], par[8]);
			break;
			case TRISURF_TEST_MESH:
				long nod,nel;
				int null;
				int orientationStatus;
				char nullMsg[128];
				char orientation[128];
				this->UpdateProgress(0.01);
				surf->TestMesh(nod, nel, null);
				
				// So faz o teste de orientação se a superfície não contiver buracos
				if(!null)
				{
					//orientationStatus = input->GetSurface()->CheckOrientation(input->GetMesh3d(), input->GetParamMesh());
					orientationStatus = input->CheckOrientation();					
					if(!orientationStatus ) 					
						strcpy(orientation, "\n Mesh's normal orientation is pointed inwards (OK)");
					else if(orientationStatus == 1)		
						strcpy(orientation, "\n ** Warning: Mesh's orientation is pointed outwards! **");
				  	else // 2 - Surface is closed with 1 group. Can't compute
				  		strcpy(orientation, "\n ** Warning: Input Mesh is closed and has just one group. **\n ** Can't check normal orientation! **");
				}
				else
					strcpy(orientation, "");

				(!null > 0) 		? strcpy(nullMsg,     "") 
										: strcpy(nullMsg, 	 "\n ** Warning: Tested mesh seems to have holes! **");
		    	sprintf(this->ResponseFilterInfo, " Number of nodes: %ld\n Number of elements: %ld\n Number of null edges: %d%s%s\n Numb. of components-num. of holes : %ld\n", nod,nel,null,nullMsg,orientation,(2*nod-nel)/4);        
			break;
			case TRISURF_MESH_INFORMATION:
				char temp[2048];
				long nodes,nelems,groups;
				this->UpdateProgress(0.01);
		    	surf->Information(&nodes,&nelems,&groups);
				long numElementsGroup[512], numNodesGroup[512];
		    	surf->GroupInformation(numElementsGroup, numNodesGroup);
		    
		   	sprintf(this->ResponseFilterInfo, "Number of nodes   : %ld\n"
			                 										"Number of elements: %ld\n"
			                 										"Number of groups  : %ld\n"
			                 										,nodes,nelems,groups);
			  	for(i = 0; i < groups; i++)
			  	{
			  		sprintf(temp, "  Group %d, Elements: %d, Nodes: %d\n", i, numElementsGroup[i], numNodesGroup[i]);
			  		strcat(this->ResponseFilterInfo,temp);
			  	}
			break;
			case TRISURF_CLOSE_SURFACE:
		    	this->UpdateProgress(0.01);
				sprintf(this->ResponseFilterInfo, "Groups created: %d", surf->PoneTapas() );			
			break;
			
			case TRISURF_REMOVE_GROUPS:
				// Le numero de inteiros da string	  
				numGroups = CountNumberOfIntegers(this->grupos);
				// Aloca vetor de inteiros
				gr = new int[ numGroups ];
				// Le inteiros da string e armazena no vetor	
				readIntegers (this->grupos, gr);		  		  
				// Ordena grupos
				ordenaGruposDecrescente(gr, 0, numGroups);                

				// Testa se tem algum grupo inválido na lista passada
				result = 1;
				for (i=0; i<numGroups; i++)				
					if(gr[i] >= surf->GetSurfaceGen()->GetNumGroups() || gr[i] < 0)
						result = 0;

				// Um dos grupos é inválido. Não prossegue
				if(!result)
					{
					sprintf(this->ResponseFilterInfo, "Failed1");				
					break;
					}      

				// Testa o número de grupos da entrada e igual ao número de grupos existentes
				if(surf->GetSurfaceGen()->GetNumGroups() == numGroups)
					{
					sprintf(this->ResponseFilterInfo, "Failed2");				
					break;
					}      		
         					
				for (i=0, result = 0; i<numGroups; i++)
         		result+=surf->RemoveGroup(gr[i]);
				sprintf(this->ResponseFilterInfo, "%ld",result);
		    	if(numGroups > 1) delete []gr;
			break;

			case TRISURF_CHANGE_ORIENTATION:				
				// Le numero de inteiros da string	  
				numGroups = CountNumberOfIntegers(this->grupos);
				// Aloca vetor de inteiros
				gr = new int[ numGroups ];
				// Le inteiros da string e armazena no vetor	
				readIntegers (this->grupos, gr);		  		  
				bool swapAllGroups;
				swapAllGroups = false;	      	  

				// Testa se foi lido apenas um grupo e o valor é zero. Se positivo, inverte todos.  	  
				if(numGroups==1 && gr[0]==-1) swapAllGroups = true;
  
				// Testa se algum dos grupos escolhidos é -1. Se positivo, inverte todos.  	  
				for (i=0; i<numGroups; i++)
					if(gr[i] == -1)
						swapAllGroups = true;
				
				// Se o flag para inverter todos foi selecionado, processar todos os grupos.
				if(swapAllGroups)   	  
					{
					for (i=0; i < surf->GetSurfaceGen()->GetNumGroups(); i++) 
						surf->SwapGroup(i);
					sprintf(this->ResponseFilterInfo, "All Groups were Swapped");
					}
				else
					{
					strcpy(aux, "");
					sprintf(this->ResponseFilterInfo, "Groups swapped: ");
					for (i=0; i<numGroups; i++) 
						{
						if(gr[i] < surf->GetSurfaceGen()->GetNumGroups() && gr[i] >= 0)
	    	  				{
		    	  				surf->SwapGroup(gr[i]);     	  			
	  	  	  					sprintf(aux, " %d", gr[i]);
	    		  				strcat(this->ResponseFilterInfo, aux);
    	  					}
    	  				}
    	  			if( !strcmp(aux, "") )
    		  			strcat(this->ResponseFilterInfo, "None");    	  		
	    			}
				if(numGroups > 1) delete []gr;    	  		
			break;

			case TRISURF_VIEW_GROUP:
				if(this->SelectedGroup > surf->GetSurfaceGen()->GetNumGroups()-1 )
			  	strcpy(this->ResponseFilterInfo,"Invalid Group."); 
			  else  	  				  
			  	strcpy(this->ResponseFilterInfo,""); 			  
			break;
  
		  case TRISURF_CREATE_GROUP:
				result = surf->GetSurfaceGen()->CreateGroupFromSelection((int)par[0], &par[1]);
				if(!result)
					{
					sprintf(this->ResponseFilterInfo, "Elements selected: 0.\nCan't Create Group");				
					break;
					}

				// Armazena quantidade inicial de grupos, pois dependendo da seleção, 
				// um dos grupos poderá ser removido
				numGroups = surf->GetSurfaceGen()->GetNumGroups();
				
				// Vetor que irá armazenar os grupos a serem removidos
				gr = new int[numGroups];
				
				// Marca grupos que serão removidos
				for(i = 0; i < numGroups; i++)
						if( surf->GetSurfaceGen()->GetNumElsGroups(i) ) 
							gr[i] = -1;
						else
							{
							gr[i] = i;
							flag = true;
							}
				
				sprintf(this->ResponseFilterInfo, "");
				// Se algum grupo for marcado para remoção, remover
				if(flag)
					{
					sprintf(this->ResponseFilterInfo, "Removed Groups' Indexes: ");

					// Reordena em ordem decrescente os grupos a serem removidos
					ordenaGruposDecrescente(gr, 0, numGroups);

					// Remove grupos marcados
					for(i = 0; i < numGroups; i++)				
						if(gr[i] != -1)
							{
							sprintf(aux, " %d  ", gr[i]);
							strcat(this->ResponseFilterInfo, aux);
							surf->RemoveGroup(gr[i]);
							}
						strcat(this->ResponseFilterInfo, "\n");							
						}				
				// Acerta string que será exibida na interface
				sprintf(aux, "Group %d created with %d elements.\n", surf->GetSurfaceGen()->GetLastCreatedGroupIndex(), result);
				strcat(this->ResponseFilterInfo, aux);				

				// Evidencia o grupo criado (NumGroups - 1)
				if(result) this->SelectedGroup = surf->GetSurfaceGen()->GetLastCreatedGroupIndex();				
			break;
			  
			case TRISURF_MERGE_GROUPS:
				// Le numero de inteiros da string	  
				numGroups = CountNumberOfIntegers(this->grupos);
				// Aloca vetor de inteiros
				gr = new int[ numGroups ];
				// Le inteiros da string e armazena no vetor	
				readIntegers (this->grupos, gr);				
			
				// Se apenas um grupo for passado como parâmetro, não tem com o que fundir
				if(numGroups == 1)
					{
					strcpy(this->ResponseFilterInfo,"Aborted! At least two groups must be set.");   	  	
		    	  	error = 1;			  	
					}

				// Testa se os grupos são válidos  			
				for (i=0; i < numGroups; i++)
					{
					if(gr[i] < 0 || gr[i] > surf->GetSurfaceGen()->GetNumGroups()-1)    	  	
						{
						strcpy(this->ResponseFilterInfo,"Aborted! One of these groups is invalid.");   	  	
						error = 1;			  	
						break;
	  	  	  			}
					}
	    	  
				// Se em um dos testes acima der erro, sair do switch
				if(error) break;

				// If there was no error, erase Response Info Message
				strcpy(this->ResponseFilterInfo,""); 			  	  	   

				// Ordena em ordem crescente todos
				ordenaGruposCrescente(gr, 0, numGroups);

				// Faz o merge dos grupos com o primeiro (o menor)
				for (i=1; i < numGroups; i++)
					surf->MergeGroups(gr[0], gr[i]);

				// Reordena em ordem decrescente a partir do segundo grupo
				ordenaGruposDecrescente(gr, 1, numGroups);

				// Remove os que foram fundidos ao primeiro passado como parâmetro) 
				for (i=1; i<numGroups; i++)
					surf->RemoveGroup(gr[i]);
				
				// Evidencia o grupo resultante na interface
				this->SelectedGroup = gr[0];    	        	      	  
		    	if(numGroups > 1) delete []gr;
		  break;
			case TRISURF_VIEW_NEEDLES:
				if(par[0]>20)
					sprintf(this->ResponseFilterInfo, "Angle must be a value between 0 and 20");						
				else
					{
					char ids[4096] = "";			
					result = surf->ViewNeedles(par[0], ids);
			    	sprintf(this->ResponseFilterInfo, "Identified Needles: %d", result);						
			    	strcat(this->ResponseFilterInfo, ids);
					}
		  break;		  
			case TRISURF_REMOVE_NEEDLES:
				if(par[0]>20)
					sprintf(this->ResponseFilterInfo, "Angle must be a value between 0 and 20");						
				else
					{
					surf->RemoveNeedles(par[0], resultVet);								
			    	sprintf(this->ResponseFilterInfo, "Removed Needles: %d\nRemoved Neighbors:"  
			    				"%d\nRemoved Nodes: %d", resultVet[0], resultVet[1], resultVet[2]);			    				
			    	if(resultVet[3]!=0)
						{
						sprintf(aux, "\nRemoved Boundary Triangles: %d\n", resultVet[3]);
						strcat(this->ResponseFilterInfo, aux);
						}
					}
			break;		  
			case TRISURF_VIEW_SMALLTRI:
				result = surf->ViewSmallTri(par[0]);
				sprintf(this->ResponseFilterInfo, "Identified Elements: %d", result);						
			break;		  
			case TRISURF_REMOVE_SMALLTRI:
				result = surf->RemoveSmallTri(par[0]);
				sprintf(this->ResponseFilterInfo, "Removed Elements: %d", result);						
			break;
			case TRISURF_AVERAGE_SMALLTRI:
				value = surf->GetSurfaceGen()->GetAverageTriangleArea();
				sprintf(this->ResponseFilterInfo, "%.14lf", value);						
			break;
			}			
		}

	// Se o filtro não for view/merge/create, anula campo SelectedGroup
	if(this->currentFilter != TRISURF_VIEW_GROUP 		&& 
		this->currentFilter != TRISURF_MERGE_GROUPS 	&&
		this->currentFilter != TRISURF_CREATE_GROUP)
		this->SelectedGroup = -1; 		
		
	// Update internal mesh3d with update MeshData's SurfaceGen
	output->GetMesh3d()->Update(output);
	
	// Atualiza Grid interno
	output->SetOutputAsSurface();  
  
	// Atualiza cores apenas se filtro apropriado tiver sido selecionado
	output->UpdateGroupProperties(0);

	// Mostra triangulos modificados se modo ViewModifiedCells estiver ligado
	// ou um grupo específico se filtro "ViewGroup" for selecionado.
	if(this->ViewModifiedCells)	output->SetModifiedCells();
	else												output->SetSelectedGroup(this->SelectedGroup);

	output->GetCellData()->SetActiveScalars("SelectedGroup");      

	surf->Delete();	
	cbc->Delete();	
  
	cout << *output << endl;

	return 1;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::SetReleaseDataFlag(int i)
{
	if (i == 1) this->ReleaseDataFlagOn();	
	else				this->ReleaseDataFlagOff();
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::SetParameterFile(char *nome)
{
	this->Modified();	
	this->currentFilter = TRISURF_PAR;	
	strcpy(this->ParString, nome);
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyViewSmallTriFilter(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_VIEW_SMALLTRI;
	par[0] = angle;		
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyRemoveSmallTriFilter(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_REMOVE_SMALLTRI;
	par[0] = angle;		
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyAverageSmallTriFilter(double x)
{
	this->Modified();	
	this->currentFilter = TRISURF_AVERAGE_SMALLTRI;	
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyRemoveNeedlesFilter(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_REMOVE_NEEDLES;
	par[0] = angle;		
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyViewNeedlesFilter(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_VIEW_NEEDLES;
	par[0] = angle;		
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyMeshInformationFilter(int i)
{
	this->Modified();	
	this->currentFilter = TRISURF_MESH_INFORMATION;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyAutoParFilter(double edgesSize)
{
	this->Modified();
	this->currentFilter = TRISURF_AUTOPAR;
	par[0] = edgesSize;	
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ViewGroupFilter(int val)
{
	this->Modified();
	this->SelectedGroup = val;	
	this->currentFilter = TRISURF_VIEW_GROUP;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyTransformationFilter(double tx, double ty, double tz, 
                                                                 double sx, double sy, double sz,
                                                                 double rx, double ry, double rz)
{
	this->Modified();
	this->currentFilter = TRISURF_TRANSFORMATION;
	par[0] = tx;
	par[1] = ty;
	par[2] = tz;
	par[3] = sx;
	par[4] = sy;
	par[5] = sz;
	par[6] = rx;
	par[7] = ry;
	par[8] = rz;
}                                                                 

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyTestMeshFilter(int i)
{
	this->Modified();	
	this->currentFilter = TRISURF_TEST_MESH;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyRemoveFreeNodesFilter(int i)
{
	this->Modified();	
	this->currentFilter = TRISURF_REMOVE_FREE_NODES;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyRemoveTresTriFilter(int i)
{
	this->Modified();	
	this->currentFilter = TRISURF_REMOVE_TRES_TRI;	
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplySmoothFilter(double ite, double fac, double ns, double group, double needle)
{	
	this->Modified();	
	this->currentFilter = TRISURF_SMOOTH;	
	par[0] = ite;
	par[1] = fac;
	par[2] = ns;
	par[3] = group;
	par[4] = needle;	
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplySwapFilter(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_SWAP_DIAGONALS;	
	par[0] = angle;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyDivideObtuse(double angle)
{
	this->Modified();	
	this->currentFilter = TRISURF_DIVIDE_OBTUSE;	
	par[0] = angle;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyInsertNodes(double value)
{
	this->Modified();	
	this->currentFilter = TRISURF_INSERT_NODES;
	par[0] = value;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyCollapseFilter(double value)
{
	this->Modified();	
	this->currentFilter = TRISURF_COLLAPSE;	
	par[0] = value;
}

//-------------------------------------------------------------------------
void vtkHM3DTrisurfFilter::ApplyCloseSurfaceFilter(int i)
{
	this->Modified();	
	this->currentFilter = TRISURF_CLOSE_SURFACE;
}

//-------------------------------------------------------------------------                          
void vtkHM3DTrisurfFilter::ApplyRemoveGroupsFilter(char* gruposParameter)
{   
	strcpy(this->grupos, gruposParameter);
	this->Modified();	
	this->currentFilter = TRISURF_REMOVE_GROUPS;
}

//-------------------------------------------------------------------------                          
void vtkHM3DTrisurfFilter::ApplyChangeOrientationFilter(char* gruposParameter)
{   
	strcpy(this->grupos, gruposParameter);
	this->Modified();	
	this->currentFilter = TRISURF_CHANGE_ORIENTATION;
} 

//-------------------------------------------------------------------------                          
void vtkHM3DTrisurfFilter::ApplyMergeGroupsFilter(char* gruposParameter)
{   
	strcpy(this->grupos, gruposParameter);
	this->Modified();	
	this->currentFilter = TRISURF_MERGE_GROUPS;
} 

//-------------------------------------------------------------------------                          
void vtkHM3DTrisurfFilter::ApplyViewModifiedCells(int view)
{   
	this->ViewModifiedCells = view;		
	this->Modified();	
} 

//-------------------------------------------------------------------------   
void vtkHM3DTrisurfFilter::ApplyCreateGroupFilter(double type, double b0, double b1, double b2, 
																									double b3, double b4, double b5)
{
	this->par[0] = type;
	this->par[1] = b0;
	this->par[2] = b1;
	this->par[3] = b2;
	this->par[4] = b3;
	this->par[5] = b4;
	this->par[6] = b5;				

	this->currentFilter = TRISURF_CREATE_GROUP;	
	this->Modified();		
}
