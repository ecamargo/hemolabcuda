/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHM3DTrisurfFilter

  Author: Rodrigo L. S. Silva, Jan Palach

=========================================================================*/
// .NAME vtkHM3DTrisurfFilter
// .SECTION Description
// Apply trisurf methods in a vtkMeshData object.

#ifndef __vtkHM3DTrisurfFilter_h
#define __vtkHM3DTrisurfFilter_h

#define TRISURF_SMOOTH					1
#define TRISURF_SWAP_DIAGONALS		2
#define TRISURF_DIVIDE_OBTUSE			3
#define TRISURF_INSERT_NODES			4	 
#define TRISURF_COLLAPSE				5 
#define TRISURF_PAR						6
#define TRISURF_REMOVE_TRES_TRI		7
#define TRISURF_REMOVE_FREE_NODES	8
#define TRISURF_TRANSFORMATION		9
#define TRISURF_TEST_MESH				10
#define TRISURF_MESH_INFORMATION		11
#define TRISURF_CLOSE_SURFACE			12
#define TRISURF_REMOVE_GROUPS			13
#define TRISURF_AUTOPAR					14
#define TRISURF_CHANGE_ORIENTATION	15
#define TRISURF_VIEW_GROUP				16
#define TRISURF_MERGE_GROUPS			17
#define TRISURF_CREATE_GROUP			18
#define TRISURF_VIEW_NEEDLES			19
#define TRISURF_REMOVE_NEEDLES		20
#define TRISURF_VIEW_SMALLTRI			21	  
#define TRISURF_REMOVE_SMALLTRI		22
#define TRISURF_AVERAGE_SMALLTRI		23
													
#include "vtkDataSetAlgorithm.h"
#include <string>
#include <iostream>

class vtkFloatArray;
class vtkIdList;
class vtkParamMesh;

class VTK_EXPORT vtkHM3DTrisurfFilter : public vtkDataSetAlgorithm
{
public:
  vtkTypeRevisionMacro(vtkHM3DTrisurfFilter,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  	// Description:
  	// Constructor
	static vtkHM3DTrisurfFilter *New();

  	// Description:
  	// Set Filter parameters from XML calls
	void ApplySmoothFilter(double, double, double, double, double);
	void ApplySwapFilter(double	);
	void ApplyDivideObtuse(double	);
	void SetParameterFile(char *);	
	void ApplyInsertNodes(double);	
	void ApplyCollapseFilter(double);
	void ApplyRemoveTresTriFilter(int i);
	void ApplyRemoveFreeNodesFilter(int i);
	void ApplyTransformationFilter(double, double, double, double, double, double, double, double, double);
	void ApplyTestMeshFilter(int i);
	void ApplyMeshInformationFilter(int i);
	void ApplyCloseSurfaceFilter(int i);
	void ApplyRemoveGroupsFilter(char*);
	void ApplyAutoParFilter(double);
	void ApplyChangeOrientationFilter(char*);
	void ApplyMergeGroupsFilter (char *);
	void ViewGroupFilter(int);  
	void ApplyViewModifiedCells(int);
	void ApplyCreateGroupFilter(double, double, double, double, double, double, double);
	void ApplyViewNeedlesFilter(double);
	void ApplyRemoveNeedlesFilter(double);
	void ApplyViewSmallTriFilter(double);
	void ApplyRemoveSmallTriFilter(double);
	void ApplyAverageSmallTriFilter(double);

	// Used to update GUI
	char* GetResponseOfFilter();
	
	// Release Data Flag	
	void SetReleaseDataFlag(int i);
	
	// Get Loaded Parameter
	int GetParameterLoaded();
    
protected:
  vtkHM3DTrisurfFilter();
  ~vtkHM3DTrisurfFilter();
  
  //FillInputInformation.
  int FillInputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
    
  // Set Number of Input Connections
  void SetNumberOfInputConnections(int port, int n);

protected:    
  // Description:
  // Parameters used in all filters 
  double par[10];
  char ParString[256];
  char grupos[250];
  char ResponseFilterInfo[2048];
  int ParameterLoaded;

  // Description:
  // Current filter to be used  
  int currentFilter;
	
  // Description:
	// If a parameter file was already loaded, get the reference	
  vtkParamMesh *currentParameter;

  // Description:
	// Used by the "View Group" Filter
	int SelectedGroup;

  // Description:
	// 0 - Modified Cells will not be highlighted
	// 1 - Modified Cells will be highlighted
	int ViewModifiedCells;	

private:
  vtkHM3DTrisurfFilter(const vtkHM3DTrisurfFilter&);  // Not implemented.
  void operator=(const vtkHM3DTrisurfFilter&);  // Not implemented.
};

#endif


