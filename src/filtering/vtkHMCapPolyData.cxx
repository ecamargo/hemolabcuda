/*
 * vtkHMCapPolyData.cxx
 *
 *  Created on: Sep 29, 2009
 *      Author: igor
 */


#include "vtkHMCapPolyData.h"
//#include "vtkvmtkPolyDataBoundaryExtractor.h"
//#include "vtkvmtkBoundaryReferenceSystems.h"
//#include "vtkvmtkConstants.h"
#include "vtkCellArray.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "vtkPolyLine.h"
#include "vtkLine.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkHMCapPolyData, "$Revision: 1.5 $");
vtkStandardNewMacro(vtkHMCapPolyData);

vtkHMCapPolyData::vtkHMCapPolyData()
{
  this->BoundaryIds = NULL;
  this->Displacement = 0;
  this->InPlaneDisplacement = 0;
  this->CapCenterIds = NULL;
}

vtkHMCapPolyData::~vtkHMCapPolyData()
{
  if (this->BoundaryIds)
    {
    this->BoundaryIds->Delete();
    this->BoundaryIds = NULL;
    }
  if (this->CapCenterIds)
    {
    this->CapCenterIds->Delete();
    this->CapCenterIds = NULL;
    }
}

//------------------------------------------------------------------------------------
int vtkHMCapPolyData::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // Declare
  vtkIdType barycenterId, trianglePoints[3];
  vtkIdType i, j;
//  vtkvmtkPolyDataBoundaryExtractor* boundaryExtractor;
  vtkPolyData* boundaries;
  vtkPoints* newPoints;
  vtkCellArray* newPolys;
  vtkPolyLine* boundary;

  // Initialize
  if ( ((input->GetNumberOfPoints()) < 1) )
    {
    //vtkErrorMacro(<< "No input!");
    return 1;
    }
  input->BuildLinks();

  // Allocate
  newPoints = vtkPoints::New();
  newPoints->DeepCopy(input->GetPoints());
  newPolys = vtkCellArray::New();
  newPolys->DeepCopy(input->GetPolys());
//  boundaryExtractor = vtkvmtkPolyDataBoundaryExtractor::New();

  // Execute
//  boundaryExtractor->SetInput(input);
//  boundaryExtractor->Update();

  boundaries = this->ExtractBoundary(input);

  if (this->CapCenterIds)
    {
    this->CapCenterIds->Delete();
    this->CapCenterIds = NULL;
    }

  this->CapCenterIds = vtkIdList::New();
  this->CapCenterIds->SetNumberOfIds(boundaries->GetNumberOfCells());
  for (i=0; i<this->CapCenterIds->GetNumberOfIds(); i++)
    {
    this->CapCenterIds->SetId(i,-1);
    }

  double barycenter[3], normal[3], outwardNormal[3], meanRadius;

  for (i=0; i<boundaries->GetNumberOfCells(); i++)
    {
    if (this->BoundaryIds)
      {
      if (this->BoundaryIds->IsId(i) == -1)
        {
        continue;
        }
      }
    boundary = vtkPolyLine::SafeDownCast(boundaries->GetCell(i));

    this->ComputeBoundaryBarycenter(boundary->GetPoints(),barycenter);
    this->ComputeBoundaryNormal(boundary->GetPoints(),barycenter,normal);
    this->OrientBoundaryNormalOutwards(input,boundaries,i,normal,outwardNormal);
    meanRadius = this->ComputeBoundaryMeanRadius(boundary->GetPoints(),barycenter);

    for (j=0; j<3; j++)
      {
      barycenter[j] += meanRadius * this->Displacement * outwardNormal[j];
      }

    double inplane1[3], inplane2[3];
    vtkMath::Perpendiculars(outwardNormal,inplane1,inplane2,0.0);
    for (j=0; j<3; j++)
      {
      barycenter[j] += meanRadius * this->InPlaneDisplacement * inplane1[j];
      }

    barycenterId = newPoints->InsertNextPoint(barycenter);
    this->CapCenterIds->SetId(i,barycenterId);

    vtkIdType numberOfBoundaryPoints = boundary->GetNumberOfPoints();
    for (j=0; j<numberOfBoundaryPoints; j++)
      {
      trianglePoints[0] = static_cast<vtkIdType>(boundaries->GetPointData()->GetScalars()->GetTuple1(boundary->GetPointId(j)));
      trianglePoints[1] = barycenterId;
      trianglePoints[2] = static_cast<vtkIdType>(boundaries->GetPointData()->GetScalars()->GetTuple1(boundary->GetPointId((j+1)%numberOfBoundaryPoints)));
      newPolys->InsertNextCell(3,trianglePoints);
      }
    }

  output->SetPoints(newPoints);
  output->SetPolys(newPolys);

  // TODO: the filter throws all the point and cell data

  // Destroy
  newPoints->Delete();
  newPolys->Delete();
  boundaries->Delete();
//  boundaryExtractor->Delete();

  return 1;
}

//------------------------------------------------------------------------------------
void vtkHMCapPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//------------------------------------------------------------------------------------
vtkPolyData *vtkHMCapPolyData::ExtractBoundary(vtkPolyData *input)
{
  vtkPolyData *output = vtkPolyData::New();

  vtkIdList *boundary, *boundaryIds, *cellEdgeNeighbors, *newCell;
  vtkIdType i, j, currentId, firstId, id, newId, id0, id1;
  newId = -1;
  vtkCell* cell;
  bool foundAny, foundNeighbor, done;
  vtkPoints* newPoints;
  vtkCellArray* newLines;
  vtkIdTypeArray* newScalars;

  input->BuildCells();
  input->BuildLinks();

  // Allocate
  boundary = vtkIdList::New();
  boundaryIds = vtkIdList::New();
  cellEdgeNeighbors = vtkIdList::New();
  newCell = vtkIdList::New();
  newPoints = vtkPoints::New();
  newLines = vtkCellArray::New();
  newScalars = vtkIdTypeArray::New();

  // Execute
  for (i=0; i<input->GetNumberOfCells(); i++)
    {
    cell = input->GetCell(i);
    for (j=0; j<3; j++)
      {
      cellEdgeNeighbors->Initialize();
      id0 = cell->GetEdge(j)->GetPointIds()->GetId(0);
      id1 = cell->GetEdge(j)->GetPointIds()->GetId(1);
      input->GetCellEdgeNeighbors(i,id0,id1,cellEdgeNeighbors);
      if (cellEdgeNeighbors->GetNumberOfIds()==0)
        {
        boundaryIds->InsertUniqueId(id0);
        boundaryIds->InsertUniqueId(id1);
        }
      }
    }

  if (boundaryIds->GetNumberOfIds() == 0)
    {
    output->SetPoints(newPoints);
    output->SetLines(newLines);
    output->GetPointData()->SetScalars(newScalars);

    newPoints->Delete();
    newLines->Delete();
    newScalars->Delete();

    boundary->Delete();
    boundaryIds->Delete();
    cellEdgeNeighbors->Delete();
    newCell->Delete();

    return output;
    }

  foundAny = false;
  foundNeighbor = false;
  done = false;
  currentId = -1;
  int loopCount = 0;
  bool isBoundaryEdge;

  while (!done)
    {
    foundAny = false;
    foundNeighbor = false;

    for (i=0; i<boundaryIds->GetNumberOfIds(); i++)
      {
      id = boundaryIds->GetId(i);
      if (id!=-1)
        {
        foundAny = true;
        isBoundaryEdge = false;
        if (currentId!=-1 && input->IsEdge(currentId,id))
          {
          cellEdgeNeighbors->Initialize();
          input->GetCellEdgeNeighbors(-1,currentId,id,cellEdgeNeighbors);
          if (cellEdgeNeighbors->GetNumberOfIds() == 1)
            {
            isBoundaryEdge = true;
            }
          }
        if ((currentId==-1) || isBoundaryEdge)
          {
          foundNeighbor = true;
          }

        if (foundNeighbor)
          {
          currentId = id;
          boundary->InsertNextId(currentId);
          boundaryIds->SetId(i,-1);
          break;
          }
        }
      }

    if ( (((!foundNeighbor)&&(foundAny))||(!foundAny)) && (boundary->GetNumberOfIds() > 2))
      {
      newCell->Initialize();
      newCell->SetNumberOfIds(boundary->GetNumberOfIds());
      firstId = newPoints->GetNumberOfPoints();
      for (j=0; j<boundary->GetNumberOfIds(); j++)
        {
        id = boundary->GetId(j);
        newId = newPoints->InsertNextPoint(input->GetPoint(id));
        newCell->SetId(j,newId);
        newScalars->InsertNextValue(id);
        }

      if (input->IsEdge(newId,firstId))
        {
        newCell->InsertNextId(firstId);
        }

      newLines->InsertNextCell(newCell);

      currentId = -1;
      boundary->Initialize();
      }

    if (!foundAny)
      {
      done = true;
      }

    loopCount++;
    if (loopCount > 2*boundaryIds->GetNumberOfIds())
      {
      int missing = 0;
      for (int n=0; n<boundaryIds->GetNumberOfIds(); n++)
        {
        if (boundaryIds->GetId(n) != -1)
          {
          missing++;
          }
        }
      vtkErrorMacro(<<"Can't find adjacent point. Bailing out.");
      done = true;
      }
    }

  output->SetPoints(newPoints);
  output->SetLines(newLines);
  output->GetPointData()->SetScalars(newScalars);

  // Destroy
  newPoints->Delete();
  newLines->Delete();
  newScalars->Delete();

  boundary->Delete();
  boundaryIds->Delete();
  cellEdgeNeighbors->Delete();
  newCell->Delete();

  return output;
}

//------------------------------------------------------------------------------------
void vtkHMCapPolyData::ComputeBoundaryBarycenter(vtkPoints* points, double* barycenter)
{
  int numberOfPoints;
  double weightSum, weight;
  vtkIdType i;

  // accounts for edge length

  numberOfPoints = points->GetNumberOfPoints();

  weightSum = 0.0;

  barycenter[0] = 0.0;
  barycenter[1] = 0.0;
  barycenter[2] = 0.0;

  double point0[3], point1[3];

  for (i=0; i<numberOfPoints; i++)
    {
    points->GetPoint(i,point0);
    points->GetPoint((i+1)%numberOfPoints,point1);
    weight = sqrt(vtkMath::Distance2BetweenPoints(point0,point1));
    if (weight < 1.0E-12)
      {
      continue;
      }
    weightSum += weight;

    barycenter[0] += (point0[0] + point1[0])/2.0 * weight;
    barycenter[1] += (point0[1] + point1[1])/2.0 * weight;
    barycenter[2] += (point0[2] + point1[2])/2.0 * weight;
    }

  barycenter[0] /= weightSum;
  barycenter[1] /= weightSum;
  barycenter[2] /= weightSum;
}

//------------------------------------------------------------------------------------
void vtkHMCapPolyData::ComputeBoundaryNormal(vtkPoints* points, double* barycenter, double* normal)
{
  int numberOfPoints;
  double point[3], vector1[3], vector0[3], cross[3], sumCross[3] = {0.0f,0.0f,0.0f};
  vtkIdType i, j;

  // TODO: weight by edge length as for barycenter

  numberOfPoints = points->GetNumberOfPoints();

  points->GetPoint(numberOfPoints-1,point);
  vector0[0] = point[0] - barycenter[0];
  vector0[1] = point[1] - barycenter[1];
  vector0[2] = point[2] - barycenter[2];

  for (i=0; i<numberOfPoints; i++)
    {
    points->GetPoint(i,point);
    vector1[0] = point[0] - barycenter[0];
    vector1[1] = point[1] - barycenter[1];
    vector1[2] = point[2] - barycenter[2];

    vtkMath::Cross(vector1,vector0,cross);

    for (j=0; j<3; j++)
      {
      vector0[j] = vector1[j];
      sumCross[j] += cross[j];
      }
    }

  vtkMath::Normalize(sumCross);

  for (j=0; j<3; j++)
    {
    normal[j] = sumCross[j];
    }
}

//------------------------------------------------------------------------------------
void vtkHMCapPolyData::OrientBoundaryNormalOutwards(vtkPolyData* surface, vtkPolyData* boundaries,
                            vtkIdType boundaryCellId, double normal[3], double outwardNormal[3])
{
  vtkPolyLine* boundary = vtkPolyLine::SafeDownCast(boundaries->GetCell(boundaryCellId));

  int k=0;
  if (!boundary)
    {
    for (k=0; k<3; k++)
      {
      outwardNormal[k] = normal[k];
      }
    return;
    }

  surface->BuildCells();
  surface->BuildLinks();

  unsigned short ncells;
  vtkIdType *cells;
  vtkIdType npts, *pts;

  double boundaryPoint[3], neighborPoint[3];

  int numberOfBoundaryPoints = boundary->GetNumberOfPoints();

  int j;
  vtkIdList* boundaryIds = vtkIdList::New();
  for (j=0; j<numberOfBoundaryPoints; j++)
    {
    boundaryIds->InsertNextId(static_cast<vtkIdType>(vtkMath::Round(boundaries->GetPointData()->GetScalars()->GetComponent(boundary->GetPointId(j),0))));
    }

  double neighborsToBoundaryNormal[3];
  neighborsToBoundaryNormal[0] = neighborsToBoundaryNormal[1] = neighborsToBoundaryNormal[2] = 0.0;
  for (j=0; j<numberOfBoundaryPoints; j++)
    {
    surface->GetPointCells(boundaryIds->GetId(j),ncells,cells);
    for (int c=0; c<ncells; c++)
      {
      surface->GetCellPoints(cells[c],npts,pts);
      for (int p=0; p<npts; p++)
        {
        if (boundaryIds->IsId(pts[p]) == -1)
          {
          surface->GetPoint(boundaryIds->GetId(j),boundaryPoint);
          surface->GetPoint(pts[p],neighborPoint);
          for (k=0; k<3; k++)
            {
            neighborsToBoundaryNormal[k] += boundaryPoint[k] - neighborPoint[k];
            }
          }
        }
      }
    }

  for (k=0; k<3; k++)
    {
    outwardNormal[k] = normal[k];
    }

  vtkMath::Normalize(neighborsToBoundaryNormal);

  if (vtkMath::Dot(normal,neighborsToBoundaryNormal) < 0.0)
    {
    for (k=0; k<3; k++)
      {
      outwardNormal[k] *= -1.0;
      }
    }

  boundaryIds->Delete();
}

//------------------------------------------------------------------------------------
double vtkHMCapPolyData::ComputeBoundaryMeanRadius(vtkPoints* points, double* barycenter)
{
  int numberOfPoints = points->GetNumberOfPoints();

  // TODO: weight by edge length as for barycenter

  double meanRadius = 0.0;

  for (int i=0; i<numberOfPoints; i++)
    {
    meanRadius += sqrt(vtkMath::Distance2BetweenPoints(points->GetPoint(i),barycenter));
    }

  meanRadius /= numberOfPoints;

  return meanRadius;
}
