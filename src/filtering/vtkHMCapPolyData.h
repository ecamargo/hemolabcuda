/*
 * vtkHMCapPolyData.h
 *
 *  Created on: Sep 29, 2009
 *      Author: igor
 */


// .NAME vtkHMCapPolyData - Add caps to boundaries.
// .SECTION Description
// This class closes the boundaries of a surface with a cap.
// Each cap is made of triangles sharing the boundary baricenter.
// Boundary baricenters are added to the dataset. It is possible to
// retrieve the ids of the added points with GetCapCenterIds.
// Boundary baricenters can be displaced along boundary normals
// through the Displacement parameter. Since this class is used as a
// preprocessing step for Delaunay tessellation, displacement is
// meant to avoid the occurence of degenerate tetrahedra on the caps.

#ifndef VTKHMCAPPOLYDATA_H_
#define VTKHMCAPPOLYDATA_H_

#include "vtkPolyDataAlgorithm.h"
#include "vtkPolyData.h"
#include "vtkPoints.h"
#include "vtkIdList.h"

class VTK_EXPORT vtkHMCapPolyData : public vtkPolyDataAlgorithm
{
  public:
  vtkTypeRevisionMacro(vtkHMCapPolyData,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMCapPolyData *New();

  // Description:
  // Set/Get the ids of the boundaries to cap.
  vtkSetObjectMacro(BoundaryIds,vtkIdList);
  vtkGetObjectMacro(BoundaryIds,vtkIdList);

  // Description:
  // Set/Get the displacement of boundary baricenters along boundary normals relative to the radius.
  vtkSetMacro(Displacement,double);
  vtkGetMacro(Displacement,double);

  // Description:
  // Set/Get the displacement of boundary baricenters on the section plane relative to the radius.
  vtkSetMacro(InPlaneDisplacement,double);
  vtkGetMacro(InPlaneDisplacement,double);

  // Description:
  // Get the ids of the newly inserted boundary baricenters.
  vtkGetObjectMacro(CapCenterIds,vtkIdList);

  protected:
  vtkHMCapPolyData();
  ~vtkHMCapPolyData();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  vtkPolyData *ExtractBoundary(vtkPolyData *input);

  void ComputeBoundaryBarycenter(vtkPoints* points, double* barycenter);
  void ComputeBoundaryNormal(vtkPoints* points, double* barycenter, double* normal);
  void OrientBoundaryNormalOutwards(vtkPolyData* surface, vtkPolyData* boundaries,
                                    vtkIdType boundaryCellId, double normal[3],
                                    double outwardNormal[3]);
  double ComputeBoundaryMeanRadius(vtkPoints* points, double* barycenter);

  vtkIdList* BoundaryIds;
  double Displacement;
  double InPlaneDisplacement;
  vtkIdList* CapCenterIds;

  private:
  vtkHMCapPolyData(const vtkHMCapPolyData&);  // Not implemented.
  void operator=(const vtkHMCapPolyData&);  // Not implemented.
};

#endif /* VTKHMCAPPOLYDATA_H_ */
