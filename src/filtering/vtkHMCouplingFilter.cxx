/*
 * $Id$
 */

#include "vtkHMCouplingFilter.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"
#include "vtkCallbackCommand.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkCellArray.h"
#include "vtkCommand.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkDataSet.h"
#include "vtkMath.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"
#include "vtkTriangleStrip.h"
#include "vtkPriorityQueue.h"
#include "vtkProcessModule.h"
#include "vtkClientServerID.h"
#include "vtkClientServerStream.h" 
#include "vtkExecutive.h"
#include "vtkCompositeDataSet.h"
#include "vtkMultiGroupDataSet.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkHM3DFullModel.h"
#include "vtkHMCoupledModelWriterPreProcessor.h"
#include "vtkHM1DStraightModel.h"
#include "vtkIntArray.h"
#include "vtkCollection.h"
#include "vtkHMUtils.h"
#include "vtkIdList.h"
#include "vtkPVConnectivityFilter.h"

#include <string>
#include <iostream>

vtkCxxRevisionMacro(vtkHMCouplingFilter, "$Revision: 1.67 $");
vtkStandardNewMacro(vtkHMCouplingFilter);

//-------------------------------------------------------------------------
vtkHMCouplingFilter::vtkHMCouplingFilter()
{
	//this->DebugOn();
	
	this->InputEnabled = -1;
	this->portIndex = 0;
	this->CoverGroups = -1;
	this->WallGroups = -1;
	//this->SetNumberOfOutputPorts(1);
	this->SetNumberOfInputPorts(10);	
	this->SelectedGroup = 1; 
 	this->CouplingArrayInfo = NULL;
	this->NumberOfMeshData = 0; 	
 	
  // marcadas com -1 para indicar que as mesmas nao foram atualizadas
//  this->StraightModelInputNumber = -1;
// 	this->MeshDataInputNumber = -1;
 	
 	this->GenerationStatus = 0 ;
 	
 	this->StraightModelInputNumber = 0;
 	
 	this->StraightModelGrid = NULL;
 	this->numInputs = 0;
 	
 	
 	this->FullModelsCollection = vtkCollection::New();
	this->FullModelsCollection->RemoveAllItems();
}

//-------------------------------------------------------------------------
vtkHMCouplingFilter::~vtkHMCouplingFilter()
{
	if (this->CouplingArrayInfo)
		{
		this->CouplingArrayInfo->Delete();
		this->CouplingArrayInfo = NULL;
		}

	this->FullModelsCollection->Delete();

}

//----------------------------------------------------------------------------
void vtkHMCouplingFilter::SetInputEnabled(int value)
{
  this->InputEnabled=value;
  this->Modified();
  this->Update();
}

//-------------------------------------------------------------------------
void vtkHMCouplingFilter::SetNumberOfInputConnections(int port, int n)
{
}

//-------------------------------------------------------------------------
int vtkHMCouplingFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{	
  vtkDebugMacro(<<"*********** vtkHMCouplingFilter::RequestData ***********");

  vtkInformation *inInfo;
  
  this->NumberOf3DValidInputs = 0;
  
  vtkInformation* info = outputVector->GetInformationObject(0);
  vtkMultiGroupDataSet *output = vtkMultiGroupDataSet::SafeDownCast(
    info->Get(vtkCompositeDataSet::COMPOSITE_DATA_SET()));
  if (!output) return 0;

  unsigned int updatePiece = static_cast<unsigned int>(
    info->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER()));
//  unsigned int updateNumPieces =  static_cast<unsigned int>(
//    info->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES()));

  int numInputs = inputVector[0]->GetNumberOfInformationObjects();
  
  // se nao existem entradas aborta a passagem pelo RequestData
  if (!numInputs)
  	return 0;
  
  // se o numero de entradas é apenas 1; aborta a passagem pelo RequestData
  if (numInputs == 1)
  	{
  	vtkErrorMacro("Wrong Number of Inputs to vtkHMCouplingFilter.");
  	return 0;
  	}
  
  int NumberOf1DModels = 0; // esta variavel é usada para contar o numero de modelos 1D - se tiver mais de um, há erro nos elementos de entrada do filtro

	// counting the number of 1D models - if more than one is found there is some error in the selected elements from input list
  if (!this->StraightModelGrid)
  	{
	  for (int i = 0; i < numInputs; ++i) 
	  	{
		  inInfo = inputVector[0]->GetInformationObject(i);	  
		  if (vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())))
		  	{
		  	if (NumberOf1DModels >= 1)
		  		{
		  		vtkErrorMacro("Wrong Number of 1D Inputs to vtkHMCouplingFilter - I found more than one 1D model!");	
		  		return 0;	
		  		}
		  	NumberOf1DModels++;
		  	}
	  	}
  	}

  // Once the number of 1D models is ok; i have to serach for this one
  // this search is done just once
  if (!this->StraightModelGrid)
  	{
	  for (int i = 0; i < numInputs; ++i) 
	  	{
		  inInfo = inputVector[0]->GetInformationObject(i);	  
		  
		  if (vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())))
		  	{
		  	this->StraightModelGrid = vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
		  	break;
		  	}
	  	}
  	}
 
  
	if (!this->StraightModelGrid)
  	{
	  vtkErrorMacro("1D model not found in the vtkHMCouplingFilter input list.");
	  return 0;
  	}
  
  // Searching for FullModels
  // this search is done just once
  if (!this->FullModelsCollection->GetNumberOfItems())
  	{
	  for (int i = 0; i < numInputs; ++i) 
	  	{
		  inInfo = inputVector[0]->GetInformationObject(i);
		  
//		  if (!vtkHM3DFullModel::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())) &&  !vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()))  )
//		  	{
//		  	vtkErrorMacro("This Coupling Filter works only with 3D FullModels not with vtkMeshDatas! - Revise your Selected itens");
//		  	return 0;
//		  	} 
		  
		  if (vtkHM3DFullModel::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())) )
		  	{ 
		  	//this->FullModelsCollection->AddItem(vtkHM3DFullModel::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT())));
		  	this->FullModelsCollection->AddItem(inInfo->Get(vtkDataObject::DATA_OBJECT() ));
		  	this->NumberOf3DValidInputs++;
		  	}
	  	}
  	}

  output->SetNumberOfGroups(numInputs);
  
  if(this->InputEnabled == 0)
  	{
  	//seta na saida do filtro o modelo 1D
  	output->SetDataSet(0, updatePiece, this->StraightModelGrid);
  	}
  else
  	{
  	//seta na saida do filtro algum dos modelos 3D
   	if (this->InputEnabled > 0)
   		{
	   	this->CoverGroups = this->GetFullModel(InputEnabled-1)->GetNumberOfCoverGroups();
	   	this->WallGroups = this->GetFullModel(InputEnabled-1)->GetNumberOfShells();
	   	output->SetDataSet(0, updatePiece, this->GetFullModel(InputEnabled-1));
	
			this->GetFullModel(InputEnabled-1)->SetOutputAsSurface();	
			this->GetFullModel(InputEnabled-1)->UpdateGroupProperties(0);		
	
			// Colore a superfície se o modo "SelectedGroup" foi selecionado.
			this->GetFullModel(InputEnabled-1)->SetSelectedGroup(this->SelectedGroup);
			this->GetFullModel(InputEnabled-1)->GetCellData()->SetActiveScalars("SelectedGroup");  
   		}
   	}
  
	if (!this->numInputs)
  	this->numInputs = numInputs;
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkHMCouplingFilter::Set1DInputNumber(int val)
{
	this->StraightModelInputNumber = val;
}

//----------------------------------------------------------------------------
void vtkHMCouplingFilter::Set3DInputNumber(int val)
{
	this->MeshDataInputNumber = val;	
}

//----------------------------------------------------------------------------	
void vtkHMCouplingFilter::AddInput(vtkDataObject* input)
{
	this->AddInput(0, input);
}

//----------------------------------------------------------------------------
void vtkHMCouplingFilter::AddInput(int index, vtkDataObject* input)
{
	if(input) this->AddInputConnection(index, input->GetProducerPort());
}

//----------------------------------------------------------------------------
int vtkHMCouplingFilter::FillInputPortInformation(int, vtkInformation *info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	info->Set(vtkAlgorithm::INPUT_IS_REPEATABLE(), 1);
	info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
 	return 1;
}

//----------------------------------------------------------------------------
void	vtkHMCouplingFilter::SelectGroup(int gr)
{
	this->SelectedGroup = gr;
	//this->Update();
	this->Modified();		
}

//-------------------------------------------------------------------------
void vtkHMCouplingFilter::SetReleaseDataFlag(int i)
{
	(i == 1) ? this->ReleaseDataFlagOn() : this->ReleaseDataFlagOff();
}

//-------------------------------------------------------------------------
void vtkHMCouplingFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}



//----------------------------------------------------------------------------
void vtkHMCouplingFilter::SetIntParam(int IntParam[37])
{
	for (int i=0; i < 37; i++)
		this->ConfigIntegerParam[i]	=	IntParam[i];
}
		

//----------------------------------------------------------------------------

void vtkHMCouplingFilter::SetModelConfigParam(double ModelConfigParam[8])
{
	for (int i=0; i < 8; i++)
		this->ModelConfigParam[i]	=	ModelConfigParam[i];
}
		
//----------------------------------------------------------------------------

void vtkHMCouplingFilter::SetTimeParam(double TimeParam[3])
{
	for (int i=0; i < 3; i++)
		this->TimeParam[i]	=	TimeParam[i];
}

//----------------------------------------------------------------------------


void vtkHMCouplingFilter::SetSegregatedSubRelaxation(double Rela)
{
	this->SegregatedSubRelaxation	=	Rela;
}

//----------------------------------------------------------------------------


void vtkHMCouplingFilter::SetMonolithicSubRelaxation(double Rela)
{
	this->MonolithicSubRelaxation	=	Rela;
}


//----------------------------------------------------------------------------

void vtkHMCouplingFilter::Set3DConvergenceParam(double ConvParam3D[15])
{
	for (int i=0; i < 15; i++)
		this->ConvParam3D[i]	=	ConvParam3D[i];
}

//----------------------------------------------------------------------------


void vtkHMCouplingFilter::Set1DConvergenceParam(double ConvParam1D[9])
{
	for (int i=0; i < 9; i++)
		this->ConvParam1D[i]	=	ConvParam1D[i];
}

//----------------------------------------------------------------------------



void vtkHMCouplingFilter::SetSolverConfigParam(double SolverParam[13])
{
	for (int i=0; i < 13; i++)
		this->SolverParam[i]	=	SolverParam[i];
}



//----------------------------------------------------------------------------

void vtkHMCouplingFilter::SetLogFileName(const char *logfilename)
{
	strcpy(this->LogFileName, logfilename);
}

//----------------------------------------------------------------------------

void vtkHMCouplingFilter::SetCouplingInformation(const char *CouplingInfo)
{
	// the string CouplingInfo must be processed in order to get the information related to the coupling points
	// and this must be stored in a vtkIntArray
	
	if (!strlen(CouplingInfo))
		vtkErrorMacro("Can not find coupling Points Information!");

	if (!this->CouplingArrayInfo)
		this->CouplingArrayInfo = vtkIntArray::New();

	this->CouplingArrayInfo->SetNumberOfComponents(4);
	this->CouplingArrayInfo->SetNumberOfTuples(this->GetNumberOfTotalIOFaces()); //FullModel->GetNumberOfCoverGroups());

	int LastPosition =0, CurrentPosition=0, ElemNumber=0, z=0, array_position=0;
	char temp[50];
  
	// resetar array **********
	for (int i=0; i < 50 ;i++)
		temp[i]=0;
	//*************************	

	double n[4];


	while (CouplingInfo[CurrentPosition])
		{
	  if (CouplingInfo[CurrentPosition] == ';')
	  	{
	    memcpy(temp, CouplingInfo+LastPosition, CurrentPosition-LastPosition);
	    n[z] = atoi(temp);
	    z++;
	    if (z==4)
	    	{
	   	  this->CouplingArrayInfo->SetTuple4(array_position, n[0], n[1], n[2], n[3]);
	   	  //cout << "Inserindo tuple 4---> seg_id:" << n[0] << " nn:"<< n[1] << " 3d group: " << n[2]<< " 3d#:" << n[3]<< " " << endl;
	   	  array_position++;
	   	  if (array_position > this->GetNumberOfTotalIOFaces())
	   	  	{
	   	  	vtkErrorMacro("Wrong Number of Coupling Points! Aborting....");
	   	  	}
	   	  z=0;	
	   	  }
	   	  LastPosition = CurrentPosition+1;
	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 			//*************************	
	    }
	  CurrentPosition++;
 	  }
}

//----------------------------------------------------------------------------


void vtkHMCouplingFilter::GenerateSolverFiles(char *path)
{
	vtkHMCoupledModelWriterPreProcessor *CoupledModelPreProcessor = vtkHMCoupledModelWriterPreProcessor::New();

	CoupledModelPreProcessor->SetWhichFile(this->WhichFile);

	CoupledModelPreProcessor->SetFullModelNumber(this->numInputs-1);
	
	
	for (int i = 0; i < this->numInputs-1; ++i)
		{
		CoupledModelPreProcessor->Insert3DFullModel(this->GetFullModel(i));
		}
	
	CoupledModelPreProcessor->SetStraightModel(this->StraightModelGrid->GetStraightModel());

	CoupledModelPreProcessor->SetCouplingArrayInfo(this->CouplingArrayInfo);

	CoupledModelPreProcessor->SetIntegerParam(this->ConfigIntegerParam);

	CoupledModelPreProcessor->SetTimeParam(this->TimeParam);

  CoupledModelPreProcessor->SetSegregatedSubRelaxation(this->SegregatedSubRelaxation);
  
  CoupledModelPreProcessor->SetMonolithicSubRelaxation(this->MonolithicSubRelaxation);

	CoupledModelPreProcessor->SetModelConfigParam(this->ModelConfigParam);
	
	CoupledModelPreProcessor->Set3DConvergenceParam(this->ConvParam3D);
	
	CoupledModelPreProcessor->Set1DConvergenceParam(this->ConvParam1D);

	CoupledModelPreProcessor->SetSolverParam(this->SolverParam);

	CoupledModelPreProcessor->SetLogFileName(this->LogFileName);

	if (!ConfigIntegerParam[0])   // 0 - Least Square / 1= ViscoElatsic (1D)
		CoupledModelPreProcessor->SetMax1DElemLib(2);
	else
		CoupledModelPreProcessor->SetMax1DElemLib(4);

	
	CoupledModelPreProcessor->CreateNumberOfDifferentRegionsList();
	
	CoupledModelPreProcessor->CreateCouplingInfoIndex();
	
	//CoupledModelPreProcessor->ConfigureBasparam(); 
	
	CoupledModelPreProcessor->SetScaleFactor3D(this->ScaleFactor3D);

	this->GenerationStatus = CoupledModelPreProcessor->InitializeWriteProcess(path);

	CoupledModelPreProcessor->Delete();
	
	// tem que se deletar pois o reference count do FullModel e StraightModel é incrementado
	// quando os mesmos sao utilizados como parametros em metodos SET (vtkSetObjectMacro)
	//FullModel->Delete();
	StraightModelGrid->GetStraightModel()->Delete();
	
	// tem que se deletar pois o reference count do this->CouplingArrayInfo é incrementado
	// quando o mesmo é utilizados como parametros em metodos SET (vtkSetObjectMacro)
	this->CouplingArrayInfo->Delete();
	
	//this->GenerationStatus =1;

}

//----------------------------------------------------------------------------


int vtkHMCouplingFilter::GetGenerationStatus()
{
	return this->GenerationStatus;
}
//----------------------------------------------------------------------------

int vtkHMCouplingFilter::GetNumberOfTotalIOFaces()
{
  int TotalNumberOfCovers = 0;
  for (int i = 0; i < this->numInputs-1; ++i) 
    TotalNumberOfCovers = TotalNumberOfCovers + this->GetFullModel(i)->GetNumberOfCoverGroups();
	
	return TotalNumberOfCovers;
}
//----------------------------------------------------------------------------


vtkHM3DFullModel *vtkHMCouplingFilter::GetFullModel(int Number)
{
	return vtkHM3DFullModel::SafeDownCast(this->FullModelsCollection->GetItemAsObject(Number));
}

//----------------------------------------------------------------------------

int vtkHMCouplingFilter::GetFullModelNumberOfCovers(int FullModel)
{
	return this->GetFullModel(FullModel)->GetNumberOfCoverGroups();
	
}

//----------------------------------------------------------------------------


const char *vtkHMCouplingFilter::GetColorCode()
{
	if (!this->FullModelsCollection->GetNumberOfItems())
		return NULL;
	
	vtkIdList *ColorCode = vtkIdList::New();
	
	// codigo de cor para cada tampa 
	// 0 - 3d 1
	// 1 - 3d 2
	// n - 3d n
	// se temos 2 3ds 1 tubo e 1 carotida
	// o vetor colocode vai ter: 0 0 1 1 1
	int counter = 0;
	
	for (int i = 0; i < this->numInputs-1; ++i) 
   	{
   	for (int z = 0; z < this->GetFullModel(i)->GetNumberOfCoverGroups(); ++z)
   		{
   		ColorCode->InsertNextId(counter);
   		//cout << "Inserindo valor no vetor "<< counter << endl;	
   		}	
   	counter++;	
   	}
    
	this->listChar = vtkHMUtils::ConvertIdListToChar(ColorCode);
	ColorCode->Delete();
	return this->listChar.c_str();
	
	
}

//----------------------------------------------------------------------------


//int vtkHMCouplingFilter::GetNumberOfDifferentRegions(vtkHM3DFullModel *FullModel)
//{
//	int NumberOfRegions;
//	vtkPVConnectivityFilter *filter = vtkPVConnectivityFilter::New();
//	filter->SetInput(FullModel);
//	filter->Update();
//	NumberOfRegions = filter->GetNumberOfExtractedRegions();
//	filter->Delete();
//	return NumberOfRegions;
//}

// ----------------------------------------------------------------------------

void vtkHMCouplingFilter::SetSelectedSolverFiles(int WhichFile[4])
{
	for (int i=0; i < 4; i++)
		this->WhichFile[i]	=	WhichFile[i];
	
}

// ----------------------------------------------------------------------------

