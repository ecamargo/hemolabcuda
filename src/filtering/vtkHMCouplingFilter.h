/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMCouplingFilter

  Author:    Jan Palach, Rodrigo L. S. Silva
  					 Paulo Ziemer 		

=========================================================================*/
// .NAME vtkHMCouplingFilter
// .SECTION Description
// Filter that receives two or more inputs: One StraightModel and one or more 3D Fullmodels. These will be used to create
// the Solver Input Files from the Coupled Model.

#ifndef __vtkHMCouplingFilter_h
#define __vtkHMCouplingFilter_h

#include "vtkHM1DStraightModelGrid.h"
#include "vtkMultiGroupDataSetAlgorithm.h"
#include "vtkPolyData.h"
#include <string>
#include <iostream>

using namespace std;
class vtkIntArray;
class vtkHM3DFullModel;
class vtkCollection;
class VTK_EXPORT vtkHMCouplingFilter : public vtkMultiGroupDataSetAlgorithm 
{
public:
  
  vtkTypeRevisionMacro(vtkHMCouplingFilter,vtkMultiGroupDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkHMCouplingFilter *New();
	
  // Release Data Flag	
  void SetReleaseDataFlag(int i);

  // Description:
  // Add an input of this algorithm.  Note that these methods support
  // old-style pipeline connections.  When writing new code you should
  // use the more general vtkAlgorithm::AddInputConnection().  See
  // SetInput() for details.
  void AddInput(vtkDataObject *);
  void AddInput(int, vtkDataObject*);

  // Description:
  // Controls which input was enabled on the interface (PV)
  void SetInputEnabled(int value);
  
  // Description:
  // Get/Set Cover Groups
	vtkGetMacro(CoverGroups,int);
	vtkSetMacro(CoverGroups,int);
	

  // Description:
  // Get/Set Wall Groups
	vtkGetMacro(WallGroups,int);
	vtkSetMacro(WallGroups,int);
	
	
	
  // Description:
  // Get and Set methods for the number of 3d models which are valid  
	vtkGetMacro(NumberOf3DValidInputs,int);
	vtkSetMacro(NumberOf3DValidInputs,int);

  // Description:
  // Set Selected Group
	void	SelectGroup(int gr);

  // Description:
  // Callback for "1DInputNumber" and "3DInputNumber" properties
	void Set1DInputNumber(int );
	void Set3DInputNumber(int );
	
	// Description:
  // Set several parameters used in the solver generation files
	void SetIntParam(int IntParam[37]);

	// Description:
  // Set several parameters used in the solver generation files
	void SetModelConfigParam(double ModelConfigParam[8]);

	// Description:
  // Set several parameters used in the solver generation files
	void SetTimeParam(double TimeParam[3]);

	// Description:
  // Set several parameters used in the solver generation files
	void SetSegregatedSubRelaxation(double Rela);
	
	// Description:
  // Set several parameters used in the solver generation files
	void SetMonolithicSubRelaxation(double Rela);

	// Description:
  // Set several parameters used in the solver generation files
	void Set3DConvergenceParam(double ConvParam[15]);

	// Description:
  // Set several parameters used in the solver generation files
	void Set1DConvergenceParam(double ConvParam[9]);

	// Description:
  // Set Log file name used in the solver generation files
	void SetLogFileName(const char *logfilename);
	
	// Description:
	// the string CouplingInfo must be processed in order to ger the information related to the coupling points
	// and this must be stored in a vtkIntArray.
	void SetCouplingInformation(const char *CouplingInfo);

	// Description:
  // creates the preprocessor which will run the 1D and 3D models and create the solver input files
	void GenerateSolverFiles(char *path);
	
	// Description:
  // set some parameters used when the Basparam is created.
	void SetSolverConfigParam(double SolverParam[13]);
	
	
	// Description:
  // returns if the generation of solver input files were (1) or not (0) successfull
	int GetGenerationStatus();
	
	// Description:
  // get the number of all covers from all 3D models from the coupled system 
	int GetNumberOfTotalIOFaces();
	
	// Description:
  // Set ang get method for the var that control the convertion between 1D and 3D scales
	vtkSetMacro(ScaleFactor3D, double);
	vtkGetMacro(ScaleFactor3D, double); 
	
	// Description:
  // returns the fullmodel given by the integer var Number 
	vtkHM3DFullModel *GetFullModel(int Number);	
	
	// Description:
  // returns the number of covers from some 3DFullModel
	int GetFullModelNumberOfCovers(int FullModel);
	
	// Description:
  // return the array that indicate the code of the color from each coupling point that will be represented 
	const char *GetColorCode();
	
	//int GetNumberOfDifferentRegions(vtkHM3DFullModel *FullModel);
	
	void SetSelectedSolverFiles(int WhichFile[4]);

//*****************************************************************************************	
//*****************************************************************************************	
//*****************************************************************************************	

protected:
  vtkHMCouplingFilter();
  ~vtkHMCouplingFilter();

	// Description:
   // Set where the Straight model and Meshdata are located (input 0 or input 1)
	int StraightModelInputNumber; // TO BE DELETED
	int MeshDataInputNumber;		// TO BE DELETED
	int NumberOfMeshData;

	// Description:  
  // Fill Input Information.
  int FillInputPortInformation( int vtkNotUsed(port), vtkInformation* info);

	// Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
    
	// Description:    
  // Set Number of Input Connections
  void SetNumberOfInputConnections(int port, int n);

	// Description:    
  // Variable to control current input
  int portIndex;
  
	// Description:
  // Variables to control which Group was selected
  int SelectedGroup; 

	// Description:
  // Current input enabled
  int InputEnabled;

	// Description:
  // Retrieve cover groups
	int CoverGroups;  
	
	
	int WallGroups;
	
	// Description:
  // several parameters used in the solver generation files
	int ConfigIntegerParam[37];	
	
	// Description:
  // several parameters used in the solver generation files
	double ModelConfigParam[8];
  
	// Description:
  // store the time parameters used during the solver files generation
  double TimeParam[3];
  
	// Description:
  // several parameters used in the solver generation files
  double SegregatedSubRelaxation;
  
	// Description:
  // several parameters used in the solver generation files
  double MonolithicSubRelaxation;
	
	// Description:
  // store the value of the convergence parameters 
  double ConvParam3D[15];
  
	// Description:
  // several parameters used in the solver generation files
  double ConvParam1D[9];

	// Description:
  // several parameters used in the solver generation files
	double SolverParam[13];

	// Description:
  // store the log filename used in Basparam
  char LogFileName[50];
 
	// Description:
 	// array that store the information related to each coupling point
 	// #1(id_segment, seg.number, 3d group);#2();..#number of covers()
 	vtkIntArray *CouplingArrayInfo;
 	
 	// Description:
  // controls if generation of solver files was ok or not
	int GenerationStatus;
	
	// Description:
  // reference to 1D model
	vtkHM1DStraightModelGrid *StraightModelGrid;
	
	// Description:
	// control the total number of inputs from this filter
	int numInputs; 
	
	// Description:
  // track the number of valid 3d inputs; this filter can have any dataset as input so is impotrtant to test if 3D input
  // is really from the class vtkHM3DFullModel
	int NumberOf3DValidInputs;
	
	// Description:
  // scale factor used to convert the dimensions of 1D and 3D models
  // currentely apllied only to the 3D coordinates
	double ScaleFactor3D;
	
	// Description:
  // List of 3DFullmodels envolved in the coupled model
	vtkCollection *FullModelsCollection;
	
	int WhichFile[4];

private:
  vtkHMCouplingFilter(const vtkHMCouplingFilter&);  // Not implemented.
  void operator=(const vtkHMCouplingFilter&);  // Not implemented.

 string listChar;	

};

#endif
