/*
 * $Id$
 */
#include "vtkHMMeshEditor.h"
#include "vtkObjectFactory.h"
#include "vtkPolyData.h"
#include "vtkPolyDataNormals.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMeshData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkCommand.h"
#include "vtkCellArray.h"
#include "vtkSphereSource.h"
#include "vtkSphere.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkClipPolyData.h"
#include "vtkAppendPolyData.h"
#include "vtkCleanPolyData.h"
#include "vtkSurfaceGen.h"

vtkCxxRevisionMacro(vtkHMMeshEditor, "$Rev$");
vtkStandardNewMacro(vtkHMMeshEditor);

//----------------------------------------------------------------------------
void vtkHMMeshEditor::InvertVectorDirection(int *boardVec, int sizeVec)
{
	int *tempVec = new int[sizeVec];
	
	tempVec[0] = boardVec[0];
	for(int i = 1; i < sizeVec; i++)
		{
		tempVec[i] = boardVec[sizeVec-i];
		}
	for(int i = 1; i < sizeVec; i++)
		{
		boardVec[i] = tempVec[i];
		}
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::CleanPoly(vtkPolyData* input)
{
	vtkCleanPolyData *Clean = vtkCleanPolyData::New();
		Clean->ConvertStripsToPolysOn();
		Clean->ConvertLinesToPointsOn();
		Clean->ConvertPolysToLinesOff ();
		Clean->PointMergingOn ();
		Clean->SetInput(input);	
		Clean->GetOutput()->Update();

	input->DeepCopy(Clean->GetOutput());
	input->BuildLinks();
//	input->BuildCells();
	input->Update();
	
	Clean->Delete();
}

//----------------------------------------------------------------------------
bool vtkHMMeshEditor::CheckVectorDirection(vtkPolyData *poly, int *board1Vec, int *board2Vec, int board1Points, int board2Points)
{
	vtkIdList *idList2 = vtkIdList::New();
	idList2->SetNumberOfIds(3);
	int b1index = 1;
	int b2index = 1;
	double pAverage[3],dist1, dist2, p1[3], p2[3], p3[3], p4[3];
//	double distMax = this->ClipRadius/1.3;
	double distMax = this->ClipRadius/1.8;
	
	// Start first two cells to force direction
	// First Cell
	idList2->SetId(0, board1Vec[0]);
	idList2->SetId(1, board2Vec[0]);
	idList2->SetId(2, board1Vec[1]);
		
	// Second Cell
	idList2->SetId(0, board1Vec[1]);
	idList2->SetId(1, board2Vec[0]);
	idList2->SetId(2, board2Vec[1]);
		
		

	while(1)
		{
		dist1 = dist2 = 0.0f;
		
		if(b1index < board1Points && b2index < board2Points)
			{
			poly->GetPoints()->GetPoint( board1Vec[b1index], p1);
			poly->GetPoints()->GetPoint( board2Vec[b2index], p2);
			ComputeAverageCoord(pAverage, p1, p2);			
			poly->GetPoints()->GetPoint( board1Vec[b1index+1], p1);
			poly->GetPoints()->GetPoint( board2Vec[b2index+1], p2);
			
			dist1 = GetDistance(p1, pAverage);
			dist2 = GetDistance(p2, pAverage);
			}
		else
			{
			if(b1index >= board1Points) 		dist1 = 1.0f;
			else										dist2 = 1.0f;			
			}
	
		// Stops only when the two vectors were visited
		if(b1index >= board1Points && b2index >= board2Points) break;		
		
		idList2->SetId(0, board1Vec[b1index]);
		idList2->SetId(1, board2Vec[b2index]);
		
		if(dist1 < dist2)
			{
			b1index++;
			idList2->SetId(2, board1Vec[b1index]);				
			}
		else
			{
			b2index++;						
			idList2->SetId(2, board2Vec[b2index]);
			}			
		
		poly->GetPoints()->GetPoint(idList2->GetId(0), p3);
		poly->GetPoints()->GetPoint(idList2->GetId(1), p4);
		if( GetDistance(p3, p4) > distMax )
			{
			idList2->Delete();
			return true;
			}
		poly->GetPoints()->GetPoint(idList2->GetId(0), p3);
		poly->GetPoints()->GetPoint(idList2->GetId(2), p4);
		if( GetDistance(p3, p4) > distMax )
			{
			idList2->Delete();
			return true;
			}
		
		poly->GetPoints()->GetPoint(idList2->GetId(1), p3);
		poly->GetPoints()->GetPoint(idList2->GetId(2), p4);
		if( GetDistance(p3, p4) > distMax )
			{
			idList2->Delete();
			return true;
			}
		}	
	idList2->Delete();
	return false;
}

//----------------------------------------------------------------------------
vtkHMMeshEditor::vtkHMMeshEditor()
{
	this->SetNumberOfInputPorts(1);
	this->SetNumberOfOutputPorts(1);
	this->PointNearCenter[0] = 0.0;
	this->PointNearCenter[1] = 0.0;
	this->PointNearCenter[2] = 0.0;
	this->ResolutionModifier = 0.0;
}

//----------------------------------------------------------------------------
double vtkHMMeshEditor::GetXInitialPlaceWidget()
{
	return this->PointNearCenter[0];
}

//----------------------------------------------------------------------------
double vtkHMMeshEditor::GetYInitialPlaceWidget()
{
	return this->PointNearCenter[1];
}

//----------------------------------------------------------------------------
double vtkHMMeshEditor::GetZInitialPlaceWidget()
{
	return this->PointNearCenter[2];
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetResolutionModifier(int value)
{
	this->ResolutionModifier = (double) value;
	this->Modified();
	
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetInitialPlaceWidget()
{
	vtkUnstructuredGrid *inputDataSet = vtkUnstructuredGrid::SafeDownCast(this->GetInput());
  double coordPoint[3];
  double pointDist = 0;
	double lastPointDist = 0;
	double centerBound[3];
	
	inputDataSet->GetCenter(centerBound);
	
	for(int i = 0; i < inputDataSet->GetNumberOfPoints(); i++)
		{
		inputDataSet->GetPoint(i, coordPoint);
		
		pointDist = sqrt( pow(centerBound[0] - coordPoint[0], 2) + pow(centerBound[1] - coordPoint[1], 2) + pow(centerBound[2] - coordPoint[2], 2));
		if(i == 0)
			{
			this->PointNearCenter[0] = coordPoint[0];
			this->PointNearCenter[1] = coordPoint[1];
			this->PointNearCenter[2] = coordPoint[2];
			lastPointDist = pointDist;
			}
		else
			{
			if(pointDist <= lastPointDist)
				{
				this->PointNearCenter[0] = coordPoint[0];
				this->PointNearCenter[1] = coordPoint[1];
				this->PointNearCenter[2] = coordPoint[2];
				lastPointDist = pointDist;
				}
			}
		}
}

//----------------------------------------------------------------------------
vtkHMMeshEditor::~vtkHMMeshEditor()
{
	
}

//----------------------------------------------------------------------------
int vtkHMMeshEditor::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
	return 1;
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::Modified()
{
	this->Superclass::Modified();
	this->InvokeEvent(vtkCommand::ModifiedEvent, this);
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetClipCenter(double PassedCenter[3])
{
	this->ClipCenter[0] = PassedCenter[0];
	this->ClipCenter[1] = PassedCenter[1];
	this->ClipCenter[2] = PassedCenter[2];

	this->Modified();
}

//----------------------------------------------------------------------------
double *vtkHMMeshEditor::GetClipCenter()
{
	return this->ClipCenter;
}
  
//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetClipRadius(double PassedRadius)
{
	this->ClipRadius = PassedRadius;
	
	this->Modified();
}

//----------------------------------------------------------------------------
double vtkHMMeshEditor::GetClipRadius()
{
	return this->ClipRadius;
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetClipFunction(vtkImplicitFunction *PassedImplicitFunction)
{
	this->SetClipCenter(vtkSphere::SafeDownCast(PassedImplicitFunction)->GetCenter());
	this->SetClipRadius(vtkSphere::SafeDownCast(PassedImplicitFunction)->GetRadius());
	
	this->Modified();
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::SetInsideOut(int PassedInsideOut)
{
	this->InsideOut = PassedInsideOut;
	this->Modified();
}

//----------------------------------------------------------------------------
void vtkHMMeshEditor::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

////////////////////////////////////////////////////////////////////////////////////////////
//Rodrigo's functions

// Compute the average coord of a cell (triangle) with 'ids' points
void vtkHMMeshEditor::GetAverageCoord(vtkPoints *points, vtkIdList* ids, double *average)
{	
	double *p1, *p2, *p3;
	p1 = points->GetPoint(ids->GetId(0));
	p2 = points->GetPoint(ids->GetId(1));
	p3 = points->GetPoint(ids->GetId(2));	
	average[0] = (p1[0] + p2[0] + p3[0]) / 3.0f;
	average[1] = (p1[1] + p2[1] + p3[1]) / 3.0f;
	average[2] = (p1[2] + p2[2] + p3[2]) / 3.0f;	
	
}

//----------------------------------------------------------------------------
// Find Board Cells. A Board Cell has at least one neighbor that is NULL
void vtkHMMeshEditor::FindBoardCells(vtkPolyData *clip, vtkPolyData *boarderTri)
{
	int numCells = clip->GetNumberOfPolys();
	vtkIdList *idList 		= vtkIdList::New();
	vtkIdList *neighEdge1 	= vtkIdList::New();
	vtkIdList *neighEdge2 	= vtkIdList::New();
	vtkIdList *neighEdge3 	= vtkIdList::New(); 	
	vtkIdList *boarderList 	= vtkIdList::New(); 	
	vtkIdType v0, v1, v2;	
	
	int boarder = 0;
	// Finding boarder Cells
	boarderList->SetNumberOfIds(numCells);	
	for (int i = 0; i < numCells; i++)
		{
		clip->GetCellPoints(i, idList);
		v0 = idList->GetId(0);
		v1 = idList->GetId(1);
		v2 = idList->GetId(2);
		clip->GetCellEdgeNeighbors(i, v0, v1, neighEdge1);
		clip->GetCellEdgeNeighbors(i, v0, v2, neighEdge2);
		clip->GetCellEdgeNeighbors(i, v1, v2, neighEdge3);		
		if(!neighEdge1->GetNumberOfIds() || !neighEdge2->GetNumberOfIds() || !neighEdge3->GetNumberOfIds())
			{
			boarderList->SetId(boarder, i);			
			boarder++;	
			}
		}
	// Setting polydata 
	SetPolyData(boarderTri, clip, boarderList, boarder);
	
	idList->Delete();
	neighEdge1->Delete();
	neighEdge2->Delete();
	neighEdge3->Delete(); 	
	boarderList->Delete(); 	
}

//----------------------------------------------------------------------------
// Search the triangles of the sphereSource that are near de polydata Board
// Result is stored in the "sphereintersection" polydata
void vtkHMMeshEditor::FindIntersectionBoard(vtkPolyData *sphereintersection, vtkPolyData *boarder, vtkPolyData *sphere, int *boarderVec)
{
	double distance 	= GetGreaterEdgeSize(boarder);
	int sphereCells 	= sphere->GetNumberOfPolys();
	int spherePoints	= sphere->GetNumberOfPoints();
	int boarderPoints	= boarder->GetNumberOfPoints();

	double *psphere, *pboard;
	int i, j;
	
	// Searching points near the boarder tri by euclidean distance
	int *pointsVec = (int *) calloc(sizeof(int),spherePoints);
	for (i = 0; i < spherePoints; i++) 
		{
		psphere = sphere->GetPoints()->GetPoint(i);
		for (j = 0; j < boarderPoints; j++)
			{
			pboard = boarder->GetPoints()->GetPoint(j);
			if(GetDistance(psphere, pboard) <= distance)		
				{
				pointsVec[i] = 1;				
				break;
				}
			}
		}
	
	// Searching cells that have an id stored in pointsVec
	vtkIdList *boarderList 	= vtkIdList::New(); 	
	boarderList->SetNumberOfIds(sphereCells); // Can't be more than this
	vtkIdList *tmpList;
	int removedCells = 0;
	for (i = 0; i < sphereCells; i++) 
		{
		tmpList = sphere->GetCell(i)->GetPointIds();
		// Se celula contiver uma das coordenadas proximas da borda, marca para remoção
		if(pointsVec[tmpList->GetId(0)] || pointsVec[tmpList->GetId(1)] || pointsVec[tmpList->GetId(2)])
			{
			boarderList->SetId(removedCells, i);		
			boarderVec[i] = 1;			
			removedCells++;			
			}
		}
	
	SetPolyData(sphereintersection, sphere, boarderList, removedCells);
	boarderList->Delete();
	free(pointsVec);
}

//----------------------------------------------------------------------------
// Returns the size of the greater polydata's edge 
double vtkHMMeshEditor::GetGreaterEdgeSize(vtkPolyData *poly)
{
	double edge = 0.0;
	vtkIdList *tmpList;
	double p1[3], p2[3], p3[3], edgeSize[3];
	for (int i = 0; i < poly->GetNumberOfPolys(); i++) 
		{
		tmpList = poly->GetCell(i)->GetPointIds();
		poly->GetPoints()->GetPoint( tmpList->GetId(0), p1);
		poly->GetPoints()->GetPoint( tmpList->GetId(1), p2);
		poly->GetPoints()->GetPoint( tmpList->GetId(2), p3);
		edgeSize[0] = GetDistance(p1, p2) * 0.6;	
		edgeSize[1] = GetDistance(p1, p3) * 0.6;	
		edgeSize[2] = GetDistance(p2, p3) * 0.6;	
		for(int j = 0; j < 3; j++) 
			if(edgeSize[j] >  edge)
				{
				edge = edgeSize[j];
				}
		}
//	edge *= 1.3;
	edge *= 2.0;
	return edge;	
}

//----------------------------------------------------------------------------
// Compute Euclidean distance between two points
double vtkHMMeshEditor::GetDistance(double *p1, double *p2)
{
	return sqrt( pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2) + pow(p1[2] - p2[2], 2));
}

// Search for a valid triangle seed (boarderVec 'id' != 1)
// and calls recursive function to find its neighbors
void vtkHMMeshEditor::FindSideA(vtkPolyData *sideA, vtkPolyData *sphere, int *boarderVec)
{
	int seed;
	int numCells = sphere->GetNumberOfPolys();
	int limitUp = numCells - (numCells/5);
	int limitDown = numCells/5;

//	for (int i = 0; i < numCells; i++)
	for (int i = limitDown; i < limitUp; i++) //força a busca fora dos pólos da esfera 
		if(boarderVec[i]!=1)
			seed = i;
	
	// Check the seed position and start looking for the others 
	boarderVec[seed] = 2;
	FindNeighbors(sphere, boarderVec, seed);
}

//----------------------------------------------------------------------------
// Recursive function that search and finds neighbors based on the 'seed' parameter 
void vtkHMMeshEditor::FindNeighbors(vtkPolyData *poly, int *boarderVec, int seed)
{
	boarderVec[seed] = 2;	// where 2 is a controled value
	vtkIdList* idList = vtkIdList::New();
	poly->GetCellPoints(seed, idList);
	vtkIdList *n1 = vtkIdList::New();
	vtkIdList *n2 = vtkIdList::New();
	vtkIdList *n3 = vtkIdList::New();
	
	// Get Neighbors pointers
	poly->GetCellEdgeNeighbors(seed, idList->GetId(0), idList->GetId(1), n1);
	poly->GetCellEdgeNeighbors(seed, idList->GetId(0), idList->GetId(2), n2);
	poly->GetCellEdgeNeighbors(seed, idList->GetId(1), idList->GetId(2), n3);
	
	// Get Neighbors ids
	int seedn1 = n1->GetId(0);
	int seedn2 = n2->GetId(0);
	int seedn3 = n3->GetId(0);	
	
	// Free memory before start recursion
	n1->Delete();
	n2->Delete();
	n3->Delete();
	idList->Delete();	
	
	// Call the method recursively to find valid neighbors
	if( !boarderVec[seedn1] ) FindNeighbors(poly, boarderVec, seedn1);
	if( !boarderVec[seedn2] ) FindNeighbors(poly, boarderVec, seedn2);
	if( !boarderVec[seedn3] ) FindNeighbors(poly, boarderVec, seedn3);
}

//----------------------------------------------------------------------------
// Copy cells from 'input' to 'output'. Cell ids are stored in the 'idList'
void vtkHMMeshEditor::SetPolyData(vtkPolyData *output, vtkPolyData *input, vtkIdList *idlist, int numCells)
{
	// Setting points
	vtkPoints* points = vtkPoints::New();
	for (int i = 0; i < input->GetNumberOfPoints(); i++) 
		points->InsertPoint( i, input->GetPoints()->GetPoint(i));
	output->SetPoints(points);
	
	// Setting cells
	vtkCellArray *cells = vtkCellArray::New();
	for (int i = 0; i < numCells; i++) 
		cells->InsertNextCell( input->GetCell( idlist->GetId(i) )->GetPointIds());
	output->SetPolys(cells);
	
	cells->Delete();
	points->Delete();
}

//----------------------------------------------------------------------------
// Find Board Points
void vtkHMMeshEditor::FindBoardPoints(vtkPolyData *poly, int *idVec)
{
	poly->BuildLinks();
	
	// Mapping merge table
	//int numCells = poly->GetNumberOfCells();
	vtkIdList *Edge 	= vtkIdList::New();
	vtkIdList *idList	= vtkIdList::New();	
	vtkIdType v0, v1, v2;
	// Finding boarder Points
	for (int i = 0; i < poly->GetNumberOfCells(); i++)
		{
		poly->GetCellPoints(i, idList);
		v0 = idList->GetId(0);
		v1 = idList->GetId(1);
		v2 = idList->GetId(2);
		// If no neighbor in one of these edges is found, that a pair of board points
		// First Edge
		poly->GetCellEdgeNeighbors(i, v0, v1, Edge);
		if(!Edge->GetNumberOfIds() ) idVec[v0] = idVec[v1] = 1;
		// Second Edge		
		poly->GetCellEdgeNeighbors(i, v0, v2, Edge);
		if(!Edge->GetNumberOfIds() ) idVec[v0] = idVec[v2] = 1;
		// Third Edge		
		poly->GetCellEdgeNeighbors(i, v1, v2, Edge);
		if(!Edge->GetNumberOfIds() ) idVec[v1] = idVec[v2] = 1;		
		}		
	Edge->Delete();
	idList->Delete();
}

//----------------------------------------------------------------------------
// Get Closest Valid Point based on distance
int vtkHMMeshEditor::GetClosestValidPoint(int reference, vtkPolyData* poly1, int *poly1Vec)
{
	int selected;
	double point1[3], point2[3];
	
	poly1->GetPoints()->GetPoint(reference, point1);
	double tempDist, distance = -1;	
	for(int s = 0; s < poly1->GetNumberOfPoints(); s++)
		{
		if(poly1Vec[s]==2)
			{
			poly1->GetPoints()->GetPoint(s, point2);
			tempDist = GetDistance(point1, point2);
			if(distance < 0 || tempDist < distance) 
				{
				distance = tempDist;
				selected = s;
				}
			}
		}
	// Return closest point id
	return selected;
}

//----------------------------------------------------------------------------
// Set scalar value (value) in the polydata (poly) points
void vtkHMMeshEditor::SetScalars(vtkPolyData* poly, int value)
{
	vtkIntArray *array = vtkIntArray::New();
	for (int i = 0;  i < poly->GetNumberOfPoints(); i++) 
		array->InsertNextValue(value);
	poly->GetPointData()->SetScalars(array);
	array->Delete();
}

//----------------------------------------------------------------------------
// Set scalar value (value) in the polydata (poly) cells   
void vtkHMMeshEditor::SetScalarsCells(vtkPolyData* poly, int value)
{
	vtkIntArray *array = vtkIntArray::New();
	if(!poly->GetCellData()->GetScalars())
		{
		for (int i = 0;  i < poly->GetNumberOfCells(); i++) 
			array->InsertNextValue(value);
		poly->GetCellData()->SetScalars(array);
		}
	else
		{
		for (int i = 0;  i < poly->GetNumberOfCells(); i++)
			{
			if ( (poly->GetCellData()->GetScalars()->GetTuple1(i) != 2) && (poly->GetCellData()->GetScalars()->GetTuple1(i) != 3) )
				poly->GetCellData()->GetScalars()->InsertNextTuple1(value);
			}
		}
	array->Delete();
}

//----------------------------------------------------------------------------
// Compute point average coord
void vtkHMMeshEditor::ComputeAverageCoord(double *average, double *p1, double *p2)
{
	average[0] = ( p1[0] + p2[0] ) / 2.0f;
	average[1] = ( p1[1] + p2[1] ) / 2.0f;
	average[2] = ( p1[2] + p2[2] ) / 2.0f;	
}

//----------------------------------------------------------------------------
// Check initial propagation direction
// Pic the dominant Axis (x, y or z) and use it to drive the propagation
bool vtkHMMeshEditor::checkDirection(vtkPolyData* poly, int p1index, int p2index)
{
	double p1[3], p2[3];
	poly->GetPoints()->GetPoint( p1index, p1);
	poly->GetPoints()->GetPoint( p2index, p2);
	
	int dominantAxis = 0;
	double greaterDist = fabs(p1[0] - p2[0]);
	if(fabs(p1[1] - p2[1]) > greaterDist)
		{
		greaterDist = fabs(p1[1] - p2[1]);
		dominantAxis = 1;
		}
	if(fabs(p1[2] - p2[2]) > greaterDist)
		dominantAxis = 2;
	
	return (p1[dominantAxis] > p2[dominantAxis]) ? true : false;
}

//----------------------------------------------------------------------------
// Sorting polydata. Basically this procedure takes the board points
// and sort them, creating a piecewise linear curve.
void vtkHMMeshEditor::SortVector(vtkPolyData* poly, int *board, int *numPoints)
{	
	vtkIdList* idList = vtkIdList::New();
	vtkIdList* CellList = vtkIdList::New();	
	vtkIdList* Edge 	= vtkIdList::New();
	
	int previous, v[3];
	poly->GetPointCells(board[0], CellList);
	int firstCell = board[0];
	bool directionSelected = false;
	
	bool rightDir = true; // Check if the initial direction is correct (x axis)
	int i = 1;
	do
		{		
		for(int j = 0; j < CellList->GetNumberOfIds(); j++)
			{
			rightDir = true;			
			poly->GetCellPoints(CellList->GetId(j), idList);			
			v[0] = idList->GetId(0);
			v[1] = idList->GetId(1);
			v[2] = idList->GetId(2);
			
			if(v[0] == board[i-1]) v[0]=-1;
			if(v[1] == board[i-1]) v[1]=-1;
			if(v[2] == board[i-1]) v[2]=-1;			
			
			if(i==1)  previous = 0;
			else		 previous = i-2;
	
			if(v[0]==-1)
				{
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[1], Edge);
				if(!Edge->GetNumberOfIds()  && board[previous] != v[1])  
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[1]);
					if(rightDir)
						{
						board[i] = v[1];
						directionSelected = true;
						break;
						}
					}
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[2], Edge);				
				if(!Edge->GetNumberOfIds()  && board[previous] != v[2]) 
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[2]);
					if(rightDir)
						{					
						board[i] = v[2];
						directionSelected = true;						
						break;
						}
					}				
				}
			if(v[1]==-1)
				{
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[0], Edge);
				if(!Edge->GetNumberOfIds()  && board[previous] != v[0]) 
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[0]);
					if(rightDir)
						{										
						board[i] = v[0];
						directionSelected = true;						
						break;
						}
					}
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[2], Edge);				
				if(!Edge->GetNumberOfIds()  && board[previous] != v[2]) 
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[2]);
					if(rightDir)
						{										
						board[i] = v[2];
						directionSelected = true;						
						break;
						}
					}				
				}
			if(v[2]==-1)
				{
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[0], Edge);
				if(!Edge->GetNumberOfIds()  && board[previous] != v[0]) 
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[0]);
					if(rightDir)
						{										
						board[i] = v[0];
						directionSelected = true;						
						break;
						}
					}
				poly->GetCellEdgeNeighbors(CellList->GetId(j), board[i-1], v[1], Edge);				
				if(!Edge->GetNumberOfIds()  && board[previous] != v[1]) 
					{
	//					if(!directionSelected) rightDir = checkDirection(poly, board[0], v[1]);
					if(rightDir)
						{
						board[i] = v[1];
						directionSelected = true;						
						break;
						}
					}				
				}
			}
			CellList->Reset();		
			if(directionSelected)
				{
				poly->GetPointCells(board[i], CellList);
				*numPoints = *numPoints + 1;
				}
			i++;
		}while( firstCell != board[i-1]  && i < poly->GetNumberOfPoints() );  
	idList->Delete();
	Edge->Delete();	
	CellList->Delete();	
}

//----------------------------------------------------------------------------
// Sew (costura) the two polydatas
void vtkHMMeshEditor::SewPolyData(vtkPolyData *poly, double *bounds)
{
	poly->Update();
	poly->BuildLinks();	

	vtkIdList* idList = vtkIdList::New();
	vtkIdList* Edge 	= vtkIdList::New();
	double p[3];
	bool outOfBounds;
	int v0, v1, v2;
		
	// Allocating auxiliary structures
	int *vec 		= (int *) calloc(sizeof(int), poly->GetNumberOfPoints());
	
	// Initializing vectors	
	for(int i = 0; i < poly->GetNumberOfPoints(); i++)
		vec[i] = -1;		
	
	// Find board Points and store them on different vectors
	for(int i = 0; i < poly->GetNumberOfCells(); i++)
		{
		outOfBounds = false;
		poly->GetCellPoints(i, idList);
		for(int j = 0; j < idList->GetNumberOfIds(); j++)
			{
			poly->GetPoints()->GetPoint( idList->GetId(j), p);
			if( p[0] < bounds[0] || p[0] > bounds[1] || 
			    p[1] < bounds[2] || p[1] > bounds[3] ||
				 p[2] < bounds[4] || p[2] > bounds[5])
				{
				outOfBounds = true;
				break;
				}
			if(poly->GetCellData()->GetScalars()->GetTuple1(i) == 2)
				{
				outOfBounds = true;
				//cout<< "Célula de borda encontrada" <<endl;
				break;
				}
			}
		// If cell is out of bounds, skip it
		if(outOfBounds) continue;
	
		v0 = idList->GetId(0);
		v1 = idList->GetId(1);
		v2 = idList->GetId(2);
		
		poly->GetCellEdgeNeighbors(i, v0, v1, Edge);
		if(!Edge->GetNumberOfIds() ) vec[v0] = vec[v1] = 0;
		// Second Edge		
		poly->GetCellEdgeNeighbors(i, v0, v2, Edge);
		if(!Edge->GetNumberOfIds() ) vec[v0] = vec[v2] = 0;
		// Third Edge		
		poly->GetCellEdgeNeighbors(i, v1, v2, Edge);
		if(!Edge->GetNumberOfIds() ) vec[v1] = vec[v2] = 0;			
		
		// Set a different value for the second polydata (scalar value == 1)
		if(vec[v0]!=-1 && poly->GetPointData()->GetScalars()->GetTuple1(v0) == 1) vec[v0] = 1;
		if(vec[v1]!=-1 && poly->GetPointData()->GetScalars()->GetTuple1(v1) == 1) vec[v1] = 1;			
		if(vec[v2]!=-1 && poly->GetPointData()->GetScalars()->GetTuple1(v2) == 1) vec[v2] = 1;
		}
	
	int *board1Vec = (int *) calloc(sizeof(int), poly->GetNumberOfPoints());
	int *board2Vec = (int *) calloc(sizeof(int), poly->GetNumberOfPoints());
	
	// Get First Board1 Point
	int board1Seed = 0;	
	for(int i = 0; i < poly->GetNumberOfPoints(); i++)
		if(vec[i]==0)
			{
			board1Seed = i;
			break;
			}	
	board1Vec[0] = board1Seed;
	
	// Searching closer board2 Point
	double tempDist, distance = -1;
	int board2Seed = 0;	
	double p1[3], p2[3];
	poly->GetPoints()->GetPoint( board1Vec[0] , p1);	
	for(int i = 0; i < poly->GetNumberOfPoints(); i++)
		{
		if(vec[i]==1) poly->GetPoints()->GetPoint(i, p2);
		else			  continue;
		tempDist = GetDistance(p1, p2);
		if(distance < 0 || tempDist < distance) 
			{
			distance = tempDist;
			board2Seed = i;
			}		
		}	
	board2Vec[0] = board2Seed;
	
	// Populando e ordenando os vetores
	int board1Points = 0, board2Points = 0;
	SortVector(poly, board1Vec, &board1Points);
	SortVector(poly, board2Vec, &board2Points);	
	
	//Verifica se os vetores estão na mesma direção
	if( CheckVectorDirection(poly, board1Vec, board2Vec, board1Points, board2Points) )
		InvertVectorDirection(board2Vec, board2Points);
	
	// Start first two cells to force direction
	// First Cell
	idList->SetId(0, board1Vec[0]);
	idList->SetId(1, board2Vec[0]);
	idList->SetId(2, board1Vec[1]);
	poly->InsertNextCell(VTK_TRIANGLE, idList);	
	// Second
	idList->SetId(0, board1Vec[1]);
	idList->SetId(1, board2Vec[0]);
	idList->SetId(2, board2Vec[1]);
	poly->InsertNextCell(VTK_TRIANGLE, idList);	
		
	int b1index = 1;
	int b2index = 1;
	double pAverage[3],dist1, dist2;
	
	while(1)
		{
		dist1 = dist2 = 0.0f;
		
		if(b1index < board1Points && b2index < board2Points)
			{
			poly->GetPoints()->GetPoint( board1Vec[b1index], p1);
			poly->GetPoints()->GetPoint( board2Vec[b2index], p2);
			ComputeAverageCoord(pAverage, p1, p2);			
			poly->GetPoints()->GetPoint( board1Vec[b1index+1], p1);
			poly->GetPoints()->GetPoint( board2Vec[b2index+1], p2);
			
			dist1 = GetDistance(p1, pAverage);
			dist2 = GetDistance(p2, pAverage);
			}
		else
			{
			if(b1index >= board1Points) 		dist1 = 1.0f;
			else										dist2 = 1.0f;			
			}
	
		// Stops only when the two vectors were visited
		if(b1index >= board1Points && b2index >= board2Points) break;		
		
		idList->SetId(0, board1Vec[b1index]);
		idList->SetId(1, board2Vec[b2index]);
		
		if(dist1 < dist2)
			{
			b1index++;
			idList->SetId(2, board1Vec[b1index]);				
			}
		else
			{
			b2index++;						
			idList->SetId(2, board2Vec[b2index]);
			}			
		poly->InsertNextCell(VTK_TRIANGLE, idList);
		}
	
	SetScalarsCells(poly, 3);
	
	idList->Delete();	
	Edge->Delete();
	free(vec);	
	free(board1Vec);
	free(board2Vec);

}

//----------------------------------------------------------------------------
//Return "true" if PolyData has cells with two, or less, neigbhors
bool vtkHMMeshEditor::SewPolyDataAgain(vtkPolyData *polyIn, double *bounds)
{
	bool again = false;
	vtkIdList* idList = vtkIdList::New();
	vtkIdList* Edge 	= vtkIdList::New();
	double p[3];
	bool outOfBounds;
	int v0, v1, v2;
	
	vtkPolyData* poly = vtkPolyData::New();
		poly->DeepCopy(polyIn);
		poly->Update();
		poly->BuildLinks();

	// Find board Points
	for(int i = 0; i < poly->GetNumberOfCells(); i++)
		{
		outOfBounds = false;
		poly->GetCellPoints(i, idList);
		for(int j = 0; j < idList->GetNumberOfIds(); j++)
			{
			poly->GetPoints()->GetPoint( idList->GetId(j), p);
			if( p[0] < bounds[0] || p[0] > bounds[1] || 
			    p[1] < bounds[2] || p[1] > bounds[3] ||
				 p[2] < bounds[4] || p[2] > bounds[5])
				{
				outOfBounds = true;
				break;
				}
			}
		// If cell is out of bounds, skip it
		if(outOfBounds) continue;

		v0 = idList->GetId(0);
		v1 = idList->GetId(1);
		v2 = idList->GetId(2);
		
		poly->GetCellEdgeNeighbors(i, v0, v1, Edge);
		// First Edge		
		if(!Edge->GetNumberOfIds() ) again = true;
		// Second Edge		
		poly->GetCellEdgeNeighbors(i, v0, v2, Edge);
		if(!Edge->GetNumberOfIds() ) again = true;
		// Third Edge		
		poly->GetCellEdgeNeighbors(i, v1, v2, Edge);
		if(!Edge->GetNumberOfIds() ) again = true;
		}
	poly->Update();
	idList->Delete();
	Edge->Delete();
	poly->Delete();
	
	return again;
}

//----------------------------------------------------------------------------
//Set scalar value for Polydata`s cells in the board
void vtkHMMeshEditor::SetScalarsBoard(vtkPolyData* polyIn, int value)
{
	vtkIdList* idList = vtkIdList::New();
	vtkIdList* Edge 	= vtkIdList::New();
	int v0, v1, v2;
	
	polyIn->Update();
	polyIn->BuildLinks();
		
	// Find board Points and set up scalar
	for(int i = 0; i < polyIn->GetNumberOfCells(); i++)
		{
		polyIn->GetCellPoints(i, idList);
		v0 = idList->GetId(0);
		v1 = idList->GetId(1);
		v2 = idList->GetId(2);
		
		polyIn->GetCellEdgeNeighbors(i, v0, v1, Edge);
		// First Edge		
		if(!Edge->GetNumberOfIds() ) polyIn->GetCellData()->GetScalars()->SetTuple1(i, value);
		// Second Edge		
		polyIn->GetCellEdgeNeighbors(i, v0, v2, Edge);
		if(!Edge->GetNumberOfIds() ) polyIn->GetCellData()->GetScalars()->SetTuple1(i, value);
		// Third Edge		
		polyIn->GetCellEdgeNeighbors(i, v1, v2, Edge);
		if(!Edge->GetNumberOfIds() )	polyIn->GetCellData()->GetScalars()->SetTuple1(i, value);
		}
	
	polyIn->Update();

	idList->Delete();
	Edge->Delete();	
}

//----------------------------------------------------------------------------
// Estimate the sphere resolution based on the input 'poly' resolution
int vtkHMMeshEditor::EstimateSphereResolution(vtkPolyData *poly, vtkSphere *implicitSphere)
{
	double polydataAverageArea = 0.0;
	double sphereAverageArea = 0.0;	
	
	polydataAverageArea = GetPolyDataAverageArea(poly);
	//cout << " >> Input Polydata Averagea Area: " << polydataAverageArea << endl;
	
	// Creating polygonal sphere with "implicitSphere" data and resolution 20
	vtkSphereSource *sphere = vtkSphereSource::New();	
		sphere->SetCenter(implicitSphere->GetCenter());
		sphere->SetRadius(implicitSphere->GetRadius());
	
	int resolution = 10;

	// Searching best resolution Value
	//cout << " >> Searching best sphere resolution value (between 10 and 150): " << endl;
//	while(resolution < 150)
	while(resolution < 250)
		{
		sphere->SetThetaResolution( resolution );
		sphere->SetPhiResolution( resolution );		
		sphere->Update();
		sphereAverageArea = GetPolyDataAverageArea(sphere->GetOutput());
		//cout << "  * Resolution(" << resolution << ") Area: " << sphereAverageArea << endl;		
		if(sphereAverageArea > (polydataAverageArea * 0.6)) resolution+=5;
		else break;
		}	
	sphere->Delete();
	return resolution;
}

//----------------------------------------------------------------------------
// Computes Polydata Average Area
double vtkHMMeshEditor::GetPolyDataAverageArea(vtkPolyData *poly)
{
	double polyArea = 0.0;
	vtkIdList *tmpList;
	double p[3][3];
	// Computing polydata average area	
	for (int i = 0; i < poly->GetNumberOfPolys(); i++) 
		{
		tmpList = poly->GetCell(i)->GetPointIds();
		poly->GetPoints()->GetPoint( tmpList->GetId(0), p[0]);
		poly->GetPoints()->GetPoint( tmpList->GetId(1), p[1]);
		poly->GetPoints()->GetPoint( tmpList->GetId(2), p[2]);
		polyArea += GetTriangleArea(p);
		}
	polyArea /= poly->GetNumberOfPolys();
	return polyArea;
}

//----------------------------------------------------------------------------
// Computes triangle's average area
double vtkHMMeshEditor::GetTriangleArea(double p[][3])
{
	// Aplica formula de Heron para calcular area	
	double a, b, c, s, area;
	a = sqrt( pow(p[1][0] - p[0][0],2) + pow(p[1][1] - p[0][1], 2) + pow(p[1][2] - p[0][2], 2));
	b = sqrt( pow(p[2][0] - p[1][0],2) + pow(p[2][1] - p[1][1], 2) + pow(p[2][2] - p[1][2], 2));
	c = sqrt( pow(p[0][0] - p[2][0],2) + pow(p[0][1] - p[2][1], 2) + pow(p[0][2] - p[2][2], 2));
	
	s = (a + b + c)/2;	
	area = sqrt( s * (s-a) * (s-b) * (s-c) );
	return area;		
}

//----------------------------------------------------------------------------
bool vtkHMMeshEditor::IsNormalsToOutside(vtkPolyData *input)
{
	// Convert PolyData to UnstructuredGrid
	vtkUnstructuredGrid *grid = vtkUnstructuredGrid::New();
		grid->SetCells(input->GetCellType(0), input->GetPolys());
		grid->SetPoints(input->GetPoints());
		grid->Update();

	vtkMeshData *output = vtkMeshData::New();
		output->CopyUnstructuredGridToMeshDataSurface( grid );
	int res = output->GetSurface()->CheckOrientation();
		
	output->Delete();
	grid->Delete();
	
	return (res == 1) ? true : false; 
}

////////////////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------
int vtkHMMeshEditor::RequestData(vtkInformation *vtkNotUsed(request),
								 vtkInformationVector **inputVector,
								 vtkInformationVector *outputVector)
{
	// Getting objects information
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);
	
	// Getting input and output filter references
	vtkUnstructuredGrid *Input = vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkMeshData *Output = vtkMeshData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
		
	//Converting UnstructuredGrid to PolyData
	vtkPolyData *Poly = vtkPolyData::New();
	Poly->SetPolys(Input->GetCells());
	Poly->SetPoints(Input->GetPoints());
	Poly->Update();
	
	SetScalarsCells(Poly, 3);	//Scalars for to all cells
	SetScalarsBoard(Poly, 2);	//Scalars for to board cells
		
	//Setting the MainMesh with the Input
	vtkPolyData *MainMesh = vtkPolyData::New();
	MainMesh->DeepCopy(Poly);
	MainMesh->Update();
		
	//-------------------------------------------------------------------------	
	//cout << "[Clipping PolyData]" << endl;		
	// Main Sphere 
	vtkSphere *BiteSphere = vtkSphere::New();
		BiteSphere->SetCenter(GetClipCenter());
		BiteSphere->SetRadius(GetClipRadius());
	
	// Clipping surface with the sphere (hole creation)
	vtkClipPolyData *ClipPolyData = vtkClipPolyData::New();	
		ClipPolyData->SetInput(MainMesh);
		ClipPolyData->SetClipFunction(BiteSphere);
		ClipPolyData->SetInsideOut(0); 
		ClipPolyData->Update();
		
	// Get Clipped area to be used later on a sphere...
	vtkClipPolyData *ClippedArea = vtkClipPolyData::New();	
		ClippedArea->SetInput(MainMesh);
		ClippedArea->SetClipFunction(BiteSphere);
		ClippedArea->SetInsideOut(1);		
		ClippedArea->Update();
		ClippedArea->GetOutput()->BuildLinks();	
		
	vtkPolyData *Clipped = vtkPolyData::New();
		Clipped->DeepCopy(ClippedArea->GetOutput());
		Clipped->Update();
		
	// Cleaning Clipped Polydata
	CleanPoly(Clipped);
	
	//-------------------------------------------------------------------------		
	//cout << "[Computing Sphere Resolution]" << endl;		
	int resolution = EstimateSphereResolution(MainMesh, BiteSphere);
	
//	// Teste dobrando a resolução obtida no método acima
//	resolution /= 2;
	
	// Applying the resolution modifier (default is 100%)
	resolution = int(resolution * this->ResolutionModifier / 100.0f);
	
	//Source used for filling the cutted area (created based on the implicit function named BiteSphere).
	vtkSphereSource *MascSphere = vtkSphereSource::New();	
		MascSphere->SetCenter(BiteSphere->GetCenter());
		MascSphere->SetRadius(BiteSphere->GetRadius());
		MascSphere->SetThetaResolution(resolution);
		MascSphere->SetPhiResolution( int (resolution * 0.75) );
	
	vtkPolyData* sphere = MascSphere->GetOutput();
		sphere->Update();	
		sphere->BuildLinks();
		SetScalarsCells(sphere, 3);	//Scalars for to all cells
	
	//-------------------------------------------------------------------------
	//cout << "[Finding Board Triangles]" << endl;	
	// Finding Clipped Area Board Triangles
	// This will be used to reduced the clipping search area
	vtkPolyData *boarderTri = vtkPolyData::New();					
	FindBoardCells(Clipped, boarderTri);
	SetScalarsCells(boarderTri, 3);
	
	// Cleaning boarderTri Polydata
	CleanPoly(boarderTri);
	
	// Control Vector
	// 0 - One side (side A)
	// 1 - Intersection
	// 2 - Other side (side B)
	int *boarderVec = (int*) calloc(sizeof(int), sphere->GetNumberOfPolys() );
	
	//-------------------------------------------------------------------------
	//cout << "[Finding Intersection Triangles]" << endl;	
	// Finding Intersection (First part)
	vtkPolyData *intersection = vtkPolyData::New();		
	FindIntersectionBoard(intersection, boarderTri, sphere, boarderVec);
	SetScalarsCells(intersection, 3);	
	
	// Cleaning intersection Polydata
	CleanPoly(intersection);

	//-------------------------------------------------------------------------
	//cout << "[Finding Sphere Sides]" << endl;
	//cout << " >> Side A" << endl;	
	// Finding SIDE A
	// It will use an recursive method to find this polydata, based
	// on the neighborhood and the BoarderVec Array
	vtkPolyData *sideA = vtkPolyData::New();		
	FindSideA(sideA, sphere, boarderVec);
	int sideANumCells = 0;
	vtkIdList *boarderList = vtkIdList::New(); 	
	boarderList->SetNumberOfIds(sphere->GetNumberOfPolys());
	for (int i = 0; i < sphere->GetNumberOfPolys(); i++) 
		if(boarderVec[i] == 2)
			{
			boarderList->SetId(sideANumCells, i);				
			sideANumCells++;
			}
	SetPolyData(sideA, sphere, boarderList, sideANumCells);
	boarderList->Delete();
	SetScalarsCells(sideA, 3);	
	
	// Cleaning sideA Polydata
	CleanPoly(sideA);

	//cout << " >> Side B" << endl;	
	// Finding SIDE B
	// Just build the surface with the id's = 0
	vtkPolyData *sideB = vtkPolyData::New();		
	int sideBNumCells = 0;
	boarderList = vtkIdList::New(); 	
	boarderList->SetNumberOfIds(sphere->GetNumberOfPolys());
	for (int i = 0; i < sphere->GetNumberOfPolys(); i++) 
		if(boarderVec[i] == 0)
			{
			boarderList->SetId(sideBNumCells, i);				
			sideBNumCells++;
			}
	SetPolyData(sideB, sphere, boarderList, sideBNumCells);	
	boarderList->Delete();	
	SetScalarsCells(sideB, 3);
		
	// Cleaning sideB Polydata
	CleanPoly(sideB);

	// Setting scalars. Different values for the main
	// mesh (ClipPolyData) and the sphere cuts (side A and B)
	//	SetScalars(ClipPolyData->GetOutput(), 0);
	SetScalars(ClipPolyData->GetOutput(), 0);
	SetScalars(sideA, 1); // Here, we don't know which one was chosen
	SetScalars(sideB, 1);	
	
	// Merging polydatas
	vtkAppendPolyData* append = vtkAppendPolyData::New();
	(!this->InsideOut) ?
			append->AddInput(sideA): 
			append->AddInput(sideB);
			append->AddInput(ClipPolyData->GetOutput());		
	vtkPolyData *final = append->GetOutput();
	final->Update();
	
	//cout << "[Sewing PolyDatas]" << endl;	
	// Sew (costura) polydata
	SewPolyData(final, sphere->GetBounds());
	
	// Cleaning final Polydata
	vtkCleanPolyData *polyClean = vtkCleanPolyData::New();
		polyClean->ConvertStripsToPolysOn();
		polyClean->ConvertLinesToPointsOn();
		polyClean->ConvertPolysToLinesOff ();
		polyClean->PointMergingOn ();
		polyClean->SetInput(final);	
		polyClean->GetOutput()->Update();
	
	final->DeepCopy(polyClean->GetOutput());
	
	//Enquanto existirem bordas para costurar
	if( SewPolyDataAgain( final, sphere->GetBounds() ) )
		{
		//Atualiza o PolyData e faz uma cópia do mesmo para um PolyData temporário
		final->Update();
		vtkPolyData *tempPoly = vtkPolyData::New();
			tempPoly->DeepCopy(final);
			tempPoly->Update();
			
		// Cleaning tempPoly Polydata
		CleanPoly(tempPoly);
		
		//Sew PolyData
		SewPolyData(tempPoly, sphere->GetBounds());
		
		// Cleaning tempPoly Polydata
		CleanPoly(tempPoly);
	
		final->DeepCopy(tempPoly);
		final->Update();
		tempPoly->Delete();
		}
	
	// Compute and fix (if necessary) polygon normals (reorder vertex)
  	vtkPolyDataNormals *fixNormals = vtkPolyDataNormals::New();
  		fixNormals->SetInput( final );
  		fixNormals->ConsistencyOn();  		
  		fixNormals->SetFeatureAngle(1000.0f);  // All angles...	
  		// Check normal direction and flip normals if necessary (if normals points outsid)   
    	if( this->IsNormalsToOutside( final ) )
    		fixNormals->FlipNormalsOn();
  		fixNormals->Update();  	
  	    
  	// Copy fixed polygon to the final grid    
	vtkUnstructuredGrid *grid = vtkUnstructuredGrid::New();
		grid->SetCells(fixNormals->GetOutput()->GetCellType(0), fixNormals->GetOutput()->GetPolys());
		grid->SetPoints(fixNormals->GetOutput()->GetPoints());
		grid->Update();
	    
	// Converting PolyData to UnstructuredGrid (!!!!old function, without normals correction)
	//	vtkUnstructuredGrid *grid = vtkUnstructuredGrid::New();
	//		grid->SetCells(final->GetCellType(0), final->GetPolys());
	//		grid->SetPoints(final->GetPoints());
	//		grid->Update();
	//	cout << *grid << endl;
		
	// Converte o input de entrada (polydata) na superfície de saída (sur)
	Output->CopyUnstructuredGridToMeshDataSurface(grid);
	
	// Atualiza Grid interno
	Output->SetOutputAsSurface();
	
	// Removing objects
	fixNormals->Delete();
	Clipped->Delete();
	polyClean->Delete();
	grid->Delete();
	append->Delete();
	sideA->Delete();
	sideB->Delete();
	MascSphere->Delete();
	MainMesh->Delete();
	intersection->Delete();
	boarderTri->Delete();
	ClippedArea->Delete();
	ClipPolyData->Delete();
	BiteSphere->Delete();
	Poly->Delete();
		
	return 1;
}

