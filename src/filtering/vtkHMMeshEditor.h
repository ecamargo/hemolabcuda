	/*
	 * $Id$
	 */
	/*=========================================================================
	
	  Project:   HeMoLab
	  Module:    vtkHMMeshEditor
	
	  Author: Rodrigo L. S. Silva
	  			 Eduardo Camargo
	  			 Vincius Pêssoa
	
	=========================================================================*/
	// .NAME vtkHMMeshEditor
	// .SECTION Description
	// Merge two UnstructuredGrid into one. 
	
	#ifndef __vtkHMMeshEditor_h
	#define __vtkHMMeshEditor_h
	
	#include "vtkPolyDataAlgorithm.h"
	#include "vtkUnstructuredGridAlgorithm.h"
	
	class vtkImplicitFunction;
	class vtkSphere;
	class vtkMeshData;
	
	class VTK_EXPORT vtkHMMeshEditor : public vtkUnstructuredGridAlgorithm
	{
	public:
		static vtkHMMeshEditor *New();
		vtkTypeRevisionMacro(vtkHMMeshEditor, vtkUnstructuredGridAlgorithm);
		void PrintSelf(ostream& os, vtkIndent indent);
		int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);
		virtual void Modified();
		
		// Description:
		//Clean PolyData using vtkCleanPolyData and call BuildLinks() and BuildCells() PolyData`s methods.
		void CleanPoly(vtkPolyData* input);
		
		// Description:
		//It verifies the vectors board1Vec and board2Vec and return false if they are in the same order.
		bool CheckVectorDirection(vtkPolyData *poly, int *board1Vec, int *board2Vec, int board1Points, int board2Points);
		
		// Description:
		//Inverts order of vector boardVec keeping position the first element (position [0]) 
		void InvertVectorDirection(int *boardVec, int sizeVec);
		
		// Description:
		// Find point nearest the center and set initial coordinates (X, Y, Z) for spherewidget	
		void SetResolutionModifier(int value);
		
		// Description:
		// Find point nearest the center and set initial coordinates (X, Y, Z) for spherewidget
		void SetInitialPlaceWidget();	  
		
		// Description:
		// Get initial coordinate X for spherewidget
		double GetXInitialPlaceWidget();
	
		// Description:
		// Get initial coordinate Y for spherewidget
		double GetYInitialPlaceWidget();
		
		// Description:
		// Get initial coordinate Z for spherewidget
		double GetZInitialPlaceWidget();
		
		// Description:
		// Set Implicit Function`s center
		void SetClipCenter(double PassedCenter[3]);
	  
		// Description:
		// Get Implicit Function`s center
		double *GetClipCenter();
	  
		// Description:
		// Set Implicit Function`s radius
		void SetClipRadius(double PassedRadius);
	  
		// Description:
		// Get Implicit Function`s radius
		double GetClipRadius();
	  
		// Description:
		// Set Implicit Function
		void SetClipFunction(vtkImplicitFunction *PassedImplicitFunction);
	  
		// Description:
		// Alternating it enters the sides of inside and outside of the Sphere
		void SetInsideOut(int PassedInsideOut);
		
		// Description:
		//Return "true" if PolyData has cells with two, or less, neigbhors
		bool SewPolyDataAgain(vtkPolyData *poly, double *bounds);
		
		// Description:
		//Set scalar value for cells in the board of Polydata
		void SetScalarsBoard(vtkPolyData* poly, int value);
		
		// Description:
		// Set scalar value (value) in the polydata (poly) cells   
		void SetScalarsCells(vtkPolyData* poly, int value);
			  
		///////////////////////////////////////////////////////////////////////////
		//Rodrigo's Functions
	  
		// Description:
		// Get Closest Valid Point based on distance
		int GetClosestValidPoint(int reference, vtkPolyData* poly1, int *poly1Vec);
	  
		// Description:
		// Estimate the sphere resolution based on the input 'poly' resolution
		int EstimateSphereResolution(vtkPolyData *poly, vtkSphere *implicitSphere);
	  
		// Description:
		// Find Board Cells. A Board Cell has at least one neighbor that is NULL
		void FindBoardCells(vtkPolyData *clip, vtkPolyData *boarderTri);
	  
		// Description:
		// Search the triangles of the sphereSource that are near de polydata Board
		// Result is stored in the "sphereintersection" polydata
		void FindIntersectionBoard(vtkPolyData *sphereintersection, vtkPolyData *boarder, vtkPolyData *sphere, int *boarderVec);
	  
		// Description:
		// Search for a valid triangle seed (boarderVec 'id' != 1)
		// and calls recursive function to find its neighbors
		void FindSideA(vtkPolyData *sideA, vtkPolyData *sphere, int *boarderVec);
	  
		// Description:
		// Copy cells from 'input' to 'output'. Cell ids are stored in the 'idList'
		void SetPolyData(vtkPolyData *output, vtkPolyData *input, vtkIdList *idlist, int numCells);
	  
		// Description:
		// Recursive function that search and finds neighbors based on the 'seed' parameter
		void FindNeighbors(vtkPolyData *poly, int *boarderVec, int seed);
	  
		// Description:
		// Find Board Points
		void FindBoardPoints(vtkPolyData *poly, int *idVec);
	  
		// Description:
		// Compute the average coord of a cell (triangle) with 'ids' points
		void GetAverageCoord(vtkPoints *points, vtkIdList* ids, double *average);
	  
		// Description:
		// Compute Euclidean distance between two points
		double GetDistance(double *p1, double *p2);  
	  
		// Description:
		// Returns the size of the greater polydata's edge
		double GetGreaterEdgeSize(vtkPolyData *poly);
	  
		// Description:
		// Computes Polydata Average Area
		double GetPolyDataAverageArea(vtkPolyData *poly);
	  
		// Description:
		// Computes triangle's average area
		double GetTriangleArea(double p[][3]);
	  
		// Description:
		// Sorting polydata. Basically this procedure takes the board points
		// and sort them, creating a piecewise linear curve.
		void SortVector(vtkPolyData* poly, int *board, int *numPoints);
	  
		// Description:
		// Compute point average coord
		void ComputeAverageCoord(double *result, double *p1, double *p2);
	  
		// Description:
		// Set scalar value (value) in the polydata (poly) points
		void SetScalars(vtkPolyData* poly, int value);
	  
		// Description:
		// Sew (costura) the two polydatas
		void SewPolyData(vtkPolyData *poly, double *bounds);
	  
		// Description:
		// Check initial propagation direction
		// Pic the dominant Axis (x, y or z) and use it to drive the propagation
		bool checkDirection(vtkPolyData* poly, int p1index, int p2index);
		
		// Description:
		// Check if normals are pointed inside or outside
		bool IsNormalsToOutside(vtkPolyData *final);
		///////////////////////////////////////////////////////////////////////////
	  
	protected:
		vtkHMMeshEditor();
		~vtkHMMeshEditor();
	
		double ClipRadius;
		double ClipCenter[3];
		int InsideOut;
		double PointNearCenter[3];
		
		double ResolutionModifier;
	
	
		// Description:
		// Usual data generation method
		virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	
	private:
		vtkHMMeshEditor(const vtkHMMeshEditor&);  // Not implemented.
		void operator=(const vtkHMMeshEditor&);  // Not implemented.
	};
	
	#endif
