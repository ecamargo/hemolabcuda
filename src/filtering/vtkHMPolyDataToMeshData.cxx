/*
 * $Id$
 */

#include "vtkHMPolyDataToMeshData.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkMeshData.h"

vtkCxxRevisionMacro(vtkHMPolyDataToMeshData, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMPolyDataToMeshData);
//----------------------------------------------------------------------------
vtkHMPolyDataToMeshData::vtkHMPolyDataToMeshData()
{
}

//----------------------------------------------------------------------------
vtkHMPolyDataToMeshData::~vtkHMPolyDataToMeshData()
{
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMPolyDataToMeshData::GetOutput(int port)
{
  return vtkMeshData::SafeDownCast(this->GetOutputDataObject(port));
}


//----------------------------------------------------------------------------
int vtkHMPolyDataToMeshData::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkMeshData *output = vtkMeshData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
    
  // Testa se input e output são válidos
  if(!input) 	vtkErrorMacro("Input is NULL.");
 	if(!output) vtkErrorMacro("Output is NULL.");
  
  // Convert PolyData to UnstructuredGrid
  vtkUnstructuredGrid *grid = vtkUnstructuredGrid::New();
		grid->SetCells(input->GetCellType(0), input->GetPolys());
		grid->SetPoints(input->GetPoints());
		grid->Update();
  
  // Converte o input de entrada (polydata) na superfície de saída (sur)
  output->CopyUnstructuredGridToMeshDataSurface( grid );

  // Atualiza Grid interno
  output->SetOutputAsSurface();
  
  grid->Delete();

  return 1;
}


//----------------------------------------------------------------------------
void vtkHMPolyDataToMeshData::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHMPolyDataToMeshData::FillOutputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHMPolyDataToMeshData::FillInputPortInformation(
  int port,
  vtkInformation* info)
{
	// Seta tipo de entrada
  if(!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
  return 1;
}
