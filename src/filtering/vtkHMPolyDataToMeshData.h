/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMPolyDataToMeshData

  Author: Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMPolyDataToMeshData
// .SECTION Description
// Convert a vtkPolyData into a vtkMeshData object.

#ifndef __vtkHMPolyDataToMeshData_h
#define __vtkHMPolyDataToMeshData_h

#include "vtkDataSetAlgorithm.h"

class vtkDataSet;
class vtkMeshData;

class VTK_EXPORT vtkHMPolyDataToMeshData : public vtkDataSetAlgorithm
{
public:
  static vtkHMPolyDataToMeshData *New();
  vtkTypeRevisionMacro(vtkHMPolyDataToMeshData,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return an vtkMeshData Object as an output of this filter
	vtkMeshData* GetOutput(int port);

protected:
  vtkHMPolyDataToMeshData();
  ~vtkHMPolyDataToMeshData();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
	int FillOutputPortInformation(int, vtkInformation*);
private:
  vtkHMPolyDataToMeshData(const vtkHMPolyDataToMeshData&);  // Not implemented.
  void operator=(const vtkHMPolyDataToMeshData&);  // Not implemented.
};

#endif
