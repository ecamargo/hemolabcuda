/*
 * $Id$
 */

#include "vtkHMPolyDataToUnstructuredGrid.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkUnstructuredGrid.h"

vtkCxxRevisionMacro(vtkHMPolyDataToUnstructuredGrid, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMPolyDataToUnstructuredGrid);
//----------------------------------------------------------------------------
vtkHMPolyDataToUnstructuredGrid::vtkHMPolyDataToUnstructuredGrid()
{
}

//----------------------------------------------------------------------------
vtkHMPolyDataToUnstructuredGrid::~vtkHMPolyDataToUnstructuredGrid()
{
}

//----------------------------------------------------------------------------
vtkUnstructuredGrid* vtkHMPolyDataToUnstructuredGrid::GetOutput(int port)
{
  return vtkUnstructuredGrid::SafeDownCast(this->GetOutputDataObject(port));
}


//----------------------------------------------------------------------------
int vtkHMPolyDataToUnstructuredGrid::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkUnstructuredGrid *output = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
    
  // Testa se input e output são válidos
  if(!input) 	vtkErrorMacro("Input is NULL.");
 	if(!output) vtkErrorMacro("Output is NULL.");

	output->SetPoints( input->GetPoints() );  
	output->SetCells(VTK_TRIANGLE, input->GetPolys());  
	output->GetPointData()->SetScalars( input->GetPointData()->GetScalars() );

  return 1;
}


//----------------------------------------------------------------------------
void vtkHMPolyDataToUnstructuredGrid::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHMPolyDataToUnstructuredGrid::FillOutputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHMPolyDataToUnstructuredGrid::FillInputPortInformation(
  int port,
  vtkInformation* info)
{
	// Seta tipo de entrada
  if(!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
  return 1;
}
