/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMPolyDataToUnstructuredGrid

  Author: Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMPolyDataToUnstructuredGrid
// .SECTION Description
// Convert a vtkPolyData into a vtkMeshData object.

#ifndef __vtkHMPolyDataToUnstructuredGrid_h
#define __vtkHMPolyDataToUnstructuredGrid_h

#include "vtkDataSetAlgorithm.h"

class vtkDataSet;
class vtkUnstructuredGrid;

class VTK_EXPORT vtkHMPolyDataToUnstructuredGrid : public vtkDataSetAlgorithm
{
public:
  static vtkHMPolyDataToUnstructuredGrid *New();
  vtkTypeRevisionMacro(vtkHMPolyDataToUnstructuredGrid,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return an vtkMeshData Object as an output of this filter
	vtkUnstructuredGrid* GetOutput(int port);

protected:
  vtkHMPolyDataToUnstructuredGrid();
  ~vtkHMPolyDataToUnstructuredGrid();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
	int FillOutputPortInformation(int, vtkInformation*);
private:
  vtkHMPolyDataToUnstructuredGrid(const vtkHMPolyDataToUnstructuredGrid&);  // Not implemented.
  void operator=(const vtkHMPolyDataToUnstructuredGrid&);  // Not implemented.
};

#endif
