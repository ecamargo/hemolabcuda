/*
 * $Id$
 */

#include "vtkCellArray.h"
#include "vtkCell.h"
#include "vtkPoints.h"
#include "vtkIdList.h"
#include "vtkFieldData.h"
#include "vtkFloatArray.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkDataSetAttributes.h"
#include "vtkPointData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"

#include "vtkHMUnstructuredGridTo1DModel.h"
#include "vtkUnstructuredGrid.h"

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DMesh.h"

#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFile.h"
#include "vtkHMDataOut.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkHMUnstructuredGridTo1DModel, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMUnstructuredGridTo1DModel);

//----------------------------------------------------------------------------
vtkHMUnstructuredGridTo1DModel::vtkHMUnstructuredGridTo1DModel()
{
	//Configurando numero de entradas.
	this->SetNumberOfInputPorts(1);

	//Dados para o mesh.
	this->points = vtkPoints::New();
	this->coords = new double[3];

	this->mesh = vtkHM1DMesh::New();
	//  this->meshWriter = vtkHM1DMeshWriter::New();

	this->StraightModel = vtkHM1DStraightModel::New();

	this->DirichletsTag = vtkIntArray::New();
	this->Dirichlets = vtkDoubleArray::New();
	this->Dirichlets->SetNumberOfComponents(3);

	this->DirichletsValueTerminal = new double[3];
	this->DirichletsValueElement = new double[3];

	this->DirichletsValueTerminal[0] = 0.;
	this->DirichletsValueTerminal[1] = 0.;
	this->DirichletsValueTerminal[2] = 0.;

	this->DirichletsValueElement[0] = 0.;
	this->DirichletsValueElement[1] = 3.141593;
	this->DirichletsValueElement[2] = 0.;

	this->BasParam = NULL;

	this->Elastin = vtkDoubleArray::New();
	this->Elastin->SetName("Elastin");

	this->Collagen = vtkDoubleArray::New();
	this->Collagen->SetName("Collagen");

	this->CoefficientA = vtkDoubleArray::New();
	this->CoefficientA->SetName("CoefficientA");

	this->CoefficientB = vtkDoubleArray::New();
	this->CoefficientB->SetName("CoefficientB");

	this->Viscoelasticity = vtkDoubleArray::New();
	this->Viscoelasticity->SetName("Viscoelasticity");

	this->ViscoelasticityExponent = vtkDoubleArray::New();
	this->ViscoelasticityExponent->SetName("ViscoelasticityExponent");

	this->InfiltrationPressure = vtkDoubleArray::New();
	this->InfiltrationPressure->SetName("InfiltrationPressure");

	this->ReferencePressure = vtkDoubleArray::New();
	this->ReferencePressure->SetName("ReferencePressure");

	this->Permeability = vtkDoubleArray::New();
	this->Permeability->SetName("Permeability");
}

//----------------------------------------------------------------------------
vtkHMUnstructuredGridTo1DModel::~vtkHMUnstructuredGridTo1DModel()
{
	if (this->StraightModel)
		this->StraightModel->Delete();

	if (this->points)
		this->points->Delete();

	if (this->DirichletsTag)
		this->DirichletsTag->Delete();

	if (this->Dirichlets)
		this->Dirichlets->Delete();

	if (this->mesh)
		this->mesh->Delete();

	if (this->BasParam)
		this->BasParam->Delete();

	this->Elastin->Delete();
	this->Collagen->Delete();
	this->CoefficientA->Delete();
	this->CoefficientB->Delete();
	this->Viscoelasticity->Delete();
	this->ViscoelasticityExponent->Delete();
	this->InfiltrationPressure->Delete();
	this->ReferencePressure->Delete();
	this->Permeability->Delete();
}

//Procura por um ponto dento do vetor de incidências.
int vtkHMUnstructuredGridTo1DModel::SearchPoint(vtkCell* cell)
{
	int find = 0;
	ListOfInt::iterator it;
	ListOfInt::iterator meshListIt;

	int centralPoint;

	for (int i=0; i < this->listOfCentralPoints.size(); i++)
		{
		it = this->listOfCentralPoints.at(i).begin();

		while (it != this->listOfCentralPoints.at(i).end())
			{
			//Se o primeiro ponto da célula é igual ao ponto central no grid.
			if (cell->GetPointId(0) == *it)
				{
				it++;
				centralPoint = *it; //Ponto central da bifurcação no mesh.
				it--;

				//Fazendo busca pelo nó central criado para a estrutura do mesh.
				for (int a=0; a < meshIncidenceVector.size(); a++)
					{
					meshListIt = meshIncidenceVector.at(a).begin();

					while (meshListIt != meshIncidenceVector.at(a).end())
						{
						if (centralPoint == *meshListIt)
							{
							cell->GetPoints()->GetPoint(0, this->coords);
							this->Point1 = this->points->InsertNextPoint(this->coords);
							meshIncidenceVector.at(a).push_back((Point1+1));
							}

						meshListIt++;
						}
					}
				find = 1;
				}

			it++;
			}
		}

	return find;
}

void vtkHMUnstructuredGridTo1DModel::SetTerminalData(vtkHM1DTerminal *Term,
    int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetTerminalData.");

	vtkHM1DTerminal::ResistanceValuesList r1;
	vtkHM1DTerminal::ResistanceTimeList r1Time;
	vtkHM1DTerminal::ResistanceValuesList r2;
	vtkHM1DTerminal::ResistanceTimeList r2Time;
	vtkHM1DTerminal::CapacitanceValuesList c;
	vtkHM1DTerminal::CapacitanceTimeList cTime;
	vtkHM1DTerminal::PressureValuesList p;
	vtkHM1DTerminal::PressureValuesList pTime;

	//Valores default para o Param.
	vtkDebugMacro(<<"Setting default data.");
	//Setando a resistencia 1
	r1Time.push_back(0);
	r1.push_back(0);

	//Setando a resistencia 2
	r2Time.push_back(0);
	r2.push_back(0);

	//Setando a capacitancia
	cTime.push_back(0);
	c.push_back(0);

	//Setando a pressão
	pTime.push_back(0);
	p.push_back(0);

	Term->SetR1(&r1);
	Term->SetR1Time(&r1Time);
	Term->SetR2(&r2);
	Term->SetR2Time(&r2Time);
	Term->SetC(&c);
	Term->SetCTime(&cTime);
	Term->SetPt(&p);
	Term->SetPtTime(&pTime);
}

//----------------------------------------------------------------------------
// Set element data
void vtkHMUnstructuredGridTo1DModel::SetElement(vtkHM1DSegment *Segm,
    int numElements, int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetElementData.");

	vtkHMElementData *elem = new vtkHMElementData();

	vtkDebugMacro(<<"Setting default data.");
	//	elem->Elastin = 8000000;
	//	elem->Collagen = 0;
	//	elem->CoefficientA = 1;
	//	elem->CoefficientB = 1;
	//	elem->Viscoelasticity = 44400;
	//	elem->ViscoelasticityExponent = 1;
	//	elem->InfiltrationPressure = 0;
	//	elem->ReferencePressure = 100000;
	//	elem->Permeability = 0;
	elem->idNode[0] = numElements-1;
	elem->idNode[1] = numElements;
	//	elem->WallThickness = 0;

	Segm->SetNumberOfElements(numElements);
	Segm->SetElementData(numElements-1, *elem);
}

//----------------------------------------------------------------------------
// Set segment data
void vtkHMUnstructuredGridTo1DModel::SetSegmentData(vtkHM1DSegment *Segm,
    int numElements, int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetSegmentData.");
	int sum = 0;

	vtkHMNodeData *node = new vtkHMNodeData();
	double *point;

	point = this->mesh->GetPoints1D(elemento);
	node->coords[0] = *point;
	node->coords[1] = *++point;

	node->coords[2] = *++point;
	node->meshID = elemento +1;

	vtkDebugMacro(<<"Setting default data.");
	//	node->area = 3.141593;
	//	node->alfa = 0.3141593;
	//	node->ResolutionArray = NULL;

	Segm->SetNodeData(numElements, *node);

	delete node;
}

//----------------------------------------------------------------------------
// Set data
void vtkHMUnstructuredGridTo1DModel::SetDataTwo(vtkHM1DSegment *Segm,
    vtkHMUnstructuredGridTo1DModel::VectorOfIntList *vet,
    ListOfInt::iterator it2, int *group)
{
	vtkDebugMacro(<<"SetDataTwo.");

	int numElements=0;

	ListOfInt lst, lst2;
	ListOfInt::iterator itv;

	int *elementT = this->mesh->GetElementType();

	int j=*group;
	//Se o segmento possuir somente um elemento
	if ( (elementT[j] == 1) && (elementT[j+1] == 2 ))
		{
		numElements++;

		//Seting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		lst2 = vet->at(j);
		it2 = lst2.begin();
		it2++;
		}

	else
		{
		while (elementT[j+1] != 2)
			{

			lst = vet->at(j);

			while (*lst.begin() != *it2)
				{
				j++;
				lst = vet->at(j);

				itv = lst.begin();
				itv++;
				}//End while(*lst.begin() != *it2)

			numElements++;
			//Seting data of the node
			this->SetElement(Segm, numElements, *it2-1, j);
			this->SetSegmentData(Segm, numElements-1, *it2-1, j);

			lst2 = vet->at(j);
			it2 = lst2.begin();
			it2++;
			} //End while ( elementType[j+1] != 4 )
		}
	//Seting data of the node
	this->SetSegmentData(Segm, numElements, *it2-1, j);
	*group=j;

}

//----------------------------------------------------------------------------
// Set data inverse
void vtkHMUnstructuredGridTo1DModel::SetDataInverseTwo(vtkHM1DSegment *Segm,
    vtkHMUnstructuredGridTo1DModel::VectorOfIntList *vet,
    ListOfInt::iterator it2, int *group)
{
	vtkDebugMacro(<<"SetDataInverseTwo.");

	int numElements=0;

	ListOfInt lst, lst2;
	ListOfInt::iterator itv;

	int *elementT = this->mesh->GetElementType();

	int j=*group;

	while (elementT[j] != 2)
		{
		lst = vet->at(j);

		j--;
		numElements++;
		//Seting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		lst2 = vet->at(j);
		it2 = lst2.begin();
		it2++;
		} //End while ( elementType[j+1] != 4 )

	it2--;

	//Seting data of the node
	this->SetSegmentData(Segm, numElements, *it2-1, j);
}

//----------------------------------------------------------------------------
//Generate StraightModel with two types of element
int vtkHMUnstructuredGridTo1DModel::GenerateStraightModelTwo(int elemGroup,
    vtkHM1DTreeElement *root)
{
	vtkDebugMacro(<<"Generating StraightModel with two types of element.");

	//float progress;

	vtkHM1DSegment *Segm = NULL;
	vtkHM1DTerminal *Term = NULL;
	vtkHM1DTreeElement *elem;

	vtkHMUnstructuredGridTo1DModel::VectorOfIntList *vet;
	vet = this->mesh->GetGroupElements();

	vtkHMUnstructuredGridTo1DModel::ListOfInt lst, lst2;
	ListOfInt::iterator it, it2;

	int *elementType = this->mesh->GetElementType();

	int numElements = 0;

	if (!root)
		{
		//Create root (heart)
		root = (vtkHM1DTerminal *)this->StraightModel->Get1DTreeRoot();

		//Adding segment child of the root
		this->StraightModel->AddSegment(root->GetId());
		elem = root->GetFirstChild();
		Segm = this->StraightModel->GetSegment(elem->GetId());

		lst2 = vet->at(elemGroup);
		it2 = lst2.begin();

		//Setting data of the terminal
		this->SetTerminalData(this->StraightModel->GetTerminal(root->GetId()), *it2
		    -1, elemGroup);

		it2++;
		int j=elemGroup+1;
		//Adding node to segment

		this->SetDataTwo(Segm, vet, it2, &j);

		this->GenerateStraightModelTwo(j+1, elem);
		} //End if(!root)

	else
		{
		lst2 = vet->at(elemGroup);
		it = lst2.begin();

		//Seting data of the terminal
		Term = this->StraightModel->GetTerminal(root->GetFirstChild()->GetId());
		this->SetTerminalData(Term, *it-1, elemGroup);

		it++;

		//If has child
		if (lst2.size() > 2)
			{
			//			elem = root->GetFirstChild();
			//Adding segments childs
			for (unsigned int i=0; i<lst2.size()-2; i++)
				{
				it++;
				//				this->StraightModel->AddSegment(root->GetId());
				this->StraightModel->AddSegment(Term->GetId());

				//				elem = root->GetNextChild();
				elem = Term->GetNextChild();

				int j=elemGroup+1;
				it2 = it;
				ListOfInt::iterator itv;

				lst = vet->at(j);
				itv = lst.begin();

				int status=1;
				while (status)
					{
					if (*itv == *it2)
						{
						this->SetDataTwo(this->StraightModel->GetSegment(elem->GetId()),
						    vet, it2, &j);
						status=0;
						}

					else
						{
						itv++;
						if (*itv == *it2)
							{
							this->SetDataInverseTwo(
							    this->StraightModel->GetSegment(elem->GetId()), vet, it2, &j);
							status=0;
							}
						}
					j++;
					lst = vet->at(j);
					itv = lst.begin();

					}//End while(*lst.begin() != *it2)

				this->GenerateStraightModelTwo(j, elem);
				} //End for ( int i=0; i<lst2.size()-2; i++ )

			} //End if(lst2.size() > 2)

		} //End else

	return 1;
} //End GenerateStraightModelTwo()


//Manipula apenas o primeiro segmento do grid, com a finalidade de obter informação do mesmo.
void vtkHMUnstructuredGridTo1DModel::MakeInformationOfFirstSegment(vtkCell* cell)
{
	for (int i=0; i < cell->GetNumberOfPoints(); i++)
		{
		//Se for o primeiro ponto da célula.
		if (i == 0)
			{
			cell->GetPoints()->GetPoint(0, this->coords);
			meshList.push_back((this->points->InsertNextPoint(this->coords)+1));
			meshList.push_back((this->points->InsertNextPoint(this->coords)+1));
			meshIncidenceVector.push_back(meshList);
			meshList.clear();

			elementTypeVector.push_back(FILTER_TERMINAL); //Inserindo um terminal na lista que guarda os tipos de elementos.
			this->Dirichlets->InsertNextTuple3(this->DirichletsValueTerminal[0],
			    this->DirichletsValueTerminal[1], this->DirichletsValueTerminal[2]);
			this->Dirichlets->InsertNextTuple3(this->DirichletsValueElement[0],
			    this->DirichletsValueElement[1], this->DirichletsValueElement[2]);
			}

		//Se for o último ponto da célula.
		if (i == (cell->GetNumberOfPoints()-1))
			{
			cell->GetPoints()->GetPoint(i, this->coords);
			this->Point1 = (this->points->InsertNextPoint(this->coords) +1);
			meshList.push_back(this->Point1-1);
			meshList.push_back(this->Point1);
			meshIncidenceVector.push_back(meshList);
			meshList.clear();

			elementTypeVector.push_back(FILTER_ELEMENT); //Inserindo um elemento na lista que guarda os tipos de elementos.
			this->Dirichlets->InsertNextTuple3(this->DirichletsValueElement[0],
			    this->DirichletsValueElement[1], this->DirichletsValueElement[2]);

			this->Point2 = (this->points->InsertNextPoint(this->coords) +1);
			meshList.push_back(this->Point2);
			meshList.push_back(this->Point1);
			meshIncidenceVector.push_back(meshList);
			meshList.clear();

			elementTypeVector.push_back(FILTER_TERMINAL); //Inserindo um elemento na lista que guarda os tipos de elementos.
			this->Dirichlets->InsertNextTuple3(this->DirichletsValueTerminal[0],
			    this->DirichletsValueTerminal[1], this->DirichletsValueTerminal[2]);

			this->centralPoints.push_back(cell->GetPointId(i));
			this->centralPoints.push_back(Point2);
			this->listOfCentralPoints.push_back(this->centralPoints);
			this->centralPoints.clear();
			}

		//Pontos intermediários.
		if ((i > 0) && (i != (cell->GetNumberOfPoints()-1)))
			{
			cell->GetPoints()->GetPoint(i, this->coords);
			this->Point1 = (this->points->InsertNextPoint(this->coords)+1);
			meshList.push_back(this->Point1-1);
			meshList.push_back(this->Point1);
			meshIncidenceVector.push_back(meshList);
			meshList.clear();

			elementTypeVector.push_back(FILTER_ELEMENT); //Inserindo um elemento na lista que guarda os tipos de elementos.
			this->Dirichlets->InsertNextTuple3(this->DirichletsValueElement[0],
			    this->DirichletsValueElement[1], this->DirichletsValueElement[2]);
			}
		}
}//Fim do método MakeInformationOfFirstSegment


//Manipula os segmentos seguintes.
void vtkHMUnstructuredGridTo1DModel::MakeInformationOfSegment(vtkCell* cell)
{
	int find = 0;

	find = this->SearchPoint(cell);

	//Se o primeiro ponto é intersecção com outra célula.
	//Significa célula de saída de bifurcação.
	if (find == 1)
		{
		for (int i=1; i < cell->GetNumberOfPoints(); i++)
			{
			//Se for o último ponto da célula.
			if (i == (cell->GetNumberOfPoints()-1))
				{
				cell->GetPoints()->GetPoint(i, this->coords);
				this->Point1 = (this->points->InsertNextPoint(this->coords)+1);
				meshList.push_back(this->Point1-1);
				meshList.push_back(this->Point1);
				meshIncidenceVector.push_back(meshList);
				meshList.clear();

				elementTypeVector.push_back(FILTER_ELEMENT); //Inserindo um terminal na lista que guarda os tipos de elementos.
				this->Dirichlets->InsertNextTuple3(this->DirichletsValueElement[0],
				    this->DirichletsValueElement[1], this->DirichletsValueElement[2]);

				this->Point2 = (this->points->InsertNextPoint(this->coords)+1);
				meshList.push_back(this->Point2);
				meshList.push_back(this->Point1);
				meshIncidenceVector.push_back(meshList);
				meshList.clear();

				elementTypeVector.push_back(FILTER_TERMINAL); //Inserindo um terminal na lista que guarda os tipos de elementos.
				this->Dirichlets->InsertNextTuple3(this->DirichletsValueTerminal[0],
				    this->DirichletsValueTerminal[1], this->DirichletsValueTerminal[2]);

				this->centralPoints.push_back(cell->GetPointId(i));
				this->centralPoints.push_back(Point2);
				this->listOfCentralPoints.push_back(this->centralPoints);
				this->centralPoints.clear();
				}
			//Nodes intermediários.
			else
				{
				cell->GetPoints()->GetPoint(i, this->coords);
				this->Point1 = (this->points->InsertNextPoint(this->coords)+1);
				meshList.push_back(this->Point1-1);
				meshList.push_back(this->Point1);
				meshIncidenceVector.push_back(meshList);
				meshList.clear();

				elementTypeVector.push_back(FILTER_ELEMENT); //Inserindo um terminal na lista que guarda os tipos de elementos.
				this->Dirichlets->InsertNextTuple3(this->DirichletsValueElement[0],
				    this->DirichletsValueElement[1], this->DirichletsValueElement[2]);
				}
			}
		}
}

//Cria uma instancia do vtkHM1DMesh e popula a mesma.
void vtkHMUnstructuredGridTo1DModel::GenerateOfMesh()
{
	int* ElementForGroup = new int[meshIncidenceVector.size()];

	//Populando o atributo SetElementForGroup da classe vtkHM1DMesh.
	for (int i=0; i < this->meshIncidenceVector.size(); i++)
		ElementForGroup[i] = this->meshIncidenceVector.at(i).size();

	this->mesh->SetElementForGroup(ElementForGroup);
	this->mesh->SetNumberOfElementGroups(this->meshIncidenceVector.size());
	this->mesh->SetDegreeOfFreedom(3);
	this->mesh->SetDimension(3);
	this->mesh->SetNumberOfPoints(this->points->GetNumberOfPoints());
	this->mesh->SetElementType(this->ElementType);
	this->mesh->SetElementMat(this->ElementMat);
	this->mesh->SetPoints(this->points);
	this->mesh->SetDirichletsTag(this->DirichletsTag);
	this->mesh->SetDirichlets(this->Dirichlets);
	this->mesh->SetGroupElements(&this->meshIncidenceVector);

	//Escrevendo o mesh em arquivo.
	//Este trecho deixou de ser usado pois tudo eh gerado em memória.
	//this->meshWriter->WriteMeshData(mesh, "/tmp");
}

//----------------------------------------------------------------------------
void vtkHMUnstructuredGridTo1DModel::BuildElementMat()
{
	// cria o vetor
	this->ElementMat = new int[this->meshIncidenceVector.size()];

	unsigned int i=0;

	while (i < this->meshIncidenceVector.size())
		{

		if (this->ElementType[i] == FILTER_TERMINAL)
			{
			if (!i)
				this->ElementMat[i]=i+1;
			else
				this->ElementMat[i]=this->ElementMat[i-1]+1;
			}

		if (this->ElementType[i] == FILTER_ELEMENT)
			{
			this->ElementMat[i] = this->ElementMat[i-1]+1;
			}
		i++;
		}
}
//-----------------------------------------------------------------------------


//----------------------------------------------------------------------------
void vtkHMUnstructuredGridTo1DModel::BuildElementType()
{
	//Povoando a lista de tipos de elemento.
	this->ElementType = new int[elementTypeVector.size()];

	for (int i=0; i < elementTypeVector.size(); i++)
		this->ElementType[i] = this->elementTypeVector[i];
}

//----------------------------------------------------------------------------
void vtkHMUnstructuredGridTo1DModel::BuildDirichletsTag()
{
	int* n = new int[3];
	n[0] = 0;
	n[1] = 0;
	n[2] = 0;

	for (int i=0; i < this->points->GetNumberOfPoints(); i++)
		this->DirichletsTag->InsertNextTupleValue(n);
}
//----------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------------------------------------
void vtkHMUnstructuredGridTo1DModel::MakeStraightModelFromUnstructureGrid(
    vtkUnstructuredGrid* grid)
{
	//Referências auxiliares.
	vtkCell* cell;
	vtkCellArray* cellArray = vtkCellArray::New();
	vtkIdList* cellIds = vtkIdList::New();

	//Percorrendo as celulas do vtkUnstructuredGrid.
	for (int cellId=0; cellId < grid->GetNumberOfCells(); cellId++)
		{
		cell = grid->GetCell(cellId); //Obtendo a célula com id cellId.

		if (cellId == 0) //Tratando o primeiro segmento.
			this->MakeInformationOfFirstSegment(cell);

		else
			//Tratando os segmentos restantes.
			this->MakeInformationOfSegment(cell);

		}//Fim do loop de células.

	//Criação do Mesh.
	this->BuildElementType();
	this->BuildElementMat();
	this->BuildDirichletsTag();
	this->GenerateOfMesh();

	//Deletando referências auxiliares.
	cellArray->Delete();
	cellIds->Delete();
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHMUnstructuredGridTo1DModel::GetOutput(int port)
{
	return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(port));
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridTo1DModel::RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **inputVector,
    vtkInformationVector *outputVector)
{
	// Pega informação dos objetos
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// Pega input e output padrão
	vtkUnstructuredGrid
	    *input =
	        vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));

	vtkHM1DStraightModelGrid
	    *output =
	        vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	// Testa se input e output são válidos
	if (!input)
		vtkErrorMacro("Input is NULL.");
	if (!output)
		vtkErrorMacro("Output is NULL.");

	//  output->CopyStructure(input);

	this->MakeStraightModelFromUnstructureGrid(input);

	vtkHM1DTreeElement *elem = NULL;

	//Generate Straight Model with two element types
	if (!this->GenerateStraightModelTwo(0, elem))
		{
		vtkErrorMacro(<<"Error generating StraightModel with two element types.");
		return 0;
		}

	output->SetStraightModel(this->StraightModel);
	output->CreateStraightModelGrid();
	//  this->CreateStraightModelGrid(output);

	return 1;
}

//----------------------------------------------------------------------------
void vtkHMUnstructuredGridTo1DModel::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridTo1DModel::FillOutputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
	// Seta tipo da saída
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
	return 1;
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridTo1DModel::FillInputPortInformation(int port,
    vtkInformation* info)
{
	// Seta tipo de entrada
	if (!this->Superclass::FillInputPortInformation(port, info))
		return 0;

	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
	return 1;
}
