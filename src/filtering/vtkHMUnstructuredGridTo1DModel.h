/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMUnstructuredGridTo1DModel

  Author: Jan Palach

=========================================================================*/
// .NAME vtkHMUnstructuredGridTo1DModel
// .SECTION Description
//This filter give a input(vtkUnstructuredGrid), and make a 1DModel,
//generating in your output an vtkHM1DStraightModelGrid.
// Convert a vtkUnstructuredGrid into a 1DModel.

#ifndef __vtkHMUnstructuredGridTo1DModel_h
#define __vtkHMUnstructuredGridTo1DModel_h

#define FILTER_TERMINAL 2
#define FILTER_ELEMENT 1

#include "vtkDataSetAlgorithm.h"

#include <map>
#include <list>
#include <vector>

class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM1DIniFile;
class vtkHM1DSegment;
class vtkHM1DMesh;
class vtkHM1DTerminal;
//class vtkHM1DMeshWriter;
class vtkHMDataOut;
class vtkHM1DTreeElement;
class vtkHM1DStraightModel;
class vtkHM1DStraightModelGrid;

class vtkDataSet;
class vtkUnstructuredGrid;
class vtkCell;
class vtkCellArray;
class vtkFloatArray;
class vtkPoints;
class vtkIntArray;
class vtkDoubleArray;

class VTK_EXPORT vtkHMUnstructuredGridTo1DModel : public vtkDataSetAlgorithm
{
public:
//BTX
  //Armazenar a incidência que vai para o mesh.
  typedef std::list<vtkIdType> ListOfInt;
  typedef std::vector<ListOfInt> VectorOfIntList;
  typedef std::vector<int> VectorOfInt;
//ETX

  static vtkHMUnstructuredGridTo1DModel *New();
  vtkTypeRevisionMacro(vtkHMUnstructuredGridTo1DModel,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  //Description:
  //Make a StraightModel representation from vtkUnstructuredGrid.
  void MakeStraightModelFromUnstructureGrid(vtkUnstructuredGrid* grid);
   
  //Description:
  //Make information about first segment.
  void MakeInformationOfFirstSegment(vtkCell* cell);
	 
  //Description:
  //Get an information of the current segment on grid.
  void 	MakeInformationOfSegment(vtkCell* cell);
  
  //Description:
  //GenerateOfMesh(), make a mesh instance.
  void GenerateOfMesh();
  
  //BTX
  // Description:
	//Set data of the nodes of the segment
	void SetSegmentData(vtkHM1DSegment *Segm, int numElements, int elemento, int elemGroup);
  
  // Description:
	//Set data of the terminal
	void SetTerminalData(vtkHM1DTerminal *Term, int elemento, int elemGroup);
	
	// Description:
	//Set data of the elements of the segment
	void SetElement(vtkHM1DSegment *Segm, int numElements, int elemento, int elemGroup);
	
	// Description:
	//Set data of the segment for number of types of elements equal 2
	void SetDataTwo(vtkHM1DSegment *Segm, vtkHMUnstructuredGridTo1DModel::VectorOfIntList *vet, ListOfInt::iterator it2, int *group);
	
	// Description:
	//Set data of the segment for number of types of
	//elements equal 4 and the segment start in the
	//end of the espcification of the group elements
	void SetDataInverseTwo(vtkHM1DSegment *Segm, vtkHMUnstructuredGridTo1DModel::VectorOfIntList *vet, ListOfInt::iterator it2, int *group);
	
  // Description :
	//Genetate Straight Model with two element types
	int GenerateStraightModelTwo(int elemGroup, vtkHM1DTreeElement *root);
	
  //ETX
  
  //Description:
  //Make a search of a cellPoints on Lists.
  int SearchPoint(vtkCell* cell);
  
  // Description:
  // Return a vtkHM1DStraightModelGrid...
  vtkHM1DStraightModelGrid* GetOutput(int port);
  
  //Description:
  //Make ElementType array.
  void BuildElementType();
  
  //Description:
  //Make ElementMat array.
  void BuildElementMat();
  
  //Description:
  //Make a DirichletsTag.
  void BuildDirichletsTag();
  
protected:
//BTX
  VectorOfIntList meshIncidenceVector;
  VectorOfIntList listOfCentralPoints;
  
  ListOfInt meshList;
  ListOfInt centralPoints;
  VectorOfInt elementTypeVector;
  
 	ListOfInt::iterator meshListIt;
 	VectorOfIntList::iterator meshIt; 
 	
 	vtkIdType Point1, Point2, Point3, Point4;
//ETX

  vtkHM1DMesh* mesh;
//  vtkHM1DMeshWriter* meshWriter;
  
  vtkHM1DStraightModel* StraightModel;

  vtkPoints* points;
  vtkPoints* Points;
  
  vtkIntArray* DirichletsTag;
  vtkDoubleArray* Dirichlets;
  
  double* DirichletsValueTerminal;
  double* DirichletsValueElement;
  
  int *ElementType;
  int *ElementMat;

  double* coords;
  double progress;
  
	// Description:
	//Object for read data of the BasParam.txt file
	vtkHM1DBasParam *BasParam;
	
	// Description:
	// Contains values to Elastin for each element of the segment 
	// of the tree
	vtkDoubleArray *Elastin;
	
	// Description:
	// Contains values to Collagen for each element of the segment 
	// of the tree
	vtkDoubleArray *Collagen;
	
	// Description:
	// Contains values to CoefficientA for each element of the segment 
	// of the tree
	vtkDoubleArray *CoefficientA;
	
	// Description:
	// Contains values to CoefficientB for each element of the segment 
	// of the tree
	vtkDoubleArray *CoefficientB;
	
	// Description:
	// Contains values to Viscoelasticity for each element of the segment 
	// of the tree
	vtkDoubleArray *Viscoelasticity;
	
	// Description:
	// Contains values to Viscoelasticity Exponent for each element of the segment 
	// of the tree
	vtkDoubleArray *ViscoelasticityExponent;
	
	// Description:
	// Contains values to Infiltration Pressure for each element of the segment 
	// of the tree
	vtkDoubleArray *InfiltrationPressure;
	
	// Description:
	// Contains values to Reference Pressure for each element of the segment 
	// of the tree
	vtkDoubleArray *ReferencePressure;
	
	// Description:
	// Contains values to Permeability for each element of the segment 
	// of the tree
	vtkDoubleArray *Permeability;
  

  vtkHMUnstructuredGridTo1DModel();
  ~vtkHMUnstructuredGridTo1DModel();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
	int FillOutputPortInformation(int, vtkInformation*);
	
private:
  vtkHMUnstructuredGridTo1DModel(const vtkHMUnstructuredGridTo1DModel&);  // Not implemented.
  void operator=(const vtkHMUnstructuredGridTo1DModel&);  // Not implemented.
};

#endif
