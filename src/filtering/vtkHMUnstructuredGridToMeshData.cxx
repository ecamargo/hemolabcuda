/*
 * $Id$
 */

#include "vtkHMUnstructuredGridToMeshData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkMeshData.h"

vtkCxxRevisionMacro(vtkHMUnstructuredGridToMeshData, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMUnstructuredGridToMeshData);
//----------------------------------------------------------------------------
vtkHMUnstructuredGridToMeshData::vtkHMUnstructuredGridToMeshData()
{
}

//----------------------------------------------------------------------------
vtkHMUnstructuredGridToMeshData::~vtkHMUnstructuredGridToMeshData()
{
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMUnstructuredGridToMeshData::GetOutput(int port)
{
  return vtkMeshData::SafeDownCast(this->GetOutputDataObject(port));
}


//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToMeshData::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkMeshData *output = vtkMeshData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
    
  // Testa se input e output são válidos
  if(!input) 	vtkErrorMacro("Input is NULL.");
 	if(!output) vtkErrorMacro("Output is NULL.");
  
  // Converte o input de entrada (UnstructuredGrid) na superfície de saída (MeshData)
  output->CopyUnstructuredGridToMeshDataSurface( input );
  
  // Atualiza Grid interno
  output->SetOutputAsSurface();  

  return 1;
}


//----------------------------------------------------------------------------
void vtkHMUnstructuredGridToMeshData::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToMeshData::FillOutputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToMeshData::FillInputPortInformation(
  int port,
  vtkInformation* info)
{
	// Seta tipo de entrada
  if(!this->Superclass::FillInputPortInformation(port, info))
    return 0;
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
  return 1;
}
