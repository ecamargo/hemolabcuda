/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMUnstructuredGridToMeshData

  Author: Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMUnstructuredGridToMeshData
// .SECTION Description
// Convert a vtkUnstructuredGrid into a vtkMeshData object.

#ifndef __vtkHMUnstructuredGridToMeshData_h
#define __vtkHMUnstructuredGridToMeshData_h

#include "vtkDataSetAlgorithm.h"

class vtkDataSet;
class vtkMeshData;

class VTK_EXPORT vtkHMUnstructuredGridToMeshData : public vtkDataSetAlgorithm
{
public:
  static vtkHMUnstructuredGridToMeshData *New();
  vtkTypeRevisionMacro(vtkHMUnstructuredGridToMeshData,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return an vtkMeshData Object as an output of this filter
	vtkMeshData* GetOutput(int port);

protected:
  vtkHMUnstructuredGridToMeshData();
  ~vtkHMUnstructuredGridToMeshData();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
	int FillOutputPortInformation(int, vtkInformation*);
private:
  vtkHMUnstructuredGridToMeshData(const vtkHMUnstructuredGridToMeshData&);  // Not implemented.
  void operator=(const vtkHMUnstructuredGridToMeshData&);  // Not implemented.
};

#endif
