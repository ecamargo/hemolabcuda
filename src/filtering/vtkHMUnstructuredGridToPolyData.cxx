/*
 * vtkHMUnstructuredGridToPolyData.cxx
 *
 *  Created on: Oct 7, 2009
 *      Author: igor
 */

/*
 * $Id$
 */

#include "vtkHMUnstructuredGridToPolyData.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPolyData.h"

vtkCxxRevisionMacro(vtkHMUnstructuredGridToPolyData, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMUnstructuredGridToPolyData);
//----------------------------------------------------------------------------
vtkHMUnstructuredGridToPolyData::vtkHMUnstructuredGridToPolyData()
{
}

//----------------------------------------------------------------------------
vtkHMUnstructuredGridToPolyData::~vtkHMUnstructuredGridToPolyData()
{
}

//----------------------------------------------------------------------------
vtkPolyData* vtkHMUnstructuredGridToPolyData::GetOutput(int port)
{
  return vtkPolyData::SafeDownCast(this->GetOutputDataObject(port));
}

//----------------------------------------------------------------------------
vtkPolyData* vtkHMUnstructuredGridToPolyData::GetOutput()
{
  return this->GetOutput(0);
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToPolyData::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // Testa se input e output são válidos
  if(!input)  vtkErrorMacro("Input is NULL.");
  if(!output) vtkErrorMacro("Output is NULL.");

//  output->SetPoints( input->GetPoints() );
//  output->SetCells(VTK_TRIANGLE, input->GetPolys());
//  output->GetPointData()->SetScalars( input->GetPointData()->GetScalars() );

  output->SetPoints( input->GetPoints() );
  output->SetPolys(input->GetCells());
  output->GetPointData()->SetScalars( input->GetPointData()->GetScalars() );

  return 1;
}


//----------------------------------------------------------------------------
void vtkHMUnstructuredGridToPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToPolyData::FillOutputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHMUnstructuredGridToPolyData::FillInputPortInformation(int port, vtkInformation* info)
{
  if(!this->Superclass::FillInputPortInformation(port, info))
    {
    return 0;
    }
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
  return 1;
}
