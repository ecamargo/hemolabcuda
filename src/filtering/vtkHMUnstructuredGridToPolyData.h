/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMUnstructuredGridToPolyData

  Author: Igor Freitas

=========================================================================*/

// .NAME vtkHMUnstructuredGridToPolyData
// .SECTION Description
// Convert a vtkUnstructuredGrid into a vtkPolyData object.

#ifndef VTKHMUNSTRUCTUREDGRIDTOPOLYDATA_H_
#define VTKHMUNSTRUCTUREDGRIDTOPOLYDATA_H_

#include "vtkDataSetAlgorithm.h"

class vtkDataSet;
class vtkUnstructuredGrid;
class vtkPolyData;

class VTK_EXPORT vtkHMUnstructuredGridToPolyData : public vtkDataSetAlgorithm
{
public:
  static vtkHMUnstructuredGridToPolyData *New();
  vtkTypeRevisionMacro(vtkHMUnstructuredGridToPolyData,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Return an vtkPolyData Object as an output of this filter
  vtkPolyData* GetOutput(int port);

  // Description:
  // Return an vtkPolyData Object as an output of this filter
  vtkPolyData* GetOutput();

protected:
  vtkHMUnstructuredGridToPolyData();
  ~vtkHMUnstructuredGridToPolyData();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  virtual int FillInputPortInformation (int, vtkInformation*);

  // Description:
  // Sets output object type
  virtual int FillOutputPortInformation(int, vtkInformation*);

private:
  vtkHMUnstructuredGridToPolyData(const vtkHMUnstructuredGridToPolyData&);  // Not implemented.
  void operator=(const vtkHMUnstructuredGridToPolyData&);  // Not implemented.
};

#endif /* VTKHMUNSTRUCTUREDGRIDTOPOLYDATA_H_ */
