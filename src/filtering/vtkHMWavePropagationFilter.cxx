/*
 * $Id$
 */

#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkCell.h" 
#include "vtkPoints.h"
#include "vtkIdList.h"
#include "vtkFieldData.h"
#include "vtkFloatArray.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkDataSetAttributes.h"
#include "vtkPointData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include "vtkHMWavePropagationFilter.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DSegment.h"

#include <string>
#include <map>
#include <vector>
#include <sstream>


vtkCxxRevisionMacro(vtkHMWavePropagationFilter, "$Revision: 1.45 $");
vtkStandardNewMacro(vtkHMWavePropagationFilter);

//----------------------------------------------------------------------------
vtkHMWavePropagationFilter::vtkHMWavePropagationFilter()
{
	this->Pressure = vtkDoubleArray::New();
	this->Pressure->SetName("Pressure");
	
	this->Flow = vtkDoubleArray::New();
	this->Flow->SetName("Flow");
	
	this->Area = vtkDoubleArray::New();
	this->Area->SetName("Area");
	
	this->Velocity = vtkDoubleArray::New();
	this->Velocity->SetName("Velocity");
	
	this->TimeStep = vtkDoubleArray::New();
	this->TimeStep->SetName("Time Step");
	
	strcpy(this->path, "");
}

//----------------------------------------------------------------------------
vtkHMWavePropagationFilter::~vtkHMWavePropagationFilter()
{
	if(this->Pressure)
	  this->Pressure->Delete();
	
	if(this->Flow)
		this->Flow->Delete();
	
	if(this->Area)
	  this->Area->Delete();
	
	if(this->Velocity)
		this->Velocity->Delete();
	
	if(this->TimeStep)
		this->TimeStep->Delete();
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHMWavePropagationFilter::GetOutput(int port)
{
  	return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(port));
}


void vtkHMWavePropagationFilter::SelectScalar(char* arg)
{
	if(!strcmp(arg, "Pressure"))
		std::cout << "Pressure is selected!" << endl;
  
 	if(!strcmp(arg, "Flow"))
		std::cout << "Flow is selected!" << endl;
  
	if(!strcmp(arg, "Area"))
		std::cout << "Area is selected!" << endl;
  
	if(!strcmp(arg, "Velocity"))
		std::cout << "Velocity is selected!" << endl;
}

void vtkHMWavePropagationFilter::SetAnimationPath(char* pathArg)
{
	strcpy(this->path, pathArg);
	this->Modified();
} 


void vtkHMWavePropagationFilter::SaveSimulation(vtkHM1DStraightModelGrid* grid)
{	
	vtkHM1DStraightModel::TreeNodeDataMap segmentsMap;
	vtkHM1DStraightModel::TreeNodeDataMap::iterator iteratorMap;
	segmentsMap = grid->GetStraightModel()->GetSegmentsMap();
	vtkHM1DSegment* segment;
	vtkIdType* idType;
	  
	vtkDoubleArray* ResolutionArray;
	int NumberOfComponents;
	double* scalars;
	  
	vtkDoubleArray* TimeStepArray;
	TimeStepArray = grid->GetStraightModel()->GetInstantOfTime();
	
	int NumberOfPoints = grid->GetNumberOfPoints();
	int NumberOfCells  = grid->GetNumberOfCells();
	
	double *coords;
	vtkCell* cell;
	  
	int offsetActualValue = 0;
	char offSetValue[256];
	char filename[256];
	char intermediaryResult[256];
	const char* extension=".vtp";
	  
	this->Flow->SetNumberOfTuples(grid->GetNumberOfPoints());
	this->Pressure->SetNumberOfTuples(grid->GetNumberOfPoints());
	this->Area->SetNumberOfTuples(grid->GetNumberOfPoints());
	
	if (!TimeStepArray->GetNumberOfTuples())
	  {
	  vtkErrorMacro("Current 1D model contains no simulation result data.");
	  return;
	  
	  }
		  
	//Varrendo os instantes de tempo.
	for(int i = 0; i < TimeStepArray->GetNumberOfTuples(); i++)
	  {
	  //BTX
	  std::string result = "";
	  //ETX
	  
	  sprintf(filename, "%s%s%d%s", this->path, "/", i, extension);
	  
	  std::ofstream out(filename);
	       
	  out << "<?xml version=\"1.0\"?>\n";
	  out << "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">\n<PolyData>\n"; 
	  out << "<Piece NumberOfPoints=\"" << NumberOfPoints << "\" NumberOfLines=\"" << NumberOfCells << "\">";
	  out << "\n<Points>\n<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n";
	    
	  //Gravando coordenadas.
	  for(int j=0; j < NumberOfPoints; j++)
	    {
	    coords = grid->GetPoint(j);
	    sprintf(intermediaryResult, "%f %f %f ", coords[0], coords[1], coords[2]);
	    result.append(intermediaryResult);
	    }
	
	  result.append("\n</DataArray>\n</Points>\n");
	  
	  //Aqui insiro os escalares para cada ponto: flow, pressure, area, velocity.
	  result.append("<PointData Scalars=\"Point_scalars\">\n");
	  result.append("<DataArray type=\"Float32\" Name=\"Pressure\" format=\"ascii\">\n");
	    
	  //Gravando os escalares, fluxo, pressão, area, e velocitade.
	  //==============================================================
	  for(iteratorMap = segmentsMap.begin(); iteratorMap != segmentsMap.end(); iteratorMap++ )
			{
			segment = vtkHM1DSegment::SafeDownCast(iteratorMap->second);
			
			for(vtkIdType j=0; j < segment->GetNodeNumber(); j++)
			  {
			  ResolutionArray = segment->GetResolutionArray(j);
		
			  if(ResolutionArray->GetNumberOfComponents() == 3)
					{
					idType = grid->GetNodeInformation(iteratorMap->first, j);
					scalars = ResolutionArray->GetTuple3(i);
					sprintf(intermediaryResult, "%f ", scalars[2]);
				  result.append(intermediaryResult);
					}
					
			  //Componentes na seguinte sequencia: fluxo, pressão_total, pressão_comum, area.
			  else if(ResolutionArray->GetNumberOfComponents() == 4)
					{
					idType=grid->GetNodeInformation(iteratorMap->first, j);
				    scalars = ResolutionArray->GetTuple4(i);
					sprintf(intermediaryResult, "%f ", scalars[3]);
					result.append(intermediaryResult);
					}
								
			  }
			
			}
	    
    result.append("</DataArray>\n");
    
    
    //Gravando dados de fluxo.
    result.append("<DataArray type=\"Float32\" Name=\"Flow\" format=\"ascii\">\n");
    
    //Gravando os escalares, fluxo, pressão, area, e velocitade.
    //==============================================================
		for(iteratorMap = segmentsMap.begin(); iteratorMap != segmentsMap.end(); iteratorMap++ )
		  {
		  segment = vtkHM1DSegment::SafeDownCast(iteratorMap->second);
	
		  for(vtkIdType j=0; j < segment->GetNodeNumber(); j++)
				{
				ResolutionArray = segment->GetResolutionArray(j);
					
				if(ResolutionArray->GetNumberOfComponents() == 3)
				  {
				  idType = grid->GetNodeInformation(iteratorMap->first, j);
				  scalars = ResolutionArray->GetTuple3(i);
				  sprintf(intermediaryResult, "%f ", scalars[0]);
				  result.append(intermediaryResult);
				  }
					
			  //Componentes na seguinte sequencia: fluxo, pressão_total, pressão_comum, area.
			  else if(ResolutionArray->GetNumberOfComponents() == 4)
					{
					idType=grid->GetNodeInformation(iteratorMap->first, j);
					scalars = ResolutionArray->GetTuple4(i);
					sprintf(intermediaryResult, "%f ", scalars[0]);
					result.append(intermediaryResult);
					}
				}
		  }
	    
    result.append("</DataArray>\n");
    
    
    //Gravando dados de Area.
    result.append("<DataArray type=\"Float32\" Name=\"Area\" format=\"ascii\">\n");
    
    //Gravando os escalares, fluxo, pressão, area, e velocitade.
    //==============================================================
		for(iteratorMap = segmentsMap.begin(); iteratorMap != segmentsMap.end(); iteratorMap++ )
		  {
		  segment = vtkHM1DSegment::SafeDownCast(iteratorMap->second);
	
		  for(vtkIdType j=0; j < segment->GetNodeNumber(); j++)
				{
				ResolutionArray = segment->GetResolutionArray(j);
		
				if(ResolutionArray->GetNumberOfComponents() == 3)
				  {
				  idType = grid->GetNodeInformation(iteratorMap->first, j);
				  scalars = ResolutionArray->GetTuple3(i);
				  sprintf(intermediaryResult, "%f ", scalars[2]);
				  result.append(intermediaryResult);
				  }
					
				//Componentes na seguinte sequencia: fluxo, pressão_total, pressão_comum, area.
				else if(ResolutionArray->GetNumberOfComponents() == 4)
				  {
				  idType=grid->GetNodeInformation(iteratorMap->first, j);
				  scalars = ResolutionArray->GetTuple4(i);
				  sprintf(intermediaryResult, "%f ", scalars[3]);
				  result.append(intermediaryResult);
				  }
				}
		  }
	    
    result.append("</DataArray>\n");
    
    result.append("</PointData>\n");
      
    result.append("<CellData></CellData>\n<Lines>\n<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

    //Gravando conectividade.
    for(int cellId=0; cellId < grid->GetNumberOfCells(); cellId++)
      {
      cell = grid->GetCell(cellId);
   
      for(int i=0; i < cell->GetNumberOfPoints(); i++)
	     	{
	     	sprintf(intermediaryResult, "%d ", cell->GetPointId(i));
	     	result.append(intermediaryResult);
	     	}
     	}
     	
    result.append("\n</DataArray>\n<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");

    for(int cellId=0; cellId < grid->GetNumberOfCells(); cellId++)
      {
      offsetActualValue += 2;
      sprintf(offSetValue, "%d ", offsetActualValue);
      result.append(offSetValue);
      }
    	     	
    //Adicionando linha final a ser gravada. 
    result.append("\n</DataArray>\n</Lines>\n</Piece>\n</PolyData>\n</VTKFile>\n");

    out << result; 
    
    result = "";
    offsetActualValue = 0;
    out.close();   
	  }
}


void vtkHMWavePropagationFilter::CreatePropertiesGrid(vtkHM1DStraightModelGrid* grid){}


void vtkHMWavePropagationFilter::SetScalarsFromGrid(vtkHM1DStraightModelGrid* grid)
{
  vtkHM1DStraightModel::TreeNodeDataMap segmentsMap;
  vtkHM1DStraightModel::TreeNodeDataMap::iterator iteratorMap;
  segmentsMap = grid->GetStraightModel()->GetSegmentsMap();
  vtkHM1DSegment* segment;
  vtkIdType* idType;
  
  vtkDoubleArray* ResolutionArray;
  int NumberOfComponents;
  double* scalars;
  
  
  this->Flow->SetNumberOfTuples(grid->GetNumberOfPoints());
	this->Pressure->SetNumberOfTuples(grid->GetNumberOfPoints());
	this->Area->SetNumberOfTuples(grid->GetNumberOfPoints());
  
  //Varrendo o map de segmentos.
  for(iteratorMap = segmentsMap.begin(); iteratorMap != segmentsMap.end(); iteratorMap++ )
  	{
		segment = vtkHM1DSegment::SafeDownCast(iteratorMap->second);

    //std::cout << "ID DO SEGMENTO:::" << iteratorMap->first << endl;

		//Percorrendo os nodes do segmento.
		//Obtendo os resolution arrays.
		for(vtkIdType i=0; i < segment->GetNodeNumber(); i++)
			{
			ResolutionArray = segment->GetResolutionArray(i);
	
		  //Componentes na seguinte sequência:
		  //scalars[0] = fluxo
		  //scalars[1] = pressão_comum
		  //scalars[2] = area
			if(ResolutionArray->GetNumberOfComponents() == 3)
			  {
			  //Aqui na verdade devo iterar sobre os pontos do grid.
	//		  for(int i=0; i < grid->GetNumberOfPoints(); i++)
	//		  	{
	//		  	grid->
	//		  	}
				
				idType = grid->GetNodeInformation(iteratorMap->first, i);
				cout << "point: " << idType[0] << "\tnode: " << idType[1] << "\tseg: " << idType[2] << endl;
				//Preenchenco os vtkDoubleArrays de propriedades.
			  //for(vtkIdType tuple=0; tuple < ResolutionArray->GetNumberOfTuples(); tuple++)
			 // 	{
					scalars = ResolutionArray->GetTuple3(1);
					this->Flow->SetTuple1(idType[0], scalars[0]);
					this->Pressure->SetTuple1(idType[0], scalars[1]);
					this->Area->SetTuple1(idType[0], scalars[2]);
			  	//}
				}
			
			//Componentes na seguinte sequencia: fluxo, pressão_total, pressão_comum, area.
			else if(ResolutionArray->GetNumberOfComponents() == 4)
				{
				//Preenchenco os vtkDoubleArrays de propriedades.
			  //for(vtkIdType tuple=0; tuple < ResolutionArray->GetNumberOfTuples(); tuple++)
			  //	{
					idType=grid->GetNodeInformation(iteratorMap->first, i);
					cout << "point: " << idType[0] << "\tnode: " << idType[1] << "\tseg: " << idType[2] << endl;
					scalars = ResolutionArray->GetTuple4(0);
					this->Flow->SetTuple1(idType[0], scalars[0]);
					this->Pressure->SetTuple1(idType[0], scalars[2]);
					this->Area->SetTuple1(idType[0], scalars[3]);
			  //	}
				}
						
				}
		}
		
		
		//Preenchendo o grid com as propriedades.
		//Definindo tamanho dos arrays.
//		this->Pressure->SetNumberOfTuples(grid->GetNumberOfPoints());
//		this->Flow->SetNumberOfTuples(grid->GetNumberOfPoints());
//		this->Area->SetNumberOfTuples(grid->GetNumberOfPoints());
//		this->Velocity->SetNumberOfTuples(grid->GetNumberOfPoints());
		
		//Criando novos arrays de propriedades ao grid.
		//Pressure, Flow, Area, Velocity.
		grid->GetPointData()->AddArray(this->Pressure);
		grid->GetPointData()->AddArray(this->Flow);
		grid->GetPointData()->AddArray(this->Area);
		grid->GetPointData()->AddArray(this->Velocity);
		grid->GetPointData()->SetActiveAttribute("Pressure", vtkDataSetAttributes::SCALARS);
		
		grid->BuildCells();
		grid->BuildLinks();
		
		grid->Update();
}


//Terminar este método.
void vtkHMWavePropagationFilter::StartSimulation(vtkHM1DStraightModelGrid* grid)
{
	for(int i=0; i < 10000; i++)
		{
		vtkDoubleArray *pressure 	= vtkDoubleArray::SafeDownCast(grid->GetPointData()->GetArray("Pressure"));
		vtkDoubleArray *flow 			= vtkDoubleArray::SafeDownCast(grid->GetPointData()->GetArray("Flow"));
		vtkDoubleArray *area			= vtkDoubleArray::SafeDownCast(grid->GetPointData()->GetArray("Area"));
		
		//Trocando os valores das propriedades
		pressure->SetValue(i, (i/0.5));
	  flow->SetValue(i, (i/0.5));
	  area->SetValue(i, (i/0.5));
	  
	  pressure->Modified();
	  flow->Modified();
	  area->Modified();
	  
	  grid->Update();
		}
}


//void vtkHMWavePropagationFilter::MakeAnimation(vtkHM1DStraightModelGrid* grid)
//{
//	//int posicao = this->LineWidgetSelected;
//	
//	//Pegando os arrays do grid
//	vtkDoubleArray *Elastin 									= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Elastin"));
//	vtkDoubleArray *Collagen 									= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Collagen"));
//	vtkDoubleArray *CoefficientA							= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("CoefficientA"));
//	vtkDoubleArray *CoefficientB							= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("CoefficientB"));
//	vtkDoubleArray *Viscoelasticity						= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Viscoelasticity"));
//	vtkDoubleArray *ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("ViscoelasticityExponent"));
//	vtkDoubleArray *InfiltrationPressure			= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("InfiltrationPressure"));
//	vtkDoubleArray *ReferencePressure					= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("ReferencePressure"));
//	vtkDoubleArray *Permeability							= vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Permeability"));
//	
//	vtkDoubleArray* TimeStepArray;
//	TimeStepArray = grid->GetStraightModel()->GetInstantOfTime();
//	std::cout << "Número de intantes de tempo::: " << TimeStepArray->GetNumberOfTuples() << endl;
//	std::cout << "Elastin::: " << Elastin->GetNumberOfTuples() << endl;
//	
////	//Trocando os valores das propriedades
////	Elastin->SetValue(posicao, array->GetValue(0));
////  Collagen->SetValue(posicao, array->GetValue(1));
////  CoefficientA->SetValue(posicao, array->GetValue(2));
////  CoefficientB->SetValue(posicao, array->GetValue(3));
////  Viscoelasticity->SetValue(posicao, array->GetValue(4));
////  ViscoelasticityExponent->SetValue(posicao, array->GetValue(5));
////  InfiltrationPressure->SetValue(posicao, array->GetValue(6));
////  ReferencePressure->SetValue(posicao, array->GetValue(7));
////  Permeability->SetValue(posicao, array->GetValue(8));
////  
////	Elastin->Modified();
////	Collagen->Modified();
////	CoefficientA->Modified();
////	CoefficientB->Modified();
////	Viscoelasticity->Modified();
////	ViscoelasticityExponent->Modified();
////	InfiltrationPressure->Modified();
////	ReferencePressure->Modified();
////	Permeability->Modified();
//	grid->Update();
//}


//----------------------------------------------------------------------------
int vtkHMWavePropagationFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // Pega informação dos objetos
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // Pega input e output padrão
  vtkHM1DStraightModelGrid *input = vtkHM1DStraightModelGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
    
  vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
      
  // Testa se input e output são válidos
  if(!input) 	vtkErrorMacro("Input is NULL.");
  if(!output) vtkErrorMacro("Output is NULL.");
  
  output->DeepCopy(input);

  //this->CreatePropertiesGrid(output);
  
  //this->SetScalarsFromGrid(output);
  
  //this->StartSimulation(output);
  
  //std::cout << *output << endl;
  
  //Iniciando a animação.
  //this->MakeAnimation(output);

  //Escrevendo os arquivos.
//  if((this->path != "None") && (this->path != "") && (this->path != NULL))
  if(strcmp(this->path, "")!=0)
  	{
//  	std::cout << "Path existe:::::::::::::::::::::::::::::::::::::::::::::" << endl;
//  	cout << this->path << endl;
  	this->SaveSimulation(output);
  	}
  
  return 1;
}


//----------------------------------------------------------------------------
void vtkHMWavePropagationFilter::PrintSelf(ostream& os, vtkIndent indent){}

//----------------------------------------------------------------------------
int vtkHMWavePropagationFilter::FillOutputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
  // Seta tipo da saída
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
  return 1;
}

//----------------------------------------------------------------------------
int vtkHMWavePropagationFilter::FillInputPortInformation(int port, vtkInformation* info)
{
		// Seta tipo de entrada
		if(!this->Superclass::FillInputPortInformation(port, info))
    	return 0;
  
		info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkHM1DStraightModelGrid");
		return 1;
}
