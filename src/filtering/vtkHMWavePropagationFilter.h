/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMWavePropagationFilter

  Author: Jan Palach

=========================================================================*/
// .NAME vtkHMWavePropagationFilter
// .SECTION Description
//This filter give a input(vtkHM1DStraightModelGrid), and make a animation scene
//representing the wave propagation in arterial system.

#ifndef __vtkHMWavePropagationFilter_h
#define __vtkHMWavePropagationFilter_h

#include "vtkDataSetAlgorithm.h"
#include "vtkDoubleArray.h"

#define FLOW_WAVE 0
#define PRESSURE_WAVE 1
#define AREA_WAVE 2

class vtkHM1DStraightModelGrid;

class VTK_EXPORT vtkHMWavePropagationFilter : public vtkDataSetAlgorithm
{
public:

  static vtkHMWavePropagationFilter *New();
  vtkTypeRevisionMacro(vtkHMWavePropagationFilter,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return a vtkHM1DStraightModelGrid...
  vtkHM1DStraightModelGrid* GetOutput(int port);
  
  void SetAnimationPath(char*);
  void SelectScalar(char*);
  
protected:

  vtkHMWavePropagationFilter();
  ~vtkHMWavePropagationFilter();

  // Description:
  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Sets input object type
  int FillInputPortInformation (int, vtkInformation*);
  
  // Description:
  // Sets output object type
  int FillOutputPortInformation(int, vtkInformation*);
	
  //This method extract all data of the pressure, flow, area, velocity from each node, and make a structure
  //that help in the process of generate animation, based in a scalar dataset.
  void SetScalarsFromGrid(vtkHM1DStraightModelGrid* grid);
	
  void StartSimulation(vtkHM1DStraightModelGrid* grid);
	
  //This method create four new properties, pressure, flow, area, velocity.
  void CreatePropertiesGrid(vtkHM1DStraightModelGrid* grid);
	
  //This method save the animation.
  void SaveSimulation(vtkHM1DStraightModelGrid* grid);
	
  vtkDoubleArray* Pressure;
  vtkDoubleArray* Flow;
  vtkDoubleArray* Area;
  vtkDoubleArray* Velocity;
	
  vtkDoubleArray* TimeStep;
  
  char path[256];
	
private:
  vtkHMWavePropagationFilter(const vtkHMWavePropagationFilter&);  // Not implemented.
  void operator=(const vtkHMWavePropagationFilter&);  // Not implemented.
};

#endif
