/*
#include "vtkImageData.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkActor.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkJPEGReader.h"
#include "vtkImageViewer.h"
#include "vtkImageGaussianSmooth.h"
#include "vtkPNGWriter.h"
#include "vtkImageCast.h"

vtkImageData *read(const char *filePath);
void write(vtkImageData *img);
void view(vtkImageData *img);
vtkImageData *ApplyGaussianFilter(vtkImageData *img, double devX, double devY, double radiusX, double radiusY);



int main( int argc, char *argv[ ])
{
  if (argc > 2)
  {

    using namespace std;  
    const char *filePath="/home/vinicius/images/ruidos.jpg";
    vtkImageData *img = vtkImageData::New();
  

    img = read(filePath);
    img->Update();

    img = ApplyGaussianFilter(img, atol(argv[1]), atol(argv[1]), atol(argv[2]), atol(argv[2]));
    img->Update();

    write(img);
    //view(img);
  
    return 1;
  }
  else
  {
  	cout << "Parâmetros incorretos" << endl;
  }
}

vtkImageData *ApplyGaussianFilter(vtkImageData *img, double devX, double devY, double radiusX, double radiusY)
{
  vtkImageGaussianSmooth *gaussianFilter = vtkImageGaussianSmooth::New();
  
  gaussianFilter->SetInput(img);
  gaussianFilter->SetDimensionality(2);
  gaussianFilter->SetStandardDeviations(devX,devY);
  gaussianFilter->SetRadiusFactors(radiusX, radiusY);
  
  return gaussianFilter->GetOutput();
}

vtkImageData *read(const char *filePath)
{
 vtkJPEGReader *reader = vtkJPEGReader::New();	
 reader->SetFileName(filePath);
 return reader->GetOutput();
}

void write(vtkImageData *img)
{
  vtkPNGWriter *writer = vtkPNGWriter::New();
  vtkImageCast *cast = vtkImageCast::New();
  
  cast->SetInput(img);
  cast->SetOutputScalarTypeToUnsignedChar();
  
  writer->SetInput(cast->GetOutput());
  writer->SetFileName("/home/vinicius/images/output.png");
  
  writer->Write();
}

void view(vtkImageData *img)
{
  vtkImageViewer *viewer = vtkImageViewer::New();
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  
  viewer->SetInput(img);
  viewer->SetColorWindow(255);
  viewer->SetColorLevel(128);

  iren->SetRenderWindow(viewer->GetRenderWindow());
  
  viewer->Render();
}
*/
