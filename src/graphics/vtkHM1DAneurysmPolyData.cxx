/*
 * $Id: vtkHM1DAneurysmPolyData.cxx 2341 2007-10-05 16:09:04Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DAneurysmPolyData.cxx,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.
 
=========================================================================*/

#include "vtkHM1DAneurysmPolyData.h"

#include "vtkCellArray.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkCellData.h"
#include "vtkPolyData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM1DAneurysmPolyData, "$Rev: $");
vtkStandardNewMacro(vtkHM1DAneurysmPolyData);

//----------------------------------------------------------------------------
// Class constructor
vtkHM1DAneurysmPolyData::vtkHM1DAneurysmPolyData()
{
	this->AneurysmSegmentInformation.clear();
}

//----------------------------------------------------------------------------
// Class destructor
vtkHM1DAneurysmPolyData::~vtkHM1DAneurysmPolyData()
{
	this->RemoveAllAneurysmSegmentInformation();
}

//----------------------------------------------------------------------------
// Copy the geometric and topological structure of an input poly data object.
void vtkHM1DAneurysmPolyData::CopyStructure(vtkDataSet *ds)
{
	vtkHM1DAneurysmPolyData *pd=(vtkHM1DAneurysmPolyData *)ds;
  vtkPointSet::CopyStructure(pd);

  if (this->Verts != pd->Verts)
    {
    if (this->Verts)
      {
      this->Verts->UnRegister(this);
      }
    this->Verts = pd->Verts;
    if (this->Verts)
      {
      this->Verts->Register(this);
      }
    }

  if (this->Lines != pd->Lines)
    {
    if (this->Lines)
      {
      this->Lines->UnRegister(this);
      }
    this->Lines = pd->Lines;
    if (this->Lines)
      {
      this->Lines->Register(this);
      }
    }

  if (this->Polys != pd->Polys)
    {
    if (this->Polys)
      {
      this->Polys->UnRegister(this);
      }
    this->Polys = pd->Polys;
    if (this->Polys)
      {
      this->Polys->Register(this);
      }
    }

  if (this->Strips != pd->Strips)
    {
    if (this->Strips)
      {
      this->Strips->UnRegister(this);
      }
    this->Strips = pd->Strips;
    if (this->Strips)
      {
      this->Strips->Register(this);
      }
    }

  if ( this->Cells )
    {
    this->Cells->UnRegister(this);
    this->Cells = NULL;
    }

  if ( this->Links )
    {
    this->Links->UnRegister(this);
    this->Links = NULL;
    }

  // Reset this information to mantain the functionality that was present when
  // CopyStructure called Initialize, which incorrectly wiped out attribute
  // data.  Someone MIGHT argue that this isn't the right thing to do.
  this->Information->Set(vtkDataObject::DATA_PIECE_NUMBER(), -1);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_PIECES(), 0);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_GHOST_LEVELS(), 0);
}

//----------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::DeepCopy(vtkDataObject *ds)
{
	vtkHM1DAneurysmPolyData *pd=(vtkHM1DAneurysmPolyData *)ds;
	this->Superclass::DeepCopy(pd);
	
	this->RemoveAllAneurysmSegmentInformation();
	
	for ( int i=0; i<pd->GetNumberOfAneurysm(); i++ )
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(pd->GetAneurysm(i));
		this->InsertAneurysmSegmentInformation(i, l);
		}
}

//----------------------------------------------------------------------------
// PrintSelf Method
void vtkHM1DAneurysmPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DAneurysmPolyData::GetAneurysm(vtkIdType idAneurysm)
{
	InformationMap::iterator it = this->AneurysmSegmentInformation.find(idAneurysm);
	if ( it !=  this->AneurysmSegmentInformation.end() )
		return it->second;
	
	return NULL;
}

//----------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::RemoveAllAneurysmSegmentInformation()
{
	InformationMap::iterator it = this->AneurysmSegmentInformation.begin();
	while ( it != this->AneurysmSegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->AneurysmSegmentInformation.clear();
	
	this->RemoveAllSegmentAneurysmInformation();
}

//----------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::RemoveAllSegmentAneurysmInformation()
{
	InformationMultimap::iterator it = this->SegmentAneurysmInformation.begin();
	while ( it != this->SegmentAneurysmInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentAneurysmInformation.clear();
}

//---------------------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::InsertAneurysmSegmentInformation(vtkIdType idAneurysm, vtkIdList *elementList)
{
	this->AneurysmSegmentInformation.insert(InformationPair(idAneurysm, elementList));
	
	vtkIdType idSegment = elementList->GetId(elementList->GetNumberOfIds()-1);
	
	//lista com os ids dos elementos e no fim o id da celula adicionada no grid
	vtkIdList *elemList = vtkIdList::New();
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		{
		elemList->InsertNextId(elementList->GetId(i));
		}
	elemList->InsertNextId(idAneurysm);
	
	//Inseri informações do multimap, chave é o id do segmento
	this->SegmentAneurysmInformation.insert(InformationPair(idSegment, elemList));
}

//---------------------------------------------------------------------------------------
vtkIdType vtkHM1DAneurysmPolyData::AddAneurysm(vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdType idPts;
	double pt;
	
	//lista para armazenar os ids dos pontos adicionados no grid
	vtkIdList *idPoints = vtkIdList::New();
		
	for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
		{
		idPts = this->Points->InsertNextPoint(pointArray->GetTuple3(j));
		//Inseri na lista o id do novo ponto
		idPoints->InsertNextId(idPts);
		}
	
	vtkIdType idCell = this->InsertNextCell(VTK_POLY_LINE, idPoints);
	this->InsertAneurysmSegmentInformation(idCell, elementList);
	
	idPoints->Delete();
	
	return idCell;
}

//---------------------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::ReplaceAneurysm(vtkIdType idAneurysm, vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DAneurysmPolyData *poly = vtkHM1DAneurysmPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllAneurysmSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i == idAneurysm )
				{
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
					{
					//Pega a coordenada do ponto
					p = pointArray->GetTuple3(j);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(p);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				poly->InsertAneurysmSegmentInformation(idCell, elementList);
				newIdPoints->Delete();
				} //Fim if ( i == idAneurysm )
			else
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetAneurysm(i));
				
				poly->InsertAneurysmSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
int vtkHM1DAneurysmPolyData::GetNumberOfAneurysm()
{
	return this->GetNumberOfCells();
}

//---------------------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::RemoveAneurysm(vtkIdType idAneurysm)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DAneurysmPolyData *poly = vtkHM1DAneurysmPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllAneurysmSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i != idAneurysm )
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetAneurysm(i));
				
				poly->InsertAneurysmSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
vtkIdList *vtkHM1DAneurysmPolyData::GetAneurysmInSegment(vtkIdType idSegment)
{
	std::pair<InformationMultimap::iterator, InformationMultimap::iterator> itSeg;
	
	vtkIdList *AneurysmList = NULL;
	
	itSeg = this->SegmentAneurysmInformation.equal_range(idSegment);
	
	if ( itSeg.first != this->SegmentAneurysmInformation.end() )
		{
		AneurysmList = vtkIdList::New();
		while ( itSeg.first != itSeg.second )
			{
			AneurysmList->InsertUniqueId(itSeg.first->second->GetId(itSeg.first->second->GetNumberOfIds()-1));
			itSeg.first++;
			}
		}
	
	return AneurysmList;
}

//---------------------------------------------------------------------------------------
void vtkHM1DAneurysmPolyData::DeleteAneurysmInSegment(vtkIdType idSegment)
{
	vtkIdList *AneurysmList = this->GetAneurysmInSegment(idSegment);
	
	if ( AneurysmList )
		{
		for ( int i=0; i<AneurysmList->GetNumberOfIds(); i++ )
			{
			this->RemoveAneurysm(AneurysmList->GetId(i));
			}
		AneurysmList->Delete();
		}
}
