/*
 * $Id: vtkHM1DAneurysmPolyData.h 2193 2007-07-26 13:07:03Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DAneurysmPolyData.h,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DAneurysmPolyData - concrete dataset represents Aneurysms, lines
// .SECTION Description
// vtkHM1DAneurysmPolyData é uma representação visualizável de Aneurysms. Nele está
// descritos o id do Aneurysm, o segmento e os elementos associados com o Aneurysm.
// Esta classe é filha da classe vtkPolyData e adiciona informações de segmentos
// e elementos associados com o Aneurysm.
// Toda a estrutura geométrica de visualização permanece igual à vtkPolyData.

// .SECTION See Also

#ifndef VTKHM1DANEURYSMPOLYDATA_H_
#define VTKHM1DANEURYSMPOLYDATA_H_

#include "vtkPolyData.h"

class vtkCellArray;
class vtkIdList;
class vtkDataObject;

#include <map>

class VTK_EXPORT vtkHM1DAneurysmPolyData : public vtkPolyData
{
public:
	static vtkHM1DAneurysmPolyData *New();

	vtkTypeRevisionMacro(vtkHM1DAneurysmPolyData, vtkPolyData);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkDataObject *dataObject);
	void CopyStructure(vtkDataSet *ds);
	
	//BTX
	// Description:
	// 
  typedef std::multimap<vtkIdType,vtkIdList*> InformationMultimap;
  typedef std::map<vtkIdType,vtkIdList*> InformationMap;
  typedef std::pair<vtkIdType,vtkIdList*> InformationPair;
  //ETX
	
  // Description:
  // Remove all information about association between Aneurysm and segment
  void RemoveAllAneurysmSegmentInformation();
  
  // Description:
  // Remove information about association between Aneurysm and segment
//  void RemoveAneurysmSegmentInformation(vtkIdType idAneurysm);  
  
  // Description:
  // Insert information of the association between Aneurysm and segment
  void InsertAneurysmSegmentInformation(vtkIdType idAneurysm, vtkIdList *elementList);
  
  // Description:
  // Add Aneurysm in grid and return Aneurysm id
  vtkIdType AddAneurysm(vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Replace Aneurysm.
  void ReplaceAneurysm(vtkIdType idAneurysm, vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Return list with ids of the elements associated with Aneurysm
  vtkIdList *GetAneurysm(vtkIdType idAneurysm);
  
  // Description:
  // Return number of Aneurysms
  int GetNumberOfAneurysm();
  
  // Description:
  // Remove Aneurysm.
  void RemoveAneurysm(vtkIdType idAneurysm);
  
  // Desctiption:
  // Return list with ids of the Aneurysm in segment.
  vtkIdList *GetAneurysmInSegment(vtkIdType idSegment);
  
  // Description:
  // Delete all Aneurysm in the segment
  void DeleteAneurysmInSegment(vtkIdType idSegment);
  
protected:

	// Description:
	// It contains id of the Aneurysm like key and id of the elements
	// and in the end of the list, has id of the segment.
	InformationMap AneurysmSegmentInformation;
	
	// Description:
	// It contains id of the segment like key and id of the elements
	// and in the end of the list, has id of the Aneurysm.
	InformationMultimap SegmentAneurysmInformation;
	
	
	// Descritpion:
	// Remove all information about association between segment and Aneurysm in the multimap
	void RemoveAllSegmentAneurysmInformation();
	
	
protected:
	vtkHM1DAneurysmPolyData();
	virtual ~vtkHM1DAneurysmPolyData();
	
private:
	vtkHM1DAneurysmPolyData(const vtkHM1DAneurysmPolyData&); 	// Not Implemented
	void operator=(const vtkHM1DAneurysmPolyData&);				// Not Implemented
};


#endif /*VTKHM1DANEURYSMPOLYDATA_H_*/
