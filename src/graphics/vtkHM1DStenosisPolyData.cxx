/*
 * $Id: vtkHM1DStenosisPolyData.cxx 2341 2007-10-05 16:09:04Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStenosisPolyData.cxx,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.
 
=========================================================================*/

#include "vtkHM1DStenosisPolyData.h"

#include "vtkCellArray.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkCellData.h"
#include "vtkPolyData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM1DStenosisPolyData, "$Rev: $");
vtkStandardNewMacro(vtkHM1DStenosisPolyData);

//----------------------------------------------------------------------------
// Class constructor
vtkHM1DStenosisPolyData::vtkHM1DStenosisPolyData()
{
	this->StenosisSegmentInformation.clear();
}

//----------------------------------------------------------------------------
// Class destructor
vtkHM1DStenosisPolyData::~vtkHM1DStenosisPolyData()
{
	this->RemoveAllStenosisSegmentInformation();
}

//----------------------------------------------------------------------------
// Copy the geometric and topological structure of an input poly data object.
void vtkHM1DStenosisPolyData::CopyStructure(vtkDataSet *ds)
{
	vtkHM1DStenosisPolyData *pd=(vtkHM1DStenosisPolyData *)ds;
  vtkPointSet::CopyStructure(pd);

  if (this->Verts != pd->Verts)
    {
    if (this->Verts)
      {
      this->Verts->UnRegister(this);
      }
    this->Verts = pd->Verts;
    if (this->Verts)
      {
      this->Verts->Register(this);
      }
    }

  if (this->Lines != pd->Lines)
    {
    if (this->Lines)
      {
      this->Lines->UnRegister(this);
      }
    this->Lines = pd->Lines;
    if (this->Lines)
      {
      this->Lines->Register(this);
      }
    }

  if (this->Polys != pd->Polys)
    {
    if (this->Polys)
      {
      this->Polys->UnRegister(this);
      }
    this->Polys = pd->Polys;
    if (this->Polys)
      {
      this->Polys->Register(this);
      }
    }

  if (this->Strips != pd->Strips)
    {
    if (this->Strips)
      {
      this->Strips->UnRegister(this);
      }
    this->Strips = pd->Strips;
    if (this->Strips)
      {
      this->Strips->Register(this);
      }
    }

  if ( this->Cells )
    {
    this->Cells->UnRegister(this);
    this->Cells = NULL;
    }

  if ( this->Links )
    {
    this->Links->UnRegister(this);
    this->Links = NULL;
    }

  // Reset this information to mantain the functionality that was present when
  // CopyStructure called Initialize, which incorrectly wiped out attribute
  // data.  Someone MIGHT argue that this isn't the right thing to do.
  this->Information->Set(vtkDataObject::DATA_PIECE_NUMBER(), -1);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_PIECES(), 0);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_GHOST_LEVELS(), 0);
}

//----------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::DeepCopy(vtkDataObject *ds)
{
	vtkHM1DStenosisPolyData *pd=(vtkHM1DStenosisPolyData *)ds;
	this->Superclass::DeepCopy(pd);
	
	this->RemoveAllStenosisSegmentInformation();
	
	for ( int i=0; i<pd->GetNumberOfStenosis(); i++ )
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(pd->GetStenosis(i));
		this->InsertStenosisSegmentInformation(i, l);
		}
}

//----------------------------------------------------------------------------
// PrintSelf Method
void vtkHM1DStenosisPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DStenosisPolyData::GetStenosis(vtkIdType idStenosis)
{
	InformationMap::iterator it = this->StenosisSegmentInformation.find(idStenosis);
	if ( it !=  this->StenosisSegmentInformation.end() )
		return it->second;
	
	return NULL;
}

//----------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::RemoveAllStenosisSegmentInformation()
{
	InformationMap::iterator it = this->StenosisSegmentInformation.begin();
	while ( it != this->StenosisSegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->StenosisSegmentInformation.clear();
	
	this->RemoveAllSegmentStenosisInformation();
}

//----------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::RemoveAllSegmentStenosisInformation()
{
	InformationMultimap::iterator it = this->SegmentStenosisInformation.begin();
	while ( it != this->SegmentStenosisInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentStenosisInformation.clear();
}

//---------------------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::InsertStenosisSegmentInformation(vtkIdType idStenosis, vtkIdList *elementList)
{
	this->StenosisSegmentInformation.insert(InformationPair(idStenosis, elementList));
	
	vtkIdType idSegment = elementList->GetId(elementList->GetNumberOfIds()-1);
	
	//lista com os ids dos elementos e no fim o id da celula adicionada no grid
	vtkIdList *elemList = vtkIdList::New();
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		{
		elemList->InsertNextId(elementList->GetId(i));
		}
	elemList->InsertNextId(idStenosis);
	
	//Inseri informações do multimap, chave é o id do segmento
	this->SegmentStenosisInformation.insert(InformationPair(idSegment, elemList));
}

//---------------------------------------------------------------------------------------
vtkIdType vtkHM1DStenosisPolyData::AddStenosis(vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdType idPts;
	double pt;
	
	//lista para armazenar os ids dos pontos adicionados no grid
	vtkIdList *idPoints = vtkIdList::New();
		
	for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
		{
		idPts = this->Points->InsertNextPoint(pointArray->GetTuple3(j));
		//Inseri na lista o id do novo ponto
		idPoints->InsertNextId(idPts);
		}
	
	vtkIdType idCell = this->InsertNextCell(VTK_POLY_LINE, idPoints);
	this->InsertStenosisSegmentInformation(idCell, elementList);
	
	idPoints->Delete();
	
	return idCell;
}

//---------------------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::ReplaceStenosis(vtkIdType idStenosis, vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DStenosisPolyData *poly = vtkHM1DStenosisPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllStenosisSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i == idStenosis )
				{
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
					{
					//Pega a coordenada do ponto
					p = pointArray->GetTuple3(j);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(p);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				poly->InsertStenosisSegmentInformation(idCell, elementList);
				newIdPoints->Delete();
				} //Fim if ( i == idStenosis )
			else
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetStenosis(i));
				
				poly->InsertStenosisSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
int vtkHM1DStenosisPolyData::GetNumberOfStenosis()
{
	return this->GetNumberOfCells();
}

//---------------------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::RemoveStenosis(vtkIdType idStenosis)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DStenosisPolyData *poly = vtkHM1DStenosisPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllStenosisSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i != idStenosis )
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetStenosis(i));
				
				poly->InsertStenosisSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStenosisPolyData::GetStenosisInSegment(vtkIdType idSegment)
{
	std::pair<InformationMultimap::iterator, InformationMultimap::iterator> itSeg;
	
	vtkIdList *StenosisList = NULL;
	
	itSeg = this->SegmentStenosisInformation.equal_range(idSegment);
	
	if ( itSeg.first != this->SegmentStenosisInformation.end() )
		{
		StenosisList = vtkIdList::New();
		while ( itSeg.first != itSeg.second )
			{
			StenosisList->InsertUniqueId(itSeg.first->second->GetId(itSeg.first->second->GetNumberOfIds()-1));
			itSeg.first++;
			}
		}
	
	return StenosisList;
}

//---------------------------------------------------------------------------------------
void vtkHM1DStenosisPolyData::DeleteStenosisInSegment(vtkIdType idSegment)
{
	vtkIdList *StenosisList = this->GetStenosisInSegment(idSegment);
	
	if ( StenosisList )
		{
		for ( int i=0; i<StenosisList->GetNumberOfIds(); i++ )
			{
			this->RemoveStenosis(StenosisList->GetId(i));
			}
		StenosisList->Delete();
		}
}
