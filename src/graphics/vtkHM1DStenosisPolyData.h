/*
 * $Id: vtkHM1DStenosisPolyData.h 2193 2007-07-26 13:07:03Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStenosisPolyData.h,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DStenosisPolyData - concrete dataset represents Stenosiss, lines
// .SECTION Description
// vtkHM1DStenosisPolyData é uma representação visualizável de Stenosiss. Nele está
// descritos o id do Stenosis, o segmento e os elementos associados com o Stenosis.
// Esta classe é filha da classe vtkPolyData e adiciona informações de segmentos
// e elementos associados com o Stenosis.
// Toda a estrutura geométrica de visualização permanece igual à vtkPolyData.

// .SECTION See Also

#ifndef VTKHM1DSTENOSISPOLYDATA_H_
#define VTKHM1DSTENOSISPOLYDATA_H_

#include "vtkPolyData.h"

class vtkCellArray;
class vtkIdList;
class vtkDataObject;

#include <map>

class VTK_EXPORT vtkHM1DStenosisPolyData : public vtkPolyData
{
public:
	static vtkHM1DStenosisPolyData *New();

	vtkTypeRevisionMacro(vtkHM1DStenosisPolyData, vtkPolyData);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkDataObject *dataObject);
	void CopyStructure(vtkDataSet *ds);
	
	//BTX
	// Description:
	// 
  typedef std::multimap<vtkIdType,vtkIdList*> InformationMultimap;
  typedef std::map<vtkIdType,vtkIdList*> InformationMap;
  typedef std::pair<vtkIdType,vtkIdList*> InformationPair;
  //ETX
	
  // Description:
  // Remove all information about association between Stenosis and segment
  void RemoveAllStenosisSegmentInformation();
  
  // Description:
  // Remove information about association between Stenosis and segment
//  void RemoveStenosisSegmentInformation(vtkIdType idStenosis);  
  
  // Description:
  // Insert information of the association between Stenosis and segment
  void InsertStenosisSegmentInformation(vtkIdType idStenosis, vtkIdList *elementList);
  
  // Description:
  // Add Stenosis in grid and return Stenosis id
  vtkIdType AddStenosis(vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Replace Stenosis.
  void ReplaceStenosis(vtkIdType idStenosis, vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Return list with ids of the elements associated with Stenosis
  vtkIdList *GetStenosis(vtkIdType idStenosis);
  
  // Description:
  // Return number of Stenosiss
  int GetNumberOfStenosis();
  
  // Description:
  // Remove Stenosis.
  void RemoveStenosis(vtkIdType idStenosis);
  
  // Desctiption:
  // Return list with ids of the Stenosis in segment.
  vtkIdList *GetStenosisInSegment(vtkIdType idSegment);
  
  // Description:
  // Delete all Stenosis in the segment
  void DeleteStenosisInSegment(vtkIdType idSegment);
  
protected:

	// Description:
	// It contains id of the Stenosis like key and id of the elements
	// and in the end of the list, has id of the segment.
	InformationMap StenosisSegmentInformation;
	
	// Description:
	// It contains id of the segment like key and id of the elements
	// and in the end of the list, has id of the Stenosis.
	InformationMultimap SegmentStenosisInformation;
	
	
	// Descritpion:
	// Remove all information about association between segment and Stenosis in the multimap
	void RemoveAllSegmentStenosisInformation();
	
	
protected:
	vtkHM1DStenosisPolyData();
	virtual ~vtkHM1DStenosisPolyData();
	
private:
	vtkHM1DStenosisPolyData(const vtkHM1DStenosisPolyData&); 	// Not Implemented
	void operator=(const vtkHM1DStenosisPolyData&);				// Not Implemented
};

#endif /*VTKHM1DSTENOSISPOLYDATA_H_*/
