/*
 * $Id$
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStentPolyData.cxx,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.
 
=========================================================================*/

#include "vtkHM1DStentPolyData.h"

#include "vtkCellArray.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkCellData.h"
#include "vtkPolyData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM1DStentPolyData, "$Rev: $");
vtkStandardNewMacro(vtkHM1DStentPolyData);

//----------------------------------------------------------------------------
// Class constructor
vtkHM1DStentPolyData::vtkHM1DStentPolyData()
{
	this->StentSegmentInformation.clear();
}

//----------------------------------------------------------------------------
// Class destructor
vtkHM1DStentPolyData::~vtkHM1DStentPolyData()
{
	this->RemoveAllStentSegmentInformation();
}

//----------------------------------------------------------------------------
// Copy the geometric and topological structure of an input poly data object.
void vtkHM1DStentPolyData::CopyStructure(vtkDataSet *ds)
{
	vtkHM1DStentPolyData *pd=(vtkHM1DStentPolyData *)ds;
  vtkPointSet::CopyStructure(pd);

  if (this->Verts != pd->Verts)
    {
    if (this->Verts)
      {
      this->Verts->UnRegister(this);
      }
    this->Verts = pd->Verts;
    if (this->Verts)
      {
      this->Verts->Register(this);
      }
    }

  if (this->Lines != pd->Lines)
    {
    if (this->Lines)
      {
      this->Lines->UnRegister(this);
      }
    this->Lines = pd->Lines;
    if (this->Lines)
      {
      this->Lines->Register(this);
      }
    }

  if (this->Polys != pd->Polys)
    {
    if (this->Polys)
      {
      this->Polys->UnRegister(this);
      }
    this->Polys = pd->Polys;
    if (this->Polys)
      {
      this->Polys->Register(this);
      }
    }

  if (this->Strips != pd->Strips)
    {
    if (this->Strips)
      {
      this->Strips->UnRegister(this);
      }
    this->Strips = pd->Strips;
    if (this->Strips)
      {
      this->Strips->Register(this);
      }
    }

  if ( this->Cells )
    {
    this->Cells->UnRegister(this);
    this->Cells = NULL;
    }

  if ( this->Links )
    {
    this->Links->UnRegister(this);
    this->Links = NULL;
    }

  // Reset this information to mantain the functionality that was present when
  // CopyStructure called Initialize, which incorrectly wiped out attribute
  // data.  Someone MIGHT argue that this isn't the right thing to do.
  this->Information->Set(vtkDataObject::DATA_PIECE_NUMBER(), -1);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_PIECES(), 0);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_GHOST_LEVELS(), 0);
}

//----------------------------------------------------------------------------
void vtkHM1DStentPolyData::DeepCopy(vtkDataObject *ds)
{
	vtkHM1DStentPolyData *pd=(vtkHM1DStentPolyData *)ds;
	this->Superclass::DeepCopy(pd);
	
	this->RemoveAllStentSegmentInformation();
	
	for ( int i=0; i<pd->GetNumberOfStent(); i++ )
		{
		vtkIdList *l = vtkIdList::New();
		l->DeepCopy(pd->GetStent(i));
		this->InsertStentSegmentInformation(i, l);
		}
}

//----------------------------------------------------------------------------
// PrintSelf Method
void vtkHM1DStentPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DStentPolyData::GetStent(vtkIdType idStent)
{
	InformationMap::iterator it = this->StentSegmentInformation.find(idStent);
	if ( it !=  this->StentSegmentInformation.end() )
		return it->second;
	
	return NULL;
}

//----------------------------------------------------------------------------
void vtkHM1DStentPolyData::RemoveAllStentSegmentInformation()
{
	InformationMap::iterator it = this->StentSegmentInformation.begin();
	while ( it != this->StentSegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->StentSegmentInformation.clear();
	
	this->RemoveAllSegmentStentInformation();
}

//----------------------------------------------------------------------------
void vtkHM1DStentPolyData::RemoveAllSegmentStentInformation()
{
	InformationMultimap::iterator it = this->SegmentStentInformation.begin();
	while ( it != this->SegmentStentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentStentInformation.clear();
}

//----------------------------------------------------------------------------
//void vtkHM1DStentPolyData::RemoveStentSegmentInformation(vtkIdType idStent)
//{
////	InformationMap::iterator it = this->StentSegmentInformation.find(idStent);
////	if ( it != this->StentSegmentInformation.end() )
////		{
////		vtkIdType idSegment = it->second->GetId(it->second->GetNumberOfIds()-1);
////		
////		it->second->Delete();
////		this->StentSegmentInformation.erase(idStent);
////		
////		std::pair<InformationMultimap::iterator, InformationMultimap::iterator> itSeg;
////			
////		itSeg = this->SegmentStentInformation.equal_range(idSegment);
////		
////		while ( itSeg.first != itSeg.second )
////			{
////			cout << "itSeg: " << itSeg.first->first << endl;
////			itSeg.first++;
////			}
////		}
//	
//}

//---------------------------------------------------------------------------------------
void vtkHM1DStentPolyData::InsertStentSegmentInformation(vtkIdType idStent, vtkIdList *elementList)
{
	this->StentSegmentInformation.insert(InformationPair(idStent, elementList));
	
	vtkIdType idSegment = elementList->GetId(elementList->GetNumberOfIds()-1);
	
	//lista com os ids dos elementos e no fim o id da celula adicionada no grid
	vtkIdList *elemList = vtkIdList::New();
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		{
		elemList->InsertNextId(elementList->GetId(i));
		}
	elemList->InsertNextId(idStent);
	
	//Inseri informações do multimap, chave é o id do segmento
	this->SegmentStentInformation.insert(InformationPair(idSegment, elemList));
}

//---------------------------------------------------------------------------------------
vtkIdType vtkHM1DStentPolyData::AddStent(vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdType idPts;
	double pt;
	
	//lista para armazenar os ids dos pontos adicionados no grid
	vtkIdList *idPoints = vtkIdList::New();
	
	for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
		{
		idPts = this->Points->InsertNextPoint(pointArray->GetTuple3(j));
		//Inseri na lista o id do novo ponto
		idPoints->InsertNextId(idPts);
		}
	
	vtkIdType idCell = this->InsertNextCell(VTK_POLY_LINE, idPoints);
	this->InsertStentSegmentInformation(idCell, elementList);
	
	idPoints->Delete();
	
	return idCell;
}

//---------------------------------------------------------------------------------------
void vtkHM1DStentPolyData::ReplaceStent(vtkIdType idStent, vtkIdList *elementList, vtkDoubleArray *pointArray)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DStentPolyData *poly = vtkHM1DStentPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllStentSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i == idStent )
				{
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<pointArray->GetNumberOfTuples(); j++ )
					{
					//Pega a coordenada do ponto
					p = pointArray->GetTuple3(j);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(p);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				poly->InsertStentSegmentInformation(idCell, elementList);
				newIdPoints->Delete();
				} //Fim if ( i == idStent )
			else
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetStent(i));
				
				poly->InsertStentSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
int vtkHM1DStentPolyData::GetNumberOfStent()
{
	return this->GetNumberOfCells();
}

//---------------------------------------------------------------------------------------
void vtkHM1DStentPolyData::RemoveStent(vtkIdType idStent)
{
	vtkIdList *idPoints;
	double pt[3], *p;
	vtkIdType idPts;
	
	vtkIdList *newIdPoints;
	
	vtkPoints *newPoints = vtkPoints::New();
	vtkCellArray *lines = vtkCellArray::New();
	vtkHM1DStentPolyData *poly = vtkHM1DStentPolyData::New();

	poly->SetLines(lines);
	lines->Delete();
	poly->SetPoints(newPoints);
	newPoints->Delete();
	newPoints = poly->GetPoints();
	
	if ( this->Lines )
		{
		//Limpa qualquer informação que esteja no map
		poly->RemoveAllStentSegmentInformation();
		for ( int i=0; i<this->GetNumberOfCells(); i++ )
			{
			if ( i != idStent )
				{
				//Pega a lista de id de pontos da celula POLYLINE
				idPoints = this->GetCell(i)->GetPointIds();
				newIdPoints = vtkIdList::New();
				
				for ( int j=0; j<idPoints->GetNumberOfIds(); j++ )
					{
					//Pega a coordenada do ponto
					this->GetPoint(idPoints->GetId(j), pt);
					//Inseri a coordenada
					idPts = newPoints->InsertNextPoint(pt);
					//Inseri na lista o id do novo ponto
					newIdPoints->InsertNextId(idPts);
					}
				//Inseri a celula
				vtkIdType idCell = poly->InsertNextCell(VTK_POLY_LINE, newIdPoints);
				
				vtkIdList *l = vtkIdList::New();
				l->DeepCopy(this->GetStent(i));
				
				poly->InsertStentSegmentInformation(idCell, l);
				newIdPoints->Delete();
				} //Fim else
			} //Fim for ( int i=0; i<this->GetNumberOfCells(); i++ )

		this->DeepCopy(poly);
		poly->Delete();
		} //Fim if ( this->Lines )
}

//---------------------------------------------------------------------------------------
vtkIdList *vtkHM1DStentPolyData::GetStentsInSegment(vtkIdType idSegment)
{
	std::pair<InformationMultimap::iterator, InformationMultimap::iterator> itSeg;
	
	vtkIdList *stentList = NULL;
	
	itSeg = this->SegmentStentInformation.equal_range(idSegment);
	
	if ( itSeg.first != this->SegmentStentInformation.end() )
		{
		stentList = vtkIdList::New();
		while ( itSeg.first != itSeg.second )
			{
			stentList->InsertUniqueId(itSeg.first->second->GetId(itSeg.first->second->GetNumberOfIds()-1));
			itSeg.first++;
			}
		}
	
	return stentList;
}

//---------------------------------------------------------------------------------------
void vtkHM1DStentPolyData::DeleteStentsInSegment(vtkIdType idSegment)
{
	vtkIdList *stentList = this->GetStentsInSegment(idSegment);
	
	if ( stentList )
		{
		for ( int i=0; i<stentList->GetNumberOfIds(); i++ )
			{
			this->RemoveStent(stentList->GetId(i));
			}
		}
}
