/*
 * $Id: vtkHM1DStentPolyData.h 2193 2007-07-26 13:07:03Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStentPolyData.h,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DStentPolyData - concrete dataset represents stents, lines
// .SECTION Description
// vtkHM1DStentPolyData é uma representação visualizável de stents. Nele está
// descritos o id do stent, o segmento e os elementos associados com o stent.
// Esta classe é filha da classe vtkPolyData e adiciona informações de segmentos
// e elementos associados com o stent.
// Toda a estrutura geométrica de visualização permanece igual à vtkPolyData.

// .SECTION See Also

#ifndef VTKHM1DSTENTPOLYDATA_H_
#define VTKHM1DSTENTPOLYDATA_H_

#include "vtkPolyData.h"

class vtkCellArray;
class vtkIdList;
class vtkDataObject;

#include <map>

class VTK_EXPORT vtkHM1DStentPolyData : public vtkPolyData
{
public:
	static vtkHM1DStentPolyData *New();

	vtkTypeRevisionMacro(vtkHM1DStentPolyData, vtkPolyData);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkDataObject *dataObject);
	void CopyStructure(vtkDataSet *ds);
	
	//BTX
	// Description:
	// 
  typedef std::multimap<vtkIdType,vtkIdList*> InformationMultimap;
  typedef std::map<vtkIdType,vtkIdList*> InformationMap;
  typedef std::pair<vtkIdType,vtkIdList*> InformationPair;
  //ETX
	
  // Description:
  // Remove all information about association between stent and segment
  void RemoveAllStentSegmentInformation();
  
  // Description:
  // Remove information about association between stent and segment
//  void RemoveStentSegmentInformation(vtkIdType idStent);  
  
  // Description:
  // Insert information of the association between stent and segment
  void InsertStentSegmentInformation(vtkIdType idStent, vtkIdList *elementList);
  
  // Description:
  // Add stent in grid and return stent id
  vtkIdType AddStent(vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Replace Stent.
  void ReplaceStent(vtkIdType idStent, vtkIdList *elementList, vtkDoubleArray *pointArray);
  
  // Description:
  // Return list with ids of the elements associated with stent
  vtkIdList *GetStent(vtkIdType idStent);
  
  // Description:
  // Return number of stents
  int GetNumberOfStent();
  
  // Description:
  // Remove stent.
  void RemoveStent(vtkIdType idStent);
  
  // Desctiption:
  // Return list with ids of the stents in segment.
  vtkIdList *GetStentsInSegment(vtkIdType idSegment);
  
  // Description:
  // Delete all stents in the segment
  void DeleteStentsInSegment(vtkIdType idSegment);
  
protected:

	// Description:
	// It contains id of the stent like key and id of the elements
	// and in the end of the list, has id of the segment.
	InformationMap StentSegmentInformation;
	
	// Description:
	// It contains id of the segment like key and id of the elements
	// and in the end of the list, has id of the stent.
	InformationMultimap SegmentStentInformation;
	
	
	// Descritpion:
	// Remove all information about association between segment and stent in the multimap
	void RemoveAllSegmentStentInformation();
	
	
protected:
	vtkHM1DStentPolyData();
	virtual ~vtkHM1DStentPolyData();
	
private:
	vtkHM1DStentPolyData(const vtkHM1DStentPolyData&); 	// Not Implemented
	void operator=(const vtkHM1DStentPolyData&);				// Not Implemented
};


#endif /*VTKHM1DSTENTPOLYDATA_H_*/
