/*
 * $Id: vtkHM1DStraightModelGrid.cxx 2884 2010-05-11 12:37:55Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStraightModelGrid.cxx,v $

  Copyright (c) Diego Mazala
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DTerminal.h"

#include "vtkCellArray.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelGrid, "$Rev: 2884 $");
vtkStandardNewMacro(vtkHM1DStraightModelGrid);

//----------------------------------------------------------------------------
// Class constructor
vtkHM1DStraightModelGrid::vtkHM1DStraightModelGrid()
{
	this->StraightModel = vtkHM1DStraightModel::New();

	//  Creating vtkCellArray for lines
	this->Segments = NULL;

	// Creating vtkCellArray for vertices
	this->Nodes = NULL;

	this->NullElements1D3D = NULL;

	// Este Array é semelhante ao this->Nodes, mas com a informação de Id embutida
	this->Terminals = NULL;
	this->Radius = NULL;
	this->Elastin = NULL;
	this->Collagen = NULL;
	this->CoefficientA = NULL;
	this->CoefficientB = NULL;
	this->Viscoelasticity = NULL;
	this->ViscoelasticityExponent = NULL;
	this->InfiltrationPressure = NULL;
	this->ReferencePressure = NULL;
	this->Permeability = NULL;

	this->NullResistance = NULL;
	this->R1 = NULL;
	this->R2 = NULL;
	this->Cap = NULL;
	this->Pressure = NULL;
	this->SpeedOfSound = NULL;

	this->MainFlow = NULL;
}

//----------------------------------------------------------------------------
// Class destructor
vtkHM1DStraightModelGrid::~vtkHM1DStraightModelGrid()
{
	if ( this->StraightModel )
		this->StraightModel->Delete();

	if ( this->Segments )
		this->Segments->Delete();

	if ( this->Terminals )
		this->Terminals->Delete();

	if ( this->Nodes )
		this->Nodes->Delete();

	if ( this->NullElements1D3D )
		this->NullElements1D3D->Delete();

	InformationMap::iterator it = this->SegmentInformation.begin();
	while ( it != this->SegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentInformation.clear();

	it = this->NodeInformation.begin();
	while ( it != this->NodeInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->NodeInformation.clear();

//	this->TerminalInformation.clear();

	it = this->NullElements1D3DInformation.begin();
	while ( it != this->NullElements1D3DInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->NullElements1D3DInformation.clear();
}


//----------------------------------------------------------------------------
// Setting the StraightModel reference
void vtkHM1DStraightModelGrid::SetStraightModel( vtkHM1DStraightModel* s )
{
	if ( this->StraightModel != s )
		{
		if ( this->StraightModel )
			this->StraightModel->UnRegister(this);

		this->StraightModel = s;
		if ( this->StraightModel )
			this->StraightModel->Register(this);

		this->Modified();
		}
}

//----------------------------------------------------------------------------
// Getting the StraightModel reference
vtkHM1DStraightModel* vtkHM1DStraightModelGrid::GetStraightModel()
{
	return this->StraightModel;
}



//----------------------------------------------------------------------------
// Setting the Segments reference
void vtkHM1DStraightModelGrid::SetSegments( vtkCellArray* s )
{
	if ( s != this->Segments)
    {
    if (this->Segments)
      {
      this->Segments->UnRegister(this);
      }
    this->Segments = s;
    if (this->Segments)
      {
      this->Segments->Register(this);
      }
    this->Modified();
    }
}


//----------------------------------------------------------------------------
// Getting the Segments reference
vtkCellArray* vtkHM1DStraightModelGrid::GetSegments()
{
	return this->Segments;
}


//----------------------------------------------------------------------------
// Setting the Terminals reference
void vtkHM1DStraightModelGrid::SetTerminals( vtkCellArray* t )
{
  if ( t != this->Terminals)
    {
    if (this->Terminals)
      {
      this->Terminals->UnRegister(this);
      }
    this->Terminals = t;
    if (this->Terminals)
      {
      this->Terminals->Register(this);
      }
    this->Modified();
    }
}


//----------------------------------------------------------------------------
// Getting the Terminals reference
vtkCellArray* vtkHM1DStraightModelGrid::GetTerminals()
{
	return this->Terminals;
}

//----------------------------------------------------------------------------
// Setting the Nodes reference
void vtkHM1DStraightModelGrid::SetNodes( vtkCellArray* n )
{
  if ( n != this->Nodes)
    {
    if (this->Nodes)
      {
      this->Nodes->UnRegister(this);
      }
    this->Nodes = n;
    if (this->Nodes)
      {
      this->Nodes->Register(this);
      }
    this->Modified();
    }
}


//----------------------------------------------------------------------------
// Getting the Nodes reference
vtkCellArray* vtkHM1DStraightModelGrid::GetNodes()
{
	return this->Nodes;
}

//----------------------------------------------------------------------------
// Setting the Nodes reference
void vtkHM1DStraightModelGrid::SetNullElements1D3D( vtkCellArray* n )
{
  if ( n != this->NullElements1D3D)
    {
    if (this->NullElements1D3D)
      {
      this->NullElements1D3D->UnRegister(this);
      }
    this->NullElements1D3D = n;
    if (this->NullElements1D3D)
      {
      this->NullElements1D3D->Register(this);
      }
    this->Modified();
    }
}


//----------------------------------------------------------------------------
// Getting the Nodes reference
vtkCellArray* vtkHM1DStraightModelGrid::GetNullElements1D3D()
{
	return this->NullElements1D3D;
}


//----------------------------------------------------------------------------
int vtkHM1DStraightModelGrid::GetNumberOfTerminals()
{
	return this->Terminals->GetNumberOfCells();
}

//----------------------------------------------------------------------------
// PrintSelf Method
void vtkHM1DStraightModelGrid::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
//	os << indent << "StraightModel : " << *this->StraightModel << "\n";
}

//----------------------------------------------------------------------------
// Copy the geometric and topological structure of an input poly data object.
void vtkHM1DStraightModelGrid::CopyStructure(vtkDataSet *ds)
{
  vtkHM1DStraightModelGrid *pd=(vtkHM1DStraightModelGrid *)ds;
  vtkPointSet::CopyStructure(ds);

  if (this->Verts != pd->Verts)
    {
    if (this->Verts)
      {
      this->Verts->UnRegister(this);
      }
    this->Verts = pd->Verts;
    if (this->Verts)
      {
      this->Verts->Register(this);
      }
    }

  if (this->Lines != pd->Lines)
    {
    if (this->Lines)
      {
      this->Lines->UnRegister(this);
      }
    this->Lines = pd->Lines;
    if (this->Lines)
      {
      this->Lines->Register(this);
      }
    }

  if (this->Polys != pd->Polys)
    {
    if (this->Polys)
      {
      this->Polys->UnRegister(this);
      }
    this->Polys = pd->Polys;
    if (this->Polys)
      {
      this->Polys->Register(this);
      }
    }

  if (this->Strips != pd->Strips)
    {
    if (this->Strips)
      {
      this->Strips->UnRegister(this);
      }
    this->Strips = pd->Strips;
    if (this->Strips)
      {
      this->Strips->Register(this);
      }
    }

  if ( this->Cells )
    {
    this->Cells->UnRegister(this);
    this->Cells = NULL;
    }

  if ( this->Links )
    {
    this->Links->UnRegister(this);
    this->Links = NULL;
    }

  // Reset this information to mantain the functionality that was present when
  // CopyStructure called Initialize, which incorrectly wiped out attribute
  // data.  Someone MIGHT argue that this isn't the right thing to do.
  this->Information->Set(vtkDataObject::DATA_PIECE_NUMBER(), -1);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_PIECES(), 0);
  this->Information->Set(vtkDataObject::DATA_NUMBER_OF_GHOST_LEVELS(), 0);
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::DeepCopy(vtkDataObject *dataObject)
{
	vtkHM1DStraightModelGrid *grid = vtkHM1DStraightModelGrid::SafeDownCast(dataObject);

	 this->ResetStructure();

	this->StraightModel->DeepCopy(grid->GetStraightModel());
	
	this->CreateStraightModelGrid();

//	if ( !this->Segments )
//		this->Segments = vtkCellArray::New();
//	if ( !this->Terminals )
//			this->Terminals = vtkCellArray::New();
//	if ( !this->Nodes )
//		this->Nodes = vtkCellArray::New();
//
//	this->Segments->DeepCopy(grid->GetSegments());
//	this->Terminals->DeepCopy(grid->GetTerminals());
//	this->Nodes->DeepCopy(grid->GetNodes());
//
//	this->SetSegmentInformation(grid->GetSegmentInformation());
//	this->SetNodeInformation(grid->GetNodeInformation());
//	this->SetTerminalInformation(grid->GetTerminalInformation());
//
//  // Do superclass
//  // We have to do this BEFORE we call BuildLinks, else there are no points
//  // to build the links on (the parent DeepCopy copies the points)
//  this->vtkPointSet::DeepCopy(dataObject);
//
////  vtkPolyData *polyData = vtkPolyData::SafeDownCast(dataObject);
//
//  if ( grid != NULL )
//    {
//    vtkCellArray *ca;
//    ca = vtkCellArray::New();
//    ca->DeepCopy(grid->GetVerts());
//    this->SetVerts(ca);
//    ca->Delete();
//
//    ca = vtkCellArray::New();
//    ca->DeepCopy(grid->GetLines());
//    this->SetLines(ca);
//    ca->Delete();
//
//    ca = vtkCellArray::New();
//    ca->DeepCopy(grid->GetPolys());
//    this->SetPolys(ca);
//    ca->Delete();
//
//    ca = vtkCellArray::New();
//    ca->DeepCopy(grid->GetStrips());
//    this->SetStrips(ca);
//    ca->Delete();
//
//    if ( this->Cells )
//      {
//      this->Cells->UnRegister(this);
//      this->Cells = NULL;
//      }
//    if (grid->Cells)
//      {
//      this->BuildCells();
//      }
//
//    if ( this->Links )
//      {
//      this->Links->UnRegister(this);
//      this->Links = NULL;
//      }
//    if (grid->Links)
//      {
//      this->BuildLinks();
//      }
//    }
}



//----------------------------------------------------------------------------
int vtkHM1DStraightModelGrid::CreateStraightModelGrid()
{
	vtkDebugMacro(<<"Creating vtkHM1DStraightModelGrid.");

	if ( this->StraightModel )
		{
		// Creating vtkPoints
		if ( !this->Points )
			this->Points = vtkPoints::New();

		//  Criando vtkCellArray para Linhas
		if ( !this->Lines )
			this->Lines = vtkCellArray::New();

		// Criando vtkCellArray para terminais
		if ( !this->Terminals )
			this->Terminals = vtkCellArray::New();

		//  Creando vtkCellArray para os elementos
		if ( !this->Segments )
			this->Segments = vtkCellArray::New();

		// Creating vtkCellArray for vertices
		if ( !this->Nodes )
			this->Nodes = vtkCellArray::New();

		// Creating vtkCellArray for vertices
		if ( !this->NullElements1D3D )
			this->NullElements1D3D = vtkCellArray::New();

		this->Radius = vtkDoubleArray::New();
		this->Radius->SetName("Radius");

		this->Elastin = vtkDoubleArray::New();
		this->Elastin->SetName("Elastin");

		this->NullResistance =  vtkDoubleArray::New();
		this->NullResistance->SetName("NullResistance");

		this->R1 =  vtkDoubleArray::New();
		this->R1->SetName("R1");

		this->R2 =  vtkDoubleArray::New();
		this->R2->SetName("R2");

		this->Cap =  vtkDoubleArray::New();
		this->Cap->SetName("Cap");

		this->Pressure =  vtkDoubleArray::New();
		this->Pressure->SetName("Pressure");


		this->SpeedOfSound =  vtkDoubleArray::New();
		this->SpeedOfSound->SetName("SpeedOfSound");




		this->Collagen = vtkDoubleArray::New();
		this->Collagen->SetName("Collagen");

		this->CoefficientA = vtkDoubleArray::New();
		this->CoefficientA->SetName("CoefficientA");

		this->CoefficientB = vtkDoubleArray::New();
		this->CoefficientB->SetName("CoefficientB");

		this->Viscoelasticity = vtkDoubleArray::New();
		this->Viscoelasticity->SetName("Viscoelasticity");

		this->ViscoelasticityExponent = vtkDoubleArray::New();
		this->ViscoelasticityExponent->SetName("ViscoelasticityExponent");

		this->InfiltrationPressure = vtkDoubleArray::New();
		this->InfiltrationPressure->SetName("InfiltrationPressure");

		this->ReferencePressure = vtkDoubleArray::New();
		this->ReferencePressure->SetName("ReferencePressure");

		this->Permeability = vtkDoubleArray::New();
		this->Permeability->SetName("Permeability");

		this->MainFlow = vtkDoubleArray::New();
		this->MainFlow->SetName("Main Flow");

		//Adicionando os arrays de propriedades ao grid
		this->GetPointData()->AddArray( this->Radius );
		this->GetCellData()->AddArray( this->Elastin );
		this->GetCellData()->AddArray( this->NullResistance);

		this->GetCellData()->AddArray( this->R1);
		this->GetCellData()->AddArray( this->R2);
		this->GetCellData()->AddArray( this->Cap);
		this->GetCellData()->AddArray( this->Pressure);
		this->GetCellData()->AddArray( this->SpeedOfSound);


		this->GetCellData()->AddArray( this->Collagen );
		this->GetCellData()->AddArray( this->CoefficientA );
		this->GetCellData()->AddArray( this->CoefficientB );
		this->GetCellData()->AddArray( this->Viscoelasticity );
		this->GetCellData()->AddArray( this->ViscoelasticityExponent );
		this->GetCellData()->AddArray( this->InfiltrationPressure );
		this->GetCellData()->AddArray( this->ReferencePressure );
		this->GetCellData()->AddArray( this->Permeability );
//		this->GetCellData()->AddArray( this->MainFlow );
		this->GetCellData()->SetActiveAttribute("Property",
                                                 vtkDataSetAttributes::SCALARS);
		this->MainFlow->Delete();
    if ( this->Radius )
    	this->Radius->Delete();

    if ( this->Elastin )
			this->Elastin->Delete();

		if ( this->NullResistance)
			this->NullResistance->Delete();

		if ( this->R1)
			this->R1->Delete();

		if ( this->R2)
			this->R2->Delete();

		if ( this->Cap)
			this->Cap->Delete();

		if ( this->Pressure)
			this->Pressure->Delete();

		if (this->SpeedOfSound)
			this->SpeedOfSound->Delete();


		if ( this->Collagen )
			this->Collagen->Delete();
		if ( this->CoefficientA )
			this->CoefficientA->Delete();
		if ( this->CoefficientB )
			this->CoefficientB->Delete();
		if ( this->Viscoelasticity )
			this->Viscoelasticity->Delete();
		if ( this->ViscoelasticityExponent )
			this->ViscoelasticityExponent->Delete();
		if ( this->InfiltrationPressure )
			this->InfiltrationPressure->Delete();
		if ( this->ReferencePressure )
			this->ReferencePressure->Delete();
		if ( this->Permeability )
			this->Permeability->Delete();

    //Pegando os arrays do grid
    this->Radius	 									= vtkDoubleArray::SafeDownCast(this->GetPointData()->GetArray("Radius"));
		this->Elastin 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Elastin"));

		this->NullResistance 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("NullResistance"));
		this->R1 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R1"));
		this->R2 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R2"));
		this->Cap 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Cap"));
		this->Pressure 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Pressure"));
		this->SpeedOfSound 		= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("SpeedOfSound"));



		this->Collagen 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Collagen"));
		this->CoefficientA							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientA"));
		this->CoefficientB							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientB"));
		this->Viscoelasticity						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Viscoelasticity"));
		this->ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ViscoelasticityExponent"));
		this->InfiltrationPressure			= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("InfiltrationPressure"));
		this->ReferencePressure					= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ReferencePressure"));
		this->Permeability							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Permeability"));

		vtkDebugMacro(<<"Walking vtkHM1DStraightModel to set segments and terminals in vtkHM1DStraightModelSource.");
		int rootId = this->StraightModel->Get1DTreeRoot()->GetId();
		this->WalkStraightModelTree(rootId);

		vtkDebugMacro(<<"Creating scalars");

		//Dimensionando o tamanho dos array para o numero de linhas
		this->Radius->Resize(this->Points->GetNumberOfPoints());
		this->Elastin->Resize(this->Lines->GetNumberOfCells());

		this->NullResistance->Resize(this->Lines->GetNumberOfCells());
		this->R1->Resize(this->Lines->GetNumberOfCells());
		this->R2->Resize(this->Lines->GetNumberOfCells());
		this->Cap->Resize(this->Lines->GetNumberOfCells());
		this->Pressure->Resize(this->Lines->GetNumberOfCells());

		this->SpeedOfSound->Resize(this->Lines->GetNumberOfCells());



		this->Collagen->Resize(this->Lines->GetNumberOfCells());
		this->CoefficientA->Resize(this->Lines->GetNumberOfCells());
		this->CoefficientB->Resize(this->Lines->GetNumberOfCells());
		this->Viscoelasticity->Resize(this->Lines->GetNumberOfCells());
		this->ViscoelasticityExponent->Resize(this->Lines->GetNumberOfCells());
		this->InfiltrationPressure->Resize(this->Lines->GetNumberOfCells());
		this->ReferencePressure->Resize(this->Lines->GetNumberOfCells());
		this->Permeability->Resize(this->Lines->GetNumberOfCells());

		this->BuildCells();
		this->BuildLinks();
		}

  return 1;
}

//----------------------------------------------------------------------------
// To cover the StraightModel to generate segments
void vtkHM1DStraightModelGrid::WalkStraightModelTree( int &id )
{
	vtkDebugMacro(<<"WalkStraightModelTree: " << id);

//	vtkHM1DTreeElement *elem;
//	vtkHM1DTreeElement *child;
//
//	if (this->StraightModel->IsSegment(id))
//	{
//		elem = this->StraightModel->GetSegment(id);
//		child = elem->GetFirstChild();
//		this->AddSegment( vtkHM1DSegment::SafeDownCast( elem ) );
//	}
//	else
//	{
//		elem = this->StraightModel->GetTerminal(id);
//		child = elem->GetFirstChild();
//		this->AddTerminal( vtkHM1DTerminal::SafeDownCast( elem ) );
//	}
//
//	if (!child)
//	{
//		// É uma folha
//		return;
//	}
//	else
//		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
//		{
//			id = child->GetId();
//			child = elem->GetNextChild();
//			this->WalkStraightModelTree(id);
//		}

	vtkHM1DStraightModel::TreeNodeDataMap segMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it = segMap.begin();

	vtkHM1DSegment *seg;

	while ( it != segMap.end() )
		{
		seg = vtkHM1DSegment::SafeDownCast(it->second);

		this->AddSegment( seg );

		it++;
		}


	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	it = termMap.begin();

	vtkHM1DTerminal *term;

	while ( it != termMap.end() )
		{
		term = vtkHM1DTerminal::SafeDownCast(it->second);

		this->AddTerminal( term );

		it++;
		}

}

//----------------------------------------------------------------------------
// Set scalars
void vtkHM1DStraightModelGrid::SetScalars()
{
	vtkDebugMacro(<<"Set Scalars.");

//	this->Scalars->SetName("Scalars");
//
//	for (int i=0; i<this->Points->GetNumberOfPoints(); i++)
//		this->Scalars->InsertNextValue(1.0);
}

//----------------------------------------------------------------------------
// Adicionar segmentos
void vtkHM1DStraightModelGrid::AddSegment(vtkHM1DSegment* s)
{
	vtkIdType idPts[4];
	int nPoints = s->GetElementNumber()+1; // Number of nodes
	vtkIdType idNode[3], id, idTerminal[3];
	vtkHM1DTreeElement *child;

	vtkIdType idLine, idCellNode;

	vtkIdList *elementList = vtkIdList::New();
	vtkIdList *nodeList = vtkIdList::New();

	if ( nPoints > 0 )
		{
		double p[3];
		p[0] = s->GetNodeData(0)->coords[0];
		p[1] = s->GetNodeData(0)->coords[1];
		p[2] = s->GetNodeData(0)->coords[2];

		this->Radius->InsertNextValue(s->GetNodeData(0)->radius);

		// Adicionando Id do ponto (x,y) inicial
		idNode[0] = idPts[0] = this->Points->InsertNextPoint(p);
		idNode[1] = 0;
		idNode[2] = s->GetId();

		idCellNode = this->Nodes->InsertNextCell(3, idNode);
		nodeList->InsertNextId(idCellNode);

		// acesso terminal que é filho desse segmento
		vtkHM1DTerminal *t = NULL;
		t = vtkHM1DTerminal::SafeDownCast(s->GetFirstChild());

		for (int i=1; i<nPoints; i++)
			{
			double pt[3];
			pt[0] = s->GetNodeData(i)->coords[0];
			pt[1] = s->GetNodeData(i)->coords[1];
			pt[2] = s->GetNodeData(i)->coords[2];
			// Adicionando Id do ponto (x,y) final
			idNode[0] = idPts[1] = this->Points->InsertNextPoint(pt);

			// Adicionando Id elemento no StraightModel
			idPts[2] = i-1;

			idPts[3] = s->GetId();

			idNode[1] = i;
			idNode[2] = s->GetId();

			idCellNode = this->Nodes->InsertNextCell(3, idNode);
			nodeList->InsertNextId(idCellNode);

			// Adiciona ao vetor de linhas do PolyData
			idLine = this->Lines->InsertNextCell(2,idPts);
			// Adiciona ao vetor de segmentos do PolyData para o StraightModel
			this->Segments->InsertNextCell(4,idPts);

			elementList->InsertNextId(idLine);

			idPts[0] = idPts[1];

			if (t)
				{
				if (!t->GetR1()->front() || !t->GetR2()->front())
					{
					this->NullResistance->InsertNextValue(100);
					this->R1->InsertNextValue(t->GetR1()->front());
					this->R2->InsertNextValue(t->GetR2()->front());
					this->Cap->InsertNextValue(t->GetC()->front());
					this->Pressure->InsertNextValue(t->GetPt()->front());
					}
				else
					{
				  this->NullResistance->InsertNextValue(0);
					this->R1->InsertNextValue(t->GetR1()->front());
					this->R2->InsertNextValue(t->GetR2()->front());
					this->Cap->InsertNextValue(t->GetC()->front());
					this->Pressure->InsertNextValue(t->GetPt()->front());
					}
				}
			else
				vtkWarningMacro("Could not find a child terminal from this segment");

			// Calculo da velocidade do Som
			double radius = s->GetNodeData(i)->radius;
			double alfa =  s->GetNodeData(i)->alfa;
			double elastin = s->GetElementData(i-1)->Elastin;
			double density = 1.04;
			double wallthickness = alfa/(vtkMath::Pi()*radius);
			double SoundSpeed = sqrt( (elastin * wallthickness)/(2 * density * radius) );
			this->SpeedOfSound->InsertNextValue(SoundSpeed);

			this->Radius->InsertNextValue(s->GetNodeData(i)->radius);
			this->Elastin->InsertNextValue(s->GetElementData(i-1)->Elastin);
			this->Collagen->InsertNextValue(s->GetElementData(i-1)->Collagen);
			this->CoefficientA->InsertNextValue(s->GetElementData(i-1)->CoefficientA);
			this->CoefficientB->InsertNextValue(s->GetElementData(i-1)->CoefficientB);
			this->Viscoelasticity->InsertNextValue(s->GetElementData(i-1)->Viscoelasticity);
			this->ViscoelasticityExponent->InsertNextValue(s->GetElementData(i-1)->ViscoelasticityExponent);
			this->InfiltrationPressure->InsertNextValue(s->GetElementData(i-1)->InfiltrationPressure);
			this->ReferencePressure->InsertNextValue(s->GetElementData(i-1)->ReferencePressure);
			this->Permeability->InsertNextValue(s->GetElementData(i-1)->Permeability);
			}

		}
	// Adiciona o Id do ponto (x,y) do terminal
	idTerminal[0] = idPts[1];

	//Inserir no map informacoes das celulas associadas ao segmento para que
	//facilite e agilize a busca pelo elemento.
	this->SegmentInformation.insert(InformationPair(s->GetId(),elementList));
	this->NodeInformation.insert(InformationPair(s->GetId(),nodeList));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::AddTerminal(vtkHM1DTerminal *t)
{
	vtkHM1DSegment *elem;
	vtkIdType idTerminal[3], *info;
	vtkIdType idCell;

	if ( !this->GetTerminalInformation(t->GetId()) )
		{
		if ( t->GetFirstParent() )
			{
			elem = vtkHM1DSegment::SafeDownCast(t->GetFirstParent());

			info = this->GetElementInformation(elem->GetId(), elem->GetElementNumber()-1);

			idTerminal[0] = info[1];
			idTerminal[1] = t->GetId();
			idTerminal[2] = elem->GetId();

			idCell = this->Terminals->InsertNextCell(3, idTerminal);
			}
		else if ( !t->IsLeaf() )
			{
			elem = vtkHM1DSegment::SafeDownCast(t->GetFirstChild());

			info = this->GetElementInformation(elem->GetId(), 0);

			idTerminal[0] = info[0];
			idTerminal[1] = t->GetId();
			idTerminal[2] = elem->GetId();

			idCell = this->Terminals->InsertNextCell(3, idTerminal);
			}

			this->TerminalInformation.insert(TerminalInformationPair(t->GetId(),idCell));
		}
}

//----------------------------------------------------------------------------
/*
 * Este método não esta funcionando corretamente, não está atualizado.
 * A remoção de segmentos e pontos é feita pela atualização de todo o grid,
 * todo o grid é deletado e refeito baseado no StraightModel.
 * Isso é feito porque o desempenho é muito superior ao ser deletado
 * apenas um segmento e atualização das informações.
 */
void vtkHM1DStraightModelGrid::RemovePoint(vtkIdType ptId, vtkIdType cellId)
{
	unsigned short int ncells;
  vtkIdType *cells;
  vtkIdType n,*p, *p2;
	int point;

	//  vtkCellArray para Linhas
	vtkCellArray *newLines = NULL;
	//  vtkCellArray para Segmentos
	vtkCellArray *newSegments = NULL;

	if ( this->Lines )
		{
		//Pegando os arrays do grid
		this->Radius	 									= vtkDoubleArray::SafeDownCast(this->GetPointData()->GetArray("Radius"));
		this->Elastin 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Elastin"));
		this->Collagen 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Collagen"));
		this->CoefficientA							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientA"));
		this->CoefficientB							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientB"));
		this->Viscoelasticity						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Viscoelasticity"));
		this->ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ViscoelasticityExponent"));
		this->InfiltrationPressure			= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("InfiltrationPressure"));
		this->ReferencePressure					= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ReferencePressure"));
		this->Permeability							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Permeability"));


		this->NullResistance 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("NullResistance"));
		this->R1 												= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R1"));
		this->R2 												= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R2"));
		this->Cap 											= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Cap"));
		this->Pressure 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Pressure"));
		this->SpeedOfSound							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("SpeedOfSound"));


		//Remove de cada array a tupla referente ao ponto a ser removido
		this->Radius->RemoveTuple(ptId);
		this->Elastin->RemoveTuple(cellId);
		this->Collagen->RemoveTuple(cellId);
		this->CoefficientA->RemoveTuple(cellId);
		this->CoefficientB->RemoveTuple(cellId);
		this->Viscoelasticity->RemoveTuple(cellId);
		this->ViscoelasticityExponent->RemoveTuple(cellId);
		this->InfiltrationPressure->RemoveTuple(cellId);
		this->ReferencePressure->RemoveTuple(cellId);
		this->Permeability->RemoveTuple(cellId);



		this->NullResistance->RemoveTuple(cellId);
		this->R1->RemoveTuple(cellId);
		this->R2->RemoveTuple(cellId);
		this->Cap->RemoveTuple(cellId);
		this->Pressure->RemoveTuple(cellId);
		this->SpeedOfSound->RemoveTuple(cellId);

		//Deleta a referencia na celula
	  this->RemoveReferenceToCell(ptId, cellId);

		//Marca a celula que sera removida
	  this->DeleteCell(cellId);

	  //  Criando vtkCellArray para Linhas
		newLines = vtkCellArray::New();

		newSegments = vtkCellArray::New();

		vtkIdList *listElement = vtkIdList::New();
		vtkIdType idLine;
		vtkIdType idSeg;

		point = 0*this->Lines->GetMaxCellSize() + 0;
		this->Lines->GetCell(point,n,p);
		idSeg = p[3];
		this->RemoveSegmentInformation(idSeg);

		for ( int i=0; i<this->Lines->GetNumberOfCells(); i++ )
			{
			if ( i!= cellId )
				{
				//Pega as informacoes da linha e as adiciona como nova linha
				point = i*this->Lines->GetMaxCellSize() + i;
				this->Lines->GetCell(point,n,p);

				//Pega as informacoes do segmento e as adiciona como novo segmento
				point = i*this->Segments->GetMaxCellSize() + i;
				this->Segments->GetCell(point,n,p2);

				//Caso i seja maior que o id da celeula removida, passo a diminuir um
				// no id do ponto ao qual a celula esta associada, pois sera removido
				// um ponto e a celula ficaria desatualizada.
				if ( i > cellId )
					{
					p[0]--;
					p[1]--;
					p2[0]--;
					p2[1]--;
					}

				newLines->InsertNextCell(2,p);
				idLine = newSegments->InsertNextCell(3,p2);

				if ( idSeg != p2[3] )
					{
					if ( listElement->GetNumberOfIds() > 0 )
						{
						this->InsertSegmentInformation(idSeg, listElement);
						listElement = vtkIdList::New();
						}

					idSeg = p[3];
					this->RemoveSegmentInformation(idSeg);
					}

				listElement->InsertNextId(idLine);

				}
			}

		if ( listElement->GetNumberOfIds() > 0 )
			this->InsertSegmentInformation(idSeg, listElement);

		newLines->SetNumberOfCells(this->Lines->GetNumberOfCells()-1);
		newSegments->SetNumberOfCells(this->Segments->GetNumberOfCells()-1);

		}  // End if ( this->Lines )

  vtkIdType numNewPts, totalPts;

  vtkPoints *newPts = vtkPoints::New();
	vtkCellArray *newVertices = NULL;

  totalPts = this->GetNumberOfPoints();

  numNewPts = totalPts - 1;

	vtkPoints *points = this->GetPoints();

  // Copy points in place
	if ( this->Verts )
	  {
	  vtkIdType idPt;
	  newVertices = vtkCellArray::New();
	  for (int i=0; i<totalPts; i++)
	    {
			if ( i!=ptId)
				{
	      idPt = newPts->InsertNextPoint(points->GetPoint(i));
	      newVertices->InsertNextCell(1, &idPt);
				}
	    }
		newVertices->SetNumberOfCells(numNewPts);
	  }
  else
	  {
	  for (int i=0; i<totalPts; i++)
	    {
			if ( i!=ptId)
	      newPts->InsertNextPoint(points->GetPoint(i));
	    }
	  }

	newPts->SetNumberOfPoints(numNewPts);
	this->GetCellData()->Update();

	vtkCellArray *newNodes = NULL;
  if ( (this->Nodes) && (this->NodeInformation.size() > 0) )
	  {
	  newNodes = vtkCellArray::New();
	  vtkIdType *info;
	  vtkIdType idNode[3];
	  vtkIdType idPt=0;

	  vtkIdList *listNode = vtkIdList::New();
		vtkIdType idNo;
		vtkIdType idSeg;

		point = 0*this->Nodes->GetMaxCellSize() + 0;
		this->Nodes->GetCell(point,n,p);
		idSeg = p[2];
		this->RemoveNodeInformation(idSeg);

	  // Copy nodes in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( i!=ptId )
	      {
	      info = this->GetNode(i);
	      idNode[0] = idPt;
				idNode[1] = info[1];
				idNode[2] = info[2];
				idNo = newNodes->InsertNextCell(3, idNode);

				if ( idSeg != info[2] )
					{
					if ( listNode->GetNumberOfIds() > 0 )
						{
						this->InsertNodeInformation(idSeg, listNode);
						listNode = vtkIdList::New();

						}
					idSeg = info[2];
					this->RemoveNodeInformation(idSeg);
					}

				listNode->InsertNextId(idNo);

				idPt++;
				}
	    }

	  if ( listNode->GetNumberOfIds() > 0 )
			this->InsertNodeInformation(idSeg, listNode);

	  newNodes->SetNumberOfCells(numNewPts);
	  }
	else if ( this->Nodes )
		{
		newNodes = vtkCellArray::New();
	  vtkIdType *info;
	  vtkIdType idNode[3];
	  vtkIdType idPt=0;

	  // Copy nodes in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( i!=ptId )
	      {
	      info = this->GetNode(i);
	      idNode[0] = idPt;
				idNode[1] = info[1];
				idNode[2] = info[2];
				newNodes->InsertNextCell(3, idNode);

				idPt++;
				}
	    }
	  newNodes->SetNumberOfCells(numNewPts);
		}

	//Caso exista cell array de terminais, atualizo sua associacao com os pontos
	if ( this->Terminals )
		{
		int k=0;
		for ( k=0; k<this->GetNumberOfTerminals(); k++ )
			{
			point = k*this->Terminals->GetMaxCellSize() + k;
		 	this->Terminals->GetCell(point, n, p);
		 	if ( ptId <= p[0] )
		 		break;
			}

		for ( int i=k+1; i<this->GetNumberOfTerminals(); i++ )
			{
			point = i*this->Terminals->GetMaxCellSize() + i;
		 	this->Terminals->GetCell(point, n, p);
		 	p[0]--;
			}

		}

  this->Points->DeepCopy(newPts);

  newPts->Delete();
  if ( newLines )
	  {
	  this->Lines->DeepCopy(newLines);
  	this->Segments->DeepCopy(newSegments);
	  newLines->Delete();
	  newSegments->Delete();
	  }

	if ( newVertices )
		{
		this->Verts->DeepCopy(newVertices);
		newVertices->Delete();
		}

	if ( newNodes )
		{
		this->Nodes->DeepCopy(newNodes);
		newNodes->Delete();
		}
}

//----------------------------------------------------------------------------
/*
 * Este método não esta funcionando corretamente, não está atualizado.
 * A remoção de segmentos e pontos é feita pela atualização de todo o grid,
 * todo o grid é deletado e refeito baseado no StraightModel.
 * Isso é feito porque o desempenho é muito superior ao ser deletado
 * apenas um segmento e atualização das informações.
 */
void vtkHM1DStraightModelGrid::RemovePoint(vtkIdList *ptList, vtkIdList *cellList)
{
	unsigned short int ncells;
  vtkIdType *cells;
  int totalCellsRemove = cellList->GetNumberOfIds();
	vtkIdType *map, numNewPts, totalPts, totalCells, maxPt;

	vtkIdType n,*p;
	int point;

	//  vtkCellArray para Linhas
	vtkCellArray *newLines = NULL;
	//  vtkCellArray para Segmentos
	vtkCellArray *newSegments = NULL;

	if ( this->Lines )
		{
		//Pegando os arrays de propriedades do grid
		this->Elastin 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Elastin"));
		this->Collagen 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Collagen"));
		this->CoefficientA							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientA"));
		this->CoefficientB							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("CoefficientB"));
		this->Viscoelasticity						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Viscoelasticity"));
		this->ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ViscoelasticityExponent"));
		this->InfiltrationPressure			= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("InfiltrationPressure"));
		this->ReferencePressure					= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("ReferencePressure"));
		this->Permeability							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Permeability"));


		this->NullResistance 						= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("NullResistance"));
		this->R1 												= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R1"));
		this->R2 												= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("R2"));
		this->Cap 											= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Cap"));
		this->Pressure 									= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("Pressure"));
		this->SpeedOfSound							= vtkDoubleArray::SafeDownCast(this->GetCellData()->GetArray("SpeedOfSound"));


		maxPt = cellList->GetId(0);
		totalCells = this->Lines->GetNumberOfCells();

		for ( int i=ptList->GetNumberOfIds()-1; i>=0; i-- )
			{
			vtkIdType c;

			this->GetPointCells(ptList->GetId(i),ncells,cells);

			if ( ncells == 2 )
				{
				if ( cells[0] > cells[1] )
					c = cells[0];
				else
					c = cells[1];
				}
			else
				c = cells[0];

			if ( maxPt < c )
				maxPt = c;

			for ( int j=c; j<totalCells; j++ )
				{
				//Pega os pontos da celula e os atualiza
				point = j*this->Lines->GetMaxCellSize() + j;
				this->Lines->GetCell(point,n,p);

				p[0]--;
				p[1]--;

				//Pega os pontos da celula e os atualiza
				point = j*this->Segments->GetMaxCellSize() + j;
				this->Segments->GetCell(point,n,p);
				p[0]--;
				p[1]--;
				}

			}  // End for ( int i=ptList->GetNumberOfIds()-1; i>=0; i-- )

		//Marca a celula que sera removida
		for ( int i=cellList->GetNumberOfIds()-1; i>=0; i-- )
			{
		  this->DeleteCell(cellList->GetId(i));

		  //Remove de cada array a tupla referente a linha a ser removida
			this->Elastin->RemoveTuple(cellList->GetId(i));
			this->Collagen->RemoveTuple(cellList->GetId(i));
			this->CoefficientA->RemoveTuple(cellList->GetId(i));
			this->CoefficientB->RemoveTuple(cellList->GetId(i));
			this->Viscoelasticity->RemoveTuple(cellList->GetId(i));
			this->ViscoelasticityExponent->RemoveTuple(cellList->GetId(i));
			this->InfiltrationPressure->RemoveTuple(cellList->GetId(i));
			this->ReferencePressure->RemoveTuple(cellList->GetId(i));
			this->Permeability->RemoveTuple(cellList->GetId(i));


			this->NullResistance->RemoveTuple(cellList->GetId(i));
			this->R1->RemoveTuple(cellList->GetId(i));
			this->R2->RemoveTuple(cellList->GetId(i));
			this->Cap->RemoveTuple(cellList->GetId(i));
			this->Pressure->RemoveTuple(cellList->GetId(i));
			this->SpeedOfSound->RemoveTuple(cellList->GetId(i));
			}

		//  Criando vtkCellArray para Linhas
		newLines = vtkCellArray::New();

		newSegments = vtkCellArray::New();

		vtkIdList *listElement = vtkIdList::New();
		vtkIdType idLine;
		vtkIdType idSeg;

		point = 0*this->Lines->GetMaxCellSize() + 0;
		this->Lines->GetCell(point,n,p);
		idSeg = p[3];
		this->RemoveSegmentInformation(idSeg);

		for ( int i=0; i<totalCells; i++ )
			{
			if ( !this->VerifyRemoveCell(cellList, i) )
				{
				//Pega as informacoes da linha e as adiciona como nova linha
				point = i*this->Lines->GetMaxCellSize() + i;
				this->Lines->GetCell(point,n,p);
				newLines->InsertNextCell(n,p);

				//Pega as informacoes do segmento e as adiciona como novo segmento
				point = i*this->Segments->GetMaxCellSize() + i;
				this->Segments->GetCell(point,n,p);
				idLine = newSegments->InsertNextCell(n,p);

				if ( idSeg != p[3] )
					{
					if ( listElement->GetNumberOfIds() > 0 )
						{
						this->InsertSegmentInformation(idSeg, listElement);
						listElement = vtkIdList::New();

						}
					idSeg = p[3];
					this->RemoveSegmentInformation(idSeg);
					}

				listElement->InsertNextId(idLine);

				}
			}

		if ( listElement->GetNumberOfIds() > 0 )
			this->InsertSegmentInformation(idSeg, listElement);

		//Seta o numero de celulas
		newLines->SetNumberOfCells(this->Lines->GetNumberOfCells()-totalCellsRemove);
		newSegments->SetNumberOfCells(this->Segments->GetNumberOfCells()-totalCellsRemove);

		}  // End if ( this->Lines )


	////////////////////////////////////////////////
	// Points
  vtkPoints *newPts = vtkPoints::New();
  totalPts = this->GetNumberOfPoints();

  map = new vtkIdType[totalPts];

  numNewPts = totalPts;

  for (int i=0; i<totalPts; i++)
    map[i] = 1;

  this->Radius = vtkDoubleArray::SafeDownCast(this->GetPointData()->GetArray("Radius"));
  for ( int j=0; j<ptList->GetNumberOfIds(); j++ )
		{
		this->Radius->RemoveTuple(ptList->GetId(j));
		map[ptList->GetId(j)] = -1;
		numNewPts--;
		}
	vtkPoints *points = this->GetPoints();
	vtkCellArray *newVertices = NULL;

  if ( this->Verts )
	  {
	  newVertices = vtkCellArray::New();
	  vtkIdType idPt;
	  // Copy points in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( map[i] > -1 )
	      {
	      idPt = newPts->InsertNextPoint(points->GetPoint(i));
				newVertices->InsertNextCell(1, &idPt);
				}
	    }
	  newVertices->SetNumberOfCells(numNewPts);
	  }
  else
	  {
	  // Copy points in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( map[i] > -1 )
	      newPts->InsertNextPoint(points->GetPoint(i));
	    }
	  }

  vtkCellArray *newNodes = NULL;
  if ( (this->Nodes) && (this->NodeInformation.size() > 0) )
	  {
	  newNodes = vtkCellArray::New();
	  vtkIdType *info;
	  vtkIdType idNode[3];
	  vtkIdType idPt=0;

	  vtkIdList *listNode = vtkIdList::New();
		vtkIdType idNo;
		vtkIdType idSeg;

		point = 0*this->Nodes->GetMaxCellSize() + 0;
		this->Nodes->GetCell(point,n,p);
		idSeg = p[2];
		this->RemoveNodeInformation(idSeg);

	  // Copy nodes in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( map[i] > -1 )
	      {
	      info = this->GetNode(i);
	      idNode[0] = idPt;
				idNode[1] = info[1];
				idNode[2] = info[2];
				idNo = newNodes->InsertNextCell(3, idNode);

				if ( idSeg != info[2] )
					{
					if ( listNode->GetNumberOfIds() > 0 )
						{
						this->InsertNodeInformation(idSeg, listNode);
						listNode = vtkIdList::New();
						}

					idSeg = info[2];
					this->RemoveNodeInformation(idSeg);
					}

				listNode->InsertNextId(idNo);

				idPt++;
				}
	    }

	  if ( listNode->GetNumberOfIds() > 0 )
			this->InsertNodeInformation(idSeg, listNode);

	  newNodes->SetNumberOfCells(numNewPts);
	  }
	else if ( this->Nodes )
		{
		newNodes = vtkCellArray::New();
	  vtkIdType *info;
	  vtkIdType idNode[3];
	  vtkIdType idPt=0;

	  // Copy nodes in place
	  for (int i=0; i < totalPts; i++)
	    {
	    if ( map[i] > -1 )
	      {
	      info = this->GetNode(i);
	      idNode[0] = idPt;
				idNode[1] = info[1];
				idNode[2] = info[2];
				newNodes->InsertNextCell(3, idNode);

				idPt++;
				}
	    }
	  newNodes->SetNumberOfCells(numNewPts);
		}

  //seta a nova quantidade de pontos
  newPts->SetNumberOfPoints(numNewPts);

	///////////////////////////////////////////////////////////////////////////
	//Caso exista cell array de terminais, atualizo sua associacao com os pontos
	if ( this->Terminals )
		{
		p = this->VerifyTerminalCell(ptList);

		if ( p )
			{
			vtkIdType *pt;
			for ( int i=0; i<this->GetNumberOfTerminals(); i++ )
				{
				pt = this->GetTerminal(i);

				if ( pt[0] > p[0] )
					pt[0] -= ptList->GetNumberOfIds();
				}
			}
		}

	this->GetCellData()->Update();

  delete [] map;

  this->Points->DeepCopy(newPts);

  newPts->Delete();
  if ( newLines )
	  {
	  this->Lines->DeepCopy(newLines);
  	this->Segments->DeepCopy(newSegments);
	  newLines->Delete();
	  newSegments->Delete();
	  }

  if ( newVertices )
  	{
  	this->Verts->DeepCopy(newVertices);
  	newVertices->Delete();

  	}
  if ( newNodes )
  	{
  	this->Nodes->DeepCopy(newNodes);
  	newNodes->Delete();
  	}

}

//----------------------------------------------------------------------------
bool vtkHM1DStraightModelGrid::VerifyRemoveCell(vtkIdList *cellList, vtkIdType id)
{
	vtkIdType n,*p;
	int point;

	for ( int i=0; i<cellList->GetNumberOfIds(); i++ )
		{
		//verifica se o id esta na lista de celulas a serem removidas do grid
		if ( cellList->GetId(i) == id )
			{
			//deleta o id da lista
			cellList->DeleteId(id);
			return true;
			}
		}

	return false;
}

vtkIdType *vtkHM1DStraightModelGrid::VerifyTerminalCell(vtkIdList *ptList)
{
	vtkIdType *p;

	for ( int i=0; i<this->Terminals->GetNumberOfCells(); i++ )
		{
		p = this->GetTerminal(i);

		for ( int j=0; j<ptList->GetNumberOfIds(); j++ )
			{
		 	if ( p[0] == ptList->GetId(j) )
			 	{
			 	return p;
			 	}
			}
		}
	return 0;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveTerminalCell(vtkIdType idTerminal)
{
	int point;
	vtkIdType n, *pt;

	if ( this->Verts )
		{
		if ( this->Terminals )
			{
			TerminalInformationMap::iterator it = this->TerminalInformation.find(idTerminal);
			//verifica se o terminal existe no grid
			if ( it == this->TerminalInformation.end() )
				return;

			vtkCellArray *newTerminals = vtkCellArray::New();
			vtkCellArray *newVerts = vtkCellArray::New();
			vtkPoints *newPoints = vtkPoints::New();

			this->TerminalInformation.clear();

			vtkIdType id[3];

		 	for ( int i=0; i<this->Terminals->GetNumberOfCells(); i++ )
			 	{
			 	point = i*this->Terminals->GetMaxCellSize() + i;
			 	this->Terminals->GetCell(point, n, pt);
			 	if ( pt[1] != idTerminal )
			 		{
			 		id[0] = newPoints->InsertNextPoint(this->Points->GetPoint(i));
			 		id[1] = pt[1];
			 		id[2] = pt[2];
			 		newTerminals->InsertNextCell(3, id);
		 			newVerts->InsertNextCell(1, id);
		 			this->InsertTerminalInformation(id[1],id[0]);
			 		}
			 	}
		 	this->RemoveTerminalInformation(idTerminal);
			this->Terminals->DeepCopy(newTerminals);
			this->Verts->DeepCopy(newVerts);
			this->Points->DeepCopy(newPoints);
			newVerts->Delete();
			newTerminals->Delete();
			newPoints->Delete();
			}
		}
	else
		{
		if ( this->Terminals )
			{
			vtkCellArray *newTerminals = vtkCellArray::New();
			this->TerminalInformation.clear();
			vtkIdType idCell;

		 	for ( int i=0; i<this->Terminals->GetNumberOfCells(); i++ )
			 	{
			 	point = i*this->Terminals->GetMaxCellSize() + i;
			 	this->Terminals->GetCell(point, n, pt);
			 	if ( pt[1] != idTerminal )
			 		{
		 			idCell = newTerminals->InsertNextCell(3, pt);
		 			this->InsertTerminalInformation(pt[1],idCell);
			 		}
			 	}
		 	this->RemoveTerminalInformation(idTerminal);
			this->Terminals->DeepCopy(newTerminals);
			newTerminals->Delete();
			}
		}
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetLine(vtkIdType CellId)
{
	if ( this->Lines )
		{
		int point;
		vtkIdType n, *pt;

	 	point = CellId*this->Lines->GetMaxCellSize() + CellId;
	 	this->Lines->GetCell(point, n, pt);

	 	return pt;
		}
	return 0;
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetSegment(vtkIdType CellId)
{
	if ( this->Segments )
		{
		int point;
		vtkIdType n, *pt;

	 	point = CellId*this->Segments->GetMaxCellSize() + CellId;
	 	this->Segments->GetCell(point, n, pt);

	 	return pt;
		}
	return 0;
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetTerminal(vtkIdType CellId)
{
	if ( this->Terminals )
		{
		int point;
		vtkIdType n, *pt;

	 	point = CellId*this->Terminals->GetMaxCellSize() + CellId;
	 	this->Terminals->GetCell(point, n, pt);

	 	return pt;
		}
	return 0;
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetNode(vtkIdType CellId)
{
	if ( this->Nodes )
		{
		int point;
		vtkIdType n, *pt;

	 	point = CellId*this->Nodes->GetMaxCellSize() + CellId;
	 	this->Nodes->GetCell(point, n, pt);

	 	return pt;
		}
	return 0;
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DStraightModelGrid::GetSegmentInformation(vtkIdType idSegment)
{
	InformationMap::iterator it = this->SegmentInformation.find(idSegment);

	if ( it == this->SegmentInformation.end() )
		return NULL;
	else
		return it->second;
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetElementInformation(vtkIdType idSegment, vtkIdType idElement)
{
	InformationMap::iterator it = this->SegmentInformation.find(idSegment);

	vtkIdList *list;
	int point;
	vtkIdType n, *pt;

	if ( it == this->SegmentInformation.end() )
		return NULL;
	else
		{
		list = it->second;
		if ( idElement <= list->GetNumberOfIds() )
			{
			point = list->GetId(idElement)*this->Segments->GetMaxCellSize() + list->GetId(idElement);
			this->Segments->GetCell(point, n, pt);
			return pt;
			}
		else
			return NULL;
		}

}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetTerminalInformation(vtkIdType idTerminal)
{
	TerminalInformationMap::iterator it = this->TerminalInformation.find(idTerminal);
	int point;
	vtkIdType n, *pt;

	if ( it == this->TerminalInformation.end() )
		return NULL;
	else
		{
		point = it->second*this->Terminals->GetMaxCellSize() + it->second;
		this->Terminals->GetCell(point, n, pt);
		return pt;
		}
}

//----------------------------------------------------------------------------
vtkIdType *vtkHM1DStraightModelGrid::GetNodeInformation(vtkIdType idSegment, vtkIdType idNode)
{
	InformationMap::iterator it = this->NodeInformation.find(idSegment);
	vtkIdList *list;
	int point;
	vtkIdType n, *pt;

	if ( it == this->NodeInformation.end() )
		return NULL;
	else
		{
		list = it->second;
		if ( idNode <= list->GetNumberOfIds() )
			{
			point = list->GetId(idNode)*this->Nodes->GetMaxCellSize() + list->GetId(idNode);
			this->Nodes->GetCell(point, n, pt);
			return pt;
			}
		else
			return NULL;
		}
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::SetTerminalPoint(vtkIdType idTerminal, vtkIdType idPoint)
{
	TerminalInformationMap::iterator it = this->TerminalInformation.find(idTerminal);
	int point;
	vtkIdType n, *pt;

	if ( it == this->TerminalInformation.end() )
		return;
	else
		{
		point = it->second*this->Terminals->GetMaxCellSize() + it->second;
		this->Terminals->GetCell(point, n, pt);
		pt[0] = idPoint;
		}

//	int point;
//	vtkIdType n, *pt;
//
//	if ( this->Terminals )
//		{
//	 	for ( int i=0; i<this->Terminals->GetNumberOfCells(); i++ )
//		 	{
//		 	point = i*this->Terminals->GetMaxCellSize() + i;
//		 	this->Terminals->GetCell(point, n, pt);
//		 	if ( pt[1] == idTerminal )
//		 		{
//		 		pt[0] = idPoint;
//	 			return;
//		 		}
//		 	}
//		}
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::InsertSegmentInformation(vtkIdType id, vtkIdList *list)
{
	this->SegmentInformation.insert(InformationPair(id,list));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveSegmentInformation(vtkIdType id)
{
	InformationMap::iterator it = this->SegmentInformation.find(id);
	if ( it != this->SegmentInformation.end() )
		{
		it->second->Delete();
		this->SegmentInformation.erase(id);
		}
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveSegmentInformation()
{
	InformationMap::iterator it = this->SegmentInformation.begin();
	while ( it != this->SegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentInformation.clear();
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::InsertNodeInformation(vtkIdType id, vtkIdList *list)
{
	this->NodeInformation.insert(InformationPair(id,list));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveNodeInformation(vtkIdType id)
{
	InformationMap::iterator it = this->NodeInformation.find(id);
	if ( it != this->NodeInformation.end() )
		{
		it->second->Delete();
		this->NodeInformation.erase(id);
		}
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveNodeInformation()
{
	InformationMap::iterator it = this->NodeInformation.begin();
	while ( it != this->NodeInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->NodeInformation.clear();
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::InsertTerminalInformation(vtkIdType id, vtkIdType idPoint)
{
	this->TerminalInformation.insert(TerminalInformationPair(id,idPoint));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveTerminalInformation(vtkIdType id)
{
	TerminalInformationMap::iterator it = this->TerminalInformation.find(id);
	if ( it != this->TerminalInformation.end() )
		{
//		it->second->Delete();
		this->TerminalInformation.erase(id);
		}
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::RemoveTerminalInformation()
{
//	TerminalInformationMap::iterator it = this->TerminalInformation.begin();
//	while ( it != this->TerminalInformation.end() )
//		{
//		it->second->Delete();
//		it++;
//		}
	this->TerminalInformation.clear();
}

void vtkHM1DStraightModelGrid::RemoveAllInformation()
{
	this->RemoveSegmentInformation();
	this->RemoveNodeInformation();
	this->RemoveTerminalInformation();
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::ResetStructure()
{
  if ( this->Segments )
    this->Segments->Reset();

  if ( this->Terminals )
    this->Terminals->Reset();

  if ( this->Nodes)
    this->Nodes->Reset();

  if ( this->Lines )
    this->Lines->Reset();

  if ( this->Points )
    this->Points->Reset();

  if ( this->Radius )
    this->Radius->Reset();
  if ( this->Elastin )
    this->Elastin->Reset();
  if ( this->Collagen )
    this->Collagen->Reset();
  if ( this->CoefficientA )
    this->CoefficientA->Reset();
  if ( this->CoefficientB )
    this->CoefficientB->Reset();
  if ( this->Viscoelasticity )
    this->Viscoelasticity->Reset();
  if ( this->ViscoelasticityExponent )
    this->ViscoelasticityExponent->Reset();
  if ( this->InfiltrationPressure )
    this->InfiltrationPressure->Reset();
  if ( this->ReferencePressure )
    this->ReferencePressure->Reset();
  if ( this->Permeability )
    this->Permeability->Reset();

  if ( this->NullResistance )
    this->NullResistance->Reset();
  if ( this->R1 )
    this->R1->Reset();
  if ( this->R2 )
    this->R2->Reset();
  if ( this->Cap )
    this->Cap->Reset();
  if ( this->Pressure )
    this->Pressure->Reset();

  if (this->SpeedOfSound)
    this->SpeedOfSound->Reset();

  this->RemoveAllInformation();
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelGrid::UpdateGrid()
{
//	cout << "vtkHM1DStraightModelGrid::UpdateGrid()" << endl;
//	this->Segments->Reset();
//
//	this->Terminals->Reset();
//
//	this->Nodes->Reset();
//
//	this->Lines->Reset();
//
//	this->Points->Reset();
//
//	if ( this->Radius )
//		this->Radius->Reset();
//	if ( this->Elastin )
//		this->Elastin->Reset();
//	if ( this->Collagen )
//		this->Collagen->Reset();
//	if ( this->CoefficientA )
//		this->CoefficientA->Reset();
//	if ( this->CoefficientB )
//		this->CoefficientB->Reset();
//	if ( this->Viscoelasticity )
//		this->Viscoelasticity->Reset();
//	if ( this->ViscoelasticityExponent )
//		this->ViscoelasticityExponent->Reset();
//	if ( this->InfiltrationPressure )
//		this->InfiltrationPressure->Reset();
//	if ( this->ReferencePressure )
//		this->ReferencePressure->Reset();
//	if ( this->Permeability )
//		this->Permeability->Reset();
//
//	if ( this->NullResistance )
//		this->NullResistance->Reset();
//	if ( this->R1 )
//		this->R1->Reset();
//	if ( this->R2 )
//		this->R2->Reset();
//	if ( this->Cap )
//		this->Cap->Reset();
//	if ( this->Pressure )
//		this->Pressure->Reset();
//
//	if (this->SpeedOfSound)
//		this->SpeedOfSound->Reset();
//
//	this->RemoveAllInformation();

  this->ResetStructure();

	this->MakeStraightModelGrid();
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelGrid::MakeStraightModelGrid()
{
	vtkDebugMacro(<<"Make vtkHM1DStraightModelGrid.");

	if ( this->StraightModel )
		{
		// Creating vtkPoints
		if ( !this->Points )
			this->Points = vtkPoints::New();

		//  Criando vtkCellArray para Linhas
		if ( !this->Lines )
			this->Lines = vtkCellArray::New();

		// Criando vtkCellArray para terminais
		if ( !this->Terminals )
			this->Terminals = vtkCellArray::New();

		//  Creando vtkCellArray para os elementos
		if ( !this->Segments )
			this->Segments = vtkCellArray::New();

		// Creating vtkCellArray for vertices
		if ( !this->Nodes )
			this->Nodes = vtkCellArray::New();

		vtkDebugMacro(<<"Walking vtkHM1DStraightModel to set segments and terminals in vtkHM1DStraightModelSource.");

		vtkHM1DTreeElement *elem;
		vtkHM1DTreeElement *child;

		vtkHM1DStraightModel::TreeNodeDataMap segMap = this->StraightModel->GetSegmentsMap();
		vtkHM1DStraightModel::TreeNodeDataMap::iterator it = segMap.begin();

		vtkHM1DSegment *s;
		while ( it != segMap.end() )
			{
			s = vtkHM1DSegment::SafeDownCast(it->second);
			this->AddSegment( s );
			it++;
			}

		vtkHM1DStraightModel::TreeNodeDataMap termMap = this->StraightModel->GetTerminalsMap();
		it = termMap.begin();

		vtkHM1DTerminal *t;

		while ( it != termMap.end() )
			{
			t = vtkHM1DTerminal::SafeDownCast(it->second);
			this->AddTerminal( t );
			it++;
			}

		vtkDebugMacro(<<"Creating scalars");

		//Dimensionando o tamanho dos array para o numero de linhas
		this->Radius->Resize(this->Points->GetNumberOfPoints());
		this->Elastin->Resize(this->Lines->GetNumberOfCells());

		this->NullResistance->Resize(this->Lines->GetNumberOfCells());
		this->R1->Resize(this->Lines->GetNumberOfCells());
		this->R2->Resize(this->Lines->GetNumberOfCells());
		this->Cap->Resize(this->Lines->GetNumberOfCells());
		this->Pressure->Resize(this->Lines->GetNumberOfCells());
		this->SpeedOfSound->Resize(this->Lines->GetNumberOfCells());



		this->Collagen->Resize(this->Lines->GetNumberOfCells());
		this->CoefficientA->Resize(this->Lines->GetNumberOfCells());
		this->CoefficientB->Resize(this->Lines->GetNumberOfCells());
		this->Viscoelasticity->Resize(this->Lines->GetNumberOfCells());
		this->ViscoelasticityExponent->Resize(this->Lines->GetNumberOfCells());
		this->InfiltrationPressure->Resize(this->Lines->GetNumberOfCells());
		this->ReferencePressure->Resize(this->Lines->GetNumberOfCells());
		this->Permeability->Resize(this->Lines->GetNumberOfCells());

		this->BuildCells();
		this->BuildLinks();
		}

  return 1;
}

vtkHM1DStraightModelGrid::InformationMap vtkHM1DStraightModelGrid::GetSegmentInformation()
{
	return this->SegmentInformation;
}

vtkHM1DStraightModelGrid::InformationMap vtkHM1DStraightModelGrid::GetNodeInformation()
{
	return this->NodeInformation;
}


vtkHM1DStraightModelGrid::TerminalInformationMap vtkHM1DStraightModelGrid::GetTerminalInformation()
{
	return this->TerminalInformation;
}

void vtkHM1DStraightModelGrid::SetSegmentInformation(InformationMap seg)
{
	InformationMap::iterator it = this->SegmentInformation.begin();
	while ( it != this->SegmentInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->SegmentInformation.clear();

	vtkIdList *list;
	it = seg.begin();
	while ( it != seg.end() )
		{
		list = vtkIdList::New();
		list->DeepCopy(it->second);
		this->SegmentInformation.insert(InformationPair(it->first, list));
		it++;
		}
}

void vtkHM1DStraightModelGrid::SetNodeInformation(InformationMap node)
{
	InformationMap::iterator it = this->NodeInformation.begin();
	while ( it != this->NodeInformation.end() )
		{
		it->second->Delete();
		it++;
		}
	this->NodeInformation.clear();

	vtkIdList *list;
	it = node.begin();
	while ( it != node.end() )
		{
		list = vtkIdList::New();
		list->DeepCopy(it->second);
		this->NodeInformation.insert(InformationPair(it->first, list));
		it++;
		}

}

void vtkHM1DStraightModelGrid::SetTerminalInformation(TerminalInformationMap term)
{
	TerminalInformationMap::iterator it;// = this->TerminalInformation.begin();
//	while ( it != this->TerminalInformation.end() )
//		{
//		it->second->Delete();
//		it++;
//		}
	this->TerminalInformation.clear();

	vtkIdList *list;
	it = term.begin();
	while ( it != term.end() )
		{
//		list = vtkIdList::New();
//		list->DeepCopy(it->second);
		this->TerminalInformation.insert(TerminalInformationPair(it->first, it->second));
		it++;
		}
}
