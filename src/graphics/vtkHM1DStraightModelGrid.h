/*
 * $Id: vtkHM1DStraightModelGrid.h 2884 2010-05-11 12:37:55Z igor $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM1DStraightModelGrid.h,v $

  Copyright (c) Diego Mazala, Igor Freitas
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DStraightModelGrid - concrete dataset represents vtkHM1DStraightModel, vertices, lines, polygons, and triangle strips
// .SECTION Description
// vtkHM1DStraightModelGrid é uma representação visualizável do vtkHM1DStraightModel. Nele estão
// descritos todos os segmentos, terminais e nós que compõem a árvore 1D.
// Esta classe é filha da classe vtkPolyData e só adiciona uma referência a um vtkHM1DStraightModel.
// Toda a estrutura geométrica de visualização permanece igual à vtkPolyData.

// .SECTION See Also
// vtkHM1DStraightModel
// vtkHM1DStraightModelSource
// vtkHM1DStraightModelReader



#ifndef _vtkHM1DStraightModelGrid_
#define _vtkHM1DStraightModelGrid_

#include "vtkPolyData.h"

class vtkHM1DStraightModel;
class vtkCellArray;
class vtkIdList;
class vtkHM1DSegment;
class vtkHM1DTerminal;

#include <map>
//using namespace std;
//#include <list> // List of node data.

class VTK_EXPORT vtkHM1DStraightModelGrid : public vtkPolyData
{
public:
	static vtkHM1DStraightModelGrid *New();

	vtkTypeRevisionMacro(vtkHM1DStraightModelGrid, vtkPolyData);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Set/Get the StraightModel object
	void SetStraightModel( vtkHM1DStraightModel* s );
	vtkHM1DStraightModel* GetStraightModel();
	
	// Description:
	// Set/Get Segments
	void SetSegments( vtkCellArray* );
	vtkCellArray* GetSegments();

	// Description:
	// Set/Get Terminal
	void SetTerminals( vtkCellArray* );
	vtkCellArray* GetTerminals();
	
	// Description:
	// Set/Get Nodes
	void SetNodes( vtkCellArray* );
	vtkCellArray* GetNodes();
	
	// Description:
	// Set/Get NullElements1D3D
	void SetNullElements1D3D( vtkCellArray* );
	vtkCellArray* GetNullElements1D3D();
	
	void DeepCopy(vtkDataObject *dataObject);
	void CopyStructure(vtkDataSet *ds);
	
	// Description :
	//Create vtkHM1DStraightModelGrid
	int CreateStraightModelGrid();
	
	// Description :
	// Walk throught StraightModel to get data
	void WalkStraightModelTree( int &id );
	
	// Decription : 
	// Set Scalars
	void SetScalars();
	
	// Description :
	// Add segments to vtkHM1DStraightModelGrid
	void AddSegment(vtkHM1DSegment* s);
	
	void AddTerminal(vtkHM1DTerminal *t);
	
	// Description:
	// Remove one point and cell of the grid
	void RemovePoint(vtkIdType ptId, vtkIdType cellId);
	
	// Description:
	// Remove one list of the point and cell of the grid
	void RemovePoint(vtkIdList *ptList, vtkIdList *cellList);
	
	// Description:
	// Verifica se a celula esta contida na lista de delecao, caso esteja,
	// deleta a celula e atualiza o id dos pontos das celulas seguintes
	bool VerifyRemoveCell(vtkIdList *cellList, vtkIdType id);
	
	// Description:
	// Remove cell terminal
	void RemoveTerminalCell(vtkIdType idTerminal);
	
	// Description:
	// Get number of terminals cell
	int GetNumberOfTerminals();
	
	// Description:
	// Get informations of the cell line
	// First and second point of the cell.
	vtkIdType *GetLine(vtkIdType CellId);
	
	// Description:
	// Get informations of the cell segment
	// First and second point of the cell, element id and segment id
	vtkIdType *GetSegment(vtkIdType CellId);
	
	// Description:
	// Get informations of the cell terminal
	// Point of the cell, terminal id and parent segment id
	vtkIdType *GetTerminal(vtkIdType CellId);
	
	// Description:
	// Get informations of the cell node
	// Point of the cell, node id and segment id
	vtkIdType *GetNode(vtkIdType CellId);
	
	// Description:
	// Get informations of the segment
	// Return list of cells associated with segment
	vtkIdList *GetSegmentInformation(vtkIdType idSegment);
	
	// Description:
	// Get informations of the segment
	// First and second point of the cell, element id and segment id
	vtkIdType *GetElementInformation(vtkIdType idSegment, vtkIdType idElement);
	
	// Description:
	// Get informations of the terminal
	// Point of the terminal and terminal id
	vtkIdType *GetTerminalInformation(vtkIdType idTerminal);
	
	// Description:
	// Get informations of the node
	// Point of the cell, node id and segment id
	vtkIdType *GetNodeInformation(vtkIdType idSegment, vtkIdType idNode);
	
	// Description:
	// Verify whicth point
	vtkIdType *VerifyTerminalCell(vtkIdList *ptList);
	
	// Description:
	void SetTerminalPoint(vtkIdType idTerminal, vtkIdType idPoint);
	
	// 
	//BTX
	// Description:
	// Map with information of the segment and id of cells associated
  typedef std::map<vtkIdType,vtkIdList*> InformationMap;
  typedef std::pair<vtkIdType,vtkIdList*> InformationPair;
  
  // Description:
  // Map with information of the terminal and id of point associated
  typedef std::map<vtkIdType,vtkIdType> TerminalInformationMap;
  typedef std::pair<vtkIdType,vtkIdType> TerminalInformationPair;
  //ETX
  
  // Description:
  // Insert in map information of the segment with cells associated
  void InsertSegmentInformation(vtkIdType id, vtkIdList *list);
	
	// Description:
  // Remove of the map information of the segment with cells associated
	void RemoveSegmentInformation(vtkIdType id);
	
	// Description:
  // Remove of the map all information of the segment with cells associated
	void RemoveSegmentInformation();
	
	// Description:
  // Insert in map information of the nodes with cells associated
  void InsertNodeInformation(vtkIdType id, vtkIdList *list);
	
	// Description:
  // Remove of the map information of the nodes with cells associated
	void RemoveNodeInformation(vtkIdType id);
	
	// Description:
  // Remove of the map all information of the nodes with cells associated
	void RemoveNodeInformation();
	
	// Description:
  // Insert in map information of the terminals with cells associated
  void InsertTerminalInformation(vtkIdType id, vtkIdType idPoint);
	
	// Description:
  // Remove of the map information of the terminals with cells associated
	void RemoveTerminalInformation(vtkIdType id);
	
	// Description:
  // Remove of the map all information of the terminals with cells associated
	void RemoveTerminalInformation();
	
	// Description:
  // Remove of the maps all information of the segments, nodes and terminals with cells associated
	void RemoveAllInformation();
	
	// Description:
	//
	void UpdateGrid();
	
	int MakeStraightModelGrid();
	
	//BTX
	void SetSegmentInformation(InformationMap);
	void SetNodeInformation(InformationMap);
	void SetTerminalInformation(TerminalInformationMap);
	
	
	InformationMap GetSegmentInformation();
	InformationMap GetNodeInformation();
	TerminalInformationMap GetTerminalInformation();
	//ETX
	
	void ResetStructure();
	
protected:
	// Description:
	// Objeto que decreve o modelo StraightModel
	vtkHM1DStraightModel* StraightModel;

	// Description:
	// Contém os índices dos pontos que compõem cada elemento, id do elemento e do segmento
	vtkCellArray*					Segments;
	// Description:
	// Contém os índices dos pontos que compõem cada terminal, id do terminal e do segmento pai
	vtkCellArray*					Terminals;
	
	// Description:
	// Contém os índices dos pontos que compõem cada terminal
	vtkCellArray*					Nodes;
	
	// Description:
	// Contém os índices dos pontos que compõem cada elemento ou terminal anulado no modelo acoplado.
	vtkCellArray* NullElements1D3D;
	
	// Description:
	// Contains values to radius for each node of the segment 
	// of the tree
	vtkDoubleArray *Radius;
	
	
	// Description:
	// Contains values to Elastin for each element of the segment 
	// of the tree
	vtkDoubleArray *Elastin;
	
	vtkDoubleArray *R1;
	vtkDoubleArray *R2;
	vtkDoubleArray *Cap;
	vtkDoubleArray *Pressure;
	
	vtkDoubleArray *SpeedOfSound;
	
	vtkDoubleArray *NullResistance;
	
	// Description:
	// Contains values to Collagen for each element of the segment 
	// of the tree
	vtkDoubleArray *Collagen;
	
	// Description:
	// Contains values to CoefficientA for each element of the segment 
	// of the tree
	vtkDoubleArray *CoefficientA;
	
	// Description:
	// Contains values to CoefficientB for each element of the segment 
	// of the tree
	vtkDoubleArray *CoefficientB;
	
	// Description:
	// Contains values to Viscoelasticity for each element of the segment 
	// of the tree
	vtkDoubleArray *Viscoelasticity;
	
	// Description:
	// Contains values to Viscoelasticity Exponent for each element of the segment 
	// of the tree
	vtkDoubleArray *ViscoelasticityExponent;
	
	// Description:
	// Contains values to Infiltration Pressure for each element of the segment 
	// of the tree
	vtkDoubleArray *InfiltrationPressure;
	
	// Description:
	// Contains values to Reference Pressure for each element of the segment 
	// of the tree
	vtkDoubleArray *ReferencePressure;
	
	// Description:
	// Contains values to Permeability for each element of the segment 
	// of the tree
	vtkDoubleArray *Permeability;
	
	// Description:
	// Information of the cells of segments
	InformationMap SegmentInformation;
	
	// Description:
	// Information of the cells of nodes
	InformationMap NodeInformation;
	
	// Description:
	// Information of the cells of terminals
	TerminalInformationMap TerminalInformation;
	
	// Description:
	// Information of the null elements or terminals in coupled model.
	InformationMap NullElements1D3DInformation;
	
	// Description:
	// Mark which segment is in main tree
	vtkDoubleArray *MainFlow;
	
protected:
	vtkHM1DStraightModelGrid();
	virtual ~vtkHM1DStraightModelGrid();
	
private:
	vtkHM1DStraightModelGrid(const vtkHM1DStraightModelGrid&); 	// Not Implemented
	void operator=(const vtkHM1DStraightModelGrid&);				// Not Implemented
};

#endif // _vtkHM1DStraightModelGrid_
