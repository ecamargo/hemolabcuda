/*
 * $Id: vtkHM1DStraightModelSource.cxx 2817 2009-05-11 19:42:45Z collares $
 */


#include "vtkHM1DStraightModelSource.h"

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"

#include "vtkCellArray.h"
#include "vtkExecutive.h"
#include "vtkFloatArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelSource, "$Revision: 2817 $");
vtkStandardNewMacro(vtkHM1DStraightModelSource);

// Construct with origin=(0,0,0) and scale factor=1.
vtkHM1DStraightModelSource::vtkHM1DStraightModelSource()
{
//	vtkWarningMacro(<< "vtkHM1DStraightModelSource chamado!");
	
  this->SetNumberOfInputPorts(0);

	this->StraightModel = NULL;
  // Criando o StraightModel
  //this->StraightModel = vtkHM1DStraightModel::New();
  //this->CreateStraightModelTemplate();

	// Criando o StraightModelGrid (output)
	vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::New();	
	this->SetOutput(output);
	// Releasing data for pipeline parallism.
	// Filters will know it is empty. 
	output->ReleaseData();
	output->Delete();

 	//  Criando vtkPoints
	this->Points = vtkPoints::New();

	//  Criando vtkCellArray para Linhas
	this->Lines = vtkCellArray::New();
	
	// Criando vtkCellArray para Vertices
	this->Vertices = vtkCellArray::New();

	//  Criando vtkFloatArray (Scalars)
	this->Scalars = vtkFloatArray::New();
	this->Scalars->SetName("Scalars");
	
	//  Criando vtkIntArray (Int)
	this->Ids = vtkIntArray::New();
	this->Ids->SetName("Ids");
	
	// Este Array é semelhante ao this->Lines, mas com a informação de Id embutida
	this->Segments = vtkCellArray::New();
	// Este Array é semelhante ao this->Nodes, mas com a informação de Id embutida
	this->Terminals = vtkCellArray::New();
}

vtkHM1DStraightModelSource::~vtkHM1DStraightModelSource()
{
	this->Points->Delete();	
	this->Lines->Delete();	
	this->Vertices->Delete();	
	this->Scalars->Delete();
	this->Ids->Delete();
	if ( this->StraightModel )
		this->StraightModel->UnRegister(this);

	this->Segments->Delete();		
	this->Terminals->Delete();
}


//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelSource::GetOutput()
{
  return this->GetOutput(0);
}
//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelSource::GetOutput(int port)
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(port));
}
//----------------------------------------------------------------------------
void vtkHM1DStraightModelSource::SetOutput(vtkDataObject* d)
{
  this->GetExecutive()->SetOutputData(0, d);
}
//----------------------------------------------------------------------------
int vtkHM1DStraightModelSource::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  // now add our info
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
  return 1;
}


//----------------------------------------------------------------------------
// Set StraightModel and register reference
void vtkHM1DStraightModelSource::SetStraightModel(vtkHM1DStraightModel* s)
{
	this->StraightModel = s;
	this->StraightModel->Register(this);
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelSource::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **vtkNotUsed(inputVector),
  vtkInformationVector *outputVector)
{
  // get the info object
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

	vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::SafeDownCast( outInfo->Get(vtkDataObject::DATA_OBJECT()) );

	if ( !this->StraightModel )
	{
		vtkDebugMacro(<<"None StraightModel was setted.");
		this->CreateStraightModelTemplate();
	}

	if ( this->StraightModel )
	{
		int firstId = 0; 
		this->Vertices->InsertNextCell(1, &firstId);
		
		
		vtkDebugMacro(<<"Walking vtkHM1DStraightModel to set segments and terminals in vtkHM1DStraightModelSource.");
		int rootId = this->StraightModel->Get1DTreeRoot()->GetId();
		this->WalkStraightModelTree(rootId);
	
		vtkDebugMacro(<<"Creating scalars");
		this->SetScalars();
			
		output->SetPoints(this->Points);
		output->GetPointData()->SetScalars(this->Scalars);
		
		output->SetLines(this->Lines);	
		
		output->SetVerts(this->Vertices);
		
		rootId = this->StraightModel->Get1DTreeRoot()->GetId();
		this->WalkStraightModelTreeToGetId(rootId);
		output->GetPointData()->AddArray( this->Ids );
		
		output->SetStraightModel(this->StraightModel);
		output->SetSegments(this->Segments);
		output->SetTerminals(this->Terminals);		
	}

		
  return 1;
}


//----------------------------------------------------------------------------
// Set scalars
void vtkHM1DStraightModelSource::SetScalars()
{
	for (int i=0; i<this->Points->GetNumberOfPoints(); i++)
		this->Scalars->InsertNextValue(1.0);	
}






//----------------------------------------------------------------------------
// Adicionar segmentos
void vtkHM1DStraightModelSource::AddSegment(vtkHM1DSegment* s)
{
	vtkIdType idPts[3];
	int nPoints = s->GetElementNumber()+1; // Number of nodes
	vtkIdType idNode, id, idTerminal[2];
	vtkHM1DTreeElement *child;
	
	if ( nPoints > 0 )
	{
		double p[3];
		p[0] = s->GetNodeData(0)->coords[0];
		p[1] = s->GetNodeData(0)->coords[1];
		p[2] =    0;
		// Adicionando Id do ponto (x,y) inicial
		idPts[0] = this->Points->InsertNextPoint(p);

		for (int i=1; i<nPoints; i++)
		{
			double pt[3];
			pt[0] = s->GetNodeData(i)->coords[0];
			pt[1] = s->GetNodeData(i)->coords[1];
			pt[2] =    0;
			// Adicionando Id do ponto (x,y) final
			idPts[1] = this->Points->InsertNextPoint(pt);
			// Adicionando Id elemento no StraightModel
			idPts[2] = s->GetId();

			// Adiciona ao vetor de linhas do PolyData
			this->Lines->InsertNextCell(2,idPts);
			// Adiciona ao vetor de segmentos do PolyData para o StraightModel
			this->Segments->InsertNextCell(3,idPts);

			idPts[0] = idPts[1];
		}
	}	
	// Adiciona o Id do ponto (x,y) do terminal
	idNode = idTerminal[0] = idPts[1];
	this->Vertices->InsertNextCell(1, &idNode);
	

	// Aqui, é necessário testar a existência do terminal para pegar seu Id
	// Um segmento pode ter um terminal ou outro segmento como filho
	child = s->GetFirstChild();
	id = child->GetId();
	// Se o primeiro filho é o terminal, é só pegar seu id
	// Senão, é preciso percorrer todos os filhos do segmento para ver se há algum terminal
	// Caso este segmento não possua nenhum terminal como filho, o id do terminal será -1
	if ( this->StraightModel->GetTerminal(id) )
	{
		idTerminal[1] = id;
	}
	else
	{
		idTerminal[1] = -1;
		child = s->GetNextChild();
		while ( child )
		{
			id = child->GetId();
			// Testa se o Id do filho é terminal
			if ( this->StraightModel->GetTerminal(id) )
				idTerminal[1] = id;
			else
				idTerminal[1] = -1;
				
			child = s->GetNextChild();
		}
	}
	this->Terminals->InsertNextCell(2, idTerminal);
}





//----------------------------------------------------------------------------
// Percorrer o StraightModel para gerar segmentos
void vtkHM1DStraightModelSource::WalkStraightModelTree( int &id )
{
	vtkHM1DTreeElement *elem;
	vtkHM1DTreeElement *child;
	
	
	if (this->StraightModel->IsSegment(id))
	{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		this->AddSegment( vtkHM1DSegment::SafeDownCast( elem ) );
	}
	else if (this->StraightModel->IsTerminal(id))
	{
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
	}
	
	if (!child)
	{
		// É uma folha
		return;
	}
	else
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
		{
			id = child->GetId();
			child = elem->GetNextChild();
			this->WalkStraightModelTree(id);
		}
}


//----------------------------------------------------------------------------
// Percorrer o StraightModel para gerar segmentos
void vtkHM1DStraightModelSource::WalkStraightModelTreeToGetId( int &id )
{
	vtkHM1DTreeElement *elem;
	vtkHM1DTreeElement *child;

		
	
	if (this->StraightModel->IsSegment(id))
	{
		// Inserindo Id do elemento dentro do Array do PolyData
		this->Ids->InsertNextValue(id);
		elem = this->StraightModel->GetSegment(id);
	}
	else if (this->StraightModel->IsTerminal(id))
	{
		// Inserindo Id do elemento dentro do Array do PolyData
		this->Ids->InsertNextValue(id);
		elem = this->StraightModel->GetTerminal(id);
	}

	child = elem->GetFirstChild();
	
	if (!child)
	{
		// É uma folha
		return;
	}
	else
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
		{
			id = child->GetId();
			child = elem->GetNextChild();
			this->WalkStraightModelTreeToGetId(id);
		}
}



//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData1(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] = 0.0; 
	node0->coords[1] = 1.0;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = 0.0; 
	node1->coords[1] = 0.75;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = 0.0; 
	node2->coords[1] = 0.5;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData2(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] =  0.0; 
	node0->coords[1] =  0.5;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = 0.1; 
	node1->coords[1] = 0.4;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = 0.2; 
	node2->coords[1] = 0.3;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMNodeData *node3 = new vtkHMNodeData();
	node3->coords[0] = 0.3; 
	node3->coords[1] = 0.2;

	tmpSegm->SetNodeData(3,*node3);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData3(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] =  0.0; 
	node0->coords[1] =  0.5;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = -0.1; 
	node1->coords[1] =  0.4;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = -0.2; 
	node2->coords[1] =  0.3;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMNodeData *node3 = new vtkHMNodeData();
	node3->coords[0] = -0.3; 
	node3->coords[1] =  0.2;

	tmpSegm->SetNodeData(3,*node3);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData4(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] = 0.0; 
	node0->coords[1] = 0.5;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = 0.0; 
	node1->coords[1] = 0.25;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = 0.0; 
	node2->coords[1] = 0.0;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData5(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] =  0.0; 
	node0->coords[1] =  0.0;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] =  0.1; 
	node1->coords[1] = -0.2;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] =  0.2; 
	node2->coords[1] = -0.4;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMNodeData *node3 = new vtkHMNodeData();
	node3->coords[0] =  0.3; 
	node3->coords[1] = -0.6;

	tmpSegm->SetNodeData(3,*node3);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Função para adicionar nodes ao segmento.
void AddNodeData6(vtkHM1DSegment *tmpSegm)
{
	vtkHMNodeData *node0 = new vtkHMNodeData();
	node0->coords[0] =  0.0; 
	node0->coords[1] =  0.0;

	tmpSegm->SetNodeData(0,*node0);

	vtkHMNodeData *node1 = new vtkHMNodeData();
	node1->coords[0] = -0.1; 
	node1->coords[1] = -0.2;

	tmpSegm->SetNodeData(1,*node1);
	
	vtkHMNodeData *node2 = new vtkHMNodeData();
	node2->coords[0] = -0.2; 
	node2->coords[1] = -0.4;

	tmpSegm->SetNodeData(2,*node2);
	
	vtkHMNodeData *node3 = new vtkHMNodeData();
	node3->coords[0] = -0.3; 
	node3->coords[1] = -0.6;

	tmpSegm->SetNodeData(3,*node3);
	
	vtkHMElementData* elem0 = new vtkHMElementData();
	tmpSegm->SetElementData(0,*elem0);
	vtkHMElementData* elem1 = new vtkHMElementData();
	tmpSegm->SetElementData(1,*elem1);
	vtkHMElementData* elem2 = new vtkHMElementData();
	tmpSegm->SetElementData(2,*elem2);
}
//----------------------------------------------------------------------------
// Criando um StraightModel template
void vtkHM1DStraightModelSource::CreateStraightModelTemplate()
{
	if ( !this->StraightModel )
	{
		vtkDebugMacro(<<"Creating a new StraightModel template");
		
		this->StraightModel = vtkHM1DStraightModel::New();
	
		vtkHM1DTerminal *root = (vtkHM1DTerminal *)this->StraightModel->Get1DTreeRoot();
		
		vtkIdType id;
	
		id = this->StraightModel->AddSegment(root->GetId());
		vtkHM1DSegment *tmpSegm =  this->StraightModel->GetSegment(id);
		tmpSegm->SetNumberOfElements(2);
		AddNodeData1(tmpSegm);
		
		id = this->StraightModel->AddSegment( tmpSegm->GetId() );
		vtkHM1DSegment *tmpSegm2 =  this->StraightModel->GetSegment(id);
		tmpSegm2->SetNumberOfElements(3);
		AddNodeData2(tmpSegm2);
		
		id = this->StraightModel->AddSegment( tmpSegm->GetId() );
		vtkHM1DSegment *tmpSegm3 =  this->StraightModel->GetSegment(id);
		tmpSegm3->SetNumberOfElements(3);
		AddNodeData3(tmpSegm3);
		
		id = this->StraightModel->AddSegment( tmpSegm->GetId() );
		vtkHM1DSegment *tmpSegm4 =  this->StraightModel->GetSegment(id);
		tmpSegm4->SetNumberOfElements(2);
		AddNodeData4(tmpSegm4);

		id = this->StraightModel->AddSegment( tmpSegm4->GetId() );
		vtkHM1DSegment *tmpSegm5 =  this->StraightModel->GetSegment(id);
		tmpSegm5->SetNumberOfElements(3);
		AddNodeData5(tmpSegm5);

		id = this->StraightModel->AddSegment( tmpSegm4->GetId() );
		vtkHM1DSegment *tmpSegm6 =  this->StraightModel->GetSegment(id);
		tmpSegm6->SetNumberOfElements(3);
		AddNodeData6(tmpSegm6);
		
	}
	else
	{
		vtkWarningMacro(<<"Try create a new StraightModel template, but wasn't possible. The object exists.");
		vtkDebugMacro(<<"Can't create a new StraightModel template");
	}
}



//----------------------------------------------------------------------------
// This source does not know how to generate pieces yet.
int vtkHM1DStraightModelSource::ComputeDivisionExtents(vtkDataObject *vtkNotUsed(output),
                                      int idx, int numDivisions)
{
  if (idx == 0 && numDivisions == 1)
    {
    // I will give you the whole thing
    return 1;
    }
  else
    {
    // I have nothing to give you for this piece.
    return 0;
    }
}





void vtkHM1DStraightModelSource::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "Points "<< indent << *this->Points << "\n";
  os << indent << "Segments : \n" << indent << *this->Lines << "\n";
  os << indent << "Nodes : \n" << indent << *this->Vertices << "\n";
}
