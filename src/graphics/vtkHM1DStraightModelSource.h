/*
 * $Id: vtkHM1DStraightModelSource.h 1399 2006-10-13 13:04:01Z nacho $
 */


// .NAME vtkHM1DStraightModelSource - Cria uma visualização para um objeto StraightModel.
// .SECTION Description
// A partir de um objeto vtkHM1DstraightModel passado à classe, esta gera uma visualização
// segundo as regras do vtk. Esta visualização é um vtkHM1DStraightModelGrid, que contém um
// vtkPolyData e um vtkHM1DStraightModel

// .SECTION See Also
// vtkHM1DStraightModel
// vtkHM1DStraightModelGrid


#ifndef __vtkHM1DStraightModelSource_h
#define __vtkHM1DStraightModelSource_h

#include "vtkPolyDataAlgorithm.h"

class vtkPoints;
class vtkCellArray;
class vtkFloatArray;
class vtkIntArray;
class vtkHM1DStraightModel;
class vtkHM1DStraightModelGrid;
class vtkHM1DSegment;

class VTK_EXPORT vtkHM1DStraightModelSource : public vtkPolyDataAlgorithm 
{
public:
  static vtkHM1DStraightModelSource *New();

  vtkTypeRevisionMacro(vtkHM1DStraightModelSource,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  vtkHM1DStraightModelGrid* GetOutput();
  vtkHM1DStraightModelGrid* GetOutput(int);
  void SetOutput(vtkDataObject* d);

  
  void	SetStraightModel(vtkHM1DStraightModel*);
  vtkHM1DStraightModel* GetStraightModel() { return this->StraightModel;};
  
      
protected:
  vtkHM1DStraightModelSource();
  ~vtkHM1DStraightModelSource();

  virtual int FillOutputPortInformation(int port, vtkInformation* info);

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  // This source does not know how to generate pieces yet.
  int ComputeDivisionExtents(vtkDataObject *output, 
                             int idx, int numDivisions);

	// Decription : 
	// Set Scalars
	void SetScalars();
	
	// Description :
	// Add segments to output vtkHM1DStraightModelGrid
	void AddSegment(vtkHM1DSegment* s);
	
	// Description :
	// Walk throught StraightModel to get data
	void WalkStraightModelTree( int &id );
	void WalkStraightModelTreeToGetId( int &id );
	
	// Description :
	// Create a StraightModel template
	void CreateStraightModelTemplate();

	// Description :
	// Contém todos os pontos do PolyData
  vtkPoints*						Points;
  // Description :
	// Contém os ids dos pontos que formam cada linha do PolyData
	vtkCellArray*					Lines;
	// Description :
	// Contém os ids dos vértices(terminais e nodes) do PolyData
	vtkCellArray*					Vertices;
	// Description :
	// Contém valores escalares para cada ponto do PolyData(sem utilização por enquanto)	
	vtkFloatArray*				Scalars;
	// Description :
	// Contém os ids de cada elemento do StraightModel 
	vtkIntArray*					Ids;
	// Description :
	// Modelo StraightModel
	vtkHM1DStraightModel* StraightModel;
	// Description :
	// Este Array é semelhante ao this->Lines, mas com a informação de Id embutida
	vtkCellArray*					Segments;
	// Description :
	// Este Array é semelhante ao this->Nodes, mas com a informação de Id embutida
	vtkCellArray*					Terminals;
	
	
private:
  vtkHM1DStraightModelSource(const vtkHM1DStraightModelSource&);  // Not implemented.
  void operator=(const vtkHM1DStraightModelSource&);  // Not implemented.
};

#endif


