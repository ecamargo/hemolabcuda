/*
 * $Id: vtkHM3DModelGrid.cxx 2318 2007-09-24 13:37:54Z ziemer $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DModelGrid.cxx,v $

  Copyright (c) Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHM3DModelGrid.h"

#include "vtkCellArray.h"
#include "vtkCellLinks.h"
#include "vtkUnsignedCharArray.h"
#include "vtkIdTypeArray.h"


#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM3DModelGrid, "$Rev: $");
vtkStandardNewMacro(vtkHM3DModelGrid);

//----------------------------------------------------------------------------
// Class constructor
vtkHM3DModelGrid::vtkHM3DModelGrid()
{
	this->InstantOfTime = vtkDoubleArray::New();
	this->DegreeOfFreedom = vtkDoubleArray::New();
  this->virtualPointsOfCaps = vtkIdList::New();
	this->MeshId = vtkIdList::New();

  this->NumberOfBlocks = 0;

}

//----------------------------------------------------------------------------
// Class destructor
vtkHM3DModelGrid::~vtkHM3DModelGrid()
{
	if(this->InstantOfTime)
		this->InstantOfTime->Delete();

	if(this->DegreeOfFreedom)
		this->DegreeOfFreedom->Delete();

	if(this->virtualPointsOfCaps)
		this->virtualPointsOfCaps->Delete();

	if(this->NumberOfBlocks)
			this->NumberOfBlocks = 0;

  if( this->MeshId )
  	this->MeshId->Delete();
}

//----------------------------------------------------------------------------
// PrintSelf Method
void vtkHM3DModelGrid::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);

	os << indent << "InstantOfTime: \n";
  this->GetInstantOfTime()->PrintSelf(os,indent.GetNextIndent());

	os << indent << "NumberOfBlocks: " << this->GetNumberOfBlocks() << "\n";

  os << indent << "Virtual Points: \n";
  this->GetVirtualPointsOfCaps()->PrintSelf(os,indent.GetNextIndent());

  os << indent << "Mesh IDs: \n";
  this->GetMeshId()->PrintSelf(os,indent.GetNextIndent());

  os << indent << "Degree Of Freedom: \n";
  this->GetDegreeOfFreedom()->PrintSelf(os,indent.GetNextIndent());
}

//----------------------------------------------------------------------------
void vtkHM3DModelGrid::DeepCopy(vtkDataObject *dataObject)
{
  vtkHM3DModelGrid *grid = vtkHM3DModelGrid::SafeDownCast(dataObject);

	if ( grid != NULL )
	  {
	  if ( this->Connectivity )
	    {
	    this->Connectivity->UnRegister(this);
	    this->Connectivity = NULL;
	    }
	  if (grid->Connectivity)
	    {
	    this->Connectivity = vtkCellArray::New();
	    this->Connectivity->DeepCopy(grid->Connectivity);
	    this->Connectivity->Register(this);
	    this->Connectivity->Delete();
	    }

	  if ( this->Links )
	    {
	    this->Links->UnRegister(this);
	    this->Links = NULL;
	    }
	  if (grid->Links)
	    {
	    this->Links = vtkCellLinks::New();
	    this->Links->DeepCopy(grid->Links);
	    this->Links->Register(this);
	    this->Links->Delete();
	    }

	  if ( this->Types )
	    {
	    this->Types->UnRegister(this);
	    this->Types = NULL;
	    }
	  if (grid->Types)
	    {
	    this->Types = vtkUnsignedCharArray::New();
	    this->Types->DeepCopy(grid->Types);
	    this->Types->Register(this);
	    this->Types->Delete();
	    }

	  if ( this->Locations )
	    {
	    this->Locations->UnRegister(this);
	    this->Locations = NULL;
	    }
	  if (grid->Locations)
	    {
	    this->Locations = vtkIdTypeArray::New();
	    this->Locations->DeepCopy(grid->Locations);
	    this->Locations->Register(this);
	    this->Locations->Delete();
	    }

	  if ( this->InstantOfTime )
	    {
	    this->InstantOfTime->Reset();
	    }
	  if (grid->InstantOfTime)
	    {
	    this->InstantOfTime->DeepCopy(grid->InstantOfTime);
	    }

	  if ( this->DegreeOfFreedom )
	    {
	    this->DegreeOfFreedom->Reset();
	    }
	  if (grid->DegreeOfFreedom)
	    {
	    this->DegreeOfFreedom->DeepCopy(grid->DegreeOfFreedom);
	    }

	  if ( this->virtualPointsOfCaps )
	    {
	    this->virtualPointsOfCaps->Reset();
	    }
	  if (grid->virtualPointsOfCaps)
	    {
	    this->virtualPointsOfCaps->DeepCopy(grid->virtualPointsOfCaps);
	    }

	  if ( this->NumberOfBlocks )
	    {
	    this->NumberOfBlocks = 0;
	    }
	  if (grid->NumberOfBlocks)
	    {
	    this->NumberOfBlocks = grid->NumberOfBlocks;
	    }

	  if( this->MeshId )
	  	{
	  	this->MeshId->Reset();
	  	}
	  if( grid->MeshId )
	  	{
	  	this->MeshId->DeepCopy(grid->MeshId);
	  	}
	  }

	// Do superclass
	this->vtkPointSet::DeepCopy(dataObject);
}

//----------------------------------------------------------------------------
void vtkHM3DModelGrid::SetMeshId(vtkIdList *Ids)
{

	this->MeshId->DeepCopy(Ids);
}

//----------------------------------------------------------------------------
vtkIdList* vtkHM3DModelGrid::GetMeshId()
{
	return this->MeshId;
}

//----------------------------------------------------------------------------
int vtkHM3DModelGrid::GetNumberOfBlocks()
{
	return this->NumberOfBlocks;
}

//----------------------------------------------------------------------------
void vtkHM3DModelGrid::SetNumberOfBlocks(int NumberOfBlocks)
{
	this->NumberOfBlocks = NumberOfBlocks;
}

//----------------------------------------------------------------------------
// Get/Set list of virtual points (center of caps)
vtkIdList *vtkHM3DModelGrid::GetVirtualPointsOfCaps()
{
	return this->virtualPointsOfCaps;
}

//----------------------------------------------------------------------------
void vtkHM3DModelGrid::SetVirtualPointsOfCaps(vtkIdList* idCaps)
{

	this->virtualPointsOfCaps->DeepCopy(idCaps);
}

//----------------------------------------------------------------------------
// Set/Get InstantOfTime
vtkDoubleArray* vtkHM3DModelGrid::GetInstantOfTime()
{
	return this->InstantOfTime;
}

void vtkHM3DModelGrid::SetInstantOfTime(vtkDoubleArray* array)
{
	this->InstantOfTime->DeepCopy(array);
}

//----------------------------------------------------------------------------
// Set/Get DegreeOfFreedom
vtkDoubleArray* vtkHM3DModelGrid::GetDegreeOfFreedom()
{
	return this->DegreeOfFreedom;
}

//----------------------------------------------------------------------------
void vtkHM3DModelGrid::SetDegreeOfFreedom(vtkDoubleArray* array)
{
	if(array)
		array->Squeeze();

	if(this->DegreeOfFreedom)
		{
		this->DegreeOfFreedom->Delete();
		this->DegreeOfFreedom = vtkDoubleArray::New();
		}

	this->DegreeOfFreedom->DeepCopy(array);
}
