/*
 * $Id: vtkHM3DModelGrid.h 2317 2007-09-24 13:02:58Z ziemer $
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DModelGrid.h,v $

  Copyright (c) Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM3DModelGrid - concrete dataset represents vtkHM3DModelReader
// .SECTION Description
// vtkHM3DModelGrid é uma representação visualizável do vtkHM3DModelReader.

// .SECTION See Also
// vtkHM3DModelReader


#ifndef VTKHM3DMODELGRID_H_
#define VTKHM3DMODELGRID_H_

#include "vtkUnstructuredGrid.h"

class vtkDoubleArray;
class vtkIdList;

class VTK_EXPORT vtkHM3DModelGrid : public vtkUnstructuredGrid
{
public:
	static vtkHM3DModelGrid *New();

	vtkTypeRevisionMacro(vtkHM3DModelGrid, vtkUnstructuredGrid);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	void DeepCopy(vtkDataObject *dataObject);	
	
	// Description :
	// Get/Set Number of Blocks	
	int GetNumberOfBlocks();
	void SetNumberOfBlocks(int NumberOfBlocks);
	
	// Description :
	// Get/Set list of virtual points (center of caps)	
	vtkIdList *GetVirtualPointsOfCaps();
	void SetVirtualPointsOfCaps(vtkIdList* idCaps);
	
	// Description :
	// Set/Get InstantOfTime
	vtkDoubleArray* GetInstantOfTime();
	void SetInstantOfTime(vtkDoubleArray* array);
	
	// Description :
	// Set/Get DegreeOfFreedom
	vtkDoubleArray* GetDegreeOfFreedom();
	void SetDegreeOfFreedom(vtkDoubleArray* array);
	
	void SetMeshId(vtkIdList *Ids);
	vtkIdList* GetMeshId();
		
protected:
	vtkHM3DModelGrid();
	virtual ~vtkHM3DModelGrid();
	
	vtkIdList *MeshId;	
	
	// Description:
	// Values of each Time Step
	vtkDoubleArray* InstantOfTime;
	
	// Description:
	// Values of each Time Step
	int NumberOfBlocks;
	
	// Description:
	// Values of Velocity, Pressure and Displacement for each Time Step	
	vtkDoubleArray* DegreeOfFreedom;
	
	// Description:
	// ID`s list of virtual points of caps  
  vtkIdList *virtualPointsOfCaps;
    	
private:
	vtkHM3DModelGrid(const vtkHM3DModelGrid&); 	// Not Implemented
	void operator=(const vtkHM3DModelGrid&);				// Not Implemented
};
#endif /*VTKHM3DMODELGRID_H_*/
