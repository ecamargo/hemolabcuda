/*
 * $Id: vtkHMDataObjectToPlot.cxx 1152 2006-09-06 18:58:44Z igor $
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMDataObjectToPlot

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/
#include "vtkHMDataObjectToPlot.h"
#include "vtkFieldData.h"
#include "vtkDoubleArray.h"
#include "vtkDataObject.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkHMDataObjectToPlot, "$Revision: 1152 $");
vtkStandardNewMacro(vtkHMDataObjectToPlot);

vtkHMDataObjectToPlot::vtkHMDataObjectToPlot()
{//cout << "vtkHMDataObjectToPlot::vtkHMDataObjectToPlot() - Construtor - " << this << endl;
  this->curve = NULL;
  this->FieldData1 = NULL;
  this->DataObject = NULL;
  MaxVelocity = -1000000; 
 	MinVelocity = 1000000;
}

vtkHMDataObjectToPlot::~vtkHMDataObjectToPlot()
{//cout << "vtkHMDataObjectToPlot::~vtkHMDataObjectToPlot() - Destrutor - " << this << endl;
  if( this->curve )
    this->curve->Delete();

  if( this->FieldData1 )
    this->FieldData1->Delete();

  if ( this->DataObject )
  	this->DataObject->Delete();
  //cout << "vtkHMDataObjectToPlot::~vtkHMDataObjectToPlot() - Destrutor - " << this << " - Saiu" << endl;
}

void vtkHMDataObjectToPlot::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//This method receive two arrays, array1 represent the coordinate and press to a point 1,
//array2 represent the coordinate and press to a point 2.
vtkDataObject* vtkHMDataObjectToPlot::ConvertArraysToDataObject(vtkDoubleArray *doubleArray, vtkDoubleArray *timeInstant,
																																 int sizeOfArrays, int posInArray)
{
  this->curve = vtkDoubleArray::New();  
  this->curve->SetNumberOfComponents(2);  
//  this->curve->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
	
  this->curve->SetNumberOfTuples(sizeOfArrays);
  
  this->DataObject = vtkDataObject::New();
	
	this->FieldData1 = vtkFieldData::New();
  this->FieldData1->AllocateArrays(1); //Aloca espaco para o array recebido.
  
  double *tmpPointer, *tmpPointer2;
  
  //for(vtkIdType i = 0; i < doubleArray->GetNumberOfTuples(); i++)
  for(vtkIdType i = posInArray; i<sizeOfArrays+posInArray; i++)
  {
  	tmpPointer = doubleArray->GetTuple(i);
  	tmpPointer2 = timeInstant->GetTuple(i);
  	this->curve->SetTuple2(i-posInArray,tmpPointer2[0], tmpPointer[0]);
  }
  
  //Adiciona o array ao FieldData.
  this->FieldData1->AddArray(this->curve);
  this->DataObject->SetFieldData(FieldData1);

  this->curve->Delete();
  this->curve = NULL;
  this->FieldData1->Delete();
  this->FieldData1 = NULL;
  
  return this->DataObject;
}


//This method receive two arrays, array1 represent the coordinate and press to a point 1,
//array2 represent the coordinate and press to a point 2.
vtkDataObject* vtkHMDataObjectToPlot::ConvertArrayToDataObject(vtkDoubleArray *doubleArray)
{
  this->curve = vtkDoubleArray::New();
  this->curve->SetNumberOfComponents(1);
  this->curve->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
  
  
  this->DataObject = vtkDataObject::New();
  //this->FieldData1 = vtkFieldData::New();
  this->FieldData1->AllocateArrays(1); //Aloca espaco para o array recebido.
  
  double *tmpPointer;
  

  for(vtkIdType i = 0; i < doubleArray->GetNumberOfTuples(); i++)
  {
  	tmpPointer = doubleArray->GetTuple(i);
  	this->curve->SetTuple(i, &tmpPointer[0]);
  }

  //Adiciona o array ao FieldData.
  this->FieldData1->AddArray(this->curve);
  this->DataObject->SetFieldData(FieldData1);
  
  this->curve->Delete();
  this->curve = NULL;
  this->FieldData1->Delete();
  this->FieldData1 = NULL;
  
  return this->DataObject;
}

//Description:
//This method make a curve to velocity variable.
vtkDoubleArray* vtkHMDataObjectToPlot::MakeVelocityCurve(vtkDoubleArray *Flow, vtkDoubleArray *Area)
{
  vtkDoubleArray *velocity = vtkDoubleArray::New();
  velocity->SetNumberOfTuples(Flow->GetNumberOfTuples());
  velocity->SetNumberOfComponents(1);
  
  double tmpVelocity;
  
  for(int i = 0; i < Flow->GetNumberOfTuples(); i++)
  { 
  	 //Se a área for maior que zero realiza a divisão, para obter o valor de velocidade média 
  	 //para o ponto em questão.
  	 if(Area->GetComponent(i, 0) > 0)
  	   tmpVelocity = Flow->GetComponent(i, 0)/Area->GetComponent(i, 0);
  	   
  	 else if(Area->GetComponent(i, 0) == 0)
  	   tmpVelocity = Flow->GetComponent(i, 0);
  	 
  	 else if(Area->GetComponent(i, 0) < 0)
  	   tmpVelocity = Flow->GetComponent(i, 0)/Area->GetComponent(i, 0);
  	   
  	 velocity->SetTuple(i, &tmpVelocity);
  }
  
  return velocity;
}


//Description:
//This method set ivars (MinVelocity, MaxVelocity), with max and min velocity.
void vtkHMDataObjectToPlot::MaxAndMinVelocity(vtkDoubleArray *velocity)
{
  double *tmpPointer, tmpMaxValue, tmpMinValue;
  
  tmpMaxValue = velocity->GetComponent(0, 0);
  tmpMinValue = velocity->GetComponent(0, 0);
  
  for (vtkIdType i = 0; i < velocity->GetNumberOfTuples(); i++)
  {
	tmpPointer = velocity->GetTuple(i);
	
	//Store the Max value of pression.
	if(tmpPointer[0] > tmpMaxValue)
	  tmpMaxValue = tmpPointer[0];
	  
	else if(tmpPointer[0] < tmpMinValue)
	  tmpMinValue = tmpPointer[0];
  }
  
  this->SetMaxVelocity(tmpMaxValue);
  this->SetMinVelocity(tmpMinValue);
  velocity->Delete();
}

