/*
 * $Id: vtkHMDataObjectToPlot.h 1399 2006-10-13 13:04:01Z nacho $
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMDataObjectToPlot

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/
// .NAME vtkHMDataObjectToPlot - Used to transform input dataArrays to dataObjects
//(used para plotar curves in XYPlot) 
// .SECTION Description
//The class implement methods to show XYPlot with information about terminals and segments.
//e.g: Pressure, area, flow.

#ifndef _vtkHMDataObjectToPlot_h_
#define _vtkHMDataObjectToPlot_h_

#include "vtkDataObject.h"
#include "vtkFieldData.h"
#include "vtkDoubleArray.h"

class vtkFieldData;
class vtkDataObject;

class VTK_EXPORT vtkHMDataObjectToPlot: public vtkDataObject
{
public:
  static vtkHMDataObjectToPlot *New();
  vtkTypeRevisionMacro(vtkHMDataObjectToPlot, vtkDataObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  //This method receive two array of type vtkDataArray and return one dataObject.
  vtkDataObject *ConvertArraysToDataObject(vtkDoubleArray *doubleArray1, vtkDoubleArray *timeInstant,
  																				int sizeOfArrays, int posInArray);// double interpolationFactor);
  
  // Description:
  //This method receive one array of type vtkDataArray and return one dataObject.
  vtkDataObject* ConvertArrayToDataObject(vtkDoubleArray *doubleArray);
  
  //Description:
  //This method receive two arrays (Flow and Area) and from this compute the velocity curve.
  vtkDoubleArray *MakeVelocityCurve(vtkDoubleArray *Flow, vtkDoubleArray *Area);

  //Description:
  //This method, compute the max and min values to velocity.
  void MaxAndMinVelocity(vtkDoubleArray *velocity);
  
   //Description:
  //Methods Set/Get to MaxVelocity.
  vtkSetMacro(MaxVelocity, double);
  
  //Description:
  //Methods Set/Get to MaxVelocity.
  vtkGetMacro(MaxVelocity, double);
  
  //Description:
  //Methods Set/Get to MinVelocity.
  vtkSetMacro(MinVelocity, double);
  
  //Description:
  //Methods Set/Get to MinVelocity.
  vtkGetMacro(MinVelocity, double);
  
  //Description:
  //This method return a reference to vtkDoubleArray to store the curve.
  vtkDoubleArray* GetCurve()
  {
  	return curve;
  }
  
  
  vtkFieldData* GetFieldData()
  {
  	return FieldData1;
  }
  
  vtkDataObject* GetDataObject()
  {
  	return DataObject;
  }
  
protected:
  vtkHMDataObjectToPlot();
  ~vtkHMDataObjectToPlot();
 
  //Description:
  //This variable represent the Max value to Velocity.
  double MaxVelocity; 
 
  //Description:
  //This variable represent the Min value to Velocity.
  double MinVelocity;
  
  //Description:
  //This reference store a curve generate.
  vtkDoubleArray* curve;
  
  //Description:
  //This a reference to vtkFieldData.
  vtkFieldData *FieldData1;
  
  //Description:
  //This a reference to vtkDataObject.
  vtkDataObject *DataObject;
  
private:
	vtkHMDataObjectToPlot(const vtkHMDataObjectToPlot&); 	// Not Implemented
	void operator=(const vtkHMDataObjectToPlot&);			// Not Implemented
};
#endif /*_vtkHMDataObjectToPlot_h_*/
