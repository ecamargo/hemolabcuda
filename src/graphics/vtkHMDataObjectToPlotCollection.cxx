/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHMXYPlotCollection.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHMDataObjectToPlotCollection.h"
#include "vtkObjectFactory.h"
#include "vtkCollection.h"
#include "vtkCollectionIterator.h"
#include "vtkGarbageCollector.h"


#include <stdlib.h>
#include <math.h>

vtkCxxRevisionMacro(vtkHMDataObjectToPlotCollection, "$Revision: 1.8 $");
vtkStandardNewMacro(vtkHMDataObjectToPlotCollection);

vtkHMDataObjectToPlotCollection::vtkHMDataObjectToPlotCollection()
{
	this->Top = NULL;
	this->Bottom = NULL;
	this->Current = NULL;
	this->NumberOfItems = 0;
}

vtkHMDataObjectToPlotCollection::~vtkHMDataObjectToPlotCollection()
{
}

//----------------------------------------------------------------------------
void vtkHMDataObjectToPlotCollection::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "Number Of Items: " << this->NumberOfItems << "\n";
}



// protected function to delete an element. Internal use only.
void vtkHMDataObjectToPlotCollection::DeleteElement(vtkHMDataPlotCollectionElement *e)
{
  if (e->Item != NULL)
    {
    
    if ( e->Item[0] )
    {
    	e->Item[0]->UnRegister(this); 
    	//e->Item[0]->Delete();
    }
    if ( e->Item[1] )
    	e->Item[1]->Delete();
    if ( e->Item[2] )
    	e->Item[2]->Delete();
    if ( e->Item[3] )
    	e->Item[3]->Delete();
    if ( e->Item[4] )
    	e->Item[4]->Delete();
    if ( e->Item[5] )
    	e->Item[5]->Delete();
    e->Item[0] = NULL;
    e->Item[1] = NULL;
    e->Item[2] = NULL;
    e->Item[3] = NULL;
    e->Item[4] = NULL;
    e->Item[5] = NULL;
    }
    
  delete e;
}

// Add an object to the list. Does not prevent duplicate entries.
void vtkHMDataObjectToPlotCollection::AddItem(vtkHMDataObjectToPlot **a)
{
  vtkHMDataPlotCollectionElement *elem;

  elem = new vtkHMDataPlotCollectionElement;

  if (this->Top == NULL)
    {
    this->Top = elem;
    }
  else
    {
    this->Bottom->Next = elem;
    }
  this->Bottom = elem;

  if (a[0] )
  	a[0]->Register(this);
  elem->Item[0] = a[0];
  elem->Item[1] = a[1];
  elem->Item[2] = a[2];
  elem->Item[3] = a[3];
  elem->Item[4] = a[4];
  elem->Item[5] = a[5];
  elem->Next = NULL;
	this->Bottom = elem;
  this->Modified();

  this->NumberOfItems++;
}

// Remove an object from the list. Removes the first object found, not
// all occurrences. If no object found, list is unaffected.  See warning
// in description of RemoveItem(int).
void vtkHMDataObjectToPlotCollection::RemoveItem(vtkHMDataObjectToPlot **a)
{
  int i;
  vtkHMDataPlotCollectionElement *elem;
  
  if (!this->Top)
    {
    return;
    }

  elem = this->Top;
  for (i = 0; i < this->NumberOfItems; i++)
    {
    if (elem->Item == a)
      {
      this->RemoveItem(i);
      this->Modified();
      return;
      }
    else
      {
      elem = elem->Next;
      }
    }
}

// Remove all objects from the list.
void vtkHMDataObjectToPlotCollection::RemoveAllItems()
{
  vtkHMDataPlotCollectionElement *elem;

  while (this->NumberOfItems )
    {
    elem = this->Top;
    this->Top = elem->Next;
    this->Current = elem->Next;
    this->NumberOfItems--;    
    this->DeleteElement(elem);
    }
  
  this->Modified();
  this->Top = this->Bottom = this->Current = NULL;
}

// Search for an object and return location in list. If location == 0,
// object was not found.
int vtkHMDataObjectToPlotCollection::IsItemPresent(vtkHMDataObjectToPlot **a)
{
  int i;
  vtkHMDataPlotCollectionElement *elem;
  
  if (!this->Top)
    {
    return 0;
    }

  elem = this->Top;
  for (i = 0; i < this->NumberOfItems; i++)
    {
    if (elem->Item == a)
      {
      return i + 1;
      }
    else
      {
      elem = elem->Next;
      }
    }

  return 0;
}


// Return the number of objects in the list.
int vtkHMDataObjectToPlotCollection::GetNumberOfItems()
{
  return this->NumberOfItems;
}

// Get the i'th item in the collection. NULL is returned if i is out
// of range
vtkHMDataObjectToPlot **vtkHMDataObjectToPlotCollection::GetItemAsObject(int i)
{
  vtkHMDataPlotCollectionElement *elem=this->Top;

  if (i < 0)
    {
    return NULL;
    }

  if (i == this->NumberOfItems - 1)
    {
    // optimize for the special case where we're looking for the last elem
    elem = this->Bottom;
    }
  else
    {
    while (elem != NULL && i > 0)
      {
      elem = elem->Next;
      i--;
      }
    }
  if ( elem != NULL )
    {
    return elem->Item;
    }
  else
    {
    return NULL;
    }
}

vtkHMDataObjectToPlot **vtkHMDataObjectToPlotCollection::GetLastItem()
{
	this->Current = this->Bottom;
	if ( this->Bottom->Item )
		return this->Bottom->Item;
	else
		return NULL;
}

vtkHMDataObjectToPlot **vtkHMDataObjectToPlotCollection::GetFirstItem()
{
	this->Current = this->Top;
	if ( this->Top->Item )
		return this->Top->Item;
	else
		return NULL;
}

// Replace the i'th item in the collection with a
void vtkHMDataObjectToPlotCollection::ReplaceItem(int i, vtkHMDataObjectToPlot **a)
{
  vtkHMDataPlotCollectionElement *elem;

  if( i < 0 || i >= this->NumberOfItems )
    {
    return;
    }
  
  elem = this->Top;
  if (i == this->NumberOfItems - 1)
    {
    elem = this->Bottom;
    }
  else
    {
    for (int j = 0; j < i; j++, elem = elem->Next ) 
      {}
    }

  // Take care of reference counting
  /*if (elem->Item != NULL)
    {
    elem->Item->UnRegister(this);
    }
  a->Register(this);
  */
  // j == i
  elem->Item[0] = a[0];
  elem->Item[1] = a[1];
  elem->Item[2] = a[2];
  elem->Item[3] = a[3];
  elem->Item[4] = a[4];
  elem->Item[5] = a[5];

  this->Modified();
}


// Remove the i'th item in the list.
// Be careful if using this function during traversal of the list using 
// GetNextItemAsObject (or GetNextItem in derived class).  The list WILL
// be shortened if a valid index is given!  If this->Current is equal to the
// element being removed, have it point to then next element in the list.
void vtkHMDataObjectToPlotCollection::RemoveItem(int i)
{
  vtkHMDataPlotCollectionElement *elem,*prev;

  if( i < 0 || i >= this->NumberOfItems )
    {
    return;
    }
  
  this->Modified();

  elem = this->Top;
  prev = NULL;
  for (int j = 0; j < i; j++)
    {
    prev = elem;
    elem = elem->Next;
    }  

  // j == i
  if (prev)
    {
    prev->Next = elem->Next;
    }
  else
    {
    this->Top = elem->Next;
    }

  if (!elem->Next)
    {
    this->Bottom = prev;
    }
      
  if ( this->Current == elem )
    {
    this->Current = elem->Next;
    }

  this->NumberOfItems--;
  this->DeleteElement(elem);
}

vtkCollectionIterator* vtkHMDataObjectToPlotCollection::NewIterator()
{
  vtkCollectionIterator* it = vtkCollectionIterator::New();
  it->SetCollection(this);
  return it;
}

//----------------------------------------------------------------------------
void vtkHMDataObjectToPlotCollection::Register(vtkObjectBase* o)
{
  this->RegisterInternal(o, 1);
}

//----------------------------------------------------------------------------
void vtkHMDataObjectToPlotCollection::UnRegister(vtkObjectBase* o)
{
  this->UnRegisterInternal(o, 1);
}

//----------------------------------------------------------------------------
/*void vtkHMDataObjectToPlotCollection::ReportReferences(vtkGarbageCollector* collector)
{
  this->Superclass::ReportReferences(collector);
  for(vtkHMDataPlotCollectionElement* elem = this->Top; elem; elem = elem->Next)
    {
    vtkGarbageCollectorReport(collector, elem->Item, "Element");
    }
}
*/
