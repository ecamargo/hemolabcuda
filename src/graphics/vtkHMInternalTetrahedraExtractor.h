/*
 * vtkHMInternalTetrahedraExtractor.h
 *
 *  Created on: Aug 11, 2009
 *      Author: igor
 */

// .NAME vtkHMInternalTetrahedraExtractor - Extract internal tetrahedra from a Delaunay tessellation.

#ifndef VTKHMINTERNALTETRAHEDRAEXTRACTOR_H_
#define VTKHMINTERNALTETRAHEDRAEXTRACTOR_H_

#include "vtkUnstructuredGridAlgorithm.h"
#include "vtkIdList.h"
//#include "vtkvmtkComputationalGeometryWin32Header.h"
//#include "vtkvmtkWin32Header.h"

class VTK_EXPORT vtkHMInternalTetrahedraExtractor : public vtkUnstructuredGridAlgorithm
{
  public:
  vtkTypeRevisionMacro(vtkHMInternalTetrahedraExtractor,vtkUnstructuredGridAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMInternalTetrahedraExtractor *New();

  // Description:
  // Set/Get the name of the array containing outward oriented point normals.
  vtkSetStringMacro(OutwardNormalsArrayName);
  vtkGetStringMacro(OutwardNormalsArrayName);

  // Description:
  // Turn on/off special handling of caps.
  vtkSetMacro(UseCaps,int);
  vtkGetMacro(UseCaps,int);
  vtkBooleanMacro(UseCaps,int);

  // Description:
  // Set/Get the ids of cap centers.
  vtkSetObjectMacro(CapCenterIds,vtkIdList);
  vtkGetObjectMacro(CapCenterIds,vtkIdList);

  vtkSetMacro(Tolerance,double);
  vtkGetMacro(Tolerance,double);

  protected:
  vtkHMInternalTetrahedraExtractor();
  ~vtkHMInternalTetrahedraExtractor();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  int UseCaps;
  vtkIdList* CapCenterIds;
  char* OutwardNormalsArrayName;

  double Tolerance;

  private:
  vtkHMInternalTetrahedraExtractor(const vtkHMInternalTetrahedraExtractor&);  // Not implemented.
  void operator=(const vtkHMInternalTetrahedraExtractor&);  // Not implemented.
};

#endif /* VTKHMINTERNALTETRAHEDRAEXTRACTOR_H_ */
