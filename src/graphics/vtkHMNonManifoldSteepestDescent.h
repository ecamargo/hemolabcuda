/*
 * vtkHMNonManifoldSteepestDescent.h
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */


// .NAME vtkHMNonManifoldSteepestDescent - Abstract class for steepest descent on a polygonal non-manifold.
// .SECTION Description
// This class is an abstract filter used as base class for performing steepest descent on a non-manifold
// surface made of convex polygons (such as the Voronoi diagram) on the basis of a given scalar field.
// Steepest descent is performed on the edges of input polygons with a first order approximation.
//

#ifndef VTKHMNONMANIFOLDSTEEPESTDESCENT_H_
#define VTKHMNONMANIFOLDSTEEPESTDESCENT_H_

#include "vtkPolyDataAlgorithm.h"
#include "vtkDataArray.h"
//#include "vtkvmtkComputationalGeometryWin32Header.h"
//#include "vtkvmtkWin32Header.h"

#ifndef DOUBLE_TOL
#define DOUBLE_TOL 1.0E-12
#endif

#ifndef LARGE_DOUBLE
#define LARGE_DOUBLE 1.0E+32
#endif

#define DOWNWARD 0
#define UPWARD 1

class VTK_EXPORT vtkHMNonManifoldSteepestDescent : public vtkPolyDataAlgorithm
{
  public:
  vtkTypeRevisionMacro(vtkHMNonManifoldSteepestDescent,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMNonManifoldSteepestDescent *New();

  // Description:
  // Set/Get the name of the point data array used as the descent scalar field.
  vtkSetStringMacro(DescentArrayName);
  vtkGetStringMacro(DescentArrayName);

  vtkSetMacro(Direction,int);
  vtkGetMacro(Direction,int);
  void SetDirectionToDownward()
  {this->SetDirection(DOWNWARD); }
  void SetDirectionToUpward()
  {this->SetDirection(UPWARD); }

  protected:
  vtkHMNonManifoldSteepestDescent();
  ~vtkHMNonManifoldSteepestDescent();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Description:
  // Compute the steepest descent point in terms of edge (point id pair) and parametric coordinate on edge. It takes in input a starting point expressed in terms of edge (point id pair) and parametric coordinate on edge. It returns the descent value.
  double GetSteepestDescent(vtkPolyData* input, vtkIdType* edge, double s, vtkIdType* steepestDescentEdge, double &steepestDescentS);
  double GetSteepestDescentInCell(vtkPolyData* input, vtkIdType cellId, vtkIdType* edge, double s, vtkIdType* steepestDescentEdge, double &steepestDescentS, double &steepestDescentLength);

  vtkDataArray* DescentArray;
  char* DescentArrayName;

  int NumberOfEdgeSubdivisions;
  int Direction;

  private:
  vtkHMNonManifoldSteepestDescent(const vtkHMNonManifoldSteepestDescent&);  // Not implemented.
  void operator=(const vtkHMNonManifoldSteepestDescent&);  // Not implemented.
};

#endif /* VTKHMNONMANIFOLDSTEEPESTDESCENT_H_ */
