/*
 * vtkHMSimplifyVoronoiDiagram.h
 *
 *  Created on: Aug 11, 2009
 *      Author: igor
 */

#ifndef VTKHMSIMPLIFYVORONOIDIAGRAM_H_
#define VTKHMSIMPLIFYVORONOIDIAGRAM_H_


// .NAME vtkHMSimplifyVoronoiDiagram - Remove non essential Voronoi polygon points.
// .SECTION Description
// This class identifies and removes Voronoi polygon points if they are used by one
// cell and they are not poles. This helps to get rid of noisy Voronoi diagram parts
// induced by non smooth surface point distribution. This operation has no effect on
// the accuracy of the computation of centerlines and of surface related quantities.
// .SECTION See Also
// vtkVoronoiDiagram3D

#include "vtkPolyDataAlgorithm.h"
#include "vtkIdList.h"
//#include "vtkvmtkComputationalGeometryWin32Header.h"
//#include "vtkvmtkWin32Header.h"

#define REMOVE_BOUNDARY_POINTS 0
#define REMOVE_BOUNDARY_CELLS 1

class vtkCellArray;
class vtkCellTypes;
class vtkCellLinks;

class VTK_EXPORT vtkHMSimplifyVoronoiDiagram : public vtkPolyDataAlgorithm
{
  public:
  vtkTypeRevisionMacro(vtkHMSimplifyVoronoiDiagram,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMSimplifyVoronoiDiagram *New();

  // Set/Get id list of Voronoi diagram points to preserve.
  vtkSetObjectMacro(UnremovablePointIds,vtkIdList);
  vtkGetObjectMacro(UnremovablePointIds,vtkIdList);

  // Set/Get id list of Voronoi diagram cells to preserve.
  vtkSetObjectMacro(UnremovableCellIds,vtkIdList);
  vtkGetObjectMacro(UnremovableCellIds,vtkIdList);

  // Description:
  // Set/Get type of simplification.
  vtkSetMacro(Simplification,int);
  vtkGetMacro(Simplification,int);
  void SetSimplificationToRemoveBoundaryPoints() {
  this->SetSimplification(REMOVE_BOUNDARY_POINTS);};
  void SetSimplificationToRemoveBoundaryCells() {
  this->SetSimplification(REMOVE_BOUNDARY_CELLS);};

  vtkSetMacro(IncludeUnremovable,int);
  vtkGetMacro(IncludeUnremovable,int);
  vtkBooleanMacro(IncludeUnremovable,int);

  vtkSetMacro(OnePassOnly,int);
  vtkGetMacro(OnePassOnly,int);
  vtkBooleanMacro(OnePassOnly,int);

  protected:
  vtkHMSimplifyVoronoiDiagram();
  ~vtkHMSimplifyVoronoiDiagram();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  vtkIdType IsBoundaryEdge(vtkCellLinks* links, vtkIdType* edge);

  vtkIdList* UnremovablePointIds;
  vtkIdList* UnremovableCellIds;

  int Simplification;
  int IncludeUnremovable;
  int OnePassOnly;

  private:
  vtkHMSimplifyVoronoiDiagram(const vtkHMSimplifyVoronoiDiagram&);  // Not implemented.
  void operator=(const vtkHMSimplifyVoronoiDiagram&);  // Not implemented.
};


#endif /* VTKHMSIMPLIFYVORONOIDIAGRAM_H_ */
