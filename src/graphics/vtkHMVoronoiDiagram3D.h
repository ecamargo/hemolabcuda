/*
 * vtkHMVoronoiDiagram3D.h
 *
 *  Created on: Aug 11, 2009
 *      Author: igor
 */

#ifndef VTKHMVORONOIDIAGRAM3D_H_
#define VTKHMVORONOIDIAGRAM3D_H_

// .NAME vtkHMVoronoiDiagram3D - Compute the Voronoi diagram of a set of points in 3D.
// .SECTION Description
// This class computes the Voronoi diagram of a set of points given their Delaunay tessellation.
// Basically, the output points are Delaunay tetrahedra circumcenters, and the cells are convex
// polygons constructed by connecting circumcenters of tetrahedra sharing a face.
// The radius of the circumsphere associated with each circumcenter is stored in a point data
// array with name specifed by RadiusArrayName. The id list of poles is also provided.
// Poles are the farthest inner and outer Voronoi points associated with a Delaunay point.
// Since this class is meant to deal with Delaunay tessellations which are internal to a given
// surface, only the internal pole is considered for each input point.

#include "vtkPolyDataAlgorithm.h"
#include "vtkIdList.h"
#include "vtkCellArray.h"
//#include "vtkvmtkComputationalGeometryWin32Header.h"
//#include "vtkvmtkWin32Header.h"

class vtkUnstructuredGrid;

class VTK_EXPORT vtkHMVoronoiDiagram3D : public vtkPolyDataAlgorithm
{
  public:
  vtkTypeRevisionMacro(vtkHMVoronoiDiagram3D,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkHMVoronoiDiagram3D *New();

  //  vtkSetMacro(BuildLines,int);
  //  vtkGetMacro(BuildLines,int);
  //  vtkBooleanMacro(BuildLines,int);

  // Description:
  // Set/Get the name of the point data array where circumsphere radius values are stored.
  vtkSetStringMacro(RadiusArrayName);
  vtkGetStringMacro(RadiusArrayName);

  // Description:
  // Get the id list of poles. The id list has the same size as input points. For every input point, one Voronoi point id is stored in the list.
  vtkGetObjectMacro(PoleIds,vtkIdList);

  protected:
  vtkHMVoronoiDiagram3D();
  ~vtkHMVoronoiDiagram3D();

  int FillInputPortInformation(int, vtkInformation *info);

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  void ExtractUniqueEdges(vtkUnstructuredGrid* input, vtkCellArray* edgeArray);
  void BuildVoronoiPolys(vtkUnstructuredGrid* input, vtkCellArray* voronoiPolys);
  void BuildVoronoiLines() {};   // not yet implemented

  int BuildLines;
  vtkIdList* PoleIds;
  char* RadiusArrayName;

  private:
  vtkHMVoronoiDiagram3D(const vtkHMVoronoiDiagram3D&);  // Not implemented.
  void operator=(const vtkHMVoronoiDiagram3D&);  // Not implemented.
};

#endif /* VTKHMVORONOIDIAGRAM3D_H_ */
