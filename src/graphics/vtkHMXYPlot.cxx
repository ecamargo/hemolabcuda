/*
 * $Id$
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMXYPlot

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/

#include "vtkHMXYPlot.h"
#include "vtkDataObject.h"
#include "vtkXYPlotActor.h"
#include "vtkObjectFactory.h"
#include "vtkTextProperty.h"
#include "vtkProperty2D.h"


vtkCxxRevisionMacro(vtkHMXYPlot, "$Revision: 286 $");
vtkStandardNewMacro(vtkHMXYPlot);

vtkHMXYPlot::vtkHMXYPlot()
{
  this->TextProp = vtkTextProperty::New();          //Referencia para configurar propriedades de texto.
  this->TextProp2 = vtkTextProperty::New();         //Referencia para configurar propriedades de texto.
  
  this->TextProp->SetFontFamilyToArial();  			//Define o tamanho o tipo.
	this->TextProp->SetColor(0,0,0);
  this->TextProp2->SetColor(0,0,0);
  this->TextProp->SetFontSize(2);   				//Define o tamanho da fonte.
  this->TextProp2->SetFontSize(2);                  //Configura o tamanho da fonte.
  this->TextProp->SetBold(0);                       //Desabilita o negrito da fonte(melhor para dar espaçamento).
  this->TextProp->SetJustification(1);              //Ativa a justificação de texto.
  this->TextProp->SetJustificationToLeft();         //Justificando a posição dos labels alinhados à esquerda.
  this->SetLabelFormat("%2.9g"); 					//Define the format numérico of label.
  this->PlotPointsOn();                             //Exibe os pontos referentes aos dados ao longo da curva.
  this->GetProperty()->SetPointSize(2);             //Configura o tamanho da curva.
  this->SetTitleTextProperty(this->TextProp2);		//Configurando a propriedade do título do plot.
  this->SetAxisTitleTextProperty(this->TextProp2);  //Configurando as propriedades dos títulos dos eixos.
  this->SetAxisLabelTextProperty(this->TextProp);   //Configurando as propriedades dos labels.
//  this->SetTitle("Select a type of plot!!!");       //Configura o título do plot.
  this->SetTitle("");       //Configura o título do plot.
  this->SetXTitle("");  							//Configura o título do eixo X.
  this->SetYTitle("");  							//Configura o título do eixo Y.
  this->SetYRange(0.0, 0.1);
  this->SetPlotColor(0, 1.0, 1.0, .0);              //Configurando a cor da curva para amarelo.
  this->SetWidth(0.9);   							//Configura a largura do eixo X.
  this->SetHeight(0.4);								//Configura a largura do eixo X.
  this->SetDataObjectXComponent(1, 0);              //Exibe as componentes de X no eixo X.
  this->SetDataObjectYComponent(0, 1);  		    //Exibe as componentes de Y no eixo Y.
  this->SetXValuesToValue();                        //Configura o eixo X para utilizar os componentes de X.
  //this->LegendOn();									//Habilita legenda.
  this->PickableOff();	  							//Habilita eventos de mouse na janela.
  //this->SetNumberOfLabels(4);
  
}

vtkHMXYPlot::~vtkHMXYPlot()
{
	this->TextProp->Delete();
	this->TextProp2->Delete();
}

void vtkHMXYPlot::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
