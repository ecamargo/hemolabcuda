/*
 * $Id$
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkHMXYPlot

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/
// .NAME vtkHMXYPlotActor - Used to show an XY plot with informations about 
//vtkHMXYPlotActor.
// .SECTION Description
//The class implement methods to show XYPlot with information about terminals and segments.
//e.g: Pressure, area, flow.

#ifndef _vtkHMXYPlot_h_
#define _vtkHMXYPlot_h_

#include "vtkXYPlotActor.h"
#include "vtkDataObject.h"
#include "vtkFieldData.h"
#include "vtkDataArray.h"

#include "vtkTextProperty.h"

// Description:
//Define type of XYPlot to show.

class VTK_EXPORT vtkHMXYPlot : public vtkXYPlotActor
{
public:
  static vtkHMXYPlot *New();
  vtkTypeRevisionMacro(vtkHMXYPlot, vtkXYPlotActor);
  void PrintSelf(ostream& os, vtkIndent indent);


  //*** OBS: All methods describes below, have implementation on class base vtkXYPlotActor.
  
  // Description:
  //This method define the line color used to plot the curve. The default is white.
  //void SetPlotLineColor(double r, double g, double b, double d);
  
  // Description:
  //This method define the position of XYPlot in the global coordinates system.
  //void SetPlotPosition(double x, double y);
  
  // Description:
  //This method control the form to show the XYPlot, with base on macro typeXYPlot.
  //e.g(multipes curves in a single XYPlot in viewPort, multiples XYPlot with one curve in viewPort and
  //    multiples XYPlots and multiples curves in viewPort).
  //void showXYPlot();
  
  // Description:
  // This method define the orientation of curve in the X axe.
  //void SetXOrientationCurve(int x, int comp);
  
  // Description:
  // This method define the orientation of curve in the Y axe.
  //void SetYOrientationCurve(int x, int comp);

  
  // Description:
  //This macro define how many points i need to make the curve. 
  //vtkSetMacro(NumberOfPoints,int);
  
  // Description:
  //This macro define the title to XY plot.
  
  //void SetPlotTitle(char *); 
  //void SetTitle(const char *);
  // Description:
  //This macro define the title to X axe.
  //vtkSetMacro(PlotXTitle,char *);
  
  // Description:
  //This macro define the title to Y axe.
  //vtkSetMacro(PlotYTitle,char *);
  
  // Description:
  //This macro define the text of description of Curve.
  //vtkSetMacro(PlotLegendTitle,char *);
  
  // Description:
  //This macro define the length to X axe.
  //vtkSetMacro(PlotXWidth,double);

  // Description:
  //This macro define the length to Y axe.
  //vtkSetMacro(PlotYHeight,double);
  
  // Description:
  //This macro define if the XYPlot have Legend.
  //vtkSetMacro(PlotLegend,int);
  

protected:
  vtkTextProperty *TextProp;
  vtkTextProperty *TextProp2;

protected:
  vtkHMXYPlot();
  ~vtkHMXYPlot();

private:
	vtkHMXYPlot(const vtkHMXYPlot&); 	// Not Implemented
	void operator=(const vtkHMXYPlot&);				// Not Implemented
};

#endif /*_vtkHMXYPlot_h_*/
