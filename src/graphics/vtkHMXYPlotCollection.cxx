/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHMXYPlotCollection.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHMXYPlotCollection.h"
#include "vtkObjectFactory.h"
#include "vtkCollection.h"
#include "vtkCollectionIterator.h"
#include "vtkGarbageCollector.h"

#include <stdlib.h>
#include <math.h>

vtkCxxRevisionMacro(vtkHMXYPlotCollection, "$Revision: 1.8 $");
vtkStandardNewMacro(vtkHMXYPlotCollection);

vtkHMXYPlotCollection::vtkHMXYPlotCollection()
{
	this->Top = NULL;
	this->Bottom = NULL;
	this->Current = NULL;
	this->NumberOfItems = 0;
}

vtkHMXYPlotCollection::~vtkHMXYPlotCollection()
{
}

//----------------------------------------------------------------------------
void vtkHMXYPlotCollection::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "Number Of Items: " << this->NumberOfItems << "\n";
}



// protected function to delete an element. Internal use only.
void vtkHMXYPlotCollection::DeleteElement(vtkHMCollectionElement *e)
{
  if (e->Item != NULL)
    {
    //e->Item->UnRegister(this);
    if ( e->Item[0] )
    {
    	e->Item[0]->RemoveAllInputs();
    	e->Item[0]->Delete();
    }
    if ( e->Item[1] )
    {
    	e->Item[1]->RemoveAllInputs();
    	e->Item[1]->Delete();
    }
    if ( e->Item[2] )
    {
    	e->Item[2]->RemoveAllInputs();
    	e->Item[2]->Delete();
    }
    if ( e->Item[3] )
    {
    	e->Item[3]->RemoveAllInputs();
    	e->Item[3]->Delete();
    }
    if ( e->Item[4] )
    {
    	e->Item[4]->RemoveAllInputs();
    	e->Item[4]->Delete();
    }
    if ( e->Item[5] )
    {
    	e->Item[5]->RemoveAllInputs();
    	e->Item[5]->Delete();
    }
    e->Item[0] = NULL;
    e->Item[1] = NULL;
    e->Item[2] = NULL;
    e->Item[3] = NULL;
    e->Item[4] = NULL;
    e->Item[5] = NULL;
    }
  
  delete e;
}

// Add an object to the list. Does not prevent duplicate entries.
void vtkHMXYPlotCollection::AddItem(vtkHMXYPlot **a)
{
  vtkHMCollectionElement *elem;

  elem = new vtkHMCollectionElement;
  
  if (!this->Top)
    {
    this->Top = elem;
    }
  else
    {
    this->Bottom->Next = elem;
    }
  this->Bottom = elem;

  //a->Register(this);
  elem->Item[0] = a[0];
  elem->Item[1] = a[1];
  elem->Item[2] = a[2];
  elem->Item[3] = a[3];
  elem->Item[4] = a[4];
  elem->Item[5] = a[5];
  elem->Next = NULL;
	this->Bottom = elem;
  this->Modified();

  this->NumberOfItems++;
}

// Remove an object from the list. Removes the first object found, not
// all occurrences. If no object found, list is unaffected.  See warning
// in description of RemoveItem(int).
void vtkHMXYPlotCollection::RemoveItem(vtkHMXYPlot **a)
{
  int i;
  vtkHMCollectionElement *elem;
  
  if (!this->Top)
    {
    return;
    }

  elem = this->Top;
  for (i = 0; i < this->NumberOfItems; i++)
    {
    if (elem->Item == a)
      {
      this->RemoveItem(i);
      this->Modified();
      return;
      }
    else
      {
      elem = elem->Next;
      }
    }
}

// Remove all objects from the list.
void vtkHMXYPlotCollection::RemoveAllItems()
{
  vtkHMCollectionElement *elem;

  while (this->NumberOfItems )
    {
    elem = this->Top;
    this->Top = elem->Next;
    this->Current = elem->Next;
    this->NumberOfItems--;    
    this->DeleteElement(elem);
    }
  
  this->Modified();
  this->Top = this->Bottom = this->Current = NULL;
}

// Search for an object and return location in list. If location == 0,
// object was not found.
int vtkHMXYPlotCollection::IsItemPresent(vtkHMXYPlot **a)
{
  int i;
  vtkHMCollectionElement *elem;
  
  if (!this->Top)
    {
    return 0;
    }

  elem = this->Top;
  for (i = 0; i < this->NumberOfItems; i++)
    {
    if (elem->Item == a)
      {
      return i + 1;
      }
    else
      {
      elem = elem->Next;
      }
    }

  return 0;
}


// Return the number of objects in the list.
int vtkHMXYPlotCollection::GetNumberOfItems()
{
  return this->NumberOfItems;
}

// Get the i'th item in the collection. NULL is returned if i is out
// of range
vtkHMXYPlot **vtkHMXYPlotCollection::GetItemAsObject(int i)
{
  vtkHMCollectionElement *elem=this->Top;

  if (i < 0)
    {
    return NULL;
    }

  if (i == this->NumberOfItems - 1)
    {
    // optimize for the special case where we're looking for the last elem
    elem = this->Bottom;
    }
  else
    {
    while (elem != NULL && i > 0)
      {
      elem = elem->Next;
      i--;
      }
    }
  if ( elem != NULL )
    {
    return elem->Item;
    }
  else
    {
    return NULL;
    }
}

vtkHMXYPlot **vtkHMXYPlotCollection::GetLastItem()
{
	this->Current = this->Bottom;
	if ( this->Bottom->Item )
		return this->Bottom->Item;
	else
		return NULL;
}

vtkHMXYPlot **vtkHMXYPlotCollection::GetFirstItem()
{
	this->Current = this->Top;
	if ( this->Top->Item )
		return this->Top->Item;
	else
		return NULL;
}

// Replace the i'th item in the collection with a
void vtkHMXYPlotCollection::ReplaceItem(int i, vtkHMXYPlot **a)
{
  vtkHMCollectionElement *elem;

  if( i < 0 || i >= this->NumberOfItems )
    {
    return;
    }
  
  elem = this->Top;
  if (i == this->NumberOfItems - 1)
    {
    elem = this->Bottom;
    }
  else
    {
    for (int j = 0; j < i; j++, elem = elem->Next ) 
      {}
    }

  // Take care of reference counting
  /*if (elem->Item != NULL)
    {
    elem->Item->UnRegister(this);
    }
  a->Register(this);
  */
  // j == i
  elem->Item[0] = a[0];
  elem->Item[1] = a[1];
  elem->Item[2] = a[2];
  elem->Item[3] = a[3];
  elem->Item[4] = a[4];
  elem->Item[5] = a[5];

  this->Modified();
}


// Remove the i'th item in the list.
// Be careful if using this function during traversal of the list using 
// GetNextItemAsObject (or GetNextItem in derived class).  The list WILL
// be shortened if a valid index is given!  If this->Current is equal to the
// element being removed, have it point to then next element in the list.
void vtkHMXYPlotCollection::RemoveItem(int i)
{
  vtkHMCollectionElement *elem,*prev;

  if( i < 0 || i >= this->NumberOfItems )
    {
    return;
    }
  
  this->Modified();

  elem = this->Top;
  prev = NULL;
  for (int j = 0; j < i; j++)
    {
    prev = elem;
    elem = elem->Next;
    }  

  // j == i
  if (prev)
    {
    prev->Next = elem->Next;
    }
  else
    {
    this->Top = elem->Next;
    }

  if (!elem->Next)
    {
    this->Bottom = prev;
    }
      
  if ( this->Current == elem )
    {
    this->Current = elem->Next;
    }

  this->NumberOfItems--;
  this->DeleteElement(elem);
}

vtkCollectionIterator* vtkHMXYPlotCollection::NewIterator()
{
  vtkCollectionIterator* it = vtkCollectionIterator::New();
  it->SetCollection(this);
  return it;
}

//----------------------------------------------------------------------------
/*void vtkHMXYPlotCollection::Register(vtkObjectBase* o)
{
  this->RegisterInternal(o, 1);
}

//----------------------------------------------------------------------------
void vtkHMXYPlotCollection::UnRegister(vtkObjectBase* o)
{
  this->UnRegisterInternal(o, 1);
}
*/
//----------------------------------------------------------------------------
/*void vtkHMXYPlotCollection::ReportReferences(vtkGarbageCollector* collector)
{
  this->Superclass::ReportReferences(collector);
  for(vtkHMCollectionElement* elem = this->Top; elem; elem = elem->Next)
    {
    vtkGarbageCollectorReport(collector, elem->Item, "Element");
    }
}
*/
