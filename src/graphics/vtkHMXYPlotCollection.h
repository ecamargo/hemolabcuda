/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHMXYPlotCollection.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHMXYPlotCollection - maintain a list of polygonal data objects
// .SECTION Description
// vtkHMXYPlotCollection is an object that creates and manipulates lists of
// datasets of type vtkHMXYPlot. 

// .SECTION See Also
// vtkDataSetCollection vtkCollection

#ifndef VTKHMXYPLOTCOLLECTION_H_
#define VTKHMXYPLOTCOLLECTION_H_

#include "vtkCollection.h"
#include "vtkObject.h"

#include "vtkHMXYPlot.h" // Needed for static cast

//BTX - begin tcl exclude
class vtkHMCollectionElement //;prevents pick-up by man page generator
{
 public:
  vtkHMCollectionElement():Next(NULL)
  {
  	Item[0] = NULL;
  	Item[1] = NULL;
  	Item[2] = NULL;
  	Item[3] = NULL;
  	Item[4] = NULL;
  	Item[5] = NULL;
  };
  vtkHMXYPlot *Item[6];
  vtkHMCollectionElement *Next;
};
typedef void * vtkCollectionSimpleIterator;

//ETX end tcl exclude

class vtkCollectionIterator;

class VTK_EXPORT vtkHMXYPlotCollection : public vtkCollection
{
public:
  static vtkHMXYPlotCollection *New();
  vtkTypeRevisionMacro(vtkHMXYPlotCollection,vtkCollection);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Add an object to the list. Does not prevent duplicate entries.
  void AddItem(vtkHMXYPlot **);

  // Description:
  // Replace the i'th item in the collection with a
  void ReplaceItem(int i, vtkHMXYPlot **);

  // Description:
  // Remove the i'th item in the list.
  // Be careful if using this function during traversal of the list using 
  // GetNextItemAsObject (or GetNextItem in derived class).  The list WILL
  // be shortened if a valid index is given!  If this->Current is equal to the
  // element being removed, have it point to then next element in the list.
  void RemoveItem(int i);  

  // Description:
  // Remove an object from the list. Removes the first object found, not
  // all occurrences. If no object found, list is unaffected.  See warning
  // in description of RemoveItem(int).
  void RemoveItem(vtkHMXYPlot **);

  // Description:
  // Remove all objects from the list.
  void RemoveAllItems();

  // Description:
  // Search for an object and return location in list. If location == 0,
  // object was not found.
  int  IsItemPresent(vtkHMXYPlot **);

  // Description:
  // Return the number of objects in the list.
  int  GetNumberOfItems();

  // Description:
  // Initialize the traversal of the collection. This means the data pointer
  // is set at the beginning of the list.
  void InitTraversal() { this->Current = this->Top;};

  //BTX
  // Description:
  // A reentrant safe way to iterate through a collection. 
  // Just pass the same cookie value around each time
  void InitTraversal(vtkCollectionSimpleIterator &cookie) {
    cookie = static_cast<vtkCollectionSimpleIterator>(this->Top);};
  //ETX

  // Description:
  // Get the next item in the collection. NULL is returned if the collection
  // is exhausted.
  vtkHMXYPlot **GetNextItemAsObject();  

  // Description:
  // Get the i'th item in the collection. NULL is returned if i is out
  // of range
  vtkHMXYPlot **GetItemAsObject(int i);
  
  vtkHMXYPlot **GetLastItem();
  vtkHMXYPlot **GetFirstItem();

  //BTX
  // Description: 
  // A reentrant safe way to get the next object as a collection. Just pass the
  // same cookie back and forth. 
  vtkHMXYPlot **GetNextItemAsObject(vtkCollectionSimpleIterator &cookie);
  //ETX
  
  // Description:
  // Get an iterator to traverse the objects in this collection.
  vtkCollectionIterator* NewIterator();

  // Description:
  // Participate in garbage collection.
  //virtual void Register(vtkObjectBase* o);
  //virtual void UnRegister(vtkObjectBase* o);
	
	/*
  // Description:
  // Add a poly data to the list.
  void AddItem(vtkHMXYPlot *pd) {
    this->vtkCollection::AddItem((vtkHMXYPlot *)pd);};
*/
  // Description:
  // Get the next poly data in the list.
  vtkHMXYPlot **GetNextItem() { 
    return static_cast<vtkHMXYPlot **>(this->GetNextItemAsObject());};

  //BTX
  // Description: 
  // Reentrant safe way to get an object in a collection. Just pass the
  // same cookie back and forth. 
  //vtkHMXYPlot *GetNextPolyData(vtkCollectionSimpleIterator &cookie) {
  //  return static_cast<vtkHMXYPlot *>(this->GetNextItemAsObject(cookie));};
  //ETX

protected:  
  vtkHMXYPlotCollection();
  ~vtkHMXYPlotCollection();
  
  virtual void DeleteElement(vtkHMCollectionElement *); 
  int NumberOfItems;
  vtkHMCollectionElement *Top;
  vtkHMCollectionElement *Bottom;
  vtkHMCollectionElement *Current;

  //BTX
  friend class vtkCollectionIterator;
  //ETX

  // See vtkGarbageCollector.h:
  //virtual void ReportReferences(vtkGarbageCollector* collector);

//private:
  // hide the standard AddItem from the user and the compiler.
  //void AddItem(vtkHMXYPlot *o) { this->vtkCollection::AddItem(o); };

private:
  vtkHMXYPlotCollection(const vtkHMXYPlotCollection&);  // Not implemented.
  void operator=(const vtkHMXYPlotCollection&);  // Not implemented.
};

inline vtkHMXYPlot **vtkHMXYPlotCollection::GetNextItemAsObject()
{
  vtkHMCollectionElement *elem=this->Current;

  if ( elem != NULL )
    {
    this->Current = elem->Next;
    //return elem->Item;
    return this->Current->Item;
    }
  else
    {
    return NULL;
    }
}

inline vtkHMXYPlot **vtkHMXYPlotCollection::GetNextItemAsObject(void *&cookie)
{
  vtkHMCollectionElement *elem=static_cast<vtkHMCollectionElement *>(cookie);

  if ( elem != NULL )
    {
    cookie = static_cast<void *>(elem->Next);
    return elem->Item;
    }
  else
    {
    return NULL;
    }
}

#endif /*VTKHMXYPLOTCOLLECTION_H_*/
