/* 
 * Resource generated for file:
 *    1DFlowDirection.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DFlowDirection_width          = 75;
static const unsigned int  image_1DFlowDirection_height         = 38;
static const unsigned int  image_1DFlowDirection_pixel_size     = 4;
static const unsigned long image_1DFlowDirection_length         = 1512;
static const unsigned long image_1DFlowDirection_decoded_length = 11400;

static const unsigned char image_1DFlowDirection[] = 
  "eNrt12lo23Ucx/Em+ef6p20SZ5L1imsu0602rWeFoajQVQUHxaEPnBv4QEQfCIoIzroqXY"
  "X4wAnq5lUrisqc9zn2wAt1+ED3QJlSxKOzdofHQJ1urb5/5RMI1Q3Fror5feFF/0fS/PL5"
  "/67U1dmyZcuWLVu2bNmyZcuWrYWqTCbjgdPe3h5oa2sLpFKpQCKRcOC16fwhqxASZLWErE"
  "4gqzQ5JeHadGbLAz8ifr8/HQwGezHgOM5VXq93Pa73eDwXc/805HAcwnBqOCuXTE4kmwGs"
  "43iUa2/gbTwPc74Ol+IcnGy6Io7X+2ulfAigiNW4E6/iC3yPH/AdduJFbMZ6XImL1OdMbo"
  "v0f/zqdz49iwWtQCDgEW+Fj+L5+6vaV91Gn44r9wNzvodTdT+EenTiMtyB1/A1fsFh/FbF"
  "5LcLr2ML7sZ1OF/jNI3FiJuxrc/xLlRuZOPARbSCqJJktUT9IY+s2pnSM27RNXO/Q3/N65"
  "ukDe1Ypr5xgfrKBvWrPZjGzJyszPkh5Wj63BQ+xnY8gluU+XnoVpvix3KsMtcaHoNswkhy"
  "XGA+WUlGN6LMyzbiXmzCfRof5vyeqmv3S+X+Zp0/gIfwoJg+8qb6zCR+PkJW1aaV208ap6"
  "YvfohteFztuI22r43H48tzuVyhr6+vaXR0tB5BeOcxq9m86EOmHxVwLlndRE5m3vgAn2he"
  "+RITsrtK5dpXMqHvM1llt97/DQ7oux/+Czn9mRnl9q3+50d4h3ZvYX3dGI1GrykWi2ePjI"
  "ykyuVydGxszD8+Pu7FPxqfpVLJ8Biu68bC4XAHn7eC9XyIvLaT1y7lNKXxshf7sF/26fpU"
  "lT26XrFX1yd1z2R1EL9WZTXzN7M6qP8zqT66g/a+TLsfi0QiN7N/6x8cHGwaHh6OkVVgHr"
  "PyGt3d3WEsKhQKuYaGhtVktonP3kobzLh5Ek/haTyHF/ASntW4Mvef0Jgw58/odebvVl1/"
  "WOfvq+/tV/84pMyOls20+qHJ90d8hvfUDjPGNzA21sRisd58Pp/p7+9PklHE5IR5GYPKal"
  "ZPT4+DSFdXVzydTjcnk8lsY2PjMuaBHl56Ok7VnsfMpSdpvi7p2ilijrs0z3do3Svp3hm4"
  "UHP7CF7RmDxaVjPKxvTPT/GWnoV5/xVaK3ph5o4m+lSErPxk5TMZwYN5n+fJyQMf/PSvWa"
  "2trUHGpqv13qzNrvbUZg8Q1LGre5X7lXsBHYf1/pjyXYUh7T8njrBnMONrHO+q75h96q24"
  "HGfpeWW0Hkf1ec6/sdc6RntRv/Ydl+B27Tk/157ggPZUJp8dGqt34QaswQrl0zxnH+fTvu"
  "r/VOaZB1kr8hjAIB5VLjs192xTPldjJZZrHC9WNrXye9B813rWijTz35lYxfxyLetGGUPk"
  "tla/AZdqjxuq0d/Olby8qVQq0tLS0oR8IpFYyt6xxPpRZEps1pwWWujfK//V6uzs9CEIl/"
  "2im81mXfZCITh1tmzZsmXLli1btmzVSP0OZVFMqA==";

