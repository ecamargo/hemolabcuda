/* 
 * Resource generated for file:
 *    1DStraightModelEditionModeButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelEditionModeButton_width          = 32;
static const unsigned int  image_1DStraightModelEditionModeButton_height         = 32;
static const unsigned int  image_1DStraightModelEditionModeButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelEditionModeButton_length         = 1668;
static const unsigned long image_1DStraightModelEditionModeButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelEditionModeButton[] = 
  "eNrNl3lsk3UYx+sVj4jGI0JAUacbgiBDCWSJiJBlMMAhMEdEBAyXOJgThsCEAQPkiAw3dk"
  "APtrFu685uOFrWt8e2tuvaru0Oxjg2AuOSRAxGCYqajy8zmc61YMZK+ONN+36fX9/P2+f5"
  "PcfvaEMdR+/xy6mP49eWh3CadnZpLiGKnxsexV2r9DvfrIvjfKMEuympS6vRRtLheQCXTe"
  "V3vq6miGnyoG6a8kgykfJRd8X/lYZkZJqncNSVd2llFUtILXv+rvDr9XOhTUKdfmWX1l7z"
  "Cn+23YfHrvY7v8ltYV9xOFGK0Z33aiGdeVlDqNLv9M9+N22nWT8OlXY7isO7ydHuoUQvZ0"
  "7mUCyGAXyuGsgmVTQ5mt2kqDeRdWgdraZgnMbE//X8Fo/llvYGIazT36vzg3hf9gazFCOY"
  "LhvJetUwOCNhf3kAUfIQ0R9vMks++m/9lAS3EOGb6dRw2TaL8/pQjptTOd5o9rm2TEhmrS"
  "rQqy0m70X05oIeemzuq5SLcfH6f5xWTplWgXMMx0qnUh7/HEL6IlrdRq/rVZUZRMhHeLVN"
  "lgWK/NIe+lTZUAoFaQ+92SM+75AeXYcej3krdbtfIDt6CMlzh1FbuNEro1CXQaTCOz9CEY"
  "jBC3+GYijFXvgarYmU1ovIL/3B0tJW1uxIICs6mPz4SXjEuuJPvt1qYU9VM4XXfmdDzVkm"
  "fOuk/0Ijs+d/gUO732f8C0T+DB/+D/fh//dE/xf9h59WZiHv2g2+cV8U97Cbsev1SMblsy"
  "K+kCaX7xxQC2kszH7dq21RTgBVlpIe+pKDQd32X4VgRtrxA4ozV5ktdzFylY4np6sYP78A"
  "l8N+m/x7l2vHJD16m8OYACclYj9c3r0+WmTcaJWIvwvtvLdZreQ2tZN94SeWq1sJjtPRb1"
  "oe/UMVFJVU3b7e6hZw1jKYBnt5N/2AehF2+/2kFnXP85vr2qoDxfeKwVlnJd3oIVeM+Vqh"
  "nZDN1Qz6qBTJ6HRS9h2h0W3rVU2sswuEyl6mWC/r/CwzZHnfu1oz8gtX2WLtYMJOMwEL1D"
  "w8/gDRX6nFd6vtdU3+LHsKS7PCOr8nFnwq1r6RPdYYq2vJPXcZedsV5iibGB6r5YnwHEZM"
  "lWI0WHrNrjApmawYJPpA1y0PMw5t/WdGqTaT5jiJ8sp14nTtTNxlZfB8NQMnZ7FP+t0d9a"
  "NI+Vud/ebfWubhJLEWBuB2VHbWOKnWwf5Lv5FQdZopSRaGL9dw/1gpu1J1NLp6F/Nmse+e"
  "tQ7msPCsV7tR9xK/ND+C2VDMwWoN2wwtTEpvIHi1jsfDlUz88AD1dxDzm33/pPVpMa+Cvd"
  "qrNFNwVgdhL9tMiyaWzJxEJq3Lp9/sSl6bqaRUbbrjWSAmO4wdhdFebXvV61mT9wEeYSPX"
  "zyTSYYxCnjSPgW8nIlVa+2QWWZwZxtYCX/wNZBQu4fuWr7nk+oRzpghs0jFk7k3B7bT1Cf"
  "9mzm3zwU9TJ6ApWsbVE2s4bZiJo2gB9iNysR6Z+mwWuxU/NX8zIe+MIn1TCLbiFeLcVtLn"
  "s+Ct/B8bvxiJ5EG2JHxJY73FL7Oor/1XZ9GzLOZjVu6J8tvc3eyp5YTlGZxCz1nAZtZjKh"
  "rLj57HaHBU+IfvNnPBNoBWo/dzlks3k9PiOaSxXrjnz9C+rr8AgbPWig==";

