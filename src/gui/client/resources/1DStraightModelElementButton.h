/* 
 * Resource generated for file:
 *    1DStraightModelElementButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelElementButton_width          = 32;
static const unsigned int  image_1DStraightModelElementButton_height         = 32;
static const unsigned int  image_1DStraightModelElementButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelElementButton_length         = 1304;
static const unsigned long image_1DStraightModelElementButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelElementButton[] = 
  "eNq9l21IU2EUx+8tP0REZYFFEQWVVJTkZl+i+pBk70X0Zi9U9GXO3l8wIjSkiKTQ2qY5a1"
  "q6pabkXZtuc9NSp5uZpWUUJERKWJuzkoKiD/+e58EbV8maztvgfNgL+/3Pec7/nue0tXjb"
  "nz9tCHjcrp5KW9knGp46Zw/9rK3FK3dQdm+104qsjCRokheRWIjM9FOocjxAa3M9yG/kjN"
  "6G2sqezKtHUH49DG3FPF7e42HTjYY2TYX6mkq5+QGbpfSTLnkOXpXy+FrH45ubx5syDleS"
  "ZiM/T4/mxhpZ+Rah+HP2+RloF3h89/D44eXx1syhRKNAgvoYiu/myakhUFtt67l2cQfcBh"
  "4dVhIWDvU3ObzwZMNaUYVDiQlyagi0PHEHrOZ7yEjdAEPqRAjamWh1p4O+/H4/BEGQUwPz"
  "wbOmOjx0WWHI0UGVcBiWchdjixrMZrNcGkQfsvf0vykjkbBo3qKGbn838aMLJ48cRZEpF0"
  "+8j2ThSzXQfGneooas8lwsPb0GK/etRv5tPZo8D2XhD9TgsjthcBjBqcLBpUwDd2YaorYr"
  "cLfAMBJ1+CNfquEEqfny03GMHZY9D3x6JCbtjMCuaAUKjbmh9sOgfKmGNfvXs7z5jLngLs"
  "/B2thwGDgOO5comYYQ6vBXvqjBlH8L0duUmBAfgbjYiXg/ZhS6CF9DNcSEpOGffBr0v+l5"
  "71bG4DZhfiABEu9I6EjsUA77LILii3WgjN0xCtwgzM4+DR19GuKHV4eg+aIG6v9dS2KQJd"
  "HgI5FNz0KhZGc1BG8Oif8nDR9JNIWHYcHmyRi3Zwoi46NQcCcn2DoMmd9PA6n5TcJfvGky"
  "88Uo6g/iE+oX+n0Q/TAsvtiT9LzjiQfH740g7Ej2fODOTcGqMxvZcyOIeTFsvrQnFVuVLG"
  "/u3FSMPT4L9sdVcFTY2Rz5h4aQ+L+9aTQg7uA6rDq7BRaPnc0Ln88XzNwMmS/tBzofad4+"
  "v6/f7E5UD6phRPgDZ5Z0dvu7u8k9ygV14gnmzcb6arx45hlxfr/7g1rF8vb1aXjZVIBCzQ"
  "qkHldAr7sAcucDufePOH9gHSxWJ1rqrqJWz+F1CblTFnEwXRoHvSYJXrdLFr60H1QqNSza"
  "6ei0cvjewONLDY9mIw9tShScdkE2vuiLG9orKEobiy4Hh5+NZL8gu0VrIeXPh6P8vqx82m"
  "OUoUmJxlMTj84Knu0YdM/KTDsg7lay8cVzKCnMw7Xzy2C8FI6c1OnQkL2u0iaIuyVl98q5"
  "4zU31rJdtqzUBKtQzPKW7LWU3f4f9uxB9+9fASm3YQ==";

