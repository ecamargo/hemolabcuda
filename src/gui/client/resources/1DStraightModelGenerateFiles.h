/* 
 * Resource generated for file:
 *    1DStraightModelGenerateFiles.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelGenerateFiles_width          = 32;
static const unsigned int  image_1DStraightModelGenerateFiles_height         = 32;
static const unsigned int  image_1DStraightModelGenerateFiles_pixel_size     = 3;
static const unsigned long image_1DStraightModelGenerateFiles_length         = 1656;
static const unsigned long image_1DStraightModelGenerateFiles_decoded_length = 3072;

static const unsigned char image_1DStraightModelGenerateFiles[] = 
  "eNq9lgtMW1UYxxeniSZmcXHJiC7TRTuXkWnIgjMYY+ImUUbcXDY1C1EzBNQx5mA8SimUQq"
  "FsDFegEB6FlmehlLYr43XLYy0UWmgpo12R8q4MQaoYKWHBiH92XdPdAVkG2cmXm3NOb37f"
  "Of/vf06v2dRtfoqhV0UsWZ7Tt6WTQwNx+m/TC0Zt2VbxNc2X7aZnXHx1w2kMDV3ireI3qy"
  "X+hQdcw7JG3hnB4S3Up6mFV1C/U9+tIIfyuuBs+Z4t5PeqAlaGt6EK5HBU/QaGfTrZVvFv"
  "Gzuyq46TmsiInADhgXZV2iaZPW2cAdUH4oZUQd210oYMqaoQ2I4Wj4viV1ji85jJlLGEN+"
  "jWNi+8+QR8E3EUIkRU0E4Uvn1KcAhPpthzZfzZAuX+gCIf7AWxOjO87SrbLzkpkZPMdo94"
  "JiMr8+chq2k9vpzgxYhp7jOWfj1TdkTbWz9kNqBPTmIBMcxwp9P578NtdmZGobhBEMR6Ka"
  "qac7Fmdzje5BCht/vVJH+grwsB34K/tLS08nADX6xsmZz9a70U4EMWd/6ozSLScsY1zQgM"
  "+w2d4OOdNfnz8/PsdH5pbVNVjexKWurj8O0jQ+DP1NdOVRYP9GgMulvw1RfCd+ISo6anp6"
  "GGVCrl5+QgioqKBIX5wCaxWSkczoWwC+hTdkHhr9reZoE+U/KK2dT4QVGurqPFqNd8U/5h"
  "Wkayw+HQaDSQgs1OtFgszvtt6UFbXl4uLatoamp0TwGfB4o83fn4NUH56Z0awQwr2h7+vZ"
  "WfDn6c7OOa2orFhQXsbmxsLC8vD0+KVih3uUQxNDmj0+tcKeDPBfN290vSoE5ZGd5hlAaN"
  "xoZN//C1/dyXlsSg5dGDjfJ8qI105fxsSGGz2QCkpCgsqUQtauTKHH7W/3dC87cTnXvdLw"
  "GhPFin246DrBcXjxza++tX/khxN/psrzBrccQ2HRY4mRCdz02BUKiFRCKRujWUI5PHs1qt"
  "qMiadu3WEccK9uEUH+PvE1Vfb70Uovd+C1mwkTluguM6dyrgpJa2m/n+uy74TbcG8REwLT"
  "cleU3+jyK/EKEvOqyK4JM8zzpFdU3oOaUXDUxksZ/4CM+613byaXsKYiIAhNTWRxpsgKP9"
  "KLyurewTwavYAvrw/PHcN1OLoySVIqDKj/oA20LbjWe6x47IXS9yPven0+mwJelV9xgfn1"
  "hz/bhqcKe5hrjxoFKDsgwpSnhXLp36DFlI+JmD+2MjfsL64aKFB83pXLhv2NU+Zf0Dxg5U"
  "+Saxi5JR1fj6bM/z7YS0qV52Ofxiks9h8AE/4u2NoxQZGYkt4CwgWKzE+IQERhwzjhlvvv"
  "ML9kW5+Yc6X+4nvCj89no//a2X+ntVOMWtMnFl4FnIgpWHBH+H+sKuLosu3vvnj4V7dx1O"
  "+5xzYHgKuSioMJEvt/o8ZTJTFodbmiwHDjLKXSrMJ+8Ek8mUkZFBLjiWwYimMyKi6GT0mQ"
  "eTkpMoqKBiX07VRnxsQasm1K0NiIK8XCgPrX93/Gn/bQ5nFms2DE5oB0bJDjJSULDlBnzq"
  "v0ZtNf5ZIDLqiD8dUn+yBGQVrl3lbob/BLGxPpuPNeubr2CElry3eTjKB3/2EIco83Asbl"
  "eTXrlZvrFjqssDHyGUeXzi4isL/jc/3S/tx4z/AKHvvZs=";

