/* 
 * Resource generated for file:
 *    1DStraightModelOutputModeButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelOutputModeButton_width          = 32;
static const unsigned int  image_1DStraightModelOutputModeButton_height         = 32;
static const unsigned int  image_1DStraightModelOutputModeButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelOutputModeButton_length         = 916;
static const unsigned long image_1DStraightModelOutputModeButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelOutputModeButton[] = 
  "eNrtl+1LU1Ecx/c/5MswKO1lqHObzrGps2S5qW0+YCvSdFqIiqGCD6Ep9PCi0ixWvlibum"
  "FSgqVm4RyGqdNtXneuFRkoChqCK/cPfNsu4QNtbcTuRuCL74t74Pw+v/M79/dwaGruG03N"
  "7URIXrbbI0RI7t9+MN/trU2MwsjfOcjPzc5idMQ/4h/SjAWktgqk4yZo61TY+aS+FkQkBF"
  "GcB8mRh9KHwHzrB5CUJNCzln1f5LKw8cnDeyAXCw6vyTJB63vCwxcmYeJBH9RlW6hpWMeb"
  "10ugjTpmnbZNs8onzQ1wFuYjJfMn9L3LMPQtI13hwv3OFZDCPJDbt9jjm/RwcuOh7aKgvL"
  "S9t8c+Pw+uxA3zs5cgmRns8B0zIMkCOI0GJGXsYmSEOrRPb/gEabYLzlQx6IHekPNJgQqk"
  "rBhFV7+jsXXN596S65swqO6C/Eu9+mj2y8/xnNubb1V16yi+tunXxpTFDmHqNpyeukDrng"
  "bN9p7L+1/55Z+OxePmSSa+gWypS7egLTeB5CuDY2u7QCQi5n598RVyOSSSXCSm7cI6sxDQ"
  "nsVshyJhFgt8UVC56K0bg/XP0dS26pPPEyhxMrYQ794uBh3PR92foTt1GSTQ3DLyCtbkNP"
  "DSdzHx3hGy/jfu8VXGp0BJ/56L3prdJu5BZ/fXkPffhpY1DMV4+tJgv292TSWmz6qQmuUC"
  "WQx9/7db59ER0wJHqeZP9p122HjJEKT9wMALwtr8UX3FgckTiaBHh/bZlRWw84SQiNZgNN"
  "Gszj9joxTUCeOwxfFANCUgqlyYuTKck676Y4d8/uo3LqNcPAY9vwpP+I0oylthcjSc85+2"
  "5wsqb2ygum4Dw8NL/9X8GR19HBwOh3VFRR3zyVddyEZ83Jm9OLAljacHHeBH+v0Z0ff3L6"
  "CIi0Q=";

