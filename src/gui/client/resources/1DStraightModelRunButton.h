/* 
 * Resource generated for file:
 *    1DStraightModelRunButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelRunButton_width          = 32;
static const unsigned int  image_1DStraightModelRunButton_height         = 32;
static const unsigned int  image_1DStraightModelRunButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelRunButton_length         = 196;
static const unsigned long image_1DStraightModelRunButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelRunButton[] = 
  "eNrtlmEOwBAMhXt4Z3OtLrbYWNoOszc/KnkhFf3qIcHMxC6KMTJSEp+IIAohqPyrDha1Lz"
  "T6Wke+xMv9E//v/Tt/HX7ZpLkv+Vr+Mj5awxv+DA9G+C1+IM5fq0tr/v6c73znr85PMaT8"
  "z3/6rup2ZoRWyc1jI1bdLzRfis/gW7mlmmbzO/yH8Fv87uFvv1Ej9A==";

