/* 
 * Resource generated for file:
 *    1DStraightModelSegmentButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelSegmentButton_width          = 32;
static const unsigned int  image_1DStraightModelSegmentButton_height         = 32;
static const unsigned int  image_1DStraightModelSegmentButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelSegmentButton_length         = 1272;
static const unsigned long image_1DStraightModelSegmentButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelSegmentButton[] = 
  "eNq9l2lI03EYx/+/WhRUihUZdBmYbtOVvuigU7og6KJj3RcFvqlXUW9K7aIsImtqqXnknL"
  "flSm05zVKnU8yrjAh8UdlJzEoKil58e34//cdmWdP578WXsYN9vnuOPc/T1lLX/qip1mG3"
  "lXdaLYUfuezVZZ38tbaWOqXF2V0VZcW4EnMEhggdKRjxFw/hXulttDbWgD6jpLpqq6yd8R"
  "cOouSyCm25DE/yGCyxQxF7Phw1lVal+Q5LUcHHuAh/PC1g+FLN8NXG0H6LIfrABBjTEtFY"
  "X6kov8ic+ykhajLazQzf7AzfSc9LGUzhDHqtGrlZaUp6cFRVWDovndbDlszwsoSho4zBns"
  "PQHCIhXZKwXatR0oOj5aHNUXwrDzEnVyE50gsFhxmaZjOA2K9Jqcp6EH3Q3FCN++XFSEmK"
  "w1aNGiZiviHJHtK4hyBFPMh9KJ7z7+aMncFBIvayB/6YT9pL9ZCTmYqHdQ8U4Tt72BWshZ"
  "GYb3s8GKYOR+h8b8xbroPxeiIa7PcV4bvEgWJ+g9iJk4ZDOjQF0ll/SBHToFunQ1ZGymDE"
  "4Y98Zw/7grSYvcBbsFUJarCYAIzZMh6bAqYj25TqaT30yXf2ELZEBylymmBL0f5YudRH9M"
  "UGdYDw4EEc/sqXPWQakzFzjQ7eW3yxgtivRwwRdRHLPQR65OGffC7+3Tzfm4nF++JdT02+"
  "IMWT1g88F27x5Thwhp5ifpWYHT0eXpLiSBsHlgu3+bIH3v96daCLhw+8R3guKA48V/3ozX"
  "7xXTxouj28JzX4qKBdOw6jdvgiQB+MjPQkd+PQb76LB6qHa8QPWTNW9MUQ3h/H/LA4TCfe"
  "d6MeBsSXa5Lnm+fda6evYKsSAsX/xPy5Xtjj3rwYMN+5JkNW8/8HP8EeeWAiisaokMVnFu"
  "XoHx484v/qTVMKFi2bgYWzRqNw3DBRk6/cm5se853rYT+x8nrNzW4P2r48DArfZWbpfp/d"
  "fI5uptnNe7O+pgKPm+2Dzu89N41OHlo0ElI2Mhzd7YekuFOgnQ+09w8638WDtnuPapojwW"
  "ZieEa7ZVs+Q+aZUUg0HEGdrVwRvnM9bKO90Xycdlor7da1DJ8rGRrJS2zkDJTdNSvGl/vi"
  "iuE8cqJH4C3t8z/q6b6g26I1m/M1KC25qSif1xhnGKJC0US/ueMOEzcGv7Piz+2RbyvF+H"
  "Ie8rPTcClqAUxnfJB0YiIM58JhtZjl25Kzu5S88Rrrq8QtW1iQiWJzrvjdTnctZ7f/hzu7"
  "z/v7J2udXw8=";

