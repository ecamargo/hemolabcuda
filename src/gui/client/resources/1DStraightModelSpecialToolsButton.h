/* 
 * Resource generated for file:
 *    1DStraightModelSpecialToolsButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelSpecialToolsButton_width          = 32;
static const unsigned int  image_1DStraightModelSpecialToolsButton_height         = 32;
static const unsigned int  image_1DStraightModelSpecialToolsButton_pixel_size     = 3;
static const unsigned long image_1DStraightModelSpecialToolsButton_length         = 1148;
static const unsigned long image_1DStraightModelSpecialToolsButton_decoded_length = 3072;

static const unsigned char image_1DStraightModelSpecialToolsButton[] = 
  "eNrllllPE1EUxz8P0s7cZabTJd2ETiWYAoWpLZZWiiAKUiOGhEVIiglxIVBadlASg1CLgh"
  "SIsQJtCaBGDCYkxheNie8++AU8LQlpgJgW6BPNZJZ7b39z7v2f/7mzt/th7xwcnV1thMqb"
  "m+/kiK83aIBPKIpEIrngCwJHKEsI8nq9Zw7f3tpUKBSUUkKI2WzORfxqtRr4GGOVSpULvk"
  "aj4Xke4oeJ5IJvMpk4joMpCILwn2FPn41esVstJUWRpddZ8R0OB8SPMeV5ZSwWPzpgbDxY"
  "aNJSTo5JPovy7A5rVvyOjg6EEKXJV6ytrad3vQzPFF8WMbmAsQwThDGCc39/X1b8UCi0ry"
  "9IMDs7s98Yi6/a7TaMWRbJGCafhSvLCQpNYGDoBBKAsjj5Y6amJuCxyXuDcgzGcjAFgGFy"
  "GHFO57UTS2w0GoGuVAplVotSRWFBWDaPYAYjFrOk1FI+F54/GTkWj7rclbAO/X3+nz9+uV"
  "xuBEzCEJYRMHVUSIuvMiX/+ft7+/O7heXnb1fD8PjxU6za40QgHGaA6fN17X79UlZWihDm"
  "OKHxVuNWYiMrES8VizIsY6hcjmVEAemIWJQPkcPKIERMYgGvgPyAQsQ3NmRXiMbHxykFC6"
  "nq624PBkaGgmNVVz2EJAUFtkajHQwOpwqFJpVE2O12ZcVXgnKCsDC/mN44NDhCCNXrDemF"
  "KDkdwlRIpZnDu7u7IcPgfLTLai3X67VphUgFfIQYUSzMnO/xeIAfjx9j/GqPC3YWuNnZ2Y"
  "D7lMWSakA5Wl5ZzJDf0tIC7puenj6ucqq0OvXIyIBKDVyW4xQgASgFKUQpGh0LZMKPRqNg"
  "/4KCi/HEanq7JEmIJSnbQiLJfT7fQVd7eychLOVkicT7TF7R2toKEMIjqdLS1uV11UhEkc"
  "xDp7Oq5robYXlT0+HNvfneXfjLzZv1madokWhhZOBLjBhq1Jl7eh6nirMEoYbDc4fGLy0v"
  "w6xFUcy2IKy8WdmMbR48ut1uUNPv9x8athiJwMLZbLZTbl7BwDDBnMGgS2/8/m23yuUEp0"
  "1OTp5+f5Qq7DAFrU75oKfjRWhieKy3xGoGl50++IOjrraB55JfEWAHTOTgg5qa2rPd5ePr"
  "248e+jvvd/c+GUjEzsWH8T9/WC3Y";

