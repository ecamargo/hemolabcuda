/* 
 * Resource generated for file:
 *    1DStraightModelToDetachSubTreeButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelToDetachSubTreeButton_width          = 32;
static const unsigned int  image_1DStraightModelToDetachSubTreeButton_height         = 32;
static const unsigned int  image_1DStraightModelToDetachSubTreeButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelToDetachSubTreeButton_length         = 752;
static const unsigned long image_1DStraightModelToDetachSubTreeButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelToDetachSubTreeButton[] = 
  "eNrtls9LFGEYxyfEQ0dvIlHgj40t7BdeOggeIpJyVXbZQ4gEhRdhCzEo8LB0EQ+bbKRrzQ"
  "xtrdWkK7sb5eq+Y1vrUruNPya0PLRCRv9BBBHE15nJXVx9Vzq8M6B4eGaY5x2ezzPPfN/n"
  "eQFw2EWmyL34/bkcSnIAed88ceOnehAL70dhNn820Ysfnzhkk3cLrFTche+LZZj/IJnOT6"
  "TGcUmwFXFGp/xwCaexrGZM50/P+MFPVuBjJlZgRV914X70kCX8ObkDyHHIyD0F1mqqBn9z"
  "B7CYjcAKDY6Em+EWGwxWhAyjM3gUb+UBU75fSfZjSW6EFO+H+NqHUHwQE7KAy4/sSM9U4r"
  "pUBa/UjdCkD/ciXgRf3sZK8hSUN3eY5KKS80a9bz63oY0/AadYj1b+JPqkY8A3Dg9i1XAL"
  "Z7V6nIFTaPjn/8phgTiY8KPEj1tSHTWW59kRqv/G01rEtP/Cgi9NB+AQ6qmxLvD0vC7ydo"
  "yRh0z4Y4kAXCKd7xDp/HbRjvAe4b/Q+O0l6t9cov4tWv3HGfEjZAhXHx+nxroWqqb6u57Y"
  "mOlPJU349YXbNtv+rHDIvaszrKg/pnnoayo5x4Q/l7iCtfRhqNlYUTydq89C3Yry1d7T1x"
  "TZY14v9nq1K4ewzBt349nKc8hm5kYulrFpPKtqkGdvZZXyMz/7ebbpfbMeTdXcBkPfW7S1"
  "/H60THf/o4t9/j5/D/H1mbbT/mM183bqP6UYut/s/mOGrQND2bxs";

