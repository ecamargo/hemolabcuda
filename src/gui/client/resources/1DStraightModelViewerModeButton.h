/* 
 * Resource generated for file:
 *    1DStraightModelViewerModeButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelViewerModeButton_width          = 32;
static const unsigned int  image_1DStraightModelViewerModeButton_height         = 32;
static const unsigned int  image_1DStraightModelViewerModeButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelViewerModeButton_length         = 1904;
static const unsigned long image_1DStraightModelViewerModeButton_decoded_length = 4096;

static const unsigned char image_1DStraightModelViewerModeButton[] = 
  "eNrFl1lMlFcUx8fYh7YxTd/a1KS0D9oobUxT626U4obCaIsgiAgqIIILrrEUrBQF2VTcsS"
  "glLlAgwwygLCPMvi/fLMwCA4OOgAoFhmnRB9v8e+dLQyUO0ynY+vBP5p77TX73nHvPOfe2"
  "6uQdrTr5wGuSm+0igjc5jCfxR9ub6DRcH7X1GlPwu/VdtOnZ+Kf/e5Hrr3V4/a5dn4VuPQ"
  "NWqgR6jQQGrRRt2j14qHsbJm0NdGrxRPkDvvBl6gbs4CyluTq1BJRKDI6oBLtZayERcifj"
  "v098paYUtwV+0KrqoVGKoJILwBNkorh2EYS8Roj4XHAb68CprkR+7kmU3yp5pfxeUwqe29"
  "6ARV0ApZQPqagZXdoENLHikBC/HclJiTiVdQJ5eaeQlvotgoPXYvmypSjIy3olfCMlRo0o"
  "DgdYoRDwGtAiZSNxXyzK2UWwdFKw2hUwdSpgsWtg6zKg94EMSkkNordsxppVgZCJuP+Kbz"
  "dcxYA5DE2Kq6iWXkedW/wbiPlpIYl7LCKjmKDabqFEk49y6hzRefysO4/bVCFuUnlwOpLx"
  "vHcO+hwc5OVmY/68ueTMiHzm95u2Ah0MnLq3BLtqVyK5LhAJ1SuQxVqH8LDF4FGX0Nlhgr"
  "3D4lGmdiE41C7YbHJ6nJlxDFuiNvnMF6tuI5c796XvrxVfxvGzh3FNnoOOttZx1d5mRKYw"
  "CTqzlB4b9RoELF8GDqvMJ36zsgrbSL69aOPWcxC8Lghl8ouoVF6B1az3qhz+bhIn9uj44o"
  "VC7EzY4RO/hfB31o7lF10qRHzcduTy9qJeVQaTQetVBfwDYMmL6d9mIwV+SxPZu40T5qel"
  "HkHG8XRkNO4AT1kDPaX0qgv8dNwUF6LVqIXFpIdSJiZnYLPP8Y+rGcvfvy8ZP2SkI/VONP"
  "jyO6QWSV8SpZZBp5HT/FLxaZSTvbJZTbhvt0GrUSAiItwnvlBVgcN3l4yxfZ9+FEcOH8Ah"
  "dgSEkgbij5CWSi6CWiGGhvB1hGHUqWl/m3RV4Jlr0NPtwC/9T6CUSxAWFupb/rVGYsRM+o"
  "2+dtR2o/QqQjcEQC1eB700G3KpAAoZYSuJ324u2WerpRX2znY8dHSgr7cZQ/1iDA91Y9g5"
  "RGpzFTasZ/rE7zEexYjlc1j0f9ctEal78+a9B1HNGthVEYQrhk6rhMmoI/lmxv2uTvT0ON"
  "D35BGGBhx46hTimUuBp78+hONBFw4d3I+jRw5OqP66RVFCfBH/PjaGzUdR/SbIDM3EVxvx"
  "9T4eP+ohMe7D0OAAnMRX17ATI64e/OYiaxnsB5tVAf/Zs6BVSyfMz+LG41jtVixetAAp6Y"
  "nIvJuER0+64RwapOWO8TDhul6Qez1NjXewckUgTufnTLj/8FVsRFb7Q6PlkX5bj0/9ZyEi"
  "iYnvquPQ1dVB+zzsdI7KPXbvR1VFGQK/CiD9MWFS/S+5LggXecdGx2rS/4OCVsJ/oR8i01"
  "aBw64iZ0ENs8kAiUiAqspyxMZuJeucjbycE5Pov1KMWOdApJzpcf5ccRTmr2ZgQYA/mMxg"
  "rGeGYDmp8/6EO2PGDDTc5Uyy/0vx2OhHtNbjfCfp09eK30FE/Gf45usQhJB7x6bwUISHh5"
  "H1MLFvz65J3z/ONCeiWJA2Tk4ISJ3+EnxlxRj72dM5mDp1Kh3/yfKzuYm4zPfM12oFiGF/"
  "Sep15Ri7oKUe06ZNw5QpU+g762T4OfcScWU8PvF/G+dlvlszyf4zGAzcLP3xtfBjoqNofk"
  "x05P8ef7eKiy7Q8f/I70Nyf5X9p+eP54GvkDTT/rtVfuv6hPmPjR/DYVg9Tv6dQY+BAbs+"
  "1eP89Okf0Py9uxO98cd9f5pI/j+zfIJBc4jH+QfGAvIufYv0S8/8M/nZSNmbBG4D29v787"
  "W+v/8EJ1nU4g==";

