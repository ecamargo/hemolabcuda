KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_RemoveTresTri.h \
    TrisurfButton_RemoveTresTri.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_MeshInformation.h \
    TrisurfButton_MeshInformation.png


KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_remove_group.h \
    TrisurfButton_remove_group.png

KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_close_surface.h \
    TrisurfButton_close_surface.png


KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_test_mesh.h \
    TrisurfButton_test_mesh.png


KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_transformation.h \
    TrisurfButton_transformation.png


KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_remove_free_nodes.h \
    TrisurfButton_remove_free_nodes.png


KWConvertImageToHeader  --zlib --base64 \
    TrisurfButton_others.h \
    TrisurfButton_others.png
