
KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_Collapse.h \
    TrisurfButton_Collapse.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_PoneTapas.h \
    TrisurfButton_PoneTapas.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_SwapDiagonals.h \
    TrisurfButton_SwapDiagonals.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_DivideObtuse.h \
    TrisurfButton_DivideObtuse.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_Remove.h \
    TrisurfButton_Remove.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_InsertNodes.h \
    TrisurfButton_InsertNodes.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_Smooth.h \
    TrisurfButton_Smooth.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_Par.h \
    TrisurfButton_Par.png
