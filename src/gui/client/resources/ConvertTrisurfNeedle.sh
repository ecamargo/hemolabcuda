
KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_RemoveNeedles.h \
    TrisurfButton_RemoveNeedles.png

KWConvertImageToHeader --zlib --base64 \
    TrisurfButton_SmallTri.h \
    TrisurfButton_SmallTri.png
