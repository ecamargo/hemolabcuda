/* 
 * Resource generated for file:
 *    TrisurfButton_AutoPar.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_AutoPar_width          = 40;
static const unsigned int  image_TrisurfButton_AutoPar_height         = 40;
static const unsigned int  image_TrisurfButton_AutoPar_pixel_size     = 4;
static const unsigned long image_TrisurfButton_AutoPar_length         = 896;
static const unsigned long image_TrisurfButton_AutoPar_decoded_length = 6400;

static const unsigned char image_TrisurfButton_AutoPar[] = 
  "eNrtlk9oE0EUxpPZtGnY2maNVaQU/HPwJHgQhUIhoGdv0ouXJpiLoPGgVz0o9lyxYBR7Ea"
  "S3Qg/FkwcFIYIItpT2EkVBUYiKRANC1u+tM+HNuGsTzFYDE/jR5M3ON9+8N2+2vu8nfIvF"
  "YrH8d6Tu5vy+YW6n71wa+cWV0cix1KwXGad5bY0IuK5zbocvTmUCnGlX1zYQJ4f8ZCLRht"
  "Zu60BXxUkrKi4ODWgaYQTzsA8xkQodJx9h/pI5oT3nzAzH5k/zhnXNeXwNtZ/f9nFksGt/"
  "Wj3Ymtpa2Df3puqkecgktfrx2vK9tef20J+YTIfWyMwrP6vkV3mjc2rO76k/Fjf7hedJjr"
  "Woh7Q10ENmjbfBXyvCH+W79cfzLO+Q0HPJzlLH/riHDuqrahtFoMFySs8H9yN8k9eu+4PV"
  "i/pD3bVa31B/GM/ye4f3ksrXVneHebdG+etES+VV6yV4jboPgz6mfIXpUj6NuVv5C7TY2p"
  "qWUfNuMd9hf/VOpXdir7T+AfZ/OEu/UKvV9lWr1eNgEowljA9ih8EBIzYOjsk54yoeh79C"
  "oXCPpMF38A6cYFYmwA+wChwWv8bmfMzn86fj8lcqle5D+gVIg+fgIfNxUfog8iw+C96DQb"
  "Dsuu7LGP0tQPoNuAA+gVvSwwBYB5Tfp2CR+bsOvoAS2PQ8bylGf3cg3aR1wE2Qkx6mZN4O"
  "gmnwDeyRY1dBS44/gb+921Bf8zMv198Ar+X3s3LsBngFVuSZiK0/isXiA0ivGd52g69gQd"
  "b9PHjE9kHn7y04SraEEDNx+SuXy7dRn8fcHH6fAR/ALhabAp/BfnAZbMj4fDabfVapVDJx"
  "+Gs0Gm69Xh/h/vB7GIyaBUfMA2kwpMbxV4CxZrPp2PeJpR/4CTJ5v00=";

