/* 
 * Resource generated for file:
 *    TrisurfButton_ChangeOrientation.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_ChangeOrientation_width          = 40;
static const unsigned int  image_TrisurfButton_ChangeOrientation_height         = 40;
static const unsigned int  image_TrisurfButton_ChangeOrientation_pixel_size     = 4;
static const unsigned long image_TrisurfButton_ChangeOrientation_length         = 2268;
static const unsigned long image_TrisurfButton_ChangeOrientation_decoded_length = 6400;

static const unsigned char image_TrisurfButton_ChangeOrientation[] = 
  "eNrFmHtQVFUcx7+rsCyJPF1WQEQeyy7CkonKAiZo4pAPRsNEYAEBF1xUyhk0iSlTsbEwyR"
  "5/2EyJPaiGppzScRpnnGqmprGatMy0mkERKxMwq0GUx+17D5eVxyImC+3MZ/YBc8/nnvP7"
  "/c7v3I6ODrS1t6HtpgNukOtEIt3D036jHZIkOZUDrxyAfpUe+m2kcgBP6Gfos/Wb9Gn8vI"
  "IsH8S9ZBspJClR8VGG+g/rPZzpV72rGpgFwErWDKIG+ZAQhkJM4Hc/4qu8+8Gd7CInyE9E"
  "ghqd5TvLLc7027t7L5DA8WxkbT88ya8o5rhFuIlwLIGWv08hQYOoIhImQqp8pnLRmPgVIY"
  "/vkqCE5ONPBGMOdP0cfclH5CrZTL9r9DON0fx9ZveTWUeycBEBiMBk4RZHzpMfSQSZSb8u"
  "+vmPgd9M0t3PT2Y9ycDXnMNH6HOdvEs8lLlcQ79a+o0fdb8ivDzITV5jK0kkU8hkVCCwXw"
  "xq6Qf6YZT9fMmVQWubS4xkKllFHsCrIpf75snY+BX3c5PzdwUJJtNJHrGRUjGX+4TjlDHz"
  "U5ET/dwsiluC8r1E+ZtV8bwPW0U9DCCskZurNo+eXxHMg+JOZrXiZh3we7FCDHMjCdCEaH"
  "Ds+LHRnL/XHPoVO/it798KWL/nYrFXuBc62jqc75co/HQc7+qQHg5QlYyTVDaVhI38no2b"
  "blq32duf2o6amhrBwYMH0d3d7az5KxvWyarkcmnPvC3YECV5LnJtQiz3EB0q4YIo9HkFBQ"
  "Whs7PTGX7j6XfSoU+J4mMTe5zEXfgaq/cXrtMn7H47c+nJUKgWY4iX0Wh0jp8ZyWL83njq"
  "rR9rxX7Rydg/i0i8wTxey/doxoPa3zYVTbbMU6vVqopR9aum30LUcXV71i6HpOJ3mHAUId"
  "wjwjCP/ZcPlik9VylZxzarIhA/5y/96mPdhIuuKvZao+RXXVsdivm4wDX7hvvV84yjh9hL"
  "hcj1gp/ANQW7vx63PGJRKFTjk5zFB66G+UqPTvKogmqc3Wu8Wg2VSiX8urtGlh+1dbXevE"
  "6MMcx4j3w9YwJJIclkLkl0TOjsCLyUviizxaiTzkdM6t4SE1mg8ZE3ExW2rslNo6DGYDCM"
  "uL7I99fZ0SnWQbzfIO3Dc+FCE9KSEj3OzYxouBjuK10y6KT6+fE7SvJyp54pXN0QDMzRuL"
  "sj3hYPc7UZ5u3meeb15khzKj+b75y7va/W1lZM0PojR+eT/VukVmrQT5Ka6PnHkvuvNMeF"
  "S+u8NM+KxU5Q9vV8HMFyNDJSo/EfXnfr19zcDB9fX3GNCq3HvisGf+k8PX+Z5i01RfhJR0"
  "N8L7jKO3KKqK0e3DsvibqQyzPDZEb3JKWvCFCQezWtc/38/Pzs11nn4151KsTrn4ZIf86l"
  "VmpmXC5wVecy+2W/lH69Wj7+Yk4uE7kn14MS5X21CN9R8ZPz9/WV6S/+HcV8od/BIK9zMa"
  "4uK1hZZb+nB/W6a7hnJ3PnzuqpV8LxYef6eXt7268TFRvr1bixsGm/t+vZeHcXW083yFeP"
  "35cO+4pCkoItWMn/We9cv5aWFgQGBkKj0QgyU1ONOTNMNh8/rQdcXG8NkIxQrt31IXsfeQ"
  "9dgGruUxBz6SS/rq4uNDY2oqGhQXDoyCFUVG3Djud2IjQ89NYA8xhlpcP0HLLngzjArFFj"
  "nHP8BrJn/x7w6hDn4z7ThzS8ad/bb+co/08a+yA3eDrLr+6DOmRYMpBhzZgVvSA6C/5cHb"
  "lW+JNUEsfzfB4uD+q7h0Ke52X4nBUyUJxnRuhXvrNcvoQPvNDG2lVkr2dBWMwxjrPHcHwu"
  "uB02ceY5w2sZRuonn9WYpWvpc5n4kQJymrSzU32LMf++/Tx1p8j3UyaeVTTzvuOc4PcpfR"
  "rJD6SVe8MLPCNHchy5ph27I6e+fXkWicf3XIdd7McNI/SLkp9ZiWdXQThM/MWzmWBxFpS7"
  "2bZh56o3t9N49onEe7xGBv28Mc0p8SfnWg4z7jC5Qk5zxy3nGGqOmekw7qx9+nOLOFefZL"
  "f5JKMtmo49zyHlsI4duV/94XpYrBZYygRR5HHLBku6bqZuHCOxtp9f71xZRY620ucd3s8K"
  "9sKeWKTUZplVJJtEOLf+9WVh5kI3jtVgP8vYlDP+bHzLPGfQYrq9wLkRF2XfGMgo+aUUpC"
  "Qw/nrjqhl6nm+Ckc64msicxFj0f7cjqSxpE+PnO1acxxiLBsxV1q6AeP//foXFhQEmg8nH"
  "FGuCKVkhiSSSGcR05/wLg20omQ==";

