/* 
 * Resource generated for file:
 *    TrisurfButton_Collapse.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_Collapse_width          = 40;
static const unsigned int  image_TrisurfButton_Collapse_height         = 40;
static const unsigned int  image_TrisurfButton_Collapse_pixel_size     = 4;
static const unsigned long image_TrisurfButton_Collapse_length         = 2800;
static const unsigned long image_TrisurfButton_Collapse_decoded_length = 6400;

static const unsigned char image_TrisurfButton_Collapse[] = 
  "eNrNmHtwVPUVx8/evfu+2d2QF4REBKwhkhBwwaRSgyTBRKrIUwsWedi0sVhTZfKQEJJNNh"
  "ibGYozpQPWP8IwtNbRWqoVMavS6egArTwr2poyFFOjHeUvlRJTfv2e+8i+HwzguDOfbDZ7"
  "7+9+7u93fuecGyEEiW84g4ODFAwGVYaGhiK++3joE3rtxSAdMPh9kF57KUgD+3H8AN51cK"
  "4PTDXG4e+Crw5QcH8sB4MHaWRkJOI6R48eHXMwOH36tPpdc3MzGa8dO3ZEnPfi4WfJupoo"
  "40EN531E1vk4cBJ58XMO+BHYBQT4AjwCPOpgCsgFE0AeyAaZRI4pCn16/rOI68ybN4+iX2"
  "vWrFG/a2trG/vbrl27Is7bf/45mvg7chT00E3Zq2mVUkE7XbPpz+4q+jinnsTkp0nkblDd"
  "BDlNgqptgkosw5QrvUVZ0haaKldTlf0GWu8iasog6nZR1i+m0Ij4KuI6dXV1MX719fUxfn"
  "v37lX/tm/fvvyenp6GSWUFv7UX0Rl3NV0a3wifZ0gUv0mi9G8kyj4gMYPBMngXwe1el6A+"
  "r6CtoM0t6AcKLmwXNN1ykXLNR8gtddNE071Kbdbs/j39rsEPBuncuXMqlZWVafktXHAnNT"
  "U2OfDrMZ4T10K4vAuHv+s+7+HzKRIlx8ExjZn/JJH3U8zfmgxBAY+gTtAFeuD5hO7bjs8/"
  "hu9ip6CbLYLG0Rmry9bvcDheARslSTKn40eF4Hqy4udp9strgNfZkEs82Dt/E/yWuTSXTk"
  "8sfo/m/oTu/CQoloQetyOgCphT+q1EnDQoCBLpfVrkFJ6lJlFyRJvDRH4z3icxBWFLS5yJ"
  "/cJhz01Y/1LMY76Z/T4CjcCd0m+Fk+h221LEtKCnMgXdrwjvEpMofUeLuaR+NY7Ufuz2ON"
  "xmW7X15nV3S0O8XYE9pV8tDpksv0qPIpa69fVYqYjMFXA8psdetB9icmo//ObatJhL5Mbj"
  "tYE5OK5e0cYOgG/Jn+t+lpR+BebpNMf634h54HHug+NyOJ6IdSw9SeKm10nIVZgTvzex2x"
  "YwC8es192MsW+38Rp3goyUfkT9al6Ingce526nyFqp+52M8vsTCctcWVCHvhei3fjdB7el"
  "zpAbw/Nwv5P9fg3yU/g56Hr5IzU3+OPMAY97l1NkP4A1fTfMkecU8em8C7HeEnVut/75Fp"
  "vm1uuNdX8MsWQz/QXXn5HCr44WOyLvL4FjznrNsVR35BztWY580ejWcp+RA9mt3Krt7V5v"
  "/LzD7/nmYU6/wJTQz2N6gVrdofVI5fhgyLEM+TtnA2pIg35+lz43t9oSu4Wvsc/KOXADsC"
  "XwK8F9fqHmzVT5K8wx7yFt/5YNkhi/EX5rM7TrsV8luzmSuxlj3ePgGNyudRAxfpPw8V+U"
  "gfEfztBqqJHneZ8EPKE58Yety1bNcfwjJGahLSvwI8eswt76Ge9J5M9UsWLA12jAeRK9Ao"
  "/Jcfwq9DojaKJ5lG6zXaI6jL0C67IONesncG7Cum2OqlM8L9uQwxc4RH4ziQmP4nwf5qwa"
  "boscmqdxT/4kfnwM9xNe6d9wqIzjx3WvCxwFL+t7/XnwBngPWfMsKaYPKUf6lK4zf07Fll"
  "H6NjzugMcy3MNa3PtMqxi7xyLkmY1ujWa3Vsu2hMVkQO8ZjN6B4XuZbrmA89FlYiZj44+7"
  "x0U6M8Es8B2wBPwQPAw2gm5uY8FvwAB4G/yVLKZTeP+S7IiRBfBeaNfmcb4OrzfnYa6bfF"
  "8LMb93O7S98z2+R6xTgVqLz4LbEuQXi8/nM/f19al96rZt26i6ulouLCx0AgV4MjMzx6sd"
  "Du8nzZ8by3vAcrXGe6QPabNbW3uOKyMXbNbrLsdJI+LlIUXrD9nr+6DGHpp/bf2y49WP3b"
  "t3xzyfXLhwYYzh4WE6dOgQnjkGqLe3lzFhHHNNTY2sKAr3ZQ3IKZdi9oURg8Yad+tx3BO2"
  "F31qjHym+/ni+UX395fD8ePHyePxyCTTftqgJO8VouH5rrB+BYU/qPeIp5ar7ccEAgFSe8"
  "0ieVT186fhxnPKdUExnce5AfXpK0F/cKV+Fy9epJJp07lGPUOrXOnlPz5mhdojvAMeMGrI"
  "tfBjWvs2kToHueYzY3klVX4utvxPzwnlyfqrq+L3RqfRKa1Fv5u8vvE+4V7YbvpE7wEnXG"
  "u/loMdWsa0mRzkMB1U83Qgydp+V629bwE8/avPZtfWbwB+XQjBKvVxYh565tG4cWjkm0ny"
  "lzjuV6AsVf+8c+dOOnz4cPmBAwfuuAzmA/nEiRMhPw7BTR50I5IJHV2/2tNHPzsZfYGZ/q"
  "E/v2Wm8uvo6KDs7OyTYbk8HUZBRVERCi/Ge/x1P1Er/vKkl1CfedgbqVD+z9h8ha9tJXI5"
  "0UtggVF3k/nx73l5eUcu049pnzZt2rgIPz/mrwtcpz52N6k9jbFX2JOfLbMkrhe9es2kNP"
  "24Pv8S/FG/t2S8rNMCv+wIv064bcUc1iv8n4Fx6C9PUYveX/Nar3aF5zx7On76+tr0+j8X"
  "3JomxVhfW4wfw+s8R92Wy6jCpvVSzEy1nu0Ft1CcV5L9QYj3yyZif4T7BeD3mJvIaZIxj8"
  "/SKqfWv7jQU/JOMlEe+rO0/K5KfnkTabaDo8ob4ilszcX8bzFaqsarpMbs22pvdoNFpiJL"
  "Wn579uy5Yr+Op7u0DtdnDTEb3Gzl/2BUhe0pjm8fTcD+GSel5VdbW0stLS1XRLmvnJK8ZL"
  "2OHQI/V3NPgpfh19raSl/zKwfcqffxrkQHrVu3TvXbvn07ITd83ZiBlOyY9vZ2+j+fm5V1";

