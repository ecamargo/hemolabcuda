/* 
 * Resource generated for file:
 *    TrisurfButton_DivideObtuse.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_DivideObtuse_width          = 40;
static const unsigned int  image_TrisurfButton_DivideObtuse_height         = 40;
static const unsigned int  image_TrisurfButton_DivideObtuse_pixel_size     = 4;
static const unsigned long image_TrisurfButton_DivideObtuse_length         = 1488;
static const unsigned long image_TrisurfButton_DivideObtuse_decoded_length = 6400;

static const unsigned char image_TrisurfButton_DivideObtuse[] = 
  "eNrV11tMk3cYx/FfD0Bh1NaiUlAGsjHbyGAHSXRkAw8EShEyO8Cpk+gMGUum2Za4GCcRxk"
  "QOMjlI4pJdLNvFZAMcU+bNkiVLQBczQyJGLdBWGSeDI0Kx2Jb/HqB1HQP6b6DALj5Je/H2"
  "/fZ527d9GGMYGhnCwNAAebAIBmC2mMHovLyyS/dCma2Ecn+Ep8pIDRFyH/OWEpVNNR71bT"
  "ueDGwDkCriJSHfEOZQT6Rcx74BfHah2KO+pBOpQBL17ZTwWEN+IWyaFhLm9niaw+d1Jd7q"
  "U5P2GdqcOsjLS9SXSHrnaHMaJJpF7ttDRjnanJ6QQ4vUd9SDrulOerFPSKrn0eb0FfFd4D"
  "4paViANqefSdAC9T1LWhewzekGeX6efa+QTi+0Od2nvtfKG8961JdwbAeQAB1S8Jgwr0qE"
  "bVdR5r7L169w9x2szH1ddVD1hyov5hZpcy+2TXV4U5vqSNx/vR/r7tj29Tnr25Ly05J5++"
  "zjdn+b3SYm4GKzwtJ9DxZTFyz3DFMcj61jFp7XENns9gBPrrFHRkfRvT1OZVCHCowx4ZgU"
  "HQbTS5Gwmwzw2nk5jY8MC4xb1DX6MKm0I1KBSRFydEatxpMu/XLoExvjo+/qI+QJHdQ06b"
  "kgdKqUy6VPQH0G6mue3mfr6V4OfaC+eupj1JbztG9DMEYuNSz5DB19exx9FvKOY45afbBv"
  "bM+BzOXQ5099N6mJOXxJbuhD/It73s1eDn2gvs3UNOrSyPShAbd7Du32XfK+LWrQ/WXi3p"
  "LSEbWq16WP9efufXNJ+8wjMG19FR0blOiMXofOF9aonHPsjgjqbUqMeltTmA7NyQxo8tPX"
  "amt35addzApIa8yCpk6LujsN3m0cH4e1909Y75tgpd85atXdXSXqM4bLTjyKVAZ//eIKgF"
  "ZW7ICa/g+04z0BQ4nfrzjlq0QhUNB6elHn+eDTj+K7s1JDr6XEoX6jHEc3y4AMv6l9SuPH"
  "hEekTFwdxMRlilsoEm6suF69JNf9zOVaYDvNLd1ln3L2VVHfFwomKPbrP9Cct6359hUMPn"
  "q4qH21zecn/r//e59y7atQUGMQvZHAx/gQ+1puti7Yua/euQZdgQ664t2zEcYcjqtGqg+b"
  "s4+IaI7CMhnb+q32WGZTDjJ/msEl8t1+7r4Lv30PxE9+5meygjQihb4H6RK3fVNzVDCU+j"
  "MU+ZxDsY8IJb7AaRel5BMRd199S+NsO/Lc+9Rsfa5KV/4oLlfIqBn0fMpZBUT58vn2ud+n"
  "ePpolvTd/p0aw582zr8vlTx0u7Px9DkbyxVd4rKVmyYb59eXS6xcOyVv3z+Nf9Esd4orPe"
  "v7oaVh4t4mgNbnFGHcksUMHwQwQaWcCc7I3Ksg5aRMlofjUu6+i1eb/CVaSZVEJ7eSETLM"
  "JUM2HPDxanPgudDRwMoQXmOBVSFjzxQGF/D2mS1mP0OfIcrQb1xH1nLrI4OmMOOQKdxDEU"
  "S91LvC/93f6HMuSA==";

