/* 
 * Resource generated for file:
 *    TrisurfButton_InsertNodes.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_InsertNodes_width          = 40;
static const unsigned int  image_TrisurfButton_InsertNodes_height         = 40;
static const unsigned int  image_TrisurfButton_InsertNodes_pixel_size     = 4;
static const unsigned long image_TrisurfButton_InsertNodes_length         = 3072;
static const unsigned long image_TrisurfButton_InsertNodes_decoded_length = 6400;

static const unsigned char image_TrisurfButton_InsertNodes[] = 
  "eNrNmAtQVOcVx8++n+zyEhQhRk2KRBB1FSg0qOArNiG+SKqp8ZHSkmqhicMjIiIvQ8qMNZ"
  "2YUZPJ4Di2qWNrja3GQKKdTjJKG5+NSRvqWEND0hHaTpOoBP36P/d+F/a9a6bT6c78WGDv"
  "/e7vnu985zt3hRAk/s/p6emhrq4uhd7eXp/PPun9lN441EXHNX7VRW8c6aLOYzi+E+8SnO"
  "sBE7Vx+LOu1zup61ggJ7tO0uDgoM91zpw5M+ygcenSJeWz6upq0l47d+70Oe/Q6VfJvIoo"
  "5gkV+6NE5jk4cBzF4udM8D2wGwjwOagAbmUwJ0gCY0AySARxRLYJTro20O9znVmzZpH/a/"
  "Xq1cpndXV1w//bvXu3z3nHBg7Q2F+SLbWV7ktcRSudebTLMYN+5yqiT0aVkRi/h0TSesVN"
  "kF0nqNgiKNPUR0n6tylBv4UmGoupyHoPrXMQVcUQNTso4YUJNCi+9LnOwoULA/zKysoC/P"
  "bv36/87/Dhwymtra3l47JTf25Np8uuYro9uhI+L5PIOEEi648ksj8kMYXBNMSWwO0Rh6D2"
  "WEHbQJ1L0HecuLBV0GTTTUoydJNL30xjdY84FyTM6NjX4ej5sIeuXr2qUFhYGJXfonkPUF"
  "VllQ2/nuWYOBbB5T04/En6vI+/L5LIPAfOqkz9C4nkHyJ+q2MEtbgFbQVNoBWez0rfevz9"
  "ffgutguabhIUT5fNDkuHzWY7Cjbq9XpDNH6UBu4mM35eYr/kcnhdGXEJBnunbILfMofqst"
  "UdSKNbdX9WOj8HMvRC5u0gKAKGiH4rkCflTiSJ/gMqsQv3Up3I7FZjGMpvygckJiBtaYk9"
  "tJ837LkJ85+FOKYY2O9jUAlcEf1K7USzLUuR04KejxP0mFPELtGJrHfVnAvrN9cW2Y/dno"
  "HbDLM63zzvLn0vL1dgjei3AIeMN75OTyGXmuV8rHCKuFI4npW55++HnJzYAb8Ci5pzodx4"
  "vDowE8eVOdWxW8C9xs+knymiX6phMs003/CJA4/zKByXw/F8oGPWBRL3vUnCWISYNMaGdt"
  "sCpuGYddJNG3u2hed4K4iJ6EfUodQF/zjwOA/ZRcIK6XfBz++3JEwFRkENci34u/G7B25L"
  "7SNuDMfhMTv7/RSkRPCz0d3Gj5Xa0BgkBjzug3aR+Djm9D0vR44p8tP+IHK9xu/cZvl3jk"
  "V1a4sNdH8auWTR/R7XnxLBbyEttvneXwjHUetUxyzpyDXavRz1otKl1j6tBrJbrlld222x"
  "wesOv6cY+rj8Al1IP7fuF1TrGpmPSI5PjDhmo36PWo89pFye3yRjk28J7eY9xx4z18D1wB"
  "LCLxP3+blSNyPVLy/H5CfV9ZvdQ2L0RvitiVGvx36F7GYL76aN9bCNc3CH2kEE+I3Dn3+l"
  "GIy/IUbdQ7U6z+ukxT0Sk0avedmmOo6uIDENbVlqI2rMSqytH/GaRP2MlCsafI1ynKeno/"
  "AYH8QvT+4zgsYahuh+y21aiLFLMS9rsWf9AM5VmLfNfvsUx2U7avg8m0ipJjHmKZzvQcyK"
  "4VZiUz21e2oM48fHcD8Rq/8bHAqD+PG+1wTOgF/LtX4QvAXeR9W8Qk7dRzRKf43uMnxGGa"
  "Yh+jo85sNjGe5hDe59qlkM32M66sxGl0q1S93LtnjlZIvsGbTegeF7mWy6jvPRZSKSgfnH"
  "3WOJZCqYBr4BloDvgg1gI2jmNhb8DHSCd8AfyKS7iPcvyIocmQfvRVY1jnMkPN9ch3nf5P"
  "tahPg+ZFPXzrf4HjFPqcpefAXcH6K+mDwej6G9vV3pU7dv307FxcXGtLQ0O3ACd1xc3Gil"
  "w+H1pPpzY/kwWK7s8W79R7TZpc4955VWCzbLfZfzpBL58qRT7Q/Z69tgrnUk/ur8JQbbP/"
  "bu3RvwfHL9+vVh+vr66NSpU3jm6KS2tjZGh3EMc+fONTqdTu7LylFTbgesCy0HtTlulnnc"
  "6rUWPUqO9Es/TzA///7+Tjh37hy53W4jGekYrXeG7xX84Xjnmb+EwmvKPeKp5b/tx7S0tJ"
  "DSa6YbhxS/xijcOKa8Lzh1Azi3RXn6CtEfBPO7desWz6fnwIEDpaAIUHd3d1C/mzdvUuak"
  "ybxHvUwrHdHVPz6mVOkR3gWPa3tItH4DAwNkt9v3ytzl9XXv9OnTDaFiWNu+iZQYJBkuD9"
  "eVSPU5w3RL1oTccP1VML/+/n7Cun1J+l0Da3Nycuwh/d7aqnVKa9Dvht/feJ1wL2zVfSp7"
  "wDFfxS8+Pl7z+xc/g+fm5jpC+dWcbFArpkVnI5vupFKnW8LM7TeVvfdtgKd/5dnsjvxu3L"
  "hBCQkJL0q/f/Cp+fn5IddITSf8mpCCRcrjxCz0zENB81CrN+OMX+A4vv/sSP3zrl276PTp"
  "07nHjx+fr3Hw4MH5MTExR6UfPyu8MGnSpBKvY+YA4/nz50f8OAU3udGN6HXo6DqUnt7/2U"
  "nrCwz0Z/n8FhfJr6GhgRITEy941fJoGAJ56enYeDHeM282EtXiP8/FEvZnHvZrlGb8+3C8"
  "vOe2ELWc6AiYp+274fz49+Tk5O479GPqEdN4H79GxK8J3KU8dlcpPY22VtiTny0T9LxftM"
  "k9k6L04/2Z8+038t4OKf2h6vFvcEL2OUfkO1MDv0Qfv61w24YYljn5m4F49JcXqUb21zzX"
  "qxzeNc8ajZ+cX4vc/wtAvuxnXpN+/5Q9DH/Llic/ZzIwv5YAP4bneaayLJdRnkXtpZipyn"
  "62H+RQkFeY9UHI92GwPgjrY4+X33rEyuZ9DOOzPrz9WuD3tIvIrjMijq/SSrvavzjQU/JK"
  "0lEy+rOo/MLUl5e8/DagvoSszzUnUGYbOKtiR3geS3Mxfy1GS5Vx9MpY7yi92T0mI6Wbov"
  "Lbt29fwPWGhobY7xWv+lJRUFAQsj437GlSM8JjHmEGmG7mbzCKvNYU57eHxmD9xOuj8luw"
  "YAHV1NT4UFFRQVarlfvpnwCug+tSUlLs/sdp5HpyKczLKPexU+DHSu0J8dL8amtrKYoXF4"
  "kMMFuOaaSv/hoFHpB9vCPUQWvXrlX8duzYQcj3/zUGoA93TH19Pf0HAKQ9OA==";

