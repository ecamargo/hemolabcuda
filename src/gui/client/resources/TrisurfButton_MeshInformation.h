/* 
 * Resource generated for file:
 *    TrisurfButton_MeshInformation.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_MeshInformation_width          = 40;
static const unsigned int  image_TrisurfButton_MeshInformation_height         = 40;
static const unsigned int  image_TrisurfButton_MeshInformation_pixel_size     = 4;
static const unsigned long image_TrisurfButton_MeshInformation_length         = 2140;
static const unsigned long image_TrisurfButton_MeshInformation_decoded_length = 6400;

static const unsigned char image_TrisurfButton_MeshInformation[] = 
  "eNrtWGtQlGUUXs2xhvJPlxmHNBsuYqPSiJch02q6jtVkhmJN1lhajiLZEF5Q0RUvi7BKy+"
  "6CK7DeCAxdFgYQl2KBTQcRsxRQNvOCNqBg3hKvjE/ved8PVoG9fCD88scZdtn3nO/5znvO"
  "cy4AFHgkvS737t1Dw/nzqKr5GyX77ci22LHVVA3jripkFdSiyGbHn1UncZ6d6U1cDQ31yM"
  "kvRfhyM156Ow2PD9dB8UIcFAPXPSiD1kPhl4CBwZsQGrYLGbvLcOFCY49hvXLlCgqKDmBq"
  "WD4GBBrY8+MZLjUU/hoohurEXz/23S/+vv8lQuGzkZ/r469D0OQMpGXYcO3afw8V59GqGk"
  "SqrAKXdyz3C3++byz8XonGjDkqbEjcCnNuIQoKi5GckoW5P/yIUe/GoK/vGnZWLzC/yPD7"
  "aDAhNB37yg8/FIylvx3Cq9NzBC7yRYAOXsOUmDlPhWKrDTdu3HD5nMN/HEXUKgMGjVvNcG"
  "oFTmaH4sKwdU+3MBYVV8D/rQyBjfmLfEG47Pa/ZNu9ePEilLFb8ORwleR7Nb9/ZXxmlzCW"
  "7TvkwMZ85j16GXLzCp3aunr1KkpsFaB3+vfSJbiKlfEfSRh5nOixPiFdFsbjx+2OO2XYAt"
  "+MwqlTp53aaGpqwqpEG54da4RXYCrGfLwFNcfsTs9TTHz6bYIDI5O8PVaPMF6/fp3nAs9P"
  "5n/ymytsJNn5NvQfsZnFP+MZX9LT4ZOZcS517ty5g9dD1G157j0uFo2NTW4xEofwPKVcYP"
  "np6k5bJTF1L8ck+EXN72zUOyvd6p0+U4dngiQ+CkhC+OIklzoUN9PC8wW3smdM/3q1Rz6v"
  "qDyCPkMlzqMcZbpLViZ6pJtsNEvvlsBzx9Vd7bHYMCAohZ8lDqmt9TxPU7dlw/+1tZxH5k"
  "bEo7m52SPd27dvI+gDR65ErUh2qhcRYxY1QYbvOnueXJ3UHQWi1jDfE993ZoPq18j3jQIf"
  "iztLkft8unXrFs6dO4f6+no0NDTw+trY2Mg/y6lhlBeDJ0oxPzgGxOntz5w8/Y+o9az+UM"
  "5SHruzW1d3Fl8s2IGXP9yOsSEZbTJuWjbnYjk+nLPUxPI/gdfAtO25HXR/P3pWulsdpny5"
  "1mPbxMuLYjKFbdKnXoa947yIDbLwbcssFvnF7CxUdny30gNnpTqmhXLdZsit0V7ESb6t/K"
  "LD/IUayKtXlcIGwzcjbFMH3fySk6JvY3m0IzNXlu2SsgqR98TNXcRHXPFccBq/gymzklzi"
  "y8zKk2XbWnag2/iqqo/h+YlGp/is5XUCH7O9Js7Y6/iKS8uFDXa/n4cldZ4fVHOZ7dBZcb"
  "2Oz/iTBf2G6Tm+iOiUDrqnztQLDme1cEjwKty8eRO9ie+7FTki/xm/bErbjY59SzN83jCI"
  "HpzNEGW28l7Dd4b1CTSbcHyMnw9Wdt77f7PYJPCx+vbVfHWv4cs0WfHE8GRe44aMWuR0Zi"
  "iw7EffAH1bL0Hv1dP4qK5OnZ8jeJ35ZUFkvPO+lsXcmMlpog6ys7O/96xHspZ2Hd+unFLB"
  "y8wnNN/U1Bxzqbcz28pnVTr/WIDaozh08LO8+nHixAlM+Mwk+k12bzR7udNpaWnBxBCN6H"
  "eYUE/nbu7PK9wn4uc+fKGzXccv9TcL11mkmq/F0yOXgfoNT3xeXVOLp0Y6dgM0J9B82Nlc"
  "RHsL7gMfjdQ7a7lOv6EboTb8ArLVXu/y5cv8t/4BWsFprJ9LzzDJ4qOfTXvbZivyI/W4Bw"
  "8dQfu+bfkaA6KVesTEpjwg1GNELkvE/vLKB3QovshvHBvFOfNBRNTGLvXCtKdo5Wzyz4BA"
  "LeJ0+R7NWe2F9ljEI8EhWeJOJbuzw1Xd2iFQLyNmfr2IL4aTuFRn/JXPuC1378LVDFnN6j"
  "71dlPmmEWM8tlLy++U/PswdjDE56MnqRx7KmnPQ/0Q7c7Ip+a8Mn6XJLR/02wuAPH9sPfS"
  "RV0lfpP2StSfy403d0KcTnuItj0PYZV2Z4SVfEP7A8LM+YzyX6qnrbMPzYTUV9PM0lN7QM"
  "pZrT4dEyathJd/NKuXKqmvV4v+p3UvyGupCn28l2LE+EVYzubhruyVuiNU/2hm1hhMWLI6"
  "HWFR27hEKrcjTrMTu80W0C7HVYw+kp6V/wGmvT4H";

