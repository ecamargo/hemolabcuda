/* 
 * Resource generated for file:
 *    TrisurfButton_Par.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_Par_width          = 40;
static const unsigned int  image_TrisurfButton_Par_height         = 40;
static const unsigned int  image_TrisurfButton_Par_pixel_size     = 4;
static const unsigned long image_TrisurfButton_Par_length         = 2452;
static const unsigned long image_TrisurfButton_Par_decoded_length = 6400;

static const unsigned char image_TrisurfButton_Par[] = 
  "eNrtmHtQVNcdx6/axBgqitWkTpoUtRNjaqSdYTRNojVaW5q0Nmp9xkYeAgosLKkvHkuiPF"
  "QQqKIiIO+gvKOIEggRcWGBRYkgAZU3u7ALu3t5Lgvs49vfkjXdkM70j17+6JQ785mze849"
  "+/vs+f3u2XsXAINpppnmvyIkJIRxcXFh+Hw+o1ar/+05A4qB56srq1ffKilwyb2bK8gszQ"
  "ooLCv2En5VttTRwZFxdnae+AwjbW1tnPqtWLGCMR4zZsxgWJb93pikU2KRlJHksjtkj3jl"
  "idd1Pz1liXmRDF68wMA6yhIfntvV+bt1mzYwZkdFRQWnfqtsbCY+d66lFSPvZZkumfK7sZ"
  "ycnJe3eW/L8rrkmReZFXH+02TBte0pH2iWZ8+BbcGP8FrGLHgl8prfeNVm8VM/sVjMqZ+9"
  "vf2SnVv/vD3QzyMwOykoOSbMLSMvO/VjtUb77DDlW9n7rzXtV44wmYnXNxy5/gn7ZtFC2N"
  "2Zj3XZP8fRYN+ENbZvLTL6iUQiTv2+yIm+KCpwgbQpAF3tPpBL/VGSa6/Pz4hLFd4pfa+s"
  "pHBneXGep6g42y8/MzrwanxQYsOdVIVr0QbsEv8Cfyp9AQdyNqE0M7zBab+zXWNj42wu/W"
  "5lRCR0tf4dMIQB2lBAF4ZxzSk8ueeJqi95uFfqjrpKNzRWH0RTrQf6pL4YVAbi0n0n7Kt6"
  "G4JHdthT/ivsyreFQ/wu2YBqaBmXfvkZZ8911uwHVD4wKH1hUPlR6wMMCGDo84OO+nSsAF"
  "o2gMYEqP2GD6eS9+FaZYfgRx/ibOsBRLbb43DdeziU6P1YJWcXcOl3IzdJ0Hz3I6DTA4ZO"
  "HvSdntB3GFsetO3uGGtxg6bFHZpWD8hEu9EsdEKIyB5R7cdwXRGBbHko0ruDEFizD1dSDr"
  "XJunsWc+mXdTV1x4Ob24EON4B8DISe0BFGv/F2D6ifHERHyVZ019I5iMKgJgLRdW74XHYJ"
  "YnUWYiXHcemBK25nHWuWSuSLuHLT6w2Mv8/hP1bnbDPom1xgaHaFvvkAtI9dMPLQAQP394"
  "Kt3oPOcnsoWnwBfQR04+EwjIUD42dQ2hqIK+3RSGoJhUR5FvnpXk3c+ukZnofnm2Wpf1WP"
  "VG0BW7oZnTc2oY1oLdoMqXgfVC2HMKY+Taf/A4bx0G/RhkE/HgbdWARymnyQ1x4A7dBJ5K"
  "QcudfdrXyeUz+vw68UJ++VKb/6PTqKd6C/WYCRXnJQnwfGjJCXmt4PhxDBRBAROIGegDoY"
  "GDkFtuNjnA12Sm941MJw6eew331ubtSOpuZMWygbKIfjycBgNAz9UXT9RhJniNMwsOTHkl"
  "ffcYKuZdafWsFEi4EAsK18HD34ftLdsipO9+ffvms3J9p/Q11j2ioMSc7DMJhKMROJOOiV"
  "F6BnybGf/PrIr8/kxxq9/L7di6gFeaqavODL2xJfJqrm1G/du3945pxgs6g+bTVGe7IpVg"
  "7FvUIkw6C4TESTZyRBOVZRftkT1D51pDVU+U/49TR64ajn7ghhObfr9866jcxpv70p4ss2"
  "tA/nU6zPKWY67dG0jgpaR0UsDD0XYJBTHcrPALKTRAjQE0ScgF7mj1GJN+oLd4Pn6uAmLB"
  "Nx7hd87KPwspg1lFsh+X1BvyV5gJLWUnEV6E0hj3hAHkNchK4rHCMtx9HztTeabruirsBd"
  "V3yF1xUXeSRr5cpVS4RCIad+Nr+2Zf62de0nlSkfAJqHQL/R8TbAfkmeN6GlnA+1xaPr/m"
  "nUFx9DSYanJjfW40l2QtC1pJhw321btm5aumzFz+bOWzgl91e/XLmK+Yvd225VmZ507XZg"
  "jL2HfkkxJHVpqCmOQFG6X19WnF/NtbTIxKy0BF5FueidxkctVnT/xYgqqpjJB9d+1tbWzP"
  "Lly23Tz/EVpbditTfSw9tzU0LvpF2OiCy6mWsvFle/LumSW47rfji3srLyB35c3z/TPTIT"
  "n5DINDbU/+bh11Uba2vrXhkcGpml1oz9x7m9vb1MTEzM9zD2/b89w9EzlzXVzRriLWKRsY"
  "Ym8QaxdFLfS8Rq05yXpFLplPk5OjomUOmA0BAyYqNZSb1MaIl6YpZZf5DZHMX69eu3j46O"
  "TokfPbd+RjEeELOJGqLQzMPb5GFkvVn/KUJOPEvcsLCweKhSqabKL5FiSAgvoo+4YHJ4hm"
  "gkjOtbQWSY+QUTA4QL8cTKyura5OdnDv3iKMaoMQ4RRfzE5LDWtG7LiJ3ECPGiaexTwmAa"
  "LyO/xVPo9zS/k4+LpviPiQ7Ta2fT2EminSgw1sSCBQuY4eHhKfFzcnK6SjG+meT2AjFEJJ"
  "ry7kmUmH0PY/1JCVuj98yZMx0KCwunxI/P58dQfoQEY8ZeopdYaNa3lugnlhBHiMem/ovz"
  "58+vjo2NnTMVfmq12oJqx9JYP2b8mJg3qc+IFTGbeM5sfCaxiPaXWdP/CU7zv8A/AdLw52"
  "U=";

