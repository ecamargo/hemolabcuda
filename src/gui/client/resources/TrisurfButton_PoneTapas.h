/* 
 * Resource generated for file:
 *    TrisurfButton_PoneTapas.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_PoneTapas_width          = 40;
static const unsigned int  image_TrisurfButton_PoneTapas_height         = 37;
static const unsigned int  image_TrisurfButton_PoneTapas_pixel_size     = 3;
static const unsigned long image_TrisurfButton_PoneTapas_length         = 180;
static const unsigned long image_TrisurfButton_PoneTapas_decoded_length = 4440;

static const unsigned char image_TrisurfButton_PoneTapas[] = 
  "eNrt1tEJwCAMBNDbyBUzQkdyM6uUYr5qNIoV7vA3vJgqNUaGYZhVCebMtRJgXM4euqyPHk"
  "5xd81547k66DYtsppfd1YPpf4KkM5zLMhVw3RF+10PXQYLDLtw3N+nvKYp6ojPFbXlZhJ0"
  "yTTXPmS6dOnSpbvSLf/QdxldXfK3Z94NpaXEtQ==";

