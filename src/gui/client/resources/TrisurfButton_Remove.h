/* 
 * Resource generated for file:
 *    TrisurfButton_Remove.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_Remove_width          = 40;
static const unsigned int  image_TrisurfButton_Remove_height         = 37;
static const unsigned int  image_TrisurfButton_Remove_pixel_size     = 3;
static const unsigned long image_TrisurfButton_Remove_length         = 260;
static const unsigned long image_TrisurfButton_Remove_decoded_length = 4440;

static const unsigned char image_TrisurfButton_Remove[] = 
  "eNrt10EKgDAMBMD8/1X+LILgQZsmm3StioYeRLADbdzqsvz1+pKtrp5K+kVBu6XHcZGr0S"
  "ChBxpB9wtywyDoTNfaaFb/d12xb2abJ03f4vYXgZszp/5xdpwfgyFacuOlRl7nkus8lYiv"
  "JC37KkJxzXYhWt3DQvh0HF/Vg6ml2xlAuuaqO0NI117V0zSp0B5MCUXa7A7XoIdRsDMprg"
  "Dl5/ZA8qs3ojwZO3SK6Edc1h+KTbO/In7XoKegqbx6eK3fki8T";

