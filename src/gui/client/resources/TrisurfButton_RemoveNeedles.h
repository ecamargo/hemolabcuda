/* 
 * Resource generated for file:
 *    TrisurfButton_RemoveNeedles.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_RemoveNeedles_width          = 40;
static const unsigned int  image_TrisurfButton_RemoveNeedles_height         = 40;
static const unsigned int  image_TrisurfButton_RemoveNeedles_pixel_size     = 4;
static const unsigned long image_TrisurfButton_RemoveNeedles_length         = 2352;
static const unsigned long image_TrisurfButton_RemoveNeedles_decoded_length = 6400;

static const unsigned char image_TrisurfButton_RemoveNeedles[] = 
  "eNrtmHtQVFUcx7/LCntZHi4gu5qCi0hopaiV+ARrxRcqjpogKyCxq6wP2iwlTQXJZZQwtR"
  "pzxtFQSy2boiYdDN20fIyaLxxTJh3wNSgPJ20YfICn3z17F3EDNXfVf7wzn9ndu/ec89lz"
  "9/c7v3MZY2DPcYqGhgbU3axD3R3idhPEz8yBBqK+mWsd24mvdXWor6932m/Pzj3Q9tNCO5"
  "/4oAmZRCwxWEJH6LX6kJHaKP7dXIfr7YjnZxNaLdauWeu03/bC7UBbAEZiskQqkUJ44N4R"
  "jP6d+6FeEYXz6I92SGty/WSHtkm2JvlL8532K/qpSBwbMBEGCdFVHL+15NaWrkjFhfe6eb"
  "HWSTKG4diFwVDc16Zp21Rbs+X5y5+snxfHC8k4oElwY1tDVEzQk980MERhFcbQ9+nPyE8c"
  "I4AYha+RARYXLbD92gAm15PbVMJI9ME7SKBrpj4DP/H/NxpZdJ7RXLJVr/iyA6GS3xSJFN"
  "xBJIZynylP0c/GRD5PhM9bMna8Uxv2a5j/PT8DkU7Eo4rmMZz7GZ+aXyRRa3fQveHBarQa"
  "VuzoJyLO70gcRzRUvI8n7xdEXOBjG20OS7v7sOuhGvZLc352Rx2+xzDIKHaepJ8nsb9xXL"
  "q3yngZ29cpgF0NU7fsZ5Ripj9yMU6awyfjt9FxXgbqPNhlrZpdepCfQTr3No/pJB7TrvTr"
  "wP0W/mdOiI8ivFllqJpdfJifNN9kV4vX0Zf7LXOB3zby60W9Z0jxaLw3lmKijO0J9eduj+"
  "Qntp1B6FEBAV1c4ldcJHMLdTOjN/bRmlWDCdJYM+neDvFgFXRvz4UFNu9nlH7TNGnuJhHD"
  "cIuiv5yi2bwif4XTflu3bsX4uPH43fq7LDc3t2NsYmxch0EdltD6tTs5xK36LOW9inA1ux"
  "6uYb+9SPk5RZojE8/PDCPwD/mcUEYoC7oN7GZKTUuNXLVylZ+vt69b3sd5Tvtt3rwZMTEx"
  "OHv2LMrLynHm1BlYi6xYvnQ54kfHBfVww8gowJIpYJfFj+fia3gZB/ACPvUL9kumtt0WzV"
  "/kte2HbSj9sxRVlVWoqKiAu7s7cnJynPYrKSlBSkoKDAYDx2g0wmQywWw2Y9asWTDQd2MT"
  "9RhDJCZNCkrSJ4UlJyR79unZG4K7AHWgGmq1GiqVCl5eXhAEgRM7PBbWndZnVXe7fZmeNp"
  "1CVIYWjh+Lf8TlG5dRVl1mo5IoezTKy8tx69atx/arpT1BWYbh2xFKj1TI3Jr184jzgJBB"
  "c2kipguthHhhteAvdBYUQuMct4R4H44dO/bYfhdrrmHDxHHx58MC69MiXopR+PlzJ41GIw"
  "9Uq23CI6S6621eS37O88JQ7KVPnniE48iRI4/td7WyEu2DguRftfPZXdu9A7OOHb4sIUYX"
  "tm7+3PR3w0Pm8wFiG9ekmTwPvSXF/ACsw2t0fgyv32x7C4dDLpc75VdTUwMvlR98ZdAUtP"
  "PddyMsgP0d3eNO5ahBtSe0frWdWsm78vkz0U7AQDWi3S9NypNRmE05HHiXCHG9X3V1NdoE"
  "BPC+KECE8T6KOdbO6vPXaK2poVz5YWvvtZSnw8mvujGP2/3E3D4ZDZQzR/N6qZPr/aqqqn"
  "h+azw8lVidos9b2tZ3d1+Fu9HXX9aL1uGS+9YZu599nU6gPBqFV9DR9X43b97E+vXrodPp"
  "eH8KhQJtvL3V/IO4p0pEIV/3DC342WvGcTgFJQJc7Wcnc3bm/R2L2XAYVmB6M3WDo59BWr"
  "dj8DO1k9/nJyO/o877mXPNoHgE1RU2RpFZegt1TXN+dsfeWIae/LcBQ8hPS34HXeD3Gfkl"
  "S3nERD2nU6wa/qefUYqZAfQuxRbT8h7kd8AFfivJbyLPcV2IqmbdHuZ3r66to3mMFveBLv"
  "bzpzFOtuj2KH72vep4XMIghLjKb8YnM5Q0rpX3nf6QuvpBfvb9lVjDj8VJWaBMc/TQUaf9"
  "LPkWz46RHd/Hq5RPolBGa9Zdyr02V5NDnd3Ur2ndnc7vrfjM6Tate39Rbbld6afs/cfhP5"
  "z2O3TwEO7evovTJ05jQ8EGf+NMY/+I2IhZ3tHe31EsnqM82MDnRcw3iZKzSdrnjUOtLFpW"
  "oopUbeil6zUjbUpa34I1Bf6lJ0thzjDDanW+dty0aRMKCwuxY8cOWHdZUbyjGFs2bkH2vG"
  "wMHTxU5dPGpw/l33doDfuGVopSdMVh+rd+IT0pjCC8Q9qHIObNGCTEJ2CSfhImTJiA4OBg"
  "7N271/m9VVERFixY0MjChQuRnZ0N2scgPz8feUvyYMm2wJJFLLL40qsnf59DLLZg8eLFyM"
  "rKwrx585CZmYk5c+ZwxD6uXLny/Pm7k/wLRwZOWg==";

