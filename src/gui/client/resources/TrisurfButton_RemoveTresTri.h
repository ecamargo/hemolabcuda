/* 
 * Resource generated for file:
 *    TrisurfButton_RemoveTresTri.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_RemoveTresTri_width          = 40;
static const unsigned int  image_TrisurfButton_RemoveTresTri_height         = 40;
static const unsigned int  image_TrisurfButton_RemoveTresTri_pixel_size     = 4;
static const unsigned long image_TrisurfButton_RemoveTresTri_length         = 1376;
static const unsigned long image_TrisurfButton_RemoveTresTri_decoded_length = 6400;

static const unsigned char image_TrisurfButton_RemoveTresTri[] = 
  "eNrt2H9Qk3UcB/AvyA+RH3KIKHUVgUM4//C82DBLrrpigJg70dTquIi6YP5IL64uUGCB/L"
  "A/bKmU0kyFMJgOlBGdf7Tb8asQFEM8AdElW2yDCaFcd2zw7jNGBJ1Zl8M9f/DH+55t92z3"
  "us/3eZ7v5zMADHOZy7T09vbieGspum51g2u2jmsdSFS8i3lSf6wuj0bT5R85Y7x4pQVx8k"
  "1g6c7wPvgEeJUCrFREoaah1uFGVYsaq048B5bhApbpDZ/PnkSoXIAQeQRCKyMhu3DSIUaL"
  "2YKqxmo8/cUKsrmCZS+c4Vv2bQSWVVCq+MitKsTY2Ngjcw4PD+PruhIsorVke93J5gsmIZ"
  "/EGwsPPYWwc8+CJ+fbjOWUc3zslKfB+r3Ztun1enyqkmJ+vh/VawFYDtlyyLbXB0zsCZck"
  "H/hnBSFIthK8MwKEno0Er4I/YdxSkQTtr9pZM/bc7MEHFzLAcskl8QLLItceb7BEei+aDx"
  "ZHiaV6Cl3hJPKAZ8piLC3kIbhklc2pjET0mY1ov37V7safO9qxTfk21YyutTSyJXuCJXiA"
  "rZt0xVPWT0s8OWPcJuKy2Qs+ewLxuDQcIWcjsPq8EOrWOrsYzWYzvlPXgn8sCuwtRiZnqh"
  "EdoykxzPY67gGJnTzvZYqQstUdHukBCDwWjqPVX8FisTyUc+i3IeyTZSM55x2k5u2E2JqC"
  "B6Rw10S2H7hP6PPUvB1IyRHjvQIxxEd2oZuDe40jMzo6CtPAAGdrMmIaQGXxl9xdM6MBZQ"
  "nrOOuz9OlwJcgXPV1dnDT+fscEw/IAKBNf46TPTPfHjZg16Av2hfpA3pSxvbmZM96GokPo"
  "5/lDR7km+Rg32y7j4usimEwmThjv3b2LOuHzMNI63w72g1GwHLqwJVCeOsWZGvZ0Xkd91D"
  "PQhfihm4z60MU4LXwB4+PjnDHqaF5SpyTBGL4UA2EBOB/oiTsce3bfKD6CNnreHE96Az/V"
  "18G6v0zs60NDKFbK8E11mcO8+r4+KD7JRHNj4wyDVquFpDQXzq9S/xU/DwdLpI6ZUe7TO3"
  "V2dWL70fep93Ki3tHN1itucEPa5x9N1dZRudR2CaL8zbbe0dq/Tu9nN7hia/abGBwcdIhR"
  "3aSG4MM1YK+wma6/GdfufhGaXzSPzGidK6tVNeClhtvq9k+2P0PXJC95BVraWmfdOHJvBK"
  "W1ZQhIfMzW2/+bbZpx0bZA1Kq+nzVjv7EfUsVhLNhEc2es83+3TcUdbhu9cEJh//8fNBoN"
  "0k9mwmk9zXpxLv/D9pfRSeSO/bICuxite1d9Uz3i94vAXpqc8YR2CM2ACRlbYDAY8LD3wu"
  "macmQVZSNfVogCO8X6W5lFWfihQTU3682FE/kDpbxxjQ==";

