/* 
 * Resource generated for file:
 *    TrisurfButton_SmallTri.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_SmallTri_width          = 40;
static const unsigned int  image_TrisurfButton_SmallTri_height         = 40;
static const unsigned int  image_TrisurfButton_SmallTri_pixel_size     = 4;
static const unsigned long image_TrisurfButton_SmallTri_length         = 2428;
static const unsigned long image_TrisurfButton_SmallTri_decoded_length = 6400;

static const unsigned char image_TrisurfButton_SmallTri[] = 
  "eNrtmHtMk1cYxp9agRZKxQJFp1y9zW3aqYiiA8zKxcJAFLkIBQRa7iouKhleQKS6eVkWs2"
  "SYqSAg6twMWxyR6IhLWDbvU2OcMRs6/UO5xS3idfLt7elprRsYYztNFr/kCacfp+f7fe97"
  "3nOeU0EQILzS/1rNXzdDFaGCaj1p7VO0jrRMNV+VphrH2gP1qyStIalUOPjNQZv56rbXAQ"
  "oAelLmU5SFECTgEULxE8IgRQ7dy+in30JSGthVu6PWZr7dNbuB12iwQpJuQPkhF9eRDIHa"
  "AmZhJ96j+wX99NWzd2FXfW39i+CTkY4Rn4Ak0kKSsT0Dy4kXyH+JfHqmvSQBeaRkzpdPyk"
  "IfpmEOy2nuS+LLRCXNMQHppEzSXFIqKYPxGT/3YCom8Pd4cXx6Ns/TEUsMC7i0ECJUjsJI"
  "tZi1kcJZZ+Ii1Yu7Jc8vJn4z6Dl3MJ/XQxEE3zix8Jufp1D9plxANt0rMDGzWEbiW5KY1c"
  "t/z+dDus5qINXEICaGPWPdhI7RSuGmv1JIDJaY+Iz82Xw+huBjyrdpHHvzjaDBikk5cGa1"
  "aoyZ3lSnogQIS1Uuwh8jvYTfie2Gn1K4NNJTGKsebOLP532N35lGrVReL/biayA+RxrMl6"
  "RFo+V5C03zThQOYUqogzA9ykEIinQQppJComgezqB5GM9rR8c5F+IextJsHMb56uzEZ7xm"
  "Ya0lp8a8JfJnZ/N1pZAzGPOaw+vD/DeB99Gxd7pOu4s/42uwA1/9bhFG05Oz+PqbyplyH+"
  "eYPVvL27mcJ8vq/zr+Lom8nwanqVqU9ohf475GMc2/TQjCL7Rn3WfrXB6PUx5nMMZpHmfR"
  "8vcw85v7FnLmGFq3J6ETTtA17GqwmW/btm3QRGqwf89+l6KlRZMCYwL1shmyGvIB52l3uM"
  "tiWczXES2PpTVPHB4gGJcHTRj0ld9Uv9K5yXPVhnWG4XK53LGmpsZmvq1btyI6Oho93T3o"
  "6epB++V2HGk+gg2VG6SaWM1EZz/nLHIH2zEdPyOQ1sQQ0hSco3u1ilGK4vDI8ODyleWK5q"
  "Zm9t07t+/g9u3bkEqlqK6utpmvtbUV8fHx0Gq1SE9PR0ZGBnQ6HQoLC1FYUIjkhGRo1Bpo"
  "IjQSTYxmgiZaMyk2KnZIkCpQJJPK4Ovji4CAAHh7e8PLywseHh5QKBQInRmKtu/bXpavFd"
  "Xk5xRRiYowwHXg0AFcu3UN7V3tJnWQ2p9NV65cwf3795+br/dRH9oX676IdnbMgmhQv3yO"
  "cxwhWSyBpIBUJBksSZZUSxSS0RIn+ix5ulxcXHDmzJnn5rtGc7VuQULy1TGef+Wo3ohwGq"
  "pgTJRfsadSaQKOJuWRstm+9Cmr+Si00ScpnuE6derUc/Pd7OjACG9vccNw16O9E0cKrfM0"
  "W1Ii1GN2rvogf+k4/1XsATEWf72IrUOJvO7fIe8dSPfjSXGkYf9mE4vFNvF1d3fDxW0o5C"
  "J41Q6X//DnGHfhVtjbDztiZ/We9RvaGzBYPJ7Fr4BWah0eWvhy+HoZSt7beEZZSvK3P19X"
  "Vxc83N3ZWFQgkvmuTitaRyuv9oxSCN3jlMLKIbIdmI1xxNdl8Rpmvly2Tz8iLxHH/FKA/f"
  "k6Ozvh4ODweECpM6oz0zZ+NEx+NNjJQS9XiCbTfnfOsvdZ8+l4DFPIe4fiLeZL7Mx37949"
  "7Nq1C2q1mo3n5OQED5lMyT64kFLRxLyEbgA+HZ+LCbhAjs7d3nxmlS4vfXJg42o4G58Y/b"
  "aFYyA+Hd8jI3CQvid+gk9EfKdt5yvZUGL0ZUYmIJwUS2T5/bDlcT69lZe1ZgzCFvIUoGpi"
  "EvsT3zE78G0kPh/ubWfTCaSYatXKe7N4pXNPEcl9mdaqTvRWfUOoNZ+tlRBPJL4f7cBXVW"
  "JKiBKv07idLE5mL5jEfW4K9+KJ/Ixq9rNJ3BPpLefpu/Ai702ZFvsR33E78K0lPjkU9Jzz"
  "Fn9rZsq0ik0u59NZxS2D90/g/XXMG1+HG/zF3vbhK15X7Ewn0VaWszQeJ70Vw9Pqo785YG"
  "SOxnmRu8jr9InTNvMZNhmkvoG+y8gPNlFm2ul82Wf5/aPAyvv/k8+cU/OZJoP57gdUI5dp"
  "vW52dnMOOnnipM18x48dR9/DPlw8exF1tXUK/SL9TFWM6n1ZmOxLqpdfKV+P2Jws4ueaAq"
  "5sltdeUZjonNs0t7rJ6snFObk5wbWf1youXbiEkiUlzJfafHZpbERTUxNaWlrQ+l0rDrcc"
  "xt76vagoq0BUeJSbq4frdFp/l1BM9tFOcQnjcYJm62f8V0IVSeY/wh8R70YgJTkF2jQtkp"
  "KS4OPjg7Y2273toUOHsHr1aovWrFmDiooKrF+/Hps3b8bGDzfCUGGAoZy01iCnv1LWriRV"
  "GVBVVYXy8nKUlZWhtLQUK1asYDKOcePGjVe/b9uovwGPkD67";

