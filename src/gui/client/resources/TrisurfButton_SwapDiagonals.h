/* 
 * Resource generated for file:
 *    TrisurfButton_SwapDiagonals.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_SwapDiagonals_width          = 40;
static const unsigned int  image_TrisurfButton_SwapDiagonals_height         = 40;
static const unsigned int  image_TrisurfButton_SwapDiagonals_pixel_size     = 4;
static const unsigned long image_TrisurfButton_SwapDiagonals_length         = 1864;
static const unsigned long image_TrisurfButton_SwapDiagonals_decoded_length = 6400;

static const unsigned char image_TrisurfButton_SwapDiagonals[] = 
  "eNrN2H1QFHUcx/HP3XKHB3rH3QEeKQJ6aA4ipYMamI3iIzdkhefzc4r5gEw0zlhoamUPJj"
  "2gKHdwGn/4FOPoTI2aomCSSaHCYCqyFgWiodwxwAhdwfbdk6PLEGjvePjjNXsw9/De3fvt"
  "/X7LcRxq6mtQVVNF7neRKpjrLODos4SYs2MBNHM00CwO7KxkkkJEnXr+PA2iNs0Q3DcpaR"
  "owCUA00xEZOUC4Flmkb4evmwyErB0luG/K5mhgCvXF9GmPhuQQ7jF5ZGC7r6XdD1s/piv7"
  "Qsj1NtrsSklYD/VNIr+302b3gEzr5r6FpKETbXZWsryb+jb+j67Hvd2FfQxJdaLNLp1IXN"
  "ynIMdd0GZ3gqic7ttEfVEIhK5PPuFc7Art+5CR68KF923TjUaM6D70Ms7VRHoPOo6whCaG"
  "Pye0b/appYHY5Z7glqbKI5wriVI9LkYcnZxYfO8nrdC++bkrgDR3uJnUvHDyKfmVcAKVkx"
  "QyDmkSrMxLEHxuefPOvQrspb4MtSMFmUO+Ig2EawtMSg5GOodGWSM5QeYTL+yVAEYFva8U"
  "S3JXO9UXm70Y2EXj1+D5JMPIJnKNcK3SZJwmw7/otW/ithgKdg83FKSCtjBc3oP0m5kIOh"
  "wGfA7a/xVO9W38YSvCssIx+tgLHZGQqeTAM8cjDyceWx59bcJwbfWEsbBMjLAxjx+D2lkz"
  "be+7nz2EsINh2Fyw3ak+wcrKpL8F93+XHSRHaaCXza0BnigbOwzN9XU90+Tgz19Y/9tPa1"
  "h2iNqbDfYBj28sixzRK/qsP5f6UF8z9b31r77xoeCs1t7Q50Z9t6ivntpCbH1BSpSNGw5r"
  "yXU01Vh6ug/U9wn1cdRWSkbYjuNQX11pgNy7+qOtvaEvmPoeUhffaCavkxslPkxs3Yfbek"
  "MfqG99S1+rigCvrCOzIxG3Zy3iUtZi1YH4RavzElVx51fjZHl29/UN6w92sAqs1nsLdTXb"
  "++5r+/+xYZRsIKZChOeRDrpE02/dJSTDf3thcvf1ab3BBiioUcmP37n2vtrBvnc2j1XE4C"
  "X3LEx358TxfTm3TG+aL7iza86/8Wx+VQF4l+7l405tZZf0NTc2oLHwMhqvFuDhhXMoCx0U"
  "Xx6ksjwY7Jv09Qj1uKDpHvl4keaADn38/EFsUFZjj3wG0hW239X3vt3R9cfTamVKYibMyh"
  "0q9907Shki18lYzHR/NI927KM5BWNScYxRZWXSlCuwm8H7F3Z2y/nOKcyB2xTo6JjV2o6b"
  "fZ7/WJ+NqWX+8xmzded3KS7tMNdbsDB5GWK36xH7wdxWkRuj4qGTNP1nHdJWX0sjY5RzIw"
  "9GmPRnlkj1Z5dCn01yliLWNA9FbLGgvjvVlbSWkgETYbu30mIHjdW210lP6rM1etPcsR+H"
  "XdJTSJWq+Xky0slK4FT+aUF9lea76DfbB4h249d8HuRgu+u49vocz7dBfZVomUw1EC/DmR"
  "/POtvH3zPK7XCd2Zm+fxrLafxEYJ3wvrvme3xfBPVVdGod3Nk++3dyH42bDZ7Lsq+cY4T0"
  "lVayYsnL/V6BTnqUPr/c6T6Tw+N0tZn+Po03+yQcvXjMU0jfzYoSMDNpXaKT8t8/OYkiH5"
  "Or5K+O+pgMFSfOUD6SruSv16w4TWmi7SyxUakRf6EEVjE4lH1E0Pm9UXGT2iS2+yqYIXYk"
  "IqEkgZwkFsJhsojDGiktSBS2sSoyejUyBtX3ZBuJpGu1jJrRKlMFrHHD4bNfCupj797GwG"
  "VD4LcoAH5LgtrjR/R+CwL3DUjSXvc/FHL8qf2BcUn57wQXPShGUTXvmoOW/5mLUVhRBEtd"
  "jaC+puYm1D6s+38a6pg6K22ttWjmmnt87u8qfwPCjDXn";

