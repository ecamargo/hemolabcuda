/* 
 * Resource generated for file:
 *    TrisurfButton_ViewGroup.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_ViewGroup_width          = 40;
static const unsigned int  image_TrisurfButton_ViewGroup_height         = 40;
static const unsigned int  image_TrisurfButton_ViewGroup_pixel_size     = 4;
static const unsigned long image_TrisurfButton_ViewGroup_length         = 2552;
static const unsigned long image_TrisurfButton_ViewGroup_decoded_length = 6400;

static const unsigned char image_TrisurfButton_ViewGroup[] = 
  "eNrtl1tsXEcZx785l73vep292Hvx7vqysb2+rC/xJXYap06cuE1ix846thPbOKmbljaxY6"
  "dtWkpFmioFqggBorxA+1AFKBGibVApVAhQhYSEEKqqAi+pAiIUgVAfqIRQ0zD8v5lNtcFO"
  "VBBUSPjhr/E5e2bOb/7fZY6llCTXta51rev/UIXpRZoqnKDNpxbI/v4YWV/eTiLuI6rwkC"
  "h3kcBIYbca1f0oxpCbyOsgwu/0A5NIktYzFomAF89CEf28iOPvlJ9EJcaYT6/B86N6XcL9"
  "jRh/JYguE4lL0G+w1OseH82PLdCBySXae/h+CnxzP9nfGyPHl8CX+Aj58Gwu5DZ+Legw+K"
  "6A73fQg1eI7Gdym5R3TZ+eI/PlEXK+PP7R8eF32oD56UDFYrnr1bexwlvQpaLA+t03XF5z"
  "98J95P3WGNkXR5nP7fjioOD1/6t8PK/C46Dm8JPG1ir5fJlTvl3CdqnI+luiZxufmoV3o1"
  "7ni/tedH5nTDqeu/NrRmvERSHXf4Yvhr+r/Pqa78d9pkj6243e2GUxWietsax8PuSWv/8n"
  "Phbu/cl/YVzAu5+ATyq9OiGt4x3vUsLfI4JOEtF/ky9UnFPpFSITCIq64IDRGn7a6Kh419"
  "yelmahXpqHcpIOt8iz2XL55zX4kIPvg2nB8VKRjfVKQdqf3CzNHWlp5CPnsH8n5wlzrsn3"
  "wxK+Z8HnL3oX8wWMlsiK0Rd/09iRvmbuq5NKu2ulNd0orflmad3dKun+dtkAH99wGKtiDP"
  "/eAdsxxffSmET+Sec39kh7qVNaRzD/APY4kLyMeKQFexhbg++VEr6vmsgJn89oDJ8xd2be"
  "MyfYI7Dc1SKtj7fp8WCjXvuevNZ9bZKOtctdA0n5c4epfPyjrg+JPjPkuzBuIL7nnReRe1"
  "8H2yM90n6wS1qLHWqu9bEmiXdJ0RZ9AnnkpTC8RG6Sz6HqT3SCdzeuF22iu3yTRnfyb2Zh"
  "o/LGujev1rFPbtLrLm/SjAvQvVj7aF5fH26WBD9TuzJyKemXT/nsr7xJlHktEqfcuXkyzu"
  "8U8H7IHK27op5nNuzJXsF6R1vVfGsmJ42hzF9F/YY74J9JHkvzhb2cc4bIlD9mFpLSKjRo"
  "ruVOaT/co7ke2yztM/3Im15pH8faC1jzSIuOMfw092WlMZi6Rh0VX6D68pgL9f5jhOXs1j"
  "00srBIZZ8bJsoFyWgMOYytyXMm4qr2z/6xluDBiU793sl6aWxJvEMxfzMFnNxfBer9OXN3"
  "GvHCvCOtal+KhZkeh05DZ7bo64e61Z7NyQZpjiAfB1MS8z8jUoFy1Q+R44G4mz6fydLkoW"
  "WamlqhnoVponyIREM5elUAY6jG3J76kTWbkxby134A73u4G6OOu434JGbS1yoGw2+5NkV+"
  "aQ5X/53fp+LFvp/q1lxP3qbFf3+qDz726tzBPrl+UTt/ENVlWT7juD+q8yTuIX9tmCZG5m"
  "n/zApNTC/RxOQiVfTkiBrKCM/rflUTtIz+xGmT85nfiXVVrE5pTs/xvIws5mRiLqPqyJrL"
  "SZv3ouLarb06C8+egE6D7ZFe7SviYWLfxu2pn4raYEgk/Mozfa7Av5ibOnYM04HZk+rbgM"
  "U+Do3PkdUUQVaCLRPQnDVQZ0UfvHnPZt/AZT/aqzgtvM9c7pJiNKtrFDVpLxWfeahLe/V4"
  "n/bu0RLfphqksa3qF2Dz8DnKXnzAFzQplW+lSfbt4NIHfKzpmZPUMrgd56FX8zFbXbnmbA"
  "5XmndUv851xzWo8p69AAdqSvOpem3T/nG98u+cF6f0s9xLFFt//AJ6tUukAzpOzKfOexd5"
  "UxEaKRxVfpWysQ4cPEH7p45RMJciSnk1XxZ8tUGtbNBhbku+YM036ZpBHToQQ+65Ft6r6p"
  "4ZVe/I637H16hRVQvD1VLko/PgEWrPmRK+OJ/3TuraNkzTsw+sYruuKXjYN7xP8VF1QPPV"
  "BfWYVLGwzZ3p15SPqEPH0zt0f7izRp8Js2DnmpoGz8RG1TfQO6+KzsrT2OMGdRbzOjXBEr"
  "4Aer5NmbY2eLS8Kq6l4lrh2Ke72nWcOb6ZMr0mn6V8hmSDtVwz9icQt89uRe/pRK5XSXMo"
  "ozwyd1VLnGdXjc3xi0ZTaAZxDKpYsnidpP8GPq7XQG0ccb1nzbiuijOe2VO4m9x1UdSSR3"
  "8XqXO+qKhb4Iz/GcdZ1QLnPfIPOc99TCJ2L0AexcPfVfx9xXu8vs8SPsI1Jby0Zdf4LeN6"
  "g4dFf8NNNZjv1u8oFcekNngS30fS3FsruZerGulPSCMXkojXnNoT87FHXKM344u7KNXRqn"
  "zj2H0YPpWHh1ao7rZeonqslQutEjiCRmfFXxBDifOGa1Ia3TEJX6/gDPIY9RuIdSs+SvvJ"
  "SpXRztE58K18aLbrfNtGp0nkw0r4zrpRrVBPLGoMJL+NuL6P/LtqDFSdx72Q0RYlo11L1e"
  "nN+KpclO3pVjX5r7BdrxPuN/H+PFFLcG2+3hjhG5FQny6MDmOwiplJ8RV1Mz4+AzwbK2nv"
  "xFGV77diWf8fe13rWte6/jf1D9/TGKM=";

