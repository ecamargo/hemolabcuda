/* 
 * Resource generated for file:
 *    TrisurfButton_close_surface.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_close_surface_width          = 40;
static const unsigned int  image_TrisurfButton_close_surface_height         = 40;
static const unsigned int  image_TrisurfButton_close_surface_pixel_size     = 4;
static const unsigned long image_TrisurfButton_close_surface_length         = 1904;
static const unsigned long image_TrisurfButton_close_surface_decoded_length = 6400;

static const unsigned char image_TrisurfButton_close_surface[] = 
  "eNrtmHtMU1cYwD8pSEstUCuPPnmpIDAeFkQEUWQMCgWEYeckgEqmOGWWuswhD0FERRk4YI"
  "iZWbaYyXBsmbrpMlwrMIcZCr6mKCLULQ5ExVYliOK309oONv9BvSH7g5P8cu49/b6cX07P"
  "ueeei4iAE4wLdXV1UF1dDWWlZbBndxX8UPc9nG09a99zvzfg+oBaevnB1YRLD64QrsZfe9"
  "j1xvW+bvfm5uYp3375DVSVfgLlu8v1+fv374ehoSHK/QQCAehKUGCQ8+qP1malHl3TGNEk"
  "u+N3MhxnNsxFpxN+BLG+ntEQgD5NoUMhypgu6cG3voraFLfEaYazuS6fwbAArVZLuV9sRK"
  "xp1r7c/NQr792bdWY+2jV4op3SA3lKLxQofVCoGkF3zyftXJUn8pq80LU1CF8/k9g2Z3Vw"
  "uMhOBBqNhhKnzs5OOHzkCLScaoED7V+Xe7ctwmk/uxEHX3RQzR4zIhLPa/BCYaPvk9dW+i"
  "3V9N2jxC87Oxsm0+lmu9LSU10zPUp4LT7o+IvfC7kZcTwpRt6v3jhzrVeV5hY14/dhbi7s"
  "WJWWnu/jfpBMnUlWSfbbRT/6PHI67Y+OzX7o2ChGhxOznzHax9Dm2CRGx1NkXrb4o/Cw11"
  "O6xHqPPcfegqr591lNDe3K6uSrqeZQAYZiyp3sa5ZoWTFlJ7+DXevylHPMFacdd9P/7/9Q"
  "74aco67IrnFG5jZet0m0xT4wh3m6fEuWJWXr48KZ0zP6wvxwDZNWBaNKsDgIJJESRkxqnG"
  "98lmxZYmlSlmxPSrGsOmWXrCqlOLEkKTt+45JkaXKsvyRCwopcEAGS8EiQSCSQlJQEAwMD"
  "lPg97Ghf1OstwgpbpsroRqPRQK1WPx//dBTj9EwevHwx9IY7F087sh9wTE0cjI43/7r5v9"
  "gz2htPuFzzcnjUM52DCjZ9t8U0G8hcnhK6o6AIDh08BK0trewbPX94dfWrJed7f48/13Mh"
  "/nzvxdjOO9fnXbp2Wag6rjKp+fwAVJZVQGVFJezdu5fS/aO2tpZ2NtDjN7WTNZ4TsgaOLX"
  "/7i3qZtD1BvvT9NyuX/TSnYuFNh1KPYdY2PtK2TEWTAra+Zm3no7DEXeNdEtgWWhBZPn/p"
  "whAbjo1+7JlMJmXrI29HMeS4u6z4k/h1zbTFfhdb/HSODbJKhQj5dIScyQi5DBLIRNjMGk"
  "F3r283R9jCRPbHIhRXhhybFebpybfhU+aXk5cHdAYDWsMC2m64TMVbTjZYEmBN3KYgFFiR"
  "eozonAsYaJZtddspcLqf9i41ftt37qIVpb+T0RsVPKwWWGKf0BYLA9hkTCxfzM9IEQN5aS"
  "7f3b9NjV9zY6Nl8ZLFm7Lm+ZZFhNoNz45hov064lZoNXY/XVyhgQxz5MaImqgav8dknd0j"
  "z9LOu70OzGT+E1hMQ1hF5tYHrGd9F46BzYQNJD7ZAiHKBLnR1PkZuaXp8+As4z2BKOIXRd"
  "ZFDEFG5v8K4voumYty0r9iFOsJ6aQ9lTglkjgp/VleNA25UlEj1X49/b0jfjEGv2hDn1GG"
  "6/9i/E1KH5Uzjn4vw4TfhN+E34TfhB/Vfp7Eb5giP+r3X23fdE4S7/Er+0mJn0SkpNqvW9"
  "1tJkh2btO9f7yyn79gq7afWr8mVRNYulnLII68z8e+pFsCIWDSbS6fK9RqqPVTKg3HX1fT"
  "Yogn/Sx+Aa9Yg1uY+UOgQzTXlkv59zWVSjXy8UBAWwvh5n36PnWucfTnxzTW0J5guBabtQ"
  "J9UrAuncul3q++vh7+U0TAhnxwh/MwF4YgBBAWGlhACCaIQQsOoAQTWEni6cZEKs+XRjo6"
  "OkAul4+wnrBOX5vKN2bOkhco4uRbFSvkRYo0eaEiRZ6jCJdvyBTJM0hMhvxfuXnkTDg4OD"
  "jxzXwc+Bu9eoDm";

