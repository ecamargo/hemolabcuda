/* 
 * Resource generated for file:
 *    TrisurfButton_others.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_others_width          = 40;
static const unsigned int  image_TrisurfButton_others_height         = 40;
static const unsigned int  image_TrisurfButton_others_pixel_size     = 4;
static const unsigned long image_TrisurfButton_others_length         = 2252;
static const unsigned long image_TrisurfButton_others_decoded_length = 6400;

static const unsigned char image_TrisurfButton_others[] = 
  "eNrtmHlMVFcUxkeJIGoQDWCCRRRcABdUQBE3VFxQQAEVRYZx31k0LmgUEFHijrsoohIrDs"
  "JgXahoXFJlRyupUZR0IFraAtaMxLjW+XrPu97wtEBNmgz/+MeXN2/mzb2/+517zzkzABT4"
  "pm/6Qh+qqjr+XlDgXlFc7FrZhOjz2sJCV1RWtjIYX12doszRcdVKhQLrmTY0odVMMUyvPT"
  "1dDMZXXa3IVSi2a2heKyvoPD2hGzIEuoEDoevZE7pu3aCztMRLExPUtWyJQ+y5Wje3SQbm"
  "S2AC5s0Dli4F5s4FlEpg2jTA1xcYMwYYMQLo2hXfs+dq3NwmNAtfVBSwaROwfj2wahWwYg"
  "WwfDkwfz4wZw7AfDU43/PnilwzM86XkADs3w/s2gVs3w7Ex3PeDRuAmBhg0iScMTSfTqfI"
  "s7VNyCM+YktNBZKTgSNHgAMHgL17gT17+GczZhie79UrRZ6zM+c7fhzIzATS0oDTpznryZ"
  "NASgq/Z/syjfjc3Q3H9+aNIt/DIyGf+IghOxs4fx7QaICMDCA9HVCrgaws6exIfD4+huN7"
  "906R7+XF+Yjl+nXgyhXOefkycPEicOECcO0aEBmJs8SnUhmO78MHRb6v72aJjzy6fRu4cY"
  "NzEtPVq0BODn9/zRqoiS88fMz/nfd1ebl91alT3pXx8SEVcXFcsbHKipgYZUV0tLJi40ZS"
  "SFVU1IxsW9sLd4nv0iWgqAjIzQXu3OFMN29y1pIS6RynsueKAwPDKsPCpmqVSqVWpQrRBg"
  "WFagMCVFp/f5XWz0+l9fZWVYaGTvtj69ax77XaTg3xqd3cirazsbKYcph+/KTsBnSR6c82"
  "bTgfsd26xbnkPubnA+vW4Rf27A9Ml5sQzUPriGUqiI2NbYgvmfGVkSfTp/McQbltyxae0+"
  "LieF6LjgY2buT5bfNmfk5Jx44BR48CSUnA4cPAoUPAwYN8jPBwICyMi2rNkiXAokXAggW8"
  "/lAeX7gQH1mtuUS8jfCdYnw1xEe16sQJnssoh+3bByQm8py2ezfPxTt3Atu2cWbKw4KZ6g"
  "jVFLbvpHpCovvVq/lrdl4kXqoxxCkYGR9Y3b7P5r/WCF8643tLfN7ewNq1QEQEH0tIvv7F"
  "i/mYND7VMlrT7Nm8Bs+aBQQHS7lZqseBgcCUKYCfH+Djw8cfPx7w8gJYbyHVaZKFBX5l81"
  "9vhC+N8b0nvtGjgWXL+LpoXvJfpQJCQ/89f1BQPUNAADB5Mu8P5Bxjx/J+QbAMGybVZQwa"
  "BLiwtmvAAKB/f6B9ezymvdgI3xnB5+HBWcTcU6fWz+/v/znDxIlcgoM8ofXJWWg8d3fAza"
  "2ex9kZ6NMHcHICWE8mqV07PGmCL83VlfMNHsxZiIHVd2l+8mICS7HjxtVzkCejRnGW4cM5"
  "y9ChnIfGIH9cXQHWF0r+9OsH9O4NODoCvXoB3bsD9vZSDwZbW8DUtEm+s4KPvKe9IpjIG6"
  "EvPRo5sp5NxI28Ij65X8TXty/nI88cHLhnxMjOhcTI8hXxXWmKr1UrPp/Yv8In4iGvSMQk"
  "YidnIcn9ohiSZ+Iq4ilns7Or9+8/+FJdXIp05B99j77foweXiAONJdZKV5qPYijnksdSxN"
  "PaGjA3l/YX2rblotzO4impdWuA/RYA+y3wcxPx/cnX93yqqemrdEvL6nQrqxdMUH+uWrWl"
  "ZU26hUVNsrHxmydmZtw7iql8vwlWurK1XW3RAidNTOrU5ubV6vbtaz6pmt2/UHfoAKaPZ8"
  "3Na9LMzWtPGBm9fZKYGNZIX2KEsjI7/aNHNvrS0qH6u3eTmPbrS0q47t1z1j982AWPH9vk"
  "BwcfLaC1U7zlsae9IM4NXRljjpER/tqxYz7Ky7/TP3hgJ6m01EZfXOyhLyhI0uflbWPz2O"
  "vv37dnY3eGXv91PQM918izRStXxhdRvOh8U54R55xei7xDV7ZPs42N8SIry8uQv8+LIiIS"
  "ilhspNwo8rM8R5LonnmYyfZWbUbGBIPzderE87ioK6SQkHrR+yyXa9h5eJ6ZaXi+zp15Ha"
  "ZehGqxqMdC9NnMmdCwc9QsfDY2vFcQ/YJgFaL3mY8aVlebha9LF97PkASfXNRjsJhrWO5r"
  "dj7ho1zU47G+R+LTaAzPRzVE9MXE8qWof2Q9mqZjR4PzFUZGbiukukT5RN6DyUW/E1h+yW"
  "D5uVajmWhIvt9SUkLSzMxennNweHrO0bFhOTk9Tbe3f5Zlbf3sHatLhv4P9W/6H/UrpP/2"
  "f3Oz6R8RTSwf";

