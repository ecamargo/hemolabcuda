/* 
 * Resource generated for file:
 *    TrisurfButton_remove_free_nodes.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_remove_free_nodes_width          = 40;
static const unsigned int  image_TrisurfButton_remove_free_nodes_height         = 40;
static const unsigned int  image_TrisurfButton_remove_free_nodes_pixel_size     = 4;
static const unsigned long image_TrisurfButton_remove_free_nodes_length         = 2920;
static const unsigned long image_TrisurfButton_remove_free_nodes_decoded_length = 6400;

static const unsigned char image_TrisurfButton_remove_free_nodes[] = 
  "eNrVmAtQVNcZx//7ZB+wu6D1ASyCGB8IOBUVjXG0oLE6IA1KWhVtEsBUQcRn0K2wCMoqSh"
  "ybqbV1ppkxjTF1EqxpBVGbWGxhJIimgopNI6mjUQuIvESW0+/cu4uACy6Qwemd+c9l95y9"
  "58f3Ot+5jDGw/2N9d+cuzpw6QyrsWfmkQppzRpDHoUOHpptMJrlpmwl0x7ZU0iYTtqaQ1p"
  "mwY+tONDU2fS98R/M+AgwA3DtpCGk4aSRpBEkLfqlIk0m3SIyUT9pImg0dPPFDSGURgHw5"
  "4LlajmZr4/fC98mNk0CaFNhNkHsM4t2kA5ZqvLBANRUhiiT4y/Pwkrwa4apGeMs4G9PNAf"
  "P/AGzEejBDBGq1U3FZG4I/6KKQ6J+mnVx4ttDjYe3DPvPU1NSgurq6Qwf/dhhIlEoQpR6H"
  "l12iMEW5nnjyMV5RhzkuDMu0DBt1DJl6RvwMKTrmEipjAX8Fm3STVAUWXAkW8HfiPQrmaS"
  "LeRWAuQbhrHOOdv2jRotz9+/ePcZYvMTERarWaK4yUr3R3OQxf2UUEKloxX8XwBvFsIZ4s"
  "4skmnp0GkS3Dpi16po2UseCrYIHlpEviPegKqYJYbxDzv4i3lGLgB0IcsOkzpv/JWb74+H"
  "jYrr8IcaQhmWwsdp4d9NnsQJwzScf0iyUsuMLG5kjlok0NS0W+iIiIZGf5Vq1aZed7Ex5S"
  "htFyhrddRTazvndxvng3NjxFwibd6IXvMuk62JBk6ZODBw4uvXfvHvrBF4c48mU6rTlFKT"
  "Lueg4jt+0yVzY8SYy7Hm1HY4o4tKdu3Brb1/zo4NNKDgtxxu2WqhMZV7v1zsjHfqJlIzf0"
  "wFcuxp97kqQ9bmVCQlurFf3m85WXCnHGYz6L1n2HGIMUFF+9MPLvF2qY36+J45pjuw1NAT"
  "NGuic2ttX3q97Z+Lwx06W+S8zxtdcQ2wRiXO8m+rI7H58/R8VG/7Ybn41tZCqYWySyX6nw"
  "QD2rGQjfAvxU82xO8M+rKQ4DiXGDA0ays2yeCxt3gmrJP5/y8TrolUZ+jYJlwmWA89W1/b"
  "d/fPEC3y+R7CbWOEc26mDUdWM0MEWYggV8IdY7zsbt5r0DzCMauylvMeEKMPPKAPjeSgCU"
  "yEOaLfbMPTD+ghiDFWKt5ox8LtVJzWtyFlQq1pBJNjb312AJIragr4DxZQPk+3mCBqPwzX"
  "NrCWfkNWeyUswdzrhZz1yXyFjQZXGPMO4muy3BXoo/gY3sOWC+NXGrJ+JlWJ2qx3bGEGI0"
  "EeMmPe0JUjb5NpjPPmJ7Hbmd2Tr4vhqCRtbSL75X571qxs+kjvOzs+z77W6at4oYQ4lxrp"
  "rpFxJbLhj1Kp8G/IPYrj5lE/hIoaUaXM0/Nry6oCD83wUFxpqCArCGhl65rFYrmh41YUzI"
  "2A+xxUW0zS7D0zvn5fnCayLfU7i9uF836cQYnKtitr6PqScS2wW4ko9BPUEXBV6X4JXzKh"
  "xwQ3QBzSXVUoe56ea77w67lJMz92x4+M7zMTFx1qau/WtZWZmb0dv4sUKnbMLraobltLct"
  "phqziP5eSOK9SzhpNrHzvupHKlFhKnEskuYMk3K+WvcVmDb6FOBztKu8PgSmnhiK6qabaK"
  "i+JSvJyor+QK2+RR0Jmw3cz6L7cYnk2zMxMSnWlq7+LyoqmmT//xFMvoolvpWkOFcxV3m9"
  "4bbiex23XbrNljtttuW93wR5Hf0+wGDQw+hjfFZGI0JDQ2FtbRfW/ObEiaE5BkNJKK0ZRF"
  "oGNF40m4Mc+beyslIxa+Yss8xb2YxtxGKx+9Tm10wbT0Yndc4T3qcC6bx4ZmZm0fmi2aGa"
  "m0S73Dl3zvMYUHmOuNLl8prYgIAjvwdaCkaMKLtfVubeUxz6zxz7Z2x16bn2dRdn5rVyhO"
  "wmoak53549e3qN9cf19dIiX9+P/kNsVK5ZRWZmFGttxcUNGxZepM/lEye+39rc7PC30Yuj"
  "NyJa8vxeyi5u5/lCbiTYGzOLxdIrX+39+9pKiaSuiVhKZ8zIaSM2+1hFUtKbZ4HqRw8eaB"
  "39dvXetV4Yhwan+Ljf+dlDI/mcsBTO8j1paZFcTU5eVxYTs6/u66/lXeoIqaqw0Lu9rU3q"
  "cP84ngw6F5wSciHTifo8TfkIMgSg0/U8voFo1ekUIBgrhLqS/ZxedAXlxERFJkbJMWh8+c"
  "S3nE7UAYq7Qu72tne8JL9FtWco/AaR7+Q6On9LqBFSHECiq2Mf85xYoOI9/0rspfO6l2xw"
  "+czEF6meSnuWVWDpfkbbTLFplF2gu5L8PPh83H4bdRKMUxQL+0RGN9tNUz7Bj1UzhXcdmf"
  "rB50ulRbhdpiiThPcF9l6G3xPI577y3wlcO/Qvli9W60XnpIdCHmfYfDtGXoNlGj9hPMN5"
  "vu9KSoxVx44tqcrLG8/a2wfOx9dOJwUpjmMrxds+frZV83qyiXxMMarvE9/na9dGfUr7xW"
  "naY0+Hh2ddO3nSr3jz5sUFISG/OTt//jprY2Pf+Pj6nCNMFYkIdTvWkF995CV0tlRS3ekz"
  "n/XxY3z53nth72u112OJcwbQQM0E+0SjqfgiPj6ejztd/3gXwtl47XhHNwwKSb3Qd01WZu"
  "CAuzhmsb0LzCEZnYu/qiNHRu3T68um07MCScTZRP1UcF/8G2t6A/ClRfxJY0g+wtvTuwKf"
  "AmkYaxvz7zSn22U2m5957reffeb3MXDrPD1nl0p15+3Q0F8dAeoL3N2v3y0qGu4sX/7JU7"
  "BkWGAxZ4vKyOb2CCUlWHZaXDu+76xsi2Azu4qLi7s8s+XhQ+kFT88T9E8yOmZab+TmzuXf"
  "XzKbZ1E/9aTc3/+PjxsbJS/qHXst9UqVMtmDR8T35bx5afysYx+7bjItOaNQXKN+yvVF8b"
  "VR7Fdu3/5WeULC9vrbt7vYqZ33+cXFQ9qtVof91P8A7lqIHQ==";

