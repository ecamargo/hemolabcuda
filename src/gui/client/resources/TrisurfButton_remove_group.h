/* 
 * Resource generated for file:
 *    TrisurfButton_remove_group.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_remove_group_width          = 40;
static const unsigned int  image_TrisurfButton_remove_group_height         = 40;
static const unsigned int  image_TrisurfButton_remove_group_pixel_size     = 4;
static const unsigned long image_TrisurfButton_remove_group_length         = 2348;
static const unsigned long image_TrisurfButton_remove_group_decoded_length = 6400;

static const unsigned char image_TrisurfButton_remove_group[] = 
  "eNrlmHtQVHUUxy+gaaiBopWD6UjIFIo6OvWH46vBYhrFUnSckB4YYiGKqCODhQEiKGClQ+"
  "P41jHzgWKkQRahKJAPfJDigiEihQbyUFdAHsu33zm7d9ldYffu2jTN+MeZ3bu7v99+7jnf"
  "8/hdABKeArtdWYm62lr8X3jq79fj3NlzSElJQXDIAtjZ26Fv//7Yum0r1Gr1f8rZ1NaIP6"
  "qu41D+XixeuwCTg8bC3e8FDFjgALctEr8SH5nk3RMverti3MzxWL15DU5czuG1/zZTW0sL"
  "jhw5wr55ZcowOE2xw8BwO+bxzJbgVSRhVKnW6L3zdMHm3wtSojOkeGErn4M0rzfzdh/Ti3"
  "m/2BSLg1mpuFh6GRqN5omYKXayT9y/lTC6QssysljLM+JSh9HnxM48cU6QooXFOmk5E3Qm"
  "8057FnZju2PoJHe+d9+Zs1kj1vJVV1WzniTPbhh+RnAUGjOZ8rmukiDNdNQyRTs9bsRL7D"
  "Iv/U7sLeu2TdNmNaNa5CNpzeOSsc88TPxHPqW4m+UzNOIkfwo+h5e6I2B+ANrb222Kd51K"
  "hSH+A/Hq70J3xVo2/z3PsNFnRnxCaxb5dGwUY2lhH37vv8j/ibR4+8JFuAcPwYRMe+wNcG"
  "J77Yy9ER9p1H78Mx36s8RGWqQ4i7hT/tnqP9lUO7ch180FheNdmM00xpTT9t4OXbPFal+N"
  "2KK1eqT7amxosJmv6VIBKiaOwu1Bzpj7bg8MVpnki3jvmaPzX1d+k9lIowkGGiA9TJVQdP"
  "WqTXytVXeYrcytH6pm+aDwu71cj0mLpjntOM1Bq3tTttgu2OTvgx2479jCV/3ZUpQOG8BW"
  "Fx/Fe6jS05mRc1rHSO+dZtlDWuKoj6VRTDtjk03c05xQ63OkuUSFG56uer6GU9n6PUwZqQ"
  "bSNedkXEe945ibY9Pdx0BfN2jarOsp5C+Z7cbooWj9q8Jo/fFzqcxE+WvUQ+K1eamITc4R"
  "kVvUE6zpvxU+4/R8pEHNvfrH1qcV7mMu6n/cQ3Q9WDGbrMEAe+Tk5ijmay4tMYotsbY3Nn"
  "a6fsP29cxIxlqjOi36LM8KsU6W+eg3y/twH1HKR1qT2cz5T7Z1SckdM5boW/R/epNzWu7B"
  "cQazQ7zOxL3QzKC0Tqsz0o34yJct5WX6taa+pJnUy8tLPwOy/+hVZxxvg2v+now0QJoQuq"
  "VePNnnTUVzuCkf2cP9u0GfV4UGguL/2BrBSPuzv2TfxHXUEDbyJ9UgynPKJTLiE8yy/2m2"
  "U9IzTPlkPRKfuXMIa9DSnGUaZ2G0LiIiwmKva29rRUP2caP8MKwznfnO0GJWx3A+Kpq1DP"
  "ocnQvMzdXUy2qT4zhXO2Mz7CHmrKqmmrVkdpYx9amIOc3T5vbV1NWyxqjPUr+96erIrzKb"
  "SjCrr11RlF9R66OU1z9dfVYyI9w8nYPk8DCETfXFef8ZHE/iJDvg54vA0GVY9GEgjm36Bs"
  "3Nj7rcj74bPW2MNidiLddnl7EDQD3BHBv9p4dfKAL3ncHqKw+Q8qcGBwuKkZcQx6wFd6tQ"
  "IORxUt2IqP0HOVfNaZnOllxXzPlQN7+QZi35Lnn9BvhsycOnP1ciPKscMQU12FjWhD33gI"
  "z7zTgttEuW/agFpxvb8Xbk1xZjMiNkVsfM0FVsxT1Q3lviS01Lx/vpv2J7zQPsuFiOtbnX"
  "Mf/oVSzNLGIL/UXF3PTZgp9usq8t7Utnczr/mstl0oGSvkF1cWrKThwTF4XNGo7l2pK/EZ"
  "yVzxaWfQNzD+ezf8k8ffwVzeURySu1NdiUka5FfU7alQSlz3heXxyHVHEr58Xfplaeh8eu"
  "QejxuYSeHzvwjDb8k9lYklODgMNFnCtK9r3f9IB7q+HMKse29xv9cOvuLUX7kC9GBi7DHu"
  "G3XHUN1pUuxvQflyLoxA8Ys8GP63uPSS4c39k78kAzgdJ5g55rcN9NdO7gFLlN+lS6B2ng"
  "rffmYfPDVvbd0TtXOIfXlLbjo7Qs+CR/xfEn7fkkHEBGZqbivWk2pmcw3H/l5wiiPqad+t"
  "6qmXlh5CrElFdzflKME0sfIrLwLtuqonbOEfIf6aC4uNiqvfNUv2lzJVHbb0kvFHtr9ohZ"
  "9yVWnC3BodqT+vhSnYm+1sw+JP0RI+WuLc8jl6xZpu0rIi/Ck5dbvZ5iRjm6T30Wy8u8MH"
  "h3N9besJUTuQdRjMPyG1gH1p5nZB/Kc9TRCxlWr6eYPT9nBfswviiO2XpH9kWfoJcxIWkj"
  "QnLqOXeJz5ZnEnTGkPmsOW/odSz6g39QCOfxB9szMGljLhv1FTLqGSO837H5vC/7wFrtmu"
  "YaxZlmRWIlX9FzRKp3dM43Nxc8bfYPHg7Htw==";

