/* 
 * Resource generated for file:
 *    TrisurfButton_test_mesh.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_test_mesh_width          = 40;
static const unsigned int  image_TrisurfButton_test_mesh_height         = 40;
static const unsigned int  image_TrisurfButton_test_mesh_pixel_size     = 4;
static const unsigned long image_TrisurfButton_test_mesh_length         = 2448;
static const unsigned long image_TrisurfButton_test_mesh_decoded_length = 6400;

static const unsigned char image_TrisurfButton_test_mesh[] = 
  "eNrtmGtQVGUYx5+9nl3Yu+JlLBtFTVHALA3SvCSsSnlF7WopaV7G1DFDTTQhQMccx2nGsS"
  "+VlRExYyMjmjpmMpZhk0l5F2uGbIoaAY1KieTt/5z3LJdld1lX/NBMO/znXPbse34893OE"
  "ECT+122prq6O0tPTyev13rpSoZRUFg69Bakp3udTRnitqWO8hH35fZDflpaWhsVXXV1Niq"
  "JQBJ+u0ExoJfQ7JFQNpB/JSiXYz4GmQn0gnf+PCwoKwuKrqakhj8cTDk8nKJ2MtI26GA7R"
  "feYrNDVK0DK7oIxoQS69iJlNIuEiiQFHSfTeQaLbChJOL9Wbe1K5Tk9v4ffzoDjIVlRUFC"
  "mfCXoQioWeJpPubepqOAaeKppiFbQYPGudgvJdgjZAuU51a5yoiLjPSMSfgU6B8zyJxAq5"
  "HVhGou8u8C4joTPQP1j3Ivj6Rci3SfWTU3+d0mGfF8Gzzo8nB1rfQtkuYRprEnFHwPYdiU"
  "EnW4vPJX5P4t4D4DPKOABfWoR8b6p8dxkEbQZPHt/fj8dfa5wierpRxJ8Az7dt+VSVg/ES"
  "Cc90lW8z+JQI+SbSKEXQBPhyklXaLBQbs2c6heMpg0g4JTkC8bGve79Hwhij+ocijj8jZd"
  "Fy+PR1cI21CJoWFZqRfb0MfJP1IuFMYDbOGY4/Y2c1R+i2+GIMH6vx/xqUC67RsOWTIRj5"
  "uvl20SlDp+ZCG7YLiLsSEuYe9BFWN9wmn0KDTOcoz9VsG/ZfoknQHFtgRs6XZ22ie6b0YS"
  "u2c6g1KMOWvnQCa0e3DKII+QbQOMuNVhx8/5UOQQlgXGSXedySj/+XJ2yix5rWfFxn4o6T"
  "iB5C57FuT/9iGiFfOs2ODszwEjOaBS21iyb7rtfqDup0r+3U5N/40zJP7COpEmv2C1TsI+"
  "Kz6nJphUPGlL8fmWMJ2BLByNf4GHFe95hV9EW7SjgrazPXO+d4+gUrJgZrRgV79tw6392G"
  "vWq8Bat3zMg+HgLG1Q7pe+SQYZJF9N8LLsRbPBjd06hW60EqYFEAXU5OJjFjhtSlS3S5rK"
  "z3vlmztpRkZeUG4YuioUplu/WOv5+HXBkKxizJbBxvFnGHZY+IyaA/sdZYn51SmgaHwKpH"
  "Th9ITl59yGCoPYhThxYufCUI33D014Z2+XyMcxCnyYrKqKSZRAJirtty9ZaTfWxR+FtFtP"
  "YkUcZf6Ov+bNWYgT4kKi7F4dfQvrS0N25i3mvD51b5VtBCm4wrn49zfHVQq4X8Xb6mLW5B"
  "z4DxHqMwuHXClUY3SI/a3uJjRk3ZSVR5AfcuJPoS22E+tsvIGzTSC2yzvdCOAQOKG65f1w"
  "WMP5enHxYroVUOyZSFLe+vsMv5YL5NzlDMMwO1ejL6HnKCJkI2ne+WuwLlwSMIwW9gqrO4"
  "5gOiOmxTaojc7xCVf4Jz+6FtUVEV18rLnUHywwr/Vqj3iIMLUiyyZ7C4v40Dw6NWycRszM"
  "isL9hkzWFGniUc+s+xht6fbzx0imj+UaxfwT4E4/s4hYtFOYSmUlexbVtSiPxV4N/j6Dxy"
  "HmC/vqr5NtfZ7NMN2paPc5vnPkpS5IyaYuG86OXPN04z7mHY9zfswnbiV+gq9IOMu7XiyJ"
  "HQ9cXu7kwx9IUaaznO9vPDV2uegx0fAt8ml2RUdFv9+bwaXxV6COxY3YjDBuimZKsCbydx"
  "4EBoPpeHPbNajbXcMNh8uRNvEk2/Yd54E9c9ZyA+1mn4+Qp2sS+uQeBdon7XHp+sL0Pgo7"
  "9pY5j1ZSZiMdncPDMw31zEpJ4y/OOvZU0B07s/Yxc15yBqjlk9v39/OHx66mH4KmT/8NmO"
  "56/BWDrT3twLs7Va1MvIuWbx8Tm1RsJKggZjwsSD39A9+flWUYYHE9bVq+H1Nz2tpwU2Od"
  "OHsh3nUXqAmZCPp0cJdQZv51NYXBzJ/BKHunIj5Cz6skP23/UB7Mwxyc9SXQxca6yh+CKc"
  "r3TUzXBYrS/ZQWzHNXFWgBnMJ47fcRa24eg7wMeEc9U4z3O1nZMX4/z9Zln/gvmfbZzp4L"
  "6y/Y7w8XuL4UpNGx8zb5JZ9o28dvKbbThc+UN7xu9oPlRMfYGaozkt2LinPawE96v//8Lv"
  "Gcy6dXeEj2hKqzk/R6snS2+hfvN19xh4hl6uPnd1LJ+Nhpkvqz5m8VzAeREsr7Nb9Gu+hp"
  "+d+RnA1FSWR3QwHwqrfnvTfdl2K7XnEl8dztPYWXwNz/uLbPJ/Gak0UKyxErl2DCvt1N59"
  "dSwf0Rg8C92kPkZBj+OeW93S39lafnKeTLXyfMDX/ERu/THMQHnaO7/+kKOj8tfhCLhUfF"
  "PbHIX5bgJmgQRdFXXH86yFinF+I5SqPjP7zQThfAoLC8Piq62tpdjYWNWGfnJCn0KnsLfA"
  "E+1+APJgFjPiWH0mCPCbsLV79+6w+BobG1Ub8nveANJD5iDf3Zbq6+v/8+/u/wWkwTLn";

