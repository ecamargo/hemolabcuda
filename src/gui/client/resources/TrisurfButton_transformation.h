/* 
 * Resource generated for file:
 *    TrisurfButton_transformation.png (zlib, base64) (image file)
 */
static const unsigned int  image_TrisurfButton_transformation_width          = 40;
static const unsigned int  image_TrisurfButton_transformation_height         = 40;
static const unsigned int  image_TrisurfButton_transformation_pixel_size     = 4;
static const unsigned long image_TrisurfButton_transformation_length         = 1972;
static const unsigned long image_TrisurfButton_transformation_decoded_length = 6400;

static const unsigned char image_TrisurfButton_transformation[] = 
  "eNrtmH1M1HUcxz/3CN5xd4B6qIOATglJQDNkhHOxSFIsZUQssQWbGiQaDszZMiQfYpYt8o"
  "+8tYnLp+YiISqu3RqEcV4USwE1xKX+obOmItPxkMS39+fud4LE48G4f/xur/H7fX8P9+Lz"
  "ffp8f0IIEo+YVDo7O6mkpISKi4uNgG7cuEEdHR1kt9sHxwZOgVq7zP6zPdz2o10G6PRPqK"
  "tz3tPc3DxmD6vVOt9sNj8LCAScOHFiCdffunWL5HK5kYh+AaFnzpyhxsZGGqTIweOkpOU0"
  "g7ZTOFnJj7p919CvmkQ6LH+Mtjmu4Z6FCxd6jdUvOTn5JTwrwBPgi8DAwGKub2trI71e/7"
  "F07RDcFE1NTS6ncLCXIpVf0jLvRsrU3KMCnaAivaBCgzBsUYj5l0lEtpCIaCARXk3C+0Xq"
  "eTpy0aGx+qWlpfHvfQcugNNz587V9fb2Uk9Pj6y0tHQF6q4fP358GdpWWV9f7/JbQpEqQR"
  "vhtM9X0B6w0yDoffC2QRh3K0X0BRLzfodjE4kF10jo36DWmOjYRW76RUtxyoQfsR9fa2ho"
  "CETdnyj+fM59yFE0lELbEats+GVqBe2CV5Hkt1kvZu2Vi+g/4NZIIqqVRMBG4n8sKDYmds"
  "z9LyUlhX/RH9wD8SaT6YGfzWYzoe7yuXPnZj3kZ1J8SrsRM2atj6AMrTN+TK5eBJtlIvoS"
  "3OA4LZNq8UQAPxYbO3a/7OxsCgoKmgqaQGxCQsJIfjJ6zqueiuG2Az67QQ4cX9UgjqjL1Y"
  "mwCqeb30r6CvfrXJ3CHb/u7m6eS2TAG8i7uroeXBvCL5DWau85/Fwx4+MCtPfrWqFYpxFh"
  "ZSQMz9PnuFfRf6C74zcc//Oz2VWYK96lXMRrC3w2o/9tAht0zrHymlbIZimEahoddsR5QJ"
  "kEv5mkpuuUMcXptMHH6ZUH8nXOGK7AtVDlocEmyknwI/KiV2ibrm9Occ0r3MbcB3kezNX9"
  "g1l7waT7OfuflpZ7X3kwPnZIjm/p+sbIByBK9bWH/Ihmync6YlYkgTWDXobbe9I5zzu5Pr"
  "3oqwke8SMKoSyMYW5jnl/WaJ39cJehL6Yc30XqHzzkR2i/I/SRb1+7csyKpLbmY173eHwr"
  "kDMQRtVk+6lpMWX7/OtYOzhuHEtuZx4f63y6KFXTSk+pa3DnQUefnQS/urq6/s3lDa4gs7"
  "pM86kSK9hnGNtvoi5JyoWmDBwfUVFRo/5tXsOGo7/f+fPnZ/L5/fv3yWKx9PG9ZZ6lssrX"
  "Ul5Flm/At1VcR5Yqy8P3SVRXVz/0/qEoLCwkzlOGgtdgvu/s2bOcv1xpbW0NqKmpobKysk"
  "F/d7Skp6fn4v0L+P3919GBZGVl0XDFaDQ67tu/fz8nX2LOnDkX8bdpnDRKOZyYPn36Gt4/"
  "DOW3fv36Yf04hhUVFbNxeHvr1q0FOF65CgX9e0zExcWtlODjVWq12oJ3vuPn52ccj59Op/"
  "PSarUXkRuWTOR4q62t1WB/Q5WVlZyfu+2HciwkJOTUnTt35O66tLe3E2K2KSIi4kiEsxw8"
  "cOCAbjTPjuCXZzAY/sacMmM8sbp58yZpNBqeZ26Dv0BBfn4+jdPvGdAZHBycMNIcMBK8P/"
  "X3520DVUvjIhR9eTx+vEe4xv8n8nwarv+ONn4qlYonit8kqvPy8lRu+vEem9eik3wyEX53"
  "795VYB77EPvDGBAOTh49enS2m357wCXXmjQRfuNhgF8q6OJpz1WB/ufRbz85OTkuldnSvj"
  "ddOo8DaZg/l5WXl0d7wo377erVq9nFCzSDT/rFkvOPYzze4uPj93rCLyMjQ6ZQKF7gb0Hg"
  "1MA9Kgrnk5eWLl3q5wm/1NRUzhXbpTnJNMCNvwd1cKqWmJjokb6XnJy82JFXEpVijQ1x5V"
  "RhYWGkVCptMpmsDZQnJSVt84Sf1Wqdajabffl7JOfrrnyR1+uWlpZQ5KJPgpirV6+aHn1H"
  "fsRE8B+L5MOL";

