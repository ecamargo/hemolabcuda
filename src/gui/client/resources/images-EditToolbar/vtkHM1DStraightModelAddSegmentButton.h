/* 
 * Resource generated for file:
 *    1DStraightModelAddSegmentButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelAddSegmentButton_width          = 20;
static const unsigned int  image_1DStraightModelAddSegmentButton_height         = 20;
static const unsigned int  image_1DStraightModelAddSegmentButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelAddSegmentButton_length         = 888;
static const unsigned long image_1DStraightModelAddSegmentButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelAddSegmentButton[] = 
  "eNrVlF1Ik1EYx//v9q43Z9mHzo+iUTpwktZKySDtysCWsu3Cm4qsJAksBmlkgRj4hoNuIs"
  "IwvwgyykolTOsiIfpwOKdb4ahRRoilSWhrQ5rTp7OYMXSu3XTRA8/Fy3P4vf/zO5xDRKD/"
  "pYeHMVZefnREFK+4KivV1Noadv2cz8U7XtQYB5+cPvvJcT8qeDYyPo5bOh2mNJout8lE0+"
  "npeRNlZTCZTKgRRYhB7f+uqqnFzcvaM+8fgN7dBg0+LrkQzHOKYvHTuLgOT2np6FxLC30v"
  "Ln75xmBolgMJCFFbgUNtaZj52svRCGNau0uMwbwfRmMdKZXky8oiysmh2fx8Gs3MpE2AKp"
  "gjZZ0DHHwFeIkD1e+Au+Wq9uSYs0sWzBvu7FQ3FRYWePT65/O5ueSqrr7oqK/fHysIwgJL"
  "wnrLOv7II4HzEkB3AXcMUFDXtNRzb18fdFotZqqq2r4ZDDTb3r7XaTZjtVz+h7U+JeqY5F"
  "KyL/d4EjWvkLiTgAP+WWNj4xKe1WpFvEKBDIUiOVUQsneqVDEZajUkEslv3j7mq1It/8lf"
  "SyWuQjkTLZcULuQOxbPb7eA4LpR67A74mme+Tm1e6ZbF8rrgeSiezWYDz/NY7H4PyzXodx"
  "/wlcJ8Lf5fJDx/0uhtq05sKEny3kiT00PGSgjBipSXx3JpDid6cSed4ooUHuUyrHA8aYCX"
  "zXwNsD06ElbQdr3CjY2CHmEq5PkO2diExy6Wyxzw1cH2qAqTK+z5vrZJy4pk57or4P2sAd"
  "1jrPgIWMvxpsZtame7lHx2UI8JvkQOOkRYDQ0NS3gfnENr22r5Z196MF93nrsODhFXqHwu"
  "lwdvHQNrpicsmaMfLbL+fgsslr+3md3JycnJf/Ym/wLBybM2";

