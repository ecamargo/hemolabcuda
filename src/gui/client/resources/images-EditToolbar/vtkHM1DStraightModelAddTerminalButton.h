/* 
 * Resource generated for file:
 *    1DStraightModelAddTerminalButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelAddTerminalButton_width          = 20;
static const unsigned int  image_1DStraightModelAddTerminalButton_height         = 20;
static const unsigned int  image_1DStraightModelAddTerminalButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelAddTerminalButton_length         = 924;
static const unsigned long image_1DStraightModelAddTerminalButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelAddTerminalButton[] = 
  "eNr7//8/w/9BhD98+cfw8xd2uT89PbwPqqsbHjY11f6qreX6f/cuTnP+/P3PMmNiZdXEKu"
  "OTOckOVU+fPGJFll+2ZAnDZT4+wS9JSV/ep6e/+2FpybOhvJyhsaWFoQUNNze3MCQlJVqs"
  "aWf4/+8ew//VrQz/N25Y5wK3688fhp329j23NTX3/mlt/fGzo+P7l8TEPRNUVNoZsAB2dg"
  "6RoKCgQ1smAJ30iOH/2g6G/zt3bLWBmfcbaN4fN7fr/1RV//8zNf3/z8Hh/y8vr/87lZUv"
  "oJslKCgo0tzcfPDbt1//p09s+B/pynbZ0VIh7+mTh0ww8/4B8aT0dKOrZWUh/z08Pv1wd3"
  "/7e9Wq8IqgIENks/j5+SXy8/MP/fjx4/+LFy/++/kHL5eV1+S8fuMuw+/fv1HC1zc8nGFd"
  "VxfPs/j4G/ejoi6/O3WKIyoiAm4WNze3aH9//6mLFy/+37179/+wsLDlIK/r6+tjja+Q0F"
  "AGCRERZml2djkpdnZZJVlZJl5ubpi7RGpraw+eOX0a7C5PD49VILNAciDz0N0GNi8kBFvQ"
  "g81qbGw8CNTz//KlS//d3NxWMjExccDkSTEP5EegWYdAZj179uw/UA3Ij2zIaog1T0hISL"
  "K+ru7k0iVL/l+5cgVmFju6ncSYJyAgIFJZWXno29ev/0FhZmNtvQKbWcSYBwqvpqamg1+B"
  "Zl24cOF/eHg4KOw5GHAAXOb5+fkxsLCwiLS1tcHDKzAwECO8iDWvoKBAB5hWz2/duvX/rV"
  "u3/gcHBy/H5UdizNu2bdvyV69e/b98+fJ/b2/vjcSYBQK6urpYzZs4cWLynj17vgPd91RO"
  "Tk6dgUgAct+vX78wzLtz5w7DvXv3dB4+fCh3/vx5hlOnTjGcPn0aLz558iTD1atXGf79+0"
  "ez8hoA5lbqPg==";

