/* 
 * Resource generated for file:
 *    1DStraightModelCheckConsistenceButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelCheckConsistenceButton_width          = 20;
static const unsigned int  image_1DStraightModelCheckConsistenceButton_height         = 20;
static const unsigned int  image_1DStraightModelCheckConsistenceButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelCheckConsistenceButton_length         = 640;
static const unsigned long image_1DStraightModelCheckConsistenceButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelCheckConsistenceButton[] = 
  "eNqllEsvA1EUx30AsfEFrCzsbZoQr3iFnYRPQCQikZImDQsRxGODhIimKJIOIU2wEA0lDa"
  "lm0CgVj6I2NtKgnU7amflzr0zNNDNVtTi553HPb86cc+8NBHw5gRSRJBGQElDqkiQhoLE3"
  "E/EcjCB8XQFRFOFyToMLloOLvmfNa+hZw4ilH7LePW6m7Gx5+7v9uGCLaL5joxcnbsOfWa"
  "Q/ft8RSD2yWOxWNI2Ng3FMU9t3dphxH0WRR9CbR/Pq2ldRbZ7Aljf/225dpyuJk32Z8ISv"
  "/jQPd9O8SCSCGB9L1slxHF0bB4Yy7qMgSqjtWKF5yrnINllrjAwSCSEZJ+eIiNY3iO83Hh"
  "EtntL3U58AZX44/IrBJWPS9vtZmBbbaB9SeYIgQGseT64CPN4eJOvxsLnY3lmi9v21Ey/H"
  "hapzLfPi8Ti0Z/zdB/K9lj6G1lbVZdPdL/Pku5nufjSaGfA8T5mj1k2kY+nNhEgoyGJ4sg"
  "rs+QWN3z08wWqvROh+D+l4ejWenzJYWClWxWbny+D1zGvyZD0Ru4Hev5Z3LCPVpzxHWjy9"
  "+kheaadFFas32f7FS62PzCZbntu9CvOYQTWvwZkS+q5mw4uGL/HxPKV6l95CcyD+dLx49I"
  "rqn8L2bTE=";

