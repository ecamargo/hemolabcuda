/* 
 * Resource generated for file:
 *    1DStraightModelCopySegmentButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelCopySegmentButton_width          = 20;
static const unsigned int  image_1DStraightModelCopySegmentButton_height         = 20;
static const unsigned int  image_1DStraightModelCopySegmentButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelCopySegmentButton_length         = 896;
static const unsigned long image_1DStraightModelCopySegmentButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelCopySegmentButton[] = 
  "eNqFk19IU1Ecx7/iv0UY0pgojSRE+yOZFvRikDVdsoIMyrSX/tEMjUgf1LTNZWPNbCNQyi"
  "RdDw5a/mHINOwfPehLVLSI9dBD4Yxxt5VmUZbO0+/uzhi4e3fh83DPuXzuOZ977oCtXdlU"
  "lz1h71JwA7cUvk79rpvOwRsyxhik+DE/h0DAj4Dfj7+LS//Hmy+pDDYz2JxboPcamHPQfF"
  "TK5X7zAg01ubjXmYVecxY62koQ4GbCc5cb1Aa7FezbewG7JeGXa6SjUsrXeFGFnnYg+Brw"
  "+zmFn+PktKfwXHXFvv0tpVh+TrfPTGBXD6V4Xk6NpUv59E1lGBDplLleXmhKwdKHbLBXSr"
  "AraWsm47WrPaMR7ZQMFHYDS38A5iPaZLIph8MBKXbnF4h2SgHyu8i3EPHpyQcgARJXIiHW"
  "KRXYGu0zCL4NxCaiKAI9hqQVH38j1onWVxTt0wm+KmKE+Ew8Je4QW4hcooD3xeoU9iUmhn"
  "1R45P0eBahIWoINzFL8O+xEVreF6sT79uel1dkifJF+skiW6Nl4CThJUKEh7CmCnte1Yn3"
  "VZSX79ClYvFTJpg7A6w1Lbw+RPn2EA8jrq8r3yNWJ953qkpdWl+C0CMdmKsVrOVAsufJeL"
  "88ysd/ayWh1mq14MlRKmN2Cv9v9WUGWycY5xbou44F13DHManzfESjidmJn6uv3au7T+d8"
  "/p1AnwnL405rhZTvcKk6Zid+rrXxRI6pefPQcE96kJjpNu00Ph67u07KV3dOLdppaPBBgq"
  "GxOH1i5Py2UfvxnNvW6rXx/l9Ds3pVp34zfq908n6cRp8xCd63dHCNKsTz1ZwuluwUnP6C"
  "poMboT+bAcuFyri+4SGHZKflUAhzviA4bxA/Z7/H9fGIdfoHb5nxCw==";

