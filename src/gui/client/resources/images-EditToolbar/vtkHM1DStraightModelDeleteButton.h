/* 
 * Resource generated for file:
 *    1DStraightModelDeleteButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelDeleteButton_width          = 20;
static const unsigned int  image_1DStraightModelDeleteButton_height         = 20;
static const unsigned int  image_1DStraightModelDeleteButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelDeleteButton_length         = 1120;
static const unsigned long image_1DStraightModelDeleteButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelDeleteButton[] = 
  "eNqd1GdMU1EUB/B/J5RXZNjaAe4CQhyYqJFgBKI4omkMUdyIE1ccHyASlTiq1uDCgRgcqR"
  "QRqQpE/YAjDkQpxEGrggYUDLUtigNFcXB8ViSgiMhLzof3cvPLuf937iUiUFM1NjbCUp4X"
  "/OxB9pCP7yubv3e2nhTtmHZXz20wHsP7i1nLQm8WFqKwg2U0GlFfX9/Ku58Xvct2AVR7C5"
  "Q0HBUywBf/8ZhMplbea+sdxblUf8PFWaAPzqBUoFwJ+HTE4vP5MJvNf+w5RZeBAGCbGewr"
  "WzrW9Ab8/uUJBAKHV1dXh9jYWCyPicGKxTGYGBoCcDgYCmy+y+PQojGe5LpAWSWWCAZ0xL"
  "PabHBmGGiEQL5UgL0eQkS5i0LDXZwSVGJeGjb2Jhz1pwn9mQq/dvL85dnsdkjlcuQqXVHU"
  "pyviJMyy7B4eHyU8rtqRywAmcXKYB9U4celoO3m29LqxXqZcjHiJeJBZJfk2002kl/G5jn"
  "UctsayeT5sylPPmr5t9NnS81IooPF0RqLM9dATHymFMsJJs9xFrdYHsWYZ612VCmjQSLcK"
  "F7lw4N+87koFIhk+Tni7F5SxXrS7aKdKyFP93kMYa/abIiXoAkgxXVapatFn8/+w18Crm5"
  "Q7n4d9j7xd6l+pPClBBBO7RNZWTrxA8Q5FXE/KDGAce/+VZ7P3woJeAYHzwqcvrjoZHUV5"
  "w/xp1aZNloio2SednEXD2zKDONCWNeV5hDVdHeZP79njUoxfs92kfdxAG4te0obzxWR4R5"
  "RW/Yn6Dg05/rf5CGqa+VoJaPdolI8YLAi+V2LGhzdvsCTp4JnLnxtJcy+XglPjKHxPMs05"
  "U0Zu3X1i25vhEWyf19dx6PVtULFeWGuvNit/nLHNh9O0OksVbX1koFFH1pPPUjXN1uUT08"
  "Vj9L/OWUo8jO+ugK4kw2apKpH/8A6kZ81LKr1DKZVGWltSR3Oz82lSYnrDFo2mr1arRXuV"
  "pdf2qjZpV1/KTRz8wlrjuAPOnr8Utig3g6YWqMl3awSFJO0n9YbDNW9f2l06c4/arVZx4L"
  "iIVL8ZS0q9Ihc8761eWLwyPmHht69fOn03F9y4hhzDaeG57JwuOYZTnMqnFZ1yvgODN2Zl";

