/* 
 * Resource generated for file:
 *    1DStraightModelEditGeometryButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelEditGeometryButton_width          = 20;
static const unsigned int  image_1DStraightModelEditGeometryButton_height         = 20;
static const unsigned int  image_1DStraightModelEditGeometryButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelEditGeometryButton_length         = 532;
static const unsigned long image_1DStraightModelEditGeometryButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelEditGeometryButton[] = 
  "eNrVlD9IAnEUx78tB01CNDbl2OAJeUNCoETTzbmkIjeIELjd2ObQVKsi+KcxCMIp8AhX4Q"
  "YXadAhLiiICLkWhet6J6ecv/udWtDQgw+84f0+93vv8TvbtmH/A4bDYajRaOwQWEW3252f"
  "m0wm6HQ6sCxrwVetVncBvBIPxDlxSITAiXw+Pz83Go0gyzLG4/GCj767QaU6YXt4Jm6JM0"
  "IkBMenqurC2VQqxfM5pReMz8sX8Uhci6IYLxaLcCgUCkgkEr5+Xd/xEt+MDyLu7T8Sifj2"
  "4fq2iPclridin51nNBoN8jlxH+B6IfZ4+1nhUwN8JnHyC5/EeD49uUWc/tC36c5pNi+J2b"
  "vPyfM1m01vyR3xxsz+MsjJ89VqNSSTSWQyGYTD4SP3fbDBdfJ8lUoFmqZNc0VRsCRYZzrI"
  "12q1prlzxxXhddrkO2B95XIZ7XZ7mudyOawRV67vht7HNuur1+vIZrMolUqIxWIQBGEd0k"
  "RIkiRfv6ZpotfrQdd19Pt9DAaDtTEM48/+y98jQ3X5";

