/* 
 * Resource generated for file:
 *    1DStraightModelFlipButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelFlipButton_width          = 20;
static const unsigned int  image_1DStraightModelFlipButton_height         = 20;
static const unsigned int  image_1DStraightModelFlipButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelFlipButton_length         = 980;
static const unsigned long image_1DStraightModelFlipButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelFlipButton[] = 
  "eNqtlF9Ik2EUxs/ntvfL5R9SERRCKdSBFDlSujASiywDAy2DwiAqughBQq1gm2XlhrqV0c"
  "UUc7JqNd20YAjWTMEleGkgkUgXXVg4KVoRwWzrPDlhTJdeNDhw3vP8nmfvt33vS/T/P1Ne"
  "vTTuqU/pt2iyOlpK1ENOk2IjDxhmk+GBFxmY32jUpJlbcg9evZwTjlSh26HbtlEeM2nM7u"
  "Fagg8ZyDJdyyrCesyx9ZtvQA5wP/e0p+rkC5cpL14WNDDM3u25Rctcf/fCWQVmXVYF+g8T"
  "hAp5bcpp1nNHnlvS4+WxlgwGOfCgIs8mOvU5BVF54cGGhE+tlYnV/c0nEqd9Hik2CzNozD"
  "SCjfiCyOCsbDCG8oSmvmO0OHGUwp7CBH9XurDokhJ3xNsfNKMQFrDwwIuMVf1SprLKmkSz"
  "S0ThdzL97BMqr36LrI2XBw0M2JkkCnH2LGccWdXLVMrtZqLx90TBRc50KxTzHULUtsiyOj"
  "YLM2icMQ8WHu7HkRHNGYiMY0Q/sMdpSfK3C9FpkOWU2DzM2N8JBiw8vDZGM+eIpItE5Tai"
  "QIAZcMxMGoTIXpPHM2gLzARWuAC8yIjm7hBl3ifyLUXyhhSKGb0QNWt+O55B+0y0HPleH7"
  "yxXANRBm+67WWEe6OSFnRq2dhVuV8x6ulVotDj2bgWvkj0+20mLXPfBu96/1urkCoeqsnP"
  "XOh1Pn037xUuU51m38jwvTQUeva7oIHh3g9PvPfgSkHqgbYS1aTjDIVG9RR0GpRzeE897v"
  "Y8FHrMoIEBC0+8vPrTu3fB47VR+OPUynnB2m2/UOqyny+NnKNf0MBgDc+/7g0wr+zKr6vn"
  "D+sn3TXFj63VxVHnMggG643uodvNmuNRd1fY8WBnWbflcCoKfbQGdjP3q+l6fp7TWqQdth"
  "VrB3oPZditpxQo9JhBA7PZ+/pmk1Y59Ois0vOsTh7sPyWcfbUSCj1m0MCs5/0DzXaPNw==";

