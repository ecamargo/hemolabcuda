/* 
 * Resource generated for file:
 *    1DStraightModelMoveButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelMoveButton_width          = 20;
static const unsigned int  image_1DStraightModelMoveButton_height         = 20;
static const unsigned int  image_1DStraightModelMoveButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelMoveButton_length         = 968;
static const unsigned long image_1DStraightModelMoveButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelMoveButton[] = 
  "eNrVk19IWnEUx39Pe95TvQajtx4d1Xxwc5mFiAQ52ZCcpgVF1NaykBLG7M+2YtYcbEKs0o"
  "jZPxOpIQ23Jlu4i7LlmDhkpSz/pVhmaso9u/eOxdZc+roDh9+9h9/93PPnewAAwX/gyx/D"
  "lBs/RYgzer1NqQYajQbdI8/ggWnr1tCiA6nmMaS1bKHDw8OCvDksTLnBGUMjL2w32KyrQB"
  "rzMgNuP17qHVtxosElB3K43Ch9lCzIm3fsoUXnHlrZiiP18qagvr6O4tWwWND//JVsamMH"
  "PXntQ9jnryiVLJzfbjyDdPYQ0mF7aNjwTnjCq6mBa/JHk2NLWNfkh/iFL9uBonikb0fTSL"
  "sZQT1TG+LfeUwmEwTN7dA8rPt27/7YJBG+U+xcgoksWn7j5NTV/eSxiHpJ001PA72SBgMD"
  "/dDZ2Ymvra29JcKXzmCV5HK5kmwmfX7Xv8P9xWMwGHBwcACjo6OgVqupWEVFBfT19YHJZM"
  "oQr5X5eBiGtZWVleHV1dW4RqPBuVwu9a1EIoFkMgkzMzOg1WqB0ArQ6XRIp9PA4XDA5XLx"
  "8vGMRmNXU1MTxeDz+Sd1nrZYLAYqlYp6JmuwWq3SfDy/39/d0dEBNpsNUqkU1NbWQiFjs9"
  "lgNpvl/+pfJBJ539DQQP4TBAJBXkY2m6VqJk+yBoLXe9Z85XJ5xGAwUPdPG47jkMlkIBqN"
  "nsyK6JOigGaelpeX4/lyI3lkLxKJBKyuroJQKEzb7fb2grs8N5dTKpV/scicSRahKxCLxU"
  "DcsQaDwdKCmg4G+WSv4/H4HyxSN+FwGAKBAMhkMlKTZp/PV8yelOp0uiOFQpG3Zr1ef8zj"
  "8XKhUKjX4/EUtXfETMg+QmtrK7S0tIBUKgXJTRGIRCKorKrKNDY2HhO8i263uxjeuf39fd"
  "H6+voEMT/NwsLC1Oys/mXP4Lj37tDD7xPj40GLxeLxer1XTvN+ADtAl7Y=";

