/* 
 * Resource generated for file:
 *    1DStraightModelAneurysmButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelAneurysmButton_width          = 20;
static const unsigned int  image_1DStraightModelAneurysmButton_height         = 20;
static const unsigned int  image_1DStraightModelAneurysmButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelAneurysmButton_length         = 576;
static const unsigned long image_1DStraightModelAneurysmButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelAneurysmButton[] = 
  "eNqt1M8rg3EcB3Bmw9aiXCgyEfJj0u4SB9k/4ODiIM7E0hItpXExrTUKYYn9YptiW2bjmc"
  "fWfrCFk5uLHOUqb9/P03Ny2/M4fHp6Ds+r9/vzPN/nuZDG02MKufSNcH1m93KGjJhnAm6H"
  "EZxvDIXsjSyPch3bjXiPloPbViGwa5KdL36xDX5Pifh0OTZam+B37aCY56VnTF3hwjUHG7"
  "Oeyspg6ekSTLm7JIMsMq0dOqybZuG0roBLhCV51JHM9XYdCloVgho1bA31mB8axGXALfn9"
  "U66gRgOwnB9seDZLhn4ETw4kedSRcpH1w+aLTVahwGqfXjBLzcklIkJHXrQo56doUk7qXm"
  "pGeoaeJeNTNMm+Y7M4OoJ8hiv5uwyeHAodySTrW9yneXgASdZByi7JpJynWi0yzIo1VsFs"
  "0MGz65R8fqi7Y3EBJrZTi74TaWUF7C3NOJNxjmhf1NHLcpH1Qt98T7dgyj1HZFh7u3FfVw"
  "nPVA2OHJMo5hKSPepIZnS5Fm+Ravg323AdPpKdM+qdwWtIDddaG0K+LdleMh7C+f64YD1k"
  "bv/l//53fgGG/d0f";

