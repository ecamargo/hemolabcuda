/* 
 * Resource generated for file:
 *    1DStraightModelStenosisButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelStenosisButton_width          = 20;
static const unsigned int  image_1DStraightModelStenosisButton_height         = 20;
static const unsigned int  image_1DStraightModelStenosisButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelStenosisButton_length         = 732;
static const unsigned long image_1DStraightModelStenosisButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelStenosisButton[] = 
  "eNqdlDFPU2EUhu+HaepAtYkuDiUOGtEBQdJoHB10M9HBjejggIurMSoqGKAhIVokRbGWyr"
  "UttFzwtoCttLUtbbFVKNXEDY0oNpHGgcGow+s5V/0B9xve4S5Pnvd859zK60W8jKtIRP1Y"
  "0K4iod/HcimLd5UlqRQy8wgMncZ7vQnrc1aEBg9g+ultvF0pSvG8dy+gGrbj9ythpJ4UuH"
  "7+FOLTE1I8LeBCZmyvwfqRF1gbEwhYLOhvbzWYZj1LhRTUoTOGV7lL4FOLgg1FQbihAXeO"
  "OpGenzHtmEvpuEYd2esrsUCpUx7YGuHrvYXVN3lTPO6kT46jp7UFEfJi1i9KmfLo+DF675"
  "zULGeCfnS1t5GXDSVibVEG9u+DOuJGdblgmsee+eQcfH3dGCUvZi2Rr6vJgdDjEayU5TyZ"
  "yzs97nHD5XCgSp43DzUbTBnP/+F3CHk9uHGw2WCyJ3eX9eTwWzCznzy5ex/NIOi5h+zsFM"
  "rFtNQdsSd37yVWhTxVeyMGd+1Ad+dF6BNPpDy5Y4i8/LttqAsFX4gb4TtqO2wwZTy5o2vP"
  "TnwWf3e+RtFpBj3OI3j+bNI0j+fFHdmr9u+ONq0Khk/YEX54CYvpmPk7om5GR/L6ZlGw3i"
  "lQS2zDWnQ7ktoVqVkykzv6TtrwfYH+SwWBn0WB3NQ56X3PvYgh6D6Lj7NWbGUFPkStyKhO"
  "6b3kpOIaNG8HirHLiIx2GN9/AD9AyGo=";

