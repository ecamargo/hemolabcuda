/* 
 * Resource generated for file:
 *    1DStraightModelClipButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelClipButton_width          = 20;
static const unsigned int  image_1DStraightModelClipButton_height         = 20;
static const unsigned int  image_1DStraightModelClipButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelClipButton_length         = 856;
static const unsigned long image_1DStraightModelClipButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelClipButton[] = 
  "eNq1lF9IU1Ecx79394qb1lxezMQZTVlOBpX2Z2KWZrAY9BBzKrT2aA8DM6JeeuipEEbgQ/"
  "+kNCgzfMgRQQQJ4UvZXIgRSkH3wZUWdDfHBjqH7vTbnLJMLxPqwId7uPeeD19+v3MOYwzs"
  "H7O8tIjoXGBbNBxQL8UCYIsBJDYg9Z6+xxdkRZ886y8ZH9BI/kHN2OXzeeoLzjx0bkJHmw"
  "YjQ+cUfb++v6+YGkJ8pgfMwaMfAA+F4e11KPoisg/tZ3CwT4evCwBrBx6RULWRiyOeP2xV"
  "9C3M+dBwCNACxpeA1F2znfGNuie0ODNnNZGfjW8+5MOp2pVFRQJnVF8smzZe3cMcAvc4Ha"
  "mVYMnnVn0QOAhFOVU3CwRpmRzHgJGki+f5uxoaKhIODzqz96VHKVDWCczsIJcgCLcNBgPM"
  "ZjN4qqrdZhK9Xm+uoq8u2VUulS/dCTsRJ26ta0kFEXC5XJc29QXJ10iefCq/lshVNaXrdW"
  "edq5yQiE9ut7tcMZ/lj3W7iY7CQhEmU1VmLkkNhHNyUaLX62G1WlNccVuxGPmckW8UTTV/"
  "7zVdQT5aWppXc03vo8zjQMKtxf3M/ywmYF4eXfNFg5N42m3BvesW9NyoXeNB12E4bKXJXD"
  "+JCZo43ghcyG4TGY5oPau+RsqSrFk2d8XAQH+JKIo9hB4CD61GVY9rhkizcxc7DXQlfQ3V"
  "2ftisRhkWU5RaapM5dmp5etf53Ch8Mr+7LIcyN6Xyd5Kc2aj64aB0MRZsGfdeBX64RO34k"
  "okEjhuKUMxHfJiHZ1L4oQeRz/0Iv7tBVhw5t3+reaTPvZh8q0HU6Npxjz4MuY5Kfk9bfPR"
  "WRX7D3f9b2mH5cQ=";

