/* 
 * Resource generated for file:
 *    1DStraightModelDeleteInterventionButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelDeleteInterventionButton_width          = 20;
static const unsigned int  image_1DStraightModelDeleteInterventionButton_height         = 20;
static const unsigned int  image_1DStraightModelDeleteInterventionButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelDeleteInterventionButton_length         = 112;
static const unsigned long image_1DStraightModelDeleteInterventionButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelDeleteInterventionButton[] = 
  "eNpjYBga4D8Q/QdT1NEHEyfFTEJ6SDGTWLXEqCPVL/jUkxMuuPSRaxY2/ZSahctMaqVLWr"
  "iNWmFHi7ilRdqjRd6gRd4lJVzoVc4NFAAAWASvUQ==";

