/* 
 * Resource generated for file:
 *    1DStraightModelStentButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_1DStraightModelStentButton_width          = 20;
static const unsigned int  image_1DStraightModelStentButton_height         = 20;
static const unsigned int  image_1DStraightModelStentButton_pixel_size     = 4;
static const unsigned long image_1DStraightModelStentButton_length         = 984;
static const unsigned long image_1DStraightModelStentButton_decoded_length = 1600;

static const unsigned char image_1DStraightModelStentButton[] = 
  "eNqNlH1IE3EYx5/hejmpCKOJsfwn+iOE5SBKsdBSV6wgCnoxgrLY5kuo22CG2Jw6yCUmoc"
  "uchkFUzM2wVyOEjKx/ehUChSTrbsu53fAlCcttv565m54veR184Hf3vftwz3O/ewghQJDp"
  "qSD8mgxCKBCaORfi58QY+HzejT6vd8Of6cC8zO/2QLVql+pa2XZna3lekpCr70PPep1ma2"
  "drbcIPW02Cy1KRYfSNuGZz5st3cUuViP72DojVlGET8hmKMstuVAFh30dovASk03HlUDT/"
  "OkSDJpV6fPsEkMochVbIZyzNbnrWhsuhCI9agDx0mnXRvJ+moVhMORgAUrdfmSfkKzinPG"
  "oqBvLxJfICiEUrdr9983Qz36enKKcffSUyWaHdbofl2Jkkk+nXQcCuAHIvHYhBEvvK4/IA"
  "36dD3yT6dgPkw9JHEpIYXsQAyJsAAkG834tUraR63ejg+7QU5Qj70gHyFnjikR3IfcQavr"
  "AKILkRfVN4/zBSQVGvXQt8JRTVHvbtBSjgPBRSg4whhAMVIEPftuV8A5F6OzifJlISdHOO"
  "PuQztx5AUsO+Bp6vEn0L6432bx/AWXzmNNKDpETKg2ykFdnE1Svnv58Rfcw/+pcW6R+2Ee"
  "J4PVyNiKInMZzvN+czUVSva7GvfRSzU1JpvlqthuXYIpXK63m+pd4v/D3CWb1SWSC0n48o"
  "lckWgOnofqlcgfuFYRb1z4OZRaEQ9B3OUsjU8RC4cwDIrSwg2kSq10PPzYN+ZuZ/s7vRV5"
  "sp7CtUKc6Y9Piv9UQw60T+7q622bk0OESLVClU191jQKpPKoqEfKaLCtu8eWDDeeCoNszN"
  "Kzr2plnsZz4BaTJnWoV8mty0Yqtxbl41lEPo+ZPmPdGcpd2i0oOJeuN5yYO6C8flQr4Op3"
  "2tyZDWaKuRDDdflgxev5qTOz4+MZuHgkEYG2ZhhGFhcnT8v+Z9AKcBy7JxfpZdw7/+F4Bb"
  "R1A=";

