#include "vtkKWPushButton.h"  
#include "vtkKWFrameWithLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWComboBox.h"
#include "vtkKWLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkCamera.h"
#include "vtkRenderer.h"
#include "vtkPVSelectionList.h"
#include "vtkIntArray.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkPVWidget.h"
#include "vtkPVWidgetCollection.h"
#include "vtkCallbackCommand.h"
#include "vtkSMSourceProxy.h"
#include "vtkPVRenderView.h"
#include "vtkPVDisplayGUI.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMUtils.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkHMDefineVariables.h"
#include "vtkPVApplication.h"
#include "vtkPVSource.h"
#include "vtkKWLoadSaveDialog.h"

#include "vtkPVHM1DResultsVisFilter.h"

vtkCxxRevisionMacro(vtkPVHM1DResultsVisFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM1DResultsVisFilter);

static void GetNodeCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
	
  vtkPVHM1DResultsVisFilter* pv = reinterpret_cast<vtkPVHM1DResultsVisFilter*>(sr);
	pv->GetStraightModelSelectedNodeId();
		   
}
 
//------------------------------------------------------------------------------
vtkPVHM1DResultsVisFilter::vtkPVHM1DResultsVisFilter()
{ 
	this->SetEnabled(1);
	this->Frame   = vtkKWFrameWithLabel::New();

	this->straightModelCamera = vtkCamera::New();
	this->straightModelCamera->ParallelProjectionOn();
	this->fullModelCamera = vtkCamera::New();

	//this->straightModelCoordinates 	= new double[3];
	this->straightModelCoordinates 	= NULL;
	
	//this->fullModelCoordinates 			= new double[3];
	this->fullModelCoordinates 			= NULL;
//	straightModelCoordinates[0] = 0;
//	straightModelCoordinates[1] = 0;
//	straightModelCoordinates[2] = 1;
//
//	fullModelCoordinates[0] = 0;
//	fullModelCoordinates[1] = 0;
//	fullModelCoordinates[2] = 0;

	this->First1DView				= true;
	this->ObserverAdded			= false;

  this->Input3DCombo = vtkPVSelectionList::New();

	this->SegmentLabel = vtkKWLabel::New();
	this->SegmentEntry = vtkKWEntry::New();
	
	
	this->Segment2Label = vtkKWLabel::New();
	this->Segment2Entry = vtkKWEntry::New();
	  


	this->GroupSelectionCombo = vtkKWComboBox::New();
	this->GroupSelectionLabel = vtkKWLabel::New();
	
  //this->CouplingListBox 		 = vtkKWListBoxWithScrollbars::New();
  this->AddCouplingButton 	 = vtkKWPushButton::New();
  this->ResetPathButton = vtkKWPushButton::New();	
  this->SaveCouplingButton	 = vtkKWPushButton::New();	
  
	this->ErrorMessageLabel 	 =	vtkKWLabel::New(); 
  
  this->ConfigureFilesGenerationButton = vtkKWPushButton::New();	
  
  
  this->OpenResultFileButton = vtkKWPushButton::New();
  
  //this->CoupledModelConfigurationWidget = NULL;
		
	this->CouplingInfoArray = vtkIntArray::New();
	this->CouplingInfoArray->SetNumberOfComponents(4);
  
  
  this->Current3dSelected = 0;
  
  this->Input1DSelectRadioButton = vtkKWRadioButton::New();
  
  this->StraightModelInputNumber = -1;
  this->MeshDataInputNumber = -1;
  
  this->Widget1DProxy = NULL;
  
  this->PVWidget = NULL;
  
  
 	this->ColorCode = NULL;
 	this->PointCounter = 0;
 	
 	this->ConfigButton  = vtkKWLoadSaveButton::New();
 	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{Files} {*.vtk}}");
 	this->ConfigLabel = vtkKWLabel::New();
 	this->PVSTM = NULL; 
 	 
}


//------------------------------------------------------------------------------
vtkPVHM1DResultsVisFilter::~vtkPVHM1DResultsVisFilter()
{                  
	
//	if (straightModelCoordinates)
//		{
//		delete [] straightModelCoordinates;
//		straightModelCoordinates = NULL;
//		}
//	
//	if (fullModelCoordinates)
//		{
//		delete [] fullModelCoordinates;
//		fullModelCoordinates = NULL;	
//		}

	this->Frame->Delete();
	this->straightModelCamera->Delete();
	this->fullModelCamera->Delete();

  this->Input3DCombo->Delete();
  
	this->SegmentLabel->Delete();
	this->SegmentEntry->Delete();
	
	this->Segment2Label->Delete();
	this->Segment2Entry->Delete();
	  


	this->GroupSelectionCombo->Delete();
	this->GroupSelectionLabel->Delete();

 // this->CouplingListBox->Delete();
  this->AddCouplingButton->Delete();
  this->ResetPathButton->Delete();	
  this->SaveCouplingButton->Delete();

	this->ErrorMessageLabel->Delete();
  
  this->ConfigureFilesGenerationButton->Delete(); 
  
  this->OpenResultFileButton->Delete();
  
  
//  if (this->CoupledModelConfigurationWidget)
//  	{
//  	this->CoupledModelConfigurationWidget->Delete(); 
//    this->CoupledModelConfigurationWidget = NULL;
//  	}
  	
	this->CouplingInfoArray->Delete();
	
	this->Input1DSelectRadioButton->Delete();
	
	if (this->Widget1DProxy)
	  {
		this->Widget1DProxy->RemoveAllCouplingActors();
		this->Widget1DProxy->SetStraightModelMode(1); // setando modo de visualizacao
		this->Widget1DProxy->IncrementCouplingVar(0);
	  }
	
	if (this->PVWidget)
		{
		this->PVWidget->RemoveObserver(CallbackCommand);
		this->PVWidget = NULL;
		}
	if (this->ColorCode)
		this->ColorCode->Delete();
	
	
  this->ConfigLabel->Delete();
  this->ConfigButton->Delete();
	
}  

//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::AddStraightModelObserver()
{                  
	// Pega as widgets do input 0 e procura pela widget do Straight Model
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetWidgets(); 
	vtkPVWidget *widget;
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));      
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)   
    	break;
  	}
	
  // Called every time left click happends on the viewport of the StraightModel Widget
  vtkCallbackCommand *LeftButtonCallback = vtkCallbackCommand::New();
  LeftButtonCallback->SetCallback(GetNodeCallback);
  
  CallbackCommand =  LeftButtonCallback;

  // Passes this because the executed method for callback is a vtkPVHMCouplingMethod
  LeftButtonCallback->SetClientData((void *)this);

  // Captures Left clicks on the StraightModelWidget
  if(widget)
  	{
	  PVWidget = widget;
	  widget->AddObserver(vtkCommand::WidgetModifiedEvent, LeftButtonCallback);
  	}
	else
  	vtkErrorMacro(<<"vtkPVHM1DResultsVisFilter::AddStraightModelObserver() : Cannot found StraightModel Widget.");	
		
	// Remove callback   
  LeftButtonCallback->Delete();
  
  // Avoid adding another observer
	this->ObserverAdded	= true;  
}

//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::Create(vtkKWApplication* app)
{

this->pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();
 	
 	this->kwApp = app;
  vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	this->Window = pvApp->GetMainWindow();

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << "already created");
    return;
    }
  
  // Call the superclass to create the whole widget
  this->Superclass::Create(app);  
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  //this->Frame->SetLabelText("Coupling Filter");

//  this->Input1DSelectRadioButton->SetParent(this->Frame->GetFrame());
//  this->Input1DSelectRadioButton->Create(pvApp);
//  this->Input1DSelectRadioButton->SetBalloonHelpString("Toggle to 1D Input Visualization");
//  this->Input1DSelectRadioButton->SetText("1D Input");
//  this->Input1DSelectRadioButton->IndicatorVisibilityOff();
//  this->Input1DSelectRadioButton->SetHighlightThickness(0);
//  //this->Input1DSelectRadioButton->SelectedStateOn();
//  this->Input1DSelectRadioButton->SetCommand(this, "Select1DInputCallback");


//  this->Input3DCombo->SetParent(this->Frame->GetFrame());
//  this->Input3DCombo->SetOptionWidth(15);
//  this->Input3DCombo->Create(pvApp);
//  this->Input3DCombo->AddItem("Select 3D Input", 0);
//  //this->Input3DCombo->SetModifiedCommand(this->GetTclName(), "Select3DInputCallback");
//  this->Input3DCombo->SetBalloonHelpString("Toggle to 3D Input Visualization");

	this->SegmentLabel->SetParent(this->Frame->GetFrame());
	this->SegmentLabel->Create(pvApp);
  this->SegmentLabel->SetText("Proximal Terminal ID: ");	
	this->SegmentEntry->SetParent(this->Frame->GetFrame());
	this->SegmentEntry->Create(pvApp);
  this->SegmentEntry->SetBalloonHelpString("proximal terminal Id - beginning of path"); 	
	this->SegmentEntry->SetValue("None");  
	this->SegmentEntry->SetReadOnly(1); 
	
	
	this->Segment2Label->SetParent(this->Frame->GetFrame());
	this->Segment2Label->Create(pvApp);
	this->Segment2Label->SetText("Distal Terminal ID: ");  
	this->Segment2Entry->SetParent(this->Frame->GetFrame());
	this->Segment2Entry->Create(pvApp);
	this->Segment2Entry->SetBalloonHelpString("proximal terminal Id - beginning of path");  
	this->Segment2Entry->SetValue("None");  
	this->Segment2Entry->SetReadOnly(1); 
	
 	this->ErrorMessageLabel->SetParent(	this->Frame->GetFrame());
  this->ErrorMessageLabel->Create(pvApp);
  this->ErrorMessageLabel->SetText("Step 1: Select a proximal point - terminal only");



//	// Coupling Buttons
  this->AddCouplingButton->SetParent(this->Frame->GetFrame());
  this->AddCouplingButton->Create(pvApp);
  //this->AddCouplingButton->SetWidth(20);
  this->AddCouplingButton->SetHeight(1);
  this->AddCouplingButton->SetText("Enable 1D widget");
  this->AddCouplingButton->SetBalloonHelpString("Click to enable 1D widget: interaction objects - terminals and segments"); 
  this->AddCouplingButton->SetCommand(this, "Select1DInputCallback");
//
  this->ResetPathButton->SetParent(this->Frame->GetFrame());
  this->ResetPathButton->Create(pvApp);
  //this->ResetPathButton->SetWidth(10);
  this->ResetPathButton->SetHeight(1);
  this->ResetPathButton->SetText("Reset Path");
  this->ResetPathButton->SetBalloonHelpString("Resets current path in the 1D tree"); 
  this->ResetPathButton->SetCommand(this, "ResetPath");
  this->ResetPathButton->SetEnabled(0);

//  this->SaveCouplingButton->SetParent(this->Frame->GetFrame());
//  this->SaveCouplingButton->Create(pvApp);
//  //this->SaveCouplingButton->SetWidth(5);
//  this->SaveCouplingButton->SetHeight(1);
//  this->SaveCouplingButton->SetText("Save");
//  //this->SaveCouplingButton->SetCommand(this, "SaveCoupling");  

  this->ConfigureFilesGenerationButton->SetParent(this->Frame->GetFrame());
  this->ConfigureFilesGenerationButton->Create(pvApp);
  //this->ConfigureFilesGenerationButton->SetWidth(5);
  this->ConfigureFilesGenerationButton->SetBalloonHelpString("Generates the visualization structure with simulation results data from current selected path"); 	
  this->ConfigureFilesGenerationButton->SetHeight(1);
  this->ConfigureFilesGenerationButton->SetText("Generate file");
  this->ConfigureFilesGenerationButton->SetCommand(this, "ConfigureSolverFilesGenerationCallback"); 
  this->ConfigureFilesGenerationButton->SetEnabled(0);
  
  
  
  this->OpenResultFileButton->SetParent(this->Frame->GetFrame());
  this->OpenResultFileButton->Create(pvApp);
  //this->ConfigureFilesGenerationButton->SetWidth(5);
  this->OpenResultFileButton->SetBalloonHelpString("Open generated file in a new pipeline item ");  
  this->OpenResultFileButton->SetHeight(1);
  //this->OpenResultFile->SetText("Generate file");
  this->OpenResultFileButton->SetCommand(this, "OpenResultFile"); 
  this->OpenResultFileButton->SetEnabled(1);
  

  // Load Parameteres 
  this->ConfigButton->SetParent(this->Frame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->GetLoadSaveDialog()->ChooseDirectoryOn();
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback"); 
  this->ConfigButton->SetBalloonHelpString("Place in the file system where the output file will be created"); 
  //this->ConfigButton->SetEnabled(0);
  
  

  this->ConfigLabel->SetParent(this->Frame->GetFrame());
  this->ConfigLabel->Create(pvApp);
  this->ConfigLabel->SetText("Destination file path");

}

// -----------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::PlaceComponents()
{
	
cout << "void vtkPVHM1DResultsVisFilter::PlaceComponents()" << endl;

this->Script("pack %s -fill x -expand t -pady 2",  
  this->Frame->GetWidgetName());

//  this->Script("grid %s -sticky news -padx 2 -pady 2",
//  this->Input1DSelectRadioButton->GetWidgetName()); //,
  //this->Input3DCombo->GetWidgetName());	
	
	  this->Script("grid %s %s -sticky news -padx 2 -pady 2",  
	  this->AddCouplingButton->GetWidgetName(),
	  this->ResetPathButton->GetWidgetName());

	

	this->Script("grid %s %s -sticky news -padx 2 -pady 2",
  this->SegmentLabel->GetWidgetName(),
  this->SegmentEntry->GetWidgetName());
	
	
	 this->Script("grid %s %s -sticky news -padx 2 -pady 2",
	  this->Segment2Label->GetWidgetName(),
	  this->Segment2Entry->GetWidgetName());


	
  this->Script("grid %s %s - - -sticky ew",
    this->ConfigLabel->GetWidgetName(), 
    this->ConfigButton->GetWidgetName());    


	this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
	this->ConfigureFilesGenerationButton->GetWidgetName());    

	
	 this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
	 this->ErrorMessageLabel->GetWidgetName());
	
}


// ----------------------------------------------------------------------------

void vtkPVHM1DResultsVisFilter::Select1DInputCallback()
{


  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("OutputSelect"));    
		
	if(prop) 
  	prop->SetElement(0, 0);
  else
  	vtkErrorMacro(<<"Cannot create Group filter property.");	  	

	this->UpdateWidgetInfo();
  
  this->PVSTM->SelectTreeElement(0, 1);
  this->PVSTM->SetWidgetMode(MODE_VISUALIZATION_VAL);
    
  if (this->Widget1DProxy)
    this->Widget1DProxy->SetStraightModelMode(5);
  
  
      
	
	this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();  
  this->GetPVSource()->PreAcceptCallback();
  
  
}	

//------------------------------------------------------------------------------
int vtkPVHM1DResultsVisFilter::GetStraightModelSelectedNodeId()
{
//cout << "int vtkPVHM1DResultsVisFilter::GetStraightModelSelectedNodeId()" << endl;	

// Get widget collection from the right input
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetWidgets();  
 // int segmentId = -1, nodeId = -1;

  // Find the Straight Model Widget from the collection and get the selected element
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    vtkPVWidget *widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));
      
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)
	    {
			vtkPVHMStraightModelWidget *hmWidget = vtkPVHMStraightModelWidget::SafeDownCast(widget);
			
			vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(hmWidget->GetWidgetProxy());
			
			this->Widget1DProxy = wProxy;
			
			
			vtkSMIntVectorProperty* typeOfElementSelect = vtkSMIntVectorProperty::SafeDownCast(
			hmWidget->GetWidgetProxy()->GetProperty("StraightModelObjectSelected"));
			
			if(typeOfElementSelect->GetElement(0) == TERMINAL || typeOfElementSelect->GetElement(0) == HEART) //O elemento é um nó
				{
				if (segmentId == wProxy->GetStraightModelElementSelected())
				  return 0; // entra 2 vezes aqui por causa do onleftdown e onleftup button
				
			//	cout << "Path terminals "<< wProxy->GetPathTerminals()[0]<< " " << wProxy->GetPathTerminals()[1] << endl;
				
				// Pega ID do segmento
			  segmentId = wProxy->GetStraightModelElementSelected();
			  // Pega número do Nó
			  //nodeId		= wProxy->GetStraightModelNodeNumber();
			  
			  
			  //if (!this->SegmentEntry->GetValueAsInt())
			  this->SegmentEntry->SetValueAsInt(wProxy->GetPathTerminals()[0]);
			  
			  
			  this->ErrorMessageLabel->SetText("Step 2: Select a distal point - terminal only");
			    
			   
			  if(wProxy->GetPathTerminals()[0] != wProxy->GetPathTerminals()[1])
			    {
			    this->Segment2Entry->SetValueAsInt(wProxy->GetPathTerminals()[1]);
			    this->ErrorMessageLabel->SetText("Step 3: Select a folder where the file will be generated");
			    this->ResetPathButton->SetEnabled(1);
			    this->ConfigButton->SetBackgroundColor(0, 0.753, 0.43);
			    
			    }
			  else
			    this->Segment2Entry->SetValue("None");
			  
			  this->Script("grid remove %s",this->OpenResultFileButton->GetWidgetName()); 
				
				
			  //this->ErrorMessageLabel->SetText("File generated ok!");
				
			  if (SegmentEntry->GetValueAsInt() && ConfigButton->GetFileName() )
			    {
			    this->ConfigureFilesGenerationButton->SetEnabled(1);
			    this->ConfigureFilesGenerationButton->SetBackgroundColor(0, 0.753, 0.43);  //verde
			    }
				
				
				}
  	  }
	  }  	
  return segmentId;
}

// -----------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());      
}


//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::StraightModelWidgetView()
{
  if (this->StraightModelInputNumber == -1)
  	return;
  
  
  this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->Select();
  
  
  this->GetPVApplication()->GetMainWindow()->SetInteractorStyle(vtkPVWindow::INTERACTOR_STYLE_2D);
  
  
  
  this->GetPVApplication()->GetMainView()->GetRenderer()->SetActiveCamera(this->straightModelCamera);
 		
  
  if (!this->straightModelCoordinates)	
  	this->straightModelCamera->SetPosition(0, 0, 1);
  else
  	this->straightModelCamera->SetPosition(this->straightModelCoordinates);
  
  this->straightModelCoordinates = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition();     
}


//------------------------------------------------------------------------------
int vtkPVHM1DResultsVisFilter::CheckInputType()
{
	bool StraightModelFound = false;
	int returnValue;
	
	// identifica em qual das entradas esta o 1DStraightModel
	// nem sempre a entrada zero é a do STM
	
	int StraightModelInputPort = 0;
	
	for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs(); ++i)
		{
		
		vtkPVSource *CurrentPVSource = this->GetPVSource()->GetPVInput(i);
		if(!strcmp(CurrentPVSource->GetClassName(), "vtkPVDReaderModule"))
			{
			StraightModelInputPort = i;
			// only one StraightModel is allowed here so we can brake the loop
			break;
			}
		}
	
	
	this->StraightModelInputNumber = StraightModelInputPort;
	
	// Pega as widgets do input 0 e procura pela widget do Straight Model
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(StraightModelInputPort)->GetWidgets(); 
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    vtkPVWidget *widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));
    
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)
    	{   
    	if (!this->PVSTM)
    	  this->PVSTM = vtkPVHMStraightModelWidget::SafeDownCast(widget);
    	
    	// seta para vizualizacao de elementos para que os nós possam ser selecionados
    	this->PVSTM->SelectTreeElement(0, 1);
    	
    	
    	this->PVSTM->SetVisibility(1);
    	
    	//PVSTM->SetWidgetMode(5);
    	
    	StraightModelFound = true;
    	
    	vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(PVSTM->GetWidgetProxy());
    	
    	
      wProxy->SetStraightModelMode(5);
      
  		wProxy->GenerateCouplingRepresentation(1);
    	}
  	}
  	

  
	// Cria as propriedades que irão armazenar o valor do input para o modelo 1D e para o 3D
  vtkSMIntVectorProperty *prop1D = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("1DInputNumber"));    		
  
  if (!prop1D) 
	  {
		vtkErrorMacro(<<"Cannot create \"1DInputNumber\" or \"3DInputNumber\" property.");	  	
  	return 0;
	  }

//	// Se na coleção de widgets do input 0 estiver a do modelo 1d, Straight Model está nesse input
//  if(StraightModelFound)
//	  {
//		this->StraightModelInputNumber = 0;
//		//this->MeshDataInputNumber = 1;
		returnValue = 1; // StraightModel no input 0
//	  }
//	else
//		{
//		this->StraightModelInputNumber = 1;
//		//this->MeshDataInputNumber = 0;
//		returnValue = 0; 		
//		}

	// Seta as propriedades correspondentes ao número do input referente ao modelo 1D e ao 3D
	prop1D->SetElement(0,this->StraightModelInputNumber);
	//prop3D->SetElement(0,this->MeshDataInputNumber);	
		
  this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();	
  return returnValue;
	

	
}

//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::Accept()  
{	
	// Atribui numeração de input para o modelo 1D e para o 3D
	this->CheckInputType();

	// Caso não tenha sido adicionado anterior, adicionar observer
	if(!this->ObserverAdded)
		this->AddStraightModelObserver();
	// A idéia é, ao menos na interface, manter o label "Input 1" como sendo o StraightModel
  //if(outputProp->GetElement(0) == 0) 
 	 	//{
  	this->StraightModelWidgetView();
		// Se é a primeira vez que mostra o modelo 1D, centraliza na tela
	  if(this->First1DView)
		  {  	
		  this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetPVOutput()->CenterCamera(); 
		  
		  //this->GetPVSource()->GetPVOutput()->CenterCamera();
		  
		  this->First1DView = false;
		  }	 	 		

  this->GetPVSource()->Select();
	this->UpdateWidgetInfo();
  this->PlaceComponents();
  this->ConfigureComponents();
  
    
}

//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{


}


//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::UpdateWidgetInfo()
{
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	this->GetPVSource()->GetProxy()->UpdateInformation();
}


//------------------------------------------------------------------------------
void vtkPVHM1DResultsVisFilter::ConfigureSolverFilesGenerationCallback()
{



if (!SegmentEntry->GetValueAsInt())
  {
  this->ErrorMessageLabel->SetText("Error: There is no segment selected");
  
  
  vtkErrorMacro("Error: There is no selected segment");
  return;
  
  }
else
  this->ErrorMessageLabel->SetText("");





// Pega propriedade referente ao elemento selecionado
vtkSMIntVectorProperty *outputProp = vtkSMIntVectorProperty::SafeDownCast(
this->GetPVSource()->GetProxy()->GetProperty("BeginTerminal"));

outputProp->SetElement(0, this->Widget1DProxy->GetPathTerminals()[0]);

// Pega propriedade referente ao elemento selecionado 2
vtkSMIntVectorProperty *outputProp2 = vtkSMIntVectorProperty::SafeDownCast(
this->GetPVSource()->GetProxy()->GetProperty("EndTerminal"));

outputProp2->SetElement(0, this->Widget1DProxy->GetPathTerminals()[1]);


if (!ConfigButton->GetFileName())
  {
  this->ErrorMessageLabel->SetText("Error: no destination path specified");
  
  
  vtkErrorMacro("Error: no destination path specified!");
  return;
  
  }
else
  this->ErrorMessageLabel->SetText("");
//PVSTM->SelectTreeElement(0, 1);
    
//Widget1DProxy->SetStraightModelMode(5);
    



vtkSMStringVectorProperty *prop3 = vtkSMStringVectorProperty::SafeDownCast(
this->GetPVSource()->GetProxy()->GetProperty("GenerateFile"));    
if (prop3) 
  prop3->SetElement(0,ConfigButton->GetFileName()); 
else
  {
  vtkErrorMacro(<<"Cannot acess GenerateSolverFiles filter property.");     
  return;
  }   
this->ModifiedCallback(); 
this->GetPVSource()->GetProxy()->UpdateVTKObjects();  


//this->ConfigureFilesGenerationButton->SetText("Generate file .... done");

this->ConfigureFilesGenerationButton->SetBackgroundColor(0.752941, 0.752941, 0.752941); //cinza


//status;
vtkSMIntVectorProperty *GenerationStatus = vtkSMIntVectorProperty::SafeDownCast(
this->GetPVSource()->GetProxy()->GetProperty("GenerationStatus"));



if(GenerationStatus)
  {
  // atualiza somente a prop. "GenerationStatus"
  this->GetPVSource()->GetProxy()->UpdatePropertyInformation(GenerationStatus);
  status = GenerationStatus->GetElement(0); // se geracao ok - variavel error = 0 
  }
else
vtkErrorMacro(<<"Cannot create \"GenerationStatus\" filter property."); 

char temp[256];

sprintf(temp, "File segments_%d_%d.vtk \ngenerated successfully", this->Widget1DProxy->GetPathTerminals()[0], this->Widget1DProxy->GetPathTerminals()[1] );

if (status)
  {
  this->ErrorMessageLabel->SetText(temp);
  
  sprintf(temp, "Open segments_%d_%d.vtk", this->Widget1DProxy->GetPathTerminals()[0], this->Widget1DProxy->GetPathTerminals()[1]);
  
  this->OpenResultFileButton->SetText(temp);
  
  this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
  this->OpenResultFileButton->GetWidgetName());   
  
  
  }
else
  {
  this->Script("grid remove %s",this->OpenResultFileButton->GetWidgetName()); 
  this->ErrorMessageLabel->SetText("Error: 1D model has no simulation results.");
  }
}
//----------------------------------------------------------------------------


void vtkPVHM1DResultsVisFilter::ConfigButtonCallback()
{

if (SegmentEntry->GetValueAsInt() && ConfigButton->GetFileName() )
  {
  this->ConfigureFilesGenerationButton->SetBackgroundColor(0, 0.753, 0.43);  //verde
  //this->ConfigureFilesGenerationButton->SetForegroundColor(0, 0.753, 0.43);  //verde
  
  this->ConfigureFilesGenerationButton->SetEnabled(1);
  
  }

this->ConfigButton->SetBackgroundColor(0.752941, 0.752941, 0.752941); //cinza
this->ErrorMessageLabel->SetText("Step 4: Click in 'Generate File'");

}


void  vtkPVHM1DResultsVisFilter::OpenResultFile()
{

char path[256];

sprintf(path, "%s/segments_%d_%d.vtk",ConfigButton->GetFileName(), this->Widget1DProxy->GetPathTerminals()[0], this->Widget1DProxy->GetPathTerminals()[1]);

this->GetPVSource()->GetPVWindow()->Open(path);
}




void  vtkPVHM1DResultsVisFilter::ResetPath()
{
  Widget1DProxy->ResetPathTree();
  
  this->SegmentEntry->SetValue("None");
  this->Segment2Entry->SetValue("None");
  Widget1DProxy->GenerateCouplingRepresentation(1);
  this->ErrorMessageLabel->SetText("Step 1: Select a proximal point - terminal only");
  this->ConfigButton->SetBackgroundColor(0.752941, 0.752941, 0.752941); //cinza
  ConfigureFilesGenerationButton->SetBackgroundColor(0.752941, 0.752941, 0.752941); //cinza
  this->ConfigureFilesGenerationButton->SetEnabled(0);
  this->ResetPathButton->SetEnabled(0);
  
}


