#ifndef _vtkPVHM1DSolverFilesConfigurationWidget_h_
#define _vtkPVHM1DSolverFilesConfigurationWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWApplication.h"
#include "vtkKWWindow.h"
#include "vtkPVApplication.h"
#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWPushButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkKWUserInterfaceManager.h"
#include "vtkKWEntry.h"
#include "vtkKWComboBox.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWThumbWheel.h"
#include <vtksys/SystemTools.hxx>
#include <vtksys/CommandLineArguments.hxx>
#include "vtkKWFrameWithLabel.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkKWTopLevel.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWComboBoxWithLabel.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWUserInterfacePanel.h"
#include "vtkKWTreeWithScrollbars.h"
#include "vtkKWTree.h"
#include "vtkPVWindow.h"
#include "vtkPVSource.h"

#include "vtkKWText.h"
#include "vtkKWTextWithScrollbars.h"
#include "vtkKWSeparator.h"



class VTK_EXPORT vtkPVHM1DSolverFilesConfigurationWidget : public vtkKWTopLevel
{
public:
  static vtkPVHM1DSolverFilesConfigurationWidget *New();
  vtkTypeRevisionMacro(vtkPVHM1DSolverFilesConfigurationWidget,vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent){};
  

  virtual void Create(vtkKWApplication *app);
  
  // Description: 
  // Get method
  int GetRenumbering();
  
  // Description: 
  // Get method
  int GetFileOutputControl();
  
  // Description: 
  // Get method
  int GetScreenOutputControl();	
  
  // Description: 
  // Get method
  int GetResumeIniFile();
  
  // Description: 
  // Get method
  int GetIterations();
  
  // Description: 
  // Get method
  int GetFormulation();
  
  // Description: 
  // Get method
  int GetAditivity();
  
  // Description: 
  // Get method
  int GetArteryWallLaw();
  
  // Description: 
  // Get method
  double GetTimeStep();
  
  // Description: 
  // Get method
  double GetInitialTime();
  
  
  // Description: 
  // Get method
  double GetFinalTime();
  
  void SetInitialTime(double result);
  
  // Description: 
  // Get method
  double GetReferenceFlux();
  
  // Description: 
  // Get method
  double GetConvergenceFlux();
  
  // Description: 
  // Get method
  double GetElasticPressure();
  
  // Description: 
  // Get method
  double GetConvergenceElasticPressure();
  
  // Description: 
  // Get method
  double GetArea();
  
  // Description: 
  // Get method
  double GetConvergenceArea();
  
  // Description: 
  // Get method
  double GetPressure();
  
  // Description: 
  // Get method
  double GetConvergencePressure();
  
  // Description: 
  // Get method
  double GetSubRelaxationParameter();
  
  // Description: 
  // Get method
  double GetPenalization();
  
  // Description: 
  // Get method
  double GetDensity();
  
  // Description: 
  // Get method
  double GetViscosity();
  
  // Description: 
  // Get method
  double GetParameterTheta();
  
  // Description: 
  // Get method
  double GetVelocityProfile();
	
	// Description: 
  // sets an array size 11 with BasParam interger parameters
	void SetIntParameters();
	
	
  // Description: 
  // sets an array size 18 with BasParam double parameters
 	void SetDoubleParameters();
 	
 	// Description: 
  // Get method for double parameters
  vtkDoubleArray *GetDoubleParametersArray();
 	
 	// Description: 
  // Get method for integer parameters
  vtkIntArray *GetIntParametersArray();
    
  
  // Description: 
  // set widget proxy from PVStraightModel
  void SetWidgetProxy(vtkSMHMStraightModelWidgetProxy *proxy);
  
  
  // Description: 
  // sets initial time with  inifile's current time 
  void GetIniFileCurrentTime();

  
  // Description: 
  // triggers solver files write process
  void ConfigureValuesFromBasParamWindow();
  
  // Description: 
  // search in the StraightModel tree any segment with null viscoelasticity
  // return 1 if any is found 
  int SeachNullViscoELasticElements();
  
  // Description: 
  // calculate time parameters such as time step and final time.
  void CalculateTimeParameters();
  
  // Description: 
  // Set/Get methods for OriginalFinalTime
  vtkSetMacro(OriginalFinalTime,double);
  vtkGetMacro(OriginalFinalTime,double);
  
    
 	// Description:
 	// set the model type (1D, 3D or coupled model)
	void SetModelType(const char *ModelName);
	
 	// Description:
 	//	
	void UpdateResolutionComponents();
 	
 	// Description:
 	//	
	void UpdateLinearSystemLib();
	
 	// Description:
 	//	
	void UpdateCassonRelatedWidgets();
	
 	// Description:
 	//	
	void UpdateNewtonRelatedWidgets();
	
 	// Description:
 	//
	void EnableInitialTimeStatus();
	
	
 	// Description:
 	//
	int GetResolutionSolverType();
 	
 	// Description:
 	//	
	void SetGenerationFilesPath();
	
	
 	// Description:
 	//	
	const char *GetSolverFilesPath();
	
 	// Description:
 	//  
	vtkPVSource *GetPVSource();	

 	// Description:
 	//
	void SetPVSource(vtkPVSource *PVSource);
	
	
 	// Description:
 	//
	void SetPvWindow(	vtkPVWindow *PVWindow);

  
  // Description: 
  // return the path where solver input files were have been generated
  char *GetChoosedPath();

 	// Description:
 	//	Set the value of Final Time entry component
	void SetFinalTime(double time);
	
	// Description:
 	//	Callback method called when the solver type combo box is changed
	void SolverTypeCallBack();
	
	// Description:
 	// return solver type 0- sequential 1: Parallel
 	int GetSolverType();

	// Description:
 	// 0: None 1: BJAcobi
	int GetParallelPreConditioning();
	
	
	void ParallelNumberOfProcessorsCallBack();
	
	
	vtkGetObjectMacro(entryCardiacCycleTime, vtkKWEntry);
	
	const char *GetLogFile();
	
	// Copy files mesh to mesh000, ...
	int CopyFiles();

  // usedb by vtkPVStraightModelWidget to acess CommitChangesButtonPage1 button
	vtkGetObjectMacro(CommitChangesButtonPage1, vtkKWPushButton);
	
	vtkIntArray *GetWhichFile();
	
//***************************************************************************************************** 
//***************************************************************************************************** 
//***************************************************************************************************** 

protected:
  vtkPVHM1DSolverFilesConfigurationWidget();
  virtual ~vtkPVHM1DSolverFilesConfigurationWidget();

  vtkPVApplication* GetPVApplication();
  vtkPVWindow* GetPVWindow();
  vtkKWEntry *entryReFl;
  
  
 
  vtkKWNotebook *notebook;

  vtkKWWidget *mc;
  vtkKWWidget *gc;
  vtkKWWidget *sc;
  vtkKWWidget *mi;
  
  // page 1  Model Config*********************
  // ****************************************************************************************************************
  // ****************************************************************************************************************
  
  vtkKWFrameWithLabel *MCFrame;

  vtkKWLabel *labelForm;
  vtkKWRadioButton *radioLSV;
  vtkKWRadioButton *radioLS;
  
  vtkKWLabel *labelDensity;
  vtkKWEntry *entryDensity;
  
  vtkKWLabel *labelViscosity;
  vtkKWEntry *entryViscosity;
  
  vtkKWLabel *labelArterialWallLaw;
  vtkKWComboBox *comboArterialWallLaw;	
  
  vtkKWLabel *labelVelocity;
  vtkKWThumbWheel *thumbVel;
  
  vtkKWLabel *labelModel;
  vtkKWRadioButton *radioCompliant;
  vtkKWRadioButton *radioRigid;

  // blood constitutive law
  vtkKWLabel *labelBloodLaw;
  vtkKWRadioButton *radioBloodLawNewton;
  vtkKWRadioButton *radioBloodLawCasson;
  
  //Asymptotic viscosity
  vtkKWLabel *labelAVisco;
  vtkKWEntry *entryAVisco;
  
  //Limit Stress
  vtkKWLabel *labelLimitStress;
  vtkKWEntry *entryLimitStress;
  
  //Regulation Parameter
  vtkKWLabel *labelRegularParameter;
  vtkKWComboBox *comboRegularParameter;
  
    
 // ****************************************************************************************************************
 // ****************************************************************************************************************
 // ******** page 2  General Config
  
  vtkKWFrameWithLabel *SolverTimeConfigFrame;
  vtkKWFrameWithLabel *ConvergenceParametersFrame;
  
  // Time Configuration frame 
  vtkKWLabel *ResumeLabel;
  vtkKWRadioButton *radioResumeYes;
  vtkKWRadioButton *radioResumeNo;

  vtkKWLabel *labelNumberOfTimeSteps;
  vtkKWEntry *entryNumberOfTimeSteps;

  vtkKWLabel *labelNumberOfCardiacCycles;
  vtkKWEntry *entryNumberOfCardiacCycles;


  vtkKWLabel *labelTimeStep;
  vtkKWEntry *entryTimeStep;
  vtkKWLabel *labelInitialTime;
  vtkKWEntry *entryInitialTime;
  vtkKWLabel *labelFinalTime;
  vtkKWEntry *entryFinalTime;
  
  // converegence parameters frame
  vtkKWLabel *labelVariable;
  vtkKWLabel *labelFlux;
  vtkKWLabel *labelElPre;
  vtkKWLabel *labelArea;
  vtkKWLabel *labelPress;
  vtkKWLabel *labelRefVal;
  vtkKWLabel *labelConEr;
  vtkKWEntry *entryCeFl;
  vtkKWEntry *entryReEP;
  vtkKWEntry *entryCeEP;
  vtkKWEntry *entryReAr;
  vtkKWEntry *entryCeAr;
  vtkKWEntry *entryRePr;
  vtkKWEntry *entryCePr;
  vtkKWLabel *labelIter;
  vtkKWEntry *entryIter;
  vtkKWLabel *labelSuPar;
  vtkKWScaleWithEntry *scaleSuPar;
  
  
  //************************************************************************************
  // *********** page 3 Solver Config
  
  vtkKWLabel *labelRenu;
  vtkKWEntry *entryRenu;
  vtkKWLabel *labelRLS;
  vtkKWLabel *labelRLSHelp;
  vtkKWComboBox *comboRLS;
  
  vtkKWLabel *labelTethaScheme;
  vtkKWThumbWheel *thumbTheta;

  
  vtkKWLabel *labelAditi;
  vtkKWLabel *labelPena;
  vtkKWEntry *entryPena;
  vtkKWRadioButton *radioYes;
  vtkKWRadioButton *radioNo;

  vtkKWLabel *labelSolverType;
  vtkKWLabel *labelConvergenceError;
  vtkKWLabel *labelDispTolerance;
  vtkKWLabel *labelFParam;
	vtkKWLabel *labelIncrementalVersion;

  vtkKWLabel *labelIterationsBeforeRestart;
  vtkKWLabel *labelkrylov;

  vtkKWEntry *entryIterationsBeforeRestart;
  vtkKWEntry *entrykrylov;

	vtkKWComboBox *comboSolverType;
	vtkKWEntry *entryConvergenceError;
	vtkKWFrameWithLabel *frameIterativeSolver;
	vtkKWFrameWithLabel *framePreconditioning;
	vtkKWEntry *entryDispTolerance;
	vtkKWEntry *entryFParam;
	vtkKWRadioButton *radioICYes;
	vtkKWRadioButton *radioICNo;
	
  //************************************************************************************
  // *********** page 4 Miscelaneuous
  vtkKWLabel *labelFileOutput;
  vtkKWEntry *entryFileOutput;
  vtkKWLabel *labelScreenOutput;
  vtkKWEntry *entryScreenOutput;
  vtkKWLabel *labelLogFile;
  vtkKWEntry *entryLogFile;
  
  vtkKWLabel *labelFilesPath;
  vtkKWEntry *entryFilesPath;
  vtkKWPushButton *pushButtonBrowse;
  vtkKWLabel *labelFilesGenerationStatus;
  vtkKWTextWithScrollbars *textFilesGenerationStatus;
  
  vtkKWRadioButtonSet *SelectedSolverFilesRadioButtonSet;
  vtkKWLabel *SelectedSolverFilesLabel;
  
  vtkKWPushButton *CommitChangesButtonPage1;
  
  
  
 
  //************************************************************************************


 	// Description:
 	//	reference to 1D Widget Proxy
  vtkSMHMStraightModelWidgetProxy *proxy;
  
  
  vtkIntArray *IntParam;
  vtkDoubleArray *DoubleParam;
  vtkKWFrame     *WindowFrame;

  // Descrition:
	// Dialog used by the user to choose a directory where Solver input files will be generated
  vtkKWLoadSaveDialog *ChooseDirectoryDialog;
  
  // quando esta janela for iniciada, o valor dessa variável será
  // igual ao tempo de um periodo da curva do coração (se não existir curva o valor será 0 )
  double OriginalFinalTime;
  
  vtkKWPushButton *CalculateTimeStepButton;
  vtkKWLabel *labelCardiacCycleTime;
  vtkKWEntry *entryCardiacCycleTime;
  
  // variavel que define o caso em que os arquivos estao sendo configurados
  char ModelType[20]; 
  
  
  vtkKWFrameWithScrollbar *FrameScroll; 

	vtkPVWindow *PVWindow;
  
  vtkPVSource *PVSource;
  
  
  	// Description: 
	// Opcoes do Solver Paralelo no modo monolitico
	vtkKWFrameWithLabel *ParallelIterativeSolverFrame;
	
	vtkKWLabel *ParallelNumberOfProcessorsLabel;
	vtkKWEntry *ParallelNumberOfProcessorsEntry;
  
  vtkKWLabel *ParallelIterationsRestartLabel;
  vtkKWEntry *ParallelIterationsRestartEntry;
  
  vtkKWLabel *ParallelRelativeErrorLabel;
	vtkKWEntry *ParallelRelativeErrorEntry;
  
  vtkKWLabel *ParallelAbsErrorLabel;
  vtkKWEntry *ParallelAbsErrorEntry;
  
  vtkKWLabel *ParallelMaxNumberIterLabel;
  vtkKWEntry *ParallelMaxNumberIterEntry;
  vtkKWLabel *ParallelPrecondLabel;
  vtkKWComboBox *ParallelPrecondCombo;
  //--------------------------------------
  
  char CurrentSolverType[20];
  
  
  vtkIntArray *WhichFile;
  
  
private:  
  vtkPVHM1DSolverFilesConfigurationWidget(const vtkPVHM1DSolverFilesConfigurationWidget&); // Not implemented
  void operator=(const vtkPVHM1DSolverFilesConfigurationWidget&); // Not implemented
}; 

#endif  /*_vtkPVHM1DSolverFilesConfigurationWidget_h_*/
