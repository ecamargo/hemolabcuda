/*
 * vtkPVHM3DCenterlines.cxx
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */

#include "vtkPVHM3DCenterlines.h"
#include "vtkSMHM3DCenterlinesProxy.h"

#include "vtkPVApplication.h"
#include "vtkPVWindow.h"
#include "vtkPVSource.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWMessageDialog.h"

#include "vtkPVProcessModule.h"
#include "vtkSMSourceProxy.h"

#include "vtkKWLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWListBoxWithScrollbars.h"
#include "vtkKWListBox.h"
#include "vtkKWPushButton.h"
#include "vtkKWCheckButton.h"
#include "vtkKWThumbWheel.h"

#include "vtkSMIntVectorProperty.h"

#include "vtkPVWidgetCollection.h"
#include "vtkPV3DWidget.h"
#include "vtkSM3DWidgetProxy.h"

vtkCxxRevisionMacro(vtkPVHM3DCenterlines, "$Rev$");
vtkStandardNewMacro(vtkPVHM3DCenterlines);

vtkPVHM3DCenterlines::vtkPVHM3DCenterlines()
{
  this->PointIDLabel = vtkKWLabel::New();
  this->PointIDEntry = vtkKWEntry::New();

  this->SourceListBoxLabel = vtkKWLabel::New();
  this->TargetListBoxLabel = vtkKWLabel::New();

  this->SourceSeedsListBox = vtkKWListBoxWithScrollbars::New();
  this->TargetSeedsListBox = vtkKWListBoxWithScrollbars::New();

  this->AddSourceSeedButton = vtkKWPushButton::New();
  this->AddTargetSeedButton = vtkKWPushButton::New();

  this->RemoveSourceSeedButton = vtkKWPushButton::New();
  this->RemoveTargetSeedButton = vtkKWPushButton::New();

  this->ViewModeCheckButton = vtkKWCheckButton::New();

  this->ResamplingStepLengthThumbWheel = vtkKWThumbWheel::New();

  this->SetWidgetProxyXMLName("HM3DCenterlinesWidget");
}

//----------------------------------------------------------------------------
vtkPVHM3DCenterlines::~vtkPVHM3DCenterlines()
{
  this->PointIDLabel->Delete();
  this->PointIDEntry->Delete();

  this->SourceListBoxLabel->Delete();
  this->TargetListBoxLabel->Delete();

  this->SourceSeedsListBox->Delete();
  this->TargetSeedsListBox->Delete();

  this->AddSourceSeedButton->Delete();
  this->AddTargetSeedButton->Delete();

  this->RemoveSourceSeedButton->Delete();
  this->RemoveTargetSeedButton->Delete();

  this->ViewModeCheckButton->Delete();

  this->ResamplingStepLengthThumbWheel->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::Create(vtkKWApplication* App)
{
  this->Superclass::Create(App);
  vtkPVApplication *pvApp = vtkPVApplication::SafeDownCast(App);
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ChildCreate(vtkPVApplication* pvApp)
{
  if ((this->GetTraceHelper()->GetObjectNameState() ==
       vtkPVTraceHelper::ObjectNameStateUninitialized ||
      this->GetTraceHelper()->GetObjectNameState() ==
       vtkPVTraceHelper::ObjectNameStateDefault) )
    {
    this->GetTraceHelper()->SetObjectName("HM3DCenterlinesWidget");
    this->GetTraceHelper()->SetObjectNameState(
      vtkPVTraceHelper::ObjectNameStateSelfInitialized);
    }

  this->SetFrameLabel("HM 3D Centerlines Seeds");

  this->PointIDLabel->SetParent(this->Frame);
  this->PointIDLabel->Create(pvApp);
  this->PointIDLabel->SetText("Point ID");

  this->PointIDEntry->SetParent(this->Frame);
  this->PointIDEntry->Create(pvApp);
  this->PointIDEntry->SetWidth(8);

  this->SourceListBoxLabel->SetParent(this->Frame);
  this->SourceListBoxLabel->Create(pvApp);
  this->SourceListBoxLabel->SetText("Source Seeds");

  this->SourceSeedsListBox->SetParent(this->Frame);
  this->SourceSeedsListBox->Create(pvApp);
  this->SourceSeedsListBox->GetWidget()->SetSelectionModeToSingle();
  this->SourceSeedsListBox->GetWidget()->SetWidth(35);
  this->SourceSeedsListBox->GetWidget()->SetHeight(5);
  this->SourceSeedsListBox->GetWidget()->ExportSelectionOn();
  this->SourceSeedsListBox->GetWidget()->SetSingleClickCommand(this, "ListBoxSourceItemSelected");

  this->AddSourceSeedButton->SetParent(this->Frame);
  this->AddSourceSeedButton->Create(pvApp);
  this->AddSourceSeedButton->SetBackgroundColor(1, 0, 0);
  this->AddSourceSeedButton->SetWidth(1);
  this->AddSourceSeedButton->SetHeight(1);
  this->AddSourceSeedButton->SetText("Add");
  this->AddSourceSeedButton->SetCommand(this, "AddSourceSeedCallback");

  this->RemoveSourceSeedButton->SetParent(this->Frame);
  this->RemoveSourceSeedButton->Create(pvApp);
  this->RemoveSourceSeedButton->SetWidth(6);
  this->RemoveSourceSeedButton->SetHeight(1);
  this->RemoveSourceSeedButton->SetText("Remove");
  this->RemoveSourceSeedButton->SetCommand(this, "RemoveSourceSeedCallback");



  //Target
  this->TargetListBoxLabel->SetParent(this->Frame);
  this->TargetListBoxLabel->Create(pvApp);
  this->TargetListBoxLabel->SetText("Target Seeds");

  this->TargetSeedsListBox->SetParent(this->Frame);
  this->TargetSeedsListBox->Create(pvApp);
  this->TargetSeedsListBox->GetWidget()->SetSelectionModeToSingle();
  this->TargetSeedsListBox->GetWidget()->SetWidth(35);
  this->TargetSeedsListBox->GetWidget()->SetHeight(5);
  this->TargetSeedsListBox->GetWidget()->SetSingleClickCommand(this, "ListBoxTargetItemSelected");

  this->AddTargetSeedButton->SetParent(this->Frame);
  this->AddTargetSeedButton->Create(pvApp);
  this->AddTargetSeedButton->SetBackgroundColor(0, 0, 1);
  this->AddTargetSeedButton->SetWidth(1);
  this->AddTargetSeedButton->SetHeight(1);
  this->AddTargetSeedButton->SetText("Add");
  this->AddTargetSeedButton->SetCommand(this, "AddTargetSeedCallback");

  this->RemoveTargetSeedButton->SetParent(this->Frame);
  this->RemoveTargetSeedButton->Create(pvApp);
  this->RemoveTargetSeedButton->SetWidth(6);
  this->RemoveTargetSeedButton->SetHeight(1);
  this->RemoveTargetSeedButton->SetText("Remove");
  this->RemoveTargetSeedButton->SetCommand(this, "RemoveTargetSeedCallback");

  this->ViewModeCheckButton->SetParent(this->Frame);
  this->ViewModeCheckButton->Create(pvApp);
  this->ViewModeCheckButton->SetText("ViewMode");
  this->ViewModeCheckButton->SetCommand(this, "ViewModeCallback");

  this->ResamplingStepLengthThumbWheel->SetParent(this->Frame);
  this->ResamplingStepLengthThumbWheel->PopupModeOn();
  this->ResamplingStepLengthThumbWheel->Create(pvApp);
  this->ResamplingStepLengthThumbWheel->ClampMaximumValueOn();
  this->ResamplingStepLengthThumbWheel->ClampMinimumValueOn();
  this->ResamplingStepLengthThumbWheel->SetRange(0.01, 1.0);
  this->ResamplingStepLengthThumbWheel->SetResolution(0.01);
  this->ResamplingStepLengthThumbWheel->SetValue(0.5);
  this->ResamplingStepLengthThumbWheel->DisplayEntryOn();
  this->ResamplingStepLengthThumbWheel->DisplayLabelOn();
  this->ResamplingStepLengthThumbWheel->GetLabel()->SetText("Resampling Step Length");
  this->ResamplingStepLengthThumbWheel->SetCommand(this, "ResamplingStepLengthCallback");

  this->PlaceComponents();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",
    this->Frame->GetWidgetName());

  this->Script("grid %s -sticky ew",
      this->ResamplingStepLengthThumbWheel->GetWidgetName());

  this->Script("grid %s %s -sticky ew -pady 15",
      this->PointIDLabel->GetWidgetName(),
      this->PointIDEntry->GetWidgetName());

  this->Script("grid %s -sticky ew",
      this->SourceListBoxLabel->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->SourceSeedsListBox->GetWidgetName());

  this->Script("grid %s %s - - -sticky ew -columnspan 2 -pady 5",
    this->AddSourceSeedButton->GetWidgetName(),
    this->RemoveSourceSeedButton->GetWidgetName());

  this->Script("grid %s -sticky ew",
      this->TargetListBoxLabel->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->TargetSeedsListBox->GetWidgetName());

  this->Script("grid %s %s - - -sticky ew -columnspan 2 -pady 5",
    this->AddTargetSeedButton->GetWidgetName(),
    this->RemoveTargetSeedButton->GetWidgetName());

  this->Script("grid %s -sticky ew",
      this->ViewModeCheckButton->GetWidgetName());
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::SetValueChanged()
{
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::Initialize()
{
  vtkSMSourceProxy *sProxy = this->GetPVSource()->GetProxy();
  vtkSMHM3DCenterlinesProxy *wProxy = vtkSMHM3DCenterlinesProxy::SafeDownCast(this->WidgetProxy);

  wProxy->SetSourceProxy(sProxy);

}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::SaveInBatchScript(ofstream *file)
{
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::Accept()
{
  this->GetPVSource()->GetPVApplication()->GetProcessModule()->SendPrepareProgress();

  vtkSMHM3DCenterlinesProxy *wProxy = vtkSMHM3DCenterlinesProxy::SafeDownCast(this->WidgetProxy);



  int n = this->SourceSeedsListBox->GetWidget()->GetNumberOfItems();

  if ( n <= 0 )
    {
    vtkKWMessageDialog::PopupMessage(
              this->GetApplication(), this->GetParentWindow(), "Warning",
              "Set source seeds",
              vtkKWMessageDialog::WarningIcon);

    return;
    }

  vtkIdList *sourceSeeds = vtkIdList::New();
  vtkIdList *targetSeeds = vtkIdList::New();

  int value;

  for ( int i=0; i<n; i++ )
    {
    value = atoi(this->SourceSeedsListBox->GetWidget()->GetItem(i));
    sourceSeeds->InsertNextId(value);
    }


  n = this->TargetSeedsListBox->GetWidget()->GetNumberOfItems();

  if ( n <= 0 )
    {
    vtkKWMessageDialog::PopupMessage(
              this->GetApplication(), this->GetParentWindow(), "Warning",
              "Set target seeds",
              vtkKWMessageDialog::WarningIcon);

    return;
    }

  for ( int i=0; i<n; i++ )
    {
    value = atoi(this->TargetSeedsListBox->GetWidget()->GetItem(i));
    targetSeeds->InsertNextId(value);
    }

  wProxy->SetSeedLists(sourceSeeds, targetSeeds);

  double resampling = this->ResamplingStepLengthThumbWheel->GetValue();

  wProxy->SetResamplingStepLength(resampling);

  this->Superclass::Accept();

  this->GetPVSource()->GetPVApplication()->GetProcessModule()->SendCleanupPendingProgress();

  //Passar para modo de visualização do resultado.
  int viewMode;

  vtkSMIntVectorProperty* smViewMode = vtkSMIntVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("ViewMode"));

  smViewMode->SetElement(0, 1);
  this->ViewModeCheckButton->SelectedStateOn();

  sourceSeeds->Delete();
  targetSeeds->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
  vtkSMIntVectorProperty* smWidgetSelect = vtkSMIntVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("PointId"));

  int pointID = smWidgetSelect->GetElement(0);

  this->PointIDEntry->SetValueAsInt(pointID);


  int state;

  vtkSMIntVectorProperty* smState = vtkSMIntVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("WidgetState"));

  state = smState->GetElement(0);

  int index;
  //desmarcar a opção que estiver selecionada em source seeds.
  index = this->SourceSeedsListBox->GetWidget()->GetSelectionIndex();
  this->SourceSeedsListBox->GetWidget()->SetSelectState(index, 0);

  //desmarcar a opção que estiver selecionada em target seeds.
  index = this->TargetSeedsListBox->GetWidget()->GetSelectionIndex();
  this->TargetSeedsListBox->GetWidget()->SetSelectState(index, 0);

  // se state for igual a SourceSelected
  if ( state == 3 )
    {
    char str[50];
    sprintf(str, "%d", pointID);

    //marcar a opção selecionada.
    index = this->SourceSeedsListBox->GetWidget()->GetItemIndex(str);
    this->SourceSeedsListBox->GetWidget()->SetSelectionIndex(index);
    }
  // se state for igual a TargetSelected
  else if ( state == 4 )
    {
    char str[50];
    sprintf(str, "%d", pointID);

    //marcar a opção selecionada.
    index = this->TargetSeedsListBox->GetWidget()->GetItemIndex(str);
    this->TargetSeedsListBox->GetWidget()->SetSelectionIndex(index);
    }
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::AddSourceSeed(int newSeed)
{
  if ( newSeed < 0 )
    {
    vtkErrorMacro(<<"Seed is not valid: " << newSeed);
    return;
    }

  double n = this->SourceSeedsListBox->GetWidget()->GetNumberOfItems();
  int seed;

  for ( int i=0; i<n; i++ )
    {
    seed = atoi(this->SourceSeedsListBox->GetWidget()->GetItem(i));

    if ( seed == newSeed )
      {
      vtkErrorMacro(<<"Seed is added in source list: " << newSeed);
      return;
      }
    }

  char temp[50];
  sprintf(temp, "%d", newSeed);
  this->SourceSeedsListBox->GetWidget()->InsertEntry(n, temp);

  vtkSMIntVectorProperty* smWidgetSelect = vtkSMIntVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("PointId"));

  smWidgetSelect->SetElement(0, newSeed);

  vtkSMHM3DCenterlinesProxy::SafeDownCast(this->WidgetProxy)->SendSourceSeed();

  this->Modified();
  this->SetValueChanged();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::AddTargetSeed(int newSeed)
{
  if ( newSeed < 0 )
    {
    vtkErrorMacro(<<"Seed is not valid: " << newSeed);
    return;
    }

  double n = this->TargetSeedsListBox->GetWidget()->GetNumberOfItems();
  int seed;

  for ( int i=0; i<n; i++ )
    {
    seed = atoi(this->TargetSeedsListBox->GetWidget()->GetItem(i));

    if ( seed == newSeed )
      {
      vtkErrorMacro(<<"Seed is added in target list: " << newSeed);
      return;
      }
    }

  char temp[50];
  sprintf(temp, "%d", newSeed);
  this->TargetSeedsListBox->GetWidget()->InsertEntry(n, temp);

  vtkSMIntVectorProperty* smWidgetSelect = vtkSMIntVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("PointId"));

  smWidgetSelect->SetElement(0, newSeed);

  vtkSMHM3DCenterlinesProxy::SafeDownCast(this->WidgetProxy)->SendTargetSeed();

  this->Modified();
  this->SetValueChanged();
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::AddSourceSeedCallback()
{
  //Adding seed from interface
  this->AddSourceSeed(this->PointIDEntry->GetValueAsInt());
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::AddTargetSeedCallback()
{
  //Adding seed from interface
  this->AddTargetSeed(this->PointIDEntry->GetValueAsInt());
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::RemoveSourceSeedCallback()
{
  int Selected = this->SourceSeedsListBox->GetWidget()->GetSelectionIndex();

  if( (Selected >= 0) && (Selected <this->SourceSeedsListBox->GetWidget()->GetNumberOfItems()))
    {
    //Removing seed from 3DWidget.
    this->RemoveSourceSeedFromWidget(Selected);

    this->SourceSeedsListBox->GetWidget()->DeleteRange(Selected, Selected);

    this->Modified();
    this->SetValueChanged();
    }
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::RemoveTargetSeedCallback()
{
  int Selected = this->TargetSeedsListBox->GetWidget()->GetSelectionIndex();

  if( (Selected >= 0) && (Selected <this->TargetSeedsListBox->GetWidget()->GetNumberOfItems()))
    {
    //Removing seed from 3DWidget.
    this->RemoveTargetSeedFromWidget(Selected);

    this->TargetSeedsListBox->GetWidget()->DeleteRange(Selected, Selected);

    this->Modified();
    this->SetValueChanged();
    }
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::RemoveSourceSeedFromWidget(int seedID)
{
  vtkSMIntVectorProperty* RemoveSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("RemoveSourceSeed"));

  //Removing the 3DWidget's seed.
  RemoveSeedProperty->SetElement(0, seedID);
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::RemoveTargetSeedFromWidget(int seedID)
{
  vtkSMIntVectorProperty* RemoveSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("RemoveTargetSeed"));

    //Removing the 3DWidget's seed.
    RemoveSeedProperty->SetElement(0, seedID);
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::SelectSourceSeedOnWidget(int seed)
{
  vtkSMIntVectorProperty* SelectSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("SelectSourceSeed"));

    SelectSeedProperty->SetElement(0, seed);
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::SelectTargetSeedOnWidget(int seed)
{
  vtkSMIntVectorProperty* SelectSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("SelectTargetSeed"));

    SelectSeedProperty->SetElement(0, seed);
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ListBoxSourceItemSelected()
{
  if(this->SourceSeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->SelectSourceSeedOnWidget(this->SourceSeedsListBox->GetWidget()->GetSelectionIndex());
  }
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ListBoxTargetItemSelected()
{
  if(this->TargetSeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->SelectTargetSeedOnWidget(this->TargetSeedsListBox->GetWidget()->GetSelectionIndex());
  }
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ViewModeCallback()
{
  int viewMode;

  vtkSMIntVectorProperty* smViewMode = vtkSMIntVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("ViewMode"));

  smViewMode->SetElement(0, this->ViewModeCheckButton->GetSelectedState());
}

//----------------------------------------------------------------------------
void vtkPVHM3DCenterlines::ResamplingStepLengthCallback()
{
  this->Modified();
  this->SetValueChanged();
}
