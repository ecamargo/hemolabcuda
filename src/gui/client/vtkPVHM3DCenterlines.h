/*
 * vtkPVHM3DCenterlines.h
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */

#ifndef VTKPVHM3DCENTERLINES_H_
#define VTKPVHM3DCENTERLINES_H_

#include "vtkObjectFactory.h"
#include "vtkPV3DWidget.h"
#include "vtkIdList.h"

class vtkSMHM3DCenterlinesProxy;

class vtkKWEntry;
class vtkKWLabel;
class vtkKWListBoxWithScrollbars;
class vtkKWPushButton;
class vtkKWCheckButton;
class vtkKWThumbWheel;

class vtkPVApplication;

class VTK_EXPORT vtkPVHM3DCenterlines : public vtkPV3DWidget
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHM3DCenterlines *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHM3DCenterlines, vtkPV3DWidget);

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();

  // Description:
  // Set/Get widget visibility
  vtkSetMacro(WidgetVisibility,int);
  vtkGetMacro(WidgetVisibility,int);

  // Description:
  // Set/Get is a widget is ready only
  vtkSetMacro(ReadOnly,int);
  vtkGetMacro(ReadOnly,int);

  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();

  //Description:
  //Method invoked when an interface component has its value changed.
  void SetValueChanged();

  // Description:
  // Method responsible for creating the Interface that interacts with this filter's source.
  void Create(vtkKWApplication *App);

  // Description:
  // Call creation on the child.
  virtual void ChildCreate(vtkPVApplication*);

  // Description:
  // Adds a seed to the list from the listbox.
  void AddSourceSeed(int newSeed);
  void AddTargetSeed(int newSeed);

  // Description:
  // Adds a seed to the list from the 4 entries above the listbox.
  void AddSourceSeedCallback();
  void AddTargetSeedCallback();

  // Description:
  // Removes a source seed from the list by a given ID.
  void RemoveSourceSeedCallback();

  // Description:
  // Removes a target seed from the list by a given ID.
  void RemoveTargetSeedCallback();

  // Description:
  // Removes a source widget's seed from a passed ID;
  void RemoveSourceSeedFromWidget(int seedID);

  // Description:
  // Removes a target widget's seed from a passed ID;
  void RemoveTargetSeedFromWidget(int seedID);

  // Description:
  // Sends to the widget the selected source seed ID
  void SelectSourceSeedOnWidget(int seed);

  // Description:
  // Sends to the widget the selected target seed ID
  void SelectTargetSeedOnWidget(int seed);

  // Description:
  // Method invoked when a source seed's listbox item is selected (single click).
  void ListBoxSourceItemSelected();

  // Description:
  // Method invoked when a target seed's listbox item is selected (single click).
  void ListBoxTargetItemSelected();

  // Description:
  //
  void ViewModeCallback();

  void ResamplingStepLengthCallback();

protected:

  // Description:
  // Constructor
  vtkPVHM3DCenterlines();

  // Description:
  // Destructor
  virtual ~vtkPVHM3DCenterlines();

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();

  // Description:
  // Method executed when any iteraction is made on the viewport.
  virtual void  ExecuteEvent(vtkObject *obj, unsigned long l, void *p);

  // Description:
  // Controls if the widget visibility is on or off.
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable.
  int ReadOnly;

  // Description:
  // Controls if the Filter is using the Widget.
  int UsingWidget;

  // Description:
  // Stores all the seeds that are going to be used on the filter.
  vtkKWListBoxWithScrollbars *SourceSeedsListBox;
  vtkKWListBoxWithScrollbars *TargetSeedsListBox;

  // Description:
  // Button that adds a seed to the list.
  vtkKWPushButton *AddSourceSeedButton;
  vtkKWPushButton *AddTargetSeedButton;

  // Description:
  // Button that removes a seed from the list.
  vtkKWPushButton *RemoveSourceSeedButton;
  vtkKWPushButton *RemoveTargetSeedButton;

  // Description:
  // Point id Label.
  vtkKWLabel *PointIDLabel;

  // Description:
  // Entry that stores the point id from the widget's just selected / clicked.
  vtkKWEntry *PointIDEntry;

  // Description:
  // ListBox Label.
  vtkKWLabel *SourceListBoxLabel;
  vtkKWLabel *TargetListBoxLabel;

  vtkKWCheckButton *ViewModeCheckButton;

  vtkKWThumbWheel *ResamplingStepLengthThumbWheel;

private:
  vtkPVHM3DCenterlines(const vtkPVHM3DCenterlines&); // Not implemented
  void operator=(const vtkPVHM3DCenterlines&); // Not implemented
};

#endif /* VTKPVHM3DCENTERLINES_H_ */
