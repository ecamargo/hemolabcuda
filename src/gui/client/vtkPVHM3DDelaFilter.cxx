/*
 * $Id: vtkPVHM3DDelaFilter.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkProcessModule.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"
#include "vtkPVWidget.h"
	
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWPushButton.h"
#include "vtkKWIcon.h"
#include "vtkKWToolbar.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMProxyManager.h"

#include "vtkPVHM3DDelaFilter.h"
#include "vtkHM3DDelaFilter.h"

#define MODE_FILE		 "File"
#define MODE_MANUAL  "Manual"

vtkCxxRevisionMacro(vtkPVHM3DDelaFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DDelaFilter);

//----------------------------------------------------------------------------
vtkPVHM3DDelaFilter::vtkPVHM3DDelaFilter()
{ 
	vtkDebugMacro(<<"vtkPVHM3DDelaFilter::vtkPVHM3DDelaFilter()");

	this->Frame						= vtkKWFrameWithLabel::New();

	this->ConfigurationMenu = vtkKWMenuButtonWithLabel::New();

	this->ConfigLabel			= vtkKWLabel::New();
	this->ConfigEntry 		= vtkKWEntry::New(); 
	this->StatusLabel 		= vtkKWLabel::New();  	  
	this->ConfigButton 		= vtkKWLoadSaveButton::New();
	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{Configuration Files} {*.cfg}}");	
	
	this->SizeElementsLabel				= vtkKWLabel::New();
	this->SizeElementsEntry				= vtkKWEntry::New();
	this->RenumberingLabel				= vtkKWLabel::New();
	this->RenumberingEntry				= vtkKWEntry::New();	
	this->EdgesSizeLabel					= vtkKWLabel::New();
	this->EdgesSizeEntry					= vtkKWEntry::New();
	
	this->FixOrientationCheckButton 	= vtkKWCheckButton::New();

	this->GeometricInfoButton 			= vtkKWPushButton::New();
	
	this->AcceptButtonPressed	= false;		
	
}

//----------------------------------------------------------------------------
vtkPVHM3DDelaFilter::~vtkPVHM3DDelaFilter()
{
	vtkDebugMacro(<<"vtkPVHM3DDelaFilter::~vtkPVHM3DDelaFilter()");	
	this->ConfigButton->Delete();	
	this->ConfigLabel->Delete();
	this->ConfigEntry->Delete();   
	this->StatusLabel->Delete(); 
	this->ConfigurationMenu->Delete();
	this->SizeElementsLabel->Delete();
	this->SizeElementsEntry->Delete();
	this->RenumberingLabel->Delete();
	this->RenumberingEntry->Delete();  
	this->EdgesSizeLabel->Delete();
	this->EdgesSizeEntry->Delete();	
	this->GeometricInfoButton->Delete();
	this->FixOrientationCheckButton->Delete();
  	this->Frame->Delete();	
}

void vtkPVHM3DDelaFilter::GeometricInformationCallback()
{
  	vtkSMIntVectorProperty *Geoinfo = vtkSMIntVectorProperty::SafeDownCast(
	 	this->GetPVSource()->GetProxy()->GetProperty("GeometricInformation"));	
	Geoinfo->SetElement(0, 1);		
	this->UpdateWidgetInfo();
	this->ModifiedCallback();  
}

void vtkPVHM3DDelaFilter::ConfigButtonCallback()
{
  	this->ConfigEntry->SetValue(this->ConfigButton->GetFileName());	
	this->StatusLabel->SetText("");  
  	this->ConfigButton->SetText(""); 	
	
	// Se um arquivo foi carregado, mudo modo para arquivo
	this->ConfigurationMenu->GetWidget()->SetValue(MODE_FILE);
	this->ConfigurationMenuCallback();
	this->ModifiedCallback();
}

//------------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::FixOrientationCheckButtonCallback()
{
	this->ModifiedCallback();	
}

// ----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::ConfigurationMenuCallback() 
{
  if (this->ConfigurationMenu->IsCreated())
    {
    const char *value = this->ConfigurationMenu->GetWidget()->GetValue();
    if (!strcmp(value, MODE_FILE))
      {
			this->EdgesSizeEntry->ReadOnlyOn();
			this->SizeElementsEntry->ReadOnlyOn();
			this->RenumberingEntry->ReadOnlyOn();	
   		//this->SetWidgetMode((int) MODE_VISUALIZATION_VAL);
      }
    if (!strcmp(value, MODE_MANUAL))
      {
      	this->ConfigEntry->SetValue("None");      
			this->EdgesSizeEntry->ReadOnlyOff();
			this->SizeElementsEntry->ReadOnlyOff();
			this->RenumberingEntry->ReadOnlyOff();
  		//this->SetWidgetMode((int) MODE_EDITION_VAL);      		
      }
	}
  	this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::Create(vtkKWApplication* app)
{
	vtkDebugMacro(<< "vtkPVHM3DDelaFilter::Create()");

  	vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  this->Frame->SetLabelText("Dela Filter");  

  this->ConfigurationMenu->SetParent(this->Frame->GetFrame());
  this->ConfigurationMenu->Create(pvApp);
  this->ConfigurationMenu->ExpandWidgetOff();
  this->ConfigurationMenu->SetBalloonHelpString("Select the visualization state.");

  vtkKWMenuButton *omenu = this->ConfigurationMenu->GetWidget();
  omenu->SetWidth(10);
  omenu->SetPadX(10);
  omenu->SetPadY(2);
  
  omenu->AddRadioButton( MODE_MANUAL, this, "ConfigurationMenuCallback");
  omenu->AddRadioButton( MODE_FILE,   this, "ConfigurationMenuCallback");
  omenu->SetValue( MODE_MANUAL );
  
  this->ConfigurationMenu->GetWidget()->IndicatorVisibilityOn();
  this->ConfigurationMenu->GetLabel()->SetText("Configuration Mode: ");
  this->ConfigurationMenu->LabelVisibilityOn();  

  // Load Parameteres	
  this->ConfigButton->SetParent(this->Frame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback"); 
  this->ConfigButton->SetBalloonHelpString("Set Configuration File"); 
  this->ConfigButton->SetText(""); 

  this->ConfigLabel->SetParent(this->Frame->GetFrame());
  this->ConfigLabel->Create(pvApp);
  this->ConfigLabel->SetText("Configuration File");
  this->ConfigEntry->SetParent(this->Frame->GetFrame());
  this->ConfigEntry->Create(pvApp);
  this->ConfigEntry->SetValue("None");
  this->ConfigEntry->ReadOnlyOn();

  this->StatusLabel->SetParent(this->Frame->GetFrame());
  this->StatusLabel->Create(pvApp);
  this->StatusLabel->SetBalloonHelpString("Status"); 
  this->StatusLabel->SetText("");

  this->EdgesSizeLabel->SetParent(this->Frame->GetFrame());
  this->EdgesSizeLabel->Create(pvApp);
  this->EdgesSizeLabel->SetText("Edges Size");
  this->EdgesSizeEntry->SetParent(this->Frame->GetFrame());
  this->EdgesSizeEntry->Create(pvApp);
  this->EdgesSizeEntry->SetBalloonHelpString("Set Edges Size"); 
  this->EdgesSizeEntry->SetValue("None");
  
  this->SizeElementsLabel->SetParent(this->Frame->GetFrame());
  this->SizeElementsLabel->Create(pvApp);
  this->SizeElementsLabel->SetText("Elements Size");
  this->SizeElementsEntry->SetParent(this->Frame->GetFrame());
  this->SizeElementsEntry->Create(pvApp);
  this->SizeElementsEntry->SetBalloonHelpString("Set Elements Size"); 
  this->SizeElementsEntry->SetValue("None");

  this->RenumberingLabel->SetParent(this->Frame->GetFrame());
  this->RenumberingLabel->Create(pvApp);
  this->RenumberingLabel->SetText("Renumbering Method");
  this->RenumberingEntry->SetParent(this->Frame->GetFrame());
  this->RenumberingEntry->Create(pvApp);
  this->RenumberingEntry->SetBalloonHelpString("Set Renumbering Method"); 
  this->RenumberingEntry->SetValueAsDouble(1);  

	// Wire Frame Checkbutton
  this->FixOrientationCheckButton->SetParent(this->Frame->GetFrame());
  this->FixOrientationCheckButton->Create(pvApp);
  this->FixOrientationCheckButton->SetText("Fix Normals Orientation");  
  this->FixOrientationCheckButton->SetCommand(this, "FixOrientationCheckButtonCallback");
  this->FixOrientationCheckButton->SetBalloonHelpString("If checked, force face normals direction to point inside.\nWorks only for meshes with more than one group.");  
  this->FixOrientationCheckButton->SelectedStateOn(); 

  this->GeometricInfoButton->SetParent(this->Frame->GetFrame());
  this->GeometricInfoButton->Create(pvApp);
  this->GeometricInfoButton->SetCommand(this, "GeometricInformationCallback"); 
  this->GeometricInfoButton->SetBalloonHelpString("Get Geometric information from the input"); 
  this->GeometricInfoButton->SetText("Geometric Information"); 
  
	//-------------------------------------------------------------------------------------      
	//Place components after accept
  this->PlaceComponents();
  this->ConfigureComponents(); 	 
}

//------------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::PlaceComponents()
{
	vtkDebugMacro(<<"vtkPVHM3DDelaFilter::PlaceComponents()");
	
  this->Script("pack %s -fill x -expand true",  
    this->Frame->GetWidgetName());  

  this->Script("grid %s - - -sticky w",
    this->ConfigurationMenu->GetWidgetName());	

  this->Script("grid %s %s %s - - -sticky ew",
    this->ConfigLabel->GetWidgetName(), 
    this->ConfigEntry->GetWidgetName(),
    this->ConfigButton->GetWidgetName());

  this->Script("grid %s %s - - -sticky ew",     
		this->EdgesSizeLabel->GetWidgetName(),
  	this->EdgesSizeEntry->GetWidgetName()); 
 
  this->Script("grid %s %s - - -sticky ew",     
  	this->SizeElementsLabel->GetWidgetName(),
		this->SizeElementsEntry->GetWidgetName());              

  this->Script("grid %s %s - - -sticky ew",     
		this->RenumberingLabel->GetWidgetName(),
  	this->RenumberingEntry->GetWidgetName());       

  this->Script("grid %s - - -sticky ew", 
  	this->FixOrientationCheckButton->GetWidgetName()); 

  this->Script("grid %s - - -sticky ew",
    this->GeometricInfoButton->GetWidgetName());      

  this->Script("grid %s - - -sticky ew",
    this->StatusLabel->GetWidgetName());      
    
  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->SizeElementsEntry->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RenumberingEntry->GetWidgetName(),
    this->GetTclName());    

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->EdgesSizeEntry->GetWidgetName(),
    this->GetTclName());    
  
	if( !this->AcceptButtonPressed )
		{	
		this->ConfigurationMenu->EnabledOff();
		this->ConfigButton->EnabledOff();
		this->ConfigLabel->EnabledOff();
		this->ConfigEntry->EnabledOff();
		this->EdgesSizeLabel->EnabledOff();
		this->EdgesSizeEntry->EnabledOff();
		this->SizeElementsLabel->EnabledOff();
		this->SizeElementsEntry->EnabledOff();
		this->RenumberingLabel->EnabledOff();
		this->RenumberingEntry->EnabledOff();
		this->GeometricInfoButton->EnabledOff();
		this->FixOrientationCheckButton->EnabledOff();	  
		}
	else
		{	  
	  	this->ConfigurationMenu->EnabledOn();
	  	this->ConfigButton->EnabledOn();
	  	this->ConfigLabel->EnabledOn();
	  	this->ConfigEntry->EnabledOn();
	  	this->EdgesSizeLabel->EnabledOn();
	  	this->EdgesSizeEntry->EnabledOn();
	  	this->SizeElementsLabel->EnabledOn();
	  	this->SizeElementsEntry->EnabledOn();
	  	this->RenumberingLabel->EnabledOn();
	  	this->RenumberingEntry->EnabledOn();
	  	this->GeometricInfoButton->EnabledOn();
	  	this->FixOrientationCheckButton->EnabledOn();	  	
		}    
}

// ----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());       
}

// ----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::Accept()
{  		
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(1, 0, 0);	

	const char *value = this->ConfigurationMenu->GetWidget()->GetValue();
	if(!strcmp(value,"Manual"))
		{
		//vtkErrorMacro(<<"Configuration File not set.");	  			
		vtkSMDoubleVectorProperty *AutoParProp = vtkSMDoubleVectorProperty::SafeDownCast(
   		this->GetPVSource()->GetProxy()->GetProperty("ApplyAutoPar"));
		if(AutoParProp)
    		{
			AutoParProp->SetElement(0, this->EdgesSizeEntry->GetValueAsDouble());
			AutoParProp->SetElement(1, this->SizeElementsEntry->GetValueAsDouble());
			AutoParProp->SetElement(2, this->RenumberingEntry->GetValueAsDouble());
			}		
		else
			{
			vtkErrorMacro(<<"Cannot create \"AutoPar\" Dela3D filter property.");	  	
			return;
		  	} 	
		this->StatusLabel->SetText("Bounding Box used to compute coordinates.");		  					
		}
	else
		{	
		if(!strcmp(this->ConfigEntry->GetValue(),"None"))
			{
			this->StatusLabel->SetText("You must select a CFG file!");
			return;
			}
		vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("ApplyDela"));    
    
		if (prop) 
			{
			// Seta propriedade com o nome do arquivo de configuração  	
			prop->SetElement(0,this->ConfigEntry->GetValue());
			}
		else
			{
			vtkErrorMacro(<<"Cannot create \"ApplyDela\" Dela3D filter property.");	  	
			return;
			}
			//this->StatusLabel->SetText("Selected CFG file used.");		  							  
		}

	vtkSMIntVectorProperty *fixProp = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("ApplyFixOrientation"));
	if(fixProp)
		fixProp->SetElement(0, this->FixOrientationCheckButton->GetSelectedState());

	if(!this->AcceptButtonPressed)
		{
		this->GeometricInformationCallback();
	  	this->AcceptButtonPressed = true;	
	  	this->Modified();
		}
		
	this->PlaceComponents();  
	// Envia propriedade para os objetos no servidor
	this->UpdateWidgetInfo();  
}

//-----------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::Initialize()
{
}  

//------------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
	this->UpdateWidgetInfo();
}


//------------------------------------------------------------------------------
void vtkPVHM3DDelaFilter::UpdateWidgetInfo()
{
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	this->GetPVSource()->GetProxy()->UpdateInformation();
	
  vtkSMStringVectorProperty *responseProp = vtkSMStringVectorProperty::SafeDownCast(
  	this->GetPVSource()->GetProxy()->GetProperty("filterInfo"));

	this->StatusLabel->SetText(responseProp->GetElement(0));	  
	
	char Information[1024], aux[64];
	double values[6];
	strcpy(Information, this->StatusLabel->GetText());

	// Testa se information lida é proveniente do botão "GetInformation" ou não
	sscanf(Information, "%s", aux);
	if( !strcmp(aux, "Triangle") )
		{
		sscanf(Information, "%s %s %s %lf %s %lf %s %lf %s %s %s %lf %s %lf %s %lf",
					 aux, aux, aux, &values[0], aux, &values[1], aux, &values[2],
					 aux, aux, aux, &values[3], aux, &values[4], aux, &values[5]);		 
		//printf("\nAverage: %.14lf %.14lf.\n", values[1], values[4]);

		// Test Values
	  	this->SizeElementsEntry->SetValueAsDouble( pow(values[1],0.2) );
		this->EdgesSizeEntry->SetValueAsDouble( pow(values[1],0.2) );	  			
		}
}

void vtkPVHM3DDelaFilter::PostAccept()
{
}
