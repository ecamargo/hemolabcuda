/*
 * $Id: vtkPVHM3DDelaFilter.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHM3DDelaFilter
// .SECTION Description
// Interface for the Dela3D filter

#ifndef _vtkPVHM3DDelaFilter_h_
#define _vtkPVHM3DDelaFilter_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkKWPushButton;
class vtkKWCheckButton;  
class vtkKWEntry;
class vtkKWLabel;
class vtkKWLoadSaveButton;
class vtkKWFrameWithLabel;
class vtkKWMenuButtonWithLabel;

class VTK_EXPORT vtkPVHM3DDelaFilter : public vtkPVObjectWidget
{
public:
  	static vtkPVHM3DDelaFilter *New();
  	vtkTypeRevisionMacro(vtkPVHM3DDelaFilter,vtkPVObjectWidget);
  	void PrintSelf(ostream& os, vtkIndent indent){};
	
  	// Description:
  	// Execute event
  	virtual void ExecuteEvent(vtkObject*, unsigned long, void*);
  
  	// Description:
  	// Save this widget to a file.
	virtual void SaveInBatchScript(ofstream *file){};
  
  	// Description:
  	// Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  	// Call TCL/TK commands to place KW Components
	void PlaceComponents();
  	void ConfigureComponents(); 	
	
  	// Description:
  	// Call creation on the child.  
	virtual void 	Create (vtkKWApplication *app); 

  	// Description:
  	// Initialize the newly created widget.
  	virtual void Initialize();
  
  	// Description:
  	// This serves a dual purpose.  For tracing and for saving state.
  	virtual void Trace(ofstream *file) {};
    
	//BTX
	// Description:
	// Called when accept button is pushed.
	// Sets objects variable to the widgets value.
	// Side effect is to turn modified flag off.
	virtual void Accept();
	virtual void PostAccept();  
	//ETX

	void SetValueChanged();

  //BTX
//  // Description:
//  // Called when the Accept button is pressed.  It moves the widget values to the 
//  // VTK calculator filter.
//  virtual void AcceptInternal(vtkClientServerID);
  //ETX

	void ConfigButtonCallback();
	void ConfigurationMenuCallback();
	void GeometricInformationCallback();
	void FixOrientationCheckButtonCallback();	
  
protected:
  vtkPVHM3DDelaFilter();
  virtual ~vtkPVHM3DDelaFilter();
  
	vtkKWFrameWithLabel *Frame;	  

	vtkKWMenuButtonWithLabel *ConfigurationMenu;

	// Config File Parameters
	vtkKWLoadSaveButton*		ConfigButton;  
  	vtkKWLabel* 				ConfigLabel;
  	vtkKWEntry* 				ConfigEntry;  
  	vtkKWLabel* 				StatusLabel;    
  
	vtkKWLabel*           	SizeElementsLabel;
	vtkKWEntry* 				SizeElementsEntry;
	vtkKWLabel*           	RenumberingLabel;
	vtkKWEntry* 				RenumberingEntry;   
	vtkKWLabel*           	EdgesSizeLabel;
	vtkKWEntry*					EdgesSizeEntry;	

	vtkKWPushButton*			GeometricInfoButton;
	vtkKWCheckButton*			FixOrientationCheckButton;	
	
	bool 							AcceptButtonPressed;
  	 	
private:  
	vtkPVHM3DDelaFilter(const vtkPVHM3DDelaFilter&); // Not implemented
	void operator=(const vtkPVHM3DDelaFilter&); // Not implemented
}; 

#endif  /*_vtkPVHM3DDelaFilter_h_*/

