#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkPVWidget.h"
#include "vtkPVWindow.h"
#include "vtkPVRenderView.h"
#include "vtkPVColorMap.h"
#include "vtkPVGenericRenderWindowInteractor.h"
#include "vtkKWComboBox.h"	 
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWPushButton.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMProxyManager.h"
#include "vtkPVHM3DFullModelFilter.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkCellPicker.h"
#include "vtkKWRadioButton.h"
#include "vtkKWApplication.h"
#include "vtkKWApplicationSettingsInterface.h"
#include "vtkMath.h"
#include "vtkHM1DHeartCurveReader.h"
#include "vtkDataArrayCollection.h"

vtkCxxRevisionMacro(vtkPVHM3DFullModelFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DFullModelFilter);

//----------------------------------------------------------------------------
vtkPVHM3DFullModelFilter::vtkPVHM3DFullModelFilter()
{ 
	vtkDebugMacro(<<"vtkPVHM3DFullModelFilter::vtkPVHM3DFullModelFilter()");

	this->Frame	= vtkKWFrameWithLabel::New();
	
	this->SurfaceElementTypeLabel  = vtkKWLabel::New();
	this->CoverElementTypeLabel  = vtkKWLabel::New();
	
	this->SurfaceOption  = vtkKWComboBox::New();
	this->CoverOption  = vtkKWComboBox::New();
	
	this->GroupSelectionFrame  = vtkKWFrameWithLabel::New();
	this->SurfaceTrianguleInfoFrame = vtkKWFrameWithLabel::New();	  
	this->CoverTrianguleInfoFrame = vtkKWFrameWithLabel::New();   
	this->GeneralOptionsFrame = vtkKWFrameWithLabel::New(); 
	
	this->SurfaceElastinLabel	= vtkKWLabel::New();         
	this->SurfaceElastinEntry  = vtkKWEntry::New();
	
	this->SurfaceCurveRadiusLabel	= vtkKWLabel::New();         
	this->SurfaceCurveRadiusEntry  = vtkKWEntry::New();
	
	this->SurfaceWallthicknessLabel	= vtkKWLabel::New();         
	this->SurfaceWallthicknessEntry  = vtkKWEntry::New();
	
	
	this->SurfaceRefPressureLabel	= vtkKWLabel::New();         
	this->SurfaceRefPressureEntry = vtkKWEntry::New();
		  
	this->SurfaceViscoLabel	= vtkKWLabel::New();         
	this->SurfaceViscoEntry = vtkKWEntry::New();
	
	this->NumberOfCoverGroups = 0;
	
	this->GroupSelectionCombo = 	vtkKWComboBox::New();
	this->GroupSelectionLabel = vtkKWLabel::New();
	
	this->InterfaceCreated = false;
	
	this->GenerateSolverFilesPushButton =	vtkKWPushButton::New();
	this->GenerateSolverFilesPushButton->EnabledOff();
	
	this->ChooseDirectoryDialog = vtkKWLoadSaveDialog::New();
	
	this->PLabelList.clear();
	this->PEntryList.clear();
	this->CoverFrameList.clear();
	
	this->GeneralConfiguration = NULL;
	 
	this->XYPlotWidget = vtkKWHM1DXYPlotWidget::New();
	
	//*****************************************************
	// elementos de interface quando tem-se pressao variante
	
	this->VariantPressureOptionsFrame=vtkKWFrameWithLabel::New();	  
  this->FunctionHeartCurve = vtkKWComboBox::New(); 
  this->AddCurveButton= vtkKWLoadSaveButton::New();
  this->AddCurveButtonOk = vtkKWPushButton::New();
  this->PointNumber= vtkKWEntryWithLabel::New();
  this->Amplitude = vtkKWEntryWithLabel::New();
  this->FinalTime= vtkKWEntryWithLabel::New();
  this->MaxValue= vtkKWEntryWithLabel::New();
  this->MinValue= vtkKWEntryWithLabel::New();
  this->PlotButton= vtkKWPushButton::New();
  this->UpdateCurveButton = vtkKWPushButton::New();
	
	
	this->CoverBCType = vtkIntArray::New();

	this->CurveFromFile = NULL;

	this->BCRadioButtonSetList.clear();

	this->NumberOfWallGroups = 0;

	this->WallGroupCollection = vtkDataArrayCollection::New();

	this->UpdateSurfacePropPushButton = 	vtkKWPushButton::New();
}

//----------------------------------------------------------------------------
vtkPVHM3DFullModelFilter::~vtkPVHM3DFullModelFilter()
{
	vtkDebugMacro(<<"vtkPVHM3DFullModelFilter::~vtkPVHM3DFullModelFilter()");	
	
	
  this->Frame->Delete();
  
	this->SurfaceElementTypeLabel->Delete();
	this->CoverElementTypeLabel->Delete();
	
	this->SurfaceOption->Delete();
	this->CoverOption->Delete();
	
	this->GroupSelectionFrame->Delete();
	this->SurfaceTrianguleInfoFrame->Delete();	  
	this->CoverTrianguleInfoFrame->Delete();
	this->GeneralOptionsFrame->Delete();
	
  this->SurfaceElastinLabel->Delete();         
	this->SurfaceElastinEntry->Delete();
	
	this->SurfaceCurveRadiusLabel->Delete();         
	this->SurfaceCurveRadiusEntry->Delete();
	
	this->SurfaceWallthicknessLabel->Delete();         
	this->SurfaceWallthicknessEntry->Delete();
	
	
	this->SurfaceRefPressureLabel->Delete();         
	this->SurfaceRefPressureEntry->Delete();
		  
	this->SurfaceViscoLabel->Delete();         
	this->SurfaceViscoEntry->Delete();
  

	if (this->PLabelList.size())
		{	
		LabelList::iterator it = this->PLabelList.begin();
		for (it; it != this->PLabelList.end(); it++)
	 		(*it)->Delete();
		}	 	

	if (this->PEntryList.size())
		{	
		EntryList::iterator it = this->PEntryList.begin();
		for (it; it != this->PEntryList.end(); it++)
 	 		(*it)->Delete();
		} 	

	if (this->CoverFrameList.size())
		{	 	 
 		FrameList::iterator it = this->CoverFrameList.begin();	
		for (it; it != this->CoverFrameList.end(); it++)
 	 		(*it)->Delete();	
		}

	if (this->VLabelList.size())
		{	
		LabelList::iterator it =  this->VLabelList.begin();
		for (it; it != this->VLabelList.end(); it++)
	 		(*it)->Delete();
		}	 	

	if (this->VEntryList.size())
		{	
		EntryList::iterator it = this->VEntryList.begin();
		for (it; it != this->VEntryList.end(); it++)
 	 		(*it)->Delete();
		} 	


	if (this->PRadioButtonList.size())
		{	
		RadioButtonList::iterator it = this->PRadioButtonList.begin();
		for (it; it != this->PRadioButtonList.end(); it++)
 	 		(*it)->Delete();
		} 	

	if (this->VRadioButtonList.size())
		{	
		RadioButtonList::iterator it = this->VRadioButtonList.begin();
		for (it; it != this->VRadioButtonList.end(); it++)
 	 		(*it)->Delete();
		} 


	if (this->CPRadioButtonList.size())
		{	
		RadioButtonList::iterator it = this->CPRadioButtonList.begin();
		for (it; it != this->CPRadioButtonList.end(); it++)
 	 		(*it)->Delete();
		} 

	if (this->BCRadioButtonSetList.size())
		{	
		RadioButtonSetList::iterator it = this->BCRadioButtonSetList.begin();
		for (it; it != this->BCRadioButtonSetList.end(); it++)
 	 		(*it)->Delete();
		} 

	
	
	this->GroupSelectionCombo->Delete();
	this->GroupSelectionLabel->Delete();
	
	this->GenerateSolverFilesPushButton->Delete();
	
	this->ChooseDirectoryDialog->Delete();
	
	  if (this->GeneralConfiguration) 
  	{
    this->GeneralConfiguration->Withdraw();
    this->GeneralConfiguration->Delete();
    this->GeneralConfiguration = NULL;
  	}
  
	if( this->XYPlotWidget )
		this->XYPlotWidget->Delete();
	this->XYPlotWidget = NULL;
	
	
	//*****************************************************
	// elementos de interface quando tem-se pressao variante
	
	this->VariantPressureOptionsFrame->Delete();
  this->FunctionHeartCurve->Delete();
  this->AddCurveButton->Delete();
  this->AddCurveButtonOk->Delete();
  
  this->PointNumber->Delete();
  this->Amplitude->Delete();
  this->FinalTime->Delete();
  this->MaxValue->Delete();
  this->MinValue->Delete();
  this->PlotButton->Delete();
  this->UpdateCurveButton->Delete();
	
	//***************************************************
	
	this->CoverBCType->Delete();
	
	
	for (int i=0; i< this->NumberOfCoverGroups; i++)
  	{
		this->PressureCurveVector[i]->Delete();
  	}
	
	if (this->CurveFromFile)
		this->CurveFromFile->Delete();

	for (int i=0; i< this->WallGroupCollection->GetNumberOfItems(); i++)
		this->WallGroupCollection->GetItem(i)->Delete();
	
	this->WallGroupCollection->RemoveAllItems();	
	

	this->WallGroupCollection->Delete();
	
	this->UpdateSurfacePropPushButton->Delete();

}

//----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::Create(vtkKWApplication* app)
{
	vtkDebugMacro(<< "vtkPVHM3DFullModelFilter::Create()");
	
  //char buffer[1024];
	
	this->kwApp = app;

  vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	this->Window = pvApp->GetMainWindow();
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	


  this->XYPlotWidget->SetTitle("Time Variant Pressure Curve Editor");
  this->XYPlotWidget->SetMasterWindow(vtkKWWindow::SafeDownCast(app->GetNthWindow(0)));
	this->XYPlotWidget->Create(app);

	
  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }
  
  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  this->Frame->SetLabelText("Element Type");  

	this->ChooseDirectoryDialog->Create(app);
  
  
  this->GroupSelectionFrame->SetParent(this);
  this->GroupSelectionFrame->Create(pvApp);
  this->GroupSelectionFrame->SetLabelText("Group Selection");  
  
	
	this->SurfaceTrianguleInfoFrame->SetParent(this);
  this->SurfaceTrianguleInfoFrame->Create(pvApp);
  this->SurfaceTrianguleInfoFrame->SetLabelText("Wall Parameters Group 0");  
  
	
	this->CoverTrianguleInfoFrame->SetParent(this);
  this->CoverTrianguleInfoFrame->Create(pvApp);
  this->CoverTrianguleInfoFrame->SetLabelText("Input/Output Face Boundary Condition");  
  //this->CoverTrianguleInfoFrame->CollapseFrame();
  
  this->GeneralOptionsFrame->SetParent(this);
//  this->GeneralOptionsFrame->SetParent(pvWindow->GetViewPanelFrame());

  //this->Window->GetViewPanelFrame()
  this->GeneralOptionsFrame->Create(pvApp);
  this->GeneralOptionsFrame->SetLabelText("General Options");  

	
  this->SurfaceElastinLabel->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceElastinLabel->Create(pvApp);
  this->SurfaceElastinLabel->SetText("Elastin: [dyn/cm^2]");
	
	this->SurfaceElastinEntry->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceElastinEntry->Create(pvApp);
  this->SurfaceElastinEntry->SetWidth(8);
  //this->SurfaceElastinEntry->SetCommand(this, "GeneralCallback");
	
	this->SurfaceCurveRadiusLabel->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceCurveRadiusLabel->Create(pvApp);
  this->SurfaceCurveRadiusLabel->SetText("Curve Radius: [cm]");
  
  this->SurfaceCurveRadiusEntry->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceCurveRadiusEntry->Create(pvApp);
  this->SurfaceCurveRadiusEntry->SetWidth(8);
 // this->SurfaceCurveRadiusEntry->SetCommand(this, "SurfaceGeneralCallback");
  
	this->SurfaceWallthicknessLabel->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceWallthicknessLabel->Create(pvApp);
  this->SurfaceWallthicknessLabel->SetText(" Wall Thickness: [cm]");
  
  this->SurfaceWallthicknessEntry->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceWallthicknessEntry->Create(pvApp);
  this->SurfaceWallthicknessEntry->SetWidth(8);
 // this->SurfaceWallthicknessEntry->SetCommand(this, "SurfaceGeneralCallback");


	this->UpdateSurfacePropPushButton->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->UpdateSurfacePropPushButton->Create(pvApp);
  this->UpdateSurfacePropPushButton->SetText("Set Current Wall Group Parameters");
  this->UpdateSurfacePropPushButton->SetCommand(this, "SetWallParamCallBack");
  this->UpdateSurfacePropPushButton->SetPadY(5);
	this->UpdateSurfacePropPushButton->SetPadX(5);

	this->SurfaceRefPressureLabel->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceRefPressureLabel->Create(pvApp);
  this->SurfaceRefPressureLabel->SetText("Reference Pressure: [dyn/cm^2]");
	
	this->SurfaceRefPressureEntry->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceRefPressureEntry->Create(pvApp);
  this->SurfaceRefPressureEntry->SetWidth(8);
  //this->SurfaceRefPressureEntry->SetCommand(this, "SurfaceGeneralCallback");
	
	
	this->SurfaceViscoLabel->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceViscoLabel->Create(pvApp);
  this->SurfaceViscoLabel->SetText("Viscoelasticity: [dyn.sec/cm^2]");
	
	this->SurfaceViscoEntry->SetParent(this->SurfaceTrianguleInfoFrame->GetFrame());
  this->SurfaceViscoEntry->Create(pvApp);
  this->SurfaceViscoEntry->SetWidth(8);
  //this->SurfaceViscoEntry->SetCommand(this, "SurfaceGeneralCallback");
  
	this->GroupSelectionCombo->SetParent(this->GroupSelectionFrame->GetFrame());
  this->GroupSelectionCombo->Create(pvApp);
	this->GroupSelectionCombo->ReadOnlyOn();
	this->GroupSelectionCombo->SetCommand(this, "UpdateGroupsCallback"); 
  this->GroupSelectionCombo->SetWidth(4);
  this->GroupSelectionCombo->SetValue("0");  
  
 	this->GroupSelectionLabel->SetParent(	this->GroupSelectionFrame->GetFrame());
  this->GroupSelectionLabel->Create(pvApp);
  this->GroupSelectionLabel->SetText("Group Selection: ");
 
	this->GenerateSolverFilesPushButton->SetParent(this->GeneralOptionsFrame->GetFrame());
  this->GenerateSolverFilesPushButton->Create(pvApp);
  this->GenerateSolverFilesPushButton->SetText("Configure and Generate 3D Solver Input Files");
  this->GenerateSolverFilesPushButton->SetCommand(this, "GenerateSolverFiles");
  
	// elementos de interface quando tem-se pressao variante
	this->VariantPressureOptionsFrame->SetParent(CoverTrianguleInfoFrame->GetFrame());           //this);
  this->VariantPressureOptionsFrame->Create(pvApp);
  this->VariantPressureOptionsFrame->SetLabelText("Time Variant Pressure Parameters");  
	
	
	this->FunctionHeartCurve->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->FunctionHeartCurve->Create(pvApp);
	this->FunctionHeartCurve->SetBalloonHelpString("Automatic generation of the curve with function selected");
	this->FunctionHeartCurve->AddValue("None");
	this->FunctionHeartCurve->AddValue("Sen(x)");
	this->FunctionHeartCurve->AddValue("sen^2(x) 1/2 period");
	this->FunctionHeartCurve->AddValue("sen^2(x)");  
	this->FunctionHeartCurve->SetValue("None");
  
    
	this->AddCurveButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->AddCurveButton->Create(pvApp);
	this->AddCurveButton->SetText("Read Function");
	this->AddCurveButton->GetLoadSaveDialog()->ChooseDirectoryOff();
  this->AddCurveButton->GetLoadSaveDialog()->SetDefaultExtension("txt");
	this->AddCurveButton->SetBalloonHelpString("Read pressure curve from a file (.txt)");
  //this->AddCurveButton->SetCommand(this, "ReadCurve");
  this->AddCurveButton->SetCommand(this, "UpdateReadCurveButton");
  
 	this->AddCurveButtonOk->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->AddCurveButtonOk->Create(pvApp);
	this->AddCurveButtonOk->SetText("Ok");
	this->AddCurveButtonOk->SetBalloonHelpString("Push Ok to read a curve from the selected file");
  this->AddCurveButtonOk->SetCommand(this, "ReadCurve");
  
	this->PointNumber->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->PointNumber->GetWidget()->SetWidth(8);
	this->PointNumber->Create(pvApp);
	this->PointNumber->SetLabelText("Number of Points ");
  this->PointNumber->GetWidget()->SetCommand(this, "GeneralCallback");	

	this->Amplitude->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->Amplitude->GetWidget()->SetWidth(8);
	this->Amplitude->Create(pvApp);
	this->Amplitude->SetLabelText("Curve Amplitude");
  this->Amplitude->GetWidget()->SetCommand(this, "GeneralCallback");	



	this->FinalTime->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->FinalTime->GetWidget()->SetWidth(8);
	this->FinalTime->Create(pvApp);
	this->FinalTime->SetLabelText("Final Time ");
	this->FinalTime->GetWidget()->SetCommand(this, "GeneralCallback");

	this->MaxValue->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->MaxValue->GetWidget()->SetWidth(8);
	this->MaxValue->Create(pvApp);
	this->MaxValue->SetLabelText("Max Value ");
	this->MaxValue->GetWidget()->SetCommand(this, "GeneralCallback");
	
	this->MinValue->SetParent(this->VariantPressureOptionsFrame->GetFrame());
	this->MinValue->GetWidget()->SetWidth(8);
	this->MinValue->Create(pvApp);
	this->MinValue->SetLabelText("Min Value ");
	this->MinValue->GetWidget()->SetCommand(this, "GeneralCallback");
	
	this->PlotButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->PlotButton->Create(pvApp);
  this->PlotButton->SetText("Plot");
  this->PlotButton->SetCommand(this, "ShowPressurePlot");
	

	this->UpdateCurveButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->UpdateCurveButton->Create(pvApp);
  this->UpdateCurveButton->SetText("Update Curve");
  this->UpdateCurveButton->SetCommand(this, "UpdatePressureCurve");


	this->FunctionHeartCurve->SetCommand(this->Amplitude->GetWidget(), "ReadOnlyOff");

  this->ConfigureComponents(); 	 
}

// ----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::PlaceComponents()
{
	//cout << "vtkPVHM3DFullModelFilter::PlaceComponents()" << endl;
	vtkDebugMacro(<<"vtkPVHM3DFullModelFilter::PlaceComponents()");
	
  // setando os frames
  this->Script("pack %s %s %s -fill x -expand true",  
    this->GroupSelectionFrame->GetWidgetName(),
		this->GeneralOptionsFrame->GetWidgetName(),
   	this->SurfaceTrianguleInfoFrame->GetWidgetName());
   
  this->Script("grid %s %s - - -sticky ew",
    this->SurfaceElastinLabel->GetWidgetName(),
    this->SurfaceElastinEntry->GetWidgetName());
    
  this->Script("grid %s %s - - -sticky ew",
    this->SurfaceCurveRadiusLabel->GetWidgetName(),
    this->SurfaceCurveRadiusEntry->GetWidgetName());
    
    
  this->Script("grid %s %s - - -sticky ew",
    this->SurfaceWallthicknessLabel->GetWidgetName(),
    this->SurfaceWallthicknessEntry->GetWidgetName());    
    
    
  this->Script("grid %s %s - - -sticky ew",
    this->SurfaceRefPressureLabel->GetWidgetName(),
    this->SurfaceRefPressureEntry->GetWidgetName());


  this->Script("grid %s %s - - -sticky ew",
    this->SurfaceViscoLabel->GetWidgetName(),
    this->SurfaceViscoEntry->GetWidgetName());
   
  this->Script("grid %s - - -sticky ew",
    this->UpdateSurfacePropPushButton->GetWidgetName());
   
   
   
 //*****************************************************************  
 //  VariantPressureOptionsFrame
 
   this->Script("grid %s %s - - -sticky ew",
    this->AddCurveButton->GetWidgetName(),
    this->AddCurveButtonOk->GetWidgetName());

   this->Script("grid %s - - -sticky ew",
    this->FunctionHeartCurve->GetWidgetName());

 
   this->Script("grid %s - - -sticky ew",
    this->PointNumber->GetWidgetName());
    
   this->Script("grid %s - - -sticky ew",
    this->Amplitude->GetWidgetName());

   this->Script("grid %s - - -sticky ew",
    this->FinalTime->GetWidgetName());

   this->Script("grid %s - - -sticky ew",
    this->MaxValue->GetWidgetName());

   this->Script("grid %s - - -sticky ew",
    this->MinValue->GetWidgetName());
    
   this->Script("grid %s %s - - -sticky ew",
    this->PlotButton->GetWidgetName(),
    this->UpdateCurveButton->GetWidgetName());
  
	char buffer[1024];

	this->CoverBCType->SetNumberOfTuples(this->NumberOfCoverGroups);
	
	for (int i=0; i< this->NumberOfCoverGroups; i++)
  	this->CoverBCType->SetValue(i,0);
	
	for (int i=0; i< this->NumberOfCoverGroups; i++)
  	{
  	vtkDoubleArray *temp = vtkDoubleArray::New();
  	temp->SetNumberOfTuples(0);
  	temp->SetNumberOfComponents(2);
		this->PressureCurveVector.push_back(temp);
	
  	}
	
	if (this->NumberOfCoverGroups) 
  	{
    
    // criando os vtkDoubleArrays que irao descrever os parametros de cada grupo de parede
    for (int i = 0; i < this->NumberOfWallGroups; ++i) 
    	{
			vtkDoubleArray *temp = vtkDoubleArray::New();
			temp->SetNumberOfValues(5);
			
			// initializing array
			for (int z = 0; z < 5; ++z)
				temp->SetValue(z, 0.0); 
			
			this->WallGroupCollection->AddItem(temp);    
    	}
    
    
    for (int i=0; i< this->NumberOfCoverGroups ; i++)
			{
			vtkKWFrameWithLabel *frame = vtkKWFrameWithLabel::New();
			vtkKWLabel *plabel = vtkKWLabel::New();
			vtkKWEntry *pentry = vtkKWEntry::New();
			pentry->SetCommand(this, "GeneralCallback");
			
			vtkKWLabel *vlabel = vtkKWLabel::New();
			vtkKWEntry *ventry = vtkKWEntry::New();
			
			ventry->SetCommand(this, "GeneralCallback");
			
			vtkKWRadioButton *PradioB = vtkKWRadioButton::New();
			vtkKWRadioButton *CPradioB = vtkKWRadioButton::New();
			vtkKWRadioButton *VradioB = vtkKWRadioButton::New();
			
			vtkKWRadioButtonSet *radioBSet = vtkKWRadioButtonSet::New();
			
			// inserindo os novos objetos KW nas listas	
			this->PLabelList.push_back(plabel);
			this->PEntryList.push_back(pentry);
			
			this->VLabelList.push_back(vlabel);
			this->VEntryList.push_back(ventry);
						
			this->CoverFrameList.push_back(frame);
			
			this->PRadioButtonList.push_back(PradioB);
			this->CPRadioButtonList.push_back(CPradioB);
			
			this->VRadioButtonList.push_back(VradioB);
			
			this->BCRadioButtonSetList.push_back(radioBSet);
			}
    
		LabelList::iterator itPLabel = this->PLabelList.begin();
 		EntryList::iterator itPEntry = this->PEntryList.begin();
		FrameList::iterator itFrameList = this->CoverFrameList.begin();
 		
 		LabelList::iterator itVLabel = this->VLabelList.begin();
 		EntryList::iterator itVEntry = this->VEntryList.begin();
		
 		RadioButtonList::iterator itPRadio =	this->PRadioButtonList.begin();
 		RadioButtonList::iterator itVRadio =	this->VRadioButtonList.begin();
 		
 		RadioButtonList::iterator itCPRadio =	this->CPRadioButtonList.begin();
 		RadioButtonSetList::iterator itBCRadioSet =	this->BCRadioButtonSetList.begin();
 		
 		
 		char CoverLabelText[100];
 		
 		for (int i=0; i< this->NumberOfCoverGroups ; i++)
 	 		{
 	 		(*itFrameList)->SetParent(this->CoverTrianguleInfoFrame->GetFrame());
 	 		(*itPLabel)->SetParent((*itFrameList)->GetFrame());
 	 		(*itPEntry)->SetParent((*itFrameList)->GetFrame());

 	 		(*itVLabel)->SetParent((*itFrameList)->GetFrame());
 	 		(*itVEntry)->SetParent((*itFrameList)->GetFrame());
 	 		
 	 		(*itPRadio)->SetParent((*itFrameList)->GetFrame());
 	 		(*itVRadio)->SetParent((*itFrameList)->GetFrame());
 	 		(*itCPRadio)->SetParent((*itFrameList)->GetFrame());
 	 		
 	 		(*itBCRadioSet)->SetParent((*itFrameList)->GetFrame());
 	 		
 	 		(*itVEntry)->SetWidth(20);
 	 		(*itPEntry)->SetWidth(8);
 	 		
 	 		(*itFrameList)->Create(this->GetApplication());
 	 		(*itPLabel)->Create(this->GetApplication());
 	 		(*itPEntry)->Create(this->GetApplication());
 	 		
 	 		(*itVLabel)->Create(this->GetApplication());
 	 		(*itVEntry)->Create(this->GetApplication());
 	 		
 	 		(*itPRadio)->Create(this->GetApplication());
 	 		(*itVRadio)->Create(this->GetApplication());

 	 		
 	 		(*itCPRadio)->Create(this->GetApplication());

 	 		(*itBCRadioSet)->Create(this->GetApplication());
			(*itBCRadioSet)->SetReliefToGroove();
			(*itBCRadioSet)->SetBorderWidth(2);

			sprintf(CoverLabelText, "I/O Face %i Boundary Condition", i+1);	
 			(*itFrameList)->SetLabelText(CoverLabelText);
 			
 			this->Script("pack %s -fill x -expand true", (*itFrameList)->GetWidgetName()); 
 			
 			sprintf(CoverLabelText, "Pressure at Cover %i: ", i+1);
 	 		(*itPLabel)->SetText(CoverLabelText);
 	 		(*itPEntry)->SetValueAsDouble(0);
 	 		(*itPEntry)->SetCommand(this, "GeneralCallback");

 	 		sprintf(CoverLabelText, "Velocity at Cover %i in XYZ direction \n(separated by blank space): ", i+1);
 	 		(*itVLabel)->SetText(CoverLabelText);
 	 		(*itVEntry)->SetValue("0 0 0");
 	 		
 	 		 char data[50];
 	 		
 	 		(*itPRadio)->SetText("Pressure");
 	 		(*itVRadio)->SetText("Velocity");
 	 		(*itCPRadio)->SetText("Constante Pressure");
	 		
			for (int id=0; id < 3; id++)
				{
				sprintf(buffer, "Radiobutton %d", id);
				vtkKWRadioButton *radioB = (*itBCRadioSet)->AddWidget(id);
				
				radioB = (*itBCRadioSet)->GetWidget(id);

		  	sprintf(data, "UpdateBCWidgets %i;", i+1);

				(*itBCRadioSet)->GetWidget(id)->SetCommand(this, data);
				
				if (!id) radioB->SetText("Constant Pressure");
				if (id==1) radioB->SetText("Time Variant Pressure");
				if (id==2) radioB->SetText("Velocity");
				}


			this->Script("grid %s - - - -sticky w",	(*itBCRadioSet)->GetWidgetName());
	 		
 	 		// incrementar os iteradores	
 	 		itPLabel++;
 	 		itPEntry++;
 	 		itFrameList++;
 	 		itVLabel++;
 	 		itVEntry++;
 	 		itPRadio++;
 	 		itVRadio++;
 	 		itCPRadio++; 
 	 		itBCRadioSet++;
 	 		}
 	 	}
 	 	
 	 	
 	this->GroupSelectionCombo->ReadOnlyOn();
	char ComboText[50];
	for (int i=0 ; i < this->NumberOfCoverGroups + this->NumberOfWallGroups ; i++) // grupos das tampas mais grupos de superficie - nao representar grupo do volume
		{
		sprintf(ComboText, "%i", i);
		this->GroupSelectionCombo->AddValue(ComboText);
		}
	this->GroupSelectionCombo->SetValue("0");
	  
 	this->Script("grid %s %s - - -sticky ew",
  this->GroupSelectionLabel->GetWidgetName(),
	this->GroupSelectionCombo->GetWidgetName());
	
  this->Script("grid %s - - -sticky ew",
  this->GenerateSolverFilesPushButton->GetWidgetName());
	
}

// ----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());       
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::Accept()
{  		
	vtkDebugMacro(<<"void vtkPVHM3DFullModelFilter::Accept()");
	
	
	if (this->InterfaceCreated)
		this->GenerateSolverFilesPushButton->EnabledOn();

	if (!this->InterfaceCreated)
		{
	 	//pegando o numero de grupos de parede
	 	vtkSMIntVectorProperty *CoverGroupNumberProp = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("CoverGroups"));    
    this->NumberOfCoverGroups = CoverGroupNumberProp->GetElement(0);


		// pegando o numero de wall groups
		vtkSMIntVectorProperty *WallGroupNumberProp = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("WallGroups"));    
    this->NumberOfWallGroups = WallGroupNumberProp->GetElement(0);



		this->PlaceComponents();
		this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->StartColorButtonCallback(255,255,255);
		this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->EndColorButtonCallback(255,0,0); 			
    this->InterfaceCreated = true;
		
		
		}

  //acessando a propriedade que define o tipo de BC para cada cover	
  vtkSMStringVectorProperty *CoverPar = vtkSMStringVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("CoverElementsBCType"));    	
	if (CoverPar  && this->NumberOfCoverGroups)
  	{
  	char Result[200000] ={0};
  	char Temp[20] = {0};
  	
  	for (int i=0; i< this->NumberOfCoverGroups; i++)
  		////cout << this->CoverBCType->GetValue(i) << endl;
	  	{
	  	sprintf(Temp, "%i ", this->CoverBCType->GetValue(i));
	  	strcat(Result, Temp); 
	  	}
	  CoverPar->SetElement(0,Result);
  	}
  else
  	{
		vtkErrorMacro(<<"Cannot access FullModel CoverElementsBCType filter property.");	  	
		return;
  	}


	// Envia propriedade para os objetos no servidor
	// atualizando a propriedade "CoverElementsBCType" para ter certeza que ela sera a primeira a ser atualizada 
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();



  //*****************************************
  // acessando propriedade SurfaceElementProp	
	vtkSMStringVectorProperty *SurfPar = vtkSMStringVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("SurfaceElementProp"));    
  
	if (SurfPar) 
  	{
		//char Result[200000] = {0};
  	char Temp[100] = {0};
  	string Result;
		
		for (int i = 0; i < this->WallGroupCollection->GetNumberOfItems(); ++i)
			{
			vtkDoubleArray *temp = this->GetWallGroupParametersArray(i);
			for (int z = 0; z < 5; ++z)
				{
				sprintf(Temp, "%f;", temp->GetValue(z));
				Result.append(Temp); 	
				}
			}
		
		//cout << "Result : "<< Result.c_str() << endl;		
		SurfPar->SetElement(0,Result.c_str());
  	}
  else
  	{
		vtkErrorMacro(<<"Cannot create FullModel SurfaceElementProp filter property.");	  	
		return;
  	}
  	
  // ************************************
  // acessando prop. Cover
  // passando os valores de pressao constante associados as tampas 
  CoverPar = vtkSMStringVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("CoverElementPressure"));    	
	if (CoverPar) 
  	{
		char Result[2000] ={0};
  	char Temp[20] = {0};
		
		EntryList::iterator it2 = this->PEntryList.begin();
		
		for (int i=0; i < this->NumberOfCoverGroups; i++)
			{
			sprintf(Temp, "%f;", (*it2)->GetValueAsDouble());
			strcat(Result, Temp); 
			it2++;
			}

		// reseta char array
		for (int i=0; i<20;i++)
				Temp[i]=0;

		// acessa os radios buttons referente ao valor de pressao constante
		// para saber se valor de pressao constante estiver selecionado
		RadioButtonSetList::iterator it10 =	this->BCRadioButtonSetList.begin();
		for (;it10 != this->BCRadioButtonSetList.end(); it10++)
			{
			// acessa radioB referente a Pressao Constante
			vtkKWRadioButton *radioB = 	(*it10)->GetWidget(0);
			//sprintf(Temp, "%i;", radioB->GetSelectedState());
			// insere na string a ser enviada para DataServer 
			strcat(Result, Temp); 
			}

		CoverPar->SetElement(0,Result);
		}
  else
  	{
		vtkErrorMacro(<<"Cannot create FullModel CoverElementPressure filter property.");	  	
		return;
  	}

  // acessando prop. Cover 
  CoverPar = vtkSMStringVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("CoverElementVelocity"));    	
	if (CoverPar) 
  	{
		char Result[2000] ={0};
  	char Temp[20] = {0};
		
		EntryList::iterator it3 = this->VEntryList.begin();

		for (int i=0; i < this->NumberOfCoverGroups; i++)
			{
			strcat(Result, Temp); 
			strcat(Result, (*it3)->GetValue());
			strcat(Result, " ");
		  it3++;
			}
		CoverPar->SetElement(0,Result);
		}
  else
  	{
		vtkErrorMacro(<<"Cannot access FullModel CoverElementPressure filter property.");	  	
		return;
  	}
  
  
  //acessando a propriedade de pressao em funcao do tempo nas tampas	
  CoverPar = vtkSMStringVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("CoverElementTimeVariantPressure"));    	
	
	if (CoverPar && this->NumberOfCoverGroups)
		{
		char Result[200000] = {0};
  	char Temp[100] = {0};

		for (int i=0; i < this->NumberOfCoverGroups; i++) // percorre todos vetores da lista de doubleArrays que descrevem as curvas se a tampa esta selecionada com pressao variante
			{
			if 	(this->PressureCurveVector[i]->GetNumberOfTuples()) // se o vetor correspondente a tampa i tem curva de pressao
				{
				for (int z=0; z<this->PressureCurveVector[i]->GetNumberOfTuples(); z++)
					{
					vtkDoubleArray *item = this->PressureCurveVector[i];//vtkDoubleArray::SafeDownCast(this->PressureCurveArray->GetItem(i)); 
					double *dupla;
					dupla= item->GetTuple2(z);
					sprintf(Temp, "%f;%f ", dupla[0], dupla[1]);
					strcat(Result, Temp); 
					}
				strcat(Result, "*");
				}
			}
		CoverPar->SetElement(0,Result);
		} 
  else
  	{
		vtkErrorMacro(<<"Cannot access FullModel CoverElementTimeVariantPressure filter property.");	  	
		return;
  	}
  
	// Envia propriedade para os objetos no servidor
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
}

// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::UpdateGroupsCallback()
{
	// para evitar que entre 2 vezes seguidas aqui -- o metodo associado a um combo box é chamado quando o valor do combo é modificado e quando o usuario
	// tira o mouse da regiao do combo (out focus) entao este callback é chamado 2 vezes.
	if (this->GroupSelectionCombo->GetValueAsInt() != this->CurrentGroup)
		// atualiza grupo atualmente selecionado
		this->CurrentGroup = this->GroupSelectionCombo->GetValueAsInt();	
	else
		return; // se esta for a segunda vez que entra aqui, entao retorna
	
	this->UpdateInterface(); 
	
  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("SelectGroup"));    
  if (prop) 
    {
		prop->SetElement(0,this->GroupSelectionCombo->GetValueAsInt());
	  }
	else
	  {
		vtkErrorMacro(<<"Cannot create Group filter property.");	  	
  	return;
	  }

  this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();

  if ( this->GetPVSource()->GetPVRenderView() )
    {
    this->GetPVSource()->GetPVRenderView()->EventuallyRender();
    }

//	if (this->GroupSelectionCombo->GetValueAsInt() == this->NumberOfCoverGroups + 1) // Volume Selected
//		this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->StartColorButtonCallback(255,0,0);
//	else																																						 // Surface Selected
	
	// este modulo lida somente com visualizacao de grupos de parede e tampas
	this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->StartColorButtonCallback(255,255,255);

	// Set Final color of the Color Bar
	this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->EndColorButtonCallback(255,0,0); 	
  
  this->GetPVSource()->PreAcceptCallback();
  
  this->GeneralCallback();
  
  this->UpdateVariantPressureWidgets();
}	

//----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::Initialize()
{
}   

//----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::UpdateInterface()
{
	int SelectedGroup = this->GroupSelectionCombo->GetValueAsInt();
	
	
	if (SelectedGroup && this->CoverBCType->GetValue(SelectedGroup-1)!= 2)
		this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
	else
		{
		this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
		  this->Script("pack %s -fill x -expand true",  
    this->VariantPressureOptionsFrame->GetWidgetName());
    //this->PointNumber->GetWidget()->SetValueAsInt(this->PressureCurveVector[this->GroupSelectionCombo->GetValueAsInt()-1]->GetNumberOfTuples());
		}
		
 	if (SelectedGroup < this->NumberOfWallGroups)
 		{
 		this->Script("pack forget %s %s ", 
		//this->TetrahedronInfoFrame->GetWidgetName(),
		this->CoverTrianguleInfoFrame->GetWidgetName(),
 		this->SurfaceTrianguleInfoFrame->GetWidgetName());
		
	  this->Script("pack %s -fill x -expand true",  
   	this->SurfaceTrianguleInfoFrame->GetWidgetName());
 		
 		this->SurfaceTrianguleInfoFrame->ExpandFrame();
 		
 		char FrameText[50];
 		
 		sprintf(FrameText, "Wall Parameters Group %i", SelectedGroup);
 		
 		this->SurfaceTrianguleInfoFrame->SetLabelText(FrameText); 
 		
 		vtkDoubleArray *WallParam = this->GetWallGroupParametersArray(SelectedGroup);
 		
 		this->SurfaceElastinEntry->SetValueAsDouble(WallParam->GetValue(0));
		this->SurfaceCurveRadiusEntry->SetValueAsDouble(WallParam->GetValue(1));
		this->SurfaceWallthicknessEntry->SetValueAsDouble(WallParam->GetValue(2));
  	this->SurfaceRefPressureEntry->SetValueAsDouble(WallParam->GetValue(3));
  	this->SurfaceViscoEntry->SetValueAsDouble(WallParam->GetValue(4));
 		}
 	else	// TAMPAS
	//if (SelectedGroup >= 1 && SelectedGroup <= this->NumberOfCoverGroups)
		{
		SelectedGroup = SelectedGroup - this->NumberOfWallGroups + 1;
		
		
		this->Script("pack forget %s %s ", 
		//this->TetrahedronInfoFrame->GetWidgetName(),
		this->CoverTrianguleInfoFrame->GetWidgetName(),
 		this->SurfaceTrianguleInfoFrame->GetWidgetName());
		
			
	  this->Script("pack %s -fill x -expand true",  
		this->CoverTrianguleInfoFrame->GetWidgetName());
		
		this->CoverTrianguleInfoFrame->ExpandFrame();
		
		EntryList::iterator it = this->PEntryList.begin();
		LabelList::iterator it2 = this->PLabelList.begin();
		FrameList::iterator it3 = this->CoverFrameList.begin();
 		
 		char CoverLabelText[50];
 		int i = 1;	
 		for (;it != this->PEntryList.end(); it++)
 	 		{
 	 		(*it)->SetReadOnly(1);
 			sprintf(CoverLabelText, "Pressure at Cover %i: ", i);
 	 		(*it2)->SetText(CoverLabelText);
 	 		(*it3)->CollapseFrame();
 	 		
 	 		if (i== SelectedGroup)
 	 			{
 	 			(*it)->SetReadOnly(0);
 	 			sprintf(CoverLabelText, "Constant Pressure at Cover %i (in red): ", i);
 	 			(*it2)->SetText(CoverLabelText);
 	 			(*it3)->ExpandFrame();
 	 			}
 	 		i++;
 	 		it2++;
 	 		it3++;
 	 		}
 	 	}

}

//----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::GeneralCallback()
{
  this->ModifiedCallback();

//	this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->VolumeOption->GetWidgetName(),
//  this->GetTclName()) ;
//     
//     
//  this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->SurfaceOption->GetWidgetName(),
//  this->GetTclName()) ;
//    
//
//  this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->CoverOption->GetWidgetName(),
//  this->GetTclName()) ;
//    
//    
//  // Set binds for surface entries
//  this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->SurfaceElastinEntry->GetWidgetName(),
//  this->GetTclName()) ;
//    
//  this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->SurfaceRefPressureEntry->GetWidgetName(),
//  this->GetTclName()) ;
//    
//    
//  this->Script("bind %s <Key> {%s SetValueChanged}",
//  this->SurfaceViscoEntry->GetWidgetName(),
//  this->GetTclName()) ;

}

//----------------------------------------------------------------------------

//void vtkPVHM3DFullModelFilter::SetValueChanged()
//{
//  this->ValueChanged = 1;
//  this->ModifiedCallback();
//}

//----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::GenerateSolverFiles()
{
		
	if (!this->GeneralConfiguration)
		{
	 	this->GeneralConfiguration = vtkPVHM3DSolverFilesConfigurationWidget::New();

		this->GeneralConfiguration->SetModelType("3D Model");
	  this->GeneralConfiguration->SetModal(1);
	  this->GeneralConfiguration->SetMasterWindow(this->Window);
	  this->GeneralConfiguration->SetPvWindow(this->Window);
		this->GeneralConfiguration->Create(this->kwApp);
	  this->GeneralConfiguration->SetDisplayPositionToMasterWindowCenterFirst();

		//                                 Larg X altura
		this->GeneralConfiguration->SetSize(790, 510);
	  this->GeneralConfiguration->Display();
	  this->GeneralConfiguration->SetDisplayPositionToMasterWindowCenterFirst();

	  this->GeneralConfiguration->SetPVSource(this->GetPVSource());
	  //this->GeneralConfiguration->SetFinalTime(this->FinalTime->GetWidget()->GetValueAsDouble());
		}
  else
  	{
  	//this->GeneralConfiguration->SetFinalTime(this->FinalTime->GetWidget()->GetValueAsDouble());
  	this->GeneralConfiguration->Display();
  	}
}

// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::UpdateBCWidgets(int cover)
{
	this->GeneralCallback();
	//cout << "vtkPVHM3DFullModelFilter::UpdateBCWidgets(int cover) " << cover << endl;
	
	LabelList::iterator it = this->PLabelList.begin();
 	EntryList::iterator it2 = this->PEntryList.begin();
	FrameList::iterator it3 = this->CoverFrameList.begin();
 	LabelList::iterator it4 = this->VLabelList.begin();
 	EntryList::iterator it5 = this->VEntryList.begin();
	RadioButtonList::iterator it6 =	this->PRadioButtonList.begin();
 	RadioButtonList::iterator it7 =	this->VRadioButtonList.begin();
	RadioButtonList::iterator it9 =	this->CPRadioButtonList.begin();
	RadioButtonSetList::iterator it10 =	this->BCRadioButtonSetList.begin();
	
	
	this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
	
 	int i=1;	
	for (;it10 != this->BCRadioButtonSetList.end(); it10++)
		{
		if (i==cover)
			{
			for (int id=0; id<3; id++)
				{
				vtkKWRadioButton *radioB = 	(*it10)->GetWidget(id);	
			
				if (radioB->GetSelectedState())
					{
					if (!id)
						{
						this->CoverBCType->SetValue(this->GroupSelectionCombo->GetValueAsInt()-this->NumberOfWallGroups, 1);
						this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
						this->Script("grid remove %s %s" ,	(*it4)->GetWidgetName(), (*it5)->GetWidgetName() );	
						this->Script("grid %s %s - - - -sticky w",	(*it)->GetWidgetName(), (*it2)->GetWidgetName() );	
						}
					if (id==1)
						{	
						this->CoverBCType->SetValue(this->GroupSelectionCombo->GetValueAsInt()-this->NumberOfWallGroups, 2);
						this->Script("grid remove %s %s" ,	(*it)->GetWidgetName(), (*it2)->GetWidgetName() );	
						this->Script("grid remove %s %s" ,	(*it4)->GetWidgetName(), (*it5)->GetWidgetName() );	
						this->Script("pack %s -fill x -expand true",this->VariantPressureOptionsFrame->GetWidgetName());
						}
					if (id==2)
						{	
						this->CoverBCType->SetValue(this->GroupSelectionCombo->GetValueAsInt()-this->NumberOfWallGroups, 3);
						this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
						this->Script("grid remove %s %s" ,	(*it)->GetWidgetName(), (*it2)->GetWidgetName());	
						this->Script("grid %s - - - -sticky w",	(*it4)->GetWidgetName() );	
						this->Script("grid %s - - - -sticky w",	(*it5)->GetWidgetName() );
						}
					}
				}
			}
		it4++;
		it5++;
		it++;
		it2++;
		i++;
		}
}
// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::ShowPressurePlot()
{
	this->UpdateCurveButton->SetBackgroundColor(0, 0.6, 0);
	
	this->XYPlotWidget->SetMaxFlow(this->MaxValue->GetWidget()->GetValueAsDouble());
	
	int CurrentCover = this->GroupSelectionCombo->GetValueAsInt()-this->NumberOfWallGroups;

	char temp[100];
	 sprintf(temp, "Pressure Curve at Cover %i",CurrentCover+1 );
	this->XYPlotWidget->SetTitle(temp);
	
	this->CalculateCurve(this->Amplitude->GetWidget()->GetValueAsDouble() , this->FinalTime->GetWidget()->GetValueAsDouble());
		
	vtkDoubleArray *time = vtkDoubleArray::New();
	vtkDoubleArray *values = vtkDoubleArray::New();
	
	time->SetNumberOfValues(this->PressureCurveVector[CurrentCover]->GetNumberOfTuples());
	values->SetNumberOfValues(this->PressureCurveVector[CurrentCover]->GetNumberOfTuples());
	
	double minimo=0;
	
	for (int i=0; i< this->PressureCurveVector[CurrentCover]->GetNumberOfTuples(); i++)
		{
		time->SetValue(i, this->PressureCurveVector[CurrentCover]->GetTuple2(i)[0]);  	
		values->SetValue(i, this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1]);
		
		if( this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1] < minimo)
			minimo= this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1];
		}
	this->XYPlotWidget->SetMaxFlow(this->MaxValue->GetWidget()->GetValueAsDouble());
	
	this->XYPlotWidget->SetMinFlow(this->MinValue->GetWidget()->GetValueAsDouble());
	
	this->XYPlotWidget->SetEditCurveHeart(0);
	this->XYPlotWidget->SetTMin(0);
	this->XYPlotWidget->SetTMax(this->FinalTime->GetWidget()->GetValueAsDouble() );


	this->XYPlotWidget->BuildHeartPlot(1, values, time);
	this->XYPlotWidget->ShowHMXYPlotWidget();
	
	time->Delete();
	values->Delete();
}
// ----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::UpdatePressureCurve()
{
	this->UpdateCurveButton->SetBackgroundColor(0.752941, 0.752941, 0.752941);
	
	this->GeneralCallback();
	this->XYPlotWidget->UpdateHeartCurve();
	
	int CurrentCover = this->GroupSelectionCombo->GetValueAsInt()-this->NumberOfWallGroups;
	
	int ArraySize = this->XYPlotWidget->GetTime()->GetNumberOfTuples();
	
	this->PressureCurveVector[CurrentCover]->SetNumberOfTuples(ArraySize);
	this->PressureCurveVector[CurrentCover]->SetNumberOfComponents(2);
	
	for (int i=0; i< ArraySize; i++)
		{
		double val1= this->XYPlotWidget->GetTime()->GetValue(i);
		double val2=	this->XYPlotWidget->GetHeartCurve()->GetValue(i);
		this->PressureCurveVector[CurrentCover]->InsertTuple2(i, val1, val2);
		}
}
// ----------------------------------------------------------------------------
int vtkPVHM3DFullModelFilter::GetNumberOfCoversWithBC(int BCType)
{
	int cover=0;
	for (int i=0; i< this->NumberOfCoverGroups; i++)
  	{
  	 if (this->CoverBCType->GetValue(i)==BCType)
  	 	cover++;
	  }

	return cover;
}
// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::CalculateCurve(double Amplitude, double time)
{
	double f;
	double n;
	double x = 0;
	int CurrentCover = this->GroupSelectionCombo->GetValueAsInt() - this->NumberOfWallGroups;
	int select = this->GetFunctionSelected();
	
	int numberOfPoints = this->PointNumber->GetWidget()->GetValueAsInt();
	
	
	if ( numberOfPoints < 2 )
		numberOfPoints = 2;
	
	//Calcula o intervalo de tempo entre um ponto e outro
	n = time/(numberOfPoints-1);
	
	this->PressureCurveVector[CurrentCover]->SetNumberOfComponents(2);
	
	
	this->PressureCurveVector[CurrentCover]->SetNumberOfTuples(numberOfPoints);
	
	switch(select)
	{
		/*
		 * Caso nenhuma das funções esteja selecionada, adiciona dois pontos na curva.
		 */
		case 0:
			
			if (this->AddCurveButton->GetFileName() ||  this->PressureCurveVector[CurrentCover]->GetNumberOfTuples()    ) // se alguma funcao é lida do disco
				{
				
				//if (this->PressureCurveVector[CurrentCover]->GetNumberOfTuples() == 1)
				if (this->ReadPressureCurveFromFile) // se curva foi lida de arquivo
					{	
					for (int i=0; i< this->CurveFromFile->GetNumberOfTuples(); i++)	
						this->PressureCurveVector[CurrentCover]->SetTuple2(i, this->CurveFromFile->GetTuple2(i)[0], this->CurveFromFile->GetTuple2(i)[1]);
					
					this->ReadPressureCurveFromFile = false;
					}
				break;
				}
			else
				{
				this->PressureCurveVector[CurrentCover]->SetNumberOfTuples(2);
				this->PressureCurveVector[CurrentCover]->SetTuple2(0, 0, 0);
				this->PressureCurveVector[CurrentCover]->SetTuple2(1, time, 0);
				break;
				}
		
		case 1:
			/*
			 * Calculate function sen(x)
			 * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*sin(2*(pi/T)*x)
			 */
			Amplitude = Amplitude/2;
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * sin(2*(vtkMath::Pi() / time ) * x);
				f = f + this->SurfaceRefPressureEntry->GetValueAsDouble();
				this->PressureCurveVector[CurrentCover]->SetTuple2(i, x, f);
				x += n;
				}
			break;
		
		case 2:
			/*
			 * Calculate function sen^2 (x) 1/2 period
			 * * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*(sin((pi/T)*x))^2
			 */
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * pow((sin((vtkMath::Pi() / time ) * x)),2);
				f = f+ this->SurfaceRefPressureEntry->GetValueAsDouble();
				this->PressureCurveVector[CurrentCover]->SetTuple2(i, x, f);
				x += n;
				}
			break;
		
		case 3:
			/*
			 * Calculate function sen^2 (x)
			 * * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*(sin(2*(pi/T)*x))^2
			 */
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * pow((sin(2*(vtkMath::Pi() / time ) * x)),2);
				f = f + this->SurfaceRefPressureEntry->GetValueAsDouble();
				this->PressureCurveVector[CurrentCover]->SetTuple2(i, x, f);
				x += n;
				}
			break;
		
		default:
			
			
			break;
		
	}
	//return array;
}

// ----------------------------------------------------------------------------


int vtkPVHM3DFullModelFilter::GetFunctionSelected()
{
	if (!strcmp(this->FunctionHeartCurve->GetValue(), "None"))
		return 0;
		
	if (!strcmp(this->FunctionHeartCurve->GetValue(), "Sen(x)") )
		return 1;

	if (!strcmp(this->FunctionHeartCurve->GetValue(), "sen^2(x) 1/2 period"))
		return 2;
	
	if (!strcmp(this->FunctionHeartCurve->GetValue(), "sen^2(x)"))
		return 3;
}

// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::ReadCurve()
{
	this->AddCurveButtonOk->SetBackgroundColor(0.752941, 0.752941, 0.752941); //cinza
	
	int result;
	char *FileName;
	FileName = this->AddCurveButton->GetFileName();
	
	vtkHM1DHeartCurveReader *reader = vtkHM1DHeartCurveReader::New();

	result = reader->ReadFile(FileName);
	char *str = this->AddCurveButton->GetText();
	
	vtkDoubleArray *p, *pTime;
	
	if (!reader->GetSingleCurve()) // se arquivo possui as 4 curvas 
		{	
		//Pegar os parametros lidos do arquivo
		p  = reader->GetP();
		pTime  = reader->GetPTime();
		}
	else // se arquivo possui apenas uma curva
		{
		p  = reader->GetGeneric();
		pTime  = reader->GetGenericTime();
		}	
		
	if (!this->CurveFromFile)
		this->CurveFromFile = vtkDoubleArray::New();
		
		
	this->CurveFromFile->SetNumberOfComponents(2);
	this->CurveFromFile->SetNumberOfTuples(p->GetNumberOfTuples());
	
	double Maximum = 0; 
	double Minimum = 0; 
	double Time = 0;
	
	for (int i=0; i< p->GetNumberOfTuples(); i++)	
		{
		if (p->GetValue(i) > Maximum)
			Maximum = p->GetValue(i);

		if (p->GetValue(i) < Minimum)
			Minimum = p->GetValue(i);

		if (pTime->GetValue(i) > Time)
			Time = pTime->GetValue(i);
		
		this->CurveFromFile->SetTuple2(i, pTime->GetValue(i), p->GetValue(i));
		}
	this->MaxValue->GetWidget()->SetValueAsDouble(Maximum);
	this->MinValue->GetWidget()->SetValueAsDouble(Minimum);
	this->PointNumber->GetWidget()->SetValueAsInt(p->GetNumberOfTuples());
	this->FinalTime->GetWidget()->SetValueAsDouble(Time);
	reader->Delete();
	
	this->ReadPressureCurveFromFile = true;
	
}
// ----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::UpdateVariantPressureWidgets()
{
	int CurrentGroup = this->GroupSelectionCombo->GetValueAsInt();
	int CurrentCover = CurrentGroup - this->NumberOfWallGroups;
	
				
	double Maximum = 0.0; 
	double Minimum = 0.0; 
	double Time = 0.0;
	
	if (CurrentCover >= 0)
		{
		if ((CoverBCType->GetValue(CurrentCover)==2) && this->PressureCurveVector[CurrentCover]->GetNumberOfTuples())
			{
			this->PointNumber->GetWidget()->SetValueAsInt(this->PressureCurveVector[CurrentCover]->GetNumberOfTuples());	
			for (int i=0; i< this->PressureCurveVector[CurrentCover]->GetNumberOfTuples(); i++)	
				{
				if (this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1] > Maximum)
					Maximum = this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1];

				if (this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1] < Minimum)
					Minimum = this->PressureCurveVector[CurrentCover]->GetTuple2(i)[1];

				if (this->PressureCurveVector[CurrentCover]->GetTuple2(i)[0] > Time)
					Time = this->PressureCurveVector[CurrentCover]->GetTuple2(i)[0];
				}
			this->FinalTime->GetWidget()->SetValueAsDouble(Time);
			this->MaxValue->GetWidget()->SetValueAsDouble(Maximum);
			this->MinValue->GetWidget()->SetValueAsDouble(Minimum);
			
			this->AddCurveButton->SetText("Custom Function");
			
			this->Script("pack %s -fill x -expand true",this->VariantPressureOptionsFrame->GetWidgetName());
			}
		else
			{
			// limpa os campos
			this->FunctionHeartCurve->SetValue("None");
			this->AddCurveButton->SetText("Read Function");
			this->AddCurveButton->GetLoadSaveDialog()->SetFileName(0);   	
			this->PointNumber->GetWidget()->SetValueAsInt(0);	
			this->FinalTime->GetWidget()->SetValueAsDouble(Time);
			this->MaxValue->GetWidget()->SetValueAsDouble(Maximum);
			this->MinValue->GetWidget()->SetValueAsDouble(Minimum);
			
			
			this->Script("pack forget %s" ,	this->VariantPressureOptionsFrame->GetWidgetName() );	
			}	
		}
}
// ----------------------------------------------------------------------------
void vtkPVHM3DFullModelFilter::UpdateReadCurveButton()
{
	this->AddCurveButtonOk->SetBackgroundColor(0, 0.6, 0);	//verde
  this->Amplitude->GetWidget()->ReadOnlyOn();
}

// ----------------------------------------------------------------------------

void vtkPVHM3DFullModelFilter::SetWallGroupParameters(int Group)
{
	vtkDoubleArray *temp = vtkDoubleArray::SafeDownCast(this->WallGroupCollection->GetItem(Group));
	
	temp->SetValue(0,this->SurfaceElastinEntry->GetValueAsDouble());
	temp->SetValue(1,this->SurfaceCurveRadiusEntry->GetValueAsDouble());
	temp->SetValue(2,this->SurfaceWallthicknessEntry->GetValueAsDouble());
  temp->SetValue(3,this->SurfaceRefPressureEntry->GetValueAsDouble());
  temp->SetValue(4,this->SurfaceViscoEntry->GetValueAsDouble());
}
// ----------------------------------------------------------------------------


void vtkPVHM3DFullModelFilter::SetWallParamCallBack()
{
	this->UpdateSurfacePropPushButton->Focus();
	this->SetWallGroupParameters(this->GroupSelectionCombo->GetValueAsInt());
	this->ModifiedCallback();
}
// ----------------------------------------------------------------------------


vtkDoubleArray *vtkPVHM3DFullModelFilter::GetWallGroupParametersArray(int Group)
{
	vtkDoubleArray *temp = vtkDoubleArray::SafeDownCast(this->WallGroupCollection->GetItem(Group));
	return temp;
}

// ----------------------------------------------------------------------------

