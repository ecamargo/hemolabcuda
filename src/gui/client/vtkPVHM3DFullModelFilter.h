
#ifndef _vtkPVHM3DFullModelFilter_h_
#define _vtkPVHM3DFullModelFilter_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"
#include "vtkPVHM3DSolverFilesConfigurationWidget.h"
#include "vtkKWRadioButton.h"
#include "vtkKWHM1DXYPlotWidget.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWPushButton.h"
#include <list>
#include <vector>

class vtkKWPushButton;  
class vtkKWEntry;
class vtkKWLabel;
class vtkKWFrameWithLabel;
class vtkKWComboBox;
class vtkKWEntry;
class vtkKWPushButton;
class vtkKWLoadSaveDialog;
class vtkDataArrayCollection;

class VTK_EXPORT vtkPVHM3DFullModelFilter : public vtkPVObjectWidget
{
public:

	//BTX
	typedef std::list<vtkKWLabel *> LabelList;
	typedef std::list<vtkKWEntry *> EntryList;
	typedef std::list<vtkKWFrameWithLabel *> FrameList;
	typedef std::list<vtkKWRadioButton *> RadioButtonList;
	typedef std::list<vtkKWRadioButtonSet *> RadioButtonSetList;
	typedef std::vector<vtkDoubleArray *> DoubleArrayVector;
	//ETX

  static vtkPVHM3DFullModelFilter *New();
  vtkTypeRevisionMacro(vtkPVHM3DFullModelFilter,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents();
  void ConfigureComponents(); 	
	
  // Description:
  // Call creation on the child.  
	virtual void 	Create (vtkKWApplication *app); 

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
    
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

	// Description:
	// Callback for Group Selection
	void UpdateGroupsCallback();

	// Description:	
	// expand the frame related to the current selected group  
	void UpdateInterface();

	// Description:	
	// Select group by pointing an cell and pressing 'p'  
  //	void CheckMouseSelection();
	
	// Description:	
	// 
	void GeneralCallback();
	
	
  // Description:	
	// Open the solver generation file window 
	void GenerateSolverFiles();
	
	
  // Description:	
	// update the boundary condition related widgets given the selected group
	void UpdateBCWidgets(int cover);

  // Description:	
	// show pressure curve 
	void ShowPressurePlot();

  // Description:	
	// record pressure curve 
	void UpdatePressureCurve();
	
  // Description:	
	// return the number of covers with a specifique BC  	
	int GetNumberOfCoversWithBC(int BCType);
	
  // Description:	
	// Calculate curve points
	void CalculateCurve(double Amplitude, double time);
	
  // Description:	
	// return 0 if "None" is selected
	// return 1 if "Sen(x)" is selected
	// return 2 if "sen^2(x) 1/2 period" is selected
	// return 3 if "sen^2(x) 1/2 period" is selected
	int GetFunctionSelected();
	
  // Description:	
	// read pressure curve from file
	void ReadCurve();
	
	
  // Description:	
	// update pressure curve related widgets - nou used at moment
	void UpdateVariantPressureWidgets();
	
  // Description:	
	// callback method 
	void UpdateReadCurveButton();
	
	
  // Description:	
	// callback method called when user wants to set some wall group parameters
	void SetWallGroupParameters(int Group);
	
  // Description:	
	// callback method called when user wants to set some wall group parameters
	void SetWallParamCallBack();	
		

  // Description:	
	// retunr the vtkDoubleArray object with parametrs values from some wall group
	vtkDoubleArray *GetWallGroupParametersArray(int Group);


//********************************************************************************************************************
//********************************************************************************************************************
//********************************************************************************************************************
//********************************************************************************************************************//
  
protected:
  vtkPVHM3DFullModelFilter();
  virtual ~vtkPVHM3DFullModelFilter();
  
	vtkKWFrameWithLabel *Frame;	  
  
	//vtkKWLabel	*VolumeElementTypeLabel;
	vtkKWLabel	*SurfaceElementTypeLabel;
	vtkKWLabel	*CoverElementTypeLabel;
	
	//vtkKWComboBox *VolumeOption;
	vtkKWComboBox *SurfaceOption;
	vtkKWComboBox *CoverOption;
	
	vtkKWFrameWithLabel *GroupSelectionFrame;
	//vtkKWFrameWithLabel *TetrahedronInfoFrame;
	vtkKWFrameWithLabel *SurfaceTrianguleInfoFrame;	  
	vtkKWFrameWithLabel *CoverTrianguleInfoFrame;	
	vtkKWFrameWithLabel *GeneralOptionsFrame;	  

	//***************************************************************************************
	// parametros para os elementos da superficie
	//***************************************************************************************

	vtkKWLabel	*SurfaceElastinLabel;     
	vtkKWEntry *SurfaceElastinEntry;
	
	vtkKWLabel	*SurfaceCurveRadiusLabel;     
	vtkKWEntry *SurfaceCurveRadiusEntry;
	
	vtkKWLabel	*SurfaceWallthicknessLabel;     
	vtkKWEntry *SurfaceWallthicknessEntry;
	
	
	vtkKWLabel	*SurfaceRefPressureLabel;     
	vtkKWEntry *SurfaceRefPressureEntry;
		  
	vtkKWLabel	*SurfaceViscoLabel;     
	vtkKWEntry *SurfaceViscoEntry;


  //**** elementos da interface quando pressao variante
  vtkKWFrameWithLabel *VariantPressureOptionsFrame;	  
  vtkKWComboBox *FunctionHeartCurve; 
  vtkKWLoadSaveButton *AddCurveButton;
  vtkKWPushButton *AddCurveButtonOk;
  vtkKWEntryWithLabel *PointNumber;
  vtkKWEntryWithLabel *Amplitude;
 
  vtkKWEntryWithLabel *FinalTime;
  vtkKWEntryWithLabel *MaxValue;
  vtkKWEntryWithLabel *MinValue;
  vtkKWPushButton *PlotButton;
  vtkKWPushButton *UpdateCurveButton;
  
  //**********************
	
	LabelList PLabelList;
	EntryList PEntryList;
	
	LabelList VLabelList;
	EntryList VEntryList;
	
	RadioButtonList CPRadioButtonList;
	RadioButtonList PRadioButtonList;
	RadioButtonList VRadioButtonList;
	
	RadioButtonSetList BCRadioButtonSetList;
		
	FrameList CoverFrameList;
	
	
	// Description:	
	// ivar indicates the number of cover groups from surface 
	int NumberOfCoverGroups;
	
	
	// Description:	
	// ivar indicates the number of groups on the wall from surface 
	int NumberOfWallGroups;
	
	vtkKWLabel *GroupSelectionLabel;
	vtkKWComboBox *GroupSelectionCombo;
	
	bool	InterfaceCreated;
	
	vtkPVWindow *Window;
	vtkPVApplication* pvApp;
	vtkKWApplication *kwApp;
	
	vtkKWPushButton *GenerateSolverFilesPushButton;
	vtkPVHM3DSolverFilesConfigurationWidget *GeneralConfiguration;
	
	vtkKWHM1DXYPlotWidget *XYPlotWidget;
	
	
	// aqui é armazenada uma lista de vtkDoubleArrays que irao armazenar as curvas quando tivermos
	// p=f(t)
	//  (*vtkDoubleArrays 0 - cover 1; *vtkDoubleArrays 1 - cover 2 - .... n = numero de tampas com p=f(t)
	// cada DoubleArray é composta de duplas de valores que especificam (tempo, valor)
	//vtkDataArrayCollection *PressureCurveArray;
	
	
	// Descrition:
	// Dialog used by the user to choose a directory where Solver input files will be generated
  vtkKWLoadSaveDialog *ChooseDirectoryDialog;
	
	
	vtkIntArray *CoverBCType;
	DoubleArrayVector PressureCurveVector;
	
	vtkDoubleArray *CurveFromFile;
	
	vtkDataArrayCollection *WallGroupCollection;
	
	vtkKWPushButton *UpdateSurfacePropPushButton;
	
	// Descrition:
	// ivar used to control behaviour of combo box that control the visualization of the current group selected.
	int CurrentGroup;
	 
	 
	bool ReadPressureCurveFromFile; 	
private:  
  vtkPVHM3DFullModelFilter(const vtkPVHM3DFullModelFilter&); // Not implemented
  void operator=(const vtkPVHM3DFullModelFilter&); // Not implemented
}; 

#endif  /*_vtkPVHM3DFullModelFilter_h_*/

