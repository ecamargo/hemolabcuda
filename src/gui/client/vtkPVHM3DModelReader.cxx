/*
 * $Id:$
 */
 
#include "vtkPVHM3DModelReader.h"
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"
	
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWComboBoxWithLabel.h"
#include "vtkKWComboBox.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"


vtkCxxRevisionMacro(vtkPVHM3DModelReader, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DModelReader);

//----------------------------------------------------------------------------
vtkPVHM3DModelReader::vtkPVHM3DModelReader()
{ 
	this->MyFrame	    = vtkKWFrameWithLabel::New();

	// Configuration options
  this->ModelLabel = vtkKWLabel::New();
  this->ModelUsedLabel = vtkKWLabel::New();
  this->Number3DObjectsLabel = vtkKWLabel::New();
  this->Config3DObjectLabel = vtkKWComboBoxWithLabel::New();  	
}

//----------------------------------------------------------------------------
vtkPVHM3DModelReader::~vtkPVHM3DModelReader()
{ 
  this->MyFrame->Delete();
  this->ModelLabel->Delete();
  this->ModelUsedLabel->Delete();
  this->Number3DObjectsLabel->Delete();
  this->Config3DObjectLabel->Delete();
}

void vtkPVHM3DModelReader::Config3DObjectLabelCallBack()
{
	int value;
  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
 	                             this->GetPVSource()->GetProxy()->GetProperty("Select3DObject"));    
	if (prop)
		{
		value = this->Config3DObjectLabel->GetWidget()->GetValueAsInt();
		prop->SetElement(0, value);
		}
  else
	 	{
	 	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
	 		return;
	  }
  
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DModelReader::Create(vtkKWApplication* app)
{
	vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->MyFrame->SetParent(this);
  this->MyFrame->Create(pvApp);
  this->MyFrame->SetLabelText("HeMoLab 3D - Couple Model Reader");
  
  this->ModelUsedLabel->SetParent(this->MyFrame->GetFrame());
  this->ModelUsedLabel->Create(pvApp);
	this->ModelUsedLabel->SetText("Model Used:");
  
  this->ModelLabel->SetParent(this->MyFrame->GetFrame());
  this->ModelLabel->Create(pvApp);
  
  this->Config3DObjectLabel->SetParent(this->MyFrame->GetFrame());
  this->Config3DObjectLabel->Create(pvApp);
  this->Config3DObjectLabel->SetLabelText("Select 3D Object");
  this->Config3DObjectLabel->GetWidget()->SetCommand(this, "Config3DObjectLabelCallBack");
  this->Config3DObjectLabel->GetWidget()->SetValue("0");
  
	//-------------------------------------------------------------------------------------      
	//Place components after accept
  this->PlaceComponents();
  this->ConfigureComponents(); 
}

//------------------------------------------------------------------------------
void vtkPVHM3DModelReader::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHM3DModelReader::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",
    this->MyFrame->GetWidgetName());

  this->Script("grid %s %s - - -sticky ew",
		this->ModelUsedLabel->GetWidgetName(),
		this->ModelLabel->GetWidgetName());
  
  this->Script("grid %s - - -sticky ew",
		this->Config3DObjectLabel->GetWidgetName());
}

// ----------------------------------------------------------------------------
void vtkPVHM3DModelReader::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->MyFrame->GetWidgetName());
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHM3DModelReader::Accept()
{  	
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(0, 0, 1);	 
  
  vtkSMIntVectorProperty *prop_IsCoupleModel = vtkSMIntVectorProperty::SafeDownCast(
    		this->GetPVSource()->GetProxy()->GetProperty("IsCoupleModel")); 
  
  if(prop_IsCoupleModel->GetElement(0))
  	{
  	this->ModelLabel->SetText("Couple Model");
  	}
  else
  	{
  	this->ModelLabel->SetText("3D Model");
  	}
  
  vtkSMIntVectorProperty *prop_NumberOf3DObjects = vtkSMIntVectorProperty::SafeDownCast(
  		this->GetPVSource()->GetProxy()->GetProperty("NumberOf3DObjects"));  
  
	char option[5];
  for(int i=0; i < prop_NumberOf3DObjects->GetElement(0); i++)
  	{
  	sprintf(option,"%d", i); 
  	this->Config3DObjectLabel->GetWidget()->AddValue(option);
  	}  
  
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();
}

//----------------------------------------------------------------------------
void vtkPVHM3DModelReader::Initialize()
{
}  

