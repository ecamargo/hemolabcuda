/*
 * $Id:$
 */

// .NAME vtkPVHM3DModelReader
// .SECTION Description
// Interface for 3D/Couple Model reader

#ifndef VTKPVHM3DMODELREADER_H_
#define VTKPVHM3DMODELREADER_H_


#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"
//#include "vtkPVAnimationManager.h"
//#include "vtkPVAnimationScene.h"

class vtkKWLabel;
class vtkKWFrameWithLabel;
class vtkKWComboBoxWithLabel;

class VTK_EXPORT vtkPVHM3DModelReader : public vtkPVObjectWidget
{
public:
  static vtkPVHM3DModelReader *New();
  vtkTypeRevisionMacro(vtkPVHM3DModelReader,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents();
  void ConfigureComponents(); 	
	
  // Description:
  // Call creation on the child.  
  virtual void 	Create (vtkKWApplication *app); 

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
    
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:  
  // Show that something was changed
  void SetValueChanged();
  
  // Description:  
	// Menu and load file callback	
	//void ConfigurationMenuCallback();
	void Config3DObjectLabelCallBack();

protected:
  vtkPVHM3DModelReader();
  virtual ~vtkPVHM3DModelReader();
  
  vtkKWFrameWithLabel *MyFrame;	 
  
  vtkKWLabel* 					ModelLabel;
  vtkKWLabel* 					ModelUsedLabel;
  vtkKWLabel* 					Number3DObjectsLabel;
  vtkKWComboBoxWithLabel* 				Config3DObjectLabel;
	
	 	
private:  
  vtkPVHM3DModelReader(const vtkPVHM3DModelReader&); // Not implemented
  void operator=(const vtkPVHM3DModelReader&); // Not implemented
}; 

#endif /*VTKPVHM3DMODELREADER_H_*/
