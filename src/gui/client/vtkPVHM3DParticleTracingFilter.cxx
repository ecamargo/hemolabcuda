/*
 * $Id:$
 */
#include "vtkPVHM3DParticleTracingFilter.h"

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"
#include "vtkKWMessageDialog.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWCheckButtonWithLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMProxyManager.h"

#include "vtkPVWidgetCollection.h"
#include "vtkPVSelectTimeSet.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMIntVectorProperty.h"


vtkCxxRevisionMacro(vtkPVHM3DParticleTracingFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DParticleTracingFilter);


//----------------------------------------------------------------------------
vtkPVHM3DParticleTracingFilter::vtkPVHM3DParticleTracingFilter()
{
	this->MyFrame	    = vtkKWFrameWithLabel::New();

	// File
	this->ConfigButton 	= vtkKWLoadSaveButton::New();
	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{Particles animation Files} {*.txt}}");
	this->ConfigDelta = vtkKWEntryWithLabel::New();
	this->ConfigRadiusOfParticles = vtkKWEntryWithLabel::New();
  this->ConfigNumberOfRepeatTimeSteps  = vtkKWScaleWithEntry::New();
  this->ConfigNumberOfInsertedTimeSteps = vtkKWScaleWithEntry::New();
  this->ConfigUseGPUProcess = vtkKWCheckButtonWithLabel::New();
}

//----------------------------------------------------------------------------
vtkPVHM3DParticleTracingFilter::~vtkPVHM3DParticleTracingFilter()
{
  this->MyFrame->Delete();
  this->ConfigButton->Delete();
  this->ConfigNumberOfRepeatTimeSteps->Delete();
  this->ConfigNumberOfInsertedTimeSteps->Delete();
  this->ConfigRadiusOfParticles->Delete();
  this->ConfigDelta->Delete();
  this->ConfigUseGPUProcess->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigUseGPUProcessCallback()
{
    vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
                               this->GetPVSource()->GetProxy()->GetProperty("UseGPUProcess"));
    if (prop)
      {
      prop->SetElement(0, this->ConfigUseGPUProcess->GetWidget()->GetSelectedState());
      }
    else
      {
      vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
        return;
      }

  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigButtonCallback()
{
   vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
 	                             this->GetPVSource()->GetProxy()->GetProperty("animationPath"));
	  if (prop)
	  	{
	  	prop->SetElement(0, this->ConfigButton->GetFileName());
	  	}
	  else
			{
	  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
      	return;
  		}

  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigNumberOfRepeatTimeStepsCallback()
{
	 vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("NumberOfRepeatTimeSteps"));
  if (prop)
  	{
  	prop->SetElement(0, this->ConfigNumberOfRepeatTimeSteps->GetValue());
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
   	return;
 		}

 this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigNumberOfInsertedTimeStepsCallback()
{
	 vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("NumberOfInsertedTimeSteps"));
	  if (prop)
	  	{
	  	prop->SetElement(0, this->ConfigNumberOfInsertedTimeSteps->GetValue());
	  	}
	  else
			{
	  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
     	return;
 		}

 this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigRadiusOfParticlesCallback()
{
  vtkSMDoubleVectorProperty *prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("RadiusOfParticles"));
  if (prop)
  	{
  	prop->SetElement(0, this->ConfigRadiusOfParticles->GetWidget()->GetValueAsDouble());
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
   	return;
 		}

 this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigDeltaCallback()
{
  vtkSMDoubleVectorProperty *prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("Delta"));
  if (prop)
  	{
  	prop->SetElement(0, this->ConfigDelta->GetWidget()->GetValueAsDouble());
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");
   	return;
 		}

 this->ModifiedCallback();
}


//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::Create(vtkKWApplication* app)
{
	vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(pvApp);
  this->MyFrame->SetParent(this);
  this->MyFrame->Create(pvApp);
  this->MyFrame->SetLabelText("HeMoLab Particle Tracing Filter");

  this->ConfigButton->SetParent(this->MyFrame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->GetLoadSaveDialog()->ChooseDirectoryOn();
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback");
  this->ConfigButton->SetBalloonHelpString("Set Output path for files");
  this->ConfigButton->SetText("Set Output path:");

  this->ConfigNumberOfRepeatTimeSteps->SetParent(this->MyFrame->GetFrame());
  this->ConfigNumberOfRepeatTimeSteps->PopupModeOn();
  this->ConfigNumberOfRepeatTimeSteps->Create(pvApp);
  this->ConfigNumberOfRepeatTimeSteps->SetRange(1, 20);
  this->ConfigNumberOfRepeatTimeSteps->SetResolution(1);
  this->ConfigNumberOfRepeatTimeSteps->SetValue(1);
  this->ConfigNumberOfRepeatTimeSteps->SetLabelText("Number of cycles:");
  this->ConfigNumberOfRepeatTimeSteps->SetBalloonHelpString("Repetitions of cardiac pulse");
  this->ConfigNumberOfRepeatTimeSteps->SetCommand(this, "ConfigNumberOfRepeatTimeStepsCallback");

  this->ConfigNumberOfInsertedTimeSteps->SetParent(this->MyFrame->GetFrame());
  this->ConfigNumberOfInsertedTimeSteps->PopupModeOn();
  this->ConfigNumberOfInsertedTimeSteps->Create(pvApp);
  this->ConfigNumberOfInsertedTimeSteps->SetRange(1, 20);
  this->ConfigNumberOfInsertedTimeSteps->SetValue(1);
  this->ConfigNumberOfInsertedTimeSteps->SetResolution(1);
  this->ConfigNumberOfInsertedTimeSteps->SetLabelText("Numbers of intermediate steps:");
  this->ConfigNumberOfInsertedTimeSteps->SetBalloonHelpString("Artificial steps that will be inserted");
  this->ConfigNumberOfInsertedTimeSteps->SetCommand(this, "ConfigNumberOfInsertedTimeStepsCallback");

  this->ConfigRadiusOfParticles->SetParent(this->MyFrame->GetFrame());
  this->ConfigRadiusOfParticles->Create(pvApp);
  this->ConfigRadiusOfParticles->SetLabelText("Radius of particles: ");
  this->ConfigRadiusOfParticles->SetBalloonHelpString("Set radius of particles.");
  this->ConfigRadiusOfParticles->GetWidget()->SetValueAsDouble(0.07);
  this->ConfigRadiusOfParticles->GetWidget()->SetCommand(this, "ConfigRadiusOfParticlesCallback");

  this->ConfigDelta->SetParent(this->MyFrame->GetFrame());
  this->ConfigDelta->Create(pvApp);
  this->ConfigDelta->SetLabelText("Delta Value: ");
  this->ConfigDelta->SetBalloonHelpString("Set Delta value.");
  this->ConfigDelta->GetWidget()->SetValueAsDouble(0.005);
  this->ConfigDelta->GetWidget()->SetCommand(this, "ConfigDeltaCallback");

  this->ConfigUseGPUProcess->SetParent(this->MyFrame->GetFrame());
  this->ConfigUseGPUProcess->Create(pvApp);
  this->ConfigUseGPUProcess->SetLabelText("Use GPU: ");
  this->ConfigUseGPUProcess->SetBalloonHelpString("On/Off GPU use.");
  this->ConfigUseGPUProcess->GetWidget()->SelectedStateOn();
  this->ConfigUseGPUProcess->GetWidget()->SetCommand(this, "ConfigUseGPUProcessCallback");

	//-------------------------------------------------------------------------------------
	//Place components
  this->PlaceComponents();
  this->ConfigureComponents();
}

//------------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::PlaceComponents()
{

  this->Script("pack %s -fill x -expand true",
    this->MyFrame->GetWidgetName());

  this->Script("grid %s -sticky we -columnspan 2",
    this->ConfigButton->GetWidgetName());

  this->Script("grid %s -sticky ew",
   	this->ConfigRadiusOfParticles->GetWidgetName());

  this->Script("grid %s -sticky ew",
   	this->ConfigDelta->GetWidgetName());

  this->Script("grid %s -sticky w",
  	this->ConfigNumberOfRepeatTimeSteps->GetWidgetName());

  this->Script("grid %s -sticky ew",
  	this->ConfigNumberOfInsertedTimeSteps->GetWidgetName());

  this->Script("grid %s -sticky ew",
    this->ConfigUseGPUProcess->GetWidgetName());
}

// ----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0",
    this->MyFrame->GetWidgetName());
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::Accept()
{
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(0, 0, 1);





	 vtkPVSelectTimeSet *PVSelectTimeSet = vtkPVSelectTimeSet::SafeDownCast(
	                          this->GetPVSource()->GetPVInput(0)->GetWidgets()->GetItemAsObject(2));

	  vtkSMIntVectorProperty *NumberOfTimeStepsProperty = vtkSMIntVectorProperty::SafeDownCast(
	                          this->GetPVSource()->GetProxy()->GetProperty("NumberOfTimeSteps"));

	    NumberOfTimeStepsProperty->SetElements1( PVSelectTimeSet->GetNumberOfTimeSteps() );
	    this->NumberOfTimeSteps = PVSelectTimeSet->GetNumberOfTimeSteps();

	  this->GetPVSource()->GetProxy()->UpdateInformation();






	this->GetPVSource()->GetProxy()->UpdateVTKObjects();

	vtkKWMessageDialog::PopupMessage( this->GetApplication(), this->GetParentWindow(),
																		"Information", "Press play button in the keyframe animation for builds EnSight files. Atention: Image will not be displayed during the process.",
																		vtkKWMessageDialog::WarningIcon);
}

//----------------------------------------------------------------------------
void vtkPVHM3DParticleTracingFilter::Initialize()
{
//	vtkPVSelectTimeSet *PVSelectTimeSet = vtkPVSelectTimeSet::SafeDownCast(
//													this->GetPVSource()->GetPVInput(0)->GetWidgets()->GetItemAsObject(2));
//
//	vtkSMIntVectorProperty *NumberOfTimeStepsProperty = vtkSMIntVectorProperty::SafeDownCast(
//													this->GetPVSource()->GetProxy()->GetProperty("NumberOfTimeSteps"));
//
//		NumberOfTimeStepsProperty->SetElements1( PVSelectTimeSet->GetNumberOfTimeSteps() );
//		this->NumberOfTimeSteps = PVSelectTimeSet->GetNumberOfTimeSteps();
//
//	this->GetPVSource()->GetProxy()->UpdateInformation();
}
