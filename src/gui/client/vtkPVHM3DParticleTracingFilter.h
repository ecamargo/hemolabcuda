/*
 * $Id:$
 */
	/*=========================================================================

	  Project:   HeMoLab
	  Module:    vtkPVHM3DParticleTracingFilter

	  Author: Eduardo Camargo

	=========================================================================*/


#ifndef VTKPVHM3DPARTICLETRACINGFILTER_H_
#define VTKPVHM3DPARTICLETRACINGFILTER_H_


#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"


class vtkKWEntry;
class vtkKWLabel;
class vtkKWLoadSaveButton;
class vtkKWFrameWithLabel;
class vtkKWScaleWithEntry;
class vtkKWCheckButtonWithLabel;
class vtkKWEntryWithLabel;

/// Interface com o usuário do filtro vtkHM3DParticlesTracingFilter
class VTK_EXPORT vtkPVHM3DParticleTracingFilter : public vtkPVObjectWidget
{
public:
  static vtkPVHM3DParticleTracingFilter *New();
  vtkTypeRevisionMacro(vtkPVHM3DParticleTracingFilter,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents();
  void ConfigureComponents();

  // Description:
  // Call creation on the child.
  virtual void 	Create (vtkKWApplication *app);

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();

  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Show that something was changed
  void SetValueChanged();

  // Description:
	// Menu and load file callback
	void ConfigButtonCallback();

	void ConfigNumberOfRepeatTimeStepsCallback();
	
	void ConfigNumberOfInsertedTimeStepsCallback();
		
	void ConfigRadiusOfParticlesCallback();

	void ConfigDeltaCallback();

	void ConfigUseGPUProcessCallback();

protected:
  vtkPVHM3DParticleTracingFilter();
  virtual ~vtkPVHM3DParticleTracingFilter();

  vtkKWFrameWithLabel *MyFrame;

  // Config File Parameters
  vtkKWLoadSaveButton*	ConfigButton;
  vtkKWEntryWithLabel*	ConfigDelta;
  vtkKWEntryWithLabel*	ConfigRadiusOfParticles;
  vtkKWScaleWithEntry*	ConfigNumberOfRepeatTimeSteps;
  vtkKWScaleWithEntry*	ConfigNumberOfInsertedTimeSteps;
  vtkKWCheckButtonWithLabel* ConfigUseGPUProcess;

  double Delta;
  int NumberOfTimeSteps;
  int NumberOfRepeatTimeSteps;
  int NumberOfInsertedTimeSteps;

private:
  vtkPVHM3DParticleTracingFilter(const vtkPVHM3DParticleTracingFilter&); // Not implemented
  void operator=(const vtkPVHM3DParticleTracingFilter&); // Not implemented
};

#endif /*VTKPVHM3DPARTICLETRACINGFILTER_H_*/
