/*
 * $Id: vtkPVHM3DQoptFilter.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkProcessModule.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"
#include "vtkPVWidget.h"
	
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWPushButton.h"
#include "vtkKWIcon.h"
#include "vtkKWToolbar.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMProxyManager.h"

#include "vtkPVHM3DQoptFilter.h"
#include "vtkHM3DQoptFilter.h"

#define MODE_FILE		 "File"
#define MODE_MANUAL  "Manual"

vtkCxxRevisionMacro(vtkPVHM3DQoptFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DQoptFilter);

//----------------------------------------------------------------------------
vtkPVHM3DQoptFilter::vtkPVHM3DQoptFilter()
{ 
	vtkDebugMacro(<<"vtkPVHM3DQoptFilter::vtkPVHM3DQoptFilter()");

	this->Frame							= vtkKWFrameWithLabel::New();
	this->ConfigurationMenu = vtkKWMenuButtonWithLabel::New();

	// File
	this->ConfigLabel				= vtkKWLabel::New();
	this->ConfigEntry 			= vtkKWEntry::New(); 
	this->StatusLabel 			= vtkKWLabel::New();  	  
	this->ConfigButton 			= vtkKWLoadSaveButton::New();
	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{Configuration Files} {*.cfg}}");	
	
	// Manual Settings
	this->MaxIterLabel				= vtkKWLabel::New();
	this->MaxIterEntry				= vtkKWEntry::New();
	this->MaxIterNodesLabel		= vtkKWLabel::New();
	this->MaxIterNodesEntry		= vtkKWEntry::New();
	this->MaxIterEdgesLabel		= vtkKWLabel::New();
	this->MaxIterEdgesEntry		= vtkKWEntry::New();
	this->MaxIterFacesLabel		= vtkKWLabel::New();
	this->MaxIterFacesEntry		= vtkKWEntry::New();	
	
	this->MaxClusterSizeLabel	= vtkKWLabel::New();
	this->MaxClusterSizeEntry	= vtkKWEntry::New();
	this->AddNodesFlag				= vtkKWCheckButton::New();
	this->AntiElemFlag				= vtkKWCheckButton::New();
	this->QualityFlag					= vtkKWCheckButton::New();
	this->SeeAllFlag					= vtkKWCheckButton::New();
	
	this->AddNodes				= 1;
	this->AntiElem				= 0;
	this->Quality					= 0;	
	this->SeeAll					= 1;	
}

//----------------------------------------------------------------------------
vtkPVHM3DQoptFilter::~vtkPVHM3DQoptFilter()
{
	vtkDebugMacro(<<"vtkPVHM3DDelaFilter::~vtkPVHM3DDelaFilter()");	
	this->ConfigButton->Delete();	
	this->ConfigLabel->Delete();
	this->ConfigEntry->Delete();   
	this->StatusLabel->Delete(); 
	this->ConfigurationMenu->Delete();
	this->MaxIterLabel->Delete();
	this->MaxIterEntry->Delete();

	this->MaxIterNodesLabel->Delete();
	this->MaxIterNodesEntry->Delete();
	this->MaxIterEdgesLabel->Delete();
	this->MaxIterEdgesEntry->Delete();
	this->MaxIterFacesLabel->Delete();
	this->MaxIterFacesEntry->Delete();	
	
	this->MaxClusterSizeLabel->Delete();
	this->MaxClusterSizeEntry->Delete();
	this->AddNodesFlag->Delete();
	this->AntiElemFlag->Delete();
	this->QualityFlag->Delete();
  this->Frame->Delete();	
  this->SeeAllFlag->Delete();
}


void vtkPVHM3DQoptFilter::ConfigButtonCallback()
{
  this->ConfigEntry->SetValue(this->ConfigButton->GetFileName());	
	this->StatusLabel->SetText("");  
  this->ConfigButton->SetText(""); 	
	
	// Se um arquivo foi carregado, mudo modo para arquivo
	this->ConfigurationMenu->GetWidget()->SetValue(MODE_FILE);
	this->ConfigurationMenuCallback();
	this->ModifiedCallback();
}

// -----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::AddNodesCallback()
{
	this->AddNodes = (this->AddNodes) ? 0 : 1;	
	this->ModifiedCallback();	
}

// -----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::AntiElemCallback()
{
	this->AntiElem = (this->AntiElem) ? 0 : 1;	
	this->ModifiedCallback();	
}

// -----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::QualityCallback()
{
	this->Quality = (this->Quality) ? 0 : 1;	
	this->ModifiedCallback();	
}

// -----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::SeeAllCallback()
{
	this->SeeAll = (this->SeeAll) ? 0 : 1;	
	this->ModifiedCallback();	
}


// ----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::ConfigurationMenuCallback() 
{
  if (this->ConfigurationMenu->IsCreated())
    {
    const char *value = this->ConfigurationMenu->GetWidget()->GetValue();
    if (!strcmp(value, MODE_FILE))
      {
			this->MaxIterEntry->ReadOnlyOn();
			this->MaxIterNodesEntry->ReadOnlyOn();
			this->MaxIterEdgesEntry->ReadOnlyOn();
			this->MaxIterFacesEntry->ReadOnlyOn();
			this->MaxClusterSizeEntry->ReadOnlyOn();
			this->AddNodesFlag->SetStateToDisabled();
			this->AntiElemFlag->SetStateToDisabled();
			this->QualityFlag->SetStateToDisabled();
			this->SeeAllFlag->SetStateToDisabled();
      }
    if (!strcmp(value, MODE_MANUAL))
      {
			this->MaxIterEntry->ReadOnlyOff();
			this->MaxIterNodesEntry->ReadOnlyOff();
			this->MaxIterEdgesEntry->ReadOnlyOff();
			this->MaxIterFacesEntry->ReadOnlyOff();	
			this->MaxClusterSizeEntry->ReadOnlyOff();
			this->AddNodesFlag->SetStateToNormal();
			this->AntiElemFlag->SetStateToNormal();
			this->QualityFlag->SetStateToNormal();
			this->SeeAllFlag->SetStateToNormal();
      }
		}
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::Create(vtkKWApplication* app)
{
	vtkDebugMacro(<< "vtkPVHM3DDelaFilter::Create()");

  vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  this->Frame->SetLabelText("Dela Filter");  

  this->ConfigurationMenu->SetParent(this->Frame->GetFrame());
  this->ConfigurationMenu->Create(pvApp);
  this->ConfigurationMenu->ExpandWidgetOff();
  this->ConfigurationMenu->SetBalloonHelpString("Select the visualization state.");

  vtkKWMenuButton *omenu = this->ConfigurationMenu->GetWidget();
  omenu->SetWidth(10);
  omenu->SetPadX(10);
  omenu->SetPadY(2);
  
  omenu->AddRadioButton( MODE_MANUAL, this, "ConfigurationMenuCallback");
  omenu->AddRadioButton( MODE_FILE,   this, "ConfigurationMenuCallback");
  omenu->SetValue( MODE_MANUAL );
  
  this->ConfigurationMenu->GetWidget()->IndicatorVisibilityOn();
  this->ConfigurationMenu->GetLabel()->SetText("Configuration Mode: ");
  this->ConfigurationMenu->LabelVisibilityOn();  

  // Load Parameteres	
  this->ConfigButton->SetParent(this->Frame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback"); 
  this->ConfigButton->SetBalloonHelpString("Set Configuration File"); 
  this->ConfigButton->SetText(""); 

  this->ConfigLabel->SetParent(this->Frame->GetFrame());
  this->ConfigLabel->Create(pvApp);
  this->ConfigLabel->SetText("Configuration File");
  this->ConfigEntry->SetParent(this->Frame->GetFrame());
  this->ConfigEntry->Create(pvApp);
  this->ConfigEntry->SetValue("None");
  this->ConfigEntry->ReadOnlyOn();

  this->StatusLabel->SetParent(this->Frame->GetFrame());
  this->StatusLabel->Create(pvApp);
  this->StatusLabel->SetBalloonHelpString("Status"); 
  this->StatusLabel->SetText("");

  this->MaxIterLabel->SetParent(this->Frame->GetFrame());
  this->MaxIterLabel->Create(pvApp);
  this->MaxIterLabel->SetText("Loop Iterations");
  this->MaxIterEntry->SetParent(this->Frame->GetFrame());
  this->MaxIterEntry->Create(pvApp);
  this->MaxIterEntry->SetBalloonHelpString("Max Number of Loop Iterations"); 
  this->MaxIterEntry->SetValue("1");

  this->MaxIterNodesLabel->SetParent(this->Frame->GetFrame());
  this->MaxIterNodesLabel->Create(pvApp);
  this->MaxIterNodesLabel->SetText("Node Iterations");
  this->MaxIterNodesEntry->SetParent(this->Frame->GetFrame());
  this->MaxIterNodesEntry->Create(pvApp);
  this->MaxIterNodesEntry->SetBalloonHelpString("Max Number of Node Iterations"); 
  this->MaxIterNodesEntry->SetValue("1");

  this->MaxIterEdgesLabel->SetParent(this->Frame->GetFrame());
  this->MaxIterEdgesLabel->Create(pvApp);
  this->MaxIterEdgesLabel->SetText("Edge Iterations");
  this->MaxIterEdgesEntry->SetParent(this->Frame->GetFrame());
  this->MaxIterEdgesEntry->Create(pvApp);
  this->MaxIterEdgesEntry->SetBalloonHelpString("Max Number of Edge Iterations"); 
  this->MaxIterEdgesEntry->SetValue("1");

  this->MaxIterFacesLabel->SetParent(this->Frame->GetFrame());
  this->MaxIterFacesLabel->Create(pvApp);
  this->MaxIterFacesLabel->SetText("Face Iterations");
  this->MaxIterFacesEntry->SetParent(this->Frame->GetFrame());
  this->MaxIterFacesEntry->Create(pvApp);
  this->MaxIterFacesEntry->SetBalloonHelpString("Max Number of Face Iterations"); 
  this->MaxIterFacesEntry->SetValue("1");
  
  this->MaxClusterSizeLabel->SetParent(this->Frame->GetFrame());
  this->MaxClusterSizeLabel->Create(pvApp);
  this->MaxClusterSizeLabel->SetText("Max Cluster Size");
  this->MaxClusterSizeEntry->SetParent(this->Frame->GetFrame());
  this->MaxClusterSizeEntry->Create(pvApp);
  this->MaxClusterSizeEntry->SetBalloonHelpString("Maximum Cluster Size"); 
  this->MaxClusterSizeEntry->SetValueAsInt(200);

  this->AddNodesFlag->SetParent(this->Frame->GetFrame());
  this->AddNodesFlag->Create(pvApp);
  this->AddNodesFlag->SetText("Add Nodes Flag   ");  
  this->AddNodesFlag->SetCommand(this, "AddNodesCallback");  	
  this->AddNodesFlag->SelectedStateOn();  

  this->AntiElemFlag->SetParent(this->Frame->GetFrame());
  this->AntiElemFlag->Create(pvApp);
  this->AntiElemFlag->SetText("Anti Element Flag");  
  this->AntiElemFlag->SetCommand(this, "AntiElemCallback");  	
  this->AntiElemFlag->SelectedStateOff();  

  this->QualityFlag->SetParent(this->Frame->GetFrame());
  this->QualityFlag->Create(pvApp);
  this->QualityFlag->SetText("Quality Flag       ");  
  this->QualityFlag->SetCommand(this, "QualityCallback");  	
  this->QualityFlag->SelectedStateOff();  

  this->SeeAllFlag->SetParent(this->Frame->GetFrame());
  this->SeeAllFlag->Create(pvApp);
  this->SeeAllFlag->SetText("See All Flag       ");  
  this->SeeAllFlag->SetCommand(this, "SeeAllCallback");  	
  this->SeeAllFlag->SelectedStateOn();  

	//-------------------------------------------------------------------------------------      
	//Place components after accept
  this->PlaceComponents();
  this->ConfigureComponents(); 
}

//------------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::PlaceComponents()
{
	vtkDebugMacro(<<"vtkPVHM3DDelaFilter::PlaceComponents()");
	
  this->Script("pack %s -fill x -expand true",  
    this->Frame->GetWidgetName());  

  this->Script("grid %s - - -sticky w",
    this->ConfigurationMenu->GetWidgetName());	

  this->Script("grid %s %s %s - - -sticky ew",
    this->ConfigLabel->GetWidgetName(), 
    this->ConfigEntry->GetWidgetName(),
    this->ConfigButton->GetWidgetName());

  this->Script("grid %s %s - - -sticky ew",     
		this->MaxIterLabel->GetWidgetName(),
  	this->MaxIterEntry->GetWidgetName()); 
 
  this->Script("grid %s %s - - -sticky ew",     
		this->MaxIterNodesLabel->GetWidgetName(),
  	this->MaxIterNodesEntry->GetWidgetName()); 

  this->Script("grid %s %s - - -sticky ew",     
		this->MaxIterEdgesLabel->GetWidgetName(),
  	this->MaxIterEdgesEntry->GetWidgetName()); 

  this->Script("grid %s %s - - -sticky ew",     
		this->MaxIterFacesLabel->GetWidgetName(),
  	this->MaxIterFacesEntry->GetWidgetName()); 
 
  this->Script("grid %s %s - - -sticky ew",     
  	this->MaxClusterSizeLabel->GetWidgetName(),
		this->MaxClusterSizeEntry->GetWidgetName());              

	this->Script("grid %s - - -sticky ew",     
  	this->AddNodesFlag->GetWidgetName());	

	this->Script("grid %s - - -sticky ew",     
  	this->AntiElemFlag->GetWidgetName());	

  this->Script("grid %s - - -sticky ew",
    this->QualityFlag->GetWidgetName()); 
    
  this->Script("grid %s - - -sticky ew",
    this->SeeAllFlag->GetWidgetName());          
    
  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->MaxIterEntry->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->MaxIterNodesEntry->GetWidgetName(),
    this->GetTclName());
    
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->MaxIterEdgesEntry->GetWidgetName(),
    this->GetTclName());
    
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->MaxIterFacesEntry->GetWidgetName(),
    this->GetTclName());        

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->MaxClusterSizeEntry->GetWidgetName(),
    this->GetTclName());          
}

// ----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());       
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::Accept()
{  		
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(1, 0, 0);	
	vtkDebugMacro(<<"void vtkPVHM3DQoptFilter::Accept()");
	vtkDebugMacro(<<"vtkPVHM3DQoptFilter::Accept() :: Config File: " << this->ConfigEntry->GetValue());;
	
	const char *value = this->ConfigurationMenu->GetWidget()->GetValue();
	if(!strcmp(value,"Manual"))
		{
		//vtkErrorMacro(<<"Configuration File not set.");	  			
    vtkSMIntVectorProperty *ManualProp = vtkSMIntVectorProperty::SafeDownCast(
    	this->GetPVSource()->GetProxy()->GetProperty("ApplyManualConfig"));
    if(ManualProp)
    	{
			int maxitv[4];
			maxitv[0] = this->MaxIterEntry->GetValueAsInt();
			maxitv[1] = this->MaxIterNodesEntry->GetValueAsInt();
			maxitv[2] = this->MaxIterEdgesEntry->GetValueAsInt();
			maxitv[3] = this->MaxIterFacesEntry->GetValueAsInt();										
												
//			char maxit[20];
//			strcpy(maxit,this->MaxIterEntry->GetValue());
//			sscanf(maxit, "%d %d %d %d", &maxitv[0], &maxitv[1], &maxitv[2], &maxitv[3]);
		
			ManualProp->SetElement(0, maxitv[0]);
			ManualProp->SetElement(1, maxitv[1]);
			ManualProp->SetElement(2, maxitv[2]);
			ManualProp->SetElement(3, maxitv[3]);

			ManualProp->SetElement(4, this->MaxClusterSizeEntry->GetValueAsInt());
			ManualProp->SetElement(5, this->AddNodes);
			ManualProp->SetElement(6, this->AntiElem);
			ManualProp->SetElement(7, this->Quality);
			ManualProp->SetElement(8, this->SeeAll);			
    	}		
	  else
		  {
			vtkErrorMacro(<<"Cannot create \"ManualProp\" Qopt filter property.");	  	
	  	return;
		  } 	
		this->StatusLabel->SetText("Bounding Box used to compute coordinates.");		  					
		}
	else
		{
		if(!strcmp(this->ConfigEntry->GetValue(),"None"))
			{
			this->StatusLabel->SetText("You must select a CFG file!");
			return;
			}
	  vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
			this->GetPVSource()->GetProxy()->GetProperty("ApplyQopt"));    
    
	  if (prop) 
			prop->SetElement(0,this->ConfigEntry->GetValue());
	  else
		  {
			vtkErrorMacro(<<"Cannot create \"ApplyQopt\" filter property.");	  	
	  	return;
		  }					  
		}
	// Envia propriedade para os objetos no servidor
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();
}

//----------------------------------------------------------------------------
void vtkPVHM3DQoptFilter::Initialize()
{
}  

