/*
 * $Id: vtkPVHM3DQoptFilter.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHM3DQoptFilter
// .SECTION Description
// Interface for Qopt filter

#ifndef _vtkPVHM3DQoptFilter_h_
#define _vtkPVHM3DQoptFilter_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkKWPushButton;  
class vtkKWEntry;
class vtkKWLabel;
class vtkKWLoadSaveButton;
class vtkKWMenuButtonWithLabel;
class vtkKWFrameWithLabel;
class vtkKWCheckButton;

class VTK_EXPORT vtkPVHM3DQoptFilter : public vtkPVObjectWidget
{
public:
  static vtkPVHM3DQoptFilter *New();
  vtkTypeRevisionMacro(vtkPVHM3DQoptFilter,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents();
  void ConfigureComponents(); 	
	
  // Description:
  // Call creation on the child.  
	virtual void 	Create (vtkKWApplication *app); 

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
    
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:  
  // Show that something was changed
	void SetValueChanged();	

  // Description:  
	// Menu and load file callback	
	void ConfigButtonCallback();
	void ConfigurationMenuCallback();

  // Description:  
  // Check Buttons Callbacks
	void AddNodesCallback();
	void AntiElemCallback();
	void QualityCallback();
	void SeeAllCallback();
	
protected:
  vtkPVHM3DQoptFilter();
  virtual ~vtkPVHM3DQoptFilter();
  
	vtkKWFrameWithLabel *Frame;	  

	vtkKWMenuButtonWithLabel *ConfigurationMenu;

	// Config File Parameters
	vtkKWLoadSaveButton*	ConfigButton;  
  vtkKWLabel* 					ConfigLabel;
  vtkKWEntry* 					ConfigEntry;  
  vtkKWLabel* 					StatusLabel;

	// Manual Settings
	
	// Number of Iteractions
	vtkKWLabel*           MaxIterLabel;
	vtkKWEntry* 					MaxIterEntry;

	// Number of Node Iteractions	
	vtkKWLabel*           MaxIterNodesLabel;
	vtkKWEntry* 					MaxIterNodesEntry;
	
	// Number of Edge Iteractions		
	vtkKWLabel*           MaxIterEdgesLabel;
	vtkKWEntry* 					MaxIterEdgesEntry;

	// Number of Face Iteractions		
	vtkKWLabel*           MaxIterFacesLabel;
	vtkKWEntry* 					MaxIterFacesEntry;
	
	vtkKWLabel*           MaxClusterSizeLabel;
	vtkKWEntry* 					MaxClusterSizeEntry;   
	vtkKWCheckButton*			AddNodesFlag;
	vtkKWCheckButton*			AntiElemFlag;
	vtkKWCheckButton*			QualityFlag;
	vtkKWCheckButton*			SeeAllFlag;	

	// Manual Setting Value  
  int	AddNodes;
  int	AntiElem;
  int	Quality;    
  int SeeAll;
	 	
private:  
  vtkPVHM3DQoptFilter(const vtkPVHM3DQoptFilter&); // Not implemented
  void operator=(const vtkPVHM3DQoptFilter&); // Not implemented
}; 

#endif  /*_vtkPVHM3DQoptFilter_h_*/

