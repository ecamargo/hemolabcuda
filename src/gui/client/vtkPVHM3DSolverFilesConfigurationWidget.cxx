/*
 * $Id: vtkPVHM3DSolverFilesConfigurationWidget.cxx 340 2006-05-12 13:40:37Z  $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWIcon.h"
#include "vtkPVHM3DSolverFilesConfigurationWidget.h"
#include "vtkKWNotebook.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWRadioButton.h"
#include "vtkKWComboBox.h"
#include "vtkKWMessageDialog.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMSourceProxy.h"
#include <time.h>
#include <vtksys/SystemTools.hxx>
 

vtkCxxRevisionMacro(vtkPVHM3DSolverFilesConfigurationWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DSolverFilesConfigurationWidget);

//----------------------------------------------------------------------------
vtkPVHM3DSolverFilesConfigurationWidget::vtkPVHM3DSolverFilesConfigurationWidget()
{
  //strcpy (ModelType,"Undefined");
  

  
  this->notebook = vtkKWNotebook::New();
  this->SolverTimeConfigFrame = vtkKWFrameWithLabel::New();
  this->ConvergenceParametersFrame = vtkKWFrameWithLabel::New();
  this->MCFrame = vtkKWFrameWithLabel::New();
  this->proxy = NULL;

  // page 1 ****************  
  // ****************************************************************************************************************
  
  this->labelForm = vtkKWLabel::New();
  this->radioLSV = vtkKWRadioButton::New();
  this->radioLS = vtkKWRadioButton::New();
  
  this->labelDensity=vtkKWLabel::New();
  this->entryDensity=vtkKWEntry::New();

  this->labelViscosity=vtkKWLabel::New();
  this->entryViscosity=vtkKWEntry::New();

  this->labelArterialWallLaw=vtkKWLabel::New();
  this->comboArterialWallLaw=vtkKWComboBox::New();

  this->labelVelocity=vtkKWLabel::New();
  this->thumbVel=vtkKWThumbWheel::New();
  
  this->labelModel =   vtkKWLabel::New();
  this->radioCompliant = vtkKWRadioButton::New();
  this->radioRigid = vtkKWRadioButton::New();
  
  
  this->labelBloodLaw =   vtkKWLabel::New();
  this->radioBloodLawNewton = vtkKWRadioButton::New();
  this->radioBloodLawCasson = vtkKWRadioButton::New();

    //Viscosity
  //this->labelVisco =   vtkKWLabel::New();
  //this->entryVisco =vtkKWEntry::New();
    
  //Asymptotic viscosity
  this->labelAVisco =   vtkKWLabel::New();
  this->entryAVisco =vtkKWEntry::New();
  
  //Limit Stress
  this->labelLimitStress =   vtkKWLabel::New();
  this->entryLimitStress =vtkKWEntry::New();
  
  //Regulation Parameter
  this->labelRegularParameter =   vtkKWLabel::New();
  this->comboRegularParameter  = vtkKWComboBox::New();
  
  //*************************
 // ****************************************************************************************************************

 
  // page 2 General Config
  this->ResumeLabel = vtkKWLabel::New();
  this->radioResumeYes =  vtkKWRadioButton::New();
  
  this->radioResumeNo =  vtkKWRadioButton::New();
  this->labelTimeStep = vtkKWLabel::New();
  this->entryTimeStep = vtkKWEntry::New();
  this->labelInitialTime = vtkKWLabel::New();
  this->entryInitialTime = vtkKWEntry::New();
  this->labelFinalTime = vtkKWLabel::New();
  
  this->entryFinalTime = vtkKWEntry::New();
 
  
  this->labelNumberOfTimeSteps = vtkKWLabel::New();
  this->entryNumberOfTimeSteps = vtkKWEntry::New();

  this->labelNumberOfCardiacCycles = vtkKWLabel::New();
  this->entryNumberOfCardiacCycles = vtkKWEntry::New();
  
  
  this->labelVariable = vtkKWLabel::New();
  this->labelFlux = vtkKWLabel::New();
  this->labelElPre = vtkKWLabel::New();
  this->labelArea = vtkKWLabel::New();
  this->labelPress = vtkKWLabel::New();
  this->labelRefVal = vtkKWLabel::New();
  this->labelConEr = vtkKWLabel::New();
  this->entryReFl = vtkKWEntry::New();
  this->entryCeFl = vtkKWEntry::New();
  
  
  this->entryReEP = vtkKWEntry::New();
  this->entryCeEP = vtkKWEntry::New();
  this->entryReAr = vtkKWEntry::New();
  this->entryCeAr = vtkKWEntry::New();
  this->entryRePr = vtkKWEntry::New();
  this->entryCePr = vtkKWEntry::New();
  this->labelIter = vtkKWLabel::New();
  this->entryIter = vtkKWEntry::New();
  this->labelSuPar = vtkKWLabel::New();
  this->scaleSuPar = vtkKWScaleWithEntry::New();
  
  
  
  
  this->labelVelocityX = vtkKWLabel::New();
  this->labelVelocityY = vtkKWLabel::New();
  this->labelVelocityZ = vtkKWLabel::New();
  this->labelDisplacementX = vtkKWLabel::New();
  this->labelDisplacementY = vtkKWLabel::New();
  this->labelDisplacementZ = vtkKWLabel::New();
  
  this->entryRefVelocityX = vtkKWEntry::New();
  this->entryRefVelocityY = vtkKWEntry::New();
  this->entryRefVelocityZ = vtkKWEntry::New();
  this->entryRefDisplacementX= vtkKWEntry::New();
  this->entryRefDisplacementY = vtkKWEntry::New();
  this->entryRefDisplacementZ = vtkKWEntry::New();
  
  this->entryConvVelocityX = vtkKWEntry::New(); 
  this->entryConvVelocityY= vtkKWEntry::New();
  this->entryConvVelocityZ = vtkKWEntry::New();
  this->entryConvDisplacementX = vtkKWEntry::New();
  this->entryConvDisplacementY = vtkKWEntry::New();
  this->entryConvDisplacementZ = vtkKWEntry::New();
  
  
  // ****************************************************************************************************************
  // page 3 Solver Config
  
  this->labelRenu = vtkKWLabel::New();
  this->entryRenu = vtkKWEntry::New();

  this->labelRLS = vtkKWLabel::New();
  this->labelRLSHelp = vtkKWLabel::New();
  
  this->comboRLS = vtkKWComboBox::New();
  
  this->labelTethaScheme=vtkKWLabel::New();
  this->thumbTheta=vtkKWThumbWheel::New();
  
  this->TetraCompressibilityLabel = vtkKWLabel::New();     
	this->TetraCompressibilityEntry	= vtkKWEntry::New();
	
  this->labelAditi = vtkKWLabel::New();
  this->labelPena = vtkKWLabel::New();
  this->entryPena = vtkKWEntry::New();
  this->radioYes = vtkKWRadioButton::New();
  this->radioNo = vtkKWRadioButton::New();
  
  this->labelSolverType = vtkKWLabel::New();
  this->labelConvergenceError = vtkKWLabel::New();
  this->labelDispTolerance = vtkKWLabel::New();
  this->labelFParam = vtkKWLabel::New();
  
  
  this->comboSolverType	= vtkKWComboBox::New();
	this->entryConvergenceError= vtkKWEntry::New();
	this->frameIterativeSolver =vtkKWFrameWithLabel::New();
	this->framePreconditioning =vtkKWFrameWithLabel::New();
	this->entryDispTolerance =vtkKWEntry::New();
	this->entryFParam = vtkKWEntry::New();
	this->labelIncrementalVersion = vtkKWLabel::New();
	this->radioICYes = vtkKWRadioButton::New();
	this->radioICNo = vtkKWRadioButton::New();
  
  
  
  this->labelIterationsBeforeRestart = vtkKWLabel::New();
  this->labelkrylov = vtkKWLabel::New();

  this->entryIterationsBeforeRestart = vtkKWEntry::New();
  this->entrykrylov = vtkKWEntry::New();
  
  
 
  // ****************************************************************************************************************
  // page 4
  this->labelFileOutput = vtkKWLabel::New();
  this->entryFileOutput = vtkKWEntry::New();
  this->labelScreenOutput = vtkKWLabel::New();
  this->entryScreenOutput = vtkKWEntry::New();
  
  this->labelLogFile = vtkKWLabel::New();
  this->entryLogFile = vtkKWEntry::New();
  
  this->labelFilesPath = vtkKWLabel::New();
  this->entryFilesPath = vtkKWEntry::New();
  
  this->pushButtonBrowse = vtkKWPushButton::New();
  
  this->labelFilesGenerationStatus = vtkKWLabel::New();
  this->textFilesGenerationStatus = vtkKWTextWithScrollbars::New(); 
  
  
  
  //********************************
  
  
  
  this->CommitChangesButtonPage1 = vtkKWPushButton::New();
  this->IntParam = 	NULL;
  this->DoubleParam = 	NULL;
  
   
  this->WindowFrame = vtkKWFrame::New(); 
   
  this->ChooseDirectoryDialog = vtkKWLoadSaveDialog::New();
  
  
  this->OriginalFinalTime = -1;
  
  this->CalculateTimeStepButton = vtkKWPushButton::New();
  this->labelCardiacCycleTime =   vtkKWLabel::New();
  this->entryCardiacCycleTime = vtkKWEntry::New();
 	
 	this->IntParam = 	vtkIntArray::New();
  this->IntParam->SetNumberOfValues(8); 

	this->DoubleParam = 	vtkDoubleArray::New();
  this->DoubleParam->SetNumberOfValues(17);
  

  this->FrameScroll = vtkKWFrameWithScrollbar::New(); 
 
 
  this->SelectedSolverFilesRadioButtonSet = vtkKWRadioButtonSet::New();
 
  this->SelectedSolverFilesLabel = vtkKWLabel::New();
 
	 
  this->ScaleFactor3DLabel = vtkKWLabel::New();
	this->ScaleFactor3DEntry = vtkKWEntry::New();

	
	
	 //**************************************************************************
	  
	  this->ParallelIterativeSolverFrame = vtkKWFrameWithLabel::New();
	  
	  this->ParallelNumberOfProcessorsLabel = vtkKWLabel::New();
	  this->ParallelNumberOfProcessorsEntry = vtkKWEntry::New();
	  
	  this->ParallelIterationsRestartLabel = vtkKWLabel::New();
	  this->ParallelIterationsRestartEntry = vtkKWEntry::New();
	  
	  this->ParallelRelativeErrorLabel = vtkKWLabel::New();
	  this->ParallelRelativeErrorEntry = vtkKWEntry::New();
	  
	  this->ParallelAbsErrorLabel = vtkKWLabel::New();
	  this->ParallelAbsErrorEntry = vtkKWEntry::New();
	  
	  this->ParallelMaxNumberIterLabel  = vtkKWLabel::New();
	  this->ParallelMaxNumberIterEntry = vtkKWEntry::New();
	  this->ParallelPrecondLabel  = vtkKWLabel::New();
	  this->ParallelPrecondCombo = vtkKWComboBox::New();
	  
	  
	  //*********
	
	
}

//----------------------------------------------------------------------------
vtkPVHM3DSolverFilesConfigurationWidget::~vtkPVHM3DSolverFilesConfigurationWidget()
{
	
	this->notebook->Delete();
  
  this->SolverTimeConfigFrame->Delete();
  this->ConvergenceParametersFrame->Delete();
  this->MCFrame->Delete();
  
 
  //***************************************************************************
  // page 1
  this->labelForm->Delete();
  this->radioLSV->Delete();
  this->radioLS->Delete();
 
  this->labelDensity->Delete();
  this->entryDensity->Delete();
  
  this->labelViscosity->Delete();
  this->entryViscosity->Delete();
  
  this->labelArterialWallLaw->Delete();
  this->comboArterialWallLaw->Delete();
 
  this->labelVelocity->Delete();
  this->thumbVel->Delete();
  
  this->labelModel->Delete();
  this->radioCompliant->Delete();
  this->radioRigid->Delete();
  
  this->labelBloodLaw->Delete();
  this->radioBloodLawNewton->Delete();
  this->radioBloodLawCasson->Delete();
  
  
      //Viscosity
  //this->labelVisco->Delete();
  //this->entryVisco->Delete();
    
  //Asymptotic viscosity
  this->labelAVisco->Delete();
  this->entryAVisco->Delete();
  
  //Limit Stress
  this->labelLimitStress->Delete();
  this->entryLimitStress->Delete();
  
  //Regulation Parameter
  this->labelRegularParameter->Delete();
  this->comboRegularParameter->Delete();
  
  
  //***************************************************************************

  //***************************************************************************
  // page 2
  this->ResumeLabel->Delete();
  this->radioResumeYes->Delete();
  this->radioResumeNo->Delete();
  
  
  this->labelTimeStep->Delete();
  this->entryTimeStep->Delete();
  this->labelInitialTime->Delete();
  this->entryInitialTime->Delete();
  this->labelFinalTime->Delete();
  this->entryFinalTime->Delete();

	this->labelNumberOfTimeSteps->Delete();
  this->entryNumberOfTimeSteps->Delete();

  this->labelNumberOfCardiacCycles->Delete();
  this->entryNumberOfCardiacCycles->Delete();
 
 
  this->labelVariable->Delete();
  this->labelFlux->Delete();
  this->labelElPre->Delete();
  this->labelArea->Delete();
  this->labelPress->Delete();
  this->labelRefVal->Delete();
  this->labelConEr->Delete();
  this->entryReFl->Delete();
  this->entryCeFl->Delete();
  
  
  this->entryReEP->Delete();
  this->entryCeEP->Delete();
  this->entryReAr->Delete();
  this->entryCeAr->Delete();
  this->entryRePr->Delete();
  this->entryCePr->Delete();
  this->labelIter->Delete();
  this->entryIter->Delete();
  this->labelSuPar->Delete();
  this->scaleSuPar->Delete();
  
  
  
  this->labelVelocityX->Delete();
  this->labelVelocityY->Delete();
  this->labelVelocityZ->Delete();
  this->labelDisplacementX->Delete();
  this->labelDisplacementY->Delete();
  this->labelDisplacementZ->Delete();
  
  this->entryRefVelocityX->Delete();
  this->entryRefVelocityY->Delete();
  this->entryRefVelocityZ->Delete();
  this->entryRefDisplacementX->Delete();
  this->entryRefDisplacementY->Delete();
  this->entryRefDisplacementZ->Delete();
  
  this->entryConvVelocityX->Delete(); 
  this->entryConvVelocityY->Delete();
  this->entryConvVelocityZ->Delete();
  this->entryConvDisplacementX->Delete();
  this->entryConvDisplacementY->Delete();
  this->entryConvDisplacementZ->Delete();
  
  
  //***************************************************************************
  
  //***************************************************************************
  // ######## page 3 solver config
  this->labelRenu->Delete();
  this->entryRenu->Delete();

  this->labelRLS->Delete();
  this->labelRLSHelp->Delete();
  this->comboRLS->Delete();
  
  this->labelTethaScheme->Delete();
  this->thumbTheta->Delete();
  
 	this->TetraCompressibilityLabel->Delete();     
	this->TetraCompressibilityEntry->Delete();
  
  this->labelAditi->Delete();

  this->labelPena->Delete();
  this->entryPena->Delete();
  this->radioYes->Delete();
  this->radioNo->Delete();
  
  this->labelSolverType->Delete();
  this->labelConvergenceError->Delete();
  this->labelDispTolerance->Delete();
  this->labelFParam->Delete();
  
  this->comboSolverType->Delete();
	this->entryConvergenceError->Delete();
	this->frameIterativeSolver->Delete();
	this->framePreconditioning->Delete();
	this->entryDispTolerance->Delete();
	this->entryFParam->Delete();
	this->labelIncrementalVersion->Delete();
	this->radioICYes->Delete();
	this->radioICNo->Delete();
	
	this->labelIterationsBeforeRestart->Delete();
  this->labelkrylov->Delete();

  this->entryIterationsBeforeRestart->Delete();
  this->entrykrylov->Delete();
	
	  
  //***************************************************************************


  //***************************************************************************
  // page 4
  this->labelFileOutput->Delete();
  this->entryFileOutput->Delete();
  this->labelScreenOutput->Delete();
  this->entryScreenOutput->Delete();
  
  this->labelLogFile->Delete();
  this->entryLogFile->Delete();
  
  this->labelFilesPath->Delete();
  this->entryFilesPath->Delete();
  this->pushButtonBrowse->Delete();
  this->labelFilesGenerationStatus->Delete();
  this->textFilesGenerationStatus->Delete(); 
  
  //***************************************************************************

  
  this->CommitChangesButtonPage1->Delete();

  this->WindowFrame->Delete();
  this->ChooseDirectoryDialog->Delete();
  this->CalculateTimeStepButton->Delete();

  this->labelCardiacCycleTime->Delete();
  this->entryCardiacCycleTime->Delete();

  
  if (this->IntParam)
  	this->IntParam->Delete();
  	
  if (this->DoubleParam)
  	this->DoubleParam->Delete();


  this->FrameScroll->Delete();
 
  this->SelectedSolverFilesRadioButtonSet->Delete();
 
  this->SelectedSolverFilesLabel->Delete();
 
 
	this->ScaleFactor3DLabel->Delete();
	this->ScaleFactor3DEntry->Delete();

	
	
	
  //**************************************************************************
    
    this->ParallelIterativeSolverFrame->Delete();
    
    this->ParallelNumberOfProcessorsLabel->Delete();
    this->ParallelNumberOfProcessorsEntry->Delete();
    
    this->ParallelIterationsRestartLabel->Delete();
    this->ParallelIterationsRestartEntry->Delete();
    
    this->ParallelRelativeErrorLabel->Delete();
    this->ParallelRelativeErrorEntry->Delete();
    
    this->ParallelAbsErrorLabel->Delete();
    this->ParallelAbsErrorEntry->Delete();
    
    this->ParallelMaxNumberIterLabel->Delete();
    this->ParallelMaxNumberIterEntry->Delete();
    this->ParallelPrecondLabel->Delete();
    this->ParallelPrecondCombo->Delete();
    
    
    //*********
  
	
	
	
	
}


//----------------------------------------------------------------------------



void vtkPVHM3DSolverFilesConfigurationWidget::Create(vtkKWApplication *app) 
{
  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
	this->Superclass::Create(app);
	
  this->SetDisplayPositionToScreenCenterFirst();
	
  char buffer[1024];
  this->WindowFrame->SetParent(this);
  this->WindowFrame->Create(this->GetPVApplication());
//  this->Script("pack %s -anchor n -side top -fill x -expand t",
//                this->WindowFrame->GetWidgetName());
  
  this->FrameScroll->SetParent(this);
  this->FrameScroll->Create(this->GetPVApplication());

  this->Script("pack %s -pady 2 -fill both -expand yes -anchor n",
               this->FrameScroll->GetWidgetName());
  
    this->Script("pack %s -anchor n -fill both -expand yes",
                this->WindowFrame->GetWidgetName());

                
                
//                  this->Script("pack %s -anchor n -side top -fill x -expand t",
//                this->WindowFrame->GetWidgetName());
  
  this->notebook->SetParent(this->FrameScroll->GetFrame());
  this->notebook->SetMinimumHeight(510);
  this->notebook->Create(this->GetPVApplication());
  this->notebook->AlwaysShowTabsOn();
  //this->Script("pack %s -side top -anchor nw -expand y -fill both -padx 2 -pady 2", this->notebook->GetWidgetName());



  this->Script("pack %s -pady 2 -padx 2 -fill both -expand yes -anchor n",
               this->notebook->GetWidgetName());



  int page1 = this->notebook->AddPage("Model Configuration", "Model Configuration", NULL);
  int page2 = this->notebook->AddPage("General Configurations", "General Configurations", NULL);
  int page3 = this->notebook->AddPage("Solver Configurations", "Solver Configurations", NULL);
  int page4 = this->notebook->AddPage("IO Options", "IO Options", NULL);

	this->ChooseDirectoryDialog->Create(app);

  this->mc = this->notebook->GetFrame("Model Configuration");
  this->gc = this->notebook->GetFrame("General Configurations");
  this->sc = this->notebook->GetFrame("Solver Configurations");
  this->mi = this->notebook->GetFrame("IO Options");
  
    
  // criacao do segundo frame do tab "General Configurations"
  this->ConvergenceParametersFrame->SetParent(this->gc);
  this->ConvergenceParametersFrame->Create(this->GetPVApplication());
  this->ConvergenceParametersFrame->SetLabelText("Convergence Parameters");
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->ConvergenceParametersFrame->GetWidgetName());
  
  //////////////////////////////////////////////
  //page 1  ******* Model Configuration 
  //*********************************************************
  //*********************************************************
  
  this->labelForm->SetParent(this->mc);
  this->labelForm->Create(app);
  this->labelForm->SetText("Formulation");
  

	// como estes dois entries tem relacao com o combobox que define o tipo de formulation, 
 	// estes devem ser criados aqui para que se possa estabelecer a relacao com componentes já criadods 
  this->entryReEP->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryReEP->Create(app);
  this->entryReEP->SetWidgetName(entryReEP->GetWidgetName());
  this->entryReEP->SetValueAsDouble(30000);
  this->entryReEP->SetWidth(6);

  this->entryCeEP->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryCeEP->Create(app);
  this->entryCeEP->SetWidgetName(entryCeEP->GetWidgetName());
  this->entryCeEP->SetValueAsDouble(0.01);
  this->entryCeEP->SetWidth(6);

  //************
  this->radioLS->SetParent(this->mc);
  this->radioLS->Create(app);
  this->radioLS->SetText("Least-Squares");
  this->radioLS->SetBalloonHelpString("Variational formulation for solving the problem");
  this->radioLS->SetValue("LW");
  
  this->radioLSV->SetParent(this->mc);
  this->radioLSV->Create(app);
  this->radioLSV->SetText("Least-Squares - Viscoelastic");
  this->radioLSV->SetBalloonHelpString("Variational formulation for solving the problem");
  sprintf(buffer, "%s config -state normal; %s config -state normal", 
	this->entryReEP->GetWidgetName(), this->entryCeEP->GetWidgetName());
  this->radioLSV->SetCommand(NULL, buffer);
  this->radioLSV->SetValue("LS");
    
  this->radioLS->SetVariableName(this->radioLSV->GetVariableName());
  sprintf(buffer, "%s config -state disabled; %s config -state disabled",
	 entryReEP->GetWidgetName(), entryCeEP->GetWidgetName());
  this->radioLS->SetCommand(NULL, buffer);
  
  if (!this->SeachNullViscoELasticElements())
  	{
  	this->radioLS->SetSelectedState(1); //least squares
  	this->entryReEP->SetReadOnly(1);
  	this->entryCeEP->SetReadOnly(1);
  	}
  else
  	{
  	// achou elementos com viscoelasticidade nula
  	this->radioLSV->SetSelectedState(1);//-Squares - Viscoelastic
  	}
  this->labelDensity->SetParent(this->mc);  // General Tree Parameters
  this->labelDensity->Create(app);
  this->labelDensity->SetText("Density [g/cm^3]");
  
  this->entryDensity->SetParent(this->mc);
  this->entryDensity->Create(app);
  this->entryDensity->SetBalloonHelpString("Density of blood.");
  this->entryDensity->SetValueAsDouble(1.04);
  this->entryDensity->SetWidth(6);
  
  
  this->labelViscosity->SetParent(this->mc);
  this->labelViscosity->Create(app);
  this->labelViscosity->SetText("Viscosity [cpoise]");
  
  this->entryViscosity->SetParent(this->mc);
  this->entryViscosity->Create(app);
  this->entryViscosity->SetBalloonHelpString("Dynamic viscosidade of blood.");
  this->entryViscosity->SetValueAsDouble(0.04);
  this->entryViscosity->SetWidth(6);
  
  this->labelArterialWallLaw->SetParent(this->mc);
  this->labelArterialWallLaw->Create(app);
  this->labelArterialWallLaw->SetText("Arterial Wall Law");
  
  
  this->comboArterialWallLaw->SetParent(this->mc);
  this->comboArterialWallLaw->Create(app);
  this->comboArterialWallLaw->SetBalloonHelpString("Constitutive behavior for the arterial wall.");
	this->comboArterialWallLaw->ReadOnlyOn();


  // Velocity profile	-> 4
  this->labelVelocity->SetParent(this->mc);
  this->labelVelocity->Create(app);
  this->labelVelocity->SetText("Velocity Profile");

  this->thumbVel->SetParent(this->mc);
  this->thumbVel->Create(app);
  this->thumbVel->SetRange(1.0, 1.33);
 	this->thumbVel->SetMinimumValue(1.0);
  this->thumbVel->SetMaximumValue(1.33);
  this->thumbVel->ClampMinimumValueOn();
  this->thumbVel->ClampMaximumValueOn();
  this->thumbVel->SetResolution(0.025);
  this->thumbVel->DisplayEntryOn();
  this->thumbVel->DisplayEntryAndLabelOnTopOff();
  this->thumbVel->SetValue(1.0);
  this->thumbVel->SetBalloonHelpString("Coefficient accounting for different velocities. (1: flat / 1.33: parabolic)");
  
  

  this->labelModel->SetParent(this->mc);
  this->labelModel->Create(app);
  this->labelModel->SetText("Model");
  
  this->radioCompliant->SetParent(this->mc);
  this->radioCompliant->Create(app);
  this->radioCompliant->SetText("Compliant");
  this->radioCompliant->SetValue("Yes");
  
  this->radioRigid->SetParent(this->mc);
  this->radioRigid->Create(app);
  this->radioRigid->SetText("Rigid");
  this->radioRigid->SetValue("No");
  
  this->radioCompliant->SetVariableName(this->radioRigid->GetVariableName());

  
  this->labelBloodLaw->SetParent(this->mc);
  this->labelBloodLaw->Create(app);
  this->labelBloodLaw->SetText("Blood Constitutive Law");
  
  this->radioBloodLawNewton->SetParent(this->mc);
  this->radioBloodLawNewton->Create(app);
  this->radioBloodLawNewton->SetText("Newton");
  this->radioBloodLawNewton->SetValue("Newton");

  this->radioBloodLawCasson->SetParent(this->mc);
  this->radioBloodLawCasson->Create(app);
  this->radioBloodLawCasson->SetText("Casson");
  this->radioBloodLawCasson->SetValue("Casson");
  
  this->radioBloodLawNewton->SetVariableName(this->radioBloodLawCasson->GetVariableName());


  this->labelAVisco->SetParent(this->mc);
  this->labelAVisco->Create(app);
  this->labelAVisco->SetText("Asymptotic Viscosity [cpoise]");
 
 
  this->entryAVisco->SetParent(this->mc);
  this->entryAVisco->Create(app);
  this->entryAVisco->SetBalloonHelpString("");
	this->entryAVisco->SetValueAsDouble(0.04);
  this->entryAVisco->SetWidth(6);
  
  this->labelLimitStress->SetParent(this->mc);
  this->labelLimitStress->Create(app);
  this->labelLimitStress->SetText("Limit Stress [dyn/cm^2]");

  this->entryLimitStress->SetParent(this->mc);
  this->entryLimitStress->Create(app);
  this->entryLimitStress->SetBalloonHelpString("");
	this->entryLimitStress->SetValueAsDouble(0.038);
  this->entryLimitStress->SetWidth(6);
  

  this->labelRegularParameter->SetParent(this->mc);
  this->labelRegularParameter->Create(app);
  this->labelRegularParameter->SetText("Regularization Parameters");

  
  this->comboRegularParameter->SetParent(this->mc);
  this->comboRegularParameter->Create(app);
  this->comboRegularParameter->SetWidth(8);
  this->comboRegularParameter->ReadOnlyOn();
  //this->comboRegularParameter->SetBalloonHelpString("Blood Constitutive Law.");
  const char *RegularParameterOpt[] = { "0.1", "0.01", "0.001", "0.0001", "0.00001", "0.000001" };
  for (int i = 0; i < 6; i++)
		this->comboRegularParameter->AddValue(RegularParameterOpt[i]);
	this->comboRegularParameter->SetValue("0.1");

	this->radioBloodLawCasson->SetCommand(this, "UpdateCassonRelatedWidgets");
	this->radioBloodLawNewton->SetCommand(this, "UpdateNewtonRelatedWidgets");
  
 	  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelModel->GetWidgetName());
 	  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->radioCompliant->GetWidgetName());
   	this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->radioRigid->GetWidgetName());

 	  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelDensity->GetWidgetName());
		this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entryDensity->GetWidgetName());

 	  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelBloodLaw->GetWidgetName());
 	  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->radioBloodLawNewton->GetWidgetName());
 	  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->radioBloodLawCasson->GetWidgetName());
 	  
 
 	  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelViscosity->GetWidgetName());
		this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->entryViscosity->GetWidgetName());
		
	  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->labelAVisco->GetWidgetName());
	  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2 -sticky w", this->entryAVisco->GetWidgetName());
		
	  this->Script("grid %s -row 7 -column 0 -padx 2 -pady 2 -sticky w", this->labelLimitStress->GetWidgetName());
	  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2 -sticky w", this->entryLimitStress->GetWidgetName());

	  this->Script("grid %s -row 8 -column 0 -padx 2 -pady 2 -sticky w", this->labelRegularParameter->GetWidgetName());
	  this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2 -sticky w", this->comboRegularParameter->GetWidgetName());
	  
 		this->Script("grid %s -row 9 -column 0 -padx 2 -pady 2 -sticky w", this->labelArterialWallLaw->GetWidgetName());
	  this->Script("grid %s -row 9 -column 1 -padx 2 -pady 2 -sticky w", this->comboArterialWallLaw->GetWidgetName());
	  
	  
	 	const char *ArterialWallLawOpt[] = { "Elastic", "Viscoelastic" };
  	for (int i = 0; i < 2; i++)
			this->comboArterialWallLaw->AddValue(ArterialWallLawOpt[i]);
	
		this->comboArterialWallLaw->SetValue("Elastic");
    
  
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // page 2 ******* General Configuration
  /////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  this->SolverTimeConfigFrame->SetParent(this->gc);
  this->SolverTimeConfigFrame->Create(this->GetPVApplication());
  this->SolverTimeConfigFrame->SetLabelText("Time Configuration");
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->SolverTimeConfigFrame->GetWidgetName());

  
  //************ primeiro frame "Time Configuration"
  this->entryInitialTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryInitialTime->Create(app);
  this->entryInitialTime->SetBalloonHelpString("Simulation initial time");  
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2", this->entryInitialTime->GetWidgetName());
  this->entryInitialTime->SetValueAsDouble(0.00);
  this->entryInitialTime->SetWidth(6);
  
  
  this->ResumeLabel->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->ResumeLabel->Create(app);
  this->ResumeLabel->SetText("Resume from Previous Results");
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->ResumeLabel->GetWidgetName());
  
  this->radioResumeYes->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->radioResumeYes->Create(app);
  this->radioResumeYes->SetText("Yes");
  this->radioResumeYes->SetValue("Yes");
   
  this->radioResumeNo->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->radioResumeNo->Create(app);
  this->radioResumeNo->SetText("No");
  this->radioResumeNo->SetValue("No");
  
	sprintf(buffer, "%s config -state normal", this->entryInitialTime->GetWidgetName());
	//this->radioResumeNo->SetCommand(NULL, buffer);
 
 this->radioResumeNo->SetCommand(this, "EnableInitialTimeStatus");
  
  
  
  this->radioResumeNo->SetSelectedState(1);
  this->radioResumeYes->SetVariableName(this->radioResumeNo->GetVariableName());
  
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->radioResumeYes->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->radioResumeNo->GetWidgetName());
  
  
  this->labelNumberOfTimeSteps->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelNumberOfTimeSteps->Create(app);
  this->labelNumberOfTimeSteps->SetText("Number of Time Steps ");
  this->labelNumberOfTimeSteps->SetBalloonHelpString("Number of Time Steps");  
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->labelNumberOfTimeSteps->GetWidgetName());
  
  this->entryNumberOfTimeSteps->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryNumberOfTimeSteps->Create(app);
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->entryNumberOfTimeSteps->GetWidgetName());
  this->entryNumberOfTimeSteps->SetValueAsDouble(320);
  this->entryNumberOfTimeSteps->SetWidth(6);
  
  this->labelTimeStep->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelTimeStep->Create(app);
  this->labelTimeStep->SetText("Time Step [s]");
  this->labelTimeStep->SetBalloonHelpString("Time Step");  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelTimeStep->GetWidgetName());
  
  
  this->entryTimeStep->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryTimeStep->Create(app);
  this->entryTimeStep->SetBalloonHelpString("Time step for finite differences scheme in time discretization");  
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2", this->entryTimeStep->GetWidgetName());
  this->entryTimeStep->SetValueAsDouble((this->GetOriginalFinalTime() -  this->GetInitialTime())/this->entryNumberOfTimeSteps->GetValueAsInt());
  this->entryTimeStep->SetWidth(6);
  this->entryTimeStep->SetValueAsDouble(0.0025);
  
  
  this->labelInitialTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelInitialTime->Create(app);
  this->labelInitialTime->SetText("Initial Time [s]");
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelInitialTime->GetWidgetName());
  
  
  this->labelNumberOfCardiacCycles->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelNumberOfCardiacCycles->Create(app);
  this->labelNumberOfCardiacCycles->SetText("Number of Cardiac Cycles ");
  this->labelNumberOfCardiacCycles->SetBalloonHelpString("Number of Cardiac Cycles ");  
  
  this->entryNumberOfCardiacCycles->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryNumberOfCardiacCycles->Create(app);
  this->entryNumberOfCardiacCycles->SetValueAsInt(1);
  this->entryNumberOfCardiacCycles->SetWidth(6);
  
  
  this->labelCardiacCycleTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelCardiacCycleTime->Create(app);
  this->labelCardiacCycleTime->SetText("Cardiac Cycle Time ");
  
  this->entryCardiacCycleTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryCardiacCycleTime->Create(app);
  this->entryCardiacCycleTime->SetValueAsDouble(this->GetOriginalFinalTime());
  this->entryCardiacCycleTime->SetWidth(6);
  this->entryCardiacCycleTime->SetReadOnly(1);





  
  this->labelFinalTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelFinalTime->Create(app);
  this->labelFinalTime->SetText("Final Time [s]");
  this->Script("grid %s -row 7 -column 0 -padx 2 -pady 2", this->labelFinalTime->GetWidgetName());

  this->entryFinalTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryFinalTime->Create(app);
  this->entryFinalTime->SetBalloonHelpString("Simulation final time");  
  this->entryFinalTime->SetWidth(6);
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2", this->entryFinalTime->GetWidgetName());
  this->entryFinalTime->SetValueAsDouble(0.8);
  
  this->CalculateTimeStepButton->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->CalculateTimeStepButton->Create(app);
  this->CalculateTimeStepButton->SetWidth(20);
  this->CalculateTimeStepButton->SetText("Calculate Time Parameters");
  this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2", this->CalculateTimeStepButton->GetWidgetName());
  
    
  // componenetes do segundo frame "Convergence Parameters"
  //------------------------------------------------
  
  this->labelVariable->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelVariable->Create(app);
  this->labelVariable->SetText("Variable");
  this->labelVariable->SetBalloonHelpString("Name of the variable");  
  
  this->labelFlux->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelFlux->Create(app);
  this->labelFlux->SetText(" Flux\n[cm^3/sec]");
  this->labelFlux->SetBalloonHelpString("Name of the variable");  
  
  this->labelElPre->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelElPre->Create(app);
  this->labelElPre->SetWidgetName(labelElPre->GetWidgetName());
  this->labelElPre->SetText("  Elastic\n Pressure\n[dyn/cm^2]");
  this->labelElPre->SetBalloonHelpString("Name of the variable");  
  
  this->labelArea->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelArea->Create(app);
  this->labelArea->SetText(" Area\n[cm^2]");
  this->labelArea->SetBalloonHelpString("Name of the variable");  
  
  this->labelPress->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelPress->Create(app);
  this->labelPress->SetText(" Pressure\n[dyn/cm^2]");
  this->labelPress->SetBalloonHelpString("Name of the variable");  
  this->Script("grid %s -row 0 -column 4 -padx 2 -pady 2", this->labelPress->GetWidgetName());
  
  this->labelRefVal->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelRefVal->Create(app);
  this->labelRefVal->SetText("Reference Value");
  this->labelRefVal->SetBalloonHelpString("Reference value from which convergence is measured");  
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelRefVal->GetWidgetName());
  
  this->labelConEr->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelConEr->Create(app);
  this->labelConEr->SetText("Convergence Error");
  this->labelConEr->SetBalloonHelpString("Error value admissible with respect to the reference value");  
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->labelConEr->GetWidgetName());
 
  this->entryReFl->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryReFl->Create(app);
  this->entryReFl->SetValueAsDouble(1);
  this->entryReFl->SetWidth(6);
  
  this->entryCeFl->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryCeFl->Create(app);
  this->entryCeFl->SetValueAsDouble(0.01);
  this->entryCeFl->SetWidth(6);
  
  this->entryReAr->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryReAr->Create(app);
  this->entryReAr->SetValueAsDouble(0.1);
  this->entryReAr->SetWidth(6);
  

  this->entryCeAr->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryCeAr->Create(app);
  this->entryCeAr->SetValueAsDouble(0.01);
  this->entryCeAr->SetWidth(6);


  this->entryRePr->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRePr->Create(app);
  this->Script("grid %s -row 1 -column 4 -padx 2 -pady 2", this->entryRePr->GetWidgetName());
  this->entryRePr->SetValueAsDouble(30000);
  this->entryRePr->SetWidth(6);
  
  this->entryCePr->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryCePr->Create(app);
  this->Script("grid %s -row 2 -column 4 -padx 2 -pady 2", this->entryCePr->GetWidgetName());
  this->entryCePr->SetValueAsDouble(0.01);
  this->entryCePr->SetWidth(6);

  this->labelIter->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelIter->Create(app);
  this->labelIter->SetText("Iterations");
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter->GetWidgetName());

  this->entryIter->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryIter->Create(app);
  this->entryIter->SetBalloonHelpString("Maximum number of iterations without convergence");  
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter->GetWidgetName());
  this->entryIter->SetValueAsInt(100);
  this->entryIter->SetWidth(6);

  this->labelSuPar->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelSuPar->Create(app);
  this->labelSuPar->SetText("Subrelaxation\nParameter");
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar->GetWidgetName());

  this->scaleSuPar->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->scaleSuPar->Create(app);
  this->scaleSuPar->SetRange(0.0, 1.0);
  this->scaleSuPar->SetResolution(0.1);
  this->scaleSuPar->SetEntryPositionToRight();
  this->scaleSuPar->SetBalloonHelpString("Subrelaxation of solution between iterations");
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar->GetWidgetName());
  this->scaleSuPar->SetValue(0.8);

  // componentes 3D
  
  this->labelVelocityX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelVelocityX->Create(app);
  this->labelVelocityX->SetText("Velocity \nin x-direction\n [cm/sec]");
  //this->labelVelocityX->SetBalloonHelpString("");  
  
  this->labelVelocityY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelVelocityY->Create(app);
  this->labelVelocityY->SetText("Velocity \nin y-direction \n[cm/sec]");
  //this->labelVelocityY->SetBalloonHelpString("");  

  this->labelVelocityZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelVelocityZ->Create(app);
  this->labelVelocityZ->SetText("Velocity \nin z-direction \n[cm/sec]");
  //this->labelVelocityX->SetBalloonHelpString("");  


  this->labelDisplacementX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelDisplacementX->Create(app);
  this->labelDisplacementX->SetText("Displacement \nin x-direction \n[cm/sec]");
  //this->labelDisplacementX->SetBalloonHelpString("");  
  
  this->labelDisplacementY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelDisplacementY->Create(app);
  this->labelDisplacementY->SetText("Displacement \nin y-direction \n[cm/sec]");
  //this->labelVelocityY->SetBalloonHelpString("");  

  this->labelDisplacementZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->labelDisplacementZ->Create(app);
  this->labelDisplacementZ->SetText("Displacement \nin z-direction\n [cm/sec]");
  //this->labelDisplacementZ->SetBalloonHelpString("");  


  this->entryRefVelocityX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefVelocityX->Create(app);
  this->entryRefVelocityX->SetValueAsDouble(1);
  this->entryRefVelocityX->SetWidth(6);    
    
  this->entryRefVelocityY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefVelocityY->Create(app);
  this->entryRefVelocityY->SetValueAsDouble(1);
  this->entryRefVelocityY->SetWidth(6);    

  this->entryRefVelocityZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefVelocityZ->Create(app);
  this->entryRefVelocityZ->SetValueAsDouble(1);
  this->entryRefVelocityZ->SetWidth(6);    


  this->entryRefDisplacementX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefDisplacementX->Create(app);
  this->entryRefDisplacementX->SetValueAsDouble(0.01);
  this->entryRefDisplacementX->SetWidth(6);    

  this->entryRefDisplacementY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefDisplacementY->Create(app);
  this->entryRefDisplacementY->SetValueAsDouble(0.01);
  this->entryRefDisplacementY->SetWidth(6);    

  this->entryRefDisplacementZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryRefDisplacementZ->Create(app);
  this->entryRefDisplacementZ->SetValueAsDouble(0.01);
  this->entryRefDisplacementZ->SetWidth(6);    


  this->entryConvVelocityX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvVelocityX->Create(app);
  this->entryConvVelocityX->SetValueAsDouble(0.01);
  this->entryConvVelocityX->SetWidth(6);    

  this->entryConvVelocityY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvVelocityY->Create(app);
  this->entryConvVelocityY->SetValueAsDouble(0.01);
  this->entryConvVelocityY->SetWidth(6);    

  this->entryConvVelocityZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvVelocityZ->Create(app);
  this->entryConvVelocityZ->SetValueAsDouble(0.01);
  this->entryConvVelocityZ->SetWidth(6);    


  this->entryConvDisplacementX->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvDisplacementX->Create(app);
  this->entryConvDisplacementX->SetValueAsDouble(0.01);
  this->entryConvDisplacementX->SetWidth(6);    

  this->entryConvDisplacementY->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvDisplacementY->Create(app);
  this->entryConvDisplacementY->SetValueAsDouble(0.01);
  this->entryConvDisplacementY->SetWidth(6);    

  this->entryConvDisplacementZ->SetParent(this->ConvergenceParametersFrame->GetFrame());
  this->entryConvDisplacementZ->Create(app);
  this->entryConvDisplacementZ->SetValueAsDouble(0.01);
  this->entryConvDisplacementZ->SetWidth(6);    


  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->labelVariable->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->labelVelocityX->GetWidgetName());
  this->Script("grid %s -row 0 -column 2 -padx 2 -pady 2", this->labelVelocityY->GetWidgetName());
  this->Script("grid %s -row 0 -column 3 -padx 2 -pady 2", this->labelVelocityZ->GetWidgetName());
  this->Script("grid %s -row 0 -column 5 -padx 2 -pady 2", this->labelDisplacementX->GetWidgetName());
  this->Script("grid %s -row 0 -column 6 -padx 2 -pady 2", this->labelDisplacementY->GetWidgetName());
  this->Script("grid %s -row 0 -column 7 -padx 2 -pady 2", this->labelDisplacementZ->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->entryRefVelocityX->GetWidgetName());
  this->Script("grid %s -row 1 -column 2 -padx 2 -pady 2", this->entryRefVelocityY->GetWidgetName());
  this->Script("grid %s -row 1 -column 3 -padx 2 -pady 2", this->entryRefVelocityZ->GetWidgetName());
  this->Script("grid %s -row 1 -column 5 -padx 2 -pady 2", this->entryRefDisplacementX->GetWidgetName());
  this->Script("grid %s -row 1 -column 6 -padx 2 -pady 2", this->entryRefDisplacementY->GetWidgetName());
  this->Script("grid %s -row 1 -column 7 -padx 2 -pady 2", this->entryRefDisplacementZ->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->entryConvVelocityX->GetWidgetName());
  this->Script("grid %s -row 2 -column 2 -padx 2 -pady 2", this->entryConvVelocityY->GetWidgetName());
  this->Script("grid %s -row 2 -column 3 -padx 2 -pady 2", this->entryConvVelocityZ->GetWidgetName());
  this->Script("grid %s -row 2 -column 5 -padx 2 -pady 2", this->entryConvDisplacementX->GetWidgetName());
  this->Script("grid %s -row 2 -column 6 -padx 2 -pady 2", this->entryConvDisplacementY->GetWidgetName());
  this->Script("grid %s -row 2 -column 7 -padx 2 -pady 2", this->entryConvDisplacementZ->GetWidgetName());
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // page 3 *********Solver Configuration

  this->labelTethaScheme->SetParent(this->sc);
  this->labelTethaScheme->Create(app);
  this->labelTethaScheme->SetText("Parameter Theta-Scheme");
  
  this->thumbTheta->SetParent(this->sc);
  this->thumbTheta->Create(app);
  this->thumbTheta->SetRange(0.0, 1.0);
  this->thumbTheta->SetMinimumValue(0.0);
  this->thumbTheta->SetMaximumValue(1.0);
  this->thumbTheta->ClampMinimumValueOn();
  this->thumbTheta->ClampMaximumValueOn();
  this->thumbTheta->SetResolution(0.05);
  this->thumbTheta->DisplayEntryOn();
  this->thumbTheta->DisplayEntryAndLabelOnTopOff();
  this->thumbTheta->SetValue(0.5);
  this->thumbTheta->SetBalloonHelpString("Parameter for implicit-explicit numerical scheme (0: implicit / 1: explicit)");

  this->TetraCompressibilityLabel->SetParent(this->sc);
  this->TetraCompressibilityLabel->Create(app);
  this->TetraCompressibilityLabel->SetText("Fluid Artificial Compressibility: ");
	
	this->TetraCompressibilityEntry->SetParent(this->sc);
  this->TetraCompressibilityEntry->Create(app);
  this->TetraCompressibilityEntry->SetValueAsDouble(1.0E-05);
  this->TetraCompressibilityEntry->SetWidth(8);


  this->labelRenu->SetParent(this->sc);
  this->labelRenu->Create(app);
  this->labelRenu->SetText("Renumbering");

  this->entryRenu->SetParent(this->sc);
  this->entryRenu->Create(app);
  this->entryRenu->SetBalloonHelpString("Number of iterations of renumbering algorithm");
  this->entryRenu->SetValueAsInt(0);
  this->entryRenu->SetWidth(6);

  
  this->labelRLS->SetParent(this->sc);
  this->labelRLS->Create(app);
  this->labelRLS->SetText("Resolution of\nlinear system");

  this->comboRLS->SetParent(this->sc);
  this->comboRLS->Create(app);
	this->comboRLS->ReadOnlyOn();
 	this->comboRLS->SetBalloonHelpString("Method of resolution of the resulting linear system of equations");
 	
  this->comboRLS->DeleteAllValues();
  this->comboRLS->AddValue("Direct");
  this->comboRLS->AddValue("CG");
  this->comboRLS->AddValue("BCG"); 
  this->comboRLS->AddValue("DBCG"); 
  this->comboRLS->AddValue("CGNR");
  this->comboRLS->AddValue("BCGSTAB");
  this->comboRLS->AddValue("TFQMR");
  this->comboRLS->AddValue("FOM");
  this->comboRLS->AddValue("GMRES");
  this->comboRLS->AddValue("FGMRES");
  this->comboRLS->AddValue("DQGMRES");
  this->comboRLS->AddValue("CGS");
  
  this->comboRLS->SetValue("CGS");
 	

  this->labelRLSHelp->SetParent(this->sc);
  this->labelRLSHelp->Create(app);


  this->labelAditi->SetParent(this->sc);
  this->labelAditi->Create(app);
  this->labelAditi->SetText("Aditivity");

  const char *textYesNo = "Defines if terminals are considered aditively or not. "
		    "If 'Yes' option is choose, the 'Penalization' is the penalty factor for imposing the equations.";

  this->labelPena->SetParent(this->sc);
  this->labelPena->Create(app);
  this->labelPena->SetWidgetName(labelPena->GetWidgetName());
  this->labelPena->SetText("Penalization");

  this->entryPena->SetParent(this->sc);
  this->entryPena->Create(app); 
  this->entryPena->SetWidgetName(entryPena->GetWidgetName());
  this->entryPena->SetBalloonHelpString(textYesNo);
  this->entryPena->SetWidth(6);
  this->entryPena->SetReadOnly(1);

  this->radioYes->SetParent(this->sc);
  this->radioYes->Create(app);
  this->radioYes->SetText("Yes");
  sprintf(buffer, "%s config -state normal; %s config -state normal", 
	labelPena->GetWidgetName(), this->entryPena->GetWidgetName());
  this->radioYes->SetCommand(NULL, buffer);
  this->radioYes->SetValue("Yes");
  this->radioYes->SetBalloonHelpString(textYesNo);

  this->radioNo->SetParent(this->sc);
  this->radioNo->Create(app);
  this->radioNo->SetText("No");
  this->radioNo->SetValue("No");
  sprintf(buffer, "%s config -state disabled; %s config -state disabled", 
	labelPena->GetWidgetName(), this->entryPena->GetWidgetName());
  this->radioNo->SetCommand(NULL, buffer);
  this->radioNo->SetBalloonHelpString(textYesNo);

  this->radioYes->SetSelectedState(1);
  this->radioNo->SetVariableName(this->radioYes->GetVariableName());
  
  this->radioYes->SetVariableName(this->radioNo->GetVariableName());
  this->radioNo->SetSelectedState(1);

  this->frameIterativeSolver->SetParent(this->sc);
  this->frameIterativeSolver->Create(this->GetPVApplication());
  this->frameIterativeSolver->SetLabelText("Iterative Solver");
  
  this->framePreconditioning->SetParent(this->frameIterativeSolver->GetFrame());
  this->framePreconditioning->Create(this->GetPVApplication());
  this->framePreconditioning->SetLabelText("Preconditioning");

  
  this->labelSolverType->SetParent(this->sc);
  this->labelSolverType->Create(app);
  this->labelSolverType->SetWidgetName(labelSolverType->GetWidgetName());
  this->labelSolverType->SetText("Solver Type");
  
  
  this->labelConvergenceError->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelConvergenceError->Create(app);
  this->labelConvergenceError->SetText("Convergence Error");


  this->labelIterationsBeforeRestart->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelIterationsBeforeRestart->Create(app);
  this->labelIterationsBeforeRestart->SetText("Iterations before restart");
  
  this->labelkrylov->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelkrylov->Create(app);
  this->labelkrylov->SetText("Krylov Subspace");
  

  this->entryIterationsBeforeRestart->SetParent(this->frameIterativeSolver->GetFrame());
  this->entryIterationsBeforeRestart->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryIterationsBeforeRestart->SetWidth(6);
  this->entryIterationsBeforeRestart->SetValueAsInt(100);
  
  
  this->entrykrylov->SetParent(this->frameIterativeSolver->GetFrame());
  this->entrykrylov->Create(app); 
  this->entrykrylov->SetWidth(6);
  this->entrykrylov->SetValueAsInt(60);


  this->labelDispTolerance->SetParent(this->framePreconditioning->GetFrame());
  this->labelDispTolerance->Create(app);
  this->labelDispTolerance->SetText("Drop Tolerance");
  
  
  this->labelFParam->SetParent(this->framePreconditioning->GetFrame());
  this->labelFParam->Create(app);
  this->labelFParam->SetText("Fill Parameter");
  
  
  this->labelIncrementalVersion->SetParent(this->sc);
  this->labelIncrementalVersion->Create(app);
  this->labelIncrementalVersion->SetText("Incremental Version");
  
  
  this->comboSolverType->SetParent(this->sc);
  this->comboSolverType->Create(app);
	this->comboSolverType->ReadOnlyOn();
 	//this->comboSolverType->SetBalloonHelpString("");
  this->comboSolverType->AddValue("Sequential");
  this->comboSolverType->AddValue("Parallel");
  this->comboSolverType->SetValue("Sequential");
  
  this->entryConvergenceError->SetParent(this->frameIterativeSolver->GetFrame());
  this->entryConvergenceError->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryConvergenceError->SetWidth(6);
  this->entryConvergenceError->SetValueAsDouble(1E-10);
  
  
  this->entryDispTolerance->SetParent(this->framePreconditioning->GetFrame());
  this->entryDispTolerance->Create(app); 
  this->entryDispTolerance->SetWidth(6);
  this->entryDispTolerance->SetValueAsDouble(1E-10);
  
  this->entryFParam->SetParent(this->framePreconditioning->GetFrame());
  this->entryFParam->Create(app); 
  this->entryFParam->SetWidth(6);
  this->entryFParam->SetValueAsDouble(20);
  
  this->radioICYes->SetParent(this->sc);
  this->radioICYes->Create(app);
  this->radioICYes->SetText("Yes");
  this->radioICYes->SetValue("Yes");
 
  this->radioICNo->SetParent(this->sc);
  this->radioICNo->Create(app);
  this->radioICNo->SetText("No");
  this->radioICNo->SetValue("No");
  
	this->radioICYes->SetVariableName(this->radioICNo->GetVariableName());
   
  this->comboRLS->SetCommand(this, "UpdateResolutionComponents");
  
  //****************************************************************************************
  
  
  this->ParallelIterativeSolverFrame->SetParent(this->sc);
  this->ParallelIterativeSolverFrame->Create(this->GetPVApplication());
  this->ParallelIterativeSolverFrame->SetLabelText("Iterative Solver");
  
  
  
  
  this->ParallelNumberOfProcessorsLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelNumberOfProcessorsLabel->Create(app);
  this->ParallelNumberOfProcessorsLabel->SetText("Number of Processors");
  

  this->ParallelNumberOfProcessorsEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelNumberOfProcessorsEntry->Create(app); 
  //this->ParallelNumberOfProcessorsEntry->SetBalloonHelpString(textYesNo);
  this->ParallelNumberOfProcessorsEntry->SetWidth(6);
  this->ParallelNumberOfProcessorsEntry->SetValueAsInt(4);
  this->ParallelNumberOfProcessorsEntry->SetCommand(this, "ParallelNumberOfProcessorsCallBack");
  
  
  
  this->ParallelIterationsRestartLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelIterationsRestartLabel->Create(app);
  this->ParallelIterationsRestartLabel->SetText("GMRES Iterations Before Restart");
  this->ParallelIterationsRestartEntry->ReadOnlyOn();
  this->ParallelIterationsRestartLabel->SetState(0); 
  

  this->ParallelIterationsRestartEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelIterationsRestartEntry->Create(app); 
  //this->ParallelIterationsRestartEntry->SetBalloonHelpString(textYesNo);
  this->ParallelIterationsRestartEntry->SetWidth(6);
  this->ParallelIterationsRestartEntry->SetValueAsInt(100);
  
  
  this->ParallelRelativeErrorLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelRelativeErrorLabel->Create(app);
  this->ParallelRelativeErrorLabel->SetText("Relative Convergence Error");
  

  this->ParallelRelativeErrorEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelRelativeErrorEntry->Create(app); 
  //this->ParallelRelativeErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelRelativeErrorEntry->SetWidth(6);
  this->ParallelRelativeErrorEntry->SetValueAsDouble(1E-08);
  
  this->ParallelAbsErrorLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelAbsErrorLabel->Create(app);
  this->ParallelAbsErrorLabel->SetText("Absolute Convergence Error");
  

  this->ParallelAbsErrorEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelAbsErrorEntry->Create(app); 
  //this->ParallelAbsErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelAbsErrorEntry->SetWidth(6);
  this->ParallelAbsErrorEntry->SetValueAsDouble(1E-20); 


  this->ParallelMaxNumberIterLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelMaxNumberIterLabel->Create(app);
  this->ParallelMaxNumberIterLabel->SetText("Maximum Number of Iterations");
  

  this->ParallelMaxNumberIterEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelMaxNumberIterEntry->Create(app); 
  //this->ParallelAbsErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelMaxNumberIterEntry->SetWidth(6);
  this->ParallelMaxNumberIterEntry->SetValueAsInt(1000);  


  this->ParallelPrecondLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelPrecondLabel->Create(app);
  this->ParallelPrecondLabel->SetText("Preconditioning");
  

  this->ParallelPrecondCombo->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelPrecondCombo->Create(app); 
  this->ParallelPrecondCombo->ReadOnlyOn();
  
  //this->ParallelPrecondCombo->SetBalloonHelpString(textYesNo);
  this->ParallelPrecondCombo->SetWidth(8);
  this->ParallelPrecondCombo->DeleteAllValues();
  this->ParallelPrecondCombo->AddValue("None");
  this->ParallelPrecondCombo->AddValue("BJacobi");
  this->ParallelPrecondCombo->SetValue("None"); 
  
  
  
  
  
  //*****************************************************************************************
  
  
  
	  
   sprintf(buffer, "%s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;" 
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled",
  this->entryRefDisplacementX->GetWidgetName(),
  this->entryRefDisplacementY->GetWidgetName(),
	this->entryRefDisplacementZ->GetWidgetName(),
	this->entryConvDisplacementX->GetWidgetName(),
	this->entryConvDisplacementY->GetWidgetName(),
	this->entryConvDisplacementZ->GetWidgetName(),
	this->radioICYes->GetWidgetName(),
	this->radioICNo->GetWidgetName());
	
	this->radioRigid->SetCommand(NULL, buffer);
	  
	
	sprintf(buffer, "%s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal",
  this->entryRefDisplacementX->GetWidgetName(),
  this->entryRefDisplacementY->GetWidgetName(),
	this->entryRefDisplacementZ->GetWidgetName(),
	this->entryConvDisplacementX->GetWidgetName(),
	this->entryConvDisplacementY->GetWidgetName(),
	this->entryConvDisplacementZ->GetWidgetName(),
	this->radioICYes->GetWidgetName(),
	this->radioICNo->GetWidgetName());
	
	this->radioCompliant->SetCommand(NULL, buffer);
  
  //this->comboRLS->SetStateToDisabled();
	  
  // renumbering
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelRenu->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryRenu->GetWidgetName());
  
  
  // parameter theta scheme
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelTethaScheme->GetWidgetName());
	this->Script("grid %s -row 1 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->thumbTheta->GetWidgetName());

  // fluid artificial compressibility
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->TetraCompressibilityLabel->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->TetraCompressibilityEntry->GetWidgetName());
  
  // aditivity
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelAditi->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->radioYes->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->radioNo->GetWidgetName());
  
  // penalization
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelPena->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->entryPena->GetWidgetName());

  // incremental version	  
  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->labelIncrementalVersion->GetWidgetName());
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2 -sticky w", this->radioICYes->GetWidgetName());
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2 -sticky w", this->radioICNo->GetWidgetName());
  
	// Solver Type
  this->Script("grid %s -row 8 -column 0 -padx 2 -pady 2 -sticky w", this->labelSolverType->GetWidgetName());
  this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2 -sticky w", this->comboSolverType->GetWidgetName());
  
  // resolution of linear system
  this->Script("grid %s -row 9 -column 0 -padx 2 -pady 2 -sticky w", this->labelRLS->GetWidgetName());
	this->Script("grid %s -row 9 -column 1 -padx 2 -pady 2 -sticky w", this->comboRLS->GetWidgetName());
	//this->Script("grid %s -row 4 -column 2 -padx 2 -pady 2 -sticky w", this->labelRLSHelp->GetWidgetName());

  // iterative solver Frame 
  this->Script("grid %s -row 10 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->frameIterativeSolver->GetWidgetName());


  // iterations before restart (dentro de iterative solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelIterationsBeforeRestart->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryIterationsBeforeRestart->GetWidgetName());

  // convergence error (dentro de iterative solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelConvergenceError->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryConvergenceError->GetWidgetName());

  // krylov subspace (dentro de iterative solver frame)
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelkrylov->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entrykrylov->GetWidgetName());	

  // framePreconditioning frame (dentro de iterative solver frame)
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->framePreconditioning->GetWidgetName());


  // fill param (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelFParam->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryFParam->GetWidgetName());

  // drop tolerance (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelDispTolerance->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryDispTolerance->GetWidgetName());
	  
	  
  
  sprintf(buffer, "%s config -state disabled; %s config -state normal; %s config -state normal; %s config -state disabled",
	this->entryViscosity->GetWidgetName(),
  this->entryAVisco->GetWidgetName(),
	this->entryLimitStress->GetWidgetName(),
	this->radioICNo->GetWidgetName());
	
	
	this->comboSolverType->SetCommand(this, "UpdateLinearSystemLib");
	
	
	
	
	
	
	//this->Script("grid %s -row 11 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->ParallelIterativeSolverFrame->GetWidgetName());

	
	
	
	
	
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelNumberOfProcessorsLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelNumberOfProcessorsEntry->GetWidgetName());
  
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelIterationsRestartLabel->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelIterationsRestartEntry->GetWidgetName());
  
  
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelRelativeErrorLabel->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelRelativeErrorEntry->GetWidgetName());
  
  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelAbsErrorLabel->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelAbsErrorEntry->GetWidgetName());
    
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelMaxNumberIterLabel->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelMaxNumberIterEntry->GetWidgetName());

  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelPrecondLabel->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelPrecondCombo->GetWidgetName());
	
	
	
	
	
  //////////////////////////////////////////////////////////////////////////////////////  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // page 4      ****Miscellaneous 

  this->ScaleFactor3DLabel->SetParent(this->mi);
  this->ScaleFactor3DLabel->Create(app);
  this->ScaleFactor3DLabel->SetText("Scale Factor");
  
  this->ScaleFactor3DEntry->SetParent(this->mi);
  this->ScaleFactor3DEntry->Create(app);
  this->ScaleFactor3DEntry->SetBalloonHelpString("Parameter used to change scale from model - Default value is 1.0");
  this->ScaleFactor3DEntry->SetValueAsDouble(1.0);
  this->ScaleFactor3DEntry->SetWidth(6);
  
  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->ScaleFactor3DLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->ScaleFactor3DEntry->GetWidgetName());
  
  this->labelFileOutput->SetParent(this->mi);
  this->labelFileOutput->Create(app);
  this->labelFileOutput->SetText("File Output");
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelFileOutput->GetWidgetName());

  this->entryFileOutput->SetParent(this->mi);
  this->entryFileOutput->Create(app);
  this->entryFileOutput->SetBalloonHelpString("Number of time steps between writings of output file");  
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryFileOutput->GetWidgetName());
  this->entryFileOutput->SetValueAsInt(1);
  this->entryFileOutput->SetWidth(6);

  this->labelScreenOutput->SetParent(mi);
  this->labelScreenOutput->Create(app);
  this->labelScreenOutput->SetText("Screen Output");
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelScreenOutput->GetWidgetName());

  this->entryScreenOutput->SetParent(this->mi);
  this->entryScreenOutput->Create(app);
  this->entryScreenOutput->SetBalloonHelpString("Number of time steps between writings of screen information");
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entryScreenOutput->GetWidgetName());
  this->entryScreenOutput->SetValueAsInt(1);
  this->entryScreenOutput->SetWidth(6);
  
  this->labelLogFile->SetParent(mi);
  this->labelLogFile->Create(app);
  this->labelLogFile->SetText("Log File Name");
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelLogFile->GetWidgetName());

  this->entryLogFile->SetParent(this->mi);
  this->entryLogFile->Create(app);
  this->entryLogFile->SetBalloonHelpString("Output from solver execution");
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->entryLogFile->GetWidgetName());
  this->entryLogFile->SetValue("Log.txt");
  this->entryLogFile->SetWidth(16);
  

  this->labelFilesPath->SetParent(mi);
  this->labelFilesPath->Create(app);
  this->labelFilesPath->SetText("Path");
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->labelFilesPath->GetWidgetName());

  this->entryFilesPath->SetParent(this->mi);
  this->entryFilesPath->Create(app);
  this->entryFilesPath->SetBalloonHelpString("Location for file generation");
  
  //this->entryLogFile->SetBalloonHelpString("");
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->entryFilesPath->GetWidgetName());

  #ifdef _WIN32
    this->entryFilesPath->SetValue("c:/temp/");
	#else
    this->entryFilesPath->SetValue("/tmp");
	#endif
  this->entryFilesPath->SetWidth(25);

  this->pushButtonBrowse->SetParent(this->mi); 
  this->pushButtonBrowse->Create(app);
  this->pushButtonBrowse->SetText("Browse");
  this->Script("grid %s -row 4 -column 2 -padx 2 -pady 2 -sticky w", this->pushButtonBrowse->GetWidgetName());
  this->pushButtonBrowse->SetCommand(this, "SetGenerationFilesPath");
  
  
	this->SelectedSolverFilesLabel->SetParent(mi);
  this->SelectedSolverFilesLabel->Create(app);
  this->SelectedSolverFilesLabel->SetText("Select which file will be generated ");
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->SelectedSolverFilesLabel->GetWidgetName());
	
	this->SelectedSolverFilesRadioButtonSet->SetParent(mi);
	this->SelectedSolverFilesRadioButtonSet->Create(app);
	this->SelectedSolverFilesRadioButtonSet->SetBorderWidth(2);
	this->SelectedSolverFilesRadioButtonSet->SetReliefToGroove();
	
	
	for (int i = 0; i < 5; ++i)
		vtkKWRadioButton *temp = this->SelectedSolverFilesRadioButtonSet->AddWidget(i);
	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->SetText("All Files");	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->SetSelectedState(1);	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->SetText("Basparam.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->SetText("Mesh.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->SetText("Param.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->SetText("Inifile.txt");
	
	this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->SelectedSolverFilesRadioButtonSet->GetWidgetName());


  this->CommitChangesButtonPage1->SetParent(this->mi); 
  this->CommitChangesButtonPage1->Create(app);
  this->CommitChangesButtonPage1->SetText("Generate Solver Files");
  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->CommitChangesButtonPage1->GetWidgetName());
  
  this->labelFilesGenerationStatus->SetParent(mi);
  this->labelFilesGenerationStatus->Create(app);
  this->labelFilesGenerationStatus->SetText("Files not generated yet");
  //this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2", this->labelFilesGenerationStatus->GetWidgetName());
  
  
  this->textFilesGenerationStatus->SetParent(mi);
  this->textFilesGenerationStatus->Create(app);
  this->textFilesGenerationStatus->GetWidget()->SetText("Files not generated yet");
  this->textFilesGenerationStatus->GetWidget()->SetWidth(25);
  this->textFilesGenerationStatus->GetWidget()->SetHeight(6);
  this->textFilesGenerationStatus->GetWidget()->ReadOnlyOn();
  
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2", this->textFilesGenerationStatus->GetWidgetName());


  // ***************************************************
  // associando chamadas de metodos a componentes KWWidgets
  // ***************************************************
  this->CommitChangesButtonPage1->SetCommand(this, "ProcessValuesFromBasParamWindow");	
  this->radioResumeYes->SetCommand(this, "GetIniFileCurrentTime");
  this->CalculateTimeStepButton->SetCommand(this, "CalculateTimeParameters");

}	


//--------------------------------------------------------------------------------------------------------------------------

//Renumbering
int vtkPVHM3DSolverFilesConfigurationWidget::GetRenumbering()
{
  return this->entryRenu->GetValueAsInt(); 	
}

//--------------------------------------------------------------------------------------------------------------------------


int vtkPVHM3DSolverFilesConfigurationWidget::GetFileOutputControl()
{
  return this->entryFileOutput->GetValueAsInt();
  
}

//--------------------------------------------------------------------------------------------------------------------------


int vtkPVHM3DSolverFilesConfigurationWidget::GetScreenOutputControl()
{
  return this->entryScreenOutput->GetValueAsInt();
  
}
 

//--------------------------------------------------------------------------------------------------------------------------

// return 1 if current InfiFile must be considered
int vtkPVHM3DSolverFilesConfigurationWidget::GetResumeIniFile()
{
  if (this->radioResumeYes->GetSelectedState()==1)
  	return 1;
  else
  	return 0;
	
}

//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetTimeStep()
{
  return this->entryTimeStep->GetValueAsDouble();	
} 

//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetInitialTime()
{
  return this->entryInitialTime->GetValueAsDouble();	
} 

//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetFinalTime()
{
  return this->entryFinalTime->GetValueAsDouble();	
} 

//--------------------------------------------------------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::SetInitialTime(double result)
{
 	this->entryInitialTime->SetValueAsDouble(result);	
}


//--------------------------------------------------------------------------------------------------------------------------
double vtkPVHM3DSolverFilesConfigurationWidget::GetReferenceFlux()
{
 	return this->entryReFl->GetValueAsDouble();	
}

//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetConvergenceFlux()
{
 	return this->entryCeFl->GetValueAsDouble();
}

//--------------------------------------------------------------------------------------------------------------------------
double vtkPVHM3DSolverFilesConfigurationWidget::GetElasticPressure()
{
 	return this->entryReEP->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetConvergenceElasticPressure()
{
 	return this->entryCeEP->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetArea()
{
 	return this->entryReAr->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetConvergenceArea()
{
 	return this->entryCeAr->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetPressure()
{
 	return this->entryRePr->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetConvergencePressure()
{
 	return this->entryCePr->GetValueAsDouble();
}
//--------------------------------------------------------------------------------------------------------------------------

int vtkPVHM3DSolverFilesConfigurationWidget::GetIterations()
{
 	return this->entryIter->GetValueAsInt();
}
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetSubRelaxationParameter()
{
 	return this->scaleSuPar->GetValue();
}
//--------------------------------------------------------------------------------------------------------------------------

int vtkPVHM3DSolverFilesConfigurationWidget::GetFormulation()
{
  if (this->radioLSV->GetSelectedState())	
		return 4;
  else 
   	return 3;	
}

//--------------------------------------------------------------------------------------------------------------------------
int vtkPVHM3DSolverFilesConfigurationWidget::GetAditivity()
{
	if (this->radioYes->GetSelectedState())
	  return 1;
	else
	 	return 0; 	
}

//--------------------------------------------------------------------------------------------------------------------------
double vtkPVHM3DSolverFilesConfigurationWidget::GetPenalization()
{
	return this->entryPena->GetValueAsDouble();
}


//--------------------------------------------------------------------------------------------------------------------------
double vtkPVHM3DSolverFilesConfigurationWidget::GetDensity()
{
	return this->entryDensity->GetValueAsDouble();
}	
//--------------------------------------------------------------------------------------------------------------------------
double vtkPVHM3DSolverFilesConfigurationWidget::GetViscosity()
{
	return this->entryViscosity->GetValueAsDouble();
}	
//--------------------------------------------------------------------------------------------------------------------------
int vtkPVHM3DSolverFilesConfigurationWidget::GetArteryWallLaw()
{
	return (this->comboArterialWallLaw->GetValueIndex(this->comboArterialWallLaw->GetValue())+1);	 	 
}	
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetParameterTheta()
{
	return this->thumbTheta->GetValue();
}	
//--------------------------------------------------------------------------------------------------------------------------

double vtkPVHM3DSolverFilesConfigurationWidget::GetVelocityProfile()
{
	return this->thumbVel->GetValue();
}
//--------------------------------------------------------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::SetIntParameters()
{
  this->IntParam->SetValue(0, this->GetRenumbering());
  this->IntParam->SetValue(1, this->GetScreenOutputControl());
  this->IntParam->SetValue(2, this->GetFileOutputControl());
  this->IntParam->SetValue(3, this->GetIterations());
  this->IntParam->SetValue(4, this->GetFormulation());
  this->IntParam->SetValue(5, this->GetAditivity());
  this->IntParam->SetValue(6, this->GetArteryWallLaw());
  this->IntParam->SetValue(7, this->GetResumeIniFile());
}

// ----------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::SetDoubleParameters()
{
  this->DoubleParam->SetValue(0, this->GetTimeStep());
  this->DoubleParam->SetValue(1, this->GetInitialTime());
  this->DoubleParam->SetValue(2, this->GetFinalTime());
  this->DoubleParam->SetValue(3, this->GetReferenceFlux());
  this->DoubleParam->SetValue(4, this->GetConvergenceFlux());
  this->DoubleParam->SetValue(5, this->GetElasticPressure());
  this->DoubleParam->SetValue(6, this->GetConvergenceElasticPressure());
  this->DoubleParam->SetValue(7, this->GetArea());
  this->DoubleParam->SetValue(8, this->GetConvergenceArea());
  this->DoubleParam->SetValue(9, this->GetPressure());
  this->DoubleParam->SetValue(10, this->GetConvergencePressure());
  this->DoubleParam->SetValue(11, this->GetSubRelaxationParameter());
  this->DoubleParam->SetValue(12, this->GetPenalization());
  this->DoubleParam->SetValue(13, this->GetDensity());
  this->DoubleParam->SetValue(14, this->GetViscosity());
  this->DoubleParam->SetValue(15, this->GetParameterTheta());
  this->DoubleParam->SetValue(16, this->GetVelocityProfile());
}	
	
// ----------------------------------------------------------------------------
	
	
void vtkPVHM3DSolverFilesConfigurationWidget::SetWidgetProxy(vtkSMHMStraightModelWidgetProxy *proxy)
{
 this->proxy= proxy;	
} 	


// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::GetIniFileCurrentTime()
{
	if (this->proxy)
		{
		double result =this->proxy->GetIniFileCurrentTime();
	  if (result==-1)
	  	{
	    this->radioResumeNo->SetSelectedState(1);
	    vtkKWMessageDialog::PopupMessage(
	      this->GetPVApplication(), this->GetParentWindow(), "No IniFile.txt file Found", 
	      "Can not resume from previous results - IniFile.txt SolverGP file not found",
	      vtkKWMessageDialog::ErrorIcon);
	  	}
	  else
	  	{
	    this->SetInitialTime(result);
	    this->entryInitialTime->ReadOnlyOn();
	  	}	
		}
}

// ----------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::EnableInitialTimeStatus()
{
    this->entryInitialTime->ReadOnlyOff();
    this->SetInitialTime(0);
  		
}


// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::ProcessValuesFromBasParamWindow()
{
	this->CalculateTimeParameters();
	
	if ( (this->GetAditivity()) && (this->entryPena->GetValueAsDouble()==0) )
		{
		vtkKWMessageDialog::PopupMessage(
	      this->GetPVApplication(), this->GetParentWindow(), "Solver Configurations Tab Error", 
	      "Aditivity value not set",
	      vtkKWMessageDialog::ErrorIcon);
	  return;   
		}
	
	


	
		
 	if (this->GetFinalTime() <  this->GetInitialTime())
		{
		vtkKWMessageDialog::PopupMessage(
	      this->GetPVApplication(), this->GetParentWindow(), "General Configurations Tab Error", 
	      "Initial Time greater than Final Time. Please redefine Final Time value",
	      vtkKWMessageDialog::ErrorIcon);
	  return;
		}
	      
	if (this->GetFinalTime() ==  this->GetInitialTime())
		{
	    vtkKWMessageDialog::PopupMessage(
	      this->GetPVApplication(), this->GetParentWindow(), "General Configurations Tab Error", 
	      "Initial Time equal Final Time. Please redefine Final Time value",
	      vtkKWMessageDialog::ErrorIcon);
		return;
		}  
	 	this->SetDoubleParameters();
	  this->SetIntParameters();
	 	//this->Withdraw();
	 	
	
	time_t rawtime;
 	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	char buffer[1024];
	
	
		if (this->entryFilesPath->GetValue())
				{
				vtkSMIntVectorProperty *propInt = vtkSMIntVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("IntParam"));    
    		if (propInt)
    			{ 
    			propInt->SetElement(0,this->radioCompliant->GetSelectedState());
    			propInt->SetElement(1,this->radioBloodLawNewton->GetSelectedState());
    			propInt->SetElement(2,this->GetArteryWallLaw());
    			propInt->SetElement(3,this->GetRenumbering());
    			propInt->SetElement(4,this->GetFileOutputControl()); 
     			propInt->SetElement(5,this->GetScreenOutputControl()); 
     			propInt->SetElement(6,this->GetAditivity()); 
    			propInt->SetElement(7,this->radioICYes->GetSelectedState());
    			propInt->SetElement(8,this->GetIterations());
    			propInt->SetElement(9,this->GetResolutionSolverType());
    			propInt->SetElement(10,this->entryFParam->GetValueAsInt());
    			propInt->SetElement(11,this->entryIterationsBeforeRestart->GetValueAsInt());
    			propInt->SetElement(12,this->entrykrylov->GetValueAsInt());
    			propInt->SetElement(13,this->GetSolverType());
    			//parallel solver parameters
    			propInt->SetElement(14,this->ParallelNumberOfProcessorsEntry->GetValueAsInt());
    			propInt->SetElement(15,this->ParallelIterationsRestartEntry->GetValueAsInt());
    			propInt->SetElement(16,this->ParallelMaxNumberIterEntry->GetValueAsInt()  );
    			propInt->SetElement(17,this->ParallelPrecondCombo->GetValueIndex(this->ParallelPrecondCombo->GetValue()) );
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess IntParam filter property.");	  	
  				return;
	  			}		
		 		
				
				propInt = vtkSMIntVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("SelectedSolverFiles"));    
    		if (propInt)
    			{ 
    			if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
    				{		
	    			propInt->SetElement(0,1);
	    			propInt->SetElement(1,1);
	    			propInt->SetElement(2,1);
	    			propInt->SetElement(3,1);
    				}
    			else
    				{
	    			propInt->SetElement(0,this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->GetSelectedState());
	    			propInt->SetElement(1,this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState());
	    			propInt->SetElement(2,this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState());
	    			propInt->SetElement(3,this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState());
    				}
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess SelectedSolverFiles filter property.");	  	
  				return;
	  			}		
				
				
				
				
				/*
				Model Configuration Param 
				0 - Fluid Density
				1 - Viscosity
				2 - Asymptotic Viscosity
				3 - Limit Stress
				4 - Regularization Parameters
				*/
				vtkSMDoubleVectorProperty *propDouble = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("ModelConfigParam"));    
    		if (propDouble)
    			{ 
    			propDouble->SetElement(0,this->GetDensity());
    			propDouble->SetElement(1,this->GetViscosity());
    			propDouble->SetElement(2,this->entryAVisco->GetValueAsDouble()); 
     			propDouble->SetElement(3,this->entryLimitStress->GetValueAsDouble()); 
     			propDouble->SetElement(4,this->comboRegularParameter->GetValueAsDouble()); 
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess ModelConfigParam filter property.");	  	
  				return;
	  			}		
				
				
				
				vtkSMDoubleVectorProperty *propDouble8 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("ScaleFactor3D"));    
    		if (propDouble8)
    			{
					propDouble8->SetElement(0, this->ScaleFactor3DEntry->GetValueAsDouble());
   				}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess ScaleFactor3D filter property.");	  	
  				return;
	  			}	
				
				//					<!--Time Config size 3
				//		0 - time step
				//		1 - initial time
				//		2 - final time
				
				propDouble = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("TimeParam"));    
    		if (propDouble)
    			{
    			propDouble->SetElement(0,this->GetTimeStep());
    			propDouble->SetElement(1,this->GetInitialTime());
    			propDouble->SetElement(2,this->GetFinalTime()); 
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess TimeParam filter property.");	  	
  				return;
	  			}		

				//		0 - reference value Vel. X
				//		1 - reference value Vel. Y
				//		2 - reference value Vel. Z
				//		3 - reference value Pressure
				//		4 - reference Displacement X
				//		5 -	reference Displacement y
				//		6 -	reference Displacement Z
				//		7 - Conv. Error  Vel. X
				//		8 - Conv. Error  Vel. Y
				//		9 - Conv. Error  Vel. Z
				//		10 -Conv. Error  Pressure
				//		11 -Conv. Error Displacement X
				//		12 -Conv. Error Displacement y
				//		13 -Conv. Error Displacement Z
				//		14 - subrelaxation param				


				propDouble = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("ConvergenceParam"));    
    		if (propDouble)
    			{

    			propDouble->SetElement(0,this->entryRefVelocityX->GetValueAsDouble());
    			propDouble->SetElement(1,this->entryRefVelocityY->GetValueAsDouble());
    			propDouble->SetElement(2,this->entryRefVelocityZ->GetValueAsDouble());
    			propDouble->SetElement(3,this->GetPressure());
    			propDouble->SetElement(4,this->entryRefDisplacementX->GetValueAsDouble());
    			propDouble->SetElement(5,this->entryRefDisplacementY->GetValueAsDouble());
    			propDouble->SetElement(6,this->entryRefDisplacementZ->GetValueAsDouble());
    			
    			propDouble->SetElement(7,this->entryConvVelocityX->GetValueAsDouble());
    			propDouble->SetElement(8,this->entryConvVelocityY->GetValueAsDouble());
    			propDouble->SetElement(9,this->entryConvVelocityZ->GetValueAsDouble());
    			propDouble->SetElement(10,this->GetConvergencePressure());
    			propDouble->SetElement(11,this->entryConvDisplacementX->GetValueAsDouble());
    			propDouble->SetElement(12,this->entryConvDisplacementY->GetValueAsDouble());
    			propDouble->SetElement(13,this->entryConvDisplacementZ->GetValueAsDouble());
    			propDouble->SetElement(14,this->GetSubRelaxationParameter());
    			
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess ConvergenceParam filter property.");	  	
  				return;
	  			}		


				//			<!--Solver Configuration Param size 4
				//			0 - Param Theta
				//			1 - Fluid Artificil Compressibility
				//			2 - Convergence error
				//			3 - Drop tolerance
				//		-->	
				propDouble = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("SolverConfigParam"));    
    		if (propDouble)
    			{
    			propDouble->SetElement(0,this->GetParameterTheta());
    			propDouble->SetElement(1,this->TetraCompressibilityEntry->GetValueAsDouble());
    			propDouble->SetElement(2,this->entryConvergenceError->GetValueAsDouble());
    			propDouble->SetElement(3,this->entryDispTolerance->GetValueAsDouble());
    			// parallel solver parameters
    			propDouble->SetElement(4,this->ParallelRelativeErrorEntry->GetValueAsDouble());
    			propDouble->SetElement(5,this->ParallelAbsErrorEntry->GetValueAsDouble());
    			
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess SolverConfigParam filter property.");	  	
  				return;
	  			}		

	 			vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("LogFileName"));    
    		if (prop) 
    			prop->SetElement(0,this->entryLogFile->GetValue()); 
	  		else
	  			{
					vtkErrorMacro(<<"Cannot acess LogFileName filter property.");	  	
  				return;
	  			}		

				
				
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();

	 			// acessa a propriedade que guarda o caminho (File System) onde serao gerados
	 			// os arquivos do solver
	 			
	 			prop = vtkSMStringVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("GenerateSolverFiles"));    
    		if (prop) 
    			prop->SetElement(0,this->entryFilesPath->GetValue()); 
	  		else
	  			{
					vtkErrorMacro(<<"Cannot acess GenerateSolverFiles filter property.");	  	
  				return;
	  			}		
		 		this->GetPVSource()->GetProxy()->UpdateVTKObjects();
		 	
		  	// criando backup dos arquivos gerados
		  	//chdir(this->entryFilesPath->GetValue());
		  	
		  	vtksys::SystemTools::ChangeDirectory(this->entryFilesPath->GetValue());

				if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
					{
					// opcao para gerar todos arquivos
					system("cp Mesh.txt Mesh000.txt");
					system("cp IniFile.txt IniFile000.txt");
		  		system("cp Param.txt Param000.txt");
					}
				else
					{
					// gera somente um arquivo	
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState())
						system("cp Mesh.txt Mesh000.txt");
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState())
						system("cp Param.txt Param000.txt");
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState())
						system("cp IniFile.txt IniFile000.txt");
					}	
		  	
		  	
		  	
		  	char buffer2[1000];
		  	for (int i = 0; i < 1000; ++i)
		  		buffer2[i]=0;
				
				char buffer3[20];
		  	for (int i = 0; i < 20; ++i)
		  		buffer3[i]=0;
				
				
		  	strcat(buffer2, this->entryFilesPath->GetValue());

				if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
			  	{
			  	strcat(buffer2, "/Basparam.txt"); // se todos arquivos do solver serao gerados, testar somente se existe Basparam
			  	strcat(buffer3, "Files");
			  	}
				else
					{
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->GetSelectedState())
						{
						strcat(buffer2, "/Basparam.txt");
						strcat(buffer3, "Basparam.txt");
						}

					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState())
						{
						strcat(buffer2, "/Mesh.txt");
						strcat(buffer3, "Mesh.txt");
						}
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState())
						{
						strcat(buffer2, "/Param.txt");
						strcat(buffer3, "Param.txt");
						}
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState())
						{
						strcat(buffer2, "/IniFile.txt");
						strcat(buffer3, "IniFile.txt");
						}
					}

		  	
		  	if (vtksys::SystemTools::FileExists(buffer2))
		  		{
					if (!strcmp(this->textFilesGenerationStatus->GetWidget()->GetText(), "Files not generated yet"))
		  			sprintf(buffer, "*%s generated at %s - %s",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo)); 
		  		else
		  			sprintf(buffer, "*%s generated at %s - %s \n",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo)); 
		  		}
		  	else
		  		sprintf(buffer, "*ERROR! Could not generate %s at %s -%s",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo) ); 
		  	

  			this->textFilesGenerationStatus->GetWidget()->SetText(buffer);
  			
  			this->textFilesGenerationStatus->GetWidget()->Focus();
  				 	
			}
		
}

// ----------------------------------------------------------------------------
vtkDoubleArray *vtkPVHM3DSolverFilesConfigurationWidget::GetDoubleParametersArray()
{
  return this->DoubleParam;
}

// ---------------------------------------------------------------------------- 
	
vtkIntArray *vtkPVHM3DSolverFilesConfigurationWidget::GetIntParametersArray()
{
  return this->IntParam;
}
// ----------------------------------------------------------------------------

vtkPVApplication* vtkPVHM3DSolverFilesConfigurationWidget::GetPVApplication()
{
  return vtkPVApplication::SafeDownCast(this->GetApplication());
}

// ----------------------------------------------------------------------------


vtkPVWindow* vtkPVHM3DSolverFilesConfigurationWidget::GetPVWindow()
{
	vtkPVApplication *pvApp = this->GetPVApplication();

  if (pvApp == NULL)
    {
    return NULL;
    }
  
  return pvApp->GetMainWindow();
}
// ----------------------------------------------------------------------------

int vtkPVHM3DSolverFilesConfigurationWidget::SeachNullViscoELasticElements()
{
 	if (this->proxy)
 		return this->proxy->CheckForNullViscoElasticElements();	
	else
		return 0;	
}
// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::CalculateTimeParameters()
{
	
	if (!this->entryNumberOfTimeSteps->GetValueAsInt())
		{
		vtkKWMessageDialog::PopupMessage(
	      this->GetPVApplication(), this->GetParentWindow(), "Time Parameters error", 
	      "Number of Time Steps can not be ZERO. Please redefine Number of Time Steps value",
	      vtkKWMessageDialog::ErrorIcon);
	  return;   
		}
		
		this->entryTimeStep->SetValueAsDouble((this->GetFinalTime() -  this->GetInitialTime())/this->entryNumberOfTimeSteps->GetValueAsInt());
			
}

// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::SetModelType(const char *ModelName)
{
	strcpy (ModelType,ModelName);
	char buffer[1024];
  sprintf(buffer, "%s Solver Files Generation Setup", this->ModelType); 
  this->SetTitle(buffer);
}
		
// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::UpdateResolutionComponents()
{
	if (!strcmp(this->comboRLS->GetValue(), "Direct"))
		{
		this->entryConvergenceError->ReadOnlyOn();
		this->entryDispTolerance->ReadOnlyOn();
		this->entryFParam->ReadOnlyOn();
		}
	else
		{
		this->entryConvergenceError->ReadOnlyOff();
		this->entryDispTolerance->ReadOnlyOff();
		this->entryFParam->ReadOnlyOff();
		}
	// somente se for solver paralelo
  if (!strcmp(this->comboRLS->GetValue(), "GMRES") && this->GetSolverType())
    {
    this->ParallelIterationsRestartEntry->ReadOnlyOff();    
    this->ParallelIterationsRestartLabel->SetState(1);
    }
  else
    {
    this->ParallelIterationsRestartEntry->ReadOnlyOn();
    this->ParallelIterationsRestartLabel->SetState(0);  
    } 
	
	
}

// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::UpdateLinearSystemLib()
{

  this->comboRLS->SetStateToNormal();

	if (!strcmp(this->comboSolverType->GetValue(), "Sequential"))
		{
	  this->comboRLS->DeleteAllValues();
	  this->comboRLS->AddValue("Direct");
	  this->comboRLS->AddValue("CG");
	  this->comboRLS->AddValue("BCG"); 
	  this->comboRLS->AddValue("DBCG"); 
		this->comboRLS->AddValue("CGNR");
		this->comboRLS->AddValue("BCGSTAB");
		this->comboRLS->AddValue("TFQMR");
		this->comboRLS->AddValue("FOM");
		this->comboRLS->AddValue("GMRES");
		this->comboRLS->AddValue("FGMRES");
		this->comboRLS->AddValue("DQGMRES");
		this->comboRLS->AddValue("CGS");
		
		this->comboRLS->SetValue("CGS");
		
		// habilitar os campos Drop Tolerance e Fill Parameter
		this->entryDispTolerance->ReadOnlyOff();
	  this->entryFParam->ReadOnlyOff();
		
	   this->Script("grid remove %s", this->ParallelIterativeSolverFrame->GetWidgetName());
	   // iterative solver Frame 
	   this->Script("grid %s -row 10 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->frameIterativeSolver->GetWidgetName());
		}
		
	else
		{	 // parallel
	  this->comboRLS->DeleteAllValues();
		this->comboRLS->SetValue("CGS");
	  this->comboRLS->AddValue("CGS"); 
	  this->comboRLS->AddValue("GMRES"); 
		
	  
	  this->Script("grid %s -row 11 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->ParallelIterativeSolverFrame->GetWidgetName());

    this->Script("grid remove %s", this->frameIterativeSolver->GetWidgetName());
	  
	  
		// desablitar os campos Drop Tolerance e Fill Parameter
		this->entryDispTolerance->ReadOnlyOn();
	  this->entryFParam->ReadOnlyOn();
		
	  
	  char aux[50];
    sprintf(aux, "run.p%d.log", this->ParallelNumberOfProcessorsEntry->GetValueAsInt());  
    this->entryLogFile->SetValue(aux);
    this->entryLogFile->SetReadOnly(1);
	  
		}
}

// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::UpdateCassonRelatedWidgets()
{
	this->entryViscosity->ReadOnlyOn();
  this->entryAVisco->ReadOnlyOff();
	this->entryLimitStress->ReadOnlyOff();
	this->comboRegularParameter->SetStateToNormal();
	this->radioICNo->SetStateToDisabled();
	this->radioICYes->SelectedStateOn();

}

// ----------------------------------------------------------------------------

void vtkPVHM3DSolverFilesConfigurationWidget::UpdateNewtonRelatedWidgets()
{
	this->entryViscosity->ReadOnlyOff();
  this->entryAVisco->ReadOnlyOn();
	this->entryLimitStress->ReadOnlyOn();
	this->comboRegularParameter->SetStateToDisabled();

	this->radioICNo->SetStateToNormal();
	this->radioICYes->SelectedStateOn();
}
// ----------------------------------------------------------------------------

int vtkPVHM3DSolverFilesConfigurationWidget::GetResolutionSolverType()
{
	/* Sequential Solver Types
 *  
0   / Direct
100 / CG - Conjugate Gradient Method
101 / BCG - Bi-Conjugate Gradient Method
102 / DBCG - BCG with partial pivoting
103 / CGNR - Conjugate Gradient method (Normal Residual equation)
104 / BCGSTAB - BCG stabilized
105 / TFQMR - Transpose-Free Quasi-Minimum Residual method
106 / FOM - Full Orthogonalization Method
107 / GMRES - Generalized Minimum RESidual method
108 / FGMRES - Flexible version of Generalized Minimum RESidual method
109 / DQGMRES - Direct versions of Quasi Generalize Minimum RESidual method
110 / CGS - Conjugate Gradient Square method
 * 
 * */
	
	//cout << "Valor de RLS " << this->comboRLS->GetValue() << endl;
	//cout << "comp de RLS " << strcmp(this->comboRLS->GetValue(), "CG") << endl;
	
	
	if (!strcmp(this->comboRLS->GetValue(), "Direct"))
		return 0;


	if (!strcmp(this->comboRLS->GetValue(), "CG"))
		return 100;
	
	if (!strcmp(this->comboRLS->GetValue(), "BCG"))
		return 101;



	if (!strcmp(this->comboRLS->GetValue(), "DBCG"))
		return 102;



	if (!strcmp(this->comboRLS->GetValue(), "CGNR"))
		return 103;


	if (!strcmp(this->comboRLS->GetValue(), "BCGSTAB"))
		return 104;



	if (!strcmp(this->comboRLS->GetValue(), "TFQMR"))
		return 105;


	if (!strcmp(this->comboRLS->GetValue(), "FOM"))
		return 106;



	if (!strcmp(this->comboRLS->GetValue(), "GMRES"))
		return 107;



	if (!strcmp(this->comboRLS->GetValue(), "FGMRES"))
		return 108;



	if (!strcmp(this->comboRLS->GetValue(), "DQGMRES"))
			return 109;


	if (!strcmp(this->comboRLS->GetValue(), "CGS"))
		return 110;


return -1;
	
}

// ----------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::SetGenerationFilesPath()
{
	char Temp[50] ;
	sprintf(Temp, "Generate %s Solver Files at", this->ModelType);

	this->ChooseDirectoryDialog->SetTitle(Temp);
	this->ChooseDirectoryDialog->ChooseDirectoryOn();
	this->ChooseDirectoryDialog->Invoke();
	if (this->ChooseDirectoryDialog->GetLastPath())
		  this->entryFilesPath->SetValue(this->ChooseDirectoryDialog->GetLastPath());
}

// ----------------------------------------------------------------------------
// copia valor de propriedades do pvwidget do 3DFullModel
void vtkPVHM3DSolverFilesConfigurationWidget::SetVolumeTetraProps(
double density, int Newton, int Casson, double Visco, double Avisco, double LimitStress, double regularParam)
{
	this->entryDensity->SetValueAsDouble(density);

	if (Newton)
	 this->radioBloodLawNewton->SelectedStateOn();
	else 
  	this->radioBloodLawCasson->SelectedStateOn();

	this->entryViscosity->SetValueAsDouble(Visco);
	this->entryAVisco->SetValueAsDouble(Avisco);
	this->entryLimitStress->SetValueAsDouble(LimitStress);
	this->comboRegularParameter->SetValueAsDouble(regularParam);
}
// ----------------------------------------------------------------------------

const char *vtkPVHM3DSolverFilesConfigurationWidget::GetSolverFilesPath()
{
	return this->entryFilesPath->GetValue();
	
} 
// ----------------------------------------------------------------------------


int vtkPVHM3DSolverFilesConfigurationWidget::GetSolverType()
{
	return this->comboSolverType->GetValueIndex(this->comboSolverType->GetValue()); 
}


// ----------------------------------------------------------------------------


void vtkPVHM3DSolverFilesConfigurationWidget::ParallelNumberOfProcessorsCallBack()
{
  char aux[50];
  sprintf(aux, "run.p%d.log", this->ParallelNumberOfProcessorsEntry->GetValueAsInt());  
  this->entryLogFile->SetValue(aux);
  this->entryLogFile->SetReadOnly(1);

}




