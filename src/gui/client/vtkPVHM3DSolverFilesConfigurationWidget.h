#ifndef _vtkPVHM3DSolverFilesConfigurationWidget_h_
#define _vtkPVHM3DSolverFilesConfigurationWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWApplication.h"
#include "vtkKWWindow.h"
#include "vtkPVApplication.h"
#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWPushButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkKWUserInterfaceManager.h"
#include "vtkKWEntry.h"
#include "vtkKWComboBox.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWThumbWheel.h"
#include <vtksys/SystemTools.hxx>
#include <vtksys/CommandLineArguments.hxx>
#include "vtkKWFrameWithLabel.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkKWTopLevel.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWComboBoxWithLabel.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWUserInterfacePanel.h"
#include "vtkKWTreeWithScrollbars.h"
#include "vtkKWTree.h"
#include "vtkPVWindow.h"
#include "vtkPVSource.h"
#include "vtkKWText.h"
#include "vtkKWTextWithScrollbars.h"
#include "vtkKWRadioButtonSet.h"


class VTK_EXPORT vtkPVHM3DSolverFilesConfigurationWidget : public vtkKWTopLevel
{
public:
  static vtkPVHM3DSolverFilesConfigurationWidget *New();
  vtkTypeRevisionMacro(vtkPVHM3DSolverFilesConfigurationWidget, vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent){};
  

  virtual void Create(vtkKWApplication *app);
  
  int GetRenumbering();
  int GetFileOutputControl();
  int GetScreenOutputControl();	
  int GetResumeIniFile();
  int GetIterations();
  int GetFormulation();
  int GetAditivity();
  int GetArteryWallLaw();
  double GetTimeStep();
  double GetInitialTime();
  double GetFinalTime();
  void SetInitialTime(double result);
  double GetReferenceFlux();
  double GetConvergenceFlux();
  double GetElasticPressure();
  double GetConvergenceElasticPressure();
  double GetArea();
  double GetConvergenceArea();
  double GetPressure();
  double GetConvergencePressure();
  double GetSubRelaxationParameter();
  double GetPenalization();
  double GetDensity();
  double GetViscosity();
  double GetParameterTheta();
  double GetVelocityProfile();
	
	
	// Description: 
  // sets an array size 7 with BasParam interger parameters
	// 0 Renumbering
	// 1 Screen output
	// 2 File output
	// 3 Iterations
	// 4 Formulation
	// 5 Aditivity
	// 6 Artery Wall Law
	void SetIntParameters();
	
	
  // Description: 
  // sets an array size 17 with BasParam double parameters
	//0 TimeStep;
	//1 InitialTime;
	//2 FinalTime;
	//3 ReferenceFlux;
	//4 ConvergenceFlux;
	//5 ElasticPressure;
	//6 ConvergenceElasticPressure;
	//7 Area;
	//8 ConvergenceArea;
	//9 Pressure;
	//10 ConvergencePressure;
	//11 SubRelaxationParameter;
	//12 Penalization;
	//13 Density;
	//14 Viscosity;
	//15 ParameterTheta;
	//16 VelocityProfile;
 	void SetDoubleParameters();
 	
 	// Description: 
  // Get method for double parameters
  vtkDoubleArray *GetDoubleParametersArray();
 	
 	// Description: 
  // Get method for integer parameters
  vtkIntArray *GetIntParametersArray();
    
  
  // Description: 
  // set widget proxy from PVStraightModel
  void SetWidgetProxy(vtkSMHMStraightModelWidgetProxy *proxy);
  
  
  // Description: 
  // sets initial time with  inifile's current time 
  void GetIniFileCurrentTime();

  
  // Description: 
  // triggers solver files write process
  void ProcessValuesFromBasParamWindow();
  
  // Description: 
  // search in the StraightModel tree any segment with null viscoelasticity
  // return 1 if any is found 
  int SeachNullViscoELasticElements();
  
  // Description: 
  // calculate time parameters such as time step and final time.
  void CalculateTimeParameters();
  
  // Description: 
  // Set/Get methods for OriginalFinalTime
  vtkSetMacro(OriginalFinalTime,double);
  vtkGetMacro(OriginalFinalTime,double);
  
  
 vtkPVSource *GetPVSource()
 {return this->PVSource; }

 void SetPVSource(vtkPVSource *PVSource)
 {this->PVSource = PVSource; }

  
  // Description: 
  // return the path where solver input files were generated
  char *GetChoosedPath()
  {	return  this->ChooseDirectoryDialog->GetLastPath(); }  
    
 	// Description:
 	// set the model type (1D, 3D or coupled model)
	void SetModelType(const char *ModelName);
	
 	// Description:
 	//	
	void UpdateResolutionComponents();
 	
 	// Description:
 	//	
	void UpdateLinearSystemLib();
	
 	// Description:
 	//	
	void UpdateCassonRelatedWidgets();
	
 	// Description:
 	//	
	void UpdateNewtonRelatedWidgets();
	
 	// Description:
 	//
	void EnableInitialTimeStatus();
	
 	// Description:
 	//
	void SetPvWindow(	vtkPVWindow *PVWindow)
	{ 	this->PVWindow = PVWindow; }
	
 	// Description:
 	//
	int GetResolutionSolverType();
 	
 	// Description:
 	//	
	void SetGenerationFilesPath();
	
	
	void SetVolumeTetraProps(double density, int Newton, int Casson, double Visco, double Avisco,
 double LimitStress, double regularParam);
	
	
	const char *GetSolverFilesPath();
	
	
	void SetFinalTime(double time)
		{
		this->entryFinalTime->SetValueAsDouble(time);	
			
		}

	// Description:
	// 0 - sequential 1-paralle solver
	int GetSolverType();

	// Description
	// it only sets the string of the name of parallel solver log file name
	void ParallelNumberOfProcessorsCallBack();
	
//***************************************************************************************************** 
//***************************************************************************************************** 
//***************************************************************************************************** 

protected:
  vtkPVHM3DSolverFilesConfigurationWidget();
  virtual ~vtkPVHM3DSolverFilesConfigurationWidget();

  vtkPVApplication* GetPVApplication();
  vtkPVWindow* GetPVWindow();
  vtkKWEntry *entryReFl;
  
  vtkKWPushButton *CommitChangesButtonPage1;
 
  vtkKWNotebook *notebook;

  vtkKWWidget *mc;
  vtkKWWidget *gc;
  vtkKWWidget *sc;
  vtkKWWidget *mi;
  
  // page 1  Model Config*********************
  // ****************************************************************************************************************
  // ****************************************************************************************************************
  
  vtkKWFrameWithLabel *MCFrame;

  vtkKWLabel *labelForm;
  vtkKWRadioButton *radioLSV;
  vtkKWRadioButton *radioLS;
  
  vtkKWLabel *labelDensity;
  vtkKWEntry *entryDensity;
  
  vtkKWLabel *labelViscosity;
  vtkKWEntry *entryViscosity;
  
  vtkKWLabel *labelArterialWallLaw;
  vtkKWComboBox *comboArterialWallLaw;
  
  vtkKWLabel *labelVelocity;
  vtkKWThumbWheel *thumbVel;
  
  vtkKWLabel *labelModel;
  vtkKWRadioButton *radioCompliant;
  vtkKWRadioButton *radioRigid;

  // blood constitutive law
  vtkKWLabel *labelBloodLaw;
  vtkKWRadioButton *radioBloodLawNewton;
  vtkKWRadioButton *radioBloodLawCasson;
  
  //Viscosity
  //vtkKWLabel *labelVisco;
  //vtkKWEntry *entryVisco;
    
  //Asymptotic viscosity
  vtkKWLabel *labelAVisco;
  vtkKWEntry *entryAVisco;
  
  //Limit Stress
  vtkKWLabel *labelLimitStress;
  vtkKWEntry *entryLimitStress;
  
  //Regulation Parameter
  vtkKWLabel *labelRegularParameter;
  vtkKWComboBox *comboRegularParameter;
  
    
 // ****************************************************************************************************************
 // ****************************************************************************************************************
 // ******** page 2  General Config
  
  vtkKWFrameWithLabel *SolverTimeConfigFrame;
  vtkKWFrameWithLabel *ConvergenceParametersFrame;
  
  // Time Configuration frame 
  vtkKWLabel *ResumeLabel;
  vtkKWRadioButton *radioResumeYes;
  vtkKWRadioButton *radioResumeNo;

  vtkKWLabel *labelNumberOfTimeSteps;
  vtkKWEntry *entryNumberOfTimeSteps;

  vtkKWLabel *labelNumberOfCardiacCycles;
  vtkKWEntry *entryNumberOfCardiacCycles;


  vtkKWLabel *labelTimeStep;
  vtkKWEntry *entryTimeStep;
  vtkKWLabel *labelInitialTime;
  vtkKWEntry *entryInitialTime;
  vtkKWLabel *labelFinalTime;
  vtkKWEntry *entryFinalTime;
  
  // converegence parameters frame
  vtkKWLabel *labelVariable;
  vtkKWLabel *labelFlux;
  vtkKWLabel *labelElPre;
  vtkKWLabel *labelArea;
  vtkKWLabel *labelPress;
  vtkKWLabel *labelRefVal;
  vtkKWLabel *labelConEr;
  vtkKWEntry *entryCeFl;
  vtkKWEntry *entryReEP;
  vtkKWEntry *entryCeEP;
  vtkKWEntry *entryReAr;
  vtkKWEntry *entryCeAr;
  vtkKWEntry *entryRePr;
  vtkKWEntry *entryCePr;
  vtkKWLabel *labelIter;
  vtkKWEntry *entryIter;
  vtkKWLabel *labelSuPar;
  vtkKWScaleWithEntry *scaleSuPar;
  
  vtkKWLabel *labelVelocityX;
  vtkKWLabel *labelVelocityY;
  vtkKWLabel *labelVelocityZ;
  vtkKWLabel *labelDisplacementX;
  vtkKWLabel *labelDisplacementY;
  vtkKWLabel *labelDisplacementZ;
  
  vtkKWEntry *entryRefVelocityX;
  vtkKWEntry *entryRefVelocityY;
  vtkKWEntry *entryRefVelocityZ;
  vtkKWEntry *entryRefDisplacementX;
  vtkKWEntry *entryRefDisplacementY;
  vtkKWEntry *entryRefDisplacementZ;
  
  vtkKWEntry *entryConvVelocityX;
  vtkKWEntry *entryConvVelocityY;
  vtkKWEntry *entryConvVelocityZ;
  vtkKWEntry *entryConvDisplacementX;
  vtkKWEntry *entryConvDisplacementY;
  vtkKWEntry *entryConvDisplacementZ;
  
  
  
  
  //************************************************************************************
  // *********** page 3 Solver Config
  
  vtkKWLabel *labelRenu;
  vtkKWEntry *entryRenu;
  vtkKWLabel *labelRLS;
  vtkKWLabel *labelRLSHelp;
  vtkKWComboBox *comboRLS;
  
  vtkKWLabel *labelTethaScheme;
  vtkKWThumbWheel *thumbTheta;
  
 	vtkKWLabel	*TetraCompressibilityLabel;     
	vtkKWEntry *TetraCompressibilityEntry;
  
  vtkKWLabel *labelAditi;
  vtkKWLabel *labelPena;
  vtkKWEntry *entryPena;
  vtkKWRadioButton *radioYes;
  vtkKWRadioButton *radioNo;

  vtkKWLabel *labelSolverType;
  vtkKWLabel *labelConvergenceError;
  vtkKWLabel *labelDispTolerance;
  vtkKWLabel *labelFParam;
	vtkKWLabel *labelIncrementalVersion;

  vtkKWLabel *labelIterationsBeforeRestart;
  vtkKWLabel *labelkrylov;

  vtkKWEntry *entryIterationsBeforeRestart;
  vtkKWEntry *entrykrylov;

	vtkKWComboBox *comboSolverType;
	vtkKWEntry *entryConvergenceError;
	vtkKWFrameWithLabel *frameIterativeSolver;
	vtkKWFrameWithLabel *framePreconditioning;
	vtkKWEntry *entryDispTolerance;
	vtkKWEntry *entryFParam;
	vtkKWRadioButton *radioICYes;
	vtkKWRadioButton *radioICNo;
	
  //************************************************************************************
  // *********** page 4 Miscelaneuous
  vtkKWLabel *labelFileOutput;
  vtkKWEntry *entryFileOutput;
  vtkKWLabel *labelScreenOutput;
  vtkKWEntry *entryScreenOutput;
  vtkKWLabel *labelLogFile;
  vtkKWEntry *entryLogFile;
  
  vtkKWLabel *labelFilesPath;
  vtkKWEntry *entryFilesPath;
  vtkKWPushButton *pushButtonBrowse;
  vtkKWLabel *labelFilesGenerationStatus;
  vtkKWTextWithScrollbars *textFilesGenerationStatus;
  
 
  //************************************************************************************



  vtkSMHMStraightModelWidgetProxy *proxy;
  
  
  vtkIntArray *IntParam;
  vtkDoubleArray *DoubleParam;
  vtkKWFrame     *WindowFrame;

  // Descrition:
	// Dialog used by the user to choose a directory where Solver input files will be generated
  vtkKWLoadSaveDialog *ChooseDirectoryDialog;
  
  // quando esta janela for iniciada, o valor dessa variável será
  // igual ao tempo de um periodo da curva do coração (se não existir curva o valor será 0 )
  double OriginalFinalTime;
  
  vtkKWPushButton *CalculateTimeStepButton;
  vtkKWLabel *labelCardiacCycleTime;
  vtkKWEntry *entryCardiacCycleTime;
  
  // variavel que define o caso em que os arquivos estao sendo configurados
  // pode ser 1D, 3D (sozinho), e acoplamento
  // pode ter os seguintes valores:
  // ********
  // 1D Model
  // 3D Model 
  // Coupled Model
  char ModelType[20]; 
  
  
  vtkKWFrameWithScrollbar *FrameScroll; 
	vtkPVWindow *PVWindow;
  
  vtkPVSource *PVSource;
  
  
  vtkKWRadioButtonSet *SelectedSolverFilesRadioButtonSet;
  
  vtkKWLabel *SelectedSolverFilesLabel;
  
  vtkKWLabel *ScaleFactor3DLabel;
	vtkKWEntry *ScaleFactor3DEntry;
  
	
	 // Description: 
	  // Opcoes do Solver Paralelo no modo monolitico
	  vtkKWFrameWithLabel *ParallelIterativeSolverFrame;
	  
	  vtkKWLabel *ParallelNumberOfProcessorsLabel;
	  vtkKWEntry *ParallelNumberOfProcessorsEntry;
	  
	  vtkKWLabel *ParallelIterationsRestartLabel;
	  vtkKWEntry *ParallelIterationsRestartEntry;
	  
	  vtkKWLabel *ParallelRelativeErrorLabel;
	  vtkKWEntry *ParallelRelativeErrorEntry;
	  
	  vtkKWLabel *ParallelAbsErrorLabel;
	  vtkKWEntry *ParallelAbsErrorEntry;
	  
	  vtkKWLabel *ParallelMaxNumberIterLabel;
	  vtkKWEntry *ParallelMaxNumberIterEntry;
	  vtkKWLabel *ParallelPrecondLabel;
	  vtkKWComboBox *ParallelPrecondCombo;
	  //--------------------------------------
	
	
  
private:  
  vtkPVHM3DSolverFilesConfigurationWidget(const vtkPVHM3DSolverFilesConfigurationWidget&); // Not implemented
  void operator=(const vtkPVHM3DSolverFilesConfigurationWidget&); // Not implemented
}; 

#endif  /*_vtkPVHM3DSolverFilesConfigurationWidget_h_*/
