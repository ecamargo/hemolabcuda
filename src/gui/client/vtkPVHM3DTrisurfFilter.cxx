/*
 * $Id: vtkPVHM3DTrisurfFilter.cxx 340 2006-05-12 13:40:37Z jan $
 */
   
#include "vtkRenderer.h" 
#include "vtkActor.h"
#include "vtkActorCollection.h" 
#include "vtkProperty.h"
#include "vtkDataSet.h"
#include "vtkCellData.h"
#include "vtkDataSetMapper.h"
#include "vtkMapper.h"
#include "vtkLookupTable.h"

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkPVWindow.h"
#include "vtkProcessModule.h"
#include "vtkPVColorMap.h"
#include "vtkPVWidgetCollection.h"
#include "vtkPVGeometryInformation.h"
#include "vtkPVRenderView.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWPushButton.h"
#include "vtkKWIcon.h"
#include "vtkKWToolbar.h"
#include "vtkKWProgressGauge.h"
#include "vtkKWComboBox.h"

#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkSMProxyManager.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMPointLabelDisplayProxy.h"
#include "vtkSM3DWidgetProxy.h"

#include "vtkPVHMBoxWidget.h"
#include "vtkPVSphereWidget.h"
#include "vtkPVHM3DTrisurfFilter.h"
#include "vtkHM3DTrisurfFilter.h"

// Buttons (/gui/client/resources)
#include "resources/TrisurfButton_Par.h"
#include "resources/TrisurfButton_AutoPar.h"
#include "resources/TrisurfButton_Smooth.h"
#include "resources/TrisurfButton_SwapDiagonals.h"
#include "resources/TrisurfButton_DivideObtuse.h"
#include "resources/TrisurfButton_InsertNodes.h"
#include "resources/TrisurfButton_Collapse.h"
#include "resources/TrisurfButton_RemoveTresTri.h"
#include "resources/TrisurfButton_MeshInformation.h"
#include "resources/TrisurfButton_close_surface.h"
#include "resources/TrisurfButton_remove_group.h"
#include "resources/TrisurfButton_test_mesh.h"
#include "resources/TrisurfButton_transformation.h"
#include "resources/TrisurfButton_remove_free_nodes.h"
#include "resources/TrisurfButton_others.h"
#include "resources/TrisurfButton_ChangeOrientation.h"
#include "resources/TrisurfButton_ViewGroup.h"
#include "resources/TrisurfButton_RemoveNeedles.h"
#include "resources/TrisurfButton_SmallTri.h"

vtkCxxRevisionMacro(vtkPVHM3DTrisurfFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHM3DTrisurfFilter);

//------------------------------------------------------------------------------
vtkPVHM3DTrisurfFilter::vtkPVHM3DTrisurfFilter()
{ 
	this->AcceptButtonPressed	= false;	
	this->InvokedFilter = 0;
	this->EnableCollapseInsertButtons = false;	
	this->ViewModifiedCells	= false;
	
	// Wireframe mode
	this->wireActor 								= vtkActor::New();
	this->renderer 									= NULL; 
	this->ViewWireFrameCheckButton 	= vtkKWCheckButton::New();
		  
  this->Frame										= vtkKWFrameWithLabel::New();
	this->ToolBarLine1 						= vtkKWToolbar::New(); 
	this->ToolBarLine2 						= vtkKWToolbar::New(); 	
	this->ToolBarLine3 						= vtkKWToolbar::New(); 
	this->ToolBarLine4 						= vtkKWToolbar::New(); 	

	// Par Button
	this->ParButton 							= vtkKWLoadSaveButton::New();
	this->ParButton->GetLoadSaveDialog()->SetFileTypes("{{Parameters Files} {*.spi}}");	

	this->AutoParButton        		= vtkKWPushButton::New();
	this->AutoParFrame						= vtkKWFrameWithLabel::New();
	this->EdgesSizeLabel					= vtkKWLabel::New();
	this->EdgesSizeEntry					= vtkKWEntry::New();		
	
	//Remove Three triangles parameters.
	this->RemoveTresTriButton 		= vtkKWPushButton::New(); //Button to RemoveTresTri method.
	
	//Close Surface Button parameters.
	this->CloseSurfaceFrame  		= vtkKWFrameWithLabel::New();	
	this->CloseSurfaceButton 		= vtkKWPushButton::New(); //Button to CloseSurface method.
	this->CloseSurfaceLabel  		= vtkKWLabel::New();
		
	//RemoveFreeNodesButton parameters.
	this->RemoveFreeNodesFrame  	= vtkKWFrameWithLabel::New();
	this->RemoveFreeNodesButton 	= vtkKWPushButton::New();
	this->RemoveFreeNodesLabel  	= vtkKWLabel::New();
	this->FreeNodesLabel        	= vtkKWLabel::New();
	
	//Transform parameters.
	this->TransformFrame  			= vtkKWFrameWithLabel::New();
	this->TransformButton 			= vtkKWPushButton::New();
	this->TranslateLabel  			= vtkKWLabel::New();
	this->ScaleLabel      			= vtkKWLabel::New();
	//	this->RotationLabel   			= vtkKWLabel::New();
	this->TXLabel         			= vtkKWLabel::New();
	this->TXEntry         			= vtkKWEntry::New();
	this->TYLabel         			= vtkKWLabel::New();
	this->TYEntry         			= vtkKWEntry::New();
	this->TZLabel         			= vtkKWLabel::New();
	this->TZEntry         			= vtkKWEntry::New();
	
	this->SXLabel         			= vtkKWLabel::New();
	this->SXEntry         			= vtkKWEntry::New();
	this->SYLabel         			= vtkKWLabel::New();
	this->SYEntry         			= vtkKWEntry::New();
	this->SZLabel         			= vtkKWLabel::New();
	this->SZEntry         			= vtkKWEntry::New();
	
	// Trisurf Smooth Parameters
	this->SmoothFrame 				= vtkKWFrameWithLabel::New();
	this->SmoothButton 				= vtkKWPushButton::New();	
	this->IterationsLabel 		= vtkKWLabel::New();
	this->IterationsEntry 		= vtkKWEntry::New();
	this->RelatationLabel			= vtkKWLabel::New();
	this->RelatationEntry 		= vtkKWEntry::New();  
	this->GroupLabel					= vtkKWLabel::New();
	this->GroupEntry 					= vtkKWEntry::New();     
	this->NoShrinkCheckButton	= vtkKWCheckButton::New();
	this->NoShrink						= 1; // Start Selected
	this->SmoothRemoveNeedlesCheckButton = vtkKWCheckButton::New(); 
	
	//Test Mesh parameters.
	this->TestMeshFrame           = vtkKWFrameWithLabel::New();
	this->TestMeshButton          = vtkKWPushButton::New();
	this->TestMeshLabel           = vtkKWLabel::New();
	
	//Mesh information parameters.
	this->MeshInformationFrame    = vtkKWFrameWithLabel::New();
	this->MeshInformationButton   = vtkKWPushButton::New();
	this->MeshInformationLabel    = vtkKWLabel::New();
	
 	// Swap Diagonals Parameters
	this->SwapDiagonalsButton	 		= vtkKWPushButton::New();
	this->SwapDiagonalsFrame			= vtkKWFrameWithLabel::New(); 	
	this->AngleLabel							= vtkKWLabel::New();
	this->AngleEntry							= vtkKWEntry::New();
	this->DiagonalsLabel					= vtkKWLabel::New();
	this->DiagonalsSwappedLabel 	= vtkKWLabel::New();

	// Divide obtuse Parameters	
	this->DivideObtuseButton	 	= vtkKWPushButton::New();
	this->DivideObtuseFrame			= vtkKWFrameWithLabel::New();	
	this->DivideAngleLabel			= vtkKWLabel::New();
	this->DivideAngleEntry			= vtkKWEntry::New();  
	this->ElementsLabel					= vtkKWLabel::New();
	this->ElementsDividedLabel	= vtkKWLabel::New();	

	// Insert Nodes Parameters	
	this->InsertNodesButton	 		= vtkKWPushButton::New();
	this->InsertNodesFrame			= vtkKWFrameWithLabel::New();  	
	this->InsertNodesLabel			= vtkKWLabel::New();
	this->InsertNodesEntry			= vtkKWEntry::New();  
	this->NodesLabel			    	= vtkKWLabel::New();
	this->NodesInsertedLabel		= vtkKWLabel::New();

	// Collapse Parameters		
	this->CollapseButton	 		= vtkKWPushButton::New(); 	
	this->CollapseFrame	 			= vtkKWFrameWithLabel::New();
	this->CollapseLabel	 			= vtkKWLabel::New();
	this->CollapseEntry				= vtkKWEntry::New(); 
	this->EdgesLabel					= vtkKWLabel::New();
	this->EdgesCollapsedLabel	= vtkKWLabel::New();
	
	// Remove Group
	this->RemoveGroupsFrame					= vtkKWFrameWithLabel::New();
	this->RemoveGroupsButton				= vtkKWPushButton::New();
	this->RemoveGroupsLabel					= vtkKWLabel::New();
	this->RemoveGroupsResponseLabel = vtkKWLabel::New();
	this->RemoveGroupsNodesLabel    = vtkKWLabel::New();
	this->RemoveGroupsEntry					= vtkKWEntry::New();

  // Change Orientation Parameters
	this->ChangeOrientationButton				= vtkKWPushButton::New();
	this->ChangeOrientationFrame				= vtkKWFrameWithLabel::New();	
	this->ChangeOrientationLabel				= vtkKWLabel::New();
	this->ChangeOrientationEntry				= vtkKWEntry::New(); 
	this->ChangeOrientationLabelResponse	= vtkKWLabel::New();

  	// Remove Needles
	this->RemoveNeedlesButton					= vtkKWPushButton::New();
	this->RemoveNeedlesFrame					= vtkKWFrameWithLabel::New();
	this->RemoveNeedlesLabel					= vtkKWLabel::New();
	this->RemoveNeedlesEntry					= vtkKWEntry::New(); 
	this->RemoveNeedlesLabelResponse	= vtkKWLabel::New();  
	this->RemoveNeedlesViewButton			= vtkKWPushButton::New();
 	this->RemoveNeedlesRemoveButton		= vtkKWPushButton::New();  

	// Remove Needles
	this->SmallTriButton				= vtkKWPushButton::New();
	this->SmallTriFrame					= vtkKWFrameWithLabel::New();
	this->SmallTriLabel					= vtkKWLabel::New();
	this->SmallTriEntry					= vtkKWEntry::New(); 

	this->SmallTriAverageLabel					= vtkKWLabel::New();
	this->SmallTriAverageLabelComputed	= vtkKWLabel::New();
	this->SmallTriPercentualLabel				= vtkKWLabel::New();      
	this->SmallTriPercentualEntry				= vtkKWEntry::New();
	this->SmallTriPercentualButton			= vtkKWPushButton::New();
  
	this->SmallTriLabelResponse	= vtkKWLabel::New();  
	this->SmallTriAverageButton = vtkKWPushButton::New();  
	this->SmallTriViewButton		= vtkKWPushButton::New();
	this->SmallTriRemoveButton	= vtkKWPushButton::New(); 
  
  	// View/Merge Group
	this->ViewGroupButton		= vtkKWPushButton::New();
	this->ViewGroupFrame		= vtkKWFrameWithLabel::New();	
 	this->ViewGroupLabel		= vtkKWLabel::New();
	this->ViewGroupEntry		= vtkKWEntry::New(); 
	this->ViewButton				= vtkKWPushButton::New();
	  
	this->MergeGroupLabel		= vtkKWLabel::New();
	this->GroupsEntry				= vtkKWEntry::New();
	this->MergeButton				= vtkKWPushButton::New();
  
	this->ViewMergeLabel		= vtkKWLabel::New();  	
	this->ViewModifiedCellsCheckButton = vtkKWCheckButton::New();	
  
  	// Icons	
	this->iconSmooth 						= vtkKWIcon::New();    
	this->iconSwap 							= vtkKWIcon::New();  	
	this->iconDivideObtuse			=	vtkKWIcon::New();  
	this->iconPar 							= vtkKWIcon::New();  		
	this->iconAutoPar						= vtkKWIcon::New();  			
	this->iconInsertNodes				= vtkKWIcon::New(); 	  	
	this->iconCollapse					= vtkKWIcon::New();	
	this->iconRemoveTresTri  		= vtkKWIcon::New();
	this->iconMeshInformation		= vtkKWIcon::New();
	this->iconCloseSurface			= vtkKWIcon::New();
	this->iconRemoveGroup				=	vtkKWIcon::New();
	this->iconTestMesh					= vtkKWIcon::New();
	this->iconTransformation		= vtkKWIcon::New();
	this->iconRemoveFreeNodes		= vtkKWIcon::New();
	this->iconChangeOrientation	= vtkKWIcon::New();
	this->iconViewGroup					= vtkKWIcon::New();	
	this->iconSmallTri					= vtkKWIcon::New();		
	this->iconRemoveNeedles			= vtkKWIcon::New();
		
	this->selectedFilterOnCallback  = 0;

	this->SelectionWidgetComboBox	= vtkKWComboBox::New();	
	
	this->SelectionWidgetButton				= vtkKWPushButton::New();
	this->SelectionWidgetLabel 				= vtkKWLabel::New();
	this->BoxWidget = NULL;	
	this->SphereWidget = NULL;  
  
	this->WireInput = NULL;   
	this->WireSelect = false;
}

//------------------------------------------------------------------------------
vtkPVHM3DTrisurfFilter::~vtkPVHM3DTrisurfFilter()
{
	// Remove ator wireframe
	if(renderer) renderer->RemoveActor(wireActor);
	this->wireActor->Delete();
	this->renderer = NULL; 
	this->ViewWireFrameCheckButton->Delete();
	
	this->ToolBarLine1->Delete();
	this->ToolBarLine2->Delete();
	this->ToolBarLine3->Delete();
	this->ToolBarLine4->Delete();
		
	//Par Button
	if(this->ParButton)
	  this->ParButton->Delete();

	this->AutoParButton->Delete();
	this->AutoParFrame->Delete();
	this->EdgesSizeLabel->Delete();
	this->EdgesSizeEntry->Delete();
	
	//Remove tres tri parameters.
	this->RemoveTresTriButton->Delete();
	
	//Close Surface parameters.
	this->CloseSurfaceFrame->Delete(); 
	this->CloseSurfaceButton->Delete();
	this->CloseSurfaceLabel->Delete();	
	
	//RemoveFreeNodes parameters.
	this->RemoveFreeNodesFrame->Delete();
	this->RemoveFreeNodesButton->Delete();
	this->RemoveFreeNodesLabel->Delete();
	this->FreeNodesLabel->Delete();
	
	//Transform parameters.
	this->TransformFrame->Delete();
	this->TransformButton->Delete();
	this->TranslateLabel->Delete();
	this->ScaleLabel->Delete();
	
	this->TXLabel->Delete();
	this->TYLabel->Delete();
	this->TZLabel->Delete();
	this->SXLabel->Delete();
	this->SYLabel->Delete();
	this->SZLabel->Delete();
	
	this->TXEntry->Delete();
	this->TYEntry->Delete();
	this->TZEntry->Delete();
	this->SXEntry->Delete();
	this->SYEntry->Delete();
	this->SZEntry->Delete();
	
	//TesMesh parameters.
	this->TestMeshFrame->Delete();
	this->TestMeshButton->Delete();
	this->TestMeshLabel->Delete();
	
	//TesMesh parameters.
	this->MeshInformationFrame->Delete();
	this->MeshInformationButton->Delete();
	this->MeshInformationLabel->Delete();
	
	//Smooth parameters.
	this->SmoothFrame->Delete();
	this->SmoothButton->Delete();
	this->IterationsLabel->Delete();
	this->IterationsEntry->Delete();
	this->RelatationLabel->Delete();
	this->RelatationEntry->Delete(); 
	this->GroupLabel->Delete();
	this->GroupEntry->Delete();   
	this->NoShrinkCheckButton->Delete();	
	this->SmoothRemoveNeedlesCheckButton->Delete();
		
	//SwapDiagonals parameters.
	this->SwapDiagonalsButton->Delete();
	this->SwapDiagonalsFrame->Delete();	
	this->AngleLabel->Delete();
	this->AngleEntry->Delete();
	this->DiagonalsLabel->Delete();
	this->DiagonalsSwappedLabel->Delete();	

	//Divide obtuse parameters
	this->DivideObtuseButton->Delete();
	this->DivideObtuseFrame->Delete();	
	this->DivideAngleLabel->Delete();	
	this->DivideAngleEntry->Delete();	
	this->ElementsLabel->Delete();	
	this->ElementsDividedLabel->Delete();	

	//Insert nodes parameters
	this->InsertNodesButton->Delete();	
	this->InsertNodesFrame->Delete();	
	this->InsertNodesLabel->Delete();	
	this->InsertNodesEntry->Delete();	
	this->NodesLabel->Delete();	
	this->NodesInsertedLabel->Delete();	

    //Collapse nodes parameters
	this->CollapseButton->Delete();
	this->CollapseFrame->Delete();
	this->CollapseLabel->Delete();
	this->CollapseEntry->Delete(); 
	this->EdgesLabel->Delete();
	this->EdgesCollapsedLabel->Delete();
	
	//Remove groups parameters
	this->RemoveGroupsFrame->Delete();
	this->RemoveGroupsButton->Delete();
	this->RemoveGroupsLabel->Delete();
	this->RemoveGroupsResponseLabel->Delete();
	this->RemoveGroupsNodesLabel->Delete();
	this->RemoveGroupsEntry->Delete();

	//Change Orientation parameters
	this->ChangeOrientationFrame->Delete();
	this->ChangeOrientationButton->Delete();
	this->ChangeOrientationLabel->Delete();
	this->ChangeOrientationEntry->Delete();
	this->ChangeOrientationLabelResponse->Delete();

	// Remove Needles
	this->RemoveNeedlesFrame->Delete();
	this->RemoveNeedlesButton->Delete();
	this->RemoveNeedlesLabel->Delete();
	this->RemoveNeedlesEntry->Delete();
	this->RemoveNeedlesLabelResponse->Delete();
	this->RemoveNeedlesViewButton->Delete();
	this->RemoveNeedlesRemoveButton->Delete();

	// Small Tri
  this->SmallTriFrame->Delete();
  this->SmallTriButton->Delete();
  this->SmallTriLabel->Delete();
  this->SmallTriEntry->Delete();
  this->SmallTriAverageLabel->Delete();
  this->SmallTriAverageLabelComputed->Delete();
  this->SmallTriPercentualLabel->Delete();      
  this->SmallTriPercentualEntry->Delete();
  this->SmallTriPercentualButton->Delete(); 
  this->SmallTriLabelResponse->Delete();
  this->SmallTriAverageButton->Delete();  
  this->SmallTriViewButton->Delete();
  this->SmallTriRemoveButton->Delete();

	// View Group Parameters
	this->ViewGroupFrame->Delete();
	this->ViewGroupButton->Delete();
	this->ViewGroupLabel->Delete();
	this->ViewGroupEntry->Delete();
	this->ViewButton->Delete();
  
	this->MergeGroupLabel->Delete();
	this->GroupsEntry->Delete();
	this->MergeButton->Delete();
  
	this->ViewMergeLabel->Delete();
	this->ViewModifiedCellsCheckButton->Delete();
      
	//Icons
	this->iconSmooth->Delete();
	this->iconSwap->Delete();
	this->iconDivideObtuse->Delete();
	this->iconPar->Delete();
	this->iconAutoPar->Delete();	
	this->iconInsertNodes->Delete();
	this->iconCollapse->Delete();
	this->iconRemoveTresTri->Delete();
	this->iconMeshInformation->Delete();
	this->iconCloseSurface->Delete();
	this->iconRemoveGroup->Delete();
	this->iconTestMesh->Delete();
	this->iconTransformation->Delete();
	this->iconRemoveFreeNodes->Delete();
	this->iconChangeOrientation->Delete();
	this->iconViewGroup->Delete();
	this->iconSmallTri->Delete();	
	this->iconRemoveNeedles->Delete();	
	
	this->Frame->Delete();

	this->SelectionWidgetLabel->Delete();
	//this->BoxWidgetCheckButton->Delete();
	//this->SphereWidget->Delete();  
	this->SelectionWidgetComboBox->Delete();
	this->SelectionWidgetButton->Delete();
	this->BoxWidget = NULL;	
	this->SphereWidget = NULL;
} 

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::Create(vtkKWApplication* app)
{
  vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << "already created");
    return;
    }
  
  this->SetBackgroundColor(0, 1, 0);

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
    
  this->SetIcons();
  
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  this->Frame->SetLabelText("Trisurf Filter");
  
  this->ToolBarLine1->SetParent(this->Frame->GetFrame());
  this->ToolBarLine1->Create(pvApp);

  this->ToolBarLine2->SetParent(this->Frame->GetFrame());
  this->ToolBarLine2->Create(pvApp);

  this->ToolBarLine3->SetParent(this->Frame->GetFrame());
  this->ToolBarLine3->Create(pvApp);

  this->ToolBarLine4->SetParent(this->Frame->GetFrame());
  this->ToolBarLine4->Create(pvApp);
  
  // Load Parameteres	
  this->ParButton->SetParent(this->ToolBarLine1);
  this->ParButton->Create(pvApp);
  this->ParButton->SetImageToIcon(iconPar);  
  this->ParButton->SetCommand(this, "ParButtonCallback"); 
  this->ParButton->SetBalloonHelpString("Open Parameter file.");
  this->ParButton->SetText("");

  // AutoPar Parameteres////////////////////////////////////////////////////////
  this->AutoParButton->SetParent(this->ToolBarLine1);
  this->AutoParButton->Create(pvApp);
  this->AutoParButton->SetImageToIcon(iconAutoPar);  
  this->AutoParButton->SetCommand(this, "AutoParButtonCallback");  
  this->AutoParButton->SetBalloonHelpString("Generate Parameters Automatically.");
  this->AutoParButton->SetText("");

  this->AutoParFrame->SetLabelText("Automatic Parameters"); 
  this->AutoParFrame->SetParent(this->Frame->GetFrame());
  this->AutoParFrame->Create(pvApp);

  this->EdgesSizeLabel->SetParent(this->AutoParFrame->GetFrame());
  this->EdgesSizeLabel->Create(pvApp);
  this->EdgesSizeLabel->SetText("Edges Size");
  this->EdgesSizeEntry->SetParent(this->AutoParFrame->GetFrame());
  this->EdgesSizeEntry->Create(pvApp);
  this->EdgesSizeEntry->SetBalloonHelpString("Set Edges Size"); 
  this->EdgesSizeEntry->SetValueAsDouble(1.0);
  ////////////////////////////////////////////////////////////////////////////////

	
  // Smooth Filter  
  this->SmoothButton->SetParent(this->ToolBarLine3);
  this->SmoothButton->Create(pvApp);
  this->SmoothButton->SetImageToIcon(iconSmooth);  
  this->SmoothButton->SetCommand(this, "SmoothButtonCallback"); 
  this->SmoothButton->SetBalloonHelpString("Apply smooth filter on surface");
    
  this->SmoothFrame->SetLabelText("Smooth Parameters"); 
  this->SmoothFrame->SetParent(this->Frame->GetFrame());
  this->SmoothFrame->Create(pvApp);

  this->IterationsLabel->SetParent(this->SmoothFrame->GetFrame());
  this->IterationsLabel->Create(pvApp);
  this->IterationsLabel->SetText("Number of Iterations");
  this->IterationsEntry->SetParent(this->SmoothFrame->GetFrame());
  this->IterationsEntry->Create(pvApp);
  this->IterationsEntry->SetBalloonHelpString("Set number of iterations"); 
  this->IterationsEntry->SetValueAsDouble(1);

  this->RelatationLabel->SetParent(this->SmoothFrame->GetFrame());
  this->RelatationLabel->Create(pvApp);
  this->RelatationLabel->SetText("Relaxation factor");
  this->RelatationEntry->SetParent(this->SmoothFrame->GetFrame());
  this->RelatationEntry->Create(pvApp);
  this->RelatationEntry->SetBalloonHelpString("Set Relatation"); 
  this->RelatationEntry->SetValueAsDouble(0.63000);
  
  this->GroupLabel->SetParent(this->SmoothFrame->GetFrame());
  this->GroupLabel->Create(pvApp);
  this->GroupLabel->SetText("Group (-1 for all)");
  this->GroupEntry->SetParent(this->SmoothFrame->GetFrame());
  this->GroupEntry->Create(pvApp);
  this->GroupEntry->SetBalloonHelpString("Set Group for all (0)"); 
  this->GroupEntry->SetValueAsDouble(-1.0);

  this->NoShrinkCheckButton->SetParent(this->SmoothFrame->GetFrame());
  this->NoShrinkCheckButton->Create(pvApp);
  this->NoShrinkCheckButton->SetText("No Shrink");  
  this->NoShrinkCheckButton->SetCommand(this, "NoShrinkCheckButtonCallback");  	
  this->NoShrinkCheckButton->SelectedStateOn(); 	
  
  this->SmoothRemoveNeedlesCheckButton->SetParent(this->SmoothFrame->GetFrame());
  this->SmoothRemoveNeedlesCheckButton->Create(pvApp);
  this->SmoothRemoveNeedlesCheckButton->SetText("Remove Needles");
  this->SmoothRemoveNeedlesCheckButton->SetCommand(this, "SmoothRemoveNeedleCheckButtonCallback");
  this->SmoothRemoveNeedlesCheckButton->SelectedStateOn();
  this->SmoothRemoveNeedlesCheckButton->SetBalloonHelpString("Enable the \"Remove Needles\" filter automatically (5 degrees)");
  
  //RemoveTresTri parameters.
  this->RemoveTresTriButton->SetParent(this->ToolBarLine2);
  this->RemoveTresTriButton->Create(pvApp); 
  this->RemoveTresTriButton->SetImageToIcon(this->iconRemoveTresTri);
  this->RemoveTresTriButton->SetCommand(this, "RemoveTresTriButtonCallback"); 
  this->RemoveTresTriButton->SetBalloonHelpString("Remove three triangles");
  
  //Close Surface parameters.
  this->CloseSurfaceButton->SetParent(this->ToolBarLine4);
  this->CloseSurfaceButton->Create(pvApp); 
  this->CloseSurfaceButton->SetImageToIcon(this->iconCloseSurface);
  this->CloseSurfaceButton->SetCommand(this, "CloseSurfaceButtonCallback"); 
  this->CloseSurfaceButton->SetBalloonHelpString("Close surface.");

  this->CloseSurfaceFrame->SetLabelText("Close Surface"); 
  this->CloseSurfaceFrame->SetParent(this->Frame->GetFrame());
  this->CloseSurfaceFrame->Create(pvApp);

  this->CloseSurfaceLabel->SetParent(this->CloseSurfaceFrame->GetFrame());
  this->CloseSurfaceLabel->Create(pvApp);
  this->CloseSurfaceLabel->SetText("");

  //Remove free nodes parameter.
  this->RemoveFreeNodesFrame->SetLabelText("Remove Free Nodes"); 
  this->RemoveFreeNodesFrame->SetParent(this->Frame->GetFrame());
  this->RemoveFreeNodesFrame->Create(pvApp);
	
  this->RemoveFreeNodesButton->SetParent(this->ToolBarLine3);
  this->RemoveFreeNodesButton->Create(pvApp); 
  this->RemoveFreeNodesButton->SetImageToIcon(this->iconRemoveFreeNodes);
  this->RemoveFreeNodesButton->SetCommand(this, "RemoveFreeNodesButtonCallback"); 
  this->RemoveFreeNodesButton->SetBalloonHelpString("Remove free nodes");
  this->RemoveFreeNodesButton->EnabledOff();
  
  this->FreeNodesLabel->SetParent(this->RemoveFreeNodesFrame->GetFrame());
  this->FreeNodesLabel->Create(pvApp);
  this->FreeNodesLabel->SetText("Free nodes removed: ");
  
  this->RemoveFreeNodesLabel->SetParent(this->RemoveFreeNodesFrame->GetFrame());
  this->RemoveFreeNodesLabel->Create(pvApp);
  this->RemoveFreeNodesLabel->SetText("0");
  
  //Transformation parameter.
  this->TransformFrame->SetLabelText("Transformation"); 
  this->TransformFrame->SetParent(this->Frame->GetFrame());
  this->TransformFrame->Create(pvApp);
	
  this->TransformButton->SetParent(this->ToolBarLine4);
  this->TransformButton->Create(pvApp); 
  this->TransformButton->SetImageToIcon(this->iconTransformation);
  this->TransformButton->SetCommand(this, "TransformButtonCallback"); 
  this->TransformButton->SetBalloonHelpString("Transformation: Scale, translate the surface.");
  
  this->TranslateLabel->SetParent(this->TransformFrame->GetFrame());
  this->TranslateLabel->Create(pvApp);
  this->TranslateLabel->SetText("Translate");
  
  this->ScaleLabel->SetParent(this->TransformFrame->GetFrame());
  this->ScaleLabel->Create(pvApp);
  this->ScaleLabel->SetText("Scale");
  
  this->TXLabel->SetParent(this->TransformFrame->GetFrame());
  this->TXLabel->Create(pvApp);
  this->TXLabel->SetText("tx");
  this->TXEntry->SetParent(this->TransformFrame->GetFrame());
  this->TXEntry->Create(pvApp);
  this->TXEntry->SetValueAsDouble(0.00000);
  this->TXEntry->SetBalloonHelpString("Set tx value");
  
  this->TYLabel->SetParent(this->TransformFrame->GetFrame());
  this->TYLabel->Create(pvApp);
  this->TYLabel->SetText("ty");
  this->TYEntry->SetParent(this->TransformFrame->GetFrame());
  this->TYEntry->Create(pvApp);
  this->TYEntry->SetValueAsDouble(0.00000);
  this->TYEntry->SetBalloonHelpString("Set ty value");
  
  this->TZLabel->SetParent(this->TransformFrame->GetFrame());
  this->TZLabel->Create(pvApp);
  this->TZLabel->SetText("tz");
  this->TZEntry->SetParent(this->TransformFrame->GetFrame());
  this->TZEntry->Create(pvApp);
  this->TZEntry->SetValueAsDouble(0.00000);
  this->TZEntry->SetBalloonHelpString("Set tz value");
  
  this->SXLabel->SetParent(this->TransformFrame->GetFrame());
  this->SXLabel->Create(pvApp);
  this->SXLabel->SetText("sx");
  this->SXEntry->SetParent(this->TransformFrame->GetFrame());
  this->SXEntry->Create(pvApp);
  this->SXEntry->SetValueAsDouble(1.00000);
  this->SXEntry->SetBalloonHelpString("Set sx value");
  
  this->SYLabel->SetParent(this->TransformFrame->GetFrame());
  this->SYLabel->Create(pvApp);
  this->SYLabel->SetText("sy");
  this->SYEntry->SetParent(this->TransformFrame->GetFrame());
  this->SYEntry->Create(pvApp);
  this->SYEntry->SetValueAsDouble(1.00000);
  this->SYEntry->SetBalloonHelpString("Set sy value");
  
  this->SZLabel->SetParent(this->TransformFrame->GetFrame());
  this->SZLabel->Create(pvApp);
  this->SZLabel->SetText("sz");
  this->SZEntry->SetParent(this->TransformFrame->GetFrame());
  this->SZEntry->Create(pvApp);
  this->SZEntry->SetValueAsDouble(1.00000);
  this->SZEntry->SetBalloonHelpString("Set sz value");
  
  //Test mesh parameter.
  this->TestMeshFrame->SetLabelText("Test Mesh"); 
  this->TestMeshFrame->SetParent(this->Frame->GetFrame());
  this->TestMeshFrame->Create(pvApp);
	
  this->TestMeshButton->SetParent(this->ToolBarLine3);
  this->TestMeshButton->Create(pvApp); 
  this->TestMeshButton->SetImageToIcon(this->iconTestMesh);
  this->TestMeshButton->SetCommand(this, "TestMeshButtonCallback"); 
  this->TestMeshButton->SetBalloonHelpString("Test Mesh");
  
  this->TestMeshLabel->SetParent(this->TestMeshFrame->GetFrame());
  this->TestMeshLabel->Create(pvApp);
  this->TestMeshLabel->SetText("");
  
  //Mesh Test parameter.
  this->MeshInformationFrame->SetLabelText("Information about mesh.");
  this->MeshInformationFrame->SetParent(this->Frame->GetFrame());
  this->MeshInformationFrame->Create(pvApp);
	
  this->MeshInformationButton->SetParent(this->ToolBarLine1);
  this->MeshInformationButton->Create(pvApp);
  this->MeshInformationButton->SetImageToIcon(this->iconMeshInformation);
  this->MeshInformationButton->SetCommand(this, "MeshInformationButtonCallback"); 
  this->MeshInformationButton->SetBalloonHelpString("Information about surface. Number of nodes, number of elements, number of groups.");
  
  this->MeshInformationLabel->SetParent(this->MeshInformationFrame->GetFrame());
  this->MeshInformationLabel->Create(pvApp);
  this->MeshInformationLabel->SetText("");
	
  // Swap Diagonals  
  this->SwapDiagonalsButton->SetParent(this->ToolBarLine2);
  this->SwapDiagonalsButton->Create(pvApp);
  this->SwapDiagonalsButton->SetImageToIcon(iconSwap);  
  this->SwapDiagonalsButton->SetCommand(this, "SwapDiagonalsCallback");   
  this->SwapDiagonalsButton->SetBalloonHelpString("Swap Diagonals");

  this->SwapDiagonalsFrame->SetLabelText("Swap Diagonals Parameters"); 
  this->SwapDiagonalsFrame->SetParent(this->Frame->GetFrame());
  this->SwapDiagonalsFrame->Create(pvApp); 
	
  this->AngleLabel->SetParent(this->SwapDiagonalsFrame->GetFrame());
  this->AngleLabel->Create(pvApp);
  this->AngleLabel->SetText("Angle");
  this->AngleEntry->SetParent(this->SwapDiagonalsFrame->GetFrame());
  this->AngleEntry->Create(pvApp);
  this->AngleEntry->SetBalloonHelpString("Set Angle"); 
  this->AngleEntry->SetValueAsDouble(1.0);
  this->AngleEntry->SetBalloonHelpString("If the angle between two elements normals is lower than the given value, the diagonal could be swapped.");	

  this->DiagonalsLabel->SetParent(this->SwapDiagonalsFrame->GetFrame());
  this->DiagonalsLabel->Create(pvApp);
  this->DiagonalsLabel->SetText("Diagonals Swapped: ");
  this->DiagonalsSwappedLabel->SetParent(this->SwapDiagonalsFrame->GetFrame());
  this->DiagonalsSwappedLabel->Create(pvApp);
  this->DiagonalsSwappedLabel->SetText("0");

	// Divide Obtuse  
  this->DivideObtuseButton->SetParent(this->ToolBarLine2);
  this->DivideObtuseButton->Create(pvApp);
  this->DivideObtuseButton->SetImageToIcon(iconDivideObtuse);  
  this->DivideObtuseButton->SetCommand(this, "DivideObtuseCallback");   
  this->DivideObtuseButton->SetBalloonHelpString("Divide Obtuse");
  
  this->DivideObtuseFrame->SetLabelText("Divide Obtuse Parameters"); 
  this->DivideObtuseFrame->SetParent(this->Frame->GetFrame());
  this->DivideObtuseFrame->Create(pvApp); 
	
  this->DivideAngleLabel->SetParent(this->DivideObtuseFrame->GetFrame());
  this->DivideAngleLabel->Create(pvApp);
  this->DivideAngleLabel->SetText("Angle");
  this->DivideAngleEntry->SetParent(this->DivideObtuseFrame->GetFrame());
  this->DivideAngleEntry->Create(pvApp);
  this->DivideAngleEntry->SetValueAsDouble(160.0);	
  this->DivideAngleEntry->SetBalloonHelpString("Elements with an angle greater than the given value will be divided.");	
  
  this->ElementsLabel->SetParent(this->DivideObtuseFrame->GetFrame());
  this->ElementsLabel->Create(pvApp);
  this->ElementsLabel->SetText("Elements Divided: ");
  this->ElementsDividedLabel->SetParent(this->DivideObtuseFrame->GetFrame());
  this->ElementsDividedLabel->Create(pvApp);
  this->ElementsDividedLabel->SetText("0");

	// Insert Nodes
  this->InsertNodesButton->SetParent(this->ToolBarLine1);
  this->InsertNodesButton->Create(pvApp);
  this->InsertNodesButton->SetImageToIcon(iconInsertNodes);
  this->InsertNodesButton->SetCommand(this, "InsertNodesCallback");   
  this->InsertNodesButton->SetBalloonHelpString("Insert Nodes");
  
  this->InsertNodesFrame->SetLabelText("Insert Nodes Parameters"); 
  this->InsertNodesFrame->SetParent(this->Frame->GetFrame());
  this->InsertNodesFrame->Create(pvApp); 
	
  this->InsertNodesLabel->SetParent(this->InsertNodesFrame->GetFrame());
  this->InsertNodesLabel->Create(pvApp);
  this->InsertNodesLabel->SetText("Value:");
  this->InsertNodesEntry->SetParent(this->InsertNodesFrame->GetFrame());
  this->InsertNodesEntry->Create(pvApp);
  this->InsertNodesEntry->SetValueAsDouble(0.31);	
  this->InsertNodesEntry->SetBalloonHelpString("Size Multiplier.");	
  
  this->NodesLabel->SetParent(this->InsertNodesFrame->GetFrame());
  this->NodesLabel->Create(pvApp);
  this->NodesLabel->SetText("Nodes Inserted: ");
  this->NodesInsertedLabel->SetParent(this->InsertNodesFrame->GetFrame());
  this->NodesInsertedLabel->Create(pvApp);
  this->NodesInsertedLabel->SetText("0");   
  
  // Collapse Nodes
  this->CollapseButton->SetParent(this->ToolBarLine1);
  this->CollapseButton->Create(pvApp);
  this->CollapseButton->SetImageToIcon(iconCollapse);  
  this->CollapseButton->SetCommand(this, "CollapseCallback");  
  this->CollapseButton->SetBalloonHelpString("Collapse");

  this->CollapseFrame->SetLabelText("Collapse Parameters"); 
  this->CollapseFrame->SetParent(this->Frame->GetFrame());
  this->CollapseFrame->Create(pvApp); 
	
  this->CollapseLabel->SetParent(this->CollapseFrame->GetFrame());
  this->CollapseLabel->Create(pvApp);
  this->CollapseLabel->SetText("Value");
  this->CollapseEntry->SetParent(this->CollapseFrame->GetFrame());
  this->CollapseEntry->Create(pvApp);
  this->CollapseEntry->SetValueAsDouble(0.316228);	
  this->CollapseEntry->SetBalloonHelpString("Elements with size lower than the given value are collapsed.");	
  
  this->EdgesLabel->SetParent(this->CollapseFrame->GetFrame());
  this->EdgesLabel->Create(pvApp);
  this->EdgesLabel->SetText("Elements collapsed: ");
  this->EdgesCollapsedLabel->SetParent(this->CollapseFrame->GetFrame());
  this->EdgesCollapsedLabel->Create(pvApp);
  this->EdgesCollapsedLabel->SetText("0");   

	// Change Orienation
	this->ChangeOrientationFrame->SetLabelText("Change Orientation");
  this->ChangeOrientationFrame->SetParent(this->Frame->GetFrame());
  this->ChangeOrientationFrame->Create(pvApp);
  
  this->ChangeOrientationButton->SetParent(this->ToolBarLine3);
  this->ChangeOrientationButton->Create(pvApp);
  this->ChangeOrientationButton->SetImageToIcon(this->iconChangeOrientation);
  this->ChangeOrientationButton->SetCommand(this, "ChangeOrientationCallback");
  this->ChangeOrientationButton->SetBalloonHelpString("Change Orientation of selected Groups, separeted with white Spaces. Set -1 for all groups.");
  
  this->ChangeOrientationLabel->SetParent(this->ChangeOrientationFrame->GetFrame());
  this->ChangeOrientationLabel->Create(pvApp);
  this->ChangeOrientationLabel->SetText("Groups (-1 for all):");
  
  this->ChangeOrientationEntry->SetParent(this->ChangeOrientationFrame->GetFrame());
  this->ChangeOrientationEntry->Create(pvApp);	
  this->ChangeOrientationEntry->SetBalloonHelpString("Type groups with white spaces. \nSet -1  for all groups. \nThe view using Gryphs will show the normal vectors pointing to the same side, even if just one group was swapped!");
  this->ChangeOrientationEntry->SetValue("-1");	  

  this->ChangeOrientationLabelResponse->SetParent(this->ChangeOrientationFrame->GetFrame());
  this->ChangeOrientationLabelResponse->Create(pvApp);
  this->ChangeOrientationLabelResponse->SetText("");   

	///////////////////////////////////////////////////////////////////////
	// Remove Needles
  this->RemoveNeedlesFrame->SetLabelText("View/Remove Needles");
  this->RemoveNeedlesFrame->SetParent(this->Frame->GetFrame());
  this->RemoveNeedlesFrame->Create(pvApp);
	
  this->RemoveNeedlesButton->SetParent(this->ToolBarLine2);
  this->RemoveNeedlesButton->Create(pvApp);
  this->RemoveNeedlesButton->SetImageToIcon(this->iconRemoveNeedles);
  this->RemoveNeedlesButton->SetCommand(this, "RemoveNeedlesCallback"); 
  this->RemoveNeedlesButton->SetBalloonHelpString("Remove triangles like needles.");

  this->RemoveNeedlesLabel->SetParent(this->RemoveNeedlesFrame->GetFrame());
  this->RemoveNeedlesLabel->Create(pvApp);
  this->RemoveNeedlesLabel->SetText("Angle: ");
  this->RemoveNeedlesLabel->SetBalloonHelpString("Set an angle between 0 and 20 degrees.");
    
  this->RemoveNeedlesEntry->SetParent(this->RemoveNeedlesFrame->GetFrame());
  this->RemoveNeedlesEntry->Create(pvApp);	
  this->RemoveNeedlesEntry->SetBalloonHelpString("Set an angle between 0 and 20 degrees.");
  this->RemoveNeedlesEntry->SetValueAsDouble(10.0);	  
  
  this->RemoveNeedlesLabelResponse->SetParent(this->RemoveNeedlesFrame->GetFrame());
  this->RemoveNeedlesLabelResponse->Create(pvApp);
  this->RemoveNeedlesLabelResponse->SetText(""); 

  this->RemoveNeedlesViewButton->SetParent(this->RemoveNeedlesFrame->GetFrame());
  this->RemoveNeedlesViewButton->Create(pvApp);
  this->RemoveNeedlesViewButton->SetText("     View     ");
  this->RemoveNeedlesViewButton->SetCommand(this, "ViewNeedlesButtonCallback"); 
  this->RemoveNeedlesViewButton->SetBalloonHelpString("View Needles");

  this->RemoveNeedlesRemoveButton->SetParent(this->RemoveNeedlesFrame->GetFrame());
  this->RemoveNeedlesRemoveButton->Create(pvApp);
  this->RemoveNeedlesRemoveButton->SetText("Remove");
  this->RemoveNeedlesRemoveButton->SetCommand(this, "RemoveNeedlesButtonCallback"); 
  this->RemoveNeedlesRemoveButton->SetBalloonHelpString("Remove Needles");

	///////////////////////////////////////////////////////////////////////
	// Small Tri
	this->SmallTriFrame->SetLabelText("View/Remove Small Elements");
  this->SmallTriFrame->SetParent(this->Frame->GetFrame());
  this->SmallTriFrame->Create(pvApp);
	
  this->SmallTriButton->SetParent(this->ToolBarLine2);
  this->SmallTriButton->Create(pvApp);
  this->SmallTriButton->SetImageToIcon(this->iconSmallTri);
  this->SmallTriButton->SetCommand(this, "SmallTriCallback"); 
  this->SmallTriButton->SetBalloonHelpString("Remove small Elements.");

  this->SmallTriLabel->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriLabel->Create(pvApp);
  this->SmallTriLabel->SetText("Area: ");
  this->SmallTriLabel->SetBalloonHelpString("Set an area.");
    
  this->SmallTriEntry->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriEntry->Create(pvApp);	
  this->SmallTriEntry->SetBalloonHelpString("Set an area.");
  this->SmallTriEntry->SetValueAsDouble(0.0);

  this->SmallTriAverageLabel->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriAverageLabel->Create(pvApp);
  this->SmallTriAverageLabel->SetText("Average Area: ");  
  this->SmallTriAverageLabel->EnabledOff();  
  this->SmallTriAverageLabelComputed->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriAverageLabelComputed->Create(pvApp);
  this->SmallTriAverageLabelComputed->SetText("");
  this->SmallTriAverageLabelComputed->EnabledOff();
  
  this->SmallTriPercentualLabel->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriPercentualLabel->Create(pvApp);
  this->SmallTriPercentualLabel->SetText("Percentual(%): ");  
  this->SmallTriPercentualLabel->EnabledOff();

  this->SmallTriPercentualEntry->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriPercentualEntry->Create(pvApp);	
  this->SmallTriPercentualEntry->SetBalloonHelpString("Set Average Area Percentual");
  this->SmallTriPercentualEntry->SetValueAsDouble(5.0);	  
  this->SmallTriPercentualEntry->EnabledOff();
  
  this->SmallTriPercentualButton->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriPercentualButton->Create(pvApp);
  this->SmallTriPercentualButton->SetText("Update");
  this->SmallTriPercentualButton->SetCommand(this, "SmallTriUpdateButtonCallback"); 
  this->SmallTriPercentualButton->SetBalloonHelpString("Get Average Area");
  this->SmallTriPercentualButton->EnabledOff();
    
  this->SmallTriLabelResponse->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriLabelResponse->Create(pvApp);
  this->SmallTriLabelResponse->SetText(""); 

  this->SmallTriAverageButton->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriAverageButton->Create(pvApp);
  this->SmallTriAverageButton->SetText("Average Area");
  this->SmallTriAverageButton->SetCommand(this, "SmallTriAverageButtonCallback"); 
  this->SmallTriAverageButton->SetBalloonHelpString("Get Average Area");

  this->SmallTriViewButton->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriViewButton->Create(pvApp);
  this->SmallTriViewButton->SetText("     View     ");
  this->SmallTriViewButton->SetCommand(this, "ViewSmallTriCallback"); 
  this->SmallTriViewButton->SetBalloonHelpString("View Small Elements");

  this->SmallTriRemoveButton->SetParent(this->SmallTriFrame->GetFrame());
  this->SmallTriRemoveButton->Create(pvApp);
  this->SmallTriRemoveButton->SetText("Remove");
  this->SmallTriRemoveButton->SetCommand(this, "RemoveSmallTriCallback"); 
  this->SmallTriRemoveButton->SetBalloonHelpString("Remove Small Elements");

	///////////////////////////////////////////////////////////////////////
	// View Group
	this->ViewGroupFrame->SetLabelText("View/Merge Groups");
  this->ViewGroupFrame->SetParent(this->Frame->GetFrame());
  this->ViewGroupFrame->Create(pvApp);
	
  this->ViewGroupButton->SetParent(this->ToolBarLine4);
  this->ViewGroupButton->Create(pvApp);
  this->ViewGroupButton->SetImageToIcon(this->iconViewGroup);
  this->ViewGroupButton->SetCommand(this, "ViewGroupCallback"); 
  this->ViewGroupButton->SetBalloonHelpString("View and Merge groups");

  this->ViewGroupLabel->SetParent(this->ViewGroupFrame->GetFrame());
  this->ViewGroupLabel->Create(pvApp);
  this->ViewGroupLabel->SetText("View Group:");
  this->ViewGroupEntry->SetParent(this->ViewGroupFrame->GetFrame());
  this->ViewGroupEntry->Create(pvApp);
  this->ViewGroupEntry->SetValue("None");	
  this->ViewGroupEntry->SetBalloonHelpString("Choose a group number to be highlighted.");	

  this->ViewButton->SetParent(this->ViewGroupFrame->GetFrame());
  this->ViewButton->Create(pvApp);
  this->ViewButton->SetText("View");
  this->ViewButton->SetCommand(this, "ViewCallback"); 
  this->ViewButton->SetBalloonHelpString("View Selected Group");

  this->ViewMergeLabel->SetParent(this->ViewGroupFrame->GetFrame());
  this->ViewMergeLabel->Create(pvApp);
  this->ViewMergeLabel->SetText("");

  this->MergeGroupLabel->SetParent(this->ViewGroupFrame->GetFrame());
  this->MergeGroupLabel->Create(pvApp);
  this->MergeGroupLabel->SetText("Merge Group:");
  this->GroupsEntry->SetParent(this->ViewGroupFrame->GetFrame());
  this->GroupsEntry->Create(pvApp);
  this->GroupsEntry->SetValue("None");	
  this->GroupsEntry->SetBalloonHelpString("Set Groups Numbers separated by spaces.");

  this->MergeButton->SetParent(this->ViewGroupFrame->GetFrame());
  this->MergeButton->Create(pvApp);
  this->MergeButton->SetText("Merge");
  this->MergeButton->SetCommand(this, "MergeGroupCallback"); 
  this->MergeButton->SetBalloonHelpString("Merge Groups");

  this->SelectionWidgetLabel->SetParent(this->ViewGroupFrame->GetFrame());
  this->SelectionWidgetLabel->Create(pvApp);
  this->SelectionWidgetLabel->SetText("Create Group:");

  this->SelectionWidgetComboBox->SetParent(this->ViewGroupFrame->GetFrame());
  this->SelectionWidgetComboBox->Create(pvApp);
  this->SelectionWidgetComboBox->AddValue("None");
  this->SelectionWidgetComboBox->AddValue("Box");
  this->SelectionWidgetComboBox->AddValue("Sphere");
  this->SelectionWidgetComboBox->SetValue("None");  
  this->SelectionWidgetComboBox->SetCommand(this, "SelectionWidgetCallback");  	
  this->SelectionWidgetComboBox->SetBalloonHelpString(																	
   "Turn Box Widget On/Off. Shortcuts: Shift key + Mouse Left Button = Translation;  "
   "Shift key + Mouse Right Button = Scale.");

  this->SelectionWidgetButton->SetParent(this->ViewGroupFrame->GetFrame());
  this->SelectionWidgetButton->Create(pvApp);
  this->SelectionWidgetButton->SetText("Create");  
  this->SelectionWidgetButton->SetCommand(this, "SelectionWidgetCreateGroupCallback");
  this->SelectionWidgetButton->SetBalloonHelpString("Create a group based on the cells inside the selection box.");
	this->SelectionWidgetButton->SetWidth(8);
	this->SelectionWidgetButton->SetHeight(1);	
	this->SelectionWidgetButton->SetEnabled(0);
	
	///////////////////////////////////////////////////////////////////////
  //Remove groups
  this->RemoveGroupsFrame->SetLabelText("Remove groups");
  this->RemoveGroupsFrame->SetParent( this->Frame->GetFrame() );
  this->RemoveGroupsFrame->Create(pvApp);
  
  this->RemoveGroupsButton->SetParent(this->ToolBarLine4);
  this->RemoveGroupsButton->Create(pvApp);
  this->RemoveGroupsButton->SetImageToIcon(this->iconRemoveGroup);
  this->RemoveGroupsButton->SetCommand(this, "RemoveGroupsCallback");
  this->RemoveGroupsButton->SetBalloonHelpString("Remove groups separating with white spaces!\nThe First group is 0.");

  this->RemoveGroupsLabel->SetParent(this->RemoveGroupsFrame->GetFrame());
  this->RemoveGroupsLabel->Create(pvApp);
  this->RemoveGroupsLabel->SetText("Group(s):");
  
  this->RemoveGroupsEntry->SetParent(this->RemoveGroupsFrame->GetFrame());
  this->RemoveGroupsEntry->Create(pvApp);	
  this->RemoveGroupsEntry->SetBalloonHelpString("Remove groups separating with white spaces!\nThe First group is 0.");
  
  this->RemoveGroupsResponseLabel->SetParent(this->RemoveGroupsFrame->GetFrame());
  this->RemoveGroupsResponseLabel->Create(pvApp);
  this->RemoveGroupsResponseLabel->SetText("Nodes removed: ");
  
  this->RemoveGroupsNodesLabel->SetParent(this->RemoveGroupsFrame->GetFrame());
  this->RemoveGroupsNodesLabel->Create(pvApp);
  this->RemoveGroupsNodesLabel->SetText("0");  

  this->ViewModifiedCellsCheckButton->SetParent(this->Frame->GetFrame());
  this->ViewModifiedCellsCheckButton->Create(pvApp);
  this->ViewModifiedCellsCheckButton->SetText("Highlight Modified Cells");  
  this->ViewModifiedCellsCheckButton->SetCommand(this, "ViewModifiedCellsCallback");  	
  this->ViewModifiedCellsCheckButton->SelectedStateOff(); 
  this->ViewModifiedCellsCheckButton->EnabledOff();  	

	// Wire Frame Checkbutton
  this->ViewWireFrameCheckButton->SetParent(this->Frame->GetFrame());
  this->ViewWireFrameCheckButton->Create(pvApp);
  this->ViewWireFrameCheckButton->SetText("View Wireframe Widget");  
  this->ViewWireFrameCheckButton->SetCommand(this, "ViewWireFrameCallback");  	
  this->ViewWireFrameCheckButton->SelectedStateOn(); 

  // Update Toolbar - Line 1
  this->ToolBarLine1->AddWidget(this->ParButton);
  this->ToolBarLine1->AddWidget(this->AutoParButton);  
  this->ToolBarLine1->AddWidget(this->InsertNodesButton);
  this->ToolBarLine1->AddWidget(this->CollapseButton);
  this->ToolBarLine1->AddWidget(this->MeshInformationButton);    

  // Update Toolbar - Line 2
  this->ToolBarLine2->AddWidget(this->SwapDiagonalsButton);
  this->ToolBarLine2->AddWidget(this->RemoveTresTriButton);  
  this->ToolBarLine2->AddWidget(this->DivideObtuseButton);
  this->ToolBarLine2->AddWidget(this->RemoveNeedlesButton);  
  this->ToolBarLine2->AddWidget(this->SmallTriButton);    
  
  // Update Toolbar - Line 3  
  this->ToolBarLine3->AddWidget(this->SmoothButton);
  this->ToolBarLine3->AddWidget(this->ChangeOrientationButton);
  this->ToolBarLine3->AddWidget(this->RemoveFreeNodesButton);
  this->ToolBarLine3->AddWidget(this->TestMeshButton);

  // Update Toolbar - Line 4    
  this->ToolBarLine4->AddWidget(this->CloseSurfaceButton);    
  this->ToolBarLine4->AddWidget(this->ViewGroupButton);  
  this->ToolBarLine4->AddWidget(this->TransformButton);  
  this->ToolBarLine4->AddWidget(this->RemoveGroupsButton);

  
//	if(!this->EnableCollapseInsertButtons)
//		{
//		this->InsertNodesButton->EnabledOff();		
//		this->CollapseButton->EnabledOff();
//		}
		
  this->PlaceComponents();
  this->ConfigureComponents();
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SetIcons()
{
  this->iconSmooth->SetImage(image_TrisurfButton_Smooth,
  								image_TrisurfButton_Smooth_width,
  								image_TrisurfButton_Smooth_height,
  								image_TrisurfButton_Smooth_pixel_size,
  								image_TrisurfButton_Smooth_length,
  								image_TrisurfButton_Smooth_decoded_length);	
  								
  this->iconSwap->SetImage(image_TrisurfButton_SwapDiagonals,
  								image_TrisurfButton_SwapDiagonals_width,
  								image_TrisurfButton_SwapDiagonals_height,
  								image_TrisurfButton_SwapDiagonals_pixel_size,
  								image_TrisurfButton_SwapDiagonals_length,
  								image_TrisurfButton_SwapDiagonals_decoded_length);
  								
  this->iconDivideObtuse->SetImage(image_TrisurfButton_DivideObtuse,
  								image_TrisurfButton_DivideObtuse_width,
  								image_TrisurfButton_DivideObtuse_height,
  								image_TrisurfButton_DivideObtuse_pixel_size,
  								image_TrisurfButton_DivideObtuse_length,
  								image_TrisurfButton_DivideObtuse_decoded_length);	  	

  this->iconPar->SetImage(image_TrisurfButton_Par,
  								image_TrisurfButton_Par_width,
  								image_TrisurfButton_Par_height,
  								image_TrisurfButton_Par_pixel_size,
  								image_TrisurfButton_Par_length,
  								image_TrisurfButton_Par_decoded_length);  																  								

  this->iconAutoPar->SetImage(image_TrisurfButton_AutoPar,
  								image_TrisurfButton_AutoPar_width,
  								image_TrisurfButton_AutoPar_height,
  								image_TrisurfButton_AutoPar_pixel_size,
  								image_TrisurfButton_AutoPar_length,
  								image_TrisurfButton_AutoPar_decoded_length); 
  								
  this->iconInsertNodes->SetImage(image_TrisurfButton_InsertNodes,
  								image_TrisurfButton_InsertNodes_width,
  								image_TrisurfButton_InsertNodes_height,
  								image_TrisurfButton_InsertNodes_pixel_size,
  								image_TrisurfButton_InsertNodes_length,
  								image_TrisurfButton_InsertNodes_decoded_length);	  	

  this->iconCollapse->SetImage(image_TrisurfButton_Collapse,
  								image_TrisurfButton_Collapse_width,
  								image_TrisurfButton_Collapse_height,
  								image_TrisurfButton_Collapse_pixel_size,
  								image_TrisurfButton_Collapse_length,
  								image_TrisurfButton_Collapse_decoded_length);  																  								
  
  this->iconRemoveTresTri->SetImage(image_TrisurfButton_RemoveTresTri,
  								image_TrisurfButton_RemoveTresTri_width,
  								image_TrisurfButton_RemoveTresTri_height,
  								image_TrisurfButton_RemoveTresTri_pixel_size,
  								image_TrisurfButton_RemoveTresTri_length,
  								image_TrisurfButton_RemoveTresTri_decoded_length);
  
	this->iconMeshInformation->SetImage(image_TrisurfButton_MeshInformation,
	  								image_TrisurfButton_MeshInformation_width,
	  								image_TrisurfButton_MeshInformation_height,
	  								image_TrisurfButton_MeshInformation_pixel_size,
	  								image_TrisurfButton_MeshInformation_length,
	  								image_TrisurfButton_MeshInformation_decoded_length);
	  								
	this->iconCloseSurface->SetImage(image_TrisurfButton_close_surface,
	  								image_TrisurfButton_close_surface_width,
	  							  image_TrisurfButton_close_surface_height,
	  								image_TrisurfButton_close_surface_pixel_size,
	  								image_TrisurfButton_close_surface_length,
	  								image_TrisurfButton_close_surface_decoded_length);  							
	  								
	this->iconRemoveGroup->SetImage(image_TrisurfButton_remove_group,
	  								image_TrisurfButton_remove_group_width,
	  							  image_TrisurfButton_remove_group_height,
	  								image_TrisurfButton_remove_group_pixel_size,
	  								image_TrisurfButton_remove_group_length,
	  								image_TrisurfButton_remove_group_decoded_length);

	this->iconSmallTri->SetImage(image_TrisurfButton_SmallTri,
	  								image_TrisurfButton_SmallTri_width,
	  							  image_TrisurfButton_SmallTri_height,
	  								image_TrisurfButton_SmallTri_pixel_size,
	  								image_TrisurfButton_SmallTri_length,
	  								image_TrisurfButton_SmallTri_decoded_length);

	
	this->iconChangeOrientation->SetImage(image_TrisurfButton_ChangeOrientation,
	  								image_TrisurfButton_ChangeOrientation_width,
	  							  image_TrisurfButton_ChangeOrientation_height,
	  								image_TrisurfButton_ChangeOrientation_pixel_size,
	  								image_TrisurfButton_ChangeOrientation_length,
	  								image_TrisurfButton_ChangeOrientation_decoded_length);
	
	this->iconTestMesh->SetImage(image_TrisurfButton_test_mesh,
	  								image_TrisurfButton_test_mesh_width,
	  							  image_TrisurfButton_test_mesh_height,
	  								image_TrisurfButton_test_mesh_pixel_size,
	  								image_TrisurfButton_test_mesh_length,
	  								image_TrisurfButton_test_mesh_decoded_length);
	
	this->iconTransformation->SetImage(image_TrisurfButton_transformation,
	  								image_TrisurfButton_transformation_width,
	  							  image_TrisurfButton_transformation_height,
	  								image_TrisurfButton_transformation_pixel_size,
	  								image_TrisurfButton_transformation_length,
	  								image_TrisurfButton_transformation_decoded_length);
	
	this->iconRemoveFreeNodes->SetImage(image_TrisurfButton_remove_free_nodes,
	  								image_TrisurfButton_remove_free_nodes_width,
	  							  image_TrisurfButton_remove_free_nodes_height,
	  								image_TrisurfButton_remove_free_nodes_pixel_size,
	  								image_TrisurfButton_remove_free_nodes_length,
	  								image_TrisurfButton_remove_free_nodes_decoded_length);
	
	this->iconViewGroup->SetImage(image_TrisurfButton_ViewGroup,
	  								image_TrisurfButton_ViewGroup_width,
	  							  image_TrisurfButton_ViewGroup_height,
	  								image_TrisurfButton_ViewGroup_pixel_size,
	  								image_TrisurfButton_ViewGroup_length,
	  								image_TrisurfButton_ViewGroup_decoded_length);
	  													 							 							 							 							
	this->iconRemoveNeedles->SetImage(image_TrisurfButton_RemoveNeedles,
	  								image_TrisurfButton_RemoveNeedles_width,
	  							  image_TrisurfButton_RemoveNeedles_height,
	  								image_TrisurfButton_RemoveNeedles_pixel_size,
	  								image_TrisurfButton_RemoveNeedles_length,
	  								image_TrisurfButton_RemoveNeedles_decoded_length);	  								
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",  
    this->Frame->GetWidgetName());

  this->Script("grid %s - - -sticky ew",
   	this->ToolBarLine1->GetWidgetName());   
   	
  this->Script("grid %s - - -sticky ew",
   	this->ToolBarLine2->GetWidgetName());   

  this->Script("grid %s - - -sticky ew",
   	this->ToolBarLine3->GetWidgetName());   
   	
  this->Script("grid %s - - -sticky ew",
   	this->ToolBarLine4->GetWidgetName());
   	
  this->Script("grid %s - - -sticky ew",
   	this->ViewModifiedCellsCheckButton->GetWidgetName());        		       	       		       		

  this->Script("grid %s - - -sticky ew", 
  	this->ViewWireFrameCheckButton->GetWidgetName());  
  	
  if( !this->AcceptButtonPressed )
	  {	
		this->ToolBarLine1->EnabledOff();
		this->ToolBarLine2->EnabledOff();		
		this->ToolBarLine3->EnabledOff();		
		this->ToolBarLine4->EnabledOff();
  	this->ViewWireFrameCheckButton->EnabledOff();
	  }
	else
		{	  
		this->ToolBarLine1->EnabledOn();
		this->ToolBarLine2->EnabledOn();		
		this->ToolBarLine3->EnabledOn();		
		this->ToolBarLine4->EnabledOn();
  	this->ViewWireFrameCheckButton->EnabledOn();
		}
		
	if(!this->EnableCollapseInsertButtons)
		{
		this->InsertNodesButton->EnabledOff();		
		this->CollapseButton->EnabledOff();
		}		
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", this->Frame->GetWidgetName());      
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SetValueChanged()
{
  this->ValueChanged = 1;
  this->ModifiedCallback();
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::RemoveNeedlesCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->ViewModifiedCellsCheckButton->EnabledOn();  	
	this->ViewModifiedCells = 1;
  this->ViewModifiedCellsCheckButton->SelectedStateOn(); 	

  this->Script("grid %s - - -sticky w",     
    this->RemoveNeedlesFrame->GetWidgetName());  
 
  this->Script("grid %s %s - - -sticky ew",     
  	this->RemoveNeedlesLabel->GetWidgetName(),
		this->RemoveNeedlesEntry->GetWidgetName());               

  this->Script("grid %s %s - - -sticky ew",     
  	this->RemoveNeedlesViewButton->GetWidgetName(),
		this->RemoveNeedlesRemoveButton->GetWidgetName());               

  this->Script("grid %s - - -sticky ew",     
		this->RemoveNeedlesLabelResponse->GetWidgetName()); 

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RemoveNeedlesEntry->GetWidgetName(),
    this->GetTclName());
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SmallTriCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->SmallTriLabelResponse->SetText("");		

  this->ViewModifiedCellsCheckButton->EnabledOn();  	
  this->ViewModifiedCells = 1;
  this->ViewModifiedCellsCheckButton->SelectedStateOn(); 	

  this->Script("grid %s - - -sticky w",     
    this->SmallTriFrame->GetWidgetName());  
 
  this->Script("grid %s %s - - -sticky ew",     
  	this->SmallTriLabel->GetWidgetName(),
		this->SmallTriEntry->GetWidgetName());               

  this->Script("grid %s %s - - -sticky ew",    
  	this->SmallTriAverageLabel->GetWidgetName(),  
  	this->SmallTriAverageLabelComputed->GetWidgetName());               

  this->Script("grid %s %s %s - - -sticky ew",    
  	this->SmallTriPercentualLabel->GetWidgetName(),  
  	this->SmallTriPercentualEntry->GetWidgetName(),
	this->SmallTriPercentualButton->GetWidgetName()); 

  this->Script("grid %s %s %s - - -sticky ew",    
  	this->SmallTriAverageButton->GetWidgetName(),  
  	this->SmallTriViewButton->GetWidgetName(),
	this->SmallTriRemoveButton->GetWidgetName()); 

  this->Script("grid %s - - -sticky ew",     
		this->SmallTriLabelResponse->GetWidgetName()); 

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->SmallTriEntry->GetWidgetName(),
    this->GetTclName());
}

void vtkPVHM3DTrisurfFilter::ViewNeedlesButtonCallback()
{
	// Erase Previous Message
  this->RemoveNeedlesLabelResponse->SetText("");		
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
  this->selectedFilterOnCallback = TRISURF_VIEW_NEEDLES;	  
}
	
void vtkPVHM3DTrisurfFilter::RemoveNeedlesButtonCallback()
{
  this->RemoveNeedlesLabelResponse->SetText("");		
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	      
  this->selectedFilterOnCallback = TRISURF_REMOVE_NEEDLES;	
}

void vtkPVHM3DTrisurfFilter::SmallTriAverageButtonCallback()
{
	// Erase Previous Message
  this->SmallTriLabelResponse->SetText("");		
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
  this->selectedFilterOnCallback = TRISURF_AVERAGE_SMALLTRI;

  this->SmallTriAverageLabel->EnabledOn();
  this->SmallTriAverageLabelComputed->EnabledOn();
  this->SmallTriPercentualLabel->EnabledOn();      
  this->SmallTriPercentualEntry->EnabledOn();
  this->SmallTriPercentualButton->EnabledOn();       	  	
}

void vtkPVHM3DTrisurfFilter::SmallTriUpdateButtonCallback()
{
	// Erase Previous Message
	double perc 				= this->SmallTriPercentualEntry->GetValueAsDouble() / 100.0;
	char   *averageText = this->SmallTriAverageLabelComputed->GetText();
	double average;
	sscanf(averageText, "%lf", &average);
	double newperc	= perc * average;
	this->SmallTriEntry->SetValueAsDouble(newperc);  	
}

void vtkPVHM3DTrisurfFilter::ViewSmallTriCallback()
{
	// Automatic update when viewing
	this->SmallTriUpdateButtonCallback();
	
	// Erase Previous Message
  this->SmallTriLabelResponse->SetText("");		
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
  this->selectedFilterOnCallback = TRISURF_VIEW_SMALLTRI;	  
}
	
void vtkPVHM3DTrisurfFilter::RemoveSmallTriCallback()
{
	// Erase Previous Message	
  this->SmallTriLabelResponse->SetText("");		
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	      
  this->selectedFilterOnCallback = TRISURF_REMOVE_SMALLTRI;	
}


// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ViewModifiedCellsCallback()
{
	this->ViewModifiedCells = (this->ViewModifiedCells) ? 0 : 1;	
	this->ModifiedCallback();	
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::NoShrinkCheckButtonCallback()
{
	this->NoShrink = (this->NoShrink) ? 0 : 1;	
	this->ModifiedCallback();	
}

// -----------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SmoothRemoveNeedleCheckButtonCallback()
{
	this->ModifiedCallback();	
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::CloseSurfaceButtonCallback()
{
	this->ForgetPreviousFrames();
	this->ModifiedCallback();
  
	this->Script("grid %s - - -sticky ew",
		this->CloseSurfaceFrame->GetWidgetName());
     
  this->Script("grid %s - - -sticky ew",
    this->CloseSurfaceLabel->GetWidgetName());
   
	this->selectedFilterOnCallback = TRISURF_CLOSE_SURFACE;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::MeshInformationButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
   
  this->Script("grid %s - - -sticky ew",
    this->MeshInformationFrame->GetWidgetName());
  
  this->Script("grid %s - - -sticky ew",     
  	this->MeshInformationLabel->GetWidgetName());
  
	this->selectedFilterOnCallback = TRISURF_MESH_INFORMATION;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::TestMeshButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky ew",
    this->TestMeshFrame->GetWidgetName());
  
  this->Script("grid %s - - -sticky ew",     
  	this->TestMeshLabel->GetWidgetName());
  
	this->selectedFilterOnCallback = TRISURF_TEST_MESH;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ChangeOrientationCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->ViewModifiedCellsCheckButton->EnabledOn();  	

  this->Script("grid %s - - -sticky w",     
    this->ChangeOrientationFrame->GetWidgetName());   

  this->Script("grid %s %s - - -sticky ew",     
  	this->ChangeOrientationLabel->GetWidgetName(),
		this->ChangeOrientationEntry->GetWidgetName());               

  this->Script("grid %s - - -sticky ew",     
		this->ChangeOrientationLabelResponse->GetWidgetName()); 

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->ChangeOrientationEntry->GetWidgetName(),
    this->GetTclName());
  
	this->selectedFilterOnCallback = TRISURF_CHANGE_ORIENTATION;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::RemoveFreeNodesButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky ew",
    this->RemoveFreeNodesFrame->GetWidgetName());
     
  this->Script("grid %s %s - - -sticky ew",
    this->FreeNodesLabel->GetWidgetName(),
  	this->RemoveFreeNodesLabel->GetWidgetName());
  	
  this->selectedFilterOnCallback = TRISURF_REMOVE_FREE_NODES;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::RemoveTresTriButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->ViewModifiedCellsCheckButton->EnabledOn();  	
     
  this->selectedFilterOnCallback = TRISURF_REMOVE_TRES_TRI;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::TransformButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky ew",
    this->TransformFrame->GetWidgetName());
     
  this->Script("grid %s - - -sticky ew",
    this->TranslateLabel->GetWidgetName());
  
  this->Script("grid %s %s %s %s %s %s - - -sticky ew",
    this->TXLabel->GetWidgetName(), 
    this->TXEntry->GetWidgetName(), 
    this->TYLabel->GetWidgetName(), 
    this->TYEntry->GetWidgetName(),
    this->TZLabel->GetWidgetName(), 
    this->TZEntry->GetWidgetName());
    
  this->Script("grid %s %s - - -sticky ew", 
    this->ScaleLabel->GetWidgetName());
  
  this->Script("grid %s %s %s %s %s %s - - -sticky ew",
    this->SXLabel->GetWidgetName(), 
    this->SXEntry->GetWidgetName(), 
    this->SYLabel->GetWidgetName(), 
    this->SYEntry->GetWidgetName(),
    this->SZLabel->GetWidgetName(), 
    this->SZEntry->GetWidgetName());
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->TXEntry->GetWidgetName(),
    this->GetTclName());
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->TYEntry->GetWidgetName(),
    this->GetTclName());
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->TZEntry->GetWidgetName(),
    this->GetTclName());
    
    
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->SXEntry->GetWidgetName(),
    this->GetTclName());
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->SYEntry->GetWidgetName(),
    this->GetTclName());
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->SZEntry->GetWidgetName(),
    this->GetTclName());
     
  this->selectedFilterOnCallback = TRISURF_TRANSFORMATION;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SmoothButtonCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky w",     
    this->SmoothFrame->GetWidgetName());   

	this->Script("grid %s %s - - -sticky ew",     
  	this->IterationsLabel->GetWidgetName(),
		this->IterationsEntry->GetWidgetName());               

	this->Script("grid %s %s - - -sticky ew",     
  	this->RelatationLabel->GetWidgetName(),
		this->RelatationEntry->GetWidgetName());
		
	this->Script("grid %s %s - - -sticky ew",     
  	this->GroupLabel->GetWidgetName(),
		this->GroupEntry->GetWidgetName());
		
	this->Script("grid %s - - -sticky ew",     
  	this->NoShrinkCheckButton->GetWidgetName());	

	this->Script("grid %s - - -sticky ew",     
  	this->SmoothRemoveNeedlesCheckButton->GetWidgetName());	
	
	// Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->IterationsEntry->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RelatationEntry->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->GroupEntry->GetWidgetName(),
    this->GetTclName());
	
	this->selectedFilterOnCallback = TRISURF_SMOOTH;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::RemoveGroupsCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky w",     
    this->RemoveGroupsFrame->GetWidgetName());   

  this->Script("grid %s %s - - -sticky ew",     
  	this->RemoveGroupsLabel->GetWidgetName(),
	this->RemoveGroupsEntry->GetWidgetName());               

  this->Script("grid %s %s - - -sticky ew",     
  	this->RemoveGroupsResponseLabel->GetWidgetName(),
	this->RemoveGroupsNodesLabel->GetWidgetName());

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RemoveGroupsEntry->GetWidgetName(),
    this->GetTclName());
	
  this->selectedFilterOnCallback = TRISURF_REMOVE_GROUPS;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SwapDiagonalsCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->ViewModifiedCellsCheckButton->EnabledOn();  	
  
  this->Script("grid %s - - -sticky w",
    this->SwapDiagonalsFrame->GetWidgetName());   

	this->Script("grid %s %s - - -sticky ew",     
  	this->AngleLabel->GetWidgetName(),
		this->AngleEntry->GetWidgetName());               

	this->Script("grid %s %s - - -sticky ew",
		this->DiagonalsLabel->GetWidgetName(),
  	this->DiagonalsSwappedLabel->GetWidgetName()); 

	// Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->AngleEntry->GetWidgetName(),
    this->GetTclName());
  
	this->selectedFilterOnCallback = TRISURF_SWAP_DIAGONALS;	
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::DivideObtuseCallback()
{
	this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->ViewModifiedCellsCheckButton->EnabledOn();  	
  
  this->Script("grid %s - - -sticky w",     
    this->DivideObtuseFrame->GetWidgetName());   

	this->Script("grid %s %s - - -sticky ew",     
  	this->DivideAngleLabel->GetWidgetName(),
		this->DivideAngleEntry->GetWidgetName());               

	this->Script("grid %s %s - - -sticky ew",     
		this->ElementsLabel->GetWidgetName(),
  	this->ElementsDividedLabel->GetWidgetName()); 

	// Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->DivideAngleEntry->GetWidgetName(),
    this->GetTclName());
  
	this->selectedFilterOnCallback = TRISURF_DIVIDE_OBTUSE;	
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ParButtonCallback()
{ 
	this->ForgetPreviousFrames();
    this->ModifiedCallback();
    this->ParButton->SetText("");
	this->selectedFilterOnCallback = TRISURF_PAR;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::AutoParButtonCallback()
{ 
  this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky w",     
    this->AutoParFrame->GetWidgetName());  

  this->Script("grid %s %s - - -sticky ew",     
		this->EdgesSizeLabel->GetWidgetName(),
  	this->EdgesSizeEntry->GetWidgetName()); 
   
  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->EdgesSizeEntry->GetWidgetName(),
    this->GetTclName());    
    
  this->selectedFilterOnCallback = TRISURF_AUTOPAR;
}


void vtkPVHM3DTrisurfFilter::ViewGroupCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->Script("grid %s - - -sticky w",     
    this->ViewGroupFrame->GetWidgetName());  

  this->Script("grid %s %s %s - - -sticky ew",     
  	this->ViewGroupLabel->GetWidgetName(),
		this->ViewGroupEntry->GetWidgetName(),
		this->ViewButton->GetWidgetName());              

  this->Script("grid %s %s %s - - -sticky ew",     
  	this->MergeGroupLabel->GetWidgetName(),
		this->GroupsEntry->GetWidgetName(),
		this->MergeButton->GetWidgetName());              

  this->Script("grid %s %s %s - - -sticky ew",
    this->SelectionWidgetLabel->GetWidgetName(),  
    this->SelectionWidgetComboBox->GetWidgetName(),
    this->SelectionWidgetButton->GetWidgetName());

  this->Script("grid %s - - -sticky ew",
    this->ViewMergeLabel->GetWidgetName());
  
  this->selectedFilterOnCallback = TRISURF_VIEW_GROUP;	  
}

void vtkPVHM3DTrisurfFilter::ViewCallback()
{
	// Erase Previous Message	
  this->ViewMergeLabel->SetText("");		

  this->selectedFilterOnCallback = TRISURF_VIEW_GROUP;
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
}

void vtkPVHM3DTrisurfFilter::MergeGroupCallback()
{
	// Erase Previous Message
  this->ViewMergeLabel->SetText("");		
	
  this->selectedFilterOnCallback = TRISURF_MERGE_GROUPS;	
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::InsertNodesCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();
  
  this->Script("grid %s - - -sticky w",     
    this->InsertNodesFrame->GetWidgetName());  

  this->Script("grid %s %s - - -sticky ew",     
  	this->InsertNodesLabel->GetWidgetName(),
	this->InsertNodesEntry->GetWidgetName());              

  this->Script("grid %s %s - - -sticky ew",     
		this->NodesLabel->GetWidgetName(),
  	this->NodesInsertedLabel->GetWidgetName()); 

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->InsertNodesEntry->GetWidgetName(),
    this->GetTclName());
    
  this->selectedFilterOnCallback = TRISURF_INSERT_NODES;	  
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::CollapseCallback()
{
  this->ForgetPreviousFrames();
  this->ModifiedCallback();

  this->Script("grid %s - - -sticky w",     
    this->CollapseFrame->GetWidgetName());   

  this->Script("grid %s %s - - -sticky ew",     
  	this->CollapseLabel->GetWidgetName(),
	this->CollapseEntry->GetWidgetName());               

  this->Script("grid %s %s - - -sticky ew",     
		this->EdgesLabel->GetWidgetName(),
    this->EdgesCollapsedLabel->GetWidgetName()); 

  // Set binds
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->CollapseEntry->GetWidgetName(),
    this->GetTclName());

  this->selectedFilterOnCallback = TRISURF_COLLAPSE;
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ViewWireFrameCallback()
{
	(this->ViewWireFrameCheckButton->GetSelectedState()) ? this->wireActor->SetVisibility(1) : this->wireActor->SetVisibility(0);
  this->ModifiedCallback();	
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SelectionWidgetCallback()
{
	if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Box") )
		{
		this->BoxWidget->SetVisibility(1);
		this->SphereWidget->SetVisibility(0);			
		this->SelectionWidgetButton->SetEnabled(1);
		return;
		}
	if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Sphere") )
		{
		this->SphereWidget->SetVisibility(1);
		this->BoxWidget->SetVisibility(0);					
		this->SelectionWidgetButton->SetEnabled(1);
		return;
		}
	this->BoxWidget->SetVisibility(0);
	this->SphereWidget->SetVisibility(0);
	this->SelectionWidgetButton->SetEnabled(0);		
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::SelectionWidgetCreateGroupCallback()
{
	if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Box") )
		this->BoxWidget->GetBounds(this->SelectionWidgetData);
	
	if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Sphere") )
		{
		double center[3];
		double radius;
		this->SphereWidget->GetCenter(center);
		radius = this->SphereWidget->GetRadius();
		this->SelectionWidgetData[0] = center[0];
		this->SelectionWidgetData[1] = center[1];
		this->SelectionWidgetData[2] = center[2];
		this->SelectionWidgetData[3] = radius;		
		}
	this->selectedFilterOnCallback = TRISURF_CREATE_GROUP;	
  this->ModifiedCallback();	
  this->GetPVSource()->PreAcceptCallback();	   
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ForgetPreviousFrames()
{
  this->Script("grid forget %s",   
    this->SmoothFrame->GetWidgetName());   	
	
  this->Script("grid forget %s",     
    this->SwapDiagonalsFrame->GetWidgetName());

  this->Script("grid forget %s",     
    this->DivideObtuseFrame->GetWidgetName());

  this->Script("grid forget %s",     
    this->InsertNodesFrame->GetWidgetName());       	
    
  this->Script("grid forget %s",     
    this->CollapseFrame->GetWidgetName());
  
  this->Script("grid forget %s",
    this->RemoveFreeNodesFrame->GetWidgetName());

  this->Script("grid forget %s",
    this->CloseSurfaceFrame->GetWidgetName());
  
  this->Script("grid forget %s",     
    this->TransformFrame->GetWidgetName());
  
  this->Script("grid forget %s",
    this->TestMeshFrame->GetWidgetName());
  
  this->Script("grid forget %s",
    this->MeshInformationFrame->GetWidgetName());
  
  this->Script("grid forget %s",
    this->RemoveGroupsFrame->GetWidgetName());

  this->Script("grid forget %s",
    this->ChangeOrientationFrame->GetWidgetName());

  this->Script("grid forget %s",
    this->ViewGroupFrame->GetWidgetName());

  this->Script("grid forget %s",     
    this->AutoParFrame->GetWidgetName());

  this->Script("grid forget %s",     
    this->RemoveNeedlesFrame->GetWidgetName());

  this->Script("grid forget %s",     
    this->SmallTriFrame->GetWidgetName());
 	  	    
  this->ViewModifiedCellsCheckButton->EnabledOff();  	    
  this->ViewModifiedCellsCheckButton->SelectedStateOff(); 
  this->ViewModifiedCells = false;  
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
	this->UpdateWidgetInfo();
}


//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::UpdateWidgetInfo()
{
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	this->GetPVSource()->GetProxy()->UpdateInformation();
	
  vtkSMStringVectorProperty *responseProp = vtkSMStringVectorProperty::SafeDownCast(
  	this->GetPVSource()->GetProxy()->GetProperty("filterInfo"));
  	
  //Obtendo resposta para o filtro swap diagonals.	
  if(this->selectedFilterOnCallback == TRISURF_SWAP_DIAGONALS)
    this->DiagonalsSwappedLabel->SetText(responseProp->GetElement(0));
  
  //Obtendo resposta para o filtro divide obtuse.
  else if(this->selectedFilterOnCallback == TRISURF_DIVIDE_OBTUSE)
    this->ElementsDividedLabel->SetText(responseProp->GetElement(0));
  
  //Obtendo resposta para o filtro remove free nodes.
  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_FREE_NODES)
    this->RemoveFreeNodesLabel->SetText(responseProp->GetElement(0));
    
  //Obtendo resposta para o filtro test mesh.
  else if(this->selectedFilterOnCallback == TRISURF_TEST_MESH)
    this->TestMeshLabel->SetText(responseProp->GetElement(0));
    
  //Obtendo resposta para o filtro mesh information.
  else if(this->selectedFilterOnCallback == TRISURF_MESH_INFORMATION)
    this->MeshInformationLabel->SetText(responseProp->GetElement(0));
  
  //Obtendo resposta do filtro collapse nodes.
  else if(this->selectedFilterOnCallback == TRISURF_COLLAPSE)
    this->EdgesCollapsedLabel->SetText(responseProp->GetElement(0));
  
  //Obtendo resposta do filtro collapse nodes.
  else if(this->selectedFilterOnCallback == TRISURF_INSERT_NODES)
    this->NodesInsertedLabel->SetText(responseProp->GetElement(0));
  
  //Obtendo a resposta para o filtro remove grupos.
  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_GROUPS)
	  {
	  if( !strcmp(responseProp->GetElement(0),"Failed1") )
		  {
		  this->RemoveGroupsResponseLabel->SetText("Aborted! One of the selected groups is invalid.");
		  this->RemoveGroupsNodesLabel->SetText(""); 	  	
		  }
	  else if( !strcmp(responseProp->GetElement(0),"Failed2") )
		  {
		  this->RemoveGroupsResponseLabel->SetText("Aborted! Can't remove all groups!");
		  this->RemoveGroupsNodesLabel->SetText(""); 	  	
		  }		  
	  else
	    this->RemoveGroupsNodesLabel->SetText(responseProp->GetElement(0));
	  }
	  
  //Obtendo a resposta para o filtro view groups
  else if(this->selectedFilterOnCallback == TRISURF_VIEW_GROUP)
    this->ViewMergeLabel->SetText(responseProp->GetElement(0));
    
  //Obtendo a resposta para o filtro merge Groups
  else if(this->selectedFilterOnCallback == TRISURF_MERGE_GROUPS)
    this->ViewMergeLabel->SetText(responseProp->GetElement(0));

  //Obtendo a resposta para o filtro merge Groups
  else if(this->selectedFilterOnCallback == TRISURF_CREATE_GROUP)
    this->ViewMergeLabel->SetText(responseProp->GetElement(0));

  //Obtendo a resposta para o filtro Change Orientation
  else if(this->selectedFilterOnCallback == TRISURF_CHANGE_ORIENTATION)
    this->ChangeOrientationLabelResponse->SetText(responseProp->GetElement(0));   

  //Obtendo a resposta para o filtro View/Remove Needles
  else if(this->selectedFilterOnCallback == TRISURF_VIEW_NEEDLES)
    this->RemoveNeedlesLabelResponse->SetText(responseProp->GetElement(0));   

  //Obtendo a resposta para o filtro View/Remove Needles
  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_NEEDLES)
    this->RemoveNeedlesLabelResponse->SetText(responseProp->GetElement(0));   

  //Obtendo a resposta para o filtro View Small Tri
  else if(this->selectedFilterOnCallback == TRISURF_VIEW_SMALLTRI)
    this->SmallTriLabelResponse->SetText(responseProp->GetElement(0));   

  //Obtendo a resposta para o filtro Remove Small
  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_SMALLTRI)
    this->SmallTriLabelResponse->SetText(responseProp->GetElement(0));     

  //Obtendo a resposta para o filtro Average Small
  else if(this->selectedFilterOnCallback == TRISURF_AVERAGE_SMALLTRI)
  	{
	this->SmallTriEntry->SetValue(responseProp->GetElement(0));     
	this->SmallTriAverageLabelComputed->SetText(responseProp->GetElement(0));
  	}
  //Obtendo resposta para o filtro close surface.
  else if(this->selectedFilterOnCallback == TRISURF_CLOSE_SURFACE)
    this->CloseSurfaceLabel->SetText(responseProp->GetElement(0));
  	
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::Accept()
{
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	this->GetPVSource()->GetProxy()->UpdateInformation();
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(1, 0, 0);
	
	vtkSMIntVectorProperty *parameterProp = vtkSMIntVectorProperty::SafeDownCast(
  	this->GetPVSource()->GetProxy()->GetProperty("ParameterLoadedProperty"));
	
	if(parameterProp->GetElement(0))
	{
	  this->EnableCollapseInsertButtons = true;
	  this->InsertNodesButton->EnabledOn();
	  this->CollapseButton->EnabledOn();
	  this->PlaceComponents();	  
	}
	
	// Set View Modified Cells Property
	vtkSMIntVectorProperty *ViewModified = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("ViewModifiedCellsProperty"));
	if(ViewModified) ViewModified->SetElement(0, (int) this->ViewModifiedCells);
	
	if(this->selectedFilterOnCallback)
	{	
		if(this->selectedFilterOnCallback == TRISURF_PAR)
			{
			//Parameter property.
	      vtkSMStringVectorProperty *parProp = vtkSMStringVectorProperty::SafeDownCast(
		    	this->GetPVSource()->GetProxy()->GetProperty("ApplyParameterFile"));
		    if(this->ParButton->GetFileName() != "None")
		    	{
		      parProp->SetElement(0, this->ParButton->GetFileName());
		      this->EnableCollapseInsertButtons = true;
	        this->InsertNodesButton->EnabledOn();
	        this->CollapseButton->EnabledOn();
		    	}
			}

		if(this->selectedFilterOnCallback == TRISURF_AUTOPAR)
			{
			//Parameter property.
	      vtkSMDoubleVectorProperty *AutoParProp = vtkSMDoubleVectorProperty::SafeDownCast(
		    	this->GetPVSource()->GetProxy()->GetProperty("ApplyAutoPar"));
		    if(AutoParProp)
		    	{
					AutoParProp->SetElement(0, this->EdgesSizeEntry->GetValueAsDouble());
		      this->EnableCollapseInsertButtons = true;
	        this->InsertNodesButton->EnabledOn();
	        this->CollapseButton->EnabledOn();
		    	}
			}
		
	  if(this->selectedFilterOnCallback == TRISURF_SMOOTH)
			{
			//Smooth property
			vtkSMDoubleVectorProperty *smoothProp = vtkSMDoubleVectorProperty::SafeDownCast(
						this->GetPVSource()->GetProxy()->GetProperty("ApplySmooth"));
		    
			//Smooth property
			smoothProp->SetElement(0, this->IterationsEntry->GetValueAsDouble());
			smoothProp->SetElement(1, this->RelatationEntry->GetValueAsDouble());
			smoothProp->SetElement(2, this->NoShrinkCheckButton->GetSelectedState());
	    smoothProp->SetElement(3, this->GroupEntry->GetValueAsDouble());
	    smoothProp->SetElement(4, this->SmoothRemoveNeedlesCheckButton->GetSelectedState());	
			}
		
		else if(this->selectedFilterOnCallback == TRISURF_SWAP_DIAGONALS)
			{
			vtkSMDoubleVectorProperty *swapProp = vtkSMDoubleVectorProperty::SafeDownCast(
		    this->GetPVSource()->GetProxy()->GetProperty("ApplySwap"));
		    
			//swap diagonals property
			swapProp->SetElement(0, this->AngleEntry->GetValueAsDouble());
			}	
    else if(this->selectedFilterOnCallback == TRISURF_DIVIDE_OBTUSE)
    	{
      vtkSMDoubleVectorProperty *obtuseProp = vtkSMDoubleVectorProperty::SafeDownCast(
 	  this->GetPVSource()->GetProxy()->GetProperty("ApplyObtuse"));	  	  
  	  obtuseProp->SetElement(0, this->DivideAngleEntry->GetValueAsDouble());
    	}
	  
	  else if(this->selectedFilterOnCallback == TRISURF_TEST_MESH)
		  {
	  	vtkSMIntVectorProperty *testProp = vtkSMIntVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyTestMesh"));
		    
		  testProp->SetElement(0, 1);
	  	}
	  
	  else if(this->selectedFilterOnCallback == TRISURF_MESH_INFORMATION)
		  {
	 	  vtkSMIntVectorProperty *meshInfoProp = vtkSMIntVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyMeshInfo"));
		    
		  meshInfoProp->SetElement(0, 1);
		  
		  this->GetPVSource()->GetProxy()->UpdatePropertyInformation(meshInfoProp);
		  }
	  
	  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_TRES_TRI)
	  	{
	  	vtkSMIntVectorProperty *tresProp = vtkSMIntVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyTresTri"));
		    
		  tresProp->SetElement(0, 1);
		  }	 
	  
	  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_FREE_NODES)
		  {
	  	vtkSMIntVectorProperty *removeProp = vtkSMIntVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyRemove"));
		    
		  removeProp->SetElement(0, 1);
		  }
	  
	  else if(this->selectedFilterOnCallback == TRISURF_TRANSFORMATION)
		  {
	    vtkSMDoubleVectorProperty *transformProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyTransform"));
		    
		  transformProp->SetElement(0, this->TXEntry->GetValueAsDouble());
		  transformProp->SetElement(1, this->TYEntry->GetValueAsDouble());
		  transformProp->SetElement(2, this->TZEntry->GetValueAsDouble());
		  transformProp->SetElement(3, this->SXEntry->GetValueAsDouble());
		  transformProp->SetElement(4, this->SYEntry->GetValueAsDouble());
		  transformProp->SetElement(5, this->SZEntry->GetValueAsDouble());
		  transformProp->SetElement(6, 0);
		  transformProp->SetElement(7, 0); 
		  transformProp->SetElement(8, 0);
	  	}
	  
	  else if(this->selectedFilterOnCallback == TRISURF_CLOSE_SURFACE)
		  {
	 	  vtkSMIntVectorProperty *closeProp = vtkSMIntVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyCloseSurface"));
		    
		  closeProp->SetElement(0, 0);
		  }
	  
	  else if(this->selectedFilterOnCallback == TRISURF_INSERT_NODES)
	  	{
	    vtkSMDoubleVectorProperty *nodesProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyInsertNodes"));
      
      nodesProp->SetElement(0, this->InsertNodesEntry->GetValueAsDouble());
		  }
	  
	  else if(this->selectedFilterOnCallback == TRISURF_COLLAPSE)
		  {
	    vtkSMDoubleVectorProperty *nodesRemoveProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyColapseNodes"));
      
      nodesRemoveProp->SetElement(0, this->CollapseEntry->GetValueAsDouble());
	  	}
	  
	  
	  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_GROUPS)
	  	{
	  	vtkSMStringVectorProperty *removeGroupsProp = vtkSMStringVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyRemoveGroups"));
		    
		  removeGroupsProp->SetElement(0, this->RemoveGroupsEntry->GetValue());
		  }

	  else if(this->selectedFilterOnCallback == TRISURF_CHANGE_ORIENTATION)
	  	{
	  	vtkSMStringVectorProperty *changeProp = vtkSMStringVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyChangeOrientation"));
		    
		  changeProp->SetElement(0, this->ChangeOrientationEntry->GetValue());
		  }
		  
	  else if(this->selectedFilterOnCallback == TRISURF_VIEW_GROUP)
	  	{ 		
	  	vtkSMIntVectorProperty *changeProp = vtkSMIntVectorProperty::SafeDownCast(
			  this->GetPVSource()->GetProxy()->GetProperty("ViewGroup"));
		  if( strcmp(this->ViewGroupEntry->GetValue(), "None") )
			  changeProp->SetElement(0, this->ViewGroupEntry->GetValueAsInt());
			else
				changeProp->SetElement(0, -1);
		  }		 
		  
	  else if(this->selectedFilterOnCallback == TRISURF_MERGE_GROUPS)
	  	{ 		
	  	vtkSMStringVectorProperty *mergeProp = vtkSMStringVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyMergeGroups"));
	  		
		  if( strcmp(this->GroupsEntry->GetValue(), "None"))
			  mergeProp->SetElement(0, this->GroupsEntry->GetValue());
			else
				mergeProp->SetElement(0, "-1");
		  }		 
	  else if(this->selectedFilterOnCallback == TRISURF_CREATE_GROUP)
	  	{ 		
	    vtkSMDoubleVectorProperty *widgetProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyCreateGroup"));
	    	
	    
			if(widgetProp)
				{
				if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Box") )
					{
					widgetProp->SetElement(0, 1.0);
					for(int i = 1; i <= 6; i++)
						widgetProp->SetElement(i, this->SelectionWidgetData[i-1]);
					}
				if( !strcmp(this->SelectionWidgetComboBox->GetValue(), "Sphere") )
					{
					widgetProp->SetElement(0, 2.0);					
					for(int i = 1; i <= 4; i++)
						widgetProp->SetElement(i, this->SelectionWidgetData[i-1]);
					}
				}
		  }			  
	  else if(this->selectedFilterOnCallback == TRISURF_VIEW_NEEDLES)
	  	{ 		
	  	vtkSMDoubleVectorProperty *needlesViewProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyViewNeedles"));
      needlesViewProp->SetElement(0, this->RemoveNeedlesEntry->GetValueAsDouble());
		  }		   
	  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_NEEDLES)
	  	{ 		
	  	vtkSMDoubleVectorProperty *needlesProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyRemoveNeedles"));
      needlesProp->SetElement(0, this->RemoveNeedlesEntry->GetValueAsDouble());
		  }		 
	  else if(this->selectedFilterOnCallback == TRISURF_VIEW_SMALLTRI)
	  	{ 		
	  	vtkSMDoubleVectorProperty *vstProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyViewSmallTri"));
      vstProp->SetElement(0, this->SmallTriEntry->GetValueAsDouble());
		  }		   
	  else if(this->selectedFilterOnCallback == TRISURF_REMOVE_SMALLTRI)
	  	{ 		
	  	vtkSMDoubleVectorProperty *rstProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyRemoveSmallTri"));
      rstProp->SetElement(0, this->SmallTriEntry->GetValueAsDouble());
		  }			  				  
	  else if(this->selectedFilterOnCallback == TRISURF_AVERAGE_SMALLTRI)
	  	{ 		
	  	vtkSMDoubleVectorProperty *sstProp = vtkSMDoubleVectorProperty::SafeDownCast(
		  this->GetPVSource()->GetProxy()->GetProperty("ApplyAverageSmallTri"));
      sstProp->SetElement(0, 0.0);		  
		  }			  
  	}
  	
	// Set Default Initial color (instead of blue)
	this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->StartColorButtonCallback(255,255,255);
	// this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->EndColorButtonCallback(255,0,0); 				

	// Wireframe mode 
	if(this->ViewWireFrameCheckButton->GetSelectedState())	
		this->SetWireFrameOn();

  this->AcceptButtonPressed = true;

  this->PlaceComponents();
  this->ConfigureComponents();
  this->UpdateWidgetInfo();
}

//------------------------------------------------------------------------------
void vtkPVHM3DTrisurfFilter::Initialize()
{
	int numWidgets = this->GetPVSource()->GetWidgets()->GetNumberOfItems();
	// Search the Box Widget (used in the view/merge/create groups filter
	for(int i = 0; i < numWidgets; i++)
		{
		if(!this->BoxWidget)
			this->BoxWidget    = vtkPVHMBoxWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetItemAsObject(i));
		if(!this->SphereWidget)
			this->SphereWidget = vtkPVSphereWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetItemAsObject(i));
    // if found, skip loop
    if(this->BoxWidget && this->SphereWidget) break;
		}
	// Remove widget from the widget GUI - Unpack Children!!!!
	this->BoxWidget->Unpack();
	this->BoxWidget->SetVisibility(0);
	this->SphereWidget->Unpack();
	this->SphereWidget->SetVisibility(0);	  	
}

void vtkPVHM3DTrisurfFilter::SetWireFrameOn()
{
  vtkLookupTable *lut = vtkLookupTable::New();
	  lut->SetTableValue(0, 255,255,255); // Initial Value (surface)
  	lut->Build();

  // Select and store the right output (enable right work in the paraview pipeline)
  if(!this->WireInput)
  	this->WireInput = this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetMainView()->GetRenderer()->GetActors()->GetLastActor()->GetMapper()->GetInput();
  else if(!this->WireSelect)
  	{
  	this->WireSelect = true;
  	this->WireInput = this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetMainView()->GetRenderer()->GetActors()->GetLastActor()->GetMapper()->GetInput();  	
  	}
  
  vtkDataSetMapper *tempMapper = vtkDataSetMapper::New();
		tempMapper->SetInput(WireInput);
		tempMapper->SetLookupTable(lut);
		tempMapper->ScalarVisibilityOff();

	this->wireActor->SetMapper(tempMapper);
	this->wireActor->GetProperty()->SetRepresentationToWireframe ();	
	this->wireActor->GetProperty()->SetColor( 0.0f, 0.0f, 0.0f);
	this->wireActor->GetProperty()->SetOpacity (1.0);
	this->wireActor->GetProperty()->EdgeVisibilityOn(); 
		
	if(!this->renderer)	
		{
		this->renderer = this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetMainView()->GetRenderer();
		this->renderer->AddActor( this->wireActor ); 	
		}
		
	tempMapper->Delete();	
	lut->Delete();
}

void vtkPVHM3DTrisurfFilter::Deselect()
{
	if(this->wireActor) this->wireActor->SetVisibility(0);
}

void vtkPVHM3DTrisurfFilter::Select()
{
	if(this->wireActor && this->ViewWireFrameCheckButton->GetSelectedState()) 
		this->wireActor->SetVisibility(1);
}

void vtkPVHM3DTrisurfFilter::Reset(){};
void vtkPVHM3DTrisurfFilter::Update(){};
void vtkPVHM3DTrisurfFilter::PostAccept(){};
void vtkPVHM3DTrisurfFilter::AcceptedCallback(){};
void vtkPVHM3DTrisurfFilter::AcceptInternal(vtkClientServerID){};
