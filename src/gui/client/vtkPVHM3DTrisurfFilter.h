/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkPVHM3DTrisurfFilter

  Author: Rodrigo L. S. Silva, Jan Palach

=========================================================================*/
// .NAME vtkPVHM3DTrisurfFilter
// .SECTION Description
// Creates KW Components for Segments

#ifndef _vtkPVHM3DTrisurfFilter_h_
#define _vtkPVHM3DTrisurfFilter_h_

#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkDataSet.h"
#include "vtkMapper.h"
#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"
#include "vtkPVHMBoxWidget.h"
#include "vtkPVSphereWidget.h" 

class vtkKWPushButton;  
class vtkKWFrameWithLabel;
class vtkKWEntry;
class vtkKWLabel;
class vtkKWCheckButton;
class vtkKWIcon; 
class vtkKWLoadSaveButton;
class vtkKWToolbar;
class vtkKWComboBox;

class VTK_EXPORT vtkPVHM3DTrisurfFilter : public vtkPVObjectWidget
{
public:
  static vtkPVHM3DTrisurfFilter *New();
  vtkTypeRevisionMacro(vtkPVHM3DTrisurfFilter, vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Execute event
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};
  
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents();
  void ConfigureComponents();
  
  // Description:
  // Call creation on the child.  
	virtual void 	Create(vtkKWApplication* app);
	
	// Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
  
  void SetValueChanged();
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX
  
  virtual void Reset();
  virtual void Update();
  virtual void AcceptedCallback();
  virtual void PostAccept();

  // Description:
  // Selection an Deselection of the object
	virtual void Deselect();
	virtual void Select();	

  //BTX
  // Description:
  // Called when the Accept button is pressed.  It moves the widget values to the 
  // VTK calculator filter.
  virtual void AcceptInternal(vtkClientServerID);
  //ETX

	// Description: 
	// Set Icon Values
	void SetIcons();

	// Description:
	// Disable all load frames   
	void ForgetPreviousFrames();
    
	// Local Callbacks
	void NoShrinkCheckButtonCallback();
	void SmoothRemoveNeedleCheckButtonCallback();	
	void SmoothButtonCallback();
	void SwapDiagonalsCallback();	
	void DivideObtuseCallback();	
	void ParButtonCallback();
	void InsertNodesCallback();
	void CollapseCallback();
	void RemoveTresTriButtonCallback(); 
	void RemoveFreeNodesButtonCallback();
	void TransformButtonCallback();
	void TestMeshButtonCallback();
	void MeshInformationButtonCallback();
	void CloseSurfaceButtonCallback();
	void RemoveGroupsCallback();
	void AutoParButtonCallback();
	void ChangeOrientationCallback();
	void ViewGroupCallback();	
	void ViewCallback();
	void MergeGroupCallback();
	void ViewModifiedCellsCallback();
	void SelectionWidgetCallback();
	void SelectionWidgetCreateGroupCallback();
	void RemoveNeedlesCallback();	
	void ViewNeedlesButtonCallback(); 
	void RemoveNeedlesButtonCallback(); 
	void SmallTriCallback(); 	
	void ViewSmallTriCallback(); 		
	void RemoveSmallTriCallback(); 			
	void ViewWireFrameCallback();	
	void SmallTriAverageButtonCallback();
	void SmallTriUpdateButtonCallback();	

	// Show wireframe mode
	void SetWireFrameOn();
			
	// Load Parameters
	vtkKWLoadSaveButton* 	ParButton;

	// AutoPar Parameters
	vtkKWPushButton*			AutoParButton;
	vtkKWFrameWithLabel*  AutoParFrame;
	vtkKWLabel*           EdgesSizeLabel;
	vtkKWEntry* 					EdgesSizeEntry;
	
	//Remove three triangles parameters.
	vtkKWPushButton*      RemoveTresTriButton; //Button to RemoveTresTri method.
	
	//Close Surface parameters.
	vtkKWFrameWithLabel*  CloseSurfaceFrame;	
	vtkKWPushButton*      CloseSurfaceButton; //Button to CloseSurface method.
	vtkKWLabel*           CloseSurfaceLabel;
		
	//RemoveFreeNodes parameters.
	vtkKWFrameWithLabel* RemoveFreeNodesFrame;
	vtkKWPushButton*		RemoveFreeNodesButton;
	vtkKWLabel*				RemoveFreeNodesLabel;
	vtkKWLabel*          FreeNodesLabel;
	
	//Transform parameters
	vtkKWFrameWithLabel* TransformFrame;
	vtkKWPushButton*     TransformButton;
	vtkKWLabel*				TranslateLabel;
	vtkKWLabel*          ScaleLabel;
	
	vtkKWLabel*          TXLabel;
	vtkKWEntry* 			TXEntry;
	vtkKWLabel*          TYLabel;
	vtkKWEntry* 			TYEntry;
	vtkKWLabel*          TZLabel;
	vtkKWEntry* 			TZEntry;
	
	vtkKWLabel*          SXLabel;
	vtkKWEntry* 			SXEntry;
	vtkKWLabel*          SYLabel;
	vtkKWEntry* 			SYEntry;
	vtkKWLabel*          SZLabel;
	vtkKWEntry* 			SZEntry;
	
	//Test Mesh parameters.
	vtkKWFrameWithLabel*	TestMeshFrame;
	vtkKWPushButton*		TestMeshButton;
	vtkKWLabel*				TestMeshLabel;
	
	//Mesh information parameters.
	vtkKWFrameWithLabel* MeshInformationFrame;
	vtkKWPushButton*		MeshInformationButton;
	vtkKWLabel*				MeshInformationLabel;
		
	// Smooth Parameters
  vtkKWPushButton *		SmoothButton;
  vtkKWFrameWithLabel*  SmoothFrame;
  vtkKWLabel* 				IterationsLabel;
  vtkKWEntry* 				IterationsEntry;  
  vtkKWLabel* 				RelatationLabel;
  vtkKWEntry* 				RelatationEntry;  
  vtkKWLabel* 				GroupLabel;
  vtkKWEntry* 				GroupEntry;     
  vtkKWCheckButton*		NoShrinkCheckButton;  
  int							NoShrink;
  vtkKWCheckButton*		SmoothRemoveNeedlesCheckButton;  
  
 	// Swap Diagonals Parameters
  vtkKWPushButton *		SwapDiagonalsButton;
  vtkKWFrameWithLabel*  SwapDiagonalsFrame;	
  vtkKWLabel* 					AngleLabel;
  vtkKWEntry* 					AngleEntry;  
  vtkKWLabel*						DiagonalsLabel;
  vtkKWLabel*						DiagonalsSwappedLabel;
  
 	// Divide Obtuse Parameters
  vtkKWPushButton *			DivideObtuseButton;
  vtkKWFrameWithLabel*  DivideObtuseFrame;	
  vtkKWLabel* 					DivideAngleLabel;
  vtkKWEntry* 					DivideAngleEntry;
  vtkKWLabel*						ElementsLabel;
  vtkKWLabel*						ElementsDividedLabel;

	// Description:
 	// Insert Nodes Parameters
  vtkKWPushButton *			InsertNodesButton;
  vtkKWFrameWithLabel*  InsertNodesFrame;	  
  vtkKWLabel* 					InsertNodesLabel;
  vtkKWEntry* 					InsertNodesEntry;  
  vtkKWLabel*						NodesLabel;
  vtkKWLabel*						NodesInsertedLabel;

	// Description:
 	// Collapse Parameters
  vtkKWPushButton *			CollapseButton;
  vtkKWFrameWithLabel*  CollapseFrame;	  
  vtkKWLabel* 					CollapseLabel;
  vtkKWEntry* 					CollapseEntry;  
  vtkKWLabel*						EdgesLabel;
  vtkKWLabel*						EdgesCollapsedLabel;

  //Description:
  //Remove groups parameters
  vtkKWPushButton *			RemoveGroupsButton;
  vtkKWFrameWithLabel*  RemoveGroupsFrame;	
  vtkKWLabel* 					RemoveGroupsLabel;
  vtkKWLabel* 					RemoveGroupsResponseLabel;
  vtkKWLabel* 					RemoveGroupsNodesLabel;
  vtkKWEntry* 					RemoveGroupsEntry;  

  // Description:
  // Change Orientation Parameters
  vtkKWPushButton *			ChangeOrientationButton;
  vtkKWFrameWithLabel*  ChangeOrientationFrame;	
  vtkKWLabel* 					ChangeOrientationLabel;
  vtkKWEntry* 					ChangeOrientationEntry;  
  vtkKWLabel* 					ChangeOrientationLabelResponse;

  // Description:
  // View/Remove Needles
  vtkKWPushButton *			RemoveNeedlesButton;
  vtkKWFrameWithLabel*  RemoveNeedlesFrame;	
  vtkKWLabel* 					RemoveNeedlesLabel;
  vtkKWEntry* 					RemoveNeedlesEntry;  
  vtkKWLabel* 					RemoveNeedlesLabelResponse;  
  vtkKWPushButton*			RemoveNeedlesViewButton;
  vtkKWPushButton*			RemoveNeedlesRemoveButton;  

  // Description:
  // View/Remove Needles
  vtkKWPushButton *			SmallTriButton;
  vtkKWFrameWithLabel*  SmallTriFrame;	
  vtkKWLabel* 					SmallTriLabel;
  vtkKWEntry* 					SmallTriEntry;  
  
  vtkKWLabel* 					SmallTriAverageLabel;  
  vtkKWLabel* 					SmallTriAverageLabelComputed;    
  vtkKWLabel* 					SmallTriPercentualLabel;      
  vtkKWEntry* 					SmallTriPercentualEntry;  
  vtkKWPushButton*			SmallTriPercentualButton;    
  
  vtkKWLabel* 					SmallTriLabelResponse;  
  vtkKWPushButton*			SmallTriAverageButton;  
  vtkKWPushButton*			SmallTriViewButton;
  vtkKWPushButton*			SmallTriRemoveButton;  

  // Description:
  // View/Merge Parameters
  vtkKWPushButton *			ViewGroupButton;
  vtkKWFrameWithLabel*  ViewGroupFrame;	
  vtkKWLabel* 					ViewGroupLabel;
  vtkKWEntry* 					ViewGroupEntry;  
  vtkKWPushButton*			ViewButton;

  vtkKWLabel* 					MergeGroupLabel;
  vtkKWEntry* 					GroupsEntry;  
  vtkKWPushButton*			MergeButton;
  vtkKWLabel*						ViewMergeLabel;

  vtkKWCheckButton*			ViewModifiedCellsCheckButton;
	bool									ViewModifiedCells;

	// Descrition
	// Box Widget Stuff
  vtkKWLabel* 					SelectionWidgetLabel;
  vtkKWComboBox*				SelectionWidgetComboBox;
  vtkKWPushButton*			SelectionWidgetButton;
  
	// Descrition:
 	// Box widget used in the "create group" filter
  vtkPVHMBoxWidget  *BoxWidget;
  vtkPVSphereWidget *SphereWidget; 	 	  

	// Descrition:
 	// Selection Widget Data
  double SelectionWidgetData[6];
 
	// Main Frame
  vtkKWFrameWithLabel   *Frame;

	// Allow the representation in Wireframe with the Surface
	vtkActor 					*wireActor;
	vtkRenderer				*renderer;
  vtkKWCheckButton	*ViewWireFrameCheckButton; 
  vtkDataSet				*WireInput;
  bool 							WireSelect;
  
	// Descrition:
	// Toolbar used to control the software operation mode 
	vtkKWToolbar *ToolBarLine1;
	vtkKWToolbar *ToolBarLine2;	
	vtkKWToolbar *ToolBarLine3;
	vtkKWToolbar *ToolBarLine4;	
	
	// Controls first interaction 
	bool AcceptButtonPressed;
     
protected:
  vtkPVHM3DTrisurfFilter();
  virtual ~vtkPVHM3DTrisurfFilter();

 	// Description:
 	// Controls if proxy is already set 
  bool SetProxy; 

 	// Description:
 	// Stores which filter was invoked (defines in vtkHM3DTrisurfFilter.h)
	int InvokedFilter;

 	// Description:
 	// The "Collapse" and "InsertNodes" buttons are enabled after reading a parameter file
	bool EnableCollapseInsertButtons;
	
	int selectedFilterOnCallback;
 
  int ValueChanged;
 
 	// Description:
 	// Button Icons
 	vtkKWIcon* iconSmooth;
 	vtkKWIcon* iconSwap;
 	vtkKWIcon* iconDivideObtuse; 	 	
 	vtkKWIcon* iconPar; 	  	
 	vtkKWIcon* iconAutoPar; 	   	
 	vtkKWIcon* iconInsertNodes; 	  	
 	vtkKWIcon* iconCollapse;
 	vtkKWIcon* iconRemoveTresTri;
 	vtkKWIcon* iconMeshInformation;
 	vtkKWIcon* iconCloseSurface;
 	vtkKWIcon* iconRemoveGroup;
 	vtkKWIcon* iconTestMesh;
 	vtkKWIcon* iconTransformation;
 	vtkKWIcon* iconRemoveFreeNodes;
 	vtkKWIcon* iconRemoveNeedles;
 	vtkKWIcon* iconChangeOrientation; 	 	
 	vtkKWIcon* iconViewGroup;
 	vtkKWIcon* iconSmallTri; 	
 
private:  
  vtkPVHM3DTrisurfFilter(const vtkPVHM3DTrisurfFilter&); // Not implemented
  void operator=(const vtkPVHM3DTrisurfFilter&); // Not implemented
}; 

#endif  /*_vtkPVHM3DTrisurfFilter_h_*/

