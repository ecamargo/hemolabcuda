#include "vtkPVHMBoxWidget.h"

#include "vtkPVApplication.h"
#include "vtkPVSource.h"
#include "vtkPVSourceNotebook.h"
#include "vtkPVWidgetCollection.h"

#include "vtkSM3DWidgetProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMBoxWidgetProxy.h"
#include "vtkSMBoxProxy.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"

#include "vtkSMRenderModuleProxy.h"

#include "vtkCallbackCommand.h"


vtkCxxRevisionMacro(vtkPVHMBoxWidget, "$Rev$");
vtkStandardNewMacro(vtkPVHMBoxWidget);

vtkPVHMBoxWidget::vtkPVHMBoxWidget()
{
  this->AlreadyAccepted = 0;
  this->BoxClicked = 0;
  
  for (int i=0; i<6; i++)
  {
  	this->Extent[i] = 0;
  	this->MaxExtent[i] = 0;
  	this->Bounds[i] = 0;
  	this->MaxBounds[i] = 0;
  }
  this->SetWidgetProxyXMLName("HMBoxWidget");
}

vtkPVHMBoxWidget::~vtkPVHMBoxWidget()
{
//  this->AlreadyAccepted = 0;
}

void vtkPVHMBoxWidget::Initialize()
{
  this->Superclass::Initialize();
  
  vtkSMIntVectorProperty* RotationProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("RotationEnabled"));
    
  //Disabling rotation box interactivity.
  RotationProperty->SetElement(0, 0);
}

void vtkPVHMBoxWidget::ModifiedCallback()
{
//  cout << "BoxWidget::ModifiedCallback" << endl;
  this->Superclass::ModifiedCallback();
}

void vtkPVHMBoxWidget::SaveInBatchScript(ofstream *file)
{
  //It's pure virtual.
}

void vtkPVHMBoxWidget::Accept()
{
  this->Superclass::Accept();

  //All the Accecpts have been made, but the InvokeEvent on the ExecuteEvent repaints it with green.
  this->GetPVSource()->GetNotebook()->SetAcceptButtonColorToUnmodified();
  this->AlreadyAccepted = 1;
}

void vtkPVHMBoxWidget::ExecuteEvent(vtkObject *obj, unsigned long l, void *p)
{
  vtkSMIntVectorProperty* BoxClickedProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("BoxClicked"));

  this->WidgetProxy->UpdatePropertyInformation(BoxClickedProperty);

  this->BoxClicked = BoxClickedProperty->GetElement(0);


  this->Superclass::ExecuteEvent(obj, l, p);
  
  this->SetExtentFromBoxWidget();

  if (this->BoxClicked)
  {
    this->InvokeEvent("LeftButtonPressEvent");
  }
}

int vtkPVHMBoxWidget::GetBoxClicked()
{
  return this->BoxClicked;
}

void vtkPVHMBoxWidget::SetExtentFromBoxWidget()
{
  vtkSMDoubleVectorProperty* Bound0Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound0"));

  vtkSMDoubleVectorProperty* Bound1Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound1"));
    
  vtkSMDoubleVectorProperty* Bound2Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound2"));
    
  vtkSMDoubleVectorProperty* Bound3Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound3"));
    
  vtkSMDoubleVectorProperty* Bound4Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound4"));
    
  vtkSMDoubleVectorProperty* Bound5Property = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Bound5"));

  this->WidgetProxy->UpdatePropertyInformation(Bound0Property);
  this->WidgetProxy->UpdatePropertyInformation(Bound1Property);
  this->WidgetProxy->UpdatePropertyInformation(Bound2Property);
  this->WidgetProxy->UpdatePropertyInformation(Bound3Property);
  this->WidgetProxy->UpdatePropertyInformation(Bound4Property);
  this->WidgetProxy->UpdatePropertyInformation(Bound5Property);
  
  this->Bounds[0] = Bound0Property->GetElement(0);
  this->Bounds[1] = Bound1Property->GetElement(0);
  this->Bounds[2] = Bound2Property->GetElement(0);
  this->Bounds[3] = Bound3Property->GetElement(0);
  this->Bounds[4] = Bound4Property->GetElement(0);
  this->Bounds[5] = Bound5Property->GetElement(0);

  for (int i=0; i<6; i=i+2)
  {
    this->Extent[i] = int((this->MaxExtent[i+1] * this->Bounds[i]) / this->MaxBounds[i+1]);
    this->Extent[i+1] = int((this->MaxExtent[i+1] * this->Bounds[i+1]) / this->MaxBounds[i+1]);
  }

  this->ModifiedCallback();
}

void vtkPVHMBoxWidget::GetBounds(double *bounds)
{
	for(int i = 0; i < 6; i++)
		bounds[i] = this->Bounds[i];	
}

void vtkPVHMBoxWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->SetFrameLabel("Box Widget");
  this->ControlFrame->SetParent(this->Frame);
  this->ControlFrame->Create(this->GetApplication());
  //Renaming FrameLabel and avoiding interface construction.
}

void vtkPVHMBoxWidget::PlaceWidget(double bds[6])
{
  if(!this->AlreadyAccepted)
  {
    this->MaxBounds[0] = bds[0];
    this->MaxBounds[1] = bds[1];
    this->MaxBounds[2] = bds[2];
    this->MaxBounds[3] = bds[3];
    this->MaxBounds[4] = bds[4];
    this->MaxBounds[5] = bds[5];
  }
  
  this->Bounds[0] = bds[0];
  this->Bounds[1] = bds[1];
  this->Bounds[2] = bds[2];
  this->Bounds[3] = bds[3];
  this->Bounds[4] = bds[4];
  this->Bounds[5] = bds[5];
  
  this->Superclass::PlaceWidget(bds);
}

void vtkPVHMBoxWidget::SetExtent(int *PassedExtent)
{
  for (int i=0; i<6; i++)
  {
  	this->Extent[i] = PassedExtent[i];
  }
  this->UpdateBoxWidget();
}

int *vtkPVHMBoxWidget::GetExtent()
{
  this->SetExtentFromBoxWidget();
  
  return this->Extent;
}

void vtkPVHMBoxWidget::GetExtent(int *PassedExtent)
{
  this->SetExtentFromBoxWidget();
  
  for (int i=0; i<6; i++)
  {
    PassedExtent[i] = this->Extent[i];
  }
}

void vtkPVHMBoxWidget::SetMaxExtent(int *PassedExtent)
{
  for (int i=0; i<6; i++)
  {
  	this->MaxExtent[i] = PassedExtent[i];
  }
}

void vtkPVHMBoxWidget::UpdateBoxWidget()
{
  //Setting the right bound.
  for (int i=0; i<6; i=i+2)
  {
    this->Bounds[i] = double((this->MaxBounds[i+1] * this->Extent[i]) / this->MaxExtent[i+1]);
  }

  //Setting the left bound.
  for (int i=1; i<6; i=i+2)
  {
    this->Bounds[i] = double((this->MaxBounds[i] * this->Extent[i]) / this->MaxExtent[i]);
  }

  this->ModifiedCallback();
  
  this->PlaceWidget(this->Bounds);
}
