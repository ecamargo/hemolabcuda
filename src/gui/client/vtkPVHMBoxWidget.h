#ifndef _vtkPVHMBoxWidget_h_
#define _vtkPVHMBoxWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVBoxWidget.h"

class vtkPV3DWidget;

class vtkKWFrameWithLabel;
class vtkKWFrameWithLabel;

class vtkPVApplication;

class VTK_EXPORT vtkPVHMBoxWidget : public vtkPVBoxWidget
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMBoxWidget *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMBoxWidget, vtkPVBoxWidget);

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  
  // Description:
  // Set/Get widget visibility
  vtkSetMacro(WidgetVisibility,int);  
  vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
  vtkSetMacro(ReadOnly,int);  
  vtkGetMacro(ReadOnly,int);	
   
  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();

  // Description:
  // Set the Extent array.
  void SetExtent(int *PassedExtent);

  // Description:
  // Get the Extent array.  
  int *GetExtent();

  // Description:
  // Get the Extent array.  
  void GetExtent(int *PassedExtent);
  
  // Description:
  // Set the MaxExtent array.
  void SetMaxExtent(int *PassedExtent);

  // Description:
  // Get Widget Bounds
  void GetBounds(double*);
  
  virtual void ModifiedCallback();
  
  int GetBoxClicked();

protected:

  // Description:6]
  // Constructor
  vtkPVHMBoxWidget();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMBoxWidget();

  // Description:
  // Method executed when any iteraction is made on the viewport.
  virtual void	ExecuteEvent(vtkObject *obj, unsigned long l, void *p);

  // Description:
  // Method responsible for creating the Interface that interacts with this filter's source.
  virtual void ChildCreate(vtkPVApplication* pvApp);

  // Description:
  // Places the widget on the viewport based on the 2 limit bounds passed to the method.
  virtual void PlaceWidget(double bds[6]);
//  virtual void PlaceWidget() { this->Superclass::PlaceWidget(); }

  // Description:
  // Update the BoxWidget based on the current extent.  
  virtual void UpdateBoxWidget();
  
  
  virtual void SetExtentFromBoxWidget();

  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;

  // Description:
  // Filter Image Data current output extents.
  int Extent[6];

  // Description:
  // Filter Image Data output maximum extents.
  int MaxExtent[6];

  // Description:
  // Widget Box current bounds.
  double Bounds[6];

  // Description:
  // Widget Box maximum bounds.
  double MaxBounds[6];

  // Description:
  // Controls if the filter has already been accepted.
  int AlreadyAccepted;
  
  int BoxClicked;
  
private:  
  vtkPVHMBoxWidget(const vtkPVHMBoxWidget&); // Not implemented
  void operator=(const vtkPVHMBoxWidget&); // Not implemented
}; 

#endif
