/*
 * $Id: vtkPVHMCoupledModelConfigurationWidget.cxx 340 2006-05-12 13:40:37Z  $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWIcon.h"
#include "vtkPVHMCoupledModelConfigurationWidget.h"
#include "vtkKWNotebook.h"
#include "vtkKWLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWRadioButton.h"
#include "vtkKWComboBox.h"
#include "vtkKWMessageDialog.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMSourceProxy.h"
#include <time.h>
#include "vtkKWScale.h"
#include <vtksys/SystemTools.hxx>
 

vtkCxxRevisionMacro(vtkPVHMCoupledModelConfigurationWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMCoupledModelConfigurationWidget);

//----------------------------------------------------------------------------
vtkPVHMCoupledModelConfigurationWidget::vtkPVHMCoupledModelConfigurationWidget()
{
	
	this->Modelconfig1DFrame = vtkKWFrameWithLabel::New();
	this->Modelconfig3DFrame = vtkKWFrameWithLabel::New();
	 
	this->CouplingSchemeLabel = vtkKWLabel::New();
	
	this->SegregationTypeRadioButtonSet = vtkKWRadioButtonSet::New();
	
	
	this->CouplingSchemeMonoRadio = vtkKWRadioButton::New();
	this->CouplingSchemeSegregatedRadio = vtkKWRadioButton::New();
 

 	this->CouplingSchemeFrame = vtkKWFrameWithLabel::New();
 
	this->ModelconfigMonolithicIterationsFrame = vtkKWFrameWithLabel::New();
 
  this->labelIter = vtkKWLabel::New();
  this->entryIter = vtkKWEntry::New();
  this->labelSuPar = vtkKWLabel::New();
  this->scaleSuPar = vtkKWScaleWithEntry::New();
  
  
	this->SegretatedOrderLabel = vtkKWLabel::New();
 	this->Segregated1d3dRadio = vtkKWRadioButton::New();
 	this->Segregated3d1dRadio = vtkKWRadioButton::New();

 	this->SegretatedSchemeTypeLabel = vtkKWLabel::New();
 	this->SegregatedLinearRadio = vtkKWRadioButton::New();
 	this->SegregatedNonLinearRadio = vtkKWRadioButton::New();
 
 
	this->SegregatedConfigFrame = vtkKWFrameWithLabel::New();
  
  this->labelIterNonLinearSegregated = vtkKWLabel::New();
  this->entryIterNonLinearSegregated = vtkKWEntry::New();
  this->labelSuParNonLinearSegregated = vtkKWLabel::New();
  this->scaleSuParNonLinearSegregated = vtkKWScaleWithEntry::New();


  this->SetTitle("Coupled Model Solver Files Generation Setup");


	this->AdditionalParamSolver1DFrame = vtkKWFrameWithLabel::New();

	this->AdditionalParamSolver3DFrame = vtkKWFrameWithLabel::New();
	
	this->SolverConfig1D3DFrame = vtkKWFrameWithLabel::New();
	
	
	this->SolverConfigMonoFrame = vtkKWFrameWithLabel::New();
	this->SolverConfigSegregated3DFrame = vtkKWFrameWithLabel::New();
	this->SolverConfigSegregated1DFrame = vtkKWFrameWithLabel::New();
	
	this->SolverConfigGeneralSegregatedFrame = vtkKWFrameWithLabel::New();
	
	// monolithic solver config


	this->frameIterativeSolver = vtkKWFrameWithLabel::New();

	this->labelIterationsBeforeRestart = vtkKWLabel::New();
	this->entryIterationsBeforeRestart = vtkKWEntry::New();

  this->labelConvergenceError = vtkKWLabel::New();
	this->entryConvergenceError = vtkKWEntry::New();
  
  this->labelkrylov = vtkKWLabel::New();
	this->entrykrylov = vtkKWEntry::New();
	
	this->framePreconditioning = vtkKWFrameWithLabel::New();
	
  this->labelFParam = vtkKWLabel::New();
	this->entryFParam = vtkKWEntry::New();
  
  this->labelDispTolerance = vtkKWLabel::New();
  this->entryDispTolerance = vtkKWEntry::New();
	//**************************************************************************
	
	
	this->FrameScroll = vtkKWFrameWithScrollbar::New();
	this->WindowFrame = vtkKWFrame::New();
	this->notebook = vtkKWNotebook::New();


 	this->SolverTimeConfigFrame = vtkKWFrameWithLabel::New();
 	
 	this->ConvergenceParametersFrame1D = vtkKWFrameWithLabel::New();
	
 	this->ConvergenceParametersFrame3D = vtkKWFrameWithLabel::New();
	
	// 1D MC
  this->labelForm = vtkKWLabel::New();
  this->radioLSV = vtkKWRadioButton::New();
  this->radioLS = vtkKWRadioButton::New();
	
	this->labelDensity1D=vtkKWLabel::New();
  this->entryDensity1D=vtkKWEntry::New();

  this->labelViscosity1D=vtkKWLabel::New();
  this->entryViscosity1D=vtkKWEntry::New();

  this->labelArterialWallLaw1D=vtkKWLabel::New();
  this->comboArterialWallLaw1D=vtkKWComboBox::New();

  this->labelVelocity=vtkKWLabel::New();
  this->thumbVel=vtkKWThumbWheel::New();
  
	// 
	
		
	//3D mc
	this->labelModel= vtkKWLabel::New();
	this->radioCompliant= vtkKWRadioButton::New();
	this->radioRigid= vtkKWRadioButton::New();
	this->labelDensity3D= vtkKWLabel::New();
	this->entryDensity3D=vtkKWEntry::New();
	this->labelBloodLaw= vtkKWLabel::New();
	this->radioBloodLawNewton= vtkKWRadioButton::New();
	this->radioBloodLawCasson= vtkKWRadioButton::New();
	this->labelViscosity3D= vtkKWLabel::New();
	this->entryViscosity3D=vtkKWEntry::New();
	this->labelAVisco= vtkKWLabel::New();
	this->entryAVisco=vtkKWEntry::New();
	this->labelLimitStress= vtkKWLabel::New();
	this->entryLimitStress=vtkKWEntry::New();
	this->labelRegularParameter= vtkKWLabel::New();
	this->comboRegularParameter=vtkKWComboBox::New();
	this->labelArterialWallLaw3D= vtkKWLabel::New();
	this->comboArterialWallLaw3D=vtkKWComboBox::New();
	//
	
	
	
	// time param
	this->ResumeLabel = vtkKWLabel::New();
	this->radioResumeYes = vtkKWRadioButton::New();
	this->radioResumeNo = vtkKWRadioButton::New();
	this->labelNumberOfTimeSteps = vtkKWLabel::New();
	this->entryNumberOfTimeSteps =vtkKWEntry::New();
	
	this->labelTimeStep = vtkKWLabel::New();
	this->entryTimeStep =vtkKWEntry::New();
	this->labelInitialTime = vtkKWLabel::New();
	this->entryInitialTime =vtkKWEntry::New();
	this->labelNumberOfCardiacCycles = vtkKWLabel::New();
	this->entryNumberOfCardiacCycles =vtkKWEntry::New();
	
	this->labelCardiacCycleTime = vtkKWLabel::New();
	this->entryCardiacCycleTime =vtkKWEntry::New();
	this->labelFinalTime = vtkKWLabel::New();
	this->entryFinalTime =vtkKWEntry::New();
	this->CalculateTimeStepButton  = vtkKWPushButton::New();
  //
	
	
	
	//3d conv
	this->labelVariable3D = vtkKWLabel::New();
	this->labelRefVal3D = vtkKWLabel::New();
	this->labelConEr3D = vtkKWLabel::New();

  this->labelVelocityX = vtkKWLabel::New();
  this->labelVelocityY = vtkKWLabel::New();
  this->labelVelocityZ = vtkKWLabel::New();
  this->labelDisplacementX = vtkKWLabel::New();
  this->labelDisplacementY = vtkKWLabel::New();
  this->labelDisplacementZ = vtkKWLabel::New();
  
  this->labelPressure3D  = vtkKWLabel::New();
  this->entryRefPressure3D  = vtkKWEntry::New();
  this->entryConvPressure3D  = vtkKWEntry::New();
  
  this->entryRefVelocityX = vtkKWEntry::New();
  this->entryRefVelocityY = vtkKWEntry::New();
  this->entryRefVelocityZ = vtkKWEntry::New();
  this->entryRefDisplacementX= vtkKWEntry::New();
  this->entryRefDisplacementY = vtkKWEntry::New();
  this->entryRefDisplacementZ = vtkKWEntry::New();
  
  this->entryConvVelocityX = vtkKWEntry::New(); 
  this->entryConvVelocityY= vtkKWEntry::New();
  this->entryConvVelocityZ = vtkKWEntry::New();
  this->entryConvDisplacementX = vtkKWEntry::New();
  this->entryConvDisplacementY = vtkKWEntry::New();
  this->entryConvDisplacementZ = vtkKWEntry::New();
  
  
  this->labelIter3D = vtkKWLabel::New();
  this->entryIter3D = vtkKWEntry::New();
  this->labelSuPar3D = vtkKWLabel::New();
  this->scaleSuPar3D = vtkKWScaleWithEntry::New();
	//**********************************************	
	
	
	
	// 1d conv
	
	this->labelVariable1D = vtkKWLabel::New();
	this->labelRefVal1D = vtkKWLabel::New();
	this->labelConEr1D = vtkKWLabel::New();
	
	this->labelPress = vtkKWLabel::New();
	this->entryRePr= vtkKWEntry::New();
	this->entryCePr= vtkKWEntry::New();

	this->labelFlux = vtkKWLabel::New();
	this->entryReFl= vtkKWEntry::New();
	this->entryCeFl= vtkKWEntry::New();


	this->labelElPre= vtkKWLabel::New();
	this->entryReEP= vtkKWEntry::New();
	this->entryCeEP= vtkKWEntry::New();

	this->labelArea= vtkKWLabel::New();
	this->entryReAr= vtkKWEntry::New();
	this->entryCeAr= vtkKWEntry::New();


  this->labelIter1D = vtkKWLabel::New();
  this->entryIter1D = vtkKWEntry::New();
  this->labelSuPar1D = vtkKWLabel::New();
  this->scaleSuPar1D = vtkKWScaleWithEntry::New();


	//**********************************************	
	
	
	// 1d solver param
  this->labelTethaScheme1D= vtkKWLabel::New();
	this->thumbTheta1D= vtkKWThumbWheel::New();
	
	this->labelAditi1D= vtkKWLabel::New();
	this->radioYes1D= vtkKWRadioButton::New();
	this->radioNo1D= vtkKWRadioButton::New();
	
	this->labelPena1D= vtkKWLabel::New();
	this->entryPena1D= vtkKWEntry::New();
  //	
	
	
	//3d solver param
	this->labelTethaScheme3D= vtkKWLabel::New();
	this->thumbTheta3D= vtkKWThumbWheel::New();
	
	this->TetraCompressibilityLabel= vtkKWLabel::New();
	this->TetraCompressibilityEntry= vtkKWEntry::New();
	this->labelAditi3D= vtkKWLabel::New();
	this->radioYes3D= vtkKWRadioButton::New();
	this->radioNo3D= vtkKWRadioButton::New();
	
	this->labelPena3D= vtkKWLabel::New();
	this->entryPena3D= vtkKWEntry::New();
	
	this->labelIncrementalVersion= vtkKWLabel::New();
	this->radioICYes= vtkKWRadioButton::New();
	this->radioICNo= vtkKWRadioButton::New();
	//	
	
	// misc

	this->ScaleFactor3DLabel = vtkKWLabel::New();
	this->ScaleFactor3DEntry = vtkKWEntry::New();
	
	this->labelRenu= vtkKWLabel::New();
	this->entryRenu= vtkKWEntry::New();
	  
	this->labelFileOutput= vtkKWLabel::New();
	
	this->entryFileOutput= vtkKWEntry::New();
	
	this->labelScreenOutput= vtkKWLabel::New();
	
	this->entryScreenOutput= vtkKWEntry::New();
	this->labelLogFile= vtkKWLabel::New();
	
	this->entryLogFile= vtkKWEntry::New();
	this->labelFilesPath= vtkKWLabel::New();
	this->entryFilesPath= vtkKWEntry::New();
	this->pushButtonBrowse= vtkKWPushButton::New();
	this->CommitChangesButtonPage1 =vtkKWPushButton::New();
	this->textFilesGenerationStatus =vtkKWTextWithScrollbars::New();
	
	
	this->ChooseDirectoryDialog =  vtkKWLoadSaveDialog::New();
	
  //
	
	
	//1D solver config	SEgregated  
  this->labelSolverType1D = vtkKWLabel::New();
	this->comboSolverType1D = vtkKWComboBox::New();

  this->labelRLS1D = vtkKWLabel::New();
  this->comboRLS1D = vtkKWComboBox::New();

	this->frameIterativeSolver1D = vtkKWFrameWithLabel::New();

	this->labelIterationsBeforeRestart1D = vtkKWLabel::New();
	this->entryIterationsBeforeRestart1D = vtkKWEntry::New();

  this->labelConvergenceError1D = vtkKWLabel::New();
	this->entryConvergenceError1D = vtkKWEntry::New();
  
  this->labelkrylov1D = vtkKWLabel::New();
	this->entrykrylov1D = vtkKWEntry::New();
	
	this->framePreconditioning1D = vtkKWFrameWithLabel::New();
	
  this->labelFParam1D = vtkKWLabel::New();
	this->entryFParam1D = vtkKWEntry::New();
  
  this->labelDispTolerance1D = vtkKWLabel::New();
  this->entryDispTolerance1D = vtkKWEntry::New();
  //
  
  //3d solver config SEgregated	  
  this->labelSolverType3D = vtkKWLabel::New();
	this->comboSolverType3D = vtkKWComboBox::New();

  this->labelRLS3D = vtkKWLabel::New();
  this->comboRLS3D = vtkKWComboBox::New();

	this->frameIterativeSolver3D = vtkKWFrameWithLabel::New();

	this->labelIterationsBeforeRestart3D = vtkKWLabel::New();
	this->entryIterationsBeforeRestart3D = vtkKWEntry::New();

  this->labelConvergenceError3D = vtkKWLabel::New();
	this->entryConvergenceError3D = vtkKWEntry::New();
  
  this->labelkrylov3D = vtkKWLabel::New();
	this->entrykrylov3D = vtkKWEntry::New();
	
	this->framePreconditioning3D = vtkKWFrameWithLabel::New();
	
  this->labelFParam3D = vtkKWLabel::New();
	this->entryFParam3D = vtkKWEntry::New();
  
  this->labelDispTolerance3D = vtkKWLabel::New();
  this->entryDispTolerance3D = vtkKWEntry::New();
  
  
	//**************************************************************************
	
	this->ParallelIterativeSolverFrame = vtkKWFrameWithLabel::New();
	
	this->ParallelNumberOfProcessorsLabel = vtkKWLabel::New();
	this->ParallelNumberOfProcessorsEntry = vtkKWEntry::New();
  
  this->ParallelIterationsRestartLabel = vtkKWLabel::New();
  this->ParallelIterationsRestartEntry = vtkKWEntry::New();
  
  this->ParallelRelativeErrorLabel = vtkKWLabel::New();
	this->ParallelRelativeErrorEntry = vtkKWEntry::New();
  
  this->ParallelAbsErrorLabel = vtkKWLabel::New();
  this->ParallelAbsErrorEntry = vtkKWEntry::New();
	
	this->ParallelMaxNumberIterLabel  = vtkKWLabel::New();
  this->ParallelMaxNumberIterEntry = vtkKWEntry::New();
  this->ParallelPrecondLabel  = vtkKWLabel::New();
  this->ParallelPrecondCombo = vtkKWComboBox::New();
	
	
	//*********
	this->GeneralSolverTypeFrame = vtkKWFrameWithLabel::New();
  this->GeneralSolverTypeLabel = vtkKWLabel::New();
  this->GeneralSolverTypeCombo = vtkKWComboBox::New();

	this->labelGeneralRLS  = vtkKWLabel::New();
	this->comboGeneralRLS = vtkKWComboBox::New();
	
	
	this->CompatibilizeLabel =  vtkKWLabel::New();
	this->radioCompatibilizeYes= vtkKWRadioButton::New();
	this->radioCompatibilizeNo= vtkKWRadioButton::New();
	

	this->labelCompatibilizeType =  vtkKWLabel::New();
	this->comboCompatibilizeType = vtkKWComboBox::New();
	
	
	
	this->SelectedSolverFilesRadioButtonSet = vtkKWRadioButtonSet::New();
 	this->SelectedSolverFilesLabel = vtkKWLabel::New();
	
}

//----------------------------------------------------------------------------
vtkPVHMCoupledModelConfigurationWidget::~vtkPVHMCoupledModelConfigurationWidget()
{
  this->Modelconfig1DFrame->Delete();
  this->Modelconfig3DFrame->Delete();

  this->CouplingSchemeLabel->Delete();
  this->CouplingSchemeMonoRadio->Delete();
  this->CouplingSchemeSegregatedRadio->Delete();


	this->SegregationTypeRadioButtonSet->Delete();


  this->CouplingSchemeFrame->Delete();
 
	this->ModelconfigMonolithicIterationsFrame->Delete();
 
	this->labelIter->Delete();
  this->entryIter->Delete();
  this->labelSuPar->Delete();
  this->scaleSuPar->Delete();
 
 
 	this->SegretatedOrderLabel->Delete();
 	this->Segregated1d3dRadio->Delete();
 	this->Segregated3d1dRadio->Delete();
 
  this->SegretatedSchemeTypeLabel->Delete();
  this->SegregatedLinearRadio->Delete();
  this->SegregatedNonLinearRadio->Delete();

 
	this->SegregatedConfigFrame->Delete(); 
	
  this->labelIterNonLinearSegregated->Delete();
  this->entryIterNonLinearSegregated->Delete();
  this->labelSuParNonLinearSegregated->Delete();
  this->scaleSuParNonLinearSegregated->Delete();

	this->AdditionalParamSolver1DFrame->Delete();

	this->AdditionalParamSolver3DFrame->Delete();

	this->SolverConfig1D3DFrame->Delete();

	this->SolverConfigMonoFrame->Delete();
	this->SolverConfigSegregated3DFrame->Delete();
	this->SolverConfigSegregated1DFrame->Delete();
  
  this->SolverConfigGeneralSegregatedFrame->Delete();
  

	// monolithic solver config
	  
	this->frameIterativeSolver->Delete();

	this->labelIterationsBeforeRestart->Delete();
	this->entryIterationsBeforeRestart->Delete();

  this->labelConvergenceError->Delete();
	this->entryConvergenceError->Delete();
  
  this->labelkrylov->Delete();
	this->entrykrylov->Delete();
	
	this->framePreconditioning->Delete();
	
  this->labelFParam->Delete();
	this->entryFParam->Delete();
  
  this->labelDispTolerance->Delete();
  this->entryDispTolerance->Delete();
	
	//**************************************************************************
	
	
	this->FrameScroll->Delete();
	this->WindowFrame->Delete();
	this->notebook->Delete();


 	this->SolverTimeConfigFrame->Delete();
 	
 	this->ConvergenceParametersFrame1D->Delete();
	
 	this->ConvergenceParametersFrame3D->Delete();
	
	
	
	// 1D MC
  this->labelForm->Delete();
  this->radioLSV->Delete();
  this->radioLS->Delete();
	
	this->labelDensity1D->Delete();
  this->entryDensity1D->Delete();

  this->labelViscosity1D->Delete();
  this->entryViscosity1D->Delete();

  this->labelArterialWallLaw1D->Delete();
  this->comboArterialWallLaw1D->Delete();

  this->labelVelocity->Delete();
  this->thumbVel->Delete();
	// FIM
	
		
	//3D mc
	this->labelModel->Delete();
	this->radioCompliant->Delete();
	this->radioRigid->Delete();
	this->labelDensity3D->Delete();
	this->entryDensity3D->Delete();
	this->labelBloodLaw->Delete();
	this->radioBloodLawNewton->Delete();
	this->radioBloodLawCasson->Delete();
	this->labelViscosity3D->Delete();
	this->entryViscosity3D->Delete();
	this->labelAVisco->Delete();
	this->entryAVisco->Delete();
	this->labelLimitStress->Delete();
	this->entryLimitStress->Delete();
	this->labelRegularParameter->Delete();
	this->comboRegularParameter->Delete();
	this->labelArterialWallLaw3D->Delete();
	this->comboArterialWallLaw3D->Delete();
	//
	
	
	
	// time param
	this->ResumeLabel ->Delete();
	this->radioResumeYes->Delete();
	this->radioResumeNo->Delete();
	this->labelNumberOfTimeSteps->Delete();
	this->entryNumberOfTimeSteps->Delete();
	
	this->labelTimeStep->Delete();
	this->entryTimeStep->Delete();
	this->labelInitialTime->Delete();
	this->entryInitialTime->Delete();
	this->labelNumberOfCardiacCycles->Delete();
	this->entryNumberOfCardiacCycles->Delete();
	
	this->labelCardiacCycleTime->Delete();
	this->entryCardiacCycleTime->Delete();
	this->labelFinalTime->Delete();
	this->entryFinalTime->Delete();
	this->CalculateTimeStepButton->Delete();
  //
	
	
	
	//3d conv
	this->labelVariable3D->Delete();
	this->labelRefVal3D->Delete();
	this->labelConEr3D->Delete();

  this->labelVelocityX->Delete();
  this->labelVelocityY->Delete();
  this->labelVelocityZ->Delete();
  this->labelDisplacementX->Delete();
  this->labelDisplacementY->Delete();
  this->labelDisplacementZ->Delete();
  
  this->labelPressure3D->Delete();
  this->entryRefPressure3D->Delete();
  this->entryConvPressure3D->Delete();
  
  
  this->entryRefVelocityX->Delete();
  this->entryRefVelocityY->Delete();
  this->entryRefVelocityZ->Delete();
  this->entryRefDisplacementX->Delete();
  this->entryRefDisplacementY->Delete();
  this->entryRefDisplacementZ->Delete();
  
  this->entryConvVelocityX->Delete(); 
  this->entryConvVelocityY->Delete();
  this->entryConvVelocityZ->Delete();
  this->entryConvDisplacementX->Delete();
  this->entryConvDisplacementY->Delete();
  this->entryConvDisplacementZ->Delete();
  
  
  this->labelIter3D->Delete();
  this->entryIter3D->Delete();
  this->labelSuPar3D->Delete();
  this->scaleSuPar3D->Delete();
	//**********************************************	
	
	
	
	// 1d conv
	this->labelVariable1D->Delete();
	this->labelRefVal1D->Delete();
	this->labelConEr1D->Delete();
	
	this->labelPress->Delete();
	this->entryRePr->Delete();
	this->entryCePr->Delete();

	this->labelFlux->Delete();
	this->entryReFl->Delete();
	this->entryCeFl->Delete();


	this->labelElPre->Delete();
	this->entryReEP->Delete();
	this->entryCeEP->Delete();

	this->labelArea->Delete();
	this->entryReAr->Delete();
	this->entryCeAr->Delete();


  this->labelIter1D->Delete();
  this->entryIter1D->Delete();
  this->labelSuPar1D->Delete();
  this->scaleSuPar1D->Delete();
	//**********************************************	
	
	
	// 1d solver param
	
	this->labelTethaScheme1D->Delete();
	this->thumbTheta1D->Delete();
	
	this->labelAditi1D->Delete();
	this->radioYes1D->Delete();
	this->radioNo1D->Delete();
	
	this->labelPena1D->Delete();
	this->entryPena1D->Delete();
	
	//	
	
	
	//3d solver param
	
	this->labelTethaScheme3D->Delete();
	this->thumbTheta3D->Delete();
	
	this->TetraCompressibilityLabel->Delete();
	this->TetraCompressibilityEntry->Delete();
	this->labelAditi3D->Delete();
	this->radioYes3D->Delete();
	this->radioNo3D->Delete();
	
	this->labelPena3D->Delete();
	this->entryPena3D->Delete();
	
	this->labelIncrementalVersion->Delete();
	this->radioICYes->Delete();
	this->radioICNo->Delete();
	//	
	
	
	// misc
	this->ScaleFactor3DLabel->Delete();
	this->ScaleFactor3DEntry->Delete();
	
	this->labelRenu->Delete();
	this->entryRenu->Delete();
	  
	this->labelFileOutput->Delete();
	
	this->entryFileOutput->Delete();
	
	this->labelScreenOutput->Delete();
	
	this->entryScreenOutput->Delete();
	this->labelLogFile->Delete();
	
	this->entryLogFile->Delete();
	this->labelFilesPath->Delete();
	this->entryFilesPath->Delete();
	this->pushButtonBrowse->Delete();
	this->CommitChangesButtonPage1->Delete();
	this->textFilesGenerationStatus->Delete();

	this->ChooseDirectoryDialog->Delete();

	//
	
	
	//1D solver config	SEgregated  
  this->labelSolverType1D->Delete();
	this->comboSolverType1D->Delete();

  this->labelRLS1D->Delete();
  this->comboRLS1D->Delete();

	this->frameIterativeSolver1D->Delete();

	this->labelIterationsBeforeRestart1D->Delete();
	this->entryIterationsBeforeRestart1D->Delete();

  this->labelConvergenceError1D->Delete();
	this->entryConvergenceError1D->Delete();
  
  this->labelkrylov1D->Delete();
	this->entrykrylov1D->Delete();
	
	this->framePreconditioning1D->Delete();
	
  this->labelFParam1D->Delete();
	this->entryFParam1D->Delete();
  
  this->labelDispTolerance1D->Delete();
  this->entryDispTolerance1D->Delete();
	//  
  
  
  
  
  //3d solver config SEgregated	  
  this->labelSolverType3D->Delete();
	this->comboSolverType3D->Delete();

  this->labelRLS3D->Delete();
  this->comboRLS3D->Delete();

	this->frameIterativeSolver3D->Delete();

	this->labelIterationsBeforeRestart3D->Delete();
	this->entryIterationsBeforeRestart3D->Delete();

  this->labelConvergenceError3D->Delete();
	this->entryConvergenceError3D->Delete();
  
  this->labelkrylov3D->Delete();
	this->entrykrylov3D->Delete();
	
	this->framePreconditioning3D->Delete();
	
  this->labelFParam3D->Delete();
	this->entryFParam3D->Delete();
  
  this->labelDispTolerance3D->Delete();
  this->entryDispTolerance3D->Delete();
  
	//**************************************************************************
	
	this->ParallelIterativeSolverFrame->Delete();
	
	this->ParallelNumberOfProcessorsLabel->Delete();
	this->ParallelNumberOfProcessorsEntry->Delete();
	  
  this->ParallelIterationsRestartLabel->Delete();
  this->ParallelIterationsRestartEntry->Delete();
  
  this->ParallelRelativeErrorLabel->Delete();
	this->ParallelRelativeErrorEntry->Delete();
  
  this->ParallelAbsErrorLabel->Delete();
  this->ParallelAbsErrorEntry->Delete();
  
  
  this->ParallelMaxNumberIterLabel->Delete();
  this->ParallelMaxNumberIterEntry->Delete();
  this->ParallelPrecondLabel->Delete();
  this->ParallelPrecondCombo->Delete();
    
	//*****************
	this->GeneralSolverTypeFrame->Delete();
  this->GeneralSolverTypeLabel->Delete();
  this->GeneralSolverTypeCombo->Delete();

	this->labelGeneralRLS->Delete();
	this->comboGeneralRLS->Delete();
	
	this->CompatibilizeLabel->Delete();
	this->radioCompatibilizeYes->Delete();
	this->radioCompatibilizeNo->Delete();
	

	this->labelCompatibilizeType->Delete();
	this->comboCompatibilizeType->Delete();
	
	
	this->SelectedSolverFilesRadioButtonSet->Delete();
 
 	this->SelectedSolverFilesLabel->Delete();
}



//----------------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::Create(vtkKWApplication *app) 
{

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
	this->Superclass::Create(app);
	
  this->SetDisplayPositionToScreenCenterFirst();
	
	
	
  //char buffer[1024];
  this->WindowFrame->SetParent(this);
  this->WindowFrame->Create(this->GetPVApplication());
//  this->Script("pack %s -anchor n -side top -fill x -expand t",
//                this->WindowFrame->GetWidgetName());
  
  this->FrameScroll->SetParent(this);
  this->FrameScroll->Create(this->GetPVApplication());

  this->Script("pack %s -pady 2 -fill both -expand yes -anchor n",
               this->FrameScroll->GetWidgetName());
  
  this->Script("pack %s -anchor n -fill both -expand yes",
                this->WindowFrame->GetWidgetName());
                
                
//                  this->Script("pack %s -anchor n -side top -fill x -expand t",
//                this->WindowFrame->GetWidgetName());
  
  this->notebook->SetParent(this->FrameScroll->GetFrame());
  this->notebook->SetMinimumHeight(900);
  this->notebook->Create(this->GetPVApplication());
  this->notebook->AlwaysShowTabsOn();
  //this->Script("pack %s -side top -anchor nw -expand y -fill both -padx 2 -pady 2", this->notebook->GetWidgetName());

  this->Script("pack %s -pady 2 -padx 2 -fill both -expand yes -anchor n",
               this->notebook->GetWidgetName());

  int page1 = this->notebook->AddPage("Model Configuration", "Model Configuration", NULL, 1);
  int page2 = this->notebook->AddPage("General Configurations", "General Configurations", NULL, 2);
  int page3 = this->notebook->AddPage("Solver Configurations", "Solver Configurations", NULL, 3);
  int page4 = this->notebook->AddPage("IO Options", "IO Options", NULL, 4);

  
	this->ChooseDirectoryDialog->Create(app);

  this->mc = this->notebook->GetFrame("Model Configuration");
  this->gc = this->notebook->GetFrame("General Configurations");
  this->sc = this->notebook->GetFrame("Solver Configurations");
  this->mi = this->notebook->GetFrame("IO Options");

  //*******************************************************************************************
  // MODEL CONFIG TAB Frames *************************************
  
  // model config 1d Frame
  this->Modelconfig1DFrame->SetParent(this->mc);
  this->Modelconfig1DFrame->Create(this->GetPVApplication());
  this->Modelconfig1DFrame->SetLabelText("1D Model Config");
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -stick we", this->Modelconfig1DFrame->GetWidgetName());
  
  // model config 3d Frame
  this->Modelconfig3DFrame->SetParent(this->mc);
  this->Modelconfig3DFrame->Create(this->GetPVApplication());
  this->Modelconfig3DFrame->SetLabelText("3D Model Config");
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -stick we", this->Modelconfig3DFrame->GetWidgetName());
  
  //*******************************************************************************************
  // GENERAL CONFIG TAB Frames ********************************************
  
  
  // Time Configuration FRAME
  this->SolverTimeConfigFrame->SetParent(this->gc);
  this->SolverTimeConfigFrame->Create(this->GetPVApplication());
  this->SolverTimeConfigFrame->SetLabelText("Time Configuration");
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -stick we", this->SolverTimeConfigFrame->GetWidgetName());
  
  // Coupling type Option FRAME
  this->CouplingSchemeFrame->SetParent(this->gc);
  this->CouplingSchemeFrame->Create(this->GetPVApplication());
  this->CouplingSchemeFrame->SetLabelText("Coupling Type Option");
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -stick we", this->CouplingSchemeFrame->GetWidgetName());
  
  
  
  // 3D Convergence Param FRAME
  this->ConvergenceParametersFrame3D->SetParent(this->gc);
  this->ConvergenceParametersFrame3D->Create(this->GetPVApplication());
  this->ConvergenceParametersFrame3D->SetLabelText("3D Convergence Parameters");
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -stick we", this->ConvergenceParametersFrame3D->GetWidgetName());
  
  // 1D Convergence Param FRAME
  this->ConvergenceParametersFrame1D->SetParent(this->gc);
  this->ConvergenceParametersFrame1D->Create(this->GetPVApplication());
  this->ConvergenceParametersFrame1D->SetLabelText("1D Convergence Parameters");
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -stick we", this->ConvergenceParametersFrame1D->GetWidgetName());
  
  // Iteration and Subrelaxation Parameters 
  this->ModelconfigMonolithicIterationsFrame->SetParent(this->gc);
  this->ModelconfigMonolithicIterationsFrame->Create(this->GetPVApplication());
  this->ModelconfigMonolithicIterationsFrame->SetLabelText("Iterations Option");
  
  this->labelIter->SetParent(this->ModelconfigMonolithicIterationsFrame->GetFrame());
  this->labelIter->Create(app);
  this->labelIter->SetText("Iterations");

  this->entryIter->SetParent(this->ModelconfigMonolithicIterationsFrame->GetFrame());
  this->entryIter->Create(app);
  this->entryIter->SetBalloonHelpString("Maximum number of iterations without convergence");  
  this->entryIter->SetValueAsInt(100);
  this->entryIter->SetWidth(6);

  this->labelSuPar->SetParent(this->ModelconfigMonolithicIterationsFrame->GetFrame());
  this->labelSuPar->Create(app);
  this->labelSuPar->SetText("Subrelaxation\nParameter");

  this->scaleSuPar->SetParent(this->ModelconfigMonolithicIterationsFrame->GetFrame());
  this->scaleSuPar->Create(app);
  this->scaleSuPar->SetRange(0.0, 1.0);
  this->scaleSuPar->SetResolution(0.1);
  this->scaleSuPar->SetEntryPositionToRight();
  this->scaleSuPar->SetBalloonHelpString("Subrelaxation of solution between iterations");
  this->scaleSuPar->SetValue(0.8);
  
  
  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter->GetWidgetName());
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar->GetWidgetName());
	
  
  //*******************************************************************************************
  // SOLVER CONFIG TAB Frames ********************************************
  
  this->SolverConfig1D3DFrame->SetParent(this->sc);
  this->SolverConfig1D3DFrame->Create(this->GetPVApplication());
  this->SolverConfig1D3DFrame->SetLabelText("General Solver Parameters");
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -stick we -columnspan 5", this->SolverConfig1D3DFrame->GetWidgetName());

  
  // Frame Additional Parameters 1D e 3D - Solver Configuration TAB
  
  
  this->AdditionalParamSolver1DFrame->SetParent(this->sc);
  this->AdditionalParamSolver1DFrame->Create(this->GetPVApplication());
  this->AdditionalParamSolver1DFrame->SetLabelText("1D Solver Parameters");
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -stick wens", this->AdditionalParamSolver1DFrame->GetWidgetName());

  this->AdditionalParamSolver3DFrame->SetParent(this->sc);
  this->AdditionalParamSolver3DFrame->Create(this->GetPVApplication());
  this->AdditionalParamSolver3DFrame->SetLabelText("3D Solver Parameters");
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -stick we", this->AdditionalParamSolver3DFrame->GetWidgetName());
  
  // visualizado somente se tipo de acoplamento é monolitico
  this->SolverConfigMonoFrame->SetParent(this->sc);
  this->SolverConfigMonoFrame->Create(this->GetPVApplication());
  this->SolverConfigMonoFrame->SetLabelText("Monolithic Solver Configuration");
  // grid é feito no metodo UpdateMonolithicWidgets


  // Visualizado somente se o tipo de acoplamento é segregated
  this->SolverConfigGeneralSegregatedFrame->SetParent(this->sc);
  this->SolverConfigGeneralSegregatedFrame->Create(this->GetPVApplication());
  this->SolverConfigGeneralSegregatedFrame->SetLabelText("Segregated Solver Configuration");
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->SolverConfigGeneralSegregatedFrame->GetWidgetName());



  //this->GeneralSolverTypeFrame->SetParent(this->SolverConfigGeneralSegregatedFrame->GetFrame());
  
  this->GeneralSolverTypeFrame->SetParent(this->sc);
  this->GeneralSolverTypeFrame->Create(this->GetPVApplication());
  this->GeneralSolverTypeFrame->SetLabelText("Solver Type");
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -stick we -columnspan 5", this->GeneralSolverTypeFrame->GetWidgetName());
  
  
  this->GeneralSolverTypeLabel->SetParent(this->GeneralSolverTypeFrame->GetFrame());
  this->GeneralSolverTypeLabel->Create(app);
  //this->GeneralSolverTypeLabel->SetWidgetName(this->labelSolverType->GetWidgetName());
  this->GeneralSolverTypeLabel->SetText("Solver Type");

  this->GeneralSolverTypeCombo->SetParent(this->GeneralSolverTypeFrame->GetFrame());
  this->GeneralSolverTypeCombo->Create(app);
	this->GeneralSolverTypeCombo->ReadOnlyOn();
 	//this->GeneralSolverTypeCombo->SetBalloonHelpString("");
  this->GeneralSolverTypeCombo->AddValue("Sequential");
  this->GeneralSolverTypeCombo->AddValue("Parallel");
  this->GeneralSolverTypeCombo->SetValue("Sequential");
  this->GeneralSolverTypeCombo->SetCommand(this, "UpdateSolverOptions");
  
  
  
  this->labelGeneralRLS->SetParent(this->GeneralSolverTypeFrame->GetFrame());
  this->labelGeneralRLS->Create(app);
  //this->labelGeneralRLS->SetWidgetName(this->labelSolverType->GetWidgetName());
  this->labelGeneralRLS->SetText("Resolution of Linear System");

  this->comboGeneralRLS->SetParent(this->GeneralSolverTypeFrame->GetFrame());
  this->comboGeneralRLS->Create(app);
	this->comboGeneralRLS->ReadOnlyOn();
 	//this->comboGeneralRLS->SetBalloonHelpString("");
//  this->comboGeneralRLS->AddValue("Sequential");
//  this->comboGeneralRLS->AddValue("Parallel");
//  this->comboGeneralRLS->SetValue("Sequential");
  this->comboGeneralRLS->SetCommand(this, "UpdateRLSParallel");
  

  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -stick we", this->GeneralSolverTypeLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -stick we", this->GeneralSolverTypeCombo->GetWidgetName());
  
  //this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelGeneralRLS->GetWidgetName());
  //this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->comboGeneralRLS->GetWidgetName());
  
  
  
  // Visualizado somente se o tipo de acoplamento é segregated
  this->SolverConfigSegregated1DFrame->SetParent(this->SolverConfigGeneralSegregatedFrame->GetFrame());
  this->SolverConfigSegregated1DFrame->Create(this->GetPVApplication());
  this->SolverConfigSegregated1DFrame->SetLabelText("1D Solver Configuration");
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->SolverConfigSegregated1DFrame->GetWidgetName());
  
  // Visualizado somente se o tipo de acoplamento é segregated
  this->SolverConfigSegregated3DFrame->SetParent(this->SolverConfigGeneralSegregatedFrame->GetFrame());
  this->SolverConfigSegregated3DFrame->Create(this->GetPVApplication());
  this->SolverConfigSegregated3DFrame->SetLabelText("3D Solver Configuration");
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->SolverConfigSegregated3DFrame->GetWidgetName());

 
  //*******************************************************************************************
  
 // Segragetd options - so aparece quando é coupling = segregated
  this->SegregatedConfigFrame->SetParent(this->gc);
  this->SegregatedConfigFrame->Create(this->GetPVApplication());
  this->SegregatedConfigFrame->SetLabelText("Segregated Scheme Configuration ");

	this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we", this->SegregatedConfigFrame->GetWidgetName());


  this->CouplingSchemeLabel->SetParent(this->CouplingSchemeFrame->GetFrame());
  this->CouplingSchemeLabel->Create(app);
  this->CouplingSchemeLabel->SetText("Coupling Scheme");
  
  
  this->SegregationTypeRadioButtonSet->SetParent(this->CouplingSchemeFrame->GetFrame());
  this->SegregationTypeRadioButtonSet->Create(app);

	vtkKWRadioButton *Radio = this->SegregationTypeRadioButtonSet->AddWidget(0);
	Radio->SetText("Monolithic Scheme");
	Radio->SetCommand(this,"UpdateMonolithicWidgetsCallback");
  
	Radio = this->SegregationTypeRadioButtonSet->AddWidget(1);
	Radio->SetText("Normal Segregated");
	Radio->SetCommand(this, "UpdateSegregatedWidgetsCallback");

	Radio = this->SegregationTypeRadioButtonSet->AddWidget(2);
	Radio->SetText("Super Segregated");
	Radio->SetSelectedState(1);
	Radio->SetCommand(this, "UpdateSegregatedWidgetsCallback");

  this->CouplingSchemeMonoRadio->SetParent(this->CouplingSchemeFrame->GetFrame());
  this->CouplingSchemeMonoRadio->Create(app);
  this->CouplingSchemeMonoRadio->SetText("Monolithic scheme");
  this->CouplingSchemeMonoRadio->SetValue("Yes");
  
  this->CouplingSchemeSegregatedRadio->SetParent(this->CouplingSchemeFrame->GetFrame());
  this->CouplingSchemeSegregatedRadio->Create(app);
  this->CouplingSchemeSegregatedRadio->SetText("Segregated scheme");
  this->CouplingSchemeSegregatedRadio->SetValue("No");
  
  this->CouplingSchemeMonoRadio->SetVariableName(this->CouplingSchemeSegregatedRadio->GetVariableName());
  this->CouplingSchemeMonoRadio->SetCommand(this,"UpdateMonolithicWidgetsCallback");
  this->CouplingSchemeSegregatedRadio->SetCommand(this, "UpdateSegregatedWidgetsCallback");
  
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->CouplingSchemeLabel->GetWidgetName());
	this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->SegregationTypeRadioButtonSet->GetWidgetName());
 	//this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->CouplingSchemeSegregatedRadio->GetWidgetName());
 	
 	
  // KWComponents used when we have a segregated scheme selected
  

  
  this->SegretatedOrderLabel->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->SegretatedOrderLabel->Create(app);
  this->SegretatedOrderLabel->SetText("Segregation Order");
  
  this->Segregated1d3dRadio->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->Segregated1d3dRadio->Create(app);
  this->Segregated1d3dRadio->SetText("1D-3D");
  this->Segregated1d3dRadio->SetValue("Yes");
  
  this->Segregated3d1dRadio->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->Segregated3d1dRadio->Create(app);
  this->Segregated3d1dRadio->SetText("3D-1D");
  this->Segregated3d1dRadio->SetValue("No");
  
  this->Segregated1d3dRadio->SetVariableName(this->Segregated3d1dRadio->GetVariableName());
 	
  
  this->SegretatedSchemeTypeLabel->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->SegretatedSchemeTypeLabel->Create(app);
  this->SegretatedSchemeTypeLabel->SetText("Scheme");
  
  this->SegregatedLinearRadio->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->SegregatedLinearRadio->Create(app);
  this->SegregatedLinearRadio->SetText("Linear");
  this->SegregatedLinearRadio->SetValue("Yes");
  
  this->SegregatedNonLinearRadio->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->SegregatedNonLinearRadio->Create(app);
  this->SegregatedNonLinearRadio->SetText("Non-Linear");
  this->SegregatedNonLinearRadio->SetValue("No");
  
  this->SegregatedLinearRadio->SetVariableName(this->SegregatedNonLinearRadio->GetVariableName());
 	
 	
 	this->SegregatedLinearRadio->SetCommand(this, "UpdateLinearSegregatedWidgetsCallback");
 	this->SegregatedNonLinearRadio->SetCommand(this, "UpdateNonLinearSegregatedWidgetsCallback");
 	
 	
	this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->SegretatedOrderLabel->GetWidgetName());
	this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->Segregated1d3dRadio->GetWidgetName());
 	this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->Segregated3d1dRadio->GetWidgetName());
	this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->SegretatedSchemeTypeLabel->GetWidgetName());
	this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->SegregatedLinearRadio->GetWidgetName());
 	this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->SegregatedNonLinearRadio->GetWidgetName());
 	
 	
 	this->labelIterNonLinearSegregated->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->labelIterNonLinearSegregated->Create(app);
  this->labelIterNonLinearSegregated->SetText("Iterations");
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelIterNonLinearSegregated->GetWidgetName());

  this->entryIterNonLinearSegregated->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->entryIterNonLinearSegregated->Create(app);
  this->entryIterNonLinearSegregated->SetBalloonHelpString("Maximum number of iterations without convergence");  
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2",this->entryIterNonLinearSegregated->GetWidgetName());
  this->entryIterNonLinearSegregated->SetValueAsInt(100);
  this->entryIterNonLinearSegregated->SetWidth(6);

  this->labelSuParNonLinearSegregated->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->labelSuParNonLinearSegregated->Create(app);
  this->labelSuParNonLinearSegregated->SetText("Subrelaxation\nParameter");
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2", this->labelSuParNonLinearSegregated->GetWidgetName());

  this->scaleSuParNonLinearSegregated->SetParent(this->SegregatedConfigFrame->GetFrame());
  this->scaleSuParNonLinearSegregated->Create(app);
  this->scaleSuParNonLinearSegregated->SetRange(0.0, 1.0);
  this->scaleSuParNonLinearSegregated->SetResolution(0.1);
  this->scaleSuParNonLinearSegregated->SetEntryPositionToRight();
  this->scaleSuParNonLinearSegregated->SetBalloonHelpString("Subrelaxation of solution between iterations");
  this->Script("grid %s -row 5 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuParNonLinearSegregated->GetWidgetName());
  this->scaleSuParNonLinearSegregated->SetValue(0.8);

	this->CreateModelConfig1D(app); 
	this->CreateModelConfig3D(app);
	
	this->CreateTimeComponents(app);	
	this->CreateConvParam3D(app); 
	this->CreateConvParam1D(app);
	
	this->CreateSolverConfig1D3D(app);
	this->CreateSolverAdditionalConfig1D(app); 
	this->CreateSolverAdditionalConfig3D(app); 
	
	this->CreateMonolithicSolverConfig(app); 
	
	this->CreateSegregated3DSolverConfig(app); 
	this->CreateSegregated1DSolverConfig(app);
	this->CreateParallelSolverConfig(app);
	this->CreateMiscParam(app); 
	
	this->CreateComponentsInteraction();
	
	
	// como Blood constitutive law tem o valor Newton como padrao
	this->UpdateNewtonRelatedWidgets();
	
} 
  
//----------------------------------------------------------------------------
  
void vtkPVHMCoupledModelConfigurationWidget::CreateModelConfig1D(vtkKWApplication *app) 
{ 
   //////////////////////////////////////////////
  //page 1  ******* Model Configuration 1D
  //*********************************************************
  //*********************************************************
  
  //char buffer[1024];
  
  this->labelForm->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->labelForm->Create(app);
  this->labelForm->SetText("Formulation");
  
  this->radioLS->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->radioLS->Create(app);
  this->radioLS->SetText("Least-Squares");
  this->radioLS->SetBalloonHelpString("Variational formulation for solving the problem");
  this->radioLS->SetValue("LW");
  
  this->radioLSV->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->radioLSV->Create(app);
  this->radioLSV->SetText("Least-Squares - Viscoelastic");
  this->radioLSV->SetBalloonHelpString("Variational formulation for solving the problem");

  this->radioLSV->SetValue("LS");
  
  this->radioLSV->SetVariableName(this->radioLS->GetVariableName());
  this->radioLSV->SelectedStateOn();
  
  this->labelDensity1D->SetParent(this->Modelconfig1DFrame->GetFrame());  // General Tree Parameters
  this->labelDensity1D->Create(app);
  this->labelDensity1D->SetText("Density [g/cm^3]");
  
  this->entryDensity1D->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->entryDensity1D->Create(app);
  this->entryDensity1D->SetBalloonHelpString("Density of blood.");
  this->entryDensity1D->SetValueAsDouble(1.04);
  this->entryDensity1D->SetWidth(6);
  
  
  this->labelViscosity1D->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->labelViscosity1D->Create(app);
  this->labelViscosity1D->SetText("Viscosity [cpoise]");
  
  this->entryViscosity1D->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->entryViscosity1D->Create(app);
  this->entryViscosity1D->SetBalloonHelpString("Dynamic viscosidade of blood.");
  this->entryViscosity1D->SetValueAsDouble(0.04);
  this->entryViscosity1D->SetWidth(6);
  
  
  this->labelArterialWallLaw1D->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->labelArterialWallLaw1D->Create(app);
  this->labelArterialWallLaw1D->SetText("Arterial Wall Law");
  
  
  this->comboArterialWallLaw1D->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->comboArterialWallLaw1D->Create(app);
  this->comboArterialWallLaw1D->SetBalloonHelpString("Constitutive behavior for the arterial wall.");
	this->comboArterialWallLaw1D->ReadOnlyOn();


  // Velocity profile	-> 4
  this->labelVelocity->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->labelVelocity->Create(app);
  this->labelVelocity->SetText("Velocity Profile");

  this->thumbVel->SetParent(this->Modelconfig1DFrame->GetFrame());
  this->thumbVel->Create(app);
  this->thumbVel->SetRange(1.0, 1.33);
 	this->thumbVel->SetMinimumValue(1.0);
  this->thumbVel->SetMaximumValue(1.33);
  this->thumbVel->ClampMinimumValueOn();
  this->thumbVel->ClampMaximumValueOn();
  this->thumbVel->SetResolution(0.025);
  this->thumbVel->DisplayEntryOn();
  this->thumbVel->DisplayEntryAndLabelOnTopOff();
  this->thumbVel->SetValue(1.0);
  this->thumbVel->SetBalloonHelpString("Coefficient accounting for different velocities. (1: flat / 1.33: parabolic)");
  
  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->labelForm->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->radioLS->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->radioLSV->GetWidgetName());
	
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelDensity1D->GetWidgetName());
	this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entryDensity1D->GetWidgetName());
		
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelViscosity1D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->entryViscosity1D->GetWidgetName());
		
	this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->labelArterialWallLaw1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->comboArterialWallLaw1D->GetWidgetName());

  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelVelocity->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->thumbVel->GetWidgetName());
	  
	  
	const char *ArterialWallLawOpt[] = { "No Linear elastin-collagen", "Linear elastin-collagen", "Generic" };
  for (int i = 0; i < 3; i++)
		this->comboArterialWallLaw1D->AddValue(ArterialWallLawOpt[i]);
	this->comboArterialWallLaw1D->SetValue("Linear elastin-collagen");
  
}  
  
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
  
void vtkPVHMCoupledModelConfigurationWidget::CreateModelConfig3D(vtkKWApplication *app) 
{ 
  //////////////////////////////////////////////
  //page 1  ******* Model Configuration 3D
  //*********************************************************
  //*********************************************************
  
	// como estes dois entries tem relacao com o combobox que define o tipo de formulation, 
 	// estes devem ser criados aqui para que se possa estabelecer a relacao com componentes já criadods 

  this->labelDensity3D->SetParent(this->Modelconfig3DFrame->GetFrame());  // General Tree Parameters
  this->labelDensity3D->Create(app);
  this->labelDensity3D->SetText("Density [g/cm^3]");
  
  this->entryDensity3D->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->entryDensity3D->Create(app);
  this->entryDensity3D->SetBalloonHelpString("Density of blood.");
  this->entryDensity3D->SetValueAsDouble(1.04);
  this->entryDensity3D->SetWidth(6);
  
  
  this->labelViscosity3D->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->labelViscosity3D->Create(app);
  this->labelViscosity3D->SetText("Viscosity [cpoise]");
  
  this->entryViscosity3D->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->entryViscosity3D->Create(app);
  this->entryViscosity3D->SetBalloonHelpString("Dynamic viscosidade of blood.");
  this->entryViscosity3D->SetValueAsDouble(0.04);
  this->entryViscosity3D->SetWidth(6);
  
  
  
  this->labelArterialWallLaw3D->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->labelArterialWallLaw3D->Create(app);
  this->labelArterialWallLaw3D->SetText("Arterial Wall Law");
  
  
  this->comboArterialWallLaw3D->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->comboArterialWallLaw3D->Create(app);
  this->comboArterialWallLaw3D->SetBalloonHelpString("Constitutive behavior for the arterial wall.");
	this->comboArterialWallLaw3D->ReadOnlyOn();

  this->labelModel->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->labelModel->Create(app);
  this->labelModel->SetText("Model");
  this->radioCompliant->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->radioCompliant->Create(app);
  this->radioCompliant->SetText("Compliant");
  this->radioCompliant->SetValue("Yes");
  
  
  this->radioRigid->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->radioRigid->Create(app);
  this->radioRigid->SetText("Rigid");
  this->radioRigid->SetValue("No");
  
  this->radioCompliant->SetVariableName(this->radioRigid->GetVariableName());
  this->radioCompliant->SelectedStateOn();

  this->labelBloodLaw->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->labelBloodLaw->Create(app);
  this->labelBloodLaw->SetText("Blood Constitutive Law");
  
  this->radioBloodLawNewton->SetParent(this->Modelconfig3DFrame->GetFrame());
  this->radioBloodLawNewton->Create(app);
  this->radioBloodLawNewton->SetText("Newton");
  this->radioBloodLawNewton->SetValue("Newton");
  
	
	this->radioBloodLawCasson->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->radioBloodLawCasson->Create(app);
	this->radioBloodLawCasson->SetText("Casson");
	this->radioBloodLawCasson->SetValue("Casson");
	  
	this->radioBloodLawNewton->SetVariableName(this->radioBloodLawCasson->GetVariableName());
	
	this->radioBloodLawNewton->SelectedStateOn();
	
	this->labelAVisco->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->labelAVisco->Create(app);
	this->labelAVisco->SetText("Asymptotic Viscosity [cpoise]");
	 
	this->entryAVisco->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->entryAVisco->Create(app);
	this->entryAVisco->SetBalloonHelpString("");
	this->entryAVisco->SetValueAsDouble(0.04);
	this->entryAVisco->SetWidth(6);
	this->entryAVisco->ReadOnlyOn();
	  
	this->labelLimitStress->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->labelLimitStress->Create(app);
	this->labelLimitStress->SetText("Limit Stress [dyn/cm^2]");
	
	this->entryLimitStress->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->entryLimitStress->Create(app);
	this->entryLimitStress->SetBalloonHelpString("");
	this->entryLimitStress->SetValueAsDouble(0.038);
	this->entryLimitStress->SetWidth(6);
	this->entryLimitStress->ReadOnlyOn();
	  
	
	this->labelRegularParameter->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->labelRegularParameter->Create(app);
	this->labelRegularParameter->SetText("Regularization Parameters");
	
	  
	this->comboRegularParameter->SetParent(this->Modelconfig3DFrame->GetFrame());
	this->comboRegularParameter->Create(app);
	this->comboRegularParameter->SetWidth(8);
	this->comboRegularParameter->ReadOnlyOn();
  //this->comboRegularParameter->SetBalloonHelpString("Blood Constitutive Law.");

  const char *RegularParameterOpt3D[] = { "0.1", "0.01", "0.001", "0.0001", "0.00001", "0.000001" };
  for (int i = 0; i < 6; i++)
		this->comboRegularParameter->AddValue(RegularParameterOpt3D[i]);
	this->comboRegularParameter->SetValue("0.1");

	this->radioBloodLawCasson->SetCommand(this, "UpdateCassonRelatedWidgets");
	this->radioBloodLawNewton->SetCommand(this, "UpdateNewtonRelatedWidgets");
  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelModel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->radioCompliant->GetWidgetName());
 	this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->radioRigid->GetWidgetName());

  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelDensity3D->GetWidgetName());
	this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entryDensity3D->GetWidgetName());

  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelBloodLaw->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->radioBloodLawNewton->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->radioBloodLawCasson->GetWidgetName());
 	  
 
 	this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelViscosity3D->GetWidgetName());
	this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->entryViscosity3D->GetWidgetName());
	
  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->labelAVisco->GetWidgetName());
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2 -sticky w", this->entryAVisco->GetWidgetName());
	
  this->Script("grid %s -row 7 -column 0 -padx 2 -pady 2 -sticky w", this->labelLimitStress->GetWidgetName());
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2 -sticky w", this->entryLimitStress->GetWidgetName());

  this->Script("grid %s -row 8 -column 0 -padx 2 -pady 2 -sticky w", this->labelRegularParameter->GetWidgetName());
  this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2 -sticky w", this->comboRegularParameter->GetWidgetName());
  
	this->Script("grid %s -row 9 -column 0 -padx 2 -pady 2 -sticky w", this->labelArterialWallLaw3D->GetWidgetName());
  this->Script("grid %s -row 9 -column 1 -padx 2 -pady 2 -sticky w", this->comboArterialWallLaw3D->GetWidgetName());
  
  
 	const char *ArterialWallLawOpt3D[] = { "Elastic", "Viscoelastic" };
	for (int i = 0; i < 2; i++)
		this->comboArterialWallLaw3D->AddValue(ArterialWallLawOpt3D[i]);

	this->comboArterialWallLaw3D->SetValue("Viscoelastic");
    
}	


// -------------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::CreateTimeComponents(vtkKWApplication *app) 
{
  char buffer[1024];
  
  
  this->entryInitialTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryInitialTime->Create(app);
  this->entryInitialTime->SetBalloonHelpString("Simulation initial time");  
  this->entryInitialTime->SetValueAsDouble(0.00);
  this->entryInitialTime->SetWidth(6);
  
  
  this->ResumeLabel->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->ResumeLabel->Create(app);
  this->ResumeLabel->SetText("Resume from Previous Results");
  
  this->radioResumeYes->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->radioResumeYes->Create(app);
  this->radioResumeYes->SetText("Yes");
  this->radioResumeYes->SetValue("Yes");
   
  this->radioResumeNo->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->radioResumeNo->Create(app);
  this->radioResumeNo->SetText("No");
  this->radioResumeNo->SetValue("No");
  
	sprintf(buffer, "%s config -state normal", this->entryInitialTime->GetWidgetName());
	//this->radioResumeNo->SetCommand(NULL, buffer);
 
	this->radioResumeNo->SetCommand(this, "EnableInitialTimeStatus");
  
  this->radioResumeNo->SetSelectedState(1);
  this->radioResumeYes->SetVariableName(this->radioResumeNo->GetVariableName());
  
  this->labelNumberOfTimeSteps->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelNumberOfTimeSteps->Create(app);
  this->labelNumberOfTimeSteps->SetText("Number of Time Steps ");
  this->labelNumberOfTimeSteps->SetBalloonHelpString("Number of Time Steps");  
  
  this->entryNumberOfTimeSteps->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryNumberOfTimeSteps->Create(app);
  this->entryNumberOfTimeSteps->SetValueAsDouble(320);
  this->entryNumberOfTimeSteps->SetWidth(6);
  
  this->labelTimeStep->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelTimeStep->Create(app);
  this->labelTimeStep->SetText("Time Step [s]");
  this->labelTimeStep->SetBalloonHelpString("Time Step");  
  
  
  this->entryTimeStep->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryTimeStep->Create(app);
  this->entryTimeStep->SetBalloonHelpString("Time step for finite differences scheme in time discretization");  
  // this->entryTimeStep->SetValueAsDouble((this->GetOriginalFinalTime() -  this->GetInitialTime())/this->entryNumberOfTimeSteps->GetValueAsInt());
  this->entryTimeStep->SetWidth(6);
  
  
  this->labelInitialTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelInitialTime->Create(app);
  this->labelInitialTime->SetText("Initial Time [s]");
  
  
  this->labelNumberOfCardiacCycles->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelNumberOfCardiacCycles->Create(app);
  this->labelNumberOfCardiacCycles->SetText("Number of Cardiac Cycles ");
  this->labelNumberOfCardiacCycles->SetBalloonHelpString("Number of Cardiac Cycles ");  
  
  this->entryNumberOfCardiacCycles->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryNumberOfCardiacCycles->Create(app);
  this->entryNumberOfCardiacCycles->SetValueAsInt(1);
  this->entryNumberOfCardiacCycles->SetWidth(6);
  
  
  this->labelCardiacCycleTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelCardiacCycleTime->Create(app);
  this->labelCardiacCycleTime->SetText("Cardiac Cycle Time ");
  
  this->entryCardiacCycleTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryCardiacCycleTime->Create(app);
  this->entryCardiacCycleTime->SetValueAsDouble(0.8);
  this->entryCardiacCycleTime->SetWidth(6);
  //this->entryCardiacCycleTime->SetReadOnly(1);

  this->labelFinalTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->labelFinalTime->Create(app);
  this->labelFinalTime->SetText("Final Time [s]");

  this->entryFinalTime->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->entryFinalTime->Create(app);
  this->entryFinalTime->SetBalloonHelpString("Simulation final time");  
  this->entryFinalTime->SetWidth(6);
  this->entryFinalTime->SetValueAsDouble(0.8);
  
  this->CalculateTimeStepButton->SetParent(this->SolverTimeConfigFrame->GetFrame());
  this->CalculateTimeStepButton->Create(app);
  this->CalculateTimeStepButton->SetWidth(20);
  this->CalculateTimeStepButton->SetText("Calculate Time Parameters");
  

  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->ResumeLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->radioResumeYes->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->radioResumeNo->GetWidgetName());

  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->labelNumberOfTimeSteps->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->entryNumberOfTimeSteps->GetWidgetName());

  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelTimeStep->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2", this->entryTimeStep->GetWidgetName());

  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelInitialTime->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2", this->entryInitialTime->GetWidgetName());

	this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2", this->labelNumberOfCardiacCycles->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2", this->entryNumberOfCardiacCycles->GetWidgetName());

  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2", this->labelCardiacCycleTime->GetWidgetName());
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2", this->entryCardiacCycleTime->GetWidgetName());

  this->Script("grid %s -row 7 -column 0 -padx 2 -pady 2", this->labelFinalTime->GetWidgetName());
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2", this->entryFinalTime->GetWidgetName());

  this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2", this->CalculateTimeStepButton->GetWidgetName());
  
  
  this->CalculateTimeStepButton->SetCommand(this, "CalculateTimeParameters");

}

//------------------------------------------------------------------
void vtkPVHMCoupledModelConfigurationWidget::CreateConvParam3D(vtkKWApplication *app) 
{
	  // componenetes do segundo frame "Convergence Parameters"
  //------------------------------------------------
  
  this->labelVariable3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelVariable3D->Create(app);
  this->labelVariable3D->SetText("Variable");
  this->labelVariable3D->SetBalloonHelpString("Name of the variable");  
  
  this->labelRefVal3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelRefVal3D->Create(app);
  this->labelRefVal3D->SetText("Reference Value");
  this->labelRefVal3D->SetBalloonHelpString("Reference value from which convergence is measured");  
  
  this->labelConEr3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelConEr3D->Create(app);
  this->labelConEr3D->SetText("Convergence Error");
  this->labelConEr3D->SetBalloonHelpString("Error value admissible with respect to the reference value");  
 

  this->labelIter3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelIter3D->Create(app);
  this->labelIter3D->SetText("Iterations");

  this->entryIter3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryIter3D->Create(app);
  this->entryIter3D->SetBalloonHelpString("Maximum number of iterations without convergence");  
  this->entryIter3D->SetValueAsInt(100);
  this->entryIter3D->SetWidth(6);

  this->labelSuPar3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelSuPar3D->Create(app);
  this->labelSuPar3D->SetText("Subrelaxation\nParameter");

  this->scaleSuPar3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->scaleSuPar3D->Create(app);
  this->scaleSuPar3D->SetRange(0.0, 1.0);
  this->scaleSuPar3D->SetResolution(0.1);
  this->scaleSuPar3D->SetEntryPositionToRight();
  this->scaleSuPar3D->SetBalloonHelpString("Subrelaxation of solution between iterations");
  this->scaleSuPar3D->SetValue(1.0);

  
  this->labelVelocityX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelVelocityX->Create(app);
  this->labelVelocityX->SetText("Velocity \nin x-direction\n [cm/sec]");
  //this->labelVelocityX->SetBalloonHelpString("");  
  
  this->labelVelocityY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelVelocityY->Create(app);
  this->labelVelocityY->SetText("Velocity \nin y-direction \n[cm/sec]");
  //this->labelVelocityY->SetBalloonHelpString("");  

  this->labelVelocityZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelVelocityZ->Create(app);
  this->labelVelocityZ->SetText("Velocity \nin z-direction \n[cm/sec]");
  //this->labelVelocityX->SetBalloonHelpString("");  

  this->labelPressure3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelPressure3D->Create(app);
  this->labelPressure3D->SetText("Pressure\n[dyn/cm^2]");


  this->labelDisplacementX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelDisplacementX->Create(app);
  this->labelDisplacementX->SetText("Displacement \nin x-direction \n[cm/sec]");
  //this->labelDisplacementX->SetBalloonHelpString("");  
  
  this->labelDisplacementY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelDisplacementY->Create(app);
  this->labelDisplacementY->SetText("Displacement \nin y-direction \n[cm/sec]");
  //this->labelVelocityY->SetBalloonHelpString("");  

  this->labelDisplacementZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->labelDisplacementZ->Create(app);
  this->labelDisplacementZ->SetText("Displacement \nin z-direction\n [cm/sec]");
  //this->labelDisplacementZ->SetBalloonHelpString("");  


  this->entryRefVelocityX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefVelocityX->Create(app);
  this->entryRefVelocityX->SetValueAsDouble(1);
  this->entryRefVelocityX->SetWidth(6);    
    
  this->entryRefVelocityY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefVelocityY->Create(app);
  this->entryRefVelocityY->SetValueAsDouble(1);
  this->entryRefVelocityY->SetWidth(6);    

  this->entryRefVelocityZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefVelocityZ->Create(app);
  this->entryRefVelocityZ->SetValueAsDouble(1);
  this->entryRefVelocityZ->SetWidth(6);    
  
  
  this->entryRefPressure3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefPressure3D->Create(app);
  this->entryRefPressure3D->SetValueAsDouble(30000);
  this->entryRefPressure3D->SetWidth(6);
  

  this->entryConvPressure3D->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvPressure3D->Create(app);
  this->entryConvPressure3D->SetValueAsDouble(0.01);
  this->entryConvPressure3D->SetWidth(6);

  this->entryRefDisplacementX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefDisplacementX->Create(app);
  this->entryRefDisplacementX->SetValueAsDouble(0.1);
  this->entryRefDisplacementX->SetWidth(6);    

  this->entryRefDisplacementY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefDisplacementY->Create(app);
  this->entryRefDisplacementY->SetValueAsDouble(0.1);
  this->entryRefDisplacementY->SetWidth(6);    

  this->entryRefDisplacementZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryRefDisplacementZ->Create(app);
  this->entryRefDisplacementZ->SetValueAsDouble(0.1);
  this->entryRefDisplacementZ->SetWidth(6);    


  this->entryConvVelocityX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvVelocityX->Create(app);
  this->entryConvVelocityX->SetValueAsDouble(0.01);
  this->entryConvVelocityX->SetWidth(6);    

  this->entryConvVelocityY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvVelocityY->Create(app);
  this->entryConvVelocityY->SetValueAsDouble(0.01);
  this->entryConvVelocityY->SetWidth(6);    

  this->entryConvVelocityZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvVelocityZ->Create(app);
  this->entryConvVelocityZ->SetValueAsDouble(0.01);
  this->entryConvVelocityZ->SetWidth(6);    


  this->entryConvDisplacementX->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvDisplacementX->Create(app);
  this->entryConvDisplacementX->SetValueAsDouble(0.01);
  this->entryConvDisplacementX->SetWidth(6);    

  this->entryConvDisplacementY->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvDisplacementY->Create(app);
  this->entryConvDisplacementY->SetValueAsDouble(0.01);
  this->entryConvDisplacementY->SetWidth(6);    

  this->entryConvDisplacementZ->SetParent(this->ConvergenceParametersFrame3D->GetFrame());
  this->entryConvDisplacementZ->Create(app);
  this->entryConvDisplacementZ->SetValueAsDouble(0.01);
  this->entryConvDisplacementZ->SetWidth(6);    


  //this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar->GetWidgetName());


  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->labelVariable3D->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->labelVelocityX->GetWidgetName());
  this->Script("grid %s -row 0 -column 2 -padx 2 -pady 2", this->labelVelocityY->GetWidgetName());
  this->Script("grid %s -row 0 -column 3 -padx 2 -pady 2", this->labelVelocityZ->GetWidgetName());

  this->Script("grid %s -row 0 -column 4 -padx 2 -pady 2", this->labelPressure3D->GetWidgetName());
  this->Script("grid %s -row 0 -column 5 -padx 2 -pady 2", this->labelDisplacementX->GetWidgetName());
  this->Script("grid %s -row 0 -column 6 -padx 2 -pady 2", this->labelDisplacementY->GetWidgetName());
  this->Script("grid %s -row 0 -column 7 -padx 2 -pady 2", this->labelDisplacementZ->GetWidgetName());

  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelRefVal3D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->entryRefVelocityX->GetWidgetName());
  this->Script("grid %s -row 1 -column 2 -padx 2 -pady 2", this->entryRefVelocityY->GetWidgetName());
  this->Script("grid %s -row 1 -column 3 -padx 2 -pady 2", this->entryRefVelocityZ->GetWidgetName());
  this->Script("grid %s -row 1 -column 4 -padx 2 -pady 2", this->entryRefPressure3D->GetWidgetName());
  this->Script("grid %s -row 1 -column 5 -padx 2 -pady 2", this->entryRefDisplacementX->GetWidgetName());
  this->Script("grid %s -row 1 -column 6 -padx 2 -pady 2", this->entryRefDisplacementY->GetWidgetName());
  this->Script("grid %s -row 1 -column 7 -padx 2 -pady 2", this->entryRefDisplacementZ->GetWidgetName());

  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->labelConEr3D->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->entryConvVelocityX->GetWidgetName());
  this->Script("grid %s -row 2 -column 2 -padx 2 -pady 2", this->entryConvVelocityY->GetWidgetName());
  this->Script("grid %s -row 2 -column 3 -padx 2 -pady 2", this->entryConvVelocityZ->GetWidgetName());
  this->Script("grid %s -row 2 -column 4 -padx 2 -pady 2", this->entryConvPressure3D->GetWidgetName());
  this->Script("grid %s -row 2 -column 5 -padx 2 -pady 2", this->entryConvDisplacementX->GetWidgetName());
  this->Script("grid %s -row 2 -column 6 -padx 2 -pady 2", this->entryConvDisplacementY->GetWidgetName());
  this->Script("grid %s -row 2 -column 7 -padx 2 -pady 2", this->entryConvDisplacementZ->GetWidgetName());
  
  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter3D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter3D->GetWidgetName());
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar3D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar3D->GetWidgetName());
	
  
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateConvParam1D(vtkKWApplication *app) 
{
	// componenetes do segundo frame "Convergence Parameters"
  //------------------------------------------------
  
  this->labelVariable1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelVariable1D->Create(app);
  this->labelVariable1D->SetText("Variable");
  this->labelVariable1D->SetBalloonHelpString("Name of the variable");  
  
  this->labelFlux->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelFlux->Create(app);
  this->labelFlux->SetText(" Flux\n[cm^3/sec]");
  this->labelFlux->SetBalloonHelpString("Name of the variable");  
  
  this->labelElPre->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelElPre->Create(app);
  this->labelElPre->SetWidgetName(this->labelElPre->GetWidgetName());
  this->labelElPre->SetText("  Elastic\n Pressure\n[dyn/cm^2]");
  this->labelElPre->SetBalloonHelpString("Name of the variable");  
  
  this->labelArea->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelArea->Create(app);
  this->labelArea->SetText(" Area\n[cm^2]");
  this->labelArea->SetBalloonHelpString("Name of the variable");  
  
  this->labelPress->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelPress->Create(app);
  this->labelPress->SetText(" Pressure\n[dyn/cm^2]");
  this->labelPress->SetBalloonHelpString("Name of the variable");  
  
  this->labelRefVal1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelRefVal1D->Create(app);
  this->labelRefVal1D->SetText("Reference Value");
  this->labelRefVal1D->SetBalloonHelpString("Reference value from which convergence is measured");  
  
  this->labelConEr1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelConEr1D->Create(app);
  this->labelConEr1D->SetText("Convergence Error");
  this->labelConEr1D->SetBalloonHelpString("Error value admissible with respect to the reference value");  
 
  this->entryReFl->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryReFl->Create(app);
  this->entryReFl->SetValueAsDouble(1);
  this->entryReFl->SetWidth(6);
  
  this->entryCeFl->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryCeFl->Create(app);
  this->entryCeFl->SetValueAsDouble(0.01);
  this->entryCeFl->SetWidth(6);
  
  
  this->entryReEP->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryReEP->Create(app);
  this->entryReEP->SetWidgetName(this->entryReEP->GetWidgetName());
  this->entryReEP->SetValueAsDouble(30000);
  this->entryReEP->SetWidth(6);

  this->entryCeEP->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryCeEP->Create(app);
  this->entryCeEP->SetWidgetName(this->entryCeEP->GetWidgetName());
  this->entryCeEP->SetValueAsDouble(0.01);
  this->entryCeEP->SetWidth(6);
  
  
  this->entryReAr->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryReAr->Create(app);
  this->entryReAr->SetValueAsDouble(0.1);
  this->entryReAr->SetWidth(6);
  

  this->entryCeAr->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryCeAr->Create(app);
  this->entryCeAr->SetValueAsDouble(0.01);
  this->entryCeAr->SetWidth(6);


  this->entryRePr->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryRePr->Create(app);
  this->entryRePr->SetValueAsDouble(30000);
  this->entryRePr->SetWidth(6);
  
  this->entryCePr->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryCePr->Create(app);
  this->entryCePr->SetValueAsDouble(0.01);
  this->entryCePr->SetWidth(6);

  this->labelIter1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelIter1D->Create(app);
  this->labelIter1D->SetText("Iterations");

  this->entryIter1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->entryIter1D->Create(app);
  this->entryIter1D->SetBalloonHelpString("Maximum number of iterations without convergence");  
  this->entryIter1D->SetValueAsInt(100);
  this->entryIter1D->SetWidth(6);

  this->labelSuPar1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->labelSuPar1D->Create(app);
  this->labelSuPar1D->SetText("Subrelaxation\nParameter");

  this->scaleSuPar1D->SetParent(this->ConvergenceParametersFrame1D->GetFrame());
  this->scaleSuPar1D->Create(app);
  this->scaleSuPar1D->SetRange(0.0, 1.0);
  this->scaleSuPar1D->SetResolution(0.1);
  this->scaleSuPar1D->SetEntryPositionToRight();
  this->scaleSuPar1D->SetBalloonHelpString("Subrelaxation of solution between iterations");
  this->scaleSuPar1D->SetValue(1.0);


  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->labelVariable1D->GetWidgetName());
	this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->labelFlux->GetWidgetName());
  this->Script("grid %s -row 0 -column 2 -padx 2 -pady 2", this->labelElPre->GetWidgetName());
  this->Script("grid %s -row 0 -column 3 -padx 2 -pady 2", this->labelArea->GetWidgetName());
  this->Script("grid %s -row 0 -column 4 -padx 2 -pady 2", this->labelPress->GetWidgetName());

  
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelRefVal1D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->entryReFl->GetWidgetName());
  this->Script("grid %s -row 1 -column 2 -padx 2 -pady 2", this->entryReEP->GetWidgetName());
  this->Script("grid %s -row 1 -column 3 -padx 2 -pady 2", this->entryReAr->GetWidgetName());
  this->Script("grid %s -row 1 -column 4 -padx 2 -pady 2", this->entryRePr->GetWidgetName());
  
  
	this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->labelConEr1D->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->entryCeFl->GetWidgetName());
  this->Script("grid %s -row 2 -column 2 -padx 2 -pady 2", this->entryCeEP->GetWidgetName());
  this->Script("grid %s -row 2 -column 3 -padx 2 -pady 2", this->entryCeAr->GetWidgetName());
  this->Script("grid %s -row 2 -column 4 -padx 2 -pady 2", this->entryCePr->GetWidgetName());
  
  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter1D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar1D->GetWidgetName());
}


//------------------------------------------------------------------

vtkPVApplication* vtkPVHMCoupledModelConfigurationWidget::GetPVApplication()
{
  return vtkPVApplication::SafeDownCast(this->GetApplication());
}

//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::CreateSolverAdditionalConfig1D(vtkKWApplication *app)
{
	char buffer[1024];
	
  this->labelTethaScheme1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->labelTethaScheme1D->Create(app);
  this->labelTethaScheme1D->SetText("Parameter Theta-Scheme");
  
  this->thumbTheta1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->thumbTheta1D->Create(app);
  this->thumbTheta1D->SetRange(0.0, 1.0);
  this->thumbTheta1D->SetMinimumValue(0.0);
  this->thumbTheta1D->SetMaximumValue(1.0);
  this->thumbTheta1D->ClampMinimumValueOn();
  this->thumbTheta1D->ClampMaximumValueOn();
  this->thumbTheta1D->SetResolution(0.05);
  this->thumbTheta1D->DisplayEntryOn();
  this->thumbTheta1D->DisplayEntryAndLabelOnTopOff();
  this->thumbTheta1D->SetValue(0.5);
  this->thumbTheta1D->SetBalloonHelpString("Parameter for implicit-explicit numerical scheme (0: implicit / 1: explicit)");


  this->labelAditi1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->labelAditi1D->Create(app);
  this->labelAditi1D->SetText("Aditivity");

  const char *textYesNo = "Defines if terminals are considered aditively or not. "
		    "If 'Yes' option is choose, the 'Penalization' is the penalty factor for imposing the equations.";

  this->labelPena1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->labelPena1D->Create(app);
  this->labelPena1D->SetWidgetName(this->labelPena1D->GetWidgetName());
  this->labelPena1D->SetText("Penalization");

  this->entryPena1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->entryPena1D->Create(app); 
  this->entryPena1D->SetWidgetName(this->entryPena1D->GetWidgetName());
  this->entryPena1D->SetBalloonHelpString(textYesNo);
  this->entryPena1D->SetWidth(6);
  this->entryPena1D->SetReadOnly(1);

  this->radioYes1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->radioYes1D->Create(app);
  this->radioYes1D->SetText("Yes");
  sprintf(buffer, "%s config -state normal; %s config -state normal", 
	this->labelPena1D->GetWidgetName(), this->entryPena1D->GetWidgetName());
  this->radioYes1D->SetCommand(NULL, buffer);
  this->radioYes1D->SetValue("Yes");
  this->radioYes1D->SetBalloonHelpString(textYesNo);

  this->radioNo1D->SetParent(this->AdditionalParamSolver1DFrame->GetFrame());
  this->radioNo1D->Create(app);
  this->radioNo1D->SetText("No");
  this->radioNo1D->SetValue("No");
  sprintf(buffer, "%s config -state disabled; %s config -state disabled", 
	this->labelPena1D->GetWidgetName(), this->entryPena1D->GetWidgetName());
  this->radioNo1D->SetCommand(NULL, buffer);
  this->radioNo1D->SetBalloonHelpString(textYesNo);

  this->radioYes1D->SetSelectedState(1);
  this->radioNo1D->SetVariableName(this->radioYes1D->GetVariableName());
  
  this->radioYes1D->SetVariableName(this->radioNo1D->GetVariableName());
  this->radioNo1D->SetSelectedState(1);
  

  // Theta scheme parameters
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelTethaScheme1D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->thumbTheta1D->GetWidgetName());

  // Aditivity
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelAditi1D->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->radioYes1D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->radioNo1D->GetWidgetName());


  // Penalization
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->labelPena1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->entryPena1D->GetWidgetName());
	  	  
	
}


//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::CreateSolverAdditionalConfig3D(vtkKWApplication *app)
{
	char buffer[1024];
	
	this->labelTethaScheme3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->labelTethaScheme3D->Create(app);
  this->labelTethaScheme3D->SetText("Parameter Theta-Scheme");
  
  this->thumbTheta3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->thumbTheta3D->Create(app);
  this->thumbTheta3D->SetRange(0.0, 1.0);
  this->thumbTheta3D->SetMinimumValue(0.0);
  this->thumbTheta3D->SetMaximumValue(1.0);
  this->thumbTheta3D->ClampMinimumValueOn();
  this->thumbTheta3D->ClampMaximumValueOn();
  this->thumbTheta3D->SetResolution(0.05);
  this->thumbTheta3D->DisplayEntryOn();
  this->thumbTheta3D->DisplayEntryAndLabelOnTopOff();
  this->thumbTheta3D->SetValue(0.5);
  this->thumbTheta3D->SetBalloonHelpString("Parameter for implicit-explicit numerical scheme (0: implicit / 1: explicit)");

  this->TetraCompressibilityLabel->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->TetraCompressibilityLabel->Create(app);
  this->TetraCompressibilityLabel->SetText("Fluid Artificial Compressibility: ");
	
	this->TetraCompressibilityEntry->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->TetraCompressibilityEntry->Create(app);
  this->TetraCompressibilityEntry->SetValueAsDouble(1.0E-05);
  this->TetraCompressibilityEntry->SetWidth(8);

  this->labelAditi3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->labelAditi3D->Create(app);
  this->labelAditi3D->SetText("Aditivity");

  const char *textYesNo = "Defines if terminals are considered aditively or not. "
		    "If 'Yes' option is choose, the 'Penalization' is the penalty factor for imposing the equations.";

  this->labelPena3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->labelPena3D->Create(app);
  this->labelPena3D->SetWidgetName(this->labelPena3D->GetWidgetName());
  this->labelPena3D->SetText("Penalization");

  this->entryPena3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->entryPena3D->Create(app); 
  this->entryPena3D->SetWidgetName(this->entryPena3D->GetWidgetName());
  this->entryPena3D->SetBalloonHelpString(textYesNo);
  this->entryPena3D->SetWidth(6);
  this->entryPena3D->SetReadOnly(1);

  this->radioYes3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->radioYes3D->Create(app);
  this->radioYes3D->SetText("Yes");
  sprintf(buffer, "%s config -state normal; %s config -state normal", 
	this->labelPena3D->GetWidgetName(), this->entryPena3D->GetWidgetName());
  this->radioYes3D->SetCommand(NULL, buffer);
  this->radioYes3D->SetValue("Yes");
  this->radioYes3D->SetBalloonHelpString(textYesNo);

  this->radioNo3D->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->radioNo3D->Create(app);
  this->radioNo3D->SetText("No");
  this->radioNo3D->SetValue("No");
  sprintf(buffer, "%s config -state disabled; %s config -state disabled", 
	this->labelPena3D->GetWidgetName(), this->entryPena3D->GetWidgetName());
  this->radioNo3D->SetCommand(NULL, buffer);
  this->radioNo3D->SetBalloonHelpString(textYesNo);

  this->radioYes3D->SetSelectedState(1);
  this->radioNo3D->SetVariableName(this->radioYes3D->GetVariableName());
  
  this->radioYes3D->SetVariableName(this->radioNo3D->GetVariableName());
  this->radioNo3D->SetSelectedState(1);

    
  
  this->labelIncrementalVersion->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->labelIncrementalVersion->Create(app);
  this->labelIncrementalVersion->SetText("Incremental Version");
  
   
  this->radioICYes->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->radioICYes->Create(app);
  this->radioICYes->SetText("Yes");
  this->radioICYes->SetValue("Yes");
 
  this->radioICNo->SetParent(this->AdditionalParamSolver3DFrame->GetFrame());
  this->radioICNo->Create(app);
  this->radioICNo->SetText("No");
  this->radioICNo->SetValue("No");
  this->radioICNo->SelectedStateOn();
  
	this->radioICYes->SetVariableName(this->radioICNo->GetVariableName());
   
  // parameter theta scheme
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelTethaScheme3D->GetWidgetName());
	this->Script("grid %s -row 1 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->thumbTheta3D->GetWidgetName());

  // fluid artificial compressibility
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->TetraCompressibilityLabel->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->TetraCompressibilityEntry->GetWidgetName());
  
  // aditivity
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelAditi3D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->radioYes3D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->radioNo3D->GetWidgetName());
  
  // penalization
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelPena3D->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->entryPena3D->GetWidgetName());

  // incremental version	  
  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->labelIncrementalVersion->GetWidgetName());
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2 -sticky w", this->radioICYes->GetWidgetName());
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2 -sticky w", this->radioICNo->GetWidgetName());

}
//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateSolverConfig1D3D(vtkKWApplication *app) 
{
	// item Renumbering do Solver Config 
  this->labelRenu->SetParent(this->SolverConfig1D3DFrame->GetFrame());
  this->labelRenu->Create(app);
  this->labelRenu->SetText("Renumbering");

  this->entryRenu->SetParent(this->SolverConfig1D3DFrame->GetFrame());
  this->entryRenu->Create(app);
  this->entryRenu->SetBalloonHelpString("Number of iterations of renumbering algorithm");
  this->entryRenu->SetValueAsInt(0);
  this->entryRenu->SetWidth(6);

  // renumbering
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelRenu->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryRenu->GetWidgetName());
  //*************************

}



//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateMiscParam(vtkKWApplication *app) 
{
	// page 4      ****Miscellaneous 
	//char buffer[1024];
   
  this->ScaleFactor3DLabel->SetParent(this->mi);
  this->ScaleFactor3DLabel->Create(app);
  this->ScaleFactor3DLabel->SetText("3D Scale Factor");
  
  this->ScaleFactor3DEntry->SetParent(this->mi);
  this->ScaleFactor3DEntry->Create(app);
  this->ScaleFactor3DEntry->SetBalloonHelpString("Parameter used to convert scale between 3D and 1D models");
  //this->ScaleFactor3DEntry->GetEntry()->ReadOnlyOn();
  this->ScaleFactor3DEntry->SetValueAsInt(1);
  this->ScaleFactor3DEntry->SetWidth(6);
  
  
  this->CompatibilizeLabel->SetParent(this->mi);
  this->CompatibilizeLabel->Create(app);
  this->CompatibilizeLabel->SetText("Compatibilize Geometric Features");

 	this->radioCompatibilizeYes->SetParent(this->mi);
  this->radioCompatibilizeYes->Create(app);
  this->radioCompatibilizeYes->SetText("Yes");
//  sprintf(buffer, "%s config -state normal; %s config -state normal", 
//	this->labelCompatibilizeType->GetWidgetName(), this->comboCompatibilizeType->GetWidgetName());
  
//  this->radioCompatibilizeYes->SetCommand(NULL, buffer);
  
  this->radioCompatibilizeYes->SetValue("Yes");
  //this->radioCompatibilizeYes->SetBalloonHelpString(textYesNo);

  this->radioCompatibilizeNo->SetParent(this->mi);
  this->radioCompatibilizeNo->Create(app);
  this->radioCompatibilizeNo->SetText("No");
  this->radioCompatibilizeNo->SetValue("No");
  
//  sprintf(buffer, "%s config -state disabled; %s config -state disabled", 
//	this->labelCompatibilizeType->GetWidgetName(), this->comboCompatibilizeType->GetWidgetName());
//  this->radioCompatibilizeNo->SetCommand(NULL, buffer);
//  
  this->radioCompatibilizeYes->SetVariableName(this->radioCompatibilizeNo->GetVariableName());
  
  this->radioCompatibilizeYes->SetCommand(this, "ShowCompatibilizationMode");
  this->radioCompatibilizeNo->SetCommand(this, "ShowCompatibilizationMode");
  
  
  
  this->labelCompatibilizeType->SetParent(this->mi);
  this->labelCompatibilizeType->Create(app);
  //this->labelSolverType3D->SetWidgetName(this->labelSolverType->GetWidgetName());
  this->labelCompatibilizeType->SetText("Extension to the Whole Segment");

  this->comboCompatibilizeType->SetParent(this->mi);
  this->comboCompatibilizeType->Create(app);
	this->comboCompatibilizeType->ReadOnlyOn();
 	this->comboCompatibilizeType->SetBalloonHelpString("Select the proper way which the compatibilization process will affect the coupling segment");
  this->comboCompatibilizeType->AddValue("Linear variation extension");
  this->comboCompatibilizeType->AddValue("Constant extension");
  this->comboCompatibilizeType->SetValue("Constant extension");
  
  
  this->labelFileOutput->SetParent(this->mi);
  this->labelFileOutput->Create(app);
  this->labelFileOutput->SetText("File Output");

  this->entryFileOutput->SetParent(this->mi);
  this->entryFileOutput->Create(app);
  this->entryFileOutput->SetBalloonHelpString("Number of time steps between writings of output file");  
  this->entryFileOutput->SetValueAsInt(1);
  this->entryFileOutput->SetWidth(6);

  this->labelScreenOutput->SetParent(this->mi);
  this->labelScreenOutput->Create(app);
  this->labelScreenOutput->SetText("Screen Output");

  this->entryScreenOutput->SetParent(this->mi);
  this->entryScreenOutput->Create(app);
  this->entryScreenOutput->SetBalloonHelpString("Number of time steps between writings of screen information");
  this->entryScreenOutput->SetValueAsInt(1);
  this->entryScreenOutput->SetWidth(6);
  
  this->labelLogFile->SetParent(this->mi);
  this->labelLogFile->Create(app);
  this->labelLogFile->SetText("Log File Name");

  this->entryLogFile->SetParent(this->mi);
  this->entryLogFile->Create(app);
  this->entryLogFile->SetBalloonHelpString("Output from solver execution");
  this->entryLogFile->SetValue("Log.txt");
  this->entryLogFile->SetWidth(16);
  

  this->labelFilesPath->SetParent(this->mi);
  this->labelFilesPath->Create(app);
  this->labelFilesPath->SetText("Path");

  this->entryFilesPath->SetParent(this->mi);
  this->entryFilesPath->Create(app);
  this->entryFilesPath->SetBalloonHelpString("Location for file generation");
  

  #ifdef _WIN32
    this->entryFilesPath->SetValue("c:/temp/");
	#else
    this->entryFilesPath->SetValue("/tmp");
	#endif
  
  this->entryFilesPath->SetWidth(25);

  this->pushButtonBrowse->SetParent(this->mi); 
  this->pushButtonBrowse->Create(app);
  this->pushButtonBrowse->SetText("Browse");
  this->pushButtonBrowse->SetCommand(this, "SetGenerationFilesPath");
  
  
  this->CommitChangesButtonPage1->SetParent(this->mi); 
  this->CommitChangesButtonPage1->Create(app);
  this->CommitChangesButtonPage1->SetText("Generate Solver Files");
  this->CommitChangesButtonPage1->SetCommand(this, "GetValuesFromConfigWindow");
  
  
  
  this->textFilesGenerationStatus->SetParent(this->mi);
  this->textFilesGenerationStatus->Create(app);
  this->textFilesGenerationStatus->GetWidget()->SetText("Files not generated yet");
  this->textFilesGenerationStatus->GetWidget()->SetWidth(25);
  this->textFilesGenerationStatus->GetWidget()->SetHeight(6);
  this->textFilesGenerationStatus->GetWidget()->ReadOnlyOn();
  
	this->SelectedSolverFilesLabel->SetParent(mi);
  this->SelectedSolverFilesLabel->Create(app);
  this->SelectedSolverFilesLabel->SetText("Select which file will be generated ");
  this->Script("grid %s -row 8 -column 0 -padx 2 -pady 2 -sticky w", this->SelectedSolverFilesLabel->GetWidgetName());

  this->SelectedSolverFilesRadioButtonSet->SetParent(mi);
	this->SelectedSolverFilesRadioButtonSet->Create(app);
	this->SelectedSolverFilesRadioButtonSet->SetBorderWidth(2);
	this->SelectedSolverFilesRadioButtonSet->SetReliefToGroove();
	
	
	for (int i = 0; i < 5; ++i)
		vtkKWRadioButton *temp = this->SelectedSolverFilesRadioButtonSet->AddWidget(i);
	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->SetText("All Files");	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->SetSelectedState(1);	
	this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->SetText("Basparam.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->SetText("Mesh.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->SetText("Param.txt");
	this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->SetText("Inifile.txt");
	
	this->Script("grid %s -row 8 -column 1 -padx 2 -pady 2 -sticky w", this->SelectedSolverFilesRadioButtonSet->GetWidgetName());
  
  
  
  
  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->ScaleFactor3DLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->ScaleFactor3DEntry->GetWidgetName());
  
  
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->CompatibilizeLabel->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->radioCompatibilizeYes->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->radioCompatibilizeNo->GetWidgetName());
  
  
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->labelCompatibilizeType->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->comboCompatibilizeType->GetWidgetName());
  
  
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->labelFileOutput->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->entryFileOutput->GetWidgetName());
  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->labelScreenOutput->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->entryScreenOutput->GetWidgetName());
  this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -sticky w", this->labelLogFile->GetWidgetName());
  this->Script("grid %s -row 6 -column 1 -padx 2 -pady 2 -sticky w", this->entryLogFile->GetWidgetName());
  this->Script("grid %s -row 7 -column 0 -padx 2 -pady 2 -sticky w", this->labelFilesPath->GetWidgetName());
  this->Script("grid %s -row 7 -column 1 -padx 2 -pady 2 -sticky w", this->entryFilesPath->GetWidgetName());
  this->Script("grid %s -row 7 -column 2 -padx 2 -pady 2 -sticky w", this->pushButtonBrowse->GetWidgetName());
  this->Script("grid %s -row 9 -column 0 -padx 2 -pady 2 -sticky w", this->CommitChangesButtonPage1->GetWidgetName());
  this->Script("grid %s -row 9 -column 1 -padx 2 -pady 2", this->textFilesGenerationStatus->GetWidgetName());

}


//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateMonolithicSolverConfig(vtkKWApplication *app) 
{

  this->frameIterativeSolver->SetParent(this->SolverConfigMonoFrame->GetFrame());
  this->frameIterativeSolver->Create(this->GetPVApplication());
  this->frameIterativeSolver->SetLabelText("Iterative Solver");
  
  this->ParallelIterativeSolverFrame->SetParent(this->sc);
  this->ParallelIterativeSolverFrame->Create(this->GetPVApplication());
  this->ParallelIterativeSolverFrame->SetLabelText("Iterative Solver");
  
    
  this->framePreconditioning->SetParent(this->SolverConfigMonoFrame->GetFrame());
  this->framePreconditioning->Create(this->GetPVApplication());
  this->framePreconditioning->SetLabelText("Preconditioning");
  

  this->labelIterationsBeforeRestart->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelIterationsBeforeRestart->Create(app);
  this->labelIterationsBeforeRestart->SetText("Iterations before restart");
  
  
  this->labelConvergenceError->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelConvergenceError->Create(app);
  this->labelConvergenceError->SetText("Convergence Error");

  this->labelkrylov->SetParent(this->frameIterativeSolver->GetFrame());
  this->labelkrylov->Create(app);
  this->labelkrylov->SetText("Krylov Subspace");
  

  this->entryIterationsBeforeRestart->SetParent(this->frameIterativeSolver->GetFrame());
  this->entryIterationsBeforeRestart->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryIterationsBeforeRestart->SetWidth(6);
  this->entryIterationsBeforeRestart->SetValueAsInt(100);
  
  this->entryConvergenceError->SetParent(this->frameIterativeSolver->GetFrame());
  this->entryConvergenceError->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryConvergenceError->SetWidth(6);
  this->entryConvergenceError->SetValueAsDouble(1E-10);

  this->entrykrylov->SetParent(this->frameIterativeSolver->GetFrame());
  this->entrykrylov->Create(app); 
  this->entrykrylov->SetWidth(6);
  this->entrykrylov->SetValueAsInt(60);


  this->labelDispTolerance->SetParent(this->framePreconditioning->GetFrame());
  this->labelDispTolerance->Create(app);
  this->labelDispTolerance->SetText("Drop Tolerance");
  
  
  this->labelFParam->SetParent(this->framePreconditioning->GetFrame());
  this->labelFParam->Create(app);
  this->labelFParam->SetText("Fill Parameter");
  
  
  
  this->entryDispTolerance->SetParent(this->framePreconditioning->GetFrame());
  this->entryDispTolerance->Create(app); 
  this->entryDispTolerance->SetWidth(6);
  this->entryDispTolerance->SetValueAsDouble(1E-10);
  
  this->entryFParam->SetParent(this->framePreconditioning->GetFrame());
  this->entryFParam->Create(app); 
  this->entryFParam->SetWidth(6);
  this->entryFParam->SetValueAsDouble(20);
  

  

  // iterative solver Frame 
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->frameIterativeSolver->GetWidgetName());


  // iterations before restart (dentro de iterative solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelIterationsBeforeRestart->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryIterationsBeforeRestart->GetWidgetName());

  // convergence error (dentro de iterative solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelConvergenceError->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryConvergenceError->GetWidgetName());

  // krylov subspace (dentro de iterative solver frame)
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelkrylov->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entrykrylov->GetWidgetName());	

  // framePreconditioning frame (dentro de iterative solver frame)
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->framePreconditioning->GetWidgetName());


  // fill param (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelFParam->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryFParam->GetWidgetName());

  // drop tolerance (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelDispTolerance->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryDispTolerance->GetWidgetName());
	  
	
}
//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::CreateSegregated3DSolverConfig(vtkKWApplication *app) 
{

  this->labelSolverType3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->labelSolverType3D->Create(app);
  //this->labelSolverType3D->SetWidgetName(this->labelSolverType->GetWidgetName());
  this->labelSolverType3D->SetText("Solver Type");

  this->comboSolverType3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->comboSolverType3D->Create(app);
	this->comboSolverType3D->ReadOnlyOn();
 	//this->comboSolverType->SetBalloonHelpString("");
  this->comboSolverType3D->AddValue("Sequential");
  this->comboSolverType3D->AddValue("Parallel");
  //this->comboSolverType->SetValue("Sequential");

	this->labelRLS3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->labelRLS3D->Create(app);
  this->labelRLS3D->SetText("Resolution of\nlinear system");

  this->comboRLS3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->comboRLS3D->Create(app);
	this->comboRLS3D->ReadOnlyOn();
 	this->comboRLS3D->SetBalloonHelpString("Method of resolution of the resulting linear system of equations");
 	this->comboRLS3D->SetCommand(this, "UpdateRLSRelatedWidgets 3");
 	
	this->comboRLS3D->DeleteAllValues();
  this->comboRLS3D->AddValue("Direct");
  this->comboRLS3D->AddValue("CG");
  this->comboRLS3D->AddValue("BCG"); 
  this->comboRLS3D->AddValue("DBCG"); 
	this->comboRLS3D->AddValue("CGNR");
	this->comboRLS3D->AddValue("BCGSTAB");
	this->comboRLS3D->AddValue("TFQMR");
	this->comboRLS3D->AddValue("FOM");
	this->comboRLS3D->AddValue("GMRES");
	this->comboRLS3D->AddValue("FGMRES");
	this->comboRLS3D->AddValue("DQGMRES");
	this->comboRLS3D->AddValue("CGS");
 	


  this->frameIterativeSolver3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->frameIterativeSolver3D->Create(this->GetPVApplication());
  this->frameIterativeSolver3D->SetLabelText("Iterative Solver");
    
  this->framePreconditioning3D->SetParent(this->SolverConfigSegregated3DFrame->GetFrame());
  this->framePreconditioning3D->Create(this->GetPVApplication());
  this->framePreconditioning3D->SetLabelText("Preconditioning");
  

  this->labelIterationsBeforeRestart3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->labelIterationsBeforeRestart3D->Create(app);
  this->labelIterationsBeforeRestart3D->SetText("Iterations before restart");
  
  
  this->labelConvergenceError3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->labelConvergenceError3D->Create(app);
  this->labelConvergenceError3D->SetText("Convergence Error");

  this->labelkrylov3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->labelkrylov3D->Create(app);
  this->labelkrylov3D->SetText("Krylov Subspace");
  

  this->entryIterationsBeforeRestart3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->entryIterationsBeforeRestart3D->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryIterationsBeforeRestart3D->SetWidth(6);
  this->entryIterationsBeforeRestart3D->SetValueAsInt(100);
  
  this->entryConvergenceError3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->entryConvergenceError3D->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryConvergenceError3D->SetWidth(6);
  this->entryConvergenceError3D->SetValueAsDouble(1E-10);

  this->entrykrylov3D->SetParent(this->frameIterativeSolver3D->GetFrame());
  this->entrykrylov3D->Create(app); 
  this->entrykrylov3D->SetWidth(6);
  this->entrykrylov3D->SetValueAsInt(60);


  this->labelDispTolerance3D->SetParent(this->framePreconditioning3D->GetFrame());
  this->labelDispTolerance3D->Create(app);
  this->labelDispTolerance3D->SetText("Drop Tolerance");
  
  
  this->labelFParam3D->SetParent(this->framePreconditioning3D->GetFrame());
  this->labelFParam3D->Create(app);
  this->labelFParam3D->SetText("Fill Parameter");
  
  
  
  this->entryDispTolerance3D->SetParent(this->framePreconditioning3D->GetFrame());
  this->entryDispTolerance3D->Create(app); 
  this->entryDispTolerance3D->SetWidth(6);
  this->entryDispTolerance3D->SetValueAsDouble(1E-10);
  
  this->entryFParam3D->SetParent(this->framePreconditioning3D->GetFrame());
  this->entryFParam3D->Create(app); 
  this->entryFParam3D->SetWidth(6);
  this->entryFParam3D->SetValueAsDouble(20);
  
  //this->comboRLS3D->SetStateToDisabled();
	  
	// Solver Type
  //this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelSolverType->GetWidgetName());
  //this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->comboSolverType->GetWidgetName());
  
  // resolution of linear system
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelRLS3D->GetWidgetName());
	this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->comboRLS3D->GetWidgetName());
	//this->Script("grid %s -row 4 -column 2 -padx 2 -pady 2 -sticky w", this->labelRLSHelp->GetWidgetName());

  // iterative solver Frame 
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->frameIterativeSolver3D->GetWidgetName());


  // iterations before restart (dentro de iterative solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelIterationsBeforeRestart3D->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryIterationsBeforeRestart3D->GetWidgetName());

  // convergence error (dentro de iterative solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelConvergenceError3D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryConvergenceError3D->GetWidgetName());

  // krylov subspace (dentro de iterative solver frame)
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelkrylov3D->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entrykrylov3D->GetWidgetName());	

  // framePreconditioning frame (dentro de iterative solver frame)
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->framePreconditioning3D->GetWidgetName());


  // fill param (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelFParam3D->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryFParam3D->GetWidgetName());

  // drop tolerance (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelDispTolerance3D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryDispTolerance3D->GetWidgetName());
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateSegregated1DSolverConfig(vtkKWApplication *app) 
{

	this->labelRLS1D->SetParent(this->SolverConfigSegregated1DFrame->GetFrame());
  this->labelRLS1D->Create(app);
  this->labelRLS1D->SetText("Resolution of\nlinear system");

  this->comboRLS1D->SetParent(this->SolverConfigSegregated1DFrame->GetFrame());
  this->comboRLS1D->Create(app);
	this->comboRLS1D->ReadOnlyOn();
 	this->comboRLS1D->SetBalloonHelpString("Method of resolution of the resulting linear system of equations");
 	this->comboRLS1D->DeleteAllValues();
  this->comboRLS1D->AddValue("Direct");
  this->comboRLS1D->AddValue("CG");
  this->comboRLS1D->AddValue("BCG"); 
  this->comboRLS1D->AddValue("DBCG"); 
	this->comboRLS1D->AddValue("CGNR");
	this->comboRLS1D->AddValue("BCGSTAB");
	this->comboRLS1D->AddValue("TFQMR");
	this->comboRLS1D->AddValue("FOM");
	this->comboRLS1D->AddValue("GMRES");
	this->comboRLS1D->AddValue("FGMRES");
	this->comboRLS1D->AddValue("DQGMRES");
	this->comboRLS1D->AddValue("CGS");
 	this->comboRLS1D->SetCommand(this, "UpdateRLSRelatedWidgets 1");
 	

  this->frameIterativeSolver1D->SetParent(this->SolverConfigSegregated1DFrame->GetFrame());
  this->frameIterativeSolver1D->Create(this->GetPVApplication());
  this->frameIterativeSolver1D->SetLabelText("Iterative Solver");
    
  this->framePreconditioning1D->SetParent(this->SolverConfigSegregated1DFrame->GetFrame());
  this->framePreconditioning1D->Create(this->GetPVApplication());
  this->framePreconditioning1D->SetLabelText("Preconditioning");
  

  this->labelIterationsBeforeRestart1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->labelIterationsBeforeRestart1D->Create(app);
  this->labelIterationsBeforeRestart1D->SetText("Iterations before restart");
  
  
  this->labelConvergenceError1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->labelConvergenceError1D->Create(app);
  this->labelConvergenceError1D->SetText("Convergence Error");

  this->labelkrylov1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->labelkrylov1D->Create(app);
  this->labelkrylov1D->SetText("Krylov Subspace");
  

  this->entryIterationsBeforeRestart1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->entryIterationsBeforeRestart1D->Create(app); 
	//this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryIterationsBeforeRestart1D->SetWidth(6);
  this->entryIterationsBeforeRestart1D->SetValueAsInt(100);
  
  this->entryConvergenceError1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->entryConvergenceError1D->Create(app); 
  //this->entryConvergenceError->SetBalloonHelpString(textYesNo);
  this->entryConvergenceError1D->SetWidth(6);
  this->entryConvergenceError1D->SetValueAsDouble(1E-10);

  this->entrykrylov1D->SetParent(this->frameIterativeSolver1D->GetFrame());
  this->entrykrylov1D->Create(app); 
  this->entrykrylov1D->SetWidth(6);
  this->entrykrylov1D->SetValueAsInt(60);


  this->labelDispTolerance1D->SetParent(this->framePreconditioning1D->GetFrame());
  this->labelDispTolerance1D->Create(app);
  this->labelDispTolerance1D->SetText("Drop Tolerance");
  
  
  this->labelFParam1D->SetParent(this->framePreconditioning1D->GetFrame());
  this->labelFParam1D->Create(app);
  this->labelFParam1D->SetText("Fill Parameter");
  
  
  
  this->entryDispTolerance1D->SetParent(this->framePreconditioning1D->GetFrame());
  this->entryDispTolerance1D->Create(app); 
  this->entryDispTolerance1D->SetWidth(6);
  this->entryDispTolerance1D->SetValueAsDouble(1E-10);
  
  this->entryFParam1D->SetParent(this->framePreconditioning1D->GetFrame());
  this->entryFParam1D->Create(app); 
  this->entryFParam1D->SetWidth(6);
  this->entryFParam1D->SetValueAsDouble(20);
  
  //this->comboRLS1D->SetStateToDisabled();
	  
  // resolution of linear system
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelRLS1D->GetWidgetName());
	this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->comboRLS1D->GetWidgetName());
	//this->Script("grid %s -row 4 -column 2 -padx 2 -pady 2 -sticky w", this->labelRLSHelp->GetWidgetName());

  // iterative solver Frame 
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->frameIterativeSolver1D->GetWidgetName());


  // iterations before restart (dentro de iterative solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelIterationsBeforeRestart1D->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryIterationsBeforeRestart1D->GetWidgetName());

  // convergence error (dentro de iterative solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelConvergenceError1D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryConvergenceError1D->GetWidgetName());

  // krylov subspace (dentro de iterative solver frame)
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->labelkrylov1D->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->entrykrylov1D->GetWidgetName());	

  // framePreconditioning frame (dentro de iterative solver frame)
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky we -columnspan 5", this->framePreconditioning1D->GetWidgetName());


  // fill param (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->labelFParam1D->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->entryFParam1D->GetWidgetName());

  // drop tolerance (dentro de Preconditioning solver frame)
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->labelDispTolerance1D->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->entryDispTolerance1D->GetWidgetName());
	  
	  
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CreateParallelSolverConfig(vtkKWApplication *app) 
{
		
	this->ParallelNumberOfProcessorsLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelNumberOfProcessorsLabel->Create(app);
  this->ParallelNumberOfProcessorsLabel->SetText("Number of Processors");
  

  this->ParallelNumberOfProcessorsEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelNumberOfProcessorsEntry->Create(app); 
	//this->ParallelNumberOfProcessorsEntry->SetBalloonHelpString(textYesNo);
  this->ParallelNumberOfProcessorsEntry->SetWidth(6);
  this->ParallelNumberOfProcessorsEntry->SetValueAsInt(4);
  this->ParallelNumberOfProcessorsEntry->SetCommand(this, "ParallelNumberOfProcessorsCallBack");
  
  
  
  this->ParallelIterationsRestartLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelIterationsRestartLabel->Create(app);
  this->ParallelIterationsRestartLabel->SetText("GMRES Iterations Before Restart");
  

  this->ParallelIterationsRestartEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelIterationsRestartEntry->Create(app); 
	//this->ParallelIterationsRestartEntry->SetBalloonHelpString(textYesNo);
  this->ParallelIterationsRestartEntry->SetWidth(6);
  this->ParallelIterationsRestartEntry->SetValueAsInt(100);
  this->ParallelIterationsRestartEntry->ReadOnlyOn();
  this->ParallelIterationsRestartLabel->SetState(0);  
  
  
  this->ParallelRelativeErrorLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelRelativeErrorLabel->Create(app);
  this->ParallelRelativeErrorLabel->SetText("Relative Convergence Error");
  

  this->ParallelRelativeErrorEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelRelativeErrorEntry->Create(app); 
	//this->ParallelRelativeErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelRelativeErrorEntry->SetWidth(6);
	this->ParallelRelativeErrorEntry->SetValueAsDouble(1E-08);
  
  this->ParallelAbsErrorLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelAbsErrorLabel->Create(app);
  this->ParallelAbsErrorLabel->SetText("Absolute Convergence Error");
  

  this->ParallelAbsErrorEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelAbsErrorEntry->Create(app); 
	//this->ParallelAbsErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelAbsErrorEntry->SetWidth(6);
  this->ParallelAbsErrorEntry->SetValueAsDouble(1E-20);	


  this->ParallelMaxNumberIterLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelMaxNumberIterLabel->Create(app);
  this->ParallelMaxNumberIterLabel->SetText("Maximum Number of Iterations");
  

  this->ParallelMaxNumberIterEntry->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelMaxNumberIterEntry->Create(app); 
	//this->ParallelAbsErrorEntry->SetBalloonHelpString(textYesNo);
  this->ParallelMaxNumberIterEntry->SetWidth(6);
  this->ParallelMaxNumberIterEntry->SetValueAsInt(1000);	


  this->ParallelPrecondLabel->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelPrecondLabel->Create(app);
  this->ParallelPrecondLabel->SetText("Preconditioning");
  

  this->ParallelPrecondCombo->SetParent(this->ParallelIterativeSolverFrame->GetFrame());
  this->ParallelPrecondCombo->Create(app); 
  this->ParallelPrecondCombo->ReadOnlyOn();
  
	//this->ParallelPrecondCombo->SetBalloonHelpString(textYesNo);
  this->ParallelPrecondCombo->SetWidth(8);
  this->ParallelPrecondCombo->DeleteAllValues();
  this->ParallelPrecondCombo->AddValue("None");
  this->ParallelPrecondCombo->AddValue("BJacobi");
  this->ParallelPrecondCombo->SetValue("None"); 


		
  
  this->Script("grid %s -row 0 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelNumberOfProcessorsLabel->GetWidgetName());
  this->Script("grid %s -row 0 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelNumberOfProcessorsEntry->GetWidgetName());
	
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelIterationsRestartLabel->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelIterationsRestartEntry->GetWidgetName());
	
	
  this->Script("grid %s -row 2 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelRelativeErrorLabel->GetWidgetName());
  this->Script("grid %s -row 2 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelRelativeErrorEntry->GetWidgetName());
	
	
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelAbsErrorLabel->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelAbsErrorEntry->GetWidgetName());
		
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelMaxNumberIterLabel->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelMaxNumberIterEntry->GetWidgetName());

  this->Script("grid %s -row 5 -column 0 -padx 2 -pady 2 -sticky w", this->ParallelPrecondLabel->GetWidgetName());
  this->Script("grid %s -row 5 -column 1 -padx 2 -pady 2 -sticky w", this->ParallelPrecondCombo->GetWidgetName());

}



//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateMonolithicWidgetsCallback()
{
	// ADD
	this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelGeneralRLS->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->comboGeneralRLS->GetWidgetName());	



	this->Script("grid remove %s", this->ParallelIterativeSolverFrame->GetWidgetName());
	



	this->Script("grid %s -row 6 -column 0 -padx 2 -pady 2 -stick we", this->ModelconfigMonolithicIterationsFrame->GetWidgetName());


  this->Script("grid remove %s", this->labelIter3D->GetWidgetName());
  this->Script("grid remove %s",this->entryIter3D->GetWidgetName());
  this->Script("grid remove %s", this->labelSuPar3D->GetWidgetName());
  this->Script("grid remove %s", this->scaleSuPar3D->GetWidgetName());
	
	
	this->Script("grid remove %s", this->labelIter1D->GetWidgetName());
  this->Script("grid remove %s",this->entryIter1D->GetWidgetName());
  this->Script("grid remove %s", this->labelSuPar1D->GetWidgetName());
  this->Script("grid remove %s", this->scaleSuPar1D->GetWidgetName());


 	this->Script("grid remove %s", this->SegregatedConfigFrame->GetWidgetName());


//	this->Script("grid remove %s", this->SolverConfigSegregated3DFrame->GetWidgetName());
//  this->Script("grid remove %s", this->SolverConfigSegregated1DFrame->GetWidgetName());

  this->Script("grid remove %s", this->SolverConfigGeneralSegregatedFrame->GetWidgetName());



  // visualizado somente se tipo de acoplamento é monolitico
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->SolverConfigMonoFrame->GetWidgetName());
//  this->Script("pack %s -pady 2 -padx 2 -fill both -expand yes -anchor n",
 //              this->SolverConfigMonoFrame->GetWidgetName());



  //this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelGeneralRLS->GetWidgetName());
  //this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->comboGeneralRLS->GetWidgetName());
  

}

//------------------------------------------------------------------
void vtkPVHMCoupledModelConfigurationWidget::UpdateSegregatedWidgetsCallback()
{
	this->Script("grid remove %s", this->ModelconfigMonolithicIterationsFrame->GetWidgetName());
	
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter3D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter3D->GetWidgetName());
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar3D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar3D->GetWidgetName());
	
	
	this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->labelIter1D->GetWidgetName());
  this->Script("grid %s -row 3 -column 1 -padx 2 -pady 2",this->entryIter1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->labelSuPar1D->GetWidgetName());
  this->Script("grid %s -row 4 -column 1 -columnspan 2 -sticky w -padx 2 -pady 2", this->scaleSuPar1D->GetWidgetName());
	
	
  this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->SolverConfigSegregated1DFrame->GetWidgetName());
  this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->SolverConfigSegregated3DFrame->GetWidgetName());
	
	
	
	this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we", this->SegregatedConfigFrame->GetWidgetName());
	
	
	// valores padroes para os campos tipo de solver 
	
 	this->Script("grid remove %s", this->SolverConfigMonoFrame->GetWidgetName());
	this->Script("grid remove %s", this->ParallelIterativeSolverFrame->GetWidgetName());
	
	
	this->Script("grid remove %s", 	this->labelGeneralRLS->GetWidgetName());
	this->Script("grid remove %s", 	this->comboGeneralRLS->GetWidgetName());

	this->GeneralSolverTypeCombo->SetValue("Sequential");
	
	
	
  this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->SolverConfigGeneralSegregatedFrame->GetWidgetName());
	
 	
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateLinearSegregatedWidgetsCallback()
{
	this->entryIterNonLinearSegregated->ReadOnlyOn();
	this->scaleSuParNonLinearSegregated->GetScale()->SetStateToDisabled();
	this->scaleSuParNonLinearSegregated->GetEntry()->ReadOnlyOn();
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateNonLinearSegregatedWidgetsCallback()
{
	this->entryIterNonLinearSegregated->ReadOnlyOff();
	this->scaleSuParNonLinearSegregated->GetScale()->SetStateToNormal();
	this->scaleSuParNonLinearSegregated->GetEntry()->ReadOnlyOff();
}

//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::CreateComponentsInteraction()
	
{	
	char buffer[1024];
	
	// 1D: quando o tipo de formulacao é LEAST SQUARE desabilitar o campo Elastic Pressure de Converngence Parameters
	sprintf(buffer, "%s config -state normal; %s config -state normal", 
	this->entryReEP->GetWidgetName(), this->entryCeEP->GetWidgetName());
  this->radioLSV->SetCommand(NULL, buffer);
    
  this->radioLS->SetVariableName(this->radioLSV->GetVariableName());
  sprintf(buffer, "%s config -state disabled; %s config -state disabled",
	 this->entryReEP->GetWidgetName(), this->entryCeEP->GetWidgetName());
  this->radioLS->SetCommand(NULL, buffer);
  
  
     sprintf(buffer, "%s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;" 
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled;"
  " %s config -state disabled",
  this->entryRefDisplacementX->GetWidgetName(),
  this->entryRefDisplacementY->GetWidgetName(),
	this->entryRefDisplacementZ->GetWidgetName(),
	this->entryConvDisplacementX->GetWidgetName(),
	this->entryConvDisplacementY->GetWidgetName(),
	this->entryConvDisplacementZ->GetWidgetName(),
	this->radioICYes->GetWidgetName(),
	this->radioICNo->GetWidgetName());
	
	this->radioRigid->SetCommand(NULL, buffer);
	  
	
	sprintf(buffer, "%s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal;"
	" %s config -state normal",
  this->entryRefDisplacementX->GetWidgetName(),
  this->entryRefDisplacementY->GetWidgetName(),
	this->entryRefDisplacementZ->GetWidgetName(),
	this->entryConvDisplacementX->GetWidgetName(),
	this->entryConvDisplacementY->GetWidgetName(),
	this->entryConvDisplacementZ->GetWidgetName(),
	this->radioICYes->GetWidgetName(),
	this->radioICNo->GetWidgetName());
	
	this->radioCompliant->SetCommand(NULL, buffer);
  
  

}
//------------------------------------------------------------------
void vtkPVHMCoupledModelConfigurationWidget::UpdateCassonRelatedWidgets()
{
	this->entryViscosity3D->ReadOnlyOn();
  this->entryAVisco->ReadOnlyOff();
	this->entryLimitStress->ReadOnlyOff();
	this->comboRegularParameter->SetStateToNormal();
	this->radioICNo->SetStateToDisabled();
	this->radioICYes->SelectedStateOn();

}

// ----------------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateNewtonRelatedWidgets()
{
	this->entryViscosity3D->ReadOnlyOff();
  this->entryAVisco->ReadOnlyOn();
	this->entryLimitStress->ReadOnlyOn();
	this->comboRegularParameter->SetStateToDisabled();

	this->radioICNo->SetStateToNormal();
	this->radioICYes->SelectedStateOn();
}



//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::GetValuesFromConfigWindow()
{
////	this->CalculateTimeParameters();
////	
////	if ( (this->GetAditivity()) && (this->entryPena->GetValueAsDouble()==0) )
////		{
////		vtkKWMessageDialog::PopupMessage(
////	      this->GetPVApplication(), this->GetParentWindow(), "Solver Configurations Tab Error", 
////	      "Aditivity value not set",
////	      vtkKWMessageDialog::ErrorIcon);
////	  return;   
////		}
////	
////		
//// 	if (this->GetFinalTime() <  this->GetInitialTime())
////		{
////		vtkKWMessageDialog::PopupMessage(
////	      this->GetPVApplication(), this->GetParentWindow(), "General Configurations Tab Error", 
////	      "Initial Time greater than Final Time. Please redefine Final Time value",
////	      vtkKWMessageDialog::ErrorIcon);
////	  return;
////		}
////	      
////	if (this->GetFinalTime() ==  this->GetInitialTime())
////		{
////	    vtkKWMessageDialog::PopupMessage(
////	      this->GetPVApplication(), this->GetParentWindow(), "General Configurations Tab Error", 
////	      "Initial Time equal Final Time. Please redefine Final Time value",
////	      vtkKWMessageDialog::ErrorIcon);
////		return;
////		}  
////	 	this->SetDoubleParameters();
////	  this->SetIntParameters();
////	 	//this->Withdraw();
////	 	
////	
	time_t rawtime;
 	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	char buffer[1024];
	
	for (int i = 0; i < 1024; ++i)
		{
		buffer[i]=0;
		}
	
	
		if (this->entryFilesPath->GetValue())
				{
				// acessar as propriedades para passar os parametros inteiros	- oriundos da tela de configuracao
				vtkSMIntVectorProperty *propInt = vtkSMIntVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("IntParam"));    
    		if (propInt)
    			{ 
   				//		0 - Least Square / 1= ViscoElatsic (1D)
					//		1 - compliant deformable model (0= no/1 =yes) (3D)
					//		2 - blood constitutive law (1= newton / 0 = casson) (3D)	
					//		3 - 1D artery wall law
					//		4 - 3D artery wall law			
					//		5 - File Output
					//		6 - Screen Output
    			propInt->SetElement(0,this->radioLSV->GetSelectedState());
    			propInt->SetElement(1,this->radioCompliant->GetSelectedState());
    			propInt->SetElement(2,this->radioBloodLawNewton->GetSelectedState());
    			propInt->SetElement(3,this->comboArterialWallLaw1D->GetValueIndex(this->comboArterialWallLaw1D->GetValue()+1));
    			propInt->SetElement(4,this->comboArterialWallLaw3D->GetValueIndex(this->comboArterialWallLaw3D->GetValue()+1));
    			propInt->SetElement(5,this->entryFileOutput->GetValueAsInt()); 
    			propInt->SetElement(6,this->entryScreenOutput->GetValueAsInt()); 

					//		* General Configuration
					//		7 - Coupling scheme (0= monolhitic / 1 = normal segregated / 2 = super segregated)
					//		8 - Segregated scheme (0=linear/1= non linear)
					//		9 - Segregated scheme iterations
					//		10 -Monolithic iterations 
					//		11 -3D iterations
					//		12 -1D iterarion
					propInt->SetElement(7,this->GetCouplingTypeSelected());
					propInt->SetElement(8,this->SegregatedNonLinearRadio->GetSelectedState());
					propInt->SetElement(9,this->entryIterNonLinearSegregated->GetValueAsInt()); 
					propInt->SetElement(10,this->entryIter->GetValueAsInt()); 
					propInt->SetElement(11,this->entryIter3D->GetValueAsInt()); 
					propInt->SetElement(12,this->entryIter1D->GetValueAsInt()); 

					//		13 -renumbering	
					//		14 -1D Aditivity 0= no/1 =yes			
					//		15 -3D Aditivity 0= no/1 =yes			
					//		16 -Incremental Version 0= no/1 =yes (3D)
					//		17 -Monolithic resolution of linear system		
					//		18 -Monolithic fill parameter
					//		19 -Monolithic iterations before restart
					//		20 -Monolithic krylov subspce
					propInt->SetElement(13,this->entryRenu->GetValueAsInt());
					propInt->SetElement(14,this->radioYes1D->GetSelectedState());
					propInt->SetElement(15,this->radioYes3D->GetSelectedState());
					propInt->SetElement(16,this->radioICYes->GetSelectedState());
					propInt->SetElement(17,this->GetResolutionSolverType(0));
					propInt->SetElement(18,this->entryFParam->GetValueAsInt());
					propInt->SetElement(19,this->entryIterationsBeforeRestart->GetValueAsInt());
					propInt->SetElement(20,this->entrykrylov->GetValueAsInt());

  			  //		21 -1D resolution of linear system		
					//		22 -1D fill parameter
					//		23 -1D iterations before restart
					//		24 -1D krylov subspce
					//		25 -3D resolution of linear system		
					//		26 -3D fill parameter
					//		27 -3D iterations before restart
					//		28 -3D krylov subspce
					//    29 -Segregated coupling order (0 == 1d-3d / 1==3d-1d)
 					propInt->SetElement(21,this->GetResolutionSolverType(1));
					propInt->SetElement(22,this->entryFParam1D->GetValueAsInt());
					propInt->SetElement(23,this->entryIterationsBeforeRestart1D->GetValueAsInt());
					propInt->SetElement(24,this->entrykrylov1D->GetValueAsInt());
					propInt->SetElement(25,this->GetResolutionSolverType(3));
					propInt->SetElement(26,this->entryFParam3D->GetValueAsInt());
					propInt->SetElement(27,this->entryIterationsBeforeRestart3D->GetValueAsInt());
					propInt->SetElement(28,this->entrykrylov3D->GetValueAsInt());
					propInt->SetElement(29,this->Segregated3d1dRadio->GetSelectedState());
					
					//		*** Parallel Solver Options ***
					//		30 - Solver type (0: Sequential 1: Parallel)
					//		31 - Number of Processors
					//		32 - GMRES Iterations before restart
					//		33 - Maximum number of iterations
					//		34 - Parallel Preconditioning (0: None 1: BJacobi)
 					propInt->SetElement(30,this->GetSolverType());
					propInt->SetElement(31,this->ParallelNumberOfProcessorsEntry->GetValueAsInt());
					propInt->SetElement(32,this->ParallelIterationsRestartEntry->GetValueAsInt());
					propInt->SetElement(33,this->ParallelMaxNumberIterEntry->GetValueAsInt());
					propInt->SetElement(34,this->GetParallelPreConditioning());

					//    35 - Compatibilize models (0: No 1: yes)
					//    36 - Compatibilize mode (0: Linear 1: Constant)
					propInt->SetElement(35,this->radioCompatibilizeYes->GetSelectedState());
					propInt->SetElement(36,this->GetCompatibilizationMode());
					
					
					
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess IntParam filter property.");	  	
  				return;
	  			}


				propInt = vtkSMIntVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("SelectedSolverFiles"));    
    		if (propInt)
    			{ 
    			if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
    				{		
	    			propInt->SetElement(0,1);
	    			propInt->SetElement(1,1);
	    			propInt->SetElement(2,1);
	    			propInt->SetElement(3,1);
    				}
    			else
    				{
	    			propInt->SetElement(0,this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->GetSelectedState());
	    			propInt->SetElement(1,this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState());
	    			propInt->SetElement(2,this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState());
	    			propInt->SetElement(3,this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState());
    				}
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess SelectedSolverFiles filter property.");	  	
  				return;
	  			}		



	  		
	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	  			
				//		- Model Configuration
				//		0 - Density (1D)
				//		1 -	Viscosity (1D)
				//		2	- Velocity (1D)
				//		3 - Fluid Density -3D
				//		4 - Viscosity -3D
				//		5 - Asymptotic Viscosity -3D
				//		6 - Limit Stress -3D
				//		7 - Regularization Parameters -3D
	  		vtkSMDoubleVectorProperty *propDouble = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("ModelConfigParam"));    
    		if (propDouble)
    			{ 
    			 propDouble->SetElement(0,this->entryDensity1D->GetValueAsDouble()); 
    			 propDouble->SetElement(1,this->entryViscosity1D->GetValueAsDouble()); 
    			 propDouble->SetElement(2,this->thumbVel->GetValue());
    			 propDouble->SetElement(3,this->entryDensity3D->GetValueAsDouble()); 
    			 propDouble->SetElement(4,this->entryViscosity3D->GetValueAsDouble()); 
    			 propDouble->SetElement(5,this->entryAVisco->GetValueAsDouble());
    			 propDouble->SetElement(6,this->entryLimitStress->GetValueAsDouble()); 
    			 propDouble->SetElement(7,this->comboRegularParameter->GetValueAsDouble());
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess ModelConfigParam filter property.");	  	
  				return;
	  			}		
	  		
				//		0 - time step
				//		1 - initial time
				//		2 - final time
	  		vtkSMDoubleVectorProperty *propDouble2 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("TimeParam"));    
    		if (propDouble2)
    			{
    			propDouble2->SetElement(0,this->entryTimeStep->GetValueAsDouble()); 
    			propDouble2->SetElement(1,this->entryInitialTime->GetValueAsDouble()); 
    			propDouble2->SetElement(2,this->entryFinalTime->GetValueAsDouble()); 
    			
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess TimeParam filter property.");	  	
  				return;
	  			}		
	  		
				// --Segregated SubRelaxation Param size 1
				// 0 - sub relaxation param
	  		vtkSMDoubleVectorProperty *propDouble3 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("SegregatedSubRelaxation"));    
    		if (propDouble3)
    			{
    			propDouble3->SetElement(0,this->scaleSuParNonLinearSegregated->GetValue()); 
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess TimeParam filter property.");	  	
  				return;
	  			}		

				// Segregated SubRelaxation Param size 1
				// 0 - sub relaxation param
	  		vtkSMDoubleVectorProperty *propDouble4 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("MonolithicSubRelaxation"));    
    		if (propDouble4)
    			{
    			propDouble4->SetElement(0, this->scaleSuPar->GetValue()); 
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess TimeParam filter property.");	  	
  				return;
	  			}
	  			
				//		0 - reference value Vel. X
				//		1 - reference value Vel. Y
				//		2 - reference value Vel. Z
				//		3 - reference value Pressure
				//		4 - reference Displacement X
				//		5 -	reference Displacement y
				//		6 -	reference Displacement Z
				//		7 - Conv. Error  Vel. X
				//		8 - Conv. Error  Vel. Y
				//		9 - Conv. Error  Vel. Z
				//		10 -Conv. Error  Pressure
				//		11 -Conv. Error Displacement X
				//		12 -Conv. Error Displacement y
				//		13 -Conv. Error Displacement Z
				//		14 - subrelaxation param
	  		vtkSMDoubleVectorProperty *propDouble5 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("3DConvergenceParam"));    
    		if (propDouble5)
    			{
					propDouble5->SetElement(0, this->entryRefVelocityX->GetValueAsDouble());
					propDouble5->SetElement(1, this->entryRefVelocityY->GetValueAsDouble());
					propDouble5->SetElement(2, this->entryRefVelocityZ->GetValueAsDouble());
					propDouble5->SetElement(3, this->entryRefPressure3D->GetValueAsDouble());
					propDouble5->SetElement(4, this->entryRefDisplacementX->GetValueAsDouble());
					propDouble5->SetElement(5, this->entryRefDisplacementY->GetValueAsDouble());
					propDouble5->SetElement(6, this->entryRefDisplacementZ->GetValueAsDouble());
					propDouble5->SetElement(7, this->entryConvVelocityX->GetValueAsDouble());
					propDouble5->SetElement(8, this->entryConvVelocityY->GetValueAsDouble());
					propDouble5->SetElement(9, this->entryConvVelocityZ->GetValueAsDouble());
					propDouble5->SetElement(10, this->entryConvPressure3D->GetValueAsDouble());
					propDouble5->SetElement(11, this->entryConvDisplacementX->GetValueAsDouble());
					propDouble5->SetElement(12, this->entryConvDisplacementY->GetValueAsDouble());
					propDouble5->SetElement(13,	this->entryConvDisplacementZ->GetValueAsDouble());
					propDouble5->SetElement(14, this->scaleSuPar3D->GetValue());
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess 3DConvergenceParam filter property.");	  	
  				return;
	  			}	  					

				//		0 - reference value Flow
				//		1 - reference value Elastic Pressure
				//		2 - reference value Elastic Area
				//		3 - reference value Pressure
				//		4 - convergence value Flow
				//		5 - convergence value Elastic Pressure
				//		6 - convergence value Elastic Area
				//		7 - convergence value Pressure
				//		8 - subrelaxation param
	  		
	  		
	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	  		
	  		
	  		vtkSMDoubleVectorProperty *propDouble6 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("1DConvergenceParam"));    
    		if (propDouble6)
    			{
					propDouble6->SetElement(0, this->entryReFl->GetValueAsDouble());
					propDouble6->SetElement(1, this->entryReEP->GetValueAsDouble());
					propDouble6->SetElement(2, this->entryReAr->GetValueAsDouble());
					propDouble6->SetElement(3, this->entryRePr->GetValueAsDouble());
					propDouble6->SetElement(4, this->entryCeFl->GetValueAsDouble());
					propDouble6->SetElement(5, this->entryCeEP->GetValueAsDouble());
					propDouble6->SetElement(6, this->entryCeAr->GetValueAsDouble());
					propDouble6->SetElement(7, this->entryCePr->GetValueAsDouble());
					propDouble6->SetElement(8, this->scaleSuPar1D->GetValue());
    			}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess 1DConvergenceParam filter property.");	  	
  				return;
	  			}	
//	  			
   			//	0 - 1D Theta Scheme
   			//  1 - 1D Penalization
   			//  2 - 3D Theta Scheme
   			//  3 - 3D Penalization
   			//  4 - 3D Fluid Artificial Compressibility
				//	5 - Monolithic convergence error
				//	6 - Monolithic Drop tolerance
				//	7 - 1D Segregated convergence error
				//	8 - 1D Segregated Drop tolerance
				//	9 - 3D Segregated Convergence error
				//	10- 3D Segregated Drop tolerance
	  		vtkSMDoubleVectorProperty *propDouble7 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("SolverConfigParam"));    
    		if (propDouble7)
    			{
					propDouble7->SetElement(0, this->thumbTheta1D->GetValue());
					propDouble7->SetElement(1, this->entryPena1D->GetValueAsDouble());
					propDouble7->SetElement(2, this->thumbTheta3D->GetValue());
					propDouble7->SetElement(3, this->entryPena3D->GetValueAsDouble());
					propDouble7->SetElement(4, this->TetraCompressibilityEntry->GetValueAsDouble());
					propDouble7->SetElement(5, this->entryConvergenceError->GetValueAsDouble());
					propDouble7->SetElement(6, this->entryDispTolerance->GetValueAsDouble());
					propDouble7->SetElement(7, this->entryConvergenceError1D->GetValueAsDouble());
					propDouble7->SetElement(8, this->entryDispTolerance1D->GetValueAsDouble());
					propDouble7->SetElement(9, this->entryConvergenceError3D->GetValueAsDouble());
					propDouble7->SetElement(10,this->entryDispTolerance3D->GetValueAsDouble());
					propDouble7->SetElement(11, this->ParallelRelativeErrorEntry->GetValueAsDouble());
					propDouble7->SetElement(12,this->ParallelAbsErrorEntry->GetValueAsDouble());


   				}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess SolverConfigParam filter property.");	  	
  				return;
	  			}	
	  		
	  		vtkSMDoubleVectorProperty *propDouble8 = vtkSMDoubleVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("ScaleFactor3D"));    
    		if (propDouble8)
    			{
					propDouble8->SetElement(0, this->ScaleFactor3DEntry->GetValueAsDouble());
   				}
    		else
	  			{
					vtkErrorMacro(<<"Cannot acess ScaleFactor3D filter property.");	  	
  				return;
	  			}	
	  		
	  		
	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	  			
	  		vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("LogFileName"));    
    		if (prop) 
    			{	
    			prop->SetElement(0,this->entryLogFile->GetValue());
    			}
	  		else
	  			{
					vtkErrorMacro(<<"Cannot acess LogFileName filter property.");	  	
  				return;
	  			}		

	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();

	  	
   	  	vtkSMStringVectorProperty *prop2 = vtkSMStringVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("CouplingInformation"));    
    		if (prop2)
    			{
    			prop2->SetElement(0,this->GetCouplingInfo());
    			}
	  		else
	  			{
					vtkErrorMacro(<<"Cannot acess CouplingInformation filter property.");	  	
  				return;
	  			}		
   
   	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();
		 	
			
				vtkSMStringVectorProperty *prop3 = vtkSMStringVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("GenerateSolverFiles"));    
    		if (prop3) 
    			prop3->SetElement(0,this->entryFilesPath->GetValue()); 
	  		else
	  			{
					vtkErrorMacro(<<"Cannot acess GenerateSolverFiles filter property.");	  	
  				return;
	  			}		
		 		
		 		
	  		// atualiza objetos no servidor	  
				this->GetPVSource()->GetProxy()->UpdateVTKObjects();
		 		
		 		
		  	// criando backup dos arquivos gerados
		  	//chdir(this->entryFilesPath->GetValue());
		  	
		  	
		  	int error= 0;
		  	vtkSMIntVectorProperty *GenerationStatus = vtkSMIntVectorProperty::SafeDownCast(
				this->GetPVSource()->GetProxy()->GetProperty("GenerationStatus"));
				
		  	
				
				if(GenerationStatus)
					{
	  			// atualiza somente a prop. "GenerationStatus"
	  			this->GetPVSource()->GetProxy()->UpdatePropertyInformation(GenerationStatus);
	  			error = GenerationStatus->GetElement(0); // se geracao ok - variavel error = 1 
					}
				else
  			vtkErrorMacro(<<"Cannot create \"GenerationStatus\" filter property.");	
		  	
		  	
		  	vtksys::SystemTools::ChangeDirectory(this->entryFilesPath->GetValue());
				
//				system("cp Mesh.txt Mesh000.txt");
//				system("cp IniFile.txt IniFile000.txt");
//		  	system("cp Param.txt Param000.txt");

				if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
					{
					// opcao para gerar todos arquivos
					system("cp Mesh.txt Mesh000.txt");
					system("cp IniFile.txt IniFile000.txt");
		  		system("cp Param.txt Param000.txt");
					}
				else
					{
					// gera somente um arquivo	
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState())
						system("cp Mesh.txt Mesh000.txt");
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState())
						system("cp Param.txt Param000.txt");
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState())
						system("cp IniFile.txt IniFile000.txt");
					}	
		  	
		  	
		  	char buffer2[1000];
		  	for (int i = 0; i < 1000; ++i)
		  		buffer2[i]=0;

				char buffer3[20];
		  	for (int i = 0; i < 20; ++i)
		  		buffer3[i]=0;
								
				
		  	strcat(buffer2, this->entryFilesPath->GetValue());
		  	//strcat(buffer2, "/Mesh.txt");

				if (this->SelectedSolverFilesRadioButtonSet->GetWidget(0)->GetSelectedState())
			  	{
			  	strcat(buffer2, "/Basparam.txt"); // se todos arquivos do solver serao gerados, testar somente se existe Basparam
			  	strcat(buffer3, "Files");
			  	}
				else
					{
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(1)->GetSelectedState())
						{
						strcat(buffer2, "/Basparam.txt");
						strcat(buffer3, "Basparam.txt");
						}

					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(2)->GetSelectedState())
						{
						strcat(buffer2, "/Mesh.txt");
						strcat(buffer3, "Mesh.txt");
						}
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(3)->GetSelectedState())
						{
						strcat(buffer2, "/Param.txt");
						strcat(buffer3, "Param.txt");
						}
					
					if (this->SelectedSolverFilesRadioButtonSet->GetWidget(4)->GetSelectedState())
						{
						strcat(buffer2, "/IniFile.txt");
						strcat(buffer3, "IniFile.txt");
						}
					}



		  	
		  	if (vtksys::SystemTools::FileExists(buffer2) && error)
		  		{
//					if (!strcmp(this->textFilesGenerationStatus->GetWidget()->GetText(), "Files not generated yet"))
//		  			sprintf(buffer, "*Files generated at %s - %s",this->entryFilesPath->GetValue(), asctime (timeinfo)); 
//		  		else
//		  			sprintf(buffer, "*Files generated at %s -%s \n %s\n",this->entryFilesPath->GetValue(), asctime (timeinfo), this->textFilesGenerationStatus->GetWidget()->GetText()); 
//		  		}
//		  	else
//		  		sprintf(buffer, "*ERROR! Could not generate files at %s -%s",this->entryFilesPath->GetValue(), asctime (timeinfo) ); 
					if (!strcmp(this->textFilesGenerationStatus->GetWidget()->GetText(), "Files not generated yet"))
		  			sprintf(buffer, "*%s generated at %s - %s",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo)); 
		  		else
		  			sprintf(buffer, "*%s generated at %s - %s \n",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo)); 
		  		}
		  	else
		  		sprintf(buffer, "*ERROR! Could not generate %s at %s -%s",buffer3, this->entryFilesPath->GetValue(), asctime (timeinfo) ); 



		  	
		  	
  			this->textFilesGenerationStatus->GetWidget()->SetText(buffer);	 	
			
			}
}

//------------------------------------------------------------------

int vtkPVHMCoupledModelConfigurationWidget::GetResolutionSolverType(int Dimension)
{
	/* Sequential Solver Types
 *  
0   / Direct
100 / CG - Conjugate Gradient Method
101 / BCG - Bi-Conjugate Gradient Method
102 / DBCG - BCG with partial pivoting
103 / CGNR - Conjugate Gradient method (Normal Residual equation)
104 / BCGSTAB - BCG stabilized
105 / TFQMR - Transpose-Free Quasi-Minimum Residual method
106 / FOM - Full Orthogonalization Method
107 / GMRES - Generalized Minimum RESidual method
108 / FGMRES - Flexible version of Generalized Minimum RESidual method
109 / DQGMRES - Direct versions of Quasi Generalize Minimum RESidual method
110 / CGS - Conjugate Gradient Square method
 * 
 * */
	
	
	vtkKWComboBox *comboRLS = NULL;
	
	if (!Dimension)
		comboRLS = this->comboGeneralRLS; // monolitico ou paralelo
	else // segregated
		{	
		if (Dimension==1) //1D
			comboRLS = this->comboRLS1D;
		else //3D
			comboRLS = this->comboRLS3D;
		}
	
	
	
	if (!strcmp(comboRLS->GetValue(), "Direct"))
		return 0;


	if (!strcmp(comboRLS->GetValue(), "CG"))
		return 100;
	
	if (!strcmp(comboRLS->GetValue(), "BCG"))
		return 101;



	if (!strcmp(comboRLS->GetValue(), "DBCG"))
		return 102;



	if (!strcmp(comboRLS->GetValue(), "CGNR"))
		return 103;


	if (!strcmp(comboRLS->GetValue(), "BCGSTAB"))
		return 104;



	if (!strcmp(comboRLS->GetValue(), "TFQMR"))
		return 105;


	if (!strcmp(comboRLS->GetValue(), "FOM"))
		return 106;



	if (!strcmp(comboRLS->GetValue(), "GMRES"))
		return 107;



	if (!strcmp(comboRLS->GetValue(), "FGMRES"))
		return 108;



	if (!strcmp(comboRLS->GetValue(), "DQGMRES"))
			return 109;


	if (!strcmp(comboRLS->GetValue(), "CGS"))
		return 110;


return -1;
	
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::SetCouplingInfo(const char *CouplingInfo)
{
	
	for (int i = 0; i < 1024; ++i) 
		{
		this->CouplingInfo[i]=0;
		}
	
	strcpy(this->CouplingInfo, CouplingInfo);	
}



//------------------------------------------------------------------


void vtkPVHMCoupledModelConfigurationWidget::SetGenerationFilesPath()
{
	char Temp[50] ;
	sprintf(Temp, "Generate %s Solver Files at", this->ModelType);

	this->ChooseDirectoryDialog->SetTitle(Temp);
	this->ChooseDirectoryDialog->ChooseDirectoryOn();
	this->ChooseDirectoryDialog->Invoke();
	if (this->ChooseDirectoryDialog->GetLastPath())
		  this->entryFilesPath->SetValue(this->ChooseDirectoryDialog->GetLastPath());
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::SetModelType(const char *ModelName)
{
	strcpy (ModelType,ModelName);
	char buffer[1024];
  sprintf(buffer, "%s Solver Files Generation Setup", this->ModelType); 
  this->SetTitle(buffer);
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::CalculateTimeParameters()
{
	
	if (!this->entryNumberOfTimeSteps->GetValueAsInt())
		{
//		vtkKWMessageDialog::PopupMessage(
//	      this->GetPVApplication(), this->GetParentWindow(), "Time Parameters error", 
//	      "Number of Time Steps can not be ZERO. Please redefine Number of Time Steps value",
//	      vtkKWMessageDialog::ErrorIcon);
//	  return;
		this->entryFinalTime->SetValueAsDouble(this->entryNumberOfCardiacCycles->GetValueAsInt() * this->entryCardiacCycleTime->GetValueAsDouble() );

    double FinalTime = this->entryFinalTime->GetValueAsDouble();
    double InitialTime = this->entryInitialTime->GetValueAsDouble();
    double TimeStep = this->entryTimeStep->GetValueAsDouble();


    this->entryNumberOfTimeSteps->SetValueAsDouble((FinalTime - InitialTime)/TimeStep);
   
		//this->entryTimeStep->SetValueAsDouble((this->entryFinalTime->GetValueAsDouble() - this->entryInitialTime->GetValueAsDouble())/this->entryNumberOfTimeSteps->GetValueAsInt());
		}
	else
		{	

		this->entryFinalTime->SetValueAsDouble(this->entryNumberOfCardiacCycles->GetValueAsInt() * this->entryCardiacCycleTime->GetValueAsDouble() );
		this->entryTimeStep->SetValueAsDouble((this->entryFinalTime->GetValueAsDouble() - this->entryInitialTime->GetValueAsDouble())/this->entryNumberOfTimeSteps->GetValueAsInt());
		}
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateRLSRelatedWidgets(int mode)
{
	
	vtkKWComboBox *comboRLS;
	vtkKWEntry *entrykrylov;
	vtkKWEntry *entryIterationsBeforeRestart;
	vtkKWEntry *entryConvergenceError;
	vtkKWEntry *entryDispTolerance;
	vtkKWEntry *entryFParam;
 
 
  if (!mode)
  	{
  	comboRLS = this->comboGeneralRLS;	
		entrykrylov = this->entrykrylov;
		entryIterationsBeforeRestart =this->entryIterationsBeforeRestart;
		entryConvergenceError = this->entryConvergenceError;
		entryDispTolerance = this->entryDispTolerance;
		entryFParam = this->entryFParam;
  	}
  
  if (mode == 1)
  	{
  	comboRLS = this->comboRLS1D;	
		entrykrylov = this->entrykrylov1D;
		entryIterationsBeforeRestart =this->entryIterationsBeforeRestart1D;
		entryConvergenceError = this->entryConvergenceError1D;
		entryDispTolerance = this->entryDispTolerance1D;
		entryFParam = this->entryFParam1D;
  	}	

  if (mode ==3)
  	{
  	comboRLS = this->comboRLS3D;	
		entrykrylov = this->entrykrylov3D;
		entryIterationsBeforeRestart =this->entryIterationsBeforeRestart3D;
		entryConvergenceError = this->entryConvergenceError3D;
		entryDispTolerance = this->entryDispTolerance3D;
		entryFParam = this->entryFParam3D;
  	}
  
  
 	if (!strcmp(comboRLS->GetValue(), "Direct"))
		{
 		entrykrylov->ReadOnlyOn();
 		entryIterationsBeforeRestart->ReadOnlyOn();
 		entryConvergenceError->ReadOnlyOn();
 		entryDispTolerance->ReadOnlyOn();
	  entryFParam->ReadOnlyOn();
		}
	else
		{
		entrykrylov->ReadOnlyOff();
 		entryIterationsBeforeRestart->ReadOnlyOff();
 		entryConvergenceError->ReadOnlyOff();
 		entryDispTolerance->ReadOnlyOff();
	  entryFParam->ReadOnlyOff();
		}
	
}

//------------------------------------------------------------------
char *vtkPVHMCoupledModelConfigurationWidget::GetCouplingInfo()
{
 return this->CouplingInfo;	
	
}

//------------------------------------------------------------------

int vtkPVHMCoupledModelConfigurationWidget::GetCouplingTypeSelected()
{

	for (int i = 0; i < 3; ++i)
		{
		if (this->SegregationTypeRadioButtonSet->GetWidget(i)->GetSelectedState())
			return i;
		}
	
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateSolverOptions()
{
	//*********************************************
	// para evitar que entre 2 vezes seguidas aqui -- o metodo associado a um combo box é chamado quando o valor do combo é modificado e quando o usuario
	// tira o mouse da regiao do combo (out focus) entao este callback é chamado 2 vezes.
	if (strcmp(this->GeneralSolverTypeCombo->GetValue(), this->CurrentSolverType)) // true se string diferentes
		// atualiza tipo de solver atualmente selecionado
		strcpy(this->CurrentSolverType, this->GeneralSolverTypeCombo->GetValue()); 	
	else // strings iguais
		return; // se esta for a segunda vez que entra aqui, entao retorna
	//*********************************************
	
	if (!strcmp(this->GeneralSolverTypeCombo->GetValue(), "Sequential"))
		{
		this->entryLogFile->SetValue("Log.txt");
  	this->entryLogFile->SetReadOnly(0);
	  
	  // se segregado
	  if (this->GetCouplingTypeSelected()) 
	  	{
	  	this->Script("grid remove %s", this->labelGeneralRLS->GetWidgetName());
  		this->Script("grid remove %s", this->comboGeneralRLS->GetWidgetName());		
	  	
	  	
	  	this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->SolverConfigGeneralSegregatedFrame->GetWidgetName());	
	  	this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->SolverConfigSegregated1DFrame->GetWidgetName());
  		this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->SolverConfigSegregated3DFrame->GetWidgetName());
	  	}
	  else // se monolitico
	  	{
			this->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->SolverConfigMonoFrame->GetWidgetName());
	  	}	
	
	  this->comboGeneralRLS->DeleteAllValues();
	  this->comboGeneralRLS->AddValue("BCG"); 
		this->comboGeneralRLS->AddValue("BCGSTAB");
	  this->comboGeneralRLS->AddValue("CG");
		this->comboGeneralRLS->AddValue("CGNR");
		this->comboGeneralRLS->AddValue("CGS");
	  this->comboGeneralRLS->AddValue("DBCG"); 
	  this->comboGeneralRLS->AddValue("Direct");
		this->comboGeneralRLS->AddValue("DQGMRES");
		this->comboGeneralRLS->AddValue("FGMRES");
		this->comboGeneralRLS->AddValue("FOM");
		this->comboGeneralRLS->AddValue("GMRES");
		this->comboGeneralRLS->AddValue("TFQMR");
		this->comboGeneralRLS->SetValue("CGS");

	  this->Script("grid remove %s", this->ParallelIterativeSolverFrame->GetWidgetName());
		}
		
	else // parallel solver
		{	 
	  this->comboGeneralRLS->DeleteAllValues();
	  this->comboGeneralRLS->AddValue("CGS"); 
	  this->comboGeneralRLS->AddValue("GMRES"); 
		this->comboGeneralRLS->SetValue("CGS");

		char aux[50];
		sprintf(aux, "run.p%d.log", this->ParallelNumberOfProcessorsEntry->GetValueAsInt());	
  	this->entryLogFile->SetValue(aux);
  	this->entryLogFile->SetReadOnly(1);
  
	  // se é monolitico
	  if (!this->GetCouplingTypeSelected())
	  	{
	  	this->Script("grid remove %s", this->SolverConfigMonoFrame->GetWidgetName());
	  	}
	  else // segregado
	  	{
	  	 this->Script("grid remove %s", this->SolverConfigGeneralSegregatedFrame->GetWidgetName());
	  	 
	  	 this->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->labelGeneralRLS->GetWidgetName());
  		 this->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->comboGeneralRLS->GetWidgetName());	
	  	}
	  
	  this->Script("grid remove %s", this->SolverConfigSegregated1DFrame->GetWidgetName());
	 	this->Script("grid remove %s", this->SolverConfigSegregated3DFrame->GetWidgetName());
	  this->Script("grid %s -row 4 -column 0 -padx 2 -pady 2 -stick we -columnspan 2", this->ParallelIterativeSolverFrame->GetWidgetName());
		}
}

//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::UpdateRLSParallel()
{
	if (!strcmp(this->comboGeneralRLS->GetValue(), "GMRES"))
		{
		this->ParallelIterationsRestartEntry->ReadOnlyOff();		
		this->ParallelIterationsRestartLabel->SetState(1);
		}
	else
		{
		this->ParallelIterationsRestartEntry->ReadOnlyOn();
		this->ParallelIterationsRestartLabel->SetState(0);	
		}	
}

//------------------------------------------------------------------

int vtkPVHMCoupledModelConfigurationWidget::GetSolverType()
{
//	if (!strcmp(this->GeneralSolverTypeCombo->GetValue(), "Sequential"))
//		return 0;
//	else
//		return 1; 	

  return this->GeneralSolverTypeCombo->GetValueIndex(this->GeneralSolverTypeCombo->GetValue()); 
}

//------------------------------------------------------------------

int vtkPVHMCoupledModelConfigurationWidget::GetParallelPreConditioning()
{
//	if (!strcmp(this->ParallelPrecondCombo->GetValue(), "None"))
//		return 0;
//	else
//		return 1; 	

  return this->ParallelPrecondCombo->GetValueIndex(this->ParallelPrecondCombo->GetValue()); 

}

//------------------------------------------------------------------

int vtkPVHMCoupledModelConfigurationWidget::GetCompatibilizationMode()
{
//	if (!strcmp(this->comboCompatibilizeType->GetValue(), "Constant extension"))
//		return 1;
//	else 
//		return 0;	


return this->comboCompatibilizeType->GetValueIndex(this->comboCompatibilizeType->GetValue()); 

}

//------------------------------------------------------------------
void vtkPVHMCoupledModelConfigurationWidget::ShowCompatibilizationMode()
{	
  if (this->radioCompatibilizeYes->GetSelectedState())
  	{
  	this->comboCompatibilizeType->SetStateToNormal();
  	this->labelCompatibilizeType->SetState(1);   	  	 
  	}
	else
		{
		this->comboCompatibilizeType->SetStateToDisabled();
		this->labelCompatibilizeType->SetState(0);
		}
}
//------------------------------------------------------------------

void vtkPVHMCoupledModelConfigurationWidget::ParallelNumberOfProcessorsCallBack()
{
	char aux[50];
	sprintf(aux, "run.p%d.log", this->ParallelNumberOfProcessorsEntry->GetValueAsInt());	
  this->entryLogFile->SetValue(aux);
  this->entryLogFile->SetReadOnly(1);
}
//------------------------------------------------------------------
