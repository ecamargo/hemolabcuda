#ifndef _vtkPVHMCoupledModelConfigurationWidget_h_
#define _vtkPVHMCoupledModelConfigurationWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWApplication.h"
#include "vtkKWWindow.h"
#include "vtkPVApplication.h"
#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWPushButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkKWUserInterfaceManager.h"
#include "vtkKWUserInterfacePanel.h"
#include "vtkKWEntry.h"
#include "vtkKWComboBox.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWThumbWheel.h"
#include <vtksys/SystemTools.hxx>
#include <vtksys/CommandLineArguments.hxx>
#include "vtkKWFrameWithLabel.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkKWTopLevel.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWComboBoxWithLabel.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWTreeWithScrollbars.h"
#include "vtkKWTree.h"
#include "vtkPVWindow.h"
#include "vtkPVSource.h"
#include "vtkKWText.h"
#include "vtkKWTextWithScrollbars.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkKWRadioButtonSet.h"

class vtkPVApplication;

class VTK_EXPORT vtkPVHMCoupledModelConfigurationWidget : public vtkKWTopLevel 
{
public:
  static vtkPVHMCoupledModelConfigurationWidget *New();
  vtkTypeRevisionMacro(vtkPVHMCoupledModelConfigurationWidget,vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent){};
  

	// Description: 
  // 
  virtual void Create(vtkKWApplication *app);
	// Description: 
  // 
  vtkPVApplication* GetPVApplication();
  
	// Description: 
  // 
  void SetPvWindow(	vtkPVWindow *PVWindow)
		{ 	this->PVWindow = PVWindow; }
	
	
	// Description: 
  // 
	vtkPVSource *GetPVSource()
 	{return this->PVSource; }

	// Description: 
  // 
 	void SetPVSource(vtkPVSource *PVSource)
 	{this->PVSource = PVSource; }
	
	// Description: 
  // 
	void CreateModelConfig1D(vtkKWApplication *app); 
	// Description: 
  // 
	void CreateModelConfig3D(vtkKWApplication *app); 
	
	// Description: 
  // 
	void CreateTimeComponents(vtkKWApplication *app); 
	
	// Description: 
  // 
	void CreateConvParam3D(vtkKWApplication *app); 
	
	// Description: 
  // 
	void CreateConvParam1D(vtkKWApplication *app); 
	
	// Description: 
  // 
	void CreateSolverConfig1D3D(vtkKWApplication *app); 
	
	// Description: 
  // 
	void CreateSolverAdditionalConfig1D(vtkKWApplication *app);

	// Description: 
  // 
	void CreateSolverAdditionalConfig3D(vtkKWApplication *app);	
	
	// Description: 
  // 
	void CreateMiscParam(vtkKWApplication *app); 


	// Description: 
  // 
	void CreateMonolithicSolverConfig(vtkKWApplication *app); 
	
	
	// Description: 
  // 
	void CreateSegregated3DSolverConfig(vtkKWApplication *app);

	// Description: 
  // 
	void CreateSegregated1DSolverConfig(vtkKWApplication *app);  
	
	
	// Description: 
  // 
	void CreateParallelSolverConfig(vtkKWApplication *app); 
	
	// Description: 
	// Callback methods
	void UpdateMonolithicWidgetsCallback();

	// Description: 
	// Callback methods
	void UpdateSegregatedWidgetsCallback();

	// Description: 
	// Callback methods
	void UpdateLinearSegregatedWidgetsCallback();

	// Description: 
	// Callback methods
	void UpdateNonLinearSegregatedWidgetsCallback();
	
	// Description: 
  // 
	void CreateComponentsInteraction();
	
	// Description: 
  // Callback method
	void UpdateCassonRelatedWidgets();

	// Description: 
  // 
	void UpdateNewtonRelatedWidgets();
	
	// Description: 
  // this is the method where the properties are updated with the values entered by the user
	void GetValuesFromConfigWindow();

	// Description: 
  // 
	int GetResolutionSolverType(int Dimension);
	
	// Description: 
  // 
	void SetCouplingInfo(const char *CouplingInfo);
	
	// Description: 
  //  Invokes a window where the user can select the path where the solver files will be generated
	void SetGenerationFilesPath();
	
	// Description: 
	// set a string with the name of the model used in this interface
	void SetModelType(const char *ModelName);
	
	
	// Description: 
	//
	void CalculateTimeParameters();
	
	// Description: 
	// update the widgets that are related with the combo Resolution Linear of System
	// the ivar mode controls if the currenbt combo is monolithic, segregated 1D or segregated 3D
	// o monolithic 
  // 1-Segregated 1D
  // 3- Segregated 3D
	void UpdateRLSRelatedWidgets(int mode);
	
	// Description:
	// return coupling char array information
	char *GetCouplingInfo();
	
	// Description:
	// return the type of segregation scheme selected
	int GetCouplingTypeSelected();
	
	// Description: 
	// Callback method
	void UpdateSolverOptions();
	
	// Description:
	// Callback method
	void UpdateRLSParallel();
	
	// Description:
	// returns the type of solver currently selected: 0:Squequential 1: Parallel
	int GetSolverType();
	
	// Description:
	// returns the Preconditioning method if Parallel solver is selected: 0: None 1: BJAcobi 
	int GetParallelPreConditioning();
	
	
	// Description:
	// Returns 0 if Constant extension - 1 if Linear
	int GetCompatibilizationMode();
	
	// Description:
	// Callback method that controls state of comboCompatibilizeType and labelCompatibilizeType components
	void ShowCompatibilizationMode();
	
	// Description:
	// Callback method that updates the log file name in Parallel script
	void ParallelNumberOfProcessorsCallBack();
	
//***************************************************************************************************** 
//***************************************************************************************************** 
//***************************************************************************************************** 

protected:
  vtkPVHMCoupledModelConfigurationWidget();
  virtual ~vtkPVHMCoupledModelConfigurationWidget();
  
  
	vtkPVWindow *PVWindow;
  
  vtkPVSource *PVSource;
  
  
  vtkKWFrameWithLabel *Modelconfig1DFrame;
  vtkKWFrameWithLabel *Modelconfig3DFrame;
  
  // Description:
  // coupling scheme KWComponents
  vtkKWLabel *CouplingSchemeLabel;
  vtkKWRadioButton *CouplingSchemeMonoRadio;
  vtkKWRadioButton *CouplingSchemeSegregatedRadio;
  //********************************************
  
	// Description:
  // frame Coupling Scheme
  vtkKWFrameWithLabel *CouplingSchemeFrame;

	// Description:
  // iterations and subrelaxation 1D frame
  vtkKWFrameWithLabel *ModelconfigMonolithicIterationsFrame;
  
  
  // Description:
  // iterations when the coupling is monolithic
	vtkKWLabel *labelIter;
 	vtkKWEntry *entryIter;
	vtkKWLabel *labelSuPar;
  vtkKWScaleWithEntry *scaleSuPar; 
  
  // Description:
  // KWComponents used when we have a segregated scheme selected
  vtkKWLabel *SegretatedOrderLabel;
  vtkKWRadioButton *Segregated1d3dRadio;
  vtkKWRadioButton *Segregated3d1dRadio;

  vtkKWLabel *SegretatedSchemeTypeLabel;
  vtkKWRadioButton *SegregatedLinearRadio;
  vtkKWRadioButton *SegregatedNonLinearRadio;
  //********************************************
  
  vtkKWFrameWithLabel *SegregatedConfigFrame;
  
	// Description:
	// number of iterations and sub-relaxation parameter
	// when Non linear scheme Segregated coupling is selected 
	vtkKWLabel *labelIterNonLinearSegregated;
 	vtkKWEntry *entryIterNonLinearSegregated;
	vtkKWLabel *labelSuParNonLinearSegregated;
  vtkKWScaleWithEntry *scaleSuParNonLinearSegregated; 
  
	// Description:
  //additional Solver Param 1d
  vtkKWFrameWithLabel *AdditionalParamSolver1DFrame;

	// Description:
  //additional Solver Param 3d
  vtkKWFrameWithLabel *AdditionalParamSolver3DFrame;
 
  // Description:
  // General frame Solver Config   
  vtkKWFrameWithLabel *SolverConfig1D3DFrame;
  
  // Description:
	// Monolithic fame Solver Config   
	vtkKWFrameWithLabel *SolverConfigMonoFrame;
	
	// Description:
	// General frame Solver Config   
	vtkKWFrameWithLabel *SolverConfigSegregated3DFrame;
	
	// Description:
	// General frame Solver Config   
	vtkKWFrameWithLabel *SolverConfigSegregated1DFrame;
	
	// Description:
	// Segregated Solver Config
	vtkKWFrameWithLabel *SolverConfigGeneralSegregatedFrame;
  
  // Description:
  // monolithic solver config
  	vtkKWFrameWithLabel *frameIterativeSolver;

	vtkKWLabel *labelIterationsBeforeRestart;
	vtkKWEntry *entryIterationsBeforeRestart;

  vtkKWLabel *labelConvergenceError;
	vtkKWEntry *entryConvergenceError;
  
  vtkKWLabel *labelkrylov;
	vtkKWEntry *entrykrylov;
	
	vtkKWFrameWithLabel *framePreconditioning;
	
  vtkKWLabel *labelFParam;
	vtkKWEntry *entryFParam;
  
  vtkKWLabel *labelDispTolerance;
  vtkKWEntry *entryDispTolerance;


// ***********************************************************************
//***********************************************************************

	vtkKWFrameWithScrollbar *FrameScroll;
 	vtkKWFrame     *WindowFrame;
  vtkKWNotebook *notebook;

  vtkKWWidget *mc;
  vtkKWWidget *gc;
  vtkKWWidget *sc;
  vtkKWWidget *mi;


  vtkKWFrameWithLabel *SolverTimeConfigFrame;
  vtkKWFrameWithLabel *ConvergenceParametersFrame1D;
  vtkKWFrameWithLabel *ConvergenceParametersFrame3D;
  
  // Description:
  // Model Config 1D
  
  vtkKWLabel *labelForm;
  vtkKWRadioButton *radioLSV;
  vtkKWRadioButton *radioLS;
  vtkKWLabel *labelDensity1D;
  vtkKWEntry *entryDensity1D;
  vtkKWLabel *labelViscosity1D;
  vtkKWEntry *entryViscosity1D;
  vtkKWLabel *labelArterialWallLaw1D;
  vtkKWComboBox *comboArterialWallLaw1D;
  vtkKWLabel *labelVelocity;
  vtkKWThumbWheel *thumbVel;
  
  
  // Description:
  //Model Config 3D
	vtkKWLabel *labelModel;
	vtkKWRadioButton *radioCompliant;
	vtkKWRadioButton *radioRigid;
	vtkKWLabel *labelDensity3D;
	vtkKWEntry *entryDensity3D;
	vtkKWLabel *labelBloodLaw;
	vtkKWRadioButton *radioBloodLawNewton;
	vtkKWRadioButton *radioBloodLawCasson;
	vtkKWLabel *labelViscosity3D;
	vtkKWEntry *entryViscosity3D;
	vtkKWLabel *labelAVisco;
	vtkKWEntry *entryAVisco;
	vtkKWLabel *labelLimitStress;
	vtkKWEntry *entryLimitStress;
	vtkKWLabel *labelRegularParameter;
	vtkKWComboBox *comboRegularParameter;
	vtkKWLabel *labelArterialWallLaw3D;
	vtkKWComboBox *comboArterialWallLaw3D;
  //
  
  
  
  // time param
	vtkKWLabel *ResumeLabel;
	vtkKWRadioButton *radioResumeYes;
	vtkKWRadioButton *radioResumeNo;
	vtkKWLabel *labelNumberOfTimeSteps;
	vtkKWEntry *entryNumberOfTimeSteps;
	
	vtkKWLabel *labelTimeStep;
	vtkKWEntry *entryTimeStep;
	vtkKWLabel *labelInitialTime;
	vtkKWEntry *entryInitialTime;
	vtkKWLabel *labelNumberOfCardiacCycles;
	vtkKWEntry *entryNumberOfCardiacCycles;
	
	vtkKWLabel *labelCardiacCycleTime;
	vtkKWEntry *entryCardiacCycleTime;
	vtkKWLabel *labelFinalTime;
	vtkKWEntry *entryFinalTime;
	vtkKWPushButton *CalculateTimeStepButton;
  //
  
  
	// Description:
  // 3D converegence parameters frame
	vtkKWLabel *labelVariable3D;
	vtkKWLabel *labelRefVal3D;
	vtkKWLabel *labelConEr3D;

  vtkKWLabel *labelVelocityX;
  vtkKWLabel *labelVelocityY;
  vtkKWLabel *labelVelocityZ;
  vtkKWLabel *labelPressure3D;
  vtkKWLabel *labelDisplacementX;
  vtkKWLabel *labelDisplacementY;
  vtkKWLabel *labelDisplacementZ;
  vtkKWEntry *entryRefVelocityX;
  vtkKWEntry *entryRefVelocityY;
  vtkKWEntry *entryRefVelocityZ;
  vtkKWEntry *entryRefPressure3D;
  vtkKWEntry *entryConvPressure3D;
  vtkKWEntry *entryRefDisplacementX;
  vtkKWEntry *entryRefDisplacementY;
  vtkKWEntry *entryRefDisplacementZ;
  vtkKWEntry *entryConvVelocityX;
  vtkKWEntry *entryConvVelocityY;
  vtkKWEntry *entryConvVelocityZ;
  vtkKWEntry *entryConvDisplacementX;
  vtkKWEntry *entryConvDisplacementY;
  vtkKWEntry *entryConvDisplacementZ;
  vtkKWLabel *labelIter3D;
 	vtkKWEntry *entryIter3D;
	vtkKWLabel *labelSuPar3D;
  vtkKWScaleWithEntry *scaleSuPar3D; 
	//  
  
  // Description:
  // 1d conv
	vtkKWLabel *labelVariable1D;
	vtkKWLabel *labelRefVal1D;
	vtkKWLabel *labelConEr1D;

	vtkKWLabel *labelPress;
	vtkKWEntry *entryRePr;
	vtkKWEntry *entryCePr;
	vtkKWLabel *labelFlux;
	vtkKWLabel *labelElPre;
	vtkKWLabel *labelArea;
	vtkKWEntry *entryReFl;
	vtkKWEntry *entryCeFl;
	vtkKWEntry *entryReEP;
	vtkKWEntry *entryCeEP;
	vtkKWEntry *entryReAr;
	vtkKWEntry *entryCeAr;
	
	vtkKWLabel *labelIter1D;
 	vtkKWEntry *entryIter1D;
	vtkKWLabel *labelSuPar1D;
  vtkKWScaleWithEntry *scaleSuPar1D; 
	//	
  
  // Description:
  // 1d solver
	vtkKWLabel *labelTethaScheme1D;
	vtkKWThumbWheel *thumbTheta1D;
	
	vtkKWLabel *labelAditi1D;
	vtkKWRadioButton *radioYes1D;
	vtkKWRadioButton *radioNo1D;
	
	vtkKWLabel *labelPena1D;
	vtkKWEntry *entryPena1D;
  //
  
	// Description:
	//3d solver
	vtkKWLabel *labelTethaScheme3D;
	vtkKWThumbWheel *thumbTheta3D;
	vtkKWLabel *TetraCompressibilityLabel;
	vtkKWEntry *TetraCompressibilityEntry;
	vtkKWLabel *labelAditi3D;
	vtkKWRadioButton *radioYes3D;
	vtkKWRadioButton *radioNo3D;

	vtkKWLabel *labelPena3D;
	vtkKWEntry *entryPena3D;

	vtkKWLabel *labelIncrementalVersion;
	vtkKWRadioButton *radioICYes;
	vtkKWRadioButton *radioICNo;
	// 
  
  // Description:
  //misc options
	vtkKWLabel *labelRenu;
	vtkKWEntry *entryRenu;
	  
	vtkKWLabel *labelFileOutput;
	
	vtkKWEntry *entryFileOutput;
	
	vtkKWLabel *labelScreenOutput;
	
	vtkKWEntry *entryScreenOutput;
	vtkKWLabel *labelLogFile;
	
	vtkKWEntry *entryLogFile;
	vtkKWLabel *labelFilesPath;
	vtkKWEntry *entryFilesPath;
	vtkKWPushButton *pushButtonBrowse;
	vtkKWPushButton *CommitChangesButtonPage1;
	vtkKWTextWithScrollbars *textFilesGenerationStatus;
	//  
  
  // Description:
  // 1D solver config
  vtkKWLabel *labelSolverType1D;
	vtkKWComboBox *comboSolverType1D;

  vtkKWLabel *labelRLS1D;
  vtkKWComboBox *comboRLS1D;

	vtkKWFrameWithLabel *frameIterativeSolver1D;

	vtkKWLabel *labelIterationsBeforeRestart1D;
	vtkKWEntry *entryIterationsBeforeRestart1D;

  vtkKWLabel *labelConvergenceError1D;
	vtkKWEntry *entryConvergenceError1D;
  
  vtkKWLabel *labelkrylov1D;
	vtkKWEntry *entrykrylov1D;
	
	vtkKWFrameWithLabel *framePreconditioning1D;
	
  vtkKWLabel *labelFParam1D;
	vtkKWEntry *entryFParam1D;
  
  vtkKWLabel *labelDispTolerance1D;
  vtkKWEntry *entryDispTolerance1D;
  //
  
  // Description:
  //3D solbver config
	vtkKWLabel *labelSolverType3D;
	vtkKWComboBox *comboSolverType3D;

  vtkKWLabel *labelRLS3D;
  vtkKWComboBox *comboRLS3D;

	vtkKWFrameWithLabel *frameIterativeSolver3D;

	vtkKWLabel *labelIterationsBeforeRestart3D;
	vtkKWEntry *entryIterationsBeforeRestart3D;

  vtkKWLabel *labelConvergenceError3D;
	vtkKWEntry *entryConvergenceError3D;
  
  vtkKWLabel *labelkrylov3D;
	vtkKWEntry *entrykrylov3D;
	
	vtkKWFrameWithLabel *framePreconditioning3D;
	
  vtkKWLabel *labelFParam3D;
	vtkKWEntry *entryFParam3D;
  
  vtkKWLabel *labelDispTolerance3D;
  vtkKWEntry *entryDispTolerance3D;
	

	// Description: 
	// store information about coupling points as a char array
	char CouplingInfo[1024];
	
	// Description: 
	// variavel que define o caso em que os arquivos estao sendo configurados
  // pode ser 1D, 3D (sozinho), e acoplamento
  // pode ter os seguintes valores:
  // ********
  // 1D Model
  // 3D Model 
  // Coupled Model
  char ModelType[20];
   
	// Description: 
	// Dialog used by the user to choose a directory where Solver input files will be generated
  vtkKWLoadSaveDialog *ChooseDirectoryDialog;

	vtkKWRadioButtonSet *SegregationTypeRadioButtonSet;
	
	// Description: 
	// Opcoes do Solver Paralelo no modo monolitico
	vtkKWFrameWithLabel *ParallelIterativeSolverFrame;
	
	vtkKWLabel *ParallelNumberOfProcessorsLabel;
	vtkKWEntry *ParallelNumberOfProcessorsEntry;
  
  vtkKWLabel *ParallelIterationsRestartLabel;
  vtkKWEntry *ParallelIterationsRestartEntry;
  
  vtkKWLabel *ParallelRelativeErrorLabel;
	vtkKWEntry *ParallelRelativeErrorEntry;
  
  vtkKWLabel *ParallelAbsErrorLabel;
  vtkKWEntry *ParallelAbsErrorEntry;
  
  vtkKWLabel *ParallelMaxNumberIterLabel;
  vtkKWEntry *ParallelMaxNumberIterEntry;
  vtkKWLabel *ParallelPrecondLabel;
  vtkKWComboBox *ParallelPrecondCombo;
  //--------------------------------------
  
  // Campo Solver Type
  vtkKWFrameWithLabel *GeneralSolverTypeFrame;
  vtkKWLabel *GeneralSolverTypeLabel;
  vtkKWComboBox *GeneralSolverTypeCombo;
	//
	
	// Description: 
	// tipo de RLS quando o solver type é paralello no modo segregado
	vtkKWLabel *labelGeneralRLS;
	vtkKWComboBox *comboGeneralRLS;
	
	
	
	vtkKWLabel *ScaleFactor3DLabel;
	vtkKWEntry *ScaleFactor3DEntry;

	
	
	vtkKWLabel *CompatibilizeLabel;
	vtkKWRadioButton *radioCompatibilizeYes;
	vtkKWRadioButton *radioCompatibilizeNo;
	

	vtkKWLabel *labelCompatibilizeType;
	vtkKWComboBox *comboCompatibilizeType;
  
  // Description: 
  // char array used to avoid double callback call by associated ComboBox.
  char CurrentSolverType[20];
  
    
  vtkKWRadioButtonSet *SelectedSolverFilesRadioButtonSet;
  
  vtkKWLabel *SelectedSolverFilesLabel;
  
private:  
  vtkPVHMCoupledModelConfigurationWidget(const vtkPVHMCoupledModelConfigurationWidget&); // Not implemented
  void operator=(const vtkPVHMCoupledModelConfigurationWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMCoupledModelConfigurationWidget_h_*/
