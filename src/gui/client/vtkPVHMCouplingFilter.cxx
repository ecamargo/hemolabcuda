#include "vtkKWPushButton.h"  
#include "vtkKWFrameWithLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWComboBox.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWListBoxWithScrollbars.h"
#include "vtkKWIcon.h" 
#include "vtkKWLoadSaveButton.h"
#include "vtkKWToolbar.h"
#include "vtkCamera.h"
#include "vtkRenderer.h"
#include "vtkPVSelectionList.h"
#include "vtkPVHMCoupledModelConfigurationWidget.h"
#include "vtkIntArray.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkPVWidget.h"
#include "vtkIdList.h"
#include "vtkPVWidgetCollection.h"
#include "vtkCallbackCommand.h"
#include "vtkKWListBox.h"
#include "vtkSMSourceProxy.h"
#include "vtkPVColorMap.h"
#include "vtkPVWidgetCollection.h"
#include "vtkPVDataInformation.h"
#include "vtkPVRenderView.h"
#include "vtkKWMessageDialog.h"
#include "vtkPVDisplayGUI.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMUtils.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMRenderModuleProxy.h"


#include "vtkPVHMCouplingFilter.h"

vtkCxxRevisionMacro(vtkPVHMCouplingFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMCouplingFilter);

static void GetNodeCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
	
  vtkPVHMCouplingFilter* pv = reinterpret_cast<vtkPVHMCouplingFilter*>(sr);
	pv->GetStraightModelSelectedNodeId();
		   
}
 
//------------------------------------------------------------------------------
vtkPVHMCouplingFilter::vtkPVHMCouplingFilter()
{ 
	this->SetEnabled(1);
	this->Frame   = vtkKWFrameWithLabel::New();

	this->straightModelCamera = vtkCamera::New();
	this->straightModelCamera->ParallelProjectionOn();
	this->fullModelCamera = vtkCamera::New();

	//this->straightModelCoordinates 	= new double[3];
	this->straightModelCoordinates 	= NULL;
	
	//this->fullModelCoordinates 			= new double[3];
	this->fullModelCoordinates 			= NULL;
//	straightModelCoordinates[0] = 0;
//	straightModelCoordinates[1] = 0;
//	straightModelCoordinates[2] = 1;
//
//	fullModelCoordinates[0] = 0;
//	fullModelCoordinates[1] = 0;
//	fullModelCoordinates[2] = 0;

	this->First1DView				= true;
	this->ObserverAdded			= false;

  this->Input3DCombo = vtkPVSelectionList::New();

	this->SegmentLabel = vtkKWLabel::New();
	this->SegmentEntry = vtkKWEntry::New();

	this->NodeLabel = vtkKWLabel::New();
	this->NodeEntry = vtkKWEntry::New();

	this->GroupSelectionCombo = vtkKWComboBox::New();
	this->GroupSelectionLabel = vtkKWLabel::New();
	
  this->CouplingListBox 		 = vtkKWListBoxWithScrollbars::New();
  this->AddCouplingButton 	 = vtkKWPushButton::New();
  this->RemoveCouplingButton = vtkKWPushButton::New();	
  this->SaveCouplingButton	 = vtkKWPushButton::New();	
  
	this->ErrorMessageLabel 	 =	vtkKWLabel::New(); 
  
  this->ConfigureFilesGenerationButton = vtkKWPushButton::New();	
  this->CoupledModelConfigurationWidget = NULL;
  
  
  for (int i = 0; i < 100; ++i)
		CouplingInfo[i] = 0;
		
	this->CouplingInfoArray = vtkIntArray::New();
	this->CouplingInfoArray->SetNumberOfComponents(4);
  
  
  this->Current3dSelected = 0;
  
  this->Input1DSelectRadioButton = vtkKWRadioButton::New();
  
  this->StraightModelInputNumber = -1;
  this->MeshDataInputNumber = -1;
  
  this->Widget1DProxy = NULL;
  
  this->PVWidget = NULL;
  
  
 	this->ColorCode = NULL;
 	this->PointCounter = 0;
  
 
}


//------------------------------------------------------------------------------
vtkPVHMCouplingFilter::~vtkPVHMCouplingFilter()
{                  
	
//	if (straightModelCoordinates)
//		{
//		delete [] straightModelCoordinates;
//		straightModelCoordinates = NULL;
//		}
//	
//	if (fullModelCoordinates)
//		{
//		delete [] fullModelCoordinates;
//		fullModelCoordinates = NULL;	
//		}

	this->Frame->Delete();
	this->straightModelCamera->Delete();
	this->fullModelCamera->Delete();

  this->Input3DCombo->Delete();
  
	this->SegmentLabel->Delete();
	this->SegmentEntry->Delete();
	this->NodeLabel->Delete();
	this->NodeEntry->Delete();

	this->GroupSelectionCombo->Delete();
	this->GroupSelectionLabel->Delete();

  this->CouplingListBox->Delete();
  this->AddCouplingButton->Delete();
  this->RemoveCouplingButton->Delete();	
  this->SaveCouplingButton->Delete();

	this->ErrorMessageLabel->Delete();
  
  this->ConfigureFilesGenerationButton->Delete(); 
  
  if (this->CoupledModelConfigurationWidget)
  	{
  	this->CoupledModelConfigurationWidget->Delete(); 
    this->CoupledModelConfigurationWidget = NULL;
  	}
  	
	this->CouplingInfoArray->Delete();
	
	this->Input1DSelectRadioButton->Delete();
	
	if (this->Widget1DProxy)
		this->Widget1DProxy->RemoveAllCouplingActors();
		
		
	if (this->PVWidget)
		{
		this->PVWidget->RemoveObserver(CallbackCommand);
		this->PVWidget = NULL;
		}
	if (this->ColorCode)
		this->ColorCode->Delete();
	
}  

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::AddStraightModelObserver()
{                  
	// Pega as widgets do input 0 e procura pela widget do Straight Model
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetWidgets(); 
	vtkPVWidget *widget;
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));      
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)   
    	break;
  	}
	
  // Called every time left click happends on the viewport of the StraightModel Widget
  vtkCallbackCommand *LeftButtonCallback = vtkCallbackCommand::New();
  LeftButtonCallback->SetCallback(GetNodeCallback);
  
  CallbackCommand =  LeftButtonCallback;

  // Passes this because the executed method for callback is a vtkPVHMCouplingMethod
  LeftButtonCallback->SetClientData((void *)this);

  // Captures Left clicks on the StraightModelWidget
  if(widget)
  	{
	  PVWidget = widget;
	  widget->AddObserver(vtkCommand::WidgetModifiedEvent, LeftButtonCallback);
  	}
	else
  	vtkErrorMacro(<<"vtkPVHMCouplingFilter::AddStraightModelObserver() : Cannot found StraightModel Widget.");	
		
	// Remove callback   
  LeftButtonCallback->Delete();
  
  // Avoid adding another observer
	this->ObserverAdded	= true;  
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::Create(vtkKWApplication* app)
{
  this->pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();
 	
 	this->kwApp = app;
  vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	this->Window = pvApp->GetMainWindow();

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << "already created");
    return;
    }
  
  // Call the superclass to create the whole widget
  this->Superclass::Create(app);  
  this->Frame->SetParent(this);
  this->Frame->Create(pvApp);
  this->Frame->SetLabelText("Coupling Filter");

  this->Input1DSelectRadioButton->SetParent(this->Frame->GetFrame());
  this->Input1DSelectRadioButton->Create(pvApp);
  this->Input1DSelectRadioButton->SetBalloonHelpString("Toggle to 1D Input Visualization");
  this->Input1DSelectRadioButton->SetText("1D Input");
  this->Input1DSelectRadioButton->IndicatorVisibilityOff();
  this->Input1DSelectRadioButton->SetHighlightThickness(0);
  //this->Input1DSelectRadioButton->SelectedStateOn();
  this->Input1DSelectRadioButton->SetCommand(this, "Select1DInputCallback");


  this->Input3DCombo->SetParent(this->Frame->GetFrame());
  this->Input3DCombo->SetOptionWidth(15);
  this->Input3DCombo->Create(pvApp);
  this->Input3DCombo->AddItem("Select 3D Input", 0);
  this->Input3DCombo->SetModifiedCommand(this->GetTclName(), "Select3DInputCallback");
  this->Input3DCombo->SetBalloonHelpString("Toggle to 3D Input Visualization");

	this->SegmentLabel->SetParent(this->Frame->GetFrame());
	this->SegmentLabel->Create(pvApp);
  this->SegmentLabel->SetText("Segment ID: ");	
	this->SegmentEntry->SetParent(this->Frame->GetFrame());
	this->SegmentEntry->Create(pvApp);
  this->SegmentEntry->SetBalloonHelpString("Segment Id where the selected node is located"); 	
	this->SegmentEntry->SetValue("None");  
	this->SegmentEntry->SetReadOnly(1); 

	this->NodeLabel->SetParent(this->Frame->GetFrame());
	this->NodeLabel->Create(pvApp);
  this->NodeLabel->SetText("Node Number: ");	
	this->NodeEntry->SetParent(this->Frame->GetFrame());
	this->NodeEntry->Create(pvApp);
  this->NodeEntry->SetBalloonHelpString("Selected Node Number"); 	
	this->NodeEntry->SetValue("None");
	this->NodeEntry->SetReadOnly(1); 
	//this->NodeEntry->SetWidth(4);

	this->GroupSelectionCombo->SetParent(this->Frame->GetFrame());
  this->GroupSelectionCombo->Create(pvApp);
	this->GroupSelectionCombo->SetCommand(this, "UpdateGroupsCallback"); 
  this->GroupSelectionCombo->SetWidth(4);
  this->GroupSelectionCombo->SetValue("None");
  this->GroupSelectionCombo->SetReadOnly(1);
  this->GroupSelectionCombo->SetBalloonHelpString("Select the 3D cover group which will be connected with 1D model");     

 	this->GroupSelectionLabel->SetParent(	this->Frame->GetFrame());
  this->GroupSelectionLabel->Create(pvApp);
  this->GroupSelectionLabel->SetText("Group Selection: ");

 	this->ErrorMessageLabel->SetParent(	this->Frame->GetFrame());
  this->ErrorMessageLabel->Create(pvApp);
  this->ErrorMessageLabel->SetText("");

  // Connections (terminal, group and type)
  this->CouplingListBox->SetParent(this->Frame->GetFrame());
  this->CouplingListBox->Create(pvApp);
  this->CouplingListBox->GetWidget()->SetSelectionModeToSingle();
  this->CouplingListBox->GetWidget()->SetWidth(40);
  this->CouplingListBox->GetWidget()->SetHeight(5);
  this->CouplingListBox->GetWidget()->SetSingleClickCommand(this, "ListBoxItemSelected");

	// Coupling Buttons
  this->AddCouplingButton->SetParent(this->Frame->GetFrame());
  this->AddCouplingButton->Create(pvApp);
  //this->AddCouplingButton->SetWidth(20);
  this->AddCouplingButton->SetHeight(1);
  this->AddCouplingButton->SetText("Add Coupling Point");
  this->AddCouplingButton->SetBalloonHelpString("Add current coupling point to list"); 
  this->AddCouplingButton->SetCommand(this, "AddCoupling");

  this->RemoveCouplingButton->SetParent(this->Frame->GetFrame());
  this->RemoveCouplingButton->Create(pvApp);
  //this->RemoveCouplingButton->SetWidth(10);
  this->RemoveCouplingButton->SetHeight(1);
  this->RemoveCouplingButton->SetText("Remove All");
  this->RemoveCouplingButton->SetBalloonHelpString("Remove all coupling points"); 
  this->RemoveCouplingButton->SetCommand(this, "RemoveCoupling");  

  this->SaveCouplingButton->SetParent(this->Frame->GetFrame());
  this->SaveCouplingButton->Create(pvApp);
  //this->SaveCouplingButton->SetWidth(5);
  this->SaveCouplingButton->SetHeight(1);
  this->SaveCouplingButton->SetText("Save");
  this->SaveCouplingButton->SetCommand(this, "SaveCoupling");  

  this->ConfigureFilesGenerationButton->SetParent(this->Frame->GetFrame());
  this->ConfigureFilesGenerationButton->Create(pvApp);
  //this->ConfigureFilesGenerationButton->SetWidth(5);
  this->ConfigureFilesGenerationButton->SetBalloonHelpString("Open the configuration window where the Coupled Model solver files can be configured and generated "); 	
  this->ConfigureFilesGenerationButton->SetHeight(1);
  this->ConfigureFilesGenerationButton->SetText("Config Solver Files Generation");
  this->ConfigureFilesGenerationButton->SetCommand(this, "ConfigureSolverFilesGenerationCallback");  
}

// -----------------------------------------------------------------------------
void vtkPVHMCouplingFilter::PlaceComponents()
{
	this->Script("pack %s -fill x -expand t -pady 2",  
  this->Frame->GetWidgetName());

  this->Script("grid %s %s -sticky news -padx 2 -pady 2",
  this->Input1DSelectRadioButton->GetWidgetName(),
  this->Input3DCombo->GetWidgetName());	

	this->Script("grid %s %s -sticky news -padx 2 -pady 2",
  this->SegmentLabel->GetWidgetName(),
  this->SegmentEntry->GetWidgetName());

	this->Script("grid %s %s -sticky news -padx 2 -pady 2",
  this->NodeLabel->GetWidgetName(),
  this->NodeEntry->GetWidgetName());

	char ComboText[50];
	for (int i=this->NumberOfWallGroups ; i < this->NumberOfCoverGroups + this->NumberOfWallGroups ; i++) // 0 é o grupo da superfície (removido)
		{
		sprintf(ComboText, "%d", i);
		this->GroupSelectionCombo->AddValue(ComboText);
		}
	  
	this->Script("grid %s %s -sticky news -padx 2 -pady 2",
	this->GroupSelectionLabel->GetWidgetName(),
	this->GroupSelectionCombo->GetWidgetName());

	this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
  this->ErrorMessageLabel->GetWidgetName());


  this->Script("grid %s %s -sticky news -padx 2 -pady 2",  
  this->AddCouplingButton->GetWidgetName(),
  this->RemoveCouplingButton->GetWidgetName());

	
  this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
  this->CouplingListBox->GetWidgetName());    


	this->Script("grid %s -sticky news -padx 2 -pady 2 -columnspan 2",
	this->ConfigureFilesGenerationButton->GetWidgetName());    

}

// ----------------------------------------------------------------------------
void vtkPVHMCouplingFilter::Select3DInputCallback()
{
  
  // se o valor selecionado é zero -- apagar o numero de grupos e selecionar o valor "None"
  if (!this->Input3DCombo->GetCurrentValue())
  	{
  	this->GroupSelectionCombo->DeleteAllValues();
  	this->GroupSelectionCombo->SetValue("None");
  	return;
  	}
  
//  // Atualizando valor da variavel no Widget do modelo 1D que é utilizada na colorizcao das esfereas de acoplamento
//	if (this->Widget1DProxy)
//  	this->Widget1DProxy->SetFullModelCode(this->Input3DCombo->GetCurrentValue() - 1);
  
  this->Input1DSelectRadioButton->SelectedStateOff();
  
  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("OutputSelect"));    
		
		
	// Reset group information when input 2 (id 1) is selected
	if(this->Input3DCombo->GetCurrentValue() > 0)
	  this->GroupSelectionCombo->SetValue("None");		
	
	
	if(prop) 
  	prop->SetElement(0, this->Input3DCombo->GetCurrentValue());
  else
  	vtkErrorMacro(<<"Cannot create Group filter property.");	  	


	this->MeshDataInputNumber = this->Input3DCombo->GetCurrentValue();
	
	
	this->UpdateWidgetInfo();
	
	this->Current3dSelected = this->Input3DCombo->GetCurrentValue();
	
	char ComboText[50];
	this->GroupSelectionCombo->DeleteAllValues();
	//for (int i=1 ; i < this->NumberOfCoverGroups+1 ; i++) // 0 é o grupo da superfície (removido)
	for (int i=this->NumberOfWallGroups ; i < this->NumberOfCoverGroups + this->NumberOfWallGroups ; i++) // 0 é o grupo da superfície (removido)
		{
		sprintf(ComboText, "%d", i);
		this->GroupSelectionCombo->AddValue(ComboText);
		}

  this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();  
  this->GetPVSource()->PreAcceptCallback();
}	

// ----------------------------------------------------------------------------

void vtkPVHMCouplingFilter::Select1DInputCallback()
{
  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("OutputSelect"));    
		
	if(prop) 
  	prop->SetElement(0, 0);
  else
  	vtkErrorMacro(<<"Cannot create Group filter property.");	  	

	this->UpdateWidgetInfo();
  this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();  
  this->GetPVSource()->PreAcceptCallback();
}	

// ----------------------------------------------------------------------------

void vtkPVHMCouplingFilter::UpdateGroupsCallback()
{
	if (this->Input1DSelectRadioButton->GetSelectedState())
		{
	  // se o modelo 3D nao estiver na tela; mostrar o mesmo
	  
	  //this->Input1DSelectRadioButton->SelectedStateOff();
	  
	  vtkSMIntVectorProperty *propInt = vtkSMIntVectorProperty::SafeDownCast(
			this->GetPVSource()->GetProxy()->GetProperty("OutputSelect"));    
			
		if(propInt) 
	  	propInt->SetElement(0, this->Input3DCombo->GetCurrentValue());
	  else
	  	vtkErrorMacro(<<"Cannot create Group filter property.");	  	
	
	
		this->MeshDataInputNumber = this->Input3DCombo->GetCurrentValue();
		
		
		this->UpdateWidgetInfo();
		
		this->Current3dSelected = this->Input3DCombo->GetCurrentValue();
		
	  this->ModifiedCallback(); 
	  this->GetPVSource()->GetProxy()->UpdateVTKObjects();  
	  this->GetPVSource()->PreAcceptCallback();
		}
	
	// se tiver o 1D selecionado nao atualizar os grupos do 3D
	if (this->Input1DSelectRadioButton->GetSelectedState())
		{
		this->Input1DSelectRadioButton->SelectedStateOff();
		return;
		}
	
	// Get selected group from the interface
	this->SelectedGroup = this->GroupSelectionCombo->GetValueAsInt();
	
	// Set property with previous information
  vtkSMIntVectorProperty *prop = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("SelectGroup"));    
  if (prop) 
		prop->SetElement(0,this->GroupSelectionCombo->GetValueAsInt());
	else
	  {
		vtkErrorMacro(<<"Cannot create Group filter property.");	  	
  	return;
	  }
	  
	// Activate "SelectedGroup" to be the group to be colored
	this->GetPVSource()->ColorByArray("SelectedGroup", 4); // 4 é o id para células

	// Set Inicial color of the Color Bar
	this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->StartColorButtonCallback(255,255,255);

	// Set Final color of the Color Bar
	this->GetPVSource()->GetPVApplication()->GetMainWindow()->GetPVColorMap("SelectedGroup", 1)->EndColorButtonCallback(255,0,0); 	

  this->ModifiedCallback();     
	
	// Se o método abaixo for chamado, o botão accept deixa de ficar verde, mas pisca a interface  
	this->GetPVSource()->PreAcceptCallback();	
}	

//------------------------------------------------------------------------------
int vtkPVHMCouplingFilter::GetStraightModelSelectedNodeId()
{
	// Get widget collection from the right input
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetWidgets();  
  int segmentId = -1, nodeId = -1;

  // Find the Straight Model Widget from the collection and get the selected element
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    vtkPVWidget *widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));
      
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)
	    {
			vtkPVHMStraightModelWidget *hmWidget = vtkPVHMStraightModelWidget::SafeDownCast(widget);
			
			vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(hmWidget->GetWidgetProxy());
			
			Widget1DProxy = wProxy;
			
			
			vtkSMIntVectorProperty* typeOfElementSelect = vtkSMIntVectorProperty::SafeDownCast(
			hmWidget->GetWidgetProxy()->GetProperty("StraightModelObjectSelected"));
			
	
			if(typeOfElementSelect->GetElement(0) == 0) //O elemento é um nó
				{
				// Pega ID do segmento
			  segmentId = wProxy->GetStraightModelElementSelected();
			  // Pega número do Nó
			  nodeId		= wProxy->GetStraightModelNodeNumber();
			  
				this->SegmentEntry->SetValueAsInt(segmentId);
				this->NodeEntry->SetValueAsInt(nodeId);				
			  this->ErrorMessageLabel->SetText("");				
				}
			else	// não é um terminal		
			  this->ErrorMessageLabel->SetText("Warning: Just NODES can be selected!");
						
			// Setting Terminal Entry  
			if(segmentId == -1)
				{
				this->SegmentEntry->SetValue("None");
				this->NodeEntry->SetValue("None");				
				}

  	  }
	  }  	
	// Se o método abaixo for chamado, o botão accept deixa de ficar verde, mas pisca a interface  
	//  this->GetPVSource()->PreAcceptCallback();	  
  return segmentId;
}

// -----------------------------------------------------------------------------
void vtkPVHMCouplingFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());      
}

// -----------------------------------------------------------------------------
void vtkPVHMCouplingFilter::SetValueChanged()
{
  this->ModifiedCallback();
}

// -----------------------------------------------------------------------------
void vtkPVHMCouplingFilter::SaveCoupling()
{
	// Esta saída é provisória!!!
	cout << "\n\nvoid vtkPVHMCouplingFilter::SaveCoupling()" << endl;
	cout << "  -----> Provisory Output <-----   " << endl;
	for(int i = 0; i < this->CouplingListBox->GetWidget()->GetNumberOfItems(); i++)
		cout << this->CouplingListBox->GetWidget()->GetItem(i) << endl;
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::AddCoupling()
{
	
	if (!this->PointCounter) // entra somente uma vez aqui
		{
		vtkSMStringVectorProperty *ColorCodeProp = vtkSMStringVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("ColorCode"));    	
  	strcpy(this->ColorCodeString, ColorCodeProp->GetElement(0));
  	this->ColorCode = vtkHMUtils::ConvertCharToIdList(this->ColorCodeString);
   	}
	this->PointCounter++;
	
	
	// Atualizando valor da variavel no Widget do modelo 1D que é utilizada na colorizacao das esfereas de acoplamento
	if (this->Widget1DProxy)
  	this->Widget1DProxy->SetFullModelCode(this->ColorCode->GetId(this->PointCounter));
	
	
	if (!this->Input3DCombo->GetCurrentValue())
		{
		vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Error","There is no 3DFullModel selected.",
		 vtkKWMessageDialog::ErrorIcon);
		return;
		}
	
	if (!strcmp(this->GroupSelectionCombo->GetValue(), "None"))
		{
		vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Error","Please select a Cover Group.",
		 vtkKWMessageDialog::ErrorIcon);
		return;
		}
	
	if (!strcmp(this->SegmentEntry->GetValue(), "None"))
		{
		vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Error","Please select a 1D point.",
		 vtkKWMessageDialog::ErrorIcon);
		return;
		}

	if (Widget1DProxy)
		Widget1DProxy->IncrementCouplingVar(this->CouplingListBox->GetWidget()->GetNumberOfItems());
		
	vtkSMIntVectorProperty *NumberOfTotalIOFaces = vtkSMIntVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("NumberOfTotalIOFaces"));
	
			
	if (this->CouplingListBox->GetWidget()->GetNumberOfItems() >= NumberOfTotalIOFaces->GetElement(0)) 
		{
		vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Warning","Limit of Coupling Points reached, remove some item from coupling list in order to add a new point.",
		 vtkKWMessageDialog::ErrorIcon);
		return;
		}

	// Set the combo text
  int TempSelected = this->CouplingListBox->GetWidget()->GetSelectionIndex();
  char TempChar[512];
  char PointNumber[20];
  
  sprintf(PointNumber, "Point %d: ", this->CouplingListBox->GetWidget()->GetNumberOfItems() + 1);
  strcpy(TempChar, PointNumber);
  strcat(TempChar, "Segment: ");
  strcat(TempChar, this->SegmentEntry->GetValue());
  strcat(TempChar, " Node: ");
  strcat(TempChar, this->NodeEntry->GetValue());  
  strcat(TempChar, " Group: ");
  strcat(TempChar, this->GroupSelectionCombo->GetValue());
  strcat(TempChar, " from ");
  strcat(TempChar, this->Input3DCombo->GetCurrentName());
  
  
  if (!this->Current3dSelected)
  	vtkErrorMacro("None 3d structure were selected");
  
  if (!this->CouplingInfoArray->GetNumberOfTuples())
  	this->CouplingInfoArray->SetNumberOfTuples(NumberOfTotalIOFaces->GetElement(0));
  
  
  int ArrayPosition = this->CouplingListBox->GetWidget()->GetNumberOfItems();
  
  this->CouplingInfoArray->SetTuple4(ArrayPosition, this->SegmentEntry->GetValueAsInt(), this->NodeEntry->GetValueAsInt(),  this->GroupSelectionCombo->GetValueAsInt(), this->Current3dSelected-1 );
  
	// Insert string in the Coupling List
  this->CouplingListBox->GetWidget()->InsertEntry(this->CouplingListBox->GetWidget()->GetNumberOfItems(), TempChar);

	// Adjust selection list and selected state
  if(this->CouplingListBox->GetWidget()->GetNumberOfItems() > 0)
    this->CouplingListBox->GetWidget()->SetSelectionIndex(this->CouplingListBox->GetWidget()->GetNumberOfItems()-1);
  if(this->CouplingListBox->GetWidget()->GetNumberOfItems() > 1)
    this->CouplingListBox->GetWidget()->SetSelectState(TempSelected, 0);
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::RemoveCoupling()
{
  
  this->CouplingListBox->GetWidget()->DeleteAll();
  

  
  int Selected = 0; //this->CouplingListBox->GetWidget()->GetSelectionIndex();

  if (Widget1DProxy)
  		//this->Widget1DProxy->RemoveCouplingActor(Selected);
  		this->Widget1DProxy->RemoveAllCouplingActors();
//  if(this->CouplingListBox->GetWidget()->GetNumberOfItems() > 0 && this->CouplingListBox->GetWidget()->GetSelectionIndex() != -1)
//	  {
//  	if(this->CouplingListBox->GetWidget()->GetNumberOfItems() == 1 || Selected == 0)
//      this->CouplingListBox->GetWidget()->SetSelectionIndex(1);
//  	else
//      this->CouplingListBox->GetWidget()->SetSelectionIndex(Selected-1);
//    this->CouplingListBox->GetWidget()->DeleteRange(Selected, Selected);
//  	}
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::ListBoxItemSelected()
{
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::StraightModelWidgetView()
{
  if (this->StraightModelInputNumber == -1)
  	return;
  
  //this->GetPVSource()->GetPVInput(this->MeshDataInputNumber)->Deselect();	
  
  
  
  //search for the 3D models in order to disable it
	for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs(); ++i)
		{
		vtkPVSource *CurrentPVSource = this->GetPVSource()->GetPVInput(i);
		if(strcmp(CurrentPVSource->GetClassName(), "vtkPVDReaderModule"))
			{
			this->GetPVSource()->GetPVInput(i)->Deselect();
			break;
			}
		} 
  
  
  this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->Select();
  
  
  this->GetPVApplication()->GetMainWindow()->SetInteractorStyle(vtkPVWindow::INTERACTOR_STYLE_2D);
  
  
  
  this->GetPVApplication()->GetMainView()->GetRenderer()->SetActiveCamera(this->straightModelCamera);
 		
  	
  	
  //this->GetPVApplication()->GetMainView()->GetRenderer()->SetActiveCamera(this->straightModelCamera);
  
  if (!this->straightModelCoordinates)	
  	this->straightModelCamera->SetPosition(0, 0, 1);
  else
  	this->straightModelCamera->SetPosition(this->straightModelCoordinates);
  
  this->straightModelCoordinates = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition();     


//	this->straightModelCoordinates[0] = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition()[0];
//this->straightModelCoordinates[1] = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition()[1];
//this->straightModelCoordinates[2] = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition()[2];
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::FullModelView()
{
	if (this->MeshDataInputNumber == -1)
		return;
	
	//search for the 1DStraightModel in order to desable it
	for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs(); ++i)
		{
		vtkPVSource *CurrentPVSource = this->GetPVSource()->GetPVInput(i);
		if(!strcmp(CurrentPVSource->GetClassName(), "vtkPVDReaderModule"))
			{
			this->GetPVSource()->GetPVInput(i)->Deselect();
			break;
			}
		} 


	//search for the selected 3D model in order to enable it
	for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs(); ++i)
		{
		vtkPVSource *CurrentPVSource = this->GetPVSource()->GetPVInput(i);
		if(strcmp(CurrentPVSource->GetClassName(), "vtkPVDReaderModule"))
			{
			if (i == (this->MeshDataInputNumber-1))
				{
				this->GetPVSource()->GetPVInput(i)->Select();
				break;
				}
			}
		} 

  this->GetPVApplication()->GetMainWindow()->SetInteractorStyle(vtkPVWindow::INTERACTOR_STYLE_3D);
	
	// Adjust camera position based on the object's bounding box
  double bounds[6];
  this->GetPVSource()->GetPVInput(this->MeshDataInputNumber)->GetDataInformation()->GetBounds(bounds);
	this->fullModelCamera->SetFocalPoint(0.5*(bounds[0]+bounds[1]), 
                                			 0.5*(bounds[2]+bounds[3]),
                                			 0.5*(bounds[4]+bounds[5]));
	// Estimate initial 'Z' position
//  if(this->fullModelCoordinates[0] == 0 && this->fullModelCoordinates[1] == 0 && this->fullModelCoordinates[2] == 0)
//  	{
//		this->fullModelCoordinates[2] = ( fabs(bounds[5]) + fabs(bounds[4]) ) * 3;
//  	}
//	this->fullModelCamera->SetPosition(fullModelCoordinates);

  if(!this->fullModelCoordinates) //[0] == 0 && this->fullModelCoordinates[1] == 0 && this->fullModelCoordinates[2] == 0)
  	this->fullModelCamera->SetPosition(0, 0, ( fabs(bounds[5]) + fabs(bounds[4]) ) * 3);
	else
		this->fullModelCamera->SetPosition(fullModelCoordinates);


  this->GetPVApplication()->GetMainView()->GetRenderer()->SetActiveCamera(this->fullModelCamera);  
  this->fullModelCoordinates = this->GetPVApplication()->GetMainView()->GetRenderer()->GetActiveCamera()->GetPosition();     


  //this->GetPVSource()->GetPVOutput()->CenterCamera();
  
  //this->GetPVSource()->GetPVInput(this->MeshDataInputNumber)->GetPVOutput()->CenterCamera();
  
    vtkPVApplication* pvApp = this->GetPVApplication();
  vtkSMRenderModuleProxy* renderModule = pvApp->GetRenderModuleProxy();
  
  vtkPVWindow* window = this->GetPVSource()->GetPVWindow();
    window->SetCenterOfRotation(0.5*(bounds[0]+bounds[1]), 
                                0.5*(bounds[2]+bounds[3]),
                                0.5*(bounds[4]+bounds[5]));
    window->ResetCenterCallback();
    
        renderModule->ResetCamera(bounds);
    renderModule->ResetCameraClippingRange();
    
  
//      if ( this->GetPVRenderView() )
//      {
//      this->GetPVRenderView()->EventuallyRender();
//      }
//    }
//  
//  this->GetTraceHelper()->AddEntry("$kw(%s) CenterCamera", this->GetTclName());
  
  
  
  //this->ModifiedCallback();     
	
	// Se o método abaixo for chamado, o botão accept deixa de ficar verde, mas pisca a interface  
	//this->GetPVSource()->PreAcceptCallback();	  
	
  
 //this->GetPVApplication()->GetMainWindow()->SetInteractorStyle(vtkPVWindow::INTERACTOR_STYLE_CENTER_OF_ROTATION);
  

}

//------------------------------------------------------------------------------
int vtkPVHMCouplingFilter::CheckInputType()
{
	bool StraightModelFound = false;
	int returnValue;
	
	// identifica em qual das entradas esta o 1DStraightModel
	// nem sempre a entrada zero é a do STM
	
	int StraightModelInputPort = 0;
	
	for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs(); ++i)
		{
		
		vtkPVSource *CurrentPVSource = this->GetPVSource()->GetPVInput(i);
		if(!strcmp(CurrentPVSource->GetClassName(), "vtkPVDReaderModule"))
			{
			StraightModelInputPort = i;
			// only one StraightModel is allowed here so we can brake the loop
			break;
			}
		}
	
	
	this->StraightModelInputNumber = StraightModelInputPort;
	
	// Pega as widgets do input 0 e procura pela widget do Straight Model
  vtkPVWidgetCollection *widgetsOfPipeline = this->GetPVSource()->GetPVInput(StraightModelInputPort)->GetWidgets(); 
  for(int i=0; i < widgetsOfPipeline->GetNumberOfItems(); i++)
  	{
    vtkPVWidget *widget = vtkPVWidget::SafeDownCast(widgetsOfPipeline->GetItemAsObject(i));
    
    if(strcmp(widget->GetClassName(), "vtkPVHMStraightModelWidget") == 0)
    	{   
    	vtkPVHMStraightModelWidget *PVSTM = vtkPVHMStraightModelWidget::SafeDownCast(widget);
    	
    	// seta para vizualizacao de elementos para que os nós possam ser selecionados
    	PVSTM->SelectTreeElement(1, 0);
    	
    	StraightModelFound = true;
    	
    	vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(PVSTM->GetWidgetProxy());
    	
    	vtkSMIntVectorProperty *NumberOfTotalIOFaces = vtkSMIntVectorProperty::SafeDownCast(
			this->GetPVSource()->GetProxy()->GetProperty("NumberOfTotalIOFaces"));
  		wProxy->GenerateCouplingRepresentation(NumberOfTotalIOFaces->GetElement(0));
    	}
  	}
  	
	// Cria as propriedades que irão armazenar o valor do input para o modelo 1D e para o 3D
  vtkSMIntVectorProperty *prop1D = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("1DInputNumber"));    		
  vtkSMIntVectorProperty *prop3D = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("3DInputNumber"));    	
  
  if (!prop1D || !prop3D) 
	  {
		vtkErrorMacro(<<"Cannot create \"1DInputNumber\" or \"3DInputNumber\" property.");	  	
  	return 0;
	  }

//	// Se na coleção de widgets do input 0 estiver a do modelo 1d, Straight Model está nesse input
//  if(StraightModelFound)
//	  {
//		this->StraightModelInputNumber = 0;
//		//this->MeshDataInputNumber = 1;
//		returnValue = 1; // StraightModel no input 0
//	  }
//	else
//		{
//		this->StraightModelInputNumber = 1;
//		//this->MeshDataInputNumber = 0;
//		returnValue = 0; 		
//		}

	// Seta as propriedades correspondentes ao número do input referente ao modelo 1D e ao 3D
	prop1D->SetElement(0,this->StraightModelInputNumber);
	prop3D->SetElement(0,this->MeshDataInputNumber);	
		
  this->ModifiedCallback(); 
  this->GetPVSource()->GetProxy()->UpdateVTKObjects();	
  		
	return returnValue;
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::Accept()  
{	
	if (this->Input3DCombo->GetNumberOfItems() == 1)
		{
	 	char temp[20];
 		for (int i = 0; i < this->GetPVSource()->GetNumberOfPVInputs()-1; ++i)
  		{
			sprintf(temp, "3DFullModel %d", i+1);
			this->Input3DCombo->AddItem(temp, i+1);
			}
		
		}
	
	// Pega propriedade referente ao elemento selecionado
  vtkSMIntVectorProperty *outputProp = vtkSMIntVectorProperty::SafeDownCast(
    this->GetPVSource()->GetProxy()->GetProperty("OutputSelect"));

	// Atribui numeração de input para o modelo 1D e para o 3D
	this->CheckInputType();

	// Caso não tenha sido adicionado anterior, adicionar observer
	if(!this->ObserverAdded)
		this->AddStraightModelObserver();

	// A idéia é, ao menos na interface, manter o label "Input 1" como sendo o StraightModel
  if(outputProp->GetElement(0) == 0) 
 	 	{
  	this->StraightModelWidgetView(); 	 		
		// Se é a primeira vez que mostra o modelo 1D, centraliza na tela
	  if(this->First1DView)
		  {  	
		  this->GetPVSource()->GetPVInput(this->StraightModelInputNumber)->GetPVOutput()->CenterCamera(); 
		  
		  //this->GetPVSource()->GetPVOutput()->CenterCamera();
		  
		  this->First1DView = false;
		  }	 	 		
		//this->SegmentEntry->SetStateToNormal();
		//this->NodeEntry->SetStateToNormal();		
		//this->GroupSelectionCombo->SetStateToDisabled();
  	}
	else	
  	{
  	this->FullModelView();
		this->SegmentEntry->SetStateToDisabled();
		this->NodeEntry->SetStateToDisabled();		
		this->GroupSelectionCombo->SetStateToNormal();	  
  	}

  this->GetPVSource()->Select();

	this->UpdateWidgetInfo();
  this->PlaceComponents();
  this->ConfigureComponents();
  
  
  
  

  
    
}

//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{


}


//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::UpdateWidgetInfo()
{
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
	this->GetPVSource()->GetProxy()->UpdateInformation();
	
	vtkSMIntVectorProperty *CoverGroupNumberProp = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("CoverGroups"));
	if(CoverGroupNumberProp)
	  this->NumberOfCoverGroups = CoverGroupNumberProp->GetElement(0);
	else
  	vtkErrorMacro(<<"Cannot create \"CoverGroups\" filter property.");
  	

	vtkSMIntVectorProperty *WallGroupNumberProp = vtkSMIntVectorProperty::SafeDownCast(
		this->GetPVSource()->GetProxy()->GetProperty("WallGroups"));
	if(WallGroupNumberProp)
	  this->NumberOfWallGroups = WallGroupNumberProp->GetElement(0);
	else
  	vtkErrorMacro(<<"Cannot create \"WallGroupNumberProp\" filter property.");

}


//------------------------------------------------------------------------------
void vtkPVHMCouplingFilter::ConfigureSolverFilesGenerationCallback()
{
	vtkSMIntVectorProperty *NumberOfTotalIOFaces = vtkSMIntVectorProperty::SafeDownCast(
	this->GetPVSource()->GetProxy()->GetProperty("NumberOfTotalIOFaces"));
	int	numberofIOFaces = NumberOfTotalIOFaces->GetElement(0);


	//	 se o numero de itens de pontos de acoplamento é diferente do numero de tampas 3D
	if (this->CouplingListBox->GetWidget()->GetNumberOfItems() != numberofIOFaces	)
		{
		if (!this->CouplingListBox->GetWidget()->GetNumberOfItems()) // Nenhum ponto de acoplamento foi definido
			{
			vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Problem!","No Coupling Points Set",
		 vtkKWMessageDialog::ErrorIcon);
			}
		else // numero de pontos definidos é menor ou maior do que o numero de tampas 3D 
			{
      vtkKWMessageDialog::PopupMessage(
		this->kwApp, this->GetParentWindow(), "Problem!","Wrong Number of Coupling Points",
		 vtkKWMessageDialog::ErrorIcon);
			}	
		}
	else // se numero de itens igual ao numero de tampas
		{
		
		for (int i = 0; i < 100; ++i)
	  	this->CouplingInfo[i]=0;
		
		// temp char array
		char Temp[20];
    
    // reset array
		for (int i = 0; i < 20; ++i)
	  	Temp[i]=0;


		double *n;
		for (int i = 0; i < numberofIOFaces; ++i)
			{
			n = this->CouplingInfoArray->GetTuple4(i);
	   	
	   	if (!n[0])
	   		{
	   		vtkKWMessageDialog::PopupMessage(
				this->kwApp, this->GetParentWindow(), "Problem!","Bad definition of Coupling Points: Segment Id can not be Null",
		 		vtkKWMessageDialog::ErrorIcon);
	   		return;
	   		}	
	   		
	   	if (!n[2])
	   		{
	   		vtkKWMessageDialog::PopupMessage(
				this->kwApp, this->GetParentWindow(), "Problem!","Bad definition of Coupling Points: 3D Group can not be Null",
		 		vtkKWMessageDialog::ErrorIcon);
	   		return;
	   		}	
	   	
	   	
	   	sprintf(Temp, "%i;", int(n[0]));
	    strcat(this->CouplingInfo, Temp);
	   	sprintf(Temp, "%i;", int(n[1]));
	    strcat(this->CouplingInfo, Temp);
	   	sprintf(Temp, "%i;", int(n[2]));
	    strcat(this->CouplingInfo, Temp);
	   	sprintf(Temp, "%i;", int(n[3]));
	    strcat(this->CouplingInfo, Temp);
			}
		
		// se janela ainda nao foi aberta	
		if (!this->CoupledModelConfigurationWidget)
			{
		 	this->CoupledModelConfigurationWidget = vtkPVHMCoupledModelConfigurationWidget::New();
		  this->CoupledModelConfigurationWidget->SetMasterWindow(this->Window);
		  this->CoupledModelConfigurationWidget->SetPvWindow(this->Window);
			this->CoupledModelConfigurationWidget->Create(this->kwApp);
		  this->CoupledModelConfigurationWidget->SetDisplayPositionToMasterWindowCenterFirst();
			this->CoupledModelConfigurationWidget->SetSize(800, 920); // largura X altura
		  this->CoupledModelConfigurationWidget->SetCouplingInfo(this->CouplingInfo);
		  this->CoupledModelConfigurationWidget->SetModelType("1D-3D Coupled Model");
		  this->CoupledModelConfigurationWidget->Display();
		  this->CoupledModelConfigurationWidget->SetPVSource(this->GetPVSource());
			}
	  else // se já foi aberta, somente mostra a janela e seta novamente os pontos de acoplamento
	  	{
	  	this->CoupledModelConfigurationWidget->Display();
		  this->CoupledModelConfigurationWidget->SetCouplingInfo(this->CouplingInfo);
	  	} 
		}	
}
//----------------------------------------------------------------------------


