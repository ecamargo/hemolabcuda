/*
 * $Id$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    vtkPVHMCouplingFilter

  Author: Jan Palach, Rodrigo L. S. Silva and
  				Paulo Ziemer

=========================================================================*/
// .NAME vtkPVHMCouplingFilter
// .SECTION Description
// Creates an interface for the "coupling" filter.

#ifndef _vtkPVHMCouplingFilter_h_
#define _vtkPVHMCouplingFilter_h_

#include "vtkPVObjectWidget.h"

class vtkKWPushButton;  
class vtkKWFrameWithLabel;
class vtkKWEntry;
class vtkKWComboBox;
class vtkKWLabel;
class vtkKWCheckButton;
class vtkKWMenuButton;
class vtkKWMenuButtonWithLabel;
class vtkKWListBoxWithScrollbars;
class vtkKWIcon; 
class vtkKWLoadSaveButton;
class vtkKWToolbar;
class vtkCamera;
class vtkRenderer;
class vtkPVSelectionList;
class vtkPVHMCoupledModelConfigurationWidget;
class vtkIntArray;
class vtkKWRadioButton;
class vtkKWRadioButtonSet;
class vtkSMHMStraightModelWidgetProxy;
class vtkPVWidget;
class vtkIdList;

class VTK_EXPORT vtkPVHMCouplingFilter : public vtkPVObjectWidget
{
public:
  static vtkPVHMCouplingFilter *New();
  vtkTypeRevisionMacro(vtkPVHMCouplingFilter, vtkPVObjectWidget); 
  void PrintSelf(ostream& os, vtkIndent indent){};

  // Description:
  // Enable visualization of StraightModelWidget.
  void StraightModelWidgetView();
  
  // Description:
  // Enable visualization of FullModel object.
  void FullModelView();
  
  // Description:
  // Gets the StraightModel Node ID selected by mouse.
  int GetStraightModelSelectedNodeId();  

  // Description:
  // Execute event 
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};
  
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents();
  void ConfigureComponents();
  
  // Description:
  // Call creation on the child.  
	virtual void 	Create(vtkKWApplication* app);
	
	// Description:
  // Initialize the newly created widget.
  virtual void Initialize(){};
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  virtual void PostAccept(){};
 
  //Description:
  //Method invoked when an interface component has its value changed.
  void SetValueChanged();   
   
  // Description:
  // Adds a seed to the list from the 4 entries above the listbox.
  void AddCoupling();
  
  // Description:
  // Removes a seed from the list by a given ID.  
  void RemoveCoupling();

  // Description:
  // Save Coupling List (NOT READY!!! JUST PRINT LIST.)
  void SaveCoupling();
	
  // Description:
  // Method invoked when a seed's listbox item is selected (single click).
  void ListBoxItemSelected();
  
	// Description:
	// Called when the group combo box is selected    
	void UpdateGroupsCallback();

	// Description:
  // Called when an 3D input is selected
	void Select3DInputCallback();
	
	// Description:
  // Called when an 1D input is selected
	void Select1DInputCallback();

	// Description:
  // Check whick type is located in input 0 and input 1
	int  CheckInputType(); 
 
	// Description:
  // Add an observer to the events on the straight Model widget
 	void AddStraightModelObserver();
 	
 	// Description:
  // Callback method that displays the configuration window where the Coupled Model solver files are generated 
 	void ConfigureSolverFilesGenerationCallback();
 	

 	
protected:
  vtkPVHMCouplingFilter();
  virtual ~vtkPVHMCouplingFilter();

	// Description:
  // Set where the Straight model and Meshdata are located (input 0 or input 1)
	int StraightModelInputNumber;
	int MeshDataInputNumber;

	// Description:
  // Controls if that's the first time that the 1D model is viewed
	bool First1DView;
	
	// Description:
  // Controls if an observer for the StraightModel events was already added
	bool ObserverAdded;

  // Description:
  // Number of Cover Groups.
	int NumberOfCoverGroups;

  // Description:
  // Group that was selected.
	int SelectedGroup;

	// Description:
	// Radio Button that controls 1D model visualization
	vtkKWRadioButton *Input1DSelectRadioButton;
	
	// Description:
  // Select the 3D input.
  vtkPVSelectionList *Input3DCombo;	

	// Description:
  // Main Frame.
  vtkKWFrameWithLabel   *Frame;

	// Description:
  // Stores previous position of 1D and 3D cameras.
  vtkCamera *straightModelCamera;
  vtkCamera *fullModelCamera;

  // Description:  
  // Store the current coordinates.
  double* straightModelCoordinates;
  double* fullModelCoordinates;

  // Description:
	// Segment info
	vtkKWLabel* SegmentLabel;
	vtkKWEntry* SegmentEntry;

  // Description:
	// Node info
	vtkKWLabel* NodeLabel;
	vtkKWEntry* NodeEntry;

  // Description:
	// Segment info
	vtkKWLabel* ErrorMessageLabel;

  // Description:
	// Groups Combo box
	vtkKWLabel *GroupSelectionLabel;
	vtkKWComboBox *GroupSelectionCombo;
		
  // Description:
  // Stores all the "couplings" selected by the user.
  vtkKWListBoxWithScrollbars *CouplingListBox;

  // Description:
  // Button that adds a seed to the list.
  vtkKWPushButton *AddCouplingButton;
  
  // Description:
  // Button that removes a seed from the list.
  vtkKWPushButton *RemoveCouplingButton;

  // Description:
  // Button that save the coupling list (NOT READY!!!)
  vtkKWPushButton *SaveCouplingButton;

  // Description:
  // Button used to open the Solver Configuration Window.  
  vtkKWPushButton *ConfigureFilesGenerationButton;

  // Description:
  // Reference to Solver Configuration Window   
  vtkPVHMCoupledModelConfigurationWidget *CoupledModelConfigurationWidget;
  
  // Description:
  // Used to create the CoupledModelConfigurationWidget 
  vtkPVWindow *Window;
	vtkPVApplication* pvApp;
	vtkKWApplication *kwApp;
	
	// Description:
  // string used to store the value of segmentId, Node e 3D Group
	char CouplingInfo[1024];
	
	// Description:
	// Temporary Int Array used to collect data from Coupling points; Later the Data collecetd is converte to char array
	vtkIntArray *CouplingInfoArray;
  
 	// Description:
  // Keeps track of current 3D model selected
	int Current3dSelected;
	
	// Description:
	// local reference to 1D model's Widget Proxy
	// used to manage Coupling Actors that represent Coupling Points
	vtkSMHMStraightModelWidgetProxy *Widget1DProxy;
	
 	// Description:
 	// used to remove observers inserted in 1D models
 	vtkCallbackCommand *CallbackCommand; 
 	
 	// Description:
 	// used to remove observers inserted in 1D models
 	vtkPVWidget *PVWidget;
 
 
	int NumberOfWallGroups;
 
 
 	int PointCounter;
 	
 	vtkIdList *ColorCode;
 	
 	// string used to store the value of colorcode from each cover from 3Ds from current coupled model
 	char ColorCodeString[100];
 
private:  
  vtkPVHMCouplingFilter(const vtkPVHMCouplingFilter&); // Not implemented
  void operator=(const vtkPVHMCouplingFilter&); // Not implemented
}; 

#endif  /*_vtkPVHMCouplingFilter_h_*/

