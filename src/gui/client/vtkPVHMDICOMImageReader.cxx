#include "vtkPVHMDICOMImageReader.h"

#include "vtkSMHMDICOMImageReaderProxy.h"

#include "vtkPVApplication.h"
#include "vtkPVSource.h"

#include "vtkKWProgressGauge.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWEntryWithLabel.h"

#include "vtkKWEntry.h"
#include "vtkKWLabel.h"

#include "vtkPVWindow.h"

#include "vtkPVScale.h"

#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWFrameWithScrollbar.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"


vtkCxxRevisionMacro(vtkPVHMDICOMImageReader, "$Rev$");
vtkStandardNewMacro(vtkPVHMDICOMImageReader);

vtkPVHMDICOMImageReader::vtkPVHMDICOMImageReader()
{
  this->MainFrame = vtkKWFrameWithLabel::New();
  this->LoadImagesButton = vtkKWLoadSaveButton::New();
  this->DICOMPath = vtkKWEntry::New();

  this->PatientNameLabel = vtkKWLabel::New();

  for (int i = 0; i < 3; i++)
  {
  	this->Spacing[i] = 0.0;
  }
  
  this->SourceProxy = NULL;
}

vtkPVHMDICOMImageReader::~vtkPVHMDICOMImageReader()
{
  this->MainFrame->Delete();
  this->DICOMPath->Delete();

  this->PatientNameLabel->Delete();

  this->LoadImagesButton->Delete();

  this->SourceProxy = NULL;
}

void vtkPVHMDICOMImageReader::Create(vtkKWApplication* kwApp)
{
  vtkPVApplication *pvApp = vtkPVApplication::SafeDownCast(kwApp);

  //Red coloring the progress bar.
  pvApp->GetMainWindow()->GetProgressGauge()->SetBarColor(1.0, 0.0, 0.0);
  
  this->Superclass::Create(kwApp);

  this->MainFrame->SetParent(this);
  this->MainFrame->Create(pvApp);
  this->MainFrame->SetLabelText("HM DICOM Image Reader");

  this->PatientNameLabel->SetParent(this->MainFrame->GetFrame());
  this->PatientNameLabel->Create(pvApp);
  this->PatientNameLabel->SetText("No DICOM Volume loaded");

  this->DICOMPath->SetParent(this->MainFrame->GetFrame());
  this->DICOMPath->Create(pvApp);
  this->DICOMPath->ReadOnlyOn();
  this->DICOMPath->SetValue("");
  this->DICOMPath->SetWidth(38);

  this->LoadImagesButton->SetParent(this->MainFrame->GetFrame());
  this->LoadImagesButton->Create(pvApp);
  this->LoadImagesButton->SetText("Browse DICOM");
  this->LoadImagesButton->GetLoadSaveDialog()->ChooseDirectoryOn();
  this->LoadImagesButton->GetLoadSaveDialog()->SetDefaultExtension("dcm");
  this->LoadImagesButton->SetBalloonHelpString("Loads a DICOM 3D image volume from its directory.");
  this->LoadImagesButton->SetWidth(106);


  this->PlaceComponents();
}

void vtkPVHMDICOMImageReader::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",  
    this->MainFrame->GetWidgetName());
    
  this->Script("grid %s -sticky ew -columnspan 2",
    this->PatientNameLabel->GetWidgetName());

  this->Script("grid %s %s - -sticky ew",
    this->DICOMPath->GetWidgetName(),
    this->LoadImagesButton->GetWidgetName());
}

void vtkPVHMDICOMImageReader::Initialize()
{
  this->SourceProxy = vtkSMHMDICOMImageReaderProxy::SafeDownCast(this->GetPVSource()->GetProxy()); 
}

void vtkPVHMDICOMImageReader::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMDICOMImageReader::SetDirectoryName()
{
  this->DirectoryName = NULL;
  this->DirectoryName = this->LoadImagesButton->GetFileName();
  
  this->SourceProxy->SetDirectoryName(this->DirectoryName);
  
  this->DICOMPath->SetValue(this->DirectoryName);
}

void vtkPVHMDICOMImageReader::Accept()
{
  this->SetDirectoryName();

  this->LoadImagesButton->SetEnabled(0);
}

void vtkPVHMDICOMImageReader::PostAccept()
{
  this->SetPatientNameFromServer();

  this->Superclass::PostAccept();
}

void vtkPVHMDICOMImageReader::SetSourceProxy(vtkSMHMDICOMImageReaderProxy *wExternal)
{
  this->SourceProxy = wExternal;
}

double *vtkPVHMDICOMImageReader::GetSpacing()
{
  return this->Spacing;
}

void vtkPVHMDICOMImageReader::SetSpacingFromServer()
{
  vtkSMDoubleVectorProperty* SpacingProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->GetPVSource()->GetProxy()->GetProperty("Spacing"));

  this->GetPVSource()->GetProxy()->UpdatePropertyInformation(SpacingProperty);

  this->Spacing[0] = SpacingProperty->GetElement(0);
  this->Spacing[1] = SpacingProperty->GetElement(1);
  this->Spacing[2] = SpacingProperty->GetElement(2);
}

void vtkPVHMDICOMImageReader::SetPatientNameFromServer()
{
  vtkSMStringVectorProperty* PatientNameProperty = vtkSMStringVectorProperty::SafeDownCast(
      this->GetPVSource()->GetProxy()->GetProperty("PatientName"));

  this->GetPVSource()->GetProxy()->UpdatePropertyInformation(PatientNameProperty);

  this->PatientNameLabel->SetText(PatientNameProperty->GetElement(0));
}
