#ifndef _vtkPVHMDICOMImageReader_h_
#define _vtkPVHMDICOMImageReader_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkKWFrameWithLabel;

class vtkKWEntry;
class vtkKWLabel;

class vtkKWFrameWithLabel;
class vtkKWEntryWithLabel;
class vtkKWLoadSaveButton;
class vtkKWLoadSaveDialog;

class vtkSMHMDICOMImageReaderProxy;

class vtkPVApplication;


class VTK_EXPORT vtkPVHMDICOMImageReader : public vtkPVObjectWidget
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMDICOMImageReader *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMDICOMImageReader,vtkPVObjectWidget);

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Method invoked right after the Accept().
  virtual void PostAccept();
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only.
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	

  // Description:
  // Sets this widgets DataServer proxy.
  void SetSourceProxy(vtkSMHMDICOMImageReaderProxy *wExternal);

  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();
  
  // Description:
  // Returns de three doubles array with the volume spacing
  double *GetSpacing();
  
  // Description:
  // Gets the Spacing from the DataServer Reader through XML and sets this classes double array
  void SetSpacingFromServer();

  // Description:
  // Gets the Patient's name from the DataServer Reader through XML.
  void SetPatientNameFromServer();

  // Description:
  // Sets this classes *char DirectoryName and the DataServer reader DirectoryName, so it can read the image volume.
  void SetDirectoryName();


protected:

  // Description:
  // Constructor
  vtkPVHMDICOMImageReader();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMDICOMImageReader();
  
  
  // Description:
  // Proxy that comunicates with DataServer data
  vtkSMHMDICOMImageReaderProxy *SourceProxy;

  // Description:
  // Char that contains the DICOM's directory path
  char *DirectoryName;

  // Description:
  // Main Frame of interface
  vtkKWFrameWithLabel *MainFrame;
  
  // Description:
  // Button that invokes the Select directory window and returns its selected directory
  vtkKWLoadSaveButton *LoadImagesButton;

  // Description:
  // Entry that contains the DICOM directory path once the module is accepted.
  vtkKWEntry *DICOMPath;

  // Description:
  // Label that prints the patient's name.  
  vtkKWLabel *PatientNameLabel;

  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;
  
  // Description:
  // Method responsible for creating the Interface that interacts with the source.
  void Create(vtkKWApplication *App);

  // Description:
  // Array that stores the image volume spacing
  double Spacing[3];
  

private:  
  vtkPVHMDICOMImageReader(const vtkPVHMDICOMImageReader&); // Not implemented
  void operator=(const vtkPVHMDICOMImageReader&); // Not implemented
}; 

#endif
