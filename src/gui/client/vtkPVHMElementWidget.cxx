/*
 * $Id: vtkPVHMElementWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"

#include "vtkPVHMElementWidget.h"
#include "vtkPVHMStraightModelWidget.h"

vtkCxxRevisionMacro(vtkPVHMElementWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMElementWidget);

//----------------------------------------------------------------------------
vtkPVHMElementWidget::vtkPVHMElementWidget()
{ 
  this->ElastinLabel				= vtkKWLabel::New();
  this->ElastinEntry 				= vtkKWEntry::New();
  this->CollagenLabel			= vtkKWLabel::New();
  this->CollagenEntry 			= vtkKWEntry::New();
  this->CoefficientALabel			= vtkKWLabel::New();
  this->CoefficientAEntry			= vtkKWEntry::New();
  this->CoefficientBLabel			= vtkKWLabel::New();
  this->CoefficientBEntry 			= vtkKWEntry::New();   
  this->ViscoElasticityLabel		= vtkKWLabel::New();
  this->ViscoElasticityEntry		= vtkKWEntry::New(); 
  this->ViscoElasticityExpLabel	= vtkKWLabel::New();
  this->ViscoElasticityExpEntry		= vtkKWEntry::New(); 
  this->InfPressureLabel			= vtkKWLabel::New();
  this->InfPressureEntry			= vtkKWEntry::New(); 
  this->RefPressureLabel			= vtkKWLabel::New();
  this->RefPressureEntry			= vtkKWEntry::New(); 
  this->PermeabilityLabel			= vtkKWLabel::New();
  this->PermeabilityEntry			= vtkKWEntry::New();  
  this->meshIDLabel = vtkKWLabel::New();
  this->meshIDEntry 	= vtkKWEntry::New();
  this->FillSpace				= vtkKWLabel::New();
//  this->SpeedOfSoundLabel			= vtkKWLabel::New();
//  this->SpeedOfSoundEntry			= vtkKWEntry::New();
 
 
 
  this->ProximalWallThicknessEntry    		= vtkKWEntry::New(); 
  this->ProximalWallThicknessLabel     		= vtkKWLabel::New(); 
  
  this->DistalWallThicknessEntry    		= vtkKWEntry::New(); 
  this->DistalWallThicknessLabel     		= vtkKWLabel::New(); 
  
  this->ProximalRadiusEntry    		= vtkKWEntry::New(); 
  this->ProximalRadiusLabel     		= vtkKWLabel::New(); 
  
  this->DistalRadiusEntry    		= vtkKWEntry::New(); 
  this->DistalRadiusLabel     		= vtkKWLabel::New(); 
  
   
  this->GeometricalFrame	= vtkKWFrameWithLabel::New();
  this->ProximalGeometricalFrame	= vtkKWFrameWithLabel::New();
  this->DistalGeometricalFrame	= vtkKWFrameWithLabel::New();
  
  
  
  this->MechanicalFrame		= vtkKWFrameWithLabel::New();
  
    
  this->UpdateSingleElemPropButton	= vtkKWPushButton::New();
}

//----------------------------------------------------------------------------
vtkPVHMElementWidget::~vtkPVHMElementWidget()
{
  this->ElastinLabel->Delete();
  this->ElastinEntry->Delete();
  this->CollagenLabel->Delete();
  this->CollagenEntry->Delete();  
  this->CoefficientALabel->Delete();  
  this->CoefficientAEntry->Delete();  
  this->CoefficientBLabel->Delete();  
  this->CoefficientBEntry->Delete();  
  this->ViscoElasticityLabel->Delete();
  this->ViscoElasticityEntry->Delete();
  this->ViscoElasticityExpLabel->Delete();
  this->ViscoElasticityExpEntry->Delete();
  this->InfPressureLabel->Delete();
  this->InfPressureEntry->Delete();
  this->RefPressureLabel->Delete();
  this->RefPressureEntry->Delete();
  this->PermeabilityLabel->Delete();
  this->PermeabilityEntry->Delete();
  this->meshIDLabel->Delete();
  this->meshIDEntry->Delete();
  this->FillSpace->Delete();
//  this->SpeedOfSoundLabel->Delete();
//  this->SpeedOfSoundEntry->Delete();
 
  this->UpdateSingleElemPropButton->Delete(); 
  
   
  this->ProximalWallThicknessEntry->Delete(); 
  this->ProximalWallThicknessLabel->Delete(); 
  
  this->DistalWallThicknessEntry->Delete(); 
  this->DistalWallThicknessLabel->Delete(); 
  
  this->ProximalRadiusEntry->Delete(); 
  this->ProximalRadiusLabel->Delete(); 
  
  this->DistalRadiusEntry->Delete(); 
  this->DistalRadiusLabel->Delete(); 
  
   
  this->GeometricalFrame->Delete();
  this->ProximalGeometricalFrame->Delete();
  this->DistalGeometricalFrame->Delete(); 
  
  this->MechanicalFrame->Delete(); 
  
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::UpdateWidgetInfo(	)
{
  int mode = this->GetReadOnly();
  this->ElastinEntry->SetReadOnly(mode);
  this->CollagenEntry->SetReadOnly(mode);	  
  this->CoefficientAEntry->SetReadOnly(mode);	  
  this->CoefficientBEntry->SetReadOnly(mode);	  
  this->ViscoElasticityExpEntry->SetReadOnly(mode);	  
  this->ViscoElasticityEntry->SetReadOnly(mode);	  
  this->InfPressureEntry->SetReadOnly(mode);	  
  this->RefPressureEntry->SetReadOnly(mode);	  
  this->PermeabilityEntry->SetReadOnly(mode);
    
  this->ProximalWallThicknessEntry->SetReadOnly(mode);
  this->DistalWallThicknessEntry->SetReadOnly(mode);
  this->ProximalRadiusEntry->SetReadOnly(mode);
  this->DistalRadiusEntry->SetReadOnly(mode);
//  this->SpeedOfSoundEntry->SetReadOnly(1);
  	  
}
	
//----------------------------------------------------------------------------
void vtkPVHMElementWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Element Information"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
  
  
  this->MechanicalFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->MechanicalFrame->Create(pvApp);
  this->MechanicalFrame->SetLabelText("Mechanical Properties");
  
  
  this->GeometricalFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->GeometricalFrame->Create(pvApp);
  this->GeometricalFrame->SetLabelText("Geometrical Properties");
  
  this->ProximalGeometricalFrame->SetParent(this->GeometricalFrame->GetFrame());
  this->ProximalGeometricalFrame->Create(pvApp);
  this->ProximalGeometricalFrame->SetLabelText("Proximal Geometrical Properties");
  
  this->DistalGeometricalFrame->SetParent(this->GeometricalFrame->GetFrame());
  this->DistalGeometricalFrame->Create(pvApp);
  this->DistalGeometricalFrame->SetLabelText("Distal Geometrical Properties");
   
             
  this->ElastinLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ElastinLabel->Create(pvApp);
  this->ElastinLabel->SetText("Elastin [dyn/cm^2]:");
  this->ElastinEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->ElastinEntry->Create(pvApp);
 
  this->CollagenLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->CollagenLabel->Create(pvApp);
  this->CollagenLabel->SetText("Collagen Elastin [dyn/cm^2]:");
	this->CollagenEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->CollagenEntry->Create(pvApp);
 
  this->CoefficientALabel->SetParent(this->MechanicalFrame->GetFrame());
  this->CoefficientALabel->Create(pvApp);
  this->CoefficientALabel->SetText("Coefficient A:");
	this->CoefficientAEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->CoefficientAEntry->Create(pvApp);
 
  this->CoefficientBLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->CoefficientBLabel->Create(pvApp);
  this->CoefficientBLabel->SetText("Coefficient B:");
	this->CoefficientBEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->CoefficientBEntry->Create(pvApp);
 
  this->ViscoElasticityLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityLabel->Create(pvApp);
  this->ViscoElasticityLabel->SetText("Viscoelasticity [dyn.sec/cm^2]:");
	this->ViscoElasticityEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityEntry->Create(pvApp);
  
  this->ViscoElasticityExpLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityExpLabel->Create(pvApp);
  this->ViscoElasticityExpLabel->SetText("Viscoelasticity Expoent:");
	this->ViscoElasticityExpEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityExpEntry->Create(pvApp);
 
  this->InfPressureLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->InfPressureLabel->Create(pvApp);
  this->InfPressureLabel->SetText("Infiltration Pressure [dyn/cm^2]:");
	this->InfPressureEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->InfPressureEntry->Create(pvApp);
  
  this->RefPressureLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->RefPressureLabel->Create(pvApp);
  this->RefPressureLabel->SetText("Reference Pressure [dyn/cm^2]:");
  this->RefPressureEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->RefPressureEntry->Create(pvApp);
  
  this->PermeabilityLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->PermeabilityLabel->Create(pvApp);
  this->PermeabilityLabel->SetText("Permeability [cm^4/sec/dyn]:");
  this->PermeabilityEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->PermeabilityEntry->Create(pvApp);
  
//  this->SpeedOfSoundLabel->SetParent(this->MechanicalFrame->GetFrame());
//  this->SpeedOfSoundLabel->Create(pvApp);
//  this->SpeedOfSoundLabel->SetText("Speed of Sound [cm/s]:");
//  this->SpeedOfSoundEntry->SetParent(this->MechanicalFrame->GetFrame());
//  this->SpeedOfSoundEntry->Create(pvApp);
  
  this->meshIDLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->meshIDLabel->Create(pvApp);
  this->meshIDLabel->SetText(" Mesh ID:");
  this->meshIDEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->meshIDEntry->SetWidth(18);
  this->meshIDEntry->Create(pvApp);
  this->meshIDEntry->SetReadOnly(1);
  
  this->FillSpace->SetParent(this->MechanicalFrame->GetFrame());
  this->FillSpace->Create(pvApp);
  this->FillSpace->SetText("");
  
  
  this->ProximalWallThicknessLabel->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProximalWallThicknessLabel->Create(pvApp);
  this->ProximalWallThicknessLabel->SetText("WallThickness [cm]:");
  this->ProximalWallThicknessEntry->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProximalWallThicknessEntry->Create(pvApp);
  
  this->ProximalRadiusLabel->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProximalRadiusLabel->Create(pvApp);
  this->ProximalRadiusLabel->SetText("Radius [cm]:");
  this->ProximalRadiusEntry->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProximalRadiusEntry->Create(pvApp);
  
  this->DistalWallThicknessLabel->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalWallThicknessLabel->Create(pvApp);
  this->DistalWallThicknessLabel->SetText("WallThickness [cm]:");
  this->DistalWallThicknessEntry->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalWallThicknessEntry->Create(pvApp);
  
  this->DistalRadiusLabel->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalRadiusLabel->Create(pvApp);
  this->DistalRadiusLabel->SetText("Radius [cm]:");
  this->DistalRadiusEntry->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalRadiusEntry->Create(pvApp);
  
   
  
  this->UpdateSingleElemPropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateSingleElemPropButton->Create(pvApp);
  this->UpdateSingleElemPropButton->SetText("Update Element");
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{		
	int mode = mainWidget->GetWidgetMode();
	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		  mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
			return;
		} 
	this->GetChildFrame()->ExpandFrame();				
	
  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->meshIDLabel->GetWidgetName(),
    this->meshIDEntry->GetWidgetName());
  
  	mainWidget->Script("grid %s -sticky ew -columnspan 2",     
		     this->MechanicalFrame->GetWidgetName());
		     
	mainWidget->Script("grid %s -sticky ew -columnspan 2",     
		     this->GeometricalFrame->GetWidgetName());	   
		     
		       
 		     
	mainWidget->Script("grid %s -sticky ew",     
		     this->ProximalGeometricalFrame->GetWidgetName());	     
			     
	mainWidget->Script("grid %s -sticky ew",     
		     this->DistalGeometricalFrame->GetWidgetName());	     
		     	     		     

  mainWidget->Script("grid %s %s - - -sticky w",         
    this->ElastinLabel->GetWidgetName(),
    this->ElastinEntry->GetWidgetName());    

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->CollagenLabel->GetWidgetName(),
    this->CollagenEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->CoefficientALabel->GetWidgetName(),
    this->CoefficientAEntry->GetWidgetName()); 

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->CoefficientBLabel->GetWidgetName(),
    this->CoefficientBEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ViscoElasticityLabel->GetWidgetName(),
    this->ViscoElasticityEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ViscoElasticityExpLabel->GetWidgetName(),
    this->ViscoElasticityExpEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->InfPressureLabel->GetWidgetName(),
    this->InfPressureEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->RefPressureLabel->GetWidgetName(),
    this->RefPressureEntry->GetWidgetName());  

  mainWidget->Script("grid %s %s %s - - -sticky w",     
    this->PermeabilityLabel->GetWidgetName(),
    this->PermeabilityEntry->GetWidgetName(),
    this->FillSpace->GetWidgetName());  
  
//  mainWidget->Script("grid %s %s - - -sticky w",     
//    this->SpeedOfSoundLabel->GetWidgetName(),
//    this->SpeedOfSoundEntry->GetWidgetName());
    
  mainWidget->Script("grid %s %s  - - -sticky w",     
    this->ProximalWallThicknessLabel->GetWidgetName(),
    this->ProximalWallThicknessEntry->GetWidgetName());
    
      mainWidget->Script("grid %s %s  - - -sticky w",     
    this->ProximalRadiusLabel->GetWidgetName(),
    this->ProximalRadiusEntry->GetWidgetName());
    
      mainWidget->Script("grid %s %s  - - -sticky w",     
    this->DistalWallThicknessLabel->GetWidgetName(),
    this->DistalWallThicknessEntry->GetWidgetName());
    
    
   mainWidget->Script("grid %s %s  - - -sticky w",     
    this->DistalRadiusLabel->GetWidgetName(),
    this->DistalRadiusEntry->GetWidgetName());
 
    
    if(mode)
  	mainWidget->Script("grid remove %s",
     this->UpdateSingleElemPropButton->GetWidgetName());
    else
    mainWidget->Script("grid %s - - -sticky w", 
   this->UpdateSingleElemPropButton->GetWidgetName());
    
    

  this->ElastinEntry->SetBalloonHelpString("Effective elastin module in the element.");  
  this->CollagenEntry->SetBalloonHelpString("Effective elastin module of colagen in the element.");  
  this->CoefficientAEntry->SetBalloonHelpString("Average value of the coefficient A in the element.");  
  this->CoefficientBEntry->SetBalloonHelpString("Average value of the coefficient B in the element.");  
  this->ViscoElasticityEntry->SetBalloonHelpString("Viscoelasticity of the artery wall in the element."); 
  this->ViscoElasticityExpEntry->SetBalloonHelpString("Viscoelasticity expoent of the artery wall in the element."); 
  this->InfPressureEntry->SetBalloonHelpString("Infiltration pressure in the element."); 
  this->RefPressureEntry->SetBalloonHelpString("Reference pressure in the element."); 
  this->PermeabilityEntry->SetBalloonHelpString("Permeability coefficient in the element ");
//  this->SpeedOfSoundEntry->SetBalloonHelpString("Speed Of Sound coefficient in the element ");
  
  this->ProximalWallThicknessEntry->SetBalloonHelpString("Element proximal wall thickness");   
  this->DistalWallThicknessEntry->SetBalloonHelpString("Element distal wall thickness");   
  this->ProximalRadiusEntry->SetBalloonHelpString("Element proximal radius");   
  this->DistalRadiusEntry->SetBalloonHelpString("Element distal radius");   
  
  
  this->UpdateSingleElemPropButton->SetBalloonHelpString("Updates current element properties");
}

  
vtkKWFrameWithLabel* vtkPVHMElementWidget::GetElementFrame()
{
	return this->GetChildFrame();
}
	
//----------------------------------------------------------------------------
void vtkPVHMElementWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::Accept()
{
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetElastin(double Elastin)
{
	this->ElastinEntry->SetValueAsDouble(Elastin);	
}
	
//----------------------------------------------------------------------------	
void vtkPVHMElementWidget::SetCollagen(double Collagen)
{
	this->CollagenEntry->SetValueAsDouble(Collagen);	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetCoefficientA(double coefA)
{
	this->CoefficientAEntry->SetValueAsDouble(coefA); 	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetCoefficientB(double coefB)
{
	this->CoefficientBEntry->SetValueAsDouble(coefB);	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetViscoelasticity(double visco)
{
	this->ViscoElasticityEntry->SetValueAsDouble(visco);	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetViscoelasticityExponent (double exp)
{
	this->ViscoElasticityExpEntry->SetValueAsDouble(exp);	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetInfiltrationPressure(double IPressure)
{
	this->InfPressureEntry->SetValueAsDouble(IPressure);	
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetReferencePressure(double RPressure)
{
	this->RefPressureEntry->SetValueAsDouble(RPressure);		
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetPermeability(double Permeability)
{
	this->PermeabilityEntry->SetValueAsDouble(Permeability);
}

//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetmeshID(int meshID)
{
	this->meshIDEntry->SetValueAsDouble(meshID);
}


//----------------------------------------------------------------------------
void vtkPVHMElementWidget::SetElementInfo(char *text)
{
	this->GetChildFrame()->SetLabelText(text);
}

//----------------------------------------------------------------------------

void vtkPVHMElementWidget::SetProximalWallThickness(double wall)
{
  this->ProximalWallThicknessEntry->SetValueAsDouble(wall);
}

//----------------------------------------------------------------------------

void vtkPVHMElementWidget::SetDistalWallThickness(double wall)
{
  this->DistalWallThicknessEntry->SetValueAsDouble(wall);
}

//----------------------------------------------------------------------------

void vtkPVHMElementWidget::SetProximalRadius(double radius)
{
  this->ProximalRadiusEntry->SetValueAsDouble(radius);
}

//----------------------------------------------------------------------------

void vtkPVHMElementWidget::SetDistalRadius(double radius)
{
  this->DistalRadiusEntry->SetValueAsDouble(radius);
}

//----------------------------------------------------------------------------

// get methods
//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetElastin()
{
	return this->ElastinEntry->GetValueAsDouble();	
}
	
//----------------------------------------------------------------------------	
double vtkPVHMElementWidget::GetCollagen()
{
	return this->CollagenEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetCoefficientA()
{
	return this->CoefficientAEntry->GetValueAsDouble(); 	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetCoefficientB()
{
	return this->CoefficientBEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetViscoelasticity()
{
	return this->ViscoElasticityEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetViscoelasticityExponent()
{
	return this->ViscoElasticityExpEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetInfiltrationPressure()
{
	return this->InfPressureEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetReferencePressure()
{
	return this->RefPressureEntry->GetValueAsDouble();		
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetPermeability()
{
	return this->PermeabilityEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------
double vtkPVHMElementWidget::GetProximalWallThickness()
{
  return this->ProximalWallThicknessEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMElementWidget::GetDistalWallThickness()
{
  return this->DistalWallThicknessEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMElementWidget::GetProximalRadius()
{
  return this->ProximalRadiusEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMElementWidget::GetDistalRadius()
{
  return this->DistalRadiusEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMElementWidget::GetmeshID()
{
  return this->meshIDEntry->GetValueAsDouble();
}
