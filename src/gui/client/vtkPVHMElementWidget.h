/*
 * $Id: vtkPVHMElementWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMElementWidget
// .SECTION Description
// Creates KW Components for Segment Elements


#ifndef _vtkPVHMElementWidget_h_
#define _vtkPVHMElementWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"

class vtkKWEntry;
class vtkKWLabel;
class vtkSMSourceProxy;
class vtkKWFrameWithLabel;
class vtkPVHMStraightModelWidget;

class VTK_EXPORT vtkPVHMElementWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMElementWidget *New();
  vtkTypeRevisionMacro(vtkPVHMElementWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();
  
  // Description:
  // Element Components access methods
  void SetElastin(double Elastin);
  void SetCollagen(double Collagen);
  void SetCoefficientA(double coefA);
  void SetCoefficientB(double coefB);
  void SetViscoelasticity(double visco);
  void SetViscoelasticityExponent (double exp);
  void SetInfiltrationPressure(double IPressure);
  void SetReferencePressure(double RPressure);
  void SetPermeability(double Permeability);
  void SetmeshID(int meshID);
  void SetElementInfo(char *text);
  void SetProximalWallThickness(double wall);
  void SetDistalWallThickness(double wall);
  void SetProximalRadius(double radius);
  void SetDistalRadius(double radius);

//  vtkGetObjectMacro(SpeedOfSoundEntry, vtkKWEntry);
  
  double GetElastin();
  double GetCollagen();
  double GetCoefficientA();
  double GetCoefficientB();
  double GetViscoelasticity();
  double GetViscoelasticityExponent();
  double GetInfiltrationPressure();
  double GetReferencePressure();
  double GetPermeability();
  double GetmeshID();
  double GetProximalWallThickness();
  double GetDistalWallThickness();
  double GetProximalRadius();
  double GetDistalRadius();
 
  vtkKWPushButton *UpdateSingleElemPropButton;	
     
protected:
  vtkPVHMElementWidget();
  virtual ~vtkPVHMElementWidget();
    
	// Description:
	// Element Widget
	// Area, Radius, alfa and Resolution
	// !!! Falta incluir KW correspondente ao instante de tempo	!!!
  vtkKWLabel* ElastinLabel;
  vtkKWEntry* ElastinEntry;  
  vtkKWLabel* CollagenLabel;
  vtkKWEntry* CollagenEntry;  
  vtkKWLabel* CoefficientALabel;
  vtkKWEntry* CoefficientAEntry;  
  vtkKWLabel* CoefficientBLabel;
  vtkKWEntry* CoefficientBEntry;  
  vtkKWLabel* ViscoElasticityLabel;
  vtkKWEntry* ViscoElasticityEntry; 
  vtkKWLabel* ViscoElasticityExpLabel;
  vtkKWEntry* ViscoElasticityExpEntry; 
  vtkKWLabel* InfPressureLabel;
  vtkKWEntry* InfPressureEntry; 
  vtkKWLabel* RefPressureLabel;
  vtkKWEntry* RefPressureEntry; 
  vtkKWLabel* PermeabilityLabel;
  vtkKWEntry* PermeabilityEntry;
  vtkKWLabel* meshIDLabel;
  vtkKWEntry* meshIDEntry; 
  vtkKWLabel* FillSpace;
//  vtkKWLabel* SpeedOfSoundLabel;
//  vtkKWEntry* SpeedOfSoundEntry; 
  
  
  // Description:
  // KWElements related with information from proximal and distal nodes
  
  vtkKWEntry* ProximalWallThicknessEntry;    
  vtkKWLabel* ProximalWallThicknessLabel;
  vtkKWEntry* DistalWallThicknessEntry;    
  vtkKWLabel* DistalWallThicknessLabel;
  
  vtkKWEntry* ProximalRadiusEntry;    
  vtkKWLabel* ProximalRadiusLabel;
  vtkKWEntry* DistalRadiusEntry;    
  vtkKWLabel* DistalRadiusLabel;
  
  
 vtkKWFrameWithLabel *GeometricalFrame;
 vtkKWFrameWithLabel*  ProximalGeometricalFrame;
 vtkKWFrameWithLabel*  DistalGeometricalFrame;
 
 vtkKWFrameWithLabel *MechanicalFrame;
  
  
 
private:  
  vtkPVHMElementWidget(const vtkPVHMElementWidget&); // Not implemented
  void operator=(const vtkPVHMElementWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMElementWidget_h_*/


