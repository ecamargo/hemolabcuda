#include "vtkPVHMExtractGrid.h"

#include "vtkPVApplication.h"
#include "vtkPVSource.h"

#include "vtkPVWidgetCollection.h"

#include "vtkPVHMBoxWidget.h"

#include "vtkPVMinMax.h"
#include "vtkKWScaleWithEntry.h"

#include "vtkSM3DWidgetProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMBoxProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"

#include "vtkCallbackCommand.h"

vtkCxxRevisionMacro(vtkPVHMExtractGrid, "$Rev$");
vtkStandardNewMacro(vtkPVHMExtractGrid);


static void UpdateSlidersFromBoxWidgetCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
	
  vtkPVHMExtractGrid* HMExtractGrid = reinterpret_cast<vtkPVHMExtractGrid*>(sr);
  HMExtractGrid->UpdateSlidersFromBoxWidget();
}

vtkPVHMExtractGrid::vtkPVHMExtractGrid()
{
  this->AlreadyAccepted = 0;
  this->UsingWidget = 0;
  
  for (int i=0; i<6; i++)
  {
  	this->Extent[i] = 0;
  	this->Bounds[i] = 0;
  }
  
  this->BoxWidget = NULL;
  this->SliderClicked = 0;
}

vtkPVHMExtractGrid::~vtkPVHMExtractGrid()
{
  this->AlreadyAccepted = 0;

  this->BoxWidget = NULL;
}

void vtkPVHMExtractGrid::Initialize()
{
  this->Superclass::Initialize();
  
  //verifies if the module is using the HMBox Widget.
  if (this->GetPVSource()->GetWidgets()->GetNumberOfItems() > 4)
  {
    this->UsingWidget = 1;
    this->BoxWidget = vtkPVHMBoxWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetItemAsObject(3));

    this->BoxWidget->SetMaxExtent(this->Extent);
    
    // Called every time left click happends on the viewport BoxWidget.
    vtkCallbackCommand *LeftButtonCallback = vtkCallbackCommand::New();
    LeftButtonCallback->SetCallback(UpdateSlidersFromBoxWidgetCallback);
    //Passes this because the executed method for callback is a vtkPVHMExtractGrid method.
    LeftButtonCallback->SetClientData((void *)this);
    //Captures Left clicks on the BoxWidget.
    this->BoxWidget->AddObserver(vtkCommand::LeftButtonPressEvent, LeftButtonCallback);
    LeftButtonCallback->Delete();
  }
}

void vtkPVHMExtractGrid::UpdateSlidersFromBoxWidget()
{
//  if (!this->SliderClicked)
  if (this->BoxWidget->GetBoxClicked() && !this->SliderClicked)
  {
    this->BoxWidget->GetExtent(this->Extent);
  
    this->MinMax[0]->GetMinScale()->SetDisableCommands(1);
    this->MinMax[0]->GetMaxScale()->SetDisableCommands(1);
    this->MinMax[1]->GetMinScale()->SetDisableCommands(1);
    this->MinMax[1]->GetMaxScale()->SetDisableCommands(1);
    this->MinMax[2]->GetMinScale()->SetDisableCommands(1);
    this->MinMax[2]->GetMaxScale()->SetDisableCommands(1);
  
    this->MinMax[0]->GetMinScale()->SetValue(double(this->Extent[0]));
    this->MinMax[0]->GetMaxScale()->SetValue(double(this->Extent[1]));
    this->MinMax[1]->GetMinScale()->SetValue(double(this->Extent[2]));
    this->MinMax[1]->GetMaxScale()->SetValue(double(this->Extent[3]));
    this->MinMax[2]->GetMinScale()->SetValue(double(this->Extent[4]));
    this->MinMax[2]->GetMaxScale()->SetValue(double(this->Extent[5]));

    this->ModifiedCallback();
  
    this->MinMax[0]->GetMinScale()->SetDisableCommands(0);
    this->MinMax[0]->GetMaxScale()->SetDisableCommands(0);
    this->MinMax[1]->GetMinScale()->SetDisableCommands(0);
    this->MinMax[1]->GetMaxScale()->SetDisableCommands(0);
    this->MinMax[2]->GetMinScale()->SetDisableCommands(0);
    this->MinMax[2]->GetMaxScale()->SetDisableCommands(0);
  }
}

void vtkPVHMExtractGrid::UpdateBoxWidgetFromSlidersCallback()
{
  this->SliderClicked = 1;
  this->UpdateBoxWidgetFromSliders();
  this->SliderClicked = 0;
}

void vtkPVHMExtractGrid::UpdateBoxWidgetFromSliders()
{
  if (this->UsingWidget && this->SliderClicked)
  {
  	this->Extent[0] = int(this->MinMax[0]->GetMinScale()->GetValue());
  	this->Extent[1] = int(this->MinMax[0]->GetMaxScale()->GetValue());
  	this->Extent[2] = int(this->MinMax[1]->GetMinScale()->GetValue());
  	this->Extent[3] = int(this->MinMax[1]->GetMaxScale()->GetValue());
  	this->Extent[4] = int(this->MinMax[2]->GetMinScale()->GetValue());
  	this->Extent[5] = int(this->MinMax[2]->GetMaxScale()->GetValue());	

    this->BoxWidget->SetExtent(this->Extent);
  }

}

void vtkPVHMExtractGrid::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMExtractGrid::Accept()
{
  this->Superclass::Accept();

  this->GetRange(this->Extent);
  this->AlreadyAccepted = 1;
}

void vtkPVHMExtractGrid::ModifiedCallback()
{
  this->Superclass::ModifiedCallback();
}

void vtkPVHMExtractGrid::ExecuteEvent(vtkObject *obj, unsigned long l, void *p)
{
  this->Superclass::ExecuteEvent(obj, l, p);
}

void vtkPVHMExtractGrid::Create(vtkKWApplication* kwApp)
{
  this->Superclass::Create(kwApp);
  
  this->MinMax[0]->GetMinScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
  this->MinMax[0]->GetMaxScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
  this->MinMax[1]->GetMinScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
  this->MinMax[1]->GetMaxScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
  this->MinMax[2]->GetMinScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
  this->MinMax[2]->GetMaxScale()->SetCommand(this, "UpdateBoxWidgetFromSlidersCallback");
}
