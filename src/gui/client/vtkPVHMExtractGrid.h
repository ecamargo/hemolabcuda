#ifndef _vtkPVHMExtractGrid_h_
#define _vtkPVHMExtractGrid_h_

#include "vtkObjectFactory.h"
#include "vtkPVExtentEntry.h"

class vtkPVApplication;
class vtkKWFrameWithLabel;
class vtkKWFrameWithLabel;

class vtkPVHMBoxWidget;

class VTK_EXPORT vtkPVHMExtractGrid : public vtkPVExtentEntry
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMExtractGrid *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMExtractGrid, vtkPVExtentEntry);

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  
  //Description:
  //This callback gets called when the user changes the widgets value, or a script changes the widgets value.
  virtual void ModifiedCallback();
  
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	
   
  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();

  // Description:
  // Updates the extent sliders from the widget box by LeftButtonPressEvent on the viewport WidgetBox.
  void UpdateSlidersFromBoxWidget();
  
  void UpdateBoxWidgetFromSliders();
  
  void UpdateBoxWidgetFromSlidersCallback();
  
protected:

  // Description:
  // Constructor
  vtkPVHMExtractGrid();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMExtractGrid();

  // Description:
  // Method executed when any iteraction is made on the viewport.
  virtual void	ExecuteEvent(vtkObject *obj, unsigned long l, void *p);

  // Description:
  // Method responsible for creating the Interface that interacts with this filter's source.
  virtual void Create(vtkKWApplication* kwApp);

  vtkPVHMBoxWidget *BoxWidget;

  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the Filter is using the HMBox Widget.  
  int UsingWidget;
  
  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;

  // Description:
  // Filter Image Data output extents.
  int Extent[6];

  // Description:
  // Filter Image Data output extents.
  double Bounds[6];
  
  bool SliderClicked;

  // Description:
  // Controls if the filter has already been accepted.
  int AlreadyAccepted;
  
private:  
  vtkPVHMExtractGrid(const vtkPVHMExtractGrid&); // Not implemented
  void operator=(const vtkPVHMExtractGrid&); // Not implemented
}; 

#endif
