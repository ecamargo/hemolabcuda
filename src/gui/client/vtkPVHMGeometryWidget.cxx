/*
 * $Id: vtkPVHMGeometryWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"

#include "vtkKWEntry.h"
#include "vtkKWLabel.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWPushButton.h"

#include "vtkPVHMGeometryWidget.h"

#include "vtkPVHMStraightModelWidget.h"

#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMSourceProxy.h"

vtkCxxRevisionMacro(vtkPVHMGeometryWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMGeometryWidget);

//----------------------------------------------------------------------------
vtkPVHMGeometryWidget::vtkPVHMGeometryWidget()
{ 
	this->SphereUpdateButton		= vtkKWPushButton::New();
	this->TubeUpdateButton			= vtkKWPushButton::New();
	this->AutoResizeButton			= vtkKWPushButton::New();    
   
  this->SphereResolutionLabel	= vtkKWLabel::New();
  this->SphereResolutionEntry = vtkKWEntry::New();
  this->SphereRadiusLabel			= vtkKWLabel::New();
  this->SphereRadiusEntry			= vtkKWEntry::New();

  this->TubeSidesLabel				= vtkKWLabel::New();
  this->TubeSidesEntry				= vtkKWEntry::New();
  this->TubeRadiusLabel				= vtkKWLabel::New();
  this->TubeRadiusEntry				= vtkKWEntry::New();
  
  this->SphereFrame						= vtkKWFrameWithLabel::New();	    
  this->TubeFrame							= vtkKWFrameWithLabel::New();
}

//----------------------------------------------------------------------------
vtkPVHMGeometryWidget::~vtkPVHMGeometryWidget()
{
	this->SphereUpdateButton->Delete();
	this->TubeUpdateButton->Delete();
	this->AutoResizeButton->Delete();

  this->SphereFrame->Delete();   
  this->SphereResolutionLabel->Delete();
  this->SphereResolutionEntry->Delete();
  this->SphereRadiusLabel->Delete();
  this->SphereRadiusEntry->Delete();

  this->TubeFrame->Delete();
  this->TubeSidesLabel->Delete();
  this->TubeSidesEntry->Delete();
  this->TubeRadiusLabel->Delete();
  this->TubeRadiusEntry->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMGeometryWidget::UpdateWidgetInfo()
{	
  int mode = this->GetReadOnly();
}

//----------------------------------------------------------------------------
void vtkPVHMGeometryWidget::UpdateEntryValues(double *values)
{
  this->SphereResolutionEntry->SetValueAsDouble(values[0]);	
  this->SphereRadiusEntry->SetValueAsDouble(values[1]);  	
  this->TubeSidesEntry->SetValueAsDouble(values[2]);  
  this->TubeRadiusEntry->SetValueAsDouble(values[3]);     
}

//----------------------------------------------------------------------------
void vtkPVHMGeometryWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Geometry Properties"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
        
  // Sphere Settings     
  this->SphereFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->SphereFrame->Create(pvApp);
  this->SphereFrame->SetLabelText("Sphere Settings");
  
  this->SphereResolutionLabel->SetParent(this->SphereFrame->GetFrame());
  this->SphereResolutionLabel->Create(pvApp);
  this->SphereResolutionLabel->SetText("Resolution: ");
	this->SphereResolutionEntry->SetParent(this->SphereFrame->GetFrame());
  this->SphereResolutionEntry->Create(pvApp);
  this->SphereResolutionEntry->SetValueAsDouble(0.0);
  
  this->SphereRadiusLabel->SetParent(this->SphereFrame->GetFrame());
  this->SphereRadiusLabel->Create(pvApp);
  this->SphereRadiusLabel->SetText("Radius:        ");
	this->SphereRadiusEntry->SetParent(this->SphereFrame->GetFrame());
  this->SphereRadiusEntry->Create(pvApp);
  this->SphereRadiusEntry->SetValueAsDouble(0.0);
  
	this->SphereUpdateButton->SetParent(this->SphereFrame->GetFrame());
	this->SphereUpdateButton->Create(pvApp);
	this->SphereUpdateButton->SetText("Update");	  

	// Tube Settings
  this->TubeFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->TubeFrame->Create(pvApp);
  this->TubeFrame->SetLabelText("Tube Settings");
  
  this->TubeSidesLabel->SetParent(this->TubeFrame->GetFrame());
  this->TubeSidesLabel->Create(pvApp);
  this->TubeSidesLabel->SetText("Sides: ");
	this->TubeSidesEntry->SetParent(this->TubeFrame->GetFrame());
  this->TubeSidesEntry->Create(pvApp);
  this->TubeSidesEntry->SetValueAsDouble(0.0);
  
  this->TubeRadiusLabel->SetParent(this->TubeFrame->GetFrame());
  this->TubeRadiusLabel->Create(pvApp);
  this->TubeRadiusLabel->SetText("Radius: ");
	this->TubeRadiusEntry->SetParent(this->TubeFrame->GetFrame());
  this->TubeRadiusEntry->Create(pvApp);
  this->TubeRadiusEntry->SetValueAsDouble(0.0);

	this->TubeUpdateButton->SetParent(this->TubeFrame->GetFrame());
	this->TubeUpdateButton->Create(pvApp);
	this->TubeUpdateButton->SetText("Update");	
	
	this->AutoResizeButton->SetParent(this->GetChildFrame()->GetFrame());
	this->AutoResizeButton->Create(pvApp);
	this->AutoResizeButton->SetText("Auto-Resize");	        
}

void vtkPVHMGeometryWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		  mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
			return;
		}  
	this->GetChildFrame()->CollapseFrame();		
	
  mainWidget->Script("grid %s -sticky ew -columnspan 2",
    this->GetChildFrame()->GetWidgetName()); 			 

  mainWidget->Script("grid %s -sticky ew",
    this->SphereFrame->GetWidgetName());

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->SphereResolutionLabel->GetWidgetName(),
    this->SphereResolutionEntry->GetWidgetName());    

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->SphereRadiusLabel->GetWidgetName(),
    this->SphereRadiusEntry->GetWidgetName());  

  mainWidget->Script("grid %s -sticky ew -columnspan 6",     
    this->SphereUpdateButton->GetWidgetName());  

  mainWidget->Script("grid %s -sticky ew",
    this->TubeFrame->GetWidgetName());

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->TubeSidesLabel->GetWidgetName(),
    this->TubeSidesEntry->GetWidgetName());    

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->TubeRadiusLabel->GetWidgetName(),
    this->TubeRadiusEntry->GetWidgetName());  
    
  mainWidget->Script("grid %s -sticky ew -columnspan 6",     
    this->TubeUpdateButton->GetWidgetName());   
    
  mainWidget->Script("grid %s -sticky ew -columnspan 2",     
    this->AutoResizeButton->GetWidgetName());         
}
  
//----------------------------------------------------------------------------  
vtkKWFrameWithLabel* vtkPVHMGeometryWidget::GetGeometryFrame()
{
	return this->GetChildFrame();
}

//----------------------------------------------------------------------------
double vtkPVHMGeometryWidget::GetSphereResolutionEntry()
{
	return this->SphereResolutionEntry->GetValueAsDouble();	 	
}

//----------------------------------------------------------------------------	
double vtkPVHMGeometryWidget::GetSphereRadiusEntry()
{
	return this->SphereRadiusEntry->GetValueAsDouble();	 	
}

//----------------------------------------------------------------------------
double vtkPVHMGeometryWidget::GetTubeSidesEntry()
{
	return this->TubeSidesEntry->GetValueAsDouble();	 	
}

//----------------------------------------------------------------------------
double vtkPVHMGeometryWidget::GetTubeRadiusEntry()
{
	return this->TubeRadiusEntry->GetValueAsDouble();	 	
}
		
//----------------------------------------------------------------------------
void vtkPVHMGeometryWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMGeometryWidget::Accept()
{  		
	this->UpdateWidgetInfo();
}

