/*
 * $Id: vtkPVHMGeometryWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMGeometryWidget
// .SECTION Description
// Creates KW Components for Geometric changes

#ifndef _vtkPVHMGeometryWidget_h_
#define _vtkPVHMGeometryWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"

class vtkKWFrame;
class vtkKWEntry;
class vtkKWLabel;
class vtkKWPushButton;
class vtkKWFrameWithLabel;

class vtkPVHM1DStraightModelSourceWidget;
class vtkPVHMStraightModelWidget;

class VTK_EXPORT vtkPVHMGeometryWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMGeometryWidget *New();
  vtkTypeRevisionMacro(vtkPVHMGeometryWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget *);
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();
  
  // Description:
  // Get Segment Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetGeometryFrame();
    
  // Description:
  // Change Geometry Entry values
	void UpdateEntryValues(double *values);     
	
	// Description:
	// Get Sphere Resolution Entry Values
	double GetSphereResolutionEntry();	

	// Description:
	// Get Sphere Radius Entry Values
	double GetSphereRadiusEntry();
	
	// Description:
	// Get Tube Sides Entry Values
	double GetTubeSidesEntry();
	
	// Description:
	// Get Tube Radius Values	
	double GetTubeRadiusEntry();			
	
public:
  vtkKWPushButton 			*SphereUpdateButton;
  vtkKWPushButton 			*TubeUpdateButton;  
  vtkKWPushButton 			*AutoResizeButton;    
  
protected:
  vtkPVHMGeometryWidget();
  virtual ~vtkPVHMGeometryWidget();

  vtkKWFrameWithLabel*	SphereFrame;	    
  vtkKWLabel* SphereResolutionLabel;
  vtkKWEntry* SphereResolutionEntry;  
  vtkKWLabel* SphereRadiusLabel;
  vtkKWEntry* SphereRadiusEntry;  

  vtkKWFrameWithLabel*	TubeFrame;	    
  vtkKWLabel* TubeSidesLabel;
  vtkKWEntry* TubeSidesEntry;  
  vtkKWLabel* TubeRadiusLabel;
  vtkKWEntry* TubeRadiusEntry;  

private:  
  vtkPVHMGeometryWidget(const vtkPVHMGeometryWidget&); // Not implemented
  void operator=(const vtkPVHMGeometryWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMGeometryWidget_h_*/


