/*
 * $Id: vtkPVHMHeartWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWIcon.h"
#include "vtkKWRadioButton.h"
#include "vtkKWComboBox.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLoadSaveButton.h"

#include "vtkSMIntVectorProperty.h"

#include "vtkDoubleArray.h"
#include "vtkMath.h"
#include "vtkStringArray.h"
#include "vtkDataArrayCollection.h"

#include "vtkPVHMStraightModelWidget.h"
#include "vtkPVHMHeartWidget.h"

#include "vtkHM1DHeartCurveReader.h"
#include "vtkHM1DHeartCurveWriter.h"

#include <cmath>

vtkCxxRevisionMacro(vtkPVHMHeartWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMHeartWidget);

//----------------------------------------------------------------------------
vtkPVHMHeartWidget::vtkPVHMHeartWidget()
{
  this->R1Label				= vtkKWLabel::New();
  this->R1Entry 			= vtkKWEntry::New();
  this->R2Label				= vtkKWLabel::New();
  this->R2Entry 			= vtkKWEntry::New();
  this->CapLabel			= vtkKWLabel::New();
  this->CapEntry			= vtkKWEntry::New();
  this->PressureLabel	= vtkKWLabel::New();
  this->PressureEntry = vtkKWEntry::New();  
  this->TerminalIcon	= vtkKWLabel::New();	
  this->PlotHeartButton =  vtkKWPushButton::New();
  this->BlankLine = vtkKWLabel::New();
  
  this->FinalTimeLabel	= vtkKWLabel::New();
  this->FinalTimeEntry 	= vtkKWEntry::New();
  
  this->MaxFlowLabel		= vtkKWLabel::New();
  this->MaxFlowEntry 		= vtkKWEntry::New();
  
  this->MinFlowLabel		= vtkKWLabel::New();
  this->MinFlowEntry		= vtkKWEntry::New();
  
  this->UpdatePropButton = vtkKWPushButton::New();
  
  this->PressureImposes = vtkKWRadioButton::New();
  this->FlowImposes = vtkKWRadioButton::New();
  
  this->FunctionHeartCurve = vtkKWComboBox::New();
  this->FunctionLabel	= vtkKWLabel::New();
  
  this->NumberOfPointsLabel = vtkKWLabel::New();
  this->NumberOfPointsEntry = vtkKWEntry::New();
  this->SetNumberOfPoints(12);
	
	this->Functions[0] = "None";
	this->Functions[1] = "sen(x)";
	this->Functions[2] = "sen^2(x) 1/2 period";
	this->Functions[3] = "sen^2(x)";
	
	this->AllFunctions = vtkStringArray::New();
	this->AllFunctions->InsertNextValue(this->Functions[0]);
	this->AllFunctions->InsertNextValue(this->Functions[1]);
	this->AllFunctions->InsertNextValue(this->Functions[2]);
	this->AllFunctions->InsertNextValue(this->Functions[3]);
	
	this->AddCurveButton = vtkKWLoadSaveButton::New();
	this->FunctionsCollection = vtkDataArrayCollection::New();
	this->PressureCurveCollection = vtkDataArrayCollection::New();
	this->TimePressureCurveCollection = vtkDataArrayCollection::New();
	
	this->SaveCurveButton = vtkKWPushButton::New();
	
	this->ChooseDirectoryDialog = vtkKWLoadSaveDialog::New();
	
  this->ScaleFactorLabel = vtkKWLabel::New();
  this->ScaleFactorEntry = vtkKWEntry::New();
  
  this->PointsScaleInitLabel = vtkKWLabel::New();
  this->PointsScaleInitEntry = vtkKWEntry::New();
  this->PointsScaleEndLabel = vtkKWLabel::New();
  this->PointsScaleEndEntry = vtkKWEntry::New();
}

//----------------------------------------------------------------------------
vtkPVHMHeartWidget::~vtkPVHMHeartWidget()
{
  this->R1Label->Delete();
  this->R1Entry->Delete();
  this->R2Label->Delete();
  this->R2Entry->Delete();  
  this->CapLabel->Delete();  
  this->CapEntry->Delete();  
  this->PressureLabel->Delete();  
  this->PressureEntry->Delete(); 
  this->TerminalIcon->Delete();
  this->PlotHeartButton->Delete();
  this->BlankLine->Delete();
  
  this->FinalTimeLabel->Delete();
  this->FinalTimeEntry->Delete();
  
  this->MaxFlowLabel->Delete();
  this->MaxFlowEntry->Delete();
  
  this->MinFlowLabel->Delete();
  this->MinFlowEntry->Delete();
  
  this->UpdatePropButton->Delete();
  
  this->PressureImposes->Delete();
  this->FlowImposes->Delete();
  
  this->FunctionHeartCurve->Delete();
  this->FunctionLabel->Delete();
  
  this->NumberOfPointsLabel->Delete();
  this->NumberOfPointsEntry->Delete();
  
  this->AddCurveButton->Delete();
  this->AllFunctions->Delete();
  
  this->FunctionsCollection->RemoveAllItems();
  this->FunctionsCollection->Delete();
  
  this->PressureCurveCollection->Delete();
  this->TimePressureCurveCollection->Delete();
  
  this->SaveCurveButton->Delete();
  this->ChooseDirectoryDialog->Delete();
  
  this->ScaleFactorLabel->Delete();
  this->ScaleFactorEntry->Delete();
  
  this->PointsScaleInitLabel->Delete();
  this->PointsScaleInitEntry->Delete();
  this->PointsScaleEndLabel->Delete();
  this->PointsScaleEndEntry->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::UpdateWidgetInfo()
{
	int mode = this->GetReadOnly();
  this->R1Entry->SetReadOnly(mode);
	this->R2Entry->SetReadOnly(mode);	  
 	this->CapEntry->SetReadOnly(mode);	  
  this->PressureEntry->SetReadOnly(mode);
  
  this->FinalTimeEntry->SetReadOnly(mode);
  this->MaxFlowEntry->SetReadOnly(mode);
  this->MinFlowEntry->SetReadOnly(mode);
  
  this->PressureImposes->SetEnabled(!mode);
  this->FlowImposes->SetEnabled(!mode);
  
  this->FunctionHeartCurve->SetEnabled(!mode);
  
  this->NumberOfPointsEntry->SetReadOnly(mode);
  
  this->AddCurveButton->SetEnabled(!mode);
  this->UpdatePropButton->SetEnabled(!mode);
  this->SaveCurveButton->SetEnabled(!mode);
  
  this->ScaleFactorEntry->SetReadOnly(mode);
  
  this->PointsScaleInitEntry->SetReadOnly(mode);
  this->PointsScaleEndEntry->SetReadOnly(mode);
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Heart Information"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
      
  this->R1Label->SetParent(this->GetChildFrame()->GetFrame());
  this->R1Label->Create(pvApp);
  this->R1Label->SetText("Resistance 1 [dyn.sec/cm^5]:");
  this->R1Entry->SetParent(this->GetFrame());
  this->R1Entry->SetWidth(10);
  this->R1Entry->Create(pvApp);
  this->R1Entry->SetValue("50000");
  
  this->R2Label->SetParent(this->GetChildFrame()->GetFrame());
  this->R2Label->Create(pvApp);
  this->R2Label->SetText("Resistance 2 [dyn.sec/cm^5]:");
  this->R2Entry->SetParent(this->GetFrame());
  this->R2Entry->SetWidth(10);
  this->R2Entry->Create(pvApp);
  this->R2Entry->SetValue("50000");
  
  this->CapLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->CapLabel->Create(pvApp);
  this->CapLabel->SetText("Capacitance [cm^5/dyn]:");
  this->CapEntry->SetParent(this->GetFrame());
  this->CapEntry->SetWidth(10);
  this->CapEntry->Create(pvApp);
  
  this->PressureLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PressureLabel->Create(pvApp);
  this->PressureLabel->SetText("Pressure [dyn/cm^2]:");
  this->PressureEntry->SetParent(this->GetFrame());
  this->PressureEntry->SetWidth(10);
  this->PressureEntry->Create(pvApp);
  
  this->BlankLine->SetParent(this->GetChildFrame()->GetFrame());
  this->BlankLine->Create(pvApp);   
  	
  this->PlotHeartButton->SetParent(this->GetChildFrame()->GetFrame());
  this->PlotHeartButton->Create(pvApp);
  this->PlotHeartButton->SetText("Plot Heart Curve");	
  //this->PlotHeartButton->SetBackgroundColor(1,0,0);
	
	// Carrega imagem do Terminal
	static const unsigned char image_Terminal_Prop[] = 
  "eNrt20sOgzAMRdHuf1XsjEodReSDCYlrP12rAwatymmIsS16HARBEMSu+BQhIzqLl4DrIt"
  "JwgWJPkf22uiRvVaBAgQIlj2qVEicomKCcCYMqMUKeeY8qFWGTp8VYnnx5IIAKu1Jr95QA"
  "qlbEvPx+J3XPvOyp5nG8jv4Bqs4VFLSgQIGKmtwWBKj4VxcoUBqocQfd+6IdAwc76vad47"
  "6s7/0bypIqmx30RIU8wWx+5FG/3zuxuxWxXxLeqDd7amFj65aR5jroLKjmsb16mZpI7N1T"
  "vfyQFOU5LHJL6elQbgWVcQKWsfaTRElW6bTz8VGMyECBAuWAqhNg/N9hYlQSHDU3Kol88z"
  "I+wp1opewP2yfaU09RveGJGEpgTyW6u6n+LYIgCCJUfAFLLO5E";
	vtkKWIcon* icon = vtkKWIcon::New();
  this->TerminalIcon->SetParent(this->GetChildFrame()->GetFrame());
  this->TerminalIcon->Create(pvApp);
  icon->SetImage(image_Terminal_Prop,71,78,3,400,16614);
  this->TerminalIcon->SetImageToIcon(icon); 
  icon->Delete();
  
  
  this->FinalTimeLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->FinalTimeLabel->Create(pvApp);
  this->FinalTimeLabel->SetText("Final Time:");
  this->FinalTimeEntry->SetParent(this->GetFrame());
  this->FinalTimeEntry->SetWidth(10);
  this->FinalTimeEntry->Create(pvApp);
  
  this->MaxFlowLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->MaxFlowLabel->Create(pvApp);
  this->MaxFlowLabel->SetText("Maximum Value:");
  this->MaxFlowEntry->SetParent(this->GetFrame());
  this->MaxFlowEntry->SetWidth(10);
  this->MaxFlowEntry->Create(pvApp);
  
  this->MinFlowLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->MinFlowLabel->Create(pvApp);
  this->MinFlowLabel->SetText("Minimum Value:");
  this->MinFlowEntry->SetParent(this->GetFrame());
  this->MinFlowEntry->SetWidth(10);
  this->MinFlowEntry->Create(pvApp);
  
  this->UpdatePropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdatePropButton->Create(pvApp);
  this->UpdatePropButton->SetText("Update Heart");
  
  this->PressureImposes->SetParent(this->GetChildFrame()->GetFrame());
  this->PressureImposes->Create(pvApp);
  this->PressureImposes->SetText("Imposes Pressure Condition");
  this->PressureImposes->SetValue("P");
  this->PressureImposes->SetBalloonHelpString("Imposes pressure curve");
  
  this->FlowImposes->SetParent(this->GetChildFrame()->GetFrame());
  this->FlowImposes->Create(pvApp);
  this->FlowImposes->SetText("Imposes Flow Condition");
  this->FlowImposes->SetValue("F");
  this->FlowImposes->SetBalloonHelpString("Imposes flow curve");
  
  this->FlowImposes->SetSelectedState(1);
  this->SelectedImposes = 'F';

	this->FunctionHeartCurve->SetParent(this->GetChildFrame()->GetFrame());
	this->FunctionHeartCurve->Create(pvApp);
	this->FunctionHeartCurve->SetBalloonHelpString("Automatic generation of the heart curve with function selected");
	this->FunctionHeartCurve->AddValue(this->Functions[0]);
	this->FunctionHeartCurve->AddValue(this->Functions[1]);
	this->FunctionHeartCurve->AddValue(this->Functions[2]);
	this->FunctionHeartCurve->AddValue(this->Functions[3]);
	
	this->FunctionHeartCurve->SetValue(this->Functions[0]);
	this->FunctionSelected = vtkPVHMHeartWidget::none;
	
	this->FunctionLabel->SetParent(this->GetChildFrame()->GetFrame());
	this->FunctionLabel->Create(pvApp);
	this->FunctionLabel->SetText("Automatic Function of the Heart");
	this->FunctionLabel->SetBalloonHelpString("Automatic generation of the heart curve with function selected");
	
	this->NumberOfPointsLabel->SetParent(this->GetChildFrame()->GetFrame());
	this->NumberOfPointsLabel->Create(pvApp);
	this->NumberOfPointsLabel->SetText("Number of Points of the Curve:");
	this->NumberOfPointsLabel->SetBalloonHelpString("Set Number of Points of the Curve");
	
	this->NumberOfPointsEntry->SetParent(this->GetChildFrame()->GetFrame());
	this->NumberOfPointsEntry->SetWidth(10);
	this->NumberOfPointsEntry->Create(pvApp);
	this->NumberOfPointsEntry->SetBalloonHelpString("Set Number of Points of the Curve");
	
	this->AddCurveButton->SetParent(this->GetChildFrame()->GetFrame());
	this->AddCurveButton->Create(pvApp);
	this->AddCurveButton->SetText("Add Function");
	this->AddCurveButton->GetLoadSaveDialog()->ChooseDirectoryOff();
  this->AddCurveButton->GetLoadSaveDialog()->SetDefaultExtension("txt");
	this->AddCurveButton->SetBalloonHelpString("Add curve of one (.txt) file in heart");
	
	this->SaveCurveButton->SetParent(this->GetChildFrame()->GetFrame());
	this->SaveCurveButton->Create(pvApp);
	this->SaveCurveButton->SetText("Save Curve");
	this->SaveCurveButton->SetBalloonHelpString("Save curve edited");
//	this->SaveCurveButton->GetLoadSaveDialog()->SaveDialogOn();
	
	this->ChooseDirectoryDialog->Create(pvApp);
	this->ChooseDirectoryDialog->SetTitle("Save Heart Curve");
	this->ChooseDirectoryDialog->SaveDialogOn();
//	this->SaveCurveButton->TrimPathFromFileNameOff();
//	this->SaveCurveButton->SetCommand(this, "");
	
	this->ScaleFactorLabel->SetParent(this->GetChildFrame()->GetFrame());
	this->ScaleFactorLabel->Create(pvApp);
	this->ScaleFactorLabel->SetText("Multiplicative Scale factor:");
	
  this->ScaleFactorEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->ScaleFactorEntry->Create(pvApp);
  this->ScaleFactorEntry->SetWidth(10);
  this->SetScaleFactor(1);
  
  this->PointsScaleInitLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PointsScaleInitLabel->Create(pvApp);
  this->PointsScaleInitLabel->SetText("Points:");
  
  this->PointsScaleInitEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->PointsScaleInitEntry->Create(pvApp);
  this->PointsScaleInitEntry->SetWidth(10);
  this->PointsScaleInitEntry->SetBalloonHelpString("Points to calculate multiplicative scale factor");
  
  this->PointsScaleEndLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PointsScaleEndLabel->Create(pvApp);
  this->PointsScaleEndLabel->SetText("To:");
  
  this->PointsScaleEndEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->PointsScaleEndEntry->Create(pvApp);
  this->PointsScaleEndEntry->SetWidth(10);
  this->PointsScaleEndEntry->SetBalloonHelpString("Points to calculate multiplicative scale factor");
}

 
//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
	int mode = mainWidget->GetWidgetMode();
	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		  mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
			return;
		} 		
	this->GetChildFrame()->ExpandFrame();			
			
  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName());  
    
  mainWidget->Script("grid %s - - -sticky w -pady 2",     
    this->TerminalIcon->GetWidgetName());  
	
	mainWidget->Script("grid %s - - -sticky w",     
    this->FunctionLabel->GetWidgetName());
  
  mainWidget->Script("grid %s %s - - -sticky w -pady 5 -columnspan 4",     
    this->FunctionHeartCurve->GetWidgetName(), this->AddCurveButton->GetWidgetName());
	
	mainWidget->Script("grid %s %s - - -sticky w -pady 5 -columnspan 4",     
    this->NumberOfPointsLabel->GetWidgetName(),
    this->NumberOfPointsEntry->GetWidgetName());
	
	mainWidget->Script("grid %s - - -sticky w",     
    this->PressureImposes->GetWidgetName());
	
	mainWidget->Script("grid %s - - -sticky w -pady 7",     
    this->FlowImposes->GetWidgetName());
	
	mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",     
    this->FinalTimeLabel->GetWidgetName(),
    this->FinalTimeEntry->GetWidgetName());
	
	mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",     
    this->MinFlowLabel->GetWidgetName(),
    this->MinFlowEntry->GetWidgetName());
  
  mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",     
    this->MaxFlowLabel->GetWidgetName(),
    this->MaxFlowEntry->GetWidgetName());
	
  mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",
    this->R1Label->GetWidgetName(),
    this->R1Entry->GetWidgetName());

  mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",
    this->R2Label->GetWidgetName(),
    this->R2Entry->GetWidgetName());  

  mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",
    this->CapLabel->GetWidgetName(),
    this->CapEntry->GetWidgetName()); 

  mainWidget->Script("grid %s %s - - -sticky w -columnspan 4",
    this->PressureLabel->GetWidgetName(),
    this->PressureEntry->GetWidgetName()); 
  
  mainWidget->Script("grid %s %s - - -sticky w -pady 7 -columnspan 4",
  	this->ScaleFactorLabel->GetWidgetName(), this->ScaleFactorEntry->GetWidgetName());
  
  mainWidget->Script("grid %s %s %s %s - - -sticky w",
  	this->PointsScaleInitLabel->GetWidgetName(),
  	this->PointsScaleInitEntry->GetWidgetName(),
  	this->PointsScaleEndLabel->GetWidgetName(),
  	this->PointsScaleEndEntry->GetWidgetName());
  
  mainWidget->Script("grid %s - - -sticky w -columnspan 4",     
    this->BlankLine->GetWidgetName());
  
  mainWidget->Script("grid %s %s %s -sticky w -columnspan 3",     
    this->PlotHeartButton->GetWidgetName(), this->UpdatePropButton->GetWidgetName(), this->SaveCurveButton->GetWidgetName());
  
//  if(mode)
//  	mainWidget->Script("grid remove %s",
//  		this->UpdatePropButton->GetWidgetName());
//  else
//    mainWidget->Script("grid %s - - -sticky w", 
//  		this->UpdatePropButton->GetWidgetName());
	
	this->R1Entry->SetBalloonHelpString("Resistive element R1 from the Windkessel model."); 
	this->R2Entry->SetBalloonHelpString("Resistive element R2 from the Windkessel model."); 
	this->CapEntry->SetBalloonHelpString("Capacitive element C from the Windkessel model."); 
	this->PressureEntry->SetBalloonHelpString("Heart pressure Pt."); 			
	this->PlotHeartButton->SetBalloonHelpString("Push Button to Visualize the Heart Curve."); 
	
	this->FinalTimeEntry->SetBalloonHelpString("Final time of the simulation from the Windkessel model.");
	this->MaxFlowEntry->SetBalloonHelpString("Maximum point of the curve from the Windkessel model.");
	this->MinFlowEntry->SetBalloonHelpString("Minimum point of the curve from the Windkessel model.");
	this->UpdatePropButton->SetBalloonHelpString("Push Button to update values."); 
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkPVHMHeartWidget::CalculateCurve(double Amplitude, double time)
{
	double f;
	double n;
	double x = 0;
	
	int select = this->GetFunctionSelected();
	
	int numberOfPoints = this->GetNumberOfPoints();
	
	if ( numberOfPoints < 2 )
		numberOfPoints = 2;
	
	//Calcula o intervalo de tempo entre um ponto e outro
	n = time/(numberOfPoints-1);
	
	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfComponents(2);
	array->SetNumberOfTuples(numberOfPoints);
	
	switch(select)
	{
		/*
		 * Caso nenhuma das funções esteja selecionada, adiciona dois pontos na curva.
		 */
		case 0:
			array->SetNumberOfTuples(2);
			array->SetTuple2(0, 0, 0);
			array->SetTuple2(1, time, 0);
			break;
		
		case 1:
			/*
			 * Calculate function sen(x)
			 * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*sin(2*(pi/T)*x)
			 */
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * sin(2*(vtkMath::Pi() / time ) * x);
				array->SetTuple2(i, x, f);
				x += n;
				
				}
			break;
		
		case 2:
			/*
			 * Calculate function sen^2 (x) 1/2 period
			 * * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*(sin((pi/T)*x))^2
			 */
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * pow((sin((vtkMath::Pi() / time ) * x)),2);
				array->SetTuple2(i, x, f);
				x += n;
				
				}
			break;
		
		case 3:
			/*
			 * Calculate function sen^2 (x)
			 * * A = Amplitude
			 * T = period
			 * x = time instant
			 * 
			 * f=A*(sin(2*(pi/T)*x))^2
			 */
			for ( int i=0; i<numberOfPoints; i++ )
				{
				f = Amplitude * pow((sin(2*(vtkMath::Pi() / time ) * x)),2);
				array->SetTuple2(i, x, f);
				x += n;
				
				}
			break;
		
		default:
			
			vtkDoubleArray *p = vtkDoubleArray::SafeDownCast(this->PressureCurveCollection->GetItem(select-4));
			vtkDoubleArray *pTime = vtkDoubleArray::SafeDownCast(this->TimePressureCurveCollection->GetItem(select-4));
			
			vtkDoubleArray *array2 = vtkDoubleArray::SafeDownCast(this->FunctionsCollection->GetItem(select-4));
			
			int i=0;
			//r1Time
			int n = (int)array2->GetValue(i);
			
			i = n;
			i++;
			//r1
			n = (int)array2->GetValue(i);
			
			i+=n;
			
			double R1 = array2->GetValue(i);
			i++;
			
			//r2Time
			n = (int)array2->GetValue(i);
			
			i+=n;	
			i++;
			
			//r2
			n = (int)array2->GetValue(i);
			i+=n;
			double R2 = array2->GetValue(i);
			
			double resistence = R1+R2;
			this->SetImposes('F');
			if ( resistence <= 0 )
				{
				resistence = 1;
				this->SetImposes('P');
				}
			
			array->SetNumberOfTuples(p->GetNumberOfTuples());
			for ( int j=0; j<p->GetNumberOfTuples(); j++ )
				{
				array->SetTuple2(j, pTime->GetValue(j), p->GetValue(j)/resistence);
				}
			
			double range[2];
			p->ComputeRange(0);
			
			p->GetRange(range, 0);
			
			this->SetNumberOfPoints(p->GetNumberOfTuples());
			this->SetFinalTime(pTime->GetValue(pTime->GetNumberOfTuples()-1));
			this->SetR1(R1);
			this->SetR2(R2);
			this->SetP(range[1]/resistence);
			this->SetMaxFlow(range[1]/resistence);
			this->SetMinFlow(range[0]/resistence);
			
			
			break;
		
	}
	return array;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkPVHMHeartWidget::ReadCurve()
{
	int result;
	char *FileName;
	FileName = this->AddCurveButton->GetFileName();
	
	vtkHM1DHeartCurveReader *reader = vtkHM1DHeartCurveReader::New();

	result = reader->ReadFile(FileName);
	
	if ( !result )
		return NULL;
	
	char *str = this->AddCurveButton->GetText();
	
	//Pegar os parametros lidos do arquivo
	vtkDoubleArray *r1 = reader->GetR1();
	vtkDoubleArray *r2 = reader->GetR2();
	vtkDoubleArray *c  = reader->GetC();
	vtkDoubleArray *p  = reader->GetP();
	
	vtkDoubleArray *r1Time = reader->GetR1Time();
	vtkDoubleArray *r2Time = reader->GetR2Time();
	vtkDoubleArray *cTime  = reader->GetCTime();
	vtkDoubleArray *pTime  = reader->GetPTime();
	
	//Array com todos os parametros lidos.
	vtkDoubleArray *array = vtkDoubleArray::New();
	
	//Setar os parametros, inserindo primeiro o numero de parametros
	//e em seguida os parametros.
	//Sequencia de insercao:
	//r1Time; r1, r2Time, r2, cTime, c, pTime, p
	//r1
	array->InsertNextValue(r1Time->GetNumberOfTuples());
	for ( int i=0; i<r1Time->GetNumberOfTuples(); i++ )
		array->InsertNextValue(r1Time->GetValue(i));
	
	array->InsertNextValue(r1->GetNumberOfTuples());
	for ( int i=0; i<r1->GetNumberOfTuples(); i++ )
		array->InsertNextValue(r1->GetValue(i));
	
	//r2
	array->InsertNextValue(r2Time->GetNumberOfTuples());
	for ( int i=0; i<r2Time->GetNumberOfTuples(); i++ )
		array->InsertNextValue(r2Time->GetValue(i));
	
	array->InsertNextValue(r2->GetNumberOfTuples());
	for ( int i=0; i<r2->GetNumberOfTuples(); i++ )
		array->InsertNextValue(r2->GetValue(i));
	
	//c
	array->InsertNextValue(cTime->GetNumberOfTuples());
	for ( int i=0; i<cTime->GetNumberOfTuples(); i++ )
		array->InsertNextValue(cTime->GetValue(i));
	
	array->InsertNextValue(c->GetNumberOfTuples());
	for ( int i=0; i<c->GetNumberOfTuples(); i++ )
		array->InsertNextValue(c->GetValue(i));
	
	//p
	array->InsertNextValue(pTime->GetNumberOfTuples());
	for ( int i=0; i<pTime->GetNumberOfTuples(); i++ )
		array->InsertNextValue(pTime->GetValue(i));
	
	array->InsertNextValue(p->GetNumberOfTuples());
	for ( int i=0; i<p->GetNumberOfTuples(); i++ )
		array->InsertNextValue(p->GetValue(i));
	
	
	for ( int i=0; i<this->AllFunctions->GetNumberOfValues(); i++ )
		{
		if ( !strcmp(this->FunctionHeartCurve->GetValueFromIndex(i), str) )
			{
			vtkDoubleArray *old = vtkDoubleArray::SafeDownCast(this->FunctionsCollection->GetItem(i-4));
			old->SetNumberOfTuples(array->GetNumberOfTuples());
			
			for ( int j=0; j<array->GetNumberOfTuples(); j++ )
				old->SetValue(j, array->GetValue(j));
			
			vtkDoubleArray *oldPressure = vtkDoubleArray::SafeDownCast(this->PressureCurveCollection->GetItem(i-4));
			vtkDoubleArray *oldTime = vtkDoubleArray::SafeDownCast(this->TimePressureCurveCollection->GetItem(i-4));
			
			oldPressure->SetNumberOfTuples(p->GetNumberOfTuples());
			oldTime->SetNumberOfTuples(pTime->GetNumberOfTuples());
			
			for ( int j=0; j<oldPressure->GetNumberOfTuples(); j++ )
			{
			oldPressure->SetValue(j, p->GetValue(j));
			oldTime->SetValue(j, pTime->GetValue(j));
			}
			
			this->FunctionHeartCurve->SetValue(str);
			reader->Delete();
			return array;
			}
		}
	
	this->FunctionHeartCurve->AddValue(str);
	this->FunctionHeartCurve->SetValue(str);
	this->AllFunctions->InsertNextValue(str);
	this->FunctionsCollection->AddItem(array);
	
	this->PressureCurveCollection->AddItem(p);
	this->TimePressureCurveCollection->AddItem(pTime);
	
	reader->Delete();
	
	return array;
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::WriteCurve(vtkDoubleArray *array)
{
	char *FileName;
	
	int select = this->GetFunctionSelected();
	
	this->ChooseDirectoryDialog->Invoke();
	if (this->ChooseDirectoryDialog->GetLastPath())
		FileName = this->ChooseDirectoryDialog->GetFileName();
	
	vtkHM1DHeartCurveWriter *writer = vtkHM1DHeartCurveWriter::New();
	
	writer->WriteData(FileName, array);
	
	writer->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::InsertFunction(char *str)
{
	this->AllFunctions->InsertNextValue(str);
}

//----------------------------------------------------------------------------
vtkKWFrameWithLabel* vtkPVHMHeartWidget::GetTerminalFrame()
{
	return this->GetChildFrame();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::Accept()
{
}

//----------------------------------------------------------------------------

void vtkPVHMHeartWidget::SetR1(const char *values)
{
	this->R1Entry->SetValue(values);
}
  
//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetR2(const char *values)
{
	this->R2Entry->SetValue(values);
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetCap(const char *values)
{

	this->CapEntry->SetValue(values);
	
}
//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetP(const char *values)
{
	this->PressureEntry->SetValue(values);
	
}
//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetR1(double value)
{
	this->R1Entry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetR2(double value)
{
	this->R2Entry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetCap(double value)
{
	this->CapEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetP(double value)
{
	this->PressureEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetTerminalLabel(char *text)
{
	this->GetChildFrame()->SetLabelText(text); 
}

//---------------------------------------------------------------------------- 

double vtkPVHMHeartWidget::GetR1(void)
{
	return this->R1Entry->GetValueAsDouble();
}


//---------------------------------------------------------------------------- 

double vtkPVHMHeartWidget::GetR2(void)
{
	return this->R2Entry->GetValueAsDouble();
}

//---------------------------------------------------------------------------- 

double vtkPVHMHeartWidget::GetCap(void)
{
	return this->CapEntry->GetValueAsDouble();
}

//---------------------------------------------------------------------------- 

double vtkPVHMHeartWidget::GetP(void)
{
	return this->PressureEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetFinalTime(double value)
{
	this->FinalTimeEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
double vtkPVHMHeartWidget::GetFinalTime()
{
	return this->FinalTimeEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetMaxFlow(double value)
{
	this->MaxFlowEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------  
double vtkPVHMHeartWidget::GetMaxFlow()
{
	return this->MaxFlowEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------  
void vtkPVHMHeartWidget::SetMinFlow(double value)
{
	this->MinFlowEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------
double vtkPVHMHeartWidget::GetMinFlow()
{
	return this->MinFlowEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------
char vtkPVHMHeartWidget::GetImposes()
{
	return this->SelectedImposes;
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::SetImposes(char v)
{
	switch(v)
	{
		case 'F':
			this->FlowImposes->SetSelectedState(1);
			this->PressureImposes->SetSelectedState(0);
			this->SelectedImposes = 'F';
			break;
		
		case 'P':
			this->FlowImposes->SetSelectedState(0);
			this->PressureImposes->SetSelectedState(1);
			this->SelectedImposes = 'P';
			break;
		
		default:
			this->FlowImposes->SetSelectedState(1);
			this->PressureImposes->SetSelectedState(0);
			this->SelectedImposes = 'F';
			break;
	}
}

//----------------------------------------------------------------------------
int vtkPVHMHeartWidget::GetNumberOfPoints()
{
	return this->NumberOfPointsEntry->GetValueAsInt();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::SetNumberOfPoints(int n)
{
	this->NumberOfPointsEntry->SetValueAsInt(n);
}

//----------------------------------------------------------------------------
int vtkPVHMHeartWidget::GetFunctionSelected()
{
	if ( this->FunctionHeartCurve->GetValue() == this->Functions[0] )
		this->FunctionSelected = vtkPVHMHeartWidget::none;
	else if ( strcmp(this->FunctionHeartCurve->GetValue(), this->Functions[1]) == 0 )
		this->FunctionSelected = vtkPVHMHeartWidget::sen1;
	else if ( strcmp(this->FunctionHeartCurve->GetValue(), this->Functions[2]) == 0 )
		this->FunctionSelected = vtkPVHMHeartWidget::sen2;
	else if ( strcmp(this->FunctionHeartCurve->GetValue(), this->Functions[3]) == 0 )
		this->FunctionSelected = vtkPVHMHeartWidget::sen3;
	else
	{
		this->FunctionSelected = 0;
	
	for ( int i=0; i<this->AllFunctions->GetNumberOfValues(); i++ )
		{
		if ( !strcmp(this->FunctionHeartCurve->GetValueFromIndex(i), this->FunctionHeartCurve->GetValue()) )
			{
			this->FunctionSelected = i;
			return this->FunctionSelected;
			}
		}
	}
	return this->FunctionSelected;
}

//----------------------------------------------------------------------------
double vtkPVHMHeartWidget::GetScaleFactor()
{
	return this->ScaleFactorEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::SetScaleFactor(double s)
{
	this->ScaleFactorEntry->SetValueAsDouble(s);
}

//----------------------------------------------------------------------------
int vtkPVHMHeartWidget::GetPointsScaleInit()
{
	return this->PointsScaleInitEntry->GetValueAsInt();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::SetPointsScaleInit(int p)
{
	this->PointsScaleInitEntry->SetValueAsInt(p);
}

//----------------------------------------------------------------------------
int vtkPVHMHeartWidget::GetPointsScaleEnd()
{
	return this->PointsScaleEndEntry->GetValueAsInt();
}

//----------------------------------------------------------------------------
void vtkPVHMHeartWidget::SetPointsScaleEnd(int p)
{
	this->PointsScaleEndEntry->SetValueAsInt(p);
}
