/*
 * $Id: vtkPVHMHeartWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMHeartWidget
// .SECTION Description
// Creates KW Components for the terminals


#ifndef _vtkPVHMHeartWidget_h_
#define _vtkPVHMHeartWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"

class vtkKWLoadSaveDialog;
class vtkKWLoadSaveButton;

class vtkKWComboBox;
class vtkKWRadioButton;
class vtkKWPushButton;          
class vtkKWEntry;
class vtkKWLabel;
class vtkKWFrameWithLabel;
class vtkSMSourceProxy;
class vtkPVHMNodeWidget;
class vtkPVHMStraightModelWidget;

class vtkDoubleArray;
class vtkStringArray;
class vtkDataArrayCollection;

class VTK_EXPORT vtkPVHMHeartWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMHeartWidget *New();
  vtkTypeRevisionMacro(vtkPVHMHeartWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents(vtkPVHMStraightModelWidget* );
	 
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();
	
  // Description:
  // Get Terminal Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetTerminalFrame();
	
  // Description:
  // methods used to update the value of terminal related widgets
  void SetR1(const char *values);
  void SetR2(const char *values);
  void SetCap(const char *values);
  void SetP(const char *values); 
  void SetR1(const double value);
  void SetR2(const double value);
  void SetCap(const double value);
  void SetP(double value);
  void SetTerminalLabel(char *text);
  
  // Description:
  // returns the current values from the termial entries
  double GetR1(void);
  double GetR2(void);
  double GetCap(void);
  double GetP(void);
//  void GetP(vtkDoubleArray *
  
  void SetFinalTime(double value);
  void SetMaxFlow(double value);
  void SetMinFlow(double value);
  double GetFinalTime();
  double GetMaxFlow();
  double GetMinFlow();
  
  char GetImposes();
  void SetImposes(char v);
  
  int GetNumberOfPoints();
  void SetNumberOfPoints(int n);
  
  double GetScaleFactor();
  void SetScaleFactor(double s);
  
  int GetPointsScaleInit();
  void SetPointsScaleInit(int p);
  
  int GetPointsScaleEnd();
  void SetPointsScaleEnd(int p);

  vtkPVHMNodeWidget 		*CurrentNodeWidget;       
  vtkKWPushButton *PlotHeartButton;
  
  // Description:
  // Button for update properties of the heart
  vtkKWPushButton *UpdatePropButton;
  
  // Description:
  // Buttons to select imposes curve of the heart.
  // Default is flow, case flow, divid values for the sum of the resistences
  vtkKWRadioButton *PressureImposes;
  vtkKWRadioButton *FlowImposes;
  
  // Description:
  // Combo box with option of choice of the function of generation
  // automatic of the heart curve
  vtkKWComboBox *FunctionHeartCurve;
  
  // Description:
  // Get function selected
  int GetFunctionSelected();
  
  // Description:
  // Calculate heart curve with function:
  // A = Amplitude
	// T = period
	// x = time instant
	// 
	// f=A*sin(2*(pi/T)*x)			=> sen(x)
	// f=A*(sin((pi/T)*x))^2		=> sen^2 (x) 1/2 period
	// f=A*(sin(2*(pi/T)*x))^2	=> sen^2 (x)
  vtkDoubleArray *CalculateCurve(double Amplitude, double time);
  
  
  // Description:
	// Button for add one curve to the heart of one function in (.txt) file.
	vtkKWLoadSaveButton *AddCurveButton;
  
  void InsertFunction(char *str);
  
  // Description:
  // Read data of the heart curve
  vtkDoubleArray *ReadCurve();
  
  // Description:
  // Button for save heart curve
  vtkKWPushButton *SaveCurveButton;
//  vtkKWLoadSaveButton *SaveCurveButton;
  
  // Description:
  // Write heart curve in (txt) file
  void WriteCurve(vtkDoubleArray *array);
  
protected:
  vtkPVHMHeartWidget();
  virtual ~vtkPVHMHeartWidget();
    
	// Description:
	// Terminal Widget
	// Resistance 1, Resistence2, Capacitance and Pressure Widgets
  vtkKWLabel* R1Label;
  vtkKWEntry* R1Entry;  
  vtkKWLabel* R2Label;
  vtkKWEntry* R2Entry;  
  vtkKWLabel* CapLabel;
  vtkKWEntry* CapEntry;  
  vtkKWLabel* PressureLabel;
  vtkKWEntry* PressureEntry;
  vtkKWLabel* TerminalIcon;  
  vtkKWLabel *BlankLine;
	
	// Description:
	// Final time of the simulation
	vtkKWLabel* FinalTimeLabel;
  vtkKWEntry* FinalTimeEntry;
	
	// Description:
	// Maximum point of the curve of the heart
	vtkKWLabel* MaxFlowLabel;
  vtkKWEntry* MaxFlowEntry;
  
  // Description:
	// Minimum point of the curve of the heart
  vtkKWLabel* MinFlowLabel;
  vtkKWEntry* MinFlowEntry;
  
  // Description:
  // Imposes selected for the user
  char SelectedImposes;
	
	vtkKWLabel* FunctionLabel;
	
	// Description:
	// Functions to generating heart curve:
	// 
	// A = Amplitude
	// T = period
	// x = time instant
	// 
	// f=A*sin(2*(pi/T)*x)			=> sen(x)
	// f=A*(sin((pi/T)*x))^2		=> sen^2 (x) 1/2 period
	// f=A*(sin(2*(pi/T)*x))^2	=> sen^2 (x)
	const char *Functions[4];
	
	// Description:
	// All function of the heart, also the added for the user.
	vtkStringArray *AllFunctions;
	
	// Description:
	// Parameters of the functions added
	vtkDataArrayCollection *FunctionsCollection;
	
	// Description:
	// Parameters of the pressure curve added
	vtkDataArrayCollection *PressureCurveCollection;
	
	// Description:
	// Parameters of the pressure time of the curve added
	vtkDataArrayCollection *TimePressureCurveCollection;
	
	// Description:
	// Label and entry to the number of points of the heart curve
	vtkKWLabel* NumberOfPointsLabel;
  vtkKWEntry* NumberOfPointsEntry;
	
	
	//BTX
	// Description:
	// Function selected
	int FunctionSelected;
	
	// Description:
	// Enumeration of the functions of the generation automatic of the heart curve
	// sen1 = sen(x)
	// sen2	= sen^2 (x) 1/2 period
	// sen3	= sen^2 (x)
	enum FunctionsType{
		none = 0,
		sen1,
		sen2,
		sen3
	};
	//ETX
	
	vtkKWLoadSaveDialog *ChooseDirectoryDialog;
	
  
  // Description:
  // Label and entry of the scale factor for heart curve
  vtkKWLabel *ScaleFactorLabel;
  vtkKWEntry *ScaleFactorEntry;
	
  // Description:
  // Labels and entry for points to calculate scale for heart curve
  vtkKWLabel *PointsScaleInitLabel;
  vtkKWEntry *PointsScaleInitEntry;
  vtkKWLabel *PointsScaleEndLabel;
  vtkKWEntry *PointsScaleEndEntry;
  
private:  
  vtkPVHMHeartWidget(const vtkPVHMHeartWidget&); // Not implemented
  void operator=(const vtkPVHMHeartWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMHeartWidget_h_*/


