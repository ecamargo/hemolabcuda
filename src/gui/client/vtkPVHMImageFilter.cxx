#include "vtkPVHMImageFilter.h"

#include "vtkPVApplication.h"

#include "vtkKWFrameWithLabel.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"

vtkCxxRevisionMacro(vtkPVHMImageFilter, "$Rev$");
vtkStandardNewMacro(vtkPVHMImageFilter);

vtkPVHMImageFilter::vtkPVHMImageFilter()
{
  this->MainFrame = vtkKWFrameWithLabel::New();
}

vtkPVHMImageFilter::~vtkPVHMImageFilter()
{
  this->MainFrame->Delete();
}

void vtkPVHMImageFilter::Create(vtkKWApplication* kwApp)
{
  vtkPVApplication *pvApp = vtkPVApplication::SafeDownCast(kwApp);

  this->Superclass::Create(kwApp);

  this->MainFrame->SetParent(this);
  this->MainFrame->Create(pvApp);
  this->MainFrame->SetLabelText("HM DICOM Image Reader");
/*
  this->PatientNameLabel->SetParent(this->MainFrame->GetFrame());
  this->PatientNameLabel->Create(pvApp);
  this->PatientNameLabel->SetText("No DICOM Volume loaded");
*/

  this->PlaceComponents();
}

void vtkPVHMImageFilter::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",  
    this->MainFrame->GetWidgetName());
/*    
  this->Script("grid %s -sticky ew -columnspan 2",
    this->PatientNameLabel->GetWidgetName());
*/
}

void vtkPVHMImageFilter::Initialize()
{
  //this->SourceProxy = vtkSMHMDICOMImageReaderProxy::SafeDownCast(this->GetPVSource()->GetProxy()); 
}

void vtkPVHMImageFilter::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMImageFilter::Accept()
{

}

void vtkPVHMImageFilter::PostAccept()
{
  this->Superclass::PostAccept();
}
