#ifndef _vtkPVHMImageFilter_h_
#define _vtkPVHMImageFilter_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkKWFrameWithLabel;

class vtkKWEntry;
class vtkKWLabel;

class vtkKWFrameWithLabel;
class vtkKWEntryWithLabel;

class vtkPVApplication;

class VTK_EXPORT vtkPVHMImageFilter : public vtkPVObjectWidget
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMImageFilter *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMImageFilter, vtkPVObjectWidget);

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();

  // Description:
  // Method executed right after Accept().
  virtual void PostAccept();
  
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	
   
  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();
  
protected:

  // Description:
  // Constructor
  vtkPVHMImageFilter();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMImageFilter();
  
  // Description:
  // Main Frame of interface
  vtkKWFrameWithLabel *MainFrame;
  
  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;

  // Description:
  // Method responsible for creating the Interface that interacts with the source.
  void Create(vtkKWApplication *App);


private:  
  vtkPVHMImageFilter(const vtkPVHMImageFilter&); // Not implemented
  void operator=(const vtkPVHMImageFilter&); // Not implemented
}; 

#endif
