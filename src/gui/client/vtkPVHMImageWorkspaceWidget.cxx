#include "vtkPVHMImageWorkspaceWidget.h"

#include "vtkSMHMImageWorkspaceWidgetDataServerProxy.h"
#include "vtkSMHMImageWorkspaceWidgetProxy.h"

#include "vtkPVApplication.h"
#include "vtkPVSourceNotebook.h"
#include "vtkPVSourceCollection.h"
#include "vtkPVWidgetCollection.h"
#include "vtkPVWindow.h"

#include"vtkPVSelectionList.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWPushButton.h"

#include "vtkKWScale.h"
#include "vtkKWLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWCheckButton.h"

#include "vtkPVSource.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"

vtkCxxRevisionMacro(vtkPVHMImageWorkspaceWidget, "$Rev$");
vtkStandardNewMacro(vtkPVHMImageWorkspaceWidget);

vtkPVHMImageWorkspaceWidget::vtkPVHMImageWorkspaceWidget()
{
  this->SetWidgetProxyXMLName("HMImageWorkspaceWidget");
  this->SetWidgetProxyName ("HMImageWorkspaceWidget");

  this->WidgetOutputSelectionList = vtkPVSelectionList::New();

  this->PlanesManipulationFrame = vtkKWFrameWithLabel::New();

  this->XSlider = vtkKWScaleWithEntry::New();
  this->YSlider = vtkKWScaleWithEntry::New();
  this->ZSlider = vtkKWScaleWithEntry::New();

  this->XPlaneEnable = vtkKWCheckButton::New();
  this->YPlaneEnable = vtkKWCheckButton::New();
  this->ZPlaneEnable = vtkKWCheckButton::New();

  this->WindowSlider = vtkKWScaleWithEntry::New();
  this->ColorSlider = vtkKWScaleWithEntry::New();

  this->LastClickedVoxelsFrame = vtkKWFrameWithLabel::New();

  this->LastClickedVoxelLabel = vtkKWLabel::New();
  this->XOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->YOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->ZOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->IntensityOfLastClickedVoxelEntry = vtkKWEntry::New();

  this->SeedsManipulationFrame = vtkKWFrameWithLabel::New();

  this->SeedsVisibilityButton = vtkKWCheckButton::New();
  this->SeedsAutoResizeButton = vtkKWCheckButton::New();
  
  this->IntersectionLinesVisibilityButton = vtkKWCheckButton::New();

  this->PVWindow = NULL;
  
  for (int i = 0; i < 6; i++)
  {
    this->Extent[i] = 0;
    this->OldExtent[i] = 0;
  }
  
  for (int i = 0; i < 3; i++)
  {
  	this->Dimensions[i] = 0;
  	this->Spacing[i] = 0;
  }
  for (int i=0; i<4; i++)
  {
  	this->  LastClickedVoxel[i] = 0;
  }
  
  for (int i = 0;i < 2; i++)
  {
    this->InputWindowAndColorLevels[i] = 0;
    this->OutputWindowAndColorLevels[i] = 0;
  }
  
  this->WidgetVisibility = 1;
  
  this->AlreadyAccepted = 0;
  this->FirstExecute = 1;
  this->WidgetsEventsStarted = 0;
  this->ExtentChanger = 0;
}

vtkPVHMImageWorkspaceWidget::~vtkPVHMImageWorkspaceWidget()
{
  this->ImageWorkspaceWidgetProxy = NULL;
  
  this->WidgetOutputSelectionList->Delete();
  
  this->PlanesManipulationFrame->Delete();
  
  this->XSlider->Delete();
  this->YSlider->Delete();
  this->ZSlider->Delete();

  this->XPlaneEnable->Delete();
  this->YPlaneEnable->Delete();
  this->ZPlaneEnable->Delete();

  this->WindowSlider->Delete();
  this->ColorSlider->Delete();
  
  this->LastClickedVoxelsFrame->Delete();
  this->LastClickedVoxelLabel->Delete();
  this->XOfLastClickedVoxelEntry->Delete();
  this->YOfLastClickedVoxelEntry->Delete();
  this->ZOfLastClickedVoxelEntry->Delete();
  this->IntensityOfLastClickedVoxelEntry->Delete();
  
  this->SeedsManipulationFrame->Delete();
  this->SeedsVisibilityButton->Delete();
  this->SeedsAutoResizeButton->Delete();
  
  this->IntersectionLinesVisibilityButton->Delete();

  this->PVWindow = NULL;
}

void vtkPVHMImageWorkspaceWidget::SetXPlaneCenter()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneCenter("SetXPlaneCenter",(int) this->XSlider->GetValue());
}
  
void vtkPVHMImageWorkspaceWidget::SetYPlaneCenter()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneCenter("SetYPlaneCenter",(int) this->YSlider->GetValue());
}

void vtkPVHMImageWorkspaceWidget::SetZPlaneCenter()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneCenter("SetZPlaneCenter",(int) this->ZSlider->GetValue());
}

void vtkPVHMImageWorkspaceWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->PVWindow = pvApp->GetMainWindow();
  this->SetFrameLabel("HM ImageWorkspace");

  this->WidgetOutputSelectionList->SetParent(this->Frame);
  this->WidgetOutputSelectionList->SetOptionWidth(10);
  this->WidgetOutputSelectionList->Create(pvApp);
  this->WidgetOutputSelectionList->SetLabel("Widget Input");
  this->WidgetOutputSelectionList->AddItem("Input", 0);
  this->WidgetOutputSelectionList->AddItem("Output", 1);
  this->WidgetOutputSelectionList->SetModifiedCommand(this->GetTclName(), "SelectWidgetInputCallback");
  this->WidgetOutputSelectionList->SetCurrentValue(0);
  
  this->PlanesManipulationFrame->SetParent(this->Frame);
  this->PlanesManipulationFrame->Create(pvApp);
  this->PlanesManipulationFrame->SetLabelText("Planes Manipulation");
  
  this->LastClickedVoxelsFrame->SetParent(this->Frame);
  this->LastClickedVoxelsFrame->Create(pvApp);
  this->LastClickedVoxelsFrame->SetBalloonHelpString("CTRL or SHIFT click the planes use the picker and on the release update this values");
  this->LastClickedVoxelsFrame->SetLabelText("Voxels Manipulation");


//Plane Visibility Buttons -----------------------------------------------------|
  this->XPlaneEnable->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->XPlaneEnable->Create(pvApp);
  this->XPlaneEnable->SetText("On");
  this->XPlaneEnable->SetSelectedState(1);
  this->XPlaneEnable->SetCommand(this,"EnableXPlane");


  this->YPlaneEnable->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->YPlaneEnable->Create(pvApp);
  this->YPlaneEnable->SetText("On");
  this->YPlaneEnable->SetSelectedState(1);
  this->YPlaneEnable->SetCommand(this,"EnableYPlane");
  
  
  this->ZPlaneEnable->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->ZPlaneEnable->Create(pvApp);
  this->ZPlaneEnable->SetText("On");
  this->ZPlaneEnable->SetSelectedState(1);
  this->ZPlaneEnable->SetCommand(this,"EnableZPlane");
  

//Plane Sliders ---------------------------------------------------------------|
  this->XSlider->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->XSlider->Create(pvApp);
  this->XSlider->SetResolution(1.0);
  this->XSlider->RangeVisibilityOff();
  this->XSlider->SetLabelText("X Plane");
  this->XSlider->SetCommand(this,"SetXPlaneCenter");
  
  this->XSlider->GetScale()->SetLength(220);
  this->XSlider->GetEntry()->SetWidth(4);
  

  this->YSlider->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->YSlider->Create(pvApp);
  this->YSlider->SetResolution(1.0);
  this->YSlider->RangeVisibilityOff();
  this->YSlider->SetLabelText("Y Plane");
  this->YSlider->SetCommand(this,"SetYPlaneCenter");

  this->YSlider->GetScale()->SetLength(220);
  this->YSlider->GetEntry()->SetWidth(4);

  this->ZSlider->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->ZSlider->Create(pvApp);
  this->ZSlider->SetResolution(1.0);
  this->ZSlider->RangeVisibilityOff();
  this->ZSlider->SetLabelText("Z Plane");
  this->ZSlider->SetCommand(this,"SetZPlaneCenter");

  this->ZSlider->GetScale()->SetLength(220);
  this->ZSlider->GetEntry()->SetWidth(4);


//Window and Color Sliders ----------------------------------------------------|
  this->WindowSlider->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->WindowSlider->PopupModeOn();
  this->WindowSlider->Create(pvApp);
  this->WindowSlider->SetOrientationToVertical();
  this->WindowSlider->SetResolution(1.0);
  this->WindowSlider->RangeVisibilityOn();
  this->WindowSlider->SetLabelText("Window Level");
  this->WindowSlider->SetCommand(this, "SetWindowAndColorLevels");
  
  this->ColorSlider->SetParent(this->PlanesManipulationFrame->GetFrame());
  this->ColorSlider->PopupModeOn();
  this->ColorSlider->Create(pvApp);
  this->ColorSlider->SetOrientationToVertical();
  this->ColorSlider->SetResolution(1.0);
  this->ColorSlider->RangeVisibilityOn();
  this->ColorSlider->SetLabelText("Color Level");
  this->ColorSlider->SetCommand(this, "SetWindowAndColorLevels");

//Voxels Manipulation ---------------------------------------------------------|
  this->LastClickedVoxelLabel->SetParent(this->LastClickedVoxelsFrame->GetFrame());
  this->LastClickedVoxelLabel->Create(pvApp);
  this->LastClickedVoxelLabel->SetText("Last Clicked Voxel");

  this->XOfLastClickedVoxelEntry->SetParent(this->LastClickedVoxelsFrame->GetFrame());
  this->XOfLastClickedVoxelEntry->Create(pvApp);
  this->XOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->XOfLastClickedVoxelEntry->ReadOnlyOn();
  this->XOfLastClickedVoxelEntry->SetWidth(8);

  this->YOfLastClickedVoxelEntry->SetParent(this->LastClickedVoxelsFrame->GetFrame());
  this->YOfLastClickedVoxelEntry->Create(pvApp);
  this->YOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->YOfLastClickedVoxelEntry->ReadOnlyOn();
  this->YOfLastClickedVoxelEntry->SetWidth(8);

  this->ZOfLastClickedVoxelEntry->SetParent(this->LastClickedVoxelsFrame->GetFrame());
  this->ZOfLastClickedVoxelEntry->Create(pvApp);
  this->ZOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->ZOfLastClickedVoxelEntry->ReadOnlyOn();
  this->ZOfLastClickedVoxelEntry->SetWidth(8);

  this->IntensityOfLastClickedVoxelEntry->SetParent(this->LastClickedVoxelsFrame->GetFrame());
  this->IntensityOfLastClickedVoxelEntry->Create(pvApp);
  this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->IntensityOfLastClickedVoxelEntry->ReadOnlyOn();
  this->IntensityOfLastClickedVoxelEntry->SetWidth(8);

  this->SeedsManipulationFrame->SetParent(this->Frame);
  this->SeedsManipulationFrame->Create(pvApp);
  this->SeedsManipulationFrame->SetLabelText("Seeds Manipulation");

  this->SeedsVisibilityButton->SetParent(this->SeedsManipulationFrame->GetFrame());
  this->SeedsVisibilityButton->Create(pvApp);
  this->SeedsVisibilityButton->SetText("Seeds Visibility");
  this->SeedsVisibilityButton->SetSelectedState(1);
  this->SeedsVisibilityButton->SetCommand(this,"SetSeedsVisibility");

  this->SeedsAutoResizeButton->SetParent(this->SeedsManipulationFrame->GetFrame());
  this->SeedsAutoResizeButton->Create(pvApp);
  this->SeedsAutoResizeButton->SetText("Auto Resize");
  this->SeedsAutoResizeButton->SetSelectedState(1);
  this->SeedsAutoResizeButton->SetCommand(this,"SetSeedsAutoResize");

  this->IntersectionLinesVisibilityButton->SetParent(this->Frame);
  this->IntersectionLinesVisibilityButton->Create(pvApp);
  this->IntersectionLinesVisibilityButton->SetText("Planes Intersection Lines Visibility");
  this->IntersectionLinesVisibilityButton->SetSelectedState(1);
  this->IntersectionLinesVisibilityButton->SetCommand(this,"SetIntersectionLinesVisibility");

  //Only enables the Widgets input selection combo box if the source is a filter (disabled for readers, sources or any other type of pipeline elements).
  if(!strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters"))
  {
    this->EnablePanel(true);
    this->WidgetOutputSelectionList->SetEnabled(0);
  }
  else
  {
    this->EnablePanel(false);
  }

  this->PlaceComponents();
}

void vtkPVHMImageWorkspaceWidget::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true", this->WidgetOutputSelectionList->GetWidgetName());

  this->Script("pack %s -fill x -expand true", this->PlanesManipulationFrame->GetWidgetName());

  this->Script("pack %s -fill x -expand true", this->LastClickedVoxelsFrame->GetWidgetName());

  this->Script("pack %s -fill x -expand true", this->SeedsManipulationFrame->GetWidgetName());

  this->Script("grid %s %s - -sticky n -columnspan 2",
    this->XSlider->GetWidgetName(),
    this->XPlaneEnable->GetWidgetName());

  this->Script("grid %s %s - -sticky n -columnspan 2",
    this->YSlider->GetWidgetName(),
    this->YPlaneEnable->GetWidgetName());

  this->Script("grid %s %s - -sticky n -columnspan 2",
    this->ZSlider->GetWidgetName(),
    this->ZPlaneEnable->GetWidgetName());

  this->Script("grid %s %s - -sticky n -columnspan 1",
    this->WindowSlider->GetWidgetName(),
    this->ColorSlider->GetWidgetName());

  this->Script("grid %s %s %s %s %s - - - - -sticky n",
    this->LastClickedVoxelLabel->GetWidgetName(),
    this->XOfLastClickedVoxelEntry->GetWidgetName(),
    this->YOfLastClickedVoxelEntry->GetWidgetName(),
    this->ZOfLastClickedVoxelEntry->GetWidgetName(),
    this->IntensityOfLastClickedVoxelEntry->GetWidgetName());
    
  this->Script("grid %s %s - -sticky n -columnspan 2",
    this->SeedsAutoResizeButton->GetWidgetName(),
    this->SeedsVisibilityButton->GetWidgetName());

  this->Script("pack %s -fill x -expand true", this->IntersectionLinesVisibilityButton->GetWidgetName());
}

void vtkPVHMImageWorkspaceWidget::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMImageWorkspaceWidget::Initialize()
{
  this->ImageWorkspaceWidgetProxy = vtkSMHMImageWorkspaceWidgetProxy::SafeDownCast(this->WidgetProxy);
 
  vtkSMIntVectorProperty* WidgetsInputVisibilityProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetProperty("WidgetsInputVisibility"));
  
  if (WidgetsInputVisibilityProperty->GetNumberOfElements() > 0)
  {
    this->EnablePanel(WidgetsInputVisibilityProperty->GetElement(0));
  }
  //Made like this because the property may not exist.
  else
  {
    this->EnablePanel(false);
  }
  
  this->WidgetOutputSelectionList->SetEnabled(0);
  
  this->Superclass::Initialize();
  this->SetWindowAndColorLevelsFromPreviousPipelineWidget();
}

void vtkPVHMImageWorkspaceWidget::SelectWidgetInputCallback()
{
  //Temporarilly disabling commands. The callback will be invoked manually after the else statement.
  this->WindowSlider->SetDisableCommands(1);
  this->ColorSlider->SetDisableCommands(1);
  
  if(!this->WidgetOutputSelectionList->GetCurrentValue())
  {
    this->ImageWorkspaceWidgetProxy->SetWindowAndColorLevels(this->InputWindowAndColorLevels[0], this->InputWindowAndColorLevels[1]);
    this->WindowSlider->SetValue(this->InputWindowAndColorLevels[0]);
    this->ColorSlider->SetValue(this->InputWindowAndColorLevels[1]);
  }
  else
  {
    this->ImageWorkspaceWidgetProxy->SetWindowAndColorLevels(this->OutputWindowAndColorLevels[0], this->OutputWindowAndColorLevels[1]);
    this->WindowSlider->SetValue(this->OutputWindowAndColorLevels[0]);
    this->ColorSlider->SetValue(this->OutputWindowAndColorLevels[1]);
  }
  this->WindowSlider->SetDisableCommands(0);
  this->ColorSlider->SetDisableCommands(0);
  
  this->SetWindowAndColorLevels();
  
  vtkSMIntVectorProperty* WidgetsInputVisibilityProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetProperty("WidgetsInputVisibility"));

  this->ImageWorkspaceWidgetProxy->GetImageWorkspaceWidgetDataServerProxy()->SetWidgetInputSource(this->WidgetOutputSelectionList->GetCurrentValue());
  
  if (!this->WidgetOutputSelectionList->GetCurrentValue())
  {
    if (WidgetsInputVisibilityProperty->GetElement(0))
    {
      this->SetInputFromSourcesInput();
    }
  }

  else
  {
    this->SetInputFromSourcesOutput();
    
    if((this->AlreadyAccepted) && ((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5])))  
    {
      //Should center the planes only if the output Extent was changed.
      this->XSlider->SetValue(this->Extent[0] + ((this->Extent[1] - this->Extent[0]) / 2));
      this->YSlider->SetValue(this->Extent[2] + ((this->Extent[3] - this->Extent[2]) / 2));
      this->ZSlider->SetValue(this->Extent[4] + ((this->Extent[5] - this->Extent[4]) / 2));
    }
  }

  //Updating OldExtent array.
  for (int i = 0; i < 6; i++)
  {
  	this->OldExtent[i] = this->Extent[i];
  }
}

void vtkPVHMImageWorkspaceWidget::SetInputFromSourcesInput()
{
  //Enables the render-server widget, adding observers and actors.
  this->ImageWorkspaceWidgetProxy->SetEnabled(1);

  //Set render-server widget input from the data-server widget object output.
  this->SetInput(this->GetOutputServerObject());

  //Set the render-server widgets input from the data-server widgets object GetImageDataInput(0) reference (input of the "filter").
  this->ImageWorkspaceWidgetProxy->SetInputFromImageDataInput(this->ImageWorkspaceWidgetProxy->GetImageWorkspaceWidgetDataServerProxy()->GetImageWorkspaceID());
    
  //Updates this classes arrays from server-side information.
  this->SetExtentFromServer();  
  this->SetDimensionsFromServer();
  this->SetSpacingFromServer();  

  int Flag = true;
    
  //If extent has been changed, center the planes and updates the sliders limits.
  if(((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5])))  
  {
    this->EnableSlider(this->XSlider, this->Extent[0], this->Extent[1], Flag);
    this->EnableSlider(this->YSlider, this->Extent[2], this->Extent[3], Flag);
    this->EnableSlider(this->ZSlider, this->Extent[4], this->Extent[5], Flag);
  	
    this->XSlider->SetValue(this->Extent[0] + ((this->Extent[1] - this->Extent[0]) / 2));
    this->YSlider->SetValue(this->Extent[2] + ((this->Extent[3] - this->Extent[2]) / 2));
    this->ZSlider->SetValue(this->Extent[4] + ((this->Extent[5] - this->Extent[4]) / 2));
  }
  //Else, just force the force the planes position to current.
  else
  {
    this->XSlider->SetValue(this->XSlider->GetValue());
    this->YSlider->SetValue(this->YSlider->GetValue());
    this->ZSlider->SetValue(this->ZSlider->GetValue());

  	this->SetXPlaneCenter();
  	this->SetYPlaneCenter();
  	this->SetZPlaneCenter();
  }

  if(!this->AlreadyAccepted)
  {
//    this->SetWindowAndColorLevelsFromServer();
  }
  this->SetWindowAndColorLevels();
}

void vtkPVHMImageWorkspaceWidget::SetInputFromSourcesOutput()
{
  int modFlag = this->GetModifiedFlag();
  this->WidgetOutputSelectionList->SetCurrentValue(1);

  this->ImageWorkspaceWidgetProxy->SetEnabled(1);

  this->SetWidgetInput();

  int Flag = true;

  if((this->AlreadyAccepted) && ((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5])))  
  {
    this->EnableSlider(this->XSlider, this->Extent[0], this->Extent[1], Flag);
    this->EnableSlider(this->YSlider, this->Extent[2], this->Extent[3], Flag);
    this->EnableSlider(this->ZSlider, this->Extent[4], this->Extent[5], Flag);
  	
    this->XSlider->SetValue(this->Extent[0] + ((this->Extent[1] - this->Extent[0]) / 2));
    this->YSlider->SetValue(this->Extent[2] + ((this->Extent[3] - this->Extent[2]) / 2));
    this->ZSlider->SetValue(this->Extent[4] + ((this->Extent[5] - this->Extent[4]) / 2));

    this->EnableSlider(this->ColorSlider, -2048, 3642, Flag);
    this->EnableSlider(this->WindowSlider, 0, 20000, Flag);

    this->SetWindowAndColorLevelsFromServer();

  }
  else
  {
    this->XSlider->SetValue(this->XSlider->GetValue());
    this->YSlider->SetValue(this->YSlider->GetValue());
    this->ZSlider->SetValue(this->ZSlider->GetValue());
        
  	this->SetXPlaneCenter();
  	this->SetYPlaneCenter();
  	this->SetZPlaneCenter();
  }
  
  this->EnableComponents(Flag);

  this->SetWindowAndColorLevels();

  if (!this->WidgetsEventsStarted)
  {
  	this->ImageWorkspaceWidgetProxy->ManipulateObservers(1);
  	this->WidgetsEventsStarted = 1;
  }
  this->AlreadyAccepted = 1;
}

void vtkPVHMImageWorkspaceWidget::SetWidgetInput()
{

//PIPELINE HERE!
  this->SetInput(this->GetOutputServerObject());
//-------------|

//Seta a Entrada do Widget com o ID do ImageWorkspace do DataServer --> Rever com MPI
  this->ImageWorkspaceWidgetProxy->SetInput(this->ImageWorkspaceWidgetProxy->GetImageWorkspaceWidgetDataServerProxy()->GetImageWorkspaceID());

  this->SetExtentFromServer();
  this->SetDimensionsFromServer();
  this->SetSpacingFromServer();
}

void vtkPVHMImageWorkspaceWidget::Accept()
{
  if (!this->AlreadyAccepted)
  {
    //Internally setting WidgetInput.
    this->SetInputFromSourcesOutput();
    
    if(((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5])) && ((this->OldExtent[1] != 0) || (this->OldExtent[3] != 0) || (this->OldExtent[5] != 0)) && ((this->Extent[1] != 0) || (this->Extent[3] != 0) || (this->Extent[5] != 0)) && (this->AlreadyAccepted == 1))
    {
      this->ExtentChanger = 1;
    }
  
    this->SelectWidgetInputCallback();

    vtkSMIntVectorProperty* WidgetsInputVisibilityProperty = vtkSMIntVectorProperty::SafeDownCast(
        this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetProperty("WidgetsInputVisibility"));

    if(strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters") || WidgetsInputVisibilityProperty->GetNumberOfElements() <= 0 || this->ExtentChanger == 1)
    {
      this->WidgetOutputSelectionList->SetEnabled(0);
    }
  }
  
  this->SetExtentFromServer();
  
//  this->AlreadyAccepted = 1;
  
  this->Superclass::Accept();
  
  //All the Accecpts have been made, but the InvokeEvent on the ExecuteEvent method repaints it with green.
  this->GetPVSource()->GetNotebook()->SetAcceptButtonColorToUnmodified();
}

void vtkPVHMImageWorkspaceWidget::PostAccept()
{
  //Internally setting WidgetInput.
  this->SetInputFromSourcesOutput();

  this->SetExtentFromServer();

  if(((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5])) && ((this->OldExtent[1] != 0) || (this->OldExtent[3] != 0) || (this->OldExtent[5] != 0)) && ((this->Extent[1] != 0) || (this->Extent[3] != 0) || (this->Extent[5] != 0)) && (this->AlreadyAccepted == 1))
  {
  	this->ExtentChanger = 1;
  }
  
  this->SelectWidgetInputCallback();
  
  vtkSMIntVectorProperty* WidgetsInputVisibilityProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetProperty("WidgetsInputVisibility"));

  if(strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters") || WidgetsInputVisibilityProperty->GetNumberOfElements() <= 0 || this->ExtentChanger == 1)
  {
    this->WidgetOutputSelectionList->SetEnabled(0);
  }

  if(!this->ExtentChanger)
  {
    this->SetWindowAndColorLevelsFromServer();
  }
  else
  {
    this->WindowSlider->SetDisableCommands(1);
    this->ColorSlider->SetDisableCommands(1);
    
    this->OutputWindowAndColorLevels[0] = this->InputWindowAndColorLevels[0];
    this->OutputWindowAndColorLevels[1] = this->InputWindowAndColorLevels[1];
    
    this->WindowSlider->SetValue(this->OutputWindowAndColorLevels[0]);
    this->ColorSlider->SetValue(this->OutputWindowAndColorLevels[1]);

    this->WindowSlider->SetDisableCommands(0);
    this->ColorSlider->SetDisableCommands(0);
  }
  
  this->SetWindowAndColorLevels();
  this->GetPVSource()->GetNotebook()->SetAcceptButtonColorToUnmodified();
  
  this->AlreadyAccepted = 1;
  this->FirstExecute = 0;
}

void vtkPVHMImageWorkspaceWidget::Delete()
{
  if (this->AlreadyAccepted)
  {
  	this->SetVisibility(0);
  }
  
  this->Superclass::Delete();
}

void vtkPVHMImageWorkspaceWidget::Select()
{
  vtkSMIntVectorProperty* WidgetsInputVisibilityProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetProperty("WidgetsInputVisibility"));

  int WidgetsInputVisibilityFlag;
  
  if (this->AlreadyAccepted)
  {
    this->SetWidgetInput();
    this->SetExtentFromServer();

    this->SelectWidgetInputCallback();
  }
  
  if (WidgetsInputVisibilityProperty->GetNumberOfElements() > 0)
  {
    WidgetsInputVisibilityFlag = 1;
  }
  else
  {
    WidgetsInputVisibilityFlag = 0;
  }

  if (WidgetsInputVisibilityFlag)
  {
    if(WidgetsInputVisibilityProperty->GetElement(0) && !this->AlreadyAccepted)
    {
      this->SetInputFromSourcesInput();
    }

    if ((!this->WidgetsEventsStarted && WidgetsInputVisibilityProperty->GetElement(0)) || (this->AlreadyAccepted))
    {
  	  this->ImageWorkspaceWidgetProxy->ManipulateObservers(1);
  	  this->WidgetsEventsStarted = 1;
    }
  }

  if (this->AlreadyAccepted)
  {
    this->SetXPlaneCenter();
    this->SetYPlaneCenter();
    this->SetZPlaneCenter();

    this->SetVisibility(this->Visibility->GetSelectedState());
    this->EnableXPlane();
    this->EnableYPlane();
    this->EnableZPlane();
    
    this->SetSeedsVisibility();
  }

  if (WidgetsInputVisibilityFlag && this->Visibility)
  {
  	if (WidgetsInputVisibilityProperty->GetElement(0))
    {
      this->SetVisibility(this->Visibility->GetSelectedState());
    }
  }
 
  if(strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters") || WidgetsInputVisibilityProperty->GetNumberOfElements() <= 0 || this->ExtentChanger)
  {
    this->WidgetOutputSelectionList->SetEnabled(0);
  }
  
  this->Superclass::Select();
}

void vtkPVHMImageWorkspaceWidget::Deselect()
{
  if (this->AlreadyAccepted)
  {
  	this->SetVisibility(0);
  	this->ImageWorkspaceWidgetProxy->SetSeedsVisibility(0);
  }
  
  if (this->WidgetsEventsStarted)
  {
 	this->ImageWorkspaceWidgetProxy->ManipulateObservers(0);
 	this->WidgetsEventsStarted = 0;
  }
  
  for (int i = 0; i < 6; i++)
  {
    this->OldExtent[i] = this->Extent[i];
  }
  
  this->Superclass::Deselect();
}

void vtkPVHMImageWorkspaceWidget::SetWidgetProxy(vtkSM3DWidgetProxy *wExternal)
{
  this->ImageWorkspaceWidgetProxy = vtkSMHMImageWorkspaceWidgetProxy::SafeDownCast(wExternal);
}

vtkSMHMImageWorkspaceWidgetProxy *vtkPVHMImageWorkspaceWidget::GetImageWorkspaceWidgetProxy()
{
  return this->ImageWorkspaceWidgetProxy;
}

void vtkPVHMImageWorkspaceWidget::SetInput(int InputServerObjectID)
{
  this->ImageWorkspaceWidgetProxy->GetImageWorkspaceWidgetDataServerProxy()->SetInput(InputServerObjectID);
}

int vtkPVHMImageWorkspaceWidget::GetOutputServerObject()
{
  return this->GetPVSource()->GetProxy()->GetID(0).ID;
}

int *vtkPVHMImageWorkspaceWidget::GetDimensions()
{
  return this->Dimensions;
}  
  
double *vtkPVHMImageWorkspaceWidget::GetSpacing()
{
  return this->Spacing;
}
  
void vtkPVHMImageWorkspaceWidget::SetDimensions(int *PassedDimensions)
{
  for (int i=0; i < 3; i++)
  {
    this->Dimensions[i] = PassedDimensions[i];
  }
}  

void vtkPVHMImageWorkspaceWidget::SetSpacing (double *PassedSpacing)
{
  for (int i=0; i < 3; i++)
  {
    this->Spacing[i] = PassedSpacing[i];
  }
}

void vtkPVHMImageWorkspaceWidget::SetExtentFromServer()
{
  vtkSMIntVectorProperty* Extent0Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent0"));

  vtkSMIntVectorProperty* Extent1Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent1"));

  vtkSMIntVectorProperty* Extent2Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent2"));

  vtkSMIntVectorProperty* Extent3Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent3"));

  vtkSMIntVectorProperty* Extent4Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent4"));

  vtkSMIntVectorProperty* Extent5Property = vtkSMIntVectorProperty::SafeDownCast(
      this->GetWidgetProxy()->GetProperty("Extent5"));

  this->GetWidgetProxy()->UpdatePropertyInformation(Extent0Property);
  this->GetWidgetProxy()->UpdatePropertyInformation(Extent1Property);
  this->GetWidgetProxy()->UpdatePropertyInformation(Extent2Property);
  this->GetWidgetProxy()->UpdatePropertyInformation(Extent3Property);
  this->GetWidgetProxy()->UpdatePropertyInformation(Extent4Property);
  this->GetWidgetProxy()->UpdatePropertyInformation(Extent5Property);

  this->Extent[0] = Extent0Property->GetElement(0);
  this->Extent[1] = Extent1Property->GetElement(0);
  this->Extent[2] = Extent2Property->GetElement(0);
  this->Extent[3] = Extent3Property->GetElement(0);
  this->Extent[4] = Extent4Property->GetElement(0);
  this->Extent[5] = Extent5Property->GetElement(0);
}

void vtkPVHMImageWorkspaceWidget::SetDimensionsFromServer()
{
  vtkSMIntVectorProperty* XDimensionProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("XDimension"));

  vtkSMIntVectorProperty* YDimensionProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("YDimension"));

  vtkSMIntVectorProperty* ZDimensionProperty = vtkSMIntVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("ZDimension"));


  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(XDimensionProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(YDimensionProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(ZDimensionProperty);


  this->Dimensions[0] = XDimensionProperty->GetElement(0);
  this->Dimensions[1] = YDimensionProperty->GetElement(0);
  this->Dimensions[2] = ZDimensionProperty->GetElement(0);
}

void vtkPVHMImageWorkspaceWidget::SetSpacingFromServer()
{
  vtkSMDoubleVectorProperty* XSpacingProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("XSpacing"));

  vtkSMDoubleVectorProperty* YSpacingProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("YSpacing"));

  vtkSMDoubleVectorProperty* ZSpacingProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("ZSpacing"));


  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(XSpacingProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(YSpacingProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(ZSpacingProperty);


  this->Spacing[0] = XSpacingProperty->GetElement(0);
  this->Spacing[1] = YSpacingProperty->GetElement(0);
  this->Spacing[2] = ZSpacingProperty->GetElement(0);
}

void vtkPVHMImageWorkspaceWidget::SetWindowAndColorLevelsFromServer()
{
  int NumberOfPipelinePVSources = this->GetPVApplication()->GetMainWindow()->GetSourceList("Sources")->GetNumberOfItems();
  
  vtkSMDoubleVectorProperty* MinDataRangeProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("MinDataRange"));

  vtkSMDoubleVectorProperty* MaxDataRangeProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("MaxDataRange"));

  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(MinDataRangeProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(MaxDataRangeProperty);
  
  int IntensitiesRange = int(MaxDataRangeProperty->GetElement(0) - MinDataRangeProperty->GetElement(0));

  //Only retrieve from server if the it hasn't been accepted, if there is more than one item in the pipeline and this is na filter module.
  if ((!this->AlreadyAccepted && NumberOfPipelinePVSources > 1) || this->ExtentChanger || ((IntensitiesRange > 1500) && (NumberOfPipelinePVSources > 1) && this->FirstExecute) && !strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters"))
  {
    this->SetWindowAndColorLevelsFromPreviousPipelineWidget();
  }
  else
  {
    if(this->FirstExecute || (IntensitiesRange < 1500))
    {
      vtkSMDoubleVectorProperty* WindowLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
          this->ImageWorkspaceWidgetProxy->GetProperty("WindowLevel"));
    
      vtkSMDoubleVectorProperty* ColorLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
          this->ImageWorkspaceWidgetProxy->GetProperty("ColorLevel"));
    
      this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(WindowLevelProperty);
      this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(ColorLevelProperty);
    
      this->WindowSlider->SetDisableCommands(1);
      this->ColorSlider->SetDisableCommands(1);
    
      this->WindowSlider->SetValue(WindowLevelProperty->GetElement(0));
      this->ColorSlider->SetValue(ColorLevelProperty->GetElement(0));
    
      this->WindowSlider->SetDisableCommands(0);
      this->ColorSlider->SetDisableCommands(0);
      
      if(!this->WidgetOutputSelectionList->GetCurrentValue() || this->ExtentChanger)
      {
        this->InputWindowAndColorLevels[0] = WindowLevelProperty->GetElement(0);
        this->InputWindowAndColorLevels[1] = ColorLevelProperty->GetElement(0);
      }
      else
      {
        this->OutputWindowAndColorLevels[0] = WindowLevelProperty->GetElement(0);
        this->OutputWindowAndColorLevels[1] = ColorLevelProperty->GetElement(0);
      }
    }
  }
}

void vtkPVHMImageWorkspaceWidget::EnableSlider(vtkKWScaleWithEntry *PassedSlider, int MinValue, int MaxValue, bool Flag)
{
  if (Flag)
  {
    PassedSlider->SetRange(MinValue , MaxValue);
    PassedSlider->SetValue(0);
    PassedSlider->DisableCommandsOff();
    PassedSlider->EnabledOn();
  }
  else
  {
    PassedSlider->SetRange(0.0 , 0.0);
    PassedSlider->SetValue(0);
    PassedSlider->DisableCommandsOn();
    PassedSlider->EnabledOff();
  }

    PassedSlider->UpdateEnableState();
    PassedSlider->Modified();
}

void vtkPVHMImageWorkspaceWidget::EnableSliders(bool Flag)
{
  this->EnableSlider(this->XSlider, this->Extent[0], this->Extent[1], Flag);
  this->EnableSlider(this->YSlider, this->Extent[2], this->Extent[3], Flag);
  this->EnableSlider(this->ZSlider, this->Extent[4], this->Extent[5], Flag);

  if((!this->AlreadyAccepted) || ((this->AlreadyAccepted) && ((this->OldExtent[0] != this->Extent[0]) || (this->OldExtent[1] != this->Extent[1]) || (this->OldExtent[2] != this->Extent[2]) || (this->OldExtent[3] != this->Extent[3]) || (this->OldExtent[4] != this->Extent[4]) || (this->OldExtent[5] != this->Extent[5]))))
  {  
    this->XSlider->SetValue(this->Extent[0] + ((this->Extent[1] - this->Extent[0]) / 2));
    this->YSlider->SetValue(this->Extent[2] + ((this->Extent[3] - this->Extent[2]) / 2));
    this->ZSlider->SetValue(this->Extent[4] + ((this->Extent[5] - this->Extent[4]) / 2));
  }
  
  this->EnableSlider(this->ColorSlider, -2048, 3642, Flag);
  this->EnableSlider(this->WindowSlider, 0, 20000, Flag);
}

void vtkPVHMImageWorkspaceWidget::EnableComponent(vtkKWWidget *PassedComponent, bool Flag)
{
  if (Flag)
  {
    PassedComponent->EnabledOn();
  }
  else
  {
    PassedComponent->EnabledOff();
  }

    PassedComponent->UpdateEnableState();
    PassedComponent->Modified();
}

void vtkPVHMImageWorkspaceWidget::EnableComponents(bool Flag)
{
  this->EnableComponent(this->WidgetOutputSelectionList, Flag);
  
  this->EnableComponent(this->XPlaneEnable, Flag);
  this->EnableComponent(this->YPlaneEnable, Flag);
  this->EnableComponent(this->ZPlaneEnable, Flag);
  
  this->EnableComponent(this->WindowSlider->GetPopupPushButton(), Flag);
  this->EnableComponent(this->ColorSlider->GetPopupPushButton(), Flag);
  
  this->EnableComponent(this->Visibility, Flag);
  
  this->EnableComponent(this->SeedsVisibilityButton, Flag);
  this->EnableComponent(this->SeedsAutoResizeButton, Flag);
  this->EnableComponent(this->IntersectionLinesVisibilityButton, Flag);
}

void vtkPVHMImageWorkspaceWidget::EnablePanel(bool Flag)
{
  this->EnableSliders(Flag);
  
  this->EnableComponents(Flag);
}

void vtkPVHMImageWorkspaceWidget::EnableXPlane()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneEnable("EnableXPlane", this->XPlaneEnable->GetSelectedState());
}

void vtkPVHMImageWorkspaceWidget::EnableYPlane()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneEnable("EnableYPlane", this->YPlaneEnable->GetSelectedState());
}

void vtkPVHMImageWorkspaceWidget::EnableZPlane()
{
  this->ImageWorkspaceWidgetProxy->SetPlaneEnable("EnableZPlane", this->ZPlaneEnable->GetSelectedState());
}

void vtkPVHMImageWorkspaceWidget::SetWindowAndColorLevels()
{
  this->WindowSlider->SetDisableCommands(1);
  this->ColorSlider->SetDisableCommands(1);
    
  vtkSMDoubleVectorProperty* WindowLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("WindowLevel"));

  vtkSMDoubleVectorProperty* ColorLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("ColorLevel"));
      
  if(!this->WidgetOutputSelectionList->GetCurrentValue())
  {
    this->InputWindowAndColorLevels[0] = this->WindowSlider->GetValue();
    this->InputWindowAndColorLevels[1] = this->ColorSlider->GetValue();

    WindowLevelProperty->SetElement(0, this->InputWindowAndColorLevels[0]);
    ColorLevelProperty->SetElement(0, this->InputWindowAndColorLevels[1]);
  }
  else
  {
    this->OutputWindowAndColorLevels[0] = this->WindowSlider->GetValue();
    this->OutputWindowAndColorLevels[1] = this->ColorSlider->GetValue();
    
    WindowLevelProperty->SetElement(0, this->OutputWindowAndColorLevels[0]);
    ColorLevelProperty->SetElement(0, this->OutputWindowAndColorLevels[1]);
  }

  this->WindowSlider->SetDisableCommands(0);
  this->ColorSlider->SetDisableCommands(0);
  
  this->ImageWorkspaceWidgetProxy->SetWindowAndColorLevels(this->WindowSlider->GetValue(), this->ColorSlider->GetValue());
}

void vtkPVHMImageWorkspaceWidget::ExecuteEvent(vtkObject *obj, unsigned long l, void *p)
{
  this->SetLastClickedVoxelFromProperty();

  //Calling this superclasse's method, its modified method will be invoked on picking interaction.
  //this->Superclass::ExecuteEvent(obj, l, p);
}

void vtkPVHMImageWorkspaceWidget::SetLastClickedVoxelFromProperty()
{
  vtkSMDoubleVectorProperty* XOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("XOfLastClickedVoxel"));

  vtkSMDoubleVectorProperty* YOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("YOfLastClickedVoxel"));

  vtkSMDoubleVectorProperty* ZOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("ZOfLastClickedVoxel"));

  vtkSMDoubleVectorProperty* IntensityOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
      this->ImageWorkspaceWidgetProxy->GetProperty("IntensityOfLastClickedVoxel"));
      
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(XOfLastClickedVoxelProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(YOfLastClickedVoxelProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(ZOfLastClickedVoxelProperty);
  this->ImageWorkspaceWidgetProxy->UpdatePropertyInformation(IntensityOfLastClickedVoxelProperty);

  this->LastClickedVoxel[0] = XOfLastClickedVoxelProperty->GetElement(0);
  this->LastClickedVoxel[1] = YOfLastClickedVoxelProperty->GetElement(0);
  this->LastClickedVoxel[2] = ZOfLastClickedVoxelProperty->GetElement(0);
  this->LastClickedVoxel[3] = IntensityOfLastClickedVoxelProperty->GetElement(0);

  this->SetLastClickedVoxelEntries(LastClickedVoxel);
}

void vtkPVHMImageWorkspaceWidget::SetLastClickedVoxelEntries(double *PassedLastClickedVoxel)
{
  this->XOfLastClickedVoxelEntry->SetValueAsDouble(PassedLastClickedVoxel[0]);
  this->YOfLastClickedVoxelEntry->SetValueAsDouble(PassedLastClickedVoxel[1]);
  this->ZOfLastClickedVoxelEntry->SetValueAsDouble(PassedLastClickedVoxel[2]);
  this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(PassedLastClickedVoxel[3]);
}

double* vtkPVHMImageWorkspaceWidget::GetLastClickedVoxel()
{
  return this->LastClickedVoxel;
}

void vtkPVHMImageWorkspaceWidget::SetVisibility (int val)
{
  this->WidgetVisibility = val;
  this->ImageWorkspaceWidgetProxy->SetVisibility(val);

  if (val)
  {
  	this->SetSeedsVisibility();
  	this->SetIntersectionLinesVisibility();
  }
  else
  {
    this->ImageWorkspaceWidgetProxy->SetSeedsVisibility(val);
    this->ImageWorkspaceWidgetProxy->SetIntersectionLinesVisibility(val);
  }
}

void vtkPVHMImageWorkspaceWidget::SetSeedsVisibility()
{
  if(this->WidgetVisibility)
  {
    this->ImageWorkspaceWidgetProxy->SetSeedsVisibility(this->SeedsVisibilityButton->GetSelectedState());
  }
}

void vtkPVHMImageWorkspaceWidget::SetSeedsAutoResize()
{
  this->ImageWorkspaceWidgetProxy->SetSeedsAutoResize(this->SeedsAutoResizeButton->GetSelectedState());
}

void vtkPVHMImageWorkspaceWidget::SetIntersectionLinesVisibility()
{
  if(this->WidgetVisibility)
  {
    this->ImageWorkspaceWidgetProxy->SetIntersectionLinesVisibility(this->IntersectionLinesVisibilityButton->GetSelectedState());
  }
}

void vtkPVHMImageWorkspaceWidget::SetWindowAndColorLevelsFromPreviousPipelineWidget()
{
  bool WidgetFound = false;
  vtkPVHMImageWorkspaceWidget *PreviousHMImageWorkspaceWidget = NULL;

  int NumberOfPipelinePVSources = this->GetPVApplication()->GetMainWindow()->GetSourceList("Sources")->GetNumberOfItems();

  if(NumberOfPipelinePVSources > 0 && !strcmp(this->GetPVSource()->GetWidgets()->GetLastPVWidget()->GetPVSource()->GetProxy()->GetXMLGroup(), "filters"))
  {
    //Gets the previous pipeline source reference.
//    vtkPVSource *PreviousSource = vtkPVSource::SafeDownCast(this->GetPVApplication()->GetMainWindow()->GetSourceList("Sources")->GetItemAsObject(NumberOfPipelinePVSources-1));

    //Gets the PVSource input reference.
    vtkPVSource *PreviousSource = this->GetPVSource()->GetPVInput(0);

    //Looking for the vtkPVHMImageWorkspaceWidget instance.
    for (int i=0; i < PreviousSource->GetWidgets()->GetNumberOfItems(); i++)
    {
      vtkPVWidget *widget = vtkPVWidget::SafeDownCast(PreviousSource->GetWidgets()->GetItemAsObject(i));

      if(strcmp(widget->GetClassName(), "vtkPVHMImageWorkspaceWidget") == 0)
      {
        WidgetFound = true;
        PreviousHMImageWorkspaceWidget = vtkPVHMImageWorkspaceWidget::SafeDownCast(PreviousSource->GetWidgets()->GetItemAsObject(i));
        break;
      }
    }
  } 
  
  if (WidgetFound)
  {
    //If a match was found, gets the window and color levels of previous object by the following two properties.
    vtkSMDoubleVectorProperty* WindowLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
        PreviousHMImageWorkspaceWidget->GetImageWorkspaceWidgetProxy()->GetProperty("WindowLevel"));

    vtkSMDoubleVectorProperty* ColorLevelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
        PreviousHMImageWorkspaceWidget->GetImageWorkspaceWidgetProxy()->GetProperty("ColorLevel"));

    if (!WindowLevelProperty || !ColorLevelProperty) 
    {
      vtkErrorMacro(<<"Cannot find window and color level properties and  property.");
    }
    else
    {
      this->WindowSlider->SetDisableCommands(1);
      this->ColorSlider->SetDisableCommands(1);
            
      this->WindowSlider->SetValue(WindowLevelProperty->GetElement(0));
      this->ColorSlider->SetValue(ColorLevelProperty->GetElement(0));

      this->WindowSlider->SetDisableCommands(0);
      this->ColorSlider->SetDisableCommands(0);
    }
  }
}

void vtkPVHMImageWorkspaceWidget::GetOutputWindowAndColorLevels(float PassedArray[2])
{
  PassedArray[0] = this->OutputWindowAndColorLevels[0];
  PassedArray[1] = this->OutputWindowAndColorLevels[1];
}
