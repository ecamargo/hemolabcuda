#ifndef _vtkPVHMImageWorkspaceWidget_h_
#define _vtkPVHMImageWorkspaceWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPV3DWidget.h"

class vtkPVSelectionList;

class vtkKWFrameWithLabel;
class vtkKWScaleWithEntry;
class vtkKWCheckButton;
class vtkKWLabel;
class vtkKWEntry;

class vtkSMHMImageWorkspaceWidgetProxy;
class vtkSMHMImageWorkspaceWidgetDataServerProxy;

class vtkPVApplication;

class VTK_EXPORT vtkPVHMImageWorkspaceWidget : public vtkPV3DWidget
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkPVHMImageWorkspaceWidget *New();

  // Description:
  // Macro required by VTK when extending its classes  
  vtkTypeRevisionMacro(vtkPVHMImageWorkspaceWidget,vtkPV3DWidget);

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();
  
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication* pvApp);

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when Accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();

  // Description:
  // Method invoked after the Accept method execution.
  virtual void PostAccept();
  
  // Description:
  // Called when Delete button is pushed.
  virtual void Delete();

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Method Invoked when source of this widget gets selected on the pipeline.
  virtual void Select();

  // Description:
  // Method Invoked when source of this widget gets deselected on the pipeline.  
  virtual void Deselect();
  
  // Description:
  // Set/Get widget visibility
  vtkSetMacro(WidgetVisibility,int);  
  vtkGetMacro(WidgetVisibility,int);	
  virtual void SetVisibility(int val);

  // Description:
  // Set/Get is a widget is ready only
  vtkSetMacro(ReadOnly,int);  
  vtkGetMacro(ReadOnly,int);	

  // Description:
  // Sets Widget Proxy executing a SafeDownCast for the needed type.
  void SetWidgetProxy(vtkSM3DWidgetProxy *wExternal);

  // Description:
  // Gets this classes WidgetProxy.
  vtkSMHMImageWorkspaceWidgetProxy *GetImageWorkspaceWidgetProxy();
  
  // Description:
  // Sets the RenderServer Widget X Plane Center, so it can reslice the Image Volume.
  void SetXPlaneCenter();

  // Description:
  // Sets the RenderServer Widget Y Plane Center, so it can reslice the Image Volume.
  void SetYPlaneCenter();

  // Description:
  // Sets the RenderServer Widget Z Plane Center, so it can reslice the Image Volume.
  void SetZPlaneCenter();

  // Description:
  // Enables / Disables the RenderServer Widgets X Plane texture visibility.
  void EnableXPlane();

  // Description:
  // Enables / Disables the RenderServer Widgets Y Plane texture visibility.
  void EnableYPlane();

  // Description:
  // Enables / Disables the RenderServer Widgets Z Plane texture visibility.
  void EnableZPlane();

  // Description:
  // Sets the window and color Levels on the three planes of the widget.
  void SetWindowAndColorLevels();

  // Description:
  // Sets the DataServer side widgets input.
  void SetInput(int InputServerObjectID);

  // Description:
  // Sets the Widget's input with the ImageData's input filter (used with the XML WidgetsInputVisibility property = 1).
  void SetInputFromSourcesInput();

  // Sets the Widget's input with the ImageData's output filter (used with the XML WidgetsInputVisibility property = 1).
  void SetInputFromSourcesOutput();

  // Description:
  // Returns the DataServer object's ID using this->PVSource.
  int GetOutputServerObject();
  
  // Description:
  // Returns the dimensions of the Widget's ImageData Copy
  int *GetDimensions();

  // Description:
  // Returns the spacing of the Widget's ImageData Copy
  double *GetSpacing();

  // Description:
  // Sets this class dimensions array.
  void SetDimensions(int *PassedDimensions);

  // Description:
  // Sets this class spacing array.
  void SetSpacing (double *PassedSpacing);

  // Description:
  // Sets this class extent from the widget's vtkImageData extent.
  void SetExtentFromServer();

  // Description:
  // Sets this class dimensions from the widget's dimensions.
  void SetDimensionsFromServer();
  
  // Description:
  // Sets this class spacing from de widget's spacing.
  void SetSpacingFromServer();

  // Description:
  // Sets the actual window and color levels from the widget's planes.
  void SetWindowAndColorLevelsFromServer();

  // Description:
  // Enables / Disables the passed vtkKWScaleWithEntry.
  void EnableSlider(vtkKWScaleWithEntry *PassedSlider, int MinValue, int MaxValue, bool Flag);

  // Description:
  // Enables / Disables all sliders.
  void EnableSliders(bool Flag);

  // Description:
  // Enables / Disables the passed vtkKWCheckButton.
  void EnableComponent(vtkKWWidget *PassedComponent, bool Flag);

  // Description:
  // Enables / Disables all check buttons.
  void EnableComponents(bool Flag);
  
  // Description:
  // Enables / Disables all panel components.
  void EnablePanel(bool Flag);

  // Description:
  // Gets the LastClickedVoxel attribute.
  double* GetLastClickedVoxel();

  // Description:
  // Enables / Disables the seed's auto resize mode.
  void SetSeedsAutoResize();

  // Description:
  // Enables / Disables the seed's visibility.  
  void SetSeedsVisibility();

  // Description:
  // Enables / Disables the Intersection lines visibility.  
  void SetIntersectionLinesVisibility();
  
  // Description:
  // Method invoked when the widget input combo box is modified.
  void SelectWidgetInputCallback();

  // Description:
  // Gets the Output WindowAndColorLevels array reference.
  void GetOutputWindowAndColorLevels(float PassedArray[2]);
  
protected:

  // Description:
  // Constructor.
  vtkPVHMImageWorkspaceWidget();

  // Description:
  // Destructor.
  virtual ~vtkPVHMImageWorkspaceWidget();

  // Description:Method invoked when the widget input combo box is modified.  
  // Method executed when any iteraction is made on the viewport.
  virtual void	ExecuteEvent(vtkObject *obj, unsigned long l, void *p);

  // Description:
  // Sets the LastClickedVoxel Entry from it's XML respective property.
  void SetLastClickedVoxelFromProperty();

  // Description:
  // Sets the LastClickedVoxelEntry from a passed double array.
  void SetLastClickedVoxelEntries(double *PassedLastClickedVoxel);

  // Description:
  // Sets the window and color levels of this object from the previous pipeline vtkPVHMImageWorkspaceWidget attribute information.
  void SetWindowAndColorLevelsFromPreviousPipelineWidget();

  // Description:
  // Sets the widget's input with the previous pipeline object output.  
  void SetWidgetInput();

  // Description:
  // Reference to the intanciated vtkPVWindow object.
  vtkPVWindow *PVWindow;
  
  // Description:
  // Proxy that comunicates with the widget and Data
  vtkSMHMImageWorkspaceWidgetProxy *ImageWorkspaceWidgetProxy;

  // Description:
  // Controls the status of the widget's events (started / added or stoped / removed).
  int WidgetsEventsStarted;

  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;
  
  // Description:
  // Flag that controls the Accepted state of the widget.
  int AlreadyAccepted;

  // Description:
  // Setted as 1 only during the first widget execution.
  int FirstExecute;

  // Description:
  // Flag responsible for disabling the WidgetOutputSelectionList if the source / filter is classified as an Extent Changer.
  int ExtentChanger;

   // Description:
  // Select the filters/sources input or output for visualization on the widget planes.
  vtkPVSelectionList *WidgetOutputSelectionList;

  // Description:
  // Frame that groups all planes manipulation tools.
  vtkKWFrameWithLabel *PlanesManipulationFrame;

  // Description:
  // Slider responsable for moving the widget's X plane.
  vtkKWScaleWithEntry *XSlider;

  // Description:
  // Slider responsable for moving the widget's Y plane.
  vtkKWScaleWithEntry *YSlider;

  // Description:
  // Slider responsable for moving the widget's Z plane.
  vtkKWScaleWithEntry *ZSlider;

  // Description:
  // Check button responsable for turning of / off the widget's X plane texture visibility.
  vtkKWCheckButton *XPlaneEnable;

  // Description:
  // Check button responsable for turning of / off the widget's Y plane texture visibility.
  vtkKWCheckButton *YPlaneEnable;

  // Description:
  // Check button responsable for turning of / off the widget's Z plane texture visibility.
  vtkKWCheckButton *ZPlaneEnable;

  // Description:
  // Slider that changes the planes window level.
  vtkKWScaleWithEntry *WindowSlider;

  // Description:
  // Slider that changes the planes color level.
  vtkKWScaleWithEntry *ColorSlider;

  // Description:
  // Frame that groups all LastClickedVoxel's manipulation tools.
  vtkKWFrameWithLabel *LastClickedVoxelsFrame;

  // Description:
  // Last Clicked Voxel's Label.
  vtkKWLabel *LastClickedVoxelLabel;

  // Description:
  // Entry that stores the X coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *XOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Y coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *YOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Z coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *ZOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Z coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *IntensityOfLastClickedVoxelEntry;

  // Description:
  // Frame that groups all planes manipulation tools.
  vtkKWFrameWithLabel *SeedsManipulationFrame;

  // Description:
  // Sets on and off the seeds visibility.
  vtkKWCheckButton *SeedsVisibilityButton;

  // Description:
  // Sets on and off the seeds auto resize funcionality.
  vtkKWCheckButton *SeedsAutoResizeButton;

  // Description:
  // Sets on and off planes intersection lines visibility.  
  vtkKWCheckButton *IntersectionLinesVisibilityButton;

  // Description:
  // Six dimension integer array that stores the volume extent.
  int Extent[6];

  // Description:
  // Six dimension integer array that stores the previous volume extent.
  int OldExtent[6];

  // Description:
  // Three dimension integer array that stores the volume dimensions.
  int Dimensions[3];

  // Description:
  // Three dimension double array that stores the volume spacing.  
  double Spacing[3];
  
  // Description:
  // Four dimensions double array that stores the last clicked voxel information (x, y, z and intensity).  
  double LastClickedVoxel[4];
  
  float InputWindowAndColorLevels[2];

  float OutputWindowAndColorLevels[2];

private:  
  vtkPVHMImageWorkspaceWidget(const vtkPVHMImageWorkspaceWidget&); // Not implemented
  void operator=(const vtkPVHMImageWorkspaceWidget&); // Not implemented

}; 

#endif
