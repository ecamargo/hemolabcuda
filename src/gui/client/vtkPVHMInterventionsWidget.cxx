/*
 * $Id: vtkPVHMInterventionsWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWThumbWheel.h"

#include "vtkPVHMInterventionsWidget.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMDefineVariables.h"

vtkCxxRevisionMacro(vtkPVHMInterventionsWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMInterventionsWidget);

//----------------------------------------------------------------------------
vtkPVHMInterventionsWidget::vtkPVHMInterventionsWidget()
{
	this->NameLabel			= vtkKWLabel::New();
	this->FactorLabel		= vtkKWLabel::New();
	this->PropertyLabel	= vtkKWLabel::New();
	
  this->ElastinLabel				= vtkKWLabel::New();
  this->ElastinEntry 				= vtkKWEntry::New();
  this->ElastinPropertyLabel				= vtkKWLabel::New();
  this->ViscoElasticityLabel		= vtkKWLabel::New();
  this->ViscoElasticityEntry		= vtkKWEntry::New();
  this->ViscoElasticityPropertyLabel		= vtkKWLabel::New();
  this->SizeLabel					= vtkKWLabel::New();
  this->SizeEntry 				= vtkKWEntry::New();
  this->SizePropertyLabel					= vtkKWLabel::New();
  
  this->MechanicalFrame	= vtkKWFrameWithLabel::New();
  
  this->UpdatePropertiesButton	= vtkKWPushButton::New();
  
  this->PercentageThumbWheel = vtkKWThumbWheel::New();
}

//----------------------------------------------------------------------------
vtkPVHMInterventionsWidget::~vtkPVHMInterventionsWidget()
{
	this->NameLabel->Delete();
	this->FactorLabel->Delete();
	this->PropertyLabel->Delete();
	
	this->ElastinLabel->Delete();
  this->ElastinEntry->Delete();
  this->ElastinPropertyLabel->Delete();
  
  this->ViscoElasticityLabel->Delete();
  this->ViscoElasticityEntry->Delete();
  this->ViscoElasticityPropertyLabel->Delete();
  
  this->SizeLabel->Delete();
  this->SizeEntry->Delete();
  this->SizePropertyLabel->Delete();
  
  this->UpdatePropertiesButton->Delete(); 
  
  this->MechanicalFrame->Delete();
  
  this->PercentageThumbWheel->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::UpdateWidgetInfo(	)
{
  int mode = this->GetReadOnly();
  if( (mode == MODE_EDITION_VAL) || (mode == MODE_TODETACHSUBTREE_VAL) || (mode == MODE_ESPECIAL_TOOLS_VAL) )
  	{
	  this->ElastinEntry->EnabledOn();
	  this->ViscoElasticityEntry->EnabledOn();
	  this->SizeEntry->EnabledOn();
	  this->PercentageThumbWheel->EnabledOn();
  	}
  else
  	{
  	this->ElastinEntry->EnabledOff();
	  this->ViscoElasticityEntry->EnabledOff();
	  this->SizeEntry->EnabledOff();
	  this->PercentageThumbWheel->EnabledOff();
  	}
}
	
//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Interventions Surgicals Information"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
  
  this->MechanicalFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->MechanicalFrame->Create(pvApp);
//  this->MechanicalFrame->SetLabelText("Multiplicative Factor Properties");
  
  this->NameLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->NameLabel->Create(pvApp);
  this->NameLabel->SetText("Names");
  
  this->FactorLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->FactorLabel->Create(pvApp);
  this->FactorLabel->SetText("Factors");
  
  this->PropertyLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->PropertyLabel->Create(pvApp);
  this->PropertyLabel->SetText("Properties");
  
  this->ElastinLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ElastinLabel->Create(pvApp);
  this->ElastinLabel->SetText("Elastin [dyn/cm^2]:");
  this->ElastinEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->ElastinEntry->Create(pvApp);
  this->ElastinEntry->SetWidth(8);
  this->ElastinPropertyLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ElastinPropertyLabel->Create(pvApp);
  
  this->ViscoElasticityLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityLabel->Create(pvApp);
  this->ViscoElasticityLabel->SetText("Viscoelasticity [dyn.sec/cm^2]:");
	this->ViscoElasticityEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityEntry->Create(pvApp);
  this->ViscoElasticityEntry->SetWidth(8);
  this->ViscoElasticityPropertyLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->ViscoElasticityPropertyLabel->Create(pvApp);
  
  this->SizeLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->SizeLabel->Create(pvApp);
  this->SizeLabel->SetText("Size [cm]:");
  this->SizeEntry->SetParent(this->MechanicalFrame->GetFrame());
  this->SizeEntry->Create(pvApp);
  this->SizeEntry->SetWidth(8);
  this->SizePropertyLabel->SetParent(this->MechanicalFrame->GetFrame());
  this->SizePropertyLabel->Create(pvApp);
  
  this->UpdatePropertiesButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdatePropertiesButton->Create(pvApp);
  this->UpdatePropertiesButton->SetText("Update");
  
  this->PercentageThumbWheel->SetParent(this->MechanicalFrame->GetFrame());
  this->PercentageThumbWheel->PopupModeOn();
  this->PercentageThumbWheel->Create(pvApp);
  this->PercentageThumbWheel->SetRange(0.0, 300.0);
  this->PercentageThumbWheel->SetMinimumValue(0.0);
  this->PercentageThumbWheel->SetMaximumValue(300.0);
  this->PercentageThumbWheel->ClampMaximumValueOn();
  this->PercentageThumbWheel->ClampMinimumValueOn();
  this->PercentageThumbWheel->SetResolution(1.0);
  this->PercentageThumbWheel->DisplayEntryOn();
  this->PercentageThumbWheel->DisplayLabelOn();
  this->PercentageThumbWheel->GetLabel()->SetText("Percentage: %");
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
	int mode = mainWidget->GetWidgetMode();
	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		  mainWidget->Script("grid remove %s",
    	this->GetChildFrame()->GetWidgetName());
			return;
		}
	this->GetChildFrame()->ExpandFrame();
	
  mainWidget->Script("grid %s - - -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName());
  
	mainWidget->Script("grid %s -ipadx 10m",
    this->MechanicalFrame->GetWidgetName());
	
	//colocar os components do stent
	mainWidget->Script("grid %s %s %s -sticky w",
	  this->NameLabel->GetWidgetName(),
	  this->PropertyLabel->GetWidgetName(),
	  this->FactorLabel->GetWidgetName());

  mainWidget->Script("grid %s %s %s -sticky w",
    this->ElastinLabel->GetWidgetName(),
    this->ElastinPropertyLabel->GetWidgetName(),
    this->ElastinEntry->GetWidgetName());

  mainWidget->Script("grid %s %s %s -sticky w",
    this->ViscoElasticityLabel->GetWidgetName(),
    this->ViscoElasticityPropertyLabel->GetWidgetName(),
    this->ViscoElasticityEntry->GetWidgetName());
  
  this->SizeEntry->SetWidth(8);
  mainWidget->Script("grid %s %s %s -sticky w",
    this->SizeLabel->GetWidgetName(),
    this->SizePropertyLabel->GetWidgetName(),
    this->SizeEntry->GetWidgetName());
	
	//retirar outros components
	mainWidget->Script("grid remove %s", this->NameLabel->GetWidgetName());
	mainWidget->Script("grid remove %s", this->PropertyLabel->GetWidgetName());
	mainWidget->Script("grid remove %s", this->FactorLabel->GetWidgetName());

  mainWidget->Script("grid remove %s", this->ElastinLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->ElastinPropertyLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->ElastinEntry->GetWidgetName());
  
  mainWidget->Script("grid remove %s", this->ViscoElasticityLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->ViscoElasticityPropertyLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->ViscoElasticityEntry->GetWidgetName());

  mainWidget->Script("grid remove %s", this->SizeLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->SizePropertyLabel->GetWidgetName());
  mainWidget->Script("grid remove %s", this->SizeEntry->GetWidgetName());
  
  mainWidget->Script("grid remove %s",
   	this->UpdatePropertiesButton->GetWidgetName());
  
	mainWidget->Script("grid remove %s", this->PercentageThumbWheel->GetWidgetName());
	
	
	if ( this->ObjectSelected == STENT )
		{
		this->MechanicalFrame->SetLabelText("Multiplicative factor properties for stent");
		
		//colocar os components do stent
		mainWidget->Script("grid %s %s %s -sticky w",
		  this->NameLabel->GetWidgetName(),
		  this->PropertyLabel->GetWidgetName(),
		  this->FactorLabel->GetWidgetName());
	
	  mainWidget->Script("grid %s %s %s -sticky w",
	    this->ElastinLabel->GetWidgetName(),
	    this->ElastinPropertyLabel->GetWidgetName(),
	    this->ElastinEntry->GetWidgetName());
	
	  mainWidget->Script("grid %s %s %s -sticky w",
	    this->ViscoElasticityLabel->GetWidgetName(),
	    this->ViscoElasticityPropertyLabel->GetWidgetName(),
	    this->ViscoElasticityEntry->GetWidgetName());
	  
	  this->SizeEntry->SetWidth(8);
	  mainWidget->Script("grid %s %s %s -sticky w",
      this->SizeLabel->GetWidgetName(),
      this->SizePropertyLabel->GetWidgetName(),
      this->SizeEntry->GetWidgetName());
	  
	  this->ElastinEntry->SetBalloonHelpString("Effective elastin module in the element.");
	  this->ViscoElasticityEntry->SetBalloonHelpString("Viscoelasticity of the artery wall in the element.");
	  this->SizeEntry->SetBalloonHelpString("Size of the affected area of the segment.");
		}
	else if ( this->ObjectSelected == STENOSIS )
		{
		this->MechanicalFrame->SetLabelText("Percentage of reduction");
		
	  this->SizeEntry->SetWidth(15);
	  mainWidget->Script("grid %s %s - - -sticky w",
      this->SizeLabel->GetWidgetName(),
      this->SizeEntry->GetWidgetName());
	  
	  this->PercentageThumbWheel->GetEntry()->SetWidth(13);
	  mainWidget->Script("grid %s - - -sticky w -columnspan 2 -pady 4", this->PercentageThumbWheel->GetWidgetName());
		}
	else if ( this->ObjectSelected == ANEURYSM )
		{
		this->MechanicalFrame->SetLabelText("Percentage of increase");
		
	  this->SizeEntry->SetWidth(15);
	  mainWidget->Script("grid %s %s - - -sticky w",
      this->SizeLabel->GetWidgetName(),
      this->SizeEntry->GetWidgetName());
	  
	  this->PercentageThumbWheel->GetEntry()->SetWidth(13);
	  mainWidget->Script("grid %s - - -sticky w -columnspan 2 -pady 4", this->PercentageThumbWheel->GetWidgetName());
		}
	else if ( this->ObjectSelected == AGING )
		{
		this->MechanicalFrame->SetLabelText("Multiplicative factor properties for aging");
		
		//colocar os components do stent
		mainWidget->Script("grid %s %s -sticky w",
		  this->NameLabel->GetWidgetName(),
		  this->FactorLabel->GetWidgetName());
	
	  mainWidget->Script("grid %s %s -sticky w",
	    this->ElastinLabel->GetWidgetName(),
	    this->ElastinEntry->GetWidgetName());
	
	  mainWidget->Script("grid %s %s -sticky w",
	    this->ViscoElasticityLabel->GetWidgetName(),
	    this->ViscoElasticityEntry->GetWidgetName());
	  
	  this->ElastinEntry->SetBalloonHelpString("Effective elastin module in the element.");
	  this->ViscoElasticityEntry->SetBalloonHelpString("Viscoelasticity of the artery wall in the element.");
		}
	
  if( (mode == MODE_EDITION_VAL) || (mode == MODE_ESPECIAL_TOOLS_VAL) )
  	mainWidget->Script("grid %s - - -sticky w -pady 2",
  			this->UpdatePropertiesButton->GetWidgetName());
  else
  	mainWidget->Script("grid remove %s",
  	  	this->UpdatePropertiesButton->GetWidgetName());
  
  this->UpdatePropertiesButton->SetBalloonHelpString("Updates element properties");
}

  
vtkKWFrameWithLabel* vtkPVHMInterventionsWidget::GetElementFrame()
{
	return this->GetChildFrame();
}
	
//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::Accept()
{
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::SetElastin(double Elastin)
{
	char s[50];
	sprintf(s, "%.5g", Elastin);
	this->ElastinPropertyLabel->SetText(s);
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::SetViscoElasticity(double visco)
{
	char s[50];
	sprintf(s, "%.5g", visco);
	this->ViscoElasticityPropertyLabel->SetText(s);
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::SetSize(double size)
{
	char s[50];
	sprintf(s, "%.3f", size);
	this->SizePropertyLabel->SetText(s);
}

//----------------------------------------------------------------------------
void vtkPVHMInterventionsWidget::SetPercentage(double percentage)
{
	this->PercentageThumbWheel->SetValue(percentage);
}

// get methods
//----------------------------------------------------------------------------
double vtkPVHMInterventionsWidget::GetElastin()
{
	double value = this->ElastinEntry->GetValueAsDouble();
	if ( value )
		{
		if ( value < 0 )
			{
			value = 1;
			this->ElastinEntry->SetValueAsDouble(value);
			}
		}
	else
		{
		value = 1;
		this->ElastinEntry->SetValueAsDouble(value);
		}
	return value;
}

//----------------------------------------------------------------------------
double vtkPVHMInterventionsWidget::GetViscoElasticity()
{
	double value = this->ViscoElasticityEntry->GetValueAsDouble();
	if ( value )
		{
		if ( value < 0 )
			{
			value = 1;
			this->ViscoElasticityEntry->SetValueAsDouble(value);
			}
		}
	else
		{
		value = 1;
		this->ViscoElasticityEntry->SetValueAsDouble(value);
		}
	return value;
}

//----------------------------------------------------------------------------
double vtkPVHMInterventionsWidget::GetSize()
{
	return this->SizeEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMInterventionsWidget::GetPercentage()
{
	return this->PercentageThumbWheel->GetValue();	
}
