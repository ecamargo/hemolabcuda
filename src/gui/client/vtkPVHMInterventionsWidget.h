

/*
 * $Id: vtkPVHMInterventionsWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMInterventionsWidget
// .SECTION Description
// Creates KW Components for Segment Elements


#ifndef VTKPVHMINTERVENTIONSWIDGET_H_
#define VTKPVHMINTERVENTIONSWIDGET_H_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"


class vtkKWEntry;
class vtkKWLabel;
class vtkSMSourceProxy;
class vtkKWFrameWithLabel;
class vtkKWThumbWheel;

class vtkPVHMStraightModelWidget;


class VTK_EXPORT vtkPVHMInterventionsWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMInterventionsWidget *New();
  vtkTypeRevisionMacro(vtkPVHMInterventionsWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();
  
  // Description:
  // Element Components access methods
  void SetElastin(double Elastin);
  void SetViscoElasticity(double visco);
  void SetSize(double size);
  void SetPercentage(double percentage);
  
  double GetElastin();
  double GetViscoElasticity();
  double GetSize();
  double GetPercentage();
  
  vtkKWPushButton *UpdatePropertiesButton;
  
  // Description:
  // Set/Get object selected
  vtkSetMacro(ObjectSelected, vtkIdType);
  vtkGetMacro(ObjectSelected, vtkIdType);
  
protected:
  vtkPVHMInterventionsWidget();
  virtual ~vtkPVHMInterventionsWidget();
  
  // Description:
  // Labels for name, factor and properties
  vtkKWLabel* NameLabel;
  vtkKWLabel* FactorLabel;
  vtkKWLabel* PropertyLabel;
  
	// Description:
	// Properties of the segment
  vtkKWLabel* ElastinLabel;
  vtkKWEntry* ElastinEntry;
  vtkKWLabel* ElastinPropertyLabel;
  vtkKWLabel* ViscoElasticityLabel;
  vtkKWEntry* ViscoElasticityEntry;
  vtkKWLabel* ViscoElasticityPropertyLabel;
  
  // Description:
  // Size of the affected area
  vtkKWLabel* SizeLabel;
  vtkKWEntry* SizeEntry;
  vtkKWLabel* SizePropertyLabel;
  
  vtkKWFrameWithLabel *MechanicalFrame;
  
  vtkIdType ObjectSelected;
  
  // Description:
  // Percentage of reduction or increase of the area
  vtkKWThumbWheel *PercentageThumbWheel;
 
private:  
  vtkPVHMInterventionsWidget(const vtkPVHMInterventionsWidget&); // Not implemented
  void operator=(const vtkPVHMInterventionsWidget&); // Not implemented
}; 


#endif /*VTKPVHMINTERVENTIONSWIDGET_H_*/
