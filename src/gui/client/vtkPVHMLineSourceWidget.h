#ifndef __vtkPVHMLineSourceWidget_h
#define __vtkPVHMLineSourceWidget_h

#include "vtkPVHMStraightModelWidget.h"

class vtkPVInputMenu;
class vtkSMSourceProxy;

class VTK_EXPORT vtkPVHMLineSourceWidget : public vtkPVHMStraightModelWidget
{
public:
  static vtkPVHMLineSourceWidget* New();
  vtkTypeRevisionMacro(vtkPVHMLineSourceWidget, vtkPVHMStraightModelWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the widget.
  virtual void Create(vtkKWApplication *app);

  // Description:
  // Saves the value of this widget into a VTK Tcl script.
  // This creates the line source (one for all parts).
  virtual void SaveInBatchScript(ofstream *file);

  //BTX
  // Description:
  // The methods get called when the Accept button is pressed. 
  // It sets the VTK objects value using this widgets value.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize place after creation
  virtual void Initialize();

  // Description:
  // The methods get called when the Reset button is pressed. 
  // It sets this widgets value using the VTK objects value.
  virtual void ResetInternal();

  // Description:
  // This is called if the input menu changes.
  virtual void Update();

  // Description:
  // 
  void SetInputMenu(vtkPVInputMenu *im);

  virtual vtkSMProxy* GetProxyByName(const char*) { return reinterpret_cast<vtkSMProxy*>(this->SourceProxy); }
 
  // Description:
  // Register the animatable proxies and make them available for animation.
  // Called by vtkPVSelectWidget when the widget is selected. This
  // is to make sure that only the selected widget shows up in the
  // animation interface and thus avoids confusion.
  virtual void EnableAnimation();

  // Description:
  // Unregister animatable proxies so that they are not available for
  // animation. Called by vtkPVSelectWidget when this widget is deselected.
  // is to make sure that only the selected widget shows up in the
  // animation interface and thus avoids confusion.
  virtual void DisableAnimation();

protected:
  vtkPVHMLineSourceWidget();
  ~vtkPVHMLineSourceWidget();
  
  vtkSMSourceProxy *SourceProxy;

  vtkPVHMLineSourceWidget(const vtkPVHMLineSourceWidget&); // Not implemented
  void operator=(const vtkPVHMLineSourceWidget&); // Not implemented

  virtual int ReadXMLAttributes(vtkPVXMLElement *element,
                                vtkPVXMLPackageParser *parser);
//BTX
  virtual void CopyProperties(vtkPVWidget *clone, vtkPVSource *pvSource,
                              vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map);
//ETX
  vtkPVInputMenu *InputMenu;
};

#endif
