/*
 * $Id: vtkPVHMMultipleSegmentWidget.cxx 340 2006-05-12 13:40:37Z ziemer $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkPVHMMultipleSegmentWidget.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMDefineVariables.h"


vtkCxxRevisionMacro(vtkPVHMMultipleSegmentWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMMultipleSegmentWidget);

//----------------------------------------------------------------------------
vtkPVHMMultipleSegmentWidget::vtkPVHMMultipleSegmentWidget()
{ 
  
  this->PropertyLabel				= vtkKWLabel::New();
  
  this->NodesRadiusLabel= vtkKWLabel::New();
  this->NodesRadiusEntry  = vtkKWEntry::New();
  this->NodesWallLabel= vtkKWLabel::New();
  this->NodesWallEntry  = vtkKWEntry::New();
  this->ElastinLabel				= vtkKWLabel::New();
  this->ElastinEntry 				= vtkKWEntry::New();
  this->CollagenLabel			= vtkKWLabel::New();
  this->CollagenEntry 			= vtkKWEntry::New();
  this->CoefficientALabel			= vtkKWLabel::New();
  this->CoefficientAEntry			= vtkKWEntry::New();
  this->CoefficientBLabel			= vtkKWLabel::New();
  this->CoefficientBEntry 			= vtkKWEntry::New();   
  this->ViscoElasticityLabel		= vtkKWLabel::New();
  this->ViscoElasticityEntry		= vtkKWEntry::New(); 
  this->ViscoElasticityExpLabel	= vtkKWLabel::New();
  this->ViscoElasticityExpEntry		= vtkKWEntry::New(); 
  this->InfPressureLabel			= vtkKWLabel::New();
  this->InfPressureEntry			= vtkKWEntry::New(); 
  this->RefPressureLabel			= vtkKWLabel::New();
  this->RefPressureEntry			= vtkKWEntry::New(); 
  this->PermeabilityLabel			= vtkKWLabel::New();
  this->PermeabilityEntry			= vtkKWEntry::New();  
  
  this->UpdateSingleElemPropButton	= vtkKWPushButton::New();
  
  this->UpdateWholeSegPropButton = vtkKWPushButton::New();
  
  this->UpdateMethodComboBox = vtkKWComboBox::New();
}

//----------------------------------------------------------------------------
vtkPVHMMultipleSegmentWidget::~vtkPVHMMultipleSegmentWidget()
{
  this->PropertyLabel->Delete();
  
  this->NodesRadiusLabel->Delete();
  this->NodesRadiusEntry->Delete();
  this->NodesWallLabel->Delete();
  this->NodesWallEntry->Delete();
  
  this->ElastinLabel->Delete();
  this->ElastinEntry->Delete();
  this->CollagenLabel->Delete();
  this->CollagenEntry->Delete();  
  this->CoefficientALabel->Delete();  
  this->CoefficientAEntry->Delete();  
  this->CoefficientBLabel->Delete();  
  this->CoefficientBEntry->Delete();  
  this->ViscoElasticityLabel->Delete();
  this->ViscoElasticityEntry->Delete();
  this->ViscoElasticityExpLabel->Delete();
  this->ViscoElasticityExpEntry->Delete();
  this->InfPressureLabel->Delete();
  this->InfPressureEntry->Delete();
  this->RefPressureLabel->Delete();
  this->RefPressureEntry->Delete();
  this->PermeabilityLabel->Delete();
  this->PermeabilityEntry->Delete();
  
 
  this->UpdateSingleElemPropButton->Delete(); 
  
  this->UpdateWholeSegPropButton->Delete();
  
  
  this->UpdateMethodComboBox->Delete();
  
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleSegmentWidget::UpdateWidgetInfo(	)
{
  int mode = this->GetReadOnly();

  // Se mode de destaque de sub-arvore, habilita modo de edição de multiplos segmentos
  if((this->GetReadOnly()==MODE_TODETACHSUBTREE_VAL))
  	{
  	mode = MODE_EDITION_VAL;
  	}
  this->NodesRadiusEntry->SetReadOnly(mode);
  this->NodesWallEntry->SetReadOnly(mode);
  this->ElastinEntry->SetReadOnly(mode);
	this->CollagenEntry->SetReadOnly(mode);	  
	this->CoefficientAEntry->SetReadOnly(mode);	  
	this->CoefficientBEntry->SetReadOnly(mode);	  
	this->ViscoElasticityExpEntry->SetReadOnly(mode);	  
	this->ViscoElasticityEntry->SetReadOnly(mode);	  
	this->InfPressureEntry->SetReadOnly(mode);	  
	this->RefPressureEntry->SetReadOnly(mode);	  
	this->PermeabilityEntry->SetReadOnly(mode);
	  
}
	
//----------------------------------------------------------------------------
void vtkPVHMMultipleSegmentWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Multiple Segment Edition Mode"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
  
  this->PropertyLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PropertyLabel->Create(pvApp);
  this->PropertyLabel->SetText("Property Update Method");
  
  this->UpdateMethodComboBox->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateMethodComboBox->Create(pvApp);
  this->UpdateMethodComboBox->SetBalloonHelpString("Select if segments should be updated by a fixed value or a multiplicative factor");
  const char *UpdateMethod[] = {"Fixed property value", "Multiplicative factor"};
  int i;
  for (i = 0; i < 2; i++)
	this->UpdateMethodComboBox->AddValue(UpdateMethod[i]);
	
  this->UpdateMethodComboBox->SetValue("Multiplicative factor");
  
  this->UpdateMethodComboBox->ReadOnlyOn();
	this->UpdateMethodComboBox->SetWidth(20);

  this->NodesRadiusLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->NodesRadiusLabel->Create(pvApp);
  this->NodesRadiusLabel->SetText("Nodes Radius [cm]:");
  this->NodesRadiusEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->NodesRadiusEntry->Create(pvApp);
  this->NodesRadiusEntry->SetWidth(6);
 
  this->NodesWallLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->NodesWallLabel->Create(pvApp);
  this->NodesWallLabel->SetText("Nodes Wallthickness [cm]:");
	this->NodesWallEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->NodesWallEntry->Create(pvApp);
  this->NodesWallEntry->SetWidth(6);
  
  
  this->ElastinLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ElastinLabel->Create(pvApp);
  this->ElastinLabel->SetText("Elastin [dyn/cm^2]:");
  this->ElastinEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->ElastinEntry->Create(pvApp);
  this->ElastinEntry->SetWidth(6);
 
  this->CollagenLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->CollagenLabel->Create(pvApp);
  this->CollagenLabel->SetText("Collagen Elastin [dyn/cm^2]:");
	this->CollagenEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->CollagenEntry->Create(pvApp);
  this->CollagenEntry->SetWidth(6);
 
  this->CoefficientALabel->SetParent(this->GetChildFrame()->GetFrame());
  this->CoefficientALabel->Create(pvApp);
  this->CoefficientALabel->SetText("Coefficient A:");
	this->CoefficientAEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->CoefficientAEntry->Create(pvApp);
  this->CoefficientAEntry->SetWidth(6);
 
  this->CoefficientBLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->CoefficientBLabel->Create(pvApp);
  this->CoefficientBLabel->SetText("Coefficient B:");
	this->CoefficientBEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->CoefficientBEntry->Create(pvApp);
  this->CoefficientBEntry->SetWidth(6);
 
  this->ViscoElasticityLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ViscoElasticityLabel->Create(pvApp);
  this->ViscoElasticityLabel->SetText("Viscoelasticity [dyn.sec/cm^2]:");
	this->ViscoElasticityEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->ViscoElasticityEntry->Create(pvApp);
  this->ViscoElasticityEntry->SetWidth(6);
  
  this->ViscoElasticityExpLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ViscoElasticityExpLabel->Create(pvApp);
  this->ViscoElasticityExpLabel->SetText("Viscoelasticity Expoent:");
	this->ViscoElasticityExpEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->ViscoElasticityExpEntry->Create(pvApp);
  this->ViscoElasticityExpEntry->SetWidth(6);
 
  this->InfPressureLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->InfPressureLabel->Create(pvApp);
  this->InfPressureLabel->SetText("Infiltration Pressure [dyn/cm^2]:");
	this->InfPressureEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->InfPressureEntry->Create(pvApp);
  this->InfPressureEntry->SetWidth(6);
  
  this->RefPressureLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->RefPressureLabel->Create(pvApp);
  this->RefPressureLabel->SetText("Reference Pressure [dyn/cm^2]:");
  this->RefPressureEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->RefPressureEntry->Create(pvApp);
  this->RefPressureEntry->SetWidth(6);
  
  this->PermeabilityLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PermeabilityLabel->Create(pvApp);
  this->PermeabilityLabel->SetText("Permeability [cm^4/sec/dyn]:");
  this->PermeabilityEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->PermeabilityEntry->Create(pvApp);
  this->PermeabilityEntry->SetWidth(6);
   	
  
  this->UpdateSingleElemPropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateSingleElemPropButton->Create(pvApp);
  this->UpdateSingleElemPropButton->SetText("Update Selected Segments");
  

  this->UpdateWholeSegPropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateWholeSegPropButton->Create(pvApp);
  this->UpdateWholeSegPropButton->SetText("Update Tree");

  
   
    
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleSegmentWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{		
	int mode = mainWidget->GetWidgetMode();
	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		mainWidget->Script("grid forget %s",
    this->GetChildFrame()->GetWidgetName());			
		return;
		} 
	this->GetChildFrame()->ExpandFrame();				
	
	mainWidget->Script("grid %s -sticky ew -columnspan 4",
  this->GetChildFrame()->GetWidgetName());  
      
  
	mainWidget->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->PropertyLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->UpdateMethodComboBox->GetWidgetName());


	mainWidget->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->NodesRadiusLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->NodesRadiusEntry->GetWidgetName());


	mainWidget->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->NodesWallLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->NodesWallEntry->GetWidgetName());

	mainWidget->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->ElastinLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 3 -column 1 -padx 2 -pady 2", this->ElastinEntry->GetWidgetName());
  
	mainWidget->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->CollagenLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 4 -column 1 -padx 2 -pady 2", this->CollagenEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 5 -column 0 -padx 2 -pady 2", this->CoefficientALabel->GetWidgetName());
  mainWidget->Script("grid %s -row 5 -column 1 -padx 2 -pady 2", this->CoefficientAEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 6 -column 0 -padx 2 -pady 2", this->CoefficientBLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 6 -column 1 -padx 2 -pady 2", this->CoefficientBEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 7 -column 0 -padx 2 -pady 2", this->ViscoElasticityLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 7 -column 1 -padx 2 -pady 2", this->ViscoElasticityEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 8 -column 0 -padx 2 -pady 2", this->ViscoElasticityExpLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 8 -column 1 -padx 2 -pady 2", this->ViscoElasticityExpEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 9 -column 0 -padx 2 -pady 2", this->InfPressureLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 9 -column 1 -padx 2 -pady 2", this->InfPressureEntry->GetWidgetName());
  
 	mainWidget->Script("grid %s -row 10 -column 0 -padx 2 -pady 2", this->RefPressureLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 10 -column 1 -padx 2 -pady 2", this->RefPressureEntry->GetWidgetName());
  
	mainWidget->Script("grid %s -row 11 -column 0 -padx 2 -pady 2", this->PermeabilityLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 11 -column 1 -padx 2 -pady 2", this->PermeabilityEntry->GetWidgetName());
  
	mainWidget->Script("grid %s -row 12 -column 0 -padx 2 -pady 2", this->PermeabilityLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 12 -column 1 -padx 2 -pady 2", this->PermeabilityEntry->GetWidgetName());
  
// Se modo de visualização, não aparece botoes de update para multiplos segmentos.
	if(mode == MODE_VISUALIZATION_VAL)
		{
		mainWidget->Script("grid remove %s %s",
		this->UpdateSingleElemPropButton->GetWidgetName(),
		this->UpdateWholeSegPropButton->GetWidgetName());
		}
	// Se modo de edição, aparece botoes de update para multiplos segmentos.	
	else if (mode == MODE_EDITION_VAL)
		{
		mainWidget->Script("grid %s %s - - -sticky w", 
		this->UpdateSingleElemPropButton->GetWidgetName(),
		this->UpdateWholeSegPropButton->GetWidgetName());
		}
	// Se modo de destaque de sub-arvore aparece o botão de update de segmentos selecionados
	else if (mode == MODE_TODETACHSUBTREE_VAL)
		{
		mainWidget->Script("grid %s %s - - -sticky w", 
		this->UpdateSingleElemPropButton->GetWidgetName(),
		this->UpdateWholeSegPropButton->GetWidgetName());
		mainWidget->Script("grid remove %s",
		this->UpdateWholeSegPropButton->GetWidgetName());
		} 

	this->ElastinEntry->SetBalloonHelpString("Effective elastin module in the element.");  
  this->CollagenEntry->SetBalloonHelpString("Effective elastin module of colagen in the element.");  
  this->CoefficientAEntry->SetBalloonHelpString("Average value of the coefficient A in the element.");  
  this->CoefficientBEntry->SetBalloonHelpString("Average value of the coefficient B in the element.");  
  this->ViscoElasticityEntry->SetBalloonHelpString("Viscoelasticity of the artery wall in the element."); 
  this->ViscoElasticityExpEntry->SetBalloonHelpString("Viscoelasticity expoent of the artery wall in the element."); 
  this->InfPressureEntry->SetBalloonHelpString("Infiltration pressure in the element."); 
  this->RefPressureEntry->SetBalloonHelpString("Reference pressure in the element."); 
  this->PermeabilityEntry->SetBalloonHelpString("Permeability coefficient in the element ");   
  
  this->UpdateSingleElemPropButton->SetBalloonHelpString("Updates elements properties from current selected segments");
  this->UpdateWholeSegPropButton->SetBalloonHelpString("Updates elements properties from whole 1D tree");
}

//----------------------------------------------------------------------------
  
vtkKWFrameWithLabel* vtkPVHMMultipleSegmentWidget::GetElementFrame()
{
	return this->GetChildFrame();
}
	
//----------------------------------------------------------------------------
void vtkPVHMMultipleSegmentWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleSegmentWidget::Accept()
{
}

//----------------------------------------------------------------------------


// get methods
//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetElastin()
{
	return this->ElastinEntry->GetValueAsDouble();	
}
	
//----------------------------------------------------------------------------	
double vtkPVHMMultipleSegmentWidget::GetCollagen()
{
	return this->CollagenEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetCoefficientA()
{
	return this->CoefficientAEntry->GetValueAsDouble(); 	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetCoefficientB()
{
	return this->CoefficientBEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetViscoelasticity()
{
	return this->ViscoElasticityEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetViscoelasticityExponent()
{
	return this->ViscoElasticityExpEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetInfiltrationPressure()
{
	return this->InfPressureEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetReferencePressure()
{
	return this->RefPressureEntry->GetValueAsDouble();		
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleSegmentWidget::GetPermeability()
{
	return this->PermeabilityEntry->GetValueAsDouble();
}


//----------------------------------------------------------------------------


vtkDoubleArray *vtkPVHMMultipleSegmentWidget::GetPropFactorDoubleArray(bool UpdateMethod)
{
	
	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfValues(11);
	
	for (int i=0; i<11 ; i++)
		{
			array->SetValue(i, -1);
		}
	
	// se usuario nao editou valor envio valor -1 
	//para indicar que este valor de propriedade nao deve ser modificado

	if (!strlen(this->ElastinEntry->GetValue()))	
		array->SetValue(0, -1);
	else
		array->SetValue(0, this->GetElastin()); 
				
	if (!strlen(this->CollagenEntry->GetValue()))	
		array->SetValue(1, -1);
	else
		array->SetValue(1, this->GetCollagen());

	if (!strlen(this->CoefficientAEntry->GetValue()))
		array->SetValue(2, -1);
	else
		array->SetValue(2, this->GetCoefficientA());

	if (!strlen(this->CoefficientBEntry->GetValue()))	
		array->SetValue(3, -1);
	else
		array->SetValue(3, this->GetCoefficientB());

	if (!strlen(this->ViscoElasticityEntry->GetValue()))	
		array->SetValue(4, -1);
	else
		array->SetValue(4, this->GetViscoelasticity());

	if (!strlen(this->ViscoElasticityExpEntry->GetValue()))	
		array->SetValue(5, -1);
	else
		array->SetValue(5, this->GetViscoelasticityExponent());	
	
	if (!strlen(this->InfPressureEntry->GetValue()))	
		array->SetValue(6, -1);
	else
		array->SetValue(6, this->GetInfiltrationPressure());	
		
	if (!strlen(this->RefPressureEntry->GetValue()))	
		array->SetValue(7, -1);
	else
		array->SetValue(7, this->GetReferencePressure());	

	if (!strlen(this->PermeabilityEntry->GetValue()))	
		array->SetValue(8, -1);
	else
		array->SetValue(8, this->GetPermeability());	

	if (!strlen(this->NodesRadiusEntry->GetValue()))	
		array->SetValue(9, -1);
	else
		array->SetValue(9, this->NodesRadiusEntry->GetValueAsDouble());	

	if (!strlen(this->NodesWallEntry->GetValue()))	
		array->SetValue(10, -1);
	else
		array->SetValue(10, this->NodesWallEntry->GetValueAsDouble());	
	
	// este "for" inicializa os valores não especificados do doubleArray em 1;
	// caso o usuario nao especifique nenhum valor para 
	// uma constante multiplicativa o valor da propriedade será mantido
	
  if (UpdateMethod) // se valores forem atualizados por fator multiplicativo
  	{
		for (int i=0; i<11 ; i++)
			{
			if (array->GetValue(i) == -1)
				array->SetValue(i, 1);
			}
  	}
  
	return array;
		
}

//----------------------------------------------------------------------------

bool vtkPVHMMultipleSegmentWidget::GetPropUpdateMethod()
{
	if (!strcmp (this->UpdateMethodComboBox->GetValue(), "Multiplicative factor"))
			return true;
	else
		return false;	
	
}	
	
//----------------------------------------------------------------------------

