
// .NAME vtkPVHMMultipleSegmentWidget
// .SECTION Description
// Creates KW Components for Segment Elements


#ifndef _vtkPVHMMultipleSegmentWidget_h_
#define _vtkPVHMMultipleSegmentWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"
#include "vtkDoubleArray.h"
#include "vtkKWComboBox.h"


class vtkKWEntry;
class vtkKWLabel;
class vtkSMSourceProxy;
class vtkKWFrameWithLabel;
class vtkPVHMStraightModelWidget;


class VTK_EXPORT vtkPVHMMultipleSegmentWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMMultipleSegmentWidget *New();
  vtkTypeRevisionMacro(vtkPVHMMultipleSegmentWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();
  
  // Description:
  // get values from text entries
	double GetElastin();
  double GetCollagen();
  double GetCoefficientA();
  double GetCoefficientB();
  double GetViscoelasticity();
  double GetViscoelasticityExponent();
  double GetInfiltrationPressure();
  double GetReferencePressure();
  double GetPermeability();
 
 	// Description:
 	// returns true if properties should be updated by a multiplicative factor; if values are specified, returns false 
  bool GetPropUpdateMethod();
  
  // Description:
  // returns a vtk double array (size 9) containing the values (multiplicative factor or segment property values) 
  // which the segments should be updated.  
  vtkDoubleArray *GetPropFactorDoubleArray(bool UpdateMethod);
  
  
  //Get methods for the Push Buttons 
  vtkGetObjectMacro(UpdateSingleElemPropButton, vtkKWPushButton);
  vtkGetObjectMacro(UpdateWholeSegPropButton, vtkKWPushButton);
  
//********************************************************************** 
//********************************************************************** 
//********************************************************************** 
    
protected:
  
  vtkPVHMMultipleSegmentWidget();
  virtual ~vtkPVHMMultipleSegmentWidget();

  vtkKWPushButton *UpdateSingleElemPropButton; 
  
  vtkKWPushButton *UpdateWholeSegPropButton;

	vtkKWLabel *PropertyLabel;

  vtkKWLabel* NodesRadiusLabel;
  
  vtkKWEntry* NodesRadiusEntry;  
  
  vtkKWLabel* NodesWallLabel;
  
  vtkKWEntry* NodesWallEntry;  
  
  vtkKWLabel* ElastinLabel;

  vtkKWEntry* ElastinEntry;  

  vtkKWLabel* CollagenLabel;

  vtkKWEntry* CollagenEntry;  

  vtkKWLabel* CoefficientALabel;

  vtkKWEntry* CoefficientAEntry;  

  vtkKWLabel* CoefficientBLabel;

  vtkKWEntry* CoefficientBEntry;  

  vtkKWLabel* ViscoElasticityLabel;

  vtkKWEntry* ViscoElasticityEntry; 

  vtkKWLabel* ViscoElasticityExpLabel;

  vtkKWEntry* ViscoElasticityExpEntry; 

  vtkKWLabel* InfPressureLabel;

  vtkKWEntry* InfPressureEntry; 

  vtkKWLabel* RefPressureLabel;

  vtkKWEntry* RefPressureEntry; 

  vtkKWLabel* PermeabilityLabel;

  vtkKWEntry* PermeabilityEntry;    
  
	vtkKWComboBox *UpdateMethodComboBox;
  
private:  
  vtkPVHMMultipleSegmentWidget(const vtkPVHMMultipleSegmentWidget&); // Not implemented
  void operator=(const vtkPVHMMultipleSegmentWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMMultipleSegmentWidget_h_*/


