/*
 * $Id: vtkPVHMMultipleTerminalWidget.cxx 340 2006-05-12 13:40:37Z ziemer $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkPVHMMultipleTerminalWidget.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMDefineVariables.h"


vtkCxxRevisionMacro(vtkPVHMMultipleTerminalWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMMultipleTerminalWidget);

//----------------------------------------------------------------------------
vtkPVHMMultipleTerminalWidget::vtkPVHMMultipleTerminalWidget()
{ 
  
  this->PropertyLabel				= vtkKWLabel::New();

  this->R1Label				= vtkKWLabel::New();
  this->R1Entry 				= vtkKWEntry::New();
  this->R2Label			= vtkKWLabel::New();
  this->R2Entry 			= vtkKWEntry::New();
  this->CapLabel			= vtkKWLabel::New();
  this->CapEntry			= vtkKWEntry::New();
  this->PressureLabel			= vtkKWLabel::New();
  this->PressureEntry 			= vtkKWEntry::New();   
  
  this->UpdateSingleTermPropButton	= vtkKWPushButton::New();
  
//  this->UpdateWholeTermPropButton = vtkKWPushButton::New();
  
  this->UpdateMethodComboBox = vtkKWComboBox::New();
}

//----------------------------------------------------------------------------
vtkPVHMMultipleTerminalWidget::~vtkPVHMMultipleTerminalWidget()
{
  this->PropertyLabel->Delete();
  
  this->R1Label->Delete();
  this->R1Entry->Delete();
  this->R2Label->Delete();
  this->R2Entry->Delete();
  this->CapLabel->Delete();
  this->CapEntry->Delete();
  this->PressureLabel->Delete();
  this->PressureEntry->Delete(); 

  this->UpdateSingleTermPropButton->Delete(); 

//  this->UpdateWholeTermPropButton->Delete();

  this->UpdateMethodComboBox->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleTerminalWidget::UpdateWidgetInfo(	)
{
  int mode = this->GetReadOnly();
  // Se mode de destaque de sub-arvore, habilita modo de edição de multiplos segmentos
  if((this->GetReadOnly()==3))
  	{
  	mode = 0;
  	}
  this->R1Entry->SetReadOnly(mode);
	this->R2Entry->SetReadOnly(mode);	  
	this->CapEntry->SetReadOnly(mode);	  
	this->PressureEntry->SetReadOnly(mode);	  
}
	
//----------------------------------------------------------------------------
void vtkPVHMMultipleTerminalWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Multiple Terminal Edition Mode"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
  
  this->PropertyLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PropertyLabel->Create(pvApp);
  this->PropertyLabel->SetText("Property Update Method");
  
  this->UpdateMethodComboBox->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateMethodComboBox->Create(pvApp);
  this->UpdateMethodComboBox->SetBalloonHelpString("Select if terminals should be updated by a fixed value or a multiplicative factor");
  const char *UpdateMethod[] = {"Fixed property value", "Multiplicative factor"};
  int i;
  for (i = 0; i < 2; i++)
	this->UpdateMethodComboBox->AddValue(UpdateMethod[i]);
	
  this->UpdateMethodComboBox->SetValue("Multiplicative factor");
  
  this->UpdateMethodComboBox->ReadOnlyOn();
	this->UpdateMethodComboBox->SetWidth(20);

  this->R1Label->SetParent(this->GetChildFrame()->GetFrame());
  this->R1Label->Create(pvApp);
  this->R1Label->SetText("Resistance 1 [dyn.sec/cm^5]:");
  this->R1Entry->SetParent(this->GetChildFrame()->GetFrame());
  this->R1Entry->Create(pvApp);
  this->R1Entry->SetWidth(6);
 
  this->R2Label->SetParent(this->GetChildFrame()->GetFrame());
  this->R2Label->Create(pvApp);
  this->R2Label->SetText("Resistance 2 [dyn.sec/cm^5]:");
	this->R2Entry->SetParent(this->GetChildFrame()->GetFrame());
  this->R2Entry->Create(pvApp);
  this->R2Entry->SetWidth(6);
 
  this->CapLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->CapLabel->Create(pvApp);
  this->CapLabel->SetText("Capacitance [cm^5/dyn]:");
	this->CapEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->CapEntry->Create(pvApp);
  this->CapEntry->SetWidth(6);
 
  this->PressureLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->PressureLabel->Create(pvApp);
  this->PressureLabel->SetText("Pressure [dym/cm^2]:");
	this->PressureEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->PressureEntry->Create(pvApp);
  this->PressureEntry->SetWidth(6);
  
  this->UpdateSingleTermPropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateSingleTermPropButton->Create(pvApp);
  this->UpdateSingleTermPropButton->SetText("Update Selected Terminals");
  

//  this->UpdateWholeTermPropButton->SetParent(this->GetChildFrame()->GetFrame());
//  this->UpdateWholeTermPropButton->Create(pvApp);
//  this->UpdateWholeTermPropButton->SetText("Update Tree");
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleTerminalWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{		
	int mode = mainWidget->GetWidgetMode();
	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		mainWidget->Script("grid forget %s",
    this->GetChildFrame()->GetWidgetName());			
		return;
		} 
	this->GetChildFrame()->ExpandFrame();				
	
	mainWidget->Script("grid %s -sticky ew -columnspan 4",
  this->GetChildFrame()->GetWidgetName());  
      
  
	mainWidget->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->PropertyLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->UpdateMethodComboBox->GetWidgetName());

	mainWidget->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->R1Label->GetWidgetName());
  mainWidget->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->R1Entry->GetWidgetName());

	mainWidget->Script("grid %s -row 2 -column 0 -padx 2 -pady 2", this->R2Label->GetWidgetName());
  mainWidget->Script("grid %s -row 2 -column 1 -padx 2 -pady 2", this->R2Entry->GetWidgetName());

	mainWidget->Script("grid %s -row 3 -column 0 -padx 2 -pady 2", this->CapLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 3 -column 1 -padx 2 -pady 2", this->CapEntry->GetWidgetName());
  
	mainWidget->Script("grid %s -row 4 -column 0 -padx 2 -pady 2", this->PressureLabel->GetWidgetName());
  mainWidget->Script("grid %s -row 4 -column 1 -padx 2 -pady 2", this->PressureEntry->GetWidgetName());
  
//// Se modo de visualização, não aparece botoes de update para multiplos segmentos.
//	if(mode == MODE_VISUALIZATION_VAL)
//		{
//		mainWidget->Script("grid remove %s %s",
//		this->UpdateSingleTermPropButton->GetWidgetName(),
//		this->UpdateWholeTermPropButton->GetWidgetName());
//		}
//	// Se modo de edição, aparece botoes de update para multiplos segmentos.	
//	else if (mode == MODE_EDITION_VAL)
//		{
//		mainWidget->Script("grid %s %s - - -sticky w", 
//		this->UpdateSingleTermPropButton->GetWidgetName(),
//		this->UpdateWholeTermPropButton->GetWidgetName());
//		}
	// Se modo de destaque de sub-arvore aparece o botão de update de segmentos selecionados
//	else if (mode == MODE_TODETACHSUBTREE_VAL)
	if (mode == MODE_TODETACHSUBTREE_VAL)
		{
		mainWidget->Script("grid %s - - -sticky w", 
		this->UpdateSingleTermPropButton->GetWidgetName());
		} 

	this->R1Entry->SetBalloonHelpString("Resistive element R1 from the Windkessel model.");  
  this->R2Entry->SetBalloonHelpString("Resistive element R2 from the Windkessel model.");  
  this->CapEntry->SetBalloonHelpString("Capacitive element C from the Windkessel model.");  
  this->PressureEntry->SetBalloonHelpString("Terminal pressure Pt.");  
  
  this->UpdateSingleTermPropButton->SetBalloonHelpString("Updates terminal propertie from current selected terminals");
//  this->UpdateWholeTermPropButton->SetBalloonHelpString("Updates elements properties from whole 1D tree");
}

//----------------------------------------------------------------------------
  
vtkKWFrameWithLabel* vtkPVHMMultipleTerminalWidget::GetElementFrame()
{
	return this->GetChildFrame();
}
	
//----------------------------------------------------------------------------
void vtkPVHMMultipleTerminalWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMMultipleTerminalWidget::Accept()
{
}

// get methods
//----------------------------------------------------------------------------
double vtkPVHMMultipleTerminalWidget::GetR1()
{
	return this->R1Entry->GetValueAsDouble();
}
	
//----------------------------------------------------------------------------	
double vtkPVHMMultipleTerminalWidget::GetR2()
{
	return this->R2Entry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleTerminalWidget::GetCap()
{
	return this->CapEntry->GetValueAsDouble(); 	
}

//----------------------------------------------------------------------------
double vtkPVHMMultipleTerminalWidget::GetPressure()
{
	return this->PressureEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------


vtkDoubleArray *vtkPVHMMultipleTerminalWidget::GetPropFactorDoubleArray(bool UpdateMethod)
{
	
	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfValues(11);
	
	// se usuario nao editou valor e metodo de atualizacao é valor fixo
	// envio valor -1 para indicar que este valor de propriedade nao deve ser
	// modificado
	if (!strlen(this->R1Entry->GetValue()) && (!UpdateMethod))	
		array->SetValue(0, -1);
	else if (!strlen(this->R1Entry->GetValue()))	
		array->SetValue(0, 1);
	else
		array->SetValue(0, this->GetR1());
						
	if (!strlen(this->R2Entry->GetValue()) && (!UpdateMethod))	
		array->SetValue(1, -1);
	else if (!strlen(this->R2Entry->GetValue()))
		array->SetValue(1, 1);
	else
		array->SetValue(1, this->GetR2());

	if (!strlen(this->CapEntry->GetValue()) && (!UpdateMethod))	
		array->SetValue(2, -1);
	else if (!strlen(this->CapEntry->GetValue()))
		array->SetValue(2, 1);
	else
		array->SetValue(2, this->GetCap());

	if (!strlen(this->PressureEntry->GetValue()) && (!UpdateMethod))	
		array->SetValue(3, -1);
	else if (!strlen(this->PressureEntry->GetValue()))
		array->SetValue(3, 1);
	else
		array->SetValue(3, this->GetPressure());

	return array;
}

//----------------------------------------------------------------------------

bool vtkPVHMMultipleTerminalWidget::GetPropUpdateMethod()
{
	if (!strcmp (this->UpdateMethodComboBox->GetValue(), "Multiplicative factor"))
			return true;
	else
		return false;	
	
}	
	
//----------------------------------------------------------------------------

