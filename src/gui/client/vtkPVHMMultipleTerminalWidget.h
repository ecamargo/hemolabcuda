
// .NAME vtkPVHMMultipleTerminalWidget
// .SECTION Description
// Creates KW Components for Terminals


#ifndef _vtkPVHMMultipleTerminalWidget_h_
#define _vtkPVHMMultipleTerminalWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"
#include "vtkDoubleArray.h"
#include "vtkKWComboBox.h"


class vtkKWEntry;
class vtkKWLabel;
class vtkSMSourceProxy;
class vtkKWFrameWithLabel;
class vtkPVHMStraightModelWidget;


class VTK_EXPORT vtkPVHMMultipleTerminalWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMMultipleTerminalWidget *New();
  vtkTypeRevisionMacro(vtkPVHMMultipleTerminalWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();
  
  // Description:
  // get values from text entries
	double GetR1();
  double GetR2();
  double GetCap();
  double GetPressure();
 
 	// Description:
 	// returns true if properties should be updated by a multiplicative factor; if values are specified, returns false 
  bool GetPropUpdateMethod();
  
  // Description:
  // returns a vtk double array (size 9) containing the values (multiplicative factor or segment property values) 
  // which the segments should be updated.  
  vtkDoubleArray *GetPropFactorDoubleArray(bool UpdateMethod);
  
  
  //Get methods for the Push Buttons 
  vtkGetObjectMacro(UpdateSingleTermPropButton, vtkKWPushButton);
//  vtkGetObjectMacro(UpdateWholeSegPropButton, vtkKWPushButton);
  
//********************************************************************** 
//********************************************************************** 
//********************************************************************** 
    
protected:
  
  vtkPVHMMultipleTerminalWidget();
  virtual ~vtkPVHMMultipleTerminalWidget();

  vtkKWPushButton *UpdateSingleTermPropButton; 
  
//  vtkKWPushButton *UpdateWholeSegPropButton;

	vtkKWLabel *PropertyLabel;

  vtkKWLabel* R1Label;

  vtkKWEntry* R1Entry;  

  vtkKWLabel* R2Label;

  vtkKWEntry* R2Entry;  

  vtkKWLabel* CapLabel;

  vtkKWEntry* CapEntry;  

  vtkKWLabel* PressureLabel;

  vtkKWEntry* PressureEntry;  

	vtkKWComboBox *UpdateMethodComboBox;

private:  
  vtkPVHMMultipleTerminalWidget(const vtkPVHMMultipleTerminalWidget&); // Not implemented
  void operator=(const vtkPVHMMultipleTerminalWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMMultipleTerminalWidget_h_*/


