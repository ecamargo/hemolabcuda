/*
 * $Id: vtkPVHMNodeWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"

#include "vtkPVHMNodeWidget.h"

#include "vtkPVHMStraightModelWidget.h"

vtkCxxRevisionMacro(vtkPVHMNodeWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMNodeWidget);

//----------------------------------------------------------------------------
vtkPVHMNodeWidget::vtkPVHMNodeWidget()
{
  this->RadiusLabel			= vtkKWLabel::New();
  this->RadiusEntry 		= vtkKWEntry::New();
  this->WallThicknessLabel			= vtkKWLabel::New();
  this->WallThicknessEntry 			= vtkKWEntry::New();
  this->FrameNodeName = "Current Node Information:";
  this->UpdateSingleNodePropButton = vtkKWPushButton::New();
   
  
  this->meshIDLabel = vtkKWLabel::New();
  this->meshIDEntry 	= vtkKWEntry::New();

  this->coordsLabel = vtkKWLabel::New();

  this->coordsXEntry 	= vtkKWEntry::New();  

  this->coordsYEntry 	= vtkKWEntry::New();  

  this->coordsZEntry 	= vtkKWEntry::New();  
  
}

//----------------------------------------------------------------------------
vtkPVHMNodeWidget::~vtkPVHMNodeWidget()
{
  this->RadiusLabel->Delete();
  this->RadiusEntry->Delete();  
  this->WallThicknessLabel->Delete();
  this->WallThicknessEntry->Delete();
  this->UpdateSingleNodePropButton->Delete();


  this->meshIDLabel->Delete();
  this->meshIDEntry->Delete();

  this->coordsLabel->Delete();

  this->coordsXEntry->Delete();  

  this->coordsYEntry->Delete();  

  this->coordsZEntry->Delete();  

}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::UpdateWidgetInfo()
{
  int mode = this->GetReadOnly();

  this->RadiusEntry->SetReadOnly(mode);	  
  this->WallThicknessEntry->SetReadOnly(mode);
}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText(this->FrameNodeName); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  
             
  this->WallThicknessLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->WallThicknessLabel->Create(pvApp);
  this->WallThicknessLabel->SetText("Wall Thickness [cm]:");
  this->WallThicknessEntry->SetParent(this->GetFrame());
  this->WallThicknessEntry->SetWidth(10);
  this->WallThicknessEntry->Create(pvApp);

  this->RadiusLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->RadiusLabel->Create(pvApp);
  this->RadiusLabel->SetText("Radius [cm]:");
  this->RadiusEntry->SetParent(this->GetFrame());
  this->RadiusEntry->SetWidth(10);
  this->RadiusEntry->Create(pvApp);
  
  this->meshIDLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->meshIDLabel->Create(pvApp);
  this->meshIDLabel->SetText("Mesh ID:");
  this->meshIDEntry->SetParent(this->GetFrame());
  this->meshIDEntry->SetWidth(10);
  this->meshIDEntry->Create(pvApp);
  this->meshIDEntry->SetReadOnly(1);
  
  this->coordsLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->coordsLabel->Create(pvApp);
  this->coordsLabel->SetText("Coordinates (X,Y,Z)");
  

  this->coordsXEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->coordsXEntry->SetWidth(5);
  this->coordsXEntry->Create(pvApp);


  this->coordsYEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->coordsYEntry->SetWidth(5);
  this->coordsYEntry->Create(pvApp);


  this->coordsZEntry->SetParent(this->GetChildFrame()->GetFrame());
  this->coordsZEntry->SetWidth(5);
  this->coordsZEntry->Create(pvApp);




  this->UpdateSingleNodePropButton->SetParent(this->GetChildFrame()->GetFrame());
  this->UpdateSingleNodePropButton->Create(pvApp);
  this->UpdateSingleNodePropButton->SetText("Update Node");
  this->UpdateSingleNodePropButton->SetBalloonHelpString("Updates only this node from the current segment.");
 


}


//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
	
	int mode = mainWidget->GetWidgetMode();
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		  mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
			return;
		} 
	this->GetChildFrame()->ExpandFrame();				
	
  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName()); 	
 
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->WallThicknessLabel->GetWidgetName(),
    this->WallThicknessEntry->GetWidgetName());
   
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->RadiusLabel->GetWidgetName(),
    this->RadiusEntry->GetWidgetName());
    
    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->meshIDLabel->GetWidgetName(),
    this->meshIDEntry->GetWidgetName());


  mainWidget->Script("grid %s - - - -sticky w",     
    this->coordsLabel->GetWidgetName());

//  mainWidget->Script("grid %s %s %s - - - -sticky w",     
//    this->coordsXEntry->GetWidgetName(),
//    this->coordsYEntry->GetWidgetName(),
//    this->coordsZEntry->GetWidgetName());    
//    
       
     
   if(mode)
  	mainWidget->Script("grid remove %s",
       this->UpdateSingleNodePropButton->GetWidgetName());
    else
    mainWidget->Script("grid %s - - -sticky w", 
   this->UpdateSingleNodePropButton->GetWidgetName());  
 // mainWidget->Script("grid %s - - -sticky w",     
  //  this->UpdateSingleNodePropButton->GetWidgetName());
    
    
  mainWidget->Script("grid %s -sticky ew -columnspan 3",
    this->GetChildFrame()->GetWidgetName()); 
     
  this->RadiusEntry->SetBalloonHelpString("Artery radius at this point."); 
  this->WallThicknessEntry->SetBalloonHelpString("Artery wall thickness at this point"); 
}

//----------------------------------------------------------------------------
vtkKWFrameWithLabel* vtkPVHMNodeWidget::GetNodeFrame()
{
	return this->GetChildFrame();
}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::Accept()
{
}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::SetRadius(double Radius)
{
	this->RadiusEntry->SetValueAsDouble(Radius);	
}

//----------------------------------------------------------------------------
void vtkPVHMNodeWidget::SetWallThickness(double wall)
{
	this->WallThicknessEntry->SetValueAsDouble(wall);   	
}

//----------------------------------------------------------------------------
double vtkPVHMNodeWidget::GetRadius(void)
{
	return this->RadiusEntry->GetValueAsDouble();	
}

//----------------------------------------------------------------------------

double vtkPVHMNodeWidget::GetWallThickness(void)
{
	return this->WallThicknessEntry->GetValueAsDouble();   	
}

//----------------------------------------------------------------------------

void vtkPVHMNodeWidget::SetNodeCoordinates(char *text)
{
	  this->coordsLabel->SetText(text);
}

//----------------------------------------------------------------------------


void vtkPVHMNodeWidget::SetNodeLabelMeshID(const char *id)
{
  	this->meshIDEntry->SetValue(id);
  	
}

//----------------------------------------------------------------------------

