/*
 * $Id: vtkPVHMNodeWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMNodeWidget
// .SECTION Description
// Creates KW Components for nodes

#ifndef _vtkPVHMNodeWidget_h_
#define _vtkPVHMNodeWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"

class vtkKWEntry;
class vtkKWLabel;
class vtkSMSourceProxy;
class vtkPVHM1DStraightModelSourceWidget;
class vtkPVHMStraightModelWidget;

class VTK_EXPORT vtkPVHMNodeWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMNodeWidget *New();
  vtkTypeRevisionMacro(vtkPVHMNodeWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
 	// Call TCL/TK commands to place KW Components
  void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Node Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetNodeFrame();
  
  // Description:
  // Set/Get the TerminalProxy
  void  SetFrameWidgetName(char *name) { FrameNodeName = name; };
  const char* GetFrameWidgetName() 					 { return FrameNodeName; };    
  
  // Description:
  // set the values of the nodes widgets
  void SetRadius(double Radius);
  void SetWallThickness(double wall);  
  
  double GetRadius(void);
  double GetWallThickness(void);

  
  void SetNodeCoordinates(char *text);
  void SetNodeLabelMeshID(const char *id);
  
protected:
  vtkPVHMNodeWidget();
  virtual ~vtkPVHMNodeWidget();
    
  const char *FrameNodeName;
	// Description:
	// Node Widget
	// Area, Radius, alfa and Resolution
  vtkKWLabel* RadiusLabel;
  vtkKWEntry* RadiusEntry;  

  vtkKWLabel* WallThicknessLabel;
  vtkKWEntry* WallThicknessEntry;


  vtkKWLabel* meshIDLabel;
  vtkKWEntry* meshIDEntry;  

  vtkKWLabel* coordsLabel;

  vtkKWEntry* coordsXEntry;  

  vtkKWEntry* coordsYEntry;  

  vtkKWEntry* coordsZEntry;  

  

  
public:  
  vtkKWPushButton *UpdateSingleNodePropButton;  
 
private:  
  vtkPVHMNodeWidget(const vtkPVHMNodeWidget&); // Not implemented
  void operator=(const vtkPVHMNodeWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMNodeWidget_h_*/


