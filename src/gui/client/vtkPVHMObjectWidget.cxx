/*
 * $Id: vtkPVHMObjectWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"

#include "vtkPVHMObjectWidget.h"
#include "vtkPVHMStraightModelWidget.h"

vtkCxxRevisionMacro(vtkPVHMObjectWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMObjectWidget);

//----------------------------------------------------------------------------
vtkPVHMObjectWidget::vtkPVHMObjectWidget()
{
  this->ChildFrame = vtkKWFrameWithLabel::New();
}

//----------------------------------------------------------------------------
vtkPVHMObjectWidget::~vtkPVHMObjectWidget()
{
  this->ChildFrame->Delete();	
	this->WidgetProxy = NULL;  
}

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::UpdateWidgetInfo(	)
{
}

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::ChildCreate(vtkPVApplication* pvApp)
{
}

	
//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::ConfigureComponents(vtkPVHMStraightModelWidget* mainWidget)
{
  mainWidget->Script("grid columnconfigure %s 1 -weight 2", 
     this->GetChildFrame()->GetWidgetName()); 
}
	

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::SaveInBatchScript(ofstream *file)
{
}

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::Accept()
{
}

//----------------------------------------------------------------------------
void vtkPVHMObjectWidget::SetWidgetProxy(vtkSM3DWidgetProxy * 	wExternal)
{
	this->WidgetProxy = wExternal;
}
 
