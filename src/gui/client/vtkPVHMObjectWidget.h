/*
 * $Id: vtkPVHMObjectWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMObjectWidget
// .SECTION Description
// Creates Common KW Components for all HM widgets (nodes, terminals, segments and elements)tkPVHMObjectWidget_h_

#ifndef _vtkPVHMObjectWidget_h_
#define _vtkPVHMObjectWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPV3DWidget.h"
//#include "vtkSMHM1DStraightModelWidgetProxy.h"

class vtkKWLabel;
class vtkKWFrameWithLabel;
class vtkSMSourceProxy;
class vtkSMHM1DTerminalProxy;
class vtkSMHM1DSegmentProxy;
class vtkPVHMStraightModelWidget;
class vtkPVHM1DStraightModelSourceWidget;

class VTK_EXPORT vtkPVHMObjectWidget : public vtkPV3DWidget
{
public:
  static vtkPVHMObjectWidget *New();
  vtkTypeRevisionMacro(vtkPVHMObjectWidget,vtkPV3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	void ConfigureComponents(vtkPVHMStraightModelWidget *);
  
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);
  
  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Set/Get main frame component.  
  void SetFrame(vtkKWFrame *f) {Frame = f;}
  vtkKWFrame* GetFrame() { return this->Frame;}
  
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	

  // Description:
  //Get ChildFrame
  vtkKWFrameWithLabel* GetChildFrame() { return this->ChildFrame;}
   
  void SetWidgetProxy(vtkSM3DWidgetProxy * 	WidgetProxy);
  
protected:
  vtkPVHMObjectWidget();
  virtual ~vtkPVHMObjectWidget();

  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;
  
  // Description:
  // Main Frame
  vtkKWFrame *Frame;	

  // Description:
  // Main Frame  
  vtkKWFrameWithLabel *ChildFrame; 
 
private:  
  vtkPVHMObjectWidget(const vtkPVHMObjectWidget&); // Not implemented
  void operator=(const vtkPVHMObjectWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMObjectWidget_h_*/

