 /* 
 * $Id: vtkPVHMPlotWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */
#include "vtkIntArray.h"
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"

#include "vtkPVHMPlotWidget.h"

#include "vtkPVHMStraightModelWidget.h"

vtkCxxRevisionMacro(vtkPVHMPlotWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMPlotWidget);

//----------------------------------------------------------------------------
vtkPVHMPlotWidget::vtkPVHMPlotWidget()
{ 
	//	RadioButtons
	this->AnimatedPlotTypeRadio 	= vtkKWRadioButton::New();
	this->NormalPlotTypeRadio 	= vtkKWRadioButton::New();

	this->AnimatedPlotRadioSet 	= vtkKWRadioButtonSet::New();

//	Nao pode dar new nos botoes que forem ser adicionados no set.
//	this->Pressure1PlotRadio 	= vtkKWRadioButton::New();
//	this->Pressure2PlotRadio 	= vtkKWRadioButton::New();
//		
//	this->AreaPlotRadio 			= vtkKWRadioButton::New();
//	this->FlowPlotRadio 			= vtkKWRadioButton::New();
//	
//	this->Velocity1PlotRadio	= vtkKWRadioButton::New();   
//	this->Velocity2PlotRadio	= vtkKWRadioButton::New();   

//	CheckButons
  this->Pressure1Plot 	= vtkKWCheckButton::New();
  this->Pressure2Plot 	= vtkKWCheckButton::New();
    
  this->AreaPlot 			= vtkKWCheckButton::New();

  this->FlowPlot 			= vtkKWCheckButton::New();
  
  this->Velocity1Plot	= vtkKWCheckButton::New();   
  this->Velocity2Plot	= vtkKWCheckButton::New();   
  
//  frames for check buttons
  this->PressureFrame 	=  vtkKWFrameWithLabel::New();
  this->AreaFrame 	=  vtkKWFrameWithLabel::New();
  this->FlowFrame 	=  vtkKWFrameWithLabel::New();
  this->VelocityFrame 	=  vtkKWFrameWithLabel::New();
  this->NumberFrame = vtkKWFrameWithLabel::New();
  
  this->AnimatedPlotFrame = vtkKWFrameWithLabel::New();
  this->NormalPlotFrame = vtkKWFrameWithLabel::New();
  
  
  this->NumberCombo = vtkKWComboBox::New();
  this->GraphsUpdateButton = vtkKWPushButton::New();
  this->ExportResultButton = vtkKWPushButton::New();
  
}

//----------------------------------------------------------------------------
vtkPVHMPlotWidget::~vtkPVHMPlotWidget()
{
	this->AnimatedPlotTypeRadio->Delete();
	this->NormalPlotTypeRadio->Delete();
		
//	this->Pressure1PlotRadio->Delete();
//	this->Pressure2PlotRadio->Delete();
//	
//	this->AreaPlotRadio->Delete();
//	this->FlowPlotRadio->Delete();
//
//	this->Velocity1PlotRadio->Delete();   
//	this->Velocity2PlotRadio->Delete();  
	
	this->AnimatedPlotRadioSet->Delete();
	
	
	this->Pressure1Plot->Delete();
  this->Pressure2Plot->Delete();
  
  this->AreaPlot->Delete();
  this->FlowPlot->Delete();
  
  this->Velocity1Plot->Delete();
  this->Velocity2Plot->Delete();
  
  this->PressureFrame->Delete();
  this->AreaFrame->Delete();
  this->FlowFrame->Delete();
  this->VelocityFrame->Delete();
  this->NumberFrame->Delete();
  
  this->AnimatedPlotFrame->Delete();
  this->NormalPlotFrame->Delete();
  

  this->NumberCombo->Delete();
  this->GraphsUpdateButton->Delete();
  this->ExportResultButton->Delete();
  
}

//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::UpdateWidgetInfo(	)
{
}
	
	
//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::ChildCreate(vtkPVApplication* pvApp)
{
	this->GetChildFrame()->SetLabelText("Plot Options"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);  

//  Change Plot Type Normal/Animated
	this->NormalPlotTypeRadio->SetParent(this->GetChildFrame()->GetFrame());
  this->NormalPlotTypeRadio->Create(pvApp);
  this->NormalPlotTypeRadio->SetText("Normal Plot Mode");
  this->NormalPlotTypeRadio->SetValueAsInt(0);
//  this->NormalPlotTypeRadio->SetCommand(this, "NormalPlotMode 1"); está setado no straightmodel
  this->NormalPlotTypeRadio->SetBalloonHelpString("Enable Normal Plot.");
    
  this->AnimatedPlotTypeRadio->SetParent(this->GetChildFrame()->GetFrame());
  this->AnimatedPlotTypeRadio->Create(pvApp);
  this->AnimatedPlotTypeRadio->SetText("Animated Plot Mode");
  this->AnimatedPlotTypeRadio->SetValueAsInt(1);
//  this->AnimatedPlotTypeRadio->SetCommand(this, "AnimatedPlotMode 1"); está setado no straightmodel
  this->AnimatedPlotTypeRadio->SetBalloonHelpString("Enable Animated Plot.");

	this->NormalPlotTypeRadio->SetSelectedState(1);
  this->AnimatedPlotTypeRadio->SetVariableName(this->NormalPlotTypeRadio->GetVariableName());
  
//  Interface to Animated Plot
  this->AnimatedPlotFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->AnimatedPlotFrame->Create(pvApp);
  this->AnimatedPlotFrame->SetLabelText("Animated Plot Options");
  
  this->AnimatedPlotRadioSet->SetParent(this->AnimatedPlotFrame->GetFrame());
  this->AnimatedPlotRadioSet->Create(pvApp);

  this->Pressure1PlotRadio = this->AnimatedPlotRadioSet->AddWidget(0);
	this->Pressure1PlotRadio->SetText("  Pressure [dyn/cm^2]  ");
	this->Pressure1PlotRadio->SetBalloonHelpString("Enable/Disable Pressure Plot in [dyn/cm^2].");
	this->Pressure1PlotRadio->SelectedStateOn();
	
	this->Pressure2PlotRadio = this->AnimatedPlotRadioSet->AddWidget(1);
	this->Pressure2PlotRadio->SetText("  Pressure [mmHg]  ");
	this->Pressure2PlotRadio->SetBalloonHelpString("Enable/Disable Pressure Plot in [mmHg].");
	
	this->AreaPlotRadio = this->AnimatedPlotRadioSet->AddWidget(2);
	this->AreaPlotRadio->SetText("  Area [cm^2]  ");
	this->AreaPlotRadio->SetBalloonHelpString("Enable/Disable Area Plot.");
	
	this->FlowPlotRadio = this->AnimatedPlotRadioSet->AddWidget(3);
	this->FlowPlotRadio->SetText("  Flow rate [cm^3/s]  ");
	this->FlowPlotRadio->SetBalloonHelpString("Enable/Disable Flow rate Plot."); 
	
	this->Velocity1PlotRadio = this->AnimatedPlotRadioSet->AddWidget(4);
	this->Velocity1PlotRadio->SetText("  Velocity [cm/s]  ");   
	this->Velocity1PlotRadio->SetBalloonHelpString("Enable/Disable Velocity Plot in [cm/s]."); 
	
	this->Velocity2PlotRadio = this->AnimatedPlotRadioSet->AddWidget(5);
	this->Velocity2PlotRadio->SetText("  Velocity [m/s]  ");  
	this->Velocity2PlotRadio->SetBalloonHelpString("Enable/Disable Velocity Plot in [m/s].");
  
//  this->AnimatedPlotRadioSet->GetWidget(0)->SetSelectedState(1);
  
//  Interface to Normal Plot
	this->NormalPlotFrame->SetParent(this->GetChildFrame()->GetFrame());
	this->NormalPlotFrame->Create(pvApp);
	this->NormalPlotFrame->SetLabelText("Normal Plot Options");
	
  this->PressureFrame->SetParent(this->NormalPlotFrame->GetFrame());
  this->PressureFrame->Create(pvApp);
  this->PressureFrame->SetLabelText("Pressure Plot Options");
  
  this->AreaFrame->SetParent(this->NormalPlotFrame->GetFrame());
  this->AreaFrame->Create(pvApp);
  this->AreaFrame->SetLabelText("Area Plot");
  
  this->FlowFrame->SetParent(this->NormalPlotFrame->GetFrame());
  this->FlowFrame->Create(pvApp);
  this->FlowFrame->SetLabelText("Flow rate Plot");
  
  this->VelocityFrame->SetParent(this->NormalPlotFrame->GetFrame());
  this->VelocityFrame->Create(pvApp);
  this->VelocityFrame->SetLabelText("Velocity Plot Options");
  

  this->Pressure1Plot->SetParent(this->PressureFrame->GetFrame());
  this->Pressure1Plot->Create(pvApp);
  this->Pressure1Plot->SetText("  Pressure [dyn/cm^2]  ");   
  this->Pressure1Plot->SelectedStateOn();
  
  this->Pressure2Plot->SetParent(this->PressureFrame->GetFrame());
  this->Pressure2Plot->Create(pvApp);
  this->Pressure2Plot->SetText("  Pressure [mmHg]  ");   
  
  
  this->AreaPlot->SetParent(this->AreaFrame->GetFrame());
  this->AreaPlot->Create(pvApp);
  this->AreaPlot->SetText("  Area [cm^2]  ");   

  this->FlowPlot->SetParent(this->FlowFrame->GetFrame());
  this->FlowPlot->Create(pvApp);
  this->FlowPlot->SetText("  Flow rate [cm^3/s]  ");   

  
  this->Velocity1Plot->SetParent(this->VelocityFrame->GetFrame());
  this->Velocity1Plot->Create(pvApp);
  this->Velocity1Plot->SetText("  Velocity [cm/s]  "); 
  
  this->Velocity2Plot->SetParent(this->VelocityFrame->GetFrame());
  this->Velocity2Plot->Create(pvApp);
  this->Velocity2Plot->SetText("  Velocity [m/s]  "); 
  
  this->NumberFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->NumberFrame->Create(pvApp);
  this->NumberFrame->SetLabelText("Number of Plots");
  this->NumberFrame->SetBalloonHelpString("Each time a point is selected to show its output properties, a plot will be generate in the HeMoLab plot window. Here you can define the maximum number of simultaneous output curves shown in the visualization window");
 
  this->NumberCombo->SetParent(this->NumberFrame->GetFrame());
	
  this->NumberCombo->Create(pvApp);
  this->NumberCombo->SetValue("5");
  this->NumberCombo->AddValue("1"); 
  this->NumberCombo->AddValue("2"); 
  this->NumberCombo->AddValue("3");  
  this->NumberCombo->AddValue("4");   
  this->NumberCombo->AddValue("5");
  this->NumberCombo->AddValue("6");    
  this->NumberCombo->AddValue("7");
  
  this->NumberCombo->SetWidth(10);
  this->NumberCombo->SetBalloonHelpString("Select the maximum number of simultaneous output curves (default value is 5)");
 
     
  this->GraphsUpdateButton->SetParent(this->GetChildFrame()->GetFrame());
  this->GraphsUpdateButton->Create(pvApp);
  this->GraphsUpdateButton->SetText("Update");
  this->GraphsUpdateButton->SetBalloonHelpString("Update plot type"); 
  this->GraphsUpdateButton->SetEnabled(0);
  
  this->ExportResultButton->SetParent(this->GetChildFrame()->GetFrame());
  this->ExportResultButton->Create(pvApp);
  this->ExportResultButton->SetEnabled(0);
//  this->ExportResultButton->SetCommand(this, "SetExportDirectory");
  this->ExportResultButton->SetText("Export all results");
  this->ExportResultButton->SetBalloonHelpString("Export 1D simulation data (all unknowns) of all selected nodes");   
  
}

void vtkPVHMPlotWidget::PlotTypeCallbackInternal(vtkIntArray *PlotOptions)
{  
	// seleciona somente o ultimo tipo de pressao selecionado
	if ((!PlotOptions->GetValue(0)) && (Pressure1Plot->GetSelectedState()))  Pressure2Plot->SelectedStateOff();
	if ((!PlotOptions->GetValue(1)) && (Pressure2Plot->GetSelectedState())) Pressure1Plot->SelectedStateOff();
	if ((PlotOptions->GetValue(0)) && (Pressure2Plot->GetSelectedState())) Pressure1Plot->SelectedStateOff();
	

	// seleciona somente o ultimo tipo de velocidade selecionado
	if ((!PlotOptions->GetValue(4)) && (Velocity1Plot->GetSelectedState()))  Velocity2Plot->SelectedStateOff();
	if ((!PlotOptions->GetValue(5)) && (Velocity2Plot->GetSelectedState())) Velocity1Plot->SelectedStateOff();

	
	PlotOptions->SetValue(0, Pressure1Plot->GetSelectedState());
	PlotOptions->SetValue(1, Pressure2Plot->GetSelectedState());
	PlotOptions->SetValue(2, AreaPlot->GetSelectedState());
	PlotOptions->SetValue(3, FlowPlot->GetSelectedState());
	PlotOptions->SetValue(4, Velocity1Plot->GetSelectedState());	
	PlotOptions->SetValue(5, Velocity2Plot->GetSelectedState());	
}

//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{	
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
		mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
			return;
		} 
	this->GetChildFrame()->ExpandFrame();				
	
	mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName());
	
	mainWidget->Script("grid %s %s -sticky w -pady 6",   
		this->NormalPlotTypeRadio->GetWidgetName(),
		this->AnimatedPlotTypeRadio->GetWidgetName());
	
  if(this->NormalPlotTypeRadio->GetSelectedState())
  	this->NormalPlotMode(mainWidget, 1);
  else
  	this->AnimatedPlotMode(mainWidget, 1);
}

  
vtkKWFrameWithLabel* vtkPVHMPlotWidget::GetElementFrame()
{
	return this->GetChildFrame();
}
	
//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::Accept()
{
}
//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::NormalPlotMode(vtkPVHMStraightModelWidget* mainWidget, int show)
{	
	//	Se for verdadeiro adiciona a interface
	if(show)
		{
			this->AnimatedPlotMode(mainWidget, 0);
			
			mainWidget->Script("grid %s -sticky we -columnspan 2",     
				this->NormalPlotFrame->GetWidgetName()); 
			
			mainWidget->Script("grid %s -sticky we -row 0 -column 0 -columnspan 2",     
				this->PressureFrame->GetWidgetName()); 
			
			mainWidget->Script("grid %s -sticky we -row 1 -column 0 -columnspan 2",     
				this->AreaFrame->GetWidgetName());	
			
			mainWidget->Script("grid %s -sticky we -row 2 -column 0 -columnspan 2",     
				this->FlowFrame->GetWidgetName());    	
			
			mainWidget->Script("grid %s -sticky we -row 3 -column 0 -columnspan 2",     
				this->VelocityFrame->GetWidgetName());
			
			//  Opcao de definir o numero maximo de curvas desabilitada - by ziemer 06-09-2007 
			//
			//  MainWidget->Script("grid %s -sticky ew",     
			//	this->NumberFrame->GetWidgetName());
			
			mainWidget->Script("grid %s - - -sticky w",   
				this->Pressure1Plot->GetWidgetName());
				this->Pressure1Plot->SetBalloonHelpString("Enable/Disable Pressure Plot in [dyn/cm^2]."); 
				
				mainWidget->Script("grid %s - - -sticky w",   
			  this->Pressure2Plot->GetWidgetName());
			  this->Pressure2Plot->SetBalloonHelpString("Enable/Disable Pressure Plot in [mmHg]."); 
			
			  mainWidget->Script("grid %s - - -sticky w",
				this->AreaPlot->GetWidgetName());
				this->AreaPlot->SetBalloonHelpString("Enable/Disable Area Plot."); 
			
				mainWidget->Script("grid %s - - -sticky w",
				this->FlowPlot->GetWidgetName());
				this->FlowPlot->SetBalloonHelpString("Enable/Disable Flow Plot."); 
			
				mainWidget->Script("grid %s - - -sticky w",
				this->Velocity1Plot->GetWidgetName());
				this->Velocity1Plot->SetBalloonHelpString("Enable/Disable Velocity Plot in [cm/s]."); 
				
				mainWidget->Script("grid %s - - -sticky w",
			  this->Velocity2Plot->GetWidgetName());
				this->Velocity2Plot->SetBalloonHelpString("Enable/Disable Velocity Plot in [m/s].");   
			
			mainWidget->Script("grid %s %s -sticky ew",
			this->GraphsUpdateButton->GetWidgetName(), this->ExportResultButton->GetWidgetName());
		}
//	Se for falso remove a interface
	else
		{
			mainWidget->Script("grid remove %s",     
			this->NormalPlotFrame->GetWidgetName());
		}
}

//----------------------------------------------------------------------------
void vtkPVHMPlotWidget::AnimatedPlotMode(vtkPVHMStraightModelWidget* mainWidget, int show)
{
	//	Se form verdadeiro adiciona a interface
	if(show)
		{
			this->NormalPlotMode(mainWidget, 0);
			
			mainWidget->Script("grid %s -sticky ew -columnspan 2", 
					this->AnimatedPlotFrame->GetWidgetName());
	
			mainWidget->Script("grid %s - - -sticky w", 
					this->AnimatedPlotRadioSet->GetWidgetName());
		}
//	Se for falso remove a interface
	else
		  mainWidget->Script("grid remove %s",	
		  		this->AnimatedPlotFrame->GetWidgetName());
			mainWidget->Script("grid remove %s %s",
					this->GraphsUpdateButton->GetWidgetName(), this->ExportResultButton->GetWidgetName());
}
