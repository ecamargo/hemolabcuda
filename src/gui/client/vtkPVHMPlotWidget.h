/*
 * $Id: vtkPVHMPlotWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMPlotWidget
// .SECTION Description
// Creates KW Components for Plots

#ifndef _vtkPVHMPlotWidget_h_
#define _vtkPVHMPlotWidget_h_

#include "vtkObjectFactory.h"
#include "vtkIntArray.h"
#include "vtkPVHMObjectWidget.h"

#include "vtkKWCheckButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"

#include "vtkKWLabel.h"
#include "vtkKWComboBox.h"
#include "vtkKWPushButton.h"
#include "vtkKWLoadSaveDialog.h"



class vtkPVHM1DStraightModelSourceWidget;
class vtkPVHMStraightModelWidget;
class vtkKWCheckButton;

class VTK_EXPORT vtkPVHMPlotWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMPlotWidget *New();
  vtkTypeRevisionMacro(vtkPVHMPlotWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo(	);

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();
  
  // Description:
  // Include interface to normal plot options
  void NormalPlotMode(vtkPVHMStraightModelWidget* mainWidget, int show);
  
  // Description:
  // Include interface to animated plot options
  void AnimatedPlotMode(vtkPVHMStraightModelWidget* mainWidget, int show);
  
protected:
  vtkPVHMPlotWidget();
  virtual ~vtkPVHMPlotWidget();

public:
	void PlotTypeCallbackInternal(vtkIntArray *);
	// Description:
	// Change Plot Types
	vtkKWRadioButton* AnimatedPlotTypeRadio;
	vtkKWRadioButton* NormalPlotTypeRadio;
	
	// Description:
	// RadioButtonSet for group of the radio buttons
	vtkKWRadioButtonSet* AnimatedPlotRadioSet;
	
	// Description:
	// RadioButton for plot types. Using in animated plot.
  vtkKWRadioButton*	Pressure1PlotRadio;
  vtkKWRadioButton*	Pressure2PlotRadio;
    	
  vtkKWRadioButton*	AreaPlotRadio;  	
  vtkKWRadioButton*	FlowPlotRadio;  	
  
  vtkKWRadioButton*	Velocity1PlotRadio;
  vtkKWRadioButton*	Velocity2PlotRadio;
	
	// Description:
	// Plot Types. Using in normal plot
  vtkKWCheckButton*	Pressure1Plot;
  vtkKWCheckButton*	Pressure2Plot;
    	
  vtkKWCheckButton*	AreaPlot;  	
  vtkKWCheckButton*	FlowPlot;  	
  
  vtkKWCheckButton*	Velocity1Plot;
  vtkKWCheckButton*	Velocity2Plot;  	   
    
  vtkKWFrameWithLabel *PressureFrame;
  vtkKWFrameWithLabel *AreaFrame;
  vtkKWFrameWithLabel *FlowFrame;
  vtkKWFrameWithLabel *VelocityFrame;
  vtkKWFrameWithLabel *NumberFrame;
  
  vtkKWFrameWithLabel *AnimatedPlotFrame;
  vtkKWFrameWithLabel *NormalPlotFrame;
  
  vtkKWComboBox *NumberCombo;
  vtkKWPushButton *GraphsUpdateButton;
  vtkKWPushButton *ExportResultButton;
  
  // Description:
  // Dialog for choose the directory of simulation will be saved
  vtkKWLoadSaveDialog *ExportDialog;
  
//  vtkPVHMStraightModelWidget* MainWidget;
  
 
private:  
  vtkPVHMPlotWidget(const vtkPVHMPlotWidget&); // Not implemented
  void operator=(const vtkPVHMPlotWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMPlotWidget_h_*/


