/*
 * $Id: vtkPVHMSegmentWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */

#include "vtkPVHMSegmentWidget.h"
#include "vtkPVHMStraightModelWidget.h"

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWListBoxWithScrollbars.h"
#include "vtkKWListBox.h"
#include "vtkStringArray.h"
#include "resources/1DFlowDirection.h"
#include "vtkKWIcon.h"

#define INTERPOLATION_CONSTANT 	"Constant"
#define INTERPOLATION_LINEAR		"Linear"

#define INTERPOLATION_CONSTANT_VAL 	0
#define INTERPOLATION_LINEAR_VAL  	1

vtkCxxRevisionMacro(vtkPVHMSegmentWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMSegmentWidget);

//----------------------------------------------------------------------------
vtkPVHMSegmentWidget::vtkPVHMSegmentWidget()
{ 
  this->SetInterpolationMode(0); // tipo de interpolacao padrao é o constante
  
  this->LengthLabel						= vtkKWLabel::New();
  this->LengthEntry 					= vtkKWEntry::New();
  this->NumberOfElementsLabel	= vtkKWLabel::New();
  this->NumberOfElementsEntry = vtkKWEntry::New();
  this->DXLabel								= vtkKWLabel::New();
  this->DXEntry								= vtkKWEntry::New();
  this->InterpolationTypeMenuButton = vtkKWMenuButtonWithLabel::New();  
  this->InterpolationLabel1		= vtkKWLabel::New();
  this->InterpolationLabel2		= vtkKWLabel::New();  
  
  this->IsViscoelasticCheckButton = vtkKWCheckButton::New(); 
  this->FillSpace							= vtkKWLabel::New();     
	this->ViscoelasticProperties = 0; 
	
	//-----------------------------
  this->ProxRadiusLabel = vtkKWLabel::New();
  this->ProxRadiusEntry= vtkKWEntry::New();  
  this->DistalRadiusLabel= vtkKWLabel::New();
  this->DistalRadiusEntry= vtkKWEntry::New();  
  
  this->ProxWallThicknessLabel= vtkKWLabel::New();
  this->ProxWallThicknessEntry= vtkKWEntry::New();  
  this->DistalWallThicknessLabel= vtkKWLabel::New();
  this->DistalWallThicknessEntry= vtkKWEntry::New();
  
  this->ProxElastLabel= vtkKWLabel::New();
  this->ProxElastEntry= vtkKWEntry::New();  
  this->DistalElastLabel= vtkKWLabel::New();
  this->DistalElastEntry= vtkKWEntry::New();
  
  this->ProxPermLabel= vtkKWLabel::New();
  this->ProxPermEntry= vtkKWEntry::New();
  this->DistalPermLabel= vtkKWLabel::New();
  this->DistalPermEntry= vtkKWEntry::New();
  
  this->ProxInfLabel= vtkKWLabel::New();
  this->ProxInfEntry= vtkKWEntry::New();
  this->DistalInfLabel= vtkKWLabel::New();
  this->DistalInfEntry= vtkKWEntry::New();  
  
  this->ProxRefLabel= vtkKWLabel::New();
  this->ProxRefEntry= vtkKWEntry::New();
  this->DistalRefLabel= vtkKWLabel::New();
  this->DistalRefEntry= vtkKWEntry::New();
  
  //---------------------------------------------
  // prop do colageno
  this->ProxColagLabel= vtkKWLabel::New();
  this->ProxColagEntry= vtkKWEntry::New();  
  this->DistalColagLabel= vtkKWLabel::New();
  this->DistalColagEntry= vtkKWEntry::New();
  
  this->ProxCoefALabel= vtkKWLabel::New();
  this->ProxCoefAEntry= vtkKWEntry::New();
  this->DistalCoefALabel= vtkKWLabel::New();
  this->DistalCoefAEntry= vtkKWEntry::New();
  
  this->ProxCoefBLabel= vtkKWLabel::New();
  this->ProxCoefBEntry= vtkKWEntry::New();
  this->DistalCoefBLabel= vtkKWLabel::New();
  this->DistalCoefBEntry= vtkKWEntry::New();
 
  // prop de viscoelasticidade
  this->ProxViscoLabel= vtkKWLabel::New();
  this->ProxViscoEntry= vtkKWEntry::New();
  this->DistalViscoLabel= vtkKWLabel::New();
  this->DistalViscoEntry= vtkKWEntry::New();
  
  this->ProxExpViscoLabel= vtkKWLabel::New();
  this->ProxExpViscoEntry= vtkKWEntry::New();
  this->DistalExpViscoLabel= vtkKWLabel::New();
  this->DistalExpViscoEntry= vtkKWEntry::New();
  
  
  
  this->ProxWallLabel = vtkKWLabel::New();
  this->ProxWallEntry = vtkKWEntry::New(); 
  this->DistalWallLabel = vtkKWLabel::New();
  this->DistalWallEntry = vtkKWEntry::New();
 
	
  
  this->GeneralFrame	= vtkKWFrameWithLabel::New();
  this->GeometricalFrame	= vtkKWFrameWithLabel::New();
  this->MechanicalFrame		= vtkKWFrameWithLabel::New();
  
  this->ProximalGeometricalFrame = vtkKWFrameWithLabel::New();
  this->DistalGeometricalFrame = vtkKWFrameWithLabel::New();  
  
  this->ProximalMechanicalFrame = vtkKWFrameWithLabel::New();
  this->DistalMechanicalFrame = vtkKWFrameWithLabel::New();  
  
  this->UpdateNodePropButton = vtkKWPushButton::New();
  this->UpdateElementsPropButton = vtkKWPushButton::New();
  
  this->SegmentNameLabel = vtkKWLabel::New();
  this->SegmentNameEntry = vtkKWEntry::New();
  
  this->ElementSelected = 0;
  this->SegmentSelected = 0;
  
  
  this->ProxElastinCombo =vtkKWComboBox::New();
  
  this->radioImposeRefPressure =vtkKWRadioButton::New();
  this->radioImposeInfPressure =vtkKWRadioButton::New();
  
  this->ProxViscoElasticAngleLabel = vtkKWLabel::New();
  this->ProxViscoElasticAngleEntry = vtkKWEntry::New(); 
  this->ProxcharacTimeLabel = vtkKWLabel::New();
  this->ProxcharacTimeEntry = vtkKWEntry::New();
  
	this->CloneSegmentFrame = vtkKWFrameWithLabel::New();
	
  this->CloneSegmentOriginLabel = vtkKWLabel::New();
//  
  this->CloneSegmentTargetLabel = vtkKWLabel::New();
  
  this->CloneSegmentButton = vtkKWPushButton::New();
  this->AddSegmentButton = vtkKWPushButton::New();
  this->RemoveSegmentButton = vtkKWPushButton::New();
  this->RemoveAllSegmentButton = vtkKWPushButton::New();
  
  this->CloneSegmentOriginListBox = vtkKWListBoxWithScrollbars::New();
	this->CloneSegmentTargetListBox = vtkKWListBoxWithScrollbars::New();
	this->CloneSegmentListBox = vtkKWListBoxWithScrollbars::New();
	
	this->SegmentNameOrigin = vtkStringArray::New();
	this->SegmentNameTarget = vtkStringArray::New();
	
	this->FlowDirectionLabel = vtkKWLabel::New();
	this->ParentLabel = vtkKWLabel::New();
	this->ChildLabel = vtkKWLabel::New();
	this->ArrowLabel = vtkKWLabel::New();
	this->ArrowIcon = vtkKWIcon::New();
	
	this->FlowDirectionButton = vtkKWPushButton::New();
	
	
	
	this->DistalElastinCombo =vtkKWComboBox::New();
  
  this->DistalViscoElasticAngleLabel = vtkKWLabel::New();
  this->DistalViscoElasticAngleEntry = vtkKWEntry::New();
  this->DistalcharacTimeLabel = vtkKWLabel::New();
  this->DistalcharacTimeEntry = vtkKWEntry::New();

  this->ProxSpeedOfSoundLabel = vtkKWLabel::New();
  this->ProxSpeedOfSoundEntry = vtkKWEntry::New();
  this->DistalSpeedOfSoundLabel = vtkKWLabel::New();
  this->DistalSpeedOfSoundEntry = vtkKWEntry::New();
	
  this->VolumeLabel = vtkKWLabel::New();
  this->VolumeEntry = vtkKWEntry::New();
  
  this->ComplianceLabel = vtkKWLabel::New();
  this->ComplianceEntry = vtkKWEntry::New();
  
}

//----------------------------------------------------------------------------
vtkPVHMSegmentWidget::~vtkPVHMSegmentWidget()
{
  this->LengthLabel->Delete();
  this->LengthEntry->Delete();
  this->NumberOfElementsLabel->Delete();
  this->NumberOfElementsEntry->Delete();  
  this->DXLabel->Delete();  
  this->DXEntry->Delete();  
  this->IsViscoelasticCheckButton->Delete();  
  this->FillSpace->Delete();   
  
  if (this->InterpolationTypeMenuButton)
    {
    this->InterpolationTypeMenuButton->Delete();
    this->InterpolationTypeMenuButton = NULL;
    }
  this->InterpolationLabel1->Delete();
  this->InterpolationLabel2->Delete();    
    	
	//-----------------------------
  this->ProxRadiusLabel->Delete();
  this->ProxRadiusEntry->Delete();
  this->DistalRadiusLabel->Delete();
  this->DistalRadiusEntry->Delete();  
  
  this->ProxWallThicknessLabel->Delete();
  this->ProxWallThicknessEntry->Delete();  
  this->DistalWallThicknessLabel->Delete();
  this->DistalWallThicknessEntry->Delete();
  
  this->ProxElastLabel->Delete();
  this->ProxElastEntry->Delete();  
  this->DistalElastLabel->Delete();
  this->DistalElastEntry->Delete();
  
  this->ProxPermLabel->Delete();
  this->ProxPermEntry->Delete();
  this->DistalPermLabel->Delete();
  this->DistalPermEntry->Delete();
  
  this->ProxInfLabel->Delete();
  this->ProxInfEntry->Delete();
  this->DistalInfLabel->Delete();
  this->DistalInfEntry->Delete();
  
  this->ProxRefLabel->Delete();
  this->ProxRefEntry->Delete();
  this->DistalRefLabel->Delete();
  this->DistalRefEntry->Delete();
  
  //---------------------------------------------   
  // prop do colageno    
  this->ProxColagLabel->Delete();
  this->ProxColagEntry->Delete();  
  this->DistalColagLabel->Delete();
  this->DistalColagEntry->Delete();
  
  this->ProxCoefALabel->Delete();
  this->ProxCoefAEntry->Delete();
  this->DistalCoefALabel->Delete();
  this->DistalCoefAEntry->Delete();
  
  this->ProxCoefBLabel->Delete();
  this->ProxCoefBEntry->Delete();
  this->DistalCoefBLabel->Delete();
  this->DistalCoefBEntry->Delete();
 
  // prop de viscoelasticidade
  this->ProxViscoLabel->Delete();
  this->ProxViscoEntry->Delete();
  this->DistalViscoLabel->Delete();
  this->DistalViscoEntry->Delete();
  
  this->ProxExpViscoLabel->Delete();
  this->ProxExpViscoEntry->Delete();
  this->DistalExpViscoLabel->Delete();
  this->DistalExpViscoEntry->Delete();
  
  
  this->ProxWallLabel->Delete();
  this->ProxWallEntry->Delete(); 
  this->DistalWallLabel->Delete();
  this->DistalWallEntry->Delete();
    
  //----------------------------------------
  this->GeneralFrame->Delete();
  this->GeometricalFrame->Delete();
  this->MechanicalFrame->Delete();
  
  this->ProximalGeometricalFrame->Delete();
  this->DistalGeometricalFrame->Delete(); 
  
  this->ProximalMechanicalFrame->Delete();
  this->DistalMechanicalFrame->Delete();
  
  
  this->UpdateNodePropButton->Delete();
  this->UpdateElementsPropButton->Delete();
	
	this->SegmentNameLabel->Delete();
  this->SegmentNameEntry->Delete();
  

  this->ProxElastinCombo->Delete();
  
  this->radioImposeRefPressure->Delete();
  this->radioImposeInfPressure->Delete();
  

  this->ProxViscoElasticAngleLabel->Delete();
  this->ProxViscoElasticAngleEntry->Delete(); 
  this->ProxcharacTimeLabel->Delete();
  this->ProxcharacTimeEntry->Delete();
	
  this->ProxSpeedOfSoundLabel->Delete();
  this->ProxSpeedOfSoundEntry->Delete();
  this->DistalSpeedOfSoundLabel->Delete();
  this->DistalSpeedOfSoundEntry->Delete();
  
	this->CloneSegmentFrame->Delete();
	
  this->CloneSegmentOriginLabel->Delete();
//  
  this->CloneSegmentTargetLabel->Delete();
	
	this->CloneSegmentButton->Delete();
	this->AddSegmentButton->Delete();
	this->RemoveSegmentButton->Delete();
	this->RemoveAllSegmentButton->Delete();
	
	this->CloneSegmentOriginListBox->Delete();
	this->CloneSegmentTargetListBox->Delete();
	this->CloneSegmentListBox->Delete();
	
	this->SegmentNameOrigin->Delete();
	this->SegmentNameTarget->Delete();
	
	this->FlowDirectionLabel->Delete();
	this->ParentLabel->Delete();
	this->ChildLabel->Delete();
	this->ArrowLabel->Delete();
	this->ArrowIcon->Delete();
	this->FlowDirectionButton->Delete();
	
	
	this->DistalElastinCombo->Delete();
  
  this->DistalViscoElasticAngleLabel->Delete();
  this->DistalViscoElasticAngleEntry->Delete();
  this->DistalcharacTimeLabel->Delete();
  this->DistalcharacTimeEntry->Delete();
	
  this->VolumeLabel->Delete();
  this->VolumeEntry->Delete();
  
  this->ComplianceLabel->Delete();
  this->ComplianceEntry->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMSegmentWidget::UpdateWidgetInfo()
{
  int mode = this->GetReadOnly();

  this->LengthEntry->SetReadOnly(mode);
  this->NumberOfElementsEntry->SetReadOnly(mode);
  this->DXEntry->SetReadOnly(1);
  
	//-----------------------------  
  this->ProxRadiusEntry->SetReadOnly(mode);
  this->DistalRadiusEntry->SetReadOnly(mode); 
  this->ProxSpeedOfSoundEntry->SetReadOnly(1);
    
  this->ProxWallThicknessEntry->SetReadOnly(mode); 
  this->DistalWallThicknessEntry->SetReadOnly(mode);
  this->DistalSpeedOfSoundEntry->SetReadOnly(1);
  
  //this->ProxElastEntry->SetReadOnly(mode);  
  //this->DistalElastEntry->SetReadOnly(mode);
  
  this->ProxPermEntry->SetReadOnly(mode);
  this->DistalPermEntry->SetReadOnly(mode);
  
  this->ProxInfEntry->SetReadOnly(mode);
  this->DistalInfEntry->SetReadOnly(mode);
    
  this->ProxRefEntry->SetReadOnly(mode);
  this->DistalRefEntry->SetReadOnly(mode);
  
  //---------------------------------------------     
  this->ProxColagEntry->SetReadOnly(mode);  
  this->DistalColagEntry->SetReadOnly(mode);
  
  this->ProxCoefAEntry->SetReadOnly(mode);
  this->DistalCoefAEntry->SetReadOnly(mode);
  
  this->ProxCoefBEntry->SetReadOnly(mode);
  this->DistalCoefBEntry->SetReadOnly(mode);
 
  // prop de viscoelasticidade
  this->ProxViscoEntry->SetReadOnly(mode);
  this->DistalViscoEntry->SetReadOnly(mode);
  
  this->ProxExpViscoEntry->SetReadOnly(mode);
  this->DistalExpViscoEntry->SetReadOnly(mode);
  
  this->ProxWallEntry->SetReadOnly(mode);
  this->DistalWallEntry->SetReadOnly(mode);
  
  this->SegmentNameEntry->SetReadOnly(mode);
  
  
  this->ProxViscoElasticAngleEntry->SetReadOnly(mode);
  this->ProxcharacTimeEntry->SetReadOnly(mode);
  
  this->DistalViscoElasticAngleEntry->SetReadOnly(mode);
  this->DistalcharacTimeEntry->SetReadOnly(mode);
  
  if (mode)
  	{
  	this->radioImposeInfPressure->SetStateToDisabled();	
  	this->radioImposeRefPressure->SetStateToDisabled();
  	
  	this->ProxElastinCombo->EnabledOff();
  	this->DistalElastinCombo->EnabledOff();
  	
  	this->CloneSegmentButton->EnabledOff();
  	this->AddSegmentButton->EnabledOff();
  	this->RemoveSegmentButton->EnabledOff();
  	this->RemoveAllSegmentButton->EnabledOff();
  	this->CloneSegmentOriginListBox->EnabledOff();
		this->CloneSegmentTargetListBox->EnabledOff();
		this->CloneSegmentListBox->EnabledOff();
		this->FlowDirectionButton->EnabledOff();
  	}
	else
  	{
  	this->radioImposeInfPressure->SetStateToNormal();	
  	this->radioImposeRefPressure->SetStateToNormal();
  	
  	this->ProxElastinCombo->SetValue("Select");
  	this->ProxElastinCombo->EnabledOn();
  	
  	this->DistalElastinCombo->SetValue("Select");
  	this->DistalElastinCombo->EnabledOn();


  	this->CloneSegmentButton->EnabledOn();
  	this->AddSegmentButton->EnabledOn();
  	this->RemoveSegmentButton->EnabledOn();
  	this->RemoveAllSegmentButton->EnabledOn();
  	this->CloneSegmentOriginListBox->EnabledOn();
		this->CloneSegmentTargetListBox->EnabledOn();
		this->CloneSegmentListBox->EnabledOn();
		this->FlowDirectionButton->EnabledOn();
  	}

}

//----------------------------------------------------------------------------
void vtkPVHMSegmentWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Segment Information"); 
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);    
  
  this->FlowDirectionLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->FlowDirectionLabel->Create(pvApp);
  this->FlowDirectionLabel->SetText("Flow direction");
  
  this->FlowDirectionButton->SetParent(this->GetChildFrame()->GetFrame());
  this->FlowDirectionButton->Create(pvApp);
  this->FlowDirectionButton->SetText("Invert direction");
  
  this->ParentLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ParentLabel->Create(pvApp);
  this->ParentLabel->SetBackgroundColor(0,0,1);
  this->ParentLabel->SetWidth(8);
  
  this->ArrowLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ArrowLabel->Create(pvApp);
  this->ArrowLabel->SetWidth(8);
  
  this->ArrowIcon->SetImage(image_1DFlowDirection,
    				  image_1DFlowDirection_width,
    				  image_1DFlowDirection_height,
    				  image_1DFlowDirection_pixel_size,
    				  image_1DFlowDirection_length,
    				  image_1DFlowDirection_decoded_length);
  
  this->ArrowLabel->SetImageToIcon(this->ArrowIcon);
  
  this->ChildLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->ChildLabel->Create(pvApp);
  this->ChildLabel->SetBackgroundColor(0,1,0);
  this->ChildLabel->SetWidth(8);
  
  this->GeometricalFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->GeometricalFrame->Create(pvApp);
  this->GeometricalFrame->SetLabelText("Geometrical Properties");
  
  this->GeneralFrame->SetParent(this->GeometricalFrame->GetFrame());
  this->GeneralFrame->Create(pvApp);
  this->GeneralFrame->SetLabelText("General");	    
  
  this->ProximalGeometricalFrame->SetParent(this->GeometricalFrame->GetFrame());
  this->ProximalGeometricalFrame->Create(pvApp);
  this->ProximalGeometricalFrame->SetLabelText("Proximal Properties");
 
  this->DistalGeometricalFrame->SetParent(this->GeometricalFrame->GetFrame());
  this->DistalGeometricalFrame->Create(pvApp);
  this->DistalGeometricalFrame->SetLabelText("Distal Properties");
  this->DistalGeometricalFrame->CollapseFrame();  
  
  
  this->MechanicalFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->MechanicalFrame->Create(pvApp);
  this->MechanicalFrame->SetLabelText("Mechanical Properties");
  
  
  this->ProximalMechanicalFrame->SetParent(this->MechanicalFrame->GetFrame());
  this->ProximalMechanicalFrame->Create(pvApp);
  this->ProximalMechanicalFrame->SetLabelText("Proximal Properties");
 
  this->DistalMechanicalFrame->SetParent(this->MechanicalFrame->GetFrame());
  this->DistalMechanicalFrame->Create(pvApp);
  this->DistalMechanicalFrame->SetLabelText("Distal Properties");
  this->DistalMechanicalFrame->CollapseFrame();
  
  this->SegmentNameLabel->SetParent(this->GeneralFrame->GetFrame());
  this->SegmentNameLabel->Create(pvApp);
  this->SegmentNameLabel->SetText("Name: ");
  this->SegmentNameEntry->SetParent(this->GeneralFrame->GetFrame());
  this->SegmentNameEntry->Create(pvApp);
  this->SegmentNameEntry->SetBalloonHelpString("Name of the current segment.");
  
  this->LengthLabel->SetParent(this->GeneralFrame->GetFrame());
  this->LengthLabel->Create(pvApp);
  this->LengthLabel->SetText("Length: [cm]");
  this->LengthEntry->SetParent(this->GeneralFrame->GetFrame());
  this->LengthEntry->Create(pvApp);
  this->LengthEntry->SetBalloonHelpString("Length of the current segment.");
  this->LengthEntry->SetWidth(8);
    
  this->NumberOfElementsLabel->SetParent(this->GeneralFrame->GetFrame());
  this->NumberOfElementsLabel->Create(pvApp);
  this->NumberOfElementsLabel->SetText("Number of Elements: ");
  this->NumberOfElementsEntry->SetParent(this->GeneralFrame->GetFrame());
  this->NumberOfElementsEntry->Create(pvApp);
  this->NumberOfElementsEntry->SetBalloonHelpString("Number of elements in the current segment.");
  this->NumberOfElementsEntry->SetWidth(8);
  
  this->DXLabel->SetParent(this->GeneralFrame->GetFrame());
  this->DXLabel->Create(pvApp);
  this->DXLabel->SetText("Element Length: [cm]");
  this->DXEntry->SetParent(this->GeneralFrame->GetFrame());
  this->DXEntry->Create(pvApp);
  this->DXEntry->SetBalloonHelpString("Length of each element of the current segment.");
  this->DXEntry->SetWidth(8);
  
  this->VolumeLabel->SetParent(this->GeneralFrame->GetFrame());
  this->VolumeLabel->Create(pvApp);
  this->VolumeLabel->SetText("Volume [cm^3]");
  
  this->VolumeEntry->SetParent(this->GeneralFrame->GetFrame());
  this->VolumeEntry->Create(pvApp);
  this->VolumeEntry->SetWidth(8);
  this->VolumeEntry->SetReadOnly(1);
  this->VolumeEntry->SetBalloonHelpString("Volume of the current segment.");
  
  
  
  this->ComplianceLabel->SetParent(this->GeneralFrame->GetFrame());
  this->ComplianceLabel->Create(pvApp);
  this->ComplianceLabel->SetText("Compliance [cm^5/dyn]");
  
  this->ComplianceEntry->SetParent(this->GeneralFrame->GetFrame());
  this->ComplianceEntry->Create(pvApp);
  this->ComplianceEntry->SetWidth(8);
  this->ComplianceEntry->SetReadOnly(1);
  this->ComplianceEntry->SetBalloonHelpString("Compliance of the current segment.");
  
  
  
  this->InterpolationLabel1->SetParent(this->GeneralFrame->GetFrame());
  this->InterpolationLabel1->Create(pvApp);
  this->InterpolationLabel1->SetText("Properties Interpolation Type: ");
	this->InterpolationLabel2->SetParent(this->GeneralFrame->GetFrame());
  this->InterpolationLabel2->Create(pvApp);
  //this->InterpolationLabel2->SetText("Constant");   // na visualizcao este campo nao deve mostrar o tipo de interpolacao
  
  this->InterpolationTypeMenuButton->SetParent(this->GeneralFrame->GetFrame());
  this->InterpolationTypeMenuButton->Create(pvApp);
  this->InterpolationTypeMenuButton->ExpandWidgetOff();
  this->InterpolationTypeMenuButton->SetBalloonHelpString("Interpolation type of properties between proximal and distal values [constante (0), linear (1)]");

  this->InterpolationMenu = this->InterpolationTypeMenuButton->GetWidget();  
  this->InterpolationMenu->SetWidth(10);
  this->InterpolationMenu->SetPadX(10);
  this->InterpolationMenu->SetPadY(2);  
	this->InterpolationMenu->SetValue( INTERPOLATION_CONSTANT );  
	// Callback deste menu é registrada na classe vtkPVHMStraightModelWidget
	
  this->InterpolationTypeMenuButton->GetWidget()->IndicatorVisibilityOn();
  this->InterpolationTypeMenuButton->GetLabel()->SetText("Properties Interpolation Type:");
  this->InterpolationTypeMenuButton->LabelVisibilityOn();   

  this->IsViscoelasticCheckButton->SetParent(this->GetChildFrame()->GetFrame());
  this->IsViscoelasticCheckButton->Create(pvApp);
  this->IsViscoelasticCheckButton->SetText("Viscoelastic Properties");   
	// Callback deste menu é registrada na classe vtkPVHMStraightModelWidget  	
  
  this->ProxRadiusLabel->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProxRadiusLabel->Create(pvApp);
  this->ProxRadiusLabel->SetText("Radius: [cm]");
  this->ProxRadiusEntry->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProxRadiusEntry->Create(pvApp);
  this->ProxRadiusEntry->SetBalloonHelpString("Artery radius in the segment proximal position."); 
  this->ProxRadiusEntry->SetWidth(8);
  
  this->DistalRadiusLabel->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalRadiusLabel->Create(pvApp);
  this->DistalRadiusLabel->SetText("Radius: [cm]");
  this->DistalRadiusEntry->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalRadiusEntry->Create(pvApp);
  this->DistalRadiusEntry->SetBalloonHelpString("Artery radius in the segment distal position.");
  this->DistalRadiusEntry->SetWidth(8);
  
  this->ProxWallThicknessLabel->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProxWallThicknessLabel->Create(pvApp);
  this->ProxWallThicknessLabel->SetText("Wall Thickness: [cm]");
  this->ProxWallThicknessEntry->SetParent(this->GetFrame());
  this->ProxWallThicknessEntry->Create(pvApp);
  this->ProxWallThicknessEntry->SetBalloonHelpString("Artery wall thickness in the segment proximal position.");
  this->ProxWallThicknessEntry->SetWidth(8);
   
  this->DistalWallThicknessLabel->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalWallThicknessLabel->Create(pvApp);
  this->DistalWallThicknessLabel->SetText("Wall Thickness: [cm]");
  this->DistalWallThicknessEntry->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalWallThicknessEntry->Create(pvApp);
  this->DistalWallThicknessEntry->SetBalloonHelpString("Artery wall thickness in the segment distal position.");
  this->DistalWallThicknessEntry->SetWidth(8);

  this->ProxSpeedOfSoundLabel->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProxSpeedOfSoundLabel->Create(pvApp);
  this->ProxSpeedOfSoundLabel->SetText("Speed of Sound [cm/s]:");
  this->ProxSpeedOfSoundEntry->SetParent(this->ProximalGeometricalFrame->GetFrame());
  this->ProxSpeedOfSoundEntry->Create(pvApp);
 // this->ProxSpeedOfSoundEntry->SetBalloonHelpString("Artery wallthickness in segment distal position."); 
  this->ProxSpeedOfSoundEntry->SetWidth(8);
  
  this->DistalSpeedOfSoundLabel->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalSpeedOfSoundLabel->Create(pvApp);
  this->DistalSpeedOfSoundLabel->SetText("Speed of Sound [cm/s]:");
  this->DistalSpeedOfSoundEntry->SetParent(this->DistalGeometricalFrame->GetFrame());
  this->DistalSpeedOfSoundEntry->Create(pvApp);
 // this->DistalSpeedOfSoundEntry->SetBalloonHelpString("Artery wallthickness in segment distal position."); 
  this->DistalSpeedOfSoundEntry->SetWidth(8);
  
  this->UpdateNodePropButton->SetParent(this->GeometricalFrame->GetFrame());
  this->UpdateNodePropButton->Create(pvApp);
  this->UpdateNodePropButton->SetText("Update Nodes");
  this->UpdateNodePropButton->SetBalloonHelpString("Updates every node from the current segment.");
  
  
  this->UpdateElementsPropButton->SetParent(this->MechanicalFrame->GetFrame());
  this->UpdateElementsPropButton->Create(pvApp);
  this->UpdateElementsPropButton->SetText("Update Elements");
  this->UpdateElementsPropButton->SetBalloonHelpString("This updates every element from the current segment.");
  
  this->ProxElastLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxElastLabel->Create(pvApp);
  this->ProxElastLabel->SetText("Elastin: [dyn/cm^2]");
  this->ProxElastEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxElastEntry->Create(pvApp);
  this->ProxElastEntry->SetBalloonHelpString("Effective elastin module in the segment proximal position.");
  this->ProxElastEntry->SetWidth(8);
  this->ProxElastEntry->SetReadOnly(1);
  
  this->DistalElastLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalElastLabel->Create(pvApp);
  this->DistalElastLabel->SetText("Elastin: [dyn/cm^2]");
  this->DistalElastEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalElastEntry->Create(pvApp);
  this->DistalElastEntry->SetBalloonHelpString("Effective elastin module in the segment distal position."); 
  this->DistalElastEntry->SetWidth(8);
  this->DistalElastEntry->SetReadOnly(1);
 
  this->ProxPermLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxPermLabel->Create(pvApp);
  this->ProxPermLabel->SetText("Permeability: [cm^4/sec/dyn]");
  this->ProxPermEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxPermEntry->Create(pvApp);
  this->ProxPermEntry->SetBalloonHelpString("Permeability coefficient in the segment proximal position.");
  this->ProxPermEntry->SetWidth(8);
  
  this->DistalPermLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalPermLabel->Create(pvApp);
  this->DistalPermLabel->SetText("Permeability: [cm^4/sec/dyn]");
  this->DistalPermEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalPermEntry->Create(pvApp);
  this->DistalPermEntry->SetBalloonHelpString("Permeability coefficient in the segment distal position."); 
  this->DistalPermEntry->SetWidth(8);
  
  this->ProxInfLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxInfLabel->Create(pvApp);
  this->ProxInfLabel->SetText("Infiltration Pressure: [dyn/cm^2]");
  this->ProxInfEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxInfEntry->Create(pvApp);
  this->ProxInfEntry->SetBalloonHelpString("Infiltration Pressure in the segment proximal position."); 
  this->ProxInfEntry->SetWidth(8);
  
  this->DistalInfLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalInfLabel->Create(pvApp);
  this->DistalInfLabel->SetText("Infiltration Pressure: [dyn/cm^2]");
  this->DistalInfEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalInfEntry->Create(pvApp);
  this->DistalInfEntry->SetBalloonHelpString("Infiltration Pressure in the segment distal position."); 
  this->DistalInfEntry->SetWidth(8);
  
  this->ProxRefLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxRefLabel->Create(pvApp);
  this->ProxRefLabel->SetText("Reference Pressure: [dyn/cm^2]");
  this->ProxRefEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxRefEntry->Create(pvApp);
  this->ProxRefEntry->SetBalloonHelpString("Reference Pressure in the segment proximal position."); 
  this->ProxRefEntry->SetWidth(8);
  
  this->DistalRefLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalRefLabel->Create(pvApp);
  this->DistalRefLabel->SetText("Reference Pressure: [dyn/cm^2]");
  this->DistalRefEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalRefEntry->Create(pvApp); 
  this->DistalRefEntry->SetBalloonHelpString("Reference Pressure in the segment distal position."); 
  this->DistalRefEntry->SetWidth(8);
  
  //---------------------------------------------------------------------------------------
  // prop colageno
  this->ProxColagLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxColagLabel->Create(pvApp);
  this->ProxColagLabel->SetText("Elastin Colagen: [dyn/cm^2]");
  this->ProxColagEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxColagEntry->Create(pvApp); 
  this->ProxColagEntry->SetBalloonHelpString("Effective Colagen Elastin  in the segment proximal position."); 
  this->ProxColagEntry->SetWidth(8);
  
  this->DistalColagLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalColagLabel->Create(pvApp);
  this->DistalColagLabel->SetText("Elastin Colagen: [dyn/cm^2]");
  this->DistalColagEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalColagEntry->Create(pvApp); 
  this->DistalColagEntry->SetBalloonHelpString("Effective Colagen Elastin  in the segment Distal position."); 
  this->DistalColagEntry->SetWidth(8);
  
  this->ProxCoefALabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxCoefALabel->Create(pvApp);
  this->ProxCoefALabel->SetText("Coefficient A:");
  this->ProxCoefAEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxCoefAEntry->Create(pvApp); 
  this->ProxCoefAEntry->SetBalloonHelpString("Average value of the coefficient A in the segment proximal position."); 
  this->ProxCoefAEntry->SetWidth(8);
  
  this->DistalCoefALabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalCoefALabel->Create(pvApp);
  this->DistalCoefALabel->SetText("Coefficient A:");
  this->DistalCoefAEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalCoefAEntry->Create(pvApp); 
  this->DistalCoefAEntry->SetBalloonHelpString("Average value of the coefficient A in the segment distal position."); 
  this->DistalCoefAEntry->SetWidth(8);
  
  this->ProxCoefBLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxCoefBLabel->Create(pvApp);
  this->ProxCoefBLabel->SetText("Coefficient B:");
  this->ProxCoefBEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxCoefBEntry->Create(pvApp); 
  this->ProxCoefBEntry->SetBalloonHelpString("Average value of the coefficient B in the segment proximal position."); 
  this->ProxCoefBEntry->SetWidth(8);
  
  this->DistalCoefBLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalCoefBLabel->Create(pvApp);
  this->DistalCoefBLabel->SetText("Coefficient B:");
  this->DistalCoefBEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalCoefBEntry->Create(pvApp); 
  this->DistalCoefBEntry->SetBalloonHelpString("Average value of the coefficient B in the segment distal position."); 
  this->DistalCoefBEntry->SetWidth(8);
  
	// prop visco....   
  this->ProxViscoLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxViscoLabel->Create(pvApp);
  this->ProxViscoLabel->SetText("Viscoelasticity [dyn.sec/cm^2]:");
  this->ProxViscoEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxViscoEntry->Create(pvApp); 
  this->ProxViscoEntry->SetBalloonHelpString("Viscoelasticity coefficient in the segment proximal position."); 
  this->ProxViscoEntry->SetWidth(8);
  
  this->DistalViscoLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalViscoLabel->Create(pvApp);
  this->DistalViscoLabel->SetText("Viscoelasticity [dyn.sec/cm^2]:");
  this->DistalViscoEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalViscoEntry->Create(pvApp); 
  this->DistalViscoEntry->SetBalloonHelpString("Viscoelasticity coefficient in the segment Distal position.");
  this->DistalViscoEntry->SetWidth(8);
  
  this->ProxExpViscoLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxExpViscoLabel->Create(pvApp);
  this->ProxExpViscoLabel->SetText("Viscoelasticity Expoent:");
  this->ProxExpViscoEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxExpViscoEntry->Create(pvApp); 
  this->ProxExpViscoEntry->SetBalloonHelpString("Viscoelasticity expoent from the artery wall in segment proximal position."); 
  this->ProxExpViscoEntry->SetWidth(8);
   
  this->DistalExpViscoLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalExpViscoLabel->Create(pvApp);
  this->DistalExpViscoLabel->SetText("Viscoelasticity Expoent:");
  this->DistalExpViscoEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalExpViscoEntry->Create(pvApp); 
  this->DistalExpViscoEntry->SetBalloonHelpString("Viscoelasticity expoent from the artery wall in segment distal position."); 
  this->DistalExpViscoEntry->SetWidth(8);
  
  this->ProxWallLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxWallLabel->Create(pvApp);
  this->ProxWallLabel->SetText("Wallthickness:");
  this->ProxWallEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxWallEntry->Create(pvApp);
  this->ProxWallEntry->SetBalloonHelpString("Artery wallthickness in segment proximal position."); 
  this->ProxWallEntry->SetWidth(8);
  
  this->DistalWallLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalWallLabel->Create(pvApp);
  this->DistalWallLabel->SetText("Wallthickness:");
  this->DistalWallEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalWallEntry->Create(pvApp);
  this->DistalWallEntry->SetBalloonHelpString("Artery wallthickness in segment distal position."); 
  this->DistalWallEntry->SetWidth(8);
  
  this->ProxElastinCombo->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxElastinCombo->Create(pvApp);
  //this->ProxElastinCombo->SetBalloonHelpString("Constitutive behavior for the arterial wall.");
	this->ProxElastinCombo->ReadOnlyOn();
  this->ProxElastinCombo->SetWidth(8);
  this->ProxElastinCombo->EnabledOff();

  this->DistalElastinCombo->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalElastinCombo->Create(pvApp);
  //this->ProxElastinCombo->SetBalloonHelpString("Constitutive behavior for the arterial wall.");
	this->DistalElastinCombo->ReadOnlyOn();
  this->DistalElastinCombo->SetWidth(8);
  this->DistalElastinCombo->EnabledOff();

  //this->comboRegularParameter->SetBalloonHelpString("Blood Constitutive Law.");
  const char *ProxElastinComboOpt[] = { "4e+6", "8e+6", "1.6e+7", "Other"};
  for (int i = 0; i < 4; i++)
  	{
		this->ProxElastinCombo->AddValue(ProxElastinComboOpt[i]);
  	this->DistalElastinCombo->AddValue(ProxElastinComboOpt[i]);
  	}
  
  this->radioImposeRefPressure->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->radioImposeRefPressure->Create(pvApp);
  this->radioImposeRefPressure->SetText("Apply to \nwhole tree");
  //this->radioImposeRefPressure->SetBalloonHelpString("Variational formulation for solving the problem");
  this->radioImposeRefPressure->SetValue("LW");

  this->radioImposeInfPressure->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->radioImposeInfPressure->Create(pvApp);
  this->radioImposeInfPressure->SetText("Apply to \nwhole tree");
  //this->radioImposeInfPressure->SetBalloonHelpString("Variational formulation for solving the problem");
  this->radioImposeInfPressure->SetValue("LW");

  this->ProxViscoElasticAngleLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxViscoElasticAngleLabel->Create(pvApp);
  this->ProxViscoElasticAngleLabel->SetText("Viscoelastic Angle [degrees]:");
  this->ProxViscoElasticAngleEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxViscoElasticAngleEntry->Create(pvApp);
 // this->ProxViscoElasticAngleEntry->SetBalloonHelpString("Artery wallthickness in segment proximal position."); 
  this->ProxViscoElasticAngleEntry->SetWidth(8);
  this->ProxViscoElasticAngleEntry->SetValueAsDouble(10);
  //this->ProxViscoElasticAngleEntry->SetCommand(this,"UpdateViscoValue");
   
  this->ProxcharacTimeLabel->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxcharacTimeLabel->Create(pvApp);
  this->ProxcharacTimeLabel->SetText("Characteristic Time [sec]:");
  this->ProxcharacTimeEntry->SetParent(this->ProximalMechanicalFrame->GetFrame());
  this->ProxcharacTimeEntry->Create(pvApp);
 // this->ProxcharacTimeEntry->SetBalloonHelpString("Artery wallthickness in segment distal position."); 
  this->ProxcharacTimeEntry->SetWidth(8);
  this->ProxcharacTimeEntry->SetValueAsDouble(0.3);

  this->DistalViscoElasticAngleLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalViscoElasticAngleLabel->Create(pvApp);
  this->DistalViscoElasticAngleLabel->SetText("Viscoelastic Angle [degrees]:");
  this->DistalViscoElasticAngleEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalViscoElasticAngleEntry->Create(pvApp);
 // this->DistalViscoElasticAngleEntry->SetBalloonHelpString("Artery wallthickness in segment proximal position."); 
  this->DistalViscoElasticAngleEntry->SetWidth(8);
  this->DistalViscoElasticAngleEntry->SetValueAsDouble(10);
  //this->ProxViscoElasticAngleEntry->SetCommand(this,"UpdateViscoValue");
  
  this->DistalcharacTimeLabel->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalcharacTimeLabel->Create(pvApp);
  this->DistalcharacTimeLabel->SetText("Characteristic Time [sec]:");
  this->DistalcharacTimeEntry->SetParent(this->DistalMechanicalFrame->GetFrame());
  this->DistalcharacTimeEntry->Create(pvApp);
 // this->ProxcharacTimeEntry->SetBalloonHelpString("Artery wallthickness in segment distal position."); 
  this->DistalcharacTimeEntry->SetWidth(8);
  this->DistalcharacTimeEntry->SetValueAsDouble(0.3);

	this->CloneSegmentFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->CloneSegmentFrame->Create(pvApp);
  this->CloneSegmentFrame->SetLabelText("Clone Segments Properties");
  
  this->CloneSegmentOriginLabel->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentOriginLabel->Create(pvApp);
  this->CloneSegmentOriginLabel->SetText("Origin Segment");
	  
  this->CloneSegmentTargetLabel->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentTargetLabel->Create(pvApp);
  this->CloneSegmentTargetLabel->SetText("Target Segment");
  
  this->CloneSegmentButton->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentButton->Create(pvApp);
  this->CloneSegmentButton->SetText("Copy");
  
  this->AddSegmentButton->SetParent(this->CloneSegmentFrame->GetFrame());
  this->AddSegmentButton->Create(pvApp);
  this->AddSegmentButton->SetText("Add");
  
  this->RemoveSegmentButton->SetParent(this->CloneSegmentFrame->GetFrame());
  this->RemoveSegmentButton->Create(pvApp);
  this->RemoveSegmentButton->SetText("Remove");
  
  this->RemoveAllSegmentButton->SetParent(this->CloneSegmentFrame->GetFrame());
  this->RemoveAllSegmentButton->Create(pvApp);
  this->RemoveAllSegmentButton->SetText("Remove All");
  
  this->CloneSegmentOriginListBox->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentOriginListBox->Create(pvApp);
  this->CloneSegmentOriginListBox->SetBorderWidth(2);
  this->CloneSegmentOriginListBox->SetReliefToGroove();
  this->CloneSegmentOriginListBox->SetPadX(2);
  this->CloneSegmentOriginListBox->SetPadY(2);
	this->CloneSegmentOriginListBox->GetWidget()->SetSelectionModeToSingle();
  this->CloneSegmentOriginListBox->GetWidget()->ExportSelectionOff();
  this->CloneSegmentOriginListBox->GetWidget()->SetHeight(5);
  
	this->CloneSegmentTargetListBox->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentTargetListBox->Create(pvApp);
  this->CloneSegmentTargetListBox->SetBorderWidth(2);
  this->CloneSegmentTargetListBox->SetReliefToGroove();
  this->CloneSegmentTargetListBox->SetPadX(2);
  this->CloneSegmentTargetListBox->SetPadY(2);
  this->CloneSegmentTargetListBox->GetWidget()->SetSelectionModeToSingle();
  this->CloneSegmentTargetListBox->GetWidget()->ExportSelectionOff();
  this->CloneSegmentTargetListBox->GetWidget()->SetHeight(5);
  
  this->CloneSegmentListBox->SetParent(this->CloneSegmentFrame->GetFrame());
  this->CloneSegmentListBox->Create(pvApp);
  this->CloneSegmentListBox->SetBorderWidth(2);
  this->CloneSegmentListBox->SetReliefToGroove();
  this->CloneSegmentListBox->SetPadX(2);
  this->CloneSegmentListBox->SetPadY(2);
  this->CloneSegmentListBox->GetWidget()->SetHeight(5);
	//-------------------------------------------------------------------------------------       
}


void vtkPVHMSegmentWidget::IsViscoelasticCallback()
{
  this->ViscoelasticProperties = (ViscoelasticProperties) ? 0 : 1;
}

// ----------------------------------------------------------------------------
void vtkPVHMSegmentWidget::InterpolationTypeCallback() 
{
  if (this->InterpolationTypeMenuButton->IsCreated())
    {
    const char *value = this->InterpolationTypeMenuButton->GetWidget()->GetValue();
    if (!strcmp(value, INTERPOLATION_LINEAR))
    		{
        this->DistalGeometricalFrame->ExpandFrame();  
   			this->DistalMechanicalFrame->ExpandFrame();  
        this->SetInterpolationMode((int) INTERPOLATION_LINEAR_VAL);
    		}
    else if (!strcmp(value, INTERPOLATION_CONSTANT))
    		{
	   		this->DistalGeometricalFrame->CollapseFrame();  
   			this->DistalMechanicalFrame->CollapseFrame();  
       	this->SetInterpolationMode((int) INTERPOLATION_CONSTANT_VAL);     
    		}  
	  this->InterpolationLabel2->SetText(value);	   		 	
    } 
}

// ----------------------------------------------------------------------------
void vtkPVHMSegmentWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
	int mode = mainWidget->GetWidgetMode();
		
	// Se visibility for 0, sai da função
	if(!this->GetWidgetVisibility())
		{
	  mainWidget->Script("grid forget %s",
    	this->GetChildFrame()->GetWidgetName());			
		return;
		}  
	this->GetChildFrame()->ExpandFrame();		
	
  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName()); 			 
  
  mainWidget->Script("grid %s %s -sticky ew",
      this->FlowDirectionLabel->GetWidgetName(), this->FlowDirectionButton->GetWidgetName());
  mainWidget->Script("grid %s %s %s -sticky ew -padx 10",     
  			     this->ParentLabel->GetWidgetName(), this->ArrowLabel->GetWidgetName(), this->ChildLabel->GetWidgetName());
  
 //  if (this->GetSegmentSelected())
 //  {
  		   mainWidget->Script("grid %s -sticky ew",     
		     this->GeneralFrame->GetWidgetName()); 
  		
  		
  			mainWidget->Script("grid %s -sticky ew -columnspan 3",     
		     this->GeometricalFrame->GetWidgetName());
  	
//   }
//   
//   if (this->GetElementSelected())
//   {
//   	  mainWidget->Script("grid remove %s",     
//		      this->GeneralFrame->GetWidgetName());
//		//        this->GeometricalFrame->GetWidgetName());
//   	
//   }

  
  
	mainWidget->Script("grid %s -sticky ew",     
		     this->ProximalGeometricalFrame->GetWidgetName());
		     
	mainWidget->Script("grid %s -sticky ew",     
		     this->DistalGeometricalFrame->GetWidgetName());
 
 
// if (this->GetSegmentSelected())
// {		  
  			mainWidget->Script("grid %s -sticky ew  -columnspan 3",     
		     this->MechanicalFrame->GetWidgetName());
// }
 

 		     
	mainWidget->Script("grid %s -sticky ew",     
		     this->ProximalMechanicalFrame->GetWidgetName());
		     
 	mainWidget->Script("grid %s -sticky ew",     
		     this->DistalMechanicalFrame->GetWidgetName());     
	
	
	mainWidget->Script("grid %s -sticky ew -columnspan 3",     
		     this->CloneSegmentFrame->GetWidgetName());
		     
	mainWidget->Script("grid %s %s - -sticky w",     
		    this->CloneSegmentOriginLabel->GetWidgetName(),
		    this->CloneSegmentTargetLabel->GetWidgetName());
	
	mainWidget->Script("grid %s %s - -sticky ew",     
		    this->CloneSegmentOriginListBox->GetWidgetName(),
		    this->CloneSegmentTargetListBox->GetWidgetName());
	
	mainWidget->Script("grid %s %s %s %s -sticky ew",     
		    this->CloneSegmentButton->GetWidgetName(),
		    this->AddSegmentButton->GetWidgetName(),
		    this->RemoveSegmentButton->GetWidgetName(),
		    this->RemoveAllSegmentButton->GetWidgetName());
	
	mainWidget->Script("grid %s - - -sticky ew",     
		    this->CloneSegmentListBox->GetWidgetName());
	
	
	mainWidget->Script("grid %s %s - - -sticky ew",     
		    this->SegmentNameLabel->GetWidgetName(),
		    this->SegmentNameEntry->GetWidgetName());
	
 	mainWidget->Script("grid %s %s - - -sticky ew",     
		    this->LengthLabel->GetWidgetName(),
		    this->LengthEntry->GetWidgetName());

	mainWidget->Script("grid %s %s - - -sticky ew",     
		    this->NumberOfElementsLabel->GetWidgetName(),
		    this->NumberOfElementsEntry->GetWidgetName());
 
	mainWidget->Script("grid %s %s - - -sticky ew",     
  	this->DXLabel->GetWidgetName(),
		this->DXEntry->GetWidgetName());
	
	mainWidget->Script("grid %s %s - - -sticky ew",     
  	this->VolumeLabel->GetWidgetName(),
		this->VolumeEntry->GetWidgetName());
	
  mainWidget->Script("grid %s %s - - -sticky ew",     
    this->ComplianceLabel->GetWidgetName(),
    this->ComplianceEntry->GetWidgetName());
	
	
  mainWidget->Script("grid %s - - -sticky w",     
    this->InterpolationLabel1->GetWidgetName());
   // this->InterpolationLabel2->GetWidgetName());    
    
  mainWidget->Script("grid %s - - -sticky w",
    this->InterpolationTypeMenuButton->GetWidgetName());    
   
//  mainWidget->Script("grid %s - - -sticky w",   
//	  this->IsViscoelasticCheckButton->GetWidgetName());
 // 	this->IsViscoelasticCheckButton->SetBalloonHelpString("Enable/Disable Viscoelastic properties.");    		
	
	//---------------------------------------------------------	
	mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxRadiusLabel->GetWidgetName(),
    this->ProxRadiusEntry->GetWidgetName());  	
	
	mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalRadiusLabel->GetWidgetName(),
    this->DistalRadiusEntry->GetWidgetName()); 
    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxWallThicknessLabel->GetWidgetName(),
    this->ProxWallThicknessEntry->GetWidgetName()); 
        
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalWallThicknessLabel->GetWidgetName(),
    this->DistalWallThicknessEntry->GetWidgetName()); 
    
  mainWidget->Script("grid %s %s - - -sticky w",     
  	this->ProxSpeedOfSoundLabel->GetWidgetName(),
  	this->ProxSpeedOfSoundEntry->GetWidgetName());

  mainWidget->Script("grid %s %s - - -sticky w",     
  	this->DistalSpeedOfSoundLabel->GetWidgetName(),
  	this->DistalSpeedOfSoundEntry->GetWidgetName());
  
   // mainWidget->Script("grid %s - - -sticky w", 
   // this->UpdateNodePropButton->GetWidgetName());
    
    
        
  if(mode)
    {
  	mainWidget->Script("grid remove %s",
  		this->UpdateElementsPropButton->GetWidgetName());
  	
    mainWidget->Script("grid remove %s",	
  		this->UpdateNodePropButton->GetWidgetName());
 		}
  else
    {
    mainWidget->Script("grid %s - - -sticky w", 
    	this->UpdateElementsPropButton->GetWidgetName());
    
    mainWidget->Script("grid %s - - -sticky w", 
    	this->UpdateNodePropButton->GetWidgetName());
    }
   
  mainWidget->Script("grid %s %s %s - - -sticky w",     
    this->ProxElastLabel->GetWidgetName(),
    this->ProxElastEntry->GetWidgetName(),
    this->ProxElastinCombo->GetWidgetName()); 
    
        
 mainWidget->Script("grid %s %s %s - - -sticky w",     
    this->DistalElastLabel->GetWidgetName(),
    this->DistalElastEntry->GetWidgetName(),
    this->DistalElastinCombo->GetWidgetName());   
    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxColagLabel->GetWidgetName(),
    this->ProxColagEntry->GetWidgetName()); 
  
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalColagLabel->GetWidgetName(),
    this->DistalColagEntry->GetWidgetName()); 
  
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxCoefALabel->GetWidgetName(),
    this->ProxCoefAEntry->GetWidgetName()); 
    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalCoefALabel->GetWidgetName(),
    this->DistalCoefAEntry->GetWidgetName()); 
           
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxCoefBLabel->GetWidgetName(),
    this->ProxCoefBEntry->GetWidgetName()); 
    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalCoefBLabel->GetWidgetName(),
    this->DistalCoefBEntry->GetWidgetName()); 

	mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxViscoLabel->GetWidgetName(),
    this->ProxViscoEntry->GetWidgetName());
     
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalViscoLabel->GetWidgetName(),
    this->DistalViscoEntry->GetWidgetName());  	
	
	mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxExpViscoLabel->GetWidgetName(),
    this->ProxExpViscoEntry->GetWidgetName());
     
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalExpViscoLabel->GetWidgetName(),
    this->DistalExpViscoEntry->GetWidgetName()); 

  
    
  mainWidget->Script("grid %s %s %s - - -sticky w",     
    this->ProxInfLabel->GetWidgetName(),
    this->ProxInfEntry->GetWidgetName(),
    this->radioImposeInfPressure->GetWidgetName()); 
        
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalInfLabel->GetWidgetName(),
    this->DistalInfEntry->GetWidgetName());
    
    
      
  mainWidget->Script("grid %s %s %s - - -sticky w",     
    this->ProxRefLabel->GetWidgetName(),
    this->ProxRefEntry->GetWidgetName(),
    this->radioImposeRefPressure->GetWidgetName()); 
        
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalRefLabel->GetWidgetName(),
    this->DistalRefEntry->GetWidgetName());
    //-------------------------------------------------------
    
    
        
	//-------------------------------------------------
	
    
  
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxPermLabel->GetWidgetName(),
    this->ProxPermEntry->GetWidgetName()); 
        
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalPermLabel->GetWidgetName(),
    this->DistalPermEntry->GetWidgetName());
  
  
    
	//--------------------------------------------------------	
   
	// Checkbox não deverá aparecer no "Input Visualization"
	if(mode)
  	mainWidget->Script("grid remove %s",
    	this->IsViscoelasticCheckButton->GetWidgetName());	
	
	// Dependendo do modo de visualização, entrará um Entry + Label (Input vis.)
	// ou um Menu Button (Edição)
	if(mode)
  	mainWidget->Script("grid remove %s",
    	this->InterpolationTypeMenuButton->GetWidgetName());  
  else
  	mainWidget->Script("grid remove %s %s",
    	this->InterpolationLabel1->GetWidgetName(),
	    this->InterpolationLabel2->GetWidgetName());		
	    
  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxViscoElasticAngleLabel->GetWidgetName(),
    this->ProxViscoElasticAngleEntry->GetWidgetName()); 
	
	
	
	  mainWidget->Script("grid %s %s - - -sticky w",     
    this->ProxcharacTimeLabel->GetWidgetName(),
    this->ProxcharacTimeEntry->GetWidgetName());    
    

  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalViscoElasticAngleLabel->GetWidgetName(),
    this->DistalViscoElasticAngleEntry->GetWidgetName());
	
	
	  mainWidget->Script("grid %s %s - - -sticky w",     
    this->DistalcharacTimeLabel->GetWidgetName(),
    this->DistalcharacTimeEntry->GetWidgetName());    

}

//----------------------------------------------------------------------------
void vtkPVHMSegmentWidget::PlaceComponents(vtkPVHM1DStraightModelSourceWidget* mainWidget)
{
}

// ----------------------------------------------------------------------------  
vtkKWFrameWithLabel* vtkPVHMSegmentWidget::GetSegmentFrame()
{
	return this->GetChildFrame();
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::Initialize()
{
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::Accept()
{  		
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetLength(double Length)
{
  this->LengthEntry->SetValueAsDouble(Length);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDx(double Dx)
{
  this->DXEntry->SetValueAsDouble(Dx);		
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetVolume(double volume)
{
  this->VolumeEntry->SetValueAsDouble(volume);		
}

//-------------------------------------------------------------------------------------------------------------------------------

void vtkPVHMSegmentWidget::SetCompliance(double value)
{
  this->ComplianceEntry->SetValueAsDouble(value);    
}


void vtkPVHMSegmentWidget::SetNumberOfElements(int NumberOfElements)
{
  this->NumberOfElementsEntry->SetValueAsInt(NumberOfElements);
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetInterpolationType(int type)
{
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetSegmentInfo(char *text)
{
	this->GetChildFrame()->SetLabelText(text); 
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxRadius(double radius)
{
  this->ProxRadiusEntry->SetValueAsDouble(radius);	
}
    
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalRadius(double radius)
{
  this->DistalRadiusEntry->SetValueAsDouble(radius);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxWall(double wall)
{
  this->ProxWallThicknessEntry->SetValueAsDouble(wall);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistWall(double wall)
{
  this->DistalWallThicknessEntry->SetValueAsDouble(wall);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxElast(double elast)
{
  this->ProxElastEntry->SetValueAsDouble(elast);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistElast(double elast)
{
  this->DistalElastEntry->SetValueAsDouble(elast);	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxPerm(double perm)
{
  this->ProxPermEntry->SetValueAsDouble(perm);	
}
  
//------------------------------------------------------------------------------------------------------------------------------- 
void vtkPVHMSegmentWidget::SetDistPerm(double perm)
{
	this->DistalPermEntry->SetValueAsDouble(perm);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxInf(double inf)
{
	this->ProxInfEntry->SetValueAsDouble(inf);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistInf(double inf)
{
	this->DistalInfEntry->SetValueAsDouble(inf);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxRef(double ref)
{
	this->ProxRefEntry->SetValueAsDouble(ref);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistRef(double ref)
{
	this->DistalRefEntry->SetValueAsDouble(ref);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxColag(double colag)
{
	this->ProxColagEntry->SetValueAsDouble(colag);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalColag(double colag)
{
	this->DistalColagEntry->SetValueAsDouble(colag);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxCoefA(double coefA)
{
	this->ProxCoefAEntry->SetValueAsDouble(coefA);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalCoefA(double coefA)
{
	this->DistalCoefAEntry->SetValueAsDouble(coefA);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxCoefB(double coefB)
{
	this->ProxCoefBEntry->SetValueAsDouble(coefB);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalCoefB(double coefB)
{
	this->DistalCoefBEntry->SetValueAsDouble(coefB);	
}

//-------------------------------------------------------------------------------------------------------------------------------
//--------------------- visco methods
void vtkPVHMSegmentWidget::SetProxVisco(double visco)
{
	this->ProxViscoEntry->SetValueAsDouble(visco);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalVisco(double visco)
{
	this->DistalViscoEntry->SetValueAsDouble(visco);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetProxExpVisco(double expvisco)
{
	this->ProxExpViscoEntry->SetValueAsDouble(expvisco);	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetDistalExpVisco(double expvisco)
{
	this->DistalExpViscoEntry->SetValueAsDouble(expvisco);	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProximalRadius(void)
{
 return this->ProxRadiusEntry->GetValueAsDouble();  	
	
}
//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalRadius(void)
{
 return this->DistalRadiusEntry->GetValueAsDouble();  
	
}
//-------------------------------------------------------------------------------------------------------------------------------

double vtkPVHMSegmentWidget::GetProximalWall(void)
{
 return this->ProxWallThicknessEntry->GetValueAsDouble();  	
	
}
//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalWall(void)
{
 return this->DistalWallThicknessEntry->GetValueAsDouble();  
	
}
//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxElast()
{
  return this->ProxElastEntry->GetValueAsDouble();	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalElast()
{
  return this->DistalElastEntry->GetValueAsDouble();	
}
  
//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxPerm()
{
  return this->ProxPermEntry->GetValueAsDouble();	
}
  
//------------------------------------------------------------------------------------------------------------------------------- 
double vtkPVHMSegmentWidget::GetDistalPerm()
{
	return this->DistalPermEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxInf()
{
	return this->ProxInfEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalInf()
{
	return this->DistalInfEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxRef()
{
	return this->ProxRefEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalRef()
{
	return this->DistalRefEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxColag()
{
	return this->ProxColagEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalColag()
{
	return this->DistalColagEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxCoefA()
{
	return this->ProxCoefAEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalCoefA()
{
	return this->DistalCoefAEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxCoefB()
{
	return this->ProxCoefBEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalCoefB()
{
	return this->DistalCoefBEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
//--------------------- visco methods
double vtkPVHMSegmentWidget::GetProxVisco()
{
	return this->ProxViscoEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalVisco()
{
	return this->DistalViscoEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetProxExpVisco()
{
	return this->ProxExpViscoEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetDistalExpVisco()
{
	return this->DistalExpViscoEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
double vtkPVHMSegmentWidget::GetLength()
{
	return this->LengthEntry->GetValueAsDouble();	
}

//-------------------------------------------------------------------------------------------------------------------------------
int vtkPVHMSegmentWidget::GetNumberOfElements()
{
	return this->NumberOfElementsEntry->GetValueAsInt();	
}

//-------------------------------------------------------------------------------------------------------------------------------
const char *vtkPVHMSegmentWidget::GetSegmentName()
{
	return this->SegmentNameEntry->GetValue();	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::SetSegmentName(const char *n)
{
	this->SegmentNameEntry->SetValue(n);
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::AddSegmentNameInCloneList()
{
	char *orig, *target;
	char *origcopy, *targetcopy;
	string s;
	
	orig = (char*)this->CloneSegmentOriginListBox->GetWidget()->GetSelection();
	target = (char*)this->CloneSegmentTargetListBox->GetWidget()->GetSelection();
	
	// se algum dos segmentos nao estiver selecionado
	if ( (!orig) || (!target) )
		return;
	
	//Caso o segmento origem e o destino sejam iguais
	if ( !strcmp(orig, target) )
		return;
	
	origcopy = new char[strlen(orig)];
	targetcopy = new char[strlen(target)];
	
	strcpy(origcopy, (char*)orig);
	strcpy(targetcopy, (char*)target);
	
	s += origcopy;
	s += " -> ";
	s += targetcopy;
	
	if ( this->CloneSegmentListBox->GetWidget()->AppendUnique(s.c_str()) )
		{
		this->SegmentNameOrigin->InsertNextValue(origcopy);
		this->SegmentNameTarget->InsertNextValue(targetcopy);
		}
	
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::AddSegmentNameInList(char *name)
{
	this->CloneSegmentOriginListBox->GetWidget()->Append(name);
	this->CloneSegmentTargetListBox->GetWidget()->Append(name);
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::RemoveSegmentNameInList(char *name)
{
	int index = this->CloneSegmentOriginListBox->GetWidget()->GetItemIndex(name);
	
	this->CloneSegmentOriginListBox->GetWidget()->DeleteRange(index, index);
	this->CloneSegmentTargetListBox->GetWidget()->DeleteRange(index, index);
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::RemoveAllSegmentNameInList()
{
	this->CloneSegmentOriginListBox->GetWidget()->DeleteAll();
	this->CloneSegmentTargetListBox->GetWidget()->DeleteAll();
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::RemoveSegmentNameInCloneList()
{
	const char *copy;
	int index;
	
	copy = this->CloneSegmentListBox->GetWidget()->GetSelection();
	index = this->CloneSegmentListBox->GetWidget()->GetSelectionIndex();

	if ( !copy )
		return;
	
	vtkStringArray *origArray = vtkStringArray::New();
	vtkStringArray *targetArray = vtkStringArray::New();
	
	for ( int i=0; i<this->SegmentNameOrigin->GetNumberOfValues(); i++ )
		{
		if ( i != index )
			{
			origArray->InsertNextValue(this->SegmentNameOrigin->GetValue(i));
			targetArray->InsertNextValue(this->SegmentNameTarget->GetValue(i));
			
			}
		}
	this->SegmentNameOrigin->DeepCopy(origArray);
	this->SegmentNameTarget->DeepCopy(targetArray);
	origArray->Delete();
	targetArray->Delete();
	this->CloneSegmentListBox->GetWidget()->DeleteRange(index, index);
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSegmentWidget::RemoveAllSegmentNameInCloneList()
{
	this->CloneSegmentListBox->GetWidget()->DeleteAll();
	this->SegmentNameOrigin->Reset();
	this->SegmentNameTarget->Reset();
}
//-------------------------------------------------------------------------------------------------------------------------------
vtkStringArray* vtkPVHMSegmentWidget::GetSegmentNameOrigin()
{
	return this->SegmentNameOrigin;
}

//-------------------------------------------------------------------------------------------------------------------------------
vtkStringArray* vtkPVHMSegmentWidget::GetSegmentNameTarget()
{
	return this->SegmentNameTarget;
}
