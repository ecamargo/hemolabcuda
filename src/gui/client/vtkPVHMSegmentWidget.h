/*
 * $Id: vtkPVHMSegmentWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMSegmentWidget
// .SECTION Description
// Creates KW Components for Segments

#ifndef _vtkPVHMSegmentWidget_h_
#define _vtkPVHMSegmentWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"

class vtkKWEntry;
class vtkKWLabel;
class vtkKWCheckButton;
class vtkKWMenuButton;
class vtkKWMenuButtonWithLabel;
class vtkKWComboBox;
class vtkSMSourceProxy;
class vtkPVHM1DStraightModelSourceWidget;
class vtkPVHMStraightModelWidget;
class vtkKWRadioButton;

class vtkKWListBoxWithScrollbars;
class vtkStringArray;

#include <list>

class VTK_EXPORT vtkPVHMSegmentWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMSegmentWidget *New();
  vtkTypeRevisionMacro(vtkPVHMSegmentWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHM1DStraightModelSourceWidget *);
	void PlaceComponents(vtkPVHMStraightModelWidget *);
	
  // Description:
  // Call creation on the child.  
  virtual void ChildCreate(vtkPVApplication*);
  
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();
  
  // Description:
  // Get Segment Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetSegmentFrame();

  // Description:
  // Modify Viscoelastic properties
	void IsViscoelasticCallback();

  // Description:
  // Set Interpolation Type (Constant or Linear)
	void InterpolationTypeCallback();

	// Description:
  // Set/Get Widget Mode
  vtkSetMacro(InterpolationMode, int);
  vtkGetMacro(InterpolationMode, int); 	

	// Description:
  // Set/Get ViscoelasticProperties	
  vtkSetMacro(ViscoelasticProperties, int);
  vtkGetMacro(ViscoelasticProperties, int);  
	
  // Description:
  // set the value of the segment widgets
  void SetLength(double Length);
  void SetDx(double Dx);
  void SetNumberOfElements(int NumberOfElements);
  void SetInterpolationType(int type);
  void SetSegmentInfo(char *text);
  void SetVolume(double volume);
  void SetCompliance(double value);
  
  //---------------------------------------------------------
  void SetProxRadius(double radius);
  void SetDistalRadius(double radius);
  void SetProxWall(double wall);
  void SetDistWall(double wall);
  void SetProxElast(double elast);
  void SetDistElast(double elast);
  void SetProxPerm(double perm);
  void SetDistPerm(double perm);
  void SetProxInf(double inf);
  void SetDistInf(double inf);
  void SetProxRef(double ref);
  void SetDistRef(double ref);
  
  //-----------------------------------------------------
  void SetProxColag(double colag);
  void SetDistalColag(double colag);
  void SetProxCoefA(double coefA);
  void SetDistalCoefA(double coefA);
  void SetProxCoefB(double coefB);
  void SetDistalCoefB(double coefB);
  void SetProxVisco(double visco);
  void SetDistalVisco(double visco);
  void SetProxExpVisco(double expvisco);
  void SetDistalExpVisco(double expvisco);
  //----------------------------------------------------
  
  //-------------------------------------------------------------------------------------------------------------------------------
  
  double GetProximalRadius(void);
  double GetDistalRadius(void);
  double GetProximalWall(void);
  double GetDistalWall(void);
  

	double GetProxElast();
	double GetDistalElast();
	double GetProxPerm();
	double GetDistalPerm();
	double GetProxInf();
	double GetDistalInf();
	double GetProxRef();
	double GetDistalRef();
	double GetProxColag();
	double GetDistalColag();
	double GetProxCoefA();
	double GetDistalCoefA();
	double GetProxCoefB();
	double GetDistalCoefB();
	double GetProxVisco();
	double GetDistalVisco();
	double GetProxExpVisco();
	double GetDistalExpVisco();
  
  double GetLength();
  int GetNumberOfElements();
  
  vtkSetMacro(ElementSelected, int);
  vtkGetMacro(ElementSelected, int);
  vtkSetMacro(SegmentSelected, int);
	vtkGetMacro(SegmentSelected, int);  
	
	const char* GetSegmentName();

	void SetSegmentName(const char *n);
	
	vtkGetObjectMacro(ProxElastinCombo, vtkKWComboBox);

	vtkGetObjectMacro(ProxViscoEntry, vtkKWEntry);

	vtkGetObjectMacro(ProxElastEntry, vtkKWEntry);

	vtkGetObjectMacro(radioImposeRefPressure, vtkKWRadioButton);

	vtkGetObjectMacro(radioImposeInfPressure, vtkKWRadioButton);


	vtkGetObjectMacro(ProxViscoElasticAngleEntry, vtkKWEntry);
	vtkGetObjectMacro(ProxcharacTimeEntry, vtkKWEntry);
//	vtkGetObjectMacro(ProxElastEntry, vtkKWEntry);
//	vtkGetObjectMacro(ProxElastEntry, vtkKWEntry);



	vtkGetObjectMacro(DistalElastinCombo, vtkKWComboBox);

	vtkGetObjectMacro(DistalViscoEntry, vtkKWEntry);

	vtkGetObjectMacro(DistalElastEntry, vtkKWEntry);
	
	vtkGetObjectMacro(ProxSpeedOfSoundEntry, vtkKWEntry);
	vtkGetObjectMacro(DistalSpeedOfSoundEntry, vtkKWEntry);


	vtkGetObjectMacro(DistalViscoElasticAngleEntry, vtkKWEntry);
	vtkGetObjectMacro(DistalcharacTimeEntry, vtkKWEntry);

	
	
	vtkGetObjectMacro(CloneSegmentButton, vtkKWPushButton);
	vtkGetObjectMacro(AddSegmentButton, vtkKWPushButton);
	vtkGetObjectMacro(RemoveSegmentButton, vtkKWPushButton);
	vtkGetObjectMacro(RemoveAllSegmentButton, vtkKWPushButton);
	
	vtkGetObjectMacro(CloneSegmentOriginListBox, vtkKWListBoxWithScrollbars);
	vtkGetObjectMacro(CloneSegmentTargetListBox, vtkKWListBoxWithScrollbars);
	vtkGetObjectMacro(CloneSegmentListBox, vtkKWListBoxWithScrollbars);
	
//*********************************************************************************
//*********************************************************************************
	
//	const char* GetCloneSegmentOrigin();
//	const char* GetCloneSegmentTarget();
	
	// Description:
	// Add and remove name of the list of segment name
	void AddSegmentNameInList(char *name);
	void RemoveSegmentNameInList(char *name);
	void RemoveAllSegmentNameInList();
	
	// Description:
	// Add and remove names of the clone list
	void AddSegmentNameInCloneList();
	void RemoveSegmentNameInCloneList();
	void RemoveAllSegmentNameInCloneList();
	
	vtkStringArray *GetSegmentNameOrigin();
	vtkStringArray *GetSegmentNameTarget();
	
	// Description:
	// Get button to invert flow direction
	vtkGetObjectMacro(FlowDirectionButton, vtkKWPushButton);
	
public:

	// KW components that uses callbacks should be 'public'
	// because their commands will be registered in vtkPVHM1DStraightModelSourceWidget

  vtkKWMenuButtonWithLabel*	InterpolationTypeMenuButton;
	vtkKWMenuButton*					InterpolationMenu; 
  vtkKWCheckButton*					IsViscoelasticCheckButton;    
  
  vtkKWPushButton *UpdateNodePropButton;
  vtkKWPushButton *UpdateElementsPropButton;
  
protected:
  vtkPVHMSegmentWidget();
  virtual ~vtkPVHMSegmentWidget();
  
  int ElementSelected;
  int SegmentSelected;
  
    
	// Description:
	// Segment Widget
	// Area, Radius, alfa and Resolution
	vtkKWLabel* SegmentNameLabel;
  vtkKWEntry* SegmentNameEntry;
  vtkKWLabel* LengthLabel;
  vtkKWEntry* LengthEntry;  
  vtkKWLabel* NumberOfElementsLabel;
  vtkKWEntry* NumberOfElementsEntry;  
  vtkKWLabel* DXLabel;
  vtkKWEntry* DXEntry;   
  
  //-----------------------------
  vtkKWLabel* ProxRadiusLabel;
  vtkKWEntry* ProxRadiusEntry;  
  vtkKWLabel* DistalRadiusLabel;
  vtkKWEntry* DistalRadiusEntry;  
  
  vtkKWLabel* ProxWallThicknessLabel;
  vtkKWEntry* ProxWallThicknessEntry;  
  vtkKWLabel* DistalWallThicknessLabel;
  vtkKWEntry* DistalWallThicknessEntry;
  
  vtkKWLabel* ProxElastLabel;
  vtkKWEntry* ProxElastEntry;  
  vtkKWLabel* DistalElastLabel;
  vtkKWEntry* DistalElastEntry;
  
  vtkKWLabel* ProxPermLabel;
  vtkKWEntry* ProxPermEntry;
  vtkKWLabel* DistalPermLabel;
  vtkKWEntry* DistalPermEntry;
  
  vtkKWLabel* ProxInfLabel;
  vtkKWEntry* ProxInfEntry;
  vtkKWLabel* DistalInfLabel;
  vtkKWEntry* DistalInfEntry;
  
  vtkKWLabel* ProxRefLabel;
  vtkKWEntry* ProxRefEntry;
  vtkKWLabel* DistalRefLabel;
  vtkKWEntry* DistalRefEntry;
  
  //---------------------------------------------
  // propreidades do colageno
  vtkKWLabel* ProxColagLabel;
  vtkKWEntry* ProxColagEntry;  
  vtkKWLabel* DistalColagLabel;
  vtkKWEntry* DistalColagEntry;  
  
  vtkKWLabel* ProxCoefALabel;
  vtkKWEntry* ProxCoefAEntry;  
  vtkKWLabel* DistalCoefALabel;
  vtkKWEntry* DistalCoefAEntry;  
  
  vtkKWLabel* ProxCoefBLabel;
  vtkKWEntry* ProxCoefBEntry;  
  vtkKWLabel* DistalCoefBLabel;
  vtkKWEntry* DistalCoefBEntry;  
 
  // prop de viscoelasticidade
  vtkKWLabel* ProxViscoLabel;
  vtkKWEntry* ProxViscoEntry;  
  vtkKWLabel* DistalViscoLabel;
  vtkKWEntry* DistalViscoEntry;  
  
  vtkKWLabel* ProxExpViscoLabel;
  vtkKWEntry* ProxExpViscoEntry;  
  vtkKWLabel* DistalExpViscoLabel;
  vtkKWEntry* DistalExpViscoEntry;  
  
  vtkKWLabel* ProxWallLabel;
  vtkKWEntry* ProxWallEntry;  
  vtkKWLabel* DistalWallLabel;
  vtkKWEntry* DistalWallEntry;  
 
  
  
  vtkKWLabel* FillSpace; 
  
  vtkKWComboBox *ProxElastinCombo;
  
  vtkKWRadioButton *radioImposeRefPressure;
  vtkKWRadioButton *radioImposeInfPressure;

  vtkKWLabel* ProxViscoElasticAngleLabel;
  vtkKWEntry* ProxViscoElasticAngleEntry;  
  vtkKWLabel* ProxcharacTimeLabel;
  vtkKWEntry* ProxcharacTimeEntry;  


  vtkKWComboBox *DistalElastinCombo;

  vtkKWLabel* DistalViscoElasticAngleLabel;
  vtkKWEntry* DistalViscoElasticAngleEntry;  
  vtkKWLabel* DistalcharacTimeLabel;
  vtkKWEntry* DistalcharacTimeEntry;  

  vtkKWLabel* ProxSpeedOfSoundLabel;
  vtkKWEntry* ProxSpeedOfSoundEntry;
  vtkKWLabel* DistalSpeedOfSoundLabel;
  vtkKWEntry* DistalSpeedOfSoundEntry;
  
  
  //---------------------------------------------              
	// Descrition:
	// Stores current widget mode (visualization, edition etc).   
  int InterpolationMode;

	// Descrition:
	// Components to be used in "Input Visualization" instead of KWMenuButton
  vtkKWLabel* InterpolationLabel1;
  vtkKWLabel* InterpolationLabel2;  
 
	// Descrition:
	// Stores current Viscoelastic property (1 - on, 0 - off)
  int ViscoelasticProperties;  
  
  vtkKWFrameWithLabel*  GeneralFrame;
  vtkKWFrameWithLabel*  GeometricalFrame;
  vtkKWFrameWithLabel*  ProximalGeometricalFrame;
  vtkKWFrameWithLabel*  DistalGeometricalFrame;
  
  vtkKWFrameWithLabel*  MechanicalFrame;
  vtkKWFrameWithLabel*  ProximalMechanicalFrame;
  vtkKWFrameWithLabel*  DistalMechanicalFrame;
  
  // Description:
  // Check button for clone segment properties
//  vtkKWCheckButton *CloneSegmentCheckButton;
  
  vtkKWFrameWithLabel*  CloneSegmentFrame;
  
  // Description:
  // Label and entry with name of the segment origin of the clone
  vtkKWLabel* CloneSegmentOriginLabel;
//  vtkKWEntry* CloneSegmentOriginEntry;
  
  // Description:
  // Label and entry with name of the segment origin of the clone
  vtkKWLabel* CloneSegmentTargetLabel;
//  vtkKWEntry* CloneSegmentTargetEntry;
  
  vtkKWPushButton *CloneSegmentButton;
  vtkKWPushButton *AddSegmentButton;
  vtkKWPushButton *RemoveSegmentButton;
  vtkKWPushButton *RemoveAllSegmentButton;
	
	vtkKWListBoxWithScrollbars *CloneSegmentOriginListBox;
	vtkKWListBoxWithScrollbars *CloneSegmentTargetListBox;
	vtkKWListBoxWithScrollbars *CloneSegmentListBox;
	
//	SegmentNameList SegmentNameOrigin;
//	SegmentNameList SegmentNameTarget;
	
	vtkStringArray *SegmentNameOrigin;
	vtkStringArray *SegmentNameTarget;
	
	// Description:
	// Labels to indicate flow direction
	vtkKWLabel *FlowDirectionLabel;
	vtkKWLabel *ParentLabel;
	vtkKWLabel *ChildLabel;
	vtkKWLabel *ArrowLabel;
	vtkKWIcon *ArrowIcon;
	
	// Description:
  // Button to invert flow direction
  vtkKWPushButton *FlowDirectionButton;
  
  // Description:
  // Label for volume of the segment.
  vtkKWLabel *VolumeLabel;
  vtkKWEntry *VolumeEntry;
  
  vtkKWLabel *ComplianceLabel;
  vtkKWEntry *ComplianceEntry;
  
  
  
	
private:  
  vtkPVHMSegmentWidget(const vtkPVHMSegmentWidget&); // Not implemented
  void operator=(const vtkPVHMSegmentWidget&); // Not implemented
}; 

#endif  /*_vtkPVHMSegmentWidget_h_*/

