/*
 * $Id:$
 */
#include "vtkPVHMSolverToEnSight.h"

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"

#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWRange.h"
#include "vtkKWCheckButtonWithLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWScale.h"
#include "vtkKWComboBoxWithLabel.h"
#include "vtkKWComboBox.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMProxyManager.h"

#include "string.h"


vtkCxxRevisionMacro(vtkPVHMSolverToEnSight, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMSolverToEnSight);

//----------------------------------------------------------------------------
vtkPVHMSolverToEnSight::vtkPVHMSolverToEnSight()
{ 
	this->MyFrame	    = vtkKWFrameWithLabel::New();

	// File
	this->ConfigButton 	= vtkKWLoadSaveButton::New();
	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{EnSight animation Files} {*.ace}}");
	
	// Byte Order
	this->ConfigByteOrder 	= vtkKWComboBoxWithLabel::New(); 
	
	// Transient Geometry
	this->ConfigTransientGeometry = vtkKWCheckButtonWithLabel::New();
	this->ConfigScaleFactor = vtkKWScaleWithEntry::New();
	
	// Information
	this->ConfigTimeStepLabel	= vtkKWLabel::New();
	this->ConfigtimeInitialLabel	= vtkKWLabel::New();
	this->ConfigtimeFinalLabel	= vtkKWLabel::New();
	this->ConfigdurationLabel	= vtkKWLabel::New();
	
	this->ValueTimeStepLabel	= vtkKWLabel::New();
	this->ValuetimeInitialLabel	= vtkKWLabel::New();
	this->ValuetimeFinalLabel	= vtkKWLabel::New();
	this->ValuedurationLabel	= vtkKWLabel::New();
	
	// Range
	this->ConfigRange = vtkKWRange::New();
}

//----------------------------------------------------------------------------
vtkPVHMSolverToEnSight::~vtkPVHMSolverToEnSight()
{ 
  this->MyFrame->Delete();
  this->ConfigButton->Delete();
  this->ConfigByteOrder->Delete();
  
  this->ConfigTransientGeometry->Delete();
  this->ConfigScaleFactor->Delete();
  
	this->ConfigTimeStepLabel->Delete();
	this->ConfigtimeInitialLabel->Delete();
	this->ConfigtimeFinalLabel->Delete();
	this->ConfigdurationLabel->Delete();
	
	this->ValueTimeStepLabel->Delete();
	this->ValuetimeInitialLabel->Delete();
	this->ValuetimeFinalLabel->Delete();
	this->ValuedurationLabel->Delete();
	
	this->ConfigRange->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigTimeStepsInformationCallback(int *NumberTimeSteps, double *timeInitial, double *timeFinal, double *duration)
{
	vtkSMIntVectorProperty *InformationNumberOfTimeSteps_prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationNumberOfTimeSteps"));   
	
	vtkSMDoubleVectorProperty *InformationInitialTimeOfTimeSteps_prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationInitialTimeOfTimeSteps"));   
	
	vtkSMDoubleVectorProperty *InformationFinalTimeOfTimeSteps_prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationFinalTimeOfTimeSteps"));   
	
	vtkSMDoubleVectorProperty *InformationDurationOfTimeSteps_prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationDurationOfTimeSteps"));  
	
	this->GetPVSource()->GetProxy()->UpdatePropertyInformation(InformationNumberOfTimeSteps_prop);
	this->GetPVSource()->GetProxy()->UpdatePropertyInformation(InformationInitialTimeOfTimeSteps_prop);
	this->GetPVSource()->GetProxy()->UpdatePropertyInformation(InformationFinalTimeOfTimeSteps_prop);
	this->GetPVSource()->GetProxy()->UpdatePropertyInformation(InformationDurationOfTimeSteps_prop);
	
  int NumberOfSteps;
  double InitialOfSteps, FinalOfSteps, internalDuration;
  
  if ( (InformationNumberOfTimeSteps_prop) && (InformationInitialTimeOfTimeSteps_prop)
  			&&	 (InformationFinalTimeOfTimeSteps_prop) && (InformationDurationOfTimeSteps_prop) )
  	{
  	NumberOfSteps = InformationNumberOfTimeSteps_prop->GetElement(0);
  	InitialOfSteps = InformationInitialTimeOfTimeSteps_prop->GetElement(0);
  	FinalOfSteps = InformationFinalTimeOfTimeSteps_prop->GetElement(0);
  	internalDuration = InformationDurationOfTimeSteps_prop->GetElement(0);
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
    	return;
		}
	*NumberTimeSteps = NumberOfSteps;
	*timeInitial = InitialOfSteps;
	*timeFinal = FinalOfSteps;
	*duration = internalDuration;
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigWidgetRangeCallback()
{
	vtkSMIntVectorProperty *low_prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationLowRange"));   
	
	vtkSMIntVectorProperty *up_prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("InformationUpRange"));   
	
	double range[2];
	
	this->ConfigRange->GetRange(range);
	
  if ( (low_prop) && (up_prop) )
  	{  	
  	low_prop->SetElement(0, int(range[0]) );
  	up_prop->SetElement(0, int(range[1]) );
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
    	return;
		}
  
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigTransientGeometryCallback()
{
	vtkSMIntVectorProperty *EnableTransientGeometry_prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("EnableTransientGeometry")); 
  
	if ( EnableTransientGeometry_prop )
  	{  	
		EnableTransientGeometry_prop->SetElement(0, this->ConfigTransientGeometry->GetWidget()->GetSelectedState() );
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
    	return;
		}
	
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigScaleFactorCallback()
{
	vtkSMDoubleVectorProperty *DisplacementFactor_prop = vtkSMDoubleVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("DisplacementFactor")); 
  
	double value = this->ConfigScaleFactor->GetWidget()->GetValue();
	
	if ( DisplacementFactor_prop )
  	{  	
		DisplacementFactor_prop->SetElement(0, value );
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
    	return;
		}
	
	if(value >= 2)
		{
		this->ConfigTransientGeometry->GetWidget()->SelectedStateOn();
		this->ConfigTransientGeometryCallback();
		}
	else
		{
		this->ConfigTransientGeometry->GetWidget()->SelectedStateOff();
		this->ConfigTransientGeometryCallback();
		}
			
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigByteOrderCallback()
{
	vtkSMIntVectorProperty *ByteOrder_prop = vtkSMIntVectorProperty::SafeDownCast(
	                             this->GetPVSource()->GetProxy()->GetProperty("ByteOrder")); 
  
	if ( ByteOrder_prop )
  	{ 
		if( strncmp(this->ConfigByteOrder->GetWidget()->GetValue(), "Big Endian", 10) == 0 )
			ByteOrder_prop->SetElement(0, 1);
		else
			ByteOrder_prop->SetElement(0, 0);
  	}
  else
		{
  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
    	return;
		}
	
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigButtonCallback()
{
   vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
 	                             this->GetPVSource()->GetProxy()->GetProperty("animationPath"));    
	  if (prop)
	  	{
	  	prop->SetElement(0, this->ConfigButton->GetFileName());
	  	}
	  else
			{
	  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
      	return;
  		}
  
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::Create(vtkKWApplication* app)
{
	vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->MyFrame->SetParent(this);
  this->MyFrame->Create(pvApp);
  this->MyFrame->SetLabelText("HeMoLab Solver to EnSight Filter");
    
  this->ConfigByteOrder->SetParent(this->MyFrame->GetFrame());
  this->ConfigByteOrder->Create(pvApp);
  this->ConfigByteOrder->SetLabelText("Select byte order");
  this->ConfigByteOrder->SetBalloonHelpString("Set byte order for files");
  this->ConfigByteOrder->GetWidget()->ReadOnlyOn();
  this->ConfigByteOrder->GetWidget()->AddValue("Big Endian");
  this->ConfigByteOrder->GetWidget()->AddValue("Little Endian");
  this->ConfigByteOrder->GetWidget()->SetValue("Big Endian"); 
  this->ConfigByteOrder->GetWidget()->SetCommand(this, "ConfigByteOrderCallback"); 
  
  this->ConfigButton->SetParent(this->MyFrame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->GetLoadSaveDialog()->ChooseDirectoryOn();
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback"); 
  this->ConfigButton->SetBalloonHelpString("Set Output path for the files"); 
  this->ConfigButton->SetText("Set Output path"); 
  
  this->ConfigRange->SetParent(this->MyFrame->GetFrame());
  this->ConfigRange->Create(pvApp);
  this->ConfigRange->SetLabelText("Range of files:");
  this->ConfigRange->SetLabelPositionToLeft();
  this->ConfigRange->SetEntry1PositionToLeft();
  this->ConfigRange->SetEntry2PositionToRight();
  this->ConfigRange->SetSliderSize(4);
  this->ConfigRange->SetThickness(23);
  this->ConfigRange->SetInternalThickness(0.6);
  this->ConfigRange->SetRequestedLength(200);
  this->ConfigRange->SliderCanPushOn();
  this->ConfigRange->SetRangeInteractionColor(1.0, 0.0, 0.0);
  this->ConfigRange->SetResolution(1.0);
  this->ConfigRange->SetBalloonHelpString("Set minimum and maximum values for makes files");
  this->ConfigRange->SetCommand(this, "ConfigWidgetRangeCallback");
  
  this->ConfigTransientGeometry->SetParent(this->MyFrame->GetFrame());
  this->ConfigTransientGeometry->Create(pvApp);
  this->ConfigTransientGeometry->SetLabelText("Set Transient Geometry:");
  this->ConfigTransientGeometry->SetBalloonHelpString("Set Transient Geometry ON/OFF");
  this->ConfigTransientGeometry->GetWidget()->SetCommand(this, "ConfigTransientGeometryCallback");    
  
  this->ConfigScaleFactor->SetParent(this->MyFrame->GetFrame());
  this->ConfigScaleFactor->PopupModeOn();
  this->ConfigScaleFactor->Create(pvApp);
  this->ConfigScaleFactor->SetRange(1.0, 50.0);
  this->ConfigScaleFactor->SetResolution(0.1);
  this->ConfigScaleFactor->SetLabelText("Displacement factor:");
  this->ConfigScaleFactor->SetBalloonHelpString("Set the factor used to Transient Geometry");
  this->ConfigScaleFactor->SetCommand(this, "ConfigScaleFactorCallback");
  
  this->ConfigTimeStepLabel->SetParent(this->MyFrame->GetFrame());
  this->ConfigTimeStepLabel->Create(pvApp);
  this->ConfigTimeStepLabel->SetText( "Number Of Time Steps:");
  this->ConfigTimeStepLabel->SetPadX(20);
    
  this->ValueTimeStepLabel->SetParent(this->MyFrame->GetFrame());
  this->ValueTimeStepLabel->Create(pvApp);

  this->ConfigtimeInitialLabel->SetParent(this->MyFrame->GetFrame());
  this->ConfigtimeInitialLabel->Create(pvApp);
  this->ConfigtimeInitialLabel->SetText( "Time Step initial time:" );
  this->ConfigtimeInitialLabel->SetPadX(20);
  
  this->ValuetimeInitialLabel->SetParent(this->MyFrame->GetFrame());
  this->ValuetimeInitialLabel->Create(pvApp);
  
  this->ConfigtimeFinalLabel->SetParent(this->MyFrame->GetFrame());
  this->ConfigtimeFinalLabel->Create(pvApp);
  this->ConfigtimeFinalLabel->SetText( "Time Step final time:" );
  this->ConfigtimeFinalLabel->SetPadX(20);
  
  this->ValuetimeFinalLabel->SetParent(this->MyFrame->GetFrame());
  this->ValuetimeFinalLabel->Create(pvApp);
  
  this->ConfigdurationLabel->SetParent(this->MyFrame->GetFrame());
  this->ConfigdurationLabel->Create(pvApp);
  this->ConfigdurationLabel->SetText( "Duration of each time step:" );
  this->ConfigdurationLabel->SetPadX(20);
  
  this->ValuedurationLabel->SetParent(this->MyFrame->GetFrame());
  this->ValuedurationLabel->Create(pvApp);
   
	//-------------------------------------------------------------------------------------      
	//Place components after acceptNumberTimeSteps
  this->PlaceComponents();
  this->ConfigureComponents(); 
}

//------------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::PlaceComponents()
{
  this->Script("pack %s -side left -fill x -expand true",  
    this->MyFrame->GetWidgetName());
  
  this->Script("grid %s -sticky w -columnspan 2",
  		this->ConfigByteOrder->GetWidgetName());
  
  this->Script("grid %s  -sticky w -columnspan 2",
  		this->ConfigTransientGeometry->GetWidgetName());
  
  this->Script("grid %s -sticky w -columnspan 2",
  	  this->ConfigScaleFactor->GetWidgetName());

  this->Script("grid %s -sticky w -row 3 -column 0 -columnspan 2",
    this->ConfigTimeStepLabel->GetWidgetName());  
  
  this->Script("grid %s -sticky w -row 3 -column 1",
    this->ValueTimeStepLabel->GetWidgetName());    
  
  this->Script("grid %s -sticky w -row 4 -column 0 -columnspan 2",
    this->ConfigtimeInitialLabel->GetWidgetName());  
  
  this->Script("grid %s -sticky w -row 4 -column 1",
    this->ValuetimeInitialLabel->GetWidgetName());  
  
  this->Script("grid %s -sticky w -row 5 -column 0 -columnspan 2",
    this->ConfigtimeFinalLabel->GetWidgetName());  
  
  this->Script("grid %s -sticky w -row 5 -column 1",
    this->ValuetimeFinalLabel->GetWidgetName());  
   
  this->Script("grid %s -sticky w -row 6 -column 0 -columnspan 2",
    this->ConfigdurationLabel->GetWidgetName());
  
  this->Script("grid %s -sticky w -row 6 -column 1",
    this->ValuedurationLabel->GetWidgetName());
    
  this->Script("grid %s -sticky w -columnspan 2",
  	this->ConfigRange->GetWidgetName());
    
  this->Script("grid %s -sticky ew -columnspan 2",
    this->ConfigButton->GetWidgetName());  
}

// ----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->MyFrame->GetWidgetName());
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::Accept()
{  	
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(0, 0, 1);		

  this->GetPVSource()->GetProxy()->UpdateVTKObjects();
}

//----------------------------------------------------------------------------
void vtkPVHMSolverToEnSight::Initialize()
{
	// Configurando Label`s de informação
	int NumberTimeSteps = 1; // número de passos de tempo
  double timeInitial = 0.0; // momento de início da simulação
  double timeFinal = 1.0; // momento de final da simulação
  double duration = 1.0; // duração de cada time step
  this->ConfigTimeStepsInformationCallback(&NumberTimeSteps, &timeInitial, &timeFinal, &duration);  
  
  char temp[128];
  sprintf(temp, "%d", NumberTimeSteps);
  this->ValueTimeStepLabel->SetText( temp );
  
  sprintf(temp, "%2.4f", timeInitial);
	this->ValuetimeInitialLabel->SetText( temp );
	
	sprintf(temp, "%2.4f", timeFinal);
	this->ValuetimeFinalLabel->SetText( temp );
	
	sprintf(temp, "%4.6f", duration);
	this->ValuedurationLabel->SetText( temp );
	
	// Configurando Widget de range
  this->ConfigRange->SetWholeRange(0, NumberTimeSteps);
  this->ConfigRange->SetRange(0, NumberTimeSteps);	
  
  //Configurando o fator de deslocamento
  this->ConfigScaleFactor->SetValue(1.0);
}  

