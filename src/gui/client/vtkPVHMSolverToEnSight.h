/*
 * $Id:$
 */
	/*=========================================================================
	
	  Project:   HeMoLab
	  Module:    vtkPVHMSolverToEnSight
	
	  Author: Eduardo Camargo
	
	=========================================================================*/
// .NAME vtkPVHMSolverToEnSight
// .SECTION Description
// Interface for Solver to EnSight filter

#ifndef VTKPVHMSOLVERTOENSIGHT_H_
#define VTKPVHMSOLVERTOENSIGHT_H_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkKWLabel;
class vtkKWLoadSaveButton;
class vtkKWFrameWithLabel;
class vtkKWRange;
class vtkKWCheckButtonWithLabel;
class vtkKWScaleWithEntry;
class vtkKWComboBoxWithLabel;


class VTK_EXPORT vtkPVHMSolverToEnSight : public vtkPVObjectWidget
{
public:
  static vtkPVHMSolverToEnSight *New();
  vtkTypeRevisionMacro(vtkPVHMSolverToEnSight,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Configure displacement factor
  void ConfigScaleFactorCallback();
  
  // Description:
  // Configure transient geometry for on/off
  void ConfigTransientGeometryCallback();
  
  // Description:
  // Configure range of time steps
  void ConfigWidgetRangeCallback();
  
  // Description:
  // Configure informations about time steps
  void ConfigTimeStepsInformationCallback(int *NumberTimeSteps, double *timeInitial, double *timeFinal, double *duration);
  
  // Description:  
	// Menu and load file callback	
	void ConfigButtonCallback();
	
  // Description:  
	// Configure Byte Order for files callback
  void ConfigByteOrderCallback();
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents();
  void ConfigureComponents(); 	
	
  // Description:
  // Call creation on the child.  
  virtual void 	Create (vtkKWApplication *app); 

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
    
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:  
  // Show that something was changed
  void SetValueChanged();
  
protected:
  vtkPVHMSolverToEnSight();
  virtual ~vtkPVHMSolverToEnSight();
  
  vtkKWFrameWithLabel *MyFrame;	 
  
  // Config File Parameters
  vtkKWLoadSaveButton*			ConfigButton;  
//  vtkKWLabel* 							ConfigLabel;
//  vtkKWEntry* 							ConfigEntry;
  vtkKWComboBoxWithLabel*		ConfigByteOrder;
  
  // Config Transient geometry
  vtkKWCheckButtonWithLabel* 			ConfigTransientGeometry;
  vtkKWScaleWithEntry*						ConfigScaleFactor;
  
  //Information fields
  vtkKWLabel*						ConfigTimeStepLabel;
  vtkKWLabel*						ConfigtimeInitialLabel;
  vtkKWLabel*						ConfigtimeFinalLabel;
  vtkKWLabel*						ConfigdurationLabel;
  vtkKWLabel*						ValueTimeStepLabel;
  vtkKWLabel*						ValuetimeInitialLabel;
  vtkKWLabel*						ValuetimeFinalLabel;
  vtkKWLabel*						ValuedurationLabel;
    
  //Config range parameters
  vtkKWRange*						ConfigRange;
	 	
private:  
  vtkPVHMSolverToEnSight(const vtkPVHMSolverToEnSight&); // Not implemented
  void operator=(const vtkPVHMSolverToEnSight&); // Not implemented
}; 

#endif /*VTKPVHMSOLVERTOENSIGHT_H_*/
