/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVHMSphereSourceWidget.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVHMSphereSourceWidget.h"

#include "vtkArrayMap.txx"
#include "vtkCamera.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWPushButton.h"
#include "vtkKWView.h"
#include "vtkObjectFactory.h"
#include "vtkPVApplication.h"
#include "vtkPVDisplayGUI.h"
#include "vtkPVDataInformation.h"
#include "vtkPVGenericRenderWindowInteractor.h"
#include "vtkPVInputMenu.h"
#include "vtkPVSource.h"
#include "vtkPVVectorEntry.h"
#include "vtkPVWindow.h"
#include "vtkPVXMLElement.h"
#include "vtkRenderer.h"
#include "vtkPVProcessModule.h"
#include "vtkPVWindow.h"
#include "vtkPVTraceHelper.h"

#include "vtkKWEvent.h"
//#include "vtkSMSphereWidgetProxy.h"
#include "vtkSM3DWidgetProxy.h"

#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMProxyManager.h"
#include "vtkSMProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkCommand.h"

vtkStandardNewMacro(vtkPVHMSphereSourceWidget);
vtkCxxRevisionMacro(vtkPVHMSphereSourceWidget, "$Revision: 1.66 $");

vtkCxxSetObjectMacro(vtkPVHMSphereSourceWidget, InputMenu, vtkPVInputMenu);

//*****************************************************************************
//----------------------------------------------------------------------------
vtkPVHMSphereSourceWidget::vtkPVHMSphereSourceWidget()
{
  int cc;

  this->InputMenu = 0;

  this->Labels[0] = vtkKWLabel::New();
  this->Labels[1] = vtkKWLabel::New();  
  for ( cc = 0; cc < 3; cc ++ )
    {
    this->CenterEntry[cc] = vtkKWEntry::New();
    this->CoordinateLabel[cc] = vtkKWLabel::New();
   }
  this->RadiusEntry = vtkKWEntry::New();
  this->NumberOfPointsLabel = vtkKWLabel::New();
  this->NumberOfPointsEntry = vtkKWEntry::New();
  this->CenterResetButton = vtkKWPushButton::New();
  
  this->SourceProxy = 0;
  this->SetWidgetProxyXMLName("SphereWidgetProxy");
}

//----------------------------------------------------------------------------
vtkPVHMSphereSourceWidget::~vtkPVHMSphereSourceWidget()
{
  int i;
  this->UnsetPropertyObservers();
  this->SetInputMenu(NULL);
  this->Labels[0]->Delete();
  this->Labels[1]->Delete();
  for (i=0; i<3; i++)
    {
    this->CenterEntry[i]->Delete();
    this->CoordinateLabel[i]->Delete();
    }
  this->RadiusEntry->Delete();
  this->NumberOfPointsLabel->Delete();
  this->NumberOfPointsEntry->Delete();
  this->CenterResetButton->Delete();
  
  if (this->SourceProxy)
    {
    vtkSMProxyManager* proxyM = vtkSMObject::GetProxyManager();
    const char* proxyName = proxyM->GetProxyName("sources", this->SourceProxy);
    if (proxyName)
      {
      proxyM->UnRegisterProxy("sources", proxyName);
      }
    this->UnregisterAnimateableProxies();
    this->SourceProxy->Delete();
    this->SourceProxy = 0;
    }
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::CenterResetCallback()
{
  vtkPVSource *input;
  double bds[6];

  if (this->PVSource == NULL)
    {
    vtkErrorMacro("PVSource has not been set.");
    return;
    }

  input = this->PVSource->GetPVInput(0);
  if (input == NULL)
    {
    return;
    }
  input->GetDataInformation()->GetBounds(bds);
  this->SetCenter(0.5*(bds[0]+bds[1]),
                  0.5*(bds[2]+bds[3]),
                  0.5*(bds[4]+bds[5]));

}


//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::ActualPlaceWidget()
{
  this->Superclass::ActualPlaceWidget();
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::Initialize()
{
  this->PlaceWidget();

  this->Accept();
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::ResetInternal()
{
  if ( ! this->ModifiedFlag )
    {
    return;
    }

  vtkSMDoubleVectorProperty* sdvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("Center"));
  if(sdvp)
    {
    double center[3];
    center[0] = sdvp->GetElement(0);
    center[1] = sdvp->GetElement(1);
    center[2] = sdvp->GetElement(2);
    this->SetCenterInternal(center[0],center[1],center[2]);
    }
  else
    {
    vtkErrorMacro("Could not find property Center for widget: "<< 
      this->SourceProxy->GetVTKClassName());
    }
  sdvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("Radius"));
  if(sdvp)
    {
    double radius = sdvp->GetElement(0);
    this->SetRadiusInternal(radius);
    }
  else
    {
    vtkErrorMacro("Could not find property Radius for widget: "<< 
      this->SourceProxy->GetVTKClassName());
    }
  this->Superclass::ResetInternal();
}

//---------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::Accept()
{
  int modFlag = this->GetModifiedFlag();
  double center[3];
  double radius;
  int sphereParticles;
  
  this->WidgetProxy->UpdateInformation();
  this->GetCenterInternal(center);
  radius = this->GetRadiusInternal();
  sphereParticles = this->GetNumberOfPointsInternal();

  vtkSMDoubleVectorProperty* sdvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("Center"));
  if (sdvp)
    {
    sdvp->SetElements3(center[0],center[1],center[2]);
    }
  else
    {
    vtkErrorMacro("Could not find property Center for widget: "
      << this->SourceProxy->GetVTKClassName());
    }
  
  sdvp = vtkSMDoubleVectorProperty::SafeDownCast( this->SourceProxy->GetProperty("Radius"));
  if (sdvp)
    {
    sdvp->SetElements1(radius);
    }
  else
    {
    vtkErrorMacro("Could not find property Radius for widget: "
      << this->SourceProxy->GetVTKClassName());
    }
  
  vtkSMIntVectorProperty* sivp = vtkSMIntVectorProperty::SafeDownCast(this->GetPVSource()->GetProxy()->GetProperty("NumberOfSphereParticles"));
  if(sivp)
  	{
  	sivp->SetElements1(sphereParticles);
  	}
  else
	  {
    vtkErrorMacro("Could not find property NumberOfSphereParticles for widget: "
      << this->GetPVSource()->GetProxy()->GetVTKClassName());
	  }
  this->GetPVSource()->GetProxy()->UpdateDataInformation(); 
  this->SourceProxy->UpdateVTKObjects();
  // 3DWidgets need to explictly call UpdateAnimationInterface on accept
  // since the animatable proxies might have been registered/unregistered
  // which needs to be updated in the Animation interface.
  this->GetPVApplication()->GetMainWindow()->UpdateAnimationInterface();
  this->ModifiedFlag = 0;
  
  // I put this after the accept internal, because
  // vtkPVGroupWidget inactivates and builds an input list ...
  // Putting this here simplifies subclasses AcceptInternal methods.
  if (modFlag)
    {
    vtkPVApplication *pvApp = this->GetPVApplication();
    ofstream* file = pvApp->GetTraceFile();
    if (file)
      {
      this->Trace(file);
      }
    }

  this->ValueChanged = 0;
}
//---------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::Trace(ofstream *file)
{
  double rad;
  double val[3];
  int cc;
  
  if ( ! this->GetTraceHelper()->Initialize(file))
    {
    return;
    }

  // Called to save the state of the widget's visibility
  this->Superclass::Trace(file);

  for ( cc = 0; cc < 3; cc ++ )
    {
    val[cc] = atof( this->CenterEntry[cc]->GetValue() );
    }
  *file << "$kw(" << this->GetTclName() << ") SetCenter "
        << val[0] << " " << val[1] << " " << val[2] << endl;

  rad = atof(this->RadiusEntry->GetValue());
  *file << "$kw(" << this->GetTclName() << ") SetRadius "
        << rad << endl;
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SaveInBatchScript(ofstream *file)
{
  if (!this->SourceProxy)
    {
    vtkErrorMacro("Source Proxy must be set to save to a batch script");
    return;
    }
  
  this->WidgetProxy->SaveInBatchScript(file);

  vtkClientServerID sphereID = this->SourceProxy->GetID(0);

  *file << endl;
  *file << "set pvTemp" << sphereID.ID
    << " [$proxyManager NewProxy sources HMSphereSource]"
    << endl;
  *file << "  $proxyManager RegisterProxy sources pvTemp"
    << sphereID.ID << " $pvTemp" << sphereID.ID
    << endl;
  *file << "  $pvTemp" << sphereID.ID << " UnRegister {}" << endl;
  
  vtkSMDoubleVectorProperty* sdvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("Center"));  
  if(sdvp)
    {
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Center] "
      << "SetElements3 " 
      << sdvp->GetElement(0) << " "
      << sdvp->GetElement(1) << " "
      << sdvp->GetElement(2)
      << endl;
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Center]"
      << " SetControllerProxy $pvTemp" << this->WidgetProxy->GetID(0) 
      << endl;
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Center]"
      << " SetControllerProperty [$pvTemp" << this->WidgetProxy->GetID(0)
      << " GetProperty Center]" << endl;
    }

  sdvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("Radius"));
  if (sdvp)
    {
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Radius] "
      << "SetElements1 "
      << sdvp->GetElement(0) << endl << endl;
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Radius]"
      << " SetControllerProxy $pvTemp" << this->WidgetProxy->GetID(0) 
      << endl;
    *file << "  [$pvTemp" << sphereID.ID << " GetProperty Radius]"
      << " SetControllerProperty [$pvTemp" << this->WidgetProxy->GetID(0)
      << " GetProperty Radius]" << endl;
    }
  *file << "  $pvTemp" << sphereID.ID << " UpdateVTKObjects" << endl;
  *file << endl;

}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "SourceProxy: " << this->SourceProxy << endl;
  os << indent << "InputMenu: " << this->InputMenu << endl;

}

//----------------------------------------------------------------------------
vtkPVWidget* vtkPVHMSphereSourceWidget::ClonePrototypeInternal(
  vtkPVSource* pvSource,
  vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map)
{
    vtkPVWidget* pvWidget = 0;
  // Check if a clone of this widget has already been created
  if ( map->GetItem(this, pvWidget) != VTK_OK )
    {
    // If not, create one and add it to the map
    pvWidget = this->NewInstance();
    map->SetItem(this, pvWidget);
    // Now copy all the properties
    this->CopyProperties(pvWidget, pvSource, map);

    vtkPVHMSphereSourceWidget* sw = vtkPVHMSphereSourceWidget::SafeDownCast(pvWidget);
    if(!sw)
      {
      vtkErrorMacro("Internal error. Could not downcast pointer.");
      pvWidget->Delete();
      return 0;
      }
     if (this->InputMenu)
      {
      // This will either clone or return a previously cloned
      // object.
      vtkPVInputMenu* im = this->InputMenu->ClonePrototype(pvSource, map);
      sw->SetInputMenu(im);
      im->Delete();
      }
    }
  else
    {
    // Increment the reference count. This is necessary
    // to make the behavior same whether a widget is created
    // or returned from the map. Always call Delete() after
    // cloning.
    pvWidget->Register(this);
    }
  return pvWidget;
}


//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetBalloonHelpString(const char *str)
{
  this->Superclass::SetBalloonHelpString(str);

  if (this->Labels[0])
    {
    this->Labels[0]->SetBalloonHelpString(str);
    }

  if (this->Labels[1])
    {
    this->Labels[1]->SetBalloonHelpString(str);
    }

  if (this->RadiusEntry)
    {
    this->RadiusEntry->SetBalloonHelpString(str);
    }

  if (this->CenterResetButton)
    {
    this->CenterResetButton->SetBalloonHelpString(str);
    }

  for (int i=0; i<3; i++)
    {
    if (this->CoordinateLabel[i])
      {
      this->CoordinateLabel[i]->SetBalloonHelpString(str);
      }
    if (this->CenterEntry[i])
      {
      this->CenterEntry[i]->SetBalloonHelpString(str);
      }
    }
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::ChildCreate(vtkPVApplication* pvApp)
{
  if ((this->GetTraceHelper()->GetObjectNameState() == 
       vtkPVTraceHelper::ObjectNameStateUninitialized ||
      this->GetTraceHelper()->GetObjectNameState() == 
       vtkPVTraceHelper::ObjectNameStateDefault) )
    {
    this->GetTraceHelper()->SetObjectName("HMSphereSource");
    this->GetTraceHelper()->SetObjectNameState(
      vtkPVTraceHelper::ObjectNameStateSelfInitialized);
    }

  this->SetFrameLabel("Sphere Widget");
  this->Labels[0]->SetParent(this->Frame);
  this->Labels[0]->Create(pvApp);
  this->Labels[0]->SetText("Center");
  this->Labels[1]->SetParent(this->Frame);
  this->Labels[1]->Create(pvApp);
  this->Labels[1]->SetText("Radius");
///////////////////////////////////////////////////////////  
  this->NumberOfPointsLabel->SetParent(this->Frame);
  this->NumberOfPointsLabel->Create(pvApp);
  this->NumberOfPointsLabel->SetText("Number of points");

  int i;
  for (i=0; i<3; i++)
    {
    this->CoordinateLabel[i]->SetParent(this->Frame);
    this->CoordinateLabel[i]->Create(pvApp);
    char buffer[3];
    sprintf(buffer, "%c", "xyz"[i]);
    this->CoordinateLabel[i]->SetText(buffer);
    }
  for (i=0; i<3; i++)
    {
    this->CenterEntry[i]->SetParent(this->Frame);
    this->CenterEntry[i]->Create(pvApp);
    }
  this->RadiusEntry->SetParent(this->Frame);
  this->RadiusEntry->Create(pvApp);
  /////////////////////////////////////////////////////////// 
  this->NumberOfPointsEntry->SetParent(this->Frame);
  this->NumberOfPointsEntry->Create(pvApp);
  vtkSMIntVectorProperty* p = vtkSMIntVectorProperty::SafeDownCast(this->GetPVSource()->GetProxy()->GetProperty("NumberOfSphereParticles"));
   if(p)
   	{
   	this->NumberOfPointsEntry->SetValueAsInt(p->GetElement(0));
   	}
///////////////////////////////////////////////////////////  
  
  this->Script("grid propagate %s 1",
    this->Frame->GetWidgetName());

  this->Script("grid x %s %s %s -sticky ew",
    this->CoordinateLabel[0]->GetWidgetName(),
    this->CoordinateLabel[1]->GetWidgetName(),
    this->CoordinateLabel[2]->GetWidgetName());
  this->Script("grid %s %s %s %s -sticky ew",
    this->Labels[0]->GetWidgetName(),
    this->CenterEntry[0]->GetWidgetName(),
    this->CenterEntry[1]->GetWidgetName(),
    this->CenterEntry[2]->GetWidgetName());
  this->Script("grid %s %s - - -sticky ew",
    this->Labels[1]->GetWidgetName(),
    this->RadiusEntry->GetWidgetName());
///////////////////////////////////////////////////////////  
  this->Script("grid %s %s - - -sticky ew",
    this->NumberOfPointsLabel->GetWidgetName(),
    this->NumberOfPointsEntry->GetWidgetName());
///////////////////////////////////////////////////////////

  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->Frame->GetWidgetName());
  this->Script("grid columnconfigure %s 1 -weight 2", 
    this->Frame->GetWidgetName());
  this->Script("grid columnconfigure %s 2 -weight 2", 
    this->Frame->GetWidgetName());
  this->Script("grid columnconfigure %s 3 -weight 2", 
    this->Frame->GetWidgetName());
///////////////////////////////////////////////////////////
  this->Script("grid columnconfigure %s 4 -weight 2", 
    this->Frame->GetWidgetName());
///////////////////////////////////////////////////////////
  
  for (i=0; i<3; i++)
    {
    this->Script("bind %s <Key> {%s SetValueChanged}",
      this->CenterEntry[i]->GetWidgetName(),
      this->GetTclName());
    this->Script("bind %s <FocusOut> {%s SetCenter}",
      this->CenterEntry[i]->GetWidgetName(),
      this->GetTclName());
    this->Script("bind %s <KeyPress-Return> {%s SetCenter}",
      this->CenterEntry[i]->GetWidgetName(),
      this->GetTclName());
    }
  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RadiusEntry->GetWidgetName(),
    this->GetTclName());
  this->Script("bind %s <FocusOut> {%s SetRadius}",
    this->RadiusEntry->GetWidgetName(),
    this->GetTclName());
  this->Script("bind %s <KeyPress-Return> {%s SetRadius}",
    this->RadiusEntry->GetWidgetName(),
    this->GetTclName()); 
  this->CenterResetButton->SetParent(this->Frame);
  this->CenterResetButton->Create(pvApp);
  this->CenterResetButton->SetText("Set Sphere Center to Center of Bounds");
  this->CenterResetButton->SetCommand(this, "CenterResetCallback"); 
  this->Script("grid %s - - - - -sticky ew", 
    this->CenterResetButton->GetWidgetName());
  
  
  this->Script("bind %s <Key> {%s SetValueChanged}",
      this->NumberOfPointsEntry->GetWidgetName(),
      this->GetTclName());
    this->Script("bind %s <FocusOut> {%s SetNumberOfPoints}",
      this->NumberOfPointsEntry->GetWidgetName(),
      this->GetTclName());
    this->Script("bind %s <KeyPress-Return> {%s SetNumberOfPoints}",
      this->NumberOfPointsEntry->GetWidgetName(),
      this->GetTclName()); 
  
  
  
  // Initialize the center of the sphere based on the input bounds.
  if (this->PVSource)
    {
    vtkPVSource *input = this->PVSource->GetPVInput(0);
    if (input)
      {
      double bds[6];
      input->GetDataInformation()->GetBounds(bds);
      this->SetCenter(0.5*(bds[0]+bds[1]), 0.5*(bds[2]+bds[3]),
        0.5*(bds[4]+bds[5]));
      this->SetRadius(0.5*(bds[1]-bds[0]));
      }
    }
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::Create( vtkKWApplication *app)
{
  this->Superclass::Create(app);


  static int proxyNum = 0;
  vtkSMProxyManager *pm = vtkSMObject::GetProxyManager();
  this->SourceProxy = pm->NewProxy("sources", "HMSphereSource");
  ostrstream str;
  str << "HMSphereSource" << proxyNum << ends;
  proxyNum++;
  pm->RegisterProxy("sources",str.str(),this->SourceProxy);
  delete[] str.str();

  this->SetupPropertyObservers();
  // Set up controller properties. Controller properties are set so 
  // that in the SM State, we can have a mapping from the widget to the 
  // controlled source.
  vtkSMProperty* p = this->SourceProxy->GetProperty("Center");
  p->SetControllerProxy(this->WidgetProxy);
  p->SetControllerProperty(this->WidgetProxy->GetProperty("Center"));

  p = this->SourceProxy->GetProperty("Radius");
  p->SetControllerProxy(this->WidgetProxy);
  p->SetControllerProperty(this->WidgetProxy->GetProperty("Radius"));
  
  // Seta as resoluções da esfera em 20, gerando um source com 362 pontos.
  // Se a resolução for alterada aqui deve ser tb na classe vtkHM3DParticlesTracingFilter (método InitializeParticles).
  vtkSMIntVectorProperty* thetaResolution = vtkSMIntVectorProperty::SafeDownCast(this->SourceProxy->GetProperty("ThetaResolution"));
  thetaResolution->SetElement(0, 20);
  vtkSMIntVectorProperty* phiResolution = vtkSMIntVectorProperty::SafeDownCast(this->SourceProxy->GetProperty("PhiResolution"));
  phiResolution->SetElement(0, 20);
  this->SourceProxy->UpdateInformation();
}
//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetupPropertyObservers()
{
  if (!this->SourceProxy)
    {
    return;
    }
  vtkSMProperty*p = this->SourceProxy->GetProperty("Center");
  if (p)
    {
    this->AddPropertyObservers(p);
    }
  p = this->SourceProxy->GetProperty("Radius");
  if (p)
    {
    this->AddPropertyObservers(p);
    }
  p = this->GetPVSource()->GetProxy()->GetProperty("NumberOfSphereParticles");
  if (p)
    {
    this->AddPropertyObservers(p);
    } 
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::UnsetPropertyObservers()
{
  if (!this->SourceProxy)
    {
    return;
    }
  vtkSMProperty*p = this->SourceProxy->GetProperty("Center");
  if (p)
    {
    this->RemovePropertyObservers(p);
    }
  p = this->SourceProxy->GetProperty("Radius");
  if (p)
    {
    this->RemovePropertyObservers(p);
    }
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
  if (vtkSM3DWidgetProxy::SafeDownCast(wdg))
    {
    if(l == vtkCommand::WidgetModifiedEvent)
      {
      double center[3];
      double radius;
      this->WidgetProxy->UpdateInformation();
      this->GetCenterInternal(center);
      radius = this->GetRadiusInternal();
      this->CenterEntry[0]->SetValueAsDouble(center[0]);
      this->CenterEntry[1]->SetValueAsDouble(center[1]);
      this->CenterEntry[2]->SetValueAsDouble(center[2]);
      this->RadiusEntry->SetValueAsDouble(radius);
      this->ModifiedCallback();
      this->ValueChanged = 0;
      }
    }
  if (vtkSMProperty::SafeDownCast(wdg))
    {
    switch(l)
      {
    case vtkCommand::ModifiedEvent:
      if (!this->ModifiedFlag)
        {
        // This is the reset to make the widget reflect the state of the properties.
        // If the widget has been modified, we don't reset it. This also helps
        // avoid the reset from being called while 'Accept'ing the values.
        this->ResetInternal();
        }
      break;
      }
    }
  this->Superclass::ExecuteEvent(wdg, l, p);
}

//----------------------------------------------------------------------------
int vtkPVHMSphereSourceWidget::ReadXMLAttributes(vtkPVXMLElement* element,
  vtkPVXMLPackageParser* parser)
{
  if(!this->Superclass::ReadXMLAttributes(element, parser)) { return 0; }  

  // Setup the InputMenu.
  const char* input_menu = element->GetAttribute("input_menu");
  if(!input_menu)
    {
    vtkErrorMacro("No input_menu attribute.");
    return 0;
    }

  vtkPVXMLElement* ame = element->LookupElement(input_menu);
  if (!ame)
    {
    vtkErrorMacro("Couldn't find InputMenu element " << input_menu);
    return 0;
    }
  
  vtkPVWidget* w = this->GetPVWidgetFromParser(ame, parser);
  vtkPVInputMenu* imw = vtkPVInputMenu::SafeDownCast(w);
  if(!imw)
    {
    if(w) { w->Delete(); }
    vtkErrorMacro("Couldn't get InputMenu widget " << input_menu);
    return 0;
    }
  imw->AddDependent(this);
  this->SetInputMenu(imw);
  imw->Delete();  
  return 1;
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::GetCenter(double pt[3])
{
  if(!this->IsCreated())
    {
    vtkErrorMacro("Not created yet");
    return;
    }
  this->WidgetProxy->UpdateInformation();
  this->GetCenterInternal(pt);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::GetCenterInternal(double pt[3])
{
  vtkSMDoubleVectorProperty* dvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("CenterInfo"));
  pt[0] = dvp->GetElement(0);
  pt[1] = dvp->GetElement(1);
  pt[2] = dvp->GetElement(2);
}

//----------------------------------------------------------------------------
double vtkPVHMSphereSourceWidget::GetRadius()
{
  if(!this->IsCreated())
    {
    vtkErrorMacro("Not created yet");
    return 0.0;
    }
  this->WidgetProxy->UpdateInformation();
  return this->GetRadiusInternal();
}

//----------------------------------------------------------------------------
double vtkPVHMSphereSourceWidget::GetRadiusInternal()
{
  vtkSMDoubleVectorProperty* dvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("RadiusInfo"));
  return dvp->GetElement(0);
}
//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetCenter(double x, double y, double z)
{
  this->SetCenterInternal(x, y, z);
  this->GetTraceHelper()->AddEntry("$kw(%s) SetCenter %f %f %f",
    this->GetTclName(), x, y, z);
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetCenter()
{
  if(!this->ValueChanged)
    {
    return;
    }
  double val[3];
  int cc;
  for ( cc = 0; cc < 3; cc ++ )
    {
    val[cc] = atof(this->CenterEntry[cc]->GetValue());
    } 
  this->SetCenterInternal(val[0],val[1],val[2]);
  this->Render();
  this->ValueChanged = 0;
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetCenterInternal(double x, double y, double z)
{
  if (!this->IsCreated())
    {
    vtkErrorMacro("Not created yet");
    return;
    }
  vtkSMDoubleVectorProperty* dvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Center"));
  dvp->SetElements3(x,y,z);
  this->WidgetProxy->UpdateVTKObjects();

  this->CenterEntry[0]->SetValueAsDouble(x);
  this->CenterEntry[1]->SetValueAsDouble(y);
  this->CenterEntry[2]->SetValueAsDouble(z);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetRadiusInternal(double r)
{
  if (!this->IsCreated())
    {
    vtkErrorMacro("Not created yet");
    return;
    }
  vtkSMDoubleVectorProperty* dvp = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("Radius"));
  dvp->SetElements1(r);
  this->WidgetProxy->UpdateVTKObjects(); 

  this->RadiusEntry->SetValueAsDouble(r);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetRadius(double r)
{
  this->SetRadiusInternal(r);
  this->GetTraceHelper()->AddEntry("$kw(%s) SetRadius %f ", this->GetTclName(), r);
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetRadius()
{
  if(this-ValueChanged == 0)
    {
    return;
    }
  double val;
  val = atof(this->RadiusEntry->GetValue());
  this->SetRadiusInternal(val);
  this->Render();
  this->ValueChanged = 0;
}

//----------------------------------------------------------------------------
int vtkPVHMSphereSourceWidget::GetNumberOfPointsInternal()
{
  vtkSMIntVectorProperty* dvp = vtkSMIntVectorProperty::SafeDownCast(this->GetPVSource()->GetProxy()->GetProperty("NumberOfSphereParticles"));
  return dvp->GetElement(0);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetNumberOfPointsInternal(int numPoints)
{
	if (!this->IsCreated())
    {
    vtkErrorMacro("Not created yet");
    return;
    }
  vtkSMIntVectorProperty* dvp = vtkSMIntVectorProperty::SafeDownCast(this->GetPVSource()->GetProxy()->GetProperty("NumberOfSphereParticles"));
  dvp->SetElements1(numPoints);
  this->GetPVSource()->GetProxy()->UpdateDataInformation(); 

  this->NumberOfPointsEntry->SetValueAsInt(numPoints);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetNumberOfPoints(int numPoints)
{
  this->SetNumberOfPointsInternal(numPoints);
//  this->GetTraceHelper()->AddEntry("$kw(%s) SetRadius %f ", this->GetTclName(), r);
  this->ModifiedCallback();
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::SetNumberOfPoints()
{
  if(this->ValueChanged == 0)
    {
    return;
    }
  double val;
  val = atof(this->NumberOfPointsEntry->GetValue());
  this->SetNumberOfPointsInternal(val);
//  this->Render();
  this->ValueChanged = 0;
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

  this->PropagateEnableState(this->InputMenu);
  this->PropagateEnableState(this->RadiusEntry);
  this->PropagateEnableState(this->CenterResetButton);

  int cc;
  for ( cc = 0; cc < 3; cc ++ )
    {
    this->PropagateEnableState(this->CenterEntry[cc]);
    this->PropagateEnableState(this->CoordinateLabel[cc]);
    }

  this->PropagateEnableState(this->Labels[0]);
  this->PropagateEnableState(this->Labels[1]);
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::RegisterAnimateableProxies()
{
  vtkSMProxyManager* pm = vtkSMObject::GetProxyManager();
  if (this->PVSource && this->SourceProxy)
    {
    vtkSMSourceProxy* sproxy = this->PVSource->GetProxy();
    if (sproxy)
      {
      const char* root = pm->GetProxyName("animateable", sproxy);
      if (root)
        {
        ostrstream animName;
        animName << root << ".HMSphereSource" << ends;
        pm->RegisterProxy(
          "animateable", animName.str(), this->SourceProxy);
        delete[] animName.str();
        }
      }
    }
}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::UnregisterAnimateableProxies()
{
  if (!this->SourceProxy)
    {
    return;
    }
  vtkSMProxyManager* proxyM = vtkSMObject::GetProxyManager();

  const char* proxyName = proxyM->GetProxyName("animateable", this->SourceProxy);
  if (proxyName)
    {
    proxyM->UnRegisterProxy("animateable", proxyName);
    }

}

//----------------------------------------------------------------------------
void vtkPVHMSphereSourceWidget::Update()
{
  vtkPVSource* input;
  double bds[6];

  this->Superclass::Update();
  //Input bounds may have changed so call place widget
  input = this->InputMenu->GetCurrentValue();
  if (input)
    {
    input->GetDataInformation()->GetBounds(bds);
    this->PlaceWidget(bds);
    this->Render();
    }
}
