/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVHMSphereSourceWidget.h,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPVHMSphereSourceWidget - A widget to manipulate a sphere source.
// .SECTION Description

#ifndef __vtkPVHMSphereSourceWidget_h
#define __vtkPVHMSphereSourceWidget_h

#include "vtkPV3DWidget.h"

class vtkPVSource;
class vtkKWEntry;
class vtkKWPushButton;
class vtkKWWidget;
class vtkKWLabel;
class vtkPVInputMenu;

class VTK_EXPORT vtkPVHMSphereSourceWidget : public vtkPV3DWidget
{
public:
  static vtkPVHMSphereSourceWidget* New();
  vtkTypeRevisionMacro(vtkPVHMSphereSourceWidget, vtkPV3DWidget);

  void PrintSelf(ostream& os, vtkIndent indent);
    
  // Description:
  // Callback that set the center to the middle of the bounds.
  void CenterResetCallback();

  // Description:
  // This class redefines SetBalloonHelpString since it
  // has to forward the call to a widget it contains.
  virtual void SetBalloonHelpString(const char *str);

  void SetCenter();
  void SetCenter(double,double,double);
  void SetCenter(double c[3]) { this->SetCenter(c[0], c[1], c[2]); }
  void GetCenter(double pts[3]);
  
  void SetRadius();
  void SetRadius(double);
  double GetRadius();
  
  void SetNumberOfPoints(int numPoints);
  void SetNumberOfPoints();

  // Description:
  // Called when the PVSources reset button is called.
  virtual void ResetInternal();

  // Description:
  // Initializes widget after creation
  virtual void Initialize();

  //BTX
  // Description:
  // Called when the PVSources accept button is called.
  //virtual void AcceptInternal(vtkClientServerID);
  virtual void Accept();
  //ETX

  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Returns the source proxy.
  virtual vtkSMProxy* GetProxyByName(const char*) { return reinterpret_cast<vtkSMProxy*>(this->SourceProxy); }

  // Description:
  // Called when the input changes (before accept).
  virtual void Update();

  // Description:
  // The input from the input menu is used to place the widget.
  virtual void SetInputMenu(vtkPVInputMenu*);
  vtkGetObjectMacro(InputMenu, vtkPVInputMenu);

  // Description:
  // Create the widgets.
  // Overloaded to create the ImplicitFunction proxy
  virtual void Create(vtkKWApplication *app);

  // Description:
  // Register the animatable proxies and make them avaiblable for animation.
  // Called by vtkPVSelectWidget when the widget is selected.
  virtual void EnableAnimation(){ this->RegisterAnimateableProxies();} ;

  // Description:
  // Unregister animatable proxies so that they are not available for
  // animation. Called by vtkPVSelectWidget when this widget is deselected.
  virtual void DisableAnimation() { this->UnregisterAnimateableProxies();} ;

protected:
  vtkPVHMSphereSourceWidget();
  ~vtkPVHMSphereSourceWidget();

  // Description:
  // These methods assume that the Property has been
  // updated before calling them; i.e. Property->UpdateInformation
  // has been invoked.  
  void GetCenterInternal(double pt[3]);
  double GetRadiusInternal();

  void SetCenterInternal(double,double,double);
  void SetCenterInternal(double c[3]) 
    { 
    this->SetCenterInternal(c[0], c[1], c[2]); 
    }
  void SetRadiusInternal(double);

  void SetNumberOfPointsInternal(int numPoints);
  int GetNumberOfPointsInternal();  
  
  // Description:
  // Call creation on the child.
  virtual void ChildCreate(vtkPVApplication*);

  // Description:
  // Execute event of the 3D Widget.
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);

  vtkKWEntry *CenterEntry[3];
  vtkKWEntry *RadiusEntry;
  vtkKWEntry *NumberOfPointsEntry;
  vtkKWPushButton *CenterResetButton;

  vtkKWLabel* Labels[2];
  vtkKWLabel* CoordinateLabel[3];
  vtkKWLabel* NumberOfPointsLabel;

  vtkPVInputMenu* InputMenu;

  vtkSMProxy *SourceProxy;

  // Description:
  // For saving the widget into a VTK tcl script.
  // This saves the implicit sphere.  Parts will share this
  // one sphere.
  virtual void SaveInBatchScript(ofstream *file);

  virtual void ActualPlaceWidget();

  int ReadXMLAttributes(vtkPVXMLElement* element,
                        vtkPVXMLPackageParser* parser);

  //BTX
  // Description:
  // Creates and returns a copy of this widget. It will create
  // a new instance of the same type as the current object
  // using NewInstance() and then copy some necessary state 
  // parameters.
  virtual vtkPVWidget* ClonePrototypeInternal(
    vtkPVSource* pvSource,
    vtkArrayMap<vtkPVWidget*, 
    vtkPVWidget*>* map);
  //ETX
  
  void SetupPropertyObservers();
  void UnsetPropertyObservers();
  void RegisterAnimateableProxies();
  void UnregisterAnimateableProxies();
private:
  vtkPVHMSphereSourceWidget(const vtkPVHMSphereSourceWidget&); // Not implemented
  void operator=(const vtkPVHMSphereSourceWidget&); // Not implemented
};

#endif
