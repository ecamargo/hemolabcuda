/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVHMStraightModelReader.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVHMStraightModelReader.h"

#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkPVHMStraightModelReader, "$Revision: 1.3 $");
vtkStandardNewMacro(vtkPVHMStraightModelReader);


//----------------------------------------------------------------------------
vtkPVHMStraightModelReader::vtkPVHMStraightModelReader()
{
  this->TimeStepRange[0] = 0;
  this->TimeStepRange[1] = 0;
}

//----------------------------------------------------------------------------
vtkPVHMStraightModelReader::~vtkPVHMStraightModelReader()
{
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "TimeStepRange: "
     << this->TimeStepRange[0] << " "
     << this->TimeStepRange[1] << "\n";
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReader::SetTimeStep(int index)
{
  this->SetRestrictionAsIndex("timestep", index);
}

//----------------------------------------------------------------------------
int vtkPVHMStraightModelReader::GetTimeStep()
{
  return this->GetRestrictionAsIndex("timestep");
}


void vtkPVHMStraightModelReader::SetupOutputInformation(vtkInformation *outInfo)
{
  this->Superclass::SetupOutputInformation(outInfo);

  int index = this->GetAttributeIndex("timestep");
  this->TimeStepRange[0] = 0;
  this->TimeStepRange[1] = this->GetNumberOfAttributeValues(index)-1;
  if (this->TimeStepRange[1] == -1)
    {
    this->TimeStepRange[1] = 0;
    }
}



