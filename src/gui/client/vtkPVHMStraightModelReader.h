/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVHMStraightModelReader.h,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPVHMStraightModelReader - ParaView-specific vtkXMLCollectionReader subclass
// .SECTION Description
// vtkPVHMStraightModelReader subclasses vtkXMLCollectionReader to add
// ParaView-specific methods.

#ifndef VTKPVHMSTRAIGHTMODELREADER_H_
#define VTKPVHMSTRAIGHTMODELREADER_H_

#include "vtkXMLCollectionReader.h" 

class VTK_EXPORT vtkPVHMStraightModelReader : public vtkXMLCollectionReader
{
public:
  static vtkPVHMStraightModelReader* New();
  vtkTypeRevisionMacro(vtkPVHMStraightModelReader,vtkXMLCollectionReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get/Set the required value for the timestep attribute.  The value
  // should be referenced by its index.  Only data sets matching this
  // value will be read.  An out-of-range index will remove the
  // restriction.
  void SetTimeStep(int index);
  int GetTimeStep();

  // Description:
  // Get the range of index values valid for the TimestepAsIndex
  // setting.
  vtkGetVector2Macro(TimeStepRange, int);

protected:
  vtkPVHMStraightModelReader();
  ~vtkPVHMStraightModelReader();

  // Set TimeStepRange
  virtual void SetupOutputInformation(vtkInformation *outInfo);

  // Save the range of valid timestep index values.
  int TimeStepRange[2];

private:
  vtkPVHMStraightModelReader(const vtkPVHMStraightModelReader&);  // Not implemented.
  void operator=(const vtkPVHMStraightModelReader&);  // Not implemented.
};

#endif /*VTKPVHMSTRAIGHTMODELREADER_H_*/
