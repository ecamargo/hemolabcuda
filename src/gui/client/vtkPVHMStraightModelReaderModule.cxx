/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVHMStraightModelReaderModule.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVHMStraightModelReaderModule.h"
#include "vtkHM1DStraightModelReader.h"

#include "vtkKWFrame.h"
#include "vtkObjectFactory.h"
#include "vtkPVApplication.h"
#include "vtkPVProcessModule.h"
#include "vtkPVRenderView.h"
#include "vtkPVScale.h"

#include "vtkDataSet.h"
#include "vtkErrorCode.h"
#include "vtkPLOT3DReader.h"
#include "vtkObjectFactory.h"
#include "vtkPVProcessModule.h"
#include "vtkPVDisplayGUI.h"
#include "vtkSMPart.h"
#include "vtkPVLabeledToggle.h"
#include "vtkPVSelectionList.h"
#include "vtkPVSourceCollection.h"
#include "vtkPVWidgetCollection.h"
#include "vtkPVWindow.h"
#include "vtkPVSourceNotebook.h"
#include "vtkSource.h"
#include "vtkStructuredGrid.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMProxyManager.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPVHMStraightModelReaderModule);
vtkCxxRevisionMacro(vtkPVHMStraightModelReaderModule, "$Revision: 1.15 $");

//----------------------------------------------------------------------------
vtkPVHMStraightModelReaderModule::vtkPVHMStraightModelReaderModule()
{
	this->PackFileEntry = 0;
  this->AlreadyAccepted = 0;
}

//----------------------------------------------------------------------------
vtkPVHMStraightModelReaderModule::~vtkPVHMStraightModelReaderModule()
{

}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReaderModule::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
// This method used to fix the output data type of clone.
// It does nothing now, so we should get rid of it........ !!!!!!!!
int vtkPVHMStraightModelReaderModule::Initialize(const char* fname, vtkPVReaderModule*& clone)
{
  int retVal = this->Superclass::Initialize(fname, clone);

  if (retVal != VTK_OK)
    {
    return retVal;
    }
  return VTK_OK;
}

//----------------------------------------------------------------------------
int vtkPVHMStraightModelReaderModule::ReadFileInformation(const char* fname)
{
  int retVal =  this->Superclass::ReadFileInformation(fname);
  if (retVal != VTK_OK)
    {
    return retVal;
    }
  
  // Re-initialize widgets to get the information from the reader.
  this->InitializeWidgets();

  return VTK_OK;
}

//----------------------------------------------------------------------------
int vtkPVHMStraightModelReaderModule::Finalize(const char* fname)
{
  vtkPVScale *scale = vtkPVScale::SafeDownCast( this->GetPVWidget("TimeStep") );
	// If we have more than 1 timestep, we need to behave as an advanced reader module.
  if(scale && scale->GetRangeMax() > 0)
    {
    return this->Superclass::Finalize(fname);
    }
  else
    {
    return this->vtkPVReaderModule::Finalize(fname);
    }
}

int vtkPVHMStraightModelReaderModule::GetNumberOfTimeSteps()
{
  vtkPVScale *scale = vtkPVScale::SafeDownCast( this->GetPVWidget("TimeStep") );
  if(scale && scale->GetRangeMax() > 0)
    {
    return static_cast<int>(scale->GetRangeMax() - scale->GetRangeMin()) + 1;
    }
  else
    {
    return 0;
    }
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReaderModule::SetRequestedTimeStep(int step)
{
  vtkPVScale *scale = vtkPVScale::SafeDownCast( this->GetPVWidget("TimeStep") );
  if(scale && scale->GetRangeMax() > 0)
    {
    scale->SetValue(step + scale->GetRangeMin());
    this->AcceptCallback();
    this->GetPVApplication()->GetMainView()->EventuallyRender();
    this->Script("update");
    }
  else
    {
    vtkErrorMacro("Cannot call SetRequestedTimeStep with no time steps.");
    }
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReaderModule::Accept(int hideFlag, int hideSource)
{
	vtkPVWindow* window = this->GetPVWindow();
  
  vtkProcessModule* pm = vtkProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule()); 

  this->UpdateVTKSourceParameters();
  vtkClientServerStream stream; 

  stream << vtkClientServerStream::Invoke 
         << this->GetVTKSourceID(0) << "GetFileName" 
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream, 0);

  int canread = 1;
  if(!canread)
    {
    vtkErrorMacro(<< "Can not read input file. Try changing parameters.");
    if (this->Initialized)
      {
      this->UnGrabFocus();
      this->Notebook->SetAcceptButtonColorToUnmodified();
      }
#ifdef _WIN32
    this->Script("%s configure -cursor arrow", window->GetWidgetName());
#else
    this->Script("%s configure -cursor left_ptr", window->GetWidgetName());
#endif  
    return;
    }

  this->AlreadyAccepted = 1;

  this->UpdateEnableState();

  this->Superclass::Accept(hideFlag, hideSource);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelReaderModule::UpdateEnableState()
{
	vtkSMSourceProxy *sProxy = this->GetProxy();

  this->Superclass::UpdateEnableState();

  if ( !this->AlreadyAccepted )
    {
    return;
    }

  vtkPVWidget *pvw = 0;
  
  this->Widgets->InitTraversal();
  int i;
  for (i = 0; i < this->Widgets->GetNumberOfItems(); i++)
    {
    pvw = static_cast<vtkPVWidget*>(this->Widgets->GetNextItemAsObject());
    vtkPVLabeledToggle* tog = vtkPVLabeledToggle::SafeDownCast(pvw);
    if (tog)
      {
      tog->SetEnabled(0);
      }

    vtkPVSelectionList* list = vtkPVSelectionList::SafeDownCast(pvw);
    if (list)
      {
      list->SetEnabled(0);
      }
    }
}
