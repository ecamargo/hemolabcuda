/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVAdvancedReaderModule.h,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPVHMStraightModelReaderModule - Module representing an "advanced" reader
// .SECTION Description
// The class vtkPVHMStraightModelReaderModule is used to represent an "advanced"
// reader (or a pipeline which contains a reader). An advanced reader is
// one which allows the user to pre-select certain attributes (for example,
// list of arrays to be loaded) before reading the whole file.  This is
// done by reading some header information during UpdateInformation.  The
// main difference between vtkPVHMStraightModelReaderModule and vtkPVAdvancedReaderModule
// is that the former does not automatically call Accept after the filename
// is selected. Instead, it prompts the user for more selections. The file
// is only fully loaded when the user presses Accept.
//
// .SECTION See also
// vtkPVAdvancedReaderModule vtkPVEnSightReaderModule vtkPVPLOT3DReaderModule

#ifndef VTKPVHMSTRAIGHTMODELREADERMODULE_H_
#define VTKPVHMSTRAIGHTMODELREADERMODULE_H_

#include "vtkPVAdvancedReaderModule.h"

class vtkPVScale;

class VTK_EXPORT vtkPVHMStraightModelReaderModule : public vtkPVAdvancedReaderModule
{
public:
  static vtkPVHMStraightModelReaderModule* New();
  vtkTypeRevisionMacro(vtkPVHMStraightModelReaderModule, vtkPVAdvancedReaderModule);
  void PrintSelf(ostream& os, vtkIndent indent);  
  
  // Description:
  // Used mainly by the scripting interface, these three methods are
  // normally called in order during the file opening process. Given
  // the reader module name, InitializeReadCustom() returns a clone
  // which can be passed to ReadFileInformation() and FinalizeRead()
  // to finish the reading process.
  virtual int Initialize(const char* fname, vtkPVReaderModule*& newModule);
  virtual int Finalize  (const char* fname);
  virtual int ReadFileInformation(const char* fname);
  
  // Description:
  // Get the number of time steps that can be provided by this reader.
  // Timesteps are available either from an animation file or from a
  // time-series of files as detected by the file entry widget.
  // Returns 0 if time steps are not available, and the number of
  // timesteps otherwise.
  virtual int GetNumberOfTimeSteps();
  
  // Description:
  // Set the time step that should be provided by the reader.  This
  // value is ignored unless GetNumberOfTimeSteps returns 1 or more.
  virtual void SetRequestedTimeStep(int);  
  
  // Description:
  virtual void Accept() { this->Accept(0); }
  virtual void Accept(int hideFlag) { this->Accept(hideFlag, 1); }
  virtual void Accept(int hideFlag, int hideSource);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();  
  
protected:
  vtkPVHMStraightModelReaderModule();
  ~vtkPVHMStraightModelReaderModule();
  
  int AlreadyAccepted;
  
private:
  vtkPVHMStraightModelReaderModule(const vtkPVHMStraightModelReaderModule&); // Not implemented
  void operator=(const vtkPVHMStraightModelReaderModule&); // Not implemented
};

#endif /*VTKPVHMSTRAIGHTMODELREADERMODULE_H_*/
