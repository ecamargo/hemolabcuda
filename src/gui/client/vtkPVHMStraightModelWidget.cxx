#include "vtkPVHMStraightModelWidget.h"
#include "vtkPVHMHeartWidget.h"
#include "vtkPVHMTerminalWidget.h"
#include "vtkPVHMNodeWidget.h"
#include "vtkPVHMSegmentWidget.h"
#include "vtkPVHMElementWidget.h"
#include "vtkPVHMPlotWidget.h"
#include "vtkPVHMGeometryWidget.h"
#include "vtkPVHMMultipleTerminalWidget.h"
#include "vtkPVHMMultipleSegmentWidget.h"
#include "vtkPVHMSubTreeMesh.h"
#include "vtkPVHMInterventionsWidget.h"
#include "vtkKWHM1DXYPlotWidget.h"
#include "vtkKWHM1DXYAnimatedPlotWidget.h"
#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkHMUnstructuredGridTo1DModel.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DHeMoLabReader.h"
#include "vtkHMDefineVariables.h"
#include "vtkObjectFactory.h"
#include "vtkPVApplication.h"
#include "vtkPVDisplayGUI.h"
#include "vtkPVDataInformation.h"
#include "vtkPVGenericRenderWindowInteractor.h"
#include "vtkPVProcessModule.h"
#include "vtkPVSource.h"
#include "vtkPVWindow.h"
#include "vtkPVXMLElement.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVColorMap.h"
#include "vtkPVColorSelectionWidget.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWIcon.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWScale.h"
#include "vtkKWCheckButton.h"
#include "vtkKWScaleWithEntrySet.h"
#include "vtkKWMenuButton.h"
#include "vtkKWThumbWheel.h"
#include "vtkKWProgressGauge.h"
#include "vtkKWRadioButton.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkSMProxyManager.h"
#include "vtkSMProxy.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMDataObjectDisplayProxy.h"
#include "vtkSMPart.h"
#include "vtkDataSet.h"
#include "vtkCommand.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "vtkStringArray.h"
#include <vtksys/SystemTools.hxx>
#include "vtkIdList.h"
#include "vtkDoubleArray.h"
#include <math.h>
#include <string>
#include "vtkHM1DKeepRemoveFilter.h"


#include "resources/1DStraightModelEditionModeButton.h"
#include "resources/1DStraightModelViewerModeButton.h"
#include "resources/1DStraightModelOutputModeButton.h"
#include "resources/1DStraightModelToDetachSubTreeButton.h"
#include "resources/1DStraightModelElementButton.h"
#include "resources/1DStraightModelSegmentButton.h"
#include "resources/1DStraightModelGenerateFiles.h"
#include "resources/1DStraightModelSpecialToolsButton.h"
#include "resources/1DStraightModelRunButton.h"

//include de imagens para a barra de edição
#include "resources/images-EditToolbar/vtkHM1DStraightModelEditGeometryButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelAddSegmentButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelAddTerminalButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelDeleteButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelMoveButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelCheckConsistenceButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelCopySegmentButton.h"
#include "resources/images-EditToolbar/vtkHM1DStraightModelFlipButton.h"

//include de imagens para a barra de intervenções cirúrgicas
#include "resources/images-SurgicalInterventionsToolbar/vtkHM1DStraightModelClipButton.h"
#include "resources/images-SurgicalInterventionsToolbar/vtkHM1DStraightModelDeleteInterventionButton.h"
#include "resources/images-SurgicalInterventionsToolbar/vtkHM1DStraightModelStentButton.h"

//include de imagens para a barra de patologias
#include "resources/images-PathologyToolbar/vtkHM1DStraightModelStenosisButton.h"
#include "resources/images-PathologyToolbar/vtkHM1DStraightModelAneurysmButton.h"
//#include "resources/images-SurgicalInterventionsToolbar/vtkHM1DStraightModelAgingButton.h"

vtkStandardNewMacro(vtkPVHMStraightModelWidget);
vtkCxxRevisionMacro(vtkPVHMStraightModelWidget, "$Revision: 1.73 $");

//----------------------------------------------------------------------------
vtkPVHMStraightModelWidget::vtkPVHMStraightModelWidget()
{
  this->SetWidgetProxyXMLName("HMStraightModelWidgetProxy");

  // StraightModel Widgets
  this->SetWidgetMode((int) MODE_VISUALIZATION_VAL);

  this->ObjectIDLabel = vtkKWLabel::New();
  this->ObjectIDValue = vtkKWLabel::New();
  //this->ObjectIdLabelText = "Object ID:";
  //this->ObjectIdValueText = "(None)";

  this->ObjectTypeLabel = vtkKWLabel::New();
  this->ObjectTypeValue = vtkKWLabel::New();
  //this->ObjectTypeLabelText = "Object Type: ";
  //this->ObjectTypeValueText = "(None)";

  this->ModelNameLabel = vtkKWLabel::New();
  this->ModelNameValue = vtkKWLabel::New();
  //this->ModelNameLabelText = "Model name:";
  s.clear();
  s.append("Model name:");
  //this->ModelNameValueText = "(None)";

  this->VisualizationStateVisibility = 1;
  this->VisualizationStateOptionMenu = vtkKWMenuButtonWithLabel::New();

  this->ElementWidget  = vtkPVHMElementWidget::New();
  this->SegmentWidget  = vtkPVHMSegmentWidget::New();
  this->TerminalWidget = vtkPVHMTerminalWidget::New();
  this->HeartWidget		 = vtkPVHMHeartWidget::New();
  this->NodeWidget  	 = vtkPVHMNodeWidget::New();
  this->PlotWidget  	 = vtkPVHMPlotWidget::New();
  this->MultipleTerminalWidget = vtkPVHMMultipleTerminalWidget::New();
  this->MultipleSegmentWidget = vtkPVHMMultipleSegmentWidget::New();
  this->InterventionsWidget = vtkPVHMInterventionsWidget::New();
  this->SubTreeMeshWidget = vtkPVHMSubTreeMesh::New();

  this->PlotOptions		 = vtkIntArray::New();
  this->PlotOptions->SetNumberOfValues(PLOTS);
  this->PlotOptions->SetValue(0, 1);
  this->PlotOptions->SetValue(1, 0);
  this->PlotOptions->SetValue(2, 0);
  this->PlotOptions->SetValue(3, 0);
  this->PlotOptions->SetValue(4, 0);
  this->PlotOptions->SetValue(5, 0);

  this->AnimatedPlotOptions		 = vtkIntArray::New();
  this->AnimatedPlotOptions->SetNumberOfValues(PLOTS);
  this->AnimatedPlotOptions->SetValue(0, 1);
  this->AnimatedPlotOptions->SetValue(1, 0);
  this->AnimatedPlotOptions->SetValue(2, 0);
  this->AnimatedPlotOptions->SetValue(3, 0);
  this->AnimatedPlotOptions->SetValue(4, 0);
  this->AnimatedPlotOptions->SetValue(5, 0);

  bool disableUI = true;

  this->SegmentsNumber = vtkKWLabel::New();
  this->TerminalsNumber = vtkKWLabel::New();
  this->NodesNumber = vtkKWLabel::New();

  this->ResistanceEq = vtkKWLabel::New();
  this->CapacitanceEq = vtkKWLabel::New();
  this->MaxRadius = vtkKWLabel::New();
  this->MinRadius = vtkKWLabel::New();
  this->MaxWall = vtkKWLabel::New();
  this->MinWall = vtkKWLabel::New();

  this->ToolBar = vtkKWToolbar::New();

  // como padrao para visualizacao e edicao o modo Segmento deve estar selecionado
  this->SegmentWidget->SetElementSelected(0);
  this->SegmentWidget->SetSegmentSelected(1);

  this->InputVisualizationModeButton = vtkKWRadioButton::New();
  this->EditionModeButton = vtkKWRadioButton::New();
  this->PlotModeButton = vtkKWRadioButton::New();
  this->ToDetachSubTreeButton = vtkKWRadioButton::New();
  this->EditElemButton = vtkKWRadioButton::New();
  this->EditSegButton= vtkKWRadioButton::New();
  this->SpecialToolsButton = vtkKWRadioButton::New();

  this->GenerateSolverFilesButton =vtkKWPushButton::New();

  this->InputVisualizationModeButtonIcon = vtkKWIcon::New();
  this->EditionModeButtonIcon = vtkKWIcon::New();
  this->PlotModeButtonIcon = vtkKWIcon::New();
  this->ToDetachSubTreeButtonIcon = vtkKWIcon::New();
  this->EditElemButtonIcon = vtkKWIcon::New();
  this->EditSegButtonIcon = vtkKWIcon::New();
  this->GenerateSolverFilesIcon = vtkKWIcon::New();
  this->SpecialToolsIcon = vtkKWIcon::New();

  this->GeneralConfiguration =  NULL;

  this->XYPlotWidget = vtkKWHM1DXYPlotWidget::New();
  this->XYAnimatedPlotWidget = vtkKWHM1DXYAnimatedPlotWidget::New();

  this->pvApp = NULL;

  this->EditToolbar = vtkKWToolbar::New();

  this->EditRadioButtonSet = vtkKWRadioButtonSet::New();

  this->EditGeometryIcon = vtkKWIcon::New();
  this->AddSegmentIcon = vtkKWIcon::New();
  this->AddTerminalIcon = vtkKWIcon::New();
  this->DeleteIcon = vtkKWIcon::New();
  this->MoveIcon = vtkKWIcon::New();
  this->CheckConsistenceIcon = vtkKWIcon::New();
  this->CopySegmentIcon = vtkKWIcon::New();
  this->FlipIcon = vtkKWIcon::New();
  this->RestartIcon = vtkKWIcon::New();

  this->SurgicalInterventionsRadioButtonSet = vtkKWRadioButtonSet::New();
  this->SurgicalInterventionsLabel = vtkKWLabel::New();

  this->ClipIcon = vtkKWIcon::New();
  this->StentIcon = vtkKWIcon::New();
  this->StenosisIcon = vtkKWIcon::New();
  this->AneurysmIcon = vtkKWIcon::New();
  this->DeleteInterventionIcon = vtkKWIcon::New();

  this->EditionMode = vtkPVHMStraightModelWidget::EditGeometry;
  this->SurgicalInterventionsMode = vtkPVHMStraightModelWidget::none;

  this->XScaleThumbWheel = vtkKWThumbWheel::New();
  this->YScaleThumbWheel = vtkKWThumbWheel::New();
  this->ZScaleThumbWheel = vtkKWThumbWheel::New();

  this->ScaleFrame = vtkKWFrameWithLabel::New();

  this->SubTreePropFrame = vtkKWFrameWithLabel::New();

  this->XSizeLabel = vtkKWLabel::New();
  this->YSizeLabel = vtkKWLabel::New();
  this->ZSizeLabel = vtkKWLabel::New();

  this->XSizeEntry = vtkKWEntry::New();
  this->YSizeEntry = vtkKWEntry::New();
  this->ZSizeEntry = vtkKWEntry::New();

  this->TreeVolumeLabel = vtkKWLabel::New();


  this->SmallerSegmentNameLabel = vtkKWLabel::New();
  this->SmallerSegmentNameEntry = vtkKWEntry::New();
  this->SmallerSegmentLengthLabel = vtkKWLabel::New();
  this->SmallerSegmentLengthEntry = vtkKWEntry::New();

  this->LengthElementLabel = vtkKWLabel::New();
  this->LengthElementEntry = vtkKWEntry::New();

  this->HighlightSmallerSegmentButton = vtkKWPushButton::New();
  this->ConfigureDefaultLengthOfElementButton = vtkKWPushButton::New();

  this->AllSegmentsNameLabel = vtkKWLabel::New();
  this->AllSegmentsNameComboBox = vtkKWComboBox::New();


  this->GeneralInfoFrame = vtkKWFrameWithLabel::New();
  this->GeneralFrame =  vtkKWFrame::New();

  this->GeneralFrame2 =  vtkKWFrame::New();

  this->GeneralInfoSeparator = vtkKWSeparator::New();
  this->GeneralInfoSeparator->SetOrientationToHorizontal();

  this->CapEq = 0.0;
  this->ResEq = 0.0;


  this->SimulationLabel = vtkKWLabel::New();

  this->SimulationToolBar = vtkKWToolbar::New();

  this->RunIcon = vtkKWIcon::New();

  this->Threader = vtkMultiThreader::New();
 	this->thread_id = -1;
 	for (int i=0; i< 512; i++)
 		this->SolverPath[i]=0;

	this->ExecuteSolverPushButton  = vtkKWPushButton::New();
	this->ReadSimulationButton = vtkKWLoadSaveButton::New();

  this->ExportDialog = vtkKWLoadSaveDialog::New();

  this->KeepRemoveFilterMode = false;
  this->RemoveFilterRadioButton = vtkKWRadioButton::New();
  this->KeepFilterRadioButton = vtkKWRadioButton::New();
}


//----------------------------------------------------------------------------
vtkPVHMStraightModelWidget::~vtkPVHMStraightModelWidget()
{
  if( this->XYPlotWidget )
    {
    this->XYPlotWidget->Delete();
    this->XYPlotWidget = NULL;
    }

  this->PlotOptions->Delete();

  if( this->XYAnimatedPlotWidget )
    {
    this->XYAnimatedPlotWidget->Delete();
    this->XYAnimatedPlotWidget = NULL;
    }

  this->AnimatedPlotOptions->Delete();

  this->ElementWidget->Delete();
  this->SegmentWidget->Delete();
  this->TerminalWidget->Delete();
  this->HeartWidget->Delete();
  this->NodeWidget->Delete();
  this->PlotWidget->Delete();
  this->MultipleTerminalWidget->Delete();
  this->MultipleSegmentWidget->Delete();
  this->InterventionsWidget->Delete();
  this->SubTreeMeshWidget->Delete();

  this->ObjectIDLabel->Delete();
  this->ObjectIDLabel = 0;
  this->ObjectIDValue->Delete();
  this->ObjectIDValue = 0;

  this->ObjectTypeLabel->Delete();
  this->ObjectTypeLabel = 0;
  this->ObjectTypeValue->Delete();
  this->ObjectTypeValue = 0;

  this->ModelNameLabel->Delete();
  this->ModelNameLabel = 0;
  this->ModelNameValue->Delete();
  this->ModelNameValue = 0;

  if (this->VisualizationStateOptionMenu)
    this->VisualizationStateOptionMenu->Delete();

  this->SegmentsNumber->Delete();
  this->TerminalsNumber->Delete();
  this->NodesNumber->Delete();

  this->ResistanceEq->Delete();
  this->CapacitanceEq->Delete();
  this->MaxRadius->Delete();
  this->MinRadius->Delete();
  this->MaxWall->Delete();
  this->MinWall->Delete();

  this->ToolBar->Delete();
  this->ToolBar = NULL;

  this->InputVisualizationModeButton->Delete();
  this->EditionModeButton->Delete();
  this->PlotModeButton->Delete();
  this->ToDetachSubTreeButton->Delete();
  this->EditElemButton->Delete();
  this->EditSegButton->Delete();
  this->SpecialToolsButton->Delete();

  this->GenerateSolverFilesButton->Delete();

  this->InputVisualizationModeButtonIcon->Delete();
  this->EditionModeButtonIcon->Delete();
  this->PlotModeButtonIcon->Delete();
  this->ToDetachSubTreeButtonIcon->Delete();
  this->EditElemButtonIcon->Delete();
  this->EditSegButtonIcon->Delete();
  this->GenerateSolverFilesIcon->Delete();
  this->SpecialToolsIcon->Delete();

  if (this->GeneralConfiguration)
    {
    this->GeneralConfiguration->Withdraw();
    this->GeneralConfiguration->Delete();
    this->GeneralConfiguration = NULL;
    }

  this->EditToolbar->UnpackChildren();
  this->EditToolbar->Unpack();
  this->EditToolbar->EnabledOff();

  this->EditToolbar->Delete();

  this->EditRadioButtonSet->UnpackChildren();
  this->EditRadioButtonSet->Unpack();
  this->EditRadioButtonSet->RemoveAllChildren();
  this->EditRadioButtonSet->EnabledOff();
  this->EditRadioButtonSet->Delete();

  this->EditGeometryIcon->Delete();
  this->AddSegmentIcon->Delete();
  this->AddTerminalIcon->Delete();
  this->DeleteIcon->Delete();
  this->MoveIcon->Delete();
  this->CheckConsistenceIcon->Delete();
  this->CopySegmentIcon->Delete();
  this->FlipIcon->Delete();
  this->RestartIcon->Delete();

  this->SurgicalInterventionsRadioButtonSet->UnpackChildren();
  this->SurgicalInterventionsRadioButtonSet->Unpack();
  this->SurgicalInterventionsRadioButtonSet->RemoveAllChildren();
  this->SurgicalInterventionsRadioButtonSet->EnabledOff();
  this->SurgicalInterventionsRadioButtonSet->Delete();

  this->SurgicalInterventionsLabel->Delete();

  this->ClipIcon->Delete();
  this->StentIcon->Delete();
  this->StenosisIcon->Delete();
  this->AneurysmIcon->Delete();
  this->DeleteInterventionIcon->Delete();

  this->XScaleThumbWheel->Delete();
  this->YScaleThumbWheel->Delete();
  this->ZScaleThumbWheel->Delete();

  this->ScaleFrame->Delete();

  this->SubTreePropFrame->Delete();

  this->XSizeLabel->Delete();
  this->YSizeLabel->Delete();
  this->ZSizeLabel->Delete();

  this->XSizeEntry->Delete();
  this->YSizeEntry->Delete();
  this->ZSizeEntry->Delete();

  this->TreeVolumeLabel->Delete();


  this->SmallerSegmentNameLabel->Delete();
  this->SmallerSegmentNameEntry->Delete();
  this->SmallerSegmentLengthLabel->Delete();
  this->SmallerSegmentLengthEntry->Delete();
  this->LengthElementLabel->Delete();
  this->LengthElementEntry->Delete();

  this->HighlightSmallerSegmentButton->Delete();
  this->ConfigureDefaultLengthOfElementButton->Delete();

  this->AllSegmentsNameLabel->Delete();
  this->AllSegmentsNameComboBox->Delete();

  this->GeneralInfoFrame->Delete();

  this->GeneralFrame->Delete();
  this->GeneralFrame2->Delete();

  this->GeneralInfoSeparator->Delete();

  this->SimulationToolBar->Delete();
  this->SimulationToolBar = NULL;

  this->SimulationLabel->Delete();

  if (this->thread_id != -1)
	  this->Threader->TerminateThread(this->thread_id);
	this->Threader->Delete();

	this->ExecuteSolverPushButton->Delete();
  this->ReadSimulationButton->Delete();

  this->RunIcon->Delete();

  this->ExportDialog->Delete();
  this->ExportDialog= NULL;

  this->RemoveFilterRadioButton->Delete();
  this->KeepFilterRadioButton->Delete();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::GetObjectTypeName(char *type, int id)
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  switch(id)
    {
    case NODE:
      strcpy(type, "Node");
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(1);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      break;
    case TERMINAL:
      strcpy(type, "Terminal");
      this->TerminalWidget->SetWidgetVisibility(1);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      //se o modo de selecao de segmentos, e o modo de Destaque de sub-arvore estiver setado
      //sera mostrada o widget de edicao de multiplos  terminais e segmentos
      if ( (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL) && ( !this->KeepRemoveFilterMode ) )
        {
        this->TerminalWidget->SetWidgetVisibility(0);
        this->SubTreeMeshWidget->SetWidgetVisibility(1);
        this->MultipleTerminalWidget->SetWidgetVisibility(1);
        this->MultipleSegmentWidget->SetWidgetVisibility(1);
        }
      break;
    case HEART:
      strcpy(type, "Heart");
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(1);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      // Se modo de destaque de sub-arvore sera mostrada o widget de edicao de multiplos segmentos
      if ((this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL) && ( !this->KeepRemoveFilterMode ) )
        {
        this->SubTreeMeshWidget->SetWidgetVisibility(1);
        this->MultipleTerminalWidget->SetWidgetVisibility(1);
        this->MultipleSegmentWidget->SetWidgetVisibility(1);
        this->HeartWidget->SetWidgetVisibility(0);
        }
      break;
    case SEGMENT:
      strcpy(type, "Element");
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);

      if ( this->SegmentWidget->GetElementSelected() )
        {
        this->ElementWidget->SetWidgetVisibility(1);
        this->SegmentWidget->SetWidgetVisibility(0);
        }
      else
        {
        if ( (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL ) && (!this->KeepRemoveFilterMode) )
          {
          this->ElementWidget->SetWidgetVisibility(0);
          this->SegmentWidget->SetWidgetVisibility(0);
          this->SubTreeMeshWidget->SetWidgetVisibility(1);
          this->MultipleTerminalWidget->SetWidgetVisibility(1);
          this->MultipleSegmentWidget->SetWidgetVisibility(1);
          }
        //Se a tecla Ctrl estiver pressionada
        else if (proxy->CrtlKeyPressed())
          {
          this->ElementWidget->SetWidgetVisibility(0);
          this->SegmentWidget->SetWidgetVisibility(0);
          this->SubTreeMeshWidget->SetWidgetVisibility(1);
          this->MultipleTerminalWidget->SetWidgetVisibility(0);
          this->MultipleSegmentWidget->SetWidgetVisibility(1);
          }
        else
          {
          this->ElementWidget->SetWidgetVisibility(0);
          this->SegmentWidget->SetWidgetVisibility(1);
          }
        }
        break;

    case CLIP:
      strcpy(type, "Clip");
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      break;

    case STENT:
      strcpy(type, "Stent");
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(1);
      if ( (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL) && (!this->KeepRemoveFilterMode) )
        {
        this->SubTreeMeshWidget->SetWidgetVisibility(1);
        this->MultipleTerminalWidget->SetWidgetVisibility(1);
        this->MultipleSegmentWidget->SetWidgetVisibility(1);
        this->InterventionsWidget->SetWidgetVisibility(0);
        }
      break;

  case STENOSIS:
    strcpy(type, "Stenosis");
    this->TerminalWidget->SetWidgetVisibility(0);
    this->NodeWidget->SetWidgetVisibility(0);
    this->SegmentWidget->SetWidgetVisibility(0);
    this->ElementWidget->SetWidgetVisibility(0);
    this->PlotWidget->SetWidgetVisibility(0);
    this->HeartWidget->SetWidgetVisibility(0);
    this->SubTreeMeshWidget->SetWidgetVisibility(0);
    this->MultipleTerminalWidget->SetWidgetVisibility(0);
    this->MultipleSegmentWidget->SetWidgetVisibility(0);
    this->InterventionsWidget->SetWidgetVisibility(1);
    if ( (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL) && (!this->KeepRemoveFilterMode) )
      {
      this->SubTreeMeshWidget->SetWidgetVisibility(1);
      this->MultipleTerminalWidget->SetWidgetVisibility(1);
      this->MultipleSegmentWidget->SetWidgetVisibility(1);
      this->InterventionsWidget->SetWidgetVisibility(0);
      }
    break;

  case ANEURYSM:
    strcpy(type, "Aneurysm");
    this->TerminalWidget->SetWidgetVisibility(0);
    this->NodeWidget->SetWidgetVisibility(0);
    this->SegmentWidget->SetWidgetVisibility(0);
    this->ElementWidget->SetWidgetVisibility(0);
    this->PlotWidget->SetWidgetVisibility(0);
    this->HeartWidget->SetWidgetVisibility(0);
    this->SubTreeMeshWidget->SetWidgetVisibility(0);
    this->MultipleTerminalWidget->SetWidgetVisibility(0);
    this->MultipleSegmentWidget->SetWidgetVisibility(0);
    this->InterventionsWidget->SetWidgetVisibility(1);
    if ( (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL) && (!this->KeepRemoveFilterMode) )
        {
        this->SubTreeMeshWidget->SetWidgetVisibility(1);
        this->MultipleTerminalWidget->SetWidgetVisibility(1);
        this->MultipleSegmentWidget->SetWidgetVisibility(1);
        this->InterventionsWidget->SetWidgetVisibility(0);
        }
    break;

//    case AGING:
//      strcpy(type, "Aging");
//      this->TerminalWidget->SetWidgetVisibility(0);
//      this->NodeWidget->SetWidgetVisibility(0);
//      this->SegmentWidget->SetWidgetVisibility(0);
//      this->ElementWidget->SetWidgetVisibility(0);
//      this->PlotWidget->SetWidgetVisibility(0);
//      this->HeartWidget->SetWidgetVisibility(0);
//      this->MultipleTerminalWidget->SetWidgetVisibility(0);
//      this->MultipleSegmentWidget->SetWidgetVisibility(0);
//      this->InterventionsWidget->SetWidgetVisibility(1);
//      if (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL)
//        {
//        this->MultipleTerminalWidget->SetWidgetVisibility(1);
//        this->MultipleSegmentWidget->SetWidgetVisibility(1);
//        this->InterventionsWidget->SetWidgetVisibility(0);
//        }
//    break;

  default:
    if(type)
      strcpy(type, "None");

    this->TerminalWidget->SetWidgetVisibility(0);
    this->NodeWidget->SetWidgetVisibility(0);
    this->SegmentWidget->SetWidgetVisibility(0);
    this->ElementWidget->SetWidgetVisibility(0);
    this->PlotWidget->SetWidgetVisibility(0);
    this->HeartWidget->SetWidgetVisibility(0);
    this->SubTreeMeshWidget->SetWidgetVisibility(0);
    this->MultipleTerminalWidget->SetWidgetVisibility(0);
    this->MultipleSegmentWidget->SetWidgetVisibility(0);
    this->InterventionsWidget->SetWidgetVisibility(0);
    break;
  }

// Geometry Widget is always visible
// this->GeometryWidget->SetWidgetVisibility(1);
// Change current components when viewing Plots
  if(this->GetWidgetMode() == MODE_PLOT_VAL)
    {
    this->TerminalWidget->SetWidgetVisibility(0);
    this->NodeWidget->SetWidgetVisibility(0);
    this->SegmentWidget->SetWidgetVisibility(0);
    this->ElementWidget->SetWidgetVisibility(0);
    this->HeartWidget->SetWidgetVisibility(0);
    this->PlotWidget->SetWidgetVisibility(1);
    this->SubTreeMeshWidget->SetWidgetVisibility(0);
    this->MultipleTerminalWidget->SetWidgetVisibility(0);
    this->MultipleSegmentWidget->SetWidgetVisibility(0);
    this->InterventionsWidget->SetWidgetVisibility(0);
    }
  this->PlaceComponents();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::VisualizationStateCallback()
{
  if (this->VisualizationStateOptionMenu->IsCreated())
    {
    const char *value = this->VisualizationStateOptionMenu->GetWidget()->GetValue();
    if (!strcmp(value, MODE_VISUALIZATION))
      {
      this->SetWidgetMode((int) MODE_VISUALIZATION_VAL);
      }
    if (!strcmp(value, MODE_EDITION))
      {
      this->SetWidgetMode((int) MODE_EDITION_VAL);
      }
    if (!strcmp(value, MODE_PLOT))
      {
      this->SetWidgetMode((int) MODE_PLOT_VAL);
      }
    if (!strcmp(value, MODE_TODETACHSUBTREE))
      {
      this->SetWidgetMode((int) MODE_TODETACHSUBTREE_VAL);
      }
    if (!strcmp(value, MODE_ESPECIAL_TOOLS))
      {
      this->SetWidgetMode((int) MODE_ESPECIAL_TOOLS_VAL);
      }
    }
  this->UpdateWidgetInfo();

  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
  this->WidgetProxy->GetProperty("StraightModelModeSelected"));
  smSelect->SetElement(0, (int)this->GetWidgetMode());
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::Accept()
{
  int modFlag = this->GetModifiedFlag();

  vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  if ( this->KeepRemoveFilterMode )
    {
    wProxy->KeepRemoveSegments(this->KeepFilterRadioButton->GetSelectedState());
    }

//  this->WidgetProxy->UpdateInformation();
  vtkSMSourceProxy* sproxy = this->GetPVSource()->GetProxy();
  sproxy->UpdateVTKObjects();
  sproxy->UpdatePipeline();

  // 3DWidgets need to explictly call UpdateAnimationInterface on accept
  // since the animatable proxies might have been registered/unregistered
  // which needs to be updated in the Animation interface.
  this->GetPVApplication()->GetMainWindow()->UpdateAnimationInterface();
  this->ModifiedFlag = 0;
  // I put this after the accept internal, because
  // vtkPVGroupWidget inactivates and builds an input list ...
  // Putting this here simplifies subclasses AcceptInternal methods.
  if (modFlag)
    {
    vtkPVApplication *pvApp = this->GetPVApplication();
    ofstream* file = pvApp->GetTraceFile();
    if (file)
      {
      this->Trace(file);
      }
    }

  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
  this->WidgetProxy->GetProperty("StraightModelObjectSelected"));

	// Atualiza a geometria aqui para pegar os valores default das esferas e tubos sem
	// precisar de clicar em nada
// 	this->SetGeometryWidgetValues();

  // Propagate accept through others widgets
  ElementWidget->Accept();
  SegmentWidget->Accept();
  TerminalWidget->Accept();
  NodeWidget->Accept();
  PlotWidget->Accept();
  HeartWidget->Accept();
  //  GeometryWidget->Accept();
  this->InterventionsWidget->Accept();

  this->SetTreeGeneralPropertiesWidgetValues();

  //pegando o server manager vtkSMHMStraightModelWidgetProxy


//	wProxy->UpdateVTKObjects();

  vtkProcessModule* pm = vtkProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());

  //pegando o reader
  vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(sproxy->GetID(0)));

  if(reader)
    {
    //verifica se possui nome, caso não tenha retorna, pois,
    //não pode ler ainda o arquivo
    if(!reader->GetFileName())
      return;
    this->ReadSimulationButton->EnabledOn();
    }
  else
    {
    vtkHM1DHeMoLabReader *r = vtkHM1DHeMoLabReader::SafeDownCast(pm->GetObjectFromID(sproxy->GetID(0)));
    if(r)
      {
      //verifica se possui nome, caso não tenha retorna, pois,
      //não pode ler ainda o arquivo
      if(!r->GetFileName())
        return;
      this->ReadSimulationButton->EnabledOff();
      }

    vtkHMUnstructuredGridTo1DModel *filter = vtkHMUnstructuredGridTo1DModel::SafeDownCast(pm->GetObjectFromID(sproxy->GetID(0)));
    }

  //seta o proxy
   if ( wProxy->SetSourceProxy(sproxy) )
      {
      vtkStringArray *names = wProxy->GenerateListOfSegmentName();

      const char *n;
      for ( int i=0; i<names->GetNumberOfValues(); i++ )
        {
        n = names->GetValue(i);
        this->SegmentWidget->AddSegmentNameInList((char*)n);
        this->AllSegmentsNameComboBox->AddValue(n);
        }
      names->Delete();

      double *size;
      size = wProxy->GetSize();

      this->XSizeEntry->SetValueAsDouble(size[0]);
      this->YSizeEntry->SetValueAsDouble(size[1]);
      this->ZSizeEntry->SetValueAsDouble(size[2]);

      this->CalculateTreeVolume();

      this->UpdateSmallerSegment();
      }

   this->InputVisualizationModeButton->EnabledOn();
   this->EditionModeButton->EnabledOn();
   this->PlotModeButton->EnabledOff();
   this->ToDetachSubTreeButton->EnabledOn();
   this->EditElemButton->EnabledOn();
   this->EditSegButton->EnabledOn();
   this->SpecialToolsButton->EnabledOn();
   this->GenerateSolverFilesButton->EnabledOn();

   this->ExecuteSolverPushButton->EnabledOn();
   this->ReadSimulationButton->EnabledOn();

   if ( this->KeepRemoveFilterMode )
     {
     wProxy->UpdateWidget();

     //remove todos os nomes da lista
     this->SegmentWidget->RemoveAllSegmentNameInList();
     this->AllSegmentsNameComboBox->DeleteAllValues();

     vtkStringArray *names = wProxy->GenerateListOfSegmentName();

     const char *n;
     for ( int i=0; i<names->GetNumberOfValues(); i++ )
       {
       n = names->GetValue(i);
//       cout << "vtkPVHMStraightModelWidget::Accept() - segm: " << n << endl;
       this->SegmentWidget->AddSegmentNameInList((char*)n);
       this->AllSegmentsNameComboBox->AddValue(n);
       }
     names->Delete();

     double *size;
     size = wProxy->GetSize();

     this->XSizeEntry->SetValueAsDouble(size[0]);
     this->YSizeEntry->SetValueAsDouble(size[1]);
     this->ZSizeEntry->SetValueAsDouble(size[2]);

     this->CalculateTreeVolume();

     this->UpdateSmallerSegment();

     this->RemoveFilterRadioButton->EnabledOff();
     this->KeepFilterRadioButton->EnabledOff();

     this->Script("grid remove %s",
         this->RemoveFilterRadioButton->GetWidgetName());

     this->Script("grid remove %s",
         this->KeepFilterRadioButton->GetWidgetName());


     this->Script("grid %s - - -sticky w -columnspan 2 -pady 5",
         this->ToolBar->GetWidgetName());

     this->Script("grid %s - - -sticky w -pady 2",
       this->SimulationLabel->GetWidgetName());

     this->Script("grid %s - - -sticky w",
       this->SimulationToolBar->GetWidgetName());

     this->Script("grid %s -sticky ew -columnspan 4 -pady 7",
       this->GeneralInfoFrame->GetWidgetName());

     this->Script("pack %s -fill x -expand t -pady 2",
       this->GeneralFrame->GetWidgetName());

     this->Script("pack %s -fill x -expand t -pady 5",
       this->GeneralFrame2->GetWidgetName());

     this->Script("grid %s -sticky ew -columnspan 4",
         this->ScaleFrame->GetWidgetName());

     this->UpdateToolbar(MODE_VISUALIZATION_VAL);
     this->SetWidgetMode((int) MODE_VISUALIZATION_VAL);
     this->PlotModeButton->EnabledOff();

     }


   this->KeepRemoveFilterMode = false;
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ActualPlaceWidget()
{
  double bds[6];
  double x, y, z;

  if ( this->PVSource->GetPVInput(0) )
    {
    this->PVSource->GetPVInput(0)->GetDataInformation()->GetBounds(bds);

    x = (bds[0]+bds[1])/2;
    y = bds[2];
    z = (bds[4]+bds[5])/2;
    x = (bds[0]+bds[1])/2;
    y = bds[3];
    z = (bds[4]+bds[5])/2;

    this->PlaceWidget(bds);
    }
  else
    {
    }
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::Create(vtkKWApplication* app)
{
  this->Superclass::Create(app);

  this->Window = vtkPVWindow::SafeDownCast(app->GetNthWindow(0));
  this->XYPlotWidget->SetTitle("HeMoLab Plot 2D");
  this->XYPlotWidget->SetMasterWindow(vtkKWWindow::SafeDownCast(app->GetNthWindow(0)));
  this->XYPlotWidget->Create(app);
  this->XYPlotWidget->ClearButton->SetCommand(this, "ClearPlot");

  this->ExportDialog->Create(app);

// 	this->WindowAnimated = vtkPVWindow::SafeDownCast(app->GetNthWindow(1));
  this->XYAnimatedPlotWidget->SetTitle("HeMoLab Animated Plot 2D");
  this->XYAnimatedPlotWidget->SetMasterWindow(vtkKWWindow::SafeDownCast(app->GetNthWindow(0)));
  this->XYAnimatedPlotWidget->Create(app);

 }

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SaveInBatchScript(ofstream *file)
{
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::UpdateWidgetInfo()
{
  if (this->KeepRemoveFilterMode)
    return;

  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast( this->WidgetProxy->GetProperty("StraightModelObjectSelected"));
  int objectType = smSelect->GetElement(0);

  char objectTypeTmp[30];
  this->GetObjectTypeName(objectTypeTmp, objectType);
  this->ObjectTypeValue->SetText(objectTypeTmp);

  if (( this->GetWidgetMode()== MODE_EDITION_VAL) || (this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL)
  || (this->GetWidgetMode() == MODE_ESPECIAL_TOOLS_VAL) )
    {// se modo de edicao habilitado ou modo de destaque de subarvore
    this->XScaleThumbWheel->EnabledOn();
    this->YScaleThumbWheel->EnabledOn();
    this->ZScaleThumbWheel->EnabledOn();

    this->XSizeEntry->EnabledOn();
    this->YSizeEntry->EnabledOn();
    this->ZSizeEntry->EnabledOn();

    this->LengthElementEntry->EnabledOn();
    this->ConfigureDefaultLengthOfElementButton->EnabledOn();
    }
  else
    {
    this->XScaleThumbWheel->EnabledOff();
    this->YScaleThumbWheel->EnabledOff();
    this->ZScaleThumbWheel->EnabledOff();

    this->XSizeEntry->EnabledOff();
    this->YSizeEntry->EnabledOff();
    this->ZSizeEntry->EnabledOff();

    this->LengthElementEntry->EnabledOff();
    this->ConfigureDefaultLengthOfElementButton->EnabledOff();
    }



  this->SmallerSegmentNameEntry->EnabledOff();
  this->SmallerSegmentLengthEntry->EnabledOff();

  // if the Widget mode is set to 'visualization', widgets enter in 'readonly' mode
  // Otherwise, widgets are editable
  if(this->ElementWidget)
    {
    this->ElementWidget->SetReadOnly(this->GetWidgetMode());
    this->ElementWidget->UpdateWidgetInfo();
    }

  if(this->SegmentWidget)
    {
    this->SegmentWidget->SetReadOnly(this->GetWidgetMode());
    this->SegmentWidget->UpdateWidgetInfo();
    }

  if(this->TerminalWidget)
    {
    this->TerminalWidget->SetReadOnly(this->GetWidgetMode());
    this->TerminalWidget->UpdateWidgetInfo();
    }

  if(this->NodeWidget)
    {
    this->NodeWidget->SetReadOnly(this->GetWidgetMode());
    this->NodeWidget->UpdateWidgetInfo();
    }

  if(this->HeartWidget)
    {
    this->HeartWidget->SetReadOnly(this->GetWidgetMode());
    this->HeartWidget->UpdateWidgetInfo();
    }

  if (this->MultipleTerminalWidget)
    {
    this->MultipleTerminalWidget->SetReadOnly(this->GetWidgetMode());
    this->MultipleTerminalWidget->UpdateWidgetInfo();
    }

  if (this->MultipleSegmentWidget)
    {
    this->MultipleSegmentWidget->SetReadOnly(this->GetWidgetMode());
    this->MultipleSegmentWidget->UpdateWidgetInfo();
    }

  if (this->SubTreeMeshWidget)
    {
    this->SubTreeMeshWidget->SetReadOnly(this->GetWidgetMode());
    this->SubTreeMeshWidget->UpdateWidgetInfo();
    }

  if ( this->InterventionsWidget )
    {
    this->InterventionsWidget->SetReadOnly(this->GetWidgetMode());
    this->InterventionsWidget->UpdateWidgetInfo();
    }
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::Initialize()
{
  vtkSMSourceProxy* sproxy = this->GetPVSource()->GetProxy();

  //pegando o server manager vtkSMHMStraightModelWidgetProxy
  vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  vtkProcessModule* pm = vtkProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());

  vtkHM1DKeepRemoveFilter *keepRemoveFilter = vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(sproxy->GetID(0)));
  if ( keepRemoveFilter )
    {
    wProxy->SetSourceProxy(sproxy);

    this->KeepRemoveFilterMode = true;

    this->UpdateToolbar(MODE_TODETACHSUBTREE_VAL);

    this->InputVisualizationModeButton->EnabledOff();
    this->EditionModeButton->EnabledOff();
    this->PlotModeButton->EnabledOff();
    this->ToDetachSubTreeButton->EnabledOn();
    this->EditElemButton->EnabledOff();
    this->EditSegButton->EnabledOff();
    this->SpecialToolsButton->EnabledOff();
    this->GenerateSolverFilesButton->EnabledOff();

    this->ExecuteSolverPushButton->EnabledOff();
    this->ReadSimulationButton->EnabledOff();

    this->EditGeometryButton->EnabledOff();
    this->AddSegmentButton->EnabledOff();
    this->AddTerminalButton->EnabledOff();
    this->DeleteButton->EnabledOff();
    this->MoveButton->EnabledOff();
    this->CheckConsistenceButton->EnabledOff();
    this->CopySegmentButton->EnabledOff();
    this->FlipButton->EnabledOff();
    this->RestartButton->EnabledOff();
    this->KeepSubTreeButton->EnabledOff();

    this->RemoveFilterRadioButton->EnabledOn();
    this->KeepFilterRadioButton->EnabledOn();

    this->Script("grid remove %s",
        this->ToolBar->GetWidgetName());

    this->Script("grid remove %s",
      this->SimulationLabel->GetWidgetName());

    this->Script("grid remove %s",
      this->SimulationToolBar->GetWidgetName());

    this->Script("grid remove %s",
      this->GeneralInfoFrame->GetWidgetName());

    this->Script("grid remove %s",
        this->ScaleFrame->GetWidgetName());

    this->Script("grid %s - - -sticky w -columnspan 2 -pady 2",
        this->RemoveFilterRadioButton->GetWidgetName());

    this->Script("grid %s - - -sticky w -columnspan 2 -pady 2",
        this->KeepFilterRadioButton->GetWidgetName());

    this->SetFrameLabel("Mode: Select Sub-Tree");
    }



  this->PlaceWidget();
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ResetInternal()
{
}

//----------------------------------------------------------------------------
vtkPVHMStraightModelWidget* vtkPVHMStraightModelWidget::ClonePrototype(vtkPVSource* pvSource,
  vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map)
{
  vtkPVWidget* clone = this->ClonePrototypeInternal(pvSource, map);
  return vtkPVHMStraightModelWidget::SafeDownCast(clone);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::CopyProperties(vtkPVWidget* clone,
  vtkPVSource* pvSource,
  vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map)
{
  this->Superclass::CopyProperties(clone, pvSource, map);
}

//----------------------------------------------------------------------------
int vtkPVHMStraightModelWidget::ReadXMLAttributes(vtkPVXMLElement* element,
  vtkPVXMLPackageParser* parser)
{
  if(!this->Superclass::ReadXMLAttributes(element, parser)) { return 0; }

  return 1;
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
  vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

//  this->Superclass::ExecuteEvent(wdg, l, p);
  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("StraightModelObjectSelected"));

  vtkSMIntVectorProperty* smWidgetSelect = vtkSMIntVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("StraightModelWidgetEvent"));

  this->TerminalWidget->UnselectButtons();

  if(smWidgetSelect->GetElement(0) == vtkCommand::StartInteractionEvent)
    {
    //	  if(this->XYAnimatedPlotWidget->PlotWindow)
    //	  	this->XYAnimatedPlotWidget->SetAnimatedControlThread(true);

    if ( !this->KeepRemoveFilterMode )
      {
      if(this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL)
        {
        this->SetInfoSubTree();
        this->Script("grid %s -sticky ew -columnspan 4", this->SubTreePropFrame->GetWidgetName());
        }
      }
    // *********************************************
    // *************Nó Selecionado *****************
    // *********************************************
    if (smSelect->GetElement(0) == NODE)
      {
      this->SetNodeWidgetValues();
      this->SetBasicSegmentWidgetValues();
      this->SetAdvancedSegmentWidgetValues();

      if ( this->GetWidgetMode() == MODE_PLOT_VAL )
        {
        vtkDoubleArray *array = wProxy->GetSegmentProxy()->GetNodeResolutionArray();
        if (array)
          {
          vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("NodeProperty"));
          vtkDoubleArray *time = wProxy->GetDataOutInstantOfTime(array->GetNumberOfTuples());
          int meshID = (int)elem->GetElement(5);
          if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
            if(this->XYPlotWidget->ExistMeshIDForTitle(meshID) == false)
              {
              if ( this->PlotGraphic(array, time, 'N', meshID) )
                wProxy->ColorPlot(0);
              }
            else
              {
              wProxy->HighlightSelectedActors(1);
              this->XYPlotWidget->ShowHMXYPlotWidget();
              }

          else if (this->PlotWidget->AnimatedPlotTypeRadio->GetSelectedState())
            if ( this->PlotGraphic(array, time, 'N', meshID) )
                wProxy->ColorPlot(1);

          array->Delete();
          time->Delete();
          }
        else
          vtkErrorMacro ("Could not get Resolution array in Client");
       }
      else if ( (this->GetWidgetMode() == MODE_EDITION_VAL) || (this->GetWidgetMode() == MODE_ESPECIAL_TOOLS_VAL) )
        {
        if ( this->EditionMode == vtkPVHMStraightModelWidget::AddSegment)
          {
          vtkStringArray *names = wProxy->GenerateListOfSegmentName();

          //remove todos os nomes da lista

          this->SegmentWidget->RemoveAllSegmentNameInList();
          this->AllSegmentsNameComboBox->DeleteAllValues();

          const char *n;
          //adiciona os nomes ordenados na lista
          for ( int i=0; i<names->GetNumberOfValues(); i++ )
            {
            n = names->GetValue(i);
            this->SegmentWidget->AddSegmentNameInList((char*)n);
            this->AllSegmentsNameComboBox->AddValue(n);
            }
          names->Delete();

          }
        }
      }

    // *********************************************
    // *************TERMINAL OU CORAÇÃO Selecionado *****************
    // *********************************************
    else if ( (smSelect->GetElement(0) == TERMINAL) || (smSelect->GetElement(0) == HEART) )
      {
        if(this->GetWidgetMode() != MODE_TODETACHSUBTREE_VAL)
          this->SetTerminalWidgetValues();

      if ( this->GetWidgetMode() == MODE_PLOT_VAL )
        {
        vtkDoubleArray *array = wProxy->GetTerminalProxy()->GetTerminalResolutionArray();
        if (array)
          {
          vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(this->WidgetProxy->GetProperty("TerminalProperties"));
          vtkDoubleArray *time = wProxy->GetDataOutInstantOfTime(array->GetNumberOfTuples());
          int meshID = (int)elem->GetElement(7);
          if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
            if(this->XYPlotWidget->ExistMeshIDForTitle(meshID) == false)
              {
              if ( this->PlotGraphic(array, time, 'T', meshID) )
                      wProxy->ColorPlot(0);
              }
            else
              {
              wProxy->HighlightSelectedActors(1);
              this->XYPlotWidget->ShowHMXYPlotWidget();
              }
          else if (this->PlotWidget->AnimatedPlotTypeRadio->GetSelectedState())
            if ( this->PlotGraphic(array, time, 'T', meshID) )
              wProxy->ColorPlot(1);

          array->Delete();
          time->Delete();

          }
        else
          vtkErrorMacro ("Could not get Terminal Resolution array in Client");
        }
      else if ( (this->GetWidgetMode() == MODE_EDITION_VAL) || (this->GetWidgetMode() == MODE_ESPECIAL_TOOLS_VAL) )
        {
        if ( this->EditionMode == vtkPVHMStraightModelWidget::AddSegment)
          {
          //pega array com nome dos segmentos
          vtkStringArray *names = wProxy->GenerateListOfSegmentName();

          //remove todos os nomes da lista
          this->SegmentWidget->RemoveAllSegmentNameInList();
          this->AllSegmentsNameComboBox->DeleteAllValues();

          const char *n;
          //adiciona os nomes ordenados na lista
          for ( int i=0; i<names->GetNumberOfValues(); i++ )
            {
            n = names->GetValue(i);
            this->SegmentWidget->AddSegmentNameInList((char*)n);
            this->AllSegmentsNameComboBox->AddValue(n);
            }
          names->Delete();
          }

        if ( this->EditionMode == vtkPVHMStraightModelWidget::AddTerminal)
          {
          //pega array com nome dos segmentos
          vtkStringArray *names = wProxy->GenerateListOfSegmentName();

          //remove todos os nomes da lista
          this->SegmentWidget->RemoveAllSegmentNameInList();
          this->AllSegmentsNameComboBox->DeleteAllValues();

          const char *n;
          //adiciona os nomes ordenados na lista
          for ( int i=0; i<names->GetNumberOfValues(); i++ )
            {
            n = names->GetValue(i);
            this->SegmentWidget->AddSegmentNameInList((char*)n);
            this->AllSegmentsNameComboBox->AddValue(n);
            }
          names->Delete();
          }
        }
      }
    // *********************************************
    // *************ELEMENTO Selecionado *****************
    // *********************************************
    else if (smSelect->GetElement(0) == SEGMENT)
      {
      if (this->SegmentWidget->GetSegmentSelected())
        {
        this->SetBasicSegmentWidgetValues();
        this->SetAdvancedSegmentWidgetValues();
        this->SegmentProxSpeedOfSoundValue();
        this->SegmentDistalSpeedOfSoundValue();
        }
      if (this->SegmentWidget->GetElementSelected())
        {
        this->SetElementGeometricalInfo();
        this->SetElementWidgetValues();
//				this->ElementSpeedOfSoundValue();
        }
      this->AllSegmentsNameComboBox->SetValue(wProxy->GetSegmentProxy()->GetSegmentName());

      if ( this->GetWidgetMode() == MODE_PLOT_VAL )
        {
        vtkDoubleArray *array = wProxy->GetSegmentProxy()->GetNodeResolutionArray();
        if (array)
          {
          vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(
                          this->WidgetProxy->GetProperty("ElementProperty"));

          vtkDoubleArray *time = wProxy->GetDataOutInstantOfTime(array->GetNumberOfTuples());
          int meshID = (int)elem->GetElement(10);

          if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
              if(this->XYPlotWidget->ExistMeshIDForTitle(meshID) == false)
                {
                if ( this->PlotGraphic(array, time, 'E', meshID) )
                            wProxy->ColorPlot(0);
                }
              else
                {
                wProxy->HighlightSelectedActors(1);
                this->XYPlotWidget->ShowHMXYPlotWidget();
                }

          else if (this->PlotWidget->AnimatedPlotTypeRadio->GetSelectedState())
            if ( this->PlotGraphic(array, time, 'E', meshID) )
              wProxy->ColorPlot(1);

          array->Delete();
          time->Delete();

          }
        else
                vtkErrorMacro ("Could not get Resolution array in Client");
        }
      }
    else if (smSelect->GetElement(0) == CLIP)
      {
      if ( this->GetWidgetMode() == MODE_ESPECIAL_TOOLS_VAL )
        {
        if ( this->SurgicalInterventionsMode == vtkPVHMStraightModelWidget::Clip)
          {
          wProxy->AddClip();

          //remove todos os nomes da lista

          this->SegmentWidget->RemoveAllSegmentNameInList();
          this->AllSegmentsNameComboBox->DeleteAllValues();

          vtkStringArray *names = wProxy->GenerateListOfSegmentName();

          const char *n;
          //adiciona os nomes ordenados na lista
          for ( int i=0; i<names->GetNumberOfValues(); i++ )
            {
            n = names->GetValue(i);
            this->SegmentWidget->AddSegmentNameInList((char*)n);
            this->AllSegmentsNameComboBox->AddValue(n);
            }
          names->Delete();

          }
        }
      }
    else if (smSelect->GetElement(0) == STENT)
      {
      this->InterventionsWidget->SetObjectSelected(smSelect->GetElement(0));
      this->SetInterventionsWidgetValues();
      }
    else if (smSelect->GetElement(0) == STENOSIS)
      {
      this->InterventionsWidget->SetObjectSelected(smSelect->GetElement(0));
      this->SetInterventionsWidgetValues();
      }
    else if (smSelect->GetElement(0) == ANEURYSM)
      {
      this->InterventionsWidget->SetObjectSelected(smSelect->GetElement(0));
      this->SetInterventionsWidgetValues();
      }
    else
      {
      this->InterventionsWidget->SetObjectSelected(-1);
      this->SetInterventionsWidgetValues();
      if ( this->GetWidgetMode() == MODE_EDITION_VAL )
        {
        if ( this->EditionMode == vtkPVHMStraightModelWidget::AddSegment)
          {
          //remove todos os nomes da lista
          this->SegmentWidget->RemoveAllSegmentNameInList();
          this->AllSegmentsNameComboBox->DeleteAllValues();

          vtkStringArray *names = wProxy->GenerateListOfSegmentName();

          const char *n;
          //adiciona os nomes ordenados na lista
          for ( int i=0; i<names->GetNumberOfValues(); i++ )
            {
            n = names->GetValue(i);
            this->SegmentWidget->AddSegmentNameInList((char*)n);
            this->AllSegmentsNameComboBox->AddValue(n);
            }
          names->Delete();
          }
        }
      }
    }
  //Evento chamado quando o segmento for deletado pela tecla "Delete"
  else if(smWidgetSelect->GetElement(0) == vtkCommand::InteractionEvent)
    {
    if (smSelect->GetElement(0) == SEGMENT)
      {
      //Pega o nome do segmento selecionado
      char *name = wProxy->GetSegmentProxy()->GetSegmentName();
      //Remove o nome da lista de nomes de segmento
      this->SegmentWidget->RemoveSegmentNameInList(name);
      int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
      this->AllSegmentsNameComboBox->DeleteValue(index);
      this->AllSegmentsNameComboBox->SetValue("");

      //limpa a interface
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      this->PlaceComponents();
      this->SegmentWidget->SetReadOnly(this->GetWidgetMode());
      this->SegmentWidget->UpdateWidgetInfo();
      this->ObjectTypeValue->SetText("none");
      }
    else if (smSelect->GetElement(0) == TERMINAL)
      {
      vtkClientServerID termCSID = wProxy->GetTerminalProxy()->GetID(0);

      //verifica se o proxy está apontando para algum terminal
      if ( termCSID.ID > 0 )
        {
        int numberOfChilds = wProxy->GetTerminalProxy()->GetTerminalChildCount();
        int numberOfParents = wProxy->GetTerminalProxy()->GetTerminalParentCount();
        //verifica se o terminal pode ser removido
        if ( (numberOfChilds == 1) && (numberOfParents == 1) )
          {
          //pega o id do segmento filho
          vtkIdType childId = wProxy->GetTerminalProxy()->GetFirstChildId();

          //pega o ClientServerId do segmento
          vtkIdType csid = wProxy->GetSegmentCSID(childId);
          //seta no proxy de segmento o segmento que irá apontar
          wProxy->GetSegmentProxy()->SetSegmentCSID(csid, -1, -1, 0, 0);
          //pega o nome do segmento
          char *n = wProxy->GetSegmentProxy()->GetSegmentName();
          //remove o nome da lista de nomes de segmentos
          this->SegmentWidget->RemoveSegmentNameInList(n);
          int index = this->AllSegmentsNameComboBox->GetValueIndex(n);
          this->AllSegmentsNameComboBox->DeleteValue(index);
          }
        }

      //limpa a interface
      this->TerminalWidget->SetWidgetVisibility(0);
      this->NodeWidget->SetWidgetVisibility(0);
      this->SegmentWidget->SetWidgetVisibility(0);
      this->ElementWidget->SetWidgetVisibility(0);
      this->PlotWidget->SetWidgetVisibility(0);
      this->HeartWidget->SetWidgetVisibility(0);
      this->SubTreeMeshWidget->SetWidgetVisibility(0);
      this->MultipleTerminalWidget->SetWidgetVisibility(0);
      this->MultipleSegmentWidget->SetWidgetVisibility(0);
      this->InterventionsWidget->SetWidgetVisibility(0);
      this->PlaceComponents();
      this->SegmentWidget->SetReadOnly(this->GetWidgetMode());
      this->SegmentWidget->UpdateWidgetInfo();
      this->ObjectTypeValue->SetText("none");
      }
    }

  if ( ((this->XYPlotWidget) && (smWidgetSelect->GetElement(0) != vtkCommand::InteractionEvent)) ||
                  ((this->XYAnimatedPlotWidget) && (smWidgetSelect->GetElement(0) != vtkCommand::InteractionEvent)))
    {
    this->SetTreeGeneralPropertiesWidgetValues();
    this->UpdateWidgetInfo();
    }

  this->InvokeEvent("WidgetModifiedEvent");

  double *size;
  size = wProxy->GetSize();

  this->XSizeEntry->SetValueAsDouble(size[0]);
  this->YSizeEntry->SetValueAsDouble(size[1]);
  this->ZSizeEntry->SetValueAsDouble(size[2]);

  this->CalculateTreeVolume();

}//fim do método ExecuteEvent



//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ChildCreate(vtkPVApplication* pvApp)
{
  //Setando cor da barra de progresso para vermelho
  vtkPVWindow *pvWindow = pvApp->GetMainWindow();
  vtkKWProgressGauge *progress = pvWindow->GetProgressGauge();
  progress->SetBarColor(1.0, 0.0, 0.0);
  //pvWindow->SetTitle("HeMoLab 0.8");
  this->Window = pvWindow;
  this->pvApp = pvApp;


  if ((this->GetTraceHelper()->GetObjectNameState() == vtkPVTraceHelper::ObjectNameStateUninitialized ||
      this->GetTraceHelper()->GetObjectNameState() == vtkPVTraceHelper::ObjectNameStateDefault) )
    {
    this->GetTraceHelper()->SetObjectName("HMStraightModelWidget");
    this->GetTraceHelper()->SetObjectNameState(vtkPVTraceHelper::ObjectNameStateSelfInitialized);
    }

  this->SetFrameLabel("Mode: Properties Visualization");
  //  this->Labels[0]->SetParent(this->Frame);
  //  this->Labels[0]->Create(pvApp);

  this->ToolBar->SetParent(this->Frame);
  this->ToolBar->Create(pvApp);


  this->InputVisualizationModeButton->SetParent(this->ToolBar);
  this->InputVisualizationModeButton->Create(pvApp);
  this->InputVisualizationModeButton->SetBalloonHelpString("Toggle to Input Visualization Mode");


  this->EditionModeButton->SetParent(this->ToolBar);
  this->EditionModeButton->Create(pvApp);
  this->EditionModeButton->SetBalloonHelpString("Toggle to Edition Mode");


  this->PlotModeButton->SetParent(this->ToolBar);
  this->PlotModeButton->Create(pvApp);
  this->PlotModeButton->SetBalloonHelpString("Toggle to Output Results Visualization Mode");

  this->ToDetachSubTreeButton->SetParent(this->ToolBar);
  this->ToDetachSubTreeButton->Create(pvApp);
  this->ToDetachSubTreeButton->SetBalloonHelpString("To detach and edit sub-tree:\n "
    "Press CTRL + left click to\n "
    "select the sub-tree.\n "
    "Press SHIFT + left click to\n "
    "deselect the sub-tree.");

  this->SpecialToolsButton->SetParent(this->ToolBar);
  this->SpecialToolsButton->Create(pvApp);
  this->SpecialToolsButton->SetBalloonHelpString("Toggle to special tool for cases of pathology and surgical interventions");

  this->EditElemButton->SetParent(this->ToolBar);
  this->EditElemButton->Create(pvApp);
  this->EditElemButton->SetHighlightThickness(0);
  this->EditElemButton->IndicatorVisibilityOff();
  this->EditElemButton->SetBalloonHelpString("Select Element mode");

  this->EditSegButton->SetParent(this->ToolBar);
  this->EditSegButton->Create(pvApp);
  this->EditSegButton->SetHighlightThickness(0);
  this->EditSegButton->IndicatorVisibilityOff();
  this->EditSegButton->SetBalloonHelpString("Select Segment mode");
  this->EditSegButton->SelectedStateOn();


  this->InputVisualizationModeButtonIcon->SetImage(image_1DStraightModelViewerModeButton,
  				  image_1DStraightModelViewerModeButton_width,
  				  image_1DStraightModelViewerModeButton_height,
  				  image_1DStraightModelViewerModeButton_pixel_size,
  				  image_1DStraightModelViewerModeButton_length,
  				  image_1DStraightModelViewerModeButton_decoded_length);

  this->EditionModeButtonIcon->SetImage(image_1DStraightModelEditionModeButton,
            image_1DStraightModelEditionModeButton_width,
            image_1DStraightModelEditionModeButton_height,
            image_1DStraightModelEditionModeButton_pixel_size,
            image_1DStraightModelEditionModeButton_length,
            image_1DStraightModelEditionModeButton_decoded_length);

  this->PlotModeButtonIcon->SetImage(image_1DStraightModelOutputModeButton,
  				  image_1DStraightModelOutputModeButton_width,
  				  image_1DStraightModelOutputModeButton_height,
  				  image_1DStraightModelOutputModeButton_pixel_size,
  				  image_1DStraightModelOutputModeButton_length,
  				  image_1DStraightModelOutputModeButton_decoded_length);

  this->ToDetachSubTreeButtonIcon->SetImage(image_1DStraightModelToDetachSubTreeButton,
  				  image_1DStraightModelToDetachSubTreeButton_width,
  				  image_1DStraightModelToDetachSubTreeButton_height,
  				  image_1DStraightModelToDetachSubTreeButton_pixel_size,
  				  image_1DStraightModelToDetachSubTreeButton_length,
  				  image_1DStraightModelToDetachSubTreeButton_decoded_length);

   this->EditElemButtonIcon->SetImage(image_1DStraightModelElementButton,
  				  image_1DStraightModelElementButton_width,
  				  image_1DStraightModelElementButton_height,
  				  image_1DStraightModelElementButton_pixel_size,
  				  image_1DStraightModelElementButton_length,
  				  image_1DStraightModelElementButton_decoded_length);

  this->EditSegButtonIcon->SetImage(image_1DStraightModelSegmentButton,
  				  image_1DStraightModelSegmentButton_width,
   				  image_1DStraightModelSegmentButton_height,
    			  image_1DStraightModelSegmentButton_pixel_size,
    			  image_1DStraightModelSegmentButton_length,
     			  image_1DStraightModelSegmentButton_decoded_length);

  this->SpecialToolsIcon->SetImage(image_1DStraightModelSpecialToolsButton,
				  image_1DStraightModelSpecialToolsButton_width,
				  image_1DStraightModelSpecialToolsButton_height,
				  image_1DStraightModelSpecialToolsButton_pixel_size,
				  image_1DStraightModelSpecialToolsButton_length,
				  image_1DStraightModelSpecialToolsButton_decoded_length);

  this->InputVisualizationModeButton->SetImageToIcon(this->InputVisualizationModeButtonIcon);
  this->EditionModeButton->SetImageToIcon(this->EditionModeButtonIcon);
  this->PlotModeButton->SetImageToIcon(this->PlotModeButtonIcon);
  this->ToDetachSubTreeButton->SetImageToIcon(this->ToDetachSubTreeButtonIcon);
  this->EditElemButton->SetImageToIcon(this->EditElemButtonIcon);
  this->EditSegButton->SetImageToIcon(this->EditSegButtonIcon);
  this->SpecialToolsButton->SetImageToIcon(this->SpecialToolsIcon);

  this->InputVisualizationModeButton->IndicatorVisibilityOff();
  this->EditionModeButton->IndicatorVisibilityOff();
  this->PlotModeButton->IndicatorVisibilityOff();
  this->ToDetachSubTreeButton->IndicatorVisibilityOff();
  this->SpecialToolsButton->IndicatorVisibilityOff();

  this->InputVisualizationModeButton->SetHighlightThickness(0);
  this->EditionModeButton->SetHighlightThickness(0);
  this->PlotModeButton->SetHighlightThickness(0);
  this->ToDetachSubTreeButton->SetHighlightThickness(0);
  this->SpecialToolsButton->SetHighlightThickness(0);

  this->InputVisualizationModeButton->SelectedStateOn();
  this->EditionModeButton->SelectedStateOff();
  this->PlotModeButton->SelectedStateOff();
  this->ToDetachSubTreeButton->SelectedStateOff();
  this->SpecialToolsButton->SelectedStateOff();

  this->ToolBar->AddWidget(this->InputVisualizationModeButton);
  this->ToolBar->AddWidget(this->EditionModeButton);
  this->ToolBar->AddWidget(this->EditElemButton);
  this->ToolBar->AddWidget(this->EditSegButton);
  this->ToolBar->AddWidget(this->PlotModeButton);
  this->ToolBar->AddWidget(this->ToDetachSubTreeButton);
  this->ToolBar->AddWidget(this->SpecialToolsButton);
  this->PlotModeButton->EnabledOff();


  this->SimulationLabel->SetParent(this->Frame);
  this->SimulationLabel->Create(pvApp);
  this->SimulationLabel->SetText("Numerical Solver Interface");

  this->SimulationToolBar->SetParent(this->Frame);
  this->SimulationToolBar->Create(pvApp);

  this->GenerateSolverFilesButton->SetParent(this->SimulationToolBar);
  this->GenerateSolverFilesButton->Create(pvApp);
  this->GenerateSolverFilesButton->SetBalloonHelpString("Displays the HeMoLab general setup window (in order to configure BasParam file) and select a directory to generate Solver input files: Mesh.txt, Param.txt, BasParam.txt and IniFile.txt");
  this->GenerateSolverFilesButton->SetCommand(this, "GenerateSolverFiles");

  this->GenerateSolverFilesIcon->SetImage(image_1DStraightModelGenerateFiles,
            image_1DStraightModelGenerateFiles_width,
            image_1DStraightModelGenerateFiles_height,
            image_1DStraightModelGenerateFiles_pixel_size,
            image_1DStraightModelGenerateFiles_length,
            image_1DStraightModelGenerateFiles_decoded_length);

  this->GenerateSolverFilesButton->SetImageToIcon(this->GenerateSolverFilesIcon);

  this->ExecuteSolverPushButton->SetParent(this->SimulationToolBar);
  this->ExecuteSolverPushButton->Create(pvApp);

  this->ExecuteSolverPushButton->SetBalloonHelpString("Simulate current 1D model");
  this->ExecuteSolverPushButton->SetCommand(this, "ExecuteSolver");

  this->RunIcon->SetImage(image_1DStraightModelRunButton,
            image_1DStraightModelRunButton_width,
            image_1DStraightModelRunButton_height,
            image_1DStraightModelRunButton_pixel_size,
            image_1DStraightModelRunButton_length,
            image_1DStraightModelRunButton_decoded_length);

  this->ExecuteSolverPushButton->SetImageToIcon(this->RunIcon);

  this->ReadSimulationButton->GetLoadSaveDialog()->SetFileTypes("{{SolverGP simulation Files} {*.txt}}");
  this->ReadSimulationButton->GetLoadSaveDialog()->SetTitle("Load Simulation Data");
  this->ReadSimulationButton->SetText("Load\nData");
  this->ReadSimulationButton->SetParent(this->SimulationToolBar);
  this->ReadSimulationButton->Create(pvApp);
  this->ReadSimulationButton->EnabledOff();
  this->ReadSimulationButton->SetBalloonHelpString("Load data from simulation");
  this->ReadSimulationButton->SetWidth(45);
  this->ReadSimulationButton->SetCommand(this, "ReadSimulationButtonCallback");

  this->SimulationToolBar->AddWidget(this->GenerateSolverFilesButton);
  this->SimulationToolBar->AddWidget(this->ExecuteSolverPushButton);
  this->SimulationToolBar->AddWidget(this->ReadSimulationButton);


  this->GeneralInfoFrame->SetParent(this->Frame);
  this->GeneralInfoFrame->Create(pvApp);
  this->GeneralInfoFrame->SetLabelText("General Information");
  this->GeneralInfoFrame->CollapseFrame();


  this->GeneralFrame->SetParent(this->GeneralInfoFrame->GetFrame());
  this->GeneralFrame->Create(pvApp);

  this->GeneralFrame2->SetParent(this->GeneralInfoFrame->GetFrame());
  this->GeneralFrame2->Create(pvApp);

  char tmp[100] = {"Valor Default"};
  this->ModelNameValueText = tmp;

  this->SubTreePropFrame->SetParent(this->Frame);
  this->SubTreePropFrame->SetLabelText("Selected SubTree Information");
  this->SubTreePropFrame->Create(pvApp);


  this->SegmentsNumber->SetParent(this->GeneralFrame);
  this->SegmentsNumber->Create(pvApp);

  this->TerminalsNumber->SetParent(this->GeneralFrame);
  this->TerminalsNumber->Create(pvApp);

  this->NodesNumber->SetParent(this->GeneralFrame);
  this->NodesNumber->Create(pvApp);


  this->ResistanceEq->SetParent(this->SubTreePropFrame->GetFrame());
  this->ResistanceEq->Create(pvApp);

  this->CapacitanceEq->SetParent(this->SubTreePropFrame->GetFrame());
  this->CapacitanceEq->Create(pvApp);

  this->MaxRadius->SetParent(this->SubTreePropFrame->GetFrame());
  this->MaxRadius->Create(pvApp);

  this->MinRadius->SetParent(this->SubTreePropFrame->GetFrame());
  this->MinRadius->Create(pvApp);

  this->MaxWall->SetParent(this->SubTreePropFrame->GetFrame());
  this->MaxWall->Create(pvApp);

  this->MinWall->SetParent(this->SubTreePropFrame->GetFrame());
  this->MinWall->Create(pvApp);


  this->TreeVolumeLabel->SetParent(this->GeneralFrame);
  this->TreeVolumeLabel->Create(pvApp);
  this->TreeVolumeLabel->SetText("Volume of the tree:");

  this->SmallerSegmentNameLabel->SetParent(this->GeneralFrame);
  this->SmallerSegmentNameLabel->Create(pvApp);
  this->SmallerSegmentNameLabel->SetText("Smaller Segment:");

  this->SmallerSegmentNameEntry->SetParent(this->GeneralFrame2);
  this->SmallerSegmentNameEntry->Create(pvApp);
  this->SmallerSegmentNameEntry->SetWidth(10);
  this->SmallerSegmentNameEntry->EnabledOff();

  this->SmallerSegmentLengthLabel->SetParent(this->GeneralFrame2);
  this->SmallerSegmentLengthLabel->Create(pvApp);
  this->SmallerSegmentLengthLabel->SetText("Length:");

  this->SmallerSegmentLengthEntry->SetParent(this->GeneralFrame2);
  this->SmallerSegmentLengthEntry->Create(pvApp);
  this->SmallerSegmentLengthEntry->SetWidth(10);
  this->SmallerSegmentLengthEntry->EnabledOff();

  this->LengthElementLabel->SetParent(this->GeneralFrame2);
  this->LengthElementLabel->Create(pvApp);
  this->LengthElementLabel->SetText("Global Element Length:");

  this->LengthElementEntry->SetParent(this->GeneralFrame2);
  this->LengthElementEntry->Create(pvApp);
  this->LengthElementEntry->SetWidth(10);
  this->LengthElementEntry->EnabledOff();

  this->ConfigureDefaultLengthOfElementButton->SetParent(this->GeneralFrame2);
  this->ConfigureDefaultLengthOfElementButton->Create(pvApp);
  this->ConfigureDefaultLengthOfElementButton->SetText("Apply");
  this->ConfigureDefaultLengthOfElementButton->SetBalloonHelpString("Configure default length of all elements of the tree based in length specified in box (Element Length)");
  this->ConfigureDefaultLengthOfElementButton->SetCommand(this, "ConfigureDefaultLengthOfElements");
  this->ConfigureDefaultLengthOfElementButton->EnabledOff();

  this->HighlightSmallerSegmentButton->SetParent(this->GeneralFrame);
  this->HighlightSmallerSegmentButton->Create(pvApp);
  this->HighlightSmallerSegmentButton->SetText("Highlight");
  this->HighlightSmallerSegmentButton->SetBalloonHelpString("Highlight smaller segment");
  this->HighlightSmallerSegmentButton->SetCommand(this, "SetSmallerSegment");

  this->AllSegmentsNameLabel->SetParent(this->GeneralFrame2);
  this->AllSegmentsNameLabel->Create(pvApp);
  this->AllSegmentsNameLabel->SetText("Select a Segment: ");

  this->GeneralInfoSeparator->SetParent(this->GeneralFrame2);
  this->GeneralInfoSeparator->Create(pvApp);

  this->AllSegmentsNameComboBox->SetParent(this->GeneralFrame2);
  this->AllSegmentsNameComboBox->Create(pvApp);
  this->AllSegmentsNameComboBox->EnabledOn();
  this->AllSegmentsNameComboBox->SetWidth(5);
  this->AllSegmentsNameComboBox->ReadOnlyOn();
  this->AllSegmentsNameComboBox->SetCommand(this, "SelectSegment");

  this->ScaleFrame->SetParent(this->Frame);
  this->ScaleFrame->Create(pvApp);
  this->ScaleFrame->SetLabelText("Scale");
  this->ScaleFrame->CollapseFrame();

  this->XScaleThumbWheel->SetParent(this->ScaleFrame->GetFrame());
  this->XScaleThumbWheel->Create(pvApp);
  this->XScaleThumbWheel->SetRange(0.0, 4.0);
  this->XScaleThumbWheel->SetResolution(0.1);
  this->XScaleThumbWheel->DisplayEntryOn();
  this->XScaleThumbWheel->DisplayLabelOn();
  this->XScaleThumbWheel->DisplayEntryAndLabelOnTopOn();
  this->XScaleThumbWheel->GetLabel()->SetText("X: ");
  this->XScaleThumbWheel->EnabledOff();
  this->XScaleThumbWheel->ClampMaximumValueOn();
  this->XScaleThumbWheel->ClampMinimumValueOn();
  this->XScaleThumbWheel->SetValue(1.0);
  this->XScaleThumbWheel->SetCommand(this, "ConfigureXScale");

  this->YScaleThumbWheel->SetParent(this->ScaleFrame->GetFrame());
  this->YScaleThumbWheel->Create(pvApp);
  this->YScaleThumbWheel->SetRange(0.0, 4.0);
  this->YScaleThumbWheel->SetResolution(0.1);
  this->YScaleThumbWheel->DisplayEntryOn();
  this->YScaleThumbWheel->DisplayLabelOn();
  this->YScaleThumbWheel->DisplayEntryAndLabelOnTopOn();
  this->YScaleThumbWheel->GetLabel()->SetText("Y: ");
  this->YScaleThumbWheel->EnabledOff();
  this->YScaleThumbWheel->ClampMaximumValueOn();
  this->YScaleThumbWheel->ClampMinimumValueOn();
  this->YScaleThumbWheel->SetValue(1.0);
  this->YScaleThumbWheel->SetCommand(this, "ConfigureYScale");

  this->ZScaleThumbWheel->SetParent(this->ScaleFrame->GetFrame());
  this->ZScaleThumbWheel->Create(pvApp);
  this->ZScaleThumbWheel->SetRange(0.0, 4.0);
  this->ZScaleThumbWheel->SetResolution(0.1);
  this->ZScaleThumbWheel->DisplayEntryOn();
  this->ZScaleThumbWheel->DisplayLabelOn();
  this->ZScaleThumbWheel->DisplayEntryAndLabelOnTopOn();
  this->ZScaleThumbWheel->GetLabel()->SetText("Z: ");
  this->ZScaleThumbWheel->EnabledOff();
  this->ZScaleThumbWheel->ClampMaximumValueOn();
  this->ZScaleThumbWheel->ClampMinimumValueOn();
  this->ZScaleThumbWheel->SetValue(1.0);
  this->ZScaleThumbWheel->SetCommand(this, "ConfigureZScale");

  this->XSizeLabel->SetParent(this->ScaleFrame->GetFrame());
  this->XSizeLabel->Create(pvApp);
  this->XSizeLabel->SetText("Size: ");

  this->YSizeLabel->SetParent(this->ScaleFrame->GetFrame());
  this->YSizeLabel->Create(pvApp);
  this->YSizeLabel->SetText("Size: ");

  this->ZSizeLabel->SetParent(this->ScaleFrame->GetFrame());
  this->ZSizeLabel->Create(pvApp);
  this->ZSizeLabel->SetText("Size: ");

  this->XSizeEntry->SetParent(this->ScaleFrame->GetFrame());
  this->XSizeEntry->Create(pvApp);
  this->XSizeEntry->SetWidth(8);
  this->XSizeEntry->SetCommand(this, "ConfigureScale");
  this->XSizeEntry->EnabledOff();

  this->YSizeEntry->SetParent(this->ScaleFrame->GetFrame());
  this->YSizeEntry->Create(pvApp);
  this->YSizeEntry->SetWidth(8);
  this->YSizeEntry->SetCommand(this, "ConfigureScale");
  this->YSizeEntry->EnabledOff();

  this->ZSizeEntry->SetParent(this->ScaleFrame->GetFrame());
  this->ZSizeEntry->Create(pvApp);
  this->ZSizeEntry->SetWidth(8);
  this->ZSizeEntry->SetCommand(this, "ConfigureScale");
  this->ZSizeEntry->EnabledOff();

  this->ModelNameLabel->SetParent(this->GeneralFrame);
  this->ModelNameLabel->Create(pvApp);
  //this->ModelNameLabel->SetText(this->ModelNameLabelText);
  this->ModelNameLabel->SetText(s.c_str());
  this->ModelNameValue->SetParent(this->Frame);
  this->ModelNameValue->Create(pvApp);
  this->ModelNameValue->SetText(this->ModelNameValueText);

  this->VisualizationStateOptionMenu->SetParent(this->Frame);
  this->VisualizationStateOptionMenu->Create(pvApp);
  this->VisualizationStateOptionMenu->ExpandWidgetOff();
  this->VisualizationStateOptionMenu->SetBalloonHelpString("Select the visualization state.");

  vtkKWMenuButton *omenu = this->VisualizationStateOptionMenu->GetWidget();
  omenu->SetWidth(20);
  omenu->SetPadX(10);
  omenu->SetPadY(2);

  omenu->AddRadioButton( MODE_VISUALIZATION, this, "VisualizationStateCallback");
  omenu->AddRadioButton( MODE_EDITION, this, "VisualizationStateCallback");
  omenu->AddRadioButton( MODE_PLOT, this, "VisualizationStateCallback");
  omenu->AddRadioButton( MODE_TODETACHSUBTREE, this, "VisualizationStateCallback");
  omenu->AddRadioButton( MODE_ESPECIAL_TOOLS, this, "VisualizationStateCallback");
  omenu->SetValue( MODE_VISUALIZATION );

  this->VisualizationStateOptionMenu->GetWidget()->IndicatorVisibilityOn();
  this->VisualizationStateOptionMenu->GetLabel()->SetText("View: ");
  this->VisualizationStateOptionMenu->LabelVisibilityOn();

  if(pvApp)
    {
    this->TerminalWidget->SetFrame(this->Frame);
    this->TerminalWidget->ChildCreate(pvApp);
    this->SegmentWidget->SetFrame(this->Frame);
    this->SegmentWidget->ChildCreate(pvApp);
    this->ElementWidget->SetFrame(this->Frame);
    this->ElementWidget->ChildCreate(pvApp);

    this->NodeWidget->SetFrame(this->Frame);
    this->NodeWidget->ChildCreate(pvApp);
    this->PlotWidget->SetFrame(this->Frame);
    this->PlotWidget->ChildCreate(pvApp);	//provavelmente terá que ser criado uma apliicação pra o modo de plot animado
    this->HeartWidget->SetFrame(this->Frame);
    this->HeartWidget->ChildCreate(pvApp);

    this->SubTreeMeshWidget->SetFrame(this->Frame);
    this->SubTreeMeshWidget->ChildCreate(pvApp);

    this->MultipleTerminalWidget->SetFrame(this->Frame);
    this->MultipleTerminalWidget->ChildCreate(pvApp);

    this->MultipleSegmentWidget->SetFrame(this->Frame);
    this->MultipleSegmentWidget->ChildCreate(pvApp);

    this->InterventionsWidget->SetFrame(this->Frame);
    this->InterventionsWidget->ChildCreate(pvApp);
    }

  // Registro das Callbacks dos segmentos, terminais, nós etc.
  this->SegmentWidget->IsViscoelasticCheckButton->SetCommand(this, "SegmentViscoelasticCallback");
  this->SegmentWidget->InterpolationTypeMenuButton->GetWidget()->AddRadioButton( INTERPOLATION_LINEAR, this, "SegmentInterpolationCallback");
  this->SegmentWidget->InterpolationTypeMenuButton->GetWidget()->AddRadioButton( INTERPOLATION_CONSTANT, this, "SegmentInterpolationCallback");

  this->SegmentWidget->GetProxElastinCombo()->SetCommand(this, "UpdateSegmentProxViscoWidgets");
  this->SegmentWidget->GetDistalElastinCombo()->SetCommand(this, "UpdateSegmentDistalViscoWidgets");

  this->SegmentWidget->GetradioImposeRefPressure()->SetCommand(this, "UpdateTreeRefPressure");
  this->SegmentWidget->GetradioImposeInfPressure()->SetCommand(this, "UpdateTreeInfPressure");

  this->SegmentWidget->GetProxViscoElasticAngleEntry()->SetCommand(this, "UpdateProxViscoValue");
  this->SegmentWidget->GetProxcharacTimeEntry()->SetCommand(this, "UpdateProxViscoValue");
  this->SegmentWidget->GetProxViscoEntry()->SetCommand(this, "UpdateProxViscoAngleValue");
  this->SegmentWidget->GetProxElastEntry()->SetCommand(this, "UpdateProxViscoAngleValue");

  this->SegmentWidget->GetDistalViscoElasticAngleEntry()->SetCommand(this, "UpdateDistalViscoValue");
  this->SegmentWidget->GetDistalcharacTimeEntry()->SetCommand(this, "UpdateDistalViscoValue");
  this->SegmentWidget->GetDistalViscoEntry()->SetCommand(this, "UpdateDistalViscoAngleValue");
  this->SegmentWidget->GetDistalElastEntry()->SetCommand(this, "UpdateDistalViscoAngleValue");

  this->SegmentWidget->GetCloneSegmentButton()->SetCommand(this, "CloneSegmentCallback");
  this->SegmentWidget->GetAddSegmentButton()->SetCommand(this, "AddCloneSegmentCallback");
  this->SegmentWidget->GetRemoveSegmentButton()->SetCommand(this, "RemoveCloneSegmentCallback");
  this->SegmentWidget->GetRemoveAllSegmentButton()->SetCommand(this, "RemoveAllCloneSegmentCallback");
  this->SegmentWidget->GetFlowDirectionButton()->SetCommand(this, "InvertFlowDirectionCallback");

  this->PlotWidget->Pressure1Plot->SetCommand(this, "PlotTypeCallback");
  this->PlotWidget->Pressure2Plot->SetCommand(this, "PlotTypeCallback");
  this->PlotWidget->AreaPlot->SetCommand(this, "PlotTypeCallback");
  this->PlotWidget->FlowPlot->SetCommand(this, "PlotTypeCallback");
  this->PlotWidget->Velocity1Plot->SetCommand(this, "PlotTypeCallback");
  this->PlotWidget->Velocity2Plot->SetCommand(this, "PlotTypeCallback");

  this->PlotWidget->Pressure1PlotRadio->SetCommand(this, "SetAnimatedPlotType 0");//provavelmente terá que ser criado um novo objeto para o plot animado
  this->PlotWidget->Pressure2PlotRadio->SetCommand(this, "SetAnimatedPlotType 1");
  this->PlotWidget->AreaPlotRadio->SetCommand(this, "SetAnimatedPlotType 2");
  this->PlotWidget->FlowPlotRadio->SetCommand(this, "SetAnimatedPlotType 3");
  this->PlotWidget->Velocity1PlotRadio->SetCommand(this, "SetAnimatedPlotType 4");
  this->PlotWidget->Velocity2PlotRadio->SetCommand(this, "SetAnimatedPlotType 5");

  // Registro de callbacks do coração
  this->HeartWidget->PlotHeartButton->SetCommand(this, "PlotHeartCallback");
  this->HeartWidget->UpdatePropButton->SetCommand(this, "TerminalUpdateProps");
  this->HeartWidget->PressureImposes->SetCommand(this, "ConfigureHeartCurve P");
  this->HeartWidget->FlowImposes->SetCommand(this, "ConfigureHeartCurve F");
  this->HeartWidget->FunctionHeartCurve->SetCommand(this, "CreateHeartCurve");
  this->HeartWidget->AddCurveButton->SetCommand(this, "AddHeartCurve");
  this->HeartWidget->SaveCurveButton->SetCommand(this, "SaveHeartCurve");

  this->PlotWidget->GraphsUpdateButton->SetCommand(this, "PlotUpdateType");
  this->PlotWidget->ExportResultButton->SetCommand(this, "SetExportDirectory");
  this->PlotWidget->NormalPlotTypeRadio->SetCommand(this, "ChangePlotMode 0");
  this->PlotWidget->AnimatedPlotTypeRadio->SetCommand(this, "ChangePlotMode 1");

  this->TerminalWidget->GetUpdateTerminalPropButton()->SetCommand(this, "TerminalUpdateProps");
  this->TerminalWidget->GetUpdateNonLeafTerminalButton()->SetCommand(this, "TerminalsUpdateProps 0");
  this->TerminalWidget->GetUpdateLeafTerminalButton()->SetCommand(this, "TerminalsUpdateProps 1");
  this->TerminalWidget->GetUpdateTerminalCurves()->SetCommand(this, "TerminalsCurvesUpdate");

  this->TerminalWidget->GetUpdateR1Curve()->SetCommand(this, "EnableR1CurveEdition");
  this->TerminalWidget->GetUpdateR2Curve()->SetCommand(this, "EnableR2CurveEdition");
  this->TerminalWidget->GetUpdateCapCurve()->SetCommand(this, "EnableCapCurveEdition");
  this->TerminalWidget->GetUpdatePCurve()->SetCommand(this, "EnablePCurveEdition");

  this->TerminalWidget->GetAddCurveButton()->SetCommand(this, "ReadCurveTerminal");
  this->TerminalWidget->GetPlotButton()->SetCommand(this, "ShowCurveTerminal");
  this->TerminalWidget->GetAddCurveButtonOk()->SetCommand(this, "CalculateCurve");

  this->SegmentWidget->UpdateNodePropButton->SetCommand(this, "SegmentNodeUpdateProps");
  this->NodeWidget->UpdateSingleNodePropButton->SetCommand(this, "SingleNodeUpdateProps");
  this->ElementWidget->UpdateSingleElemPropButton->SetCommand(this, "SingleElementUpdateProps");
  this->SegmentWidget->UpdateElementsPropButton->SetCommand(this, "SegmentUpdateProps");
  this->InterventionsWidget->UpdatePropertiesButton->SetCommand(this, "InterventionsUpdateProperties");

  this->InputVisualizationModeButton->SetCommand(this, "UpdateToolbar 1");
  this->EditionModeButton->SetCommand(this, "UpdateToolbar 0");
  this->PlotModeButton->SetCommand(this, "UpdateToolbar 2");

  this->ToDetachSubTreeButton->SetCommand(this, "UpdateToolbar 3");
  this->SpecialToolsButton->SetCommand(this, "UpdateToolbar 4");

  this->EditSegButton->SetCommand(this, "SelectTreeElement 0 1");
  this->EditElemButton->SetCommand(this, "SelectTreeElement 1 0");
  this->MultipleTerminalWidget->GetUpdateSingleTermPropButton()->SetCommand(this, "SetMultipleTerminals");
  this->MultipleSegmentWidget->GetUpdateSingleElemPropButton()->SetCommand(this, "SetMultipleSegments 0");
  this->MultipleSegmentWidget->GetUpdateWholeSegPropButton()->SetCommand(this, "SetMultipleSegments 1");
  this->SubTreeMeshWidget->GetApplyButton()->SetCommand(this, "RefineSegments");

  //Setando o interactor para modo 3D
  pvWindow->SetInteractorStyle(vtkPVWindow::INTERACTOR_STYLE_3D);

  this->CreateEditToolbar(pvApp);
  this->CreateSurgicalInterventionsToolbar(pvApp);
  //this->CreatePathologyToolbar(pvApp);


  this->RemoveFilterRadioButton->SetParent(this->Frame);
  this->RemoveFilterRadioButton->Create(pvApp);
  this->RemoveFilterRadioButton->SetText("Remove");
  this->RemoveFilterRadioButton->SetValueAsInt(0);

  this->KeepFilterRadioButton->SetParent(this->Frame);
  this->KeepFilterRadioButton->Create(pvApp);
  this->KeepFilterRadioButton->SetText("Keep");
  this->KeepFilterRadioButton->SetValueAsInt(1);

  this->RemoveFilterRadioButton->SetSelectedState(1);
  this->KeepFilterRadioButton->SetVariableName(this->RemoveFilterRadioButton->GetVariableName());

  if(!disableUI)
    {
    disableUI = false;
    GetObjectTypeName(NULL, -1);
    }


  this->PlaceComponents();
  this->ConfigureComponents();
}

//-----------------------------------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::CreateEditToolbar(vtkPVApplication* pvApp)
{
	this->EditToolbar->SetParent(this->Window->GetViewPanelFrame());
  this->EditToolbar->Create(pvApp);

  this->EditRadioButtonSet->SetParent(this->EditToolbar);
  this->EditRadioButtonSet->Create(pvApp);

  this->EditToolbar->AddWidget(this->EditRadioButtonSet);

  this->EditGeometryIcon->SetImage(image_1DStraightModelEditGeometryButton,
  				  image_1DStraightModelEditGeometryButton_width,
  				  image_1DStraightModelEditGeometryButton_height,
  				  image_1DStraightModelEditGeometryButton_pixel_size,
  				  image_1DStraightModelEditGeometryButton_length,
  				  image_1DStraightModelEditGeometryButton_decoded_length);

  this->AddSegmentIcon->SetImage(image_1DStraightModelAddSegmentButton,
  				  image_1DStraightModelAddSegmentButton_width,
  				  image_1DStraightModelAddSegmentButton_height,
  				  image_1DStraightModelAddSegmentButton_pixel_size,
  				  image_1DStraightModelAddSegmentButton_length,
  				  image_1DStraightModelAddSegmentButton_decoded_length);

  this->AddTerminalIcon->SetImage(image_1DStraightModelAddTerminalButton,
  				  image_1DStraightModelAddTerminalButton_width,
  				  image_1DStraightModelAddTerminalButton_height,
  				  image_1DStraightModelAddTerminalButton_pixel_size,
  				  image_1DStraightModelAddTerminalButton_length,
  				  image_1DStraightModelAddTerminalButton_decoded_length);

  this->DeleteIcon->SetImage(image_1DStraightModelDeleteButton,
  				  image_1DStraightModelDeleteButton_width,
  				  image_1DStraightModelDeleteButton_height,
  				  image_1DStraightModelDeleteButton_pixel_size,
  				  image_1DStraightModelDeleteButton_length,
  				  image_1DStraightModelDeleteButton_decoded_length);

  this->MoveIcon->SetImage(image_1DStraightModelMoveButton,
  				  image_1DStraightModelMoveButton_width,
  				  image_1DStraightModelMoveButton_height,
  				  image_1DStraightModelMoveButton_pixel_size,
  				  image_1DStraightModelMoveButton_length,
  				  image_1DStraightModelMoveButton_decoded_length);

  this->CheckConsistenceIcon->SetImage(image_1DStraightModelCheckConsistenceButton,
    				  image_1DStraightModelCheckConsistenceButton_width,
    				  image_1DStraightModelCheckConsistenceButton_height,
    				  image_1DStraightModelCheckConsistenceButton_pixel_size,
    				  image_1DStraightModelCheckConsistenceButton_length,
    				  image_1DStraightModelCheckConsistenceButton_decoded_length);

  this->CopySegmentIcon->SetImage(image_1DStraightModelCopySegmentButton,
    				  image_1DStraightModelCopySegmentButton_width,
    				  image_1DStraightModelCopySegmentButton_height,
    				  image_1DStraightModelCopySegmentButton_pixel_size,
    				  image_1DStraightModelCopySegmentButton_length,
    				  image_1DStraightModelCopySegmentButton_decoded_length);

  this->FlipIcon->SetImage(image_1DStraightModelFlipButton,
    				  image_1DStraightModelFlipButton_width,
    				  image_1DStraightModelFlipButton_height,
    				  image_1DStraightModelFlipButton_pixel_size,
    				  image_1DStraightModelFlipButton_length,
    				  image_1DStraightModelFlipButton_decoded_length);

  this->RestartIcon->SetImage(image_1DStraightModelDeleteInterventionButton,
    				  image_1DStraightModelDeleteInterventionButton_width,
    				  image_1DStraightModelDeleteInterventionButton_height,
    				  image_1DStraightModelDeleteInterventionButton_pixel_size,
    				  image_1DStraightModelDeleteInterventionButton_length,
    				  image_1DStraightModelDeleteInterventionButton_decoded_length);

  this->EditGeometryButton 	= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::EditGeometry);
  this->AddSegmentButton 	= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::AddSegment);
  this->AddTerminalButton  	= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::AddTerminal);
  this->DeleteButton 		= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Delete);
  this->MoveButton 		= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Move);
  this->CheckConsistenceButton	= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::CheckConsistence);
  this->CopySegmentButton	= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::CopySegment);
  this->FlipButton		= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Flip);
  this->RestartButton		= this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Restart);
  this->KeepSubTreeButton   = this->EditRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::KeepSubTree);

  char s[50];
	char s1[50]={"UpdateEditButton"};

	this->EditGeometryButton->SetHighlightThickness(0);
  this->EditGeometryButton->IndicatorVisibilityOff();
  this->EditGeometryButton->SetBalloonHelpString("Edit Tree (Keep pressing 'A' key)");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::EditGeometry);
  this->EditGeometryButton->SetCommand(this, s);

  this->AddSegmentButton->SetHighlightThickness(0);
  this->AddSegmentButton->IndicatorVisibilityOff();
  this->AddSegmentButton->SetBalloonHelpString("Add Segment (Keep pressing 'A' key)");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::AddSegment);
  this->AddSegmentButton->SetCommand(this, s);

  this->AddTerminalButton->SetHighlightThickness(0);
  this->AddTerminalButton->IndicatorVisibilityOff();
  this->AddTerminalButton->SetBalloonHelpString("Add Terminal (Keep pressing 'A' key)");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::AddTerminal);
  this->AddTerminalButton->SetCommand(this, s);

  this->DeleteButton->SetHighlightThickness(0);
  this->DeleteButton->IndicatorVisibilityOff();
  this->DeleteButton->SetBalloonHelpString("Delete");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Delete);
  this->DeleteButton->SetCommand(this, s);

  this->MoveButton->SetHighlightThickness(0);
  this->MoveButton->IndicatorVisibilityOff();
  this->MoveButton->SetBalloonHelpString("Move (Keep pressing 'A' key)");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Move);
  this->MoveButton->SetCommand(this, s);

  this->CheckConsistenceButton->SetHighlightThickness(0);
  this->CheckConsistenceButton->IndicatorVisibilityOff();
  this->CheckConsistenceButton->SetBalloonHelpString("Check Consistence of the tree");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::CheckConsistence);
  this->CheckConsistenceButton->SetCommand(this, s);

  this->CopySegmentButton->SetHighlightThickness(0);
  this->CopySegmentButton->IndicatorVisibilityOff();
  this->CopySegmentButton->SetBalloonHelpString("Create a segment equal to the segment selected");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::CopySegment);
  this->CopySegmentButton->SetCommand(this, s);

  this->FlipButton->SetHighlightThickness(0);
  this->FlipButton->IndicatorVisibilityOff();
  this->FlipButton->SetBalloonHelpString("Flip segment selected");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Flip);
  this->FlipButton->SetCommand(this, s);

  this->RestartButton->SetHighlightThickness(0);
  this->RestartButton->IndicatorVisibilityOff();
  this->RestartButton->SetBalloonHelpString("Restart sub-tree!");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Restart);
  this->RestartButton->SetCommand(this, s);

  this->KeepSubTreeButton->SetHighlightThickness(0);
  this->KeepSubTreeButton->IndicatorVisibilityOff();
  this->KeepSubTreeButton->SetBalloonHelpString("Keep sub-tree!");
  this->KeepSubTreeButton->SetText("Keep");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::KeepSubTree);
  this->KeepSubTreeButton->SetCommand(this, s);

  this->EditGeometryButton->SetImageToIcon(this->EditGeometryIcon);
  this->AddSegmentButton->SetImageToIcon(this->AddSegmentIcon);
  this->AddTerminalButton->SetImageToIcon(this->AddTerminalIcon);
  this->DeleteButton->SetImageToIcon(this->DeleteIcon);
  this->MoveButton->SetImageToIcon(this->MoveIcon);
  this->CheckConsistenceButton->SetImageToIcon(this->CheckConsistenceIcon);
  this->CopySegmentButton->SetImageToIcon(this->CopySegmentIcon);
  this->FlipButton->SetImageToIcon(this->FlipIcon);
  this->RestartButton->SetImageToIcon(this->RestartIcon);

  this->EditRadioButtonSet->PackHorizontallyOn();

  this->EditRadioButtonSet->GetWidget(vtkPVHMStraightModelWidget::EditGeometry)->SetSelectedState(1);
  this->EditRadioButtonSet->ExpandWidgetsOn();
  this->EditRadioButtonSet->EnabledOff();
  this->CheckConsistenceButton->EnabledOn();
  this->CopySegmentButton->EnabledOff();
  this->FlipButton->EnabledOff();
  this->RestartButton->EnabledOff();
  this->KeepSubTreeButton->EnabledOff();
}

//-----------------------------------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::UpdateEditButton(int option)
{
  this->UpdateWidgetInfo();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  if ( option == vtkPVHMStraightModelWidget::Delete )
    {
    if(this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL)
      {
      if(proxy->GetStraightModelObjectSelected() == SEGMENT)
        {
        vtkIdList *listSegm = proxy->GetSubTreeListSegments();//pego a lista de segmentos
        option = vtkPVHMStraightModelWidget::EditGeometry;
        //pego o segmento clicado
        vtkHM1DTreeElement *segm = proxy->GetSegment(listSegm->GetId(0));
        //pego o terminal parent do segmento clicado
        vtkHM1DTreeElement *termParent = segm->GetFirstParent();
        //pego o segmento parent do terminal parent
        vtkHM1DTreeElement *segmParent = termParent->GetFirstParent();
        //verificar se o segmento 2 está na lista ou a lista está vazia
        if( (listSegm->IsId(2) != -1)||(listSegm->GetNumberOfIds()==0) )
          {
          if(listSegm->IsId(2) != -1)//mostra o aviso que o segmento 2 não pode ser apagado
                  vtkKWMessageDialog::PopupMessage(
                          this->GetApplication(), this->GetParentWindow(), "Error",
                          "Sorry, but the segment 2 cannot be removed.", vtkKWMessageDialog::ErrorIcon);
          else//se nada estiver selecionado mostra o aviso
                  vtkKWMessageDialog::PopupMessage(
                  this->GetApplication(), this->GetParentWindow(), "Error",
                  "Sorry, there are no segments selected.", vtkKWMessageDialog::ErrorIcon);
//	  		this->EditGeometryButton->SetSelectedState(1);
          listSegm->Delete();
          return;
          }
        //apago a lista de segmentos da combobox da interface
        else
          {
          for(int i=0; i < listSegm->GetNumberOfIds(); i++)
            {
            proxy->SetSegment(listSegm->GetId(i));
            const char *name = proxy->GetSegmentProxy()->GetSegmentName();
            this->SegmentWidget->RemoveSegmentNameInList((char*)name);
            int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
            this->AllSegmentsNameComboBox->DeleteValue(index);
            this->AllSegmentsNameComboBox->SetValue("");
            }
          }
        proxy->DeleteSegment(listSegm);
        if(segmParent)
                proxy->SetTermPropAfterDelSubTree(segmParent, this->CapEq, this->ResEq);
        listSegm->Delete();
        }
      else if(proxy->GetStraightModelObjectSelected() == TERMINAL)
        {
        vtkIdList *listSegm = proxy->GetSubTreeListSegments();//pego a lista de segmentos
        vtkIdList *listTerm = proxy->GetSubTreeListTerminals();//pego a lista de terminais
        option = vtkPVHMStraightModelWidget::EditGeometry;
        //pego o terminal clicado que é o primeiro da lista
        vtkHM1DTreeElement *term = proxy->GetTerminal(listTerm->GetId(0));
        //pego o segmento parent do terminal clicado
        vtkHM1DTreeElement *segmParent = term->GetFirstParent();
        //verificar se o segmento 2 está na lista ou a lista está vazia
        if( (listSegm->IsId(2) != -1)||(listSegm->GetNumberOfIds()==0) )
          {
          if(listSegm->IsId(2) != -1)//mostra o aviso que o segmento 2 não pode ser apagado
                  vtkKWMessageDialog::PopupMessage(
                          this->GetApplication(), this->GetParentWindow(), "Error",
                          "Sorry, but the segment 2 cannot be removed.", vtkKWMessageDialog::ErrorIcon);
          else//se nada estiver selecionado mostra o aviso
                  vtkKWMessageDialog::PopupMessage(
                  this->GetApplication(), this->GetParentWindow(), "Error",
                  "Sorry, there are no segments selected.", vtkKWMessageDialog::ErrorIcon);
//	  		this->EditGeometryButton->SetSelectedState(1);
          listSegm->Delete();
          return;
          }
        //apago a lista de segmentos da combobox da interface
        else
          {
          for(int i=0; i < listSegm->GetNumberOfIds(); i++)
            {
            proxy->SetSegment(listSegm->GetId(i));
            const char *name = proxy->GetSegmentProxy()->GetSegmentName();
            this->SegmentWidget->RemoveSegmentNameInList((char*)name);
            int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
            this->AllSegmentsNameComboBox->DeleteValue(index);
            this->AllSegmentsNameComboBox->SetValue("");
            }
          }
        proxy->DeleteSegment(listSegm);
        if(segmParent)
          proxy->SetTermPropAfterDelSubTree(segmParent, this->CapEq, this->ResEq);
        listSegm->Delete();
        listTerm->Delete();
        }
      }
    else
      {
      //se objeto selecionado for um segmento
      if ( proxy->GetStraightModelObjectSelected() == SEGMENT )
        {
        //pega o nome do segmento selecionado
        const char *name = proxy->GetSegmentProxy()->GetSegmentName();
        //remove da list box para clonar propriedades do segmento o nome
        this->SegmentWidget->RemoveSegmentNameInList((char*)name);
                int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
                this->AllSegmentsNameComboBox->DeleteValue(index);
                this->AllSegmentsNameComboBox->SetValue("");
        proxy->DeleteSegmentSelected();

        this->CalculateTreeVolume();
        option = vtkPVHMStraightModelWidget::EditGeometry;
        }
      else if ( proxy->GetStraightModelObjectSelected() == TERMINAL )
        {
        vtkClientServerID termCSID = proxy->GetTerminalProxy()->GetID(0);

        //verifica se o proxy está apontando para algum terminal
        if ( termCSID.ID > 0 )
          {
          int numberOfChilds = proxy->GetTerminalProxy()->GetTerminalChildCount();
          int numberOfParents = proxy->GetTerminalProxy()->GetTerminalParentCount();
          //verifica se o terminal pode ser removido
          if ( (numberOfChilds == 1) && (numberOfParents == 1) )
            {
            //pega o id do segmento filho
            vtkIdType childId = proxy->GetTerminalProxy()->GetFirstChildId();

            //pega o ClientServerId do segmento
            vtkIdType csid = proxy->GetSegmentCSID(childId);
            //seta no proxy de segmento o segmento que irá apontar
            proxy->GetSegmentProxy()->SetSegmentCSID(csid, -1, -1, 0, 0);
            //pega o nome do segmento
            char *n = proxy->GetSegmentProxy()->GetSegmentName();
            //remove o nome da lista de nomes de segmentos
            this->SegmentWidget->RemoveSegmentNameInList(n);
            int index = this->AllSegmentsNameComboBox->GetValueIndex(n);
            this->AllSegmentsNameComboBox->DeleteValue(index);

            //deleta o terminal
            proxy->DeleteTerminalSelected();
            option = vtkPVHMStraightModelWidget::EditGeometry;
            }
          }
        } // Fim else if ( proxy->GetStraightModelObjectSelected() == TERMINAL )
      }
    this->EditGeometryButton->SetSelectedState(1);
    } // Fim if ( option == vtkPVHMStraightModelWidget::Delete )
  else if ( option == vtkPVHMStraightModelWidget::CheckConsistence )
    {
    if ( this->EditionMode == option )
      {
      option = vtkPVHMStraightModelWidget::EditGeometry;
      this->EditGeometryButton->SetSelectedState(1);
      }
    else
          this->SelectTreeElement(0, 1);
//  	this->EditGeometryButton->SetSelectedState(1);
    }
  else if ( option == vtkPVHMStraightModelWidget::CopySegment )
    {
    proxy->CopySegment();

    vtkStringArray *names = proxy->GenerateListOfSegmentName();

    //remove todos os nomes da lista

    this->SegmentWidget->RemoveAllSegmentNameInList();
    this->AllSegmentsNameComboBox->DeleteAllValues();

    const char *n;
    //adiciona os nomes ordenados na lista
    for ( int i=0; i<names->GetNumberOfValues(); i++ )
      {
      n = names->GetValue(i);
      this->SegmentWidget->AddSegmentNameInList((char*)n);
      this->AllSegmentsNameComboBox->AddValue(n);
      }
    names->Delete();

    option = vtkPVHMStraightModelWidget::EditGeometry;
    this->EditGeometryButton->SetSelectedState(1);
    }
  else if ( option == vtkPVHMStraightModelWidget::Flip )
    {
    option = vtkPVHMStraightModelWidget::EditGeometry;
    this->EditGeometryButton->SetSelectedState(1);

    proxy->FlipSegments();
    }
  else if ( option == vtkPVHMStraightModelWidget::Restart )
    {
    option = vtkPVHMStraightModelWidget::EditGeometry;
    vtkIdList *segm;
    segm = proxy->RestartSubTree();//pego todos os segmentos do grid
    if(segm->GetNumberOfIds()<=0)
      {
      this->EditGeometryButton->SetSelectedState(1);
      segm->Delete();
      return;
      }
    else
      {
      option = vtkPVHMStraightModelWidget::EditGeometry;
      for(int i=0; i < segm->GetNumberOfIds(); i++)
        { //apago todos os segmentos da combobox
        proxy->SetSegment(segm->GetId(i));
        const char *name = proxy->GetSegmentProxy()->GetSegmentName();
        this->SegmentWidget->RemoveSegmentNameInList((char*)name);
        int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
        this->AllSegmentsNameComboBox->DeleteValue(index);
        this->AllSegmentsNameComboBox->SetValue("");
        }
      //apago todos os segmentos do grid
      proxy->DeleteSegment(segm);
      segm->Delete();
      this->EditGeometryButton->SetSelectedState(1);
      }
    }

  else if ( option == vtkPVHMStraightModelWidget::KeepSubTree )
    {
    if(this->GetWidgetMode() == MODE_TODETACHSUBTREE_VAL)
      {
      if ( (proxy->GetStraightModelObjectSelected() == SEGMENT) || (proxy->GetStraightModelObjectSelected() == TERMINAL) )
        {
        vtkIdList *listSegm = proxy->GetSubTreeListSegments();//pego a lista de segmentos
        option = vtkPVHMStraightModelWidget::EditGeometry;
        //pego o segmento clicado
        vtkHM1DTreeElement *segm = proxy->GetSegment(listSegm->GetId(0));
        //pego o terminal parent do segmento clicado
        vtkHM1DTreeElement *termParent = segm->GetFirstParent();
        //pego o segmento parent do terminal parent
        vtkHM1DTreeElement *segmParent = termParent->GetFirstParent();
        //verificar se o segmento 2 está na lista ou a lista está vazia
        if( (listSegm->GetNumberOfIds()==0) )
          {
          //se nada estiver selecionado mostra o aviso
          vtkKWMessageDialog::PopupMessage(
          this->GetApplication(), this->GetParentWindow(), "Error",
          "Sorry, there are no segments selected.", vtkKWMessageDialog::ErrorIcon);

          listSegm->Delete();
          return;
          }
        //apago a lista de segmentos da combobox da interface
        else
          {
          for(int i=0; i < listSegm->GetNumberOfIds(); i++)
            {
            proxy->SetSegment(listSegm->GetId(i));
            const char *name = proxy->GetSegmentProxy()->GetSegmentName();
            this->SegmentWidget->RemoveSegmentNameInList((char*)name);
            int index = this->AllSegmentsNameComboBox->GetValueIndex(name);
            this->AllSegmentsNameComboBox->DeleteValue(index);
            this->AllSegmentsNameComboBox->SetValue("");
            }
          }
        proxy->KeepSubTree(listSegm);
        listSegm->Delete();
        }
      }
    this->EditGeometryButton->SetSelectedState(1);
    } // Fim if ( option == vtkPVHMStraightModelWidget::KeepSubTree )

  this->EditionMode = option;
  proxy->SetEditionMode(option);
  this->UpdateWidgetInfo();
}

//-----------------------------------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::CreateSurgicalInterventionsToolbar(vtkPVApplication* pvApp)
{
	this->SurgicalInterventionsRadioButtonSet->SetParent(this->EditToolbar);
  this->SurgicalInterventionsRadioButtonSet->Create(pvApp);

  this->SurgicalInterventionsLabel->SetParent(this->EditToolbar);
  this->SurgicalInterventionsLabel->Create(pvApp);
  this->SurgicalInterventionsLabel->SetText("Surgical Interventions Tools:");

  this->EditToolbar->AddWidget(this->SurgicalInterventionsLabel);
  this->EditToolbar->AddWidget(this->SurgicalInterventionsRadioButtonSet);

  //Seta a imagem do icone
  this->ClipIcon->SetImage(image_1DStraightModelClipButton,
    image_1DStraightModelClipButton_width,
    image_1DStraightModelClipButton_height,
    image_1DStraightModelClipButton_pixel_size,
    image_1DStraightModelClipButton_length,
    image_1DStraightModelClipButton_decoded_length);

  this->StentIcon->SetImage(image_1DStraightModelStentButton,
    image_1DStraightModelStentButton_width,
    image_1DStraightModelStentButton_height,
    image_1DStraightModelStentButton_pixel_size,
    image_1DStraightModelStentButton_length,
    image_1DStraightModelStentButton_decoded_length);

  this->StenosisIcon->SetImage(image_1DStraightModelStenosisButton,
    image_1DStraightModelStenosisButton_width,
    image_1DStraightModelStenosisButton_height,
    image_1DStraightModelStenosisButton_pixel_size,
    image_1DStraightModelStenosisButton_length,
    image_1DStraightModelStenosisButton_decoded_length);

  this->AneurysmIcon->SetImage(image_1DStraightModelAneurysmButton,
    image_1DStraightModelAneurysmButton_width,
    image_1DStraightModelAneurysmButton_height,
    image_1DStraightModelAneurysmButton_pixel_size,
    image_1DStraightModelAneurysmButton_length,
    image_1DStraightModelAneurysmButton_decoded_length);

//  this->AgingIcon->SetImage(image_1DStraightModelAgingButton,
//    				  image_1DStraightModelAgingButton_width,
//    				  image_1DStraightModelAgingButton_height,
//    				  image_1DStraightModelAgingButton_pixel_size,
//    				  image_1DStraightModelAgingButton_length,
//    				  image_1DStraightModelAgingButton_decoded_length);

  this->DeleteInterventionIcon->SetImage(image_1DStraightModelDeleteInterventionButton,
    image_1DStraightModelDeleteInterventionButton_width,
    image_1DStraightModelDeleteInterventionButton_height,
    image_1DStraightModelDeleteInterventionButton_pixel_size,
    image_1DStraightModelDeleteInterventionButton_length,
    image_1DStraightModelDeleteInterventionButton_decoded_length);

  //Adiciona o botao na barra
  this->ClipButton 		= this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Clip);
  this->StentButton 		= this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Stent);
  this->StenosisButton 		= this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Stenosis);
  this->AneurysmButton 		= this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Aneurysm);
  this->AgingButton 		= this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::Aging);
  this->DeleteInterventionButton = this->SurgicalInterventionsRadioButtonSet->AddWidget(vtkPVHMStraightModelWidget::DeleteIntervention);

  //Seta a imagem do botao
  this->ClipButton->SetImageToIcon(this->ClipIcon);
  this->StentButton->SetImageToIcon(this->StentIcon);
  this->StenosisButton->SetImageToIcon(this->StenosisIcon);
  this->AneurysmButton->SetImageToIcon(this->AneurysmIcon);
//  this->AgingButton->SetImageToIcon(this->AgingIcon);
  this->DeleteInterventionButton->SetImageToIcon(this->DeleteInterventionIcon);

  char s[60];
  char s1[60]={"UpdateSurgicalInterventionsButton"};

	this->ClipButton->SetHighlightThickness(0);
  this->ClipButton->IndicatorVisibilityOff();
  this->ClipButton->SetBalloonHelpString("Create clip");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Clip);
  this->ClipButton->SetCommand(this, s);

  this->StentButton->SetHighlightThickness(0);
  this->StentButton->IndicatorVisibilityOff();
  this->StentButton->SetBalloonHelpString("Add stent");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Stent);
  this->StentButton->SetCommand(this, s);

	this->StenosisButton->SetHighlightThickness(0);
  this->StenosisButton->IndicatorVisibilityOff();
  this->StenosisButton->SetBalloonHelpString("Create Stenosis");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Stenosis);
  this->StenosisButton->SetCommand(this, s);

  this->AneurysmButton->SetHighlightThickness(0);
  this->AneurysmButton->IndicatorVisibilityOff();
  this->AneurysmButton->SetBalloonHelpString("Create Aneurysm");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Aneurysm);
  this->AneurysmButton->SetCommand(this, s);

  this->AgingButton->SetHighlightThickness(0);
  this->AgingButton->IndicatorVisibilityOff();
  this->AgingButton->SetBalloonHelpString("Aging");
  this->AgingButton->SetText("Aging");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::Aging);
  this->AgingButton->SetCommand(this, s);

  this->DeleteInterventionButton->SetHighlightThickness(0);
  this->DeleteInterventionButton->IndicatorVisibilityOff();
  this->DeleteInterventionButton->SetBalloonHelpString("Delete intervention");
  sprintf(s, "%s %d", s1,vtkPVHMStraightModelWidget::DeleteIntervention);
  this->DeleteInterventionButton->SetCommand(this, s);

  this->SurgicalInterventionsRadioButtonSet->PackHorizontallyOn();
  this->SurgicalInterventionsRadioButtonSet->ExpandWidgetsOn();
  this->SurgicalInterventionsRadioButtonSet->EnabledOff();
}

//-----------------------------------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::UpdateSurgicalInterventionsButton(int option)
{
  this->UpdateWidgetInfo();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  if ( this->SurgicalInterventionsMode == option )
    {
    this->SurgicalInterventionsRadioButtonSet->GetWidget(option)->SetSelectedState(0);
    option = vtkPVHMStraightModelWidget::none;
    }
  else if ( option == vtkPVHMStraightModelWidget::DeleteIntervention )
    {
    char *type = this->ObjectTypeValue->GetText();

    if ( strcmp(type,"Clip") == 0 )
      {
      vtkIdList *ids = proxy->GetSegmentsWithClip();

      vtkIdType csid = proxy->GetSegmentCSID(ids->GetId(1));

      proxy->GetSegmentProxy()->SetSegmentCSID(csid, -1, -1, 0, 0);
      char *n = proxy->GetSegmentProxy()->GetSegmentName();

      this->SegmentWidget->RemoveSegmentNameInList(n);
      int index = this->AllSegmentsNameComboBox->GetValueIndex(n);
      this->AllSegmentsNameComboBox->DeleteValue(index);

      ids->Delete();
      }
    this->SurgicalInterventionsRadioButtonSet->GetWidget(option)->SetSelectedState(0);
    proxy->SetSurgicalInterventionsMode(option);
    this->UpdateWidgetInfo();
    option = vtkPVHMStraightModelWidget::none;
    }
  else if ( option == vtkPVHMStraightModelWidget::Aging )
    {
    this->InterventionsWidget->SetObjectSelected(AGING);
    this->SetInterventionsWidgetValues();

    this->TerminalWidget->SetWidgetVisibility(0);
    this->NodeWidget->SetWidgetVisibility(0);
    this->SegmentWidget->SetWidgetVisibility(0);
    this->ElementWidget->SetWidgetVisibility(0);
    this->PlotWidget->SetWidgetVisibility(0);
    this->HeartWidget->SetWidgetVisibility(0);
    this->SubTreeMeshWidget->SetWidgetVisibility(0);
    this->MultipleTerminalWidget->SetWidgetVisibility(0);
    this->MultipleSegmentWidget->SetWidgetVisibility(0);
    this->InterventionsWidget->SetWidgetVisibility(1);

    this->PlaceComponents();
    }

  this->SurgicalInterventionsMode = option;
  proxy->SetSurgicalInterventionsMode(option);

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",
  this->Frame->GetWidgetName());

  this->Script("pack %s -padx 0 -pady 0 -side bottom -fill x -expand no",
  this->EditToolbar->GetWidgetName());

  this->Script("grid %s - - -sticky w -columnspan 2 -pady 5",
      this->ToolBar->GetWidgetName());

  this->Script("grid %s - - -sticky w -pady 2",
    this->SimulationLabel->GetWidgetName());

  this->Script("grid %s - - -sticky w",
    this->SimulationToolBar->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 4 -pady 7",
    this->GeneralInfoFrame->GetWidgetName());

  this->Script("pack %s -fill x -expand t -pady 2",
    this->GeneralFrame->GetWidgetName());

  this->Script("pack %s -fill x -expand t -pady 5",
    this->GeneralFrame2->GetWidgetName());

  this->Script("grid %s -sticky wns",
    this->ModelNameLabel->GetWidgetName());

  this->Script("grid %s - - -sticky w",
    this->SegmentsNumber->GetWidgetName());

  this->Script("grid %s - - -sticky w",
  this->TerminalsNumber->GetWidgetName());

  this->Script("grid %s - - -sticky w",
    this->NodesNumber->GetWidgetName());

  this->Script("grid %s -sticky w -row 0 -column 0 -columnspan 2",
    this->ResistanceEq->GetWidgetName());

  this->Script("grid %s -sticky w -row 1 -column 0 -columnspan 2",
    this->CapacitanceEq->GetWidgetName());

  this->Script("grid %s -sticky w -row 2 -column 0",
    this->MaxRadius->GetWidgetName());

  this->Script("grid %s -sticky w -row 3 -column 0",
      this->MinRadius->GetWidgetName());

  this->Script("grid %s -sticky w -row 4 -column 0",
    this->MaxWall->GetWidgetName());

  this->Script("grid %s -sticky w -row 5 -column 0",
    this->MinWall->GetWidgetName());

  this->Script("grid %s -sticky w",
    this->TreeVolumeLabel->GetWidgetName());

  this->Script("grid %s %s -sticky news -pady 2 -padx 4",
    this->SmallerSegmentNameLabel->GetWidgetName(),
    this->HighlightSmallerSegmentButton->GetWidgetName());

  this->Script("grid %s %s %s -sticky ew -pady 2 -padx 2",
    this->LengthElementLabel->GetWidgetName(),
    this->LengthElementEntry->GetWidgetName(),
    this->ConfigureDefaultLengthOfElementButton->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 4 -pady 10",
    this->GeneralInfoSeparator->GetWidgetName());

  this->Script("grid %s %s -sticky ew -pady 5",
    this->AllSegmentsNameLabel->GetWidgetName(),
    this->AllSegmentsNameComboBox->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 4",
      this->ScaleFrame->GetWidgetName());

  this->Script("grid %s %s %s - - -sticky w -pady 5 -padx 5",
    this->XScaleThumbWheel->GetWidgetName(),
    this->YScaleThumbWheel->GetWidgetName(),
    this->ZScaleThumbWheel->GetWidgetName());

  this->Script("grid %s %s %s - - -sticky w -pady 5 -padx 5",
    this->XSizeLabel->GetWidgetName(),
    this->YSizeLabel->GetWidgetName(),
    this->ZSizeLabel->GetWidgetName());

  this->Script("grid %s %s %s - - -sticky w -padx 5",
    this->XSizeEntry->GetWidgetName(),
    this->YSizeEntry->GetWidgetName(),
    this->ZSizeEntry->GetWidgetName());

  this->ElementWidget->PlaceComponents(this);
  this->TerminalWidget->PlaceComponents(this);
  this->HeartWidget->PlaceComponents(this);
  this->NodeWidget->PlaceComponents(this);
  this->SegmentWidget->PlaceComponents(this);
  this->PlotWidget->PlaceComponents(this);
  this->SubTreeMeshWidget->PlaceComponents(this);
  this->MultipleTerminalWidget->PlaceComponents(this);
  this->MultipleSegmentWidget->PlaceComponents(this);
  this->InterventionsWidget->PlaceComponents(this);
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0",
    this->Frame->GetWidgetName());

  this->Script("grid columnconfigure %s 1 -weight 2",
    this->Frame->GetWidgetName());

  this->ElementWidget->ConfigureComponents(this);
  this->TerminalWidget->ConfigureComponents(this);
  this->HeartWidget->ConfigureComponents(this);
  this->NodeWidget->ConfigureComponents(this);
  this->SegmentWidget->ConfigureComponents(this);
  this->PlotWidget->ConfigureComponents(this);
  this->SubTreeMeshWidget->PlaceComponents(this);
  this->MultipleTerminalWidget->PlaceComponents(this);
  this->MultipleSegmentWidget->PlaceComponents(this);
  this->InterventionsWidget->ConfigureComponents(this);
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SegmentViscoelasticCallback()
{
  this->SegmentWidget->IsViscoelasticCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SegmentInterpolationCallback()
{
  this->SegmentWidget->InterpolationTypeCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::PlotTypeCallback()
{
  this->PlotWidget->PlotTypeCallbackInternal(this->PlotOptions);
  this->SetGraphType();
  this->XYPlotWidget->SetPlotOptions(this->PlotOptions);
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::PlotHeartCallback(void)
{
  vtkSMHMStraightModelWidgetProxy* wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkDoubleArray *pt = wProxy->GetTerminalProxy()->GetTerminalProp('P', 1);

  //Caso coracao nao possua fluxo de entrada
  if ( (pt->GetNumberOfTuples() <= 1) && ( this->GetWidgetMode() == MODE_VISUALIZATION_VAL) )
    {
    vtkKWMessageDialog::PopupMessage(this->GetApplication(), this->Window, "Warning",
        "Heart not possesses configured curve!",
        vtkKWMessageDialog::WarningIcon);
    pt->Delete();
    return;
    }

  // first I have to acess the property for the terminal -- I supose it has been already updated
  vtkSMDoubleVectorProperty* term = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("TerminalProperties"));

  double FinalTime = term->GetElement(4);
  double MinFlow = term->GetElement(5);
  double MaxFlow = term->GetElement(6);

  FinalTime = this->HeartWidget->GetFinalTime();
  MaxFlow = this->HeartWidget->GetMaxFlow();
  MinFlow = this->HeartWidget->GetMinFlow();

  this->XYPlotWidget->SetMaxFlow(MaxFlow);
  this->XYPlotWidget->SetMinFlow(MinFlow);

  this->XYPlotWidget->SetEditCurveHeart(this->WidgetMode);

  vtkDoubleArray *pTime = wProxy->GetTerminalProxy()->GetTerminalTimeProp('P', 1);
  vtkDoubleArray *r1 = wProxy->GetTerminalProxy()->GetTerminalProp('R', 1);
  vtkDoubleArray *r2 = wProxy->GetTerminalProxy()->GetTerminalProp('R', 2);

  float tmpR1;
  float tmpR2;

  //Obtendo os dados de Resistência 1 e 2 e dados de pressão, para assim,
  //calcular o fluxo.

  //Pegando os dados de entrada para gerar curva do coração.
  vtkDoubleArray *FlowInHeart = vtkDoubleArray::New();
  FlowInHeart->SetNumberOfComponents(1);

  double flow, pressure;

  FlowInHeart->SetNumberOfTuples(pt->GetNumberOfTuples());

  tmpR1 = r1->GetValue(0); //Obtendo o valor da resistencia1 para o primeiro instante de tempo.
  tmpR2 = r2->GetValue(0); //Obtendo o valor da resistencia2 para o primeiro instante de tempo.

  //Somando as resistências.
  double TotalResistence = (double)(tmpR1+tmpR2);

  int i = this->HeartWidget->GetPointsScaleInit()-1;
  int n = this->HeartWidget->GetPointsScaleEnd();

  if ( (i < 0) || (i > n) )
    {
    i=1;
    this->HeartWidget->SetPointsScaleInit(i);
    }
  if ( (n > pt->GetNumberOfTuples()) || (n < i) )
    {
    n = pt->GetNumberOfTuples();
    this->HeartWidget->SetPointsScaleEnd(n);
    }

  while( i < n )
    {
    pt->SetValue(i, pt->GetValue(i)/this->XYPlotWidget->GetScaleFactor());
    pt->SetValue(i, pt->GetValue(i)*this->HeartWidget->GetScaleFactor());
    i++;
    }
    wProxy->GetTerminalProxy()->SetTerminalProp(pt, 'P', 1);

    i=0;
  while( i < pt->GetNumberOfTuples() )
    {
    //Se a soma das resistências for diferente de zero,
    //então é possível dividir a pressão pela soma das resistências,
    //e assim, encontrar a vazão.

    if(TotalResistence > 0)
      {
      flow = ((double)pt->GetValue(i)/TotalResistence);
      FlowInHeart->SetValue(i,flow);

      //Descobrindo o máximo e o mínimo.
      if(flow > this->XYPlotWidget->GetMaxFlow())
        this->XYPlotWidget->SetMaxFlow(flow);
      if(flow < this->XYPlotWidget->GetMinFlow())
        this->XYPlotWidget->SetMinFlow(flow);
      }
    else
      {
      //Descobrindo o máximo e o mínimo.
      if(pt->GetValue(i) > this->XYPlotWidget->GetMaxFlow())
        this->XYPlotWidget->SetMaxFlow(pt->GetValue(i));
      if(pt->GetValue(i) < this->XYPlotWidget->GetMinFlow())
        this->XYPlotWidget->SetMinFlow(pt->GetValue(i));

      pressure = pt->GetValue(i);
      FlowInHeart->SetValue(i,pt->GetValue(i));
      }
    i++;
    }

  //Obtendo o intervalo de tempo.
  this->XYPlotWidget->SetTMin(pTime->GetValue(0));
  this->XYPlotWidget->SetTMax(pTime->GetValue(pTime->GetNumberOfTuples()-1));

  //Chamando o método que constrói a curva de fluxo, para o coração, com base nos dados de resistência
  //e pressão iniciais.
  this->XYPlotWidget->BuildHeartPlot(3, FlowInHeart, pTime);
  this->XYPlotWidget->SetScaleFactor(this->HeartWidget->GetScaleFactor());


  //Adiciona as curvas ao plot e as exibe
  if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
    this->XYPlotWidget->ShowHMXYPlotWidget();

  FlowInHeart->Delete();
  pt->Delete();
  pTime->Delete();
  r1->Delete();
  r2->Delete();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::BuildHeartCurve()
{
  vtkSMHMStraightModelWidgetProxy* wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  wProxy->ClearPlot();

  //Pega valores da curva do coração
  vtkDoubleArray *pt = wProxy->GetTerminalProxy()->GetTerminalProp('P', 1);
  vtkDoubleArray *pTime = wProxy->GetTerminalProxy()->GetTerminalTimeProp('P', 1);

  // first I have to acess the property for the terminal -- I supose it has been already updated
  vtkSMDoubleVectorProperty* term = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("TerminalProperties"));

  double FinalTime = term->GetElement(4);
  double MinFlow = term->GetElement(5);
  double MaxFlow = term->GetElement(6);

  FinalTime = this->HeartWidget->GetFinalTime();
  MaxFlow = this->HeartWidget->GetMaxFlow();
  MinFlow = this->HeartWidget->GetMinFlow();

  //Caso coracao nao possua fluxo de entrada, seto valores iniciais
  if ( pt->GetNumberOfTuples() <= 1 )
    {
    this->XYPlotWidget->SetTMax(FinalTime);
    this->XYPlotWidget->SetMaxFlow(MaxFlow);
    this->XYPlotWidget->SetMinFlow(MinFlow);

    pt->SetNumberOfTuples(2);
    pTime->SetNumberOfValues(2);
    pt->SetValue(0, MinFlow);
    pt->SetValue(pt->GetNumberOfTuples()-1, MaxFlow);

    pTime->SetValue(0, 0);
    pTime->SetValue(1, FinalTime);
    }

  this->XYPlotWidget->SetMaxFlow(MaxFlow);
  this->XYPlotWidget->SetMinFlow(MinFlow);

  //Seta se esta selecionado modo visualização ou edição
  this->XYPlotWidget->SetEditCurveHeart(this->WidgetMode);

  //Pega valores das resistencias do coração
  vtkDoubleArray *r1 = wProxy->GetTerminalProxy()->GetTerminalProp('R', 1);
  vtkDoubleArray *r2 = wProxy->GetTerminalProxy()->GetTerminalProp('R', 2);

  float tmpR1;
  float tmpR2;

  //Obtendo os dados de Resistência 1 e 2 e dados de pressão, para assim,
  //calcular o fluxo.

  //Pegando os dados de entrada para gerar curva do coração.
  vtkDoubleArray *FlowInHeart = vtkDoubleArray::New();
  FlowInHeart->SetNumberOfComponents(1);

  double flow, pressure;

  FlowInHeart->SetNumberOfTuples(pt->GetNumberOfTuples());

  tmpR1 = r1->GetValue(0); //Obtendo o valor da resistencia1 para o primeiro instante de tempo.
  tmpR2 = r2->GetValue(0); //Obtendo o valor da resistencia2 para o primeiro instante de tempo.

  //Somando as resistências.
  double TotalResistence = (double)(tmpR1+tmpR2);

  int i = this->HeartWidget->GetPointsScaleInit()-1;
  int n = this->HeartWidget->GetPointsScaleEnd();

  if ( (i < 0) || (i > n) )
    {
    i=1;
    this->HeartWidget->SetPointsScaleInit(i);
    }
  if ( (n > pt->GetNumberOfTuples()) || (n < i) )
    {
    n = pt->GetNumberOfTuples();
    this->HeartWidget->SetPointsScaleEnd(n);
    }

  while( i < n )
    {
    pt->SetValue(i, pt->GetValue(i)/this->XYPlotWidget->GetScaleFactor());
    pt->SetValue(i, pt->GetValue(i)*this->HeartWidget->GetScaleFactor());
    i++;
    }
  wProxy->GetTerminalProxy()->SetTerminalProp(pt, 'P', 1);

  i=0;

  while( i < pt->GetNumberOfTuples() )
    {
    //Se a soma das resistências for diferente de zero,
    //então é possível dividir a pressão pela soma das resistências,
    //e assim, encontrar a vazão.
    if(TotalResistence > 0)
      {
      flow = ((double)pt->GetValue(i)/TotalResistence);
      FlowInHeart->SetValue(i,flow);

      //Descobrindo o máximo e o mínimo.
      if(flow > this->XYPlotWidget->GetMaxFlow())
        this->XYPlotWidget->SetMaxFlow(flow);
      if(flow < this->XYPlotWidget->GetMinFlow())
        this->XYPlotWidget->SetMinFlow(flow);
      }
    else
      {
      //Descobrindo o máximo e o mínimo.
      if(flow > this->XYPlotWidget->GetMaxFlow())
        this->XYPlotWidget->SetMaxFlow(flow);
      if(flow < this->XYPlotWidget->GetMinFlow())
        this->XYPlotWidget->SetMinFlow(flow);

      pressure = pt->GetValue(i);
      FlowInHeart->SetValue(i,pt->GetValue(i));

      }
    i++;
    }

  //Obtendo o intervalo de tempo.
  this->XYPlotWidget->SetTMin(pTime->GetValue(0));
  this->XYPlotWidget->SetTMax(pTime->GetValue(pTime->GetNumberOfTuples()-1));

  //Chamando o método que constrói a curva de fluxo, para o coração, com base nos dados de resistência
  //e pressão iniciais.
  this->XYPlotWidget->BuildHeartPlot(3, FlowInHeart, pTime);
  this->XYPlotWidget->SetScaleFactor(this->HeartWidget->GetScaleFactor());

  if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
    this->XYPlotWidget->Modified();

  FlowInHeart->Delete();
  pt->Delete();
  pTime->Delete();
  r1->Delete();
  r2->Delete();

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::PlotUpdateType()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

//	if (this->PlotWidget->AnimatedPlotTypeRadio->GetSelectedState())
//		{
//		this->SetAnimatedPlotType();
//		this->XYAnimatedPlotWidget->SetTypePlot(AnimatedPlot);
//	 	this->XYAnimatedPlotWidget->RebuildPlot();
//		}
//	else if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
  if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
    {
    this->SetGraphType();
    this->XYPlotWidget->SetTypePlot(this->PlotOptions);
    this->XYPlotWidget->RebuildPlot();
    }
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::TerminalUpdateProps()
{
  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
  this->WidgetProxy->GetProperty("StraightModelObjectSelected"));

  double R1, R2, Cap, P, finalTime, maxFlow, minFlow;

  vtkDoubleArray *prop = vtkDoubleArray::New();
  vtkDoubleArray *propTime = vtkDoubleArray::New();


  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  if (smSelect->GetElement(0) == HEART) // coracao selecionado
    {
    vtkDoubleArray *heartFlow;
    //vtkDoubleArray *time, *timeAnimated;
	vtkDoubleArray *time;

    //Limpa a janela do plot caso não esteja em modo de plotagem dos gráficos
    this->XYPlotWidget->Clear();

    //Pega os valores setados na interface do coração
    R1 				= this->HeartWidget->GetR1();
    R2 				= this->HeartWidget->GetR2();
    Cap 			= this->HeartWidget->GetCap();
    P 				= this->HeartWidget->GetP();
    finalTime = this->HeartWidget->GetFinalTime();
    maxFlow 	= this->HeartWidget->GetMaxFlow();
    minFlow 	= this->HeartWidget->GetMinFlow();

    //Seta no plot o máximo e mínimo da curva
    this->XYPlotWidget->SetMaxFlow(maxFlow);
    this->XYPlotWidget->SetMinFlow(minFlow);

    //Atualiza a curva editada, para pegar os valores aqui atualizados
    this->XYPlotWidget->UpdateHeartCurve();

    //Pega os valores da curva
    heartFlow = this->XYPlotWidget->GetHeartCurve();
    time = this->XYPlotWidget->GetTime();

    //Pega o penultimo valor de tempo para verificar se o valor de tempo
    //setado na interface é menor, caso seja não altera o valor final
    double n = time->GetValue(time->GetNumberOfTuples()-2);

    if ( n < finalTime )
      {
      this->XYPlotWidget->SetTMax(finalTime);
      this->XYAnimatedPlotWidget->SetTMax(finalTime);
      time->SetValue(time->GetNumberOfTuples()-1, finalTime);
      }
    else
      this->HeartWidget->SetFinalTime(time->GetValue(time->GetNumberOfTuples()-1));

    //Verifica se a soma das resistencias é maior que zero para calcular
    //o valor correto da curva e setar os valores das resistencias e capacitancia
    double TotalResistence = R1 + R2;
    if ( TotalResistence > 0 )
      {
      //Calcula o novo valor de pressão
      for ( int i=0; i<heartFlow->GetNumberOfTuples(); i++ )
        heartFlow->SetValue(i, heartFlow->GetValue(i)*TotalResistence);

      //Seta o tempo e os valores de pressão
      proxy->GetTerminalProxy()->SetTerminalTimeProp(time, 'P', 1);
      proxy->GetTerminalProxy()->SetTerminalProp(heartFlow, 'P', 1);

      //Calcula o range da curva para setar os valores das resistencias
      //e capacitancia
      prop->SetNumberOfValues(2);
      heartFlow->ComputeRange(0);
      double r[2];
      heartFlow->GetRange(r);

      //Seta resistencia 1
      prop->SetValue(0,R1);
      prop->SetValue(1,R1);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

      //Seta resistencia 2
      prop->SetValue(0,R2);
      prop->SetValue(1,R2);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

      //Seta capacitancia
      prop->SetValue(0,Cap);
      prop->SetValue(1,Cap);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'C', 0);
      }
    else
      {
      //Seta o tempo e os valores de pressão
      proxy->GetTerminalProxy()->SetTerminalTimeProp(time, 'P', 1);
      proxy->GetTerminalProxy()->SetTerminalProp(heartFlow, 'P', 1);

      prop->SetNumberOfValues(2);

      //Seta resistencia 1
      prop->SetValue(0,0);
      prop->SetValue(1,0);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

      //Seta resistencia 2
      prop->SetValue(0,0);
      prop->SetValue(1,0);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

      //Seta capacitancia
      prop->SetValue(0,0);
      prop->SetValue(1,0);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'C', 0);
      }

    //Setando tempo das resistencias e capacitancia
    prop->SetValue(0,time->GetValue(0));
    prop->SetValue(1,time->GetValue(time->GetNumberOfTuples()-1));
    proxy->GetTerminalProxy()->SetTerminalTimeProp(prop, 'R', 1);
    proxy->GetTerminalProxy()->SetTerminalTimeProp(prop, 'R', 2);
    proxy->GetTerminalProxy()->SetTerminalTimeProp(prop, 'C', 1);
    }
  else
    {
    //*******************************************
    // Setando parametros para os terminais "comuns"
    //*******************************************
    if (!strcmp(this->TerminalWidget->GetR1Entry()->GetValue(), "Function"))
      {
      //cout << "Funcao definida R1" << endl;
      proxy->GetTerminalProxy()->SetTerminalProp(this->TerminalWidget->GetR1(), 'R', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(this->TerminalWidget->GetR1Time(), 'R', 1);
      }
    else
      {
      if (this->TerminalWidget->SomeFunctionIsSelectd())
        {
        // se alguma parametro esta definido com funcao
        int ParamNumber = this->TerminalWidget->SomeFunctionIsSelectd();
        R1 = this->TerminalWidget->GetR1Single();
        //cout << "valor constante R1 mas outra funcao é variante" << R1 << endl;
        // cria funcao constante com 2 pontos
        prop->SetNumberOfValues(2);
        prop->SetValue(0,R1);
        prop->SetValue(1,R1);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

        // criando vetor para tempo
        propTime->SetNumberOfValues(2);
        propTime->SetValue(0,0);
        // obtendo o tempo final da funcao definida
        int TimeTuples = this->TerminalWidget->GetTimeArray(ParamNumber)->GetNumberOfTuples() - 1;
        double LastTime = this->TerminalWidget->GetTimeArray(ParamNumber)->GetValue(TimeTuples);
        propTime->SetValue(1,LastTime);
        proxy->GetTerminalProxy()->SetTerminalTimeProp(propTime, 'R', 1);

        }
      else
        {
        //cout << "valor constante R1" << endl;
        // se valor de R1 e os demais parametros sao constantes
        R1 = this->TerminalWidget->GetR1Single();
        prop->SetNumberOfValues(1);
        prop->SetValue(0,R1);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);
        }
      }

    // R2 *********************
    if (!strcmp(this->TerminalWidget->GetR2Entry()->GetValue(), "Function"))
      {
      //cout << "Funcao definida R2" << endl;
      proxy->GetTerminalProxy()->SetTerminalProp(this->TerminalWidget->GetR2(), 'R', 2);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(this->TerminalWidget->GetR2Time(), 'R', 2);
      }
    else
      {
      if (this->TerminalWidget->SomeFunctionIsSelectd())
        {
        int ParamNumber = this->TerminalWidget->SomeFunctionIsSelectd();
        R2 = this->TerminalWidget->GetR2Single();
        //cout << "valor constante R2 mas outra funcao é variante" << R2 << endl;
        prop->SetNumberOfValues(2);
        prop->SetValue(0,R2);
        prop->SetValue(1,R2);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

        propTime->SetNumberOfValues(2);
        propTime->SetValue(0,0);
        // obtendo o tempo final da funcao definida
        int TimeTuples = this->TerminalWidget->GetTimeArray(ParamNumber)->GetNumberOfTuples() - 1;
        double LastTime = this->TerminalWidget->GetTimeArray(ParamNumber)->GetValue(TimeTuples);
        propTime->SetValue(1,LastTime);
        proxy->GetTerminalProxy()->SetTerminalTimeProp(propTime, 'R', 2);

        }
      else
        {
        //cout << "valor constante R2" << endl;
        R2 = this->TerminalWidget->GetR2Single();
        prop->SetNumberOfValues(1);
        prop->SetValue(0,R2);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);
        }
      }

    // CAP ***************************
    if (!strcmp(this->TerminalWidget->GetCapEntry()->GetValue(), "Function"))
      {
      //cout << "Funcao definida Cap" << endl;
      proxy->GetTerminalProxy()->SetTerminalProp(this->TerminalWidget->GetCap(), 'C', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(this->TerminalWidget->GetCapTime(), 'C', 1);
      }
    else
      {
      if (this->TerminalWidget->SomeFunctionIsSelectd())
        {
        int ParamNumber = this->TerminalWidget->SomeFunctionIsSelectd();
        Cap = this->TerminalWidget->GetCapSingle();
        //cout << "valor constante Cap mas outra funcao é variante" << Cap << endl;
        prop->SetNumberOfValues(2);
        prop->SetValue(0,Cap);
        prop->SetValue(1,Cap);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'C', 1);

        propTime->SetNumberOfValues(2);
        propTime->SetValue(0,0);
        // obtendo o tempo final da funcao definida
        int TimeTuples = this->TerminalWidget->GetTimeArray(ParamNumber)->GetNumberOfTuples() - 1;
        double LastTime = this->TerminalWidget->GetTimeArray(ParamNumber)->GetValue(TimeTuples);
        propTime->SetValue(1,LastTime);
        proxy->GetTerminalProxy()->SetTerminalTimeProp(propTime, 'C', 1);

        }
      else
        {
        //cout << "valor constante Cap" << endl;
        Cap = this->TerminalWidget->GetCapSingle();
        prop->SetNumberOfValues(1);
        prop->SetValue(0,Cap);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'C', 1);
        }
      }

    // *********************** Pressure *****************************************************
    if (!strcmp(this->TerminalWidget->GetPressureEntry()->GetValue(), "Function"))
      {
      //cout << "Funcao definida Pressure" << endl;
      proxy->GetTerminalProxy()->SetTerminalProp(this->TerminalWidget->GetP(), 'P', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(this->TerminalWidget->GetPTime(), 'P', 1);
      }
    else
      {
      if (this->TerminalWidget->SomeFunctionIsSelectd())
        {
        int ParamNumber = this->TerminalWidget->SomeFunctionIsSelectd();
        P = this->TerminalWidget->GetPSingle();
        //cout << "valor constante Pressure mas outra funcao é variante" << P << endl;
        prop->SetNumberOfValues(2);
        prop->SetValue(0,P);
        prop->SetValue(1,P);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'P', 1);

        propTime->SetNumberOfValues(2);
        propTime->SetValue(0,0);
        // obtendo o tempo final da funcao definida
        int TimeTuples = this->TerminalWidget->GetTimeArray(ParamNumber)->GetNumberOfTuples() - 1;
        double LastTime = this->TerminalWidget->GetTimeArray(ParamNumber)->GetValue(TimeTuples);
        propTime->SetValue(1,LastTime);
        proxy->GetTerminalProxy()->SetTerminalTimeProp(propTime, 'P', 1);

        }
      else
        {
        //cout << "valor constante Pressure" << endl;
        P = this->TerminalWidget->GetPSingle();
        prop->SetNumberOfValues(1);
        prop->SetValue(0,P);
        proxy->GetTerminalProxy()->SetTerminalProp(prop, 'P', 1);
        }
      }
    }

  prop->Delete();
  propTime->Delete();
  this->TerminalWidget->GetUpdateTerminalPropButton()->Focus();

}

// ----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::TerminalsUpdateProps(int option)
{

  vtkKWMessageDialog::PopupMessage(
  this->GetApplication(), this->GetParentWindow(), "Function Disabled",
  "Function Disabled - AT WORK! ziemer",
   vtkKWMessageDialog::ErrorIcon);


//	double R1, R2, Cap, P;
//	R1 = this->TerminalWidget->GetR1();
//	R2 = this->TerminalWidget->GetR2();
//	Cap = this->TerminalWidget->GetCap();
//	P = this->TerminalWidget->GetP();
//
//	vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
//
//	proxy->TerminalsUpdateProps(option, R1, R2, Cap, P);
//
//	if (option)
//		vtkKWMessageDialog::PopupMessage(
//		this->GetApplication(), this->GetParentWindow(), "Ok",
//		"Tree Leaf Terminals Updated",
//		vtkKWMessageDialog::OkDefault );
//	else
//		vtkKWMessageDialog::PopupMessage(
//		this->GetApplication(), this->GetParentWindow(), "Ok",
//		"Tree Non Leaf Terminals Updated",
//		vtkKWMessageDialog::OkDefault );
//

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetGeometryWidgetValues(void)
{
//	double values[4];
//	vtkSMDoubleVectorProperty* geo = vtkSMDoubleVectorProperty::SafeDownCast(
//		      		this->WidgetProxy->GetProperty("GeometryValuesProperty"));
//	if (geo)
//		for(int i = 0; i < 4; i++)
//			values[i] = geo->GetElement(i);
//	this->GeometryWidget->UpdateEntryValues(values);
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetNodeWidgetValues(void)
{
  // first I have to acess the property for the node -- I supose it has been already updated
  vtkSMDoubleVectorProperty* node = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("NodeProperty"));
  // if I found the prop
  if (node)
    {
    double wallthickness 	=    node->GetElement(0);
    double radius 			=      node->GetElement(1);
    this->NodeWidget->SetRadius(radius);
    this->NodeWidget->SetWallThickness(wallthickness);

    char id[20];
    sprintf(id, "%.0f", node->GetElement(5));
    this->NodeWidget->SetNodeLabelMeshID(id);

    char data[100];
//		if (node->GetElement(4))
    sprintf(data, "Coordinates:  %.6f  %.6f  %.6f", node->GetElement(2), node->GetElement(3), node->GetElement(4) );
//		else
//				sprintf(data, "Coordinates: %.6f %.6f", node->GetElement(2), node->GetElement(3));

    this->NodeWidget->SetNodeCoordinates(data);
    }
  else
    {
    vtkDebugMacro("I dont have the properties for this node .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetTerminalWidgetValues(void)
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkDoubleArray *r1 = proxy->GetTerminalProxy()->GetTerminalProp('R', 1);
  vtkDoubleArray *r2 = proxy->GetTerminalProxy()->GetTerminalProp('R', 2);
  vtkDoubleArray *cap = proxy->GetTerminalProxy()->GetTerminalProp('C', 1);
  vtkDoubleArray *p = proxy->GetTerminalProxy()->GetTerminalProp('P', 1);

  this->TerminalWidget->SetR1(r1);
  this->TerminalWidget->SetR2(r2);
  this->TerminalWidget->SetCap(cap);
  this->TerminalWidget->SetP(p);

  vtkDoubleArray *r1Time = proxy->GetTerminalProxy()->GetTerminalTimeProp('R', 1);
  vtkDoubleArray *r2Time = proxy->GetTerminalProxy()->GetTerminalTimeProp('R', 2);
  vtkDoubleArray *capTime = proxy->GetTerminalProxy()->GetTerminalTimeProp('C', 1);
  vtkDoubleArray *pTime = proxy->GetTerminalProxy()->GetTerminalTimeProp('P', 1);

  this->TerminalWidget->SetR1Time(r1Time);
  this->TerminalWidget->SetR2Time(r2Time);
  this->TerminalWidget->SetCapTime(capTime);
  this->TerminalWidget->SetPTime(pTime);

  r1->Delete();
  r2->Delete();
  cap->Delete();
  p->Delete();

  r1Time->Delete();
  r2Time->Delete();
  capTime->Delete();
  pTime->Delete();

  // first I have to acess the property for the terminal -- I supose it has been already updated
  vtkSMDoubleVectorProperty* term = vtkSMDoubleVectorProperty::SafeDownCast(
              this->WidgetProxy->GetProperty("TerminalProperties"));
  // if I found the prop
  if (term)
    {
    vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
        this->WidgetProxy->GetProperty("StraightModelObjectSelected"));

    double R1	= term->GetElement(0);
    double R2	= term->GetElement(1);
    double Cap = term->GetElement(2);
    double P	= term->GetElement(3);
    double FinalTime 	= term->GetElement(4);
    double MinFlow 	= term->GetElement(5);
    double MaxFlow 	= term->GetElement(6);
    int meshID 	= (int)term->GetElement(7);
    string AssNodesList;

    s.clear();
    s.append("Function");


    if (smSelect->GetElement(0) == HEART) // coracao selecionado
      {
       if ( this->GetWidgetMode() != MODE_PLOT_VAL )
        this->BuildHeartCurve();

        vtkDoubleArray *heartFlow = this->XYPlotWidget->GetHeartCurve();

      //Seta na interface do coração o número de pontos e o tempo final da curva.
      this->HeartWidget->SetNumberOfPoints(heartFlow->GetNumberOfTuples());
      this->HeartWidget->SetFinalTime(FinalTime);
      this->HeartWidget->SetPointsScaleInit(1);
      this->HeartWidget->SetPointsScaleEnd(heartFlow->GetNumberOfTuples());

      double TotalResistence = R1 + R2;
      if ( TotalResistence > 0 )
        {
        this->HeartWidget->SetMaxFlow(MaxFlow/TotalResistence);
        this->HeartWidget->SetMinFlow(MinFlow/TotalResistence);
        this->HeartWidget->SetP(P/TotalResistence);
        this->HeartWidget->SetImposes('F');
        }
      else
        {
        this->HeartWidget->SetMaxFlow(MaxFlow);
        this->HeartWidget->SetMinFlow(MinFlow);
        this->HeartWidget->SetP(P);
        this->HeartWidget->SetImposes('P');
        }


      if (R1 == -1)
        //this->HeartWidget->SetR1("Function");
        this->HeartWidget->SetR1(s.c_str());
      else
        this->HeartWidget->SetR1(R1);

      if (R2 == -1)
        this->HeartWidget->SetR2(s.c_str());
      else
        this->HeartWidget->SetR2(R2);

      if (Cap == -1)
        this->HeartWidget->SetCap(s.c_str());
      else
        this->HeartWidget->SetCap(Cap);

      }
    else // outro terminal qualquer
      {
      //this->TerminalWidget->GetAddCurveButton()->SetText("Read Function");
      this->TerminalWidget->GetUpdateTerminalCurves()->SetText("Read functions from file");
      if (R1 == -1)
        this->TerminalWidget->SetR1(s.c_str());
      else
        this->TerminalWidget->SetR1(R1);

      if (R2 == -1)
        this->TerminalWidget->SetR2(s.c_str());
      else
        this->TerminalWidget->SetR2(R2);

      if (Cap == -1)
        this->TerminalWidget->SetCap(s.c_str());
      else
        this->TerminalWidget->SetCap(Cap);

      if (P == -1)
        this->TerminalWidget->SetP(s.c_str());
      else
        this->TerminalWidget->SetP(P);

      this->TerminalWidget->SetmeshID(meshID);

      vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

      AssNodesList = proxy->GetTerminalProxy()->GetAssNodesMeshID();

      //troca os caracteres ";" por espaços
	 // size_t found;
	 // for(int i = 0; i < AssNodesList.length(); i++)
		//AssNodesList.replace(AssNodesList.find(";"),1,"-");

	  this->TerminalWidget->SetAssNodes(AssNodesList.c_str());
      }
    }
  else
    {
    vtkDebugMacro("I dont have the properties for this terminal .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetElementWidgetValues(void)
{
  // first I have to acess the property for the node -- I supose it has been already updated
  vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("ElementProperty"));

  // if I found the prop
  if (elem)
    {
    double Elastin		=	elem->GetElement(0);
    double Collagen		=	elem->GetElement(1);
    double CoefficientA	=	elem->GetElement(2);
    double CoefficientB	=	elem->GetElement(3);
    double Viscoelasticity	=	elem->GetElement(4);
    double ViscoelasticityExponent	=	elem->GetElement(5);
    double InfiltrationPressure	=	elem->GetElement(6);
    double ReferencePressure		=	elem->GetElement(7);
    double Permeability		=	elem->GetElement(8);
    double WallThickness		= 	elem->GetElement(9);
    int meshID			= (int)elem->GetElement(10);

    this->ElementWidget->SetElastin(Elastin);
    this->ElementWidget->SetCollagen(Collagen);
    this->ElementWidget->SetCoefficientA(CoefficientA);
    this->ElementWidget->SetCoefficientB(CoefficientB);
    this->ElementWidget->SetViscoelasticity(Viscoelasticity);
    this->ElementWidget->SetViscoelasticityExponent (ViscoelasticityExponent);
    this->ElementWidget->SetInfiltrationPressure(InfiltrationPressure);
    this->ElementWidget->SetReferencePressure(ReferencePressure);
    this->ElementWidget->SetPermeability(Permeability);
    this->ElementWidget->SetmeshID(meshID);
    }
  else
    {
    vtkWarningMacro("I dont have the properties for this element .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetBasicSegmentWidgetValues(void)
{
  /*
  0, Length
  1, DX
  2, NumberOfElements
  3, -1); // definir o tipo de interpolacao
  4, Proximal_Radius
  5, Distal_Radius
  6, Proximal_Wallthickness
  7, Distal_Wallthickness
  8, Proximal_Elastin
  9, Distal_Elastin
  10, Proximal_Permeability
  11, Distal_Permeability
  12, Proximal_InfiltrationPressure
  13, Distal_InfiltrationPressure
  14, Proximal_ReferencePressure
  15, Distal_ReferencePressure
  */

  // first I have to acess the property for the segment -- I supose it has been already updated
  vtkSMDoubleVectorProperty* seg = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("BasicSegmentProperty"));

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  char *name = proxy->GetSegmentProxy()->GetSegmentName();

  char n[100];
  strcpy(n, name);

  this->SegmentWidget->SetSegmentName(n);

  // if I found the prop
  if (seg)
    {
    this->SegmentWidget->SetLength(seg->GetElement(0));
    this->SegmentWidget->SetDx(seg->GetElement(1));
    this->SegmentWidget->SetNumberOfElements(int(seg->GetElement(2)));
    this->SegmentWidget->SetVolume(seg->GetElement(16));

    this->SegmentWidget->SetCompliance(seg->GetElement(17));



    this->SegmentWidget->SetProxRadius(seg->GetElement(4));
    this->SegmentWidget->SetDistalRadius(seg->GetElement(5));

    this->SegmentWidget->SetProxWall(seg->GetElement(6));
    this->SegmentWidget->SetDistWall(seg->GetElement(7));

    this->SegmentWidget->SetProxElast(seg->GetElement(8));
    this->SegmentWidget->SetDistElast(seg->GetElement(9));

    this->SegmentWidget->SetProxPerm(seg->GetElement(10));
    this->SegmentWidget->SetDistPerm(seg->GetElement(11));

    this->SegmentWidget->SetProxInf(seg->GetElement(12));
    this->SegmentWidget->SetDistInf(seg->GetElement(13));

    this->SegmentWidget->SetProxRef(seg->GetElement(14));
    this->SegmentWidget->SetDistRef(seg->GetElement(15));
    }
  else
    {
    vtkWarningMacro("I dont have the properties for this segment .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetAdvancedSegmentWidgetValues(void)
{
  // first I have to acess the property for the segment -- I supose it has been already updated
  vtkSMDoubleVectorProperty* seg = vtkSMDoubleVectorProperty::SafeDownCast(
    this->WidgetProxy->GetProperty("AdvancedSegmentProperty"));
    /*
    <!--  name: AdvancedSegmentProperty (double array - size 10)
    These values are updated when a element or node is selected by the user
    0- Elastina do colageno proximal
    1- Elastina do colageno distal
    2- Valor medio de enrolamento proximal
    3- Valor medio de enrolamento distal
    4- Dispersao do enrolamento proximal
    5- Dispersao do enrolamento distal
    6- Fluidez da parede arterial proximal
    7- Fluidez da parede arterial distal
    8- Expoente viscoelastico proximal
    9- Expoente viscoelastico distal
    -->
    */
  // if I found the prop
  if (seg)
    {
    this->SegmentWidget->SetProxColag(seg->GetElement(0));
    this->SegmentWidget->SetDistalColag(seg->GetElement(1));
    this->SegmentWidget->SetProxCoefA(seg->GetElement(2));
    this->SegmentWidget->SetDistalCoefA(seg->GetElement(3));
    this->SegmentWidget->SetProxCoefB(seg->GetElement(4));
    this->SegmentWidget->SetDistalCoefB(seg->GetElement(5));
    this->SegmentWidget->SetProxVisco(seg->GetElement(6));
    this->SegmentWidget->SetDistalVisco(seg->GetElement(7));
    this->SegmentWidget->SetProxExpVisco(seg->GetElement(8));
    this->SegmentWidget->SetDistalExpVisco(seg->GetElement(9));
    }
  else
    {
    vtkWarningMacro("I dont have the properties for this segment .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetInterventionsWidgetValues()
{
  vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("InterventionsProperty"));

  // if I found the prop
  if (elem)
    {
    double Size			=	elem->GetElement(0);
    double Elastin      	=	elem->GetElement(1);
    double Viscoelasticity	=	elem->GetElement(2);

    this->InterventionsWidget->SetSize(Size);
    this->InterventionsWidget->SetElastin(Elastin);
    this->InterventionsWidget->SetViscoElasticity(Viscoelasticity);
    }
  else
    {
    vtkWarningMacro("I dont have the properties for this intervention or pathology .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetGraphType()
{
  vtkSMIntVectorProperty* graph = vtkSMIntVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("SelectedGraphic"));

  // if I found the prop
  if (graph)
    {
    for(int i = 0; i < PLOTS; i++)
      {
      graph->SetElement(i, this->PlotOptions->GetValue(i));
      }
    }
  else
    {
    vtkWarningMacro("Can not find the properties for graph type .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetTreeGeneralPropertiesWidgetValues(void)
{

  // first I have to acess the property for the tree -- I supose it has been already updated
  vtkSMIntVectorProperty* tree = vtkSMIntVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("TreeGeneralProperty"));

  // 0 - number of segments
  // 1 - number of terminals
  // 2 - number of nodes

  if (tree)
    {
    char data[50];
    sprintf(data, "Segments: %i", tree->GetElement(0));
    this->SegmentsNumber->SetText(data);

    sprintf(data, "Terminals: %i", tree->GetElement(1));
    this->TerminalsNumber->SetText(data);

    sprintf(data, "Nodes: %i", tree->GetElement(2));
    this->NodesNumber->SetText(data);
    }
  else
    {
    vtkWarningMacro("I dont have the properties for this tree .. .");
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SegmentNodeUpdateProps()
{
  double ProxRadius = this->SegmentWidget->GetProximalRadius();
  double DistalRadius = this->SegmentWidget->GetDistalRadius();
  int InterpolationType = this->SegmentWidget->GetInterpolationMode();
  double ProximalWall = this->SegmentWidget->GetProximalWall();
  double DistalWall = this->SegmentWidget->GetDistalWall();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  const char *name = this->SegmentWidget->GetSegmentName();

  char *newName = new char[strlen(name)*2];
  strcpy(newName, (char *)name);

  //substituir espaços por underline
  for ( int i=0; i<strlen(newName); i++ )
    if ( newName[i] == ' ' )
            newName[i] = '_';

  char *n = proxy->GetSegmentProxy()->GetSegmentName();

  //se o nome do segmento for alterado, muda o nome na lista de
  //nome de clone.
  if ( (strcmp(newName, n) != 0) && (strcmp(newName, "") != 0) )
    {
    proxy->GetSegmentProxy()->SetSegmentName(newName);

    //remove todos os nomes da lista
    this->SegmentWidget->RemoveAllSegmentNameInList();
    this->AllSegmentsNameComboBox->DeleteAllValues();

    vtkStringArray *names = proxy->GenerateListOfSegmentName();

    const char *n;
    //adiciona os nomes ordenados na lista
    for ( int i=0; i<names->GetNumberOfValues(); i++ )
      {
      n = names->GetValue(i);
      this->SegmentWidget->AddSegmentNameInList((char*)n);
      this->AllSegmentsNameComboBox->AddValue(n);
      }
    names->Delete();
    this->AllSegmentsNameComboBox->SetValue(newName);
    }

  double length = this->SegmentWidget->GetLength();
  int numberOfElements = this->SegmentWidget->GetNumberOfElements();
  //pega informações básicas do segmento
  vtkDoubleArray *array = proxy->GetSegmentProxy()->GetSegmentBasicInfo();
  //diferença entre o valor da interface e o valor do segmento
  double d = length - array->GetValue(0);

  if ( numberOfElements > 0 )
    {
    if ( (proxy->GetSegmentProxy()->GetNumberOfElements() != numberOfElements) || (fabs(d) >= 0.0001) )
      {
      int res = proxy->SetSegmentData(numberOfElements, length);
      if ( res )
        proxy->GetSegmentProxy()->SetSegmentProps(numberOfElements, length);
      }
    }
  array->Delete();

  proxy->ColorTree();

  //atualiza as propriedades dos novos nós
  proxy->GetSegmentProxy()->SetSegmentNodeProps(ProxRadius, DistalRadius, InterpolationType, ProximalWall, DistalWall);
  //atualiza as propriedades dos novos elementos
  this->SegmentUpdateProps();

  //Pegando color map de todos os arrays de propriedades e atualizando o range
  vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Elastin", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Collagen", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientA", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientB", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Viscoelasticity", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ViscoelasticityExponent", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("InfiltrationPressure", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ReferencePressure", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Permeability", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Radius", 1);
  colorMap->ResetScalarRange();

  vtkPVDisplayGUI *display = this->GetPVSource()->GetPVOutput();
  vtkPVColorSelectionWidget *selectMenu = display->GetColorSelectionMenu();

  //Colorindo a arvore pela propriedade selecionada
  if( strcmp(selectMenu->GetValue(), "Point Radius") == 0 )
    {
    display->ColorByArray("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
    selectMenu->SetValue("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell Scalars") == 0 )
    {
    display->ColorByArray("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if ( strcmp(selectMenu->GetValue(), "Cell Ids") == 0 )
    {
    vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Ids", 1);
    colorMap->ResetScalarRange();
    display->ColorByArray("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell Elastin") == 0 )
    {
    display->ColorByArray("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell Collagen") == 0 )
    {
    display->ColorByArray("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientA") == 0 )
    {
    display->ColorByArray("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientB") == 0 )
    {
    display->ColorByArray("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell Viscoelasticity") == 0 )
    {
    display->ColorByArray("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell ViscoelasticityExponent") == 0 )
    {
    display->ColorByArray("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell InfiltrationPressure") == 0 )
    {
    display->ColorByArray("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell ReferencePressure") == 0 )
    {
    display->ColorByArray("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }
  else if( strcmp(selectMenu->GetValue(), "Cell Permeability") == 0 )
    {
    display->ColorByArray("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    selectMenu->SetValue("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
    }

  this->PVSource->Accept();
  delete newName;
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SingleNodeUpdateProps()
{
  double Radius = this->NodeWidget->GetRadius();
  double Wall = this->NodeWidget->GetWallThickness();
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->GetSegmentProxy()->SetSingleNode(Radius, Wall);

  //Metodo para atualizar a propriedade da arvore modificada
  proxy->ColorTree();

  //Pegando color map de todos os arrays de propriedades e atualizando o range
  vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Radius", 1);
  colorMap->ResetScalarRange();

  vtkPVDisplayGUI *display = this->GetPVSource()->GetPVOutput();
  vtkPVColorSelectionWidget *selectMenu = display->GetColorSelectionMenu();

  //Colorindo a arvore pela propriedade selecionada
  if( strcmp(selectMenu->GetValue(), "Point Radius") == 0 )
    {
    display->ColorByArray("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
    selectMenu->SetValue("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
    }

  this->PVSource->Accept();
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SingleElementUpdateProps()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkDoubleArray *array = vtkDoubleArray::New();
  array->SetNumberOfValues(10);

  array->SetValue(0, this->ElementWidget->GetElastin());
  array->SetValue(1, this->ElementWidget->GetCollagen());
  array->SetValue(2, this->ElementWidget->GetCoefficientA());
  array->SetValue(3, this->ElementWidget->GetCoefficientB());
  array->SetValue(4, this->ElementWidget->GetViscoelasticity());
  array->SetValue(5, this->ElementWidget->GetViscoelasticityExponent());
  array->SetValue(6, this->ElementWidget->GetInfiltrationPressure());
  array->SetValue(7, this->ElementWidget->GetReferencePressure());
  array->SetValue(8, this->ElementWidget->GetPermeability());


  vtkDoubleArray *Geometrical_Element_Info = vtkDoubleArray::New();
  Geometrical_Element_Info->SetNumberOfValues(4);

  Geometrical_Element_Info->SetValue(0, this->ElementWidget->GetProximalWallThickness());
  Geometrical_Element_Info->SetValue(1, this->ElementWidget->GetDistalWallThickness());
  Geometrical_Element_Info->SetValue(2, this->ElementWidget->GetProximalRadius());
  Geometrical_Element_Info->SetValue(3, this->ElementWidget->GetDistalRadius());

  proxy->GetSegmentProxy()->SetSingleElem(array, Geometrical_Element_Info);

  //Metodo para atualizar a propriedade da arvore modificada
  proxy->ColorTree();

  //Pegar color map de todos os arrays de propriedades e atualizar o range
  vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Elastin", 1);
	colorMap->ResetScalarRange();

 	colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Collagen", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientA", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientB", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Viscoelasticity", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ViscoelasticityExponent", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("InfiltrationPressure", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ReferencePressure", 1);
 	colorMap->ResetScalarRange();

 	colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Permeability", 1);
 	colorMap->ResetScalarRange();

 	colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Radius", 1);
	colorMap->ResetScalarRange();

  vtkPVDisplayGUI *display = this->GetPVSource()->GetPVOutput();
  vtkPVColorSelectionWidget *selectMenu = display->GetColorSelectionMenu();

  //Colorindo a arvore pela propriedade selecionada
  if( strcmp(selectMenu->GetValue(), "Point Radius") == 0 )
  	{
  	display->ColorByArray("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
  	selectMenu->SetValue("Radius", vtkSMDataObjectDisplayProxy::POINT_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Scalars") == 0 )
  	{
  	display->ColorByArray("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if ( strcmp(selectMenu->GetValue(), "Cell Ids") == 0 )
  	{
  	vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Ids", 1);
  	colorMap->ResetScalarRange();
  	display->ColorByArray("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Elastin") == 0 )
  	{
  	display->ColorByArray("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Collagen") == 0 )
  	{
  	display->ColorByArray("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientA") == 0 )
  	{
  	display->ColorByArray("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientB") == 0 )
  	{
  	display->ColorByArray("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Viscoelasticity") == 0 )
  	{
  	display->ColorByArray("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell ViscoelasticityExponent") == 0 )
  	{
  	display->ColorByArray("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell InfiltrationPressure") == 0 )
  	{
  	display->ColorByArray("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell ReferencePressure") == 0 )
  	{
  	display->ColorByArray("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Permeability") == 0 )
  	{
  	display->ColorByArray("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}

  this->PVSource->Accept();

  array->Delete();
  Geometrical_Element_Info->Delete();
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SegmentUpdateProps()
{
  vtkDoubleArray *proxElem = vtkDoubleArray::New();
  vtkDoubleArray *distalElem = vtkDoubleArray::New();
  proxElem->SetNumberOfValues(11);
  distalElem->SetNumberOfValues(11);


  proxElem->SetValue(0, this->SegmentWidget->GetProxElast());
  proxElem->SetValue(1, this->SegmentWidget->GetProxPerm());
  proxElem->SetValue(2, this->SegmentWidget->GetProxInf());
  proxElem->SetValue(3, this->SegmentWidget->GetProxRef());
  proxElem->SetValue(4, this->SegmentWidget->GetProxColag());
  proxElem->SetValue(5, this->SegmentWidget->GetProxCoefA());
  proxElem->SetValue(6, this->SegmentWidget->GetProxCoefB());
  proxElem->SetValue(7, this->SegmentWidget->GetProxVisco());
  proxElem->SetValue(8, this->SegmentWidget->GetProxExpVisco());



  distalElem->SetValue(0, this->SegmentWidget->GetDistalElast());
  distalElem->SetValue(1, this->SegmentWidget->GetDistalPerm());
  distalElem->SetValue(2, this->SegmentWidget->GetDistalInf());
  distalElem->SetValue(3, this->SegmentWidget->GetDistalRef());
  distalElem->SetValue(4, this->SegmentWidget->GetDistalColag());
  distalElem->SetValue(5, this->SegmentWidget->GetDistalCoefA());
  distalElem->SetValue(6, this->SegmentWidget->GetDistalCoefB());
  distalElem->SetValue(7, this->SegmentWidget->GetDistalVisco());
  distalElem->SetValue(8, this->SegmentWidget->GetDistalExpVisco());

  int InterpolationType = this->SegmentWidget->GetInterpolationMode();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->GetSegmentProxy()->SetSegment(proxElem, distalElem, InterpolationType);

  proxy->ColorTree();

  //Pegando color map de todos os arrays de propriedades e atualizando o range
  vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Elastin", 1);
	colorMap->ResetScalarRange();

 	colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Collagen", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientA", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("CoefficientB", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Viscoelasticity", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ViscoelasticityExponent", 1);
  colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("InfiltrationPressure", 1);
 	colorMap->ResetScalarRange();

  colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("ReferencePressure", 1);
 	colorMap->ResetScalarRange();

 	colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Permeability", 1);
 	colorMap->ResetScalarRange();

  vtkPVDisplayGUI *display = this->GetPVSource()->GetPVOutput();
  vtkPVColorSelectionWidget *selectMenu = display->GetColorSelectionMenu();

  //Colorindo a arvore pela propriedade selecionada
  if( strcmp(selectMenu->GetValue(), "Cell Scalars") == 0 )
  	{
  	display->ColorByArray("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Scalars", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if ( strcmp(selectMenu->GetValue(), "Cell Ids") == 0 )
  	{
  	vtkPVColorMap *colorMap = this->GetPVApplication()->GetMainWindow()->GetPVColorMap("Ids", 1);
  	colorMap->ResetScalarRange();
  	display->ColorByArray("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Ids", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Elastin") == 0 )
  	{
  	display->ColorByArray("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Elastin", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Collagen") == 0 )
  	{
  	display->ColorByArray("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Collagen", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientA") == 0 )
  	{
  	display->ColorByArray("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("CoefficientA", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell CoefficientB") == 0 )
  	{
  	display->ColorByArray("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("CoefficientB", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Viscoelasticity") == 0 )
  	{
  	display->ColorByArray("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Viscoelasticity", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell ViscoelasticityExponent") == 0 )
  	{
  	display->ColorByArray("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("ViscoelasticityExponent", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell InfiltrationPressure") == 0 )
  	{
  	display->ColorByArray("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("InfiltrationPressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell ReferencePressure") == 0 )
  	{
  	display->ColorByArray("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("ReferencePressure", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}
  else if( strcmp(selectMenu->GetValue(), "Cell Permeability") == 0 )
  	{
  	display->ColorByArray("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	selectMenu->SetValue("Permeability", vtkSMDataObjectDisplayProxy::CELL_FIELD_DATA);
  	}

  this->PVSource->Accept();


  proxElem->Delete();
  distalElem->Delete();

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::InterventionsUpdateProperties()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkDoubleArray *array = vtkDoubleArray::New();

  if ( this->SurgicalInterventionsMode == vtkPVHMStraightModelWidget::Stent )
    {
    array->SetNumberOfValues(3);
    array->SetValue(0, this->InterventionsWidget->GetSize());
    array->SetValue(1, this->InterventionsWidget->GetElastin());
    array->SetValue(2, this->InterventionsWidget->GetViscoElasticity());

    proxy->AddStent(array);
    }
  else if ( this->SurgicalInterventionsMode == vtkPVHMStraightModelWidget::Stenosis )
    {
    array->SetNumberOfValues(2);
    array->SetValue(0, this->InterventionsWidget->GetSize());
    array->SetValue(1, this->InterventionsWidget->GetPercentage());
    proxy->AddStenosis(array);
    }
  else if ( this->SurgicalInterventionsMode == vtkPVHMStraightModelWidget::Aneurysm )
    {
    array->SetNumberOfValues(2);
    array->SetValue(0, this->InterventionsWidget->GetSize());
    array->SetValue(1, this->InterventionsWidget->GetPercentage());
    proxy->AddAneurysm(array);
    }
  else if ( this->SurgicalInterventionsMode == vtkPVHMStraightModelWidget::Aging )
    {
    array->SetNumberOfValues(2);
    array->SetValue(0, this->InterventionsWidget->GetElastin());
    array->SetValue(1, this->InterventionsWidget->GetViscoElasticity());
    proxy->AddAging(array);
    this->UpdateSurgicalInterventionsButton(vtkPVHMStraightModelWidget::Aging);
    }

  array->Delete();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::UpdateToolbar(int option)
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  //  proxy->ClearElementSelectedProxy();
  this->Script("grid forget %s", this->SubTreePropFrame->GetWidgetName());
  this->InputVisualizationModeButton->SelectedStateOff();
  this->EditionModeButton->SelectedStateOff();
  this->PlotModeButton->SelectedStateOff();
  this->ToDetachSubTreeButton->SelectedStateOff();
  this->SpecialToolsButton->SelectedStateOff();
  proxy->RenderWidgetsWithDefaultColors(); //apaga a subtreelist e coloca a render com as cores default
  this->TerminalWidget->UnselectButtons();

  if ( option == MODE_EDITION_VAL ) // edition mode
    {
    this->SetWidgetMode(MODE_EDITION_VAL);
    this->EditionModeButton->SelectedStateOn();
    if (!this->ToolBar->HasWidget(this->EditElemButton))
      {
      this->ToolBar->RemoveAllWidgets();
      this->ToolBar->AddWidget(this->InputVisualizationModeButton);
      this->ToolBar->AddWidget(this->EditionModeButton);
      this->ToolBar->AddWidget(this->EditElemButton);
      this->ToolBar->AddWidget(this->EditSegButton);
      this->ToolBar->AddWidget(this->PlotModeButton);
      this->ToolBar->AddWidget(this->ToDetachSubTreeButton);
      this->ToolBar->AddWidget(this->SpecialToolsButton);
      }
    this->EditElemButton->SetBalloonHelpString("Edit Element mode");
    this->EditSegButton->SetBalloonHelpString("Edit Segment mode. To select multiples segments at once, keep the Crtl key pressed while you select the segments.");
    this->SetFrameLabel("Mode: Tree Element Properties Edition");

    this->EditRadioButtonSet->EnabledOn();
    this->SurgicalInterventionsRadioButtonSet->EnabledOff();

    this->AddTerminalButton->EnabledOn();
    this->MoveButton->EnabledOn();
    this->CheckConsistenceButton->EnabledOn();

    this->CopySegmentButton->EnabledOn();
    this->FlipButton->EnabledOn();
    this->RestartButton->EnabledOff();
    this->KeepSubTreeButton->EnabledOff();
    }
  else if ( option == MODE_VISUALIZATION_VAL ) // input v. mode
    {
    this->SetWidgetMode(MODE_VISUALIZATION_VAL);
    this->InputVisualizationModeButton->SelectedStateOn();
    if (!this->ToolBar->HasWidget(this->EditElemButton))
      {
      this->ToolBar->RemoveAllWidgets();
      this->ToolBar->AddWidget(this->InputVisualizationModeButton);
      this->ToolBar->AddWidget(this->EditionModeButton);
      this->ToolBar->AddWidget(this->EditElemButton);
      this->ToolBar->AddWidget(this->EditSegButton);
      this->ToolBar->AddWidget(this->PlotModeButton);
      this->ToolBar->AddWidget(this->ToDetachSubTreeButton);
      this->ToolBar->AddWidget(this->SpecialToolsButton);
      }
    this->EditElemButton->SetBalloonHelpString("Visualize Element mode");
    this->EditSegButton->SetBalloonHelpString("Visualize Segment mode");
            this->SetFrameLabel("Mode: Tree Element Properties Visualization");

    this->EditRadioButtonSet->EnabledOff();
    this->EditRadioButtonSet->GetWidget(vtkPVHMStraightModelWidget::EditGeometry)->SetSelectedState(1);
    this->EditionMode = vtkPVHMStraightModelWidget::EditGeometry;

    this->CheckConsistenceButton->EnabledOn();
    this->CopySegmentButton->EnabledOff();
    this->FlipButton->EnabledOff();
    this->RestartButton->EnabledOff();
    this->KeepSubTreeButton->EnabledOff();
    this->SurgicalInterventionsRadioButtonSet->EnabledOff();
    }
  else if ( option == MODE_PLOT_VAL ) // plot mode
    {
    proxy->ClearElementSelectedProxy();
    this->SetWidgetMode(MODE_PLOT_VAL);
    this->PlotModeButton->SelectedStateOn();
    this->SelectTreeElement(1, 0);
    if (this->ToolBar->HasWidget(this->EditElemButton))
      {
      this->ToolBar->RemoveWidget(this->EditElemButton);
      this->ToolBar->RemoveWidget(this->EditSegButton);
      }

    this->SetFrameLabel("Mode: Output Results Plot");

    this->EditRadioButtonSet->EnabledOff();
    this->CheckConsistenceButton->EnabledOff();
    this->CopySegmentButton->EnabledOff();
    this->FlipButton->EnabledOff();
    this->RestartButton->EnabledOff();
    this->KeepSubTreeButton->EnabledOff();
    this->EditRadioButtonSet->GetWidget(vtkPVHMStraightModelWidget::EditGeometry)->SetSelectedState(1);
    this->EditionMode = vtkPVHMStraightModelWidget::EditGeometry;
    this->SurgicalInterventionsRadioButtonSet->EnabledOff();

    proxy->ReloadPlotActors();
    this->XYPlotWidget->SetPlotOptions(this->PlotOptions);
    }
  else if ( option == MODE_TODETACHSUBTREE_VAL ) // detach sub-tree mode
    {
    proxy->ClearElementSelectedProxy();
    this->SetWidgetMode(MODE_TODETACHSUBTREE_VAL);
    this->ToDetachSubTreeButton->SelectedStateOn();
    this->EditionModeButton->SelectedStateOn();
    this->SelectTreeElement(0, 1);
    if (this->ToolBar->HasWidget(this->EditSegButton))
      {
      this->ToolBar->RemoveWidget(this->EditElemButton);
      this->ToolBar->RemoveWidget(this->EditSegButton);
      }
    this->SetFrameLabel("Mode: Edition Sub-Tree");

    this->EditRadioButtonSet->EnabledOff();//barra de edição da arvore 1D
    this->EditGeometryButton->EnabledOn();
    this->CheckConsistenceButton->EnabledOff();
    this->CopySegmentButton->EnabledOn();
    this->DeleteButton->EnabledOn();
    this->MoveButton->EnabledOn();
    this->FlipButton->EnabledOn();
    this->RestartButton->EnabledOn();
    this->KeepSubTreeButton->EnabledOn();
    //Pega a seta da barra de edição "GetWidget(0)" e coloca como pressionada "SetSelectedState(1)"
    this->EditRadioButtonSet->GetWidget(vtkPVHMStraightModelWidget::EditGeometry)->SetSelectedState(1);
//		this->EditionMode = vtkPVHMStraightModelWidget::EditGeometry;
    this->SurgicalInterventionsRadioButtonSet->EnabledOff();
    }

  else if ( option == MODE_ESPECIAL_TOOLS_VAL ) // especial tools mode
    {
    proxy->ClearElementSelectedProxy();
    this->SetWidgetMode(MODE_ESPECIAL_TOOLS_VAL);
    this->SpecialToolsButton->SelectedStateOn();
    this->SelectTreeElement(1, 0);
    if (this->ToolBar->HasWidget(this->EditElemButton))
      {
      this->ToolBar->RemoveWidget(this->EditElemButton);
      this->ToolBar->RemoveWidget(this->EditSegButton);
      }

    this->EditElemButton->SetBalloonHelpString("Visualize Element mode");
    this->EditSegButton->SetBalloonHelpString("Visualize Segment mode");
    this->SetFrameLabel("Mode: Pathology and surgical interventions");

    this->EditRadioButtonSet->EnabledOn();
    this->EditRadioButtonSet->GetWidget(vtkPVHMStraightModelWidget::EditGeometry)->SetSelectedState(1);
    this->EditionMode = vtkPVHMStraightModelWidget::EditGeometry;

    this->SurgicalInterventionsRadioButtonSet->EnabledOn();
    this->RestartButton->EnabledOff();
    this->KeepSubTreeButton->EnabledOff();
    }
   this->UpdateWidgetInfo();
   proxy->SetStraightModelMode((int)this->GetWidgetMode());
   proxy->SetEditionMode(this->EditionMode);
   //proxy->RenderWidgetsWithDefaultColors();
}


// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SelectTreeElement(int element, int segment)
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->SelectTreeElement(element, segment);
  this->SegmentWidget->SetSegmentSelected(0);
  this->SegmentWidget->SetElementSelected(0);
  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
  this->WidgetProxy->GetProperty("StraightModelModeSelected"));
  smSelect->SetElement(1, element);
  smSelect->SetElement(2, segment);
  this->EditSegButton->SelectedStateOff();
  this->EditElemButton->SelectedStateOff();

  proxy->ClearElementSelectedProxy();
  this->UpdateWidgetInfo();

  if (element)
    {
    this->EditElemButton->SelectedStateOn();
    this->SegmentWidget->SetSegmentSelected(0);
    this->SegmentWidget->SetElementSelected(1);
    }
  if (segment)
    {
    this->EditSegButton->SelectedStateOn();
    this->SegmentWidget->SetElementSelected(0);
    this->SegmentWidget->SetSegmentSelected(1);
    }
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SetElementGeometricalInfo(void)
{
  /*
  0, Proximal_Radius
  1, Proximal_Wallthickness
  2, Distal_Radius
  3, Distal_Wallthickness
  */

  // first I have to acess the property for the segment -- I supose it has been already updated
  vtkSMDoubleVectorProperty* seg = vtkSMDoubleVectorProperty::SafeDownCast(
                  this->WidgetProxy->GetProperty("GeometricalElementProperty"));

  // if I found the prop
  if (seg)
  {
  this->ElementWidget->SetProximalRadius(seg->GetElement(0));
  this->ElementWidget->SetProximalWallThickness(seg->GetElement(1));
  this->ElementWidget->SetDistalRadius(seg->GetElement(2));
  this->ElementWidget->SetDistalWallThickness(seg->GetElement(3));
  }
  else
  {
  vtkWarningMacro("I dont have the properties GeometricalElementProperty  for this element .. .");
  }

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::GenerateSolverFiles()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  double result = proxy->GetHeartFinalTime();
  if (result==-1)
    {
    vtkKWMessageDialog::PopupMessage(
    this->GetApplication(), this->GetParentWindow(), "Heart Curve Missing",
    "Define Heart Curve",
     vtkKWMessageDialog::ErrorIcon);
    return;
    }

  if (!this->GeneralConfiguration)
    {
    this->GeneralConfiguration = vtkPVHM1DSolverFilesConfigurationWidget::New();
    this->GeneralConfiguration->SetModelType("1D Model");

    if (result == -1)
      this->GeneralConfiguration->SetOriginalFinalTime(0);
    else
      this->GeneralConfiguration->SetOriginalFinalTime(result);

    vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
    this->GeneralConfiguration->SetWidgetProxy(proxy);
    this->GeneralConfiguration->SetModal(1);
    this->GeneralConfiguration->SetMasterWindow(this->Window);
    this->GeneralConfiguration->Create(this->pvApp);
    this->GeneralConfiguration->SetSize(550, 510);
    this->GeneralConfiguration->GetCommitChangesButtonPage1()->SetCommand(this, "ProcessValuesFromBasParamWindow");
    //this->GeneralConfiguration->SetMinimumSize(550, 510);
    this->GeneralConfiguration->Display();
    }
  else
    {
    this->GeneralConfiguration->GetentryCardiacCycleTime()->SetValueAsDouble(result);
    this->GeneralConfiguration->Display();
    }


}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ProcessValuesFromBasParamWindow()
{
  if (this->GeneralConfiguration->GetSolverFilesPath())
    {
    this->GeneralConfiguration->ConfigureValuesFromBasParamWindow();

    vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
    proxy->GenerateSolverFiles(this->GeneralConfiguration->GetDoubleParametersArray(),
     this->GeneralConfiguration->GetIntParametersArray(),
     this->GeneralConfiguration->GetSolverFilesPath(),
     this->GeneralConfiguration->GetLogFile(),
     this->GeneralConfiguration->GetWhichFile()
     );

    if ( this->GeneralConfiguration->CopyFiles() )
      this->ReadSimulationButton->EnabledOn();
    }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetMultipleSegments(int WholeTree)
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  bool UpdateMethod = this->MultipleSegmentWidget->GetPropUpdateMethod();

  int SubTree = this->GetWidgetMode();

  proxy->SetSegmentProxyFromList(this->MultipleSegmentWidget->GetPropFactorDoubleArray(UpdateMethod), UpdateMethod, WholeTree, SubTree);

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetMultipleTerminals()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  bool UpdateMethod = this->MultipleTerminalWidget->GetPropUpdateMethod();

  int SubTree = this->GetWidgetMode();

  proxy->SetTerminalProxyFromList(this->MultipleTerminalWidget->GetPropFactorDoubleArray(UpdateMethod), UpdateMethod, SubTree);

}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::RefineSegments()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  int mode = this->GetWidgetMode();

  proxy->RefineSubTreeMesh(this->SubTreeMeshWidget->GetRefineMethod(), this->SubTreeMeshWidget->GetRefineValue(), mode);
}

// ----------------------------------------------------------------------------
int vtkPVHMStraightModelWidget::PlotGraphic(vtkDoubleArray *array, vtkDoubleArray *time, char type, int meshID)
{
  if (this->PlotWidget->AnimatedPlotTypeRadio->GetSelectedState())
    {
    //	this->XYAnimatedPlotWidget->SetTypePlot(this->AnimatedPlotOptions);

    //Setando tempo minimo e maximo
    this->XYAnimatedPlotWidget->SetTMin( time->GetValue(0) );
    this->XYAnimatedPlotWidget->SetTMax( time->GetValue(time->GetNumberOfTuples()-1) );

    //configura o plot
    this->XYAnimatedPlotWidget->MakeHMXYPlotWidget(array, time, type, meshID);

    //this->XYAnimatedPlotWidget->ShowHMXYPlotWidget();
    }
  else if (this->PlotWidget->NormalPlotTypeRadio->GetSelectedState())
    {
    this->XYPlotWidget->SetTypePlot(this->PlotOptions);

    //Setando tempo minimo e maximo
    this->XYPlotWidget->SetTMin( time->GetValue(0) );
    this->XYPlotWidget->SetTMax( time->GetValue(time->GetNumberOfTuples()-1) );

    //configura o plot
    this->XYPlotWidget->MakeHMXYPlotWidget(array,time, type, meshID);
    if ( this->XYPlotWidget->GetNumberOfPlots() == 0 )
      return 0;

    vtkSMHMStraightModelWidgetProxy *wProxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);
    wProxy->SetNumberOfGraphicsSum(this->XYPlotWidget->GetNumberOfGraphicsSum());
    wProxy->SetNumberOfGraphics(this->XYPlotWidget->GetNumberOfGraphics());

    //Exibe as curvas do plot
    this->XYPlotWidget->ShowHMXYPlotWidget();
    //habilita o botao na inteface para exportar as curvas para arquivo txt;
    this->PlotWidget->ExportResultButton->SetEnabled(1);
    //habilita o botão para fazer update das curvas
    this->PlotWidget->GraphsUpdateButton->SetEnabled(1);

    }
  return 1;
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ConfigureHeartCurve(char v)
{
  vtkDoubleArray *prop;

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  double R1, R2, maxFlow, minFlow;
  double TotalResistence;
  double r[2];

  //Curva do coração
  vtkDoubleArray *heartFlow;

  switch(v)
    {
    case 'F':
      if ( this->HeartWidget->GetImposes() == 'F' )
        return;

      this->HeartWidget->SetImposes('F');

      //Limpa a janela do plot caso não esteja em modo de plotagem dos gráficos
      this->XYPlotWidget->Clear();


      //Pega os valores setados na interface do coração
      R1 				= this->HeartWidget->GetR1();
      R2 				= this->HeartWidget->GetR2();
      maxFlow 	= this->HeartWidget->GetMaxFlow();
      minFlow 	= this->HeartWidget->GetMinFlow();

      //Atualiza a curva editada, para pegar os valores aqui atualizados
      this->XYPlotWidget->UpdateHeartCurve();

      //Pega os valores da curva
      heartFlow = this->XYPlotWidget->GetHeartCurve();

      //Verifica se a soma das resistencias é maior que zero para calcular
      //o valor correto da curva e setar os valores das resistencias e capacitancia
      TotalResistence = R1 + R2;
      if ( TotalResistence <= 0 )
        {
        int res = vtkKWMessageDialog::PopupYesNo(
        this->GetApplication(), vtkKWWindow::SafeDownCast(this->GetParentWindow()), "Warning",
        "The resistences are with equal values the zero.\nConfigure default value '50000'?",
        vtkKWMessageDialog::WarningIcon);

        if ( res )
          {
          R1 = 50000;
          R2 = 50000;
          }
        else
          {
          this->HeartWidget->SetImposes('P');
          return;
          }
        TotalResistence = R1 + R2;
        }

      //Seta o tempo e os valores de pressão
      proxy->GetTerminalProxy()->SetTerminalProp(heartFlow, 'P', 1);

      //Calcula o range da curva para setar os valores das resistencias
      //e capacitancia
      prop = vtkDoubleArray::New();
      prop->SetNumberOfValues(2);
      heartFlow->ComputeRange(0);

      heartFlow->GetRange(r);

      this->XYPlotWidget->SetMaxFlow(maxFlow/TotalResistence);
      this->XYPlotWidget->SetMinFlow(minFlow/TotalResistence);

      //Seta resistencia 1
      prop->SetValue(0,R1);
      prop->SetValue(1,R1);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

      //Seta resistencia 2
      prop->SetValue(0,R2);
      prop->SetValue(1,R2);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

      this->HeartWidget->SetR1(R1);
      this->HeartWidget->SetR2(R2);
      this->HeartWidget->SetP(r[1]/TotalResistence);
      this->HeartWidget->SetMaxFlow(maxFlow/TotalResistence);
      this->HeartWidget->SetMinFlow(minFlow/TotalResistence);

      break;

    case 'P':
      if ( this->HeartWidget->GetImposes() == 'P' )
        return;
      this->HeartWidget->SetImposes('P');
      //Limpa a janela do plot caso não esteja em modo de plotagem dos gráficos
      this->XYPlotWidget->Clear();

      //Pega os valores setados na interface do coração
      R1 				= this->HeartWidget->GetR1();
      R2 				= this->HeartWidget->GetR2();
      maxFlow 	= this->HeartWidget->GetMaxFlow();
      minFlow 	= this->HeartWidget->GetMinFlow();

      //Atualiza a curva editada, para pegar os valores aqui atualizados
      this->XYPlotWidget->UpdateHeartCurve();

      //Pega os valores da curva
      heartFlow = this->XYPlotWidget->GetHeartCurve();

      //Verifica se a soma das resistencias é maior que zero para calcular
      //o valor correto da curva e setar os valores das resistencias e capacitancia
      TotalResistence = R1 + R2;
      if ( TotalResistence <= 0 )
        {
        R1 = 50000;
        R2 = 50000;
        TotalResistence = R1 + R2;
        }

      //Calcula o novo valor de pressão
      for ( int i=0; i<heartFlow->GetNumberOfTuples(); i++ )
        heartFlow->SetValue(i, heartFlow->GetValue(i)*TotalResistence);

      //Seta o tempo e os valores de pressão
      proxy->GetTerminalProxy()->SetTerminalProp(heartFlow, 'P', 1);

      //Calcula o range da curva para setar os valores das resistencias
      //e capacitancia
      prop = vtkDoubleArray::New();
      prop->SetNumberOfValues(2);
      heartFlow->ComputeRange(0);
      heartFlow->GetRange(r);

      this->XYPlotWidget->SetMaxFlow(maxFlow*TotalResistence);
      this->XYPlotWidget->SetMinFlow(minFlow*TotalResistence);

      this->XYAnimatedPlotWidget->SetMaxFlow(maxFlow*TotalResistence);
      this->XYAnimatedPlotWidget->SetMinFlow(minFlow*TotalResistence);

      R1 = R2 = 0;
      //Seta resistencia 1
      prop->SetValue(0,R1);
      prop->SetValue(1,R1);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

      //Seta resistencia 2
      prop->SetValue(0,R2);
      prop->SetValue(1,R2);
      proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

      this->HeartWidget->SetR1(R1);
      this->HeartWidget->SetR2(R2);
      this->HeartWidget->SetP(r[1]);
      this->HeartWidget->SetMaxFlow(maxFlow*TotalResistence);
      this->HeartWidget->SetMinFlow(minFlow*TotalResistence);

      break;
    }
  this->BuildHeartCurve();
  prop->Delete();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::CreateHeartCurve()
{
  vtkDoubleArray *prop = vtkDoubleArray::New();
  vtkDoubleArray *time = vtkDoubleArray::New();
  vtkDoubleArray *array;

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  double R1, R2, maxFlow, minFlow, finalTime;

  int functionSelected = this->HeartWidget->GetFunctionSelected();

  //Pega os valores setados na interface do coração

  maxFlow 	= this->HeartWidget->GetMaxFlow();
  minFlow 	= this->HeartWidget->GetMinFlow();
  finalTime = this->HeartWidget->GetFinalTime();

  array = this->HeartWidget->CalculateCurve(maxFlow, finalTime);

  R1 		= this->HeartWidget->GetR1();
  R2 		= this->HeartWidget->GetR2();
  maxFlow 	= this->HeartWidget->GetMaxFlow();
  minFlow 	= this->HeartWidget->GetMinFlow();
  finalTime = this->HeartWidget->GetFinalTime();

  double TotalResistence = R1 + R2;

  if ( array )
    {
    if ( TotalResistence > 0 )
      {
      double value;
      for ( int i=0; i<array->GetNumberOfTuples(); i++ )
        {
        value = array->GetComponent(i, 1);
        array->SetComponent(i, 1, value*TotalResistence);
        }
      }
    //Seta resistencia 1
    prop->SetNumberOfTuples(2);

    prop->SetValue(0,R1);
    prop->SetValue(1,R1);
    proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 1);

    //Seta resistencia 2
    prop->SetValue(0,R2);
    prop->SetValue(1,R2);
    proxy->GetTerminalProxy()->SetTerminalProp(prop, 'R', 2);

    prop->SetNumberOfTuples(array->GetNumberOfTuples());
    time->SetNumberOfTuples(array->GetNumberOfTuples());

    for ( int i=0; i<array->GetNumberOfTuples(); i++ )
      {
      time->SetValue(i, array->GetComponent(i,0));
      prop->SetValue(i, array->GetComponent(i,1));
      }

    proxy->GetTerminalProxy()->SetTerminalProp(prop, 'P', 1);
    proxy->GetTerminalProxy()->SetTerminalTimeProp(time, 'P', 1);
    array->Delete();
    }

  prop->Delete();
  time->Delete();
  this->BuildHeartCurve();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::AddHeartCurve()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkDoubleArray *array = this->HeartWidget->ReadCurve();

  vtkDoubleArray *r1Time = vtkDoubleArray::New();
  vtkDoubleArray *r2Time = vtkDoubleArray::New();
  vtkDoubleArray *cTime  = vtkDoubleArray::New();
  vtkDoubleArray *pTime  = vtkDoubleArray::New();

  vtkDoubleArray *r1 = vtkDoubleArray::New();
  vtkDoubleArray *r2 = vtkDoubleArray::New();
  vtkDoubleArray *c  = vtkDoubleArray::New();
  vtkDoubleArray *p  = vtkDoubleArray::New();

  double maxFlow, minFlow;

  if ( array )
    {
      int j, i=0;
      //r1Time
      int n = (int)array->GetValue(i);
      r1Time->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        r1Time->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //r1
      n = (int)array->GetValue(i);

      r1->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        r1->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //r2Time
      n = (int)array->GetValue(i);

      r2Time->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        r2Time->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //r2
      n = (int)array->GetValue(i);

      r2->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        r2->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //cTime
      n = (int)array->GetValue(i);

      cTime->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        cTime->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //c
      n = (int)array->GetValue(i);

      c->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        c->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //pTime
      n = (int)array->GetValue(i);

      pTime->SetNumberOfTuples(n);
      i++;
      j=0;
      while ( j<n )
        {
        pTime->SetValue(j, array->GetValue(i));
        i++;
        j++;
        }

      //p
      n = (int)array->GetValue(i);

      p->SetNumberOfTuples(n);
      i++;
      j=0;
      maxFlow = minFlow = array->GetValue(i);
      while ( j<n )
        {
        p->SetValue(j, array->GetValue(i));

        if ( maxFlow < array->GetValue(i) )
                maxFlow = array->GetValue(i);

        if ( minFlow > array->GetValue(i) )
                minFlow = array->GetValue(i);

        i++;
        j++;
        }

      //Envia os dados
      proxy->GetTerminalProxy()->SetTerminalProp(r1, 'R', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(r1Time, 'R', 1);

      proxy->GetTerminalProxy()->SetTerminalProp(r2, 'R', 2);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(r2Time, 'R', 2);

      proxy->GetTerminalProxy()->SetTerminalProp(c, 'C', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(cTime, 'C', 1);

      proxy->GetTerminalProxy()->SetTerminalProp(p, 'P', 1);
      proxy->GetTerminalProxy()->SetTerminalTimeProp(pTime, 'P', 1);


      this->HeartWidget->SetNumberOfPoints(p->GetNumberOfTuples());

      double TotalResistence = r1->GetValue(r1->GetNumberOfTuples()-1)+r2->GetValue(r2->GetNumberOfTuples()-1);

      if ( TotalResistence > 0 )
        {
        this->HeartWidget->SetFinalTime(pTime->GetValue(pTime->GetNumberOfTuples()-1));
        this->HeartWidget->SetR1(r1->GetValue(r1->GetNumberOfTuples()-1));
        this->HeartWidget->SetR2(r2->GetValue(r2->GetNumberOfTuples()-1));
        this->HeartWidget->SetP(maxFlow/TotalResistence);
        this->HeartWidget->SetMaxFlow(maxFlow/TotalResistence);
        this->HeartWidget->SetMinFlow(minFlow/TotalResistence);
        this->HeartWidget->SetImposes('F');
        }
      else
        {
        this->HeartWidget->SetR1(r1->GetValue(r1->GetNumberOfTuples()-1));
        this->HeartWidget->SetR2(r2->GetValue(r2->GetNumberOfTuples()-1));
        this->HeartWidget->SetP(maxFlow);
        this->HeartWidget->SetMaxFlow(maxFlow);
        this->HeartWidget->SetMinFlow(minFlow);
        this->HeartWidget->SetImposes('P');
        }

      this->BuildHeartCurve();

      r1Time->Delete();
      r2Time->Delete();
      cTime->Delete();
      pTime->Delete();

      r1->Delete();
      r2->Delete();
      c->Delete();
      p->Delete();
      array->Delete();
    }
}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::SaveHeartCurve()
{
  vtkDoubleArray *prop;
  vtkDoubleArray *time;
  vtkDoubleArray *array = vtkDoubleArray::New();

  int quantityOfParameters;

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());


  //Pega resistencia 1
  prop = proxy->GetTerminalProxy()->GetTerminalProp('R', 1);
  time = proxy->GetTerminalProxy()->GetTerminalTimeProp('R', 1);

  quantityOfParameters = prop->GetNumberOfTuples();
  if ( quantityOfParameters > 1 )
    {
    double R1 = prop->GetValue(0)+prop->GetValue(1);
    if ( R1 > 0 )
      {
      //Inserir quantidade de parametros e em seguida,
      //tempo e valor de resistencia.
      array->InsertNextValue(quantityOfParameters);
      for ( int i=0; i<quantityOfParameters; i++ )
        {
        array->InsertNextValue(time->GetValue(i));
        array->InsertNextValue(prop->GetValue(i));
        }

      }
    else
      {
      quantityOfParameters = 0;
      array->InsertNextValue(quantityOfParameters);
      }
    }
  else
    {
    quantityOfParameters = 0;
    array->InsertNextValue(quantityOfParameters);
    }

  prop->Delete();
  time->Delete();

  /////////////////////////////////////////////////////////////////////////////
  //Pega resistencia 2
  prop = proxy->GetTerminalProxy()->GetTerminalProp('R', 2);
  time = proxy->GetTerminalProxy()->GetTerminalTimeProp('R', 2);

  quantityOfParameters = prop->GetNumberOfTuples();
  if ( quantityOfParameters > 1 )
    {
    double R2 = prop->GetValue(0)+prop->GetValue(1);
    if ( R2 > 0 )
      {
      //Inserir quantidade de parametros e em seguida,
      //tempo e valor de resistencia.
      array->InsertNextValue(quantityOfParameters);
      for ( int i=0; i<quantityOfParameters; i++ )
        {
        array->InsertNextValue(time->GetValue(i));
        array->InsertNextValue(prop->GetValue(i));
        }
      }
    else
      {
      quantityOfParameters = 0;
      array->InsertNextValue(quantityOfParameters);
      }
    }
  else
    {
    quantityOfParameters = 0;
    array->InsertNextValue(quantityOfParameters);
    }

  prop->Delete();
  time->Delete();

  /////////////////////////////////////////////////////////////////////////////
  //Pega capacitancia
  prop = proxy->GetTerminalProxy()->GetTerminalProp('C', 1);
  time = proxy->GetTerminalProxy()->GetTerminalTimeProp('C', 1);

  quantityOfParameters = prop->GetNumberOfTuples();
  if ( quantityOfParameters > 1 )
    {
    //Inserir quantidade de parametros e em seguida,
    //tempo e valor de capacitancia.
    array->InsertNextValue(quantityOfParameters);
    for ( int i=0; i<quantityOfParameters; i++ )
      {
      array->InsertNextValue(time->GetValue(i));
      array->InsertNextValue(prop->GetValue(i));
      }
    }
  else
    {
    quantityOfParameters = 0;
    array->InsertNextValue(quantityOfParameters);
    }

  prop->Delete();
  time->Delete();

  /////////////////////////////////////////////////////////////////////////////
  //Pega pressao
  prop = proxy->GetTerminalProxy()->GetTerminalProp('P', 1);
  time = proxy->GetTerminalProxy()->GetTerminalTimeProp('P', 1);

  quantityOfParameters = prop->GetNumberOfTuples();
  if ( quantityOfParameters > 1 )
    {
    //Inserir quantidade de parametros e em seguida,
    //tempo e valor de pressao.
    array->InsertNextValue(quantityOfParameters);
    for ( int i=0; i<quantityOfParameters; i++ )
      {
      array->InsertNextValue(time->GetValue(i));
      array->InsertNextValue(prop->GetValue(i));
      }
    }
  else
    {
    quantityOfParameters = 0;
    array->InsertNextValue(quantityOfParameters);
    }

  prop->Delete();
  time->Delete();

  this->HeartWidget->WriteCurve(array);

  array->Delete();
}

//----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::UpdateSegmentProxViscoWidgets()
{

  if (!strcmp(this->SegmentWidget->GetProxElastinCombo()->GetValue(), "4e+6"))
    {
    this->SegmentWidget->GetProxElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetProxElastEntry()->SetValueAsDouble(4e+6);
    this->SegmentWidget->GetProxViscoEntry()->SetValueAsDouble(44400);
    }
  if (!strcmp(this->SegmentWidget->GetProxElastinCombo()->GetValue(), "8e+6"))
    {
    this->SegmentWidget->GetProxElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetProxElastEntry()->SetValueAsDouble(8e+6);
    this->SegmentWidget->GetProxViscoEntry()->SetValueAsDouble(89000);
    }

  if (!strcmp(this->SegmentWidget->GetProxElastinCombo()->GetValue(), "1.6e+7"))
    {
    this->SegmentWidget->GetProxElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetProxElastEntry()->SetValueAsDouble(1.6e+7);
    this->SegmentWidget->GetProxViscoEntry()->SetValueAsDouble(1.78e+5);
    }

  if (!strcmp(this->SegmentWidget->GetProxElastinCombo()->GetValue(), "Other"))
    {
    this->SegmentWidget->GetProxElastEntry()->SetValueAsDouble(0);
    this->SegmentWidget->GetProxViscoEntry()->SetValueAsDouble(0);
    this->SegmentWidget->GetProxElastEntry()->ReadOnlyOff();
    }


  double visco = 	this->SegmentWidget->GetProxViscoEntry()->GetValueAsDouble();
  double time = this->SegmentWidget->GetProxcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetProxElastEntry()->GetValueAsDouble();
  double angle;
  angle =  atan(  ((2 * vtkMath::Pi()) * visco)  / (time * elastin)       );

  // angle in radians -- must be converted to degree
  angle = (angle *180)/ vtkMath::Pi();
  this->SegmentWidget->GetProxViscoElasticAngleEntry()->SetValueAsDouble(angle);
}

//----------------------------------------------------------------------------



void vtkPVHMStraightModelWidget::UpdateSegmentDistalViscoWidgets()
{
  if (!strcmp(this->SegmentWidget->GetDistalElastinCombo()->GetValue(), "4e+6"))
    {
    this->SegmentWidget->GetDistalElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetDistalElastEntry()->SetValueAsDouble(4e+6);
    this->SegmentWidget->GetDistalViscoEntry()->SetValueAsDouble(44400);
    }
  if (!strcmp(this->SegmentWidget->GetDistalElastinCombo()->GetValue(), "8e+6"))
    {
    this->SegmentWidget->GetDistalElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetDistalElastEntry()->SetValueAsDouble(8e+6);
    this->SegmentWidget->GetDistalViscoEntry()->SetValueAsDouble(89000);
    }

  if (!strcmp(this->SegmentWidget->GetDistalElastinCombo()->GetValue(), "1.6e+7"))
    {
    this->SegmentWidget->GetDistalElastEntry()->ReadOnlyOn();
    this->SegmentWidget->GetDistalElastEntry()->SetValueAsDouble(1.6e+7);
    this->SegmentWidget->GetDistalViscoEntry()->SetValueAsDouble(1.78e+5);
    }

  if (!strcmp(this->SegmentWidget->GetDistalElastinCombo()->GetValue(), "Other"))
    {
    this->SegmentWidget->GetDistalElastEntry()->SetValueAsDouble(0);
    this->SegmentWidget->GetDistalViscoEntry()->SetValueAsDouble(0);
    this->SegmentWidget->GetDistalElastEntry()->ReadOnlyOff();
    }


  double visco = 	this->SegmentWidget->GetDistalViscoEntry()->GetValueAsDouble();
  double time = this->SegmentWidget->GetDistalcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetDistalElastEntry()->GetValueAsDouble();
  double angle;
  angle =  atan(  ((2 * vtkMath::Pi()) * visco)  / (time * elastin)       );

  // angle in radians -- must be converted to degree
  angle = (angle *180)/ vtkMath::Pi();
  this->SegmentWidget->GetDistalViscoElasticAngleEntry()->SetValueAsDouble(angle);
}

//----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::UpdateTreeRefPressure()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->SetTreePressure(0,this->SegmentWidget->GetProxRef());
}
//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::UpdateTreeInfPressure()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->SetTreePressure(1,this->SegmentWidget->GetProxInf());

}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::UpdateProxViscoValue()
{
  double visco;
  double time = this->SegmentWidget->GetProxcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetProxElastEntry()->GetValueAsDouble();
  double angle = this->SegmentWidget->GetProxViscoElasticAngleEntry()->GetValueAsDouble();
  double Rad_angle = angle * (vtkMath::Pi()/180);
  visco = (tan(Rad_angle) * time * elastin) / (2 * (vtkMath::Pi()) );
  this->SegmentWidget->GetProxViscoEntry()->SetValueAsDouble(visco);
}

//----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::UpdateProxViscoAngleValue()
{
  double visco = 	this->SegmentWidget->GetProxViscoEntry()->GetValueAsDouble();
  double time = this->SegmentWidget->GetProxcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetProxElastEntry()->GetValueAsDouble();
  double angle; //     = this->SegmentWidget->GetProxViscoElasticAngleEntry()->GetValueAsDouble();
  angle =  atan(  (2 * vtkMath::Pi()) * visco  / (time * elastin)       );

  // angle in radians -- must be converted to degree
  angle = (angle *180)/ vtkMath::Pi();

  this->SegmentWidget->GetProxViscoElasticAngleEntry()->SetValueAsDouble(angle);
}

//----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::UpdateDistalViscoValue()
{
  double visco;
  double time = this->SegmentWidget->GetDistalcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetDistalElastEntry()->GetValueAsDouble();
  double angle = this->SegmentWidget->GetDistalViscoElasticAngleEntry()->GetValueAsDouble();
  double Rad_angle = angle * (vtkMath::Pi()/180);
  visco = (tan(Rad_angle) * time * elastin) / (2 * (vtkMath::Pi()) );
  this->SegmentWidget->GetDistalViscoEntry()->SetValueAsDouble(visco);
}

//----------------------------------------------------------------------------


void vtkPVHMStraightModelWidget::UpdateDistalViscoAngleValue()
{
  double visco = 	this->SegmentWidget->GetDistalViscoEntry()->GetValueAsDouble();
  double time = this->SegmentWidget->GetDistalcharacTimeEntry()->GetValueAsDouble();
  double elastin = this->SegmentWidget->GetDistalElastEntry()->GetValueAsDouble();
  double angle; //     = this->SegmentWidget->GetProxViscoElasticAngleEntry()->GetValueAsDouble();
  angle =  atan(  (2 * vtkMath::Pi()) * visco  / (time * elastin)       );

  // angle in radians -- must be converted to degree
  angle = (angle *180)/ vtkMath::Pi();

  this->SegmentWidget->GetDistalViscoElasticAngleEntry()->SetValueAsDouble(angle);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SegmentProxSpeedOfSoundValue()
{
  double elastin = this->SegmentWidget->GetProxElast();

  double proxRadius = this->SegmentWidget->GetProximalRadius();
  double proxWall = this->SegmentWidget->GetProximalWall();

  double density = 1.04;

  double speedOfSound = sqrt( (elastin * proxWall)/(2 * density * proxRadius) );

  this->SegmentWidget->GetProxSpeedOfSoundEntry()->SetValueAsDouble(speedOfSound);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SegmentDistalSpeedOfSoundValue()
{
  double elastin = this->SegmentWidget->GetProxElast();

  double distalRadius = this->SegmentWidget->GetDistalRadius();
  double distalWall = this->SegmentWidget->GetDistalWall();

  double density = 1.04;

  double speedOfSound = sqrt( (elastin * distalWall)/(2 * density * distalRadius) );

  this->SegmentWidget->GetDistalSpeedOfSoundEntry()->SetValueAsDouble(speedOfSound);
}

////----------------------------------------------------------------------------
//void vtkPVHMStraightModelWidget::ElementSpeedOfSoundValue()
//{
//	double elastin = this->ElementWidget->GetElastin();
//
//	double proxRadius = this->ElementWidget->GetProximalRadius();
//	double distalRadius = this->ElementWidget->GetDistalRadius();
//	double mediaRadius = (proxRadius + distalRadius)/2;
//
//	double proxWall = this->ElementWidget->GetProximalWallThickness();
//	double distalWall = this->ElementWidget->GetDistalWallThickness();
//	double mediaWall = (proxWall + distalWall)/2;
//
//	double density = 1.04;
//
//	double speedOfSound = sqrt( (elastin * mediaWall)/(2 * density * mediaRadius) );
//
//	this->ElementWidget->GetSpeedOfSoundEntry()->SetValueAsDouble(speedOfSound);
//}

//----------------------------------------------------------------------------
//
void vtkPVHMStraightModelWidget::CloneSegmentCallback()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  vtkStringArray *origArray = this->SegmentWidget->GetSegmentNameOrigin();
  vtkStringArray *targetArray = this->SegmentWidget->GetSegmentNameTarget();

  if ( (origArray->GetNumberOfValues() > 0 ) || (targetArray->GetNumberOfValues() > 0 ) )
    {
    proxy->CloneSegment(origArray, targetArray);
    this->SegmentWidget->RemoveAllSegmentNameInCloneList();
    }

}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::AddCloneSegmentCallback()
{
  this->SegmentWidget->AddSegmentNameInCloneList();
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::RemoveCloneSegmentCallback()
{
  this->SegmentWidget->RemoveSegmentNameInCloneList();
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::RemoveAllCloneSegmentCallback()
{
  this->SegmentWidget->RemoveAllSegmentNameInCloneList();
}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::ConfigureXScale()
{
  double x;

  x = this->XScaleThumbWheel->GetValue();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  proxy->ConfigureScale(x, 'x');

  double *size;
  size = proxy->GetSize();

  this->XSizeEntry->SetValueAsDouble(size[0]);
}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::ConfigureYScale()
{
  double y;

  y = this->YScaleThumbWheel->GetValue();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  proxy->ConfigureScale(y, 'y');

  double *size;
  size = proxy->GetSize();

  this->YSizeEntry->SetValueAsDouble(size[1]);
}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::ConfigureZScale()
{
  double z;

  z = this->ZScaleThumbWheel->GetValue();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  proxy->ConfigureScale(z, 'z');

  double *size;
  size = proxy->GetSize();

  this->ZSizeEntry->SetValueAsDouble(size[2]);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ConfigureScale()
{
  double size[3];

  size[0] = this->XSizeEntry->GetValueAsDouble();
  size[1] = this->YSizeEntry->GetValueAsDouble();
  size[2] = this->ZSizeEntry->GetValueAsDouble();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->ConfigureScale(size);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::InvertFlowDirectionCallback()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  if ( proxy->GetSegmentProxy()->InvertFlowDirection() )
          proxy->InvertFlowDirection();
}

//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::PrintSelf(ostream& os, vtkIndent indent)
{

}

void vtkPVHMStraightModelWidget::ClearPlot()
{
  this->XYPlotWidget->Clear();

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());
  proxy->ClearPlot();
  this->PlotWidget->ExportResultButton->SetEnabled(0);
  this->PlotWidget->GraphsUpdateButton->SetEnabled(0);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::Select()
{
  this->Superclass::Select();
  this->Script("pack %s -padx 0 -pady 0 -side bottom -fill x -expand no",  this->EditToolbar->GetWidgetName());
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::Deselect()
{
  this->Superclass::Deselect();
  this->Script("pack forget %s", this->EditToolbar->GetWidgetName());
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ReadSimulationButtonCallback()
{

  this->ReadSimulation(this->ReadSimulationButton->GetFileName());

/*
vtkSMHMStraightModelWidgetProxy *wProxy =
      vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  int Result = wProxy->ReadSimulationData(this->ReadSimulationButton->GetFileName());

  if ( Result )
    {
    this->PlotModeButton->EnabledOn();

    }
  else
    {
    vtkKWMessageDialog::PopupMessage(
    this->GetApplication(), this->GetParentWindow(), "Error",
    "DataOut file corrupted!",
     vtkKWMessageDialog::ErrorIcon);

    this->PlotModeButton->EnabledOff();
    }
  this->ReadSimulationButton->SetText("Load\nData");
  */
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ChangePlotMode(int show)
{
  if(show==0)
    this->PlotWidget->NormalPlotMode(this, 1);

  if(show==1)
    this->PlotWidget->AnimatedPlotMode(this, 1);
}


void vtkPVHMStraightModelWidget::SetAnimatedPlotType(int type)
{
  this->XYAnimatedPlotWidget->SetTypePlot(type);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::CalculateTreeVolume()
{
  double volume;

  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  volume = proxy->CalculateTreeVolume();

  char temp[50];
  sprintf(temp, "Volume of tree: %.2f", volume);

  this->TreeVolumeLabel->SetText(temp);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetSmallerSegment()
{
  vtkSMHMStraightModelWidgetProxy *proxy =
                  vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  vtkIdType idSegment = proxy->GetSmallerSegment();

  char *name = proxy->GetSegmentProxy()->GetSegmentName();
  double length = proxy->GetSegmentProxy()->GetLength();

  char temp[100];
  char temp2[100];

  for (int i = 0; i < 100; ++i)
    {
    temp[i]=0;
    temp2[i]=0;
    }


  strcat (temp, "Smaller Segment: ");
  strcat (temp, name);

  sprintf(temp2, " with length of %.3f cm", length);
  strcat (temp, temp2);

  SmallerSegmentNameLabel->SetText(temp);


  vtkDoubleArray *BasicSeg = proxy->GetSegmentProxy()->GetSegmentBasicInfo();
  this->LengthElementEntry->SetValueAsDouble(BasicSeg->GetValue(1));

  BasicSeg->Delete();

  proxy->HighlightSegment(idSegment);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::UpdateSmallerSegment()
{
  vtkSMHMStraightModelWidgetProxy *proxy =
                          vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  vtkIdType idSegment = proxy->GetSmallerSegment();

  char *name = proxy->GetSegmentProxy()->GetSegmentName();
  double length = proxy->GetSegmentProxy()->GetLength();

  char temp[100];
  char temp2[100];


  for (int i = 0; i < 100; ++i)
    {
    temp[i]=0;
    temp2[i]=0;
    }

  strcat (temp, "Smaller Segment: ");
  strcat (temp, name);

  sprintf(temp2, " with length of %.3f cm", length);
  strcat (temp, temp2);
  SmallerSegmentNameLabel->SetText(temp);

  vtkDoubleArray *BasicSeg = proxy->GetSegmentProxy()->GetSegmentBasicInfo();
  this->LengthElementEntry->SetValueAsDouble(BasicSeg->GetValue(1));
  BasicSeg->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ConfigureDefaultLengthOfElements()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);
  proxy->ConfigureDefaultLengthOfElements(this->LengthElementEntry->GetValueAsDouble());
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SelectSegment()
{
  vtkSMHMStraightModelWidgetProxy *proxy =
                  vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  char *name = (char*)this->AllSegmentsNameComboBox->GetValue();

  if ( name )
    if ( strcmp(name, "") )
      {
      //seta o tipo de visualização - segment ou de elementos
      this->SelectTreeElement(0, 1);

      //pega a propriedade xml
      vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
      this->WidgetProxy->GetProperty("StraightModelObjectSelected"));
      //seta a propriedade como SEGMENT
      smSelect->SetElement(0, SEGMENT);
      proxy->SetStraightModelObjectSelected(SEGMENT);

      //exibe o segmento selecionado na lista e seta no widget o id do segmento selecionado
      proxy->HighlightSegment(name);

      //atualiza as informações do xml com o segmento selecionado
      proxy->UpdateWidgetProperties();

      this->SetElementWidgetValues();

      if (this->SegmentWidget->GetSegmentSelected())
        this->SetBasicSegmentWidgetValues();
      if (this->SegmentWidget->GetElementSelected())
        this->SetElementGeometricalInfo();

      this->SetAdvancedSegmentWidgetValues();

      this->InvokeEvent("WidgetModifiedEvent");
      this->UpdateWidgetInfo();
      }
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetInfoSubTree()
{
  vtkSMHMStraightModelWidgetProxy *proxy = vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->GetWidgetProxy());

  //************************Informações dos terminais

  //Resistência Equivalente
  this->ResEq=proxy->GetEquivalentResistance();

  char Res[512];
  sprintf(Res, "Eq.Resistance [dyn.sec/cm^5]: %.3e", 1/this->ResEq);
  this->ResistanceEq->SetText(Res);

  //Capacitância Equivalente
  this->CapEq=proxy->GetEquivalentCapacitance();

  char Cap[512];
  sprintf(Cap, "Eq.Capacitance [cm^5/dyn]: %.3e", this->CapEq);
  this->CapacitanceEq->SetText(Cap);

  //************************Informações dos segmentos

  //Maior Raio
  vtkDoubleArray *arrayRMax = proxy->GetMaxRadiusInSubTree();

  double maxRadius = arrayRMax->GetValue(0);
  int idSegRMax = int(arrayRMax->GetValue(1));
  int idNodeRMax = int(arrayRMax->GetValue(2));

  char maxR[512];
  sprintf(maxR, "Radius [cm]Max.: %.3f  Segm.: %d  Node: %d", maxRadius, idSegRMax, idNodeRMax);
  this->MaxRadius->SetText(maxR);

  arrayRMax->Delete();

  //Menor Raio
  vtkDoubleArray *arrayRMin = proxy->GetMinRadiusInSubTree();

  double minRadius = arrayRMin->GetValue(0);
  int idSegRMin = int(arrayRMin->GetValue(1));
  int idNodeRMin = int(arrayRMin->GetValue(2));

  char minR[512];
  sprintf(minR, "Radius [cm]Min.: %.3f  Segm.: %d  Node: %d", minRadius, idSegRMin, idNodeRMin);
  this->MinRadius->SetText(minR);

  arrayRMin->Delete();

  //Maior Wall Thickness
  vtkDoubleArray *arrayWMax = proxy->GetMaxWallThicknessInSubTree();

  double maxWall = arrayWMax->GetValue(0);
  int idSegWMax = int(arrayWMax->GetValue(1));
  int idNodeWMax = int(arrayWMax->GetValue(2));

  char maxW[512];
  sprintf(maxW, "Wall Thickness[cm] Max.: %.3f  Segm.: %d  Node: %d", maxWall, idSegWMax, idNodeWMax);
  this->MaxWall->SetText(maxW);

  arrayWMax->Delete();

  //Menor Wall Thickness
  vtkDoubleArray *arrayWMin = proxy->GetMinWallThicknessInSubTree();

  double minWall = arrayWMin->GetValue(0);
  int idSegWMin = int(arrayWMin->GetValue(1));
  int idNodeWMin = int(arrayWMin->GetValue(2));

  char minW[512];
  sprintf(minW, "Wall Thickness[cm] Min.: %.3f  Segm.: %d  Node: %d", minWall, idSegWMin, idNodeWMin);
  this->MinWall->SetText(minW);

  arrayWMin->Delete();

  this->ResEq = 1/this->ResEq;


}


// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::TerminalsCurvesUpdate()
{
  this->TerminalWidget->ReadCurve();
}
// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::EnableR1CurveEdition()
{
  this->TerminalWidget->EnableR1CurveEdition();
}
// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::EnableR2CurveEdition()
{
  this->TerminalWidget->EnableR2CurveEdition();
}
// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::EnableCapCurveEdition()
{
  this->TerminalWidget->EnableCapCurveEdition();
}
// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::EnablePCurveEdition()
{
  this->TerminalWidget->EnablePCurveEdition();
}
// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ReadCurveTerminal()
{
  this->TerminalWidget->ReadCurveSingleParam();
}

// ----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ShowCurveTerminal()
{
  this->TerminalWidget->ShowPressurePlot();
}

// ----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::CalculateCurve()
{
  this->TerminalWidget->CalculateCurve();
}

// ----------------------------------------------------------------------------
VTK_THREAD_RETURN_TYPE vtkThreadedExecute( void *arg )
{
	int threadId, threadCount;

	threadId = ((vtkMultiThreader::ThreadInfo *)(arg))->ThreadID;
  threadCount = ((vtkMultiThreader::ThreadInfo *)(arg))->NumberOfThreads;

	#ifdef _WIN32

	  system("Solver1D.exe");
  	system("del Solver1D.exe");


	#else

	  system("./SolverGP.x");
  	system("rm SolverGP.x");


	#endif

  return VTK_THREAD_RETURN_VALUE;
}
//----------------------------------------------------------------------------

void vtkPVHMStraightModelWidget::ExecuteSolver()
{
  if (this->GeneralConfiguration)
		{

		if (this->GeneralConfiguration->GetSolverFilesPath())
			{
			vtkSMHMStraightModelWidgetProxy *wProxy =
					vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);
			strcpy(this->SolverPath, this->GeneralConfiguration->GetSolverFilesPath());

			#ifdef _WIN32
        // *******************************
				// no Windows, chamar Solver1D.exe
				// *******************************

				string s;
				if (!vtksys::SystemTools::ChangeDirectory(this->GetApplication()->GetInstallationDirectory()))
					{
					string str = this->SolverPath;
					int res = str.find("/", 0);
					while ( res != -1 )
						{
						str.replace(res, 1, "\\");
						res = str.find("/", 0);
						}
					s = "copy Solver1D.exe "; // assumindo que o Solver esta no mesmo diretorio do paraview

					s += str.c_str();

					system(s.c_str());
					vtksys::SystemTools::ChangeDirectory(this->SolverPath);
					}
        //this->thread_id = this->Threader->SpawnThread(vtkThreadedExecute, (void *)this);

				int returnValue =  system("Solver1D.exe");

				if (returnValue)
				  {
				  vtkKWMessageDialog::PopupMessage(
				      this->GetApplication(), this->GetParentWindow(), "Ok",
				      "Simulation error - review your SolverGP input files",
				      vtkKWMessageDialog::Beep);
				  }
				else
				  {
				  string Message("Simulation terminated ");
				  system("del Solver1D.exe");
				  string DataPath(this->SolverPath);
				  DataPath += "\\DataOut.txt";

				  if (this->ReadSimulation(DataPath.c_str()))
				    Message +="and simulation data loaded";
				  else
				    Message +="and simulation data not loaded - Review your DataOut.txt";

				  vtkKWMessageDialog::PopupMessage(
				      this->GetApplication(), this->GetParentWindow(), "Warning",
				      Message.c_str(),
				      vtkKWMessageDialog::Beep);
				  }
       // **********FIM BLOCO WINDOWS*******************

	    #else // ****** linux OS *********
	       string command;
	       //if (!chdir(this->GetApplication()->GetInstallationDirectory()))
          //{
	       //strcat(command, "cp  ../../utilities/SolverSerial/binary/SolverGP.x ");

	       command.append("cp $HeMoLab_PATH/SolverGP.x "); // para ser utilizado na versao instalada
	       command.append(this->SolverPath);
	       system(command.c_str());
	       chdir(this->SolverPath);
					//}
	       this->GetPVApplication()->GetMainWindow()->SetProgress("Running Solver",20);
	       int returnValue =  system("./SolverGP.x");

	       if (returnValue)
	         {
	         vtkKWMessageDialog::PopupMessage(
		         this->GetApplication(), this->GetParentWindow(), "Ok",
		         "Simulation error - review your SolverGP input files",
		         vtkKWMessageDialog::Beep);
	         }
	       else
	         {
	         string Message("Simulation terminated ");
	         system("rm SolverGP.x");
		       string DataPath(this->SolverPath);

		       DataPath += "//DataOut.txt";

		       if (this->ReadSimulation(DataPath.c_str()))
		         Message +="and simulation data loaded";
		       else
		         Message +="and simulation data not loaded - Review your DataOut.txt";

		       vtkKWMessageDialog::PopupMessage(
             this->GetApplication(), this->GetParentWindow(), "Warning",
             Message.c_str(),
             vtkKWMessageDialog::Beep);
	         }
			#endif // ******* FIM BLOCO LINUX **************
			}// fim do IF que testa se os arquivos do solver já foram gerados
	  else
	    {
			vtkKWMessageDialog::PopupMessage(
			    this->GetApplication(), this->GetParentWindow(), "Error",
			    "Solver input files not yet generated.",
			    vtkKWMessageDialog::ErrorIcon);
	    }
		}
	else
		{
		// caso os arquivos do solver nao tenham ainda sido gerados
		// e nem a janela de configuracao tenha sido instancida
		vtkKWMessageDialog::PopupMessage(
		    this->GetApplication(), this->GetParentWindow(), "Error",
		    "Solver input files not yet generated.",
		    vtkKWMessageDialog::ErrorIcon);
       this->GenerateSolverFiles();
		}

}

int vtkPVHMStraightModelWidget::ReadSimulation(const char *Path)
{
  vtkSMHMStraightModelWidgetProxy *wProxy =
  vtkSMHMStraightModelWidgetProxy::SafeDownCast(this->WidgetProxy);

  int Result = wProxy->ReadSimulationData(Path);

  if ( Result )
    {
    this->PlotModeButton->EnabledOn();
    this->ReadSimulationButton->SetText("Load\nData");
    return 1;
    }
  else
    {
    vtkKWMessageDialog::PopupMessage(
    this->GetApplication(), this->GetParentWindow(), "Error",
    "DataOut file corrupted or data not correspond the tree.!",
     vtkKWMessageDialog::ErrorIcon);

    this->PlotModeButton->EnabledOff();
    this->ReadSimulationButton->SetText("Load\nData");
    return 0;
    }
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::SetExportDirectory()
{

//	if(this->XYPlotWidget->GetMeshIDForTitle(0) == -1)
//		{
//		vtkKWMessageDialog::PopupMessage( this->GetApplication(), this->GetParentWindow(), "Warning!",
//				"There are not unknowns to save", vtkKWMessageDialog::Beep);
//				return;
//		}

	this->ExportDialog->SetTitle("Exports 1D Simulation");
	this->ExportDialog->ChooseDirectoryOn();
	this->ExportDialog->SetLastPath("");
	this->ExportDialog->Invoke();

	char* directory;
	directory = this->ExportDialog->GetLastPath();
	if (!strcmp(directory,""))
		return;
	else
		this->ExportData(directory);
}

//----------------------------------------------------------------------------
void vtkPVHMStraightModelWidget::ExportData(char* directory)
{
	//[0]pressionDyn
	//[1]pressionMMHG
	//[2]area
	//[3]flow
	//[4]velocityCMs
	//[5]velocityMs

	int MaxCurves = this->XYPlotWidget->GetMaxCurves();
	for(int i = 0; i < MaxCurves; i++)//MAX_CURVES contem o numero total de curvas plotadas
		{
		if( (this->XYPlotWidget->GetMeshIDForTitle(i) != -1) && (this->XYPlotWidget->GetTypeForTitle(i) != '1') )
			{
			vtkDoubleArray *pressionDyn;
			vtkDoubleArray *pressionMMHG;
			vtkDoubleArray *area;
			vtkDoubleArray *flow;
			vtkDoubleArray *velocityCMs;
			vtkDoubleArray *velocityMs;

			pressionDyn 	= this->XYPlotWidget->GetDataArrayCollection(0, i);
			pressionMMHG 	= this->XYPlotWidget->GetDataArrayCollection(1, i);
			area 					= this->XYPlotWidget->GetDataArrayCollection(2, i);
			flow 					= this->XYPlotWidget->GetDataArrayCollection(3, i);
			velocityCMs		= this->XYPlotWidget->GetDataArrayCollection(4, i);
			velocityMs		= this->XYPlotWidget->GetDataArrayCollection(5, i);
			char name[500];

			if ( this->XYPlotWidget->GetTypeForTitle(i) == 'E' )
				sprintf(name, "%s/Element_%d.txt ", directory, this->XYPlotWidget->GetMeshIDForTitle(i));
			else if ( (this->XYPlotWidget->GetTypeForTitle(i) == 'N') || (this->XYPlotWidget->GetTypeForTitle(i) == 'T') )
				sprintf(name, "%s/Node_%d.txt ", directory, this->XYPlotWidget->GetMeshIDForTitle(i));

			ofstream os(name);

			//para escrever a legenda no arquivo
		  os << "*T[sec] ";
		  if(this->PlotOptions->GetValue(0))
		  	os << "P[dyn/cm^2] ";
		  if(this->PlotOptions->GetValue(1))
		  	os << "P[mmHg] ";
//		  if(this->PlotOptions->GetValue(2))
		  os << "A[cm^2] ";
//		  if(this->PlotOptions->GetValue(3))
		  os << "Q[cm^3] ";
		  if(this->PlotOptions->GetValue(4))
		  	os << "V[cm/s] ";
		  if(this->PlotOptions->GetValue(5))
		  	os << "V[m/s] ";
			os << "\n";

			//para escrever os dados no arquivo
			int NumberOfTuples = this->XYPlotWidget->GetTime()->GetNumberOfTuples();
			for ( int j = 0; j < NumberOfTuples; j++ )
				{
			  os << this->XYPlotWidget->GetTime()->GetValue(j) << " ";
			  if(this->PlotOptions->GetValue(0))
			  	os << pressionDyn->GetValue(j) << " ";
			  if(this->PlotOptions->GetValue(1))
			  	os << pressionMMHG->GetValue(j) << " ";
//			  if(this->PlotOptions->GetValue(2))
			  os << area->GetValue(j) << " ";
//			  if(this->PlotOptions->GetValue(3))
			  os << flow->GetValue(j) << " ";
			  if(this->PlotOptions->GetValue(4))
			  	os << velocityCMs->GetValue(j) << " ";
			  if(this->PlotOptions->GetValue(5))
			  	os << velocityMs->GetValue(j) << " ";
				os << "\n";
			  }
		  os.close();
			}
		}
}
