#ifndef __vtkPVHMStraightModelWidget_h
#define __vtkPVHMStraightModelWidget_h

#include "vtkPV3DWidget.h"
#include "vtkIntArray.h"
#include "vtkKWToolbar.h"
#include "vtkKWPushButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkPVHM1DSolverFilesConfigurationWidget.h"
#include "vtkHMGraphicProperty.h"
#include "vtkKWHM1DXYAnimatedPlotWidget.h"
#include <string>
using namespace std;

#include "vtkMultiThreader.h"

class vtkKWEntry;
class vtkKWLabel;
class vtkKWScaleWithEntrySet;
class vtkKWMenuButtonWithLabel;
class vtkKWThumbWheel;
class vtkKWSeparator;

class vtkPVHMTerminalWidget;
class vtkPVHMHeartWidget;
class vtkPVHMSegmentWidget;
class vtkPVHMElementWidget;
class vtkPVHMNodeWidget;
class vtkPVHMPlotWidget;
class vtkPVHMGeometryWidget;
class vtkPVHMMultipleTerminalWidget;
class vtkPVHMMultipleSegmentWidget;
class vtkPVHMInterventionsWidget;
class vtkPVHMSubTreeMesh;
class vtkKWHM1DXYPlotWidget;
class vtkKWHM1DXYAnimatedPlotWidget;
class vtkDataArrayCollection;

class vtkMultiThreader;

class VTK_EXPORT vtkPVHMStraightModelWidget : public vtkPV3DWidget
{
public:
  static vtkPVHMStraightModelWidget* New();
  vtkTypeRevisionMacro(vtkPVHMStraightModelWidget, vtkPV3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the widget.
  // Overridden to set up control dependencies among properties.
  virtual void Create(vtkKWApplication* app);

  // Description:
  // This method does the actual placing. It sets the first point at
  // (bounds[0]+bounds[1])/2, bounds[2], (bounds[4]+bounds[5])/2
  // and the second point at
  // (bounds[0]+bounds[1])/2, bounds[3], (bounds[4]+bounds[5])/2
  virtual void ActualPlaceWidget();

//BTX
  // Description:
  // Creates and returns a copy of this widget. It will create
  // a new instance of the same type as the current object
  // using NewInstance() and then copy some necessary state
  // parameters.
  vtkPVHMStraightModelWidget* ClonePrototype(vtkPVSource* pvSource,
                                 vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map);
//ETX

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Called when the reset button is pushed.
  // Sets widget's value to the object-variable's value.
  // Side effect is to turn the modified flag off.
  virtual void ResetInternal();

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  //////////////////////////////////////////////////////////////////
  // 1DStraightModel Methods
  // Description:
  // Get object type (heart, terminal, node, element) from an ID
  void GetObjectTypeName(char *type, int id);

  // Description:
  // Updates Widgets Labels
  void UpdateWidgetInfo();

  // Description:
  // Set/Get current widget mode (visualization, edition etc).
  vtkSetMacro(WidgetMode, int);
  vtkGetMacro(WidgetMode, int);

  // Description:
  // 1D Straight Model Callbacks
  void SegmentViscoelasticCallback();
  void SegmentInterpolationCallback();
  void PlotTypeCallback();
  void PlotHeartCallback();
  void VisualizationStateCallback();
  void PlotUpdateType();

  //Description
  //Switch between NormalPlotMode and AnimatedPlotmode
  void ChangePlotMode(int show);

  // Description:
  // tree element update methods
  void TerminalUpdateProps();

  // Description:
  // updates all the nodes properties from a segment using interpolation or constant values
  void SegmentNodeUpdateProps();

  // Description:
  // update only single node properties
  void SingleNodeUpdateProps();

  // Description:
  // methos used to update the parameters values of single segment
  void SingleElementUpdateProps();

  // Description:
  // used to update the values from every element from a given segment
  void SegmentUpdateProps();

  // Description:
  //
  void InterventionsUpdateProperties();

  // Description:
  // Update toolbar visible buttons and current widget mode (visualization, edition or plot mode)
  void UpdateToolbar(int option);

  // Description:
  // used when the visualization or editon mode is selected;
  void SelectTreeElement(int element, int segment);

  // Description:
  //
  void SetElementGeometricalInfo(void);

  // Description:
  // Displays the Window Dialog where the user can select the path to generate Solver input files
  void GenerateSolverFiles();

  // To force the UI appear invisible on the creation
  bool disableUI;

  // Description:
  //Set and Get for XYPlotWidget
  vtkGetObjectMacro(XYPlotWidget,vtkKWHM1DXYPlotWidget);

  // Description:
  //Set and Get for XYAnimatedPlotWidget
  vtkGetObjectMacro(XYAnimatedPlotWidget,vtkKWHM1DXYAnimatedPlotWidget);

  // Description:
  // Plot graphic of informations on the elements
	int PlotGraphic(vtkDoubleArray *array, vtkDoubleArray *time, char type, int meshID);

  // Description:
  // Update the properties from  multiple segments or from whole tree segments (if
  // WholeTree is true)
  void SetMultipleSegments(int WholeTree);

  // Description:
  // Update the properties from  multiple terminals
  void SetMultipleTerminals();

  // Description:
  // Refine mesh multiple segments
  void RefineSegments();

  // Description:
  // Update button selected in bar of buttons to edit mode
  void UpdateEditButton(int option);

  // Description:
  // Set/Get edition mode selected
  vtkGetMacro(EditionMode, int);
  vtkSetMacro(EditionMode, int);

  // Description:
  // Update button selected in bar of buttons to surgical interventions
  void UpdateSurgicalInterventionsButton(int option);

  // Description:
  // Set/Get surgical interventions mode selected
  vtkGetMacro(SurgicalInterventionsMode, int);
  vtkSetMacro(SurgicalInterventionsMode, int);

  // Description:
  // Configure heart curve for imposes flow or pressure curve
  void ConfigureHeartCurve(char v);

  // Description:
  // Create automatic heart curve with function selected in menu
  void CreateHeartCurve();

  // Description:
  // Add heart curve of one (.txt) file
  void AddHeartCurve();

  // Description:
  // Save the heart curve edited
  void SaveHeartCurve();

  // Description:
  // Callback method used when tha value of proximal Viscoelasticity is modified. This update the value of visco angle
  void UpdateSegmentProxViscoWidgets();

  // Description:
  // Callback method used when tha value of distal Viscoelasticity is modified. This update the value of visco angle
  void UpdateSegmentDistalViscoWidgets();

  // Description:
  // used to update the value of tree's reference pressure
  void UpdateTreeRefPressure();

  // Description:
  // used to update the value of tree's infiltration pressure
	void UpdateTreeInfPressure();

  // Description:
  //  update the value of proximal viscoelasticity when the caracteristic time is modified
  void UpdateProxViscoValue();

  // Description:
  //  update the value of proximal visco angle when proximal visco or elastin is modified
  void UpdateProxViscoAngleValue();

  // Description:
  //  update the value of distal viscoelasticity when the caracteristic time is modified
  void UpdateDistalViscoValue();


  // Description:
  //  update the value of distal visco angle when distal visco or elastin is modified
  void UpdateDistalViscoAngleValue();

  // Description:
  //  Set the proximal and distal value for segment's SpeedOfSound
  void SegmentProxSpeedOfSoundValue();
  void SegmentDistalSpeedOfSoundValue();

//  //  Set the value for element's SpeedOfSound
//  void ElementSpeedOfSoundValue();

  // Description:
  // Clone Segment
  void CloneSegmentCallback();

  // Description:
  // Add name of the segments in list to clone
  void AddCloneSegmentCallback();

  // Description:
  // Remove name of the segments in list
  void RemoveCloneSegmentCallback();

  // Description:
  // Remove all names of the segments in list
  void RemoveAllCloneSegmentCallback();

  // Description:
  // Configure scale of the axle x, y and z of the grid
  void ConfigureXScale();
  void ConfigureYScale();
  void ConfigureZScale();

  // Description:
  // Configure scale of the axle x, y and z of the grid
  void ConfigureScale();


  // Description:
  //
  void TerminalsUpdateProps(int option);

  // Description:
  // Inver flow direction.
  void InvertFlowDirectionCallback();

  // Description:
  // Clear render with select button clear
  void ClearPlot();

  // Description:
  // This method is called when the source that contains this widget
  // is selected.
  virtual void Select();

  // Description:
  // This method is called when the source that contains this widget
  // is deselected.
  virtual void Deselect();

  // Description:
  // Callback to read solver simulation data
  void ReadSimulationButtonCallback();

  // Description:
  // triggers solver files write process
  void ProcessValuesFromBasParamWindow();

  // Description:
  // Set a type of animated plot graphic
  void SetAnimatedPlotType(int type);

  // Description:
  // Configure volume of the tree.
  void CalculateTreeVolume();

  // Description:
  // Set smaller segment name and length and highlight the segment.
  void SetSmallerSegment();

  // Description:
  // Update smaller segment property.
  void UpdateSmallerSegment();

  // Description:
  // Configure all elements of the tree with value especified in box "LengthElementEntry" or
  // based in the length of the element of the smaller segment of the tree.
  void ConfigureDefaultLengthOfElements();

  // Description:
  // Highlight segment selected.
  void SelectSegment();

  // Description:
  // Show Equivalent Resistance and Capacitance, the greater and fewer Wallthickness and Radius.
  void SetInfoSubTree();

  // Description:
  // Calback method used to update the properties from a terminal with functions read from file
  void TerminalsCurvesUpdate();



  void EnableR1CurveEdition();
  void EnableR2CurveEdition();
  void EnableCapCurveEdition();
  void EnablePCurveEdition();
  void ReadCurveTerminal();
  void ShowCurveTerminal();
  void CalculateCurve();

  // Description:
  // method associated with "Simulate" push button from HeMoLab toolbar.
  // This triggers solver execution using input files that has been generated
  // in current session.
  void ExecuteSolver();

  //
  // 1 - sucess
  // 0 - error while loading simulation data
  int ReadSimulation(const char *Path);

  // Decription:
  // Set the directory for export results od simulation
  void SetExportDirectory();

  // Decription:
  // Export data in directory choose
  void ExportData(char* directory);


protected:
  vtkPVHMStraightModelWidget();
  ~vtkPVHMStraightModelWidget();

//BTX
  virtual void CopyProperties(vtkPVWidget* clone, vtkPVSource* pvSource,
                              vtkArrayMap<vtkPVWidget*, vtkPVWidget*>* map);

//ETX

  int ReadXMLAttributes(vtkPVXMLElement* element,
                        vtkPVXMLPackageParser* parser);

  // Description:
  // Call creation on the child.
  virtual void ChildCreate(vtkPVApplication*);

  // Description:
  // Place Components in the interface
  void PlaceComponents();

  // Description:
  // Configure Components in the interface
	void ConfigureComponents();

  // Description:
  // Execute event of the RM3DWidget.
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);

  void SetNodeWidgetValues(void);
  void SetTerminalWidgetValues(void);
  void SetElementWidgetValues(void);
  void SetBasicSegmentWidgetValues(void);
  void SetAdvancedSegmentWidgetValues(void);
	void SetGeometryWidgetValues(void);
  void SetGraphType();
  void SetTreeGeneralPropertiesWidgetValues(void);


  void SetInterventionsWidgetValues();


  ///////////////////////////////////////////////////////
  //1D Straight Model Widget
  // Descrition:
  // Labels to show object's Type.
  vtkKWLabel* ObjectTypeLabel;
  vtkKWLabel* ObjectTypeValue;

  // Descrition:
  // Text variables containing the text.
  char *ObjectTypeLabelText;
  char *ObjectTypeValueText;
  vtkSetStringMacro(ObjectTypeValueText);

  // Descrition:
  // Labels to show object's Id.
  vtkKWLabel* ObjectIDLabel;
  vtkKWLabel* ObjectIDValue;

  // Descrition:
  // Text variables containing the text.
  char *ObjectIdLabelText;
  char *ObjectIdValueText;
  vtkSetStringMacro(ObjectIdValueText);

  // Descrition:
  // Labels to show model name.
  vtkKWLabel* ModelNameLabel;
  vtkKWLabel* ModelNameValue;

  //Description
  //Labels to show information of number of segments
  vtkKWLabel* SegmentsNumber;

  //Description
  //Labels to show information of number of terminals
  vtkKWLabel* TerminalsNumber;

  //Description
  //Labels to show information of number of nodes
  vtkKWLabel* NodesNumber;

  //Description
  //Labels to show information of Equivalent Resistance of Sub-tree
  vtkKWLabel* ResistanceEq;

  //Description
  //Labels to show information of Equivalent Capacitance of Sub-tree
  vtkKWLabel* CapacitanceEq;

  //Description
  //Labels for name Radius
//  vtkKWLabel* RadiusLabel;

  //Description
  //Labels to show information the greater number of Radius of Sub-tree
  vtkKWLabel* MaxRadius;

  //Description
  //Labels to show information the fewer Radius of Sub-tree
  vtkKWLabel* MinRadius;

  //Description
  //Labels for name Wall Thickness
//  vtkKWLabel* WallLabel;

  //Description
  //Labels to show information the greater number of Wall Thickness of Sub-tree
  vtkKWLabel* MaxWall;

  //Description
  //Labels to show information the fewer Wall Thickness od Sub-tree
  vtkKWLabel* MinWall;

  // Descrition:
  // Text variables containing the text.
  char *ModelNameLabelText;
  char *ModelNameValueText;
  vtkSetStringMacro(ModelNameValueText);

  vtkPVHMElementWidget  *ElementWidget;
  vtkPVHMSegmentWidget  *SegmentWidget;
  vtkPVHMTerminalWidget *TerminalWidget;
  vtkPVHMHeartWidget 	*HeartWidget;
  vtkPVHMNodeWidget 	*NodeWidget;
  vtkPVHMPlotWidget	*PlotWidget;
  vtkPVHMGeometryWidget  *GeometryWidget;
  vtkPVHMMultipleTerminalWidget *MultipleTerminalWidget;
  vtkPVHMMultipleSegmentWidget *MultipleSegmentWidget;
  vtkPVHMInterventionsWidget *InterventionsWidget;
  vtkPVHMSubTreeMesh *SubTreeMeshWidget;

  int VisualizationStateVisibility;
  vtkKWMenuButtonWithLabel *VisualizationStateOptionMenu;

  // Descrition:
  // Stores current widget mode (visualization, edition etc).
  int WidgetMode;

  // Descrition:
  // Stores 4 values representing which plot will be displayed:
  // Pressure, Area, Flow and All-in-one, respectly
  vtkIntArray *PlotOptions;

  // Descrition:
  // Stores 4 values representing which animated plot will be displayed:
  // Pressure, Area, Flow and All-in-one, respectly
  vtkIntArray *AnimatedPlotOptions;


  // Descrition:
  // Toolbar used to control the software operation mode
  vtkKWToolbar *ToolBar;

  // Descrition:
  // Toolbar radio buttons
  vtkKWRadioButton *InputVisualizationModeButton;
  vtkKWRadioButton *EditionModeButton;
  vtkKWRadioButton *PlotModeButton;
  vtkKWRadioButton *ToDetachSubTreeButton;
  vtkKWRadioButton *EditElemButton;
  vtkKWRadioButton *EditSegButton;
  // Despription:
  // Push button that enable special tools for cases of pathology and surgical interventions
  vtkKWRadioButton *SpecialToolsButton;

  // Descrition:
  // Toolbar radio buttons
  vtkKWPushButton *GenerateSolverFilesButton;

  // Descrition:
  // Icons used to set some image to the toolbar buttons
  vtkKWIcon *InputVisualizationModeButtonIcon;
  vtkKWIcon *EditionModeButtonIcon;
  vtkKWIcon *PlotModeButtonIcon;
  vtkKWIcon *ToDetachSubTreeButtonIcon;
  vtkKWIcon *EditElemButtonIcon;
  vtkKWIcon *EditSegButtonIcon;
  vtkKWIcon *GenerateSolverFilesIcon;
  vtkKWIcon *SpecialToolsIcon;

  vtkPVWindow *Window;
	vtkPVApplication* pvApp;

  //  vtkPVWindow *WindowAnimated;
  //	vtkPVApplication* kwApp;
  // Descrition:
  //
  vtkPVHM1DSolverFilesConfigurationWidget *GeneralConfiguration;

  // Description:
  // Object for plot curves referent to the element clicked
  vtkKWHM1DXYPlotWidget* XYPlotWidget;

  // Description:
  // Object for plot curves animated referent to the element clicked
  vtkKWHM1DXYAnimatedPlotWidget* XYAnimatedPlotWidget;

  void BuildHeartCurve();

  /////////////////////////////////////////////////////////////////
  //Configurações da barra de edição da árvore 1D

  // Description:
  // Tool bar for edition 1D tree
  vtkKWToolbar *EditToolbar;

  // Description:
  // Set of tools for edition 1D tree
  vtkKWRadioButtonSet *EditRadioButtonSet;

  // Description:
  // Button for edition 1D tree
  vtkKWRadioButton *EditGeometryButton;
  vtkKWRadioButton *AddSegmentButton;
  vtkKWRadioButton *AddTerminalButton;
  vtkKWRadioButton *DeleteButton;
  vtkKWRadioButton *MoveButton;
  vtkKWRadioButton *CheckConsistenceButton;
  vtkKWRadioButton *CopySegmentButton;
  vtkKWRadioButton *FlipButton;
  vtkKWRadioButton *RestartButton;
  vtkKWRadioButton *KeepSubTreeButton;

  // Description:
  // Icon for edition 1D tree
  vtkKWIcon *EditGeometryIcon;
  vtkKWIcon *AddSegmentIcon;
  vtkKWIcon *AddTerminalIcon;
  vtkKWIcon *DeleteIcon;
  vtkKWIcon *MoveIcon;
  vtkKWIcon *CheckConsistenceIcon;
  vtkKWIcon *CopySegmentIcon;
  vtkKWIcon *FlipIcon;
  vtkKWIcon *RestartIcon;

  int EditionMode;
  //BTX
  enum EditionRadioButtonSelected
    {
    EditGeometry=0,
    AddSegment,
    AddTerminal,
    Delete,
    Move,
    CheckConsistence,
    CopySegment,
    Flip,
    Restart,
    KeepSubTree,
    Out
    };
  //ETX
  /////////////////////////////////////////////////////////////////
  //Fim das configurações da barra de edição da árvore 1D


  /////////////////////////////////////////////////////////////////
  //Configurações da barra de intervenções cirúrgicas

  vtkKWLabel *SurgicalInterventionsLabel;
  // Description:
  // Set of tools for surgical interventions
  vtkKWRadioButtonSet *SurgicalInterventionsRadioButtonSet;

  // Description:
  // Button for surgical interventions
  vtkKWRadioButton *ClipButton;
  vtkKWRadioButton *StentButton;
  vtkKWRadioButton *StenosisButton;
  vtkKWRadioButton *AneurysmButton;
  vtkKWRadioButton *AgingButton;
  vtkKWRadioButton *DeleteInterventionButton;

  // Description:
  // Icon for surgical interventions
  vtkKWIcon *ClipIcon;
  vtkKWIcon *StentIcon;
  vtkKWIcon *StenosisIcon;
  vtkKWIcon *AneurysmIcon;
//  vtkKWIcon *AgingIcon;
  vtkKWIcon *DeleteInterventionIcon;

  int SurgicalInterventionsMode;
  //BTX
  enum SurgicalInterventionsModelSelected
    {
    Clip=0,
    Stent,
    Stenosis,
    Aneurysm,
    Aging,
    DeleteIntervention,
    none
    };
  //ETX

  /////////////////////////////////////////////////////////////////
  //Fim das configurações da barra de intervenções cirúrgicas

  // Description:
  // Create edit tool bar
  void CreateEditToolbar(vtkPVApplication* pvApp);

  // Description:
  // Create surgical interventions tool bar
  void CreateSurgicalInterventionsToolbar(vtkPVApplication* pvApp);

  // Description:
  // Frame for scale property
  vtkKWFrameWithLabel*  ScaleFrame;

  // Description:
  // Frame general prop and sub tree prop
  vtkKWFrameWithLabel*  SubTreePropFrame;

  // Description:
  // Thumb wheel for axle x, y and z
  vtkKWThumbWheel *XScaleThumbWheel;
  vtkKWThumbWheel *YScaleThumbWheel;
  vtkKWThumbWheel *ZScaleThumbWheel;

  // Description:
  // Label for axle x, y and z
  vtkKWLabel *XSizeLabel;
  vtkKWLabel *YSizeLabel;
  vtkKWLabel *ZSizeLabel;

  // Description:
  // Entry for axle x, y and z
  vtkKWEntry *XSizeEntry;
  vtkKWEntry *YSizeEntry;
  vtkKWEntry *ZSizeEntry;

  // Description:
  // Button to read solver simulation
  vtkKWLoadSaveButton *ReadSimulationButton;

  // Description:
  // Label for volume of the tree.
  vtkKWLabel *TreeVolumeLabel;


  // Description:
  // Label for smaller segment of the tree.
  vtkKWLabel *SmallerSegmentNameLabel;
  vtkKWEntry *SmallerSegmentNameEntry;
  vtkKWLabel *SmallerSegmentLengthLabel;
  vtkKWEntry *SmallerSegmentLengthEntry;

  // Description:
  // Highlight smaller segment.
  vtkKWPushButton *HighlightSmallerSegmentButton;

  // Description:
  // Label for length of the element of the smaller segment.
  vtkKWLabel *LengthElementLabel;
  vtkKWEntry *LengthElementEntry;

  // Description:
  // Configure all elements of the tree based in length specified in LengthElementEntry
  vtkKWPushButton *ConfigureDefaultLengthOfElementButton;

  // Description:
  // Menu button with name of all segments on tree. When select a name,
  // the system highligths the segment in the view.
  vtkKWLabel *AllSegmentsNameLabel;
  vtkKWComboBox *AllSegmentsNameComboBox;


  vtkKWFrameWithLabel*  GeneralInfoFrame;
  vtkKWFrame *GeneralFrame;
  vtkKWFrame *GeneralFrame2;

  vtkKWSeparator *GeneralInfoSeparator;

  double ResEq;
  double CapEq;

  // general use string
  string s;


  vtkKWLabel *SimulationLabel;

  // Descrition:
  // Toolbar used to simulation operating
  vtkKWToolbar *SimulationToolBar;

  vtkKWPushButton *ExecuteSolverPushButton;

  vtkKWIcon *RunIcon;
  vtkMultiThreader *Threader;
  int thread_id;
  char SolverPath[512];

  // Description:
  // Dialog for choose the directory of simulation will be saved
  vtkKWLoadSaveDialog *ExportDialog;

  bool KeepRemoveFilterMode;

  // Description:
  // Radio button to filter vtkKeepRemoveFilter.
  vtkKWRadioButton *RemoveFilterRadioButton;
  vtkKWRadioButton *KeepFilterRadioButton;

private:
  vtkPVHMStraightModelWidget(const vtkPVHMStraightModelWidget&); // Not implemented
  void operator=(const vtkPVHMStraightModelWidget&); // Not implemented
};

#endif

