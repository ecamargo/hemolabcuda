/*
 * vtkPVHMSubTreeMesh.cxx
 *
 *  Created on: Jul 9, 2009
 *      Author: igor
 */

/*
 * $Id: vtkPVHMSubTreeMesh.cxx 340 2006-05-12 13:40:37Z ziemer $
 */

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWThumbWheel.h"

#include "vtkPVHMSubTreeMesh.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkHMDefineVariables.h"


vtkCxxRevisionMacro(vtkPVHMSubTreeMesh, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMSubTreeMesh);

//----------------------------------------------------------------------------
vtkPVHMSubTreeMesh::vtkPVHMSubTreeMesh()
{
  this->RefineLabel       = vtkKWLabel::New();

//  this->RefineValueLabel     = vtkKWLabel::New();
//  this->RefineValueEntry     = vtkKWEntry::New();

  this->RefineThumbWeel = vtkKWThumbWheel::New();

  this->ApplyButton  = vtkKWPushButton::New();

  this->RefineMethodComboBox = vtkKWComboBox::New();
}

//----------------------------------------------------------------------------
vtkPVHMSubTreeMesh::~vtkPVHMSubTreeMesh()
{
  this->RefineLabel->Delete();

//  this->RefineValueLabel->Delete();
//  this->RefineValueEntry->Delete();

  this->RefineThumbWeel->Delete();
  this->ApplyButton->Delete();

  this->RefineMethodComboBox->Delete();
}

//----------------------------------------------------------------------------
void vtkPVHMSubTreeMesh::UpdateWidgetInfo(  )
{
  int mode = this->GetReadOnly();

  // Se mode de destaque de sub-arvore, habilita modo de edição de multiplos segmentos
  if((this->GetReadOnly()==MODE_TODETACHSUBTREE_VAL))
    {
    mode = MODE_EDITION_VAL;
    }

//  this->RefineValueEntry->SetReadOnly(mode);
//  this->RefineThumbWeel->SetReadOnly(mode);
}

//----------------------------------------------------------------------------
void vtkPVHMSubTreeMesh::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Sub-Tree Mesh Edition");
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);

  this->RefineLabel->SetParent(this->GetChildFrame()->GetFrame());
  this->RefineLabel->Create(pvApp);
  this->RefineLabel->SetText("Refine Method");

  this->RefineMethodComboBox->SetParent(this->GetChildFrame()->GetFrame());
  this->RefineMethodComboBox->Create(pvApp);
  this->RefineMethodComboBox->SetBalloonHelpString("Select if segments should be updated by a fixed value or a multiplicative factor");
  const char *Method[] = {"Refine", "De-Refine"};
  int i;
  for (i = 0; i < 2; i++)
  this->RefineMethodComboBox->AddValue(Method[i]);

  this->RefineMethodComboBox->SetValue("Refine");

  this->RefineMethodComboBox->ReadOnlyOn();
  this->RefineMethodComboBox->SetWidth(20);

//  this->RefineValueLabel->SetParent(this->GetChildFrame()->GetFrame());
//  this->RefineValueLabel->Create(pvApp);
//  this->RefineValueLabel->SetText("Value:");
//  this->RefineValueEntry->SetParent(this->GetChildFrame()->GetFrame());
//  this->RefineValueEntry->Create(pvApp);
//  this->RefineValueEntry->SetWidth(6);

  this->RefineThumbWeel->SetParent(this->GetChildFrame()->GetFrame());
  this->RefineThumbWeel->PopupModeOn();
  this->RefineThumbWeel->Create(pvApp);
  this->RefineThumbWeel->SetRange(1.0, 10.0);
  this->RefineThumbWeel->ClampMinimumValueOn();
  this->RefineThumbWeel->ClampMaximumValueOn();
  this->RefineThumbWeel->SetResolution(1.0);
  this->RefineThumbWeel->DisplayEntryOn();
  this->RefineThumbWeel->ExpandEntryOn();
  this->RefineThumbWeel->DisplayLabelOn();
  this->RefineThumbWeel->SetValue(1);
  this->RefineThumbWeel->GetLabel()->SetText("Value:");

  this->ApplyButton->SetParent(this->GetChildFrame()->GetFrame());
  this->ApplyButton->Create(pvApp);
  this->ApplyButton->SetText("Apply");

}

//----------------------------------------------------------------------------
void vtkPVHMSubTreeMesh::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
  int mode = mainWidget->GetWidgetMode();

  // Se visibility for 0, sai da função
  if(!this->GetWidgetVisibility())
    {
    mainWidget->Script("grid forget %s",
    this->GetChildFrame()->GetWidgetName());
    return;
    }
  this->GetChildFrame()->ExpandFrame();

  mainWidget->Script("grid %s -sticky ew -columnspan 4",
  this->GetChildFrame()->GetWidgetName());


//  mainWidget->Script("grid %s -row 0 -column 0 -padx 2 -pady 2", this->RefineLabel->GetWidgetName());
//  mainWidget->Script("grid %s -row 0 -column 1 -padx 2 -pady 2", this->RefineMethodComboBox->GetWidgetName());
//
//
//  mainWidget->Script("grid %s -row 1 -column 0 -padx 2 -pady 2", this->RefineValueLabel->GetWidgetName());
//  mainWidget->Script("grid %s -row 1 -column 1 -padx 2 -pady 2", this->RefineValueEntry->GetWidgetName());

  mainWidget->Script("grid %s %s -sticky ew -pady 5", this->RefineLabel->GetWidgetName(), this->RefineMethodComboBox->GetWidgetName());

  mainWidget->Script("grid %s -sticky ew -columnspan 4", this->RefineThumbWeel->GetWidgetName());


  //Se modo de visualização, não aparece botoes de update para multiplos segmentos.
  if(mode == MODE_VISUALIZATION_VAL)
    {
    mainWidget->Script("grid remove %s",
    this->ApplyButton->GetWidgetName());
    }
  // Se modo de edição, aparece botoes de update para multiplos segmentos.
  else if (mode == MODE_EDITION_VAL)
    {
    mainWidget->Script("grid %s - - -sticky ew -pady 5",
    this->ApplyButton->GetWidgetName());
    }
  // Se modo de destaque de sub-arvore aparece o botão de update de segmentos selecionados
  else if (mode == MODE_TODETACHSUBTREE_VAL)
    {
    mainWidget->Script("grid %s - - -sticky ew -pady 5",
    this->ApplyButton->GetWidgetName());
    }

    this->ApplyButton->SetBalloonHelpString("Updates elements properties from whole 1D tree");
}

//----------------------------------------------------------------------------

vtkKWFrameWithLabel* vtkPVHMSubTreeMesh::GetElementFrame()
{
  return this->GetChildFrame();
}

//----------------------------------------------------------------------------
void vtkPVHMSubTreeMesh::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMSubTreeMesh::Accept()
{
}

//----------------------------------------------------------------------------
int vtkPVHMSubTreeMesh::GetRefineMethod()
{
  if (!strcmp (this->RefineMethodComboBox->GetValue(), "Refine"))
      return 1;
  else if (!strcmp (this->RefineMethodComboBox->GetValue(), "De-Refine"))
    return 2;
  else
    return 0;
}

//----------------------------------------------------------------------------
int vtkPVHMSubTreeMesh::GetRefineValue()
{
  int value = (int)this->RefineThumbWeel->GetValue();

  return value;
}
