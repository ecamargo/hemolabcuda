/*
 * vtkPVHMSubTreeMesh.h
 *
 *  Created on: Jul 9, 2009
 *      Author: igor
 */

// .NAME vtkPVHMSubTreeMesh
// .SECTION Description
// Create KW components for edit segment mesh.

#ifndef VTKPVHMSUBTREEMESH_H_
#define VTKPVHMSUBTREEMESH_H_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"
#include "vtkDoubleArray.h"
#include "vtkKWComboBox.h"


//class vtkKWEntry;
//class vtkKWLabel;
class vtkSMSourceProxy;
class vtkKWFrameWithLabel;
class vtkKWThumbWheel;

class vtkPVHMStraightModelWidget;


class VTK_EXPORT vtkPVHMSubTreeMesh : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMSubTreeMesh *New();
  vtkTypeRevisionMacro(vtkPVHMSubTreeMesh,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};

  // Description:
  // Updates Widgets Labels
  void UpdateWidgetInfo(  );

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents(vtkPVHMStraightModelWidget* );

  // Description:
  // Call creation on the child.
  virtual void ChildCreate(vtkPVApplication*);

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Element Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetElementFrame();

  // Description:
  // Return refine value.
  int GetRefineValue();

  // Description:
  // Return method of refinement. Return '1' to refine method, multiply the elements number by the
  // refine value. Return '2' to de-refine method, divide the elements number by the refine value.
  // Return '0' if no method is chosen.
  int GetRefineMethod();


  //Get methods for the Push Buttons
  vtkGetObjectMacro(ApplyButton, vtkKWPushButton);

//**********************************************************************
//**********************************************************************
//**********************************************************************

protected:

  vtkPVHMSubTreeMesh();
  virtual ~vtkPVHMSubTreeMesh();

  vtkKWLabel *RefineLabel;

  vtkKWPushButton *ApplyButton;

//  vtkKWLabel* RefineValueLabel;
//  vtkKWEntry* RefineValueEntry;

  vtkKWThumbWheel *RefineThumbWeel;

  vtkKWComboBox *RefineMethodComboBox;

private:
  vtkPVHMSubTreeMesh(const vtkPVHMSubTreeMesh&); // Not implemented
  void operator=(const vtkPVHMSubTreeMesh&); // Not implemented
};


#endif /* VTKPVHMSUBTREEMESH_H_ */
