/*
 * $Id: vtkPVHMTerminalWidget.cxx 340 2006-05-12 13:40:37Z rodrigo $
 */

#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWLabel.h"
#include "vtkKWIcon.h"
#include "vtkPVHMTerminalWidget.h"
#include "vtkPVHMStraightModelWidget.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkHM1DHeartCurveReader.h"
#include "vtkDoubleArray.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWComboBox.h"
#include "vtkKWHM1DXYPlotWidget.h"
#include "vtkHM1DHeartCurveReader.h"
#include "vtkKWCheckButton.h"
#include "vtkHMXYPlot.h"
#include "vtkKWRange.h"

vtkCxxRevisionMacro(vtkPVHMTerminalWidget, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMTerminalWidget);

//----------------------------------------------------------------------------
vtkPVHMTerminalWidget::vtkPVHMTerminalWidget()
{
  this->R1Label = vtkKWLabel::New();
  this->R1Entry = vtkKWEntry::New();
  this->R2Label	= vtkKWLabel::New();
  this->R2Entry = vtkKWEntry::New();
  this->CapLabel = vtkKWLabel::New();
  this->CapEntry = vtkKWEntry::New();
  this->PressureLabel = vtkKWLabel::New();
  this->PressureEntry = vtkKWEntry::New();
  this->meshIDLabel = vtkKWLabel::New();
  this->meshIDEntry 	= vtkKWEntry::New();
  this->AssNodesLabel = vtkKWLabel::New();
  this->AssNodes = vtkKWLabel::New();
  this->TerminalIcon		= vtkKWLabel::New();

  this->UpdateTerminalPropButton = vtkKWPushButton::New();


  this->UpdateNonLeafTerminalButton = vtkKWPushButton::New();
  this->UpdateLeafTerminalButton = vtkKWPushButton::New();


  this->UpdateTerminalCurves = vtkKWLoadSaveButton::New();

  //*****************************************************
  // elementos de interface quando tem-se pressao variante

  this->VariantPressureOptionsFrame=vtkKWFrameWithLabel::New();
  this->FunctionHeartCurve = vtkKWComboBox::New();
  this->AddCurveButton= vtkKWLoadSaveButton::New();
  this->AddCurveButtonOk = vtkKWPushButton::New();
  this->PointNumber= vtkKWEntryWithLabel::New();
  this->Amplitude = vtkKWEntryWithLabel::New();
  this->FinalTime= vtkKWEntryWithLabel::New();
  this->MaxValue= vtkKWEntryWithLabel::New();
  this->MinValue= vtkKWEntryWithLabel::New();
  this->PlotButton= vtkKWPushButton::New();
  this->UpdateCurveButton = vtkKWPushButton::New();


  this->UpdateR1Curve  = vtkKWCheckButton::New();
  this->UpdateR2Curve  = vtkKWCheckButton::New();
  this->UpdateCapCurve  = vtkKWCheckButton::New();
  this->UpdatePCurve  = vtkKWCheckButton::New();


  this->TerminalPropsFrame = vtkKWFrame::New();

  this->TerminalUpdateButtonsFrame  = vtkKWFrame::New();

  this->XYPlotWidget = vtkKWHM1DXYPlotWidget::New();

  this->reader = vtkHM1DHeartCurveReader::New();


  this->R1 = vtkDoubleArray::New();
  this->R1Time = vtkDoubleArray::New();
  this->R1->SetNumberOfTuples(0);
  this->R1Time->SetNumberOfTuples(0);

  this->R2 = vtkDoubleArray::New();
  this->R2Time = vtkDoubleArray::New();
  this->R2->SetNumberOfTuples(0);
  this->R2Time->SetNumberOfTuples(0);

  this->Cap =vtkDoubleArray::New();
  this->CapTime =vtkDoubleArray::New();
  this->Cap->SetNumberOfTuples(0);
  this->CapTime->SetNumberOfTuples(0);



  this->P =vtkDoubleArray::New();
  this->PTime =vtkDoubleArray::New();
  this->P->SetNumberOfTuples(0);
  this->PTime->SetNumberOfTuples(0);


  this->CheckButtonsArray[0] = this->UpdateR1Curve;
  this->CheckButtonsArray[1] = this->UpdateR2Curve;
  this->CheckButtonsArray[2] = this->UpdateCapCurve;
  this->CheckButtonsArray[3] = this->UpdatePCurve;

  this->GeneralInfoFrame = vtkKWFrame::New();

}

//----------------------------------------------------------------------------
vtkPVHMTerminalWidget::~vtkPVHMTerminalWidget()
{
  this->R1Label->Delete();
  this->R1Entry->Delete();
  this->R2Label->Delete();
  this->R2Entry->Delete();
  this->CapLabel->Delete();
  this->CapEntry->Delete();
  this->PressureLabel->Delete();
  this->PressureEntry->Delete();
  this->meshIDLabel->Delete();
  this->meshIDEntry->Delete();
  this->AssNodesLabel->Delete();
  this->AssNodes->Delete();
  this->TerminalIcon->Delete();

  this->UpdateTerminalPropButton->Delete();


  this->UpdateNonLeafTerminalButton->Delete();
  this->UpdateLeafTerminalButton->Delete();


  this->UpdateTerminalCurves->Delete();

  //*****************************************************
  // elementos de interface quando tem-se pressao variante

  this->VariantPressureOptionsFrame->Delete();
  this->FunctionHeartCurve->Delete();
  this->AddCurveButton->Delete();
  this->AddCurveButtonOk->Delete();
  this->PointNumber->Delete();
  this->Amplitude->Delete();
  this->FinalTime->Delete();
  this->MaxValue->Delete();
  this->MinValue->Delete();
  this->PlotButton->Delete();
  this->UpdateCurveButton->Delete();

  this->UpdateR1Curve->Delete();
  this->UpdateR2Curve->Delete();
  this->UpdateCapCurve->Delete();
  this->UpdatePCurve->Delete();

  this->TerminalPropsFrame->Delete();

  this->TerminalUpdateButtonsFrame->Delete();

  this->XYPlotWidget->Delete();

  this->reader->Delete();

  this->R1->Delete();
  this->R1Time->Delete();

  this->R2->Delete();
  this->R2Time->Delete();

  this->Cap->Delete();
  this->CapTime->Delete();

  this->P->Delete();
  this->PTime->Delete();

  this->GeneralInfoFrame->Delete();
  
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::UpdateWidgetInfo()
{
  int mode = this->GetReadOnly();
  this->R1Entry->SetReadOnly(mode);
  this->R2Entry->SetReadOnly(mode);
  this->CapEntry->SetReadOnly(mode);
  this->PressureEntry->SetReadOnly(mode);
  this->UpdateTerminalCurves->SetEnabled(!mode);

  //this->FunctionHeartCurve->SetEnabled(!mode);
  this->AddCurveButton->SetEnabled(!mode);
  //this->AddCurveButtonOk->SetEnabled(!mode);
  this->PointNumber->GetWidget()->SetReadOnly(mode);
  this->Amplitude->GetWidget()->SetReadOnly(mode);
  this->FinalTime->GetWidget()->SetReadOnly(mode);
  this->MaxValue->GetWidget()->SetReadOnly(mode);
  this->MinValue->GetWidget()->SetReadOnly(mode);

   if (!mode)
     {
     this->UpdateR1Curve->SetStateToNormal();
     this->UpdateR2Curve->SetStateToNormal();
     this->UpdateCapCurve->SetStateToNormal();
     this->UpdatePCurve->SetStateToNormal();

     this->UpdateR1Curve->SetText("Enable function");
     this->UpdateR2Curve->SetText("Enable function");
     this->UpdateCapCurve->SetText("Enable function");
     this->UpdatePCurve->SetText("Enable function");
     }
   else
     {
     this->UpdateR1Curve->SetText("View function");
     this->UpdateR2Curve->SetText("View function");
     this->UpdateCapCurve->SetText("View function");
     this->UpdatePCurve->SetText("View function");

     //A zero value indicates that both strings are equal.
     if (!strcmp(this->R1Entry->GetValue(), "Function"))
       this->UpdateR1Curve->SetStateToNormal();
     else
       this->UpdateR1Curve->SetStateToDisabled();


     if (!strcmp(this->R2Entry->GetValue(), "Function"))
       this->UpdateR2Curve->SetStateToNormal();
     else
       this->UpdateR2Curve->SetStateToDisabled();

     if (!strcmp(this->CapEntry->GetValue(), "Function"))
       this->UpdateCapCurve->SetStateToNormal();
     else
       this->UpdateCapCurve->SetStateToDisabled();

     if (!strcmp(this->PressureEntry->GetValue(), "Function"))
       this->UpdatePCurve->SetStateToNormal();
     else
       this->UpdatePCurve->SetStateToDisabled();
     }

 }

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::ChildCreate(vtkPVApplication* pvApp)
{
  this->GetChildFrame()->SetLabelText("Terminal Information");
  this->GetChildFrame()->SetParent(this->GetFrame());
  this->GetChildFrame()->Create(pvApp);

  this->TerminalPropsFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->TerminalPropsFrame->Create(pvApp);
  //this->TerminalPropsFrame->SetLabelText("Terminal Properties");

  this->TerminalUpdateButtonsFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->TerminalUpdateButtonsFrame->Create(pvApp);
  //this->TerminalUpdateButtonsFrame->SetLabelText("Update");

  this->GeneralInfoFrame->SetParent(this->GetChildFrame()->GetFrame());
  this->GeneralInfoFrame->Create(pvApp);

  
  this->XYPlotWidget->SetTitle("Time Variant Pressure Curve Editor");
  this->XYPlotWidget->SetMasterWindow(vtkKWWindow::SafeDownCast(pvApp->GetNthWindow(0)));
  this->XYPlotWidget->Create(pvApp);

  this->meshIDLabel->SetParent(this->TerminalPropsFrame);
  this->meshIDLabel->Create(pvApp);
  this->meshIDLabel->SetText("Mesh ID:");
  this->meshIDEntry->SetParent(this->GetChildFrame()->GetFrame());
  //this->meshIDEntry->SetWidth(10);
  this->meshIDEntry->Create(pvApp);
  this->meshIDEntry->SetReadOnly(1);

  this->AssNodesLabel->SetParent(this->TerminalPropsFrame);
  this->AssNodesLabel->Create(pvApp);
  this->AssNodesLabel->SetText("Associated Nodes:");
  this->AssNodes->SetParent(this->TerminalPropsFrame);
  this->AssNodes->Create(pvApp);

  this->R1Label->SetParent(this->TerminalPropsFrame);
  this->R1Label->Create(pvApp);
  this->R1Label->SetText("Resistance 1 [dyn.sec/cm^5]:");
  this->R1Entry->SetParent(this->GetFrame());
  this->R1Entry->SetWidth(10);
  this->R1Entry->Create(pvApp);

  this->R2Label->SetParent(this->TerminalPropsFrame);
  this->R2Label->Create(pvApp);
  this->R2Label->SetText("Resistance 2 [dyn.sec/cm^5]:");
  this->R2Entry->SetParent(this->GetFrame());
  this->R2Entry->SetWidth(10);
  this->R2Entry->Create(pvApp);

  this->CapLabel->SetParent(this->TerminalPropsFrame);
  this->CapLabel->Create(pvApp);
  this->CapLabel->SetText("Capacitance [cm^5/dyn]:");
  this->CapEntry->SetParent(this->GetFrame());
  this->CapEntry->SetWidth(10);
  this->CapEntry->Create(pvApp);

  this->PressureLabel->SetParent(this->TerminalPropsFrame);
  this->PressureLabel->Create(pvApp);
  this->PressureLabel->SetText("Pressure [dyn/cm^2]:");
  this->PressureEntry->SetParent(this->GetFrame());
  this->PressureEntry->SetWidth(10);
  this->PressureEntry->Create(pvApp);

	// Carrega imagem do Terminal
  static const unsigned char image_Terminal_Prop[] =
  "eNrt20sOgzAMRdHuf1XsjEodReSDCYlrP12rAwatymmIsS16HARBEMSu+BQhIzqLl4DrIt"
  "JwgWJPkf22uiRvVaBAgQIlj2qVEicomKCcCYMqMUKeeY8qFWGTp8VYnnx5IIAKu1Jr95QA"
  "qlbEvPx+J3XPvOyp5nG8jv4Bqs4VFLSgQIGKmtwWBKj4VxcoUBqocQfd+6IdAwc76vad47"
  "6s7/0bypIqmx30RIU8wWx+5FG/3zuxuxWxXxLeqDd7amFj65aR5jroLKjmsb16mZpI7N1T"
  "vfyQFOU5LHJL6elQbgWVcQKWsfaTRElW6bTz8VGMyECBAuWAqhNg/N9hYlQSHDU3Kol88z"
  "I+wp1opewP2yfaU09RveGJGEpgTyW6u6n+LYIgCCJUfAFLLO5E";

  vtkKWIcon* icon = vtkKWIcon::New();
  this->TerminalIcon->SetParent(this->GeneralInfoFrame);
  this->TerminalIcon->Create(pvApp);
  icon->SetImage(image_Terminal_Prop,71,78,3,400,16614);
  this->TerminalIcon->SetImageToIcon(icon);
  icon->Delete();

  this->UpdateTerminalPropButton->SetParent(this->TerminalUpdateButtonsFrame);
  this->UpdateTerminalPropButton->Create(pvApp);
  this->UpdateTerminalPropButton->SetText("Update Terminal");
  this->UpdateTerminalPropButton->SetBalloonHelpString("Update this terminal properties values");

  this->UpdateNonLeafTerminalButton->SetParent(this->TerminalUpdateButtonsFrame);
  this->UpdateNonLeafTerminalButton->Create(pvApp);
  this->UpdateNonLeafTerminalButton->SetText("Update Non Leafs");
  this->UpdateNonLeafTerminalButton->SetBalloonHelpString("Update every non leaf terminals from current tree with user specified value");

  this->UpdateLeafTerminalButton->SetParent(this->TerminalUpdateButtonsFrame);
  this->UpdateLeafTerminalButton->Create(pvApp);
  this->UpdateLeafTerminalButton->SetText("Update Leafs");
  this->UpdateLeafTerminalButton->SetBalloonHelpString("Update every leaf terminals from current tree with user specified value");

  this->UpdateTerminalCurves->SetParent(this->GeneralInfoFrame);
  this->UpdateTerminalCurves->Create(pvApp);
  this->UpdateTerminalCurves->SetText("Read functions from file");
  this->UpdateTerminalCurves->GetLoadSaveDialog()->ChooseDirectoryOff();
  this->UpdateTerminalCurves->GetLoadSaveDialog()->SetDefaultExtension("txt");
  this->UpdateTerminalCurves->SetBalloonHelpString("Select a txt file that contains the variant time values for current terminal parameters (R1, R2, Cap and Pr).");


// elementos de interface quando tem-se pressao variante
  this->VariantPressureOptionsFrame->SetParent(this->GetChildFrame()->GetFrame());           
  this->VariantPressureOptionsFrame->Create(pvApp);
  this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
  //this->VariantPressureOptionsFrame->SetAllowFrameToCollapse(0);


  this->FunctionHeartCurve->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->FunctionHeartCurve->Create(pvApp);
  this->FunctionHeartCurve->SetBalloonHelpString("Automatic generation of the curve with function selected");
  this->FunctionHeartCurve->AddValue("None");
  this->FunctionHeartCurve->AddValue("Sen(x)");
  this->FunctionHeartCurve->AddValue("sen^2(x) 1/2 period");
  this->FunctionHeartCurve->AddValue("sen^2(x)");
  this->FunctionHeartCurve->SetValue("None");
  this->FunctionHeartCurve->SetEnabled(0);


  this->AddCurveButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->AddCurveButton->Create(pvApp);
  this->AddCurveButton->SetText("Read Function");
  this->AddCurveButton->GetLoadSaveDialog()->ChooseDirectoryOff();
  this->AddCurveButton->GetLoadSaveDialog()->SetDefaultExtension("txt");
  this->AddCurveButton->SetBalloonHelpString("Read pressure curve from a file (.txt)");
  //this->AddCurveButton->SetCommand(this, "ReadCurve");
  //this->AddCurveButton->SetCommand(this, "UpdateReadCurveButton");

  this->AddCurveButtonOk->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->AddCurveButtonOk->Create(pvApp);
  this->AddCurveButtonOk->SetText("Ok");
  this->AddCurveButtonOk->SetBalloonHelpString("Push Ok to read a curve from the selected file");
  //this->AddCurveButtonOk->SetCommand(this, "ReadCurve");
  this->AddCurveButtonOk->SetEnabled(0);

  this->PointNumber->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->PointNumber->GetWidget()->SetWidth(8);
  this->PointNumber->Create(pvApp);
  this->PointNumber->SetLabelText("Number of Points ");
  //this->PointNumber->GetWidget()->SetCommand(this, "GeneralCallback");

  this->Amplitude->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->Amplitude->GetWidget()->SetWidth(8);
  this->Amplitude->Create(pvApp);
  this->Amplitude->SetLabelText("Curve Amplitude");
  //this->Amplitude->GetWidget()->SetCommand(this, "GeneralCallback");



  this->FinalTime->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->FinalTime->GetWidget()->SetWidth(8);
  this->FinalTime->Create(pvApp);
  this->FinalTime->SetLabelText("Final Time ");
  //this->FinalTime->GetWidget()->SetCommand(this, "GeneralCallback");

  this->MaxValue->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->MaxValue->GetWidget()->SetWidth(8);
  this->MaxValue->Create(pvApp);
  this->MaxValue->SetLabelText("Max Value ");
  //this->MaxValue->GetWidget()->SetCommand(this, "GeneralCallback");

  this->MinValue->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->MinValue->GetWidget()->SetWidth(8);
  this->MinValue->Create(pvApp);
  this->MinValue->SetLabelText("Min Value ");
  //this->MinValue->GetWidget()->SetCommand(this, "GeneralCallback");

  this->PlotButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->PlotButton->Create(pvApp);
  this->PlotButton->SetText("Plot");
  //this->PlotButton->SetEnabled(0);
 // this->PlotButton->SetCommand(this, "ShowPressurePlot");


  this->UpdateCurveButton->SetParent(this->VariantPressureOptionsFrame->GetFrame());
  this->UpdateCurveButton->Create(pvApp);
  this->UpdateCurveButton->SetText("Update Curve");
 // this->UpdateCurveButton->SetCommand(this, "UpdatePressureCurve");

  this->UpdateR1Curve->SetParent(this->TerminalPropsFrame);
  this->UpdateR1Curve->Create(pvApp);
  this->UpdateR1Curve->SetText("Time Variant");
//  this->UpdateR1Curve->SetBalloonHelpString("Update this terminal properties values");



  this->UpdateR2Curve->SetParent(this->TerminalPropsFrame);
  this->UpdateR2Curve->Create(pvApp);
  this->UpdateR2Curve->SetText("Time Variant");
//  this->UpdateR2Curve->SetBalloonHelpString("Update this terminal properties values");


  this->UpdateCapCurve->SetParent(this->TerminalPropsFrame);
  this->UpdateCapCurve->Create(pvApp);
  this->UpdateCapCurve->SetText("Time Variant");
//  this->UpdateCapCurve->SetBalloonHelpString("Update this terminal properties values");


  this->UpdatePCurve->SetParent(this->TerminalPropsFrame);
  this->UpdatePCurve->Create(pvApp);
  this->UpdatePCurve->SetText("Time Variant");
//  this->UpdatePCurve->SetBalloonHelpString("Update this terminal properties values");


}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::PlaceComponents(vtkPVHMStraightModelWidget* mainWidget)
{
  int mode = mainWidget->GetWidgetMode();

  // Se visibility for 0, sai da função
  if(!this->GetWidgetVisibility())
    {
    mainWidget->Script("grid forget %s",
        this->GetChildFrame()->GetWidgetName());
    return;
    }
  this->GetChildFrame()->ExpandFrame();

  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GetChildFrame()->GetWidgetName());

  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->GeneralInfoFrame->GetWidgetName());
  
  mainWidget->Script("grid %s -sticky ew -columnspan 4",
    this->TerminalPropsFrame->GetWidgetName());

  mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news -columnspan 4",
    this->TerminalIcon->GetWidgetName());

   //mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky w",
    mainWidget->Script("grid %s - -sticky news -padx 2 -pady 2 -columnspan 4",
    this->UpdateTerminalCurves->GetWidgetName()
    );

   mainWidget->Script("grid %s %s %s - -padx 2 -pady 2 -sticky news",
    this->R1Label->GetWidgetName(),
    this->R1Entry->GetWidgetName(),
    this->UpdateR1Curve->GetWidgetName());

  mainWidget->Script("grid %s %s %s - -padx 2 -pady 2 -sticky news",
    this->R2Label->GetWidgetName(),
    this->R2Entry->GetWidgetName(),
    this->UpdateR2Curve->GetWidgetName());

  mainWidget->Script("grid %s %s %s - -padx 2 -pady 2 -sticky news",
    this->CapLabel->GetWidgetName(),
    this->CapEntry->GetWidgetName(),
    this->UpdateCapCurve->GetWidgetName());

  mainWidget->Script("grid %s %s %s - -padx 2 -pady 2 -sticky news",
    this->PressureLabel->GetWidgetName(),
    this->PressureEntry->GetWidgetName(),
    this->UpdatePCurve->GetWidgetName());

  mainWidget->Script("grid %s %s - -padx 2 -pady 2 -sticky news",
    this->meshIDLabel->GetWidgetName(),
    this->meshIDEntry->GetWidgetName());

  mainWidget->Script("grid %s %s - -padx 2 -pady 2 -sticky news",
    this->AssNodesLabel->GetWidgetName(),
    this->AssNodes->GetWidgetName());

  if(mode) // visualizacao
    {
    mainWidget->Script("grid remove %s %s",
    this->TerminalUpdateButtonsFrame->GetWidgetName(),
    this->UpdateTerminalPropButton->GetWidgetName());
//    this->UpdateNonLeafTerminalButton->GetWidgetName(),
//    this->UpdateLeafTerminalButton->GetWidgetName());
    
    this->VariantPressureOptionsFrame->CollapseFrame();
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    this->VariantPressureOptionsFrame->SetEnabled(0);
    }
  else // edicao
    {
    mainWidget->Script("grid %s -sticky news -columnspan 4",
    this->TerminalUpdateButtonsFrame->GetWidgetName());
    mainWidget->Script("grid %s - -sticky news -padx 2 -pady 2 -columnspan 4",
    this->UpdateTerminalPropButton->GetWidgetName());
//    this->UpdateNonLeafTerminalButton->GetWidgetName(),
//    this->UpdateLeafTerminalButton->GetWidgetName());
    
    this->VariantPressureOptionsFrame->CollapseFrame();
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    }
  
  this->R1Entry->SetBalloonHelpString("Resistive element R1 from the Windkessel model.");
  this->R2Entry->SetBalloonHelpString("Resistive element R2 from the Windkessel model.");
  this->CapEntry->SetBalloonHelpString("Capacitive element C from the Windkessel model.");
  this->PressureEntry->SetBalloonHelpString("Terminal pressure Pt.");


 //*****************************************************************
 //  VariantPressureOptionsFrame

   mainWidget->Script("grid %s -sticky news -columnspan 4",
    this->VariantPressureOptionsFrame->GetWidgetName());


   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->AddCurveButton->GetWidgetName());

   //mainWidget->Script("grid %s %s - -padx 2 -pady 2 -sticky news",

   //   mainWidget->Script("grid %s %s - -sticky news -padx 2 -pady 2",
//    this->FunctionHeartCurve->GetWidgetName(),
//    this->AddCurveButtonOk->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->PointNumber->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->Amplitude->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->FinalTime->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->MaxValue->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->MinValue->GetWidgetName());

   mainWidget->Script("grid %s - -padx 2 -pady 2 -sticky news",
    this->PlotButton->GetWidgetName());
    //this->UpdateCurveButton->GetWidgetName());

}

//----------------------------------------------------------------------------
vtkKWFrameWithLabel* vtkPVHMTerminalWidget::GetTerminalFrame()
{
  return this->GetChildFrame();
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::Initialize()
{
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::Accept()
{
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetR1(const char *values)
{
  this->R1Entry->SetValue(values);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetR2(const char *values)
{
  this->R2Entry->SetValue(values);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetCap(const char *values)
{
  this->CapEntry->SetValue(values);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetP(const char *values)
{
  this->PressureEntry->SetValue(values);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetR1(double value)
{
  if (!value) // para lidar com bug do vtkKWEntry que quando valor a ser escrito é ZERO o mesmo não apaga o conteudo anterior do entry
    {
    s.clear();
    s.append("0");
    this->R1Entry->SetValue(s.c_str());
    }
  else
    this->R1Entry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetR2(double value)
{
  if (!value) // para lidar com bug do vtkKWEntry que quando valor a ser escrito é ZERO o mesmo não apaga o conteudo anterior do entry
    {
    s.clear();
    s.append("0");
    this->R2Entry->SetValue(s.c_str());
    }
  else
    this->R2Entry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetCap(double value)
{
  if (!value) // para lidar com bug do vtkKWEntry que quando valor a ser escrito é ZERO o mesmo não apaga o conteudo anterior do entry
    {
    s.clear();
    s.append("0.0");
    this->CapEntry->SetValue(s.c_str());
    }
  else
    this->CapEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetP(double value)
{
  if (!value) // para lidar com bug do vtkKWEntry que quando valor a ser escrito é ZERO o mesmo não apaga o conteudo anterior do entry
    {
    s.clear();
    s.append("0.0");
    this->PressureEntry->SetValue(s.c_str());
    }
  else
    this->PressureEntry->SetValueAsDouble(value);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetTerminalLabel(char *text)
{
  this->GetChildFrame()->SetLabelText(text);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetmeshID(int meshID)
{
  this->meshIDEntry->SetValueAsDouble(meshID);
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::SetAssNodes(const char *AssNodesList)
{
//int x = AssNodesList.find(";");
//while (x < string::npos)
//	{
//	AssNodesList.replace(x,1," ");
//	x = AssNodesList.find(";", x+1);
//	}
//char temp [50];
//	for(int i=0; i<50 ;i++)
//		{
//		if(AssNodesList[i] != '\0')
//		sprintf(temp[i], "%s ", AssNodesList[i]);
//		cout << "Temp: " << temp[i] << endl;
//		}
  this->AssNodes->SetText(AssNodesList);
}

//----------------------------------------------------------------------------
double vtkPVHMTerminalWidget::GetR1Single(void)
{
  return this->R1Entry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMTerminalWidget::GetR2Single(void)
{
  return this->R2Entry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMTerminalWidget::GetCapSingle(void)
{
  return this->CapEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

double vtkPVHMTerminalWidget::GetPSingle(void)
{
  return this->PressureEntry->GetValueAsDouble();
}       

//----------------------------------------------------------------------------

double vtkPVHMTerminalWidget::GetmeshID(void)
{
  return this->meshIDEntry->GetValueAsDouble();
}

//----------------------------------------------------------------------------

//double vtkPVHMTerminalWidget::GetAssNodes(void)
//{
//	return this->AssNodesEntry->GetValueAsDouble();
//}

//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::ReadCurve()
{
  int result;

  char *FileName = this->UpdateTerminalCurves->GetFileName();

  vtkHM1DHeartCurveReader *reader = vtkHM1DHeartCurveReader::New();

  result = reader->ReadFile(FileName);

  if ( !result )
    return;

  //char *str = this->AddCurveButton->GetText();

  //Pegar os parametros lidos do arquivo
  vtkDoubleArray *r1 = reader->GetR1();
  vtkDoubleArray *r2 = reader->GetR2();
  vtkDoubleArray *c  = reader->GetC();
  vtkDoubleArray *p  = reader->GetP();


  this->R1->DeepCopy(r1);
  this->R2->DeepCopy(r2);
  this->Cap->DeepCopy(c);
  this->P->DeepCopy(p);

  vtkDoubleArray *r1Time = reader->GetR1Time();
  vtkDoubleArray *r2Time = reader->GetR2Time();
  vtkDoubleArray *cTime  = reader->GetCTime();
  vtkDoubleArray *pTime  = reader->GetPTime();


  this->R1Time->DeepCopy(r1Time);
  this->R2Time->DeepCopy(r2Time);
  this->CapTime->DeepCopy(cTime);
  this->PTime->DeepCopy(pTime);

  this->R1Entry->SetValue("Function");
  this->R2Entry->SetValue("Function");
  this->CapEntry->SetValue("Function");
  this->PressureEntry->SetValue("Function");


  reader->Delete();

  this->UnselectButtons();
  this->VariantPressureOptionsFrame->CollapseFrame();


//	return array;
}

//----------------------------------------------------------------------------
void vtkPVHMTerminalWidget::EnableR1CurveEdition()
{
  if (!this->GetSelectedTerminalParam()) // se clicou no radio button que já estava selecionado
    {
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    this->VariantPressureOptionsFrame->CollapseFrame();
    return;
    }


  this->VariantPressureOptionsFrame->SetEnabled(1);
  this->VariantPressureOptionsFrame->SetLabelText("R1 Time Variant Parameters");
  this->VariantPressureOptionsFrame->ExpandFrame();
  this->ManageCheckButtons(0);
  this->EraseParam();
}

//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::EnableR2CurveEdition()
{
  if (!this->GetSelectedTerminalParam()) // se clicou no radio button que já estava selecionado
    {
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    this->VariantPressureOptionsFrame->CollapseFrame();
    return;
    }

  this->VariantPressureOptionsFrame->SetEnabled(1);  
  this->VariantPressureOptionsFrame->SetLabelText("R2 Time Variant Parameters");
  this->VariantPressureOptionsFrame->ExpandFrame();
  this->ManageCheckButtons(1);
  this->EraseParam();
}
//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::EnableCapCurveEdition()
{
  if (!this->GetSelectedTerminalParam()) // se clicou no radio button que já estava selecionado
    {
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    this->VariantPressureOptionsFrame->CollapseFrame();
    return;
    }


  this->VariantPressureOptionsFrame->SetEnabled(1);  
  this->VariantPressureOptionsFrame->SetLabelText("Cap Time Variant Parameters");
  this->VariantPressureOptionsFrame->ExpandFrame();
  this->ManageCheckButtons(2);
  this->EraseParam();
}
//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::EnablePCurveEdition()
{
  if (!this->GetSelectedTerminalParam()) // se clicou no radio button que já estava selecionado
    {
    this->VariantPressureOptionsFrame->SetLabelText("Time Variant Parameters");
    this->VariantPressureOptionsFrame->CollapseFrame();
    return;
    }

  this->VariantPressureOptionsFrame->SetEnabled(1);  
  this->VariantPressureOptionsFrame->SetLabelText("Pressure Time Variant Parameters");
  this->VariantPressureOptionsFrame->ExpandFrame();

  this->ManageCheckButtons(3);
  this->EraseParam();
}
//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::ReadCurveSingleParam()
{
  int result;
  char *FileName;
  FileName = this->AddCurveButton->GetFileName();

  //vtkHM1DHeartCurveReader *reader = vtkHM1DHeartCurveReader::New();

  result = reader->ReadFile(FileName);
  char *str = this->AddCurveButton->GetText();


  if (reader->GetSingleCurve()) // se leitor le apenas arquivo com uma curva só
    {
    if (this->GetSelectedTerminalParam() == 1)
      {
      this->R1->DeepCopy(reader->GetGeneric());
      this->R1Time->DeepCopy(reader->GetGenericTime());
      strcpy(R1_ReadFile, this->AddCurveButton->GetText());
      this->R1Entry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 2)
      {
      this->R2->DeepCopy(reader->GetGeneric());
      this->R2Time->DeepCopy(reader->GetGenericTime());
      strcpy(R2_ReadFile, this->AddCurveButton->GetText());
      this->R2Entry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 3)
      {
      this->Cap->DeepCopy(reader->GetGeneric());
      this->CapTime->DeepCopy(reader->GetGenericTime());
      strcpy(Cap_ReadFile, this->AddCurveButton->GetText());
      this->CapEntry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 4)
      {
      this->P->DeepCopy(reader->GetGeneric());
      this->PTime->DeepCopy(reader->GetGenericTime());
      strcpy(P_ReadFile, this->AddCurveButton->GetText());
      this->PressureEntry->SetValue("Function");
      }
    }
  else
    {
    if (this->GetSelectedTerminalParam() == 1)
      {
      this->R1->DeepCopy(reader->GetR1());
      this->R1Time->DeepCopy(reader->GetR1Time());
      strcpy(R1_ReadFile, this->AddCurveButton->GetText());
      this->R1Entry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 2)
      {
      this->R2->DeepCopy(reader->GetR2());
      this->R2Time->DeepCopy(reader->GetR2Time());
      strcpy(R2_ReadFile, this->AddCurveButton->GetText());
      this->R2Entry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 3)
      {
      this->Cap->DeepCopy(reader->GetC());
      this->CapTime->DeepCopy(reader->GetCTime());
      strcpy(Cap_ReadFile, this->AddCurveButton->GetText());
      this->CapEntry->SetValue("Function");
      }

    if (this->GetSelectedTerminalParam() == 4)
      {
      this->P->DeepCopy(reader->GetP());
      this->PTime->DeepCopy(reader->GetPTime());
      strcpy(P_ReadFile, this->AddCurveButton->GetText());
      this->PressureEntry->SetValue("Function");
      }
    }

  vtkDoubleArray *G, *GTime;

  if (this->GetSelectedTerminalParam() == 1)
    {
    G = this->R1;
    GTime = this->R1Time;
    }

  if (this->GetSelectedTerminalParam() == 2)
    {
    G = this->R2;
    GTime = this->R2Time;
    }

  if (this->GetSelectedTerminalParam() == 3)
    {
    G = this->Cap;
    GTime = this->CapTime;
    }

  if (this->GetSelectedTerminalParam() == 4)
    {
    G = this->P;
    GTime = this->PTime;
    }


  //Pegar os parametros lidos do arquivo
//	vtkDoubleArray *p  = reader->GetP();
//	vtkDoubleArray *pTime  = reader->GetPTime();
//	//vtkDoubleArray *array = vtkDoubleArray::New();

//	if (!this->CurveFromFile)
//		this->CurveFromFile = vtkDoubleArray::New();
//

//	this->CurveFromFile->SetNumberOfComponents(2);
//	this->CurveFromFile->SetNumberOfTuples(p->GetNumberOfTuples());
//
  double Maximum = 0;
  double Minimum = 0;
  double Time = 0;

  for (int i=0; i< G->GetNumberOfTuples(); i++)
    {
    if (G->GetValue(i) > Maximum)
      Maximum = G->GetValue(i);

    if (G->GetValue(i) < Minimum)
      Minimum = G->GetValue(i);

    if (GTime->GetValue(i) > Time)
      Time = GTime->GetValue(i);

    //this->CurveFromFile->SetTuple2(i, pTime->GetValue(i), p->GetValue(i));
    }
  this->MaxValue->GetWidget()->SetValueAsDouble(Maximum);
  this->MinValue->GetWidget()->SetValueAsDouble(Minimum);
  this->PointNumber->GetWidget()->SetValueAsInt(G->GetNumberOfTuples());
  this->FinalTime->GetWidget()->SetValueAsDouble(Time);
  //reader->Delete();

  //this->ReadPressureCurveFromFile = true;

}

//----------------------------------------------------------------------------


void vtkPVHMTerminalWidget::ShowPressurePlot()
{
  this->XYPlotWidget->SetMaxFlow(this->MaxValue->GetWidget()->GetValueAsDouble());

  this->XYPlotWidget->SetMaxFlow(this->MaxValue->GetWidget()->GetValueAsDouble());

  this->XYPlotWidget->SetMinFlow(this->MinValue->GetWidget()->GetValueAsDouble());

  this->XYPlotWidget->SetEditCurveHeart(0);
  this->XYPlotWidget->SetTMin(0);
  this->XYPlotWidget->SetTMax(this->FinalTime->GetWidget()->GetValueAsDouble() );

  vtkDoubleArray *G, *GTime;
  //char Title[100];

  if (this->GetSelectedTerminalParam() == 1)
    {
    G = this->R1;
    GTime = this->R1Time;
    title.clear();
    ylegend.clear();
    title.append("Resistance 1 X Time"); 
    ylegend.append("R1\n[dyn.sec/cm^5]");
    }
  else if (this->GetSelectedTerminalParam() == 2)
    {
    G = this->R2;
    GTime = this->R2Time;
    title.clear();
    ylegend.clear();
    title.append("Resistance 2 X Time");
    ylegend.append("R2\n[dyn.sec/cm^5]");
    }
  else if (this->GetSelectedTerminalParam() == 3)
    {
    G = this->Cap;
    GTime = this->CapTime;
    title.clear();
    ylegend.clear();
    title.append("Capacitance X Time");
    ylegend.append("Cap.\n[cm^5/dyn]");
    }
  else if (this->GetSelectedTerminalParam() == 4)
    {
    G = this->P;
    GTime = this->PTime;
    title.clear();
    ylegend.clear();
    title.append("Pressure X Time");
    ylegend.append("Pr\n[dyn/cm^2]");
    }
  else
    return;

  this->XYPlotWidget->SetTitle(title.c_str());
  this->XYPlotWidget->BuildHeartPlot(1, G, GTime);
  
  this->XYPlotWidget->GetKWPiecewiseFunctionEditor()->SetLabelText(title.c_str());
  vtkKWRange *kwRangeValue =  this->XYPlotWidget->GetKWPiecewiseFunctionEditor()->GetValueRange();
  kwRangeValue->SetLabelText(ylegend.c_str());
  this->XYPlotWidget->ShowHMXYPlotWidget();
}

//----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::EraseParam()
{
  double Maximum = 0;
  double Minimum = 0;
  double Time = 0;


  vtkDoubleArray *G, *GTime;


  if (this->GetSelectedTerminalParam() == 1)
    {
    G = this->R1;
    GTime = this->R1Time;
    //this->AddCurveButton->SetText(R1_ReadFile);
    }

  if (this->GetSelectedTerminalParam() == 2)
    {
    G = this->R2;
    GTime = this->R2Time;
    //this->AddCurveButton->SetText(R2_ReadFile);
    }

  if (this->GetSelectedTerminalParam() == 3)
    {
    G = this->Cap;
    GTime = this->CapTime;
    //this->AddCurveButton->SetText(Cap_ReadFile);
    }

  if (this->GetSelectedTerminalParam() == 4)
    {
    G = this->P;
    GTime = this->PTime;
    //this->AddCurveButton->SetText(P_ReadFile);
    }

  if (!this->GetSelectedTerminalParam())
          return;

  for (int i=0; i< G->GetNumberOfTuples(); i++)
    {
    if (G->GetValue(i) > Maximum)
      Maximum = G->GetValue(i);

    if (G->GetValue(i) < Minimum)
      Minimum = G->GetValue(i);

    if (GTime->GetValue(i) > Time)
      Time = GTime->GetValue(i);

    //this->CurveFromFile->SetTuple2(i, pTime->GetValue(i), p->GetValue(i));
    }
  this->MaxValue->GetWidget()->SetValueAsDouble(Maximum);
  this->MinValue->GetWidget()->SetValueAsDouble(Minimum);
  this->PointNumber->GetWidget()->SetValueAsInt(G->GetNumberOfTuples());
  this->FinalTime->GetWidget()->SetValueAsDouble(Time);



//	this->MaxValue->GetWidget()->SetValueAsDouble(0.0);
//	this->MinValue->GetWidget()->SetValueAsDouble(0.0);
//	this->PointNumber->GetWidget()->SetValueAsInt(0);
//	this->FinalTime->GetWidget()->SetValueAsDouble(0.0);
//	this->AddCurveButton->SetText("Read Function");
}


int vtkPVHMTerminalWidget::GetSelectedTerminalParam()
{
  if (this->UpdateR1Curve->GetSelectedState())
    return 1;
  
  if (this->UpdateR2Curve->GetSelectedState())
    return 2;
  
  
  if (this->UpdateCapCurve->GetSelectedState())
    return 3;
  
  if (this->UpdatePCurve->GetSelectedState())
    return 4;
  
  return 0;
}

//----------------------------------------------------------------------------

//
void vtkPVHMTerminalWidget::ManageCheckButtons(int ButtonNumber)
{
  for (int i = 0; i < 4; ++i)
    {
    if (ButtonNumber == i)
      {
      //cout << "Current Button "<< endl;//this->CheckButtonsArray[ButtonNumber]->SetSelectedState(0);
      }
    else
      this->CheckButtonsArray[i]->SetSelectedState(0);
    }
}

//----------------------------------------------------------------------------


void vtkPVHMTerminalWidget::CalculateCurve()
{
  double f;
  double n;
  double x = 0;
  double Amplitude = this->Amplitude->GetWidget()->GetValueAsDouble();
  double time = this->FinalTime->GetWidget()->GetValueAsDouble();

  //int CurrentCover = this->GroupSelectionCombo->GetValueAsInt() - this->NumberOfWallGroups;

  int select = this->FunctionHeartCurve->GetValueIndex(this->FunctionHeartCurve->GetValue());

  //int select = this->GetFunctionSelected();

  int numberOfPoints = this->PointNumber->GetWidget()->GetValueAsInt();


  if ( numberOfPoints < 2 )
    numberOfPoints = 2;

  //Calcula o intervalo de tempo entre um ponto e outro
  n = time/(numberOfPoints-1);

  vtkDoubleArray *G, *GTime;


  if (this->GetSelectedTerminalParam() == 1)
    {
    G = this->R1;
    GTime = this->R1Time;
    }

  if (this->GetSelectedTerminalParam() == 2)
    {
    G = this->R2;
    GTime = this->R2Time;
    }

  if (this->GetSelectedTerminalParam() == 3)
    {
    G = this->Cap;
    GTime = this->CapTime;
    }

  if (this->GetSelectedTerminalParam() == 4)
    {
    G = this->P;
    GTime = this->PTime;
    }




  //G->SetNumberOfComponents(2);
  G->SetNumberOfTuples(numberOfPoints);
  GTime->SetNumberOfTuples(numberOfPoints);

  switch(select)
    {
    /*
     * Caso nenhuma das funções esteja selecionada, adiciona dois pontos na curva.
     */
    case 0:

//			if (this->AddCurveButton->GetFileName() ||  this->PressureCurveVector[CurrentCover]->GetNumberOfTuples()    ) // se alguma funcao é lida do disco
//				{
//
//				//if (this->PressureCurveVector[CurrentCover]->GetNumberOfTuples() == 1)
//				if (this->ReadPressureCurveFromFile) // se curva foi lida de arquivo
//					{
//					for (int i=0; i< this->CurveFromFile->GetNumberOfTuples(); i++)
//						this->PressureCurveVector[CurrentCover]->SetTuple2(i, this->CurveFromFile->GetTuple2(i)[0], this->CurveFromFile->GetTuple2(i)[1]);
//
//					this->ReadPressureCurveFromFile = false;
//					}
//				break;
//				}
//			else
//				{
//				this->PressureCurveVector[CurrentCover]->SetNumberOfTuples(2);
//				this->PressureCurveVector[CurrentCover]->SetTuple2(0, 0, 0);
//				this->PressureCurveVector[CurrentCover]->SetTuple2(1, time, 0);
//				break;
//				}

    case 1:
      /*
       * Calculate function sen(x)
       * A = Amplitude
       * T = period
       * x = time instant
       *
       * f=A*sin(2*(pi/T)*x)
       */
      Amplitude = Amplitude/2;
      for ( int i=0; i<numberOfPoints; i++ )
        {
        f = Amplitude * sin(2*(vtkMath::Pi() / time ) * x);
        //f = f + this->SurfaceRefPressureEntry->GetValueAsDouble();
        //G->SetTuple2(i, x, f);
        G->SetValue(i, f);
        GTime->SetValue(i, x);

        x += n;
        }
        break;

      case 2:
        /*
         * Calculate function sen^2 (x) 1/2 period
         * * A = Amplitude
         * T = period
         * x = time instant
         *
         * f=A*(sin((pi/T)*x))^2
         */
        for ( int i=0; i<numberOfPoints; i++ )
          {
          f = Amplitude * pow((sin((vtkMath::Pi() / time ) * x)),2);
          //f = f+ this->SurfaceRefPressureEntry->GetValueAsDouble();
          //this->PressureCurveVector[CurrentCover]->SetTuple2(i, x, f);

          G->SetValue(i, f);
          GTime->SetValue(i, x);

          x += n;
          }
          break;

      case 3:
              /*
               * Calculate function sen^2 (x)
               * * A = Amplitude
               * T = period
               * x = time instant
               *
               * f=A*(sin(2*(pi/T)*x))^2
               */
              for ( int i=0; i<numberOfPoints; i++ )
                      {
                      f = Amplitude * pow((sin(2*(vtkMath::Pi() / time ) * x)),2);
                      //f = f + this->SurfaceRefPressureEntry->GetValueAsDouble();
                      //this->PressureCurveVector[CurrentCover]->SetTuple2(i, x, f);
                      G->SetValue(i, f);
                      GTime->SetValue(i, x);
    
    
                      x += n;
                      }
              break;

      default:

      
    break;

    }
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetR1(vtkDoubleArray *array)
{
  this->R1->DeepCopy(array);
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetR2(vtkDoubleArray *array)
{
  this->R2->DeepCopy(array);
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetCap(vtkDoubleArray *array)
{
  this->Cap->DeepCopy(array);
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetP(vtkDoubleArray *array)
{
  this->P->DeepCopy(array);
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetR1Time(vtkDoubleArray *array)
{
  this->R1Time->DeepCopy(array);
}

// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetR2Time(vtkDoubleArray *array)
{
  this->R2Time->DeepCopy(array);
}
// ----------------------------------------------------------------------------


void vtkPVHMTerminalWidget::SetCapTime(vtkDoubleArray *array)
{
  this->CapTime->DeepCopy(array);
}
// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::SetPTime(vtkDoubleArray *array)
{
  this->PTime->DeepCopy(array);
}
// ----------------------------------------------------------------------------

void vtkPVHMTerminalWidget::UnselectButtons()
{
  for (int i = 0; i < 4; ++i)
    this->CheckButtonsArray[i]->SetSelectedState(0);

}
// ----------------------------------------------------------------------------

short int vtkPVHMTerminalWidget::SomeFunctionIsSelectd()
{
  if (!strcmp(this->R1Entry->GetValue(), "Function"))
    return 1;

  if (!strcmp(this->R2Entry->GetValue(), "Function"))
    return 2;
    
  if (!strcmp(this->CapEntry->GetValue(), "Function"))
    return 3;
	  
  if (!strcmp(this->PressureEntry->GetValue(), "Function"))
    return 4;
  
  // se nenhum dos parametros esta setado para valores variantes
  return 0;
}

// ----------------------------------------------------------------------------

vtkDoubleArray *vtkPVHMTerminalWidget::GetTimeArray(int ParamNumber)
{
  if (ParamNumber == 1)
    return this->R1Time;

  if (ParamNumber == 2)
    return this->R2Time;

  if (ParamNumber == 3)
    return this->CapTime;

  if (ParamNumber == 4)
    return this->PTime;
}

// ----------------------------------------------------------------------------
