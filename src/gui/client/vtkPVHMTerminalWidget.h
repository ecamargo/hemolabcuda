/*
 * $Id: vtkPVHMTerminalWidget.h 362 2006-05-15 19:55:40Z rodrigo $
 */

// .NAME vtkPVHMTerminalWidget
// .SECTION Description
// Creates KW Components for the terminals



#ifndef _vtkPVHMTerminalWidget_h_
#define _vtkPVHMTerminalWidget_h_

#include "vtkObjectFactory.h"
#include "vtkPVHMObjectWidget.h"
#include "vtkKWPushButton.h"
#include <string>
using namespace std;

class vtkKWEntry;
class vtkKWLabel;
class vtkKWFrame;
class vtkKWFrameWithLabel;
class vtkSMSourceProxy;
class vtkPVHMStraightModelWidget;
class vtkKWLoadSaveButton;
class vtkDoubleArray;
class vtkKWEntryWithLabel;
class vtkKWComboBox;
class vtkKWHM1DXYPlotWidget;
class vtkHM1DHeartCurveReader;
class vtkKWRadioButton;
class vtkKWCheckButton;

class VTK_EXPORT vtkPVHMTerminalWidget : public vtkPVHMObjectWidget
{
public:
  static vtkPVHMTerminalWidget *New();
  vtkTypeRevisionMacro(vtkPVHMTerminalWidget,vtkPVHMObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};

  // Description:
  // Updates Widgets Labels
	void UpdateWidgetInfo();

	// Description:
  // Call TCL/TK commands to place KW Components
	void PlaceComponents(vtkPVHMStraightModelWidget* );

  // Description:
  // Call creation on the child.
  virtual void ChildCreate(vtkPVApplication*);

  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:
  // Initialize widget after creation
  virtual void Initialize();

  // Description:
  // Get Terminal Frame to enable collapse and expand features
  vtkKWFrameWithLabel* GetTerminalFrame();

  // Description:
  // methods used to update the value of terminal related widgets
  void SetR1(const char *values);
  void SetR2(const char *values);
  void SetCap(const char *values);
  void SetP(const char *values);
  void SetmeshID(int meshID);
  void SetAssNodes(const char *AssNodes);

  // Description:
  // Overload of the previous methods used to update the value of terminal related widgets
  void SetR1(double value);
  void SetR2(double value);
  void SetCap(double value);
  void SetP(double value);
  void SetTerminalLabel(char *text);
  // Description:
  // returns the current values from the termial entries
  double GetR1Single(void);
  double GetR2Single(void);
  double GetCapSingle(void);
  double GetPSingle(void);
  double GetmeshID(void);
	//  double GetAssNodes(void);


  vtkGetObjectMacro(UpdateTerminalPropButton, vtkKWPushButton);

  vtkGetObjectMacro(UpdateNonLeafTerminalButton, vtkKWPushButton);

  vtkGetObjectMacro(UpdateLeafTerminalButton, vtkKWPushButton);

  vtkGetObjectMacro(UpdateTerminalCurves, vtkKWLoadSaveButton);

  void ReadCurve();


  vtkGetObjectMacro(UpdateR1Curve, vtkKWCheckButton);
  vtkGetObjectMacro(UpdateR2Curve, vtkKWCheckButton);
  vtkGetObjectMacro(UpdateCapCurve, vtkKWCheckButton);
  vtkGetObjectMacro(UpdatePCurve, vtkKWCheckButton);

  void EnableR1CurveEdition();
  void EnableR2CurveEdition();
  void EnableCapCurveEdition();
  void EnablePCurveEdition();

  vtkGetObjectMacro(PlotButton, vtkKWPushButton);
  vtkGetObjectMacro(AddCurveButton, vtkKWLoadSaveButton);
  vtkGetObjectMacro(AddCurveButtonOk, vtkKWPushButton);

  void ReadCurveSingleParam();
  void ShowPressurePlot();
  void EraseParam();
  int GetSelectedTerminalParam();
  void ManageCheckButtons(int ButtonNumber);
  void CalculateCurve();

  vtkGetObjectMacro(R1, vtkDoubleArray);
  vtkGetObjectMacro(R2, vtkDoubleArray);
  vtkGetObjectMacro(Cap, vtkDoubleArray);
  vtkGetObjectMacro(P, vtkDoubleArray);

  vtkGetObjectMacro(R1Time, vtkDoubleArray);
  vtkGetObjectMacro(R2Time, vtkDoubleArray);
  vtkGetObjectMacro(CapTime, vtkDoubleArray);
  vtkGetObjectMacro(PTime, vtkDoubleArray);

  void SetR1(vtkDoubleArray *array);
  void SetR2(vtkDoubleArray *array);
  void SetCap(vtkDoubleArray *array);
  void SetP(vtkDoubleArray *array);
  void SetR1Time(vtkDoubleArray *array);
  void SetR2Time(vtkDoubleArray *array);
  void SetCapTime(vtkDoubleArray *array);
  void SetPTime(vtkDoubleArray *array);

  void UnselectButtons();
	
  vtkGetObjectMacro(R1Entry, vtkKWEntry);
  vtkGetObjectMacro(R2Entry, vtkKWEntry);
  vtkGetObjectMacro(CapEntry, vtkKWEntry);
  vtkGetObjectMacro(PressureEntry, vtkKWEntry);

  short int SomeFunctionIsSelectd();
  
  // return the time array for some selected terminal parameter
  vtkDoubleArray *GetTimeArray(int ParamNumber);

  //******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************

protected:
  vtkPVHMTerminalWidget();
  virtual ~vtkPVHMTerminalWidget();

	// Description:
	// Terminal Widget
	// Resistance 1, Resistence2, Capacitance and Pressure Widgets
  vtkKWLabel* R1Label;
  vtkKWEntry* R1Entry;
  vtkKWLabel* R2Label;
  vtkKWEntry* R2Entry;
  vtkKWLabel* CapLabel;
  vtkKWEntry* CapEntry;
  vtkKWLabel* PressureLabel;
  vtkKWEntry* PressureEntry;

  vtkKWLabel* TerminalIcon;

  vtkKWLabel* meshIDLabel;
  vtkKWEntry* meshIDEntry;

  vtkKWLabel* AssNodesLabel;
  vtkKWLabel* AssNodes;

  vtkKWPushButton *UpdateTerminalPropButton;

  vtkKWPushButton *UpdateNonLeafTerminalButton;
  vtkKWPushButton *UpdateLeafTerminalButton;

  vtkKWLoadSaveButton *UpdateTerminalCurves;


  //**** elementos da interface quando pressao variante
  vtkKWFrameWithLabel *VariantPressureOptionsFrame;
  vtkKWComboBox *FunctionHeartCurve;
  vtkKWLoadSaveButton *AddCurveButton;
  vtkKWPushButton *AddCurveButtonOk;
  vtkKWEntryWithLabel *PointNumber;
  vtkKWEntryWithLabel *Amplitude;

  vtkKWEntryWithLabel *FinalTime;
  vtkKWEntryWithLabel *MaxValue;
  vtkKWEntryWithLabel *MinValue;
  vtkKWPushButton *PlotButton;
  vtkKWPushButton *UpdateCurveButton;

  //**********************

  //vtkKWPushButton *UpdateR1Curve;

  vtkKWCheckButton *UpdateR1Curve;

  vtkKWCheckButton *UpdateR2Curve;
  vtkKWCheckButton *UpdateCapCurve;
  vtkKWCheckButton *UpdatePCurve;

  vtkKWFrame *TerminalPropsFrame;

  vtkKWFrame *TerminalUpdateButtonsFrame;

  vtkKWHM1DXYPlotWidget *XYPlotWidget;

  vtkHM1DHeartCurveReader *reader;


  vtkDoubleArray *R1;
  vtkDoubleArray *R1Time;

  vtkDoubleArray *R2;
  vtkDoubleArray *R2Time;

  vtkDoubleArray *Cap;
  vtkDoubleArray *CapTime;

  vtkDoubleArray *P;
  vtkDoubleArray *PTime;


  vtkKWCheckButton *CheckButtonsArray[4];

  char R1_ReadFile[20];
  char R2_ReadFile[20];
  char Cap_ReadFile[20];
  char P_ReadFile[20];
  string title;
  string ylegend;
  
  // general use string
  string s;
  
  
  vtkKWFrame *GeneralInfoFrame;


private:
  vtkPVHMTerminalWidget(const vtkPVHMTerminalWidget&); // Not implemented
  void operator=(const vtkPVHMTerminalWidget&); // Not implemented
};

#endif  /*_vtkPVHMTerminalWidget_h_*/


