#include "vtkPVHMTopologicalDerivative.h"
#include "vtkSMHMTopologicalDerivativeProxy.h"

#include "vtkPV3DWidget.h"
#include "vtkPVApplication.h"
#include "vtkPVSource.h"
#include "vtkPVWidgetCollection.h"

#include "vtkSM3DWidgetProxy.h"

#include "vtkKWFrameWithLabel.h"
#include "vtkKWListBoxWithScrollbars.h"
#include "vtkKWListBox.h"
#include "vtkKWPushButton.h"
#include "vtkKWThumbWheel.h"
#include "vtkKWLabel.h"
#include "vtkKWEntry.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"

vtkCxxRevisionMacro(vtkPVHMTopologicalDerivative, "$Rev$");
vtkStandardNewMacro(vtkPVHMTopologicalDerivative);

vtkPVHMTopologicalDerivative::vtkPVHMTopologicalDerivative()
{
  this->UsingWidget = 0;
	
  this->MainFrame = vtkKWFrameWithLabel::New();
  this->RhoThumbWheel = vtkKWThumbWheel::New();
  this->ClassesListBox = vtkKWListBoxWithScrollbars::New();
  this->AddClassFromWidgetButton = vtkKWPushButton::New();
  this->AddClassButton = vtkKWPushButton::New();
  this->RemoveClassButton = vtkKWPushButton::New();
  
  this->LastClickedVoxelLabel = vtkKWLabel::New();
  this->IntensityOfLastClickedVoxelEntry = vtkKWEntry::New();
}

vtkPVHMTopologicalDerivative::~vtkPVHMTopologicalDerivative()
{
  this->MainFrame->Delete();
  this->RhoThumbWheel->Delete();
  this->ClassesListBox->Delete();
  this->AddClassFromWidgetButton->Delete();
  this->AddClassButton->Delete();
  this->RemoveClassButton->Delete();
  
  this->LastClickedVoxelLabel->Delete();
  this->IntensityOfLastClickedVoxelEntry->Delete();  
}

void vtkPVHMTopologicalDerivative::Create(vtkKWApplication* kwApp)
{
  vtkPVApplication *pvApp = vtkPVApplication::SafeDownCast(kwApp);

  //Verifica se ImagesWorkspaceWidget está sendo utilizado.
  if (this->GetPVSource()->GetWidgets()->GetNumberOfItems() > 2)
  {
    this->UsingWidget = 1;
  }

  this->Superclass::Create(kwApp);

  this->MainFrame->SetParent(this);
  this->MainFrame->Create(pvApp);
  this->MainFrame->SetLabelText("HM Topological Derivative");

  this->RhoThumbWheel->SetParent(this->MainFrame->GetFrame());
  this->RhoThumbWheel->Create(pvApp);
  this->RhoThumbWheel->SetStartCommand (this, "SetValueChanged");
  this->RhoThumbWheel->SetResolution(0.1);
  this->RhoThumbWheel->SetMinimumValue(0.0);
  this->RhoThumbWheel->SetMaximumValue(1.0);
  this->RhoThumbWheel->SetValue(0.5);
  this->RhoThumbWheel->DisplayEntryOn();
  this->RhoThumbWheel->DisplayLabelOn();
  this->RhoThumbWheel->ClampMinimumValueOn();
  this->RhoThumbWheel->ClampMaximumValueOn();
  this->RhoThumbWheel->DisplayEntryAndLabelOnTopOff();
  this->RhoThumbWheel->GetLabel()->SetText("Rho");

  this->LastClickedVoxelLabel->SetParent(this->MainFrame->GetFrame());
  this->LastClickedVoxelLabel->Create(pvApp);
  this->LastClickedVoxelLabel->SetText("Class");

  this->IntensityOfLastClickedVoxelEntry->SetParent(this->MainFrame->GetFrame());
  this->IntensityOfLastClickedVoxelEntry->Create(pvApp);
  this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->IntensityOfLastClickedVoxelEntry->SetWidth(8);

  this->ClassesListBox->SetParent(this->MainFrame->GetFrame());
  this->ClassesListBox->Create(pvApp);
  this->ClassesListBox->GetWidget()->SetSelectionModeToSingle();
  this->ClassesListBox->GetWidget()->SetWidth(50);
  this->ClassesListBox->GetWidget()->SetHeight(10);
//  this->ClassesListBox->GetWidget()->SetSingleClickCommand(this, "ListBoxItemSelected");

  this->AddClassFromWidgetButton->SetParent(this->MainFrame->GetFrame());
  this->AddClassFromWidgetButton->Create(pvApp);
  this->AddClassFromWidgetButton->SetBackgroundColor(1, 0, 0);
  this->AddClassFromWidgetButton->SetWidth(20);
  this->AddClassFromWidgetButton->SetHeight(1);
  this->AddClassFromWidgetButton->EnabledOff();
  this->AddClassFromWidgetButton->SetText("Add Class From Widget");
  this->AddClassFromWidgetButton->SetCommand(this, "AddClassFromWidget");
  
  if(this->UsingWidget)
  {
    this->AddClassFromWidgetButton->EnabledOn();
  }
  
  this->AddClassButton->SetParent(this->MainFrame->GetFrame());
  this->AddClassButton->Create(pvApp);
  this->AddClassButton->SetWidth(1);
  this->AddClassButton->SetHeight(1);
  this->AddClassButton->SetText("Add");
  this->AddClassButton->SetCommand(this, "AddClass");

  this->RemoveClassButton->SetParent(this->MainFrame->GetFrame());
  this->RemoveClassButton->Create(pvApp);
  this->RemoveClassButton->SetWidth(6);
  this->RemoveClassButton->SetHeight(1);
  this->RemoveClassButton->SetText("Remove");
  this->RemoveClassButton->SetCommand(this, "RemoveClass");

  this->PlaceComponents();
}

void vtkPVHMTopologicalDerivative::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",  
    this->MainFrame->GetWidgetName());
    
  this->Script("grid %s -sticky ew -columnspan 3",
    this->RhoThumbWheel->GetWidgetName());

  this->Script("grid %s %s - -sticky ew -columnspan 1",
    this->LastClickedVoxelLabel->GetWidgetName(),
    this->IntensityOfLastClickedVoxelEntry->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 3",
    this->ClassesListBox->GetWidgetName());

  this->Script("grid %s %s %s - - -sticky ew -columnspan 1",
    this->AddClassFromWidgetButton->GetWidgetName(),
    this->AddClassButton->GetWidgetName(),
    this->RemoveClassButton->GetWidgetName());


  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->RhoThumbWheel->GetEntry()->GetWidgetName(),
    this->GetTclName()) ;
}

void vtkPVHMTopologicalDerivative::SetValueChanged()
{
  this->ModifiedCallback();
}

void vtkPVHMTopologicalDerivative::Initialize()
{
  this->SourceProxy = vtkSMHMTopologicalDerivativeProxy::SafeDownCast(this->GetPVSource()->GetProxy());
}

void vtkPVHMTopologicalDerivative::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMTopologicalDerivative::Accept()
{
  vtkSMDoubleVectorProperty* RhoProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    this->GetPVSource()->GetProxy()->GetProperty("Rho"));
  RhoProperty->SetElement(0, this->RhoThumbWheel->GetValue());

  this->Superclass::Accept();
}

void vtkPVHMTopologicalDerivative::PostAccept()
{
  this->Superclass::PostAccept();
}

void vtkPVHMTopologicalDerivative::AddClassFromWidget()
{
  if (this->UsingWidget)
  {
  vtkSMDoubleVectorProperty* IntensityOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("IntensityOfLastClickedVoxel"));

    this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(IntensityOfLastClickedVoxelProperty->GetElement(0));
  }
  this->AddClass();
}

void vtkPVHMTopologicalDerivative::AddClass()
{
  char TempChar[30];
  int TempSelected = this->ClassesListBox->GetWidget()->GetSelectionIndex();

  strcpy(TempChar, "CLASS: ");
  strcat(TempChar, this->IntensityOfLastClickedVoxelEntry->GetValue());  
  
  this->ClassesListBox->GetWidget()->InsertEntry(this->ClassesListBox->GetWidget()->GetNumberOfItems(), TempChar);

  if(this->ClassesListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->ClassesListBox->GetWidget()->SetSelectionIndex(this->ClassesListBox->GetWidget()->GetNumberOfItems()-1);
  }
  if(this->ClassesListBox->GetWidget()->GetNumberOfItems() > 1)
  {
    this->ClassesListBox->GetWidget()->SetSelectState(TempSelected, 0);
  }

  vtkSMDoubleVectorProperty* AddClassProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    this->GetPVSource()->GetProxy()->GetProperty("AddClass"));
   
    AddClassProperty->SetElement(0, this->IntensityOfLastClickedVoxelEntry->GetValueAsDouble());

  AddClassProperty = NULL;
  
  this->SetValueChanged();
}

void vtkPVHMTopologicalDerivative::RemoveClass()
{
  int Selected = this->ClassesListBox->GetWidget()->GetSelectionIndex();

  if(this->ClassesListBox->GetWidget()->GetNumberOfItems() > 0 && this->ClassesListBox->GetWidget()->GetSelectionIndex() != -1)
  {
  	if(this->ClassesListBox->GetWidget()->GetNumberOfItems() == 1 || Selected == 0)
  	{
      this->ClassesListBox->GetWidget()->SetSelectionIndex(1);
  	}
  	else
  	{
      this->ClassesListBox->GetWidget()->SetSelectionIndex(Selected-1);
  	}

  vtkSMIntVectorProperty* RemoveClassProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->GetPVSource()->GetProxy()->GetProperty("RemoveClass"));
  RemoveClassProperty->SetElement(0, Selected);

  this->ClassesListBox->GetWidget()->DeleteRange(Selected, Selected);
    
  RemoveClassProperty = NULL;
  }
  
  this->SetValueChanged();
}
