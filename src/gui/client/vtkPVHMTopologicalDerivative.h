#ifndef _vtkPVHMTopologicalDerivative_h_
#define _vtkPVHMTopologicalDerivative_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

class vtkSMHMTopologicalDerivativeProxy;

class vtkKWFrameWithLabel;

class vtkKWEntry;
class vtkKWLabel;

class vtkKWThumbWheel;
class vtkKWListBoxWithScrollbars;
class vtkKWPushButton;

class vtkKWFrameWithLabel;
class vtkKWEntryWithLabel;

class vtkPVApplication;

class VTK_EXPORT vtkPVHMTopologicalDerivative : public vtkPVObjectWidget
{
public:

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMTopologicalDerivative *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMTopologicalDerivative, vtkPVObjectWidget);

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();
  
  //Description:
  //Method invoked when an interface component has its value changed.
  void SetValueChanged();
  
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();

  // Description:
  // Method executed right after Accept().
  virtual void PostAccept();
  
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	
   
  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();
  
  // Description:
  // Adds a class to the list from the entry above the listbox.
  void AddClass();
  
  // Description:
  // Removes a class from the list by a given ID.  
  void RemoveClass();

  // Description:
  // Adds a class to the widget from a previously captured voxel.
  void AddClassFromWidget();
  
protected:

  // Description:
  // Constructor
  vtkPVHMTopologicalDerivative();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMTopologicalDerivative();
  
  // Description:
  // Filter Source Proxy.
  vtkSMHMTopologicalDerivativeProxy *SourceProxy;

  // Description:
  // Main Frame of interface
  vtkKWFrameWithLabel *MainFrame;
  
  // Description:
  // Controls if the widget visibility is on or off
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable
  int ReadOnly;

  // Description:
  // Method responsible for creating the Interface that interacts with the source.
  void Create(vtkKWApplication *App);

  // Description:
  // Controls if the Filter is using ImageWorkspaceWidget.  
  int UsingWidget;

  // Description:
  // Rho widget Thumb Wheel controler.
  vtkKWThumbWheel *RhoThumbWheel;

  // Description:
  // Stores all the seeds that are going to be used on the filter.
  vtkKWListBoxWithScrollbars *ClassesListBox;

  // Description:
  // Button that adds a class to the list from the widget's captured voxel intensity.
  vtkKWPushButton *AddClassFromWidgetButton;

  // Description:
  // Button that adds a class to the list.
  vtkKWPushButton *AddClassButton;
  
  // Description:
  // Button that removes a class from the list.
  vtkKWPushButton *RemoveClassButton;
  
  // Description:
  // Last Clicked Voxel's Label.
  vtkKWLabel *LastClickedVoxelLabel;

  // Description:
  // Entry that stores the Intensity from the widget's just selected / clicked voxel.
  vtkKWEntry *IntensityOfLastClickedVoxelEntry;

private:  
  vtkPVHMTopologicalDerivative(const vtkPVHMTopologicalDerivative&); // Not implemented
  void operator=(const vtkPVHMTopologicalDerivative&); // Not implemented
}; 

#endif
