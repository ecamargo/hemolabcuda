#include "vtkPVHMVoxelGrow.h"
#include "vtkSMHMVoxelGrowProxy.h"

#include "vtkPVApplication.h"
#include "vtkPVWindow.h"
#include "vtkPVSource.h"
#include "vtkKWMessageDialog.h"

#include "vtkPVProcessModule.h"
#include "vtkSMSourceProxy.h"

#include "vtkKWFrameWithLabel.h"
#include "vtkKWThumbWheel.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWScale.h"

#include "vtkKWRadioButtonSetWithLabel.h"
#include "vtkKWRadioButtonSet.h"
#include "vtkKWRadioButton.h"

#include "vtkKWLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWListBoxWithScrollbars.h"
#include "vtkKWListBox.h"
#include "vtkKWPushButton.h"
#include "vtkKWCheckButton.h"

#include "vtkSMIntVectorProperty.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMStringVectorProperty.h"

#include "vtkPVWidgetCollection.h"
#include "vtkPV3DWidget.h"
#include "vtkSM3DWidgetProxy.h"

vtkCxxRevisionMacro(vtkPVHMVoxelGrow, "$Rev$");
vtkStandardNewMacro(vtkPVHMVoxelGrow);

vtkPVHMVoxelGrow::vtkPVHMVoxelGrow()
{
  this->UsingWidget = 0;
  this->UpperThreshold = 0;
  this->LowerThreshold = 0;
  
  this->MainFrame = vtkKWFrameWithLabel::New();

  this->UpperThresholdThumbWheel = vtkKWThumbWheel::New();
  this->LowerThresholdThumbWheel = vtkKWThumbWheel::New();
  
  this->ThresholdsModeRadioButtonSet = vtkKWRadioButtonSetWithLabel::New();
  this->IntensitiesAnalysisModeRadioButtonSet = vtkKWRadioButtonSetWithLabel::New();
  
  
  this->SeedsListBox = vtkKWListBoxWithScrollbars::New();
  this->AddSeedFromWidgetButton = vtkKWPushButton::New();
  this->AddSeedButton = vtkKWPushButton::New();
  this->RemoveSeedButton = vtkKWPushButton::New();
  
  this->LastClickedVoxelLabel = vtkKWLabel::New();
  this->XOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->YOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->ZOfLastClickedVoxelEntry = vtkKWEntry::New();
  this->IntensityOfLastClickedVoxelEntry = vtkKWEntry::New();
  
  this->IslandsFrame = vtkKWFrameWithLabel::New();
  this->IslandRemovalEnabledButton = vtkKWCheckButton::New();
  this->IslandDetectionFactorSlider = vtkKWScaleWithEntry::New();
  this->NeighboorsAnalizableRadiusThumbWheel = vtkKWThumbWheel::New();
}

vtkPVHMVoxelGrow::~vtkPVHMVoxelGrow()
{
  this->MainFrame->Delete();
  this->UpperThresholdThumbWheel->Delete();
  this->LowerThresholdThumbWheel->Delete();
  this->ThresholdsModeRadioButtonSet->Delete();
  this->IntensitiesAnalysisModeRadioButtonSet->Delete();
  this->SeedsListBox->Delete();
  this->AddSeedFromWidgetButton->Delete();
  this->AddSeedButton->Delete();
  this->RemoveSeedButton->Delete();

  this->LastClickedVoxelLabel->Delete();
  this->XOfLastClickedVoxelEntry->Delete();
  this->YOfLastClickedVoxelEntry->Delete();
  this->ZOfLastClickedVoxelEntry->Delete();
  this->IntensityOfLastClickedVoxelEntry->Delete();

  this->IslandsFrame->Delete();
  this->IslandRemovalEnabledButton->Delete();
  this->IslandDetectionFactorSlider->Delete();
  this->NeighboorsAnalizableRadiusThumbWheel->Delete();
}

void vtkPVHMVoxelGrow::Create(vtkKWApplication* App)
{
  //verifies if the module is using the HMImageWorkspace Widget.
  if (this->GetPVSource()->GetWidgets()->GetNumberOfItems() > 2)
  {
    this->UsingWidget = 1;
  }

  this->Superclass::Create(App);
  vtkPVApplication *pvApp = vtkPVApplication::SafeDownCast(App);

  this->MainFrame->SetParent(this);
  this->MainFrame->Create(pvApp);
  this->MainFrame->SetLabelText("HM Voxel Grow");

  this->UpperThresholdThumbWheel->SetParent(this->MainFrame->GetFrame());
  this->UpperThresholdThumbWheel->Create(pvApp);
  this->UpperThresholdThumbWheel->SetEndCommand (this, "SetThresholdsCallback");
  this->UpperThresholdThumbWheel->SetEntryCommand(this, "SetThresholdsEntriesCallback");  
  this->UpperThresholdThumbWheel->SetMinimumValue(0.0);
  this->UpperThresholdThumbWheel->SetValue(4000.0);
  this->UpperThresholdThumbWheel->DisplayEntryOn();
  this->UpperThresholdThumbWheel->DisplayLabelOn();
  this->UpperThresholdThumbWheel->ClampMinimumValueOn();
  this->UpperThresholdThumbWheel->DisplayEntryAndLabelOnTopOff();
  this->UpperThresholdThumbWheel->GetLabel()->SetText("Upper Threshold");
  
  this->LowerThresholdThumbWheel->SetParent(this->MainFrame->GetFrame());
  this->LowerThresholdThumbWheel->Create(pvApp);
  this->LowerThresholdThumbWheel->SetEndCommand (this, "SetThresholdsCallback");
  this->LowerThresholdThumbWheel->SetEntryCommand(this, "SetThresholdsEntriesCallback");
  this->LowerThresholdThumbWheel->SetMinimumValue(0.0);
  this->LowerThresholdThumbWheel->SetValue(400.0);
  this->LowerThresholdThumbWheel->DisplayEntryOn();
  this->LowerThresholdThumbWheel->DisplayLabelOn();
  this->LowerThresholdThumbWheel->ClampMinimumValueOn();
  this->LowerThresholdThumbWheel->DisplayEntryAndLabelOnTopOff();
  this->LowerThresholdThumbWheel->GetLabel()->SetText("Lower Threshold");
  
  this->UpperThreshold = int(this->UpperThresholdThumbWheel->GetValue());
  this->LowerThreshold = int(this->LowerThresholdThumbWheel->GetValue());

  this->IntensitiesAnalysisModeRadioButtonSet->SetParent(this->MainFrame->GetFrame());
  this->IntensitiesAnalysisModeRadioButtonSet->ExpandWidgetOn();
  this->IntensitiesAnalysisModeRadioButtonSet->Create(pvApp);
  this->IntensitiesAnalysisModeRadioButtonSet->SetLabelPositionToLeft();
  this->IntensitiesAnalysisModeRadioButtonSet->SetLabelText("Intensities Analisys");  
  
  vtkKWRadioButton *TempRadioButton = this->IntensitiesAnalysisModeRadioButtonSet->GetWidget()->AddWidget(0);
    TempRadioButton->SetText("Individual");
    TempRadioButton->SetValueAsInt(0);
    TempRadioButton->SelectedStateOn();
    TempRadioButton->SetCommand(this, "SetValueChanged");
    
  TempRadioButton = this->IntensitiesAnalysisModeRadioButtonSet->GetWidget()->AddWidget(1);
    TempRadioButton->SetValueAsInt(1);
    TempRadioButton->SetText("Average");
    TempRadioButton->SetCommand(this, "SetValueChanged");

  this->ThresholdsModeRadioButtonSet->SetParent(this->MainFrame->GetFrame());
  this->ThresholdsModeRadioButtonSet->ExpandWidgetOn();
  this->ThresholdsModeRadioButtonSet->Create(pvApp);
  this->ThresholdsModeRadioButtonSet->SetLabelPositionToLeft();
  this->ThresholdsModeRadioButtonSet->SetLabelText("Thresholds Mode");  

  TempRadioButton = this->ThresholdsModeRadioButtonSet->GetWidget()->AddWidget(0);
    TempRadioButton->SetText("Unique");
    TempRadioButton->SetValueAsInt(0);
    TempRadioButton->SelectedStateOn();
    TempRadioButton->SetCommand(this, "ThresholdsModeRadioButtonSetCallback");
    
  TempRadioButton = this->ThresholdsModeRadioButtonSet->GetWidget()->AddWidget(1);
    TempRadioButton->SetValueAsInt(1);
    TempRadioButton->SetText("Per Seed");
    TempRadioButton->SetCommand(this, "ThresholdsModeRadioButtonSetCallback");
    
  TempRadioButton = NULL;

//Voxels Manipulation ---------------------------------------------------------|
  this->LastClickedVoxelLabel->SetParent(this->MainFrame->GetFrame());
  this->LastClickedVoxelLabel->Create(pvApp);
  this->LastClickedVoxelLabel->SetText("Seed");

  this->XOfLastClickedVoxelEntry->SetParent(this->MainFrame->GetFrame());
  this->XOfLastClickedVoxelEntry->Create(pvApp);
  this->XOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->XOfLastClickedVoxelEntry->SetWidth(8);

  this->YOfLastClickedVoxelEntry->SetParent(this->MainFrame->GetFrame());
  this->YOfLastClickedVoxelEntry->Create(pvApp);
  this->YOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->YOfLastClickedVoxelEntry->SetWidth(8);

  this->ZOfLastClickedVoxelEntry->SetParent(this->MainFrame->GetFrame());
  this->ZOfLastClickedVoxelEntry->Create(pvApp);
  this->ZOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->ZOfLastClickedVoxelEntry->SetWidth(8);

  this->IntensityOfLastClickedVoxelEntry->SetParent(this->MainFrame->GetFrame());
  this->IntensityOfLastClickedVoxelEntry->Create(pvApp);
  this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(0.0);
  this->IntensityOfLastClickedVoxelEntry->SetWidth(8);


  this->SeedsListBox->SetParent(this->MainFrame->GetFrame());
  this->SeedsListBox->Create(pvApp);
  this->SeedsListBox->GetWidget()->SetSelectionModeToSingle();
  this->SeedsListBox->GetWidget()->SetWidth(50);
  this->SeedsListBox->GetWidget()->SetHeight(10);
  this->SeedsListBox->GetWidget()->SetSingleClickCommand(this, "ListBoxItemSelected");

  this->AddSeedFromWidgetButton->SetParent(this->MainFrame->GetFrame());
  this->AddSeedFromWidgetButton->Create(pvApp);
  this->AddSeedFromWidgetButton->SetBackgroundColor(1, 0, 0);
  this->AddSeedFromWidgetButton->SetWidth(20);
  this->AddSeedFromWidgetButton->SetHeight(1);
  this->AddSeedFromWidgetButton->EnabledOff();
  this->AddSeedFromWidgetButton->SetText("Add Seed From Widget");
  this->AddSeedFromWidgetButton->SetCommand(this, "AddSeedFromWidgetCallback");
  
  if(this->UsingWidget)
  {
    this->AddSeedFromWidgetButton->EnabledOn();
    this->AddSeedButton->EnabledOff();
  }
  
  this->AddSeedButton->SetParent(this->MainFrame->GetFrame());
  this->AddSeedButton->Create(pvApp);
  this->AddSeedButton->SetWidth(1);
  this->AddSeedButton->SetHeight(1);
  this->AddSeedButton->SetText("Add");
  this->AddSeedButton->SetCommand(this, "AddSeedCallback");

  this->RemoveSeedButton->SetParent(this->MainFrame->GetFrame());
  this->RemoveSeedButton->Create(pvApp);
  this->RemoveSeedButton->SetWidth(6);
  this->RemoveSeedButton->SetHeight(1);
  this->RemoveSeedButton->SetText("Remove");
  this->RemoveSeedButton->SetCommand(this, "RemoveSeedCallback");

//  this->Superclass::Create(App);
  
  this->IslandsFrame->SetParent(this);
  this->IslandsFrame->Create(pvApp);
  this->IslandsFrame->SetLabelText("Islands Removal");

  this->IslandRemovalEnabledButton->SetParent(this->IslandsFrame->GetFrame());
  this->IslandRemovalEnabledButton->Create(pvApp);
  this->IslandRemovalEnabledButton->SetText("On");
  this->IslandRemovalEnabledButton->SetSelectedState(0);
  this->IslandRemovalEnabledButton->SetCommand(this,"SetIslandRemovalEnabledCallback");

  this->IslandDetectionFactorSlider->SetParent(this->IslandsFrame->GetFrame());
  this->IslandDetectionFactorSlider->Create(pvApp);
  this->IslandDetectionFactorSlider->SetResolution(0.01);
  this->IslandDetectionFactorSlider->SetRange(0, 1);
  this->IslandDetectionFactorSlider->SetValue(0.9);
  this->IslandDetectionFactorSlider->SetEnabled(this->IslandRemovalEnabledButton->GetSelectedState());
  this->IslandDetectionFactorSlider->RangeVisibilityOff();
  this->IslandDetectionFactorSlider->SetLabelText("Island Detection Factor");
  this->IslandDetectionFactorSlider->SetCommand(this,"SetIslandDetectionFactorCallback");
  
  this->IslandDetectionFactorSlider->GetScale()->SetLength(220);
  this->IslandDetectionFactorSlider->GetEntry()->SetWidth(4);

  this->NeighboorsAnalizableRadiusThumbWheel->SetParent(this->IslandsFrame->GetFrame());
  this->NeighboorsAnalizableRadiusThumbWheel->Create(pvApp);
  this->NeighboorsAnalizableRadiusThumbWheel->SetStartCommand (this, "SetNeighboorsAnalizableRadiusCallback");
  this->NeighboorsAnalizableRadiusThumbWheel->SetMinimumValue(0.0);
  this->NeighboorsAnalizableRadiusThumbWheel->SetValue(2.0);
  this->NeighboorsAnalizableRadiusThumbWheel->SetEnabled(this->IslandRemovalEnabledButton->GetSelectedState());
  this->NeighboorsAnalizableRadiusThumbWheel->DisplayEntryOn();
  this->NeighboorsAnalizableRadiusThumbWheel->DisplayLabelOn();
  this->NeighboorsAnalizableRadiusThumbWheel->ClampMinimumValueOn();
  this->NeighboorsAnalizableRadiusThumbWheel->DisplayEntryAndLabelOnTopOff();
  this->NeighboorsAnalizableRadiusThumbWheel->GetLabel()->SetText("Neighboors Analizable Radius");

  this->PlaceComponents();
}

void vtkPVHMVoxelGrow::PlaceComponents()
{
  this->Script("pack %s -fill x -expand true",  
    this->MainFrame->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->UpperThresholdThumbWheel->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->LowerThresholdThumbWheel->GetWidgetName());

  this->Script("grid %s %s - -sticky ew -columnspan 2",
    this->ThresholdsModeRadioButtonSet->GetWidgetName(),
    this->IntensitiesAnalysisModeRadioButtonSet->GetWidgetName());

  this->Script("grid %s %s %s %s %s - - - - -sticky n",
    this->LastClickedVoxelLabel->GetWidgetName(),
    this->XOfLastClickedVoxelEntry->GetWidgetName(),
    this->YOfLastClickedVoxelEntry->GetWidgetName(),
    this->ZOfLastClickedVoxelEntry->GetWidgetName(),
    this->IntensityOfLastClickedVoxelEntry->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->SeedsListBox->GetWidgetName());

  this->Script("grid %s %s %s - - -sticky ew -columnspan 2",
    this->AddSeedFromWidgetButton->GetWidgetName(),
    this->AddSeedButton->GetWidgetName(),
    this->RemoveSeedButton->GetWidgetName());
    
  this->Script("pack %s -fill x -expand true",  
    this->IslandsFrame->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->IslandRemovalEnabledButton->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->IslandDetectionFactorSlider->GetWidgetName());

  this->Script("grid %s -sticky ew -columnspan 5",
    this->NeighboorsAnalizableRadiusThumbWheel->GetWidgetName());


  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->UpperThresholdThumbWheel->GetEntry()->GetWidgetName(),
    this->GetTclName());


  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->LowerThresholdThumbWheel->GetEntry()->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->IslandRemovalEnabledButton->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->IslandDetectionFactorSlider->GetEntry()->GetWidgetName(),
    this->GetTclName());

  this->Script("bind %s <Key> {%s SetValueChanged}",
    this->NeighboorsAnalizableRadiusThumbWheel->GetEntry()->GetWidgetName(),
    this->GetTclName());
}

void vtkPVHMVoxelGrow::SetIslandRemovalEnabledCallback()
{
  this->IslandDetectionFactorSlider->SetEnabled(this->IslandRemovalEnabledButton->GetSelectedState());  
  this->NeighboorsAnalizableRadiusThumbWheel->SetEnabled(this->IslandRemovalEnabledButton->GetSelectedState());
  
  vtkSMIntVectorProperty* IslandDetectionEnabledProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("IslandDetectionEnabled"));
    
  IslandDetectionEnabledProperty->SetElement(0, int(this->IslandRemovalEnabledButton->GetSelectedState()));
  
  this->SetIslandDetectionFactorCallback();
  this->SetNeighboorsAnalizableRadiusCallback();
  
  this->ModifiedCallback();
}

void vtkPVHMVoxelGrow::SetIslandDetectionFactorCallback()
{
  vtkSMDoubleVectorProperty* IslandDetectionFactorProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("IslandDetectionFactor"));
    
  IslandDetectionFactorProperty->SetElement(0, this->IslandDetectionFactorSlider->GetValue());
  
  this->ModifiedCallback();
}

void vtkPVHMVoxelGrow::SetNeighboorsAnalizableRadiusCallback()
{
  vtkSMIntVectorProperty* NeighboorsAnalizableRadiusProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->SourceProxy->GetProperty("NeighboorsAnalizableRadius"));
    
  NeighboorsAnalizableRadiusProperty->SetElement(0, int(this->NeighboorsAnalizableRadiusThumbWheel->GetValue()));
  
  this->ModifiedCallback();
}

void vtkPVHMVoxelGrow::SetValueChanged()
{
  this->ModifiedCallback();
}

void vtkPVHMVoxelGrow::Initialize()
{
  this->SourceProxy = vtkSMHMVoxelGrowProxy::SafeDownCast(this->GetPVSource()->GetProxy());
}

void vtkPVHMVoxelGrow::SaveInBatchScript(ofstream *file)
{
}

void vtkPVHMVoxelGrow::Accept()
{
  this->GetPVSource()->GetPVApplication()->GetProcessModule()->SendPrepareProgress();
  this->SourceProxy->SetThreshold(int (this->LowerThresholdThumbWheel->GetValue()), int (this->UpperThresholdThumbWheel->GetValue()));
  this->SourceProxy->SendStream("SetIntensitiesAnalysisMode", this->GetSelectedIntensitiesAnalysisModeValue());
  this->SourceProxy->SendStream("SetThresholdsMode", this->GetSelectedThresholdsModeValue());
  
  this->SetIslandRemovalEnabledCallback();
  this->SetIslandDetectionFactorCallback();
  this->SetNeighboorsAnalizableRadiusCallback();

  //Sending Seeds
  double TempSeed[4];

  this->Superclass::Accept();
  
  this->GetPVSource()->GetPVApplication()->GetProcessModule()->SendCleanupPendingProgress();
}

int vtkPVHMVoxelGrow::GetLastNumberOfIslands()
{
  vtkSMIntVectorProperty* LastNumberOfIslandsProperty = vtkSMIntVectorProperty::SafeDownCast(
    this->GetPVSource()->GetProxy()->GetProperty("LastNumberOfIslands"));

  this->GetPVSource()->GetProxy()->UpdatePropertyInformation(LastNumberOfIslandsProperty);

  return LastNumberOfIslandsProperty->GetElement(0);
}

void vtkPVHMVoxelGrow::AddSeed(double SeedX, double SeedY, double SeedZ, double SeedIntensity, int LowerThreshold, int UpperThreshold)
{
  //Adding seed on the Seeds Vector
  this->Seeds[0].push_back(SeedX);
  this->Seeds[1].push_back(SeedY);
  this->Seeds[2].push_back(SeedZ);
  this->Seeds[3].push_back(SeedIntensity);

  this->UpperThresholds.push_back(UpperThreshold);
  this->LowerThresholds.push_back(LowerThreshold);
  
  //Char used for the list box item text.
  char TempChar[70];
  int TempSelected = this->SeedsListBox->GetWidget()->GetSelectionIndex();

  //Deselect currently selected listbox item.
  this->SeedsListBox->GetWidget()->SetSelectState(TempSelected, 0);

  sprintf(TempChar, "SEED: %d x %d x %d: %d", this->XOfLastClickedVoxelEntry->GetValueAsInt(), this->YOfLastClickedVoxelEntry->GetValueAsInt(), this->ZOfLastClickedVoxelEntry->GetValueAsInt(), this->IntensityOfLastClickedVoxelEntry->GetValueAsInt());

  this->SourceProxy->SendSeed(this->XOfLastClickedVoxelEntry->GetValueAsDouble(), this->YOfLastClickedVoxelEntry->GetValueAsDouble(), this->ZOfLastClickedVoxelEntry->GetValueAsDouble(), this->IntensityOfLastClickedVoxelEntry->GetValueAsDouble());

  //Add threshold values for this seed.
  this->SourceProxy->AddSeedThresholds(int(this->LowerThresholdThumbWheel->GetValue()), int(this->UpperThresholdThumbWheel->GetValue()));

  if(this->GetSelectedThresholdsModeValue())
  {
    //casting for decimal cases elimination and adding to the string of the new list box item.
    sprintf (TempChar, "%s - LT %d - UT %d", TempChar, int(this->LowerThresholdThumbWheel->GetValue()), int(this->UpperThresholdThumbWheel->GetValue()));
  }
  
  this->SeedsListBox->GetWidget()->InsertEntry(this->SeedsListBox->GetWidget()->GetNumberOfItems(), TempChar);
  
  if(this->SeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->SeedsListBox->GetWidget()->SetSelectionIndex(this->SeedsListBox->GetWidget()->GetNumberOfItems()-1);
  }
  
  this->Modified();
  this->SetValueChanged();
}

void vtkPVHMVoxelGrow::ExecuteEvent(vtkObject* wdg, unsigned long l, void* p)
{
}

void vtkPVHMVoxelGrow::AddSeedFromWidgetCallback()
{
  if (this->UsingWidget)
  {
  vtkSMDoubleVectorProperty* XOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("XOfLastClickedVoxel"));

  vtkSMDoubleVectorProperty* YOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("YOfLastClickedVoxel"));
    
  vtkSMDoubleVectorProperty* ZOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("ZOfLastClickedVoxel"));
    
  vtkSMDoubleVectorProperty* IntensityOfLastClickedVoxelProperty = vtkSMDoubleVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("IntensityOfLastClickedVoxel"));

  vtkSMIntVectorProperty* AddSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("AddSeed"));

    this->XOfLastClickedVoxelEntry->SetValueAsDouble(XOfLastClickedVoxelProperty->GetElement(0));
    this->YOfLastClickedVoxelEntry->SetValueAsDouble(YOfLastClickedVoxelProperty->GetElement(0));
    this->ZOfLastClickedVoxelEntry->SetValueAsDouble(ZOfLastClickedVoxelProperty->GetElement(0));
    this->IntensityOfLastClickedVoxelEntry->SetValueAsDouble(IntensityOfLastClickedVoxelProperty->GetElement(0));
    
    //Adding the 3DWidget's seed.
    AddSeedProperty->SetElement(0,1);
  }
  this->AddSeedCallback();
  this->ListBoxItemSelected();
}

void vtkPVHMVoxelGrow::AddSeedCallback()
{
  //Adding seed from interface
  this->AddSeed(this->XOfLastClickedVoxelEntry->GetValueAsDouble(),
                this->YOfLastClickedVoxelEntry->GetValueAsDouble(),
                this->ZOfLastClickedVoxelEntry->GetValueAsDouble(),
                this->IntensityOfLastClickedVoxelEntry->GetValueAsDouble(),
                int(this->LowerThresholdThumbWheel->GetValue()),
                int(this->UpperThresholdThumbWheel->GetValue()));
}

void vtkPVHMVoxelGrow::RemoveSeedCallback()
{
  int Selected = this->SeedsListBox->GetWidget()->GetSelectionIndex();

  if(this->SeedsListBox->GetWidget()->GetSelectionIndex() >= 0)
  {
    if(this->SeedsListBox->GetWidget()->GetNumberOfItems() > 0)
    {
      //Removing seed from 3DWidget.
      if (this->UsingWidget)
      {
        this->RemoveSeedFromWidget(Selected);
      }

      if(this->SeedsListBox->GetWidget()->GetNumberOfItems() == 1 || Selected == 0)
      {
        this->SeedsListBox->GetWidget()->SetSelectionIndex(1);
   	  }
      else
  	  {
        this->SeedsListBox->GetWidget()->SetSelectionIndex(Selected-1);
        this->ListBoxItemSelected(Selected-1);
  	  }
      //Removing seed from Voxel Grow data-server filter object.
      this->SourceProxy->SendStream("RemoveSeed", Selected);
      this->SourceProxy->RemoveSeedThresholds(Selected);
      this->SeedsListBox->GetWidget()->DeleteRange(Selected, Selected);
    }

    //Erasing seed from local vectors.
    SeedsVector::iterator XSeedsIteractor = this->Seeds[0].begin();
    SeedsVector::iterator YSeedsIteractor = this->Seeds[1].begin();
    SeedsVector::iterator ZSeedsIteractor = this->Seeds[2].begin();
    SeedsVector::iterator IntensitySeedsIteractor = this->Seeds[3].begin();

    SeedsThresholdsVector::iterator UpperThresholdsIterator = this->UpperThresholds.begin();
    SeedsThresholdsVector::iterator LowerThresholdsIterator = this->LowerThresholds.begin();

    for(int i = 0; i < Selected; i++)
    {
      XSeedsIteractor++;
      YSeedsIteractor++;
      ZSeedsIteractor++;
      IntensitySeedsIteractor++;
      
      UpperThresholdsIterator++;
      LowerThresholdsIterator++;
    }

    this->Seeds[0].erase(XSeedsIteractor);
    this->Seeds[1].erase(YSeedsIteractor);
    this->Seeds[2].erase(ZSeedsIteractor);
    this->Seeds[3].erase(IntensitySeedsIteractor);
  
    this->UpperThresholds.erase(UpperThresholdsIterator);
    this->LowerThresholds.erase(LowerThresholdsIterator);

    this->ListBoxItemSelected();
    this->SetValueChanged();
  }
}

void vtkPVHMVoxelGrow::RemoveSeedFromWidget(int PassedSeedID)
{
  vtkSMIntVectorProperty* RemoveSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("RemoveSeed"));
   
    //Removing the 3DWidget's seed.
    RemoveSeedProperty->SetElement(0, PassedSeedID);
}

void vtkPVHMVoxelGrow::SelectSeedOnWidget(int PassedID)
{
  vtkSMIntVectorProperty* SelectSeedProperty = vtkSMIntVectorProperty::SafeDownCast(
    vtkPV3DWidget::SafeDownCast(this->GetPVSource()->GetWidgets()->GetLastPVWidget())->GetWidgetProxy()->GetProperty("SelectSeed"));

    SelectSeedProperty->SetElement(0,  PassedID);
}
  
void vtkPVHMVoxelGrow::ListBoxItemSelected()
{
  if(this->UsingWidget && this->SeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->SelectSeedOnWidget(this->SeedsListBox->GetWidget()->GetSelectionIndex());
  }

  //If Per Seed mode is ON, set the threshold thumbwheels with the selected seed thresholds values.
  if(this->GetSelectedThresholdsModeValue() && this->SeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->UpperThresholdThumbWheel->SetValue(this->UpperThresholds.at(this->SeedsListBox->GetWidget()->GetSelectionIndex()));
    this->LowerThresholdThumbWheel->SetValue(this->LowerThresholds.at(this->SeedsListBox->GetWidget()->GetSelectionIndex()));
  }
}

void vtkPVHMVoxelGrow::ListBoxItemSelected(int PassedID)
{
  if(this->UsingWidget && this->SeedsListBox->GetWidget()->GetNumberOfItems() > 0)
  {
    this->SelectSeedOnWidget(PassedID);
  }
}

int vtkPVHMVoxelGrow::GetSelectedIntensitiesAnalysisModeValue()
{
  if (this->IntensitiesAnalysisModeRadioButtonSet->GetWidget()->GetWidget(0)->GetSelectedState())
  {
  	return this->IntensitiesAnalysisModeRadioButtonSet->GetWidget()->GetWidget(0)->GetValueAsInt();
  }
  else
  {
  	return this->IntensitiesAnalysisModeRadioButtonSet->GetWidget()->GetWidget(1)->GetValueAsInt();
  }
}

int vtkPVHMVoxelGrow::GetSelectedThresholdsModeValue()
{
  if (this->ThresholdsModeRadioButtonSet->GetWidget()->GetWidget(0)->GetSelectedState())
  {
    return this->ThresholdsModeRadioButtonSet->GetWidget()->GetWidget(0)->GetValueAsInt();
//    return 0;
  }
  else
  {
    return this->ThresholdsModeRadioButtonSet->GetWidget()->GetWidget(1)->GetValueAsInt();
//    return 1;
  }
}

void vtkPVHMVoxelGrow::ThresholdsModeRadioButtonSetCallback()
{
  int NumberOfSeeds = this->Seeds[0].size();

  //Getting the selected item index before cleaning up listbox.
  int TempSelected = this->SeedsListBox->GetWidget()->GetSelectionIndex();
  //Cleaning listbox.
  this->SeedsListBox->GetWidget()->DeleteAll();

  //Removing seeds from server and its respective thresholds from server.
  for(int i = NumberOfSeeds-1; i >= 0 ; i--)
  {
    this->SourceProxy->SendStream("RemoveSeed", i);
    this->SourceProxy->RemoveSeedThresholds(i);
  }

  //Adding the seeds and thresholds back updating the listbox items output acordding to the IntensitiesAnalysisModeValue.
  for(int i = 0; i < NumberOfSeeds; i++)
  {
    this->SourceProxy->SendSeed(this->Seeds[0].at(i), this->Seeds[1].at(i), this->Seeds[2].at(i), this->Seeds[3].at(i));
    this->SourceProxy->AddSeedThresholds(this->LowerThresholds.at(i), this->UpperThresholds.at(i));

    //Filling Listbox.
    //Char used for the list box item text.
    char TempChar[70];

    sprintf (TempChar, "SEED: %d x %d x %d: %d", int(this->Seeds[0].at(i)), int(this->Seeds[1].at(i)), int(this->Seeds[2].at(i)), int(this->Seeds[3].at(i)));

    if(this->GetSelectedThresholdsModeValue())
    {
      //casting for decimal cases elimination and adding to the string of the new list box item.
      sprintf(TempChar, "%s - LT %d - UT %d", TempChar, int(this->LowerThresholds.at(i)), int(this->UpperThresholds.at(i)));
    }

    //Inserting item back to the Listbox.
    this->SeedsListBox->GetWidget()->InsertEntry(this->SeedsListBox->GetWidget()->GetNumberOfItems(), TempChar);

    if(TempSelected >=0)
    {
      this->SeedsListBox->GetWidget()->SetSelectionIndex(TempSelected);
      this->SeedsListBox->GetWidget()->SetSelectState(TempSelected, 1);
    }
  }
  
  //Use Unique thresholds if Unique Mode is selected.
  if(!this->GetSelectedThresholdsModeValue())
  {
    this->UpperThresholdThumbWheel->SetValue(this->UpperThreshold);
    this->LowerThresholdThumbWheel->SetValue(this->LowerThreshold);
  }
  else
  {
    if (TempSelected >= 0)
    {
      this->UpperThresholdThumbWheel->SetValue(this->UpperThresholds.at(this->SeedsListBox->GetWidget()->GetSelectionIndex()));
      this->LowerThresholdThumbWheel->SetValue(this->LowerThresholds.at(this->SeedsListBox->GetWidget()->GetSelectionIndex()));
    }
  }

  this->SetValueChanged();
}

void vtkPVHMVoxelGrow::SetThresholdsCallback()
{
  this->UpperThresholdThumbWheel->GetEntry()->SetValueAsDouble(this->UpperThresholdThumbWheel->GetValue());
  this->LowerThresholdThumbWheel->GetEntry()->SetValueAsDouble(this->LowerThresholdThumbWheel->GetValue());
  
  if(!this->GetSelectedThresholdsModeValue())
  {
    this->UpperThreshold = int(this->UpperThresholdThumbWheel->GetValue());
    this->LowerThreshold = int(this->LowerThresholdThumbWheel->GetValue());
  }
  else
  {
    if(this->SeedsListBox->GetWidget()->GetSelectionIndex() >= 0)
    {
      //Selecting and updating the vectors entries to the new thresholds values.
      this->UpperThresholds[this->SeedsListBox->GetWidget()->GetSelectionIndex()] = int(this->UpperThresholdThumbWheel->GetValue());
      this->LowerThresholds[this->SeedsListBox->GetWidget()->GetSelectionIndex()] = int(this->LowerThresholdThumbWheel->GetValue());

      this->ThresholdsModeRadioButtonSetCallback();
    }
  }
  
  this->SetValueChanged();
}

void vtkPVHMVoxelGrow::SetThresholdsEntriesCallback()
{
  this->UpperThresholdThumbWheel->SetValue(this->UpperThresholdThumbWheel->GetEntry()->GetValueAsInt());
  this->LowerThresholdThumbWheel->SetValue(this->LowerThresholdThumbWheel->GetEntry()->GetValueAsInt());

  if(!this->GetSelectedThresholdsModeValue())
  {
    this->UpperThreshold = int(this->UpperThresholdThumbWheel->GetEntry()->GetValueAsInt());
    this->LowerThreshold = int(this->LowerThresholdThumbWheel->GetEntry()->GetValueAsInt());
  }
  else
  {
    if(this->SeedsListBox->GetWidget()->GetSelectionIndex() >= 0)
    {
      //Selecting and updating the vectors entries to the new thresholds values.
      this->UpperThresholds[this->SeedsListBox->GetWidget()->GetSelectionIndex()] = this->UpperThresholdThumbWheel->GetEntry()->GetValueAsInt();
      this->LowerThresholds[this->SeedsListBox->GetWidget()->GetSelectionIndex()] = this->LowerThresholdThumbWheel->GetEntry()->GetValueAsInt();

      this->ThresholdsModeRadioButtonSetCallback();
    }
  }
  
  this->SetValueChanged();
}
