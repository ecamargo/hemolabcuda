#ifndef _vtkPVHMVoxelGrow_h_
#define _vtkPVHMVoxelGrow_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"

#include <vector>

class vtkSMHMVoxelGrowProxy;

class vtkKWFrameWithLabel;

class vtkKWEntry;
class vtkKWLabel;
class vtkKWThumbWheel;
class vtkKWScaleWithEntry;
class vtkKWRadioButtonSetWithLabel;
class vtkKWListBoxWithScrollbars;
class vtkKWPushButton;

class vtkKWFrameWithLabel;
class vtkKWEntryWithLabel;
class vtkKWCheckButton;

class vtkPVApplication;

class VTK_EXPORT vtkPVHMVoxelGrow : public vtkPVObjectWidget
{
public:

//BTX
  typedef std::vector <double> SeedsVector;
  typedef std::vector <int> SeedsThresholdsVector;
//ETX

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkPVHMVoxelGrow *New();

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkPVHMVoxelGrow, vtkPVObjectWidget);

  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file);

  // Description:
  // Empty method to keep superclass from complaining.
  virtual void Trace(ofstream*) {};     

  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  
  // Description:
  // Set/Get widget visibility
	vtkSetMacro(WidgetVisibility,int);  
	vtkGetMacro(WidgetVisibility,int);	

  // Description:
  // Set/Get is a widget is ready only
	vtkSetMacro(ReadOnly,int);  
	vtkGetMacro(ReadOnly,int);	
   
  // Description:
  // Invoked when the module is called at Source Menu
  void Initialize();

  //Description:
  //Method invoked when an interface component has its value changed.
  void SetValueChanged();

  // Description:
  // Method responsible for creating the Interface that interacts with this filter's source.
  void Create(vtkKWApplication *App);

  // Description:
  // Adds a seed to the list from the 4 entries above the listbox.
  void AddSeed(double SeedX, double SeedY, double SeedZ, double SeedIntensity, int LowerThreshold, int UpperThreshold);

  // Description:
  // Adds a seed to the list from the 4 entries above the listbox.
  void AddSeedCallback();

  // Description:
  // Adds a seed to the widget from a previously captured seed (4 doubles).
  void AddSeedFromWidgetCallback();
  
  // Description:
  // Removes a seed from the list by a given ID.  
  void RemoveSeedCallback();

  // Description:
  // Removes a widget's seed from a passed ID;
  void RemoveSeedFromWidget(int PassedSeedID);
  
  // Description:
  // Sends to the widget the selected seed ID
  void SelectSeedOnWidget(int PassedID);
  
  // Description:
  // Method invoked when a seed's listbox item is selected (single click).
  void ListBoxItemSelected();

  // Description:
  // Method invoked when a single click is executed on a list box item.
  void ListBoxItemSelected(int PassedID);

  // Description:
  // Returns the ID of the selected Intensities Analysis mode.
  int GetSelectedIntensitiesAnalysisModeValue();

  // Description:
  // Returns the ID of the selected Threshold mode.
  int GetSelectedThresholdsModeValue();

  // Description:
  // Method Invoked when Island removal tool is turned On or Off.
  void SetIslandRemovalEnabledCallback();
  
  // Description:
  // Method Invoked when the IslandDetectionSlider has its value changed.
  void SetIslandDetectionFactorCallback();

  // Description:
  // Method Invoked when the upper and lower thresholds thumbwheels has its value changed.  
  void SetThresholdsCallback();

  // Description:
  // Method Invoked when the upper and lower thresholds thumbwheel's entries has its value changed.    
  void SetThresholdsEntriesCallback();

  // Description:
  // Set the Radius of neighboors that will be analized for each seed.
  void SetNeighboorsAnalizableRadiusCallback();
  
  // Description:
  // Returns the Last Number of islands found by Voxel Grow.
  int GetLastNumberOfIslands();
  
  void ThresholdsModeRadioButtonSetCallback();

protected:

  // Description:
  // Constructor
  vtkPVHMVoxelGrow();
  
  // Description:
  // Destructor
  virtual ~vtkPVHMVoxelGrow();

  // Description:
  // Invoked by the Create method, it's responsible for the TCL commands that positionates the widgets on the interface.
  void PlaceComponents();
  
  // Description:
  // Method executed when any iteraction is made on the viewport.
  virtual void	ExecuteEvent(vtkObject *obj, unsigned long l, void *p);

  // Description:
  // Filter Source Proxy.
  vtkSMHMVoxelGrowProxy *SourceProxy;

  // Description:
  // Vector that stores the seeds passed from the interface.
  SeedsVector Seeds[4];

  // Description:
  // Stores the seed's Upper threshold values.
  SeedsThresholdsVector UpperThresholds;

  // Description:
  // Stores the seed's Lower threshold values.
  SeedsThresholdsVector LowerThresholds;
  
  // Description:
  // Stores the Upper threshold value (used on unique mode).  
  int UpperThreshold;

  // Description:
  // Stores the Lower threshold value (used on unique mode).  
  int LowerThreshold;

  // Description:
  // Main Frame of interface.
  vtkKWFrameWithLabel *MainFrame;

  // Description:
  // Main Frame of interface.
  vtkKWFrameWithLabel *IslandsFrame;

  // Description:
  // Controls if the widget visibility is on or off.
  int WidgetVisibility;

  // Description:
  // Controls if the entries are read only or editable.
  int ReadOnly;

  // Description:
  // Controls if the Filter is using the HMImageWorkspace Widget.  
  int UsingWidget;

  // Description:
  // Upper Threshold widget Thumb Wheel controler.
  vtkKWThumbWheel *UpperThresholdThumbWheel;

  // Description:
  // Lower Threshold widget Thumb Wheel controler.
  vtkKWThumbWheel *LowerThresholdThumbWheel;

  // Description:
  // Set of radio buttons that configures if the thresholds are gon'na be used per seed or unique mode.
  vtkKWRadioButtonSetWithLabel *ThresholdsModeRadioButtonSet;

  // Description:
  // Set of radio buttons that configures the threshold mode (individual intensities of average).
  vtkKWRadioButtonSetWithLabel *IntensitiesAnalysisModeRadioButtonSet;

  // Description:
  // Stores all the seeds that are going to be used on the filter.
  vtkKWListBoxWithScrollbars *SeedsListBox;

  // Description:
  // Button that adds a seed to the list from the widget's captured seed.
  vtkKWPushButton *AddSeedFromWidgetButton;

  // Description:
  // Button that adds a seed to the list.
  vtkKWPushButton *AddSeedButton;
  
  // Description:
  // Button that removes a seed from the list.
  vtkKWPushButton *RemoveSeedButton;
  
  // Description:
  // Last Clicked Voxel's Label.
  vtkKWLabel *LastClickedVoxelLabel;

  // Description:
  // Entry that stores the X coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *XOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Y coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *YOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Z coordinate from the widget's just selected / clicked voxel.
  vtkKWEntry *ZOfLastClickedVoxelEntry;

  // Description:
  // Entry that stores the Intensity from the widget's just selected / clicked voxel.
  vtkKWEntry *IntensityOfLastClickedVoxelEntry;

  // Description:
  // Enables / Disables the Island Removal tool.
  vtkKWCheckButton *IslandRemovalEnabledButton;
  
  // Description:
  // Slider that configures the Island detection factor.
  vtkKWScaleWithEntry *IslandDetectionFactorSlider;
  
  // Description:
  // Set the Neighboors radius to be analized by the Island Detection.
  vtkKWThumbWheel *NeighboorsAnalizableRadiusThumbWheel;

private:  
  vtkPVHMVoxelGrow(const vtkPVHMVoxelGrow&); // Not implemented
  void operator=(const vtkPVHMVoxelGrow&); // Not implemented
}; 

#endif
