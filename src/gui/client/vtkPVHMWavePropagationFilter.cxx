/*
 * $Id: vtkPVHMWavePropagationFilter.cxx 340 2006-05-12 13:40:37Z jan $
 */
 
#include "vtkPVApplication.h"
#include "vtkPVTraceHelper.h"
#include "vtkPVSource.h"
#include "vtkProcessModule.h"
#include "vtkPVWindow.h"
#include "vtkKWProgressGauge.h"
#include "vtkPVWidget.h"
#include "vtkPVAnimationManager.h"
	
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMenuButton.h"
#include "vtkKWMenuButtonWithLabel.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWComboBox.h"
#include "vtkKWPushButton.h" 
#include "vtkKWIcon.h"
#include "vtkKWToolbar.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMProxyManager.h"

#include "vtkPVHMWavePropagationFilter.h"
#include "vtkHM3DQoptFilter.h"
#include "vtkPVAnimationManager.h"

#define MODE_FILE		 "File"
#define MODE_MANUAL  "Manual"

vtkCxxRevisionMacro(vtkPVHMWavePropagationFilter, "$Rev: 340 $");
vtkStandardNewMacro(vtkPVHMWavePropagationFilter);

//----------------------------------------------------------------------------
vtkPVHMWavePropagationFilter::vtkPVHMWavePropagationFilter()
{ 
	this->MyFrame	    = vtkKWFrameWithLabel::New();
	this->ScalarOption	= vtkKWComboBox::New();

	// File
	this->ConfigLabel	= vtkKWLabel::New();
	this->ConfigEntry 	= vtkKWEntry::New(); 
	this->StatusLabel 	= vtkKWLabel::New();  	  
	this->ConfigButton 	= vtkKWLoadSaveButton::New();
	this->ConfigButton->GetLoadSaveDialog()->SetFileTypes("{{Sequencial animation Files} {*.vtk}}");
}

//----------------------------------------------------------------------------
vtkPVHMWavePropagationFilter::~vtkPVHMWavePropagationFilter()
{ 
  this->MyFrame->Delete();
  this->ScalarOption->Delete();
  this->ConfigEntry->Delete();
  this->StatusLabel->Delete();
  this->ConfigLabel->Delete();
  this->ConfigButton->Delete();
}


// -----------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::ScalarOptionCallback()
{
	//std::cout << "vtkPVHMWavePropagationFilter::ScalarOptionCallback()" << endl;
	
	vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
 	                             this->GetPVSource()->GetProxy()->GetProperty("SelectScalar"));    
	  if (prop)
	  	{
		prop->SetElement(0, this->ScalarOption->GetValue());
	  	}
	  	
	  else
		{
	  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
      	return;
  		}
  		
	this->ModifiedCallback();
}


void vtkPVHMWavePropagationFilter::ConfigButtonCallback()
{
  const char *Path;
  
  if (this->ConfigButton->GetFileName())
    {
    this->ConfigEntry->SetValue(this->ConfigButton->GetFileName());
    Path = this->ConfigButton->GetFileName();
    }
  else
    {
    Path = this->ConfigEntry->GetValue();
    
    }
  
   vtkSMStringVectorProperty *prop = vtkSMStringVectorProperty::SafeDownCast(
 	                             this->GetPVSource()->GetProxy()->GetProperty("animationPath"));    
	  if (prop)
	  	{
	  	//prop->SetElement(0, this->ConfigButton->GetFileName());
	  	prop->SetElement(0, Path);
	  	}
	  	
	  else
	    {
	  	vtkErrorMacro(<<"Cannot create \"apply\" filter property.");	  	
      return;
  		}
  
  this->ModifiedCallback();
}


// ----------------------------------------------------------------------------
//void vtkPVHMWavePropagationFilter::ConfigurationMenuCallback() 
//{
//  if (this->ConfigurationMenu->IsCreated())
//    {
//    const char *value = this->ConfigurationMenu->GetWidget()->GetValue();
//    if (!strcmp(value, MODE_FILE))
//      {
//			this->EdgesSizeEntry->ReadOnlyOn();
//			this->SizeElementsEntry->ReadOnlyOn();
//			this->RenumberingEntry->ReadOnlyOn();
//   		//this->SetWidgetMode((int) MODE_VISUALIZATION_VAL);
//      }
//    if (!strcmp(value, MODE_MANUAL))
//      {
//			this->EdgesSizeEntry->ReadOnlyOff();
//			this->SizeElementsEntry->ReadOnlyOff();
//			this->RenumberingEntry->ReadOnlyOff();
//  		//this->SetWidgetMode((int) MODE_EDITION_VAL);      		
//      }
//		}
//  this->ModifiedCallback();
//}

//----------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::Create(vtkKWApplication* app)
{
	vtkPVApplication* pvApp = vtkPVApplication::SafeDownCast(app);
 	vtkPVWindow *pvWindow = pvApp->GetMainWindow();	

  // Check if already created
  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
  this->MyFrame->SetParent(this);
  this->MyFrame->Create(pvApp);
  this->MyFrame->SetLabelText("HeMoLab Wave Propagation Filter");
  
  this->ScalarOption->SetParent(this->MyFrame->GetFrame());
  this->ScalarOption->Create(pvApp);
  this->ScalarOption->ReadOnlyOn();
  this->ScalarOption->AddValue("Pressure");
  this->ScalarOption->AddValue("Flow");
  this->ScalarOption->AddValue("Area");
  this->ScalarOption->AddValue("Velocity");
  this->ScalarOption->SetWidth(10);
  this->ScalarOption->SetValue("Pressure");
  this->ScalarOption->SetCommand(this, "ScalarOptionCallback");
  
  // Load Parameteres	
  this->ConfigButton->SetParent(this->MyFrame->GetFrame());
  this->ConfigButton->Create(pvApp);
  this->ConfigButton->GetLoadSaveDialog()->ChooseDirectoryOn();
  this->ConfigButton->SetCommand(this, "ConfigButtonCallback"); 
  this->ConfigButton->SetBalloonHelpString("Select the destination path where the animation files will be generated"); 


  this->ConfigLabel->SetParent(this->MyFrame->GetFrame());
  this->ConfigLabel->Create(pvApp);
  this->ConfigLabel->SetText("Destination animation files path");
  this->ConfigEntry->SetParent(this->MyFrame->GetFrame());
  this->ConfigEntry->Create(pvApp);
  
  //#ifdef _WIN32
  //  this->ConfigEntry->SetValue("c:/temp/");
  //  this->ConfigButton->SetText("c:/temp/"); 
  //#else
  //  this->ConfigEntry->SetValue("/tmp");
  //  this->ConfigButton->SetText("/tmp"); 
  //#endif
    
  //this->ConfigEntry->SetValue("None");
  this->ConfigEntry->ReadOnlyOn();

  this->StatusLabel->SetParent(this->MyFrame->GetFrame());
  this->StatusLabel->Create(pvApp);
  this->StatusLabel->SetBalloonHelpString("Status"); 
  this->StatusLabel->SetText("");
  
	//-------------------------------------------------------------------------------------      
	//Place components after accept
  this->PlaceComponents();
  this->ConfigureComponents(); 
}

//------------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::SetValueChanged()
{
  this->ModifiedCallback();
}

// ----------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::PlaceComponents()
{

  this->Script("pack %s -fill x -expand true",  
    this->MyFrame->GetWidgetName());

  this->Script("grid %s - - -sticky w",
    this->ScalarOption->GetWidgetName());

  //this->Script("grid %s %s %s - - -sticky ew",
  this->Script("grid %s %s - - -sticky ew",
    this->ConfigLabel->GetWidgetName(), 
    //this->ConfigEntry->GetWidgetName(),
    this->ConfigButton->GetWidgetName());        
}

// ----------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::ConfigureComponents()
{
  this->Script("grid columnconfigure %s 0 -weight 0", 
    this->MyFrame->GetWidgetName());
}

//-------------------------------------------------------------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::Accept()
{  	
	this->GetPVSource()->GetPVWindow()->GetProgressGauge()->SetBarColor(1, 0, 0);


	if(!this->ConfigButton->GetFileName())
	  {
	  //this->StatusLabel->SetText("You must select a path to save animation files!");
	  return;
	  }
   // this->ConfigButtonCallback(); 
	
	
	this->GetPVSource()->GetProxy()->UpdateVTKObjects();
    
    
//	string Path(this->ConfigButton->GetFileName());
//    
//  #ifdef _WIN32
//    Path.append("\0.vtp");
//  #else
//    Path.append("/0.vtp");
//  #endif
  
  //cout << "Path dos  arquivos "<< Path.c_str() << endl;  
    
//  char temp[strlen(Path.c_str())];
//  strcpy(temp, Path.c_str());
      
  //this->GetPVSource()->GetPVWindow()->Open(temp);
}

//----------------------------------------------------------------------------
void vtkPVHMWavePropagationFilter::Initialize()
{
	//this->GetPVSource()->GetPVWindow()->ShowAnimationPanes();
}  

