/*
 * $Id:$
 */

// .NAME vtkPVHMWavePropagationFilter
// .SECTION Description
// Interface for Wave propagation filter

#ifndef _vtkPVHMWavePropagationFilter_h_
#define _vtkPVHMWavePropagationFilter_h_

#include "vtkObjectFactory.h"
#include "vtkPVObjectWidget.h"
#include "vtkPVAnimationManager.h"
#include "vtkPVAnimationScene.h"

class vtkKWPushButton;  
class vtkKWEntry;
class vtkKWLabel;
class vtkKWLoadSaveButton;
class vtkKWMenuButtonWithLabel;
class vtkKWFrameWithLabel;
class vtkKWCheckButton;
class vtkKWComboBox;

class VTK_EXPORT vtkPVHMWavePropagationFilter : public vtkPVObjectWidget
{
public:
  static vtkPVHMWavePropagationFilter *New();
  vtkTypeRevisionMacro(vtkPVHMWavePropagationFilter,vtkPVObjectWidget);
  void PrintSelf(ostream& os, vtkIndent indent){};
	
  // Description:
  // Save this widget to a file.
  virtual void SaveInBatchScript(ofstream *file){};

  // Description:
  // Call TCL/TK commands to place KW Components
  void PlaceComponents();
  void ConfigureComponents(); 	
	
  // Description:
  // Call creation on the child.  
  virtual void 	Create (vtkKWApplication *app); 

  // Description:
  // Initialize the newly created widget.
  virtual void Initialize();
  
  // Description:
  // This serves a dual purpose.  For tracing and for saving state.
  virtual void Trace(ofstream *file) {};
    
  //BTX
  // Description:
  // Called when accept button is pushed.
  // Sets objects variable to the widgets value.
  // Side effect is to turn modified flag off.
  virtual void Accept();
  //ETX

  // Description:  
  // Show that something was changed
  void SetValueChanged();
  
  void ScalarOptionCallback();

  // Description:  
	// Menu and load file callback	
	void ConfigButtonCallback();
	//void ConfigurationMenuCallback();

  // Description:  
  // Check Buttons Callbacks
//	void AddNodesCallback();
//	void AntiElemCallback();
//	void QualityCallback();
	//void SeeAllCallback();
	
protected:
  vtkPVHMWavePropagationFilter();
  virtual ~vtkPVHMWavePropagationFilter();
  
  vtkKWFrameWithLabel *MyFrame;	 
  
  vtkKWComboBox* ScalarOption;

	//vtkKWMenuButtonWithLabel *ConfigurationMenu;

  // Config File Parameters
  vtkKWLoadSaveButton*	ConfigButton;  
  vtkKWLabel* 					ConfigLabel;
  vtkKWLabel* 					StatusLabel;
  vtkKWEntry* 					ConfigEntry;	
	
  //BTX
  int EditionMode;
  enum RadioButtonSelected 
  {
    eFlowButton=0,
    ePressureButton,
    eAreaButton,
    eVelocityButton
  };
//ETX
	 	
private:  
  vtkPVHMWavePropagationFilter(const vtkPVHMWavePropagationFilter&); // Not implemented
  void operator=(const vtkPVHMWavePropagationFilter&); // Not implemented
}; 

#endif  /*_vtkPVHMWavePropagationFilter_h_*/

