# $Id: CMakeLists.txt 912 2006-08-15 14:20:24Z rodrigo $
#
#====================== Nosso codigo ============================================
PROJECT(vtkhmKWWidgetsCS)

SET ( LIB_NAME		${PROJECT_NAME}   	)

SET( PKG_KWWIDGETS_SRC
	vtkKWHM1DXYPlotWidget.cxx
	vtkKWHM1DXYAnimatedPlotWidget.cxx
	vtkKWHMXYPlot.cxx
)


#==== Librarias utilizadas ============
# Libs de VTK/Paraview utilizadas
SET(EXT_LIBS
	vtkWidgetsCS
	vtkKWParaView
)

# Libs do HeMoLab utilizadas
SET(HM_LIBS
	vtkhmGraphics
	vtkhmCommon
	vtkhmIO
)

#====================== Nosso codigo ============================================
# Debo criar o instanciados das classes que vou utilizar desde o XML
VTK_MAKE_INSTANTIATOR3(hmKWWidgetsInstantiator hmKWWidgetsInstantiator_SRCS
  "${PKG_KWWIDGETS_SRC}"
  VTK_EXPORT
  "${vtkhmKWWidgetsCS_BINARY_DIR}" "")

#====================== Nosso codigo ============================================
# Cria os wrapper do cliente/servidor para o pacote, tambem cria uma lib chamada
# ${PROJECT_NAME}
VTK_WRAP_ClientServer(${PROJECT_NAME} CS_WRAP_SRCS "${PKG_KWWIDGETS_SRC}")

# Build the package as a plugin for ParaView.
ADD_LIBRARY(${PROJECT_NAME} 
	${CS_WRAP_SRCS} 
	${hmKWWidgetsInstantiator_SRCS} 
)

TARGET_LINK_LIBRARIES (${PROJECT_NAME} KWWidgets vtkCommonTCL)

FOREACH(c ${EXT_LIBS})
  TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${c})
ENDFOREACH(c)

FOREACH(c ${HM_LIBS})
  TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${c})
ENDFOREACH(c)

#SUBDIRS(testing)