#ifndef VTKHMGRAPHICPROPERTY_H_
#define VTKHMGRAPHICPROPERTY_H_

/* 
 * As variaveis definidas aqui, são para caso alguma altercao no numero
 * maximo de curvas, no tipo de plot ou nas cores das curvas, sejam alterados
 * automaticamente em todas as classes que as utilizam, tornando necessario
 * realizar suas alterações somente aqui.
 * Estas variaveis sao utilizadas pelas classes vtkKWHM1DXYPlotWidget e
 * vtkHMStraightModelWidget, elas sao usadas para colorir as curvas do plot
 * e os elementos clicados e tambem para definir numero maximo de curvas e
 * tipos de plots diferentes, por exemplo, pressure, flow, etc.
*/

//Variavel para converter pressao de dyn/cm² para mmHg
static const double ConvertPressure = 0.000751;

//Variavel para converter velocidade de cm/s para m/s
static const double ConvertVelocity = 0.01;

//Define the maximum number of curves inserted in one xyplot.
static const int MAX_CURVES=5;
//Define the number of different plots
static const int PLOTS=6;

//Description:
//Vector with color for curves of plot and elements clicked
static const double PlotColor[MAX_CURVES][3]={ {0.0, 0.4,  0.4},
																	{0.4, 0.4,  0.4},
																	{0.35, 0.12, 0.0},
																	{0.0, 0.0,  1.0},
																	{0.8, 0, 0.8}
																};
//{0.8, 0.49, 0.0}
static const double AnimatedPlotColor[3]={1.0, 0.5, 0.0};


static const char *NameOfGraphics[PLOTS]={"Pressure [dyn/cm^2]",
																				 "Pressure [mmHg]",
																				 "Area [cm^2]",
																				 "Flow rate [cm^3/s]",
																				 "Velocity [cm/s]",
																				 "Velocity [m/s]"};

#endif /*VTKHMGRAPHICPROPERTY_H_*/
