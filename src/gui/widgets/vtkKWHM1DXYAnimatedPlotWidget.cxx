/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkKWHM1DXYAnimatedPlotWidget.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWHM1DXYAnimatedPlotWidget.h"

#include "vtkHMDataObjectToPlot.h"
#include "vtkHMXYPlot.h"
#include "vtkHMGraphicProperty.h"

#include "vtkObjectFactory.h"
#include "vtkDoubleArray.h"
#include "vtkCallbackCommand.h"

#include "vtkKWObject.h"
#include "vtkCoordinate.h"
#include "vtkIntArray.h"
#include "vtkCommand.h"

#include "vtkDataArrayCollection.h"

#include "vtkKWApplication.h"
#include "vtkKWLabel.h"
#include "vtkKWRenderWidget.h"
#include "vtkRenderWindow.h"
#include "vtkKWWindow.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWEntry.h"
#include "vtkKWRange.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWPushButton.h"
#include "vtkKWTkUtilities.h"
#include "vtkKWSplitFrame.h"
#include "vtkKWToolbar.h"
#include "vtkWindowToImageFilter.h"
#include "vtkKWEntryWithLabel.h"

#include "vtkProperty2D.h"
#include "vtkAxisActor2D.h"

#include "vtkImageData.h"
#include "vtkImageWriter.h"
#include "vtkErrorCode.h"
#include "vtkInstantiator.h"

#include "vtkMPEG2Writer.h"

//#include "vtkMultiThreader.h"
//#include "vtkMutexLock.h"

//#include <X11/Xlib.h> //Usado para chamar a função XInitThreads()

#include <cmath>

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWHM1DXYAnimatedPlotWidget );
vtkCxxRevisionMacro(vtkKWHM1DXYAnimatedPlotWidget, "$Revision: 1.37 $");

//----------------------------------------------------------------------------
vtkKWHM1DXYAnimatedPlotWidget::vtkKWHM1DXYAnimatedPlotWidget()
{
	
	this->HMXYPlot = vtkHMXYPlot::New();
	this->HMXYAnimatedPlot = vtkHMXYPlot::New();
	
	vtkProperty2D *property = vtkProperty2D::New();
	vtkTextProperty * textProperty = vtkTextProperty::New();
	
	property->SetColor(1.0, 1.0, 1.0);
	textProperty->SetColor(1.0, 1.0, 1.0);

	this->HMXYPlot->GetYAxisActor2D()->SetProperty(property);
	this->HMXYPlot->GetXAxisActor2D()->SetProperty(property);
	this->HMXYPlot->SetProperty(property);
	this->HMXYPlot->SetTitleTextProperty(textProperty);
	this->HMXYPlot->SetAxisTitleTextProperty(textProperty);
	this->HMXYPlot->SetAxisLabelTextProperty(textProperty);

	vtkTextProperty *animatedTextProperty = vtkTextProperty::New();
	animatedTextProperty->SetOpacity(0);
	this->HMXYAnimatedPlot->SetTitleTextProperty(animatedTextProperty);
	this->HMXYAnimatedPlot->SetAxisTitleTextProperty(animatedTextProperty);
	this->HMXYAnimatedPlot->SetAxisLabelTextProperty(animatedTextProperty);	
	
	vtkProperty2D *animatedProperty = vtkProperty2D::New();
	animatedProperty->SetColor(1.0, 1.0, 1.0);
	animatedProperty->SetOpacity(1);
	
	animatedProperty->SetPointSize(5);
	
	this->HMXYAnimatedPlot->GetYAxisActor2D()->SetProperty(animatedProperty);
	this->HMXYAnimatedPlot->GetXAxisActor2D()->SetProperty(animatedProperty);
	this->HMXYAnimatedPlot->SetProperty(animatedProperty);
	
 	property->Delete();
 	textProperty->Delete();
 	
	animatedProperty->Delete();
 	animatedTextProperty->Delete();
 
 	this->HMXYPlot->SetPlotColor(0, AnimatedPlotColor[0], AnimatedPlotColor[1], AnimatedPlotColor[2]);
// 	Para efeito de informacao
// 	AnimatedPlotColor[0]=1.0; AnimatedPlotColor[1]=0.5; AnimatedPlotColor[2]=0;
 	this->HMXYAnimatedPlot->SetPlotColor(0, AnimatedPlotColor[0], AnimatedPlotColor[0], AnimatedPlotColor[2]);

  this->Time = vtkDoubleArray::New();
  this->Time->SetNumberOfComponents(1);
  
  this->MaxPressDyn 	= -10000000;
  this->MaxPressMMHG 	= -10000000;
  this->MaxArea 			= -10000000;
  this->MaxFlow 			= -10000000;
  this->MaxVelocityCM	= -10000000;
  this->MaxVelocityM	= -10000000;
  this->MinPressDyn 	= 10000000;
  this->MinPressMMHG 	= 10000000;
  this->MinArea 			= 10000000;
  this->MinFlow 			= 10000000;
  this->MinVelocityCM	= 10000000;
  this->MinVelocityM	= 10000000;
  
  this->KWRenderWidget = NULL;
  
  this->SplitFrame = NULL;
  
  PressionDyn = vtkDoubleArray::New();
  PressionDyn->SetNumberOfComponents(1);
   
  PressionMMHG = vtkDoubleArray::New();
  PressionMMHG->SetNumberOfComponents(1);
   
  Area = vtkDoubleArray::New();
  Area->SetNumberOfComponents(1);
 
  Flow = vtkDoubleArray::New();
  Flow->SetNumberOfComponents(1);
			
  VelocityCMs = vtkDoubleArray::New();
  VelocityCMs->SetNumberOfComponents(1);
	  
  VelocityMs = vtkDoubleArray::New();
  VelocityMs->SetNumberOfComponents(1);
  
  AnimatedPlotType = 0;
  
//	this->AnimatedThreader = vtkMultiThreader::New();
// 	this->AnimatedThreadID = -1;
// 	
// 	XInitThreads(); //Para habilitar multithread no servidor gráfico.
// 	
// 	this->SpawnedThreadActiveFlagLock = vtkMutexLock::New();
// 	this->CloseAnimatedPlot=0;
  this->FrameOfLaps = NULL;
  this->LabelOfLaps = NULL;
  this->EntryOfLaps = NULL;
  this->ButtonOfLapsON = NULL;
  this->ButtonOfSave = NULL;
  this->LabelTypeMeshID = NULL;
  this->LabelMax = NULL;
  this->LabelMin = NULL;
  this->LabelOfTimeStepShift = NULL;
  this->EntryOfTimeStepShift = NULL;
}

//----------------------------------------------------------------------------
vtkKWHM1DXYAnimatedPlotWidget::~vtkKWHM1DXYAnimatedPlotWidget()
{
  this->Time->Delete();
  
 	if ( this->HMXYPlot )
 		{
 		this->HMXYPlot->RemoveAllInputs();
	  this->HMXYPlot->Delete();
    this->HMXYPlot = NULL;
 		}

 	if ( this->HMXYAnimatedPlot )
 		{
 		this->HMXYAnimatedPlot->RemoveAllInputs();
	  this->HMXYAnimatedPlot->Delete();
    this->HMXYAnimatedPlot = NULL;
 		}
 	
  if ( this->KWRenderWidget )
  	{
	  this->KWRenderWidget->RemoveAllViewProps();
	  this->KWRenderWidget->Delete();
  	}
  this->KWRenderWidget = NULL;
  
  if ( this->SplitFrame )
  	this->SplitFrame->Delete();
  this->SplitFrame = NULL;

	PressionDyn->Delete();
	PressionMMHG->Delete();
	Area->Delete();
	Flow->Delete();
	VelocityCMs->Delete();
	VelocityMs->Delete();
	
	if(this->FrameOfLaps)
		this->FrameOfLaps->Delete();
	this->FrameOfLaps = NULL;
	
	if(this->LabelOfLaps)
		this->LabelOfLaps->Delete();
	this->LabelOfLaps = NULL;
	
  if(this->EntryOfLaps)
  	this->EntryOfLaps->Delete();
  this->EntryOfLaps = NULL;
  
  if(this->ButtonOfLapsON)
  	this->ButtonOfLapsON->Delete();
  this->ButtonOfLapsON = NULL;
  
  if(this->ButtonOfSave)
  	this->ButtonOfSave->Delete();
  this->ButtonOfSave = NULL;

//  if(this->ButtonOfStop)
//  	this->ButtonOfStop->Delete();
//  this->ButtonOfStop = NULL;
  
  if(this->LabelTypeMeshID)
  	this->LabelTypeMeshID->Delete();
  this->LabelTypeMeshID = NULL;
  
  if(this->LabelMax)
  	this->LabelMax->Delete();
  this->LabelMax = NULL;
  
  if(this->LabelMin)
  	this->LabelMin->Delete();
  this->LabelMin = NULL;
  
  if(this->LabelOfTimeStepShift)
  	this->LabelOfTimeStepShift->Delete();
  this->LabelOfTimeStepShift = NULL;
  
  if(this->EntryOfTimeStepShift)
  	this->EntryOfTimeStepShift->Delete();
  this->EntryOfTimeStepShift = NULL;
  
//	if (this->AnimatedThreadID != -1)
//	  this->AnimatedThreader->TerminateThread(this->AnimatedThreadID);
//	this->AnimatedThreader->Delete();
//	
//	this->SpawnedThreadActiveFlagLock->Delete();
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::Create(vtkKWApplication *app)
{
  // Check if already created
  if (this->IsCreated())
    return;

  // Call the superclass to create the whole widget
  this->Superclass::Create(app);
	
	//Cria o frame
	this->SplitFrame = vtkKWSplitFrame::New();
	this->SplitFrame->SetParent(this);
	this->SplitFrame->Create(app);
	this->SplitFrame->SetFrame1Size(200);
	this->Script("pack %s -side right -fill both -expand t",
               this->SplitFrame->GetWidgetName());
	
	//Cria a render widget do frame da direita
	this->KWRenderWidget = vtkKWRenderWidget::New();
  this->KWRenderWidget->SetParent(this->SplitFrame->GetFrame2());
  this->KWRenderWidget->Create(app);
  
  //Cria o frame da esquerda
	this->FrameOfLaps = vtkKWFrame::New();
  this->FrameOfLaps->SetParent(this->SplitFrame->GetFrame1());
  this->FrameOfLaps->Create(app);
	this->Script("grid %s -sticky nw", this->FrameOfLaps->GetWidgetName());
  
  this->LabelMin = vtkKWLabel::New();
  this->LabelMin->SetParent(this->FrameOfLaps);
  this->LabelMin->Create(app);
  this->Script("grid %s -row 0 -column 0 -sticky w", this->LabelMin->GetWidgetName());

  this->LabelMax = vtkKWLabel::New();
  this->LabelMax->SetParent(this->FrameOfLaps);
  this->LabelMax->Create(app);
  this->Script("grid %s -row 1 -column 0 -sticky w", this->LabelMax->GetWidgetName());
  
  this->LabelTypeMeshID = vtkKWLabel::New();
  this->LabelTypeMeshID->SetParent(this->FrameOfLaps);
  this->LabelTypeMeshID->Create(app);
  this->Script("grid %s -row 2 -column 0 -sticky w", this->LabelTypeMeshID->GetWidgetName());

	this->LabelOfLaps = vtkKWLabel::New();
  this->LabelOfLaps->SetParent(this->FrameOfLaps);
  this->LabelOfLaps->Create(app);
  this->LabelOfLaps->SetText("Cardiac Beats (default 10):");
  this->Script("grid %s -row 3 -column 0 -sticky w", this->LabelOfLaps->GetWidgetName());
	
  this->EntryOfLaps = vtkKWEntry::New();
  this->EntryOfLaps->SetParent(this->FrameOfLaps);
  this->EntryOfLaps->Create(app);
  this->EntryOfLaps->SetWidth(10);
  this->EntryOfLaps->SetValueAsInt(10);
  this->Script("grid %s -row 4 -column 0 -sticky ew", this->EntryOfLaps->GetWidgetName());
	
  this->ButtonOfLapsON = vtkKWPushButton::New();
  this->ButtonOfLapsON->SetParent(this->FrameOfLaps);
  this->ButtonOfLapsON->Create(app);
  this->ButtonOfLapsON->SetText("Simulate");
  this->ButtonOfLapsON->SetWidth(10);
  this->Script("grid %s -row 5 -column 0 -sticky ew", this->ButtonOfLapsON->GetWidgetName());
  this->ButtonOfLapsON->SetCommand(this, "Start");
 
	this->LabelOfTimeStepShift = vtkKWLabel::New();
  this->LabelOfTimeStepShift->SetParent(this->FrameOfLaps);
  this->LabelOfTimeStepShift->Create(app);
  this->LabelOfTimeStepShift->SetText("Autput Timestep Shift (default 1):");
  this->Script("grid %s -row 6 -column 0 -sticky w", this->LabelOfTimeStepShift->GetWidgetName());

  this->EntryOfTimeStepShift = vtkKWEntry::New();
  this->EntryOfTimeStepShift->SetParent(this->FrameOfLaps);
  this->EntryOfTimeStepShift->Create(app);
  this->EntryOfTimeStepShift->SetWidth(10);
  this->EntryOfTimeStepShift->SetValueAsInt(1);
  this->Script("grid %s -row 7 -column 0 -sticky ew", this->EntryOfTimeStepShift->GetWidgetName());
   
  this->ButtonOfSave = vtkKWPushButton::New();
  this->ButtonOfSave->SetParent(this->FrameOfLaps);
  this->ButtonOfSave->Create(app);
  this->ButtonOfSave->SetText("Save");
  this->ButtonOfSave->SetWidth(10);
  this->Script("grid %s -row 8 -column 0 -sticky ew", this->ButtonOfSave->GetWidgetName());
  this->ButtonOfSave->SetCommand(this, "Save");

//  this->ButtonOfStop = vtkKWPushButton::New();
//  this->ButtonOfStop->SetParent(this->FrameOfLaps);
//  this->ButtonOfStop->Create(app);
//  this->ButtonOfStop->SetText("WriteMovie");
//  this->ButtonOfStop->SetWidth(10);
//  this->Script("grid %s -row 9 -column 0 -sticky ew", this->ButtonOfStop->GetWidgetName());
//  this->ButtonOfStop->SetCommand(this, "WriteMovie");
  
  //Seta a cor de fundo da renderer
  this->KWRenderWidget->SetRendererBackgroundColor(0.0, 0.0, 0.0);
  
  this->Script("pack %s -side top -expand 1 -fill both",
               this->KWRenderWidget->GetWidgetName());
	
  this->Script("set commandList \"\"");
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::Display()
{
  this->Superclass::Display();
	this->KWRenderWidget->Render();
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//--------------------------------------------------------------------
//Este método retorna uma referência
vtkHMXYPlot* vtkKWHM1DXYAnimatedPlotWidget::GetHMXYPlot()
{
  return this->HMXYPlot;
}

void vtkKWHM1DXYAnimatedPlotWidget::SetHMXYPlot(vtkHMXYPlot* plot)
{
	this->HMXYPlot = plot;
}

//--------------------------------------------------------------------
//Este método retorna uma referência
vtkHMXYPlot* vtkKWHM1DXYAnimatedPlotWidget::GetHMXYAnimatedPlot()
{
  return this->HMXYAnimatedPlot;
}

void vtkKWHM1DXYAnimatedPlotWidget::SetHMXYAnimatedPlot(vtkHMXYPlot* animatedPlot)
{
	this->HMXYAnimatedPlot = animatedPlot;
}

//--------------------------------------------------------------------
double vtkKWHM1DXYAnimatedPlotWidget::MaxValue(vtkDoubleArray *array)
{
	double max = array->GetValue(0);
	double tuple;
	
	for ( int i=1; i<array->GetNumberOfTuples(); i++ )
		{
	  tuple = array->GetValue(i);
	  
	  //Armazenando o valor máximo
	  if(tuple > max)
	    max = tuple;
		}
	
	return max;
}

//--------------------------------------------------------------------
double vtkKWHM1DXYAnimatedPlotWidget::MinValue(vtkDoubleArray *array)
{
	double min = array->GetValue(0);
	double tuple;
	
	for ( int i=1; i<array->GetNumberOfTuples(); i++ )
		{
	  tuple = array->GetValue(i);
	    
	  //Armazenando o valor minimo
	  if(tuple < min)
	  	min = tuple;
		}
	return min;
}

////--------------------------------------------------------------------
//VTK_THREAD_RETURN_TYPE vtkThreadedExecuteAnimatedPlot( void *arg )
//{
//	int threadId, threadCount;
//	vtkKWHM1DXYAnimatedPlotWidget *localXYAnimatedPlotWidget;
//	
//	threadId = ((vtkMultiThreader::ThreadInfo *)(arg))->ThreadID;
//  threadCount = ((vtkMultiThreader::ThreadInfo *)(arg))->NumberOfThreads;
//
////  vtkDoubleArray *localTime = localXYAnimatedPlotWidget->GetTime();
////  cout << "GetTime: " << localXYAnimatedPlotWidget->GetTime() << endl;
////  cout << "localTime: " << localTime << endl;
////  vtkKWRenderWidget *localKWRenderWidget = localXYAnimatedPlotWidget->GetKWRenderWidget();
////  vtkHMXYPlot *localHMXYAnimatedPlot = localXYAnimatedPlotWidget->GetHMXYAnimatedPlot();
////  int localAnimatedPlotType = localXYAnimatedPlotWidget->GetAnimatedPlotType();
//  
//  localXYAnimatedPlotWidget = (vtkKWHM1DXYAnimatedPlotWidget *)(((vtkMultiThreader::ThreadInfo *)(arg))->UserData);
//  localXYAnimatedPlotWidget->Display();  
//// 	vtkIntArray *typePlot = localXYPlotWidget->GetTypePlot();
//
////  for(int i=0; i<localTime->GetNumberOfTuples(); i++)
////		{
////// 		system("sleep 0.05");
////		localKWRenderWidget->RemoveViewProp(localHMXYAnimatedPlot);
////		localKWRenderWidget->Render();
////// 		cria o ponto que será animado por cima do plot modelo
////		localXYAnimatedPlotWidget->BuildAnimatedPlot(localAnimatedPlotType, i);
////		localKWRenderWidget->AddViewProp(localHMXYAnimatedPlot);
////		localXYAnimatedPlotWidget->Display();
////		}
//  cout << "vtkThreadedExecuteAnimatedPlot antes do FOR:" << endl;
//  for(int i=0; i<localXYAnimatedPlotWidget->GetTime()->GetNumberOfTuples(); i++)
//		{
//// 		system("sleep 0.05");
//		localXYAnimatedPlotWidget->KWRenderWidget->RemoveViewProp(localXYAnimatedPlotWidget->HMXYAnimatedPlot);
//		localXYAnimatedPlotWidget->KWRenderWidget->Render();
//// 		cria o ponto que será animado por cima do plot modelo
//		localXYAnimatedPlotWidget->BuildAnimatedPlot(localXYAnimatedPlotWidget->AnimatedPlotType, i);
//		localXYAnimatedPlotWidget->KWRenderWidget->AddViewProp(localXYAnimatedPlotWidget->HMXYAnimatedPlot);
//		localXYAnimatedPlotWidget->Display();
////		if(i==localXYAnimatedPlotWidget->GetTime()->GetNumberOfTuples()-1)
////			i=0;
//		}
//  cout << "vtkThreadedExecuteAnimatedPlot depois do FOR:" << endl;
//  localXYAnimatedPlotWidget->SpawnedThreadActiveFlagLock->Unlock();
//  return VTK_THREAD_RETURN_VALUE;
//}

//--------------------------------------------------------------------
//Este método constrói um gráfico 2D a partir de dois doubleArray recebido como parâmetro.
vtkHMXYPlot* vtkKWHM1DXYAnimatedPlotWidget::MakeHMXYPlotWidget(vtkDoubleArray *doubleArray, vtkDoubleArray* timeInstant, char type, int meshID)
{
//	this->SpawnedThreadActiveFlagLock->Lock();

	this->SplitFrame->UnpackChildren();
	
	this->Script("pack %s -side top -expand 1 -fill both",
               this->KWRenderWidget->GetWidgetName());
	this->KWRenderWidget->RemoveAllViewProps();
	
	Time->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
	Flow->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
	Area->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
  PressionDyn->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
  PressionMMHG->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
  VelocityCMs->SetNumberOfTuples(doubleArray->GetNumberOfTuples());
  VelocityMs->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

  
	char temp[20];
	sprintf(temp, "Object and MeshID:  %c - %d ", type, meshID);
	this->LabelTypeMeshID->SetText(temp);

  
  //Ponteiro auxiliar para ajudar na filtragem dos dados de pressão, área e fluxo.
  double *tmpPointer;
  tmpPointer = NULL;
    
  //Tratamento para data out com três tuplas.
  if(doubleArray->GetNumberOfComponents() == 3)
  	{    
    //Filtrando os dados.
		for (vtkIdType i = 0; i < doubleArray->GetNumberOfTuples(); i++)
			{
		  tmpPointer = doubleArray->GetTuple(i);
	
	    //Configurando arrays.
	  	Flow->SetTuple(i, &tmpPointer[0]);
	  	Area->SetTuple(i, &tmpPointer[1]);
		  PressionDyn->SetTuple(i, &tmpPointer[2]);
		  PressionMMHG->SetValue(i, PressionDyn->GetValue(i)*ConvertPressure);
		  Time->SetTuple(i, timeInstant->GetTuple(i));
			}
  	}
  //Tratando arquivos com 4 tuplas.
  else if(doubleArray->GetNumberOfComponents() == 4)
	  {
		//Filtrando dados.
		for (int i = 0; i < doubleArray->GetNumberOfTuples(); i++)
			{
		  tmpPointer = doubleArray->GetTuple(i);
		
		  Area->SetTuple(i, &tmpPointer[2]);
		  Flow->SetTuple(i, &tmpPointer[0]);
		  PressionDyn->SetTuple(i, &tmpPointer[3]);
		  PressionMMHG->SetValue(i, PressionDyn->GetValue(i)*ConvertPressure);
		  Time->SetTuple(i, timeInstant->GetTuple(i));
			}
  	}
  
  //criar arrays de velocidade
  vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();	
	
	DataPlot->MaxAndMinVelocity(DataPlot->MakeVelocityCurve(Flow, Area));
	vtkDoubleArray *velocity = DataPlot->MakeVelocityCurve(Flow, Area);
	VelocityCMs->DeepCopy(velocity);
	velocity->Delete();
	
	//Converte velocidade de cm/s para m/s
	for ( int i=0; i<VelocityCMs->GetNumberOfTuples(); i++ )
		VelocityMs->SetValue(i, VelocityCMs->GetValue(i)*ConvertVelocity);
	
	DataPlot->Delete();
	
  //Criar o plot modelo
	this->BuildPlot(this->AnimatedPlotType, type, meshID);
	
  //Define p tamanho da janela.
 	this->SetSize(800,400);
  HMXYPlot->GetPositionCoordinate()->SetValue(0.05, 0.26, 0); //Configurando sua coordenada na ViewPort.
  HMXYPlot->SetHeight(0.6);
  HMXYAnimatedPlot->GetPositionCoordinate()->SetValue(0.05, 0.26, 0); //Configurando sua coordenada na ViewPort.
  HMXYAnimatedPlot->SetHeight(0.6);
	
 	this->KWRenderWidget->AddViewProp(this->HMXYPlot);
 	this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot); 	 	

 	this->BuildAnimatedPlot(this->AnimatedPlotType, 0);
 	
 	this->Display();
// 	this->AnimatedThreadID = this->AnimatedThreader->SpawnThread(vtkThreadedExecuteAnimatedPlot, (void *)this);
 	
// 	this->AnimatedThreader->SetSingleMethod(vtkThreadedExecuteAnimatedPlot,(void*)(this));
// 	this->AnimatedThreader->SingleMethodExecute();
 	
 	
//  for(int i=0; i<this->GetTime()->GetNumberOfTuples(); i++)
//		{
//// 		system("sleep 0.05");
//		this->KWRenderWidget->RemoveViewProp(this->HMXYAnimatedPlot);
//		this->KWRenderWidget->Render();
//// 		cria o ponto que será animado por cima do plot modelo
//		this->BuildAnimatedPlot(this->AnimatedPlotType, i);
//		this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot);
//		this->Display();
//		if(i==this->GetTime()->GetNumberOfTuples()-1)
//			i=0;
//		}
 	
 	
// 	this->AnimatedPlotThread();
 	
  //Retornando a referência para os plots.
  return NULL;
}//End of method.	

//--------------------------------------------------------------------
//Este método procura o tipo de plot a ser exibido e constrói o mesmo.
void vtkKWHM1DXYAnimatedPlotWidget::BuildPlot(int id, char type, int meshID)
{
  //Pressão dyn/cm²
  if(id == 0)
  	{
  	HMXYPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->PressionDyn, this->Time, 
  				 this->Time->GetNumberOfTuples(), 0));
  	DataPlot->Delete();

  	double maxPressDyn = this->MaxValue(this->PressionDyn);
		double minPressDyn = this->MinValue(this->PressionDyn);
		
    //Configura as propriedades do plot.
		HMXYPlot->SetYRange(minPressDyn, maxPressDyn);
    HMXYPlot->SetYTitle("Pressure \n [dyn/cm^2]");
    HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Pressure [dyn/cm^2] vs Time   %c - %d", type, meshID);
		HMXYPlot->SetTitle(title);
    HMXYPlot->SetDataObjectYComponent(1, 1);

		char min[20];
		sprintf(min, "Min: %f", minPressDyn);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxPressDyn);
		this->LabelMax->SetText(max);
  	} //Fim if(id == 0)
        
  //Pressao mmHg
  else if(id == 1)
  	{
		HMXYPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->PressionMMHG, this->Time, 
  				 this->Time->GetNumberOfTuples(), 0));
  	DataPlot->Delete();

		double maxPressMMHG = this->MaxValue(this->PressionMMHG);
		double minPressMMHG = this->MinValue(this->PressionMMHG);
	  	
    //Configurando as propriedades do plot.
    HMXYPlot->SetYRange(minPressMMHG, maxPressMMHG);
    HMXYPlot->SetYTitle("Pressure \n [mmHg]");
    HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Pressure [mmHg] vs Time   %c - %d", type, meshID);
    HMXYPlot->SetTitle(title);
  	HMXYPlot->SetDataObjectYComponent(1, 1);

		char min[20];
		sprintf(min, "Min: %f", minPressMMHG);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxPressMMHG);
		this->LabelMax->SetText(max);
  	} //Fim else if(id == 1)
  
  //Area
  else if(id == 2)
  	{
		HMXYPlot->RemoveAllInputs();
	  
//Adiciona as curvas ao plot
	  vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->Area, this->Time, 
	  			 this->Time->GetNumberOfTuples(), 0));
	  DataPlot->Delete();
 
	  double maxArea = this->MaxValue(this->Area);
	  double minArea = this->MinValue(this->Area);
	  	
    //Configurando as propriedades do plot.
    HMXYPlot->SetLabelFormat("%1.3g");
    HMXYPlot->SetYRange(minArea, maxArea);
    HMXYPlot->SetYTitle("Area \n [cm^2]");
    HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Area vs Time   %c - %d", type, meshID);
    HMXYPlot->SetTitle(title);
  	HMXYPlot->SetDataObjectYComponent(1, 1);
  	
		char min[20];
		sprintf(min, "Min: %f", minArea);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxArea);
		this->LabelMax->SetText(max);
  	} //Fim else if(id == 2)
       
  //Fluxo
  else if(id == 3)
  	{
		HMXYPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->Flow, this->Time, 
  				 this->Time->GetNumberOfTuples(), 0));
  	DataPlot->Delete();

		double maxFlow = this->MaxValue(this->Flow);
		double minFlow = this->MinValue(this->Flow);
	  	
  	//Configurando as propriedades do plot.
  	HMXYPlot->SetLabelFormat("%1.2g");
    HMXYPlot->SetYRange(minFlow, maxFlow);
	  HMXYPlot->SetYTitle("Flow rate \n [cm^3/s]");
		HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Flow rate vs Time   %c - %d", type, meshID);
		HMXYPlot->SetTitle(title);
  	HMXYPlot->SetDataObjectYComponent(1, 1);

		char min[20];
		sprintf(min, "Min: %f", minFlow);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxFlow);
		this->LabelMax->SetText(max);
  	} //Fim else if(id == 3)
       
  //Velocidade cm/s
  else if(id == 4)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYPlot->SetLabelFormat("%1.2g");
		HMXYPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->VelocityCMs, this->Time, 
  				 this->Time->GetNumberOfTuples(), 0));
		DataPlot->Delete();

		double maxVelocityCM = this->MaxValue(this->VelocityCMs);
		double minVelocityCM = this->MinValue(this->VelocityCMs);
			
		//Configurando as propriedades do plot.
		HMXYPlot->SetYRange(minVelocityCM, maxVelocityCM);
		HMXYPlot->SetYTitle("Velocity \n [cm/s]");
		HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Velocity [cm/s] vs Time   %c - %d", type, meshID);
		HMXYPlot->SetTitle(title);
		HMXYPlot->SetDataObjectYComponent(1, 1);

		char min[20];
		sprintf(min, "Min: %f", minVelocityCM);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxVelocityCM);
		this->LabelMax->SetText(max);
  	} //Fim else if(id == 4)
   
   //Velocidade m/s
  else if(id == 5)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYPlot->SetLabelFormat("%1.2g");
		HMXYPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->VelocityMs, this->Time, 
  				 this->Time->GetNumberOfTuples(), 0));
		DataPlot->Delete();

		double maxVelocityM = this->MaxValue(this->VelocityMs);
		double minVelocityM = this->MinValue(this->VelocityMs);
			
		////Configurando as propriedades do plot.
		HMXYPlot->SetYRange(minVelocityM, maxVelocityM);
		HMXYPlot->SetYTitle("Velocity \n [m/s]");
		HMXYPlot->SetXTitle("Time \n [s]");
    char title[50];
		sprintf(title, "Velocity [m/s] vs Time   %c - %d", type, meshID);
		HMXYPlot->SetTitle(title);	
		HMXYPlot->SetDataObjectYComponent(1, 1);

		char min[20];
		sprintf(min, "Min: %f", minVelocityM);
		this->LabelMin->SetText(min);
		
		char max[20];
		sprintf(max, "Max: %f", maxVelocityM);
		this->LabelMax->SetText(max);
  	} //Fim else if(id == 5)
}

//Este método procura o tipo de plot a ser exibido e constrói o mesmo.
void vtkKWHM1DXYAnimatedPlotWidget::BuildAnimatedPlot(int id, int point)
{
  //Pressão dyn/cm²
  if(id == 0)
  	{
		HMXYAnimatedPlot->RemoveAllInputs();	

  	//  		Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->PressionDyn, this->Time, 
  			1, point));

		double maxPressDyn = this->MaxValue(this->PressionDyn);
		double minPressDyn = this->MinValue(this->PressionDyn);
  	
  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minPressDyn, maxPressDyn);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));

  	DataPlot->Delete();
  	
    //Configura as propriedades do plot.
    HMXYAnimatedPlot->SetYTitle("Pressure \n [dyn/cm^2]");
    HMXYAnimatedPlot->SetXTitle("Time \n [s]");
    HMXYAnimatedPlot->SetTitle("Pressure [dyn/cm^2] vs Time");
		
    HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
    
  	} //Fim if(id == 0)
        
  //Pressao mmHg
  else if(id == 1)
  	{
		HMXYAnimatedPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->PressionMMHG, this->Time, 
  			1, point));

		double maxPressMMHG = this->MaxValue(this->PressionMMHG);
		double minPressMMHG = this->MinValue(this->PressionMMHG);
	  	
	  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minPressMMHG, maxPressMMHG);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));

	  	
	  	DataPlot->Delete();
   
    //Configurando as propriedades do plot.
    HMXYAnimatedPlot->SetYTitle("Pressure \n [mmHg]");
    HMXYAnimatedPlot->SetXTitle("Time \n [s]");
    HMXYAnimatedPlot->SetTitle("Pressure [mmHg] vs Time	");

  	HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
  	
  	} //Fim else if(id == 1)
  
  //Area
  else if(id == 2)
  	{
		HMXYAnimatedPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->Area, this->Time, 
  			1, point));
  	
		double maxArea = this->MaxValue(this->Area);
		double minArea = this->MinValue(this->Area);
	  	
	  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minArea, maxArea);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));

	  	DataPlot->Delete();
  
    //Configurando as propriedades do plot.
    HMXYAnimatedPlot->SetLabelFormat("%1.3g");
    HMXYAnimatedPlot->SetYTitle("Area \n [cm^2]");
    HMXYAnimatedPlot->SetXTitle("Time \n [s]");
    HMXYAnimatedPlot->SetTitle("Area vs Time	");
  	HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
  	
  	} //Fim else if(id == 2)
       
  //Fluxo
  else if(id == 3)
  	{
		HMXYAnimatedPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->Flow, this->Time, 
  				 1, point));
	  	
		double maxFlow = this->MaxValue(this->Flow);
		double minFlow = this->MinValue(this->Flow);
  	
  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minFlow, maxFlow);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));
	  	
	  	DataPlot->Delete();
	  
  	//Configurando as propriedades do plot.
  	HMXYAnimatedPlot->SetLabelFormat("%1.2g");
	  HMXYAnimatedPlot->SetYTitle("Flow rate \n [cm^3/s]");
		HMXYAnimatedPlot->SetXTitle("Time \n [s]");
		HMXYAnimatedPlot->SetTitle("Flow rate vs Time	");
//		sprintf(LabelOfLegend, "Max: %7f \n Min: %7f", this->GetMaxFlow(), this->GetMinFlow());

  	HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
  	
  	} //Fim else if(id == 3)
       
  //Velocidade cm/s
  else if(id == 4)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYAnimatedPlot->SetLabelFormat("%1.2g");
		HMXYAnimatedPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->VelocityCMs, this->Time, 
  				 1, point));
  	
		double maxVelocityCM = this->MaxValue(this->VelocityCMs);
		double minVelocityCM = this->MinValue(this->VelocityCMs);
  	
  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minVelocityCM, maxVelocityCM);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));

		DataPlot->Delete();
	  		
		//Configurando as propriedades do plot.
		HMXYAnimatedPlot->SetYTitle("Velocity \n [cm/s]");
		HMXYAnimatedPlot->SetXTitle("Time \n [s]");
		HMXYAnimatedPlot->SetTitle("Velocity [cm/s] vs Time	");
		
		HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
		
  	} //Fim else if(id == 4)
   
   //Velocidade m/s
  else if(id == 5)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYAnimatedPlot->SetLabelFormat("%1.2g");
		HMXYAnimatedPlot->RemoveAllInputs();
	  
  //Adiciona as curvas ao plot
  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
  	HMXYAnimatedPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(this->VelocityMs, this->Time, 
  				 1, point));

		double maxVelocityM = this->MaxValue(this->VelocityMs);
		double minVelocityM = this->MinValue(this->VelocityMs);
  	
  	//Setar o maximo e minimo
  	HMXYAnimatedPlot->SetYRange(minVelocityM, maxVelocityM);
  	HMXYAnimatedPlot->SetXRange(this->Time->GetValue(0), this->Time->GetValue(this->Time->GetNumberOfTuples()-10));
  	
  	DataPlot->Delete();
	  
		////Configurando as propriedades do plot.
//		HMXYAnimatedPlot->SetYRange(this->GetMinVelocityM(), this->GetMaxVelocityM());
		HMXYAnimatedPlot->SetYTitle("Velocity \n [m/s]");
		HMXYAnimatedPlot->SetXTitle("Time \n [s]");
		HMXYAnimatedPlot->SetTitle("Velocity [m/s] vs Time	");	
		
		HMXYAnimatedPlot->SetDataObjectYComponent(1, 1);
		
  	} //Fim else if(id == 5)
}

//--------------------------------------------------------------------
//void vtkKWHM1DXYAnimatedPlotWidget::RebuildPlot()
//{
//	this->KWRenderWidget->RemoveAllViewProps();
//  this->KWRenderWidget->Render();
//  
//	//Variável local para controlar as coordenadas dos plots.
//	int position=0;
//	
//	//Variável local para armazenar o número de plots a serem exibidos.
//	int tmpNumberOfPlots = 0;
//	
//	//Este loop controla o número de referências para vtkHMXYPlot que devem ser alocadas.
//	for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
//		{ 
//	   //Identifica plots ativos.
//	   if(this->GetTypePlot()->GetValue(i) == 1)
//	  	 {
//	     tmpNumberOfPlots += 1;
//	  	 }
//		}
//	
//  this->SetNumberOfPlots(tmpNumberOfPlots);
//	if ( (this->NumberOfPlots == 0) || (this->NumberOfGraphics == 0))
//		{
//		this->ShowLegends();
//		return;
//		}
//	
//	//Esta condicional verifica se o número de plots a serem exibidos é igual a 1,
//	//se for, redimensiona a janela de acordo ajusta a coordenada do plot.
////	if(this->GetNumberOfPlots() == 1)
////		{
////		//Define p tamanho da janela.
//		this->SetSize(800,500);
////		//Verificando o(s) plot(s) ativo(s).
////		for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
////			{
////  	  if(this->GetTypePlot()->GetValue(i) == 1)
////  	  	{
//		    HMXYPlot->GetPositionCoordinate()->SetValue(0.05, 0.26, 0); //Configurando sua coordenada na ViewPort.
//				
//	      HMXYPlot->SetHeight(0.6);
////  	  	}
////			}
////		}
//	
//	//Este condicional verifica se o número de plots a serem exibidos é igual a 2,
//  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
////  else if(this->GetNumberOfPlots() == 2)
////  	{
////  	//Este array armazena as duas posições do plot, utilizado para controlar a atribuição das coordenadas.
////    position = 0;
////    
////    //Define p tamanho da janela.
////    this->SetSize(800,600);
////    //Verificando o(s) plot(s) ativo(s).
////    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
////    	{	
////  	  if(this->GetTypePlot()->GetValue(i) == 1)
////  	  	{
////        HMXYPlot[i]->SetHeight(0.4);
////        
////        //Posição para o primeiro plot.
////        if(position == 0)
////        {
////          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.55, 0);
////          position += 1;
////        }
////        
////        else if(position != 0)
////          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
////  	  	} 
////	  	}
////	  }
//	
//  //Este condicional verifica se o número de plots a serem exibidos é igual a 3,
//  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
////  else if(this->GetNumberOfPlots() == 3)
////  	{
////  	position = 0;
////  	
////  	//Define p tamanho da janela.
////    this->SetSize(800,800);
////    
////    //Verificando o(s) plot(s) ativo(s).
////    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
////    	{
////  	  if(this->GetTypePlot()->GetValue(i) == 1)
////  	  	{
////        HMXYPlot[i]->SetHeight(0.27);
////  	    
////  	    //Posição para o primeiro plot.
////        if(position == 0)
////        	{
////          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.65, 0);
////          position += 1;
////        	}
////        
////        //Posição para o segundo plot.
////        else if(position == 1)
////        	{
////          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.35, 0);
////          position += 1;
////        	}
////        
////        //Posição para o terceiro plot.
////        else if(position == 2)
////          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
////        
////  	  	}
////	  	}
////	  }
//	
//  //Este condicional verifica se o número de plots a serem exibidos é igual a 4,
//  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
////  else if(this->GetNumberOfPlots() == 4)
////  	{
////  	position = 0;
////  	
////  	//Define p tamanho da janela.
////    this->SetSize(800,800);
////    
////    //Verificando o(s) plot(s) ativo(s).
////    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
////    	{	
////  	  if(this->GetTypePlot()->GetValue(i) == 1)
////  	  	{
////        this->HMXYPlot[i]->SetHeight(0.2);
////  	  	  	    
////  	    //Posição para o primeiro plot.
////        if(position == 0)
////        	{
////          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.80, 0);
////          position += 1;
////        	}
////        
////        //Posição para o segundo plot.
////        else if(position == 1)
////        	{
////          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.55, 0);
////          position += 1;
////        	}
////        
////        //Posição para o terceiro plot.
////        else if(position == 2)
////        	{
////          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.30, 0);
////          position += 1;
////        	}
////        
////        //posição para o quarto plot.
////        else if(position == 3)
////          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
////        
////  	  	}
////	  	}  
////	  }
////	
////	for ( int i=0; i<PLOTS; i++ )
////		{
////		if ( this->GetTypePlot()->GetValue(i) == 1 )
////			{
////			HMXYPlot->RemoveAllInputs();
//		  
//		  //Adiciona as curvas ao plot
////		  for ( int k=0; k<this->NumberOfGraphics; k++ )
////		  	{
////		  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
////		  	HMXYPlot->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(, this->Time, 
////		  				 this->Time->GetNumberOfTuples(), 0));
////		  	DataPlot->Delete();
////		  	}
//		  	
////			}
////		}
//	
////	for ( int i=0; i<PLOTS; i++ )
////		if( this->TypePlot->GetValue(i))
//    	this->KWRenderWidget->AddViewProp(this->HMXYPlot);
//	
//	this->ShowHMXYPlotWidget();
//}

//--------------------------------------------------------------------
//Este método exibe o plot em uma vtkRenderWindow.
void vtkKWHM1DXYAnimatedPlotWidget::ShowHMXYPlotWidget()
{
	int j = 0;
	for(int i=0; i<this->GetTime()->GetNumberOfTuples(); i++)
		{
		this->KWRenderWidget->RemoveViewProp(this->HMXYAnimatedPlot);
		this->KWRenderWidget->Render();
	// 		cria o ponto que será animado por cima do plot modelo
		this->BuildAnimatedPlot(this->AnimatedPlotType, ++++i);
		this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot);
//		this->Display();
		this->KWRenderWidget->Render();
		if((i==this->GetTime()->GetNumberOfTuples())
			||(i==this->GetTime()->GetNumberOfTuples()-1)
			||(i==this->GetTime()->GetNumberOfTuples()-2))
			{
				i=0;
				j++;
			}
		if(j==this->NumberOfLaps)
			return;
		}

//  this->KWRenderWidget->ResetCamera();
//  this->Display();
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetPressionDyn(vtkDoubleArray *p)
{
	this->PressionDyn = p;
}

vtkDoubleArray* vtkKWHM1DXYAnimatedPlotWidget::GetPressionDyn()
{
	return this->PressionDyn;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetPressionMMHG(vtkDoubleArray *p)
{
	this->PressionMMHG = p;
}

vtkDoubleArray* vtkKWHM1DXYAnimatedPlotWidget::GetPressionMMHG()
{
	return this->PressionMMHG;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetArea(vtkDoubleArray *a)
{
	this->Area = a;
}

vtkDoubleArray* vtkKWHM1DXYAnimatedPlotWidget::GetArea()
{
	return this->Area;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetFlow(vtkDoubleArray *f)
{
	this->Flow = f;
}

vtkDoubleArray* vtkKWHM1DXYAnimatedPlotWidget::GetFlow()
{
	return this->Flow;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetTime(vtkDoubleArray *t)
{
	this->Time = t;
}

vtkDoubleArray* vtkKWHM1DXYAnimatedPlotWidget::GetTime()
{
	return this->Time;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetTypePlot(int type)
{
	this->AnimatedPlotType = type;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::SetKWRenderWidget(vtkKWRenderWidget *ren)
{
	this->KWRenderWidget = ren;
}

vtkKWRenderWidget *vtkKWHM1DXYAnimatedPlotWidget::GetKWRenderWidget()
{
	return this->KWRenderWidget;
}

void vtkKWHM1DXYAnimatedPlotWidget::Start()
{
	if( (!this->EntryOfLaps->GetValueAsInt()) || (this->EntryOfLaps->GetValueAsInt() <= 0) )
		this->NumberOfLaps = 10;
	else
		this->NumberOfLaps = this->EntryOfLaps->GetValueAsInt();
	
	this->ShowHMXYPlotWidget();
	
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYAnimatedPlotWidget::Save()
{
//********************************************************
//Esse bacalhau é feito por causa de um bug de interface
//do paraview que anula o entry depois que eu obtenho o seu
//valor para a variável jumpAux, daí ele altera o valor 
//do jumpAux quando anula o entry gerando um segmentation 
//fault... lamentável.
//tenho que copiar o valor uma segunda vez para a variavel
//jump, e essa segunda variável não é alterada
	int jumpAux = 1;
	int jump = 1;
	if(this->EntryOfTimeStepShift)
		{
		jumpAux = this->EntryOfTimeStepShift->GetValueAsInt();
		jump = jumpAux;
		}
//********************************************************
	
	char *filename;
  int j=0;
//  this->Script("tk_getSaveFile -filetypes {{{Text} {.txt}}} -defaultextension .txt -initialfile ParaViewLog.txt");
//  this->Script("tk_getSaveFile -filetypes {{{Image} {.jpg}}} -defaultextension .jpg");
  this->Script("tk_getSaveFile -filetypes {{{Image} {jpg}}}");
  filename = new char[strlen(this->GetApplication()->GetMainInterp()->result)+1];
  sprintf(filename, "%s", this->GetApplication()->GetMainInterp()->result);
  
  if (strcmp(filename, "") == 0)
    {
    delete [] filename;
    return;
    }
  
  for(int i=0; i<this->GetTime()->GetNumberOfTuples(); i+=jump)
		{
		this->KWRenderWidget->RemoveViewProp(this->HMXYAnimatedPlot);
	// 		cria o ponto que será animado por cima do plot modelo
		this->BuildAnimatedPlot(this->AnimatedPlotType, i);
		this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot);
		this->KWRenderWidget->Render();
		
		char *temp = new char[strlen(this->GetApplication()->GetMainInterp()->result)+10];
		sprintf(temp, "%s%d%s", filename, j, ".jpg");
	  j++;

	  this->KWRenderWidget->GetRenderWindow()->SetDesiredUpdateRate(0.002);
	  this->WriteImage(temp, "vtkJPEGWriter");
	  delete [] temp;

	  if( (i+jump > this->GetTime()->GetNumberOfTuples()) && (i < this->GetTime()->GetNumberOfTuples()-1) )
	  	{
			this->KWRenderWidget->RemoveViewProp(this->HMXYAnimatedPlot);
		// 		cria o ponto que será animado por cima do plot modelo
			this->BuildAnimatedPlot(this->AnimatedPlotType, this->GetTime()->GetNumberOfTuples()-1);
			this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot);
			this->KWRenderWidget->Render();
			
			char *temp = new char[strlen(this->GetApplication()->GetMainInterp()->result)+10];
			sprintf(temp, "%s%d%s", filename, j, ".jpg");
		  j++;

		  this->KWRenderWidget->GetRenderWindow()->SetDesiredUpdateRate(0.002);
		  this->WriteImage(temp, "vtkJPEGWriter");
		  delete [] temp;
	  	}
		}

}

//----------------------------------------------------------------------------
int vtkKWHM1DXYAnimatedPlotWidget::WriteImage(const char* filename, const char* writerName)
{
  if (!filename || !writerName)
    {
    return vtkErrorCode::UnknownError;
    }

  vtkObject* object = vtkInstantiator::CreateInstance(writerName);
  if (!object)
    {
    vtkErrorMacro("Failed to create Writer " << writerName);
    return vtkErrorCode::UnknownError;
    }
  vtkImageWriter* writer = vtkImageWriter::SafeDownCast(object);
  if (!writer)
    {
    vtkErrorMacro("Object is not a vtkImageWriter: " << object->GetClassName());
    object->Delete();
    return vtkErrorCode::UnknownError;
    }

  vtkImageData* shot = this->CaptureWindow(1);
  writer->SetInput(shot);
  writer->SetFileName(filename);
  writer->Write();
  int error_code = writer->GetErrorCode();

  writer->Delete();
  shot->Delete();
//  object->Delete();
  
  return error_code;
}

//void vtkKWHM1DXYAnimatedPlotWidget::WriteMovie()
//{
//	char *filename;
//	this->Script("tk_getSaveFile -filetypes {{{Movie} {.mpg}}} -defaultextension .mpg");
//	filename = new char[strlen(this->GetApplication()->GetMainInterp()->result)+1];
//	sprintf(filename, "%s", this->GetApplication()->GetMainInterp()->result);
//	
//	if (strcmp(filename, "") == 0)
//	  {
//	  delete [] filename;
//	  return;
//	  }
//	
//  vtkImageMandelbrotSource* Shot = vtkImageMandelbrotSource::New();
//  Shot->SetWholeExtent( 0, 247, 0, 247, 0, 0 );
//  Shot->SetProjectionAxes( 0, 1, 2 );
//  Shot->SetOriginCX( -1.75, -1.25, 0, 0 );
//  Shot->SetSizeCX( 2.5, 2.5, 2, 1.5 );
//  Shot->SetMaximumNumberOfIterations( 100);
//
//	
////	vtkAlgorithmOutput *alg = vtkAlgorithmOutput::New();
////	alg->GetOutputPort(this->KWRenderWidget);
//	
//  vtkImageCast* cast = vtkImageCast::New();
//  cast->SetInputConnection(Shot);
//  cast->SetOutputScalarTypeToUnsignedChar();
//
//  vtkLookupTable* table = vtkLookupTable::New();
//  table->SetTableRange(0, 100);
//  table->SetNumberOfColors(100);
//  table->Build();
//  table->SetTableValue(99, 0, 0, 0);
//
//  vtkImageMapToColors* colorize = vtkImageMapToColors::New();
//  colorize->SetOutputFormatToRGB();
//  colorize->SetLookupTable(table);
//  colorize->SetInputConnection(cast->GetOutputPort());
//	
//	
//	vtkMPEG2Writer *write = vtkMPEG2Writer::New();
//  write->SetInputConnection(colorize->GetOutputPort());
//  write->SetFileName(filename);
//	write->Start();
//	for(int i=0; i<this->GetTime()->GetNumberOfTuples(); i++)
//		{
//		this->KWRenderWidget->RemoveViewProp(this->HMXYAnimatedPlot);
//	//			this->KWRenderWidget->Render();
//	// 		cria o ponto que será animado por cima do plot modelo
//		this->BuildAnimatedPlot(this->AnimatedPlotType, i);
//		this->KWRenderWidget->AddViewProp(this->HMXYAnimatedPlot);
//		this->KWRenderWidget->Render();
//		write->Write();
//		}
//	write->End();
//	
//	delete [] filename;
//	write->Delete();
//	shot->Delete();
//
//}

//-----------------------------------------------------------------------------
vtkImageData* vtkKWHM1DXYAnimatedPlotWidget::CaptureWindow(int magnification)
{
  // I am using the vtkPVRenderView approach for saving the image.
  // instead of vtkSMDisplayWindowProxy approach of creating a proxy.
  this->KWRenderWidget->GetRenderWindow()->SetOffScreenRendering(1);
  this->KWRenderWidget->GetRenderWindow()->SwapBuffersOff();

  vtkWindowToImageFilter* w2i = vtkWindowToImageFilter::New();
  w2i->SetInput(this->KWRenderWidget->GetRenderWindow());
  w2i->SetMagnification(magnification);
  w2i->ReadFrontBufferOff();
  w2i->ShouldRerenderOff();
  w2i->Update();

  vtkImageData* capture = vtkImageData::New();
  capture->ShallowCopy(w2i->GetOutput());
  w2i->Delete();

  this->KWRenderWidget->GetRenderWindow()->SwapBuffersOn();
  this->KWRenderWidget->GetRenderWindow()->Frame();
  this->KWRenderWidget->GetRenderWindow()->SetOffScreenRendering(0);

  return capture;
}
