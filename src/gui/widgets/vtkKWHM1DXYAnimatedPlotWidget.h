/*
 * $Id$
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkKWHM1DXYAnimatedPlotWidget

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/
// .NAME vtkKWHM1DXYAnimatedPlotWidget - Used to show an XY plot with informations about 
//vtkKWHM1DXYAnimatedPlotWidget.
// .SECTION Description
//The class implement methods to show XYPlot with information about terminals and segments.
//e.g: Pressure, area, flow.

#ifndef VTKKWHM1DXYANIMATEDPLOTWIDGET_H_
#define VTKKWHM1DXYANIMATEDPLOTWIDGET_H_

#include "vtkKWTopLevel.h"

#include "vtkDoubleArray.h"
#include "vtkCommand.h"
#include "vtkIntArray.h"

#include "vtkInteractorStyleSwitch.h"
#include "vtkKWApplication.h"
#include "vtkKWLabel.h"
#include "vtkKWRenderWidget.h"
#include "vtkKWWindow.h"
#include "vtkRenderWindow.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWRange.h"
#include "vtkKWPushButton.h"

#include "vtkHMGraphicProperty.h"

#include "vtkMPEG2Writer.h"
#include "vtkImageMandelbrotSource.h"
#include "vtkImageCast.h"
#include "vtkLookupTable.h"
#include "vtkImageMapToColors.h"
#include "vtkAlgorithmOutput.h"
#include "vtkAlgorithm.h"

#include <stdlib.h>

using namespace std;

class vtkImageData;
class vtkKWSplitFrame;
class vtkKWToolbar;

//Para ser usado no futuro no acréscimo de ballon helps!!!
//#include "vtkBalloonWidget.h"

class vtkHMDataObjectToPlot;   //This class manage data to plot curve.
class vtkHMXYPlot;             //This class manage all this properties to an XYPlot.
class vtkDataArrayCollection;

class VTK_EXPORT vtkKWHM1DXYAnimatedPlotWidget : public vtkKWTopLevel
{
public:
  static vtkKWHM1DXYAnimatedPlotWidget* New();
  vtkTypeRevisionMacro(vtkKWHM1DXYAnimatedPlotWidget, vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Create the widget.
  virtual void Create(vtkKWApplication *app);
  
  // Description:
  // Display the toplevel.
  virtual void Display();

  //Description:
  //This method make an HMXYPlotWidget composed by curves.
  vtkHMXYPlot* MakeHMXYPlotWidget(vtkDoubleArray *doubleArray, vtkDoubleArray *timeInstant, char type, int meshID);
  
  //Description:
  //Show the make plot in the ViewPort.
  void ShowHMXYPlotWidget();
  
  //Description:
  //This method retrieve the HMXYPlot with a reference to this class.
  vtkHMXYPlot* GetHMXYPlot();
  void SetHMXYPlot(vtkHMXYPlot*);
  
  //Description:
  //This method retrieve the HMXYPlot with a reference to this class.
  vtkHMXYPlot* GetHMXYAnimatedPlot();
  void SetHMXYAnimatedPlot(vtkHMXYPlot*);
  
  //Description:
  //This method build plot from received id.
	void BuildPlot(int id, char type, int meshID);

  //Description:
  //This method build plot from received id.
	void BuildAnimatedPlot(int id, int point);
//	void BuildAnimatedPlot(int id);
	
	//Description:
  //This method build plot from received id.
//	void RebuildPlot();
	
  //Description:
  //This method set the type of plot to show.
  void SetTypePlot(int type);
  
  //Description:
  //This method get the type plot selected.
  vtkIntArray* GetTypePlot()
  	{
  	return TypePlot;
  	}
  
  //Description:
  //Methods Set/Get to TimeOfMax.
  vtkSetMacro(TMax, double);
  vtkGetMacro(TMax, double);  
  
  //Description:
  //Methods Set/Get to TimeOfMin.
  vtkSetMacro(TMin, double);
  vtkGetMacro(TMin, double);
  
  //Description:
  //Methods Set/Get to MaxPress in dyn/cm².
  vtkSetMacro(MaxPressDyn, double);
  vtkGetMacro(MaxPressDyn, double);
  //Description:
  //Methods Set/Get to MinPress in dyn/cm².
  vtkSetMacro(MinPressDyn, double);
  vtkGetMacro(MinPressDyn, double);
  
  //Description:
  //Methods Set/Get to MaxPress in mmHg.
  vtkSetMacro(MaxPressMMHG, double);
  vtkGetMacro(MaxPressMMHG, double);
  //Description:
  //Methods Set/Get to MinPress in mmHg.
  vtkSetMacro(MinPressMMHG, double);
  vtkGetMacro(MinPressMMHG, double);
  
  //Description:
  //Methods Set/Get to MaxArea.
  vtkSetMacro(MaxArea, double);
  vtkGetMacro(MaxArea, double);
  //Description:
  //Methods Set/Get to MinArea.
  vtkSetMacro(MinArea, double);
  vtkGetMacro(MinArea, double);
  
  //Description:
  //Methods Set/Get to MaxFlow.
  vtkSetMacro(MaxFlow, double);
  vtkGetMacro(MaxFlow, double);
  //Description:
  //Methods Set/Get to MinFlow.
  vtkSetMacro(MinFlow, double);
  vtkGetMacro(MinFlow, double);
  
  //Description:
  //Methods Set/Get to MaxVelocityCM.
  vtkSetMacro(MaxVelocityCM, double);
  vtkGetMacro(MaxVelocityCM, double);
  //Description:
  //Methods Set/Get to MinVelocityCM.
  vtkSetMacro(MinVelocityCM, double);
  vtkGetMacro(MinVelocityCM, double);
  
  //Description:
  //Methods Set/Get to MaxVelocityM.
  vtkSetMacro(MaxVelocityM, double);
  vtkGetMacro(MaxVelocityM, double);
  //Description:
  //Methods Set/Get to MinVelocityM.
  vtkSetMacro(MinVelocityM, double);
  vtkGetMacro(MinVelocityM, double);

  //Description:
  //Methods Set/Get to AnimatedPlotType.
  vtkSetMacro(AnimatedPlotType, int);
  vtkGetMacro(AnimatedPlotType, int);
  
  //Description:
  //Methods Set/Get to Pression Dyn/cm².
  void SetPressionDyn(vtkDoubleArray *p);
	vtkDoubleArray* GetPressionDyn();
	
	//Description:
  //Methods Set/Get to Pression mmHg.
	void SetPressionMMHG(vtkDoubleArray *p);
	vtkDoubleArray* GetPressionMMHG();

	//Description:
  //Methods Set/Get to Area.
	void SetArea(vtkDoubleArray *a);
	vtkDoubleArray* GetArea();

	//Description:
  //Methods Set/Get to Flow.
	void SetFlow(vtkDoubleArray *f);
	vtkDoubleArray* GetFlow();
	
	//Description:
  //Methods Set/Get to Time
	void SetTime(vtkDoubleArray *t);
	vtkDoubleArray* GetTime();
	
	//Description:
  //Methods Set/Get to Maximum of all points of the tree,
  //this value exist in DataOut Object
//	void SetMax(double *m, int t);
//	double *GetMax();
	
  // Description:
	// Set/Get KWRenderWidget
	void SetKWRenderWidget(vtkKWRenderWidget *ren);
	vtkKWRenderWidget *GetKWRenderWidget();
	
	//Description:
  //This is a reference to the class HMXYPlot(Handle properties of Plot).
  vtkHMXYPlot *HMXYAnimatedPlot;

  //Description:
  //store a type of animated plot to show   
  int AnimatedPlotType;
  
  /*
   * 
   * Components from interface
   * 
   */
  
  // Description:
  // Frame with render are adder.
  vtkKWSplitFrame *SplitFrame;
  
  // Description:
  // Render for plot
  vtkKWRenderWidget *KWRenderWidget;
  
  //Description:
  //Start animation in the render
  void Start();
  
  //Description:
  //Saves the animation in jpeg file. 1 jpeg for each update of the render
  //Get the path for save the file, uses one loop for update the window and 
  //call the WhiteImage function below that saves the jpeg file in the path
  void Save();
  
  //Description:
  //Call the CaptureWindow function below, and saves this image in directory
  //The directory is passed in filename, writerName load the name of writer 
  //that will create the file
  int WriteImage(const char* filename, const char* writerName);
  
  //Description:
  //Used for get the photo of the window, and return the image.
  //Use magnification for definition of the size of the image
  vtkImageData* CaptureWindow(int magnification);  
//  void WriteMovie();

  
protected:
  vtkKWHM1DXYAnimatedPlotWidget();
  ~vtkKWHM1DXYAnimatedPlotWidget();
	
	//Description:
  //This is a reference to the class HMXYPlot(Handle properties of Plot).
  vtkHMXYPlot *HMXYPlot;
  
  //Description:
  //This variable define the type of Plot to show in ViewPort.
  vtkIntArray *TypePlot;
  
  //Description:
  //This variable represent the instant of time of Max(Pression, Area, Flow).
  double TMax;
  
  //Description:
  //This variable represent the instant of time of Min(Pression, Area, Flow).
  double TMin;
  
  //Description:
  //This variable represent the Max value to Pression in Dyn/cm².
  double MaxPressDyn;
  //Description:
  //This variable represent the Min value to Pression in Dyn/cm².
  double MinPressDyn;
  
  //Description:
  //This variable represent the Max value to Pression in mmHg.
  double MaxPressMMHG;
  //Description:
  //This variable represent the Min value to Pression in mmHg.
  double MinPressMMHG;
  
  //Description:
  //This variable represent the Max value to Area.
  double MaxArea;
  //Description:
  //This variable represent the Min value to Area.
  double MinArea;
  
  //Description:
  //This variable represent the Max value to Flow.
  double MaxFlow;
  //Description:
  //This variable represent the Min value to Flow.
  double MinFlow;
  
  //Description:
  //This variable represent the Max value to velocity cm/s.
  double MaxVelocityCM;
  //Description:
  //This variable represent the Min value to velocity cm/s.
  double MinVelocityCM;
  
  //Description:
  //This variable represent the Max value to velocity m/s.
  double MaxVelocityM;
  //Description:
  //This variable represent the Min value to velocity m/s.
  double MinVelocityM;
  
  //Description:
  //Store pression curve in Dyn/cm².
  vtkDoubleArray* PressionDyn;
  
  //Description:
  //Store pression curve in mmHg.
  vtkDoubleArray* PressionMMHG;
    
  //Description:
  //Store area curve.
  vtkDoubleArray* Area;
  
  //Description:
  //Store flow curve.
  vtkDoubleArray* Flow;
  
  //Description:
  //Store time curve.
  vtkDoubleArray* Time;
  
  //Description:
  //Store velocity curve.
  vtkDoubleArray *VelocityCMs;
  vtkDoubleArray *VelocityMs;
  
	//Description:
	//Return maximum value of vtkDoubleArray
  double MaxValue(vtkDoubleArray *array);
  
  //Description:
	//Return minimum value of vtkDoubleArray
  double MinValue(vtkDoubleArray *array);
    
  // Description:
  //Collection for curves
  vtkDataArrayCollection *DataArrayCollection[PLOTS];
  
// 	vtkMultiThreader *AnimatedThreader;
//	int AnimatedThreadID;
  
  int NumberOfLaps;
  

  vtkKWLabel *LabelMax;
  vtkKWLabel *LabelMin;

  vtkKWLabel *LabelTypeMeshID;
    
  vtkKWFrame *FrameOfLaps;
  
  vtkKWLabel *LabelOfLaps;
  vtkKWEntry *EntryOfLaps;
  vtkKWPushButton *ButtonOfLapsON;
  vtkKWPushButton *ButtonOfSave;
  
  vtkKWPushButton *ButtonOfStop;
  
  vtkKWLabel *LabelOfTimeStepShift;
  vtkKWEntry *EntryOfTimeStepShift;
  
  
private:
  vtkKWHM1DXYAnimatedPlotWidget(const vtkKWHM1DXYAnimatedPlotWidget&); // Not implemented
  void operator=(const vtkKWHM1DXYAnimatedPlotWidget&); // Not implemented
};

#endif /*VTKKWHM1DXYANIMATEDPLOTWIDGET_H_*/
