/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkKWHM1DXYPlotWidget.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWHM1DXYPlotWidget.h"

#include "vtkHMDataObjectToPlot.h"
#include "vtkHMXYPlot.h"
#include "vtkHMGraphicProperty.h"

#include "vtkObjectFactory.h"
#include "vtkDoubleArray.h"
#include "vtkCallbackCommand.h"

#include "vtkKWObject.h"
#include "vtkCoordinate.h"
#include "vtkIntArray.h"
#include "vtkCommand.h"

#include "vtkDataArrayCollection.h"

#include "vtkKWApplication.h"
#include "vtkKWLabel.h"
#include "vtkKWRenderWidget.h"
#include "vtkRenderWindow.h"
#include "vtkKWWindow.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWEntry.h"
#include "vtkKWRange.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWPushButton.h"
#include "vtkKWTkUtilities.h"
#include "vtkKWSplitFrame.h"
#include "vtkKWToolbar.h"
#include "vtkWindowToImageFilter.h"
#include "vtkKWEntryWithLabel.h"

#include "vtkProperty2D.h"
#include "vtkAxisActor2D.h"

#include "vtkImageData.h"
#include "vtkImageWriter.h"
#include "vtkErrorCode.h"
#include "vtkInstantiator.h"

#include <ostream>

#include <cmath>

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWHM1DXYPlotWidget );
vtkCxxRevisionMacro(vtkKWHM1DXYPlotWidget, "$Revision: 1.37 $");

//----------------------------------------------------------------------------
vtkKWHM1DXYPlotWidget::vtkKWHM1DXYPlotWidget()
{
  this->SaveButton = vtkKWPushButton::New();
  this->ClearButton = vtkKWPushButton::New();
  this->ExportResultButton = vtkKWPushButton::New();
  this->ExportDialog = vtkKWLoadSaveDialog::New();

	this->NumberOfGraphics = 0;
	this->NumberOfGraphicsSum = 0;

	vtkProperty2D *property = vtkProperty2D::New();
  for ( int i=0; i<PLOTS; i++ )
  	{
  	this->HMXYPlot[i] = vtkHMXYPlot::New();

		property->SetColor(0,0,0);

		this->HMXYPlot[i]->GetYAxisActor2D()->SetProperty(property);
		this->HMXYPlot[i]->GetXAxisActor2D()->SetProperty(property);
		this->HMXYPlot[i]->SetProperty(property);

  	for ( int j=0; j<MAX_CURVES; j++ )
  	 	this->HMXYPlot[i]->SetPlotColor(j, 	PlotColor[j][0],
																					PlotColor[j][1],
																					PlotColor[j][2]);

		this->DataArrayCollection[i] = vtkDataArrayCollection::New();
  	}
  property->Delete();

  this->pressionDyn = NULL;
  this->pressionMMHG = NULL;
  this->area = NULL;
  this->flow = NULL;
  this->time = NULL;

  this->TypePlot = vtkIntArray::New();
  this->TypePlot->SetNumberOfComponents(1);
  this->TypePlot->SetNumberOfTuples(PLOTS);
  this->TypePlot->SetValue(0, 1);
  this->TypePlot->SetValue(1, 0);
  this->TypePlot->SetValue(2, 0);
  this->TypePlot->SetValue(3, 0);
  this->TypePlot->SetValue(4, 0);
  this->TypePlot->SetValue(5, 0);

  this->time = vtkDoubleArray::New();
  this->time->SetNumberOfComponents(1);

  for (int i = 0; i<MAX_CURVES; i++)
  	{
	  this->MaxPressDyn[i] 	= -10000000;
	  this->PressDynHideShowMAX[i] = -10000000;

	  this->MaxPressMMHG[i] 	= -10000000;
	  this->PressMMHGHideShowMAX[i] = -10000000;

  	this->MaxArea[i] 			= -10000000;
  	this->AreaHideShowMAX[i] = -10000000;

  	this->MaxFlow[i] 			= -10000000;
  	this->FlowHideShowMAX[i] = -10000000;

  	this->MaxVelocityCM[i]	= -10000000;
  	this->VelocityCMHideShowMAX[i] = -10000000;

  	this->MaxVelocityM[i]	= -10000000;
  	this->VelocityMHideShowMAX[i] = -10000000;


  	this->MinPressDyn[i] 	= 10000000;
	  this->PressDynHideShowMIN[i] = 10000000;

    this->MinPressMMHG[i] 	= 10000000;
    this->PressMMHGHideShowMIN[i] = 10000000;

    this->MinArea[i] 			= 10000000;
    this->AreaHideShowMIN[i] = 10000000;

    this->MinFlow[i] 			= 10000000;
    this->FlowHideShowMIN[i] = 10000000;

    this->MinVelocityCM[i]	= 10000000;
    this->VelocityCMHideShowMIN[i] = 10000000;

    this->MinVelocityM[i]	= 10000000;
    this->VelocityMHideShowMIN[i] = 10000000;
  	}
  this->MaxPressDynControl 	= -10000000;
  this->MaxPressMMHGControl 	= -10000000;
  this->MaxAreaControl 			= -10000000;
  this->MaxFlowControl 			= -10000000;
  this->MaxVelocityCMControl	= -10000000;
  this->MaxVelocityMControl	= -10000000;
  this->MAX_FLOW = -10000000;

  this->MinPressDynControl 	= 10000000;
  this->MinPressMMHGControl 	= 10000000;
	this->MinAreaControl 			= 10000000;
  this->MinFlowControl 			= 10000000;
  this->MinVelocityCMControl	= 10000000;
  this->MinVelocityMControl	= 10000000;
  this->MIN_FLOW = 10000000;

  this->KWRenderWidget = NULL;

	this->KWFrameWithScrollbar = NULL;
	this->KWTimeRange = NULL;
  this->KWValueRange = NULL;
  this->KWUpdateRangeButton = NULL;
  this->KWRemoveButton = NULL;
  this->Toolbar = NULL;
  this->SplitFrame = NULL;

  this->MaxCurves = MAX_CURVES;

  this->PiecewiseFunction = NULL;
	this->KWPiecewiseFunctionEditor = NULL;

  this->EditCurveHeart = false;

  this->HeartCurve = vtkDoubleArray::New();

  this->HeartAreaLabel1 = vtkKWLabel::New();
  this->HeartAreaEntry1 = vtkKWEntry::New();

  this->HeartAreaLabel2 = vtkKWLabel::New();
  this->HeartAreaEntry2 = vtkKWEntry::New();

  this->CalculateAreaButton = vtkKWPushButton::New();

  this->ScaleFactor = 1;

  for(int i=0; i<MAX_CURVES; i++)
	{
  this->MeshIDForTitle[i] = -1;
	this->TypeForTitle[i] = '1';
//	this->BoolShowHideCheckButton[i] = true;
	this->ShowHideCheckButton[i] = vtkKWCheckButton::New();
	this->TitleToGraphic[i] = vtkKWLabel::New();
	}
  this->KWTitleToGraphicFrame = vtkKWFrame::New();

  this->KWFrameEditYAxis = vtkKWFrameWithLabel::New();
  for ( int i=0; i<PLOTS; i++ )
	  {
	  this->KWYAxisEntryMin[i] = vtkKWEntry::New();
	  this->KWYAxisEntryMax[i] = vtkKWEntry::New();
	  this->KWYAxisLabelMin[i] = vtkKWLabel::New();
	  this->KWYAxisLabelMax[i] = vtkKWLabel::New();
	  this->KWYAxisFrameWithLabel[i] = vtkKWFrameWithLabel::New();
		}

  this->PlotOptions		 = vtkIntArray::New();
  this->PlotOptions->SetNumberOfValues(PLOTS);

}

//----------------------------------------------------------------------------
vtkKWHM1DXYPlotWidget::~vtkKWHM1DXYPlotWidget()
{
	this->TypePlot->Delete();
  this->time->Delete();

  for ( int i=0; i<PLOTS; i++ )
  	if ( this->HMXYPlot[i] )
  		{
  		this->HMXYPlot[i]->RemoveAllInputs();
	    this->HMXYPlot[i]->Delete();
	    this->HMXYPlot[i] = NULL;

	    this->DataArrayCollection[i]->RemoveAllItems();
	    this->DataArrayCollection[i]->Delete();
	    this->DataArrayCollection[i] = NULL;
  		}

	if ( this->KWFrameWithScrollbar )
		{
		for ( int i=0; i<MAX_CURVES; i++ )
			{
			for ( int j=0; j<PLOTS; j++ )
				{
				this->Label[i][j]->Delete();
				this->Label[i][j] = NULL;
				}
		  this->KWFrameWithLabel[i]->GetFrame()->RemoveAllChildren();
		  this->KWFrameWithLabel[i]->RemoveAllChildren();
			this->KWFrameWithLabel[i]->Delete();
			this->KWFrameWithLabel[i] = NULL;
			}
		}

	if ( this->KWFrameWithScrollbar )
		{
	  this->KWFrameWithScrollbar->GetFrame()->RemoveAllChildren();
	  this->KWFrameWithScrollbar->RemoveAllChildren();
		this->KWFrameWithScrollbar->Delete();
		}
	this->KWFrameWithScrollbar = NULL;

	if ( this->KWTimeRange )
		this->KWTimeRange->Delete();
	this->KWTimeRange = NULL;

  if ( this->KWValueRange )
  	this->KWValueRange->Delete();
  this->KWValueRange = NULL;

  if ( this->KWUpdateRangeButton )
  	this->KWUpdateRangeButton->Delete();
  this->KWUpdateRangeButton = NULL;

	this->SaveButton->Delete();
  this->SaveButton = NULL;
  this->ClearButton->Delete();
  this->ClearButton = NULL;

  this->ExportDialog->Delete();
  this->ExportDialog= NULL;

  this->ExportResultButton->Delete();
  this->ExportResultButton = NULL;

  if ( this->KWRemoveButton )
  	this->KWRemoveButton->Delete();
  this->KWRemoveButton = NULL;

  if ( this->KWRenderWidget )
  	{
	  this->KWRenderWidget->RemoveAllViewProps();
	  this->KWRenderWidget->Delete();
  	}
  this->KWRenderWidget = NULL;

  if ( this->SplitFrame )
  	this->SplitFrame->Delete();
  this->SplitFrame = NULL;

  if ( this->Toolbar )
  	this->Toolbar->Delete();
  this->Toolbar = NULL;

  if ( this->PiecewiseFunction )
	  this->PiecewiseFunction->Delete();

	if ( this->KWPiecewiseFunctionEditor )
		this->KWPiecewiseFunctionEditor->Delete();

	this->HeartCurve->Delete();

	this->HeartAreaLabel1->Delete();
  this->HeartAreaEntry1->Delete();

  this->HeartAreaLabel2->Delete();
  this->HeartAreaEntry2->Delete();

  this->CalculateAreaButton->Delete();

	for(int i=0; i<MAX_CURVES; i++)
		{
		this->ShowHideCheckButton[i]->Delete();
		this->ShowHideCheckButton[i] = NULL;
		this->TitleToGraphic[i]->Delete();
		this->TitleToGraphic[i] = NULL;
		}
	this->KWTitleToGraphicFrame->Delete();

  this->KWFrameEditYAxis->Delete();
	for ( int i=0; i<PLOTS; i++ )
		{
	  this->KWYAxisEntryMin[i]->Delete();
	  this->KWYAxisEntryMax[i]->Delete();
	  this->KWYAxisLabelMin[i]->Delete();
	  this->KWYAxisLabelMax[i]->Delete();
	  this->KWYAxisFrameWithLabel[i]->Delete();
		}

  this->PlotOptions->Delete();

  if(this->ColorButton)
  	this->ColorButton->Delete();
  this->ColorButton = NULL;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::Create(vtkKWApplication *app)
{
  // Check if already created
  if (this->IsCreated())
    return;

  // Call the superclass to create the whole widget

  this->Superclass::Create(app);

	//Cria os frames da esquerda (Frame1) e da direita (Frame2)
	this->SplitFrame = vtkKWSplitFrame::New();
	this->SplitFrame->SetParent(this);
	this->SplitFrame->Create(app);
	this->Script("pack %s -side right -fill both -expand t",
               this->SplitFrame->GetWidgetName());

  //Cria o Frame que terá o nome do gráfico.
	//O nome do gráfico é comporto de 5 labels com os nomes dos 5 elementos clicados.
	this->KWTitleToGraphicFrame->SetParent(this->SplitFrame->GetFrame2());
  this->KWTitleToGraphicFrame->Create(app);
  this->KWTitleToGraphicFrame->SetBackgroundColor(0.9,0.9,0.9);
  //Cria os 5 labels com os nomes dos 5 elementos clicados.
  for(int i = 0; i<MAX_CURVES; i++)
  	{
  	this->TitleToGraphic[i]->SetParent(this->KWTitleToGraphicFrame);
  	this->TitleToGraphic[i]->Create(app);
  	}
  this->TitleToGraphic[0]->SetForegroundColor(PlotColor[0][0], PlotColor[0][1], PlotColor[0][2]);
  this->TitleToGraphic[1]->SetForegroundColor(PlotColor[1][0], PlotColor[1][1], PlotColor[1][2]);
  this->TitleToGraphic[2]->SetForegroundColor(PlotColor[2][0], PlotColor[2][1], PlotColor[2][2]);
  this->TitleToGraphic[3]->SetForegroundColor(PlotColor[3][0], PlotColor[3][1], PlotColor[3][2]);
  this->TitleToGraphic[4]->SetForegroundColor(PlotColor[4][0], PlotColor[3][1], PlotColor[4][2]);

  this->Script("pack %s -side top -expand 1 -fill both",
               this->KWTitleToGraphicFrame->GetWidgetName());

	//Cria a render widget no frame da direita
	this->KWRenderWidget = vtkKWRenderWidget::New();
  this->KWRenderWidget->SetParent(this->SplitFrame->GetFrame2());
  this->KWRenderWidget->Create(app);
  //Seta a cor de fundo da renderer
  this->KWRenderWidget->SetRendererBackgroundColor(0.8,0.8,0.8);

  this->Script("pack %s -side top -expand 1 -fill both",
               this->KWRenderWidget->GetWidgetName());

	//Cria o range de tempo no frame da esquerda
 	this->KWTimeRange = vtkKWRange::New();
 	this->KWTimeRange->SetParent(this->SplitFrame->GetFrame1());
 	this->KWTimeRange->Create(app);
 	this->KWTimeRange->SetLabelText("Time [sec]:");

	this->KWTimeRange->SetReliefToGroove();
  this->KWTimeRange->SetBorderWidth(2);

  //Cria a toolbar no frame da esquerda
  this->Toolbar = vtkKWToolbar::New();
  this->Toolbar->SetParent(this->SplitFrame->GetFrame1());
	this->Toolbar->Create(app);

  //Cria o botao save
	this->SaveButton->SetParent(this->Toolbar);
  this->SaveButton->Create(app);
  this->SaveButton->SetCommand(this, "Save");
  this->SaveButton->SetText("Save");

	//Cria o botao clear
  this->ClearButton->SetParent(this->Toolbar);
  this->ClearButton->Create(app);
  this->ClearButton->SetText("Clear");

  //Cria o botao save
	this->ExportResultButton->SetParent(this->Toolbar);
  this->ExportResultButton->Create(app);
  this->ExportResultButton->SetCommand(this, "SetExportDirectory");
  this->ExportResultButton->SetText("Export results");
  this->ExportResultButton->SetBalloonHelpString("Export 1D simulation data (all unknowns) of all selected nodes");

	this->ExportDialog->Create(app);

	//cria o botao para atualizar o range de tempo
	this->KWUpdateRangeButton = vtkKWPushButton::New();
	this->KWUpdateRangeButton->SetParent(this->Toolbar);
	this->KWUpdateRangeButton->Create(app);
	this->KWUpdateRangeButton->SetText("UpdateRange");
	this->KWUpdateRangeButton->SetCommand(this, "UpdateRange");
	this->KWUpdateRangeButton->SetAnchorToCenter();

	//cria o botão para alteração do fundo da tela do plot
	this->ColorButton = vtkKWChangeColorButton::New();
	this->ColorButton->SetParent(this->Toolbar);
	this->ColorButton->Create(app);
	this->ColorButton->SetColor(0.8, 0.8, 0.8);
	this->ColorButton->LabelVisibilityOff();
	this->ColorButton->SetBalloonHelpString("Set the background color of the Plot's render");
	this->ColorButton->SetCommand(this->KWRenderWidget, "SetRendererBackgroundColor");
//	this->KWRenderWidget->Script("pack %s -side left -fill none", this->ColorButton->GetWidgetName());

	//Adiciona os botoes na toolbar
	this->Toolbar->AddWidget(this->SaveButton);
	this->Toolbar->AddWidget(this->ClearButton);
	this->Toolbar->AddWidget(this->KWUpdateRangeButton);
	this->Toolbar->AddWidget(this->ExportResultButton);
	this->Toolbar->AddWidget(this->ColorButton);

	//Cria o frame com scrollbar
	this->KWFrameWithScrollbar = vtkKWFrameWithScrollbar::New();
  this->KWFrameWithScrollbar->SetParent(this->SplitFrame->GetFrame1());
  this->KWFrameWithScrollbar->SetWidth(300);
  this->KWFrameWithScrollbar->Create(app);

  this->KWFrameEditYAxis->SetParent(this->SplitFrame->GetFrame1());
  this->KWFrameEditYAxis->Create(app);
  this->KWFrameEditYAxis->SetWidth(300);
  this->KWFrameEditYAxis->CollapseFrame();
  this->KWFrameEditYAxis->SetLabelText(" Edit Y scale ");

  for ( int i=0; i<PLOTS; i++ )
  	{
    this->KWYAxisFrameWithLabel[i]->SetParent(this->KWFrameEditYAxis->GetFrame());
    this->KWYAxisFrameWithLabel[i]->Create(app);
    this->KWYAxisFrameWithLabel[i]->SetWidth(300);
    this->KWYAxisFrameWithLabel[i]->SetReliefToFlat();
    this->KWYAxisFrameWithLabel[i]->SetLabelText(NameOfGraphics[i]);
//    this->KWYAxisFrameWithLabel[i]->CollapseFrame();

  	this->KWYAxisEntryMin[i]->SetParent(this->KWYAxisFrameWithLabel[i]->GetFrame());
  	this->KWYAxisEntryMax[i]->SetParent(this->KWYAxisFrameWithLabel[i]->GetFrame());
  	this->KWYAxisLabelMin[i]->SetParent(this->KWYAxisFrameWithLabel[i]->GetFrame());
  	this->KWYAxisLabelMax[i]->SetParent(this->KWYAxisFrameWithLabel[i]->GetFrame());

  	this->KWYAxisEntryMax[i]->Create(app);
  	this->KWYAxisEntryMin[i]->Create(app);
  	this->KWYAxisLabelMin[i]->Create(app);
  	this->KWYAxisLabelMax[i]->Create(app);

  	this->KWYAxisLabelMin[i]->SetText("Min: ");
  	this->KWYAxisLabelMax[i]->SetText("Max: ");

	 	this->KWYAxisEntryMax[i]->SetBorderWidth(2);
  	this->KWYAxisEntryMax[i]->SetWidth(10);
  	this->KWYAxisEntryMax[i]->EnabledOn();

  	this->KWYAxisEntryMin[i]->SetBorderWidth(2);
  	this->KWYAxisEntryMin[i]->SetWidth(10);
  	this->KWYAxisEntryMin[i]->EnabledOn();

//  	char min[30];
//  	sprintf(min, "SetRangeBetweenEntries %d 0", i);
//  	this->KWYAxisEntryMin[i]->SetCommand(this, min);
//  	char max[30];
//  	sprintf(max, "SetRangeBetweenEntries %d 1", i);
//  	this->KWYAxisEntryMax[i]->SetCommand(this, max);

  	this->KWYAxisFrameWithLabel[i]->Script("pack %s -side left -fill none -expand 1 -pady 3", this->KWYAxisLabelMin[i]->GetWidgetName());
  	this->KWYAxisFrameWithLabel[i]->Script("pack %s -side left -fill none -expand 1 -pady 3", this->KWYAxisEntryMin[i]->GetWidgetName());
  	this->KWYAxisFrameWithLabel[i]->Script("pack %s -side left -fill none -expand 1 -pady 3", this->KWYAxisLabelMax[i]->GetWidgetName());
  	this->KWYAxisFrameWithLabel[i]->Script("pack %s -side left -fill none -expand 1 -pady 3", this->KWYAxisEntryMax[i]->GetWidgetName());
  	}

  	//criando os labels para as legendas e os botoes Hide e Show
	for ( int i=0; i<MAX_CURVES; i++ )
		{
		//criando os frames
		this->KWFrameWithLabel[i] = vtkKWFrameWithLabel::New();
		this->KWFrameWithLabel[i]->SetParent(this->KWFrameWithScrollbar->GetFrame());
		this->KWFrameWithLabel[i]->Create(app);

		//criando os botoes Hide e Show
	  this->ShowHideCheckButton[i]->SetParent(this->KWFrameWithLabel[i]->GetFrame());
		this->ShowHideCheckButton[i]->Create(app);
		this->ShowHideCheckButton[i]->SetText("  Show/Hide  ");
		this->ShowHideCheckButton[i]->SetBalloonHelpString("Show and Hide element in active plots.");
		this->ShowHideCheckButton[i]->SetSelectedState(1);
		this->ShowHideCheckButton[i]->SetCommand(this, "ShowHideElem");

		for ( int j=0; j<PLOTS; j++ )
			{
			//criando os labels
			this->Label[i][j] = vtkKWLabel::New();
			this->Label[i][j]->SetParent(this->KWFrameWithLabel[i]->GetFrame());
			this->Label[i][j]->Create(app);
			}
		}

	this->PiecewiseFunction = vtkPiecewiseFunction::New();
	this->KWPiecewiseFunctionEditor = vtkKWPiecewiseFunctionEditor::New();

	this->KWPiecewiseFunctionEditor->SetParent(this->SplitFrame->GetFrame2());
  this->KWPiecewiseFunctionEditor->Create(app);

	this->HeartAreaLabel1->SetParent(this->SplitFrame->GetFrame1());
	this->HeartAreaLabel1->Create(app);
	this->HeartAreaLabel1->SetText("Area 1");
  this->HeartAreaEntry1->SetParent(this->SplitFrame->GetFrame1());
	this->HeartAreaEntry1->Create(app);
	this->HeartAreaEntry1->SetWidth(10);

	this->HeartAreaLabel2->SetParent(this->SplitFrame->GetFrame1());
	this->HeartAreaLabel2->Create(app);
	this->HeartAreaLabel2->SetText("Area 2");
  this->HeartAreaEntry2->SetParent(this->SplitFrame->GetFrame1());
	this->HeartAreaEntry2->Create(app);
	this->HeartAreaEntry2->SetWidth(10);

	this->CalculateAreaButton->SetParent(this->SplitFrame->GetFrame1());
	this->CalculateAreaButton->Create(app);
	this->CalculateAreaButton->SetText("Calculate Area");
	this->CalculateAreaButton->SetCommand(this, "CalculateArea");

  this->Script("set commandList \"\"");

}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::Display()
{
  this->Superclass::Display();

	this->KWRenderWidget->Render();
}


//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::Save()
{
  char *filename;

//  this->Script("tk_getSaveFile -filetypes {{{Text} {.txt}}} -defaultextension .txt -initialfile ParaViewLog.txt");
  this->Script("tk_getSaveFile -filetypes {{{Image} {.jpg}}} -defaultextension .jpg");
  filename = new char[strlen(this->GetApplication()->GetMainInterp()->result)+1];
  sprintf(filename, "%s", this->GetApplication()->GetMainInterp()->result);

  if (strcmp(filename, "") == 0)
    {
    delete [] filename;
    return;
    }
  this->Save(filename);
}


//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::Save(const char *fileName)
{
	this->KWRenderWidget->GetRenderWindow()->SetDesiredUpdateRate(0.002);

  this->KWRenderWidget->GetRenderWindow()->Render();

	this->WriteImage(fileName, "vtkJPEGWriter");
}

int vtkKWHM1DXYPlotWidget::WriteImage(const char* filename,
                                       const char* writerName)
{
  if (!filename || !writerName)
    {
    return vtkErrorCode::UnknownError;
    }

  vtkObject* object = vtkInstantiator::CreateInstance(writerName);
  if (!object)
    {
    vtkErrorMacro("Failed to create Writer " << writerName);
    return vtkErrorCode::UnknownError;
    }
  vtkImageWriter* writer = vtkImageWriter::SafeDownCast(object);
  if (!writer)
    {
    vtkErrorMacro("Object is not a vtkImageWriter: " << object->GetClassName());
    object->Delete();
    return vtkErrorCode::UnknownError;
    }

  vtkImageData* shot = this->CaptureWindow(1);
  writer->SetInput(shot);
  writer->SetFileName(filename);
  writer->Write();
  int error_code = writer->GetErrorCode();

  writer->Delete();
  shot->Delete();

  return error_code;
}

//-----------------------------------------------------------------------------
vtkImageData* vtkKWHM1DXYPlotWidget::CaptureWindow(int magnification)
{
  // I am using the vtkPVRenderView approach for saving the image.
  // instead of vtkSMDisplayWindowProxy approach of creating a proxy.
  this->KWRenderWidget->GetRenderWindow()->SetOffScreenRendering(1);
  this->KWRenderWidget->GetRenderWindow()->SwapBuffersOff();

  vtkWindowToImageFilter* w2i = vtkWindowToImageFilter::New();
  w2i->SetInput(this->KWRenderWidget->GetRenderWindow());
  w2i->SetMagnification(magnification);
  w2i->ReadFrontBufferOff();
  w2i->ShouldRerenderOff();
  w2i->Update();

  vtkImageData* capture = vtkImageData::New();
  capture->ShallowCopy(w2i->GetOutput());
  w2i->Delete();

  this->KWRenderWidget->GetRenderWindow()->SwapBuffersOn();
  this->KWRenderWidget->GetRenderWindow()->Frame();
  this->KWRenderWidget->GetRenderWindow()->SetOffScreenRendering(0);

  return capture;
}



//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//--------------------------------------------------------------------
//Este método retorna uma referência
vtkHMXYPlot** vtkKWHM1DXYPlotWidget::GetHMXYPlot()
{
  return this->HMXYPlot;
}

void vtkKWHM1DXYPlotWidget::SetHMXYPlot(vtkHMXYPlot** plot)
{
	for ( int i=0; i<PLOTS; i++ )
		this->HMXYPlot[i] = plot[i];
}

//--------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::MaxValue(vtkDoubleArray *array)
{
	double max = array->GetValue(0);
	double tuple;

	for ( int i=1; i<array->GetNumberOfTuples(); i++ )
		{
	  tuple = array->GetValue(i);

	  //Armazenando o valor máximo
	  if(tuple > max)
	    max = tuple;
		}

	return max;
}

//--------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::MinValue(vtkDoubleArray *array)
{
	double min = array->GetValue(0);
	double tuple;

	for ( int i=1; i<array->GetNumberOfTuples(); i++ )
		{
	  tuple = array->GetValue(i);

	  //Armazenando o valor minimo
	  if(tuple < min)
	  	min = tuple;
		}

	return min;
}

//--------------------------------------------------------------------
//Este método constrói um gráfico 2D a partir de dois doubleArray recebido como parâmetro.
vtkHMXYPlot** vtkKWHM1DXYPlotWidget::MakeHMXYPlotWidget(vtkDoubleArray *doubleArray, vtkDoubleArray* timeInstant, char type, int meshID)
{

  this->SplitFrame->GetFrame2()->UnpackChildren();
  this->SplitFrame->GetFrame1()->UnpackChildren();
  this->SplitFrame->Frame1VisibilityOn();

	this->Script("pack %s -side top ",
               this->KWTitleToGraphicFrame->GetWidgetName());

	this->Script("pack %s -side top -expand 1 -fill both",
               this->KWRenderWidget->GetWidgetName());
	this->KWRenderWidget->RemoveAllViewProps();

	//Variável local para armazenar o número de plots a serem exibidos.
  int tmpNumberOfPlots = 0;

  //Este loop controla o número de referências para vtkHMXYPlot que devem ser alocadas.
  for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
  	{
		//Identifica plots ativos.
		if(this->GetTypePlot()->GetValue(i) == 1)
			{
			tmpNumberOfPlots += 1;
			}
  	}

  //Armazena em um atributo o número de plots ativos.
  this->SetNumberOfPlots(tmpNumberOfPlots);
	if ( this->NumberOfPlots == 0 )
		{
		this->ShowLegends();
		return NULL;
		}

  if ( this->NumberOfGraphics == 0 )
	  {
	  this->KWTimeRange->SetWholeRange(timeInstant->GetValue(0), timeInstant->GetValue(timeInstant->GetNumberOfTuples()-1));
	  this->KWTimeRange->SetRange(timeInstant->GetValue(0), timeInstant->GetValue(timeInstant->GetNumberOfTuples()-1));
	  }

  //Se numero de curvas e igual ou maior que o maximo
  if ( this->NumberOfGraphics < this->MaxCurves )
  	{
		this->NumberOfGraphics++;

		//Cria um vtkDoubleArray para armazenar a curva de pressão em dyn/cm².
	  this->pressionDyn = vtkDoubleArray::New();
	  this->pressionDyn->SetNumberOfComponents(1);
	  this->pressionDyn->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

	  //Cria um vtkDoubleArray para armazenar a curva de pressão em mmHg.
	  this->pressionMMHG = vtkDoubleArray::New();
	  this->pressionMMHG->SetNumberOfComponents(1);
	  this->pressionMMHG->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

	  //Cria um vtkDoubleArray para armazenar a curva de área.
	  this->area = vtkDoubleArray::New();
	  this->area->SetNumberOfComponents(1);
	  this->area->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

	  //Cria um vtkDoubleArray para armazenar a curva de fluxo.
	  this->flow = vtkDoubleArray::New();
	  this->flow->SetNumberOfComponents(1);
	  this->flow->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

	  this->velocityCMs = vtkDoubleArray::New();
	  this->velocityCMs->SetNumberOfComponents(1);
	  this->velocityCMs->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

	  this->velocityMs = vtkDoubleArray::New();
	  this->velocityMs->SetNumberOfComponents(1);
	  this->velocityMs->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

		this->DataArrayCollection[0]->AddItem(pressionDyn);
		this->DataArrayCollection[1]->AddItem(pressionMMHG);
		this->DataArrayCollection[2]->AddItem(area);
		this->DataArrayCollection[3]->AddItem(flow);
		this->DataArrayCollection[4]->AddItem(velocityCMs);
		this->DataArrayCollection[5]->AddItem(velocityMs);

		time->SetNumberOfTuples(doubleArray->GetNumberOfTuples());

		this->pressionDyn->Delete();
		this->pressionMMHG->Delete();
		this->area->Delete();
		this->flow->Delete();
		this->velocityCMs->Delete();
		this->velocityMs->Delete();
  	}

  this->KWTimeRange->EnabledOn();
  this->KWUpdateRangeButton->EnabledOn();

  //Pegando o range e calculando o numero de pontos a ser exibido
  double r[2];
  this->KWTimeRange->GetRange(r[0], r[1]);

  int posicao = this->NumberOfGraphicsSum%this->MaxCurves;

  this->pressionDyn 	= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[0]->GetItem(posicao));
  this->pressionMMHG 	= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[1]->GetItem(posicao));
  this->area 					= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[2]->GetItem(posicao));
  this->flow 					= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[3]->GetItem(posicao));
  this->velocityCMs 	= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[4]->GetItem(posicao));
  this->velocityMs 		= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[5]->GetItem(posicao));

  this->sizeOfArrays = time->GetNumberOfTuples();
  //Ponteiro auxiliar para ajudar na filtragem dos dados de pressão, área e fluxo.
  double *tmpPointer;
  tmpPointer = NULL;

  //Variável local para controlar as coordenadas dos plots.
  int position=0;

  //Tratamento para data out com três tuplas.
  if(doubleArray->GetNumberOfComponents() == 3)
  	{
    //Filtrando os dados.
		for (vtkIdType i = 0; i < doubleArray->GetNumberOfTuples(); i++)
			{
		  tmpPointer = doubleArray->GetTuple(i);

	    //Configurando arrays.
		  this->flow->SetTuple(i, &tmpPointer[0]);
		  this->area->SetTuple(i, &tmpPointer[1]);
		  this->pressionDyn->SetTuple(i, &tmpPointer[2]);
		  this->pressionMMHG->SetValue(i, pressionDyn->GetValue(i)*ConvertPressure);
		  this->time->SetTuple(i, timeInstant->GetTuple(i));
			}
  	}
  //Tratando arquivos com 4 tuplas.
  else if ( (doubleArray->GetNumberOfComponents() == 4) || (doubleArray->GetNumberOfComponents() == 7) )
	  {
		//Filtrando dados.
		for (int i = 0; i < doubleArray->GetNumberOfTuples(); i++)
			{
		  tmpPointer = doubleArray->GetTuple(i);

		  this->area->SetTuple(i, &tmpPointer[2]);
		  this->flow->SetTuple(i, &tmpPointer[0]);
		  this->pressionDyn->SetTuple(i, &tmpPointer[3]);
		  this->pressionMMHG->SetValue(i, pressionDyn->GetValue(i)*ConvertPressure);
		  this->time->SetTuple(i, timeInstant->GetTuple(i));
			}
  	}

  //criar arrays de velocidade
  vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();

	DataPlot->MaxAndMinVelocity(DataPlot->MakeVelocityCurve(this->flow, this->area));
	vtkDoubleArray *velocity = DataPlot->MakeVelocityCurve(this->flow, this->area);
	this->velocityCMs->DeepCopy(velocity);
	velocity->Delete();

	//Converte velocidade de cm/s para m/s
	for ( int i=0; i<velocityCMs->GetNumberOfTuples(); i++ )
		this->velocityMs->SetValue(i, velocityCMs->GetValue(i)*ConvertVelocity);

	DataPlot->Delete();

  //Variavel para armazenar posicao de inicio da curva
  //no array, de acordo com o range de tempo
  this->posInArray=0;
  for ( int i=0; i<doubleArray->GetNumberOfTuples(); i++)
  	{
  	if ( timeInstant->GetValue(i) == r[0] )
  		{
  		this->posInArray = i;
  		break;
  		}
  	else if ( timeInstant->GetValue(i) > r[0] )
  		{
  		this->posInArray = i-1;
  		break;
  		}
  	}

  //Pegando o tamanho do array a partir da posicao inicial e a
  //final de acordo com o range do array de tempo
  for ( int i=this->posInArray; i<this->time->GetNumberOfTuples(); i++)
  	{
  	if ( this->time->GetValue(i) == r[1] )
  		{
  		this->sizeOfArrays = i-this->posInArray+1;
  		break;
  		}
  	else if ( this->time->GetValue(i) > r[1] )
  		{
  		this->sizeOfArrays = i-this->posInArray;
  		break;
  		}
  	}

  //Criar o plot
  for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
  	this->BuildPlot(i, type, meshID);

  //Esta condicional verifica se o número de plots a serem exibidos é igual a 1,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  if(this->GetNumberOfPlots() == 1)
  	{
  	//Define p tamanho da janela.
  	this->SetSize(800,350);
  	//Verificando o(s) plot(s) ativo(s).
  	for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
  		{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
//  	  	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.26, 0); //Configurando sua coordenada na ViewPort.
  	  	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0); //Configurando sua coordenada na ViewPort.

//  	  	this->HMXYPlot[i]->SetHeight(0.6);
  	  	this->HMXYPlot[i]->SetHeight(0.95);
  	  	this->HMXYPlot[i]->SetWidth (0.95);
  	  	}
  		}
  	}

  //Este condicional verifica se o número de plots a serem exibidos é igual a 2,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 2)
  	{
  	//Este array armazena as duas posições do plot, utilizado para controlar a atribuição das coordenadas.
    position = 0;

    //Define p tamanho da janela.
    this->SetSize(800,400);
    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
//  	  	this->HMXYPlot[i]->SetHeight(0.4);
  	  	this->HMXYPlot[i]->SetHeight(0.45);
  	  	this->HMXYPlot[i]->SetWidth (0.95);
        //Posição para o primeiro plot.
        if(position == 0)
        	{
//        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.55, 0);
        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.50, 0);
          position += 1;
        	}

        else if(position != 0)
        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);
//        this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
  	  	}
    	}
  	}

  //Este condicional verifica se o número de plots a serem exibidos é igual a 3,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 3)
  	{
  	position = 0;

  	//Define p tamanho da janela.
    this->SetSize(800,500);

    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
//  	  	this->HMXYPlot[i]->SetHeight(0.27);
  	  	this->HMXYPlot[i]->SetHeight(0.3);
  	  	this->HMXYPlot[i]->SetWidth (0.95);
  	    //Posição para o primeiro plot.
        if(position == 0)
        	{
//        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.65, 0);
        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.66, 0);
          position += 1;
        	}

        //Posição para o segundo plot.
        else if(position == 1)
        	{
//        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.35, 0);
        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.33, 0);
          position += 1;
        	}

        //Posição para o terceiro plot.
        else if(position == 2)
        	this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);
//        this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
  	  	}
    	}
  	}

  //Este condicional verifica se o número de plots a serem exibidos é igual a 4,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 4)
  	{
  	position = 0;

  	//Define p tamanho da janela.
    this->SetSize(800,620);

    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
        this->HMXYPlot[i]->SetHeight(0.25);
        this->HMXYPlot[i]->SetWidth (0.95);
  	    //Posição para o primeiro plot.
        if(position == 0)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.75, 0);
          position += 1;
        	}

        //Posição para o segundo plot.
        else if(position == 1)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.50, 0);
          position += 1;
        	}

        //Posição para o terceiro plot.
        else if(position == 2)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.25, 0);
          position += 1;
        	}

        //posição para o quarto plot.
        else if(position == 3)
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);
  	  	}
    	}
  	}

	this->NumberOfGraphicsSum++;


	// calcula a media da curva baseado no intervalo de tempo.
  double medium[PLOTS];

  for ( int j=0; j<this->NumberOfGraphics; j++ )
    {
    for ( int i=0; i<PLOTS; i++ )
      medium[i] = 0.0;

    for(vtkIdType i = this->posInArray; i<this->sizeOfArrays+this->posInArray; i++)
      {
      medium[0] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[0]->GetItem(j))->GetValue(i);
      medium[1] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[1]->GetItem(j))->GetValue(i);
      medium[2] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[2]->GetItem(j))->GetValue(i);
      medium[3] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[3]->GetItem(j))->GetValue(i);
      medium[4] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[4]->GetItem(j))->GetValue(i);
      medium[5] += vtkDoubleArray::SafeDownCast(this->DataArrayCollection[5]->GetItem(j))->GetValue(i);
      }

    medium[0] /= sizeOfArrays;
    medium[1] /= sizeOfArrays;
    medium[2] /= sizeOfArrays;
    medium[3] /= sizeOfArrays;
    medium[4] /= sizeOfArrays;
    medium[5] /= sizeOfArrays;

    this->Medium[0][j] = medium[0];
    this->Medium[1][j] = medium[1];
    this->Medium[2][j] = medium[2];
    this->Medium[3][j] = medium[3];
    this->Medium[4][j] = medium[4];
    this->Medium[5][j] = medium[5];
    }





	for(int i=0; i<MAX_CURVES; i++)//Numero de curvas plotadas --> Numero de frames com label
		{
		if(this->MeshIDForTitle[i]!=-1)
			{
			char labelOfFrame[50];
			sprintf(labelOfFrame, "%c - %d ", this->TypeForTitle[i], this->MeshIDForTitle[i]);
			this->KWFrameWithLabel[i]->SetLabelText(labelOfFrame);

		  char pressDyn[70];
		  sprintf(pressDyn, "%s - [%.0f, %.0f]\n  Mean Value - %.0f", NameOfGraphics[0], this->MinPressDyn[i], this->MaxPressDyn[i], this->Medium[0][i]);
		  this->Label[i][0]->SetText(pressDyn);
		  this->Label[i][0]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

		  char pressMMHG[70];
		  sprintf(pressMMHG, "%s - [%.0f, %.0f]\n  Mean Value - %.0f", NameOfGraphics[1], this->MinPressMMHG[i], this->MaxPressMMHG[i], this->Medium[1][i]);
		  this->Label[i][1]->SetText(pressMMHG);
		  this->Label[i][1]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

		  char area[70];
		  sprintf(area, "%s - [%.4f, %.4f]\n  Mean Value - %.4f", NameOfGraphics[2], this->MinArea[i], this->MaxArea[i], this->Medium[2][i]);
		  this->Label[i][2]->SetText(area);
		  this->Label[i][2]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

		  char flow[70];
		  sprintf(flow, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[3], this->MinFlow[i], this->MaxFlow[i], this->Medium[3][i]);
		  this->Label[i][3]->SetText(flow);
		  this->Label[i][3]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

		  char velocityCM[70];
		  sprintf(velocityCM, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[4], this->MinVelocityCM[i], this->MaxVelocityCM[i], this->Medium[4][i]);
		  this->Label[i][4]->SetText(velocityCM);
		  this->Label[i][4]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

		  char velocityM[70];
		  sprintf(velocityM, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[5], this->MinVelocityM[i], this->MaxVelocityM[i], this->Medium[5][i]);
		  this->Label[i][5]->SetText(velocityM);
		  this->Label[i][5]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

			} //Fim do if(this->MeshIDForTitle[i]!=-1)
		} //Fim do For

	//Para o caso de um botão show/hide esti como hide
	//e plotar uma nova curvapara o mesmo gráfico.
	this->ShowHideElem();

  //exibe as legendas
	this->ShowLegends();

	//exibe o fame para edicao dos eixos y dos gráficos
	this->ShowEditYAxis();

	this->Script("pack %s -side top -fill x -expand 1 -pady 3",
      this->KWTimeRange->GetWidgetName());

	this->Script("pack %s -side top -fill x -expand 1 -pady 3",
      this->KWFrameEditYAxis->GetWidgetName());

	this->Script("pack %s -side top -fill x -expand 1 -pady 3",
      this->Toolbar->GetWidgetName());

  this->Script("pack %s -side top -fill x -expand 1 -pady 3",
						this->KWFrameWithScrollbar->GetWidgetName());

  this->SplitFrame->SetFrame1Size(320);

	for ( int i=0; i<PLOTS; i++ )
		if( this->TypePlot->GetValue(i))
    	this->KWRenderWidget->AddViewProp(this->HMXYPlot[i]);

	this->ShowHMXYPlotWidget();
  //Retornando a referência para os plots.
  return NULL;
}//End of method.

//--------------------------------------------------------------------
//Este método procura o tipo de plot a ser exibido e constrói o mesmo.
void vtkKWHM1DXYPlotWidget::BuildPlot(int id, char type, int meshID)
{
	this->KWTimeRange->SetResolution(time->GetValue(1)-time->GetValue(0));

  //Texto da legenda.
//  char LabelOfLegend[70];

  double maxPressDyn, maxPressMMHG, maxFlow, maxArea;
	double minPressDyn, minPressMMHG, minFlow, minArea;

  //configurando atributos maximo e minimo.
	maxPressDyn = this->MaxValue(this->pressionDyn);
	minPressDyn = this->MinValue(this->pressionDyn);

	maxPressMMHG = this->MaxValue(this->pressionMMHG);
	minPressMMHG = this->MinValue(this->pressionMMHG);

	if( this->MaxValue(this->area) == 0 )
	   maxArea = 0.5;
	else
	 	maxArea = this->MaxValue(this->area);

	//se minimo da area for menor que zero, atribui zero ao minimo
	if(this->MinValue(this->area) < 0)
	   minArea = 0.0;
	else
	 	minArea = this->MinValue(this->area);

	maxFlow = this->MaxValue(this->flow);
	minFlow = this->MinValue(this->flow);

	//configurando maximo de pressao, fluxo e area
	this->MaxPressDyn[this->NumberOfGraphicsSum%MAX_CURVES] = maxPressDyn;
//	if(this->MaxPressDynControl < maxPressDyn)
//		this->MaxPressDynControl = maxPressDyn;
	if ( this->GetMaxPressDyn() < maxPressDyn )
	 	this->SetMaxPressDyn( maxPressDyn );

	this->MaxPressMMHG[this->NumberOfGraphicsSum%MAX_CURVES] = maxPressMMHG;
//	if(this->MaxPressMMHGControl < maxPressMMHG)
//		this->MaxPressMMHGControl = maxPressMMHG;
	if ( this->GetMaxPressMMHG() < maxPressMMHG )
	 	this->SetMaxPressMMHG( maxPressMMHG );

	this->MaxFlow[this->NumberOfGraphicsSum%MAX_CURVES] = maxFlow;
//	if(this->MaxFlowControl < maxFlow)
//		this->MaxFlowControl = maxFlow;
	if ( this->GetMaxFlow() < maxFlow )
	 	this->SetMaxFlow( maxFlow );

	this->MaxArea[this->NumberOfGraphicsSum%MAX_CURVES] = maxArea;
//	if(this->MaxAreaControl < maxArea)
//		this->MaxAreaControl = maxArea;
  if ( this->GetMaxArea() < maxArea )
	 	this->SetMaxArea( maxArea );

  //configurando minimo de pressao, fluxo e area
  this->MinPressDyn[this->NumberOfGraphicsSum%MAX_CURVES] = minPressDyn;
//	if(this->MinPressDynControl > minPressDyn)
//		this->MinPressDynControl = minPressDyn;
  if ( this->GetMinPressDyn() > minPressDyn )
	 	this->SetMinPressDyn( minPressDyn );

  this->MinPressMMHG[this->NumberOfGraphicsSum%MAX_CURVES] = minPressMMHG;
//	if(this->MinPressMMHGControl > minPressMMHG)
//		this->MinPressMMHGControl = minPressMMHG;
	if ( this->GetMinPressMMHG() > minPressMMHG )
	 	this->SetMinPressMMHG( minPressMMHG );

  this->MinFlow[this->NumberOfGraphicsSum%MAX_CURVES] = minFlow;
//	if(this->MinFlowControl > minFlow)
//		this->MinFlowControl = minFlow;
  if ( this->GetMinFlow() > minFlow )
	 	this->SetMinFlow( minFlow );

  this->MinArea[this->NumberOfGraphicsSum%MAX_CURVES] = minArea;
//	if(this->MinAreaControl > minArea)
//		this->MinAreaControl = minArea;
  if ( this->GetMinArea() > minArea )
	 	this->SetMinArea( minArea );

	this->MeshIDForTitle[this->NumberOfGraphicsSum%MAX_CURVES] = meshID;
	this->TypeForTitle[this->NumberOfGraphicsSum%MAX_CURVES] = type;

//	string title;
	for(int i = 0; i<MAX_CURVES; i++)
		{
		if( (this->MeshIDForTitle[i] != -1) && (this->TypeForTitle[i] != '1') )
			{
			char temp[20];
			sprintf(temp, "%c-%d/ ", this->TypeForTitle[i], this->MeshIDForTitle[i]);
			this->TitleToGraphic[i]->SetText(temp);
//			title.append(temp);
			}
		}
	this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[0]->GetWidgetName());
	this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[1]->GetWidgetName());
	this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[2]->GetWidgetName());
	this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[3]->GetWidgetName());
	this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[4]->GetWidgetName());

//	const char* graphicName = title.c_str();

  //Pressão dyn/cm²
  if(id == 0)
  	{
  	HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
	  	DataPlot->Delete();
	  	}

    //Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
		HMXYPlot[id]->SetYRange(this->MinPressDynControl, this->MaxPressDynControl);
    HMXYPlot[id]->SetYTitle("Pressure \n [dyn/cm^2]");
    HMXYPlot[id]->SetXTitle("Time [sec]\n");
    HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

    if ( (minPressDyn < 0) && (this->NumberOfGraphicsSum < 1) )
	  	vtkKWMessageDialog::PopupMessage(
		      this->GetApplication(), vtkKWWindow::SafeDownCast(this->GetMasterWindow()), "Warning",
		      "Pressure possesses negative values, check the model, it this is not allowable!",
		      vtkKWMessageDialog::WarningIcon);

//    this->KWYAxisRange[id]->SetWholeRange(this->GetMinPressDyn()-100000.0, this->GetMaxPressDyn()+100000.0);
//    this->KWYAxisRange[id]->SetRange(this->GetMinPressDyn(), this->GetMaxPressDyn());
//    this->KWYAxisRange[id]->SetResolution(1);

    this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinPressDynControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxPressDynControl);

  	} //Fim if(id == 0)

  //Pressao mmHg
  else if(id == 1)
  	{
		HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
	  	DataPlot->Delete();
	  	}

    //Configurando as propriedades do plot.
    double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
    HMXYPlot[id]->SetYRange(this->MinPressMMHGControl, this->MaxPressMMHGControl);
    HMXYPlot[id]->SetYTitle("Pressure \n [mmHg]");
    HMXYPlot[id]->SetXTitle("Time [sec]\n");
  	HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

//  	this->KWYAxisRange[id]->SetWholeRange(this->GetMinPressMMHG(), this->GetMaxPressMMHG());
//  	this->KWYAxisRange[id]->SetRange(this->GetMinPressMMHG(), this->GetMaxPressMMHG());
//  	this->KWYAxisRange[id]->SetResolution(1);

    this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinPressMMHGControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxPressMMHGControl);
  	} //Fim else if(id == 1)

  //Area
  else if(id == 2)
  	{
		HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
	  	DataPlot->Delete();
	  	}

    //Configurando as propriedades do plot.
    HMXYPlot[id]->SetLabelFormat("%1.3g");
    double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
    HMXYPlot[id]->SetYRange(this->MinAreaControl, this->MaxAreaControl);
    HMXYPlot[id]->SetYTitle("Area \n [cm^2]");
    HMXYPlot[id]->SetXTitle("Time [sec]\n");
  	HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

//  	this->KWYAxisRange[id]->SetWholeRange(this->GetMinArea(), this->GetMaxArea());
//  	this->KWYAxisRange[id]->SetRange(this->GetMinArea(), this->GetMaxArea());
//  	this->KWYAxisRange[id]->SetResolution(0.0001);

    this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinAreaControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxAreaControl);
  	} //Fim else if(id == 2)

  //Fluxo
  else if(id == 3)
  	{
		HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
	  	DataPlot->Delete();
	  	}

  	//Configurando as propriedades do plot.
  	HMXYPlot[id]->SetLabelFormat("%1.2g");
  	double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
    HMXYPlot[id]->SetYRange(this->MinFlowControl, this->MaxFlowControl);
	  HMXYPlot[id]->SetYTitle("Flow rate \n [cm^3/s]");
		HMXYPlot[id]->SetXTitle("Time [sec]\n");
  	HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

//  	this->KWYAxisRange[id]->SetWholeRange(this->MinFlowControl, this->MaxFlowControl);
//  	this->KWYAxisRange[id]->SetRange(this->MinFlowControl, this->MaxFlowControl);
//  	this->KWYAxisRange[id]->SetResolution(0.001);

    this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinFlowControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxFlowControl);
  	} //Fim else if(id == 3)

  //Velocidade cm/s
  else if(id == 4)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
			DataPlot->Delete();
	  	}

		this->MaxVelocityCM[this->NumberOfGraphicsSum%MAX_CURVES] = this->MaxValue(Array);
//		if(this->MaxVelocityCMControl < this->MaxValue(Array))
//			this->MaxVelocityCMControl = this->MaxValue(Array);
		if ( this->GetMaxVelocityCM() < this->MaxValue(Array) )
		 	this->SetMaxVelocityCM( this->MaxValue(Array) );

		this->MinVelocityCM[this->NumberOfGraphicsSum%MAX_CURVES] = this->MinValue(Array);
//		if(this->MinVelocityCMControl > this->MinValue(Array))
//			this->MinVelocityCMControl = this->MinValue(Array);
	  if ( this->GetMinVelocityCM() > this->MinValue(Array) )
		 	this->SetMinVelocityCM( this->MinValue(Array) );
	  
//		this->MinVelocityCM[this->NumberOfGraphicsSum%MAX_CURVES] = 0.0;

		//Configurando as propriedades do plot.
		HMXYPlot[id]->SetLabelFormat("%1.2g");
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
		HMXYPlot[id]->SetYRange(this->MinVelocityCMControl, this->MaxVelocityCMControl);
		HMXYPlot[id]->SetYTitle("Velocity \n [cm/s]");
		HMXYPlot[id]->SetXTitle("Time [sec]\n");
		HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

//		this->KWYAxisRange[id]->SetWholeRange(this->GetMinVelocityCM(), this->GetMaxVelocityCM());
//		this->KWYAxisRange[id]->SetRange(this->GetMinVelocityCM(), this->GetMaxVelocityCM());
//		this->KWYAxisRange[id]->SetResolution(0.001);

		this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinVelocityCMControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxVelocityCMControl);
  	} //Fim else if(id == 4)

   //Velocidade m/s
  else if(id == 5)
  	{
		//Obtendo o máximo e mínimo de velocidade.
		HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;

	  //Adiciona as curvas ao plot
	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  	HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, time,
	  				 this->sizeOfArrays, this->posInArray));
			DataPlot->Delete();
	  	}

		this->MaxVelocityM[this->NumberOfGraphicsSum%MAX_CURVES] = this->MaxValue(Array);
//		if(this->MaxVelocityMControl < this->MaxValue(Array))
//			this->MaxVelocityMControl = this->MaxValue(Array);
	  if ( this->GetMaxVelocityM() < this->MaxValue(Array) )
		 	this->SetMaxVelocityM( this->MaxValue(Array) );

		this->MinVelocityM[this->NumberOfGraphicsSum%MAX_CURVES] = this->MinValue(Array);
//		if(this->MinVelocityMControl > this->MinValue(Array))
//			this->MinVelocityMControl = this->MinValue(Array);
	  if ( this->GetMinVelocityM() > this->MinValue(Array) )
		 	this->SetMinVelocityM( this->MinValue(Array) );
	  
//		this->MinVelocityM[this->NumberOfGraphicsSum%MAX_CURVES] = 0.0;

		////Configurando as propriedades do plot.
		HMXYPlot[id]->SetLabelFormat("%1.2g");
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);
		HMXYPlot[id]->SetYRange(this->MinVelocityMControl, this->MaxVelocityMControl);
		HMXYPlot[id]->SetYTitle("Velocity \n [m/s]");
		HMXYPlot[id]->SetXTitle("Time [sec]\n");
		HMXYPlot[id]->SetDataObjectYComponent(this->NumberOfGraphicsSum%this->MaxCurves, 1);

//		this->KWYAxisRange[id]->SetWholeRange(this->GetMinVelocityM(), this->GetMaxVelocityM());
//		this->KWYAxisRange[id]->SetRange(this->GetMinVelocityM(), this->GetMaxVelocityM());
//		this->KWYAxisRange[id]->SetResolution(0.001);

    this->KWYAxisEntryMin[id]->SetValueAsDouble(this->MinVelocityMControl);
    this->KWYAxisEntryMax[id]->SetValueAsDouble(this->MaxVelocityMControl);
  	} //Fim else if(id == 5)
//  textProp->Delete();

  //Valores na ordem Press[Dyn/cm2], Press[mmHg], Area[cm2], Flow[cm3], Vel[cm/s], Vel[m/s]
//  this->YAxisMin[0] = this->GetMinPressDyn()-100000.000;
//  this->YAxisMin[1] = this->GetMinPressMMHG()-750.187;
//  this->YAxisMin[2] = this->GetMinArea()-0;
//  this->YAxisMin[3] = this->MinFlowControl-100000.000;
//  this->YAxisMin[4] = this->GetMinVelocityCM()-100000.000;
//  this->YAxisMin[5] = this->GetMinVelocityM()-1000.000;
//
//  this->YAxisMax[0] = this->GetMaxPressDyn()+100000.000;
//  this->YAxisMin[1] = this->GetMaxPressMMHG()+750.187;
//  this->YAxisMin[2] = this->GetMaxArea()+100000.000;
//  this->YAxisMin[3] = this->MaxFlowControl+100000.000;
//  this->YAxisMin[4] = this->GetMaxVelocityCM()+100000.000;
//  this->YAxisMin[5] = this->GetMaxVelocityM()+1000.000;

}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::RebuildPlot()
{
	this->KWRenderWidget->RemoveAllViewProps();
  this->KWFrameWithScrollbar->GetFrame()->UnpackChildren();
//  this->KWFrameWithScrollbar->GetFrame()->RemoveAllChildren();
  for ( int i=0; i<MAX_CURVES; i++ )
  	{
	  this->KWFrameWithLabel[i]->GetFrame()->UnpackChildren();
//	  this->KWFrameWithLabel[i]->GetFrame()->RemoveAllChildren();
  	}
  this->KWRenderWidget->Render();

	//Variável local para controlar as coordenadas dos plots.
	int position=0;

	//Variável local para armazenar o número de plots a serem exibidos.
	int tmpNumberOfPlots = 0;

	//Este loop controla o número de referências para vtkHMXYPlot que devem ser alocadas.
	for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
		{
	   //Identifica plots ativos.
	   if(this->GetTypePlot()->GetValue(i) == 1)
	  	 {
	     tmpNumberOfPlots += 1;
	  	 }
		}

  this->SetNumberOfPlots(tmpNumberOfPlots);
	if ( (this->NumberOfPlots == 0) || (this->NumberOfGraphics == 0))
		{
		this->ShowLegends();
		return;
		}

	//Esta condicional verifica se o número de plots a serem exibidos é igual a 1,
	//se for, redimensiona a janela de acordo ajusta a coordenada do plot.
	if(this->GetNumberOfPlots() == 1)
		{
		//Define p tamanho da janela.
		this->SetSize(800,350);
		//Verificando o(s) plot(s) ativo(s).
		for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
			{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
//		    HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.26, 0); //Configurando sua coordenada na ViewPort.
		    HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0); //Configurando sua coordenada na ViewPort.

	      HMXYPlot[i]->SetHeight(0.90);
	      this->HMXYPlot[i]->SetWidth (0.95);
  	  	}
			}
		}

	//Este condicional verifica se o número de plots a serem exibidos é igual a 2,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 2)
  	{
  	//Este array armazena as duas posições do plot, utilizado para controlar a atribuição das coordenadas.
    position = 0;

    //Define p tamanho da janela.
    this->SetSize(800,350);
    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
        HMXYPlot[i]->SetHeight(0.45);
        this->HMXYPlot[i]->SetWidth (0.95);

        //Posição para o primeiro plot.
        if(position == 0)
        {
          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.50, 0);
//          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.55, 0);
          position += 1;
        }

        else if(position != 0)
          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);
//        HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);
  	  	}
	  	}
	  }

  //Este condicional verifica se o número de plots a serem exibidos é igual a 3,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 3)
  	{
  	position = 0;

  	//Define p tamanho da janela.
    this->SetSize(800,500);

    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
        HMXYPlot[i]->SetHeight(0.3);
        this->HMXYPlot[i]->SetWidth (0.95);

  	    //Posição para o primeiro plot.
        if(position == 0)
        	{
//          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.65, 0);
          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.66, 0);
          position += 1;
        	}

        //Posição para o segundo plot.
        else if(position == 1)
        	{
//          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.35, 0);
          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.33, 0);
          position += 1;
        	}

        //Posição para o terceiro plot.
        else if(position == 2)
          HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);
//        HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.05, 0);

  	  	}
	  	}
	  }

  //Este condicional verifica se o número de plots a serem exibidos é igual a 4,
  //se for, redimensiona a janela de acordo ajusta a coordenada do plot.
  else if(this->GetNumberOfPlots() == 4)
  	{
  	position = 0;

  	//Define p tamanho da janela.
    this->SetSize(800,620);

    //Verificando o(s) plot(s) ativo(s).
    for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
    	{
  	  if(this->GetTypePlot()->GetValue(i) == 1)
  	  	{
        this->HMXYPlot[i]->SetHeight(0.25);
        this->HMXYPlot[i]->SetWidth (0.95);

  	    //Posição para o primeiro plot.
        if(position == 0)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.75, 0);
          position += 1;
        	}

        //Posição para o segundo plot.
        else if(position == 1)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.50, 0);
          position += 1;
        	}

        //Posição para o terceiro plot.
        else if(position == 2)
        	{
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.25, 0);
          position += 1;
        	}

        //posição para o quarto plot.
        else if(position == 3)
          this->HMXYPlot[i]->GetPositionCoordinate()->SetValue(0.05, 0.00, 0);

  	  	}
	  	}
	  }

	this->UpdateRange();

	this->ShowLegends();

	this->ShowEditYAxis();

	for (int i=0; i<MAX_CURVES; i++)
		{
//		if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
//			this->BoolShowHideCheckButton[i] = false;
//		else if (this->ShowHideCheckButton[i]->GetSelectedState() == 1)
//			this->BoolShowHideCheckButton[i] = true;

		this->KWTitleToGraphicFrame->Script("pack forget %s", this->TitleToGraphic[i]->GetWidgetName());
		if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
			this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[i]->GetWidgetName());
		}

	for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
	  if(this->GetTypePlot()->GetValue(i) == 1)
	  	this->ResizeCurves(i);

	for ( int i=0; i<PLOTS; i++ )
		if( this->GetTypePlot()->GetValue(i) == 1)
    	this->KWRenderWidget->AddViewProp(this->HMXYPlot[i]);

	this->ShowHMXYPlotWidget();

}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::BuildHeartPlot(int id, vtkDoubleArray* flow, vtkDoubleArray *time)
{
	this->SetNumberOfPlots(1);

	this->time->DeepCopy(time);

	//setando tipo de plot
	this->TypePlot->SetValue(0, 0);
  this->TypePlot->SetValue(1, 0);
  this->TypePlot->SetValue(2, 0);
  this->TypePlot->SetValue(3, 1);
  this->TypePlot->SetValue(4, 0);
  this->TypePlot->SetValue(5, 0);

	this->NumberOfGraphics = 1;

  this->SplitFrame->GetFrame2()->UnpackChildren();
  this->SplitFrame->GetFrame1()->UnpackChildren();
  this->SplitFrame->Frame1VisibilityOn();

	this->Script("pack %s -side top -expand 1 -fill both",
	            this->KWPiecewiseFunctionEditor->GetWidgetName());


	this->Script("grid %s - -sticky w -pady 7",
	            this->CalculateAreaButton->GetWidgetName());

	this->Script("grid %s %s -sticky w -pady 5",
	            this->HeartAreaLabel1->GetWidgetName(),
	            this->HeartAreaEntry1->GetWidgetName()
	            );

	this->Script("grid %s %s -sticky w",
	            this->HeartAreaLabel2->GetWidgetName(),
	            this->HeartAreaEntry2->GetWidgetName());

	this->SplitFrame->SetFrame1Size(150);

  this->HeartCurve->Reset();
  this->HeartCurve->DeepCopy(flow);

  this->SetSize(800,500);

  this->PiecewiseFunction->RemoveAllPoints();

  for ( int i=0; i<time->GetNumberOfTuples(); i++ )
	  this->PiecewiseFunction->AddPoint(time->GetValue(i), flow->GetValue(i));

	this->KWPiecewiseFunctionEditor->SetPiecewiseFunction(this->PiecewiseFunction);

	this->KWPiecewiseFunctionEditor->SetBorderWidth(2);
  this->KWPiecewiseFunctionEditor->SetReliefToGroove();
  this->KWPiecewiseFunctionEditor->SetPadX(2);
  this->KWPiecewiseFunctionEditor->SetPadY(5);

	this->KWPiecewiseFunctionEditor->CanvasBackgroundVisibilityOff();
  this->KWPiecewiseFunctionEditor->SetPointRadius(3);

  this->KWPiecewiseFunctionEditor->SetCanvasWidth(400);
  this->KWPiecewiseFunctionEditor->SetCanvasHeight(350);

	//Setando range do eixo y
	this->KWPiecewiseFunctionEditor->SetWholeValueRange(this->GetMinFlow(), this->GetMaxFlow());
	this->KWPiecewiseFunctionEditor->SetWholeParameterRange(time->GetValue(0),
																							time->GetValue(time->GetNumberOfTuples()-1));

	this->KWPiecewiseFunctionEditor->SetVisibleParameterRange(time->GetValue(0),
																							time->GetValue(time->GetNumberOfTuples()-1));
	this->KWPiecewiseFunctionEditor->SetVisibleValueRange(this->GetMinFlow(), this->GetMaxFlow());

	this->KWPiecewiseFunctionEditor->ParameterEntryVisibilityOn();
  this->KWPiecewiseFunctionEditor->ParameterRangeVisibilityOn();

  this->KWPiecewiseFunctionEditor->ExpandCanvasWidthOn();

  this->KWPiecewiseFunctionEditor->SetPointColor(PlotColor[0][0],
																								 PlotColor[0][1],
																								 PlotColor[0][2]);

  if (id ==1)
    this->KWPiecewiseFunctionEditor->SetLabelText("Pressure [dyn/cm^2] X Time [s]");
	else
	  this->KWPiecewiseFunctionEditor->SetLabelText("Flow [cm^3/s]");


  this->KWPiecewiseFunctionEditor->MidPointVisibilityOff();
  this->KWPiecewiseFunctionEditor->SelectedPointIndexVisibilityOn();

	this->KWPiecewiseFunctionEditor->SetLabelPositionToTop();
  this->KWPiecewiseFunctionEditor->ParameterTicksVisibilityOn();
  this->KWPiecewiseFunctionEditor->ValueTicksVisibilityOn();
  this->KWPiecewiseFunctionEditor->SetValueTicksFormat("%10.f");
  this->KWPiecewiseFunctionEditor->SetFunctionLineWidth(1);

	//Parameter e igual a eixo x
	vtkKWRange *kwRangeParameter = this->KWPiecewiseFunctionEditor->GetParameterRange();
	kwRangeParameter->SetResolution(time->GetValue(1)-time->GetValue(0));
	kwRangeParameter->SetEntriesVisibility(1);
	kwRangeParameter->SetEntry1PositionToLeft();
	kwRangeParameter->SetEntry2PositionToRight();
	kwRangeParameter->SetLabelVisibility(1);
	kwRangeParameter->SetLabelText("Time [sec]");
	kwRangeParameter->SetLabelPositionToBottom();

	//Value e igual a eixo y
	vtkKWRange *kwRangeValue = this->KWPiecewiseFunctionEditor->GetValueRange();
	kwRangeValue->SetEntriesVisibility(1);
	kwRangeValue->SetEntry1PositionToTop();
	kwRangeValue->SetEntry2PositionToBottom();
	kwRangeValue->SetLabelVisibility(1);

	if (id==1)
		kwRangeValue->SetLabelText("Pressure [dyn/cm^2]");
	else
		kwRangeValue->SetLabelText("Flow rate\n[cm^3/s]");
	kwRangeValue->SetLabelPositionToRight();
	kwRangeValue->SetEntriesWidth(8);

  if ( !this->EditCurveHeart )
	  {
	  this->KWPiecewiseFunctionEditor->ReadOnlyOff();
	  this->KWPiecewiseFunctionEditor->MidPointVisibilityOn();
	  }
	else
  	this->KWPiecewiseFunctionEditor->ReadOnlyOn();

	this->KWPiecewiseFunctionEditor->SetPiecewiseFunction(this->PiecewiseFunction);

	this->CalculateArea();

	this->NumberOfGraphics = 0;
  this->NumberOfGraphicsSum = 0;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ShowEditYAxis()
{
	this->KWFrameEditYAxis->GetFrame()->UnpackChildren();

	for ( int i=0; i<PLOTS; i++ )
		{
	  if(!this->TypePlot->GetValue(i))
			{
			this->KWFrameEditYAxis->Script("pack forget %s",
					this->KWYAxisFrameWithLabel[i]->GetWidgetName());
			}
	  else
	  	{
			this->KWFrameEditYAxis->Script("pack %s -side top -fill x -expand 1 -padx 3",
					this->KWYAxisFrameWithLabel[i]->GetWidgetName());
	  	}
	  }
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ShowLegends()
{
	this->KWRenderWidget->RemoveAllViewProps();
	this->KWFrameWithScrollbar->GetFrame()->UnpackChildren();
//	this->KWFrameWithScrollbar->GetFrame()->RemoveAllChildren();

	for ( int i=0; i<NumberOfGraphics; i++ )
		{
	  this->KWFrameWithLabel[i]->GetFrame()->UnpackChildren();
//	  this->KWFrameWithLabel[i]->GetFrame()->RemoveAllChildren();
	  if(this->MeshIDForTitle[i] != -1)
			{
			this->KWFrameWithScrollbar->Script("grid %s -sticky ew", this->KWFrameWithLabel[i]->GetWidgetName());

			this->KWFrameWithScrollbar->Script("grid %s -sticky w",this->ShowHideCheckButton[i]->GetWidgetName());

			for ( int j=0; j<PLOTS; j++ )
				{
				if(!this->TypePlot->GetValue(j))
					this->KWFrameWithScrollbar->Script("grid remove %s", this->Label[i][j]->GetWidgetName());
				else
					this->KWFrameWithScrollbar->Script("grid %s -sticky w", this->Label[i][j]->GetWidgetName());
				}

			} //Fim if(this->GetTypePlot()->GetValue(i) == 1)

		} //Fim for ( int i=0; i<PLOTS; i++ )

}

//--------------------------------------------------------------------
//Este método exibe o plot em uma vtkRenderWindow.
void vtkKWHM1DXYPlotWidget::ShowHMXYPlotWidget()
{
  this->KWRenderWidget->ResetCamera();
  this->Display();
}

//---------------------------------------------------------------------
//Atualiza o range de tempo
void vtkKWHM1DXYPlotWidget::UpdateRange()
{
	this->KWRenderWidget->RemoveAllViewProps();

	//Pegando o range e calculando o numero de pontos a ser exibido
  double r[2];
  this->KWTimeRange->GetRange(r[0], r[1]);

  //Variavel para armazenar posicao de inicio da curva
  this->posInArray=0;
  for ( int i=0; i<this->time->GetNumberOfTuples(); i++)
  	{
  	if ( this->time->GetValue(i) == r[0] )
  		{
  		this->posInArray = i;
			break;
  		}
  	else if ( this->time->GetValue(i) > r[0] )
  		{
  		this->posInArray = i-1;
  		break;
  		}
  	}

  //Pegando o tamanho do array
  for ( int i=this->posInArray; i<this->time->GetNumberOfTuples(); i++)
  	{
  	if ( this->time->GetValue(i) == r[1] )
  		{
  		this->sizeOfArrays = i-this->posInArray+1;
  		break;
  		}
  	else if ( this->time->GetValue(i) > r[1] )
  		{
  		this->sizeOfArrays = i-this->posInArray;
  		break;
  		}
  	}

	for(int i = 0; i < this->GetTypePlot()->GetNumberOfTuples(); i++)
	  if(this->GetTypePlot()->GetValue(i) == 1)
	  	this->ResizeCurves(i);

	for ( int i=0; i<PLOTS; i++ )
		if( this->GetTypePlot()->GetValue(i) == 1)
    	this->KWRenderWidget->AddViewProp(this->HMXYPlot[i]);



  // calcula a media da curva baseado no intervalo de tempo.
  double medium[PLOTS];

  vtkDoubleArray *velocityCMs;
  vtkDoubleArray *velocityMs;

  for ( int j=0; j<this->NumberOfGraphics; j++ )
    {
    pressionDyn   = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[0]->GetItem(j));
    pressionMMHG  = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[1]->GetItem(j));
    area          = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[2]->GetItem(j));
    flow          = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[3]->GetItem(j));
    velocityCMs   = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[4]->GetItem(j));
    velocityMs    = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[5]->GetItem(j));

    for ( int i=0; i<PLOTS; i++ )
      medium[i] = 0.0;

    for(vtkIdType i = this->posInArray; i<this->sizeOfArrays+this->posInArray; i++)
      {
      medium[0] += pressionDyn->GetValue(i);
      medium[1] += pressionMMHG->GetValue(i);
      medium[2] += area->GetValue(i);
      medium[3] += flow->GetValue(i);
      medium[4] += velocityCMs->GetValue(i);
      medium[5] += velocityMs->GetValue(i);
      }

    medium[0] /= sizeOfArrays;
    medium[1] /= sizeOfArrays;
    medium[2] /= sizeOfArrays;
    medium[3] /= sizeOfArrays;
    medium[4] /= sizeOfArrays;
    medium[5] /= sizeOfArrays;

    this->Medium[0][j] = medium[0];
    this->Medium[1][j] = medium[1];
    this->Medium[2][j] = medium[2];
    this->Medium[3][j] = medium[3];
    this->Medium[4][j] = medium[4];
    this->Medium[5][j] = medium[5];
    }


  for(int i=0; i<MAX_CURVES; i++)//Numero de curvas plotadas --> Numero de frames com label
    {
    if(this->MeshIDForTitle[i]!=-1)
      {
      char labelOfFrame[50];
      sprintf(labelOfFrame, "%c - %d ", this->TypeForTitle[i], this->MeshIDForTitle[i]);
      this->KWFrameWithLabel[i]->SetLabelText(labelOfFrame);

      char pressDyn[70];
      sprintf(pressDyn, "%s - [%.0f, %.0f]\n  Mean Value - %.0f", NameOfGraphics[0], this->MinPressDyn[i], this->MaxPressDyn[i], this->Medium[0][i]);
      this->Label[i][0]->SetText(pressDyn);
      this->Label[i][0]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      char pressMMHG[70];
      sprintf(pressMMHG, "%s - [%.0f, %.0f]\n  Mean Value - %.0f", NameOfGraphics[1], this->MinPressMMHG[i], this->MaxPressMMHG[i], this->Medium[1][i]);
      this->Label[i][1]->SetText(pressMMHG);
      this->Label[i][1]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      char area[70];
      sprintf(area, "%s - [%.4f, %.4f]\n  Mean Value - %.4f", NameOfGraphics[2], this->MinArea[i], this->MaxArea[i], this->Medium[2][i]);
      this->Label[i][2]->SetText(area);
      this->Label[i][2]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      char flow[70];
      sprintf(flow, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[3], this->MinFlow[i], this->MaxFlow[i], this->Medium[3][i]);
      this->Label[i][3]->SetText(flow);
      this->Label[i][3]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      char velocityCM[70];
      sprintf(velocityCM, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[4], this->MinVelocityCM[i], this->MaxVelocityCM[i], this->Medium[4][i]);
      this->Label[i][4]->SetText(velocityCM);
      this->Label[i][4]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      char velocityM[70];
      sprintf(velocityM, "%s - [%.3f, %.3f]\n  Mean Value - %.3f", NameOfGraphics[5], this->MinVelocityM[i], this->MaxVelocityM[i], this->Medium[5][i]);
      this->Label[i][5]->SetText(velocityM);
      this->Label[i][5]->SetForegroundColor(PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);

      } //Fim do if(this->MeshIDForTitle[i]!=-1)
    } //Fim do For




	this->ShowHMXYPlotWidget();
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ResizeCurves(int id)
{
	//Pressão dyn/cm²
	if(id == 0)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

	  //Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}

	//Pressao mmHg
	else if(id == 1)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		//Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}

	//Area
	else if(id == 2)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		//Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}

	//Fluxo
	else if(id == 3)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		//Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}

	//Velocidade cm/s
	else if(id == 4)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		//Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}

	 //Velocidade m/s
	else if(id == 5)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		//Configura as propriedades do plot.
		double *r = this->KWTimeRange->GetRange();
		HMXYPlot[id]->SetXRange(r[0], r[1]);

		//poderia ser o KWYAxisEntryMax também pois tem o range igual
		double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
		double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();
		HMXYPlot[id]->SetYRange(min, max);
		}
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::Clear()
{
  for ( int i=0; i<PLOTS; i++ )
  	{
  	if ( this->HMXYPlot[i] )
  		{
  		this->HMXYPlot[i]->RemoveAllInputs();
  		}
  	if ( this->DataArrayCollection[i] )
  		this->DataArrayCollection[i]->RemoveAllItems();
  	}

  if ( this->KWRenderWidget )
  	{
	  this->KWRenderWidget->RemoveAllViewProps();
	  this->KWRenderWidget->Render();
	  this->KWFrameWithScrollbar->GetFrame()->UnpackChildren();
//	  this->KWFrameWithScrollbar->GetFrame()->RemoveAllChildren();
	  for ( int i=0; i<MAX_CURVES; i++ )
    	{
  	  this->KWFrameWithLabel[i]->GetFrame()->UnpackChildren();
//  	  this->KWFrameWithLabel[i]->GetFrame()->RemoveAllChildren();
  	  for(int j=0; j<PLOTS; j++)
				this->Label[i][j]->RemoveAllChildren();
    	}
  	}
  //remover o frame para editar as escalas de Y.
  this->KWFrameEditYAxis->GetFrame()->UnpackChildren();
  this->SplitFrame->GetFrame1()->Script("pack forget %s", this->KWFrameEditYAxis->GetWidgetName());

	this->NumberOfGraphics = 0;
  this->NumberOfGraphicsSum = 0;

	for (int i=0; i<MAX_CURVES; i++)
		{
	  this->MaxPressDyn[i] 	= -10000000;
	  this->PressDynHideShowMAX[i] = -10000000;

	  this->MaxPressMMHG[i] 	= -10000000;
	  this->PressMMHGHideShowMAX[i] = -10000000;

  	this->MaxArea[i] 			= -10000000;
  	this->AreaHideShowMAX[i] = -10000000;

  	this->MaxFlow[i] 			= -10000000;
  	this->FlowHideShowMAX[i] = -10000000;

  	this->MaxVelocityCM[i]	= -10000000;
  	this->VelocityCMHideShowMAX[i] = -10000000;

  	this->MaxVelocityM[i]	= -10000000;
  	this->VelocityMHideShowMAX[i] = -10000000;


  	this->MinPressDyn[i] 	= 10000000;
	  this->PressDynHideShowMIN[i] = 10000000;

    this->MinPressMMHG[i] 	= 10000000;
    this->PressMMHGHideShowMIN[i] = 10000000;

    this->MinArea[i] 			= 10000000;
    this->AreaHideShowMIN[i] = 10000000;

    this->MinFlow[i] 			= 10000000;
    this->FlowHideShowMIN[i] = 10000000;

    this->MinVelocityCM[i]	= 10000000;
    this->VelocityCMHideShowMIN[i] = 10000000;

    this->MinVelocityM[i]	= 10000000;
    this->VelocityMHideShowMIN[i] = 10000000;
		}

	this->MaxPressDynControl 	= -10000000;
	this->MaxPressMMHGControl 	= -10000000;
	this->MaxAreaControl			= -10000000;
	this->MaxFlowControl 			= -10000000;
	this->MaxVelocityCMControl	= -10000000;
	this->MaxVelocityMControl	= -10000000;
	this->MAX_FLOW = -10000000;

	this->MinPressDynControl 	= 10000000;
	this->MinPressMMHGControl 	= 10000000;
	this->MinAreaControl 			= 10000000;
	this->MinFlowControl 			= 10000000;
	this->MinVelocityCMControl	= 10000000;
	this->MinVelocityMControl	= 10000000;
	this->MIN_FLOW = 10000000;

	this->KWTitleToGraphicFrame->UnpackChildren();
	this->SplitFrame->GetFrame2()->Script("pack forget %s", this->KWTitleToGraphicFrame->GetWidgetName());

  for(int i=0; i<MAX_CURVES; i++)
  	{
	  this->MeshIDForTitle[i] = -1;
		this->TypeForTitle[i] = '1';
		this->ShowHideCheckButton[i]->SetSelectedState(1);
//		this->BoolShowHideCheckButton[i] = true;
		this->TitleToGraphic[i]->SetText("");
  	}

}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::UpdateHeartCurve()
{
	this->PiecewiseFunction->Update();

	int size = this->PiecewiseFunction->GetSize();

	//Caso ainda não possua curva, adiciona dois pontos
	if ( size <= 1 )
		{
		this->PiecewiseFunction->RemoveAllPoints();
	  this->PiecewiseFunction->AddPoint(this->TMin, this->GetMinFlow());

	  if ( this->TMin >= this->TMax )
	  	this->TMax = this->TMin+1;
	  this->PiecewiseFunction->AddPoint(this->TMax, this->GetMaxFlow());
  	size = 2;
		}

	this->time->SetNumberOfTuples(size);

	this->HeartCurve->SetNumberOfTuples(size);

	double *pt = this->PiecewiseFunction->GetDataPointer();
	double val[4];

	for ( int i=0; i<size; i++ )
		{
		this->PiecewiseFunction->GetNodeValue(i, val);

		this->time->SetValue(i, val[0]);
		this->HeartCurve->SetValue(i, val[1]);
		}
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::CalculateArea()
{
	this->UpdateHeartCurve();

	int i=0;

	double p1, p2, t1, t2;
	double result=0;

	this->Area1 = 0;
	this->Area2 = 0;

	while ( i<this->HeartCurve->GetNumberOfTuples()-1 )
		{
		p1 = this->HeartCurve->GetValue(i);
		p2 = this->HeartCurve->GetValue(i+1);
		t1 = this->time->GetValue(i);
		t2 = this->time->GetValue(i+1);

		if ( p2 < 0 )
			break;

		result = ((p1+p2) * (t2-t1)) / 2;
		this->Area1 += result;

		i++;
		}

	this->HeartAreaEntry1->SetValueAsDouble(this->Area1);

	result = 0;

	while ( i<this->HeartCurve->GetNumberOfTuples()-1 )
		{
		p1 = this->HeartCurve->GetValue(i);
		p2 = this->HeartCurve->GetValue(i+1);
		t1 = this->time->GetValue(i);
		t2 = this->time->GetValue(i+1);

		if ( p2 > 0 )
			break;

		result = ((p1+p2) * (t2-t1)) / 2;
		this->Area2 += result;

		i++;
		}

	this->HeartAreaEntry2->SetValueAsDouble(this->Area2);
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ApplyScale(double scale)
{
	for ( int i=0; i<this->HeartCurve->GetNumberOfTuples(); i++ )
		this->HeartCurve->SetValue(i, this->HeartCurve->GetValue(i) / this->ScaleFactor);

	this->HeartCurve->Modified();

	if ( scale <= 0 )
		scale = 1;

	this->ScaleFactor = scale;

	for ( int i=0; i<this->HeartCurve->GetNumberOfTuples(); i++ )
		{
		this->HeartCurve->SetValue(i, this->HeartCurve->GetValue(i) * this->ScaleFactor);
		}

	this->HeartCurve->Modified();

	double range[2];


  this->PiecewiseFunction->RemoveAllPoints();

  for ( int i=0; i<time->GetNumberOfTuples(); i++ )
	  this->PiecewiseFunction->AddPoint(this->time->GetValue(i), this->HeartCurve->GetValue(i));

  this->HeartCurve->GetRange(range);
	this->SetMinFlow(range[0]);
	this->SetMaxFlow(range[1]);

	this->KWPiecewiseFunctionEditor->SetPiecewiseFunction(this->PiecewiseFunction);

	//Setando range do eixo y
	this->KWPiecewiseFunctionEditor->SetWholeValueRange(range[0], range[1]);
	this->KWPiecewiseFunctionEditor->SetWholeParameterRange(this->time->GetValue(0),
																							this->time->GetValue(this->time->GetNumberOfTuples()-1));
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::RemoveCurve()
{
//	if ( this->NumberOfGraphics > 0 )
//		{
//		this->NumberOfGraphics--;
//  	this->NumberOfGraphicsSum--;
//  	this->ShowHMXYPlotWidget();
//		}
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetPressionDyn(vtkDoubleArray *p)
{
	this->pressionDyn = p;
}

vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetPressionDyn()
{
	return this->pressionDyn;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetPressionMMHG(vtkDoubleArray *p)
{
	this->pressionMMHG = p;
}

vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetPressionMMHG()
{
	return this->pressionMMHG;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetArea(vtkDoubleArray *a)
{
	this->area = a;
}

vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetArea()
{
	return this->area;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetFlow(vtkDoubleArray *f)
{
	this->flow = f;
}

vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetFlow()
{
	return this->flow;
}

//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetTime(vtkDoubleArray *t)
{
	this->time = t;
}

vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetTime()
{
	return this->time;
}

//--------------------------------------------------------------------
vtkDoubleArray *vtkKWHM1DXYPlotWidget::GetHeartCurve()
{
	return this->HeartCurve;
}

void vtkKWHM1DXYPlotWidget::SetHeartCurve(vtkDoubleArray *c)
{
	this->HeartCurve = c;
}

//--------------------------------------------------------------------
//Set max de todos os pontos da arvore, este maximo tem no arquivo objeto DataOut
void vtkKWHM1DXYPlotWidget::SetMax(double *m, int t)
{
	this->Max = new double[3];
	if ( t == 3 )
		{
		this->Max[0] = m[0];
		this->Max[1] = m[1];
		this->Max[2] = m[2];
		}
	else
		{
		this->Max[0] = m[0];
		this->Max[1] = m[2];
		this->Max[2] = m[3];
		}
}

//Get max de todos os pontos da arvore, este maximo tem no arquivo objeto DataOut
double *vtkKWHM1DXYPlotWidget::GetMax()
{
	return this->Max;
}

//--------------------------------------------------------------------
//Set min de todos os pontos da arvore, este minimo tem no arquivo objeto DataOut
void vtkKWHM1DXYPlotWidget::SetMin(double *m, int t)
{
	this->Min = new double[3];
	if ( t == 3 )
		{
		this->Min[0] = m[0];
		this->Min[1] = m[1];
		this->Min[2] = m[2];
		}
	else
		{
		this->Min[0] = m[0];
		this->Min[1] = m[2];
		this->Min[2] = m[3];
		}
}

//Get min de todos os pontos da arvore, este minimo tem no arquivo objeto DataOut
double *vtkKWHM1DXYPlotWidget::GetMin()
{
	return this->Min;
}


//--------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetTypePlot(vtkIntArray *type)
{
	for ( int i=0; i<type->GetNumberOfTuples(); i++ )
		if ( type->GetValue(i) != TypePlot->GetValue(i) )
			this->TypePlot->SetValue(i, type->GetValue(i));
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetKWRenderWidget(vtkKWRenderWidget *ren)
{
	this->KWRenderWidget = ren;
}

vtkKWRenderWidget *vtkKWHM1DXYPlotWidget::GetKWRenderWidget()
{
	return this->KWRenderWidget;
}

//----------------------------------------------------------------------------
vtkKWFrameWithScrollbar *vtkKWHM1DXYPlotWidget::GetKWFrameWithScrollbar()
{
	return this->KWFrameWithScrollbar;
}

void vtkKWHM1DXYPlotWidget::SetKWFrameWithScrollbar(vtkKWFrameWithScrollbar *Frame)
{
	this->KWFrameWithScrollbar = Frame;
}

//----------------------------------------------------------------------------
vtkKWRange *vtkKWHM1DXYPlotWidget::GetKWTimeRange()
{
	return this->KWTimeRange;
}

void vtkKWHM1DXYPlotWidget::SetKWTimeRange(vtkKWRange *range)
{
	this->KWTimeRange = range;
}

//----------------------------------------------------------------------------
vtkKWRange *vtkKWHM1DXYPlotWidget::GetKWValueRange()
{
	return this->KWValueRange;
}

void vtkKWHM1DXYPlotWidget::SetKWValueRange(vtkKWRange *range)
{
	this->KWValueRange = range;
}

//----------------------------------------------------------------------------
vtkKWPushButton *vtkKWHM1DXYPlotWidget::GetKWUpdateRangeButton()
{
	return this->KWUpdateRangeButton;
}

void vtkKWHM1DXYPlotWidget::SetKWUpdateRangeButton(vtkKWPushButton *b)
{
	this->KWUpdateRangeButton = b;
}

//----------------------------------------------------------------------------
vtkKWPushButton *vtkKWHM1DXYPlotWidget::GetKWRemoveButton()
{
	return this->KWRemoveButton;
}

void vtkKWHM1DXYPlotWidget::SetKWRemoveButton(vtkKWPushButton *b)
{
	this->KWRemoveButton = b;
}

//----------------------------------------------------------------------------
vtkPiecewiseFunction *vtkKWHM1DXYPlotWidget::GetPiecewiseFunction()
{
	return this->PiecewiseFunction;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetPiecewiseFunction(vtkPiecewiseFunction *f)
{
	this->PiecewiseFunction = f;
}

///////////////////////////////Set and Get PressDyn///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxPressDyn(double maxPressDyn)
{
	this->MaxPressDynControl = maxPressDyn;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxPressDyn()
{
	this->MaxPressDynControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxPressDynControl < this->MaxPressDyn[i])
			this->MaxPressDynControl = this->MaxPressDyn[i];
		}
	return this->MaxPressDynControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinPressDyn(double minPressDyn)
{
	this->MinPressDynControl = minPressDyn;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinPressDyn()
{
	this->MinPressDynControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinPressDynControl > this->MinPressDyn[i])
			this->MinPressDynControl = this->MinPressDyn[i];
		}
	return this->MinPressDynControl;
}

///////////////////////////////Set and Get PressMMHG///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxPressMMHG(double maxPressMMHG)
{
	this->MaxPressMMHGControl = maxPressMMHG;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxPressMMHG()
{
	this->MaxPressMMHGControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxPressMMHGControl < this->MaxPressMMHG[i])
			this->MaxPressMMHGControl = this->MaxPressMMHG[i];
		}
	return this->MaxPressMMHGControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinPressMMHG(double minPressMMHG)
{
	this->MinPressMMHGControl = minPressMMHG;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinPressMMHG()
{
	this->MinPressMMHGControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinPressMMHGControl > this->MinPressMMHG[i])
			this->MinPressMMHGControl = this->MinPressMMHG[i];
		}
	return this->MinPressMMHGControl;
}

///////////////////////////////Set and Get Area///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxArea(double maxArea)
{
	this->MaxAreaControl = maxArea;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxArea()
{
	this->MaxAreaControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxAreaControl < this->MaxArea[i])
			this->MaxAreaControl = this->MaxArea[i];
		}
	return this->MaxAreaControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinArea(double minArea)
{
	this->MinAreaControl = minArea;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinArea()
{
	this->MinAreaControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinAreaControl > this->MinArea[i])
			this->MinAreaControl = this->MinArea[i];
		}
	return this->MinAreaControl;
}

///////////////////////////////Set and Get Flow///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxFlow(double maxFlow)
{
	this->MAX_FLOW = maxFlow;
	this->MaxFlowControl = maxFlow;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxFlow()
{
	this->MaxFlowControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxFlowControl < this->MaxFlow[i])
			this->MaxFlowControl = this->MaxFlow[i];
		}
	return this->MAX_FLOW;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinFlow(double minFlow)
{
	this->MIN_FLOW = minFlow;
	this->MinFlowControl = minFlow;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinFlow()
{
	this->MinFlowControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinFlowControl > this->MinFlow[i])
			this->MinFlowControl = this->MinFlow[i];
		}
	return this->MIN_FLOW;
}

///////////////////////////////Set and Get VelocityCM///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxVelocityCM(double maxVelocityCM)
{
	this->MaxVelocityCMControl = maxVelocityCM;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxVelocityCM()
{
	this->MaxVelocityCMControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxVelocityCMControl < this->MaxVelocityCM[i])
			this->MaxVelocityCMControl = this->MaxVelocityCM[i];
		}
	return this->MaxVelocityCMControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinVelocityCM(double minVelocityCM)
{
	this->MinVelocityCMControl = minVelocityCM;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinVelocityCM()
{
	this->MinVelocityCMControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinVelocityCMControl > this->MinVelocityCM[i])
			this->MinVelocityCMControl = this->MinVelocityCM[i];
		}
	return this->MinVelocityCMControl;
}

///////////////////////////////Set and Get VelocityM///////////////////////////////
//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMaxVelocityM(double maxVelocityM)
{
	this->MaxVelocityMControl = maxVelocityM;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxVelocityM()
{
	this->MaxVelocityMControl = -10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MaxVelocityMControl < this->MaxVelocityM[i])
			this->MaxVelocityMControl = this->MaxVelocityM[i];
		}
	return this->MaxVelocityMControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetMinVelocityM(double minVelocityM)
{
	this->MinVelocityMControl = minVelocityM;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinVelocityM()
{
	this->MinVelocityMControl = 10000000;
	for(int i=0; i<MAX_CURVES; i++)
		{
		if(this->MinVelocityMControl > this->MinVelocityM[i])
			this->MinVelocityMControl = this->MinVelocityM[i];
		}
	return this->MinVelocityMControl;
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ShowHideElem()
{
	for (int i=0; i<MAX_CURVES; i++)
		{
//		if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
//			this->BoolShowHideCheckButton[i] = false;
//		else if (this->ShowHideCheckButton[i]->GetSelectedState() == 1)
//			this->BoolShowHideCheckButton[i] = true;

		this->KWTitleToGraphicFrame->Script("pack forget %s", this->TitleToGraphic[i]->GetWidgetName());
		if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
			this->KWTitleToGraphicFrame->Script("pack %s -side left -fill y", this->TitleToGraphic[i]->GetWidgetName());
		}


	for (int i=0; i < PLOTS; i++)
		{
		this->ShowHideControl(i);
//		this->ResizeCurves(i);
		if( this->TypePlot->GetValue(i))
    	this->KWRenderWidget->AddViewProp(this->HMXYPlot[i]);
		}

	this->ShowHMXYPlotWidget();
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMaxPlotsValues(int plot)
{
if(plot == 0)
	return this->GetMaxPressDyn();
else if(plot == 1)
	return this->GetMaxPressMMHG();
else if(plot == 2)
	return this->GetMaxArea();
else if(plot == 3)
	return this->GetMaxFlow();
else if(plot == 4)
	return this->GetMaxVelocityCM();
else if(plot == 5)
	return this->GetMaxVelocityM();
else
	return -1;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::GetMinPlotsValues(int plot)
{
	if(plot == 0)
		return this->GetMinPressDyn();
	else if(plot == 1)
		return this->GetMinPressMMHG();
	else if(plot == 2)
		return this->GetMinArea();
	else if(plot == 3)
		return this->GetMinFlow();
	else if(plot == 4)
		return this->GetMinVelocityCM();
	else if(plot == 5)
		return this->GetMinVelocityM();
	else
		return -1;
}

//----------------------------------------------------------------------------
double vtkKWHM1DXYPlotWidget::ShowHideControl(int id)
{

	//Pressão dyn/cm²
	if(id==0)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxPressDyn[i] == this->MaxPressDynControl)
					{
					this->PressDynHideShowMAX[i] = this->MaxPressDyn[i];//copio do vetor principal para o vetor auxiliar
					this->MaxPressDyn[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxPressDynControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxPressDynControl < this->MaxPressDyn[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxPressDynControl = this->MaxPressDyn[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinPressDyn[i] == this->MinPressDynControl)
					{
					this->PressDynHideShowMIN[i] = this->MinPressDyn[i];//copio do vetor principal para o vetor auxiliar
					this->MinPressDyn[i] = 10000000;//coloco um valor default no vetor principal
					this->MinPressDynControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinPressDynControl > this->MinPressDyn[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinPressDynControl = this->MinPressDyn[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxPressDyn[i] == -10000000)
					{
					this->MaxPressDyn[i] = this->PressDynHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->PressDynHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxPressDynControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxPressDynControl < this->MaxPressDyn[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxPressDynControl = this->MaxPressDyn[j];
					}
				if(this->MinPressDyn[i] == 10000000)
					{
					this->MinPressDyn[i] = this->PressDynHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->PressDynHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinPressDynControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinPressDynControl > this->MinPressDyn[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinPressDynControl = this->MinPressDyn[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinPressDynControl, this->MaxPressDynControl);
		}

	//Pressao mmHg
	else if(id==1)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxPressMMHG[i] == this->MaxPressMMHGControl)
					{
					this->PressMMHGHideShowMAX[i] = this->MaxPressMMHG[i];//copio do vetor principal para o vetor auxiliar
					this->MaxPressMMHG[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxPressMMHGControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxPressMMHGControl < this->MaxPressMMHG[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxPressMMHGControl = this->MaxPressMMHG[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinPressMMHG[i] == this->MinPressMMHGControl)
					{
					this->PressMMHGHideShowMIN[i] = this->MinPressMMHG[i];//copio do vetor principal para o vetor auxiliar
					this->MinPressMMHG[i] = 10000000;//coloco um valor default no vetor principal
					this->MinPressMMHGControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinPressMMHGControl > this->MinPressMMHG[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinPressMMHGControl = this->MinPressMMHG[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxPressMMHG[i] == -10000000)
					{
					this->MaxPressMMHG[i] = this->PressMMHGHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->PressMMHGHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxPressMMHGControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxPressMMHGControl < this->MaxPressMMHG[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxPressMMHGControl = this->MaxPressMMHG[j];
					}
				if(this->MinPressMMHG[i] == 10000000)
					{
					this->MinPressMMHG[i] = this->PressMMHGHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->PressMMHGHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinPressMMHGControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinPressMMHGControl > this->MinPressMMHG[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinPressMMHGControl = this->MinPressMMHG[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinPressMMHGControl, this->MaxPressMMHGControl);
		}

	//Area
	else if(id==2)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxArea[i] == this->MaxAreaControl)
					{
					this->AreaHideShowMAX[i] = this->MaxArea[i];//copio do vetor principal para o vetor auxiliar
					this->MaxArea[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxAreaControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxAreaControl < this->MaxArea[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxAreaControl = this->MaxArea[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinArea[i] == this->MinAreaControl)
					{
					this->AreaHideShowMIN[i] = this->MinArea[i];//copio do vetor principal para o vetor auxiliar
					this->MinArea[i] = 10000000;//coloco um valor default no vetor principal
					this->MinAreaControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinAreaControl > this->MinArea[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinAreaControl = this->MinArea[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxArea[i] == -10000000)
					{
					this->MaxArea[i] = this->AreaHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->AreaHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxAreaControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxAreaControl < this->MaxArea[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxAreaControl = this->MaxArea[j];
					}
				if(this->MinArea[i] == 10000000)
					{
					this->MinArea[i] = this->AreaHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->AreaHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinAreaControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinAreaControl > this->MinArea[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinAreaControl = this->MinArea[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinAreaControl, this->MaxAreaControl);
		}
	//Fluxo
	else if(id==3)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxFlow[i] == this->MaxFlowControl)
					{
					this->FlowHideShowMAX[i] = this->MaxFlow[i];//copio do vetor principal para o vetor auxiliar
					this->MaxFlow[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxFlowControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxFlowControl < this->MaxFlow[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxFlowControl = this->MaxFlow[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinFlow[i] == this->MinFlowControl)
					{
					this->FlowHideShowMIN[i] = this->MinFlow[i];//copio do vetor principal para o vetor auxiliar
					this->MinFlow[i] = 10000000;//coloco um valor default no vetor principal
					this->MinFlowControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinFlowControl > this->MinFlow[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinFlowControl = this->MinFlow[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxFlow[i] == -10000000)
					{
					this->MaxFlow[i] = this->FlowHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->FlowHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxFlowControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxFlowControl < this->MaxFlow[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxFlowControl = this->MaxFlow[j];
					}
				if(this->MinFlow[i] == 10000000)
					{
					this->MinFlow[i] = this->FlowHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->FlowHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinFlowControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinFlowControl > this->MinFlow[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinFlowControl = this->MinFlow[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinFlowControl, this->MaxFlowControl);
		}

	//Velocidade cm/s
	else if(id==4)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxVelocityCM[i] == this->MaxVelocityCMControl)
					{
					this->VelocityCMHideShowMAX[i] = this->MaxVelocityCM[i];//copio do vetor principal para o vetor auxiliar
					this->MaxVelocityCM[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxVelocityCMControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxVelocityCMControl < this->MaxVelocityCM[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxVelocityCMControl = this->MaxVelocityCM[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinVelocityCM[i] == this->MinVelocityCMControl)
					{
					this->VelocityCMHideShowMIN[i] = this->MinVelocityCM[i];//copio do vetor principal para o vetor auxiliar
					this->MinVelocityCM[i] = 10000000;//coloco um valor default no vetor principal
					this->MinVelocityCMControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinVelocityCMControl > this->MinVelocityCM[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinVelocityCMControl = this->MinVelocityCM[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxVelocityCM[i] == -10000000)
					{
					this->MaxVelocityCM[i] = this->VelocityCMHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->VelocityCMHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxVelocityCMControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxVelocityCMControl < this->MaxVelocityCM[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxVelocityCMControl = this->MaxVelocityCM[j];
					}
				if(this->MinVelocityCM[i] == 10000000)
					{
					this->MinVelocityCM[i] = this->VelocityCMHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->VelocityCMHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinVelocityCMControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinVelocityCMControl > this->MinVelocityCM[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinVelocityCMControl = this->MinVelocityCM[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinVelocityCMControl, this->MaxVelocityCMControl);
		}

	//Velocidade m/s
	else if(id==5)
		{
	  this->HMXYPlot[id]->RemoveAllInputs();
	  vtkDoubleArray *Array;
	  int count = 0;

	  for ( int i=0; i<this->NumberOfGraphics; i++ )
	  	{
	  	vtkHMDataObjectToPlot *DataPlot = vtkHMDataObjectToPlot::New();
	  	if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
	  		{
	//  	  		this->HMXYPlot[id]->SetPlotColor(i, 0.8, 0.8,	0.8);
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->RemoveDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
		  				 this->sizeOfArrays, this->posInArray));
	  		count ++;
	  		}
	  	else
	  		{
	  		Array = vtkDoubleArray::SafeDownCast(this->DataArrayCollection[id]->GetItem(i));
	  		HMXYPlot[id]->AddDataObjectInput(DataPlot->ConvertArraysToDataObject(Array, this->time,
	  					 this->sizeOfArrays, this->posInArray));
	  		this->HMXYPlot[id]->SetPlotColor(i-count, 	PlotColor[i][0], PlotColor[i][1], PlotColor[i][2]);
	  		}
	  	DataPlot->Delete();
	  	}

		for (int i=0; i<MAX_CURVES; i++)
			{
			//Se Show desmarcado
			if(this->ShowHideCheckButton[i]->GetSelectedState() == 0)
				{
				//Se a curva que está sendo retirada for a maior do gráfico
				if (this->MaxVelocityM[i] == this->MaxVelocityMControl)
					{
					this->VelocityMHideShowMAX[i] = this->MaxVelocityM[i];//copio do vetor principal para o vetor auxiliar
					this->MaxVelocityM[i] = -10000000;//coloco um valor default no vetor principal
					this->MaxVelocityMControl = -10000000;//coloco um valor default no máximo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MaxVelocityMControl < this->MaxVelocityM[j])//percorro o vetor principal para atualizar o máximo do gráfico
							this->MaxVelocityMControl = this->MaxVelocityM[j];
					}
				//Se a curva que está sendo retirada for a menor do gráfico
				if (this->MinVelocityM[i] == this->MinVelocityMControl)
					{
					this->VelocityMHideShowMIN[i] = this->MinVelocityM[i];//copio do vetor principal para o vetor auxiliar
					this->MinVelocityM[i] = 10000000;//coloco um valor default no vetor principal
					this->MinVelocityMControl = 10000000;//coloco um valor default no minimo do gráfico
					for(int j=0; j<MAX_CURVES; j++)
						if(this->MinVelocityMControl > this->MinVelocityM[j])//percorro o vetor principal para atualizar o minimo do gráfico
							this->MinVelocityMControl = this->MinVelocityM[j];
					}
				}
			//Se Show Marcado
			else if(this->ShowHideCheckButton[i]->GetSelectedState() == 1)
				{
				//Se o vetor auxiliar tiver um valor que não o default
				if(this->MaxVelocityM[i] == -10000000)
					{
					this->MaxVelocityM[i] = this->VelocityMHideShowMAX[i];//copio do vetor auxiliar de volta para o vetor principal
					this->VelocityMHideShowMAX[i] = -10000000;//coloco valor default no vetor auxiliar
		  		this->MaxVelocityMControl = -10000000;//coloco um valor default no máximo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MaxVelocityMControl < this->MaxVelocityM[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MaxVelocityMControl = this->MaxVelocityM[j];
					}
				if(this->MinVelocityM[i] == 10000000)
					{
					this->MinVelocityM[i] = this->VelocityMHideShowMIN[i];//copio do vetor auxiliar de volta para o vetor principal
					this->VelocityMHideShowMIN[i] = 10000000;//coloco valor default no vetor auxiliar
		  		this->MinVelocityMControl = 10000000;//coloco um valor default no minimo do gráfico
		  		for(int j=0; j<MAX_CURVES; j++)
		  			if(this->MinVelocityMControl > this->MinVelocityM[j])//percorro o vetor principal para atualizar o máximo do gráfico
		  				this->MinVelocityMControl = this->MinVelocityM[j];
		  		}
				}
			}
		this->HMXYPlot[id]->SetYRange(this->MinVelocityMControl, this->MaxVelocityMControl);
		}
	return 0;
}

//----------------------------------------------------------------------------
bool vtkKWHM1DXYPlotWidget::ExistMeshIDForTitle(int pos)
{
	for(int i = 0; i<MAX_CURVES; i++)
		if(pos == this->MeshIDForTitle[i])
			return true;

	return false;
}

void vtkKWHM1DXYPlotWidget::SetExportDirectory()
{
	this->ExportDialog->SetTitle("Exports 1D Simulation");
	this->ExportDialog->ChooseDirectoryOn();
	this->ExportDialog->SetLastPath("");
	this->ExportDialog->Invoke();

	char* directory;
	directory = this->ExportDialog->GetLastPath();
	if (!strcmp(directory,""))
		return;
	else
		this->ExportData(directory);
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::ExportData(char* directory)
{
	//[0]pressionDyn
	//[1]pressionMMHG
	//[2]area
	//[3]flow
	//[4]velocityCMs
	//[5]velocityMs

	for(int i = 0; i<MAX_CURVES; i++)//MAX_CURVES contem o numero total de curvas plotadas
		{
		if( (this->MeshIDForTitle[i] != -1) && (this->TypeForTitle[i] != '1')
				&& (this->ShowHideCheckButton[i]->GetSelectedState() == 1))
			{
			vtkDoubleArray *pressionDyn;
			vtkDoubleArray *pressionMMHG;
			vtkDoubleArray *area;
			vtkDoubleArray *flow;
			vtkDoubleArray *velocityCMs;
			vtkDoubleArray *velocityMs;

			pressionDyn 	= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[0]->GetItem(i));
			pressionMMHG 	= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[1]->GetItem(i));
			area 					= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[2]->GetItem(i));
			flow 					= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[3]->GetItem(i));
			velocityCMs		= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[4]->GetItem(i));
			velocityMs		= vtkDoubleArray::SafeDownCast(this->DataArrayCollection[5]->GetItem(i));
			char name[200];

			if ( (this->TypeForTitle[i] == 'N') || (this->TypeForTitle[i] == 'T') )
				sprintf(name, "%s/Node_%d.txt", directory, this->MeshIDForTitle[i]);
			else if ( this->TypeForTitle[i] == 'E' )
				sprintf(name, "%s/Element_%d.txt", directory, this->MeshIDForTitle[i]);

			ofstream os(name);

			//para escrever a legenda no arquivo
		  os << "*T[sec] ";
		  if(this->PlotOptions->GetValue(0))
		  	os << "P[dyn/cm^2] ";
		  if(this->PlotOptions->GetValue(1))
		  	os << "P[mmHg] ";
		  if(this->PlotOptions->GetValue(2))
		  os << "A[cm^2] ";
		  if(this->PlotOptions->GetValue(3))
		  os << "Q[cm^3] ";
		  if(this->PlotOptions->GetValue(4))
		  	os << "V[cm/s] ";
		  if(this->PlotOptions->GetValue(5))
		  	os << "V[m/s] ";
			os << "\n";

			//para escrever os dados no arquivo
			for ( int k=0; k<this->time->GetNumberOfTuples(); k++ )
				{
				os << this->time->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(0))
			  	os << pressionDyn->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(1))
			  	os << pressionMMHG->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(2))
			  	os << area->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(3))
			  	os << flow->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(4))
			  	os << velocityCMs->GetValue(k) << " ";
			  if(this->PlotOptions->GetValue(5))
			  	os << velocityMs->GetValue(k) << " ";
				os << "\n";
			  }
		  os.close();
			}
		}
}

//----------------------------------------------------------------------------
int vtkKWHM1DXYPlotWidget::GetMeshIDForTitle(int MAX_CURVES)
{
	return this->MeshIDForTitle[MAX_CURVES];
}

//----------------------------------------------------------------------------
char vtkKWHM1DXYPlotWidget::GetTypeForTitle(int MAX_CURVES)
{
	return this->TypeForTitle[MAX_CURVES];
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetPlotOptions(vtkIntArray *plotOptions)
{

	this->PlotOptions->SetValue(0, plotOptions->GetValue(0));//Pressure1Plot
	this->PlotOptions->SetValue(1, plotOptions->GetValue(1));//Pressure2Plot
	this->PlotOptions->SetValue(2, plotOptions->GetValue(2));//AreaPlot
	this->PlotOptions->SetValue(3, plotOptions->GetValue(3));//FlowPlot
	this->PlotOptions->SetValue(4, plotOptions->GetValue(4));//Velocity1Plot
	this->PlotOptions->SetValue(5, plotOptions->GetValue(5));//Velocity1Plot

}
//----------------------------------------------------------------------------
vtkDoubleArray* vtkKWHM1DXYPlotWidget::GetDataArrayCollection(int array, int pos)
{
	return vtkDoubleArray::SafeDownCast( this->DataArrayCollection[array]->GetItem(pos) );
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetRangeBetweenEntries(int id, int entry)
{
	double min = this->KWYAxisEntryMin[id]->GetValueAsDouble();
	double max = this->KWYAxisEntryMax[id]->GetValueAsDouble();

	if(entry == 0)//comando que vem do entry min
		{
		if(min < YAxisMin[id])
			this->KWYAxisEntryMin[id]->SetValueAsDouble(YAxisMin[id]);
		else if(min > max)
			this->KWYAxisEntryMin[id]->SetValueAsDouble(max);
		}
	else if (entry == 1)//comando que vem do entry max
		{
		if(max > YAxisMax[id])
			this->KWYAxisEntryMax[id]->SetValueAsDouble(YAxisMax[id]);
		else if(max < min)
			this->KWYAxisEntryMax[id]->SetValueAsDouble(min);
		}
}

//----------------------------------------------------------------------------
void vtkKWHM1DXYPlotWidget::SetRenderBackgroundColor(int r, int g, int b)
{
	double *color = this->ColorButton->GetForegroundColor();
//	cout << "R: " << endl;//color[0] << " G: " << color[1] << " B: " << color[2] << endl;
	this->KWRenderWidget->SetBackgroundColor(color[0], color[1], color[2]);
}

