/*
 * $Id$
 */

/*=========================================================================

  Project:   HeMoLab
  Module:    vtkKWHM1DXYPlotWidget

  Author: Jan Palach
          palach16@yahoo.com.br

=========================================================================*/
// .NAME vtkKWHM1DXYPlotWidget - Used to show an XY plot with informations about
//vtkKWHM1DXYPlotWidget.
// .SECTION Description
//The class implement methods to show XYPlot with information about terminals and segments.
//e.g: Pressure, area, flow.

#ifndef VTKKWHM1DXYPLOTWIDGET_H_
#define VTKKWHM1DXYPLOTWIDGET_H_

#include "vtkKWTopLevel.h"

#include "vtkDoubleArray.h"
#include "vtkCommand.h"
#include "vtkIntArray.h"
#include "vtkTextProperty.h"

#include "vtkInteractorStyleSwitch.h"
#include "vtkKWApplication.h"
#include "vtkKWLabel.h"
#include "vtkKWRenderWidget.h"
#include "vtkKWWindow.h"
#include "vtkRenderWindow.h"
#include "vtkKWFrame.h"
#include "vtkKWFrameWithScrollbar.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWRange.h"
#include "vtkKWPushButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWChangeColorButton.h"

#include "vtkHMGraphicProperty.h"

#include "vtkPiecewiseFunction.h"
#include "vtkKWPiecewiseFunctionEditor.h"

//#include <stdio.h>
#include <stdlib.h>

using namespace std;

class vtkImageData;
class vtkKWSplitFrame;
class vtkKWToolbar;

//Para ser usado no futuro no acréscimo de ballon helps!!!
//#include "vtkBalloonWidget.h"

class vtkHMDataObjectToPlot;   //This class manage data to plot curve.
class vtkHMXYPlot;             //This class manage all this properties to an XYPlot.
class vtkDataArrayCollection;

class VTK_EXPORT vtkKWHM1DXYPlotWidget : public vtkKWTopLevel
{
public:
  static vtkKWHM1DXYPlotWidget* New();
  vtkTypeRevisionMacro(vtkKWHM1DXYPlotWidget, vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the widget.
  virtual void Create(vtkKWApplication *app);

  // Description:
  // Display the toplevel.
  virtual void Display();

  // Description:
  // Saves the current plot to a file.
  void Save();

  //Description:
  //This method make an HMXYPlotWidget composed by curves.
  vtkHMXYPlot** MakeHMXYPlotWidget(vtkDoubleArray *doubleArray, vtkDoubleArray *timeInstant, char type, int meshID);

  //Description:
  //Show the Plot gerado in the ViewPort.
  void ShowHMXYPlotWidget();

  //Description:
  //This method retrieve the HMXYPlot with a reference to this class.
  vtkHMXYPlot** GetHMXYPlot();
  void SetHMXYPlot(vtkHMXYPlot**);

  //Description:
  //This method build plot from received id.
	void BuildPlot(int id, char type, int meshID);

	//Description:
  //This method build plot from received id.
	void RebuildPlot();

  //Description:
  //This method build a plot with initial data of flow in heart.
  //To plot flow the id is equal to two.
  void BuildHeartPlot(int id, vtkDoubleArray* flow, vtkDoubleArray* time);

  //Description:
  //This method set the type of plot to show.
  void SetTypePlot(vtkIntArray *type);

  //Description:
  //This method get the type plot selected.
  vtkIntArray* GetTypePlot()
  	{
  	return TypePlot;
  	}

  //Description:
  //Methods Set/Get to NUmberOfPlots.
  vtkSetMacro(NumberOfPlots, int);
  vtkGetMacro(NumberOfPlots, int);

  //Description:
  //Methods Set/Get to NumberOfGraphics.
  vtkSetMacro(NumberOfGraphics, int);
  vtkGetMacro(NumberOfGraphics, int);

  //Description:
  //Methods Set/Get to NumberOfGraphicsSum.
  vtkSetMacro(NumberOfGraphicsSum, int);
  vtkGetMacro(NumberOfGraphicsSum, int);

  //Description:
  //Methods Set/Get to TimeOfMax.
  vtkSetMacro(TMax, double);
  vtkGetMacro(TMax, double);

  //Description:
  //Methods Set/Get to TimeOfMin.
  vtkSetMacro(TMin, double);
  vtkGetMacro(TMin, double);

  //Description:
  //Methods Set/Get to MaxPress in dyn/cm².
  void SetMaxPressDyn(double maxPressDyn);
  double GetMaxPressDyn();
  //Description:
  //Methods Set/Get to MinPress in dyn/cm².
  void SetMinPressDyn(double minPressDyn);
  double GetMinPressDyn();

  //Description:
  //Methods Set/Get to MaxPress in mmHg.
  void SetMaxPressMMHG(double maxPressMMHG);
  double GetMaxPressMMHG();
  //Description:
  //Methods Set/Get to MinPress in mmHg.
  void SetMinPressMMHG(double minPressMMHG);
  double GetMinPressMMHG();

  //Description:
  //Methods Set/Get to MaxArea.
  void SetMaxArea(double maxArea);
  double GetMaxArea();
  //Description:
  //Methods Set/Get to MinArea.
  void SetMinArea(double minArea);
  double GetMinArea();

  //Description:
  //Methods Set/Get to MaxFlow.
  void SetMaxFlow(double maxFlow);
  double GetMaxFlow();
  //Description:
  //Methods Set/Get to MinFlow.
  void SetMinFlow(double minFlow);
  double GetMinFlow();

  //Description:
  //Methods Set/Get to MaxVelocityCM.
  void SetMaxVelocityCM(double maxVelocityCM);
  double GetMaxVelocityCM();
  //Description:
  //Methods Set/Get to MinVelocityCM.
  void SetMinVelocityCM(double minVelocityCM);
  double GetMinVelocityCM();

  //Description:
  //Methods Set/Get to MaxVelocityM.
  void SetMaxVelocityM(double maxVelocityM);
  double GetMaxVelocityM();
  //Description:
  //Methods Set/Get to MinVelocityM.
  void SetMinVelocityM(double minVelocityM);
  double GetMinVelocityM();

  //Description:
  //Methods Set/Get to MaxCurves.
  vtkSetMacro(MaxCurves, int);
  vtkGetMacro(MaxCurves, int);

  //Description:
  //Methods Set/Get to Pression Dyn/cm².
  void SetPressionDyn(vtkDoubleArray *p);
	vtkDoubleArray* GetPressionDyn();

	//Description:
  //Methods Set/Get to Pression mmHg.
	void SetPressionMMHG(vtkDoubleArray *p);
	vtkDoubleArray* GetPressionMMHG();

	//Description:
  //Methods Set/Get to Area.
	void SetArea(vtkDoubleArray *a);
	vtkDoubleArray* GetArea();

	//Description:
  //Methods Set/Get to Flow.
	void SetFlow(vtkDoubleArray *f);
	vtkDoubleArray* GetFlow();

	//Description:
  //Methods Set/Get to Time
	void SetTime(vtkDoubleArray *t);
	vtkDoubleArray* GetTime();

	//Description:
  //Methods Set/Get to Maximum of all points of the tree,
  //this value exist in DataOut Object
	void SetMax(double *m, int t);
	double *GetMax();

	//Description:
  //Methods Set/Get to Minimum of all points of the tree,
  //this value exist in DataOut Object
	void SetMin(double *m, int t);
	double *GetMin();

	// Description:
	// Method for clear data of the window, XYPlot and actors.
	void Clear();

  // Description:
	// Set/Get KWRenderWidget
	void SetKWRenderWidget(vtkKWRenderWidget *ren);
	vtkKWRenderWidget *GetKWRenderWidget();

	// Description:
	// Set/Get KWFrameWithScrollbar
	vtkKWFrameWithScrollbar *GetKWFrameWithScrollbar();
	void SetKWFrameWithScrollbar(vtkKWFrameWithScrollbar *Frame);

	// Description:
	// Set/Get KWTimeRange
	vtkKWRange *GetKWTimeRange();
	void SetKWTimeRange(vtkKWRange *range);

	// Description:
	// Set/Get KWValueRange
	vtkKWRange *GetKWValueRange();
	void SetKWValueRange(vtkKWRange *range);

	// Description:
	// Set/Get KWUpdateRangeButton
	vtkKWPushButton *GetKWUpdateRangeButton();
	void SetKWUpdateRangeButton(vtkKWPushButton *b);

	// Description:
	// Set/Get KWRemoveButton
	vtkKWPushButton *GetKWRemoveButton();
	void SetKWRemoveButton(vtkKWPushButton *b);

	// Description:
	// Update range of time of the plot
	void UpdateRange();

	// Description:
	// Remove one curve of the plot
	void RemoveCurve();

  //Description:
  //Methods Set/Get to EditCurveHeart.
  vtkSetMacro(EditCurveHeart, bool);
  vtkGetMacro(EditCurveHeart, bool);

  // Description:
	// Set/Get PiecewiseFunction
	vtkPiecewiseFunction *GetPiecewiseFunction();
	void SetPiecewiseFunction(vtkPiecewiseFunction *f);

	// Description:
	// Update edited heart curve
	void UpdateHeartCurve();

	// Description:
	// Set/Get heart curve.
	vtkDoubleArray *GetHeartCurve();
	void SetHeartCurve(vtkDoubleArray *);

	// Description:
  // Calculate area of the heart curve.
	void CalculateArea();

	// Description:
	// Calculata heart curve scale
	void ApplyScale(double scale);

	//Description:
  //Methods Set/Get to Area1.
  vtkSetMacro(Area1, double);
  vtkGetMacro(Area1, double);

	//Description:
  //Methods Set/Get to Area2.
  vtkSetMacro(Area2, double);
  vtkGetMacro(Area2, double);

  // Description:
  // Set/Get to scale factor
  vtkSetMacro(ScaleFactor, double);
  vtkGetMacro(ScaleFactor, double);

  // Description:
  // Button for clear render and legends
  vtkKWPushButton*  ClearButton;

  // Decription:
  // Show graphic legend
  void ShowLegends();

  // Decription:
  // Show/Hide Elements in active plots
  void ShowHideElem();

  // Decription:
  // For update Max and Min of Plots when Hide/Show is clicked
  double ShowHideControl(int id);

  // Decription:
  // Show Frame for edition of Y Axis of graphics
  void ShowEditYAxis();

  // Decription:
  // Get Max value of plots in Y axis
  double GetMaxPlotsValues(int plot);

  // Decription:
  // Get Min value of plots in Y axis
  double GetMinPlotsValues(int plot);

  // Decription:
  // Return if specified MeshId is in list
  bool ExistMeshIDForTitle(int pos);

  // Decription:
  // Get method for KWPiecewiseFunctionEditor
  vtkGetObjectMacro(KWPiecewiseFunctionEditor, vtkKWPiecewiseFunctionEditor);

  // Decription:
  // Set the directory for export results od simulation
  void SetExportDirectory();

  // Decription:
  // Export data in directory choose
  void ExportData(char* directory);

  int GetMeshIDForTitle(int MAX_CURVES);
  char GetTypeForTitle(int MAX_CURVES);

  void SetPlotOptions(vtkIntArray *plotOptions);

  // Decription:
  // Get method for DataArrayCollection
  vtkDoubleArray* GetDataArrayCollection(int array, int pos);


  void SetRangeBetweenEntries(int id, int entry);


  void SetRenderBackgroundColor(int r, int g, int b);

protected:
  vtkKWHM1DXYPlotWidget();
  ~vtkKWHM1DXYPlotWidget();

	// Descrition:
	// Write plot as jpg
	int WriteImage(const char* filename, const char* writerName);

	// Description:
	// Capture image in window of plot
	vtkImageData* CaptureWindow(int magnification);

	//Description:
  //This is a reference to the class HMXYPlot(Handle properties of Plot).
  vtkHMXYPlot *HMXYPlot[PLOTS];

	// Description:
	// Function for update the size of the curves in relation to range
	void ResizeCurves(int id);

  //Description:
  //used to compute relative movements
  float StartPosition[2];

  //Description:
  //This variable define the type of Plot to show in ViewPort.
  vtkIntArray *TypePlot;

  //Description:
  //This variable represent the instant of time of Max(Pression, Area, Flow).
  double TMax;

  //Description:
  //This variable represent the instant of time of Min(Pression, Area, Flow).
  double TMin;

  //Description:
  //This variable stores the MAX values for all graphics of Pression in Dyn/cm².
  double MaxPressDyn[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of Pression in Dyn/cm²
  double MaxPressDynControl;
  //Description:
  //This variable stores the MIN values for all graphics of Pression in Dyn/cm².
  double MinPressDyn[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of Pression in Dyn/cm²
  double MinPressDynControl;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double PressDynHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double PressDynHideShowMIN[MAX_CURVES];

  //Description:
  //This variable stores the MAX values for all graphics of Pression in mmHg.
  double MaxPressMMHG[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of Pression in mmHg.
  double MaxPressMMHGControl;
  //Description:
  //This variable stores the MIN values for all graphics of Pression in mmHg.
  double MinPressMMHG[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of Pression in mmHg.
  double MinPressMMHGControl;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double PressMMHGHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double PressMMHGHideShowMIN[MAX_CURVES];

  //Description:
  //This variable stores the MAX values for all graphics of Area.
  double MaxArea[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of Area.
  double MaxAreaControl;
  //Description:
  //This variable stores the MIN values for all graphics of Area.
  double MinArea[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of Area.
  double MinAreaControl;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double AreaHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double AreaHideShowMIN[MAX_CURVES];

  //Description:
  //This variable stores the MAX values for all graphics of Flow.
  double MaxFlow[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of Flow.
  double MaxFlowControl;
  //Description:
  //This variable stores the MAX value of all plotted graphs of Flow.
  //It is necessary because of heart curve.
  double MAX_FLOW;
  //Description:
  //This variable stores the MIN values for all graphics of Flow.
  double MinFlow[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of Flow.
  double MinFlowControl;
  //Description:
  //This variable stores the MAX value of all plotted graphs of Flow.
  //It is necessary because of heart curve.
  double MIN_FLOW;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double FlowHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double FlowHideShowMIN[MAX_CURVES];

  //Description:
  //This variable stores the MAX values for all graphics of velocity cm/s.
  double MaxVelocityCM[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of velocity cm/s.
  double MaxVelocityCMControl;
  //Description:
  //This variable stores the MIN values for all graphics of velocity cm/s.
  double MinVelocityCM[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of velocity cm/s.
  double MinVelocityCMControl;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double VelocityCMHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double VelocityCMHideShowMIN[MAX_CURVES];

  //Description:
  //This variable stores the MAX values for all graphics of velocity m/s.
  double MaxVelocityM[MAX_CURVES];
  //Description:
  //This variable stores the MAX value of the five plotted graphs of velocity m/s.
  double MaxVelocityMControl;
  //Description:
  //This variable stores the MIN values for all graphics of velocity m/s.
  double MinVelocityM[MAX_CURVES];
  //Description:
  //This variable stores the MIN value of the five plotted graphs of velocity m/s.
  double MinVelocityMControl;
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double VelocityMHideShowMAX[MAX_CURVES];
  //Description:
  //This variable serves as a auxiliar vector for store the curves will not be shown in the PLOT
  double VelocityMHideShowMIN[MAX_CURVES];

  double Medium[PLOTS][MAX_CURVES];

  //Description:vtkKWHM1DXYPlotWidget::
  //This variable represent the title of Plot, composed by (Name of segment and Id of Node in the segment).
  const char *TitleOfPlot;

  //Description:
  //Store pression curve in Dyn/cm².
  vtkDoubleArray* pressionDyn;

  //Description:
  //Store pression curve in mmHg.
  vtkDoubleArray* pressionMMHG;

  //Description:
  //Store area curve.
  vtkDoubleArray* area;

  //Description:
  //Store flow curve.
  vtkDoubleArray* flow;

  //Description:
  //Store velocityCMs curve.
  vtkDoubleArray *velocityCMs;
  
  //Description:
  //Store velocityMs curve.
  vtkDoubleArray *velocityMs;

  //Description:
  //Store time curve.
  vtkDoubleArray* time;

  //Description:
  //Number of graphics ploted
  int NumberOfGraphics;

  //Description:
  //Total of the number of graphics ploted
  int NumberOfGraphicsSum;

	//Description:
	//Return maximum value of vtkDoubleArray
  double MaxValue(vtkDoubleArray *array);

  //Description:
	//Return minimum value of vtkDoubleArray
  double MinValue(vtkDoubleArray *array);

	//Description:
	//Vector with maximum value of pressure, flow, area
  double *Max;

  //Description:
	//Vector with minimum value of pressure, flow, area
  double *Min;

  // Description:
  //Collection for curves
	//[0]pressionDyn
	//[1]pressionMMHG
	//[2]area
	//[3]flow
	//[4]velocityCMs
	//[5]velocityMs
  vtkDataArrayCollection *DataArrayCollection[PLOTS];

  // Description:
  // Size of arrays of the curves
  int sizeOfArrays;

  // Description:
  // Initial position of the array
  int posInArray;

	//Description:
  //This variable, control the number of plots to show.
  int NumberOfPlots;

  //Description:
  //controls the maximum number of plots that can be shown at same time
  int MaxCurves;

  int MeshIDForTitle[MAX_CURVES];
  char TypeForTitle[MAX_CURVES];

  // Description:
  // Saves the current plot to a file.
  virtual void Save(const char* fileName);

  /*
   *
   * Components from interface
   *
   */

  // Description:
  // Frame with two separate frames where the range of the time,
  //toolbar and render are adder.
  vtkKWSplitFrame *SplitFrame;

  // Description:
  // Toolbar with buttons Save, Clear and UpdateRange
  vtkKWToolbar *Toolbar;

  // Description:
  // Render for plot
  vtkKWRenderWidget *KWRenderWidget;

  // Description:
  // Frame with legends of the maximum and minimum of each curve
  vtkKWFrameWithScrollbar *KWFrameWithScrollbar;

  // Description:
  // Frames to legend of maximum and minimum of the curves
//  vtkKWFrameWithLabel *KWFrameWithLabel[PLOTS];
  vtkKWFrameWithLabel *KWFrameWithLabel[MAX_CURVES];

  // Description:
  // Label to legend of maximum and minimum of the curves
//  vtkKWLabel *Label[PLOTS][MAX_CURVES];
  vtkKWLabel *Label[MAX_CURVES][PLOTS];


  // Description:
  // Range for the time
  vtkKWRange *KWTimeRange;

  vtkKWRange *KWValueRange;

  // Description:
  // Button for update range
  vtkKWPushButton *KWUpdateRangeButton;

  // Description:
  // Button for remove curve
  vtkKWPushButton *KWRemoveButton;

  // Description:
  // Button for save the plot
  vtkKWPushButton*  SaveButton;

  // Description:
  // Button for export the results os simulation of txt file
  vtkKWPushButton*  ExportResultButton;

  // Description:
  // Dialog for choose the directory of simulation will be saved
  vtkKWLoadSaveDialog *ExportDialog;

  /*
   *
   * Components of the heart
   *
   */
  // Description:
  // Function with values and time of the heart curve
  vtkPiecewiseFunction *PiecewiseFunction;

	// Description:
	// Interface of edit of the heart curve
	vtkKWPiecewiseFunctionEditor *KWPiecewiseFunctionEditor;

  // Description:
  // Mode of edit curve of the heart, case true, edit curve,
  // case false, it visualizes curve.
  bool EditCurveHeart;

  // Description:
  // Array of the heart curve
  vtkDoubleArray *HeartCurve;

  // Description:
  // Label and entry of the area 1
  vtkKWLabel *HeartAreaLabel1;
  vtkKWEntry *HeartAreaEntry1;

  // Description:
  // Label and entry of the area 2
  vtkKWLabel *HeartAreaLabel2;
  vtkKWEntry *HeartAreaEntry2;

  // Description:
  // Button for calculate areas
  vtkKWPushButton *CalculateAreaButton;

  // Description:
  // Area of the heart curve
  double Area1;
  double Area2;

  // Description;
  // Factor to calculate heart curve scale
  double ScaleFactor;

//  // Description:
//  // Button to calculate heart curve scale
//  vtkKWPushButton *ScaleFactorButton;
//
//  // Description:
//  // Label and entry of the scale factor for heart curve
//  vtkKWLabel *ScaleFactorLabel;
//  vtkKWEntry *ScaleFactorEntry;


	// Description:
	// RadioButton for plot types. Using in animated plot.
  vtkKWCheckButton	*ShowHideCheckButton[MAX_CURVES];

  // Description:
  // bool for contro the active elements in plots
//  bool BoolShowHideCheckButton[MAX_CURVES];

  // Description:
  // Range for the PressDyn
//  vtkKWRange *KWYAxisRange[PLOTS];
  vtkKWEntry *KWYAxisEntryMin[PLOTS];
  vtkKWEntry *KWYAxisEntryMax[PLOTS];
  vtkKWLabel *KWYAxisLabelMin[PLOTS];
  vtkKWLabel *KWYAxisLabelMax[PLOTS];
  vtkKWFrameWithLabel *KWYAxisFrameWithLabel[PLOTS];

  //Valores na ordem Press[Dyn/cm2], Press[mmHg], Area[cm2], Flow[cm3], Vel[cm/s], Vel[m/s]
  double YAxisMin[PLOTS];
  double YAxisMax[PLOTS];

  // Description:
  // Frame for edit the Y axis of plots
  vtkKWFrameWithLabel *KWFrameEditYAxis;

  // Description:
  // Frame for show left interface of the plot window
  vtkKWFrame *KWTitleToGraphicFrame;

  // Description:
  // Labem to show the titles of the graphics
  vtkKWLabel *TitleToGraphic[MAX_CURVES];

  vtkIntArray *PlotOptions;

  vtkKWChangeColorButton *ColorButton;

private:
  vtkKWHM1DXYPlotWidget(const vtkKWHM1DXYPlotWidget&); // Not implemented
  void operator=(const vtkKWHM1DXYPlotWidget&); // Not implemented
};

#endif /*VTKKWHM1DXYPLOTWIDGET_H_*/
