/*=========================================================================

  Module:    $RCSfile: vtkKWHMXYPlot.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWApplication.h"
#include "vtkKWHMXYPlot.h"
#include "vtkObjectFactory.h"

#include "vtkKWPushButton.h"

#include "vtkKWHM1DXYPlotWidget.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkKWHMXYPlot);
vtkCxxRevisionMacro(vtkKWHMXYPlot, "$Revision: 1.28 $");

vtkKWHMXYPlot::vtkKWHMXYPlot()
{
}

vtkKWHMXYPlot::~vtkKWHMXYPlot()
{
}

//----------------------------------------------------------------------------
void vtkKWHMXYPlot::Create(vtkKWApplication *app)
{
  // Call the superclass to set the appropriate flags then create manually
	this->Superclass::Create(app);
	
}

//----------------------------------------------------------------------------
void vtkKWHMXYPlot::UpdateRange()
{
	this->HMXYPlotWidget->UpdateRange();
}

//----------------------------------------------------------------------------
void vtkKWHMXYPlot::RemoveCurve()
{
	this->HMXYPlotWidget->RemoveCurve();
}

//----------------------------------------------------------------------------
void vtkKWHMXYPlot::SetHMXYPlotWidget(vtkKWHM1DXYPlotWidget *Widget)
{
	this->HMXYPlotWidget = Widget;
}

//----------------------------------------------------------------------------
vtkKWHM1DXYPlotWidget *vtkKWHMXYPlot::GetHMXYPlotWidget()
{
	return this->HMXYPlotWidget;
}

//----------------------------------------------------------------------------
vtkKWPushButton *vtkKWHMXYPlot::GetKWUpdateRangeButton()
{
	return this->KWUpdateRangeButton;
}

void vtkKWHMXYPlot::SetKWUpdateRangeButton(vtkKWPushButton *b)
{
	this->KWUpdateRangeButton = b;
}

//----------------------------------------------------------------------------
vtkKWPushButton *vtkKWHMXYPlot::GetKWRemoveButton()
{
	return this->KWRemoveButton;
}

void vtkKWHMXYPlot::SetKWRemoveButton(vtkKWPushButton *b)
{
	this->KWRemoveButton = b;
}

//----------------------------------------------------------------------------
void vtkKWHMXYPlot::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

