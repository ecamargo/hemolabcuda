/*=========================================================================

  Module:    $RCSfile: vtkKWHMXYPlot.h,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWHMXYPlot - a simple frame
// .SECTION Description
// The core frame


#ifndef VTKKWHMXYPLOT_H_
#define VTKKWHMXYPLOT_H_

#include "vtkKWCoreWidget.h"

#include "vtkKWPushButton.h"

class vtkKWHM1DXYPlotWidget;

class KWWIDGETS_EXPORT vtkKWHMXYPlot : public vtkKWCoreWidget
{
public:
  static vtkKWHMXYPlot* New();
  vtkTypeRevisionMacro(vtkKWHMXYPlot,vtkKWCoreWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the widget.
  virtual void Create(vtkKWApplication *app);
	
	// Description:
	// Set/Get KWUpdateRangeButton
	vtkKWPushButton *GetKWUpdateRangeButton();
	void SetKWUpdateRangeButton(vtkKWPushButton *b);
	
	// Description:
	// Set/Get KWRemoveButton
	vtkKWPushButton *GetKWRemoveButton();
	void SetKWRemoveButton(vtkKWPushButton *b);
	
	// Description:
	// Set/Get HMXYPlotWidget
	void SetHMXYPlotWidget(vtkKWHM1DXYPlotWidget *Widget);
	vtkKWHM1DXYPlotWidget *GetHMXYPlotWidget();
	
  // Description:
  // Method for call update range of the class
  // vtkKWHM1DXYPlotWidget
  void UpdateRange();
  
  void RemoveCurve();
  
protected:
  vtkKWHMXYPlot();
  ~vtkKWHMXYPlot();

	vtkKWPushButton *KWUpdateRangeButton;
	
	// Description:
  // Button for remove curve
  vtkKWPushButton *KWRemoveButton;
	
	vtkKWHM1DXYPlotWidget *HMXYPlotWidget;
	vtkKWHM1DXYAnimatedPlotWidget *HMXYAnimatedPlotWidget;
	
	
private:
  vtkKWHMXYPlot(const vtkKWHMXYPlot&); // Not implemented
  void operator=(const vtkKWHMXYPlot&); // Not implemented
};

#endif /*VTKKWHMXYPLOT_H_*/
