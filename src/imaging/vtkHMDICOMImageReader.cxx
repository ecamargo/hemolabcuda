#include "vtkHMDICOMImageReader.h"
#include "vtkDICOMImageReader.h"
#include "vtkImageData.h"
#include "vtkPNGWriter.h"
#include "vtkImageCast.h"
#include "vtkPlane.h"

#include "vtkRenderWindowInteractor.h"

#include "vtkImagePlaneWidget.h"
#include "vtkCutter.h"

#include "vtkInformation.h"
#include "vtkStreamingDemandDrivenPipeline.h"

#include "vtkInformationVector.h"
#include "vtkSMIntVectorProperty.h"

#include "vtkHMImageWorkspace.h"

vtkCxxRevisionMacro(vtkHMDICOMImageReader, "$Rev$");
vtkStandardNewMacro(vtkHMDICOMImageReader);

//----------------------------------------------------------------------------
// Class constructor
vtkHMDICOMImageReader::vtkHMDICOMImageReader()
{
  cout << "    Instanciou o vtkHMDICOMImageReader: " << this << endl;
//  cout << "  Reference: " << this << endl << endl;
  this->SetNumberOfInputPorts(0);

  this->reader = vtkDICOMImageReader::New();
  this->input = vtkImageData::New();
  this->output = vtkImageData::New();
}

//----------------------------------------------------------------------------
// Class destructor
vtkHMDICOMImageReader::~vtkHMDICOMImageReader()
{
  cout << "|----------------------------------------------------------|" << endl;
  cout << "    Destruiu o DICOMImageReader - DATASERVER" << endl;

  this->reader->Delete();
  this->input->Delete();
  this->output->Delete();
  cout << "    Bye bye DICOM DATASERVER!" << endl;
  cout << "|----------------------------------------------------------|" << endl << endl;
}

void vtkHMDICOMImageReader::SetDirectoryName(char *DirectoryName)
{
  this->DirectoryName = DirectoryName;

  this->reader->SetDirectoryName(this->DirectoryName);
  this->reader->GetOutput()->SetExtent(this->reader->GetOutput()->GetExtent());
  this->reader->Update();
/*
  this->reader->GetOutput()->GetDimensions(this->GetDimensions());
  this->reader->GetOutput()->GetSpacing(this->GetSpacing());

//  cout << "Dimensions" << this->Dimensions[0] << " x " << this->Dimensions[1] << " x " << this->Dimensions[2] << endl << endl;

  double XYOrigin = (this->Dimensions[0] * Spacing[0] / 2) *-1;
  double ZOrigin = ((this->Dimensions[2] * Spacing[0]) * (Spacing[2] / Spacing[0])) /2 * -1;

  this->reader->SetDataOrigin(XYOrigin, XYOrigin, ZOrigin);

  this->reader->GetOutput()->Update();
*/
  this->SetOutputImageData();
}

void vtkHMDICOMImageReader::SetOutputImageData()
{
//  cout << "  vtkHMDICOMImageReader->SetInput" << endl;
  this->SetNumberOfInputPorts(1);
	
  this->SetInputConnection(0,reader->GetOutputPort(0));
  
  this->input = this->GetImageDataInput(0);
  this->input->Update();

  int Ext[6];
  this->input->GetExtent(Ext);
//  cout << "Extents: " << Ext[0] << " x " << Ext[1] << " x " << Ext[2] << " x " << Ext[3] << " x " << Ext[4] << " x " << Ext[5] << endl;
  
  
//Copiando Input para Output --> No caso de um filtro, aqui deveria ser feita a chamada do método
  this->CopyInputToOutput();
  this->SetOutput(this->output);
  
  this->GetOutput()->Update();
}

int vtkHMDICOMImageReader::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");
  return 1;
}


//----------------------------------------------------------------------------
int vtkHMDICOMImageReader::RequestData(
  vtkInformation* vtkNotUsed( request ),
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{

  // get the data object
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkImageData *output = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkImageData *input = vtkImageData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  int inExt[6];
  input->GetExtent(inExt);
  // if the input extent is empty then exit
  if (inExt[1] < inExt[0] ||
      inExt[3] < inExt[2] ||
      inExt[5] < inExt[4])
    {
    return 1;
    }

  this->reader->GetDataSpacing(this->Spacing);
  input->GetDimensions(this->Dimensions);

  double Origin = (this->Dimensions[0] * this->Spacing[0]) * -1;
  
//  cout << "HMDICOMImageReader - Origin: " << Origin << endl;
  
  output->SetOrigin(Origin, Origin, Origin);
//  output->SetOrigin(Origin, Origin, Origin);
  
  // Set the extent of the output and allocate memory.
  output->SetExtent(outInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT()));
  output->AllocateScalars();
  
  input = NULL;
  output = NULL;
  return 1;
}

void vtkHMDICOMImageReader::WritePNGS(char *outputPath, vtkImageData *img)
{
  vtkPNGWriter *writer = vtkPNGWriter::New();
  vtkImageCast *cast = vtkImageCast::New();

  cast->SetInput(img);
  cast->SetOutputScalarTypeToUnsignedChar();
  cast->Update();

  writer->SetInput(cast->GetOutput());
  writer->SetFilePrefix(outputPath);
  writer->SetFilePattern("%s%.3d.png");
	
  writer->Write();

  writer->Delete();
  cast->Delete();
}

void vtkHMDICOMImageReader::WritePNGSStream(char *outputPath)
{
  this->WritePNGS(outputPath, this->output);
}

void vtkHMDICOMImageReader::PrintInputsAndOutputs(bool connections)
{
//TEST -----------------------------------------------------------------------------------|
  cout << endl;
  cout << "numberOfOutputs: " << this->GetNumberOfOutputPorts() << endl;
  cout << "numberOfInputs: " << this->GetNumberOfInputPorts() << endl;
  cout << "Number or Input Connections: ";

  if (connections)
    cout << this->GetNumberOfInputConnections (0);
  	else
  	  cout << "0 (NOT CHECKING)";

	cout << endl<< endl;
}

bool vtkHMDICOMImageReader::CopyInputToOutput()
{
  if(this->VerifyImageData(this->input))
  {
    this->output->DeepCopy(this->input);

    return 1;
  }
  return 0;
}

bool vtkHMDICOMImageReader::VerifyImageData(vtkImageData *img)
{
  int inExt[6];
  img->GetExtent(inExt);
  // if the input extent is empty then exit

  if (inExt[1] < inExt[0] || inExt[3] < inExt[2] || inExt[5] < inExt[4])
  {
//    cout << "ImageData inválido (problemas com Extent)!" << endl;
//    cout << inExt[0] << ", " << inExt[1] << ", " << inExt[2] << ", " << inExt[3] << ", " << inExt[4] << ", " << inExt[5] << endl;
    return 0;
  }
  return 1;
}

int *vtkHMDICOMImageReader::GetDimensionsFromOutput()
{
//  cout << "  DATASERVER - HMDICOM: GetDimensions" << endl;
  return this->output->GetDimensions();
//  return this->Dimensions;
}

int vtkHMDICOMImageReader::GetDimension(int Index)
{
  int *TempDimensions;
  TempDimensions = this->GetDimensionsFromOutput();

  return TempDimensions[Index];
}

int vtkHMDICOMImageReader::GetXDimension()
{
  return this->GetDimension(0);
}

int vtkHMDICOMImageReader::GetYDimension()
{
  return this->GetDimension(1);
}

int vtkHMDICOMImageReader::GetZDimension()
{
  return this->GetDimension(2);
}

//------------------------------
double *vtkHMDICOMImageReader::GetSpacingsFromOutput()
{
  return this->output->GetSpacing();
}

double vtkHMDICOMImageReader::GetSpacing(int Index)
{
  double *TempSpacings;
  TempSpacings = this->GetSpacingsFromOutput();
  return TempSpacings[Index];
}

double vtkHMDICOMImageReader::GetXSpacing()
{
  return this->GetSpacing(0);
}

double vtkHMDICOMImageReader::GetYSpacing()
{
  return this->GetSpacing(1);
}

double vtkHMDICOMImageReader::GetZSpacing()
{
  return this->GetSpacing(2);
}

int *vtkHMDICOMImageReader::GetDimensions()
{
  return this->Dimensions;
}

double *vtkHMDICOMImageReader::GetSpacing()
{
  return this->Spacing;
}
