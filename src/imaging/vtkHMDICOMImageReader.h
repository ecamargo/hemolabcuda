#ifndef __vtkHMDICOMImageReader_h
#define __vtkHMDICOMImageReader_h

#include <vtkObjectFactory.h>

#include <vtkImageAlgorithm.h>

class vtkDICOMImageReader;

class vtkImageData;

//class vtkHMImageWorkspace;

class VTK_EXPORT vtkHMDICOMImageReader : public vtkImageAlgorithm
{
//Public Methods
public:

  static vtkHMDICOMImageReader *New();

  vtkTypeRevisionMacro(vtkHMDICOMImageReader, vtkImageAlgorithm);
  
//  void PrintSelf(ostream& os, vtkIndent indent);

  void SetDirectoryName(char *DirectoryName);

  void SetOutputImageData();
  
  int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);
  
  void WritePNGS(char *outputPath, vtkImageData *img);
  
  bool VerifyImageData(vtkImageData *img);
  
  bool CopyInputToOutput();

  int RequestData(vtkInformation *vtkNotUsed(request),
									 vtkInformationVector **vtkNotUsed(inputVector),
									 vtkInformationVector *outputVector);

  void WritePNGSStream(char *outputPath);
  
  int *GetDimensionsFromOutput();
  int GetDimension(int Index);
  int GetXDimension();
  int GetYDimension();
  int GetZDimension();

  double *GetSpacingsFromOutput();
  double GetSpacing(int Index);
  double GetXSpacing();
  double GetYSpacing();
  double GetZSpacing();

  int *GetDimensions();
  double *GetSpacing();

//Protected Methods      
protected:
  vtkHMDICOMImageReader();
  ~vtkHMDICOMImageReader();

//DEBUG METHODS -------------------------------------------------------------------------|
  void PrintInputsAndOutputs(bool connections);
  
  int Dimensions[3];
  double Spacing[3];
 
//Private Attributes	
private:
    vtkDICOMImageReader *reader;
    
	vtkImageData *input;
	vtkImageData *output;
	
    char *DirectoryName;
};

#endif
