#include "vtkHMImageAnisotropicDiffusion3D.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageAnisotropicDiffusion3D, "$Rev$");
vtkStandardNewMacro(vtkHMImageAnisotropicDiffusion3D);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageAnisotropicDiffusion3D* HMImageAnisotropicDiffusion3D = reinterpret_cast<vtkHMImageAnisotropicDiffusion3D*>(sr);

  if (HMImageAnisotropicDiffusion3D->GetProgress() == 1.0)
  {
    if(HMImageAnisotropicDiffusion3D->GetAlreadyAccepted())
    {
      HMImageAnisotropicDiffusion3D->SetDataArrayFromOutput();
    }

    HMImageAnisotropicDiffusion3D->SetAlreadyAccepted(1);
  }
}

vtkHMImageAnisotropicDiffusion3D::vtkHMImageAnisotropicDiffusion3D()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageAnisotropicDiffusion3D::~vtkHMImageAnisotropicDiffusion3D()
{
}

void vtkHMImageAnisotropicDiffusion3D::SetDataArrayFromOutput()
{
    
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }

    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMImageAnisotropicDiffusion3D");
  }
}

void vtkHMImageAnisotropicDiffusion3D::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageAnisotropicDiffusion3D::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
