#ifndef __vtkHMImageAnisotropicDiffusion3D_h
#define __vtkHMImageAnisotropicDiffusion3D_h

#include <vtkObjectFactory.h>
#include "vtkImageAnisotropicDiffusion3D.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageAnisotropicDiffusion3D : public vtkImageAnisotropicDiffusion3D
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageAnisotropicDiffusion3D *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageAnisotropicDiffusion3D, vtkImageAnisotropicDiffusion3D);

  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();
  
  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageAnisotropicDiffusion3D();

  // Description:
  // Destructor
  ~vtkHMImageAnisotropicDiffusion3D();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
