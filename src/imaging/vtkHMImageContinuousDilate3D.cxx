#include "vtkHMImageContinuousDilate3D.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageContinuousDilate3D, "$Rev$");
vtkStandardNewMacro(vtkHMImageContinuousDilate3D);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageContinuousDilate3D* HMImageContinuousDilate3D = reinterpret_cast<vtkHMImageContinuousDilate3D*>(sr);

  if (HMImageContinuousDilate3D->GetProgress() == 1.0)
  {
    if(HMImageContinuousDilate3D->GetAlreadyAccepted())
    {
      HMImageContinuousDilate3D->SetDataArrayFromOutput();
    }

    HMImageContinuousDilate3D->SetAlreadyAccepted(1);
  }
}

vtkHMImageContinuousDilate3D::vtkHMImageContinuousDilate3D()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageContinuousDilate3D::~vtkHMImageContinuousDilate3D()
{
}

void vtkHMImageContinuousDilate3D::SetDataArrayFromOutput()
{
    
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }

    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMImageContinuousDilate3D");
  }
}

void vtkHMImageContinuousDilate3D::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageContinuousDilate3D::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
