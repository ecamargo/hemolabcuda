#ifndef __vtkHMImageContinuousDilate3D_h
#define __vtkHMImageContinuousDilate3D_h

#include <vtkObjectFactory.h>
#include "vtkImageContinuousDilate3D.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageContinuousDilate3D : public vtkImageContinuousDilate3D
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageContinuousDilate3D *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageContinuousDilate3D, vtkImageContinuousDilate3D);

  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();
  
  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageContinuousDilate3D();

  // Description:
  // Destructor
  ~vtkHMImageContinuousDilate3D();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
