#include "vtkHMImageContinuousErode3D.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageContinuousErode3D, "$Rev$");
vtkStandardNewMacro(vtkHMImageContinuousErode3D);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageContinuousErode3D* HMImageContinuousErode3D = reinterpret_cast<vtkHMImageContinuousErode3D*>(sr);
  
  if (HMImageContinuousErode3D->GetProgress() == 1.0)
  {
    if(HMImageContinuousErode3D->GetAlreadyAccepted())
    {
      HMImageContinuousErode3D->SetDataArrayFromOutput();
    }

    HMImageContinuousErode3D->SetAlreadyAccepted(1);
  }
}

vtkHMImageContinuousErode3D::vtkHMImageContinuousErode3D()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageContinuousErode3D::~vtkHMImageContinuousErode3D()
{
}

void vtkHMImageContinuousErode3D::SetDataArrayFromOutput()
{
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }
    
    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMImageContinuousErode3D");
  }
}

void vtkHMImageContinuousErode3D::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageContinuousErode3D::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
