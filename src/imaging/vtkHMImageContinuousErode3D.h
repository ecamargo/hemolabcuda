#ifndef __vtkHMImageContinuousErode3D_h
#define __vtkHMImageContinuousErode3D_h

#include <vtkObjectFactory.h>
#include "vtkImageContinuousErode3D.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageContinuousErode3D : public vtkImageContinuousErode3D
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageContinuousErode3D *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageContinuousErode3D, vtkImageContinuousErode3D);
  
  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();
  
  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageContinuousErode3D();

  // Description:
  // Destructor
  ~vtkHMImageContinuousErode3D();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
