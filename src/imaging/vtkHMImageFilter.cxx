#include "vtkHMImageFilter.h"
#include "vtkImageData.h"
#include "vtkInformation.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkInformationVector.h"

vtkCxxRevisionMacro(vtkHMImageFilter, "$Rev$");
vtkStandardNewMacro(vtkHMImageFilter);

vtkHMImageFilter::vtkHMImageFilter()
{
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
}

vtkHMImageFilter::~vtkHMImageFilter()
{
}

int vtkHMImageFilter::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");
  return 1;
}

int vtkHMImageFilter::RequestData(vtkInformation* vtkNotUsed( request ), vtkInformationVector** vtkNotUsed( inputVector) , vtkInformationVector* outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkImageData *output = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  output->DeepCopy(this->GetImageDataInput(0));
  
  output->GetExtent(this->outExt);

  output->GetDimensions(this->Dimensions);
  output->GetSpacing(this->Spacing);

  return 1;
}

bool vtkHMImageFilter::VerifyImageData(vtkImageData *img)
{
  int inExt[6];
  img->GetExtent(inExt);

  if (inExt[1] < inExt[0] || inExt[3] < inExt[2] || inExt[5] < inExt[4])
  {
    return 0;
  }
  return 1;
}

void vtkHMImageFilter::SetExtentsFromInput()
{
  this->GetImageDataInput(0)->GetExtent(this->outExt);
}
