#ifndef __vtkHMImageFilter_h
#define __vtkHMImageFilter_h

#include <vtkObjectFactory.h>
#include <vtkImageAlgorithm.h>

class vtkImageData;

class VTK_EXPORT vtkHMImageFilter : public vtkImageAlgorithm
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageFilter *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageFilter, vtkImageAlgorithm);

  // Description:
  // Sets the type of output for this filter. 
  int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Description:
  // Verifies any Image Data integrity by it's Extents. 
  bool VerifyImageData(vtkImageData *img);

  // Description:
  // Allocates the Image Data needed memory size and gets it disponible as an output.
  int RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **vtkNotUsed(inputVector), vtkInformationVector *outputVector);

  // Description:
  // Sets filter's output extent from it's input.
  void SetExtentsFromInput();

protected:

  // Description:
  // Constructor
  vtkHMImageFilter();

  // Description:
  // Destructor
  ~vtkHMImageFilter();

//  void ProcessFilter();

  // Description:
  // Filter Image Data output.
  vtkImageData *output;

  // Description:
  // Filter Image Data output extents.
  int outExt[6];

  // Description:
  // Double 3 dimensions array that stores the Image Data dimensions information.
  double Spacing[3];
  
  // Description:
  // Integer 3 dimensions array that stores the Image Data dimensions information.
  int Dimensions[3];
};

#endif
