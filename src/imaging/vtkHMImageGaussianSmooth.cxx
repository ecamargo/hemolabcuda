#include "vtkHMImageGaussianSmooth.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageGaussianSmooth, "$Rev$");
vtkStandardNewMacro(vtkHMImageGaussianSmooth);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageGaussianSmooth* HMImageGaussianSmooth = reinterpret_cast<vtkHMImageGaussianSmooth*>(sr);

  if (HMImageGaussianSmooth->GetProgress() == 1.0)
  {
    if(HMImageGaussianSmooth->GetAlreadyAccepted())
    {
      HMImageGaussianSmooth->SetDataArrayFromOutput();
    }

    HMImageGaussianSmooth->SetAlreadyAccepted(1);
  }
}

vtkHMImageGaussianSmooth::vtkHMImageGaussianSmooth()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageGaussianSmooth::~vtkHMImageGaussianSmooth()
{
}

void vtkHMImageGaussianSmooth::SetDataArrayFromOutput()
{
    
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }

    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMImageGaussianSmooth");
  }
}

void vtkHMImageGaussianSmooth::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageGaussianSmooth::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
