#ifndef __vtkHMImageGaussianSmooth_h
#define __vtkHMImageGaussianSmooth_h

#include <vtkObjectFactory.h>
#include "vtkImageGaussianSmooth.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageGaussianSmooth : public vtkImageGaussianSmooth
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageGaussianSmooth *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageGaussianSmooth, vtkImageGaussianSmooth);

  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();
  
  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageGaussianSmooth();

  // Description:
  // Destructor
  ~vtkHMImageGaussianSmooth();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
