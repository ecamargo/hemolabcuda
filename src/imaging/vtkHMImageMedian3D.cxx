#include "vtkHMImageMedian3D.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageMedian3D, "$Rev$");
vtkStandardNewMacro(vtkHMImageMedian3D);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageMedian3D* HMImageMedian3D = reinterpret_cast<vtkHMImageMedian3D*>(sr);

  if (HMImageMedian3D->GetProgress() == 1.0)
  {
    if(HMImageMedian3D->GetAlreadyAccepted())
    {
      HMImageMedian3D->SetDataArrayFromOutput();
    }

    HMImageMedian3D->SetAlreadyAccepted(1);
  }
}

vtkHMImageMedian3D::vtkHMImageMedian3D()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageMedian3D::~vtkHMImageMedian3D()
{
}

void vtkHMImageMedian3D::SetDataArrayFromOutput()
{
    
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }

    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMImageMedian3D");
  }
}

void vtkHMImageMedian3D::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageMedian3D::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
