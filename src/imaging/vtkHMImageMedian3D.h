#ifndef __vtkHMImageMedian3D_h
#define __vtkHMImageMedian3D_h

#include <vtkObjectFactory.h>
#include "vtkImageMedian3D.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageMedian3D : public vtkImageMedian3D
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageMedian3D *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageMedian3D, vtkImageMedian3D);

  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();
  
  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageMedian3D();

  // Description:
  // Destructor
  ~vtkHMImageMedian3D();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
