#include "vtkHMImageOpenClose3D.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkImageDilateErode3D.h"
#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHMImageOpenClose3D, "$Rev$");
vtkStandardNewMacro(vtkHMImageOpenClose3D);

static void EndEventCallback(vtkObject *vtkNotUsed( caller ),
                           unsigned long vtkNotUsed(eventId), 
                           void *sr, void *)
{
  vtkHMImageOpenClose3D* HMImageOpenClose3D = reinterpret_cast<vtkHMImageOpenClose3D*>(sr);
  
  if ((HMImageOpenClose3D->GetProgress() == 1.0) && (HMImageOpenClose3D->GetFilter0()->GetProgress() == 1.0) && (HMImageOpenClose3D->GetFilter1()->GetProgress() == 1.0))
  {
    if(HMImageOpenClose3D->GetAlreadyAccepted())
    {
      HMImageOpenClose3D->SetDataArrayFromOutput();
    }

    HMImageOpenClose3D->SetAlreadyAccepted(1);
  }
}

vtkHMImageOpenClose3D::vtkHMImageOpenClose3D()
{
  this->AlreadyAccepted = 0;
  
  vtkCallbackCommand *cbc = vtkCallbackCommand::New();
  cbc->SetCallback(EndEventCallback);
  cbc->SetClientData((void *)this);
  this->GetFilter0()->AddObserver(vtkCommand::EndEvent, cbc);
  this->GetFilter1()->AddObserver(vtkCommand::EndEvent, cbc);
  cbc->Delete();
}

vtkHMImageOpenClose3D::~vtkHMImageOpenClose3D()
{
}

void vtkHMImageOpenClose3D::SetDataArrayFromOutput()
{
  if(this->GetOutput())
  {
    //Erasing Input and Old DataArrays
    int NumberOfArrays = this->GetOutput()->GetPointData()->GetNumberOfArrays();
    for (int i=1; i < NumberOfArrays; i++)
    {
      this->GetOutput()->GetPointData()->RemoveArray(this->GetOutput()->GetPointData()->GetArrayName(i));
    }
    
    //Renaming the DataArray, will force paraview to update its values.
    this->GetOutput()->GetPointData()->GetArray(0)->SetName("HMOpenClose3D");
  }
}

void vtkHMImageOpenClose3D::SetAlreadyAccepted(int Flag)
{
  this->AlreadyAccepted = Flag;
}

int vtkHMImageOpenClose3D::GetAlreadyAccepted()
{
  return this->AlreadyAccepted;
}
