#ifndef __vtkHMImageOpenClose3D_h
#define __vtkHMImageOpenClose3D_h

#include <vtkObjectFactory.h>
#include "vtkImageOpenClose3D.h"

#include "vtkInformation.h"

class vtkImageData;

class VTK_EXPORT vtkHMImageOpenClose3D : public vtkImageOpenClose3D
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMImageOpenClose3D *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMImageOpenClose3D, vtkImageOpenClose3D);

  // Description:
  // Sets the DataArray from output scalars.
  void SetDataArrayFromOutput();

  // Description:
  // Sets AlreadyAccepted attribute value.  
  void SetAlreadyAccepted(int Flag);

  // Description:
  // Returns the AlreadyAccepted attribute value.
  int GetAlreadyAccepted();
  
protected:

  // Description:
  // Constructor
  vtkHMImageOpenClose3D();

  // Description:
  // Destructor
  ~vtkHMImageOpenClose3D();

  // Description:
  // Flag that controls if the filters has been already accepted.
  int AlreadyAccepted;

};

#endif
