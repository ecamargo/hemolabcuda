#include "vtkHMTopologicalDerivative.h"
#include "vtkHMTopologicalDerivativeMethod.h"

#include "vtkImageData.h"
#include "vtkInformation.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkInformationVector.h"
#include "vtkPointData.h"
#include "vtkFloatArray.h"

vtkCxxRevisionMacro(vtkHMTopologicalDerivative, "$Rev$");
vtkStandardNewMacro(vtkHMTopologicalDerivative);

vtkHMTopologicalDerivative::vtkHMTopologicalDerivative()
{
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
  
  this->ModifiedFlag = 0;
  
  this->tder = vtkHMTopologicalDerivativeMethod::New();
  this->tder->SetProgressBarUpdater(this);

  this->InfoArray = vtkFloatArray::New();
  this->InfoArray->SetName("TopologicalDerivative");
}

vtkHMTopologicalDerivative::~vtkHMTopologicalDerivative()
{
  this->Output = NULL;

  this->tder->Delete();
  this->InfoArray->Delete();
}

int vtkHMTopologicalDerivative::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");
  return 1;
}

int vtkHMTopologicalDerivative::RequestData(vtkInformation* vtkNotUsed( request ), vtkInformationVector** vtkNotUsed( inputVector) , vtkInformationVector* outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  this->Output = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  if (!this->ModifiedFlag)
  {
    this->Output->DeepCopy(this->GetImageDataInput(0));
    this->SetInformationFromInput();
    
    this->ModifiedFlag = 1;
    this->Modified();
    this->tder->Modified();
  }
  else
  {
  	this->ExecuteTopologicalDerivative();
    this->Output->DeepCopy(tder->GetOutput());

    this->Output->GetPointData()->AddArray(this->InfoArray);
    
    this->ModifiedFlag = 1;
    this->Modified();
    this->tder->Modified();
  }
  
  return 1;
}

void vtkHMTopologicalDerivative::ExecuteTopologicalDerivative()
{
  this->SetProgress(0.1);
  this->UpdateProgress(0.1);
  
  this->classes.empty();
  this->InfoArray->Reset();

  this->activeDirections[0]=activeDirections[1]=activeDirections[2]=1;

  tder->SetInfoArray(this->InfoArray);
  tder->SetInput(this->GetImageDataInput(0));
  
  this->GetImageDataInput(0)->GetExtent(this->OutExt);

  vtkImageData::SafeDownCast(tder->GetInput())->SetExtent(this->OutExt);

  tder->setClasses(classes);
  tder->setRho(rho);
  int activeDirections[3];
	//segmentate in the xy direction
  if (direction == xy)
  {
	activeDirections[0] = activeDirections[1] = 1;
	activeDirections[2] = 0;
	//segmentate in the xz direction
  }
  else if (direction == xz)
  {
	activeDirections[0] = activeDirections[2] = 1;
	activeDirections[1] = 0;
	//segmentate in the yz direction
  }
  else if (direction == yz)
  {
	activeDirections[1] = activeDirections[2] = 1;
	activeDirections[0] = 0;
	//segmentate in 3 directions
  }
  else if (direction == td)
  {
	activeDirections[0] = activeDirections[1] = activeDirections[2] = 1;
  }
  tder->setActiveDirections(activeDirections);

  tder->Update();
  
  this->SetProgress(1.0);
  this->UpdateProgress(1.0);  

  double Range[2];

  this->InfoArray->GetRange(Range);

  this->ModifiedFlag = 1;
  this->Modified();
}

void vtkHMTopologicalDerivative::SetInformationFromInput()
{
  this->GetImageDataInput(0)->GetExtent(this->OutExt);
  this->GetImageDataInput(0)->GetDimensions(this->Dimensions);
  this->GetImageDataInput(0)->GetSpacing(this->Spacing);
}

void vtkHMTopologicalDerivative::IncreaseProgress(double Value, char* ProgressText)
{
  this->SetProgressText(ProgressText);
  this->IncreaseProgress(Value);
}

void vtkHMTopologicalDerivative::IncreaseProgress(double Value)
{
  if ((this->GetProgress() + Value) > 0.95)
  {
  	Value = 0.95;
  }
  else
  {
    Value = this->GetProgress() + Value;
  }
  this->SetProgress(Value);
  this->UpdateProgress(this->GetProgress());
}

bool vtkHMTopologicalDerivative::VerifyImageData(vtkImageData *img)
{
  int inExt[6];
  img->GetExtent(inExt);

  if (inExt[1] < inExt[0] || inExt[3] < inExt[2] || inExt[5] < inExt[4])
  {
    return 0;
  }
  return 1;
}

void vtkHMTopologicalDerivative::SetExtentsFromInput()
{
  this->GetImageDataInput(0)->GetExtent(this->OutExt);
}

void vtkHMTopologicalDerivative::AddClass(double PassedClass)
{
  this->classes.push_back(PassedClass);
  
  this->Modified();
}

void vtkHMTopologicalDerivative::RemoveClass(int PassedID)
{
  ClassesVector::iterator ClassesIteractor = this->classes.begin();

  for(int i = 0; i < PassedID; i++)
  {
    ClassesIteractor++;
  }
  this->classes.erase(ClassesIteractor);
  
  this->Modified();
}

void vtkHMTopologicalDerivative::SetRho(double PassedRho)
{
  this->rho = PassedRho;
  
  this->Modified();
}

void vtkHMTopologicalDerivative::SetActiveDirections(int *PassedActiveDirections)
{
  this->activeDirections[0] = PassedActiveDirections[0];
  this->activeDirections[1] = PassedActiveDirections[1];
  this->activeDirections[2] = PassedActiveDirections[2];
}
