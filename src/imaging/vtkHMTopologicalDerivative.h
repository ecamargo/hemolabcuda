#ifndef __vtkHMTopologicalDerivative_h
#define __vtkHMTopologicalDerivative_h

#include <vtkObjectFactory.h>
#include <vtkImageAlgorithm.h>
#include <vector>

class vtkFloatArray;
class vtkImageData;
class vtkHMTopologicalDerivativeMethod;

class VTK_EXPORT vtkHMTopologicalDerivative : public vtkImageAlgorithm
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMTopologicalDerivative *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMTopologicalDerivative, vtkImageAlgorithm);

  //BTX
  // Description:  
  //Type of vector wich stores the algorithm classes.
  typedef std::vector<double> ClassesVector;
  //ETX

  // Description:
  //Vector that stores all the classes that are going to be used on the filter.
  ClassesVector classes;

  // Description:
  // Sets the type of output for this filter. 
  int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Description:
  // Sets filter's output extent, spacing and dimensions from it's input.
  void SetInformationFromInput();

  // Description:
  // Verifies any Image Data integrity by it's Extents. 
  bool VerifyImageData(vtkImageData *img);

  // Description:
  // Allocates the Image Data needed memory size and gets it disponible as an output.
  int RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **vtkNotUsed(inputVector), vtkInformationVector *outputVector);

  // Description:
  // Sets filter's output extent from it's input.
  void SetExtentsFromInput();
  
  //Description:
  //Add a class to the Classes vector.
  void AddClass(double PassedClass);

  //Description:
  //Remove a class from the Classes vector.
  void RemoveClass(int PassedID);

  //Description:
  //Set rho attribute.  
  void SetRho(double PassedRho);

  //Description:
  //Set activeDirections array attribute.
  void SetActiveDirections(int *PassedActiveDirections);

  //Description:
  //Executes the Topological Derivative Filter.
  void ExecuteTopologicalDerivative();

protected:

  // Description:
  // Constructor
  vtkHMTopologicalDerivative();

  // Description:
  // Destructor
  ~vtkHMTopologicalDerivative();
 
  // Description:
  // Increases progress with passed double.
  void IncreaseProgress(double Value);

  // Description:
  // Increases progress with passed double and displays a message.
  void IncreaseProgress(double Value, char* ProgressText);

  // Description:
  // Data array used to fill the Information tab.
  vtkFloatArray *InfoArray;
  
  //BTX
  enum SegmentationDirections
	   {xy=0, xz=1, yz=2, td=3};
  //ETX
  
  // Description:
  // Stores the directions that the segmentation should run.  // Description:  
  //Linear combination of the distance/line integral.
  SegmentationDirections direction;

  // Description:
  // Topological Derivative Filter.
  vtkHMTopologicalDerivativeMethod *tder;

  // Description:
  // vtkImageData. object that stores the Topological Derivative Filter Output as a local copy.
  vtkImageData *TderOutput;

  // Description:  
  //Linear combination of the distance/line integral.
  double rho;

  // Description:
  //Contains 1's in the position of the active segmentation directions.
  int activeDirections[3];
  
  // Description:
  // Output of this class.
  vtkImageData *Output;

  // Description:
  // Filter Image Data output extents.
  int OutExt[6];

  // Description:
  // Double 3 dimensions array that stores the Image Data dimensions information.
  double Spacing[3];
  
  // Description:
  // Integer 3 dimensions array that stores the Image Data dimensions information.
  int Dimensions[3];

  // Description:
  // Flag that knows if the filter has already been executed (needed because of the "show input" funcionality).  
  int ModifiedFlag;
};

#endif
