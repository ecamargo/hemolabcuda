#include "vtkHMTopologicalDerivativeMethod.h"

#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkStructuredPoints.h>

#include "vtkImageCast.h"
#include "vtkPNGWriter.h"

vtkCxxRevisionMacro(vtkHMTopologicalDerivativeMethod, "$Rev$");
vtkStandardNewMacro(vtkHMTopologicalDerivativeMethod);

vtkHMTopologicalDerivativeMethod::vtkHMTopologicalDerivativeMethod():vtkSimpleImageToImageFilter()
{
  distImage = vtkFloatArray::New();
  edgeImage = vtkFloatArray::New();
	
  for (int i=0; i<6; i++)
  {
    this->Extent[i] = 0;
  }
	
  rho=1;
  this->inputImage=NULL;
}

vtkHMTopologicalDerivativeMethod::~vtkHMTopologicalDerivativeMethod()
{
  edgeImage->Delete();
  distImage->Delete();
}

void vtkHMTopologicalDerivativeMethod::AddClass(double cl)
{
  this->classes.push_back(cl);
}

void vtkHMTopologicalDerivativeMethod::setClasses(ClassesVector cls)
{
  int comp = cls.size();
  this->classes.empty();
  classes = cls;
}

vtkHMTopologicalDerivativeMethod::ClassesVector vtkHMTopologicalDerivativeMethod::getClasses()
{
  return classes;
}

void vtkHMTopologicalDerivativeMethod::SimpleExecute(vtkImageData* input,vtkImageData* output)
{
  if (!this->InfoArray)
  {
	vtkWarningMacro("InfoArray not setted...");
  }

  this->ProgressBarUpdater->SetProgress(0.0);
  this->ProgressBarUpdater->UpdateProgress(0.0);

  this->inputImage = input;
  this->inputImage->GetExtent(this->Extent);
  this->inputImage->GetDimensions(this->dims);
	
  this->outputImage = output;
  this->outputImage->SetScalarTypeToFloat();
  this->outputImage->SetExtent(this->Extent);
  this->outputImage->SetSpacing(this->inputImage->GetSpacing());
  this->outputImage->SetDimensions(this->inputImage->GetDimensions());
  this->outputImage->AllocateScalars();

  input->GetDimensions(dims);
  input->GetScalarRange(scalarRange);

  imgSize = dims[0]*dims[1]*dims[2];
	
  inPtr = (float *)malloc(sizeof(float) * imgSize);
  outPtr = (float*)output->GetScalarPointer();

  if (classes.size()==0)
  {
    for (long int i = 0 ; i<imgSize ; i++)
    {
	  outPtr[i] = inPtr[i];
	}
	return;
  }

  normPtr = (float *)malloc(sizeof(float) * imgSize);

//----------------------------------------------------------------------------------|    

  int cont = 0;

  for (int z = this->Extent[4]; z <= this->Extent[5]; z++)
  {
    for (int y = this->Extent[2]; y <= this->Extent[3]; y++)
    {
      for (int x = this->Extent[0]; x <= this->Extent[1]; x++)
      {
	    inPtr[cont] = this->inputImage->GetScalarComponentAsFloat(x, y, z, 0);
		cont++;
  	  }
   	}
  }

//----------------------------------------------------------------------------------|

  this->ProgressBarUpdater->SetProgress(0.2);
  this->ProgressBarUpdater->UpdateProgress(0.2);
    
  for (long int i = 0 ; i<imgSize ; i++)
  {
	normPtr[i] = 0.0;
  }

  //Allocate memory for the variations	
  edgeImage->SetNumberOfComponents(classes.size());
  edgeImage->SetNumberOfTuples(imgSize);

  edgeImage->Allocate(imgSize);

  distImage->SetNumberOfComponents(classes.size());
  distImage->SetNumberOfTuples(imgSize);
  
  distImage->Allocate(imgSize);

  // sort classes vector
  sort(classes.begin(),classes.end());

  //normalize classes values to [0,1].
  for (ClassesIteractor = classes.begin(); ClassesIteractor<classes.end() ; ClassesIteractor++)
  {
	*ClassesIteractor=float(*ClassesIteractor-scalarRange[0])/(scalarRange[1]-scalarRange[0]);
  }

  // initialize with the value that is in the middle of the list 
  int pos = int(classes.size() / 2);
 
  //normalize the input data to [0,1] and give initial value to outPtr.
  for (cont = 0; cont < imgSize; cont++)
  {
    normPtr[cont] = float((inPtr[cont] - scalarRange[0])/(scalarRange[1]-scalarRange[0]));
    outPtr[cont] = classes[pos];
  }

  this->ProgressBarUpdater->SetProgress(0.4);
  this->ProgressBarUpdater->UpdateProgress(0.4);

  //here the algorithm actaly does its job
  this->iterativeProcess();

  //Get the values back to the original range
  int i = 0;

  this->ProgressBarUpdater->SetProgress(0.7);
  this->ProgressBarUpdater->UpdateProgress(0.7);

  this->outputImage->SetExtent(this->Extent);

  for (int z = Extent[4]; z <= Extent[5]; z++)
  {
    for (int y = Extent[2]; y <= Extent[3]; y++)
    {
      for (int x = Extent[0]; x <= Extent[1]; x++)
      {
    	output->SetScalarComponentFromFloat(x, y, z, 0, float((outPtr[i] * (scalarRange[1]-scalarRange[0]))+scalarRange[0]));
	    if (this->InfoArray)
	    {
		  this->InfoArray->InsertNextTuple1(int(output->GetScalarComponentAsFloat(x, y, z, 0)));
	    }
	    
	    if (i == (imgSize/2))
	    {
		  this->ProgressBarUpdater->SetProgress(0.86);
		  this->ProgressBarUpdater->UpdateProgress(0.86);
	    }

		i++;
  	  }
  	}
  }

  this->inputImage = NULL;

  free(normPtr);
  free(inPtr);
}

void vtkHMTopologicalDerivativeMethod::iterativeProcess()
{

	// Calculate the initial value for the cost function
	// because in the initial step every voxel is the same 
	// class we don't have to calculate the line integral.
	// CAUTION!!! this have to be alterated if we start from 
	// a better start point.
	calculateDistanceVariation(normPtr,outPtr,classes,distImage);

	//Search for the minimum in tdImage
	//that means what is the class that's minimum for this voxel
	//in the cost function in this step. Then assign the class value 
	//to that voxel un the segmentation image.

	minimazeCostFunction(outPtr,classes,distImage);
	float costFunctionAnt;
	costFunctionAnt=costFunctionMin-1;

    //Number of Iteractions
	float iter=0;

/*
	cout << "Iteration: "<< iter <<"\n   costFunctionMin: "<<costFunctionMin
		                         <<"\n   costFunctionAnt: "<<costFunctionAnt
								 <<"\n   totalVoxels: "    <<imgSize
								 <<"\n   changedVoxels: "  <<changedVoxels
								 <<"\n\n";
//	cout << "\nRho: "<<rho<<"\n";

	cout << "\n***Iterative process started***\n\n";
*/
	//if the minimum is negative, we continue to iterate.
//	while (costFunctionMin > costFunctionAnt){
    while (iter<1){
		if (iter<10){

			this->ProgressBarUpdater->SetProgress(this->ProgressBarUpdater->GetProgress() + iter/10);
			this->ProgressBarUpdater->UpdateProgress(this->ProgressBarUpdater->GetProgress() + iter/10);
		}else{
			this->ProgressBarUpdater->SetProgress(this->ProgressBarUpdater->GetProgress() + iter/50);
			this->ProgressBarUpdater->UpdateProgress(this->ProgressBarUpdater->GetProgress() + iter/50);
		}
		costFunctionAnt=costFunctionMin;
		//Calculate distance
		calculateDistanceVariation(normPtr,outPtr,classes,distImage);
//		cout << "\n\nEn iterativeProcess costFunctionRange[0] vale: " << costFunctionRange[0] << "\n\n";

		this->ProgressBarUpdater->SetProgress(this->ProgressBarUpdater->GetProgress() + 0.01);
		this->ProgressBarUpdater->UpdateProgress(this->ProgressBarUpdater->GetProgress() + 0.01);

		//Calculate edges longitude
		calculateEdgesVariation(normPtr,outPtr,classes,distImage,edgeImage);

		this->ProgressBarUpdater->SetProgress(this->ProgressBarUpdater->GetProgress() + 0.03);
		this->ProgressBarUpdater->UpdateProgress(this->ProgressBarUpdater->GetProgress() + 0.03);
		//Search for the minimum in distImage
		//that means what is the class that's minimum for this voxel
		//in the cost function in this step. Then assign the class value
		//to that voxel un the segmentation image.
		minimazeCostFunction(outPtr,classes,edgeImage);
		
		this->ProgressBarUpdater->SetProgress(this->ProgressBarUpdater->GetProgress() + 0.04);
		this->ProgressBarUpdater->UpdateProgress(this->ProgressBarUpdater->GetProgress() + 0.04);
		
		iter++;
	}
//	cout << "\n***Iterative process ended in "<< iter <<" iterations***\n";
}

void vtkHMTopologicalDerivativeMethod::calculateEdgesVariation(float *normPtr, float *outPtr, ClassesVector classes, vtkFloatArray *distImage, vtkFloatArray *edgeImage)
{
	int ijk[3],ijkNeigs[6][3],nNeigs;
	float *neigValues,actValue,newDiff,actDiff,deriv;
	long int auxId,actId;
	nNeigs = (activeDirections[0]+activeDirections[1]+activeDirections[2])*2;
	neigValues = (float *) malloc(sizeof(float)* 6);
	costFunctionMin = edgeImage->GetDataTypeMax();

	for (ijk[2]=0;ijk[2]<dims[2];ijk[2]++){
		for (ijk[1]=0;ijk[1]<dims[1];ijk[1]++){
			for (ijk[0]=0;ijk[0]<dims[0];ijk[0]++){
/*
	for (ijk[2] = this->Extent[4]; ijk[2] < Extent[5]; ijk[2]++)
	{
		for (ijk[1] = this->Extent[2]; ijk[1] < Extent[3]; ijk[1]++)
		{
			for (ijk[0] = this->Extent[0]; ijk[0] < this->Extent[1]; ijk[0]++)
			{
			*/
				//ATENTION!!!!! depending on how am I going to treat limits
				//I have to initialize the neighbor values with different values.
				//Initialize all neighbors
				actId = inputImage->ComputePointId(ijk);
				actValue = outPtr[actId];
				int i;
				for (i=0;i<6;i++){
					neigValues[i]=actValue;
				}
				if (activeDirections[0]){
					//neighbor to the left
					ijkNeigs[0][0] = ijk[0]-1;
					ijkNeigs[0][1] = ijk[1];
					ijkNeigs[0][2] = ijk[2];
					
					//neighbor to the right
					ijkNeigs[1][0] = ijk[0]+1;
					ijkNeigs[1][1] = ijk[1];
					ijkNeigs[1][2] = ijk[2];

					//Get left neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[0]);
					//If it's inside the image get class value
					if (ijk[0]-1 >= 0){
						neigValues[0] = outPtr[auxId];
					}

					//Get right neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[1]);
					//If it's inside the image get class value
					if (ijk[0]+1 < dims[0] ){
						neigValues[1] = outPtr[auxId];
					}
				}
				if (activeDirections[1]){
					//neighbor to the back
					ijkNeigs[2][0] = ijk[0];
					ijkNeigs[2][1] = ijk[1]-1;
					ijkNeigs[2][2] = ijk[2];

					//neighbor to the front
					ijkNeigs[3][0] = ijk[0];
					ijkNeigs[3][1] = ijk[1]+1;
					ijkNeigs[3][2] = ijk[2];

					//Get back neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[2]);
					//If it's inside the image get class value
					if (ijk[1]-1 >= 0){
						neigValues[2] = outPtr[auxId];
					}

					//Get front neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[3]);
					//If it's inside the image get class value
					if (ijk[1]+1 < dims[1]){
						neigValues[3] = outPtr[auxId];
					}
				}

				if (activeDirections[2]){
					//neighbor to the top
					ijkNeigs[4][0] = ijk[0];
					ijkNeigs[4][1] = ijk[1];
					ijkNeigs[4][2] = ijk[2]-1;

					//neighbor to the bottom
					ijkNeigs[5][0] = ijk[0];
					ijkNeigs[5][1] = ijk[1];
					ijkNeigs[5][2] = ijk[2]+1;

					//Get top neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[4]);
					//If it's inside the image get class value
					if (ijk[2]-1 >= 0){
						neigValues[4] = outPtr[auxId];
					}

					//Get bottom neighbor id
					auxId = inputImage->ComputePointId(ijkNeigs[5]);
					//If it's inside the image get class value
					if (ijk[2]+1 < dims[2]){
						neigValues[5] = outPtr[auxId];
					}
				}
				int c,j;
				ClassesVector::iterator it;
				actDiff=0;
				for (j=0;j<6;j++){
					if (neigValues[j]!=actValue){
						actDiff++;
					}						
				}
				c = 0;
//				cout << "Para Id " << actId << "\n";
				for (it = classes.begin(); it < classes.end() ; it++){
					//For each neighbor, variate and count if is different
					newDiff=0;
					for (j=0;j<6;j++){
						//(neigValues[j]!=*it)?newDiff++:NULL;
						(neigValues[j]!=*it)?newDiff++:0.0;
					}
					//Divide by 6 because I'm using normalized
					//values, we can control that with rho
					deriv = distImage->GetComponent(actId,c);
//					cout << "		cls: "<< *it << " derivAnt: " << deriv;
					//The signal is negative because instead of calculating
					//how many neighbors are the same, y calculate how many 
					//are different
					deriv += (1-rho) * ((newDiff-actDiff)/6) * 0.05;
//					cout << " derivAct: " << deriv << "\n";
					edgeImage->SetComponent(actId,c,deriv);
					c++;
				}
			}
		}
	}
	free(neigValues);
}

void vtkHMTopologicalDerivativeMethod::minimazeCostFunction(float *outPtr, ClassesVector classes, vtkFloatArray *edgeImage)
{
	long int auxpid; //run the hole image
	float min,iterMin;
	double *comps = (double *)malloc(sizeof(double)*edgeImage->GetNumberOfComponents());
	float oldValue;
	changedVoxels = 0;
	edgeImage->GetTuple(0,comps);
	iterMin = comps[0];

//	search, on every voxel, the minimum component value.
//	loop over voxels
	for (auxpid=0 ; auxpid<imgSize ; auxpid++)
	{
//		get the next tuple
		edgeImage->GetTuple(auxpid,comps);
		min = comps[0];
		oldValue = outPtr[auxpid];
		outPtr[auxpid] = classes[0];
//		loop over components
		for (int i = 1 ; i < classes.size() ; i++)
		{
//			is this component the minimum?
            if (comps[i] < min) // && comps[i] < 0
            {
				//Set that class value (the minimum one) to the voxel being processed.
				outPtr[auxpid] = classes[i];
				min = comps[i];
            }
		}

		if (outPtr[auxpid]!=oldValue) changedVoxels++;
		if (min<iterMin) {
			iterMin = min;
		}
	}
	if (iterMin<costFunctionMin) costFunctionMin = iterMin;
	free(comps);
}

void vtkHMTopologicalDerivativeMethod::calculateDistanceVariation(float *normPtr, float *outPtr, ClassesVector classes, vtkFloatArray *distImage)
{
	// Calculate the initial value for the cost function
	// because in the initial step every voxel is the same 
	// class we don't have to calculate the line integral.
	// CAUTION!!! this have to be alterated if we start from 
	// a better start point.
	long int ipid;
	float newCost,actCost,deriv;
	//Iterators to run the vector.
	ClassesVector::iterator clsIt;
	costFunctionMin = distImage->GetDataTypeMax();
	//For every point in the image
	for (ipid = 0 ; ipid<imgSize ; ipid++){
		//calculate the cost function value for the old (class) value
		//old = (normalizedInputImage[ipid]-outPtr[ipid])^2
		actCost = (normPtr[ipid]-outPtr[ipid]);
        actCost = actCost*actCost;
		int i;
		// for every class (TD image)
		for (clsIt=classes.begin(), i = 0; 
			clsIt < classes.end() ; i++ , clsIt++){
			//calculate the cost function this specific class.
			//deriv = (normalizedInputImage[ipid] - classes[clsIt])^2 - 
			//	      (normalizedInputImage[ipid] -   outPtr[ipid])^2

			newCost = (normPtr[ipid]-(*clsIt));
			newCost = newCost*newCost;
			//calculate the Topological derivative, for this pixel, for this test class
			deriv = rho * (newCost - actCost);
			distImage->SetComponent(ipid,i,deriv);
		}
	}
}


void vtkHMTopologicalDerivativeMethod::setRho(double rho)
{
	this->rho = rho;
}

double vtkHMTopologicalDerivativeMethod::getRho()
{
	return rho;
}

void vtkHMTopologicalDerivativeMethod::setActiveDirections(int *activeDirections)
{
	this->activeDirections[0] = activeDirections[0];
	this->activeDirections[1] = activeDirections[1];
	this->activeDirections[2] = activeDirections[2];
}

void vtkHMTopologicalDerivativeMethod::SetInfoArray(vtkFloatArray *PassedFloatArray)
{
	this->InfoArray = PassedFloatArray;
}
	
vtkFloatArray *vtkHMTopologicalDerivativeMethod::GetInfoArray()
{
	return this->InfoArray;
}

void vtkHMTopologicalDerivativeMethod::SetProgressBarUpdater(vtkImageAlgorithm *Obj)
{
  this->ProgressBarUpdater = Obj; 
}
