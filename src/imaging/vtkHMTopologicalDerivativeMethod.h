#ifndef __vtkHMTopologicalDerivativeMethod_h
#define __vtkHMTopologicalDerivativeMethod_h

#include <vtkObjectFactory.h>

#include <vtkImageData.h>
#include <vtkPointSet.h>
#include <vtkSimpleImageToImageFilter.h>
#include <vtkFloatArray.h>
#include <vector>
#include <algorithm>

class VTK_EXPORT vtkHMTopologicalDerivativeMethod : public vtkSimpleImageToImageFilter
{
public:

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMTopologicalDerivativeMethod *New();

  // Description:
  // Macro required by VTK when extending its classes.
  vtkTypeRevisionMacro(vtkHMTopologicalDerivativeMethod, vtkSimpleImageToImageFilter);

  //BTX
  // Description:  
  //Type of vector wich stores the algorithm classes.
  typedef std::vector<double> ClassesVector;
  //ETX

  // Description:
  // Adds a class (double value) to the algorithm classes vector.
  void AddClass(double cl);

  // Description:
  // Set a ClassesVector as the objects current ClassesVector attribute.
  void setClasses(ClassesVector cls);

  // Description:
  // Returns the objects classes vector.
  ClassesVector getClasses();

  // Description:
  // Set the Linear combination of the distance/line integral for the algorithm
  void setRho(double rho);

  // Description:
  // Returns the Linear combination of the distance/line integral for the algorithm
  double getRho();

  // Description:
  // Set the InfoArray for this object.
  void SetInfoArray(vtkFloatArray *PassedFloatArray);

  // Description:
  // Returns the Info Array of this object.
  vtkFloatArray *GetInfoArray();

  // Description:
  // Set the directions that the algorithm should run.
  void setActiveDirections(int *activeDirections);

  // Description:
  // Set the Progress Bar updater object.
  void SetProgressBarUpdater(vtkImageAlgorithm *Obj);

protected:

  // Description:
  // Constructor
  vtkHMTopologicalDerivativeMethod();

  // Description:
  // Destructor	
  virtual ~vtkHMTopologicalDerivativeMethod();

  // Description:
  //Contains the classes to segment the image in.
  ClassesVector classes;

  //BTX
  // Description:
  //Iteractor for classes vector.
  ClassesVector::iterator ClassesIteractor;
  //ETX

  // Description:
  //Object that contains the reference (SetProgress() and UpdateProgress() methods connected to Paraview) to the Paraview's progress bar.
  vtkImageAlgorithm *ProgressBarUpdater;

  //Description:
  //Linear combination of the distance/line integral
  double rho;

  //Description:
  //Image size
  long int imgSize;

  //Description:
  //Contains 1's in the position of the active segmentation directions.
  int activeDirections[3];

  //Description:
  //Input image Extent
  int Extent[6];

  //Description:
  //Input image Dimensions
  int dims[3];

  //Description:
  //Store the number of changed voxels for a iteraction.
  long changedVoxels;

  //Description:
  //Contains pointer to image input. ONLY VALID DURING SIMPLE EXECUTION!!!
  vtkImageData* inputImage;

  //Description:
  //Contains pointer to image output. ONLY VALID DURING SIMPLE EXECUTION!!!
  vtkImageData* outputImage;

  //Description:
  //Contains the input image data.
  float* inPtr;

  //Description:
  //Contains the output(segmented) image data.
  float* outPtr;

  //Description:
  //Contains the normalized input image data.
  float* normPtr;

  //Description:
  // Data array used to fill the Information tab.	
  vtkFloatArray* InfoArray;

  //Description:
  //Contains a vtkFloatArray for every class.
  vtkFloatArray* distImage;
	
  //Description:
  //Contains a vtkFloatArray for every class.
  vtkFloatArray* edgeImage;

  //Description:	
  //Original Image values range.
  double costFunctionMin;

  //Description:	
  //Stores the minimum and maximum voxel values of the image.
  double scalarRange[2];

  //Description:	
  //Method that calculates the cost function for the algorithm.
  void minimazeCostFunction(float *outPtr, ClassesVector classes, vtkFloatArray *derivImage);

  //Description:	
  //Starts the iterative process (where the algorithm accually does its job).
  virtual void iterativeProcess();

  //Description:	
  //Calculate the initial value for the cost function.
  void calculateDistanceVariation(float *normPtr, float *outPtr, ClassesVector classes, vtkFloatArray *derivImage);

  //Description:	
  //Calculate the edges variation of the input vtkImageData object bassed on the passed ClassesVector.	
  void calculateEdgesVariation(float *normPtr, float *outPtr, ClassesVector classes, vtkFloatArray *distImage, vtkFloatArray *edgeImage);

  //Description:	
  //Method invoked during the object's Update(). Update() method must be invoked so the Topological Derivative work.
  virtual void SimpleExecute(vtkImageData* input,vtkImageData* output);
};

#endif
