#include "vtkHMVoxelGrow.h"
#include "vtkImageData.h"
#include "vtkLookupTable.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkStreamingDemandDrivenPipeline.h"

#include "vtkPointData.h"
#include "vtkUnsignedIntArray.h"
#include "vtkImageMapToColors.h"

#include <stack>
using std::stack;


#include "vtkPNGWriter.h"
#include "vtkImageCast.h"

vtkCxxRevisionMacro(vtkHMVoxelGrow, "$Rev$");
vtkStandardNewMacro(vtkHMVoxelGrow);

vtkHMVoxelGrow::vtkHMVoxelGrow()
{
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);

  this->LowerThreshold = 0;
  this->UpperThreshold = 0;
  
  this->IntensitiesAnalysisMode = 0;
  this->ThresholdsMode = 0;


  this->NumberOfNeighboors = 0;
  this->IslandDetectionFactor = 0;
  this->NeighboorsAnalizableRadius = 0;
  this->IslandDetectionEnabled = 0;
  this->LastNumberOfIslands = 0;
  
  this->ModifiedFlag = 0;

  for (int i = 0; i < 6; i++)
  {
    this->InExt[i] = 0;
  	this->OutExt[i] = 0;
  }
  
  this->InfoArray = vtkUnsignedIntArray::New();
  this->InfoArray->SetName("VoxelGrow");
}

vtkHMVoxelGrow::~vtkHMVoxelGrow()
{
  this->Output = NULL;
  this->InfoArray->Delete();
}

int vtkHMVoxelGrow::FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkImageData");

  return 1;
}

void vtkHMVoxelGrow::SetThreshold(int LowerThreshold, int UpperThreshold)
{
  this->LowerThreshold = LowerThreshold;
  this->UpperThreshold = UpperThreshold;
  
  this->Modified();
}

void vtkHMVoxelGrow::AddSeedThresholds(int LowerThreshold, int UpperThreshold)
{
  this->LowerThresholds.push_back(LowerThreshold);
  this->UpperThresholds.push_back(UpperThreshold);
  
  this->Modified();
}

void vtkHMVoxelGrow::RemoveSeedThresholds(int PassedID)
{
  SeedsThresholdsVector::iterator LowerThresholdIteractor = this->LowerThresholds.begin();
  SeedsThresholdsVector::iterator UpperThresholdIteractor = this->UpperThresholds.begin();

  for(int i = 0; i < PassedID; i++)
  {
    LowerThresholdIteractor++;
    UpperThresholdIteractor++;
  }

  this->Modified();

  this->LowerThresholds.erase(LowerThresholdIteractor);
  this->UpperThresholds.erase(UpperThresholdIteractor);

  this->Modified();
}

int vtkHMVoxelGrow::RequestData(vtkInformation* vtkNotUsed( request ), vtkInformationVector** vtkNotUsed( inputVector) , vtkInformationVector* outputVector)
{
//  cout << "vtkHMVoxelGrow::RequestData" << endl;
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  this->Output = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
  this->GetImageDataInput(0)->GetExtent(this->InExt);

  //Controls the disponibilization of the input or output of this filter.
  if(this->ModifiedFlag)
  {
  	//this->Output->DeepCopy(this->GetImageDataInput(0));
    this->InfoArray->Reset();

    this->Output->SetScalarTypeToUnsignedInt();
    this->SetInformationFromInput();

    this->ExecuteVoxelGrow();

    this->Output->SetExtent(this->OutExt);
    this->Output->SetWholeExtent(this->OutExt);

    double Range[2];

    this->InfoArray->GetRange(Range);
    this->Output->GetPointData()->AddArray(this->InfoArray);

	//this->ExportPNG(this->Output);
  }
  else
  {
  	this->Output->DeepCopy(this->GetImageDataInput(0));
  	this->Output->GetExtent(OutExt);
  	this->ModifiedFlag = 1;
  	this->Modified();
  }

  return 1;
}

void vtkHMVoxelGrow::ExecuteVoxelGrow()
{
//  cout << "ExecuteVoxelGrow" << endl;

  this->SetProgress(0);
  this->SetProgressText("Initializing...");
  
  this->SetInformationFromInput();
  this->Output->SetExtent(this->OutExt);
  this->Output->SetWholeExtent(this->OutExt);
  this->Output->SetDimensions(this->Dimensions);
  this->Output->SetSpacing(this->Spacing);
  this->Output->SetScalarTypeToUnsignedInt();
  this->Output->AllocateScalars();

  //Fills the whole output with zeros (needed on windows).
  this->InitializeOutput();

  //ImageData that controls the visited voxels.
  vtkImageData *VisitedVoxels = vtkImageData::New();

  VisitedVoxels->SetExtent(this->OutExt);
  this->Output->SetScalarTypeToUnsignedInt();
  VisitedVoxels->AllocateScalars();

  //Stack used on Island Removal.
   stack<int> IslandRemovalStack;

  double IntensitiesAverage;

  int NumberOfSeeds = this->Seeds[0].size();
  int ImageSize = (this->Dimensions[0] * this->Dimensions[1] * this->Dimensions[2]);

  int MinX = this->OutExt[0];
  int MinY = this->OutExt[2];
  int MinZ = this->OutExt[4];
  
  int MaxX = this->OutExt[1];
  int MaxY = this->OutExt[3];
  int MaxZ = this->OutExt[5];

  if (this->IntensitiesAnalysisMode)
  {
  	double sum = 0;
  	for (int i = 0; i < NumberOfSeeds; i++)
  	{
      if ( (int(this->Seeds[0].at(i)) < this->InExt[0]) || (int(this->Seeds[0].at(i)) > this->InExt[1]) || (int(this->Seeds[1].at(i)) < this->InExt[2]) || (int(this->Seeds[1].at(i)) > this->InExt[3]) || (int(this->Seeds[2].at(i)) < this->InExt[4]) || (int(this->Seeds[2].at(i)) > this->InExt[5]) )
      {
    	continue;
      }

  	  sum += this->GetImageDataInput(0)->GetScalarComponentAsFloat(int(this->Seeds[0].at(i)), int(this->Seeds[1].at(i)), int(this->Seeds[2].at(i)), 0);
  	}

    IntensitiesAverage = sum / NumberOfSeeds;
  }
  
  //Controls the filter execution for each seed.
  for (int i=0; i < NumberOfSeeds; i++)
  {
    this->IncreaseProgress((0.2 / NumberOfSeeds), "Working on the Image...");

    int SeedX = int(this->Seeds[0].at(i));
    int SeedY = int(this->Seeds[1].at(i));
    int SeedZ = int(this->Seeds[2].at(i));

    //Thresholds initialized with Unique mode values.
    int LowerThreshold = this->LowerThreshold;
    int UpperThreshold = this->UpperThreshold;

    if ( (SeedX < this->InExt[0]) || (SeedX > this->InExt[1]) || (SeedY < this->InExt[2]) || (SeedY > this->InExt[3]) || (SeedZ < this->InExt[4]) || (SeedZ > this->InExt[5]) )
    {
    	continue;
    }

    float Limiar = this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX, SeedY, SeedZ, 0);
    
    if (this->IntensitiesAnalysisMode)
    {
      Limiar = IntensitiesAverage;
    }

    if(this->ThresholdsMode)
    {
    //Using independent thresholds (per seed mode).
      LowerThreshold = this->LowerThresholds.at(i);
      UpperThreshold = this->UpperThresholds.at(i);
    }
    
    int v1x = SeedX;   int v1y = SeedY; int v1z = SeedZ+1;

    int v2x = SeedX-1; int v2y = SeedY; int v2z = SeedZ;

    int v3x = SeedX;   int v3y = SeedY+1; int v3z = SeedZ;

    int v4x = SeedX; int v4y = SeedY;  int v4z = SeedZ-1;

    int v5x = SeedX+1; int v5y = SeedY;  int v5z = SeedZ;

    int v6x = SeedX; int v6y = SeedY-1;  int v6z = SeedZ; 

    stack<int> SeedStack;

    //Working on the seed...
    
    if( (v1x > MinX) && (v1y > MinY) && (v1z > MinZ))
    {  
  	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v1x,v1y,v1z,0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v1x,v1y,v1z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v1x,v1y,v1z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v1x, v1y, v1z, 0) != 1.0))
  	  {
	    SeedStack.push(v1x);
	    SeedStack.push(v1y);
	    SeedStack.push(v1z);
	    this->Output->SetScalarComponentFromFloat(v1x-InExt[0],v1y-InExt[2],v1z-InExt[4],0,255.0);
 	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v1x, v1y, v1z, 0, 1.0);
    }

    if( (v2x > MinX) && (v2y > MinY) && (v2z > MinZ))
    {
	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v2x,v2y,v2z,0)) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v2x,v2y,v2z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v2x,v2y,v2z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v2x, v2y, v2z, 0) != 1.0))
	  {
	    SeedStack.push(v2x);
		SeedStack.push(v2y);
		SeedStack.push(v2z);
	    this->Output->SetScalarComponentFromFloat(v2x-InExt[0],v2y-InExt[2],v2z-InExt[4],0,255.0);
	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v2x, v2y, v2z, 0, 1.0);
    }

    if( (v3x > MinX) && (v3y > MinY) && (v3z > MinZ))
    {
	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v3x,v3y,v3z,0) ) >=  (Limiar - LowerThreshold)  && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v3x,v3y,v3z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v3x,v3y,v3z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v3x, v3y, v3z, 0) != 1.0))
	  {
		SeedStack.push(v3x);
		SeedStack.push(v3y);
		SeedStack.push(v3z);
		this->Output->SetScalarComponentFromFloat(v3x-InExt[0],v3y-InExt[2],v3z-InExt[4],0,255.0);  
	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v3x, v3y, v3z, 0, 1.0);
    }
    if((v4x > MinX) && (v4y > MinY) && (v4z > MinZ))
    {
	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v4x,v4y,v4z,0) ) >= (Limiar - LowerThreshold)  && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v4x,v4y,v4z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v4x,v4y,v4z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v4x, v4y, v4z, 0) != 1.0))
	  {
		SeedStack.push(v4x);
		SeedStack.push(v4y);
		SeedStack.push(v4z);
		this->Output->SetScalarComponentFromFloat(v4x-InExt[0],v4y-InExt[2],v4z-InExt[4],0,255.0);  
	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v4x, v4y, v4z, 0, 1.0);
    }

    if((v5x > MinX) && (v5y > MinY) && (v5z > MinZ))
    {
	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v5x,v5y,v5z,0) ) >=  (Limiar - LowerThreshold)  && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v5x,v5y,v5z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v5x,v5y,v5z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v5x, v5y, v5z, 0) != 1.0))
	  {
		SeedStack.push(v5x);
		SeedStack.push(v5y);
		SeedStack.push(v5z);
		this->Output->SetScalarComponentFromFloat(v5x-InExt[0], v5y-InExt[2], v5z-InExt[4], 0, 255.0);  
	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v5x, v5y, v5z, 0, 1.0);
    }

    if((v6x > MinX) && (v6y > MinY) && (v6z > MinZ))
    {
	  if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(v6x,v6y,v6z,0) ) >=  (Limiar - LowerThreshold)  && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v6x,v6y,v6z,0)) <= (Limiar + UpperThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(v6x,v6y,v6z,0) != 255.0) && (VisitedVoxels->GetScalarComponentAsFloat(v6x, v6y, v6z, 0) != 1.0))
	  { 
		SeedStack.push(v6x);
		SeedStack.push(v6y);
		SeedStack.push(v6z);
		this->Output->SetScalarComponentFromFloat(v6x-InExt[0],v6y-InExt[2],v6z-InExt[4],0,255.0);  
	  }
	  VisitedVoxels->SetScalarComponentFromFloat(v6x, v6y, v6z, 0, 1.0);
    }

    int StackLimitReached = 0;
    int ProgressInactivityControler = 0;

    //Working on the image...
    
    while(SeedStack.size() != 0)
    {
	  SeedZ = SeedStack.top();
	  SeedStack.pop();

	  SeedY = SeedStack.top();
	  SeedStack.pop();

	  SeedX = SeedStack.top();
	  SeedStack.pop();
	  
      int InExt[6];
      
      this->Output->GetExtent(OutExt);
      this->GetImageDataInput(0)->GetExtent(InExt);
      
      this->Output->SetWholeExtent(OutExt);
      
	  //- Averaged calcule for progress bar ------------------------------------------|
	  int StackPosition = SeedStack.size();
      if((StackPosition < StackLimitReached) && (StackPosition < StackLimitReached - 180) && (StackPosition < ((this->Dimensions[0] * this->Dimensions[1] * this->Dimensions[2])*0.2)) && StackPosition < (ImageSize / 450))
      {
        double Factor = ((StackPosition/3)*100 / (0.8 / (StackLimitReached / 3))) / (this->Dimensions[0] * this->Dimensions[1] * this->Dimensions[2])/ImageSize;

        this->IncreaseProgress(Factor, "Verifing seed's neighboors...");
      }
      ProgressInactivityControler ++;
      if(ProgressInactivityControler > (ImageSize * 0.001))
      {
      	this->IncreaseProgress(0.01, "Verifing seed's neighboors...");
      	ProgressInactivityControler = 0;
      }
     //------------------------------------------------------------------------------|
     
	  if(((SeedX > MinX) && (SeedY > MinY) ) &&  ( (SeedX < MaxX) && (SeedY < MaxY) ) && ( (SeedZ >MinZ) && ( SeedZ < MaxZ)))
	  {	 
        if (VisitedVoxels->GetScalarComponentAsFloat(SeedX, SeedY, SeedZ+1, 0) != 1.0)
        {
	      if( ( this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX, SeedY, SeedZ + 1, 0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY,SeedZ+1,0)) <= (Limiar + UpperThreshold))
	      {
	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0], SeedY-InExt[2], SeedZ-InExt[4] + 1, 0, 255.0);
	        SeedStack.push(SeedX);
  		    SeedStack.push(SeedY);
		    SeedStack.push(SeedZ+1);
	      }
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX);
	        IslandRemovalStack.push(SeedY);
	        IslandRemovalStack.push(SeedZ+1);
	      }
          VisitedVoxels->SetScalarComponentFromFloat(SeedX, SeedY, SeedZ+1, 0 , 1.0);
	    }

        if(VisitedVoxels->GetScalarComponentAsFloat(SeedX-1, SeedY, SeedZ, 0) != 1.0)
        {
          if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX-1,SeedY,SeedZ,0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX-1,SeedY,SeedZ,0)) <= (UpperThreshold + Limiar))
	      {
 	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0] - 1, SeedY-InExt[2], SeedZ-InExt[4], 0, 255.0);
		    SeedStack.push(SeedX-1);
		    SeedStack.push(SeedY);
		    SeedStack.push(SeedZ);
	      }
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX-1);
	        IslandRemovalStack.push(SeedY);
	        IslandRemovalStack.push(SeedZ);
	      }
  	      VisitedVoxels->SetScalarComponentFromFloat(SeedX-1, SeedY, SeedZ, 0, 1.0);
        }
        
        if(VisitedVoxels->GetScalarComponentAsFloat(SeedX, SeedY+1, SeedZ, 0) != 1.0)        
        {
	      if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY+1,SeedZ,0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY+1,SeedZ,0)) <= (Limiar + UpperThreshold))
	      {
	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0], SeedY-InExt[2] + 1, SeedZ-InExt[4], 0, 255.0);
		    SeedStack.push(SeedX);
		    SeedStack.push(SeedY+1);
		    SeedStack.push(SeedZ);
	      }	
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX);
	        IslandRemovalStack.push(SeedY+1);
	        IslandRemovalStack.push(SeedZ);
	      }
 	      VisitedVoxels->SetScalarComponentFromFloat(SeedX, SeedY+1, SeedZ, 0, 1.0);
        }

        if(VisitedVoxels->GetScalarComponentAsFloat(SeedX, SeedY, SeedZ-1, 0) != 1.0)
        {
	      if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY,SeedZ-1,0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY,SeedZ-1,0)) <= (Limiar + UpperThreshold))
	      {
	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0], SeedY-InExt[2], SeedZ-InExt[4] - 1, 0, 255.0);
		    SeedStack.push(SeedX);
		    SeedStack.push(SeedY);
		    SeedStack.push(SeedZ-1);
	      }	
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX);
	        IslandRemovalStack.push(SeedY);
	        IslandRemovalStack.push(SeedZ-1);
	      }
 	      VisitedVoxels->SetScalarComponentFromFloat(SeedX, SeedY, SeedZ-1, 0, 1.0);
        }
        
        if(VisitedVoxels->GetScalarComponentAsFloat(SeedX+1, SeedY, SeedZ, 0) != 1.0)
        {
  	      if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX+1,SeedY,SeedZ,0) ) >= (Limiar - LowerThreshold) && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX+1,SeedY,SeedZ,0)) <= (Limiar + UpperThreshold))
	      {
	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0] + 1, SeedY-InExt[2], SeedZ-InExt[4], 0, 255.0);
		    SeedStack.push(SeedX+1);
		    SeedStack.push(SeedY);
		    SeedStack.push(SeedZ);
	      }	
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX+1);
	        IslandRemovalStack.push(SeedY);
	        IslandRemovalStack.push(SeedZ);
	      }
	      VisitedVoxels->SetScalarComponentFromFloat(SeedX+1, SeedY, SeedZ, 0, 1.0);
        }

        if(VisitedVoxels->GetScalarComponentAsFloat(SeedX, SeedY-1, SeedZ, 0) != 1.0)
        {
   	      if((this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY-1,SeedZ,0) ) >=  (Limiar - LowerThreshold)  && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY-1,SeedZ,0)) <= (Limiar + UpperThreshold))// && (this->GetImageDataInput(0)->GetScalarComponentAsFloat(SeedX,SeedY-1,SeedZ,0) != 255.0))
	      {
	        this->Output->SetScalarComponentFromFloat(SeedX-InExt[0], SeedY-InExt[2] - 1, SeedZ-InExt[4], 0, 255.0);
		    SeedStack.push(SeedX);
		    SeedStack.push(SeedY-1);
		    SeedStack.push(SeedZ);
	      }
	      else
	      {
	        //Island control.
	        IslandRemovalStack.push(SeedX);
	        IslandRemovalStack.push(SeedY-1);
	        IslandRemovalStack.push(SeedZ);
	      }
 	      VisitedVoxels->SetScalarComponentFromFloat(SeedX, SeedY-1, SeedZ, 0, 1.0);
        }
      }
      
      if(StackPosition >= StackLimitReached)
      {
        StackLimitReached = StackPosition;
      }
    }
  }
  
  if(this->IslandDetectionEnabled)
  {
//  |-Island Removal ---------------------------------------------------|
    int Voxel[3] = {0,0,0};
//    float IslandRemovalCounter = IslandRemovalStack.size() / 3;
    
    float NumberOfNeighboors = 26;
    float NeighboorsOfRegionCounter = 0;
    int IslandCounter = 0;
  
//    for (int i=0; i < IslandRemovalCounter; i++)
    while((IslandRemovalStack.size() / 3) > 0)
    {
      Voxel[2] = IslandRemovalStack.top() - InExt[4];
      IslandRemovalStack.pop();

      Voxel[1] = IslandRemovalStack.top() - InExt[2];
      IslandRemovalStack.pop();

      Voxel[0] = IslandRemovalStack.top() - InExt[0];
      IslandRemovalStack.pop();

      NeighboorsOfRegionCounter = 0;
    
      //Calculating probability of beeing an Island.
      for (int x = -(this->NeighboorsAnalizableRadius - 1); x < this->NeighboorsAnalizableRadius; x++)
      { 
        for(int y =  -(this->NeighboorsAnalizableRadius - 1); y < this->NeighboorsAnalizableRadius; y++)
        {
          for(int z =  -(this->NeighboorsAnalizableRadius - 1); z < this->NeighboorsAnalizableRadius; z++)
          {
            if(this->IsInOutExtent(Voxel[0]+x, Voxel[1]+y, Voxel[2]+z))
            {    
              if ((this->Output->GetScalarComponentAsFloat(Voxel[0]+x, Voxel[1]+y, Voxel[2]+z, 0) == 255))
              {
                NeighboorsOfRegionCounter++;
              }
            }
          }
        }
      }
    
      float IslandPropability = NeighboorsOfRegionCounter / NumberOfNeighboors;

      if (IslandPropability >= this->IslandDetectionFactor)
      {
        IslandCounter++;
        this->Output->SetScalarComponentFromFloat(Voxel[0], Voxel[1], Voxel[2], 0, 255.0);

        if(this->IsInOutExtent(Voxel[0], Voxel[1], Voxel[2]))
        {    
          IslandRemovalStack.push(Voxel[0]);
          IslandRemovalStack.push(Voxel[1]);
          IslandRemovalStack.push(Voxel[2]);
        }
      }
    }

  this->LastNumberOfIslands = IslandCounter;
//    cout << "Number of Island Voxels found: " << IslandCounter << endl;
  }
  
  VisitedVoxels->Delete();
  int IncreaserCounter = 0;

  for (int z = 0; z < this->Dimensions[2]; z++)
  {
  	for (int y = 0; y < this->Dimensions[1]; y++)
  	{
  	  for (int x = 0; x < this->Dimensions[0]; x++)
  	  {
  	  	this->InfoArray->InsertNextTuple1(int(this->Output->GetScalarComponentAsFloat(x, y, z, 0)));
  	  	IncreaserCounter++;
  	  	if (IncreaserCounter > (ImageSize / 6))
  	  	{
  	  	  IncreaserCounter = 0;
  	  	  this->IncreaseProgress(1, "Creating Information Array...");
  	  	}
  	  }
  	}
  }

  this->ModifiedFlag = 1;

  this->GetImageDataInput(0)->GetExtent(InExt);
  this->GetImageDataInput(0)->GetExtent(OutExt);

  this->SetProgress(1.0);
  this->UpdateProgress(1.0);
  this->SetProgressText("Voxel Grow finished!");

//  this->ExportPNG(this->Output);
}

bool vtkHMVoxelGrow::IsInOutExtent(int VoxelX, int VoxelY, int VoxelZ)
{
  if ((VoxelX > this->OutExt[0]) && (VoxelX < this->OutExt[1]) &&  (VoxelY > this->OutExt[2]) && (VoxelY < this->OutExt[3]) && (VoxelZ > this->OutExt[4]) && ( VoxelZ < this->OutExt[5]))
  {
  	return true;
  }
  
  return false;
}

bool vtkHMVoxelGrow::VerifyImageData(vtkImageData *img)
{
  int inExt[6];
  img->GetExtent(inExt);

  if (inExt[1] < inExt[0] || inExt[3] < inExt[2] || inExt[5] < inExt[4])
  {
    return 0;
  }
  return 1;
}

void vtkHMVoxelGrow::SetInformationFromInput()
{
  this->GetImageDataInput(0)->GetExtent(this->OutExt);
  this->GetImageDataInput(0)->GetDimensions(this->Dimensions);
  this->GetImageDataInput(0)->GetSpacing(this->Spacing);
}

void vtkHMVoxelGrow::IncreaseProgress(double Value, char* ProgressText)
{
  this->SetProgressText(ProgressText);
  this->IncreaseProgress(Value);
}

void vtkHMVoxelGrow::IncreaseProgress(double Value)
{
  if ((this->GetProgress() + Value) > 0.95)
  {
  	Value = 0.95;
  }
  else
  {
    Value = this->GetProgress() + Value;
  }
  this->SetProgress(Value);
  this->UpdateProgress(this->GetProgress());
}

void vtkHMVoxelGrow::AddSeed(double PassedX, double PassedY, double PassedZ, double PassedIntensity)
{
  this->Seeds[0].push_back(PassedX);
  this->Seeds[1].push_back(PassedY);
  this->Seeds[2].push_back(PassedZ);
  this->Seeds[3].push_back(PassedIntensity);
  
  this->Modified();
}

void vtkHMVoxelGrow::RemoveSeed(int PassedSeedID)
{
  SeedsVector::iterator XSeedsIteractor = this->Seeds[0].begin();
  SeedsVector::iterator YSeedsIteractor = this->Seeds[1].begin();
  SeedsVector::iterator ZSeedsIteractor = this->Seeds[2].begin();
  SeedsVector::iterator IntensitySeedsIteractor = this->Seeds[3].begin();

  for(int i = 0; i < PassedSeedID; i++)
  {
    XSeedsIteractor++;
    YSeedsIteractor++;
    ZSeedsIteractor++;
    IntensitySeedsIteractor++;
  }

  this->Seeds[0].erase(XSeedsIteractor);
  this->Seeds[1].erase(YSeedsIteractor);
  this->Seeds[2].erase(ZSeedsIteractor);
  this->Seeds[3].erase(IntensitySeedsIteractor);

  this->Modified();
}

void vtkHMVoxelGrow::SetIntensitiesAnalysisMode(int PassedIntensitiesAnalysisMode)
{
  this->IntensitiesAnalysisMode = PassedIntensitiesAnalysisMode;
  
  this->Modified();
}

void vtkHMVoxelGrow::SetThresholdsMode(int PassedThresholdsMode)
{
  this->ThresholdsMode = PassedThresholdsMode;
  
  this->Modified();
}

void vtkHMVoxelGrow::SetIslandDetectionEnabled(int Flag)
{
  this->IslandDetectionEnabled = Flag;
}

void vtkHMVoxelGrow::SetIslandDetectionFactor(double PassedFactor)
{
  this->IslandDetectionFactor = PassedFactor;
}

void vtkHMVoxelGrow::SetNeighboorsAnalizableRadius(int PassedRadius)
{
  this->NeighboorsAnalizableRadius = PassedRadius;
}

int vtkHMVoxelGrow::GetLastNumberOfIslands()
{
  return this->LastNumberOfIslands;
}

void vtkHMVoxelGrow::InitializeOutput()
{
  int ImageSize = (this->Dimensions[0] * this->Dimensions[1] * this->Dimensions[2]); 

  float* outPtr = (float*)this->Output->GetScalarPointer();

  for (long int i = 0 ; i<ImageSize ; i++)
  {
    outPtr[i] = 0;
  }

  outPtr = NULL;
}
