#ifndef __vtkHMVoxelGrow_h
#define __vtkHMVoxelGrow_h

#include "vtkObjectFactory.h"
#include "vtkThreadedImageAlgorithm.h"
#include <vector>

class vtkImageData;
class vtkImageMapToColors;
class vtkLookupTable;
class vtkUnsignedIntArray;

class VTK_EXPORT vtkHMVoxelGrow : public vtkThreadedImageAlgorithm
{
public:

//BTX
  typedef std::vector <double> SeedsVector;
  typedef std::vector <int> SeedsThresholdsVector;
//ETX

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkHMVoxelGrow *New();

  // Description:
  // Macro required by VTK when extending its classes. 
  vtkTypeRevisionMacro(vtkHMVoxelGrow, vtkThreadedImageAlgorithm);

  // Description:
  // Sets the type of output for this filter. 
  int FillOutputPortInformation( int vtkNotUsed(port), vtkInformation* info);

  // Description:
  // Verifies any Image Data integrity by it's Extents. 
  bool VerifyImageData(vtkImageData *img);

  // Description:
  // Allocates the Image Data needed memory size and gets it disponible as an output.
  int RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **vtkNotUsed(inputVector), vtkInformationVector *outputVector);

  // Description:
  // Sets filter's output extent, spacing and dimensions from it's input.
  void SetInformationFromInput();

  // Description:
  // Sets filter's single Lower and Upper Threshold from its respective PV ThumbWheels.
  void SetThreshold(int LowerThreshold, int UpperThreshold);

  // Description:
  // Adds Lower and Upper Thresholds to their specific vectors, sharing a seed Id (seed and thresholds share the same stack index).
  void AddSeedThresholds(int LowerThreshold, int UpperThreshold);

  // Description:
  // Removes Lower and Upper Thresholds from their specific vectors by the Passed ID.
  void RemoveSeedThresholds(int PassedID);

  // Description:
  // Execute Voxel Grow.
  void ExecuteVoxelGrow();

  // Description:
  // Adds a seed to this filter.
  void AddSeed(double PassedX, double PassedY, double PassedZ, double PassedIntensity);

  // Description:
  // Remove a seed from PassedSeedID.
  void RemoveSeed(int PassedSeedID);

  // Description:
  // Sets the intensities analysis utilization mode (individual intensities or averaged).
  void SetIntensitiesAnalysisMode(int PassedIntensitiesAnalysisMode);

  // Description:
  // Sets the threshold utilization mode (unique or per seed).
  void SetThresholdsMode(int PassedThresholdsMode);

  // Description:
  // Enables / disables Island removal tool.
  void SetIslandDetectionEnabled(int Flag);

  // Description:
  // Set the Island removal detection factor.
  void SetIslandDetectionFactor(double PassedFactor);

  // Description:
  // Set the Radius of neighboors that will be analized for each seed.
  void SetNeighboorsAnalizableRadius(int PassedRadius);

  // Description:
  // Returns the Last Number of islands found by Voxel Grow.
  int GetLastNumberOfIslands();

protected:

  // Description:
  // Constructor.
  vtkHMVoxelGrow();

  // Description:
  // Destructor.
  ~vtkHMVoxelGrow();

  // Description:
  // Data array used to fill the Information tab.
  vtkUnsignedIntArray *InfoArray;
  
  // Description:
  // Increases progress with passed double.
  void IncreaseProgress(double Value);

  // Description:
  // Increases progress with passed double and displays a message.
  void IncreaseProgress(double Value, char* ProgressText);

  // Description:
  // Initialize the Output with 0 (zero) values).
  void InitializeOutput();

  // Description:
  // Verifies if the passed voxel is inside of the output extent.
  bool IsInOutExtent(int VoxelX, int VoxelY, int VoxelZ);

  // Description:
  // Filter Image Data output.
  vtkImageData *Output;

  // Description:
  // Filter Image Data input extents.
  int InExt[6];

  // Description:
  // Filter Image Data output extents.
  int OutExt[6];

  // Description:
  // Double 3 dimensions array that stores the Image Data dimensions information.
  double Spacing[3];
  
  // Description:
  // Integer 3 dimensions array that stores the Image Data dimensions information.
  int Dimensions[3];

  // Description:
  // Filter Upper Threshold variable.
  int UpperThreshold;

  // Description:
  // Filter Lower Threshold variable.
  int LowerThreshold;
  
  int IntensitiesAnalysisMode ;
  // Description:
  // Configures the threshold utilization mode (individual intensities or averaged).
  int ThresholdsMode;

  // Description:
  // Number of neighboors for Island Removal Analysis.
  int NumberOfNeighboors;

  // Description:
  // Stores the seed's Upper threshold values.
  SeedsThresholdsVector UpperThresholds;

  // Description:
  // Stores the seed's Lower threshold values.
  SeedsThresholdsVector LowerThresholds;
  
  // Description:
  // Enables the use of IslandDetection.
  int IslandDetectionEnabled;
  
  // Description:
  // Factor for Island removal.
  double IslandDetectionFactor;

  // Description:
  // Neighboors Radius to be analized on Island removal.
  int NeighboorsAnalizableRadius;

  // Description:
  // Flag that knows if the filter has already been executed or if any parameter has been modified.
  int ModifiedFlag;

  // Description:
  // Stores the Last number of islands found. Usefull for interface reporting Information.  
  int LastNumberOfIslands;

  // Description:
  // Vector that stores the seeds passed from the interface.  
  SeedsVector Seeds[4];
  
  // Description:
  // LookupTable that mappers the input to the 0 to 255 grayscale table.  
  vtkLookupTable *GrayscaleTable;

  // Description:
  // Mapper for DICOM Volumes, that uses this->GrayscaleTable as its vtkLookupTable.
  vtkImageMapToColors *DicomMapper;
};

#endif
