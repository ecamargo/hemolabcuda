/* Este programa teste necessita receber como
 * argumento um diretorio onde contenha os
 * arquivos Mesh.txt, BasParam.txt, Param.txt
 * e IniFile.txt
 */
 
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DMeshReader.h"
#include "vtkHM1DBasParamReader.h"
#include "vtkHM1DParamReader.h"
#include "vtkHM1DIniFileReader.h"
#include "vtkHMDataOutReader.h"

#include "vtkHM1DTerminal.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DTreeElement.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"

#include <iostream>
#include <string>
#include <list.h>
#include <vector.h>
#include <map.h>

using namespace std;

void show(vtkHM1DTreeElement *tree)
{
	vtkHM1DTreeElement *child;
	vtkHM1DTreeElement *elem;
		
	child = tree->GetFirstChild();
  	if (!child)
    	return;
    	
	cout << endl << "No: " << tree->GetId();
	
	cout << "\nFilhos: ";
	while(child)
	{
		cout << child->GetId() << ", ";
		child = tree->GetNextChild();
	}
	cout << endl;
	child = tree->GetFirstChild();
  	while (child)
    {	
    	  
    	show(child);
	    child = tree->GetNextChild();
	    
    }
}

int main(int argc, char *argv[] )
{
	if (argc <= 1)
	{
	  std::cerr << "Falta Parametros." << std::endl;
	  std::cerr << "[1] Diretorio dos arquivos" << std::endl;
	  return 0;
	}
	
	typedef std::map<vtkIdType,vtkHM1DTreeElement *>  TreeNodeDataMap;
	typedef std::pair<vtkIdType,vtkHM1DTreeElement *> TreeNodeDataPair;
	
	vtkHM1DStraightModel *straightModel = vtkHM1DStraightModel::New();
	
	vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::New();
	reader->SetFileName(argv[1]);
		
	vtkInformation *info;
	vtkInformationVector **infoVector;
	vtkInformationVector *outputVector;
	
	//lendo os arquivos
	//if ( !reader->ReadFile() )
	//	return 1;
	reader->Update();

	vtkHM1DMeshReader *mReader;  //declaracao do reader Mesh
	vtkHM1DBasParamReader *BPReader;  //declaracao do reader BasParam
	vtkHM1DParamReader *PReader;  //declaracao do reader Param
	vtkHM1DIniFileReader *IFReader;  //declaracao do reader IniFile

	mReader = reader->GetMeshReader();
	BPReader = reader->GetBasParamReader();
	PReader = reader->GetParamReader();
	//IFReader = reader->GetIniFileReader();
	
	vtkHM1DSegment *Segm;
	vtkHM1DTreeElement *elem;
		
	vtkHMNodeData *node = new vtkHMNodeData();
	
	vector <list <int> >vet;
	vet = mReader->GetGroupElements();
	vector <list <int> >::iterator vetIt = vet.begin();
	
	list <int> lst, lst2;
	list <int>::iterator it, it2;
	
	int *pti = mReader->GetPointsPerFace();
	int numOfGroupElements = mReader->GetNumberOfElementGroups();
	int *elementType = mReader->GetElementType();
	
	Segm = NULL;
	int numElements = 0;
	vtkHM1DTerminal *root = NULL;
	
	int *realParameters = PReader->GetQuantityOfRealParameters(); //Quantidade de parametros reais do arquivo Param.txt
	list <double> realParamList = PReader->GetRealParameters();  //Lista com os parametros reais do arquivo Param.txt
	list <double>::iterator realIt = realParamList.begin();
	
	TreeNodeDataMap segmentos;
	TreeNodeDataMap::iterator segmIt, segmIt2;	
	
	for ( int i=0; i<numOfGroupElements; i++)
	{
		//cout << "ElementType - " << elementType[i] << " i - " << i << endl;
		//Se tipo de elemento e igual a terminal
		if(elementType[i] == 2)
		{
			//se raiz ainda nao exixte
			if(!root)
			{
				//cria terminal raiz (coracao)
				root = (vtkHM1DTerminal *)straightModel->Get1DTreeRoot();
				straightModel->AddSegment(root->GetId());
				//cout << "-----------No raiz criado - Id = " << root->GetId() << endl;
				elem = root->GetFirstChild();
				Segm = straightModel->GetSegment(elem->GetId());
				//cout << "-----------Filho = " << elem->GetId() << endl;
				
			}
			else
			{
				int n = pti[i]-2;			
						
				for (int j=0; j<n; j++)
					straightModel->AddSegment(elem->GetId());
				
				segmentos = straightModel->GetSegmentsMap();
				
				segmIt = segmIt2 = segmentos.find(elem->GetId());
				segmIt2++;
				if(segmIt2 == segmentos.end())
				{
					elem = (*segmIt).second;
					Segm = straightModel->GetSegment(elem->GetId());
				}
				else
				{
					elem = (*segmIt2).second;
					Segm = straightModel->GetSegment(elem->GetId());
				}
			}
			if((elementType[i+1] == 1) && (elementType[i+2] != 3))
			{
				int j=i;
				while( elementType[j] != 4)
				{
					j++;
				}
				i=j+1;
			}
			
		} //fim if (terminal)
		//se tipo de elemento e igual a inicio de segmento
		else if((elementType[i] == 1) && (elementType[i+1] == 3))
		{
			//se o tipo de elemento anterior ao no
			//de entrada nao e um terminal
			if(elementType[i-1] != 2)
			{	
				lst2 = vet.at(i-1);
				it2 = lst2.begin();
				it2++;
				//cout << "it2 - " << *it2 << endl;
				lst = vet.at(i);
				
				int j=i, z=i;			
				
				while(*lst.begin() != *it2)
				{
					lst = vet.at(j);
					j++;
				}
				j--;
				int k=j;
				while(elementType[k] != 4)
				{
					if ((elementType[k] == 1) && (elementType[k+1] == 4))
					{			
						//cout << "Setando propriedades do no de saida do segmento " <<
						//elem->GetId() << endl;
						numElements++;
						Segm->SetNumberOfElements(numElements);
						
						vtkHMNodeData *node = new vtkHMNodeData();
						double *point;
						//vector <list <int> >vetor;
						list <int> l;
						list <int>::iterator itv;
						l = vet.at(k);
						itv = l.begin();
						//cout << "itv - " << *itv << endl;
						point = mReader->GetPoints1D(*itv-1);
						node->coords[0] = *point;
						node->coords[1] = *++point;
						Segm->SetNodeData(numElements-1, *node);
						
						numElements++;
						Segm->SetNumberOfElements(numElements);
						
						itv++;
						//cout << "itv - " << *itv << endl;
						point = mReader->GetPoints1D(*itv-1);
						node->coords[0] = *point;
						node->coords[1] = *++point;
						Segm->SetNodeData(numElements-1, *node);
			
						
						cout << "Numero de elementos = " << Segm->GetNumberOfElements() << endl;
						numElements = 0;
						cout << "*********Fim do segmento continuado*********  " << elem->GetId() << endl;
					}
					//If type of element is a element intermediate in segment
					else if(elementType[k] == 1)
					{
						
						list <int> l, l2;
						list <int>::iterator itv, itv2;
						l = vet.at(k);						
						l2 = vet.at(z-1);
						
						itv = l.begin();
						//cout << "itv - " << *itv << " | l2.end - " << *--l2.end() << endl;
						if(*itv == *--l2.end())
						{
							
							numElements++;
							Segm->SetNumberOfElements(numElements);
							vtkHMNodeData *node = new vtkHMNodeData();
							double *point;
							//vector <list <int> >vetor;
							//list <int> l;
							//list <int>::iterator itv;
							//l = vet.at(k);
							//itv = l.begin();
							
							point = mReader->GetPoints1D(*itv-1);
							node->coords[0] = *point;
							node->coords[1] = *++point;
							Segm->SetNodeData(numElements-1, *node);
													
							z=k+1;
							//cout << "Setando propriedades do segmento " << elem->GetId()
							//<< " no - " << numElements << endl;
						}
						else
						{
							//lst2 = vet.at(k-1);
							it2 = --l2.end();
							//cout << "it2 - " << *it2 << endl;
							//it2++;
							//cout << "l begin - " << *l.begin() << endl;
							//lst = vet.at(i);
							
							j=k;				
							
							while(*l.begin() != *it2)
							{
								l = vet.at(j);
								//cout << "l begin - " << *l.begin() << endl;
								j++;
							}
							j--;
							k=j-1;
							
						}
						
					}
					k++;
					
				}
				int n = pti[k+1]-2;
				for (int j=0; j<n; j++)
					straightModel->AddSegment(elem->GetId());
				
				segmentos = straightModel->GetSegmentsMap();
				segmIt = segmIt2 = segmentos.find(elem->GetId());
				segmIt2++;
				if(segmIt2 == segmentos.end())
				{
					elem = (*segmIt).second;
					Segm = straightModel->GetSegment(elem->GetId());
				}
				else
				{
					elem = (*segmIt2).second;
					Segm = straightModel->GetSegment(elem->GetId());
				}
			}
			cout << "\n*********Inicio do segmento*********" << endl;
			//cout << "Setando propriedades do no de entrada ... " << endl;
						
			numElements++;
			Segm->SetNumberOfElements(numElements);
			vtkHMNodeData *node = new vtkHMNodeData();
			double *point;
			vector <list <int> >vetor;
			list <int> l;
			list <int>::iterator itv;
			l = vet.at(i);
			itv = l.begin();
			
			point = mReader->GetPoints1D(*itv-1);
			node->coords[0] = *point;
			node->coords[1] = *++point;
			Segm->SetNodeData(numElements, *node);
			
		}  //fim if (inicio de segmento)
		//se tipo de elemento e igual a fim de segmento	
		else if ((elementType[i] == 1) && (elementType[i+1] == 4))  
		{			
			//cout << "Setando propriedades do no de saida do segmento " <<
			//			Segm->GetId() << endl;
			numElements++;
			Segm->SetNumberOfElements(numElements);
			cout << "Numero de elementos = " << Segm->GetNumberOfElements() << endl;
			
			vtkHMNodeData *node = new vtkHMNodeData();
			double *point;
			vector <list <int> >vetor;
			list <int> l;
			list <int>::iterator itv;
			l = vet.at(i);
			itv = l.begin();
			
			point = mReader->GetPoints1D(*itv-1);
			node->coords[0] = *point;
			node->coords[1] = *++point;
			Segm->SetNodeData(numElements, *node);
			numElements = 0;
			cout << "*********Fim do segmento*********   " << Segm->GetId() << endl;
		}  //fim if (fim de segmento)
		//se tipo de elemento e igual a elemento intermediario no segmento
		else if(elementType[i] == 1)
		{
			if(elementType[i-1] != 2)
			{
				int j=i, z=i;
				
				list <int> l, l2;
				list <int>::iterator itv, itv2;
				l = vet.at(j);						
				l2 = vet.at(z-1);
				
				itv = l.begin();
				//cout << "itv - " << *itv << " | l2.end - " << *--l2.end() << endl;
				if(*itv == *--l2.end())
				{
					
					numElements++;
					Segm->SetNumberOfElements(numElements);
					vtkHMNodeData *node = new vtkHMNodeData();
					double *point;
					
					point = mReader->GetPoints1D(*itv-1);
					node->coords[0] = *point;
					node->coords[1] = *++point;
					Segm->SetNodeData(numElements-1, *node);
											
					//cout << "Setando propriedades do segmento " << elem->GetId()
					//<< " no - " << numElements << endl;
				}
				else
				{
					//lst2 = vet.at(k-1);
					it2 = --l2.end();
					//cout << "it2 - " << *it2 << endl;
					//it2++;
					//cout << "l begin - " << *l.begin() << endl;
					//lst = vet.at(i);
					
					//j=k;				
					
					while(*l.begin() != *it2)
					{
						l = vet.at(j);
						//cout << "l begin - " << *l.begin() << endl;
						j++;
					}
					j-=1;
					i=j;
					
					numElements++;
					Segm->SetNumberOfElements(numElements);
					vtkHMNodeData *node = new vtkHMNodeData();
					double *point;
					
					point = mReader->GetPoints1D(*itv-1);
					node->coords[0] = *point;
					node->coords[1] = *++point;
					Segm->SetNodeData(numElements-1, *node);
											
					//cout << "Setando propriedades do segmento " << elem->GetId()
					//<< " no - " << numElements << endl;
				}
			}
			else
			{
				int j=i;
				while( elementType[j] != 4)
				{
					j++;
				}
				i=j+1;
			}
		}  //fim if (elemento intermediario)
		
	}
	
	
	//show(straightModel->Get1DTreeRoot());
	
	//cout << *straightModel << endl;
	
	//reader->Delete();
	//straightModel->ClearTree();
	//straightModel->Delete();
	
	return 0;
}
