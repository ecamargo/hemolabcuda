#include "vtkHM1DStraightModel.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DMeshReader.h"
#include "vtkHM1DBasParamReader.h"
#include "vtkHM1DParamReader.h"
#include "vtkHM1DIniFileReader.h"
#include "vtkHMDataOutReader.h"

#include "vtkHM1DTerminal.h"
#include "vtkHM1DSegment.h"

#include "vtkHM1DTreeElement.h"

#include "vtkPolyDataMapper.h"
#include "vtkPolyData.h"
#include "vtkActor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkHM1DStraightModelSource.h"
#include "vtkPolyData.h"

#include "vtkDataSetMapper.h"

int main(int argc, char *argv[] )
{
	if (argc <= 1)
	{
	  std::cerr << "Falta Parametros." << std::endl;
	  std::cerr << "[1] Diretorio dos arquivos" << std::endl;
	  return 0;
	}
	
	vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::New();
	reader->SetFileName(argv[1]);
	
	reader->Update();
	
	vtkPolyData *poly = (vtkPolyData*)reader->GetOutput();
		
	vtkPolyDataMapper *map = vtkPolyDataMapper::New();
	map->SetInput(poly);
	
	// actor coordinates geometry, properties, transformation
	vtkActor *ahmTree = vtkActor::New();
	ahmTree->SetMapper(map);
	ahmTree->GetProperty()->SetColor(0,0,1); // hmTree color blue
	
	// a renderer and render window
	vtkRenderer *ren1 = vtkRenderer::New();
	vtkRenderWindow *renWin = vtkRenderWindow::New();
	renWin->AddRenderer(ren1);
	
	// an interactor
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
	iren->SetRenderWindow(renWin);
	
	// add the actor to the scene
	ren1->AddActor(ahmTree);
	ren1->SetBackground(1,1,1); // Background color white
	
	// render an image (lights and cameras are created automatically)
	renWin->Render();
		
	// begin mouse interaction
	iren->Start();
		
	reader->Delete();
	ahmTree->Delete();  
	iren->Delete();
	ren1->Delete();
	renWin->Delete();
	map->Delete();	
	
    return 0;
}
