/* Este programa teste necessita receber como
 * argumento um diretorio onde contenha os
 * arquivos Mesh.txt, BasParam.txt, Param.txt
 * e IniFile.txt
 */

#include "vtkHM1DStraightModelReader.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkDoubleArray.h"

#include <iostream>
#include <stdlib.h>
#include <string>
#include <list.h>
#include <vector.h>

using namespace std;

int main(int argc, char *argv[] )
{
	if (argc <= 1)
	{
	  std::cerr << "Falta Parametros." << std::endl;
	  std::cerr << "[1] Diretorio dos arquivos" << std::endl;
	  return 0;
	}
	
	string *str;
	double *ptr;
	int *ptr2;
	int max;
	
	vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::New();
	reader->SetFileName(argv[1]);
	
	vtkHM1DMeshReader *mReader;
	
	reader->Update();	
		
	mReader = reader->GetMeshReader();
	
	cout << "===========Arquivo Mesh.txt===========" << endl;
	cout << "File Mesh = " << mReader->GetFileName() << endl;
	cout << "Graus de Liberdade 	= " << mReader->GetDegreeOfLiberty() << endl;
	cout << "Dimensoes 				= " << mReader->GetDimension() << endl;
	cout << "Numero de coordenadas 	= " << mReader->GetNumberOfPoints() << endl;
	cout << "Coordenadas" << endl;
	for (int i=0; i<mReader->GetNumberOfPoints(); i++)
	{
		ptr = mReader->GetPoints1D(i);
		cout << *ptr;
		cout << " " << *++ptr;
		cout << " " << *++ptr << endl;
	}
	
	cout << "Numero de Faces " << mReader->GetNumberOfElementGroups() << endl;
	cout << "Faces" << endl;
	
	int n;
	int *pointsPerFace = mReader->GetPointsPerFace();
	vector <list <int> >vet;
	list <int> lst;
	list <int>::iterator it;
	
	vet = mReader->GetGroupElements();
	vector <list <int> >::iterator vetIt = vet.begin();
	
	
	
	max = mReader->GetNumberOfElementGroups();
	for (int i=0; i<max; i++)
	{		
		cout << pointsPerFace[i];
		lst = vet.at(i);
		it = lst.begin();
		for (int j=0; j<lst.size(); j++)
		{
			cout << " " << *it++;
		}
		cout << endl;
	}	
	
	cout << "Element Type" << endl;
	ptr2 = mReader->GetElementType();
	for (int i=0; i<max; i++)
	{
		cout << *ptr2 << endl;
		ptr2++;
	}
	
	cout << "Element Mat" << endl;
	ptr2 = mReader->GetElementMat();
	for (int i=0; i<max; i++)
	{
		cout << *ptr2 << endl;
		ptr2++;
	}
	
	cout << "Dirichlets Conditions Tag" << endl;
	ptr2 = mReader->GetDirichletsConditionsTag();
	max = mReader->GetNumberOfPoints()*mReader->GetDegreeOfLiberty();
	for (int i=0; i<mReader->GetNumberOfPoints(); i++)
	{
		for( int j=0; j<mReader->GetDegreeOfLiberty(); j++)
		{
			cout << *ptr2 << " ";
			ptr2++;
		}
		cout << endl;
	}
	
	cout << "Dirichlets Conditions" << endl;
	ptr = mReader->GetDirichletsConditions();
	for (int i=0; i<mReader->GetNumberOfPoints(); i++)
	{
		for( int j=0; j<mReader->GetDegreeOfLiberty(); j++)
		{	
			cout << *ptr << " ";
			ptr++;
		}
		cout << endl;
	}
	
	vtkHM1DBasParamReader *BPReader;
	BPReader = reader->GetBasParamReader();
	
	max = mReader->GetDegreeOfLiberty();
	
	cout << "\n========Arquivo BasParam.txt==========" << endl;
	cout << "File BasParam = " << BPReader->GetFileName() << endl;
	cout << "Renumbering - " << BPReader->GetRenumbering() << endl;
	cout << "StepContinuationControl" << endl;
	cout << BPReader->GetConfigProcessExternal() << " " << BPReader->GetMaxIteraction() << 
			" " << BPReader->GetRalaxacao() << " " << BPReader->GetQuantityStep() << endl;

	ptr = BPReader->GetNorma();
	for ( int i=0; i<max; i++)
		cout << *ptr++ << " ";	
	
	cout << endl;
	ptr = BPReader->GetTolerancia();
	for ( int i=0; i<max; i++)		
		cout << *ptr++ << " ";
	
	cout << endl;
	str = BPReader->GetNameDegreeOfLiberty();	
	for ( int i=0; i<max; i++)
		cout << str[i] << endl;
	
	ptr2 = BPReader->GetNums();
	for ( int i=0; i<3; i++)
	{
		cout << *ptr2 << " ";
		ptr2++;
	}
	
	cout << endl << "TimeStep:" << endl;
	cout << BPReader->GetDelt() << " " << BPReader->GetTini() << " " <<
			BPReader->GetTmax() << endl;
	
	cout << "Output Control:" << endl;
	cout << BPReader->GetTimeQuantityScreen() << " " << BPReader->GetTimeQuantityFile() << endl;
	
	cout << "Node Output Control:" << endl;
	cout << BPReader->GetWriterOutput() << endl;
	if(BPReader->GetWriterOutput() > 0)
	{
		ptr2 = BPReader->GetOutputList();
		for ( int i=0; i<BPReader->GetWriterOutput(); i++)
			cout << *ptr2++ << " ";
		cout << endl;
	}
	
	cout << "ElementLibraryControl:" << endl;
	cout << BPReader->GetQuantityOfDifferentElements() << " " << BPReader->GetMaximumQuantityOfParameters() << endl;
	
	cout << "SubStep:" << endl;
	cout << BPReader->GetResultantMatrixIsSymmetrical() << " " << BPReader->GetCalculationStepIsLinear();
	cout << " " << BPReader->GetMaxIteractionSubStep() << " " << BPReader->GetRalaxacaoSubStep() <<
			" " << BPReader->GetResolveLibrary() << endl;	
	
	if (BPReader->GetResolveLibrary())
		cout << BPReader->GetMaxIteractionOfResolver() << " " << BPReader->GetToleranceOfConvergence() << 
				BPReader->GetSpacesOfKrylov() << " " << BPReader->GetNumberOfCoefficients() << " " <<
				BPReader->GetToleranceOfExcuse() << endl;
	
	ptr2 = BPReader->GetElementType();
	for ( int i=0; i<BPReader->GetQuantityOfDifferentElements(); i++ )
	{
		for (int j=0; j<5; j++)
		{
			cout << *ptr2 << " ";
			ptr2++;
		}
		cout << endl;
	}
	
	ptr = BPReader->GetParamElementType();
	for ( int i=0; i<BPReader->GetQuantityOfDifferentElements(); i++ )
	{
		for (int j=0; j<BPReader->GetMaximumQuantityOfParameters() ; j++)
		{
			cout << *ptr << " ";
			ptr++;
		}
		cout << endl;
	}
	
	max = mReader->GetDegreeOfLiberty();
	
	ptr = BPReader->GetNormaSubStep();
	for ( int i=0; i<max; i++)
		cout << *ptr++ << " ";	
	
	cout << endl;
	ptr = BPReader->GetToleranciaSubStep();
	for ( int i=0; i<max; i++)		
		cout << *ptr++ << " ";
	
	cout << endl;
	str = BPReader->GetNameDegreeOfLibertySubStep();	
	for ( int i=0; i<max; i++)
		cout << str[i] << endl;
	
	ptr2 = BPReader->GetNumsSubStep();
	for ( int i=0; i<3; i++)
	{
		cout << *ptr2 << " ";
		ptr2++;
	}
	
	vtkHM1DParamReader *PReader;
	PReader = reader->GetParamReader();
	list <int> lsti;
	list <double> lstd;
	
	cout << endl << "\n========Arquivo Param.txt==========" << endl;
	cout << "File Param = " << PReader->GetFileName() << endl;
	cout << "Parameter Groups - " << PReader->GetNumberOfGroups() << endl;
	ptr2 = PReader->GetQuantityOfRealParameters();
	cout << "Quantidade de parametros reais:" << endl;
	int soma=0;
	for ( int i=0; i<PReader->GetNumberOfGroups(); i++ )
	{
		cout << *ptr2 << " ";
		soma += *ptr2;
		ptr2++;
	}
	cout << endl << "Quantidade de parametros reais total - " << soma << endl;
	
	cout << "Parametros reais" << endl;
	lstd = PReader->GetRealParameters();
	ptr2 = PReader->GetQuantityOfRealParameters();
	
	list <double>::iterator d = lstd.begin();
	while(d != lstd.end())
	{
		cout << *d << " ";
		d++;
	}
	
	
	ptr2 = PReader->GetQuantityOfIntegerParameters();
	cout << endl << "Quantidade de parametros inteiros:" << endl;
	soma=0;
	for ( int i=0; i<PReader->GetNumberOfGroups(); i++ )
	{
		cout << *ptr2 << " ";
		soma += *ptr2;
		ptr2++;
	}
	cout << endl << "Quantidade de parametros inteiros total - " << soma << endl;
	
	cout << "Parametros inteiros" << endl;
	lsti = PReader->GetIntegerParameters();
	list <int>::iterator i = lsti.begin();
	while(i != lsti.end())
	{
		cout << *i << " ";
		i++;
	}
	
	cout <<endl;
	
	vtkHM1DIniFileReader *IFReader;
	IFReader = reader->GetIniFileReader();
	cout << endl << "============Arquivo IniFile.txt==========" << endl;
	cout << "File IniFile = " << IFReader->GetFileName() << endl;
	ptr = IFReader->GetInitialConditions();
	cout << "Initial Conditioins" << endl;
	for( int i=0; i<mReader->GetNumberOfPoints(); i++ )
	{
		for( int j=0; j<mReader->GetDegreeOfLiberty(); j++ )
		{
			cout << *ptr << " ";
			ptr++;
		}
		cout <<endl;
	}
	
	vtkHMDataOutReader *DOReader;
	DOReader = reader->GetDataOutReader();
	cout << endl << "============Arquivo DataOut.txt==========" << endl;
	cout << "File DataOut = " << DOReader->GetFileName() << endl;
	
	vtkDoubleArray * array = DOReader->GetDegreeOfLibertyArray();
	
	double *results = new double[mReader->GetDegreeOfLiberty()];
	int *timeStep = DOReader->GetTimeStep();
	double *instantOfTime = DOReader->GetInstantOfTime();
	double *timeStepUsed = DOReader->GetTimeStepUsed();
	
	int sum=0;
	
	for ( int k=0; k<DOReader->GetNumberOfBlock(); k++ )
	{
		cout << endl << timeStep[k] << "\t" << instantOfTime[k] << "\t" << timeStepUsed[k];;
		for ( int i=0; i<mReader->GetNumberOfPoints(); i++ )
		{
			cout << "\nPonto " << i << " - " ;
			results = array->GetTuple(sum+i);
			for ( int j=0; j<mReader->GetDegreeOfLiberty(); j++ )
			{
				cout << results[j] << "\t";
			}
		}
		sum += mReader->GetNumberOfPoints();
	}
	
	cout << endl;
	reader->Delete();
	
	return 0;
}
