/*
 * $Id: vtkHM1DBasParamReader.cxx 2883 2010-04-26 18:58:23Z igor $
 */
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DBasParamReader.h"
#include "vtkHM1DMeshReader.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DMesh.h"
 
#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>
#include <string>

#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHM1DBasParamReader, "$Revision: 2883 $");
vtkStandardNewMacro(vtkHM1DBasParamReader);

vtkHM1DBasParamReader::vtkHM1DBasParamReader()
{
	vtkDebugMacro(<<"Reading BasParam data...");
	this->BasParam = vtkHM1DBasParam::New();
}

vtkHM1DBasParamReader::~vtkHM1DBasParamReader()
{
	this->BasParam->Delete();
}

void vtkHM1DBasParamReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
//Open File
int vtkHM1DBasParamReader::OpenFile()
{
	if (this->ReadFromInputString)
		{
		if (this->InputArray)
			{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0),
					this->InputArray->GetNumberOfTuples()*
					this->InputArray->GetNumberOfComponents());
			return 1;
			} else if (this->InputString)
			{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
			}
		} else
		{
		vtkDebugMacro(<< "Opening BasParam file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
			{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode(vtkErrorCode::NoFileNameError);
			return 0;
			}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0)
			{
			//			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
			return 0;
			}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
			return 0;
			}

		return 1;
		}

	return 0;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DBasParamReader::CloseFile()
{
	vtkDebugMacro(<<"Closing BasParam file");
	if (this->IS != NULL)
		{
		delete this->IS;
		}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
// Find and Read Tags in file.
int vtkHM1DBasParamReader::FindReadTags(vtkHM1DMesh *mesh)
{
	char line[256];
	int i = 1;

	for (int z = 0; z < 256; z++)
		line[z] = 0;

	while ( !this->IS->eof() )
		{
		if (!this->ReadLine(line))
			{
			if (this->IS->eof())
				return 1;
			vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
			return 0;
			}

		if (strncmp("*Param Write Swicht", line, 19) == 0)
			{
      if ( !this->ReadParamWriteSwicht() )
        {
        vtkErrorMacro(<<"Can't read ParamWriteSwicht.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*Renumbering", line, 12) == 0)
			{
      if ( !this->ReadHeader() )
        {
        vtkErrorMacro(<<"Can't read Renumbering.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*StepContinuationControl", line, 24) == 0)
			{
      if ( !this->ReadStepContinuationControl(mesh) )
        {
        vtkErrorMacro(<<"Can't read StepContinuationControl.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*TimeStep", line, 9) == 0)
			{
      if ( !this->ReadTimeStep() )
        {
        vtkErrorMacro(<<"Can't read TimeStep.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*OutputControl", line, 14) == 0)
			{
      if ( !this->ReadOutputControl() )
        {
        vtkErrorMacro(<<"Can't read OutputControl.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*NodeOutputControl", line, 18) == 0)
			{
      if ( !this->ReadNodeOutputControl() )
        {
        vtkErrorMacro(<<"Can't read NodeOutputControl.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*ElementLibraryControl", line, 22) == 0)
			{
      if ( !this->ReadElementLibraryControl() )
        {
        vtkErrorMacro(<<"Can't read ElementLibraryControl.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*SubStep", line, 8) == 0)
			{
      if ( !this->ReadSubStep(mesh) )
        {
        vtkErrorMacro(<<"Can't read SubStep.");
        this->CloseFile();
        return 0;
        }
			} else if (strncmp("*Initial Time", line, 13) == 0)
			{
      if ( !this->ReadInitialTime() )
        {
        vtkErrorMacro(<<"Can't read InitialTime.");
        this->CloseFile();
        return 0;
        }
			}
		//											else
		//												{
		//												if( strncmp("*LogFile", line, 8) == 0 )												
		//												}

		}

	return 1;
}

//----------------------------------------------------------------------------
//Read Param Write Swicht
int vtkHM1DBasParamReader::ReadParamWriteSwicht()
{
	vtkDebugMacro(<< "Reading BasParam file Param Write Swicht");
	//
	// read Param Write Swicht
	//

	int paramWrite;
	if (!this->Read(&paramWrite))
		{
		vtkErrorMacro(<<"Can't read Renumbering!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetParamWriteSwicht(paramWrite);

	return 1;
}

//----------------------------------------------------------------------------
// Read Initial Time tag in file.
int vtkHM1DBasParamReader::ReadInitialTime()
{
	vtkDebugMacro(<< "Reading BasParam file Initial Time");

	int iniTime;
	if (!this->Read(&iniTime))
		{
		vtkErrorMacro(<<"Can't read Initial Time!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetInitialTime(iniTime);
	return 1;
}

//----------------------------------------------------------------------------
//Read BasParam File
int vtkHM1DBasParamReader::ReadBasParamFile(char *file, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<< "Reading BasParam file");

	this->SetFileName(file);

	if ( !this->OpenFile())
		{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
		return 0;
		}

	// Find and read the tags in file
	if ( !this->FindReadTags(mesh) )
		{
		vtkErrorMacro(<<"Can't read file. " << this->GetFileName());
		return 0;
		}

	//	if ( !this->OpenFile() || !this->ReadHeader() )
	//		{
	//		//		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
	//		return 0;
	//		}
	//
	//	if ( !this->ReadStepContinuationControl(mesh) )
	//		{
	//		vtkErrorMacro(<<"Can't read StepContinuationControl.");
	//		this->CloseFile();
	//		return 0;
	//		}
	//
	//	if ( !this->ReadTimeStep() )
	//		{
	//		vtkErrorMacro(<<"Can't read TimeStep.");
	//		this->CloseFile();
	//		return 0;
	//		}
	//
	//	if ( !this->ReadOutputControl() )
	//		{
	//		vtkErrorMacro(<<"Can't read OutputControl.");
	//		this->CloseFile();
	//		return 0;
	//		}
	//
	//	if ( !this->ReadNodeOutputControl() )
	//		{
	//		vtkErrorMacro(<<"Can't read NodeOutputControl.");
	//		this->CloseFile();
	//		return 0;
	//		}
	//
	//	if ( !this->ReadElementLibraryControl() )
	//		{
	//		vtkErrorMacro(<<"Can't read ElementLibraryControl.");
	//		this->CloseFile();
	//		return 0;
	//		}
	//
	//	if ( !this->ReadSubStep(mesh) )
	//		{
	//		vtkErrorMacro(<<"Can't read SubStep.");
	//		this->CloseFile();
	//		return 0;
	//		}

	this->CloseFile();

	return 1;
}

//----------------------------------------------------------------------------
//Read Header
int vtkHM1DBasParamReader::ReadHeader()
{
	vtkDebugMacro(<< "Reading BasParam file header");
	//
	// read header
	//

	int ren;
	if (!this->Read(&ren))
		{
		vtkErrorMacro(<<"Can't read Renumbering!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetRenumbering(ren);

	return 1;

	//	char line[256];
	//
	//	for (int z = 0; z < 256; z++)
	//		line[z] = 0;
	//
	//	vtkDebugMacro(<< "Reading BasParam file header");
	//	//
	//	// read header
	//	//
	//
	//	if (!this->ReadLine(line))
	//	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}

	//====================== CÓDIGO ANTIGO ======================================

	//	if ( strncmp ("*Renumbering", line, 20) )
	//	{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
	//		                  << (this->FileName?this->FileName:"(Null FileName)"));
	//		    
	//		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
	//		return 0;
	//	}
	//	int ren;
	//	if(!this->Read(&ren))
	//	{
	//		vtkErrorMacro(<<"Can't read Renumbering!" << " for file: " 
	//						<< (this->FileName?this->FileName:"(Null FileName)"));
	//		return 0;
	//	}
	//	this->BasParam->SetRenumbering(ren);

	//==============================================================================	

	//====== ADAPTAÇÃO PARA SUPORTAR ARQUIVOS DO 3D ================================ 	
	//	if ( !strncmp("*Param Write Swicht", line, 19) )
	//		{
	//		int paramWrite;
	//		if (!this->Read(&paramWrite))
	//			{
	//			vtkErrorMacro(<<"Can't read Param Write Swicht!" << " for file: "
	//					<< (this->FileName?this->FileName:"(Null FileName)"));
	//			return 0;
	//			}
	//		this->BasParam->SetParamWriteSwicht(paramWrite);
	//
	//		if (!this->ReadString(line))
	//			{
	//			vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: "
	//					<< (this->FileName?this->FileName:"(Null FileName)"));
	//			this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//			return 0;
	//			}
	//
	//		if (strncmp("*Renumbering", line, 12) )
	//			{
	//			vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//					<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//			this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//			return 0;
	//			}
	//		int ren;
	//		if (!this->Read(&ren))
	//			{
	//			vtkErrorMacro(<<"Can't read Renumbering!" << " for file: "
	//					<< (this->FileName?this->FileName:"(Null FileName)"));
	//			return 0;
	//			}
	//		this->BasParam->SetRenumbering(ren);
	//		} else
	//		{
	//		if ( !strncmp("*Renumbering", line, 12) )
	//			{
	//			int ren;
	//			if (!this->Read(&ren))
	//				{
	//				vtkErrorMacro(<<"Can't read Renumbering!" << " for file: "
	//						<< (this->FileName?this->FileName:"(Null FileName)"));
	//				return 0;
	//				}
	//			this->BasParam->SetRenumbering(ren);
	//			} else
	//			{
	//			vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//					<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//			this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//			return 0;
	//			}
	//		}
	//	//==============================================================================
	//
	//	return 1;
}

//----------------------------------------------------------------------------
//Read Step Continuation Control
int vtkHM1DBasParamReader::ReadStepContinuationControl(vtkHM1DMesh *mesh)
{
	char line[256];

	vtkDebugMacro(<< "Reading StepContinuationControl");

	//	this->ReadLine(line);
	//
	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading StepContinuationControl line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*StepContinuationControl", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Can't read option of more exterm loop!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	if (strcmp("T", line)==0)
		this->BasParam->SetConfigProcessExternal(true);
	else
		this->BasParam->SetConfigProcessExternal(false);

	int max;
	if (!this->Read(&max))
		{
		vtkErrorMacro(<<"Can't read maximum number of iterations!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetMaxIteration(max);

	double ral;
	if (!this->Read(&ral))
		{
		vtkErrorMacro(<<"Can't read the parameter of ralaxação!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetRalaxacao(ral);

	int quan;
	if (!this->Read(&quan))
		{
		vtkErrorMacro(<<"Can't read the quantity of time steps!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetQuantityStep(quan);

	double *norma = new double[mesh->GetDegreeOfFreedom()];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->Read(&norma[i]))
			{
			vtkErrorMacro(<<"Can't read the vector Norma!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		}
	this->BasParam->SetNorma(norma);

	double *tolerance = new double[mesh->GetDegreeOfFreedom()];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->Read(&tolerance[i]))
			{
			vtkErrorMacro(<<"Can't read the vector Tolerance!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}
	this->BasParam->SetTolerance(tolerance);

	string *nameDegreeOfFreedom = new string[mesh->GetDegreeOfFreedom()];
	char str[50];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->ReadString(str))
			{
			vtkErrorMacro(<<"Can't read the names of the degree of liberty!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		nameDegreeOfFreedom[i] = str;
		}
	this->BasParam->SetNameDegreeOfFreedom(nameDegreeOfFreedom);

	int nums[3];
	for (int i=0; i<3; i++)
		{
		if (!this->Read(&nums[i]))
			{
			vtkErrorMacro(<<"Can't read the vector of nums!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}

	return 1;
}

//----------------------------------------------------------------------------
//Read Time Step
int vtkHM1DBasParamReader::ReadTimeStep()
{
	char line[256];

	vtkDebugMacro(<< "Reading TimeStep");

	//	this->ReadLine(line);

	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading TimeStep line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*TimeStep", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}
	//	this->ReadLine(line);

	double delt;
	if (!this->Read(&delt))
		{
		vtkErrorMacro(<<"Can't read time step!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetDelT(delt);

	double tini;
	if (!this->Read(&tini))
		{
		vtkErrorMacro(<<"Can't read time of begin of the round!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetTini(tini);

	double tmax;
	if (!this->Read(&tmax))
		{
		vtkErrorMacro(<<"Can't read time of finishing of the round!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetTmax(tmax);

	return 1;
}

//----------------------------------------------------------------------------
//Read Output Control
int vtkHM1DBasParamReader::ReadOutputControl()
{
	char line[256];

	vtkDebugMacro(<< "Reading Output Control");

	//	this->ReadLine(line);
	//
	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading OutputControl line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*OutputControl", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}

	int timeScreen;
	if (!this->Read(&timeScreen))
		{
		vtkErrorMacro(<<"Can't read steps of time between exits in the screen!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetTimeQuantityScreen(timeScreen);

	int timeFile;
	if (!this->Read(&timeFile))
		{
		vtkErrorMacro(<<"Can't read steps of time between exits in the file!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetTimeQuantityFile(timeFile);

	return 1;
}

//----------------------------------------------------------------------------
//Read Node Output Control
int vtkHM1DBasParamReader::ReadNodeOutputControl()
{
	char line[256];

	vtkDebugMacro(<< "Reading Node Output Control");

	//	this->ReadLine(line);
	//
	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading NodeOutputControl line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*NodeOutputControl", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}
	//
	//	this->ReadLine(line);

	int write;
	if (!this->Read(&write))
		{
		vtkErrorMacro(<<"Can't read option for which node goes to be carried through the writing of the results!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetWriterOutput(write);

	if (write > 0)
		{
		int *outputList = new int[write];
		for (int i=0; i<write; i++)
			{
			if (!this->Read(&outputList[i]))
				{
				vtkErrorMacro(<<"Can't read list of the nodes that is asked for in the exit of the program !" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}
		this->BasParam->SetOutputList(outputList);
		}

	return 1;
}

//----------------------------------------------------------------------------
//Read Element Library Control
int vtkHM1DBasParamReader::ReadElementLibraryControl()
{
	char line[256];

	vtkDebugMacro(<< "Reading Element Library Control");

	//	this->ReadLine(line);
	//
	//	if (!this->ReadString(line))
	//		{
	//		vtkErrorMacro(<<"Premature EOF reading ElementLibraryControl line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*ElementLibraryControl", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}
	//
	//	this->ReadLine(line);

	int qtd;
	if (!this->Read(&qtd))
		{
		vtkErrorMacro(<<"Can't read the quantity of elements!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetQuantityOfDifferentElements(qtd);

	int max;
	if (!this->Read(&max))
		{
		vtkErrorMacro(<<"Can't read the maximum quantity of parameters of the elements!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetMaximumQuantityOfParameters(max);

	return 1;
}

//----------------------------------------------------------------------------
//Read Sub Step
int vtkHM1DBasParamReader::ReadSubStep(vtkHM1DMesh *mesh)
{
	char line[256];

	vtkDebugMacro(<< "Reading Sub Step");

	//	this->ReadLine(line);

	if (this->IS->eof())
		return 1;

	//	if (!this->ReadString(line))
	//		{
	//		if (this->IS->eof())
	//			return 1;
	//
	//		vtkErrorMacro(<<"Premature EOF reading SubStep line! " << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
	//		return 0;
	//		}
	//
	//	if (strncmp("*SubStep", line, 30) )
	//		{
	//		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
	//				<< (this->FileName?this->FileName:"(Null FileName)"));
	//
	//		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
	//		return 0;
	//		}
	//
	//	this->ReadLine(line);

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Can't read if resultant matrix is symetrical!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	if (strcmp("T", line) == 0)
		this->BasParam->SetResultantMatrixIsSymmetrical(true);
	else
		this->BasParam->SetResultantMatrixIsSymmetrical(false);

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Can't read if calculation step is linear!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	if (strcmp("T", line) == 0)
		this->BasParam->SetCalculationStepIsLinear(true);
	else
		this->BasParam->SetCalculationStepIsLinear(false);

	int max;
	if (!this->Read(&max))
		{
		vtkErrorMacro(<<"Can't read maximum number of iterations (SubStep)!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetMaxIterationSubStep(max);

	double ral;
	if (!this->Read(&ral))
		{
		vtkErrorMacro(<<"Can't read the parameter ralaxacao (SubStep)!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetRalaxacaoSubStep(ral);

	int resolve;
	if (!this->Read(&resolve))
		{
		vtkErrorMacro(<<"Can't read the parameter resolver of library!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->BasParam->SetResolveLibrary(resolve);

	//Case the resolver of the library is different of zero,
	//read some parameters more
	//	if (resolve != 0)
	if ( (resolve != 0) && (resolve != 200) && (resolve != 20061129))
		{
		if (!this->Read(&max))
			{
			vtkErrorMacro(<<"Can't read the maximum quantity of iterations!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->BasParam->SetMaxIterationOfResolver(max);

		double tol;
		if (!this->Read(&tol))
			{
			vtkErrorMacro(<<"Can't read the parameter convergence tolerance !" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->BasParam->SetToleranceOfConvergence(tol);

		int space;
		if (!this->Read(&space))
			{
			vtkErrorMacro(<<"Can't read the parameter spaces of Krylov!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->BasParam->SetSpacesOfKrylov(space);

		int num;
		if (!this->Read(&num))
			{
			vtkErrorMacro(<<"can't read number of coefficients of the matrix!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->BasParam->SetNumberOfCoefficients(num);

		if (!this->Read(&tol))
			{
			vtkErrorMacro(<<"Can't read the parameter to excuse coefficients!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->BasParam->SetToleranceOfExcuse(tol);
		this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
		}

	int *elementType = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
	for (int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++)
		{
		for (int j=0; j<5; j++)
			{
			if (!this->Read(&elementType[i*5+j]))
				{
				vtkErrorMacro(<<"Can't read the type of element and its parametes!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			if (j == 4)
				this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
			}
		}
	this->BasParam->SetElementType(elementType);

	double *paramElementType =
	    new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	for (int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++)
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			{
			if (!this->Read(&paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]))
				{
				vtkErrorMacro(<<"Can't read the parameters for the types of elements!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}
		}
	this->BasParam->SetParamElementType(paramElementType);

	double *normaSubStep = new double[mesh->GetDegreeOfFreedom()];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->Read(&normaSubStep[i]))
			{
			vtkErrorMacro(<<"Can't read the vector Norma (SubStep)!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		}
	this->BasParam->SetNormaSubStep(normaSubStep);

	double *toleranceSubStep = new double[mesh->GetDegreeOfFreedom()];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->Read(&toleranceSubStep[i]))
			{
			vtkErrorMacro(<<"Can't read the vector Tolerance (SubStep)!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}
	this->BasParam->SetToleranceSubStep(toleranceSubStep);

	string *name = new string[mesh->GetDegreeOfFreedom()];
	char str[50];

	for (int i=0; i<mesh->GetDegreeOfFreedom(); i++)
		{
		if (!this->ReadString(str))
			{
			vtkErrorMacro(<<"Can't read the names of the degrees of liberty (SubStep)!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		name[i] = str;
		}
	this->BasParam->SetNameDegreeOfFreedomSubStep(name);

	int nums[3];
	for (int i=0; i<3; i++)
		{
		if (!this->Read(&nums[i]))
			{
			vtkErrorMacro(<<"Can't read the vector of nums (SubStep)!" << " for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}
	this->BasParam->SetNumsSubStep(nums);

	return 1;
}

void vtkHM1DBasParamReader::SetBasParam(vtkHM1DBasParam *BP)
{
	this->BasParam = BP;
}

vtkHM1DBasParam *vtkHM1DBasParamReader::GetBasParam()
{
	return this->BasParam;
}
