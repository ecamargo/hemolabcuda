/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DBasParamWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DBasParamWriter.h"
#include "vtkHM1DBasParam.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DBasParamWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DBasParamWriter);

//----------------------------------------------------------------------------
void vtkHM1DBasParamWriter::WriteBasParamData(vtkHM1DBasParam *BasParam, char *dir)
{
  ostream *fp;
	
	char d[256];
	strcpy(d, dir);
	strcat(d, "/Basparam.txt");
	
	this->SetFileName(d);
	
  vtkDebugMacro(<<"Writing BasParam data...");

  if ( !(fp=this->OpenFile(BasParam)) || !this->WriteHeader(fp, BasParam) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if ( !this->WriteStepContinuationControl(fp, BasParam) )
    {
    errorOccured = 1;
    }    
  if ( !this->WriteTimeStep(fp, BasParam) )
    {
    errorOccured = 1;
    }
  if ( !this->WriteOutputControl(fp, BasParam) )
    {
    errorOccured = 1;
    }
  if ( !this->WriteNodeOutputControl(fp, BasParam) )
    {
    errorOccured = 1;
    }
  if ( !this->WriteElementLibraryControl(fp, BasParam) )
    {
    errorOccured = 1;
    }
		
//	if ( !this->WriteSubStep(fp, BasParam,  1) )
//    {
//    errorOccured = 1;
//    }
 	
 	// escreve o segundo SUBSTEP
 	// BasParam->GetCalculationStepIsLinear() só é verdadeiro no 3D
	// no caso 3D, teremos dois Substeps
	if (BasParam->GetCalculationStepIsLinear())
		{
		if (BasParam->GetCoupledModel()) // se é modelo acoplado
			{
			if (BasParam->GetCouplingScheme() == 2) // 0 - monolitico 1: normal Segre e 2: Super Segregated
				{
				/*
				Super Segregation:
				1D Substep: Somente 1D habilitados
				3D -1 Substep: somente elementos 3D do volume 1 habilitados
				3D -n Substep: somente elementos 3D do volume n habilitados
				Bubble Substep: somente elementos de volume habilitados 
				*/
				
				if (!BasParam->GetSegregatedSubStepOrder())
					{
					//*** ordem de escrita dos substeps: 1D -3Ds	and bubble
					
					//substep 1D
					if ( !this->WriteSubStep(fp, BasParam,  1) )
		 	  		errorOccured = 1;
						
					// substeps 3Ds
					for (int i = 0; i < BasParam->GetNumberOfSubsteps()-2; ++i)
						{
						if ( !this->WriteSubStepSuperSegregated(fp, BasParam,  i) )
		 	  			errorOccured = 1;
						}
						
	   	  	// substep bubble
	   	  	if (!this->WriteSubStepBuble(fp, BasParam ))	
		 	  		errorOccured = 1;
					}
				else
					{
					//*** ordem de escrita dos substeps: 3Ds -1D	and bubble
					
					// substeps 3Ds
					for (int i = 0; i < BasParam->GetNumberOfSubsteps()-2; ++i)
						{
						if ( !this->WriteSubStepSuperSegregated(fp, BasParam, i) )
		 	  			errorOccured = 1;
						}
					
					//substep 1D
					if ( !this->WriteSubStep(fp, BasParam,  1) )
		 	  		errorOccured = 1;
						
						
	   	  	// substep bubble
	   	  	if (!this->WriteSubStepBuble(fp, BasParam))	
		 	  		errorOccured = 1;	
					}	
				}
				
			if (BasParam->GetCouplingScheme() == 1)	// normal segregation model 3 substeps
				{
				/*
				Normal Segregation:
				1D Substep: Somente 1D habilitados
				3D Substeps: Todos 3Ds habilitados
				Bubble Substep: somente elementos de volume habilitados 
				*/
				// escreve 3 substeps 
				if (!BasParam->GetSegregatedSubStepOrder())
					{
					//*** ordem de escrita dos substeps: 1D -3D	and bubble
					
					//substep 1D
					if ( !this->WriteSubStep(fp, BasParam,  1) )
	 	  			errorOccured = 1;
					
					// substep 3D	
					if ( !this->WriteSubStep(fp, BasParam,  2) )
	 	  			errorOccured = 1;
					
   	  		// substep bubble
   	  		if (!this->WriteSubStepBuble(fp, BasParam))	
   	  			errorOccured = 1;
					}
				else
					{
					//*** ordem de escrita dos substeps: 3D-1D	and bubble
					
					// substep 3D
					if ( !this->WriteSubStep(fp, BasParam,  2) )
	 	  			errorOccured = 1;
					
					//substep 1D	
					if ( !this->WriteSubStep(fp, BasParam,  1) )
	 	  			errorOccured = 1;
					// substep bubble
   	  		if (!this->WriteSubStepBuble(fp, BasParam ))	
   	  			errorOccured = 1;
					}		
				}
			// monolitico
			if (BasParam->GetCouplingScheme() == 0) // 0 - monolitico 1: normal Segre e 2: Super Segregated
				{
				if (!this->WriteSubStep(fp, BasParam,  1) )
	 	  		errorOccured = 1;

				// somente substep bubble
				if (!this->WriteSubStepBuble(fp, BasParam ))	
   	  		errorOccured = 1;
				}
			}
		else // ou somente 3D "sozinho"
			{
			// modelo 1D - somente um substep	
			if ( !this->WriteSubStep(fp, BasParam,  1) )
 	  		errorOccured = 1;
			
			// escreve somente o substep buble
   	  if (!this->WriteSubStepBuble(fp, BasParam))	
   	  	errorOccured = 1;
			}	
		
		}
	else
		{
		// modelo 1D - somente um substep	
		if ( !this->WriteSubStep(fp, BasParam,  1) )
 	  	errorOccured = 1;
			
		}	

  
  
  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  
  
	// se solver é serial somente
	if (!BasParam->GetSolverType())
		{
		if ( !this->WriteLogFilename(fp, BasParam->GetLogFilename()) )
    	{
    	errorOccured = 1;
    	}
		}
  
	if ( !this->WriteInitialTime(fp, BasParam) )
    errorOccured = 1;
  
  
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a BasParam data file. Returns NULL if error.
ostream *vtkHM1DBasParamWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening BasParam file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close a BasParam file.
void vtkHM1DBasParamWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing BasParam file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}
//----------------------------------------------------------------------------
// Write the header of the BasParam data file. Returns 0 if error.
int vtkHM1DBasParamWriter::WriteHeader(ostream *fp, vtkHM1DBasParam *input)
{
  vtkDebugMacro(<<"Writing header...");

	// somente se for 3d
	if (input->GetCalculationStepIsLinear())
		{
		*fp << "*Param Write Swicht\n";
		*fp << "1\n";
		}

  *fp << "*Renumbering\n";
  *fp << input->GetRenumbering() << "\n"; 

  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
//Writing block of the data StepContinuationControl
int vtkHM1DBasParamWriter::WriteStepContinuationControl(ostream *fp, vtkHM1DBasParam *basParam )
{
  vtkDebugMacro(<<"Writing block of the data StepContinuationControl...");

  *fp << "*StepContinuationControl\n";
  
	if(basParam->GetConfigProcessExternal())
	  *fp << "T ";
	else
		*fp << "F ";
	
	*fp << basParam->GetMaxIteration() << " ";
	*fp << basParam->GetRalaxacao() << " ";
	*fp << basParam->GetQuantityStep() << "\n";
	
	double *norma = basParam->GetNorma();
	
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		*fp << norma[i] << " ";
	*fp << "\n";
	
	double *tolerance = basParam->GetTolerance();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		*fp << tolerance[i] << " ";
	*fp << "\n";
	
	string *name = basParam->GetNameDegreeOfFreedom();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		*fp << name[i] << " ";
	*fp << "\n";
	
	int *nums = basParam->GetNums();
	// a ultima linha de parametro de *StepContinuationControl
	// sempre é "0 0 0" e portanto tem 3 valores
	for ( int i=0; i<3; i++ )
		*fp << nums[i] << " ";
	*fp << "\n";

  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
// WriteTimeStep
int vtkHM1DBasParamWriter::WriteTimeStep(ostream *fp, vtkHM1DBasParam *basParam)
{
	vtkDebugMacro(<<"Writing block of the data TimeStep...");

  *fp << "*TimeStep ( DelT, Tini, Tmax )\n";
  *fp << basParam->GetDelT() << " " << basParam->GetTini() << " " << basParam->GetTmax() << "\n";
  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
// WriteOutputControl
int vtkHM1DBasParamWriter::WriteOutputControl(ostream *fp, vtkHM1DBasParam *basParam)
{
	vtkDebugMacro(<<"Writing block of the data OutputControl...");

  *fp << "*OutputControl\n";
  *fp << basParam->GetTimeQuantityScreen() << " " << basParam->GetTimeQuantityFile() << "\n";
  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
// WriteNodeOutputControl
int vtkHM1DBasParamWriter::WriteNodeOutputControl(ostream *fp, vtkHM1DBasParam *basParam)
{
	vtkDebugMacro(<<"Writing block of the data NodeOutputControl...");

  *fp << "*NodeOutputControl (#nodes=0 > All >> OutputNodesList )\n";
  *fp << basParam->GetWriterOutput() << " ";
  
  
  if( basParam->GetWriterOutput() > 0 )
  {
  	int *outputList = basParam->GetOutputList();
  	// writing list of node
  	for ( int i=0; i<basParam->GetWriterOutput(); i++ )
  		*fp << outputList[i] << " ";
  	
  }
  *fp << "\n";
  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
// WriteElementLibraryControl
int vtkHM1DBasParamWriter::WriteElementLibraryControl(ostream *fp, vtkHM1DBasParam *basParam)
{
	vtkDebugMacro(<<"Writing block of the data ElementLibraryControl...");

  *fp << "\n*ElementLibraryControl  (MaxElemLib,MaxLCommonPar)\n";
  *fp << basParam->GetQuantityOfDifferentElements() << " ";
  *fp << basParam->GetMaximumQuantityOfParameters() << "\n";

  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
// WriteSubStep
int vtkHM1DBasParamWriter::WriteSubStep(ostream *fp, vtkHM1DBasParam *basParam,  int SubStepNumber)
{
	vtkDebugMacro(<<"Writing block of the data SubStep...");

  *fp << "\n*SubStep\n";
  if (SubStepNumber==1)
  	{
	  if ( basParam->GetResultantMatrixIsSymmetrical() )
	  	*fp << "T ";
	  else
	  	*fp << "F ";
	  }
	else // substep 3D
		{
	  if ( basParam->GetResultantMatrixIsSymmetricalSubStep3D() )
	  	*fp << "T ";
	  else
	  	*fp << "F ";
	  }
	
	
	if (SubStepNumber==1)
	  {
  if ( basParam->GetCalculationStepIsLinear() )
  	*fp << "T ";
  else
  	*fp << "F ";


  *fp << basParam->GetMaxIterationSubStep() << " ";
  *fp << basParam->GetRalaxacaoSubStep() << " ";


	  }
	else // // substep 3D quando normal segregated
		{
	  if ( basParam->GetCalculationStepIsLinearSubStep3D() )
  		*fp << "T ";
  	else
  		*fp << "F ";	
		
		
  *fp << basParam->GetMaxIterationSubStep3D() << " ";
  *fp << basParam->GetRalaxacaoSubStep3D() << " ";
		}  
  
  if (SubStepNumber==1)
	  *fp << basParam->GetResolveLibrary() << " ";
  else
	  *fp << basParam->GetResolveLibrarySubStep3D() << " ";
  
  //Case the resolver of the library is different of zero,
	//write some parameters more
	if (SubStepNumber==1)	
  	{
	  //if ( basParam->GetResolveLibrary() )
	  if ( basParam->GetResolveLibrary() && (basParam->GetResolveLibrary() != 200) )
	  	{
  		*fp << "\n";
	  	*fp << basParam->GetMaxIterationOfResolver() << " ";
	  	*fp << basParam->GetToleranceOfConvergence() << " ";
	  	*fp << basParam->GetSpacesOfKrylov() << " ";
	  	*fp << basParam->GetNumberOfCoefficients() << " ";
	  	*fp << basParam->GetToleranceOfExcuse();// << "\n";
	  	}
  	}
  else // substep 3D
  	{
	  //if ( basParam->GetResolveLibrarySubStep3D() )
	  if ( basParam->GetResolveLibrarySubStep3D() && (basParam->GetResolveLibrarySubStep3D() != 200) )
	  	{
  	  *fp << "\n";
	  	*fp << basParam->GetMaxIterationOfResolver3D() << " ";
	  	*fp << basParam->GetToleranceOfConvergence3D() << " ";
	  	*fp << basParam->GetSpacesOfKrylov3D() << " ";
	  	*fp << basParam->GetNumberOfCoefficients3D() << " ";
	  	*fp << basParam->GetToleranceOfExcuse3D();// << "\n";
	  	}
  	}
  
  
  *fp << "\n";
  
  int *elementType;
  
  if (SubStepNumber == 1)
  	elementType= basParam->GetElementType();
  else // substep 3D
  	elementType = basParam->GetElementTypeSubStep3D();
  	
  
  for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			*fp << elementType[i*5+j] << " ";
			}
		*fp << "\n";
		}
	
	double *paramElementType = basParam->GetParamElementType();
	for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
	{
		for ( int j=0; j<basParam->GetMaximumQuantityOfParameters(); j++ )
		{
			*fp << paramElementType[i*basParam->GetMaximumQuantityOfParameters()+j] << " ";
		}
		*fp << "\n";
	}
	
	
	if (basParam->GetCoupledModel())
		{

		double *normaSubStep;
		
	  //	BasParam->GetCouplingScheme() == 0 monolitico
		
		if (SubStepNumber==1 && basParam->GetCouplingScheme())
			normaSubStep = basParam->GetNormaSubStep1D();
		else
			normaSubStep = basParam->GetNormaSubStep(); // para caso monolitico
		
		if (SubStepNumber==2)
			normaSubStep = basParam->GetNormaSubStep3D();
		
		
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << normaSubStep[i] << " ";
		}
		*fp << "\n";
		
		double *toleranceSubStep;
		
		if (SubStepNumber==1 && basParam->GetCouplingScheme()) // se este é  o primeiro substep de segegrado normal
			toleranceSubStep = basParam->GetToleranceSubStep1D();
		else
			toleranceSubStep = basParam->GetToleranceSubStep(); // para caso monolitico
		
		
		// substep 3D
		if (SubStepNumber==2) 
			toleranceSubStep = basParam->GetToleranceSubStep3D();
		
		
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << toleranceSubStep[i] << " ";
		}
		*fp << "\n";

		}
	else // modelos 3D ou 1D
		{

		double *normaSubStep = basParam->GetNormaSubStep();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << normaSubStep[i] << " ";
		}
		*fp << "\n";
		
		double *toleranceSubStep = basParam->GetToleranceSubStep();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << toleranceSubStep[i] << " ";
		}
		*fp << "\n";
			
			
		}
	
	string *name = basParam->GetNameDegreeOfFreedomSubStep();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
	{
		*fp << name[i] << " ";
	}
	*fp << "\n";
	
	int *nums = basParam->GetNumsSubStep();
	for ( int i=0; i<3; i++)
	{
		*fp << nums[i] << " ";
	}
	*fp << "\n";
	
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}


//----------------------------------------------------------------------------
// WriteSubStep
int vtkHM1DBasParamWriter::WriteSubStepSuperSegregated(ostream *fp, vtkHM1DBasParam *basParam,  int SubStepNumber)
{
	vtkDebugMacro(<<"Writing block of the data SubStep...");

  *fp << "\n*SubStep\n";
	if ( basParam->GetResultantMatrixIsSymmetricalSubStep3D() )
		*fp << "T ";
	else
	 	*fp << "F ";
	
	
	if ( basParam->GetCalculationStepIsLinearSubStep3D() )
  	*fp << "T ";
  else
  	*fp << "F ";	

  *fp << basParam->GetMaxIterationSubStep3D() << " ";
  
  *fp << basParam->GetRalaxacaoSubStep3D() << " ";
  
	*fp << basParam->GetResolveLibrarySubStep3D() << " ";
  
  //Case the resolver of the library is different of zero,
	//write some parameters more
	
	// if resolver library equals some parallel solver I dont write these parameters
	if ( basParam->GetResolveLibrarySubStep3D() && (basParam->GetResolveLibrarySubStep3D() != 200) )
		{
  	*fp << "\n";
	  *fp << basParam->GetMaxIterationOfResolver3D() << " ";
	  *fp << basParam->GetToleranceOfConvergence3D() << " ";
	  *fp << basParam->GetSpacesOfKrylov3D() << " ";
	  *fp << basParam->GetNumberOfCoefficients3D() << " ";
	  *fp << basParam->GetToleranceOfExcuse3D();
	 	}
  
  
  *fp << "\n";
  
  int *elementType = basParam->GetSubStepSuperSegregated(SubStepNumber);
  	
  
  for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			*fp << elementType[i*5+j] << " ";
			}
		*fp << "\n";
		}
	
	double *paramElementType = basParam->GetParamElementType();
	for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
	{
		for ( int j=0; j<basParam->GetMaximumQuantityOfParameters(); j++ )
		{
			*fp << paramElementType[i*basParam->GetMaximumQuantityOfParameters()+j] << " ";
		}
		*fp << "\n";
	}
	
	double *normaSubStep = basParam->GetNormaSubStep3D();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
	{
		*fp << normaSubStep[i] << " ";
	}
	*fp << "\n";
	
	double *toleranceSubStep = basParam->GetToleranceSubStep3D();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
	{
		*fp << toleranceSubStep[i] << " ";
	}
	*fp << "\n";
	
	string *name = basParam->GetNameDegreeOfFreedomSubStep();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
	{
		*fp << name[i] << " ";
	}
	*fp << "\n";
	
	int *nums = basParam->GetNumsSubStep();
	for ( int i=0; i<3; i++)
	{
		*fp << nums[i] << " ";
	}
	*fp << "\n";
	
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}


//----------------------------------------------------------------------------


int vtkHM1DBasParamWriter::WriteSubStepBuble(ostream *fp, vtkHM1DBasParam *basParam)
{
	vtkDebugMacro(<<"Writing block of the Buble SubStep...");

  *fp << "\n*SubStep\n";
	
	if ( basParam->GetResultantMatrixIsSymmetricalSubStepBubble() )
	  	*fp << "T ";
	else
	  	*fp << "F ";
	
	
	  if ( basParam->GetCalculationStepIsLinearSubStepBubble() )
  		*fp << "T ";
  	else
  		*fp << "F ";	

  *fp << basParam->GetMaxIterationSubStep() << " ";

	// sempre 1 no substep bubble
  *fp << 1 << " "; 
	

  //*fp << basParam->GetRalaxacaoSubStep() << " ";
 
	*fp << basParam->GetResolveLibrarySubStepBubble() << " ";
  
  //Case the resolver of the library is different of zero,
	//write some parameters more
	  if ( basParam->GetResolveLibrarySubStepBubble() && !basParam->GetSolverType())
	  	{
  	  *fp << "\n";
	  	*fp << basParam->GetMaxIterationOfResolver() << " ";
	  	*fp << basParam->GetToleranceOfConvergence() << " ";
	  	*fp << basParam->GetSpacesOfKrylov() << " ";
	  	*fp << basParam->GetNumberOfCoefficients() << " ";
	  	*fp << basParam->GetToleranceOfExcuse();// << "\n";
	  	}
  
  *fp << "\n";
  
  int *elementType;
  elementType= basParam->GetElementTypeSubStepBubble();
  
  	
  
  for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
	{
		for (int j=0; j<5; j++)
		{
			*fp << elementType[i*5+j] << " ";
		}
		*fp << "\n";
	}
	
	double *paramElementType = basParam->GetParamElementType();
	for ( int i=0; i<basParam->GetQuantityOfDifferentElements(); i++ )
	{
		for ( int j=0; j<basParam->GetMaximumQuantityOfParameters(); j++ )
		{
			*fp << paramElementType[i*basParam->GetMaximumQuantityOfParameters()+j] << " ";
		}
		*fp << "\n";
	}
	
	
	
	if (basParam->GetCoupledModel())
		{
		double *normaSubStep = basParam->GetNormaSubStep3D();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << normaSubStep[i] << " ";
		}
		*fp << "\n";

		double *toleranceSubStep = basParam->GetToleranceSubStep3D();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << toleranceSubStep[i] << " ";
		}
		*fp << "\n";



		}
	else // 3d sozinho
		{
		double *normaSubStep = basParam->GetNormaSubStep();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << normaSubStep[i] << " ";
		}
		*fp << "\n";

		double *toleranceSubStep = basParam->GetToleranceSubStep();
		for ( int i=0; i<basParam->GetDoFNumber(); i++ )
		{
			*fp << toleranceSubStep[i] << " ";
		}
		*fp << "\n";
					
			
		}
	
	
	
	
	string *name = basParam->GetNameDegreeOfFreedomSubStep();
	for ( int i=0; i<basParam->GetDoFNumber(); i++ )
	{
		*fp << name[i] << " ";
	}
	*fp << "\n";
	
	int *nums = basParam->GetNumsSubStep();
	for ( int i=0; i<3; i++)
	{
		*fp << nums[i] << " ";
	}
	*fp << "\n";
	
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------

//PrintSelf
void vtkHM1DBasParamWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}


//----------------------------------------------------------------------------
// WriteLogFilename
int vtkHM1DBasParamWriter::WriteLogFilename(ostream *fp, const char *filename)
{
	//vtkDebugMacro(<<"Writing WriteLogFilename...");
  *fp << "\n*LogFile\n";
  *fp << filename << "\n";

  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}
//----------------------------------------------------------------------------

int vtkHM1DBasParamWriter::WriteInitialTime(ostream *fp, vtkHM1DBasParam *input)

{
	vtkDebugMacro(<<"Writing Initial Time...");

	*fp << "\n*Initial Time\n";
	*fp << input->GetInitialTime() << "\n"; 
  
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}
//----------------------------------------------------------------------------





