/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DBasParamWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DBasParamWriter - write vtk polygonal data
// .SECTION Description
// vtkHM1DBasParamWriter is a source object that writes ASCII or binary 
// polygonal data files in vtk format. See text for format details.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef VTKHM1DBASPARAMWRITER_H_
#define VTKHM1DBASPARAMWRITER_H_

#include "vtkDataWriter.h"

class vtkHM1DBasParam;
class vtkDataObject;

class VTK_EXPORT vtkHM1DBasParamWriter : public vtkDataWriter
{
public:
  static vtkHM1DBasParamWriter *New();
  vtkTypeRevisionMacro(vtkHM1DBasParamWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void WriteBasParamData(vtkHM1DBasParam *BasParam, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // Close a BasParam file.
  void CloseFile(ostream *fp);
  
  // Description:
  // Write the header of the BasParam data file. Returns 0 if error.
  int WriteHeader(ostream *fp, vtkHM1DBasParam *input);
  
  // Description:
	// Write parameters of configuration of the
	// process of more external calculation
	int WriteStepContinuationControl(ostream *fp, vtkHM1DBasParam *basParam);
	
	// Description:
	// Write parameters that caracterize the problem transient
	int WriteTimeStep(ostream *fp, vtkHM1DBasParam *basParam);
	
	// Description:
	// Write the quantity of steps that occur between two outputs
	int WriteOutputControl(ostream *fp, vtkHM1DBasParam *basParam);
	
	// Description:
	// Write parameters for which nodos goes to be
	// made the writing of the results
	int WriteNodeOutputControl(ostream *fp, vtkHM1DBasParam *basParam);
	
	// Description:
	// Write general information concerning the
	// elements used in the problem 
	int WriteElementLibraryControl(ostream *fp, vtkHM1DBasParam *basParam);
	
	// Description:
	// Write general configuration of the
	// step of corresponding calculation
	int WriteSubStep(ostream *fp, vtkHM1DBasParam *basParam,  int SubStepNumber);


	int WriteSubStepSuperSegregated(ostream *fp, vtkHM1DBasParam *basParam, int SubStepNumber);

	// Description:
	// Write the bubble substep - used only in 3D and coupled model
	int WriteSubStepBuble(ostream *fp, vtkHM1DBasParam *basParam );
	
	// Description:
	// Write the log filename
	int WriteLogFilename(ostream *fp, const char *filename);
	
	int WriteInitialTime(ostream *fp, vtkHM1DBasParam *input);
	


protected:
  vtkHM1DBasParamWriter() {};
  ~vtkHM1DBasParamWriter() {};



};



#endif /*VTKHM1DBASPARAMWRITER_H_*/
