/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeMoLabReader.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DHeMoLabReader.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DStraightModelGrid.h"

#include "vtkHM1DTreeElement.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHMDataOut.h"
#include "vtkHMUtils.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyDataReader.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkExecutive.h"
#include "vtkMath.h"

#include <fstream>
#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHM1DHeMoLabReader, "$Revision: 2396 $");
vtkStandardNewMacro(vtkHM1DHeMoLabReader);

vtkHM1DHeMoLabReader::vtkHM1DHeMoLabReader()
{
	this->FileName = NULL;

  vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::New();
  this->SetOutput(output);
  // Releasing data for pipeline parallism.
  // Filters will know it is empty.
  output->ReleaseData();
  output->Delete();
  this->ExecutePiece = this->ExecuteNumberOfPieces = 0;
  this->ExecuteGhostLevel = 0;

  this->StraightModel = NULL;

  this->Points = vtkPoints::New();

  this->FirstSegmentsClip = vtkStringArray::New();
	this->SecondSegmentsClip = vtkStringArray::New();

  this->AgingElastinFactor = 1;
  this->AgingViscoelasticityFactor = 1;

	vtkDebugMacro(<<"Reading file data...");

}

vtkHM1DHeMoLabReader::~vtkHM1DHeMoLabReader()
{
	if (this->StraightModel)
		this->StraightModel->Delete();

	this->Points->Delete();

	NamesMap::iterator nameIt = this->ParentNameMap.begin();
	while ( nameIt != this->ParentNameMap.end() )
		{
		nameIt->second->Delete();
		nameIt++;
		}

	nameIt = this->ChildNameMap.begin();
	while ( nameIt != this->ChildNameMap.end() )
		{
		nameIt->second->Delete();
		nameIt++;
		}

	if ( this->FirstSegmentsClip )
		this->FirstSegmentsClip->Delete();
	if ( this->SecondSegmentsClip )
		this->SecondSegmentsClip->Delete();

	InterventionsMap::iterator itIntervention = this->StentMap.begin();
	while ( itIntervention != this->StentMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	itIntervention = this->StenosisMap.begin();
	while ( itIntervention != this->StenosisMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}

	itIntervention = this->AneurysmMap.begin();
	while ( itIntervention != this->AneurysmMap.end() )
		{
		itIntervention->second->Delete();
		itIntervention++;
		}
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DHeMoLabReader::GetOutput()
{
  return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DHeMoLabReader::GetOutput(int idx)
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHM1DHeMoLabReader::SetOutput(vtkDataObject *output)
{
  this->GetExecutive()->SetOutputData(0, output);
}


//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::RequestUpdateExtent(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  int piece, numPieces, ghostLevel;

  piece = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  numPieces = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  ghostLevel = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS());

  // make sure piece is valid
  if (piece < 0 || piece >= numPieces)
    {
    return 1;
    }

  if (ghostLevel < 0)
    {
    return 1;
    }

  // Save the piece so execute can use this information.
  this->ExecutePiece = piece;
  this->ExecuteNumberOfPieces = numPieces;

  this->ExecuteGhostLevel = ghostLevel;

  return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::FillOutputPortInformation(int,
                                                 vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
  return 1;
}

void vtkHM1DHeMoLabReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//------------------------------------------------------------------
int vtkHM1DHeMoLabReader::RequestData(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
	vtkDebugMacro(<<"Casting output to vtkHM1DStraightModelGrid.");

	//get the info object
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the ouptut
	vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	if ( !this->StraightModel )
		this->StraightModel = vtkHM1DStraightModel::New();

	if (!this->ReadFile())
		{
		vtkErrorMacro(<<"Error reading file.");
		this->CloseFile();
		return 0;
		}

	//Setting progress of the read
	float progress=this->GetProgress();
	this->SetProgress(progress + 0.07*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	this->MakeStraightModel();

	output->SetStraightModel(this->StraightModel);
	if (!output->CreateStraightModelGrid())
		{
		vtkErrorMacro(<<"Error creating StraightModelGrid.");
		return 0;
		}

	//Setting progress
	progress=this->GetProgress();
	this->SetProgress(progress + 0.01*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	return 1;
}

//----------------------------------------------------------------------------
//Open File
int vtkHM1DHeMoLabReader::OpenFile()
{
	if (this->ReadFromInputString)
		{
		if (this->InputArray)
			{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0),
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
			}
		else if (this->InputString)
			{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
			}
		}
	else
		{
		vtkDebugMacro(<< "Opening file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
			{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
			}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0)
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
			}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
			}

		return 1;
		}

	return 0;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DHeMoLabReader::CloseFile()
{
	vtkDebugMacro(<<"Closing file");
	if ( this->IS != NULL )
		delete this->IS;

	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Read Mesh File
int vtkHM1DHeMoLabReader::ReadFile()
{
	vtkDebugMacro(<<"ReadFile()...");

	if ( !this->OpenFile() )
		{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    	return 0;
		}

	if ( !this->ReadPoints() )
		{
		vtkErrorMacro(<<"Can't read points.");
		this->CloseFile ();
		return 0;
		}

	if ( !this->ReadData() )
		{
		vtkErrorMacro(<<"Can't read data.");
		this->CloseFile ();
		return 0;
		}

	this->CloseFile();

	return 1;
}

//----------------------------------------------------------------------------
// Read Points
int vtkHM1DHeMoLabReader::ReadPoints()
{
	double x, y, z=0.0;
	char line[256];

	//read coordinates
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading coordinates line! " << " for file: "
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*coordinates", this->LowerCase(line), 12) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
		                  << (this->FileName?this->FileName:"(Null FileName)"));

		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
		}
	int n;

	if(!this->Read(&n))
		{
		vtkErrorMacro(<<"Can't read number of cordinates!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	vtkDebugMacro(<<"Reading points data...");

	for ( int i=0; i<n; i++ )
		{
		if ( !this->Read(&x) ||	!this->Read(&y) || !this->Read(&z) )
			{
			vtkErrorMacro(<<"Cannot read points data in line " << i+1
						  << " of field 'points' for file: "
						  << (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->Points->InsertNextPoint(x,y,z);
		}

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadData()
{
	char line[256];

	while (this->ReadString(line))
		{
		if ( !strncmp ("*heart", this->LowerCase(line), 6) )
			{
			if(!this->ReadHeartParameters())
				{
				vtkErrorMacro(<<"Can't read heart parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*terminals", this->LowerCase(line), 10) )
			{
			if(!this->ReadTerminalsParameters())
				{
				vtkErrorMacro(<<"Can't read terminals parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*segments", this->LowerCase(line), 9) )
			{
			if(!this->ReadSegmentsParameters())
				{
				vtkErrorMacro(<<"Can't read segments parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*clips", this->LowerCase(line), 6) )
			{
			if(!this->ReadClipsParameters())
				{
				vtkErrorMacro(<<"Can't read clips parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*stents", this->LowerCase(line), 7) )
			{
			if(!this->ReadStentParameters())
				{
				vtkErrorMacro(<<"Can't read stents parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*stenosis", this->LowerCase(line), 9) )
			{
			if(!this->ReadStenosisParameters())
				{
				vtkErrorMacro(<<"Can't read stenosis parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*aneurysm", this->LowerCase(line), 9) )
			{
			if(!this->ReadAneurysmParameters())
				{
				vtkErrorMacro(<<"Can't read aneurysm parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

		else if ( !strncmp ("*aging", this->LowerCase(line), 6) )
			{
			if(!this->ReadAgingParameters())
				{
				vtkErrorMacro(<<"Can't read aging parameters" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			}

    else
      {
      vtkErrorMacro(<< "Unsupported cell attribute type: " << line
                    << " for file: " << (this->FileName?this->FileName:"(Null FileName)"));
      return 0;
      }
		}
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadHeartParameters()
{
	char line[256];

	vtkStringArray *childNames = vtkStringArray::New();
	vtkStringArray *parentNames = vtkStringArray::New();

	//read data
	vtkDebugMacro(<<"Reading HEART data...");

	int n;
//	if(!this->Read(&n))
//		{
//		vtkErrorMacro(<<"Can't read point id!" << " for file: "
//						<< (this->FileName?this->FileName:"(Null FileName)"));
//		return 0;
//		}

	if(!this->ReadString(line))
		{
		vtkErrorMacro(<<"Can't read child name" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		parentNames->Delete();
		childNames->Delete();
		return 0;
		}
	childNames->InsertNextValue(line);

	//ler quantidade de parametros para resistencia 1 e 2 e para capacitancia
	if(!this->Read(&n))
		{
		vtkErrorMacro(<<"Can't read number of parameters" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		parentNames->Delete();
		childNames->Delete();
		return 0;
		}

	double value;
	std::list<double> l;
	//ler parametros de resistencia 1 e 2 e de capacitancia
	for ( int i=0; i<n; i++ )
		{
		if ( !this->Read(&value) )
			{
			vtkErrorMacro(<<"Cannot read parameters data" << " for file: "
						  << (this->FileName?this->FileName:"(Null FileName)"));
			parentNames->Delete();
			childNames->Delete();
			return 0;
			}
		l.push_back(value);
		}

	vtkHM1DTerminal *heart = vtkHM1DTerminal::SafeDownCast(this->StraightModel->Get1DTreeRoot());
	std::list<double>::iterator it = l.begin();

	vtkHM1DTerminal::ResistanceValuesList r1;
	vtkHM1DTerminal::ResistanceTimeList r1Time;
	vtkHM1DTerminal::ResistanceValuesList r2;
	vtkHM1DTerminal::ResistanceTimeList r2Time;
	vtkHM1DTerminal::CapacitanceValuesList c;
	vtkHM1DTerminal::CapacitanceTimeList cTime;
	vtkHM1DTerminal::PressureValuesList p;
	vtkHM1DTerminal::PressureValuesList pTime;
	//setar valores
	r1Time.push_back(*it);
	it++;
	r1.push_back(*it);
	it++;
	r1Time.push_back(*it);
	it++;
	r1.push_back(*it);
	it++;

	r2Time.push_back(*it);
	it++;
	r2.push_back(*it);
	it++;
	r2Time.push_back(*it);
	it++;
	r2.push_back(*it);
	it++;

	cTime.push_back(*it);
	it++;
	c.push_back(*it);
	it++;
	cTime.push_back(*it);
	it++;
	c.push_back(*it);
	it++;

	//ler quantidade de pontos da curva de pressão
	if(!this->Read(&n))
		{
		vtkErrorMacro(<<"Can't read number of points of the heart curve" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		parentNames->Delete();
		childNames->Delete();
		return 0;
		}

	double time;
	//ler parametros de pressão
	for ( int i=0; i<n; i++ )
		{
		if ( !this->Read(&time) || !this->Read(&value) )
			{
			vtkErrorMacro(<<"Cannot read point" << " for file: "
						  << (this->FileName?this->FileName:"(Null FileName)"));
			parentNames->Delete();
			childNames->Delete();
			return 0;
			}
		pTime.push_back(time);
		p.push_back(value);
		}

	heart->SetR1(&r1);
	heart->SetR1Time(&r1Time);
	heart->SetR2(&r2);
	heart->SetR2Time(&r2Time);
	heart->SetC(&c);
	heart->SetCTime(&cTime);
	heart->SetPt(&p);
	heart->SetPtTime(&pTime);

	this->ParentNameMap.insert(NamesPair(heart->GetId(), parentNames));
	this->ChildNameMap.insert(NamesPair(heart->GetId(), childNames));

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadTerminalsParameters()
{
	char line[256];

	//read data
	vtkDebugMacro(<<"Reading TERMINAL data...");

	int numberOfTerminals;
	if(!this->Read(&numberOfTerminals))
		{
		vtkErrorMacro(<<"Can't read number of terminals!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	for (int i=0; i<numberOfTerminals; i++ )
		{
		int n;
//		if(!this->Read(&n))
//			{
//			vtkErrorMacro(<<"Can't read point id!" << " for file: "
//							<< (this->FileName?this->FileName:"(Null FileName)"));
//			return 0;
//			}

		int numberOfParents;
		if(!this->Read(&numberOfParents))
			{
			vtkErrorMacro(<<"Can't read number of parents!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		vtkStringArray *parentNames = vtkStringArray::New();
		for ( int j=0; j<numberOfParents; j++ )
			{
			if(!this->ReadString(line))
				{
				vtkErrorMacro(<<"Can't read parent name" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			parentNames->InsertNextValue(line);
			}

		int numberOfChilds;
		if(!this->Read(&numberOfChilds))
			{
			vtkErrorMacro(<<"Can't read number of childs!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		vtkStringArray *childNames = vtkStringArray::New();
		for ( int j=0; j<numberOfChilds; j++ )
			{
			if(!this->ReadString(line))
				{
				vtkErrorMacro(<<"Can't read child name" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			childNames->InsertNextValue(line);
			}

		if(!this->Read(&n))
			{
			vtkErrorMacro(<<"Can't read number of parameters" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		double value;
		std::list<double> l;
		for ( int j=0; j<n; j++ )
			{
			if ( !this->Read(&value) )
				{
				vtkErrorMacro(<<"Cannot read parameters data" << " for file: "
							  << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			l.push_back(value);
			}
		std::list<double>::iterator it = l.begin();

		vtkHM1DTerminal::ResistanceValuesList r1;
		vtkHM1DTerminal::ResistanceTimeList r1Time;
		vtkHM1DTerminal::ResistanceValuesList r2;
		vtkHM1DTerminal::ResistanceTimeList r2Time;
		vtkHM1DTerminal::CapacitanceValuesList c;
		vtkHM1DTerminal::CapacitanceTimeList cTime;
		vtkHM1DTerminal::PressureValuesList p;
		vtkHM1DTerminal::PressureValuesList pTime;

		//setar parametros de resistencia 1 e 2, capacitancia e pressao.
		r1Time.push_back(0);
		r2Time.push_back(0);
		cTime.push_back(0);
		pTime.push_back(0);

		while ( it!=l.end() )
			{
			r1.push_back(*it);
			it++;
			r2.push_back(*it);
			it++;
			c.push_back(*it);
			it++;
			p.push_back(*it);
			it++;
			}

		//Adicionar novo terminal
		vtkIdType newTerm = this->StraightModel->AddTerminal(-1);
		vtkHM1DTerminal *Term = this->StraightModel->GetTerminal(newTerm);
		Term->SetR1(&r1);
		Term->SetR1Time(&r1Time);
		Term->SetR2(&r2);
		Term->SetR2Time(&r2Time);
		Term->SetC(&c);
		Term->SetCTime(&cTime);
		Term->SetPt(&p);
		Term->SetPtTime(&pTime);

		this->ParentNameMap.insert(NamesPair(newTerm, parentNames));
		this->ChildNameMap.insert(NamesPair(newTerm, childNames));
		} // Fim for (int i=0; i<numberOfTerminals; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadSegmentsParameters()
{
	//read data
	vtkDebugMacro(<<"Reading SEGMENT data...");

	int numberOfSegments;
	if(!this->Read(&numberOfSegments))
		{
		vtkErrorMacro(<<"Can't read number of segments!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	for (int i=0; i<numberOfSegments; i++ )
		{
		char *name = new char[256];
		if(!this->ReadString(name))
			{
			vtkErrorMacro(<<"Can't read segment name" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		int numberOfElements;
		if(!this->Read(&numberOfElements))
			{
			vtkErrorMacro(<<"Can't read number of elements!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		double value;
		vtkDoubleArray *array = vtkDoubleArray::New();
		array->SetNumberOfComponents(13);
		array->SetNumberOfTuples(numberOfElements);
		for ( int j=0; j<numberOfElements; j++ )
			{
			for (int k=0; k<13; k++ )
				{
				if ( !this->Read(&value) )
					{
					vtkErrorMacro(<<"Cannot read parameters data" << " for file: "
								  << (this->FileName?this->FileName:"(Null FileName)"));
					return 0;
					}
				array->SetComponent(j,k, value);
				}

			}

		vtkIdList *nodeList = vtkIdList::New();
		int idNode;
		for (int j=0; j<numberOfElements+1; j++ )
			{
			if ( !this->Read(&idNode) )
				{
				vtkErrorMacro(<<"Cannot read parameters data" << " for file: "
							  << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			nodeList->InsertNextId(idNode);
			}

		vtkHM1DSegment *seg = vtkHM1DSegment::New();
		seg->SetSegmentName(name);
		seg->SetNumberOfElements(numberOfElements);
		for (int j=0; j<numberOfElements; j++ )
			{
			vtkHMNodeData *node = new vtkHMNodeData();
			double p[3];
			this->Points->GetPoint(nodeList->GetId(j), p);
			node->coords[0] = p[0];
			node->coords[1] = p[1];
			node->coords[2] = p[2];

			node->area = array->GetComponent(j, 0);
			node->radius = sqrt(node->area/vtkMath::Pi());
			node->alfa = array->GetComponent(j, 2);
			node->ResolutionArray = NULL;

			vtkHMElementData *elem = new vtkHMElementData();
			elem->Elastin = array->GetComponent(j, 4);
			elem->Collagen = array->GetComponent(j, 5);
			elem->CoefficientA = array->GetComponent(j, 6);
			elem->CoefficientB = array->GetComponent(j, 7);
			elem->Viscoelasticity = array->GetComponent(j, 8);
			elem->ViscoelasticityExponent = array->GetComponent(j, 9);
			elem->ReferencePressure = array->GetComponent(j, 10);
			elem->InfiltrationPressure = array->GetComponent(j, 11);
			elem->Permeability = array->GetComponent(j, 12);

			elem->idNode[0] = j;
			elem->idNode[1] = j+1;

			elem->WallThickness = 0;


			seg->SetElementData(j, *elem);
			seg->SetNodeData(j, *node);

			delete node;
			delete elem;
			}
		vtkHMNodeData *node = new vtkHMNodeData();
		double p[3];
		this->Points->GetPoint(nodeList->GetId(numberOfElements), p);
		node->coords[0] = p[0];
		node->coords[1] = p[1];
		node->coords[2] = p[2];

		node->area = array->GetComponent(numberOfElements-1, 0);
		node->radius = sqrt(node->area/vtkMath::Pi());
		node->alfa = array->GetComponent(numberOfElements-1, 2);
		node->ResolutionArray = NULL;
		seg->SetNodeData(numberOfElements, *node);
		delete node;

		seg->SetLength(10);
		this->SegmentMap.insert(DataPair(name,seg));
		array->Delete();
		nodeList->Delete();
		} // Fim for (int i=0; i<numberOfSegments; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadClipsParameters()
{
	//read data
	vtkDebugMacro(<<"Reading CLIPS data...");

	int numberOfClips;
	if(!this->Read(&numberOfClips))
		{
		vtkErrorMacro(<<"Can't read number of clips!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	for (int i=0; i<numberOfClips; i++ )
		{
		char *seg1 = new char[256];
		char *seg2 = new char[256];
		if(!this->ReadString(seg1) || !this->ReadString(seg2))
			{
			vtkErrorMacro(<<"Can't read segment name" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
//		int pointId;
//		if ( !this->Read(&pointId) )
//			{
//			vtkErrorMacro(<<"Can't read point id" << " for file: "
//							<< (this->FileName?this->FileName:"(Null FileName)"));
//			return 0;
//			}

		this->FirstSegmentsClip->InsertNextValue(seg1);
		this->SecondSegmentsClip->InsertNextValue(seg2);
		} // Fim for (int i=0; i<numberOfClips; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadStentParameters()
{
	//read data
	vtkDebugMacro(<<"Reading STENT data...");

	int numberOfStents;
	if(!this->Read(&numberOfStents))
		{
		vtkErrorMacro(<<"Can't read number of stents!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}


	int element, numberOfElements;
	double elastinFactor, viscoElasticityFactor;
	for (int i=0; i<numberOfStents; i++ )
		{
		char *name = new char[256];
		//ler nome do segmento com stent
		if(!this->ReadString(name))
			{
			vtkErrorMacro(<<"Can't read segment name" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//ler numero de elementos com stent
		if(!this->Read(&numberOfElements))
			{
			vtkErrorMacro(<<"Can't read number of elements!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		vtkIdList *stentList = vtkIdList::New();
		//ler lista de elementos com stent
		for ( int j=0; j<numberOfElements; j++ )
			{
			if(!this->Read(&element))
				{
				vtkErrorMacro(<<"Can't read elements!" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			stentList->InsertUniqueId(element);
			}

		//ler fatores multiplicativos de elastina e visco elasticidade
		if(!this->Read(&elastinFactor) || !this->Read(&viscoElasticityFactor))
			{
			vtkErrorMacro(<<"Can't read multiplicative factors!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//pegar o segmento
		vtkHM1DSegment *seg;
		DataMap::iterator itData = this->SegmentMap.begin();
		while ( itData != this->SegmentMap.end() )
			{
			if ( strcmp(itData->first, name)==0 )
				{
				seg = itData->second;
				break;
				}
			itData++;
			}

		for ( int j=0; j<numberOfElements; j++ )
			seg->AddStent(stentList->GetId(j), elastinFactor, viscoElasticityFactor);

		this->StentMap.insert(InterventionsPair(name, stentList));
		} // Fim for (int i=0; i<numberOfStent; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadStenosisParameters()
{
	//read data
	vtkDebugMacro(<<"Reading STENOSIS data...");

	int numberOfStenosis;
	if(!this->Read(&numberOfStenosis))
		{
		vtkErrorMacro(<<"Can't read number of Stenosis!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}


	int node, numberOfNodes;
	double percentage;
	for (int i=0; i<numberOfStenosis; i++ )
		{
		char *name = new char[256];
		//ler nome do segmento com Stenosis
		if(!this->ReadString(name))
			{
			vtkErrorMacro(<<"Can't read segment name" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//ler numero de nodes com Stenosis
		if(!this->Read(&numberOfNodes))
			{
			vtkErrorMacro(<<"Can't read number of nodes!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		vtkIdList *StenosisList = vtkIdList::New();
		//ler lista de nodes com Stenosis
		for ( int j=0; j<numberOfNodes; j++ )
			{
			if(!this->Read(&node))
				{
				vtkErrorMacro(<<"Can't read nodes!" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			StenosisList->InsertUniqueId(node);
			}

		//ler percentual de redução de área
		if(!this->Read(&percentage))
			{
			vtkErrorMacro(<<"Can't read percentage!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//pegar o segmento
		vtkHM1DSegment *seg;
		DataMap::iterator itData = this->SegmentMap.begin();
		while ( itData != this->SegmentMap.end() )
			{
			if ( strcmp(itData->first, name)==0 )
				{
				seg = itData->second;
				break;
				}
			itData++;
			}

		for ( int j=0; j<numberOfNodes; j++ )
			seg->AddStenosis(StenosisList->GetId(j), percentage);

		this->StenosisMap.insert(InterventionsPair(name, StenosisList));
		} // Fim for (int i=0; i<numberOfStenosis; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadAneurysmParameters()
{
	//read data
	vtkDebugMacro(<<"Reading ANEURYSM data...");

	int numberOfAneurysm;
	if(!this->Read(&numberOfAneurysm))
		{
		vtkErrorMacro(<<"Can't read number of Aneurysm!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}


	int node, numberOfNodes;
	double percentage;
	for (int i=0; i<numberOfAneurysm; i++ )
		{
		char *name = new char[256];
		//ler nome do segmento com Aneurysm
		if(!this->ReadString(name))
			{
			vtkErrorMacro(<<"Can't read segment name" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//ler numero de nodes com Aneurysm
		if(!this->Read(&numberOfNodes))
			{
			vtkErrorMacro(<<"Can't read number of nodes!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		vtkIdList *AneurysmList = vtkIdList::New();
		//ler lista de nodes com Aneurysm
		for ( int j=0; j<numberOfNodes; j++ )
			{
			if(!this->Read(&node))
				{
				vtkErrorMacro(<<"Can't read nodes!" << " for file: "
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			AneurysmList->InsertUniqueId(node);
			}

		//ler percentual de redução de área
		if(!this->Read(&percentage))
			{
			vtkErrorMacro(<<"Can't read percentage!" << " for file: "
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}

		//pegar o segmento
		vtkHM1DSegment *seg;
		DataMap::iterator itData = this->SegmentMap.begin();
		while ( itData != this->SegmentMap.end() )
			{
			if ( strcmp(itData->first, name)==0 )
				{
				seg = itData->second;
				break;
				}
			itData++;
			}

		for ( int j=0; j<numberOfNodes; j++ )
			seg->AddAneurysm(AneurysmList->GetId(j), percentage);

		this->AneurysmMap.insert(InterventionsPair(name, AneurysmList));
		} // Fim for (int i=0; i<numberOfAneurysm; i++ )

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::ReadAgingParameters()
{
	//read data
	vtkDebugMacro(<<"Reading AGING data...");

	if( !this->Read(&this->AgingElastinFactor) || !this->Read(&this->AgingViscoelasticityFactor) )
		{
		vtkErrorMacro(<<"Can't read aging parameters!" << " for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	return 1;
}

//----------------------------------------------------------------------------
const char *vtkHM1DHeMoLabReader::GetDataOutTimeArray()
{
	string Result;
	char *str;

	if ( !this->DataOut )
		return NULL;

	vtkDoubleArray *TimeArray = this->DataOut->GetInstantOfTime();

	if (TimeArray)
		{
		Result = vtkHMUtils::ConvertDoubleArrayToChar(TimeArray);
		str = new char[Result.size()+2];
		strcpy(str, Result.c_str());
		return (const char*)str;
		}
	else
		vtkErrorMacro("vtkHM1DHeMoLabReader::GetDataOutTimeArray Unable to find DataOut and get InstantOfTime array ");
	return Result.c_str(); // retorna string vazia

}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabReader::SetSimulationData(vtkHMDataOut *dataOut)
{
	//Setting data of the DataOut file
	int sum = 0;
	int meshId;

	this->DataOut = dataOut;

	vtkDoubleArray *array = dataOut->GetDegreeOfFreedomArray();

	if ( !array )
		return 0;

	vtkDebugMacro(<<"Setting data of the DataOut file.");

	vtkHM1DTerminal *term;

	vtkHM1DStraightModel::TreeNodeDataMap termMap =
	    this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	int numberOfPoints = array->GetNumberOfTuples()/this->DataOut->GetNumberOfBlock();
	int n = this->StraightModel->GetNumberOfNodes() + this->StraightModel->GetNumberOfTerminals();

	// se numero de pontos da arvore for diferente do numero do arquivo, não é possível ler os dados.
	if ( n != numberOfPoints )
		{
		vtkWarningMacro(<<"Error setting in simulation data, data not correspond the tree.");
		return 0;
		}

	//setar dados dos terminais
	for (it = termMap.begin(); it!= termMap.end(); it++)
		{
		//pega o terminal e seu mesh id
		term = vtkHM1DTerminal::SafeDownCast(it->second);
		meshId = term->GetmeshID();

		if ( meshId != -1 )
			{
			vtkDoubleArray *result = vtkDoubleArray::New();
			result->SetNumberOfComponents(array->GetNumberOfComponents());
			result->SetNumberOfTuples(dataOut->GetNumberOfBlock());

			sum = 0;

			//inserir dados no array do terminal
			for (int k=0; k<dataOut->GetNumberOfBlock(); k++)
				{
				result->SetTuple(k, array->GetTuple(sum+meshId-1));

				sum += numberOfPoints;
				}
			term->SetResolutionArray(result);
			result->Delete();
			}
		}

	vtkHM1DStraightModel::TreeNodeDataMap segmMap =
	    this->StraightModel->GetSegmentsMap();

	//setar dados dos nodes
	for (it = segmMap.begin(); it!= segmMap.end(); it++)
		{
		//pegar segmento
		vtkHM1DSegment *segm = vtkHM1DSegment::SafeDownCast(it->second);
		for (int i=0; i<segm->GetNodeNumber(); i++)
			{
			//pegar o mesh id do node
			meshId = segm->GetNodeData(i)->meshID;

			if ( meshId != -1 )
				{
				vtkDoubleArray *result = vtkDoubleArray::New();
				result->SetNumberOfComponents(array->GetNumberOfComponents());
				result->SetNumberOfTuples(dataOut->GetNumberOfBlock());

				sum = 0;
				//inserir dados no array dos nodes
				for (int k=0; k<dataOut->GetNumberOfBlock(); k++)
					{
					result->SetTuple(k, array->GetTuple(sum+meshId-1));

					sum += numberOfPoints;
					}
				segm->GetNodeData(i)->ResolutionArray = result;
				}
			}
		}

	vtkDoubleArray *time = dataOut->GetInstantOfTime();
	this->StraightModel->SetInstantOfTime(time);

	return 1;
}

void vtkHM1DHeMoLabReader::printTree(vtkHM1DTreeElement *root)
{
	vtkHM1DTreeElement *child = root->GetFirstChild();
	if ( this->StraightModel->IsSegment(root->GetId()) )
		{
		cout << "Segmento: " << root->GetId() << endl;
		while (child)
			{
			cout << "  " << child->GetId() << endl;
			child = root->GetNextChild();
			}
		}
	else if ( this->StraightModel->IsTerminal(root->GetId()) )
		{
		cout << "Terminal: " << root->GetId() << endl;
		while (child)
			{
			cout << "  " << vtkHM1DSegment::SafeDownCast(child)->GetSegmentName()<< endl;;
			child = root->GetNextChild();
			}
		}
	child = root->GetFirstChild();
	while (child)
		{
		printTree(child);
		child = root->GetNextChild();
		}
}

//----------------------------------------------------------------------------
void vtkHM1DHeMoLabReader::MakeStraightModel()
{
	const char *name;

	NamesMap::iterator it = this->ChildNameMap.begin();
	//Adicionar os segmentos no straight model
	while ( it != this->ChildNameMap.end() )
		{
		for ( int i=0; i<it->second->GetNumberOfValues(); i++ )
			{
			//pega o nome do segmento filho do terminal
			name = it->second->GetValue(i);
			//pega o terminal já inserido no straight model
			vtkHM1DTerminal *term = this->StraightModel->GetTerminal(it->first);

			//adiciona um segmento no straight model sem terminal como filho
			vtkIdType newSegId = this->StraightModel->AddSegmentWithoutChild(it->first);

			vtkHM1DSegment *seg = this->StraightModel->GetSegment(newSegId);

			DataMap::iterator itData = this->SegmentMap.begin();
			while ( itData != this->SegmentMap.end() )
				{
				if ( strcmp(itData->first, name)==0 )
					{
					seg->DeepCopy(itData->second);
					break;
					}
				itData++;
				}
			}

		it++;
		}

	//setar os terminais filhos do segmento
	it = this->ParentNameMap.begin();
	while ( it != this->ParentNameMap.end() )
		{
		vtkHM1DTerminal *term = this->StraightModel->GetTerminal(it->first);
		for ( int i=0; i<it->second->GetNumberOfValues(); i++ )
			{
			name = it->second->GetValue(i);

			vtkHM1DSegment *seg = this->StraightModel->GetSegment((char*)name);
			seg->AddChild(term);
			}

		it++;
		}

	//Adicionar clip no straight model
	const char *seg1, *seg2;
	for ( int i=0; i<this->FirstSegmentsClip->GetNumberOfValues(); i++ )
		{
		seg1 = this->FirstSegmentsClip->GetValue(i);
		seg2 = this->SecondSegmentsClip->GetValue(i);
		this->StraightModel->AddClip((char*)seg1, (char*)seg2);
		}

	//Adiconar stent no straight model
	InterventionsMap::iterator itIntervention = this->StentMap.begin();
	vtkIdType n=0;
	while ( itIntervention != this->StentMap.end() )
		{
		vtkHM1DSegment *seg = this->StraightModel->GetSegment((char*)itIntervention->first);
		this->StraightModel->AddStent(seg->GetId(), n, itIntervention->second);
		n++;
		itIntervention++;
		}

	//Adiconar estenose no straight model
	itIntervention = this->StenosisMap.begin();
	n=0;
	while ( itIntervention != this->StenosisMap.end() )
		{
		vtkHM1DSegment *seg = this->StraightModel->GetSegment((char*)itIntervention->first);
		this->StraightModel->AddStenosis(seg->GetId(), n, itIntervention->second);
		n++;
		itIntervention++;
		}

	//Adiconar aneurisma no straight model
	itIntervention = this->AneurysmMap.begin();
	n=0;
	while ( itIntervention != this->AneurysmMap.end() )
		{
		vtkHM1DSegment *seg = this->StraightModel->GetSegment((char*)itIntervention->first);
		this->StraightModel->AddAneurysm(seg->GetId(), n, itIntervention->second);
		n++;
		itIntervention++;
		}

	//setar os fatores de envelhecimento
	this->StraightModel->SetAgingFactor(this->AgingElastinFactor, this->AgingViscoelasticityFactor);

	//deletar os segmentos criados no reader
	DataMap::iterator itData = this->SegmentMap.begin();
	while ( itData != this->SegmentMap.end() )
		{
		itData->second->Delete();
		itData++;
		}
	this->SegmentMap.clear();
}

//----------------------------------------------------------------------------
vtkHM1DStraightModel *vtkHM1DHeMoLabReader::GetStraightModel()
{
	return this->StraightModel;
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DHeMoLabReader::GetStent(vtkIdType idStent)
{
	InterventionsMap::iterator itStent = this->StentMap.begin();
	int i=0;
	while ( itStent != this->StentMap.end() )
		{
		if ( i == idStent )
			{
			vtkHM1DSegment *seg = this->StraightModel->GetSegment(itStent->first);
			vtkIdList *l = vtkIdList::New();
			l->DeepCopy(itStent->second);
			l->InsertNextId(seg->GetId());
			return l;
			}
		itStent++;
		i++;
		}
	return NULL;
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DHeMoLabReader::GetStenosis(vtkIdType idStenosis)
{
	InterventionsMap::iterator itStenosis = this->StenosisMap.begin();
	int i=0;
	while ( itStenosis != this->StenosisMap.end() )
		{
		if ( i == idStenosis )
			{
			vtkHM1DSegment *seg = this->StraightModel->GetSegment(itStenosis->first);
			vtkIdList *l = vtkIdList::New();
			l->DeepCopy(itStenosis->second);
			l->InsertNextId(seg->GetId());
			return l;
			}
		itStenosis++;
		i++;
		}
	return NULL;
}

//----------------------------------------------------------------------------
vtkIdList *vtkHM1DHeMoLabReader::GetAneurysm(vtkIdType idAneurysm)
{
	InterventionsMap::iterator itAneurysm = this->AneurysmMap.begin();
	int i=0;
	while ( itAneurysm != this->AneurysmMap.end() )
		{
		if ( i == idAneurysm )
			{
			vtkHM1DSegment *seg = this->StraightModel->GetSegment(itAneurysm->first);
			vtkIdList *l = vtkIdList::New();
			l->DeepCopy(itAneurysm->second);
			l->InsertNextId(seg->GetId());
			return l;
			}
		itAneurysm++;
		i++;
		}
	return NULL;
}
