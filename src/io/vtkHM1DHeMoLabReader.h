#ifndef VTKHM1DHEMOLABREADER_H_
#define VTKHM1DHEMOLABREADER_H_

/*
 * $Id: vtkHM1DHeMoLabReader.h 2154 2007-07-03 12:49:53Z igor $
 */
/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeMoLabReader.h,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DHeMoLabReader - read HeMoLab informations
// .SECTION Description
// vtkHM1DHeMoLabReader is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The output of this reader is a single vtkHM1DStraightModelGrid data object.
// The superclass of this class, vtkDataReader, provides many methods for
// controlling the reading of the data file, see vtkDataReader for more
// information.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.
// .SECTION See Also
// vtkPolyData vtkDataReader vtkHM1DStraightModel vtkHM1DStraightModelGrid 

#include "vtkDataReader.h"
#include "vtkDoubleArray.h"
#include "vtkPoints.h"
#include "vtkStringArray.h"

#include <map>

class vtkHM1DStraightModel;
class vtkHM1DStraightModelGrid;
class vtkHM1DSegment;
class vtkHM1DTerminal;
class vtkHM1DTreeElement;
class vtkHMDataOut;

class VTK_EXPORT vtkHM1DHeMoLabReader : public vtkDataReader
{
public:
	static vtkHM1DHeMoLabReader *New();
	vtkTypeRevisionMacro(vtkHM1DHeMoLabReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
  //BTX
	typedef std::map<char *,vtkHM1DSegment *>  DataMap;
	typedef std::pair<char *,vtkHM1DSegment *> DataPair;
	
	typedef std::map<vtkIdType, vtkStringArray *> NamesMap;
	typedef std::pair<vtkIdType, vtkStringArray *> NamesPair;
	
	typedef std::map<char*, vtkIdList*> InterventionsMap;
	typedef std::pair<char*, vtkIdList*> InterventionsPair;
	//ETX
	void printTree(vtkHM1DTreeElement *root);
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	//ler os pontos, faces e os dados restantes do arquivo Mesh
	int ReadFile();
		
	// Desccription:
	// Read points
	int ReadPoints();
	
	// Description:
	// Read data
	int ReadData();
	
	// Description:
	// Read heart parameters.
	int ReadHeartParameters();
	
	// Description:
	// Read terminals parameters.
	int ReadTerminalsParameters();
	
	// Description:
	// Read segment parameters.
	int ReadSegmentsParameters();
	
	// Description:
	// Read clip parameters.
	int ReadClipsParameters();
	
	// Description:
	// Read stent parameters.
	int ReadStentParameters();

	// Description:
	// Read stenosis parameters.
	int ReadStenosisParameters();

	// Description:
	// Read aneurysm parameters.
	int ReadAneurysmParameters();
	
	int ReadAgingParameters();
	
	// Description:
	// Set data of the simulation, data read of the DataOut.txt
	int SetSimulationData(vtkHMDataOut *dataOut);
	
	const char *GetDataOutTimeArray();
	
	// Description:
	// Get the output of this reader.
	vtkHM1DStraightModelGrid *GetOutput();
	vtkHM1DStraightModelGrid *GetOutput(int idx);
	void SetOutput(vtkDataObject *output);
  
	// Description :
	// Get StraightModel
	vtkHM1DStraightModel *GetStraightModel();
	
	// Description:
	// Return number of stent.
	int GetNumberOfStent() { return StentMap.size(); }
	
	// Description:
	// Return list with ids of the elements with stent and the
	// last id is segment id.
	vtkIdList *GetStent(vtkIdType idStent);

	// Description:
	// Return number of stenosis.
	int GetNumberOfStenosis() { return StenosisMap.size(); }
	
	// Description:
	// Return list with ids of the nodes with stenosis and the
	// last id is segment id.
	vtkIdList *GetStenosis(vtkIdType idStenosis);

	// Description:
	// Return number of aneurysm.
	int GetNumberOfAneurysm() { return AneurysmMap.size(); }
	
	// Description:
	// Return list with ids of the nodes with aneurysm and the
	// last id is segment id.
	vtkIdList *GetAneurysm(vtkIdType idAneurysm);
	
protected:
	vtkHM1DHeMoLabReader();
	~vtkHM1DHeMoLabReader();

	virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);
  
	// Description:
	// Update extent of PolyData is specified in pieces.  
	// Since all DataObjects should be able to set UpdateExent as pieces,
	// just copy output->UpdateExtent  all Inputs.
	virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **,
                                  vtkInformationVector *);
  
	virtual int FillOutputPortInformation(int, vtkInformation*);
	
	// Description:
	// Make straight model with readed informations
	void MakeStraightModel();
	
	// Description:
	// Used by streaming: The extent of the output being processed
	// by the execute method. Set in the ComputeInputUpdateExtents method.
	int ExecutePiece;
  int ExecuteNumberOfPieces;
	int ExecuteGhostLevel;
	
	// Description:
	//Object of the tree 1D
	vtkHM1DStraightModel *StraightModel;
	
	vtkPoints *Points;
	
	// Descripiton:
	// Map with name of the segment like key and segment like data
	DataMap SegmentMap;
	
	// Description:
	// Map with id of the terminal like key and string array with names
	// of childs like data
	NamesMap ChildNameMap;
	
	// Description:
	// Map with id of the terminal like key and string array with names
	// of parents like data
	NamesMap ParentNameMap;
	
	// Description:
	// Arrays with names of first and second segments with clip.
	vtkStringArray *FirstSegmentsClip;
	vtkStringArray *SecondSegmentsClip;
	
	// Description:
	// Map with name of the segment like key and list with id of the
	// elements with stent like data. 
	InterventionsMap StentMap;
	
	// Description:
	// Map with name of the segment like key and list with id of the
	// nodes with stenosis like data. 
	InterventionsMap StenosisMap;
	
	// Description:
	// Map with name of the segment like key and list with id of the
	// nodes with aneurysm like data. 
	InterventionsMap AneurysmMap;
	
  // Description:
  // Factors of elastin and viscoelasticity for aging
  double AgingElastinFactor;
  double AgingViscoelasticityFactor;
  
  vtkHMDataOut *DataOut;
  
};

#endif /*VTKHM1DHEMOLABREADER_H_*/

