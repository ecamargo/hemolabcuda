/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeMoLabWriter.cxx,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DHeMoLabWriter.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkDataObject.h"
#include "vtkIdList.h"

#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DTreeElement.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DHeMoLabWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DHeMoLabWriter);

vtkHM1DStraightModelGrid* vtkHM1DHeMoLabWriter::GetInput()
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->Superclass::GetInput());
}

vtkHM1DStraightModelGrid* vtkHM1DHeMoLabWriter::GetInput(int port)
{
  return vtkHM1DStraightModelGrid::SafeDownCast(this->Superclass::GetInput(port));
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::FillInputPortInformation(int port, vtkInformation *info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkHM1DStraightModelGrid");
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::RequestData(
  vtkInformation* vtkNotUsed( request ),
  vtkInformationVector** inputVector,
  vtkInformationVector* vtkNotUsed( outputVector) )
{
  this->SetErrorCode(vtkErrorCode::NoError);
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkHM1DStraightModelGrid *input = 
  vtkHM1DStraightModelGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  this->SetStraightModel(input->GetStraightModel());
  
  this->WriteData();
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkHM1DHeMoLabWriter::WriteData()
{
  ostream *fp;
	
  vtkDebugMacro(<<"Writing HeMoLab data...");

  if ( !(fp=this->OpenFile()))
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if (!this->WritePoints(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteHeart(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteTerminals(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteSegments(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteClips(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteStents(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteStenosis(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteAneurysm(fp))
    {
    errorOccured = 1;
    }
  if (!this->WriteAging(fp))
    {
    errorOccured = 1;
    }
  
  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a data file. Returns NULL if error.
ostream *vtkHM1DHeMoLabWriter::OpenFile()
{
  ostream *fptr;
  vtkDataObject *input = this->GetInput();
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DHeMoLabWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}

//----------------------------------------------------------------------------
//Writing Points
int vtkHM1DHeMoLabWriter::WritePoints(ostream *fp)
{
	vtkDebugMacro(<<"Writing Points...");
	
	*fp << "*COORDINATES\n"; 
	
	string	buffer;
	
	vtkHM1DStraightModel::TreeNodeDataMap segMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it = segMap.begin();
	
	vtkHM1DSegment *seg;
	double p[3];
	char str[1024];
	int numberOfPoints = 0;
	while ( it != segMap.end() )
		{
		seg = vtkHM1DSegment::SafeDownCast(it->second);
		for ( int i=0; i<seg->GetNodeNumber(); i++ )
			{
			sprintf (str, "%.6f ", seg->GetNodeData(i)->coords[0]);
			buffer.append(str);
			sprintf (str, "%.6f ", seg->GetNodeData(i)->coords[1]);
			buffer.append(str);
			sprintf (str, "%.6f\n", seg->GetNodeData(i)->coords[2]);
			buffer.append(str);
			numberOfPoints++;
			}
		it++;
		}
	*fp << numberOfPoints << "\n";
	*fp << buffer ;
	buffer.clear();
	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteHeart(ostream *fp)
{
	vtkDebugMacro(<<"Writing heart data");
	
	vtkHM1DTerminal *heart = vtkHM1DTerminal::SafeDownCast(this->StraightModel->Get1DTreeRoot());
	vtkHM1DSegment *child = vtkHM1DSegment::SafeDownCast(heart->GetFirstChild());
	
	vtkHM1DTerminal::ResistanceValuesList *r1;
	vtkHM1DTerminal::ResistanceTimeList *r1Time;
	vtkHM1DTerminal::ResistanceValuesList *r2;
	vtkHM1DTerminal::ResistanceTimeList *r2Time;
	vtkHM1DTerminal::CapacitanceValuesList *c;
	vtkHM1DTerminal::CapacitanceTimeList *cTime;
	vtkHM1DTerminal::PressureValuesList *p;
	vtkHM1DTerminal::PressureValuesList *pTime;
	
	vtkHM1DTerminal::ResistanceValuesList::iterator it;
	vtkHM1DTerminal::ResistanceTimeList::iterator itTime;
	
	r1Time = heart->GetR1Time();
	r1 = heart->GetR1();
	r2Time = heart->GetR2Time();
	r2 = heart->GetR2();
	cTime = heart->GetCTime();
	c = heart->GetC();
	pTime = heart->GetPtTime();
	p = heart->GetPt();
	
	*fp << "\n*HEART\n"; 
	
	*fp << child->GetSegmentName() << "\n";
	*fp << 12 << "\n";
	
	char str[1024];
	string	buffer;
	
	itTime = r1Time->begin();
	it = r1->begin();
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	itTime++; it++;
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	
	itTime = r2Time->begin();
	it = r2->begin();
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	itTime++; it++;
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	
	itTime = cTime->begin();
	it = c->begin();
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	itTime++; it++;
	sprintf (str, "%.6f %.6f\n", *itTime, *it);
	buffer.append(str);
	
	itTime = pTime->begin();
	it = p->begin();
	
	//escreve o numero de pontos da curva de pressao
	sprintf (str, "%d\n", p->size());
	buffer.append(str);
	
	while ( itTime != pTime->end() )
		{
		sprintf (str, "%.6f %.6f\n", *itTime, *it);
		buffer.append(str);
		itTime++; it++;
		}
	
	*fp << buffer ;
	buffer.clear();
	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteTerminals(ostream *fp)
{
	vtkDebugMacro(<<"Writing terminals data");
	
	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator itTerm = termMap.begin();
	
	vtkHM1DTerminal *term;
	vtkHM1DSegment *segm;
	
	vtkHM1DTerminal::ResistanceValuesList *r1;
	vtkHM1DTerminal::ResistanceTimeList *r1Time;
	vtkHM1DTerminal::ResistanceValuesList *r2;
	vtkHM1DTerminal::ResistanceTimeList *r2Time;
	vtkHM1DTerminal::CapacitanceValuesList *c;
	vtkHM1DTerminal::CapacitanceTimeList *cTime;
	vtkHM1DTerminal::PressureValuesList *p;
	vtkHM1DTerminal::PressureValuesList *pTime;
	
	vtkHM1DTerminal::ResistanceValuesList::iterator it;
	vtkHM1DTerminal::ResistanceTimeList::iterator itTime;
	
	*fp << "\n*TERMINALS\n"; 
	
	*fp << termMap.size()-1 << "\n\n";
		
	char str[1024];
	string	buffer;
	
	while ( itTerm != termMap.end() )
		{
		term = vtkHM1DTerminal::SafeDownCast(itTerm->second);
		if ( term->GetId() != this->StraightModel->Get1DTreeRoot()->GetId() )
			{
			//escrever numero de pais
			*fp << term->GetNumberOfParents();
			segm = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
			//escrever o nome dos pais
			while ( segm )
				{
				*fp << " " << segm->GetSegmentName();
				segm = vtkHM1DSegment::SafeDownCast(term->GetNextParent());
				}
			
			//escrever numero de filhos
			*fp << "\n" << term->GetChildCount();
			segm = vtkHM1DSegment::SafeDownCast(term->GetFirstChild());
			//escrever o nome dos filhos
			while ( segm )
				{
				*fp << " " << segm->GetSegmentName();
				segm = vtkHM1DSegment::SafeDownCast(term->GetNextChild());
				}
			*fp << "\n";
			
			r1 = term->GetR1();
			r2 = term->GetR2();
			c = term->GetC();
			p = term->GetPt();
			
			sprintf (str, "%d\n", 4);
			buffer.append(str);
			// escrever parametros de resistencia 1 e 2, capacitancia e pressao
			sprintf (str, "%.6f %.6f %.6f %.6f\n", *r1->begin(), *r2->begin(), *c->begin(), *p->begin());
			buffer.append(str);
			
			*fp << buffer ;
			buffer.clear();
			*fp << "\n";
			}
		itTerm++;
		}
	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteSegments(ostream *fp)
{
	vtkDebugMacro(<<"Writing segments data");
	
	vtkHM1DStraightModel::TreeNodeDataMap segmMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator itSegm = segmMap.begin();
	
	vtkHM1DTerminal *term;
	vtkHM1DSegment *segm;
	vtkHMElementData *elem;
	vtkHMNodeData *node1, *node2;
	
	
	*fp << "\n*SEGMENTS\n"; 
	
	*fp << segmMap.size() << "\n\n";
		
	char str[1024];
	string	buffer;
	int countPoints = 0;
	vtkIdList *l = vtkIdList::New();
	
	while ( itSegm != segmMap.end() )
		{
		segm = vtkHM1DSegment::SafeDownCast(itSegm->second);
		
		//escreve o nome do segmento e o numero de elementos
		*fp << segm->GetSegmentName() << " " << segm->GetElementNumber() << "\n";
		
		for ( int i=0; i<segm->GetElementNumber(); i++ )
			{
			//pega o elemento e os nodes
			elem = segm->GetElementData(i);
			node1 = segm->GetNodeData(i);
			node2 = segm->GetNodeData(i+1);
			
//			*fp << node1->area << " " << node2->area << " " << node1->alfa << " " << node2->alfa  << " ";
//			
//			*fp << elem->Elastin << " " << elem->Collagen << " " << elem->CoefficientA << " " << elem->CoefficientB << " " <<
//									elem->Viscoelasticity << " " << elem->ViscoelasticityExponent << " " <<
//									 elem->ReferencePressure << " " << elem->InfiltrationPressure << " " << elem->Permeability << "\n";
			//escrever area e alfa dos nós esquerdo e direito do elemento
			sprintf (str, "%f %f %f %f ", node1->area, node2->area, node1->alfa, node2->alfa);
			buffer.append(str);
			//escreve as propriedades do elemento
			sprintf (str, "%e %e %e %e %e %e %e %e %e\n", elem->Elastin, elem->Collagen, elem->CoefficientA, elem->CoefficientB,
																										elem->Viscoelasticity, elem->ViscoelasticityExponent,
																										 elem->ReferencePressure,elem->InfiltrationPressure,elem->Permeability);
			
			buffer.append(str);
			l->InsertNextId(countPoints);
			countPoints++;
			}
		l->InsertNextId(countPoints);
		countPoints++;
		for ( int i=0; i<l->GetNumberOfIds(); i++ )
			{
			sprintf (str, "%d ", l->GetId(i));
			buffer.append(str);
			}
		sprintf (str, "\n");
		buffer.append(str);
		
		l->Reset();
		
		*fp << buffer ;
		buffer.clear();
		*fp << "\n";
		itSegm++;
		}
	l->Delete();
	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteClips(ostream *fp)
{
	vtkDebugMacro(<<"Writing clips data");
	
	if ( this->StraightModel->GetNumberOfClips() < 1 )
		return 1;
	
	*fp << "\n*CLIPS\n"; 
	
	*fp << this->StraightModel->GetNumberOfClips() << "\n\n";
	
	vtkIdType idSegm[2];
	char *name1, *name2;
	for ( int i=0; i<this->StraightModel->GetNumberOfClips(); i++ )
		{
		this->StraightModel->GetClip(i, idSegm);
		name1 = this->StraightModel->GetSegment(idSegm[0])->GetSegmentName();
		name2 = this->StraightModel->GetSegment(idSegm[1])->GetSegmentName();
		*fp << name1 << " " << name2 << "\n\n";
		}
	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteStents(ostream *fp)
{
	vtkDebugMacro(<<"Writing stents data");
	
	if ( this->StraightModel->GetNumberOfStents() < 1 )
		return 1;
	
	*fp << "\n*STENTS\n"; 
	
	*fp << this->StraightModel->GetNumberOfStents() << "\n\n";
	
	char *name;
	vtkIdList *l;
	vtkHM1DSegment *seg;
	vtkHM1DStraightModel::InterventionsInformationMultimap multiMap = this->StraightModel->GetStentsInformation();
	vtkHM1DStraightModel::InterventionsInformationMultimap::iterator itMulti = multiMap.begin();
  
  while ( itMulti != multiMap.end() )
  	{
  	//pega o segmento com stent
  	seg = this->StraightModel->GetSegment(itMulti->first);
  	name = seg->GetSegmentName();
  	//pega a lista com os ids dos elementos com stent
  	l = this->StraightModel->GetStent(itMulti->second);
  	//escreve o nome do segmento e o numero de elementos com stent
  	*fp << name << " " << l->GetNumberOfIds() << "\n";
  	//escreve os ids dos elementos
  	for ( int i=0; i<l->GetNumberOfIds(); i++ )
  		{
  		*fp << l->GetId(i) << " ";
  		}
  	//escreve os fatores de elastina e viscoelasticidade
  	*fp << "\n" << seg->GetElementData(l->GetId(0))->ElastinStentFactor << " " << seg->GetElementData(l->GetId(0))->ViscoelasticityStentFactor;
  	*fp << "\n\n";
  	itMulti++;
  	}
  	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteStenosis(ostream *fp)
{
	vtkDebugMacro(<<"Writing stenosis data");
	
	if ( this->StraightModel->GetNumberOfStenosis() < 1 )
		return 1;
	
	*fp << "\n*STENOSIS\n"; 
	
	*fp << this->StraightModel->GetNumberOfStenosis() << "\n\n";
	
	char *name;
	vtkIdList *l;
	vtkHM1DSegment *seg;
	vtkHM1DStraightModel::InterventionsInformationMultimap multiMap = this->StraightModel->GetStenosisInformation();
	vtkHM1DStraightModel::InterventionsInformationMultimap::iterator itMulti = multiMap.begin();
  
  while ( itMulti != multiMap.end() )
  	{
  	//pega o segmento com stenosis
  	seg = this->StraightModel->GetSegment(itMulti->first);
  	name = seg->GetSegmentName();
  	//pega a lista com os ids dos elementos com stenosis
  	l = this->StraightModel->GetStenosis(itMulti->second);
  	//escreve o nome do segmento e o numero de elementos com stenosis
  	*fp << name << " " << l->GetNumberOfIds() << "\n";
  	//escreve os ids dos elementos
  	for ( int i=0; i<l->GetNumberOfIds(); i++ )
  		{
  		*fp << l->GetId(i) << " ";
  		}
  	//escreve o percentual de redução de área
  	*fp << "\n" << seg->GetNodeData(l->GetId(0))->PercentageToCalculateStenosis;
  	*fp << "\n\n";
  	itMulti++;
  	}
  	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteAneurysm(ostream *fp)
{
	vtkDebugMacro(<<"Writing aneurysm data");
	
	if ( this->StraightModel->GetNumberOfAneurysm() < 1 )
		return 1;
	
	*fp << "\n*ANEURYSM\n";
	
	*fp << this->StraightModel->GetNumberOfAneurysm() << "\n\n";
	
	char *name;
	vtkIdList *l;
	vtkHM1DSegment *seg;
	vtkHM1DStraightModel::InterventionsInformationMultimap multiMap = this->StraightModel->GetAneurysmInformation();
	vtkHM1DStraightModel::InterventionsInformationMultimap::iterator itMulti = multiMap.begin();
  
  while ( itMulti != multiMap.end() )
  	{
  	//pega o segmento com aneurisma
  	seg = this->StraightModel->GetSegment(itMulti->first);
  	name = seg->GetSegmentName();
  	//pega a lista com os ids dos elementos com aneurisma
  	l = this->StraightModel->GetAneurysm(itMulti->second);
  	//escreve o nome do segmento e o numero de elementos com aneurisma
  	*fp << name << " " << l->GetNumberOfIds() << "\n";
  	//escreve os ids dos elementos
  	for ( int i=0; i<l->GetNumberOfIds(); i++ )
  		{
  		*fp << l->GetId(i) << " ";
  		}
  	//escreve o percentual de aumento de área
  	*fp << "\n" << seg->GetNodeData(l->GetId(0))->PercentageToCalculateAneurysm;
  	*fp << "\n\n";
  	itMulti++;
  	}
  	
	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DHeMoLabWriter::WriteAging(ostream *fp)
{
	vtkDebugMacro(<<"Writing aging data");
	double factor[2];
	this->StraightModel->GetAgingFactor(factor);
	
	if ( (factor[0] == 0) && (factor[1] == 0))
		return 1;
	
	*fp << "\n*AGING\n";
	
	*fp << factor[0] << " " << factor[1] << "\n\n";
	  	
	return 1;
}

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM1DHeMoLabWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkHM1DHeMoLabWriter::SetStraightModel( vtkHM1DStraightModel* s )
{
	this->StraightModel = s;
}

//----------------------------------------------------------------------------
vtkHM1DStraightModel *vtkHM1DHeMoLabWriter::GetStraightModel()
{
	return this->StraightModel;
}
