/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeMoLabWriter.h,v $

  Copyright (c) Igor Freitas
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DHeMoLabWriter - write HeMoLab data
// .SECTION Description
// vtkHM1DHeMoLabWriter is a source object that writes ASCII 
// data files in HeMoLab format. See text for format details.

#ifndef VTKHM1DHEMOLABWRITER_H_
#define VTKHM1DHEMOLABWRITER_H_

#include <string>
using namespace std;
//#include <vtksys/stl/string>
#include "vtkDataWriter.h"

class vtkDataObject;

class vtkHM1DStraightModelGrid;
class vtkHM1DStraightModel;

class VTK_EXPORT vtkHM1DHeMoLabWriter : public vtkDataWriter
{
public:
  static vtkHM1DHeMoLabWriter *New();
  vtkTypeRevisionMacro(vtkHM1DHeMoLabWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Get the input to this writer.
  vtkHM1DStraightModelGrid* GetInput();
  vtkHM1DStraightModelGrid* GetInput(int port);
  
  // Description:
  // Set/Get straight model.
  void SetStraightModel( vtkHM1DStraightModel* s );
  vtkHM1DStraightModel* GetStraightModel();
  
  // Description:
  // Write data.
  void WriteData();
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile();
		
	// Description:
  // Close a file.
  void CloseFile(ostream *fp);
  
  // Description:
  // Write out the points of the data set.
  int WritePoints(ostream *fp);

  // Description:
  // Write heart data.
  int WriteHeart(ostream *fp);

  // Description:
  // Write terminals data.
  int WriteTerminals(ostream *fp);

  // Description:
  // Write segments data.
  int WriteSegments(ostream *fp);

  // Description:
  // Write clips data.
  int WriteClips(ostream *fp);

  // Description:
  // Write stents data.
  int WriteStents(ostream *fp);

  // Description:
  // Write stenosis data.
  int WriteStenosis(ostream *fp);

  // Description:
  // Write aneurysm data.
  int WriteAneurysm(ostream *fp);

  // Description:
  // Write aging data.
  int WriteAging(ostream *fp);
  
protected:
  vtkHM1DHeMoLabWriter() {};
  ~vtkHM1DHeMoLabWriter() {};
  
  // This is called by the superclass.
  // This is the method you should override.
  virtual int RequestData(vtkInformation *request,
                           vtkInformationVector** inputVector,
                           vtkInformationVector* outputVector);

  virtual int FillInputPortInformation(int port, vtkInformation *info);
  
  vtkHM1DStraightModel *StraightModel;
  
};


#endif /*VTKHM1DHEMOLABWRITER_H_*/
