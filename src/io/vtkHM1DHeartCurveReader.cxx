/*
 * $Id: vtkHM1DHeartCurveReader.cxx 1330 2006-10-06 12:50:53Z igor $
 */
#include "vtkHM1DHeartCurveReader.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkDoubleArray.h"

#include <fstream>
#include <sys/stat.h>


vtkCxxRevisionMacro(vtkHM1DHeartCurveReader, "$Revision: 1330 $");
vtkStandardNewMacro(vtkHM1DHeartCurveReader);

vtkHM1DHeartCurveReader::vtkHM1DHeartCurveReader()
{
	this->R1 = vtkDoubleArray::New();
	this->R2 = vtkDoubleArray::New();
	this->C = vtkDoubleArray::New();
	this->P = vtkDoubleArray::New();
	this->R1Time = vtkDoubleArray::New();
	this->R2Time = vtkDoubleArray::New();
	this->CTime = vtkDoubleArray::New();
	this->PTime = vtkDoubleArray::New();
	
	this->Generic = vtkDoubleArray::New();
	this->GenericTime = vtkDoubleArray::New();
	
	this->SingleCurve = false;
}

//----------------------------------------------------------------------------
vtkHM1DHeartCurveReader::~vtkHM1DHeartCurveReader()
{
	this->R1->Delete();
	this->R2->Delete();
	this->C->Delete();
	this->P->Delete();
	this->R1Time->Delete();
	this->R2Time->Delete();
	this->CTime->Delete();
	this->PTime->Delete();

	this->Generic->Delete();
	this->GenericTime->Delete();

}

//----------------------------------------------------------------------------
void vtkHM1DHeartCurveReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
// Read Param file
int vtkHM1DHeartCurveReader::ReadFile(char *file)
{
	vtkDebugMacro(<<"Reading file");
	
	this->SetFileName(file);
	
	if ( !this->OpenFile() )
	{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    	return 0;
	}
	
	if ( !this->ReadParameters() )
	{
		vtkErrorMacro(<<"Can't read parameters. " << this->GetFileName());
    	return 0;
	}
		
	this->CloseFile();
	
	return 1;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DHeartCurveReader::CloseFile()
{
	vtkDebugMacro(<<"Closing file");
	if ( this->IS != NULL )
	{
		delete this->IS;
	}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Open File
int vtkHM1DHeartCurveReader::OpenFile()
{
	if (this->ReadFromInputString)
	{
		if (this->InputArray)
		{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
		}
		else if (this->InputString)
		{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
		}
	}
	else
	{
		vtkDebugMacro(<< "Opening file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
		{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
		}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
			
		return 1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
//Read Read Parameters
int vtkHM1DHeartCurveReader::ReadParameters()
{
	double num=0;
	int quantityOfParameters;
	
	vtkDebugMacro(<< "Reading Parameters");
	
	//Ler dados de resistencia 1
	if(!this->Read(&quantityOfParameters))
		{
			vtkErrorMacro(<<"Can't read the quantity of parameters of the resistence 1!" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	
	if ( quantityOfParameters == 0 )
		{
		quantityOfParameters = 1;
		this->R1->SetNumberOfTuples(quantityOfParameters);
		this->R1Time->SetNumberOfTuples(quantityOfParameters);
		
		this->R1Time->SetValue(0, 0);
		this->R1->SetValue(0, 0);
		}
	else
		{
		this->R1->SetNumberOfTuples(quantityOfParameters);
		this->R1Time->SetNumberOfTuples(quantityOfParameters);
		
		for ( int j=0; j<quantityOfParameters; j++ )
			{
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of time of the resistence 1!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->R1Time->SetValue(j, num);
			
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of the resistence 1!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->R1->SetValue(j, num);
			}
		}
	
	//Ler dados de resistencia 2
	if(!this->Read(&quantityOfParameters))
		{
			//vtkErrorMacro(<<"Can't read the quantity of parameters of the resistence 2!" << "! for file: " 
			//				<< (this->FileName?this->FileName:"(Null FileName)"));
			
			// se nao achar curva do R2, entao este arquivo possui somente uma curva
			this->Generic->DeepCopy(this->R1);
			this->GenericTime->DeepCopy(this->R1Time);
			this->SingleCurve = true;
			// aborta a leitura aqui
			return 1;
		}
	
	if ( quantityOfParameters == 0 )
		{
		quantityOfParameters = 1;
		this->R2->SetNumberOfTuples(quantityOfParameters);
		this->R2Time->SetNumberOfTuples(quantityOfParameters);
		
		this->R2Time->SetValue(0, 0);
		this->R2->SetValue(0, 0);
		}
	else
		{
		this->R2->SetNumberOfTuples(quantityOfParameters);
		this->R2Time->SetNumberOfTuples(quantityOfParameters);
			
		for ( int j=0; j<quantityOfParameters; j++ )
			{
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of time of the resistence 2!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->R2Time->SetValue(j, num);
			
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of the resistence 2!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->R2->SetValue(j, num);
			}
		}
	
	//Ler dados de capacitancia
	if(!this->Read(&quantityOfParameters))
		{
			vtkErrorMacro(<<"Can't read the quantity of parameters of the capacitance!" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	
	if ( quantityOfParameters == 0 )
		{
		quantityOfParameters = 1;
		this->C->SetNumberOfTuples(quantityOfParameters);
		this->CTime->SetNumberOfTuples(quantityOfParameters);
		
		this->CTime->SetValue(0, 0);
		this->C->SetValue(0, 0);
		}
	else
		{
		this->C->SetNumberOfTuples(quantityOfParameters);
		this->CTime->SetNumberOfTuples(quantityOfParameters);
			
		for ( int j=0; j<quantityOfParameters; j++ )
			{
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of time of the capacitance!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->CTime->SetValue(j, num);
			
			if(!this->Read(&num))
				{
				vtkErrorMacro(<<"can't read the parameters of the capacitance!" << "! for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			this->C->SetValue(j, num);
			}
		}
	
	//Ler dados de pressao
	if(!this->Read(&quantityOfParameters))
		{
			vtkErrorMacro(<<"Can't read the quantity of parameters of the pressure" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	
	this->P->SetNumberOfTuples(quantityOfParameters);
	this->PTime->SetNumberOfTuples(quantityOfParameters);
	
	for ( int j=0; j<quantityOfParameters; j++ )
		{
		if(!this->Read(&num))
			{
			vtkErrorMacro(<<"can't read the parameters of time of the pressure!" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->PTime->SetValue(j, num);
		
		if(!this->Read(&num))
			{
			vtkErrorMacro(<<"can't read the parameters of the pressure!" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		this->P->SetValue(j, num);
		}
	
	return 1;
}

