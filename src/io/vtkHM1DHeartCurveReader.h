/*
 * $Id: vtkHM1DHeartCurveReader.h 1330 2006-10-06 12:50:53Z igor $
 */

#ifndef VTKHM1DHEARTCURVEREADER_H_
#define VTKHM1DHEARTCURVEREADER_H_

#include "vtkDataReader.h"

class vtkDoubleArray;

class VTK_EXPORT vtkHM1DHeartCurveReader : public vtkDataReader
{
public:
	static vtkHM1DHeartCurveReader *New();
	vtkTypeRevisionMacro(vtkHM1DHeartCurveReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
		
	// Description:
	// Read the data of the file
	int ReadFile(char *file);
	
	// Descripiton:
	// Read parameters of file
	int ReadParameters();
	
	// Description:
	// Get resistence 1
	vtkGetObjectMacro(R1, vtkDoubleArray);
	
	// Description:
	// Get time resistence 1
	vtkGetObjectMacro(R1Time, vtkDoubleArray);
	
	// Description:
	// Get resistence 2
	vtkGetObjectMacro(R2, vtkDoubleArray);
	
	// Description:
	// Get time resistence 2
	vtkGetObjectMacro(R2Time, vtkDoubleArray);
	
	// Description:
	// Get capacitance
	vtkGetObjectMacro(C, vtkDoubleArray);
	
	// Description:
	// Get capacitance
	vtkGetObjectMacro(CTime, vtkDoubleArray);
	
	// Description:
	// Get pressure
	vtkGetObjectMacro(P, vtkDoubleArray);
	
	// Description:
	// Get time pressure
	vtkGetObjectMacro(PTime, vtkDoubleArray);
	
	// Description:
	// Get generic
	vtkGetObjectMacro(Generic, vtkDoubleArray);
	
	// Description:
	// Get Generic time
	vtkGetObjectMacro(GenericTime, vtkDoubleArray);
	
	// Description:
	// Get Single Curve
	vtkGetMacro(SingleCurve, bool);
	
	
protected:
	vtkHM1DHeartCurveReader();
	~vtkHM1DHeartCurveReader();
	
	// Description:
	// Array of resistence 1
	vtkDoubleArray *R1;
	vtkDoubleArray *R1Time;
	
	// Description:
	// Array of resistence 2
	vtkDoubleArray *R2;
	vtkDoubleArray *R2Time;
	
	// Description:
	// Array of capacitance
	vtkDoubleArray *C;
	vtkDoubleArray *CTime;
	
	// Description:
	// Array of pressure
	vtkDoubleArray *P;
	vtkDoubleArray *PTime;
	
	
	// Description:
	// Array of Generic Data
	vtkDoubleArray *Generic;
	vtkDoubleArray *GenericTime;

	// Description:	
	// indicates if the file has only one curve
	bool SingleCurve;
	
	
	
}; //End class

#endif /*VTKHM1DHEARTCURVEREADER_H_*/
