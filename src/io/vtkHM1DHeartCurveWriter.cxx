/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeartCurveWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DHeartCurveWriter.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkDoubleArray.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DHeartCurveWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DHeartCurveWriter);

//----------------------------------------------------------------------------
void vtkHM1DHeartCurveWriter::WriteData(char *file, vtkDoubleArray *array)
{
  ostream *fp;
	
	this->SetFileName(file);
	
  vtkDebugMacro(<<"Writing data...");

  if ( !(fp=this->OpenFile(array)) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if ( !this->WriteParameters(fp, array) )
    {
    errorOccured = 1;
    }


  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a Param data file. Returns NULL if error.
ostream *vtkHM1DHeartCurveWriter::OpenFile(vtkDataArray *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DHeartCurveWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}


//----------------------------------------------------------------------------
//Writing data Parameters
int vtkHM1DHeartCurveWriter::WriteParameters(ostream *fp, vtkDoubleArray *array)
{
  vtkDebugMacro(<<"Writing data Parameters...");
	
	int i=0;
	//resistencia 1
	int n = (int)array->GetValue(i);
	
	*fp << n << "\n";
	for ( int j=0; j<n; j++ )
		{
		i++;
		*fp << array->GetValue(i) << " ";
		i++;
		*fp << array->GetValue(i) << "\n";
		
		}
	
	*fp << "\n";
	
	//resistencia 2
	i++;
	n = (int)array->GetValue(i);
	
	*fp << n << "\n";
	for ( int j=0; j<n; j++ )
		{
		i++;
		*fp << array->GetValue(i) << " ";
		i++;
		*fp << array->GetValue(i) << "\n";
		}
	
	*fp << "\n";
	
	//capacitancia
	i++;
	n = (int)array->GetValue(i);
	
	*fp << n << "\n";
	for ( int j=0; j<n; j++ )
		{
		i++;
		*fp << array->GetValue(i) << " ";
		i++;
		*fp << array->GetValue(i) << "\n";
		}
	
	*fp << "\n";
	
	//pressao
	i++;
	n = (int)array->GetValue(i);
	
	*fp << n << "\n";
	for ( int j=0; j<n; j++ )
		{
		i++;
		*fp << array->GetValue(i) << " ";
		i++;
		*fp << array->GetValue(i) << "\n";
		}
	
  fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
}

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM1DHeartCurveWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

