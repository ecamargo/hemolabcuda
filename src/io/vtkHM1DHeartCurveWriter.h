/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DHeartCurveWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DHeartCurveWriter - write heart curve data
// .SECTION Description


#ifndef VTKHM1DHEARTCURVEWRITER_H_
#define VTKHM1DHEARTCURVEWRITER_H_

#include "vtkDataWriter.h"

class vtkDataObject;
class vtkDoubleArray;

class VTK_EXPORT vtkHM1DHeartCurveWriter : public vtkDataWriter
{
public:
  static vtkHM1DHeartCurveWriter *New();
  vtkTypeRevisionMacro(vtkHM1DHeartCurveWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void WriteData(char *dir, vtkDoubleArray *array);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataArray *input);
		
	// Description:
  // Close file.
  void CloseFile(ostream *fp);
  
  
  // Description:
	// Write parameters
	int WriteParameters(ostream *fp, vtkDoubleArray *array);
	
  
protected:
  vtkHM1DHeartCurveWriter() {};
  ~vtkHM1DHeartCurveWriter() {};


};


#endif /*VTKHM1DHEARTCURVEWRITER_H_*/
