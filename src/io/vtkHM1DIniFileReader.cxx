/*
 * $Id: vtkHM1DIniFileReader.cxx 2469 2008-01-09 18:54:52Z igor $
 */
#include "vtkHM1DIniFileReader.h"
#include "vtkHM1DIniFile.h"
#include "vtkHM1DMesh.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkDoubleArray.h"

#include <fstream>

#include <sys/stat.h>


vtkCxxRevisionMacro(vtkHM1DIniFileReader, "$Revision: 2469 $");
vtkStandardNewMacro(vtkHM1DIniFileReader);

vtkHM1DIniFileReader::vtkHM1DIniFileReader()
{
	vtkDebugMacro(<<"Reading IniFile data...");
	this->IniFile = vtkHM1DIniFile::New();
}

//----------------------------------------------------------------------------
vtkHM1DIniFileReader::~vtkHM1DIniFileReader()
{
	this->IniFile->Delete();
}

//----------------------------------------------------------------------------
void vtkHM1DIniFileReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
int vtkHM1DIniFileReader::ReadIniFile(char *file, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<<"Reading IniFile file");	
	
	this->SetFileName(file);
	
	if ( !this->OpenFile() )
	{
//		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    	return 0;
	}
	
	if ( !this->ReadInitialConditions(mesh) )
	{
		vtkErrorMacro(<<"Can't read Initial Conditions. " << this->GetFileName());
    	return 0;
	}
	
	
	this->CloseFile();
	
	return 1;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DIniFileReader::CloseFile()
{
	vtkDebugMacro(<<"Closing IniFile file");
	if ( this->IS != NULL )
	{
		delete this->IS;
	}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Open File
int vtkHM1DIniFileReader::OpenFile()
{
	if (this->ReadFromInputString)
	{
		if (this->InputArray)
		{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
		}
		else if (this->InputString)
		{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
		}
	}
	else
	{
		vtkDebugMacro(<< "Opening IniFile file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
		{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
		}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
		{
//			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
			
		return 1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
//Read Initial Conditions
int vtkHM1DIniFileReader::ReadInitialConditions(vtkHM1DMesh *mesh)
{
	char line[256];
	
	vtkDebugMacro(<< "Reading IniFile Initial Conditions");
	
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading Initial Conditions line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*Initial", line, 30) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	
	this->ReadString(line);

	vtkDoubleArray *initialConditions = vtkDoubleArray::New();
	initialConditions->SetNumberOfComponents(mesh->GetDegreeOfFreedom());
	initialConditions->SetNumberOfTuples(mesh->GetNumberOfPoints());
	
	double *num = new double[mesh->GetDegreeOfFreedom()];
	for ( int i=0; i<mesh->GetNumberOfPoints(); i++ )
	{
		for ( int j=0; j<mesh->GetDegreeOfFreedom(); j++ )
		{
			if(!this->Read(&num[j]))
			{
				vtkErrorMacro(<<"Can't read initial conditions!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				initialConditions->Delete();
				return 0;
			}
		}
		initialConditions->SetTupleValue(i, num);
	}
	this->IniFile->SetInitialConditions(initialConditions);
	initialConditions->Delete();
		
	double CurrentTimeStep, DeltaT; 
	
	this->ReadString(line);
	
	if ( strncmp ("*Time", line, 30) )
	{
//	    vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
//		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 1;
	  
	}
	this->ReadLine(line);
	
	if(!this->Read(&CurrentTimeStep))
	{
		vtkErrorMacro(<<"Can't read Current Time Step!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	
	
	if(!this->Read(&DeltaT))
	{
		vtkErrorMacro(<<"Can't read Delta Time" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	
	this->IniFile->SetCurrentTimeStep(CurrentTimeStep);
	this->IniFile->SetDeltaT(DeltaT);
	// ********************************************
	
	float progress=this->GetProgress();
	this->UpdateProgress(progress + 0.5*(1.0 - progress));
	  
	return 1;
}

void vtkHM1DIniFileReader::SetIniFile(vtkHM1DIniFile *IF)
{
	this->IniFile = IF;
}

vtkHM1DIniFile *vtkHM1DIniFileReader::GetIniFile()
{
	return this->IniFile;
}
