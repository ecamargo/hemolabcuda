/*
 * $Id: vtkHM1DIniFileReader.h 2469 2008-01-09 18:54:52Z igor $
 */
#ifndef VTKHM1DINIFILEREADER_H_
#define VTKHM1DINIFILEREADER_H_

#include "vtkDataReader.h"

#include "vtkHM1DMesh.h"

class vtkHM1DIniFile;
	
class VTK_EXPORT vtkHM1DIniFileReader : public vtkDataReader
{
public:
	static vtkHM1DIniFileReader *New();
	vtkTypeRevisionMacro(vtkHM1DIniFileReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:	
	// Read the data of the IniFile.txt file
	int ReadIniFile(char *file, vtkHM1DMesh *mesh);
	
	// Description:
	// Read the initial conditions of the round
	int ReadInitialConditions(vtkHM1DMesh *mesh);
	
	//double *GetInitialConditions();	
	
	void SetIniFile(vtkHM1DIniFile *IF);
	vtkHM1DIniFile *GetIniFile();
	
protected:
	vtkHM1DIniFileReader();
	~vtkHM1DIniFileReader();
	
	vtkHM1DIniFile *IniFile;
	
}; //End class

#endif /*VTKHM1DIniFileREADER_H_*/
