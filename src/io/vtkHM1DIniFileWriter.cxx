/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DIniFileWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DIniFileWriter.h"
#include "vtkHM1DIniFile.h"
#include "vtkHM1DMesh.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkDoubleArray.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DIniFileWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DIniFileWriter);

//----------------------------------------------------------------------------
void vtkHM1DIniFileWriter::WriteIniFileData(vtkHM1DIniFile *IniFile, char *dir)
{
  ostream *fp;
	//this->DebugOn();
	char d[256];
	strcpy(d, dir);
	strcat(d, "/IniFile.txt");
	
	this->SetFileName(d);
	
  vtkDebugMacro(<<"Writing IniFile data...");

  if ( !(fp=this->OpenFile(IniFile)) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if ( !this->WriteInitialConditions(fp, IniFile) )
    {
    errorOccured = 1;
    }

	*fp << buffer ;
	buffer.clear();

  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a IniFile data file. Returns NULL if error.
ostream *vtkHM1DIniFileWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening IniFile file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close a IniFile file.
void vtkHM1DIniFileWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing IniFile file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}

//----------------------------------------------------------------------------
//Writing block of the data RealIniFileeters
int vtkHM1DIniFileWriter::WriteInitialConditions(ostream *fp, vtkHM1DIniFile *IniFile)
{
  vtkDebugMacro(<<"Writing IniFile Initial Conditions...");

  //*fp << "*Initial Conditions\n";
  buffer.append("*Initial Conditions\n");
  
  vtkDoubleArray *initialConditions = IniFile->GetInitialConditions();
	
	double *num;
	char str[1024];
	
	for ( int i=0; i<initialConditions->GetNumberOfTuples(); i++ )
	{
		num = initialConditions->GetTuple(i);
		for ( int i=0; i<initialConditions->GetNumberOfComponents(); i++ )
			{
			sprintf (str, "%.6f ", num[i]);
			buffer.append(str);
			//*fp << str << "\t";
			}
		//*fp << "\n";
		buffer.append("\n");
	}

//  fp->flush();
//  if (fp->fail())
//    {
//    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
//    return 0;
//    }
  
  return 1;
}

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM1DIniFileWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

