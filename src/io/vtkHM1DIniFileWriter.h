/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DIniFileWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DIniFileWriter - write vtk polygonal data
// .SECTION Description
// vtkHM1DIniFileWriter is a source object that writes ASCII or binary 
// polygonal data files in vtk format. See text for format details.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef VTKHM1DINIFILEWRITER_H_
#define VTKHM1DINIFILEWRITER_H_

#include "vtkDataWriter.h"
#include <string>
using namespace std;

class vtkHM1DIniFile;
class vtkDataObject;

class VTK_EXPORT vtkHM1DIniFileWriter : public vtkDataWriter
{
public:
  static vtkHM1DIniFileWriter *New();
  vtkTypeRevisionMacro(vtkHM1DIniFileWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void WriteIniFileData(vtkHM1DIniFile *IniFile, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // Close a IniFile file.
  void CloseFile(ostream *fp);
    
  // Description:
	// Write real parameters
	int WriteInitialConditions(ostream *fp, vtkHM1DIniFile *iniFile);
	
  
protected:
  vtkHM1DIniFileWriter() {};
  ~vtkHM1DIniFileWriter() {};

 string	buffer;


};

#endif /*VTKHM1DINIFILEWRITER_H_*/
