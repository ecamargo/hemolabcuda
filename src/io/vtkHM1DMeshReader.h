/*
 * $Id: vtkHM1DMeshReader.h 2469 2008-01-09 18:54:52Z igor $
 */
#ifndef vtkHM1DMeshReader_H_
#define vtkHM1DMeshReader_H_

#include "vtkPoints.h"
#include "vtkDataReader.h"

class vtkHM1DMesh;

#include <iostream>
	
class VTK_EXPORT vtkHM1DMeshReader : public vtkDataReader
{
public:
	static vtkHM1DMeshReader *New();
	vtkTypeRevisionMacro(vtkHM1DMeshReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	// Read the header of a Mesh data file. Returns 0 if error.	
	int	ReadHeader();
	
	//ler os pontos, faces e os dados restantes do arquivo Mesh
	int ReadMeshFile(char *file);
		
	// Desccription:
	// Read points
	int ReadPoints();
			
	// Desccription:
	// Read element groups
	int ReadElementGroups();
	
	// Desccription:
	// Read types of elements of the 1D tree
	int ReadElementType();
	
	// Desccription:
	// Read data that indicate for each element which group
	// that has related in Param.txt
	int ReadElementMat();
	
	// Description:
	// Read which elements they will have dirichlet conditions
	int ReadDirichletsConditionsTag();
		
	// Description:
	// Read the parameters of the dirichlet conditions
	int ReadDirichletsConditions();
	
	vtkHM1DMesh *GetMesh();
	void SetMesh(vtkHM1DMesh *m);

protected:
	vtkHM1DMeshReader();
	~vtkHM1DMeshReader();
	
	vtkHM1DMesh *Mesh;
	
}; //End class

#endif /*vtkHM1DMeshReader_H_*/
