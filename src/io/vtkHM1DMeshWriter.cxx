/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DMeshWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DMeshWriter.h"
#include "vtkHM1DMesh.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"



#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DMeshWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DMeshWriter);

//----------------------------------------------------------------------------
void vtkHM1DMeshWriter::WriteMeshData(vtkHM1DMesh *mesh, char *dir)
{
  ostream *fp;
	
	//this->DebugOn();
	
	char d[256];
	strcpy(d, dir);
	strcat(d, "/Mesh.txt");
	this->SetFileName(d);
	
  vtkDebugMacro(<<"Writing Mesh data...");

  if ( !(fp=this->OpenFile(mesh)) || !this->WriteHeader(fp, mesh) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if (!this->WritePoints(fp, mesh->GetPoints(), mesh->GetDimension()))
    {
    errorOccured = 1;
    }
  if (!errorOccured && !this->WriteElementGroups(fp, mesh))
    {
    errorOccured = 1;
    }
  if (!errorOccured && !this->WriteElementType(fp, mesh))
    {
    errorOccured = 1;
    }
	if (!errorOccured && !this->WriteElementMat(fp, mesh))
    {
    errorOccured = 1;
    }
//  if (!errorOccured && !this->WriteDirichletsConditionsTag(fp, mesh,1))
//    {
//    errorOccured = 1;
//    }
//  if (!errorOccured && !this->WriteDirichletsConditions(fp, mesh))
//    {
//    errorOccured = 1;
//    }
  
 
 // no modelo 3d stand alone como o numero de substeps é 2 repete-se
 // os tags e valores das condicoes dirichlet
 // 
  if (mesh->GetDegreeOfFreedom()==7) // modelo 3D e acoplado tem 7 DOFs
  	{
 	 	if (mesh->GetCoupledModel()) // se é modelo acoplado pode ter 2 ou 3 substeps 
 	 		{
	 	 	if (!errorOccured && !this->WriteDirichletsConditionsTag(fp, mesh,mesh->GetSubStepNumber()))
	    	errorOccured = 1;
	  	if (!errorOccured && !this->WriteDirichletsConditions(fp, mesh,mesh->GetSubStepNumber()))
	    	errorOccured = 1;
 	 		}
 	 	else // se for 3D stand alone tem-se 2 substeps
 	 		{
	 	 	if (!errorOccured && !this->WriteDirichletsConditionsTag(fp, mesh,2))
	    	errorOccured = 1;
	  	if (!errorOccured && !this->WriteDirichletsConditions(fp, mesh,2))
	    	errorOccured = 1;
 	 		}
  	}
	else //1D -- apenas 1 substep
		{
		if (!errorOccured && !this->WriteDirichletsConditionsTag(fp, mesh,1))
    	errorOccured = 1;
	 	if (!errorOccured && !this->WriteDirichletsConditions(fp, mesh, 1))
    	errorOccured = 1;
		}
	*fp << buffer ;
	buffer.clear();


  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a Mesh data file. Returns NULL if error.
ostream *vtkHM1DMeshWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  //vtkDataObject *input = this->GetInput();
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening Mesh file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close Mesh file.
void vtkHM1DMeshWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing Mesh file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}

//----------------------------------------------------------------------------
// Write the header of a Mesh data file. Returns 0 if error.
int vtkHM1DMeshWriter::WriteHeader(ostream *fp, vtkHM1DMesh *input)
{
  vtkDebugMacro(<<"Writing header...");

//  *fp << "*NODAL DOFs\n";
//  *fp << input->GetDegreeOfFreedom() << "\n"; 
//  *fp << "*DIMEN\n";
////  *fp << input->GetDimension() << "\n";
//  *fp << "3" << "\n";
//  *fp << "*COORDINATES\n";
//  *fp << input->GetNumberOfPoints() << "\n"; 

	char aux[100];
	sprintf(aux,"*NODAL DOFs\n %d \n*DIMEN\n 3 \n*COORDINATES\n %d\n", input->GetDegreeOfFreedom(), input->GetNumberOfPoints()) ;
	buffer.append(aux);




//  fp->flush();
//  if (fp->fail())
//    {
//    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
//    return 0;
//    }
  
  return 1;
}

//----------------------------------------------------------------------------
//Writing Points
int vtkHM1DMeshWriter::WritePoints(ostream *fp, vtkPoints *points, int dimension)
{
	vtkDebugMacro(<<"Writing Points...");
	
	int npts = points->GetNumberOfPoints();

	double *p;
	char str[1024];
	
//	// Case two dimensions
//	if ( dimension == 2 )
//	{
//		for(int i=0; i<npts; i++)
//		{
//			p = points->GetPoint(i);
//			sprintf (str, "%.6f ", p[0]);
//			*fp << str << " ";
//			sprintf (str, "%.6f", p[1]);
//			*fp << str << "\n";			
//		}
//	}
//	// Case three dimensions
//	else
//	{
		for(int i=0; i<npts; i++)
		{
			p = points->GetPoint(i);
			sprintf (str, "%.6f ", p[0]);
			//*fp << str << " ";
			buffer.append(str);
			
			sprintf (str, "%.6f ", p[1]);
			//*fp << str << " ";
			buffer.append(str);

			sprintf (str, "%.6f ", p[2]);
			//*fp << str << "\n";
			buffer.append(str);
			buffer.append("\n");


//			*fp << "0.000000" << "\n"; 
		}
//	}
	
	
	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();
	
	return 1;
}

//----------------------------------------------------------------------------
//Writing Element groups
int vtkHM1DMeshWriter::WriteElementGroups(ostream *fp, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<<"Writing Element Groups...");
	
	// Number of elements
	int numElements = mesh->GetNumberOfElementGroups();
	char aux[1024];
	//*fp << "*ELEMENT GROUPS\n";
	buffer.append("*ELEMENT GROUPS\n");
	//*fp << "1\n";
	buffer.append("1\n");
	//*fp << "1 " << numElements << " Generic\n";
	sprintf(aux,"1 %d Generic\n", numElements) ;
	buffer.append(aux);

	
	// List with number of elements for group
	int *nElements = mesh->GetElementForGroup();
	
	for ( int i=0; i<numElements; i++ )
		{
		//*fp << nElements[i] << "\n";
		sprintf(aux,"%d\n", nElements[i]) ;
		buffer.append(aux);
		
		}
	// Vector with element groups
	vtkHM1DMesh::VectorOfIntList *vet = mesh->GetGroupElements();
	vtkHM1DMesh::ListOfInt lst;
	vtkHM1DMesh::ListOfInt::iterator it;
	
	//*fp << "*INCIDENCE\n";
	buffer.append("*INCIDENCE\n");
	for ( int i=0; i<numElements; i++ )
	{
		lst = vet->at(i);
		it = lst.begin();
		while(it != lst.end())
		{
			//*fp << *it << " ";
			sprintf(aux,"%d ", *it) ;
			//cout << aux << endl;
			buffer.append(aux);
			it++;
		}
		//*fp << "\n";
		buffer.append("\n");
	}
	
	// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();
	
	return 1;
}

//----------------------------------------------------------------------------
//Writing Element type
int vtkHM1DMeshWriter::WriteElementType(ostream *fp, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<<"Writing Element Type...");
	
	// Number of elements
	int numElements = mesh->GetNumberOfElementGroups();
	
	char aux[20];
	//*fp << "*ELEMENT TYPE\n";
		buffer.append("*ELEMENT TYPE\n");
		
	int *elementType = mesh->GetElementType();

	for ( int i=0; i<numElements; i++ )
	{
	//	*fp << elementType[i] << "\n";
	sprintf(aux,"%d \n", elementType[i]) ;
	buffer.append(aux);
	
	
	}
	
	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();
	
		
	return 1;
}

//----------------------------------------------------------------------------
//Writing Element mat
int vtkHM1DMeshWriter::WriteElementMat(ostream *fp, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<<"Writing Element Mat...");
	
	// Number of elements
	int numElements = mesh->GetNumberOfElementGroups();
	char aux[20];
	buffer.append("*ELEMENT MAT\n");
	
	
	int *elementMat = mesh->GetElementMat();

	for ( int i=0; i<numElements; i++ )
	{
		//*fp << elementMat[i] << "\n";
		sprintf(aux,"%d \n", elementMat[i]) ;
		buffer.append(aux);
	}
	
	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();
	
		
	return 1;
}

//----------------------------------------------------------------------------
//Writing Dirichlets conditions tag
int vtkHM1DMeshWriter::WriteDirichletsConditionsTag(ostream *fp, vtkHM1DMesh *mesh, int NumberOfSubSteps)
{
	vtkDebugMacro(<<"Writing DirichletsConditionsTag...");
	
	int npts; // = mesh->GetNumberOfPoints();
	
//	if (NumberOfSubSteps==2)
//		npts = (mesh->GetNumberOfPoints()*2);
//	else
//		npts = mesh->GetNumberOfPoints();


	npts = (mesh->GetNumberOfPoints()*NumberOfSubSteps);

	
	// Number of elements
	
	char aux[20];
	int numDegree = mesh->GetDegreeOfFreedom();
	
//	if (NumberOfSubSteps != 2)
	buffer.append("*DIRICHLET CONDITIONS\n");
		
	vtkIntArray *dirichletsTag = mesh->GetDirichletsTag();

	int *tuple = new int[numDegree];
	for ( int i=0; i<npts; i++ )
	{
		dirichletsTag->GetTupleValue(i,tuple);
		for (int j=0; j<numDegree; j++)
			{
			sprintf(aux,"%d ", tuple[j]) ;
			//strcat(buffer, temp);
			buffer.append(aux);
			}
		
		buffer.append("\n");
	}

	

	//*fp << buffer ;

	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();
	
	delete [] tuple;


	return 1;
}

//----------------------------------------------------------------------------
//Writing Dirichlets conditions
int vtkHM1DMeshWriter::WriteDirichletsConditions(ostream *fp, vtkHM1DMesh *mesh, int NumberOfSubSteps)
{
	vtkDebugMacro(<<"Writing DirichletsConditionsTag...");
	
	int npts; // = mesh->GetNumberOfPoints();
	
//	if (NumberOfSubSteps==2)
//		npts = (mesh->GetNumberOfPoints()*2);
//	else
//		npts = mesh->GetNumberOfPoints();


	npts = (mesh->GetNumberOfPoints()*NumberOfSubSteps);
	
	
	// Number of elements
	//int npts = mesh->GetNumberOfPoints();
	char aux[20];
	int numDegree = mesh->GetDegreeOfFreedom();
		
	vtkDoubleArray *dirichlets = mesh->GetDirichlets();
	//*fp << "** \n";
	buffer.append("\n");
	
	for ( int i=0; i<npts; i++ )
	{
		for (int j=0; j<numDegree; j++)
		//	*fp << dirichlets->GetComponent(i, j) << " ";
			{
			sprintf(aux,"%f ", dirichlets->GetComponent(i, j)) ;
			buffer.append(aux);
			}
	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();	
		
		//*fp << "\n";
	buffer.append("\n");
	}
		
	// esvaziar buffer 
	*fp << buffer ;
	buffer.clear();


	return 1;
}

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM1DMeshWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

