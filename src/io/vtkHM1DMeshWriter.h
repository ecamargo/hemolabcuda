/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DMeshWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DMeshWriter - write vtk polygonal data
// .SECTION Description
// vtkHM1DMeshWriter is a source object that writes ASCII or binary 
// polygonal data files in vtk format. See text for format details.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef VTKHM1DMESHWRITER_H_
#define VTKHM1DMESHWRITER_H_
#include <string>
using namespace std;
//#include <vtksys/stl/string>
#include "vtkDataWriter.h"

class vtkHM1DMesh;
class vtkDataObject;

class VTK_EXPORT vtkHM1DMeshWriter : public vtkDataWriter
{
public:
  static vtkHM1DMeshWriter *New();
  vtkTypeRevisionMacro(vtkHM1DMeshWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  //vtkHM1DMesh* GetInput();
  //vtkHM1DMesh* GetInput(int port);
  
  void WriteMeshData(vtkHM1DMesh *mesh, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // Close a Mesh file.
  void CloseFile(ostream *fp);
  
  // Description:
  // Write the header of the Mesh data file. Returns 0 if error.
  int WriteHeader(ostream *fp, vtkHM1DMesh *input);
  
  // Description:
  // Write out the points of the data set.
  int WritePoints(ostream *fp, vtkPoints *p, int dimension);
  
  // Description:
  // Write ElementGroups
  int WriteElementGroups(ostream *fp, vtkHM1DMesh *mesh);
  
  // Desccription:
	// Write types of elements of the 1D tree
	int WriteElementType(ostream *fp, vtkHM1DMesh *mesh);
	
	// Desccription:
	// Write data that indicate for each element which group
	// that has related in Param.txt
	int WriteElementMat(ostream *fp, vtkHM1DMesh *mesh);

	// Description:
	// Write which elements they will have dirichlet conditions
	//int WriteDirichletsConditionsTag(ostream *fp, vtkHM1DMesh *mesh);
	int WriteDirichletsConditionsTag(ostream *fp, vtkHM1DMesh *mesh, int NumberOfSubSteps);
	
	// Description:
	// Write the parameters of the dirichlet conditions
	int WriteDirichletsConditions(ostream *fp, vtkHM1DMesh *mesh, int NumberOfSubSteps);

protected:
  vtkHM1DMeshWriter() {};
  ~vtkHM1DMeshWriter() {};

  //virtual int FillInputPortInformation(int port, vtkInformation *info);
 string	buffer;


};


#endif /*VTKHM1DMESHWRITER_H_*/
