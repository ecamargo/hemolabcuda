/*
 * $Id: vtkHM1DParamReader.cxx 2519 2008-03-13 20:13:00Z igor $
 */
#include "vtkHM1DParamReader.h"
#include "vtkHM1DParam.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>
#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHM1DParamReader, "$Revision: 2519 $");
vtkStandardNewMacro(vtkHM1DParamReader);

vtkHM1DParamReader::vtkHM1DParamReader()
{
	vtkDebugMacro(<<"Reading Param data...");
	this->Param = vtkHM1DParam::New();
}

//----------------------------------------------------------------------------
vtkHM1DParamReader::~vtkHM1DParamReader()
{
	this->Param->Delete();
}

//----------------------------------------------------------------------------
void vtkHM1DParamReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
// Read Param file
int vtkHM1DParamReader::ReadParamFile(char *file)
{
	vtkDebugMacro(<<"Reading Param file");

	this->SetFileName(file);

	if ( !this->OpenFile() || !this->ReadHeader() )
		{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
		return 0;
		}

	if ( !this->ReadRealParameters() )
		{
		vtkErrorMacro(<<"Can't read Real Parameters. " << this->GetFileName());
		return 0;
		}

	if ( !this->ReadIntegerParameters() )
		{
		vtkErrorMacro(<<"Can't read Integer Parameters. " << this->GetFileName());
		return 0;
		}

	this->CloseFile();

	return 1;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM1DParamReader::CloseFile()
{
	vtkDebugMacro(<<"Closing Param file");
	if (this->IS != NULL)
		{
		delete this->IS;
		}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Open File
int vtkHM1DParamReader::OpenFile()
{
	if (this->ReadFromInputString)
		{
		if (this->InputArray)
			{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0),
					this->InputArray->GetNumberOfTuples()*
					this->InputArray->GetNumberOfComponents());
			return 1;
			} else if (this->InputString)
			{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
			}
		} else
		{
		vtkDebugMacro(<< "Opening Param file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
			{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode(vtkErrorCode::NoFileNameError);
			return 0;
			}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0)
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
			return 0;
			}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
			return 0;
			}

		return 1;
		}

	return 0;
}

//----------------------------------------------------------------------------
//Read header
int vtkHM1DParamReader::ReadHeader()
{
	char line[256];

	vtkDebugMacro(<< "Reading Param file header");
	//
	// read header
	//

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
		return 0;
		}

	if (strncmp("*Parameter", line, 30) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));

		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
		return 0;
		}

	this->ReadString(line);
	int num;
	if (!this->Read(&num))
		{
		vtkErrorMacro(<<"Can't read number of element groups!" << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	this->Param->SetNumberOfGroups(num);

	return 1;
}

//----------------------------------------------------------------------------
//Read Read Parameters
int vtkHM1DParamReader::ReadRealParameters()
{
	char line[256];

	vtkDebugMacro(<< "Reading Real Parameters");

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Premature EOF reading (*Real Parametes) line! " << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
		return 0;
		}

	if (strncmp("*Real", line, 30) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));

		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
		return 0;
		}

	this->ReadLine(line);

	int *quantityOfRealParameters = new int[this->Param->GetNumberOfGroups()];

	for (int i=0; i<this->Param->GetNumberOfGroups(); i++)
		{
		if (!this->Read(&quantityOfRealParameters[i]))
			{
			vtkErrorMacro(<<"Can't read the quantity of real parameters of the group!" << i << "! for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}
	this->Param->SetQuantityOfRealParameters(quantityOfRealParameters);

	vtkHM1DParam::ListOfDouble lst;
	double num;

	vtkHM1DParam::VectorOfDoubleList realParameters;

	for (int i=0; i<this->Param->GetNumberOfGroups(); i++)
		{
		for (int j=0; j<quantityOfRealParameters[i]; j++)
			{
			if (!this->ReadString(line))
				{
				vtkErrorMacro(<<"can't read the real parameters of the group!" << i << "! for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}

			if ( !strncmp("nan", this->LowerCase(line), 3) )
				num = 0;
			else
				num = atof(line);

			lst.push_back(num);
			}

		realParameters.push_back(lst);
		lst.clear();
		}
	this->Param->SetRealParameters(&realParameters);

	return 1;
}

//----------------------------------------------------------------------------
//Read Integer Parameters
int vtkHM1DParamReader::ReadIntegerParameters()
{
	char line[256];

	vtkDebugMacro(<< "Reading Integer Parameters");

	if (!this->ReadString(line))
		{
		vtkErrorMacro(<<"Premature EOF reading (*Integer Parametes) line! " << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode(vtkErrorCode::PrematureEndOfFileError);
		return 0;
		}

	if (strncmp("*Integer", line, 30) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: "
				<< (this->FileName?this->FileName:"(Null FileName)"));

		this->SetErrorCode(vtkErrorCode::UnrecognizedFileTypeError);
		return 0;
		}
	this->ReadLine(line);

	int *quantityOfIntegerParameters = new int[this->Param->GetNumberOfGroups()];

	for (int i=0; i<this->Param->GetNumberOfGroups(); i++)
		{
		if (!this->Read(&quantityOfIntegerParameters[i]))
			{
			vtkErrorMacro(<<"Can't read the quantity of integer parameters of the group!" << i << "! for file: "
					<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		}
	this->Param->SetQuantityOfIntegerParameters(quantityOfIntegerParameters);

	int num;
	vtkHM1DParam::ListOfInt lst;

	vtkHM1DParam::VectorOfIntList integerParameters;

	for (int i=0; i<this->Param->GetNumberOfGroups(); i++)
		{
		for (int j=0; j<quantityOfIntegerParameters[i]; j++)
			{
			if (!this->Read(&num))
				{
				vtkErrorMacro(<<"Can't read the integer parameters of the group!" << i << "! for file: "
						<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			lst.push_back(num);
			}
		integerParameters.push_back(lst);
		lst.clear();
		}
	this->Param->SetIntegerParameters(&integerParameters);

	return 1;
}

void vtkHM1DParamReader::SetParam(vtkHM1DParam *p)
{
	this->Param = p;
}

vtkHM1DParam *vtkHM1DParamReader::GetParam()
{
	return this->Param;
}
