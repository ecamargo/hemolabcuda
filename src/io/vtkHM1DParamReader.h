/*
 * $Id: vtkHM1DParamReader.h 2469 2008-01-09 18:54:52Z igor $
 */
#ifndef VTKHM1DPARAMREADER_H_
#define VTKHM1DPARAMREADER_H_

#include "vtkDataReader.h"

class vtkHM1DParam;

class VTK_EXPORT vtkHM1DParamReader : public vtkDataReader
{
public:
	static vtkHM1DParamReader *New();
	vtkTypeRevisionMacro(vtkHM1DParamReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	// Read the header of a data file. Returns 0 if error.	
	int	ReadHeader();
	
	// Description:
	// Read the data of the Param.txt file
	int ReadParamFile(char *file);
	
	// Description:
	// Read real parameters
	int ReadRealParameters();
	
	// Description:
	//Read integer parameters
	int ReadIntegerParameters();
	
	// Description:
	// Set/Get Param	
	vtkHM1DParam *GetParam();
	void SetParam(vtkHM1DParam *p);
	
protected:
	vtkHM1DParamReader();
	~vtkHM1DParamReader();
	
	vtkHM1DParam *Param;
	
}; //End class

#endif /*vtkHM1DParamREADER_H_*/
