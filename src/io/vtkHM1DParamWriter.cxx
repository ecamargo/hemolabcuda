/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DParamWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DParamWriter.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DMesh.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DParamWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DParamWriter);

//----------------------------------------------------------------------------
void vtkHM1DParamWriter::WriteParamData(vtkHM1DParam *Param, char *dir)
{
  ostream *fp;
	//this->DebugOn();
	char d[256];
	strcpy(d, dir);
	strcat(d, "/Param.txt");
	
	this->SetFileName(d);
	
  vtkDebugMacro(<<"Writing Param data...");

  if ( !(fp=this->OpenFile(Param)) || !this->WriteHeader(fp, Param) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  if ( !this->WriteRealParameters(fp, Param) )
    {
    errorOccured = 1;
    }
  if ( !this->WriteIntegerParameters(fp, Param) )
    {
    errorOccured = 1;
    }   

	// depois que a string buffer já completa com os valores de parametros, utilizados na formacao do arquivo Param,
	// a mesma é enviada para ser escrita
	*fp << buffer ;
	buffer.clear();

  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------
// Open a Param data file. Returns NULL if error.
ostream *vtkHM1DParamWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening Param file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close a Param file.
void vtkHM1DParamWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing Param file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}
//----------------------------------------------------------------------------
// Write the header of the Param data file. Returns 0 if error.
int vtkHM1DParamWriter::WriteHeader(ostream *fp, vtkHM1DParam *input)
{
  vtkDebugMacro(<<"Writing header...");

  char aux[100];
	sprintf(aux,"*Parameter Groups\n %d\n", input->GetNumberOfGroups()) ;
	buffer.append(aux);

  return 1;
}

//----------------------------------------------------------------------------
//Writing block of the data RealParameters
int vtkHM1DParamWriter::WriteRealParameters(ostream *fp, vtkHM1DParam *param)
{
  vtkDebugMacro(<<"Writing block of the data RealParameters...");

  buffer.append("*Real Parameters\n");
  char aux[1024];
  
  int *quantityOfRealParameters = param->GetQuantityOfRealParameters();
	
	int intbuffer=0;
	
	vtkDebugMacro(<<"Writing number of RealParameters ...");
	
	for ( int i=0; i<param->GetNumberOfGroups(); i++ )
		{
		sprintf(aux,"%d ", quantityOfRealParameters[i]);
		buffer.append(aux);
		intbuffer++;
		if (intbuffer == 30)
			{
			buffer.append("\n");
			intbuffer=0;
			}
		}
		buffer.append("\n \n");
	
	
		// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();
	
	
	
	vtkHM1DParam::ListOfDouble lst;
	vtkHM1DParam::ListOfDouble::iterator it;
	vtkHM1DParam::VectorOfDoubleList *realParameters;
	
	realParameters = param->GetRealParameters();
	
	int RealParam1D =0; // variavel que controla se o param é de modelo 1D ou 3D
	
	lst = realParameters->at(0); // lista que descreve a curva do coracao no 1D
	
	vtkDebugMacro(<<"Writing Real Parameters...");
	
	if (lst.size()) // no caso 3D a lista zero nao armazena uma curva de pressao
		{
		RealParam1D = 1; // modelo 1D
		it = lst.begin();
		// escreve  R1 R2 C P
		//sprintf(aux,"%.10lf %.10lf %.10lf %.10lf\n", *it,*++it, *++it,*++it);
    // escrevendo em formato cientifico para resolver problemas de precisao para valores bem pequenos
    // http://www.cppreference.com/stdio/printf.html
		sprintf(aux,"%e %e %e %e\n", *it,*++it, *++it,*++it);
		buffer.append(aux);
		it++;
		while ( it != lst.end() ) // escreve curva do coracao
			{
			//*fp << *it << " ";
			// escrevendo em formato cientifico para resolver problemas de precisao para valores bem pequenos
    	// http://www.cppreference.com/stdio/printf.html
			sprintf(aux,"%e ", *it);
			buffer.append(aux);
			it++;
			sprintf(aux,"%e\n", *it);
			buffer.append(aux);
			it++;
			}
		}
	
		// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();
	
	
	for ( int i=1; i<param->GetNumberOfGroups(); i++)
		{
		lst = realParameters->at(i);
		it = lst.begin();
		int intbuffer=0;
		
		while ( it != lst.end() )
			{
			// escrevendo em formato cientifico para resolver problemas de precisao para valores bem pequenos
    	// http://www.cppreference.com/stdio/printf.html
			sprintf(aux,"%e ", *it);
			buffer.append(aux);
			it++;
			intbuffer++;
			if (lst.size()>4 && intbuffer == 2)
				{
				buffer.append("\n");
				intbuffer=0;
				}
			}
		buffer.append("\n");
		
	// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();	
		
	}
	
	
		// esvaziando buffer !	
	//*fp << buffer ;
	//buffer.clear();
	
	
	if (!RealParam1D) // se é 3D
		{
		lst = realParameters->at(param->GetNumberOfGroups());
		it = lst.begin();
		int intbuffer=0;
		while ( it != lst.end() )
			{
			// escrevendo em formato cientifico para resolver problemas de precisao para valores bem pequenos
    	// http://www.cppreference.com/stdio/printf.html
			sprintf(aux,"%e ", *it);
			buffer.append(aux);
			it++;
			intbuffer++;
			if (lst.size()>4 && intbuffer == 2)
				{
				buffer.append("\n");
				intbuffer=0;
				}
			}
		buffer.append("\n");
		}

	// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();

  return 1;
}

//----------------------------------------------------------------------------
//Writing block of the data IntegerParameters
int vtkHM1DParamWriter::WriteIntegerParameters(ostream *fp, vtkHM1DParam *param)
{
	vtkDebugMacro(<<"Writing block of the data IntegerParameters...");
  char aux[100];
  buffer.append("*Integer Parameters\n");
  int *quantityOfIntegerParameters = param->GetQuantityOfIntegerParameters();
	int intbuffer=0;
	for ( int i=0; i<param->GetNumberOfGroups(); i++ )
		{
		sprintf(aux,"%d ", quantityOfIntegerParameters[i]);
		buffer.append(aux);
		intbuffer++;
		if (intbuffer == 30)
			{
			buffer.append("\n");
			intbuffer=0;
			}
		}
	buffer.append("\n");
	
		// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();
	
	
	vtkHM1DParam::ListOfInt lst;
	vtkHM1DParam::ListOfInt::iterator it;
	vtkHM1DParam::VectorOfIntList *intParameters;

	intParameters = param->GetIntegerParameters();
	
	for (unsigned int i=0; i<intParameters->size(); i++)
		{
		lst = intParameters->at(i);
		it = lst.begin();
		while ( it != lst.end() )
			{
			sprintf(aux,"%d ", *it);
			buffer.append(aux);
			it++;
			}
		}
	buffer.append("\n");

	// esvaziando buffer !	
	*fp << buffer ;
	buffer.clear();

  return 1;
}

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM1DParamWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

