/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DParamWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DParamWriter - write vtk polygonal data
// .SECTION Description
// vtkHM1DParamWriter is a source object that writes ASCII or binary 
// polygonal data files in vtk format. See text for format details.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef VTKHM1DPARAMWRITER_H_
#define VTKHM1DPARAMWRITER_H_

#include "vtkDataWriter.h"

#include <string>
using namespace std;

class vtkHM1DParam;
class vtkDataObject;

class VTK_EXPORT vtkHM1DParamWriter : public vtkDataWriter
{
public:
  static vtkHM1DParamWriter *New();
  vtkTypeRevisionMacro(vtkHM1DParamWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  void WriteParamData(vtkHM1DParam *Param, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // Close a Param file.
  void CloseFile(ostream *fp);
  
  // Description:
  // Write the header of the Param data file. Returns 0 if error.
  int WriteHeader(ostream *fp, vtkHM1DParam *input);
  
  // Description:
	// Write real parameters
	int WriteRealParameters(ostream *fp, vtkHM1DParam *param);
	
	// Description:
	// Write integer parameters
	int WriteIntegerParameters(ostream *fp, vtkHM1DParam *param);
  
protected:
  vtkHM1DParamWriter() {};
  ~vtkHM1DParamWriter() {};

	// buffer utilizado na otimizacao do processo de escrita dos arquivos
	string buffer;


};

#endif /*VTKHM1DPARAMWRITER_H_*/
