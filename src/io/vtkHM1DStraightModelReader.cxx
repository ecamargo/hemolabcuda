/*
 * $Id: vtkHM1DStraightModelReader.cxx 2884 2010-05-11 12:37:55Z igor $
 */
/*=========================================================================

 Program:   Visualization Toolkit
 Module:    $RCSfile: vtkHM1DStraightModelReader.cxx,v $

 Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
 All rights reserved.
 See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

 =========================================================================*/
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DMeshReader.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParamReader.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParamReader.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFileReader.h"
#include "vtkHM1DIniFile.h"
#include "vtkHMDataOutReader.h"
#include "vtkHMDataOut.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHMGeneralInfoReader.h"
#include "vtkHMGeneralInfo.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHMUtils.h"

#include "vtkPolyDataReader.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkExecutive.h"
#include "vtkMath.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelReader, "$Revision: 2884 $");
vtkStandardNewMacro(vtkHM1DStraightModelReader);

vtkHM1DStraightModelReader::vtkHM1DStraightModelReader()
{
	this->FileName = NULL;

	vtkHM1DStraightModelGrid *output = vtkHM1DStraightModelGrid::New();
	this->SetOutput(output);
	// Releasing data for pipeline parallism.
	// Filters will know it is empty.
	output->ReleaseData();
	output->Delete();
	this->ExecutePiece = this->ExecuteNumberOfPieces = 0;
	this->ExecuteGhostLevel = 0;

	this->StraightModel = NULL;
	this->Mesh = NULL;
	this->BasParam = NULL;
	this->Param = NULL;
	this->IniFile = NULL;
	this->DataOut = NULL;
	this->GeneralInfo = NULL;

	this->StraightModelInformation = vtkIntArray::New();

	vtkDebugMacro(<<"Reading Mesh, Param, IniFile, DataOut and BasParam file data...");

	}

vtkHM1DStraightModelReader::~vtkHM1DStraightModelReader()
{
	if (this->Mesh)
		this->Mesh->Delete();
	if (this->BasParam)
		this->BasParam->Delete();
	if (this->Param)
		this->Param->Delete();
	if (this->IniFile)
		this->IniFile->Delete();
	if (this->DataOut)
		this->DataOut->Delete();
	if (this->GeneralInfo)
		this->GeneralInfo->Delete();

	if (this->StraightModel)
		this->StraightModel->Delete();

	this->StraightModelInformation->Delete();
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelReader::GetOutput()
{
	return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkHM1DStraightModelGrid* vtkHM1DStraightModelReader::GetOutput(int idx)
{
	return vtkHM1DStraightModelGrid::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetOutput(vtkDataObject *output)
{
	this->GetExecutive()->SetOutputData(0, output);
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelReader::RequestUpdateExtent(vtkInformation *,
    vtkInformationVector **, vtkInformationVector *outputVector)
{
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	int piece, numPieces, ghostLevel;

	piece = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
	numPieces
	    = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
	ghostLevel
	    = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS());

	// make sure piece is valid
	if (piece < 0 || piece >= numPieces)
		{
		return 1;
		}

	if (ghostLevel < 0)
		{
		return 1;
		}

	// Save the piece so execute can use this information.
	this->ExecutePiece = piece;
	this->ExecuteNumberOfPieces = numPieces;

	this->ExecuteGhostLevel = ghostLevel;

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelReader::FillOutputPortInformation(int,
    vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM1DStraightModelGrid");
	return 1;
}

void vtkHM1DStraightModelReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

//------------------------------------------------------------------
int vtkHM1DStraightModelReader::RequestData(vtkInformation *,
    vtkInformationVector **, vtkInformationVector *outputVector)
{
	vtkDebugMacro(<<"Casting output to vtkHM1DStraightModelGrid.");

	//get the info object
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the ouptut
	vtkHM1DStraightModelGrid
	    *output =
	        vtkHM1DStraightModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	char str[256];

	strcpy(str, this->GetFileName());

	int n = strlen(str);

	//Test to verify if the directory has
	// the bar in the end, in case that
	// it does not have, adds

	while (str[n]!='/')
		n--;
	str[n+1]='\0';

	this->SetFileName(str);

	//Read file Mesh.txt
	vtkHM1DMeshReader *MReader = vtkHM1DMeshReader::New();
	sprintf(str, "%s/Mesh.txt", this->GetFileName());
	if (!MReader->ReadMeshFile(str))
		{
		vtkErrorMacro(<<"Error reading Mesh file.");
		MReader->CloseFile();
		MReader->Delete();
		MReader = NULL;
		return 0;
		}
	this->SetMesh(MReader->GetMesh());

	//Setting progress of the read
	float progress=this->GetProgress();
	this->SetProgress(progress + 0.07*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	MReader->Delete();

	//Read file BasParam.txt
	vtkHM1DBasParamReader *BPReader = vtkHM1DBasParamReader::New();
	sprintf(str, "%s/Basparam.txt", this->GetFileName());
	if (!BPReader->ReadBasParamFile(str, this->Mesh))
		{
		sprintf(str, "%s/BasParam.txt", this->GetFileName());
		if (!BPReader->ReadBasParamFile(str, this->Mesh))
			{
			vtkErrorMacro(<<"Error reading BasParam file.");
			BPReader->CloseFile();
			BPReader->Delete();
			BPReader = NULL;
			return 0;
			}
		}

	this->SetBasParam(BPReader->GetBasParam());

	//Setting progress of the read
	progress=this->GetProgress();
	this->SetProgress(progress + 0.05*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	BPReader->Delete();

	//Read file Param.txt
	vtkHM1DParamReader *PReader = vtkHM1DParamReader::New();
	sprintf(str, "%s/Param.txt", this->GetFileName());
	if (!PReader->ReadParamFile(str))
		{
		vtkErrorMacro(<<"Error reading Param file.");
		PReader->CloseFile();
		PReader->Delete();
		PReader = NULL;
		return 0;
		}

	this->SetParam(PReader->GetParam());
	PReader->Delete();

	//Setting progress of the read
	progress=this->GetProgress();
	this->SetProgress(progress + 0.05*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	//Read file IniFile.txt
	vtkHM1DIniFileReader *IFReader = vtkHM1DIniFileReader::New();
	sprintf(str, "%s/IniFile.txt", this->GetFileName());
	if (!IFReader->ReadIniFile(str, this->Mesh))
		{
		//vtkWarningMacro(<<"Error reading IniFile file or IniFile.txt not exists.");
		IFReader->CloseFile();
		IFReader->Delete();
		IFReader = NULL;
		}
	if (IFReader)
		{
		this->SetIniFile(IFReader->GetIniFile());
		IFReader->Delete();
		}

	//Setting progress of the read
	progress=this->GetProgress();
	this->SetProgress(progress + 0.05*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	//Read file ModelInfo.hml13D
	vtkHMGeneralInfoReader *GIReader = vtkHMGeneralInfoReader::New();
	if (!GIReader->ReadGeneralInfoFile(this->GetFileName()))
		{
		GIReader->CloseFile();
		GIReader->Delete();
		GIReader = NULL;
		}

	if (GIReader)
		{
		this->SetGeneralInfo(GIReader->GetGeneralInfo());
		this->BasParam->SetQuantityOfDifferentElements(this->GeneralInfo->GetNumberOfElementType());

		this->ConfigureEliminatedElements();

		GIReader->Delete();
		GIReader = NULL;
		}

	int numberOfElements = this->BasParam->GetQuantityOfDifferentElements();
	vtkHM1DTreeElement *elem = NULL;

	this->StraightModelInformation->SetNumberOfValues(this->Mesh->GetNumberOfElementGroups());
	for (int i=0; i<this->Mesh->GetNumberOfElementGroups(); i++)
		this->StraightModelInformation->SetValue(i, 0);

	if (numberOfElements == 2)
		{
		//Generate Straight Model with two element types
		if (!this->GenerateStraightModelTwo(0, elem))
			{
			vtkErrorMacro(<<"Error generating StraightModel with two element types.");
			return 0;
			}
		} else if (numberOfElements == 4)
		{
		//Generate Straight Model with four element types
		if (!this->GenerateStraightModelFour(0, elem))
			{
			vtkErrorMacro(<<"Error generating StraightModel with four element types.");
			return 0;
			}
		}

	//configura o tempo final lido do arquivo basparam.
	double finalTime = this->BasParam->GetTmax();
	this->StraightModel->SetFinalTime(finalTime);

	if (this->DataOut)
		{
		vtkDoubleArray *time = this->DataOut->GetInstantOfTime();
		this->StraightModel->SetInstantOfTime(time);
		}

	//Setting progress
	progress=this->GetProgress();
	this->SetProgress(progress + 0.06*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	output->SetStraightModel(this->StraightModel);
	if (!output->CreateStraightModelGrid())
		{
		vtkErrorMacro(<<"Error creating StraightModelGrid.");
		return 0;
		}

	//Setting progress
	progress=this->GetProgress();
	this->SetProgress(progress + 0.01*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());

	return 1;
}

//----------------------------------------------------------------------------
//Generating StraightModel with four types of element
int vtkHM1DStraightModelReader::GenerateStraightModelFour(int elemGroup,
    vtkHM1DTreeElement *root)
{
	vtkDebugMacro(<<"Generating StraightModel with four types of element.");

	float progress;

	vtkHM1DSegment *Segm = NULL;
	vtkHM1DTerminal *Term = NULL;
	vtkHM1DTreeElement *elem;

	///////////////////////////////////////////////////////////////////////////////
	//Declarações de variaveis relacionadas ao mesh
	vtkHM1DMesh::VectorOfIntList *vet;
	vet = this->Mesh->GetGroupElements();

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator it, it2;

	int *elementType = this->Mesh->GetElementType();
	//Vetor com valores de ligação de cada elemento do mesh com dados do param
	int *elementMat = this->Mesh->GetElementMat();

	///////////////////////////////////////////////////////////////////////////////
	//Declarações de variaveis relacionadas ao param
	//Quantidade de parametros inteiros do param
	int *qtdIntParam = this->Param->GetQuantityOfIntegerParameters();
	//Vetor com lista de parametros inteiros do param
	vtkHM1DParam::VectorOfIntList *vetIntParam =
	    this->Param->GetIntegerParameters();

	int numElements = 0;

	if (!root)
		{
		this->StraightModel = vtkHM1DStraightModel::New();
		//Create root (heart)
		root = (vtkHM1DTerminal *)this->StraightModel->Get1DTreeRoot();

		this->StraightModel->AddSegment(root->GetId());

		elem = root->GetFirstChild();

		//Adding segment child of the root
		Segm = this->StraightModel->GetSegment(elem->GetId());

		lst2 = vet->at(elemGroup);
		it2 = lst2.begin();

		//Setting data of the terminal
		this->SetTerminalData(this->StraightModel->GetTerminal(root->GetId()), *it2
		    -1, elemGroup);

		this->StraightModelInformation->SetValue(elemGroup, root->GetId());

		it2++;
		int j=elemGroup+1;
		//Adding node to segment
		this->SetData(this->StraightModel->GetSegment(elem->GetId()), vet, it2, &j);

		this->GenerateStraightModelFour(j, elem);
		} //End if(!root)
	else
		{
		int n = elemGroup;
		while (n < vet->size() )
			{
			lst2 = vet->at(n);
			it = lst2.begin();

			//If has child or more parents
			if (lst2.size() > 2)
				{
				elemGroup = n;
				vtkHM1DParam::ListOfInt lstOfInt; //list com parametros inteiros do param
				vtkHM1DParam::ListOfInt::iterator itIntParam; //iterator da lista de parametros

				lstOfInt = vetIntParam->at(elementMat[elemGroup]-1);

				itIntParam = lstOfInt.begin();
				it++;
				while (itIntParam != lstOfInt.end() )
					{
					//fluxo saindo do terminal, criar segmento filho
					if ( *itIntParam == -1)
						{
						Term
						    = this->StraightModel->GetTerminal(this->StraightModelInformation->GetValue(elemGroup));

						if (!Term) // tratamento para quando encontrar algum segmento com o fluxo invertido e dessa forma o HeMoLab não cair..
							{
							vtkErrorMacro("Bad format of Mesh.txt - There is some segment with wrong flow direction");
							return 0;
							}
						vtkIdType id = this->StraightModel->AddSegment(Term->GetId());
						elem = Term->GetChild(id);

						int j=elemGroup+1;

						j = this->FindElementGroup(elemGroup+1, *it);

						if (j == -1)
							j = this->FindElementGroup(0, *it);

						lst = vet->at(j);
						it2 = lst.begin();
						this->SetData(this->StraightModel->GetSegment(elem->GetId()), vet,
						    it2, &j);

						itIntParam++;
						it++;
						} //End if ( *itIntParam == -1 )
					//---------------------------------------------------------------------------------------------
					//fluxo chegando no terminal, tenho que setar os pais do terminal
					else if ( *itIntParam == 1)
						{
						//Pego o terminal com o id setado no vetor de informacoes de elementos do StraightModel
						Term
						    = this->StraightModel->GetTerminal(this->StraightModelInformation->GetValue(elemGroup));
						//Procuro pelo elemento ligado ao elemento atual
						int j = this->FindElementGroup(elemGroup+1, *it);

						//Caso nao tenha encontra o elemento na busca acima,
						//procuro pelo elemento ligado ao elemento atual iniciando a busca pelo
						//inicio da lista de grupos de elementos
						if (j == -1)
							j = this->FindElementGroup(0, *it);

						if ( (elementType[j] == 3) || (elementType[j] == 4))
							{
							j++;
							j = this->FindElementGroup(j, *it2);
							}
						if (j == -1)
							j = this->FindElementGroup(0, *it2);

						//Pego o id do Segmento
						int stmdl = this->StraightModelInformation->GetValue(j);

						Segm = this->StraightModel->GetSegment(stmdl);

						if (Segm)
							{
							//Procurar pelo terminal filho do segmento
							vtkHM1DTreeElement *ch = Segm->GetFirstChild();

							int childId;
							while ( !this->StraightModel->IsTerminal(ch->GetId()) )
								ch = Segm->GetNextChild();

							childId = ch->GetId();

							if (childId != Term->GetId() )
								{
								//Remover o terminal filho do segmento
								Segm->RemoveChild(Segm->GetChild(childId));
								this->StraightModel->RemoveTerminal(childId, 0);
								//Adiciona como filho do segmento, o terminal que já possui outro pai
								Segm->AddChild(Term);
								//Adiciona o segmento como pai do terminal também
								}
							}
						itIntParam++;
						it++;
						} // End else if ( *itIntParam == 1 )
					else
						{
						itIntParam++;
						it++;
						}

					} // End while ( itIntParam != lstOfInt.end() )

				//Setting progress
				progress=this->GetProgress();
				this->SetProgress(progress + 0.01*(1.0 - progress));
				this->UpdateProgress(this->GetProgress());

				} // End if(lst2.size() > 2)
			n++;
			} //End while ( n < vet->size() )

		} //End else

	return 1;
} //End GenerateStraightModelFour()

//----------------------------------------------------------------------------
// Set data
void vtkHM1DStraightModelReader::SetData(vtkHM1DSegment *Segm,
    vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2,
    int *group)
{
	vtkDebugMacro(<<"SetData.");

	int numElements=0;

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator itv;

	int *elementT = this->Mesh->GetElementType();

	int j=*group;
	//Se o segmento possuir somente um elemento
	if ( (elementT[j] == 1) && (elementT[j+1] == 3) &&(elementT[j+2] == 4))
		{
		numElements++;
		//Setting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		this->StraightModelInformation->SetValue(j, Segm->GetId());

		it2++;
		j+=2;

		//Setting data of the node
		this->SetSegmentData(Segm, numElements, *it2-1, j);

		this->StraightModelInformation->SetValue(j, Segm->GetId());

		j++;
		int k=j;
		j = this->FindElementGroup(j, *it2);

		if (j == -1)
			j = this->FindElementGroup(0, *it2);

		//Se encontrou o terminal
		if (elementT[j] == 2)
			{
			int stml = this->StraightModelInformation->GetValue(j);

			//Se terminal ja foi visitado, tera mais de um pai
			if (stml)
				{
				//Pego o terminal no straightModel
				vtkHM1DTerminal *term = this->StraightModel->GetTerminal(stml);

				//Setar os dados do ultimo node
				this->SetSegmentData(Segm, numElements, *it2-1, k-1);
				//Adicionar no array de elementos visitados
				this->StraightModelInformation->SetValue(k-1, Segm->GetId());

				//Pego o primeiro filho do segmento, que no caso é um terminal
				//como estou em um terminal ja visitado, associo o terminal ja
				//visitado como filho do segmento e removo o terminal que o segmento possui
				vtkHM1DTreeElement *t = Segm->GetFirstChild();
				Segm->RemoveChild(t);
				this->StraightModel->RemoveTerminal(t, 0);

				//Adicionar o terminal visitado como filho do segmento
				Segm->AddChild(term);

				return;
				} else
				{
				vtkHM1DTerminal *term =
				    this->StraightModel->GetTerminal(Segm->GetFirstChild()->GetId());
				//Setting data of the node
				this->SetSegmentData(Segm, numElements, *it2-1, k-1);
				this->StraightModelInformation->SetValue(k-1, Segm->GetId());
				this->SetTerminalData(term, *it2, j);
				this->StraightModelInformation->SetValue(j, term->GetId());
				return;
				}
			}

		} else
		{
		while (1)
			{
			lst = vet->at(j);
			int k = j;

			j = this->FindElementGroup(j, *it2);

			if (j == -1)
				j = this->FindElementGroup(0, *it2);

			if ( (elementT[j] == 3) || (elementT[j] == 4))
				{
				j++;
				j = this->FindElementGroup(j, *it2);
				}

			if (j == -1)
				j = this->FindElementGroup(0, *it2);

			if (elementT[j] == 2)
				{
				int stml = this->StraightModelInformation->GetValue(j);

				//Se terminal ja foi visitado, tera mais de um pai
				if (stml)
					{
					//Pego o terminal no straightModel
					vtkHM1DTerminal *term = this->StraightModel->GetTerminal(stml);

					//Setar os dados do ultimo node
					this->SetSegmentData(Segm, numElements, *it2-1, k-1);
					this->StraightModelInformation->SetValue(k-1, Segm->GetId());

					//Pego o primeiro filho do segmento, que no caso é um terminal
					//como estou em um terminal ja visitado, associo o terminal ja
					//visitado como filho do segmento e removo o terminal que o segmento possui
					vtkHM1DTreeElement *t = Segm->GetFirstChild();
					Segm->RemoveChild(t);
					this->StraightModel->RemoveTerminal(t, 0);
					//Adicionar o terminal visitado como filho do segmento
					Segm->AddChild(term);

					return;
					} else
					{
					vtkHM1DTerminal *term =
					    this->StraightModel->GetTerminal(Segm->GetFirstChild()->GetId());

					//Setting data of the node
					this->SetSegmentData(Segm, numElements, *it2-1, k-1);
					this->StraightModelInformation->SetValue(k-1, Segm->GetId());
					this->SetTerminalData(term, *it2, j);
					this->StraightModelInformation->SetValue(j, term->GetId());
					return;
					}
				} //End if ( elementT[j] == 2 )

			numElements++;
			//Setting data of the node
			this->SetElement(Segm, numElements, *it2-1, j);
			this->SetSegmentData(Segm, numElements-1, *it2-1, j);

			this->StraightModelInformation->SetValue(j, Segm->GetId());

			lst2 = vet->at(j);
			it2 = lst2.begin();
			it2++;
			j++;

			if (j >= vet->size() )
				{
				this->SetSegmentData(Segm, numElements, *it2-1, j-1);
				return;
				}

			} //End while ( 1 )
		} //End else

}

//----------------------------------------------------------------------------
// Set data inverse
void vtkHM1DStraightModelReader::SetDataInverse(vtkHM1DSegment *Segm,
    vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2,
    int *group)
{
	vtkDebugMacro(<<"SetDataInverse.");

	int numElements=0;

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator itv;

	int *elementT = this->Mesh->GetElementType();

	int j=*group;

	while (elementT[j] != 3)
		{
		lst = vet->at(j);

		j--;
		numElements++;
		//Setting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		this->StraightModelInformation->SetValue(j, Segm->GetId());

		lst2 = vet->at(j);
		it2 = lst2.begin();
		it2++;
		} //End while ( elementType[j+1] != 4 )

	it2--;

	//Setting data of the node
	this->SetSegmentData(Segm, numElements, *it2-1, j);

}

//----------------------------------------------------------------------------
//Generate StraightModel with two types of element
int vtkHM1DStraightModelReader::GenerateStraightModelTwo(int elemGroup,
    vtkHM1DTreeElement *root)
{
	vtkDebugMacro(<<"Generating StraightModel with two types of element.");

	float progress;

	vtkHM1DSegment *Segm = NULL;
	vtkHM1DTerminal *Term = NULL;
	vtkHM1DTreeElement *elem;

	///////////////////////////////////////////////////////////////////////////////
	//Declarações de variaveis relacionadas ao mesh
	vtkHM1DMesh::VectorOfIntList *vet;
	vtkHM1DMesh::VectorOfIntList::iterator itGroupElement;
	vet = this->Mesh->GetGroupElements();

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator it, it2;

	//Vetor com tipo de elementos do mesh
	// 1 = elemento
	// 2 = terminal
	int *elementType = this->Mesh->GetElementType();
	//Vetor com valores de ligação de cada elemento do mesh com dados do param
	int *elementMat = this->Mesh->GetElementMat();

	///////////////////////////////////////////////////////////////////////////////
	//Declarações de variaveis relacionadas ao param
	//Quantidade de parametros inteiros do param
	int *qtdIntParam = this->Param->GetQuantityOfIntegerParameters();
	//Vetor com lista de parametros inteiros do param
	vtkHM1DParam::VectorOfIntList *vetIntParam =
	    this->Param->GetIntegerParameters();

	int numElements = 0;

	if (!root)
		{
		this->StraightModel = vtkHM1DStraightModel::New();
		//Create root (heart)
		root = (vtkHM1DTerminal *)this->StraightModel->Get1DTreeRoot();

		//Adding segment child of the root
		this->StraightModel->AddSegment(root->GetId());
		elem = root->GetFirstChild();
		Segm = this->StraightModel->GetSegment(elem->GetId());

		lst2 = vet->at(elemGroup);
		it2 = lst2.begin();

		//Setting data of the terminal
		this->SetTerminalData(this->StraightModel->GetTerminal(root->GetId()), *it2
		    -1, elemGroup);

		this->StraightModelInformation->SetValue(elemGroup, root->GetId());

		it2++;
		int j=elemGroup+1;
		//Adding node to segment

		this->SetDataTwo(Segm, vet, it2, &j);

		this->GenerateStraightModelTwo(j, elem);
		} //End if(!root)
	else
		{
		int n = elemGroup;
		while (n < vet->size() )
			{
			lst2 = vet->at(n);
			it = lst2.begin();

			//If has child or more parents
			if (lst2.size() > 2)
				{
				elemGroup = n;
				vtkHM1DParam::ListOfInt lstOfInt; //list com parametros inteiros do param
				vtkHM1DParam::ListOfInt::iterator itIntParam; //iterator da lista de parametros

				lstOfInt = vetIntParam->at(elementMat[elemGroup]-1);

				itIntParam = lstOfInt.begin();
				it++;
				while (itIntParam != lstOfInt.end() )
					{
					//fluxo saindo do terminal, criar segmento filho
					if ( *itIntParam == -1)
						{
						Term
						    = this->StraightModel->GetTerminal(this->StraightModelInformation->GetValue(elemGroup));
						vtkIdType id = this->StraightModel->AddSegment(Term->GetId());
						elem = Term->GetChild(id);

						int j=elemGroup+1;

						j = this->FindElementGroup(elemGroup+1, *it);

						if (j == -1)
							j = this->FindElementGroup(0, *it);

						lst = vet->at(j);
						it2 = lst.begin();
						this->SetDataTwo(this->StraightModel->GetSegment(elem->GetId()),
						    vet, it2, &j);

						itIntParam++;
						it++;
						} //End if ( *itIntParam == -1 )
					//---------------------------------------------------------------------------------------------
					//fluxo chegando no terminal, tenho que setar os pais do terminal
					else if ( *itIntParam == 1)
						{
						//Pego o terminal com o id setado no vetor de informacoes de elementos do StraightModel
						Term
						    = this->StraightModel->GetTerminal(this->StraightModelInformation->GetValue(elemGroup));

						//Procuro pelo elemento ligado ao elemento atual
						int j = this->FindElementGroup(elemGroup+1, *it);

						//Caso nao tenha encontra o elemento na busca acima,
						//procuro pelo elemento ligado ao elemento atual iniciando a busca pelo
						//inicio da lista de grupos de elementos
						if (j == -1)
							j = this->FindElementGroup(0, *it);

						//Pego o id do Segmento
						int stmdl = this->StraightModelInformation->GetValue(j);

						Segm = this->StraightModel->GetSegment(stmdl);

						if (Segm)
							{
							//Procurar pelo terminal filho do segmento
							vtkHM1DTreeElement *ch = Segm->GetFirstChild();
							int childId;
							while ( !this->StraightModel->IsTerminal(ch->GetId()) )
								ch = Segm->GetNextChild();

							childId = ch->GetId();

							if (childId != Term->GetId() )
								{
								//Remover o terminal filho do segmento
								Segm->RemoveChild(Segm->GetChild(childId));
								this->StraightModel->RemoveTerminal(childId, 0);
								//Adiciona como filho do segmento, o terminal que já possui outro pai
								Segm->AddChild(Term);
								//Adiciona o segmento como pai do terminal também
								Term->AddParent(Segm);
								}
							}
						itIntParam++;
						it++;
						} // End else if ( *itIntParam == 1 )
					else
						{
						itIntParam++;
						it++;
						}
					//Setting progress
					progress=this->GetProgress();
					this->SetProgress(progress + 0.01*(1.0 - progress));
					this->UpdateProgress(this->GetProgress());
					} // End while ( itIntParam != lstOfInt.end() )


				} // End if(lst2.size() > 2)
			n++;

			} //End while ( n < vet->size() )

		} //End else

	return 1;
} //End GenerateStraightModelTwo()

//----------------------------------------------------------------------------
// Set data
void vtkHM1DStraightModelReader::SetDataTwo(vtkHM1DSegment *Segm,
    vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2,
    int *group)
{
	vtkDebugMacro(<<"SetDataTwo.");

	int numElements=0;

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator itv;

	int *elementT = this->Mesh->GetElementType();
	int posicao;
	int j=*group;
	//Se o segmento possuir somente um elemento
	if ( (elementT[j] == 1) && (elementT[j+1] == 2 ))
		{
		numElements++;

		//Setting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		this->StraightModelInformation->SetValue(j, Segm->GetId());

		it2++;
		this->SetSegmentData(Segm, numElements, *it2-1, j);

		j++;
		int k=j;
		j = this->FindElementGroup(j, *it2);

		if (j == -1)
			j = this->FindElementGroup(0, *it2);

		//Se encontrou o terminal
		if (elementT[j] == 2)
			{
			int stml = this->StraightModelInformation->GetValue(j);

			//Se terminal ja foi visitado, tera mais de um pai
			if (stml)
				{
				//Pego o terminal no straightModel
				vtkHM1DTerminal *term = this->StraightModel->GetTerminal(stml);

				//Setar os dados do ultimo node
				this->SetSegmentData(Segm, numElements, *it2-1, k-1);
				//Adicionar no array de elementos visitados
				this->StraightModelInformation->SetValue(k-1, Segm->GetId());

				//Pego o primeiro filho do segmento, que no caso é um terminal
				//como estou em um terminal ja visitado, associo o terminal ja
				//visitado como filho do segmento e removo o terminal que o segmento possui
				vtkHM1DTreeElement *t = Segm->GetFirstChild();
				Segm->RemoveChild(t);
				this->StraightModel->RemoveTerminal(t, 0);

				//Adicionar o terminal visitado como filho do segmento
				Segm->AddChild(term);

				return;
				} else
				{
				vtkHM1DTerminal *term =
				    this->StraightModel->GetTerminal(Segm->GetFirstChild()->GetId());
				//Setting data of the node
				this->SetSegmentData(Segm, numElements, *it2-1, k-1);
				this->StraightModelInformation->SetValue(k-1, Segm->GetId());
				this->SetTerminalData(term, *it2, j);
				this->StraightModelInformation->SetValue(j, term->GetId());
				return;
				}
			} //End if ( elementT[j] == 2 )

		} //End if( (elementT[j] == 1) && (elementT[j+1] == 2 ) )
	else
		{
		while ( 1)
			{
			int k = j;
			lst = vet->at(j);

			j = this->FindElementGroup(j, *it2);

			if (j == -1)
				j = this->FindElementGroup(0, *it2);

			if (elementT[j] == 2)
				{
				int stml = this->StraightModelInformation->GetValue(j);

				//Se terminal ja foi visitado, tera mais de um pai
				if (stml)
					{
					//Pego o terminal no straightModel
					vtkHM1DTerminal *term = this->StraightModel->GetTerminal(stml);

					//Setar os dados do ultimo node
					this->SetSegmentData(Segm, numElements, *it2-1, k-1);
					this->StraightModelInformation->SetValue(k-1, Segm->GetId());

					//Pego o primeiro filho do segmento, que no caso é um terminal
					//como estou em um terminal ja visitado, associo o terminal ja
					//visitado como filho do segmento e removo o terminal que o segmento possui
					vtkHM1DTreeElement *t = Segm->GetFirstChild();
					Segm->RemoveChild(t);
					this->StraightModel->RemoveTerminal(t, 0);
					//Adicionar o terminal visitado como filho do segmento
					Segm->AddChild(term);

					return;
					} //End if ( stml )
				else
					{
					vtkHM1DTerminal *term =
					    this->StraightModel->GetTerminal(Segm->GetFirstChild()->GetId());

					//Setting data of the node
					this->SetSegmentData(Segm, numElements, *it2-1, k-1);
					this->StraightModelInformation->SetValue(k-1, Segm->GetId());
					this->SetTerminalData(term, *it2, j);
					this->StraightModelInformation->SetValue(j, term->GetId());
					return;
					}
				} //End if ( elementT[j] == 2 )

			numElements++;
			//Setting data of the node
			this->SetElement(Segm, numElements, *it2-1, j);
			this->SetSegmentData(Segm, numElements-1, *it2-1, j);

			this->StraightModelInformation->SetValue(j, Segm->GetId());

			lst2 = vet->at(j);
			it2 = lst2.begin();
			it2++;
			j++;

			if (j >= vet->size() )
				{
				this->SetSegmentData(Segm, numElements, *it2-1, j-1);
				return;
				}
			} //End while ( 1 )
		} //End else

}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::ConfigureEliminatedElements()
{
	vtkDebugMacro(<<"ConfigureEliminatedElements.");
	vtkIntArray *elementList = this->GeneralInfo->GetNullElementList();
	vtkIntArray *terminalList = this->GeneralInfo->GetNullTerminalList();

	int *elementType = this->Mesh->GetElementType();
	int *elementMat = this->Mesh->GetElementMat();
	double *tuple;

	//se lista de elementos anulados existe, trocar os tipos de nulos para seu valor
	//padrao (1, 3 ou 4) e tambem trocar sua associação com o param (elementMat).
	if (elementList)
		{
		for (int i=0; i<elementList->GetNumberOfTuples(); i++)
			{
			tuple = elementList->GetTuple3(i);
			elementType[(int)tuple[0]] = (int)tuple[1];
			elementMat[(int)tuple[0]] = (int)tuple[2];
			}
		}

	//se lista de terminais anulados existe, trocar os tipos de nulos para seu valor
	//padrao (2) e tambem trocar sua associação com o param (elementMat).
	if (terminalList)
		{
		for (int i=0; i<terminalList->GetNumberOfTuples(); i++)
			{
			tuple = terminalList->GetTuple3(i);
			elementType[(int)tuple[0]] = (int)tuple[1];
			elementMat[(int)tuple[0]] = (int)tuple[2];
			}
		}
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelReader::FindElementGroup(int group, int elem)
{
	vtkHM1DMesh::VectorOfIntList *vet = this->Mesh->GetGroupElements();

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator it;

	while (group < vet->size() )
		{
		lst = vet->at(group);
		it = lst.begin();

		while (it != lst.end() )
			{
			if ( *it == elem)
				return group;
			it++;
			}
		group++;
		} //End while(group < vet->size())

	return -1;
}

//----------------------------------------------------------------------------
// Set data inverse
void vtkHM1DStraightModelReader::SetDataInverseTwo(vtkHM1DSegment *Segm,
    vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2,
    int *group)
{
	vtkDebugMacro(<<"SetDataInverseTwo.");

	int numElements=0;

	vtkHM1DMesh::ListOfInt lst, lst2;
	vtkHM1DMesh::ListOfInt::iterator itv;

	int *elementT = this->Mesh->GetElementType();

	int j=*group;

	while (elementT[j] != 2)
		{
		lst = vet->at(j);

		j--;
		numElements++;
		//Setting data of the node
		this->SetElement(Segm, numElements, *it2-1, j);
		this->SetSegmentData(Segm, numElements-1, *it2-1, j);

		this->StraightModelInformation->SetValue(j, Segm->GetId());

		lst2 = vet->at(j);
		it2 = lst2.begin();
		it2++;
		} //End while ( elementType[j+1] != 4 )

	it2--;

	//Setting data of the node
	this->SetSegmentData(Segm, numElements, *it2-1, j);
}

//----------------------------------------------------------------------------
// Set element data
void vtkHM1DStraightModelReader::SetElement(vtkHM1DSegment *Segm,
    int numElements, int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetElementData.");

	vtkHMElementData *elem = new vtkHMElementData();

	if ( this->GeneralInfo )
		{
		vtkIntArray *array = this->GeneralInfo->GetNullElementList();
		if ( array )
			{
			for ( int i=0; i<array->GetNumberOfTuples(); i++ )
				{
				double *tuple = array->GetTuple(i);
				if ( (int)tuple[0] == elemGroup )
					{
					this->StraightModel->AddNullElements1D3D(Segm->GetId(), numElements-1);
					}
				}
			}
		}
	//Setting data of the Param file
	if (this->Param)
		{
		vtkHM1DParam::VectorOfDoubleList *vet = this->Param->GetRealParameters();
		vtkHM1DParam::ListOfDouble lst;
		vtkHM1DParam::ListOfDouble::iterator it;

		int *elementMat = this->Mesh->GetElementMat();

		int *quantityOfRealParameters = this->Param->GetQuantityOfRealParameters();

		vtkDebugMacro(<<"Setting data of the Param file.");
		lst = vet->at(elementMat[elemGroup]-1);
		it = lst.begin();

		it++;
		it++;
		it++;
		it++;
		while (it!=lst.end() )
			{
			elem->Elastin = *it;
			it++;
			elem->Collagen = *it;
			it++;
			elem->CoefficientA = *it;
			it++;
			elem->CoefficientB = *it;
			it++;
			elem->Viscoelasticity = *it;
			it++;
			elem->ViscoelasticityExponent = *it;
			it++;

			elem->ReferencePressure = *it;
			it++;
			elem->InfiltrationPressure = *it;
			it++;
			elem->Permeability = *it;
			it++;
			elem->idNode[0] = numElements-1;
			elem->idNode[1] = numElements;

			elem->WallThickness = 0;
			elem->meshID = elemGroup;
			}
		}
	//Se nao possui param, seta o elemento com valores default
	else
		{
		vtkDebugMacro(<<"Setting default data.");
		elem->idNode[0] = numElements-1;
		elem->idNode[1] = numElements;
		elem->meshID = elemGroup;
		}
	Segm->SetNumberOfElements(numElements);
	Segm->SetElementData(numElements-1, *elem);
}

//----------------------------------------------------------------------------
// Set segment data
void vtkHM1DStraightModelReader::SetSegmentData(vtkHM1DSegment *Segm,
    int numElements, int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetSegmentData.");
	int sum = 0;

	vtkHMNodeData *node = new vtkHMNodeData();
	double *point;

	point = this->Mesh->GetPoints1D(elemento);
	node->coords[0] = *point;
	node->coords[1] = *++point;

	/*
	 atencao:

	 as coordenadas do eixo Y estavam sendo invertidas pois
	 a arvore padrao fica "de cabeca para baixo"


	 */

	node->coords[2] = *++point;
	node->meshID = elemento +1;

	//Setting data of the Param file
	if (this->Param)
		{
		vtkHM1DParam::VectorOfDoubleList *vet = this->Param->GetRealParameters();
		vtkHM1DParam::ListOfDouble lst;
		vtkHM1DParam::ListOfDouble::iterator it;

		int *elementMat = this->Mesh->GetElementMat();

		vtkDebugMacro(<<"Setting data of the Param file.");
		lst = vet->at(elementMat[elemGroup]-1);
		it = lst.begin();

		node->area = *it;
		node->radius = sqrt(node->area/vtkMath::Pi());
		it++;
		it++;
		node->alfa = *it;
		node->ResolutionArray = NULL;
		}

	vtkDoubleArray *result = NULL;
	if (this->DataOut)
		{
		vtkDebugMacro(<<"Setting data of the DataOut file.");
		//Setting data of the DataOut file
		vtkDoubleArray *array = this->DataOut->GetDegreeOfFreedomArray();
		result = vtkDoubleArray::New();
		result->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
		result->SetNumberOfTuples(this->DataOut->GetNumberOfBlock());

		for (int k=0; k<this->DataOut->GetNumberOfBlock(); k++)
			{
			result->SetTuple(k, array->GetTuple(sum+elemento));
			sum += Mesh->GetNumberOfPoints();
			}

		node->ResolutionArray = result;

		}
	Segm->SetNodeData(numElements, *node);

}

//----------------------------------------------------------------------------
// Set terminal data
void vtkHM1DStraightModelReader::SetTerminalData(vtkHM1DTerminal *Term,
    int elemento, int elemGroup)
{
	vtkDebugMacro(<<"SetTerminalData.");

//	cout << "SetTerminalData--> elemento: "<< elemento << " elemGroup: " << elemGroup << endl;

	vtkHM1DTerminal::ResistanceValuesList r1;
	vtkHM1DTerminal::ResistanceTimeList r1Time;
	vtkHM1DTerminal::ResistanceValuesList r2;
	vtkHM1DTerminal::ResistanceTimeList r2Time;
	vtkHM1DTerminal::CapacitanceValuesList c;
	vtkHM1DTerminal::CapacitanceTimeList cTime;
	vtkHM1DTerminal::PressureValuesList p;
	vtkHM1DTerminal::PressureValuesList pTime;

	if ( this->GeneralInfo )
		{
		vtkIntArray *array = this->GeneralInfo->GetNullTerminalList();
		if ( array )
			{
			for ( int i=0; i<array->GetNumberOfTuples(); i++ )
				{
				double *tuple = array->GetTuple(i);
				if ( tuple[0] == elemGroup )
					{
					this->StraightModel->AddNullElements1D3D(Term->GetId(), Term->GetId());
					}
				}
			}
		}

	//Setting data of the Param file
	if (this->Param)
		{
		vtkHM1DParam::VectorOfDoubleList *vet = this->Param->GetRealParameters();
		vtkHM1DParam::ListOfDouble lst;
		vtkHM1DParam::ListOfDouble::iterator it;

		int *elementMat = this->Mesh->GetElementMat();

		vtkDebugMacro(<<"Setting data of the Param file.");
		lst = vet->at(elementMat[elemGroup]-1);

		it=lst.begin();

		//if ( (elemGroup == 0) && (lst.size() > 4))
		// como podem haver terminais com mais de 4 elementos reais a serem lidos
		if ( lst.size() > 4)
			{
			// entra neste If para o coracao e os demais terminais com propriedades transientes

			vtkHM1DParam::VectorOfIntList *vetInt =
			    this->Param->GetIntegerParameters();
			vtkHM1DParam::ListOfInt listInt;
			vtkHM1DParam::ListOfInt::iterator itInt;

			//Pulando os quatro primeiros parametros que não interessam
			it++;
			it++;
			it++;
			it++;

			int Position;


			if (!elemGroup)
				{
				Position = 0;
//				cout << "Tamanho lista inteiro " << vetInt->at(elemGroup).size() << endl;
				}
			else
				{
				Position = elementMat[elemGroup]-1;
//				cout << "Tamanho lista inteiro " << vetInt->at(Position).size() << endl;
				}

			//Pegando a lista do coração
			//listInt = vetInt->at(0);

			//listInt = vetInt->at(elemGroup);

			listInt = vetInt->at(Position);

			itInt = listInt.end();
			itInt--;
			itInt--;
			itInt--;
			itInt--;

//			cout << "Tamanho double R1 " << (*itInt) << endl;

			//Setando a resistencia 1
			for (int i=0; i<(*itInt); i++)
				{

				r1Time.push_back(*it);
				it++;

				r1.push_back(*it);
				it++;

				}
			itInt++;

//			cout << "Tamanho double R2 " << (*itInt) << endl;

			//Setando a resistencia 2
			for (int i=0; i<(*itInt); i++)
				{
				r2Time.push_back(*it);
				it++;
				r2.push_back(*it);
				it++;

				}
			itInt++;

//		cout << "Tamnaho double Cap " << (*itInt) << endl;

			//Setando a capacitancia
			for (int i=0; i<(*itInt); i++)
				{
				cTime.push_back(*it);
				it++;
				c.push_back(*it);
				it++;

				}
			itInt++;

//		cout << "Tamnaho double Pressao " << (*itInt) << endl;

			//Setando a pressão
			for (int i=0; i<(*itInt); i++)
				{
				pTime.push_back(*it);
				it++;
				p.push_back(*it);
				it++;

				}
			//itInt++;
			} // End if( elemGroup == 1 )
		else
			{
			// tratar aqui terminais com propriedades transientes!!!!

			//cout << "lst.size() " << lst.size() << endl;


			r1Time.push_back(0);
			r2Time.push_back(0);
			cTime.push_back(0);
			pTime.push_back(0);

			while (it!=lst.end() )
				{
				r1.push_back(*it);
				it++;
				r2.push_back(*it);
				it++;
				c.push_back(*it);
				it++;
				p.push_back(*it);
				it++;
				}

			}
		}
	//Se nao possui param, seta o terminal com valores default
	else
		{
		vtkDebugMacro(<<"Setting default data.");
		//Setando a resistencia 1
		r1Time.push_back(0);
		r1.push_back(0);

		//Setando a resistencia 2
		r2Time.push_back(0);
		r2.push_back(0);

		//Setando a capacitancia
		cTime.push_back(0);
		c.push_back(0);

		//Setando a pressão
		pTime.push_back(0);
		p.push_back(0);
		}

	Term->SetR1(&r1);
	Term->SetR1Time(&r1Time);
	Term->SetR2(&r2);
	Term->SetR2Time(&r2Time);
	Term->SetC(&c);
	Term->SetCTime(&cTime);
	Term->SetPt(&p);
	Term->SetPtTime(&pTime);
	Term->SetmeshID(elemento+1);

	if (this->DataOut)
		{
		//Setting data of the DataOut file
		int sum = 0;

		vtkDoubleArray *array = this->DataOut->GetDegreeOfFreedomArray();

		vtkDebugMacro(<<"Setting data of the DataOut file.");
		vtkDoubleArray *result = vtkDoubleArray::New();
		result->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
		result->SetNumberOfTuples(this->DataOut->GetNumberOfBlock());

		for (int k=0; k<this->DataOut->GetNumberOfBlock(); k++)
			{
			result->SetTuple(k, array->GetTuple(sum+elemento));
			sum += Mesh->GetNumberOfPoints();
			}
		Term->SetResolutionArray(result);
		result->Delete();
		}
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelReader::ReadSimulationData(char *file)
{
	//Read file DataOut.txt
	vtkHMDataOutReader *DOReader = vtkHMDataOutReader::New();

	if ( this->GeneralInfo )
	  {
	  DOReader->CoupledModelOn();
	  int n = this->StraightModel->GetNumberOfNodes() + this->StraightModel->GetNumberOfTerminals();
	  DOReader->SetNumberOfPointsOf1DTree(n);
	  }

	if (!DOReader->ReadDataOut(file, this->Mesh, this->BasParam, this))
		{
		vtkWarningMacro(<<"Error reading DataOut file or DataOut.txt not exists.");
		DOReader->CloseFile();
		DOReader->Delete();
		DOReader = NULL;
		return 0;
		}

	if (DOReader)
		{
		this->SetDataOut(DOReader->GetDataOut());
		if ( !this->SetSimulationData() )
		  {
	    DOReader->Delete();
	    this->DataOut->Delete();
	    this->DataOut = NULL;

	    return 0;
		  }
		this->StraightModel->SetInstantOfTime(this->DataOut->GetInstantOfTime());

		//Libera a memoria dos dados lidos apos seta-los na arvore.
		this->DataOut->GetDegreeOfFreedomArray()->Delete();
		DOReader->Delete();
    this->DataOut->SetDegreeOfFreedomArray(NULL);
		}

	return 1;
}

//----------------------------------------------------------------------------
int vtkHM1DStraightModelReader::SetSimulationData()
{
	//Setting data of the DataOut file
	int sum = 0;
	int meshId;

	int numberOf1DPoints = this->StraightModel->GetNumberOfNodes() + this->StraightModel->GetNumberOfTerminals();

	vtkDoubleArray *array = this->DataOut->GetDegreeOfFreedomArray();

	vtkDebugMacro(<<"Setting data of the DataOut file.");

	vtkHM1DTerminal *term;

	vtkHM1DStraightModel::TreeNodeDataMap termMap =
	    this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	// Teste realizado devido a possibilidade de leitura de arquivo acoplado "1D-3D",
	// caso o numero de graus de liberdade seja maior que 4, tenho um modelo acoplado,
	// não sendo necessário o teste de consistencia do número de pontos do mesh com o
	// número de pontos do árvore
	if ( this->Mesh->GetDegreeOfFreedom() <= 4 )
	  {
    int numberOfPoints = this->Mesh->GetNumberOfPoints();
    int n = this->StraightModel->GetNumberOfNodes() + this->StraightModel->GetNumberOfTerminals();

    // se numero de pontos da arvore for diferente do numero do arquivo, não é possível ler os dados.
    if ( n != numberOfPoints )
      {
      vtkWarningMacro(<<"Error setting in simulation data. Data not correspond the tree.");
  		return 0;
      }
	  }

	//setar dados dos terminais
	for (it = termMap.begin(); it!= termMap.end(); it++)
		{
		//pega o terminal e seu mesh id
		term = vtkHM1DTerminal::SafeDownCast(it->second);
		meshId = term->GetmeshID();

		if ( meshId != -1 )
			{
			vtkDoubleArray *result = vtkDoubleArray::New();
			result->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
			result->SetNumberOfTuples(this->DataOut->GetNumberOfBlock());

			sum = 0;
			//inserir dados no array do terminal
			for (int k=0; k<this->DataOut->GetNumberOfBlock(); k++)
				{
				result->SetTuple(k, array->GetTuple(sum+meshId-1));
//				sum += this->Mesh->GetNumberOfPoints();
				sum += numberOf1DPoints;
				}
			term->SetResolutionArray(result);
			result->Delete();
			}
		}

	vtkHM1DStraightModel::TreeNodeDataMap segmMap =
	    this->StraightModel->GetSegmentsMap();

	//setar dados dos nodes
	for (it = segmMap.begin(); it!= segmMap.end(); it++)
		{
		//pegar segmento
		vtkHM1DSegment *segm = vtkHM1DSegment::SafeDownCast(it->second);
		for (int i=0; i<segm->GetNodeNumber(); i++)
			{
			//pegar o mesh id do node
			meshId = segm->GetNodeData(i)->meshID;
			if ( meshId != -1 )
				{
				vtkDoubleArray *result = vtkDoubleArray::New();
				result->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
				result->SetNumberOfTuples(this->DataOut->GetNumberOfBlock());

				sum = 0;
				//inserir dados no array dos nodes
				for (int k=0; k<this->DataOut->GetNumberOfBlock(); k++)
					{
					result->SetTuple(k, array->GetTuple(sum+meshId-1));
//					sum += this->Mesh->GetNumberOfPoints();
          sum += numberOf1DPoints;
					}
				segm->SetResolutionArray(i, result);
				result->Delete();
				}
			}
		}

	vtkDoubleArray *time = this->DataOut->GetInstantOfTime();
	this->StraightModel->SetInstantOfTime(time);

	return 1;
}

//----------------------------------------------------------------------------
const char *vtkHM1DStraightModelReader::GetDataOutTimeArray()
{
	string Result;
	char *str;

	if ( !this->DataOut )
		return NULL;

	vtkDoubleArray *TimeArray = this->DataOut->GetInstantOfTime();

	if (TimeArray)
		{
		Result = vtkHMUtils::ConvertDoubleArrayToChar(TimeArray);
		str = new char[Result.size()+2];
		strcpy(str, Result.c_str());
		return (const char*)str;
		}
	else
		vtkErrorMacro("vtkHM1DStraightModelReader::GetDataOutTimeArray Unable to find DataOut and get InstantOfTime array ");
	return Result.c_str(); // retorna string vazia

}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetMesh(vtkHM1DMesh *mesh)
{
	if (mesh != this->Mesh)
		{
		if (this->Mesh)
			this->Mesh->UnRegister(this);

		this->Mesh = mesh;

		if (this->Mesh)
			this->Mesh->Register(this);
		}
}

//----------------------------------------------------------------------------
vtkHM1DMesh *vtkHM1DStraightModelReader::GetMesh()
{
	return this->Mesh;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetBasParam(vtkHM1DBasParam *BP)
{
	if (BP != this->BasParam)
		{
		if (this->BasParam)
			this->BasParam->UnRegister(this);

		this->BasParam = BP;

		if (this->BasParam)
			this->BasParam->Register(this);
		}
}

//----------------------------------------------------------------------------
vtkHM1DBasParam *vtkHM1DStraightModelReader::GetBasParam()
{
	return this->BasParam;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetParam(vtkHM1DParam *p)
{
	if (p != this->Param)
		{
		if (this->Param)
			this->Param->UnRegister(this);

		this->Param = p;

		if (this->Param)
			this->Param->Register(this);
		}
}

//----------------------------------------------------------------------------
vtkHM1DParam *vtkHM1DStraightModelReader::GetParam()
{
	return this->Param;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetIniFile(vtkHM1DIniFile *IF)
{
	if (IF != this->IniFile)
		{
		if (this->IniFile)
			this->IniFile->UnRegister(this);

		this->IniFile = IF;

		if (this->IniFile)
			this->IniFile->Register(this);
		}
}

//----------------------------------------------------------------------------
vtkHM1DIniFile *vtkHM1DStraightModelReader::GetIniFile()
{
	return this->IniFile;
}

//----------------------------------------------------------------------------
vtkHM1DStraightModel *vtkHM1DStraightModelReader::GetStraightModel()
{
	return this->StraightModel;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetDataOut(vtkHMDataOut *DO)
{
	if (DO != this->DataOut)
		{
		if (this->DataOut)
			this->DataOut->UnRegister(this);

		this->DataOut = DO;

		if (this->DataOut)
			this->DataOut->Register(this);
		}
}

vtkHMDataOut *vtkHM1DStraightModelReader::GetDataOut()
{
	return this->DataOut;
}

//----------------------------------------------------------------------------
void vtkHM1DStraightModelReader::SetGeneralInfo(vtkHMGeneralInfo *GI)
{
	if (GI != this->GeneralInfo)
		{
		if (this->GeneralInfo)
			this->GeneralInfo->UnRegister(this);

		this->GeneralInfo = GI;

		if (this->GeneralInfo)
			this->GeneralInfo->Register(this);
		}
}

vtkHMGeneralInfo *vtkHM1DStraightModelReader::GetGeneralInfo()
{
	return this->GeneralInfo;
}
