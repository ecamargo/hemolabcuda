/*
 * $Id: vtkHM1DStraightModelReader.h 2884 2010-05-11 12:37:55Z igor $
 */
/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DStraightModelReader.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DStraightModelReader - read Mesh, BasParam, Param, 
// IniFile and DataOut data file and generate an vtkHM1DStraightModel
// .SECTION Description
// vtkHM1DStraightModelReader is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The output of this reader is a single vtkHM1DStraightModelGrid data object.
// The superclass of this class, vtkDataReader, provides many methods for
// controlling the reading of the data file, see vtkDataReader for more
// information.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.
// .SECTION See Also
// vtkPolyData vtkDataReader vtkHM1DStraightModel vtkHM1DStraightModelGrid 

#ifndef __vtkHM1DStraightModelReader_h
#define __vtkHM1DStraightModelReader_h

#include "vtkDataReader.h"

#include "vtkHM1DMesh.h"

class vtkHM1DStraightModel;
class vtkHM1DMeshReader;
class vtkHM1DBasParamReader;
class vtkHM1DParamReader;
class vtkHM1DIniFileReader;
class vtkHMDataOutReader;
class vtkHM1DStraightModelGrid;
class vtkHM1DSegment;
class vtkHM1DTerminal;
class vtkHM1DTreeElement;
class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM1DIniFile;
class vtkHMDataOut;
class vtkHMGeneralInfo;

class VTK_EXPORT vtkHM1DStraightModelReader : public vtkDataReader
{
public:
	static vtkHM1DStraightModelReader *New();
	vtkTypeRevisionMacro(vtkHM1DStraightModelReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
  // Set position of first end point.
  vtkSetVector3Macro(Point1,double);
  vtkGetVectorMacro(Point1,double,3);

  // Description:
  // Set position of other end point.
  vtkSetVector3Macro(Point2,double);
  vtkGetVectorMacro(Point2,double,3);

  // Description:
  // Divide line into resolution number of pieces.
  vtkSetClampMacro(Resolution,int,1,VTK_LARGE_INTEGER);
  vtkGetMacro(Resolution,int);
	
	
	// Description:
	// Get the output of this reader.
	vtkHM1DStraightModelGrid *GetOutput();
	vtkHM1DStraightModelGrid *GetOutput(int idx);
	void SetOutput(vtkDataObject *output);
  
	// Description :
	//Genetate Straight Model with two element types
	int GenerateStraightModelTwo(int elemGroup, vtkHM1DTreeElement *root);
	
	// Description :
	//Genetate Straight Model with four element types
	int GenerateStraightModelFour(int elemGroup, vtkHM1DTreeElement *root);
	
	// Description :
	// Set/Get Mesh
	void SetMesh(vtkHM1DMesh *mesh);	
	vtkHM1DMesh *GetMesh();
	
	// Description :
	// Set/Get BasParam
	void SetBasParam(vtkHM1DBasParam *BP);	
	vtkHM1DBasParam *GetBasParam();
	
	// Description :
	// Set/Get Param
	void SetParam(vtkHM1DParam *p);	
	vtkHM1DParam *GetParam();
	
	// Description :
	// Set/Get IniFile
	void SetIniFile(vtkHM1DIniFile *IF);	
	vtkHM1DIniFile *GetIniFile();
	
	// Description :
	// Set/Get DataOut
	void SetDataOut(vtkHMDataOut *DO);	
	vtkHMDataOut *GetDataOut();
	
	// Description :
	// Set/Get GeneralInfo
	void SetGeneralInfo(vtkHMGeneralInfo *GI);	
	vtkHMGeneralInfo *GetGeneralInfo();
	
	// Description :
	// Get StraightModel
	vtkHM1DStraightModel *GetStraightModel();
	
	// Description:
	//Set data of the nodes of the segment
	void SetSegmentData(vtkHM1DSegment *Segm, int numElements, int elemento, int elemGroup);
	
	// Description:
	//Set data of the terminal
	void SetTerminalData(vtkHM1DTerminal *Term, int elemento, int elemGroup);
	
	// Description:
	//Set data of the elements of the segment
	void SetElement(vtkHM1DSegment *Segm, int numElements, int elemento, int elemGroup);
	
	//BTX
	// Description:
	//Set data of the segment for number of types of elements equal 4 
	void SetData(vtkHM1DSegment *Segm, vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2, int *group);
	
	// Description:
	//Set data of the segment for number of types of
	//elements equal 4 and the segment start in the
	//end of the espcification of the group elements
	void SetDataInverse(vtkHM1DSegment *Segm, vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2, int *group);
	
	// Description:
	//Set data of the segment for number of types of elements equal 2
	void SetDataTwo(vtkHM1DSegment *Segm, vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2, int *group);
	
	// Description:
	//Set data of the segment for number of types of
	//elements equal 2 and the segment start in the
	//end of the espcification of the group elements
	void SetDataInverseTwo(vtkHM1DSegment *Segm, vtkHM1DMesh::VectorOfIntList *vet, vtkHM1DMesh::ListOfInt::iterator it2, int *group);

	//ETX
	
	// Description:
	// Find element group linked with actual element
	int FindElementGroup(int group, int elem);
	
	// Description:
	// Read data of the DataOut.txt file
	int ReadSimulationData(char *file);
	
	// Description:
	// Set data of the simulation, data read of the DataOut.txt
	int SetSimulationData();
	
	// Description:
	// Configure the elements eliminated, "elements and terminals",
	// of the 1D model, in files of the coupling
	void ConfigureEliminatedElements();
	
	const char *GetDataOutTimeArray();
	
protected:
	vtkHM1DStraightModelReader();
	~vtkHM1DStraightModelReader();

	virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);
  
	// Description:
	// Update extent of PolyData is specified in pieces.  
	// Since all DataObjects should be able to set UpdateExent as pieces,
	// just copy output->UpdateExtent  all Inputs.
	virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **,
                                  vtkInformationVector *);
  
	virtual int FillOutputPortInformation(int, vtkInformation*);
	
	double Point1[3];
  double Point2[3];
  int Resolution;
	                                
	// Description:
	// Used by streaming: The extent of the output being processed
	// by the execute method. Set in the ComputeInputUpdateExtents method.
	int ExecutePiece;
  int ExecuteNumberOfPieces;
	int ExecuteGhostLevel;
	
	// Description:
	//Object of the tree 1D
	vtkHM1DStraightModel *StraightModel;
	
	// Description:
	//Object for read data of the Mesh.txt file
	vtkHM1DMesh *Mesh;
	
	// Description:
	//Object for read data of the BasParam.txt file
	vtkHM1DBasParam *BasParam;
	
	// Description:
	//Object for read data of the Param.txt file
	vtkHM1DParam *Param;
	
	// Description:
	//Object for read data of the IniFile.txt file
	vtkHM1DIniFile *IniFile;
	
	// Description:
	//Object for read data of the DataOut.txt file
	vtkHMDataOut *DataOut;
	
	// Description:
	// Object for read data of the ModelInfo.hml13D
	vtkHMGeneralInfo *GeneralInfo;
	
	
	vtkIntArray *StraightModelInformation;
	
};

#endif


