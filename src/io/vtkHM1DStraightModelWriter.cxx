/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DStraightModelWriter.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM1DStraightModelWriter.h"
#include "vtkHM1DMeshWriter.h"
#include "vtkHM1DBasParamWriter.h"
#include "vtkHM1DParamWriter.h"
#include "vtkHM1DIniFileWriter.h"
#include "vtkHM3DGeneralInfoWriter.h"
#include "vtkHM1DData.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkIdList.h"
#include "vtkIntArray.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM1DStraightModelWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM1DStraightModelWriter);

vtkHM1DStraightModelWriter::vtkHM1DStraightModelWriter()
{
	this->WhichFile = NULL;
}

void vtkHM1DStraightModelWriter::WriteData()
{
  //this->DebugOn();

  //ostream *fp;
  vtkHM1DData *input = this->GetInput();

  vtkDebugMacro(<<"Writing StraightModel data...");

	if (this->WhichFile[0])
  	{
	  //Obtendo BasParam do objeto de entrada
	  vtkHM1DBasParam *basParam = input->GetBasParam();
	  //Criando BasParamWriter
	  vtkHM1DBasParamWriter *basParamWriter = vtkHM1DBasParamWriter::New();
	  //Escrevendo BasParam
	  vtkDebugMacro(<<"Writing BasParam data...");
	  basParamWriter->WriteBasParamData(basParam, this->GetFileName());
  	basParamWriter->Delete();
  	}


	if (this->WhichFile[1])
  	{
	  //Obtendo Mesh do objeto de entrada
	  vtkHM1DMesh *mesh = input->GetMesh();
	  //Criando MeshWriter
	  vtkHM1DMeshWriter *MeshWriter = vtkHM1DMeshWriter::New();
	  //Escrevendo Mesh
	  vtkDebugMacro(<<"Writing Mesh data...");
	  MeshWriter->WriteMeshData(mesh, this->GetFileName());
	  MeshWriter->Delete();
  	}  
  
	if (this->WhichFile[2])
  	{
	  //Obtendo Param do objeto de entrada
	  vtkHM1DParam *Param = input->GetParam();
	  //Criando ParamWriter
	  vtkHM1DParamWriter *ParamWriter = vtkHM1DParamWriter::New();
	  //Escrevendo Param
	  vtkDebugMacro(<<"Writing Param data...");
	  ParamWriter->WriteParamData(Param, this->GetFileName());
	  ParamWriter->Delete();
  	}


	if (this->WhichFile[3])
  	{
	  if ( input->GetIniFile() )
	  	{
	  	//Obtendo IniFile do objeto de entrada
	  	vtkHM1DIniFile *IniFile = input->GetIniFile();
	  	//Criando IniFileWriter
	  	vtkHM1DIniFileWriter *IniFileWriter = vtkHM1DIniFileWriter::New();
	  	//Escrevendo IniFile
	  	vtkDebugMacro(<<"Writing IniFile data...");
	  	IniFileWriter->WriteIniFileData(IniFile, this->GetFileName());
	  	IniFileWriter->Delete();
	  	}
  	}

  // se o writer esta sendo utilizado para escrever arquivos do 3D
  //if (input->Get3DFullModel()) // input é do tipo vtkHmData
  if (input->GetModelType()) 
    {
  	vtkHM3DFullModel *FullModel = input->Get3DFullModel();
  	vtkHM3DGeneralInfoWriter *GeneralInfo3D = vtkHM3DGeneralInfoWriter::New();
  	
  	if (input->GetModelType() == 1) //modelo 3d apenas  
	  	{
	  	GeneralInfo3D->Start3D(FullModel, this->GetFileName());
	  	}
  	else //modelo acoplado
  		{
  		if (this->WhichFile[1] && this->WhichFile[2]) // Se Mesh e Param gerados entao todos arquivos foram gerados
  			GeneralInfo3D->StartCoupled(input->GetModelInfo(), input->GetElementTypeArray(), input->GetElementMatArray(), this->GetFileName());
  		}
  	
  	GeneralInfo3D->Delete();
  	}
  
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriter::SetInput(vtkHM1DData *d)
{
  this->Superclass::SetInput(d);
}

//-------------------------------------------------------------------------------------

int vtkHM1DStraightModelWriter::FillInputPortInformation(int vtkNotUsed(port), vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkHM1DData");
  return 1;
}

//-------------------------------------------------------------------------------------

vtkHM1DData* vtkHM1DStraightModelWriter::GetInput()
{
  return vtkHM1DData::SafeDownCast(this->Superclass::GetInput());
}

//-------------------------------------------------------------------------------------

vtkHM1DData* vtkHM1DStraightModelWriter::GetInput(int port)
{
  return vtkHM1DData::SafeDownCast(this->Superclass::GetInput(port));
}
//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriter::SetWhichFile(int *WhichFile)
{
	this->WhichFile = WhichFile;	
	
}
//-------------------------------------------------------------------------------------
