/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM1DStraightModelWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM1DStraightModelWriter - write vtk polygonal data
// .SECTION Description
// vtkHM1DStraightModelWriter is a source object that writes ASCII or binary 
// polygonal data files in vtk format. See text for format details.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef VTKHM1DSTRAIGHTMODELWRITER_H_
#define VTKHM1DSTRAIGHTMODELWRITER_H_

#include "vtkDataWriter.h"

class vtkHM1DData;

class VTK_EXPORT vtkHM1DStraightModelWriter : public vtkDataWriter
{
public:
  static vtkHM1DStraightModelWriter *New();
  vtkTypeRevisionMacro(vtkHM1DStraightModelWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  vtkHM1DData* GetInput();
  vtkHM1DData* GetInput(int port);
  void SetInput(vtkHM1DData *d);
  
  // Description:
  // int array var used to indicate which file will be generated
  void SetWhichFile(int *WhichFile);
  
  // Description:
  // Method for write Mesh, BasParam, Param and IniFile data
	void WriteData();

protected:
  vtkHM1DStraightModelWriter() ;
  ~vtkHM1DStraightModelWriter() {};

  virtual int FillInputPortInformation(int port, vtkInformation *info);
  
  // Description:
  // int array var used to indicate which file will be generated
  int *WhichFile;
  

private:
  vtkHM1DStraightModelWriter(const vtkHM1DStraightModelWriter&);  // Not implemented.
  void operator=(const vtkHM1DStraightModelWriter&);  // Not implemented.
};

#endif /*VTKHM1DSTRAIGHTMODELWRITER_H_*/
