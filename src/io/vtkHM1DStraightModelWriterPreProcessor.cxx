#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFile.h"
#include "vtkHM1DStraightModelWriterPreProcessor.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DData.h"
#include "vtkHM1DStraightModelWriter.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DMeshWriter.h"
//#include "vtkHM1DPropertyWriter.h"
#include "vtkHMParallelSolverScriptWriter.h"
#include "vtkHM1DHeMoLabWriter.h"

#include "vtkProcessModule.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelWriterPreProcessor, "$Revision: 444 $");
vtkStandardNewMacro(vtkHM1DStraightModelWriterPreProcessor);

//----------------------------------------------------------------------------
vtkHM1DStraightModelWriterPreProcessor::vtkHM1DStraightModelWriterPreProcessor()
{
	this->StraightModel = NULL;
//	this->PropertyData = vtkHM1DPropertyData::New();
	this->points = vtkPoints::New();
	this->point=0;
	this->GroupElements.clear(); 
	this->ElementTypeArray = vtkIntArray::New();
	this->MaxElemLib=0; 
	this->IniFileArray = vtkDoubleArray::New();
	this->DirichletsTag = NULL;
	this->Dirichlets = NULL;
	this->IniFileDoubleArray = NULL;

	//this->BasParam = vtkHM1DBasParam::New();
	this->NumberOfSegments = 0;
	
	this->StraightModelVisited = vtkIdList::New();
}
//----------------------------------------------------------------------------

vtkHM1DStraightModelWriterPreProcessor::~vtkHM1DStraightModelWriterPreProcessor()
{
	this->points->Delete();
	this->ElementTypeArray->Delete();
	this->StraightModel = NULL;
	this->IniFileArray->Delete();
	
	if (this->DirichletsTag) this->DirichletsTag->Delete();
	if (this->Dirichlets) this->Dirichlets->Delete();
	

	if (this->IniFileDoubleArray) this->IniFileDoubleArray->Delete(); 
	
	//this->BasParam->Delete();
	
//	if(this->PropertyData) this->PropertyData->Delete();
	
	this->StraightModelVisited->Delete();
}
//----------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
//----------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetStraightModel(vtkHM1DStraightModelGrid* grid)
{
	this->StraightModel = grid->GetStraightModel();
}

//----------------------------------------------------------------------------

// gera lista de pontos *COORDINATES e cria mapeamento de pontos com atores (segmentos) e pontos com terminal-id para a posterior 
// criacao da lista de incidencia 
void vtkHM1DStraightModelWriterPreProcessor::GenerateCoordinates(int id)
{
 	vtkHM1DTreeElement *elem = NULL;
	vtkHM1DTreeElement *child = NULL;
	vtkHM1DTreeElement *parent = NULL;
	double x,y,z;
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
		{
	//	cout << "  ::GenerateCoordinates -> Elemento já foi visitado: " << id << endl;
		return;
		}
	
//	cout << "vtkHM1DStraightModelWriterPreProcessor::GenerateCoordinates: " << id << endl;
	
	this->StraightModelVisited->InsertNextId(id);
	
	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
		{
		this->NumberOfSegments++;
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
		int NodeNumber = Seg->GetNodeNumber();
		int i=0;
		while (i < NodeNumber) // visita cada nó do segmento
			{
		  vtkHMNodeData *Node = Seg->GetNodeData(i);
		  this->point++;
		  Node->meshID = this->point;
		  // fills the "Node-Point Number" Pair map
		  NodeIDPair.insert(pair<vtkHMNodeData *, int>(Node,this->point));
		  x=Node->coords[0];
		  y=Node->coords[1];
		  z=Node->coords[2];
		  this->points->InsertNextPoint(x,y,z);
		  i++;
			}
		}
	else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
		{
		elem = this->StraightModel->GetTerminal(id);
		vtkHM1DTerminal *term = vtkHM1DTerminal::SafeDownCast(elem);
		this->point++;
		term->SetmeshID(this->point);
		// fills the "terminal id-Point number" map
		TerminalIDPair.insert(pair<int, int>(id,this->point));
		if (!elem->GetFirstParent())	// se é o coracao
  		{
	    child = elem->GetFirstChild();
	    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child);
	    if (Seg)
	    	{     
	     	vtkHMNodeData *Node = Seg->GetNodeData(0);
	     	if (Node)  
	     		{
	     	  x=Node->coords[0];
	  			y=Node->coords[1];
	  			z=Node->coords[2];
	  			this->points->InsertNextPoint(x,y,z);
	  		  }
	      }
  		}
		else // se o terminal tem pai
			{
		  parent = elem->GetFirstParent();
//		  child = parent->GetFirstChild(); // retorna o terminal
//		  child = parent->GetNextChild();  // retorna o primeiro segmento filho
		  child = elem->GetFirstChild(); // retorna o primeiro segmento filho
		  if (child)
		  	{
		    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child);
		   	vtkHMNodeData *Node = Seg->GetNodeData(0);
		    x=Node->coords[0];
		  	y=Node->coords[1];
		  	z=Node->coords[2];
		  	this->points->InsertNextPoint(x,y,z);
		   	}
		  else // o terminal é folha
		  	{
		   	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(parent);
		   	int NumberOfNodes = Seg->GetNodeNumber();
		   	vtkHMNodeData *Node = Seg->GetNodeData(NumberOfNodes-1);
		    x=Node->coords[0];
		  	y=Node->coords[1];
		  	z=Node->coords[2];
		  	this->points->InsertNextPoint(x,y,z);
		    }
			}  
		child = elem->GetFirstChild();
		} // fim else
	
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->GenerateCoordinates(id);
			}
		}	
}

//----------------------------------------------------------------------------

vtkPoints *vtkHM1DStraightModelWriterPreProcessor::GetPoints()
{
	return this->points;
}

//----------------------------------------------------------------------------

int vtkHM1DStraightModelWriterPreProcessor::GetNumberOfPoints()
{
	return this->points->GetNumberOfPoints();
}


//----------------------------------------------------------------------------

int vtkHM1DStraightModelWriterPreProcessor::GetNodeID(vtkHMNodeData *Node)
{

	std::map<vtkHMNodeData *, int>::iterator it = NodeIDPair.find(Node);
 	return (*it).second ;

}

//----------------------------------------------------------------------------

int vtkHM1DStraightModelWriterPreProcessor::GetTerminalID(int id)
{

  std::map<int, int>::iterator it = TerminalIDPair.find(id);
  return (*it).second ;
}

//----------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::GenerateLists(int id)
{
	vtkHM1DTreeElement *elem 		= NULL;
	vtkHM1DTreeElement *child 	= NULL;
	vtkHM1DTreeElement *parent 	= NULL;
	vtkHM1DTreeElement *child2 	= NULL;
	vtkHM1DTreeElement *elem2 	= NULL;
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
	{
//	cout << "  ::GenerateLists -> Elemento já foi visitado: " << id << endl;
	return;
	}
//	cout << "vtkHM1DStraightModelWriterPreProcessor::GenerateLists: " << id << endl;
	this->StraightModelVisited->InsertNextId(id);
	
	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore  é um segmento
		{ //******************************************************************************
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
		int NodeNumber = Seg->GetNodeNumber();
		int i=0;
		while (i < NodeNumber) // visita cada nó do segmento
			{
		  vtkHMNodeData *Node = Seg->GetNodeData(i);
		  	
		  if (!i) //i==0   //acessa primeiro no do segmento
		 		{
		  	vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
		  	if (!this->MaxElemLib)
		  		{
		  		vtkErrorMacro("Max Element libs not definied!!");
		  		}
		  	else
		  		{
	  			if (this->MaxElemLib==4)
		  			{
		  			if (NodeNumber==2) // se o Segmento tem somente um ELEMENTO
		  				{
		  				vtkHM1DMesh::ListOfInt lst;
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->GroupElements.push_back(lst);
				  		this->GroupElements.push_back(lst);
				  		this->GroupElements.push_back(lst);
				  		this->ElementTypeArray->InsertNextValue(1); // elem
			  			this->ElementTypeArray->InsertNextValue(3); // elem in
			  			this->ElementTypeArray->InsertNextValue(4); // elem out
			  			
			  			//  quando temos segmentos somente com um elemento ai teremos elementos: 1 3 4
			  			//  lista de incidencia tambem será modificada
			  			// elem ex:  1 2
			  			// elem in:  1 2
			  			// elem out: 1 2
			  			
			  			// geracao do INIFILE
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				}
		  			else  // para segmentos com mais de um elemento
		  				{
		  				
		  				// visita o primeiro elemento do segmento
		  				// e visita o nó 0 do elemento
							// por exemplo, se o segmento tem 8 nós, sera visitados o nó 0 
							// assumindo que o primeiro nó é o 0 e o último é o nó 7
		  				vtkHM1DMesh::ListOfInt lst;
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->GroupElements.push_back(lst);
				  		this->GroupElements.push_back(lst);
				  		this->ElementTypeArray->InsertNextValue(1); // elem
			  			this->ElementTypeArray->InsertNextValue(3); // elem in
			  			
			  			// geracao do INIFILE
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				}
		  			}
	  				if (this->MaxElemLib==2)
		  				{
			  			vtkHM1DMesh::ListOfInt lst;
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->GroupElements.push_back(lst);
				  		this->ElementTypeArray->InsertNextValue(1); // elem
				  		
				  		// geracao do INIFILE
			  			this->IniFileArray->InsertNextValue(Node->area);
			  			}
		  		} // fim else
		  	}  //fim if (!i)
		  else
		  	{
		  	if (i== NodeNumber-2) // penultimo nó do segmento
		  		{
					// visita o penultimo e o último nó do segmento
					// por exemplo, se o segmento tem 8 nós, serão visitados os nós 6 e 7 
					// assumindo que o primeiro nó é o 0 e o último é o nó 7
					vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
	  			if (!this->MaxElemLib)
	  				{
	  				vtkErrorMacro("Max Element libs not definied!!");
	  				}
	  			else
	  				{
	  				if (this->MaxElemLib==4)
		  				{
					  	vtkHM1DMesh::ListOfInt lst;
				  		lst.clear();
				  		lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->GroupElements.push_back(lst);
				  		this->GroupElements.push_back(lst);
					  	this->ElementTypeArray->InsertNextValue(1); // elem
		  				this->ElementTypeArray->InsertNextValue(4); // elem in
		  				// geracao do INIFILE
							this->IniFileArray->InsertNextValue(Node->area);
							
							// como o último nó não é visitado, o mesmo deve ser visitado para que sua área seja inserida no IniFileArray
							this->IniFileArray->InsertNextValue(Node2->area); 
							}
	  				if (this->MaxElemLib==2)
		  				{
		  				vtkHM1DMesh::ListOfInt lst;
			  			lst.clear();
			  			lst.push_back(this->GetNodeID(Node));
			  			lst.push_back(this->GetNodeID(Node2));
			  			this->GroupElements.push_back(lst);
			  			this->ElementTypeArray->InsertNextValue(1); // elem
		  				// geracao do INIFILE
							this->IniFileArray->InsertNextValue(Node->area);
							
							// como o último nó não é visitado, o mesmo deve ser visitado para que sua área seja inserida no IniFileArray
							this->IniFileArray->InsertNextValue(Node2->area); 
		  				}
	  				}
		  		} // fecha if if (i== NodeNumber-2)
		  	else
		  		{
		  		if (i != NodeNumber -1) // nao preciso acessar informacao do ultimo no do segmento
		  			{
						// visita todos nós do segmento entre o primeiro nó (Zero) e o penultimo nó (excluindo estes)
						// por exemplo, se o segmento tem 8 nós, serão visitados o nó 1, 2, 3, 4, 5 
						// assumindo que o primeiro nó é o 0 e o último é o nó 7
						vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
						vtkHM1DMesh::ListOfInt lst;
						lst.clear();
						lst.push_back(this->GetNodeID(Node));
						lst.push_back(this->GetNodeID(Node2));
						this->GroupElements.push_back(lst);
							
						// geracao do INIFILE
						this->IniFileArray->InsertNextValue(Node->area);
						
						if (!this->MaxElemLib)
							{
		  				vtkErrorMacro("Max Element libs not definied!!");
							}
		  			else
		  				{
		  				this->ElementTypeArray->InsertNextValue(1); // ordinary element
		  				}
				 		} //fecha  if (i != NodeNumber -1)
					} //fecha else	
				}	//fecha else
		  i++;
			} // FECHA WHILE  
		} // fecha if se elemento é segmento  //******************************************************************************
	
	else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
		{
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
		
		// geracao do INIFILE
		// **************************************
		// area do no associado ao terminal é nula
		// **************************************
		this->IniFileArray->InsertNextValue(0);
		
		if (!this->MaxElemLib)
			{
			vtkErrorMacro("Max Element libs not definied!!");
			}
		else
			{
			this->ElementTypeArray->InsertNextValue(2); // terminal or heart
			}
		elem2 = elem;
		
		if (!elem2->GetFirstParent())	// se é o coracao pois o mesmo nao tem elemento pai
  		{
  	  child2 = elem2->GetFirstChild();   //retorna o segmento filho
	  	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
	  	if (Seg)
		  	{      
		    vtkHMNodeData *Node = Seg->GetNodeData(0);
		    vtkHM1DMesh::ListOfInt lst;
		  	lst.clear();
		  	lst.push_back(this->GetTerminalID(id));
		  	lst.push_back(this->GetNodeID(Node));
		  	this->GroupElements.push_back(lst);
		  	}
			}// fecha if se é coracao
		
		//******************************************************************************************************************
	  else // se o terminal tem pai
	  	{
		  // pegar o no zero do segmento filho
		  parent = elem2->GetFirstParent();
//		  child2 = parent->GetFirstChild(); // retorna o terminal
//		  child2 = parent->GetNextChild();  // retorna o primeiro segmento filho
		  child2 = elem2->GetFirstChild(); // retorna o primeiro segmento filho
		  if (child2)
		  	{
		    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
		   	vtkHMNodeData *Node = Seg->GetNodeData(0);
		   	
		   	vtkHM1DMesh::ListOfInt lst;
		   	lst.clear();
		   	//inseri na lista de grupo de elementos o id do ponto do terminal
		   	lst.push_back(this->GetTerminalID(id));
		   	
		   	while ( parent )
			   	{
			   	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
		     	int NodeNumber = SegPai->GetNodeNumber();
			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
			    lst.push_back(this->GetNodeID(NodePai));
			    parent = elem2->GetNextParent();
			   	}
			  
			  while ( child2 )
				  {
				  vtkHM1DSegment *SegFilho = vtkHM1DSegment::SafeDownCast(child2);
			    vtkHMNodeData *NodeFilho = SegFilho->GetNodeData(0);
			    lst.push_back(this->GetNodeID(NodeFilho));
				  child2 = elem2->GetNextChild();
				  }
		   	this->GroupElements.push_back(lst);
		   	
//		    if (parent->GetChildCount()==2) // se o terminal tem um pai e este segmento tem somente 2 filhos (seg+terminal)
//		    	{
//		     	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
//		     	int NodeNumber = SegPai->GetNodeNumber();
//			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
//			    vtkHM1DSegment *SegFilho = vtkHM1DSegment::SafeDownCast(child2);
//			    vtkHMNodeData *NodeFilho = SegFilho->GetNodeData(0);
//			    vtkHM1DMesh::ListOfInt lst;
//		  		lst.clear();
//		  		lst.push_back(this->GetTerminalID(id));
//		  		lst.push_back(this->GetNodeID(NodePai));
//		  		lst.push_back(this->GetNodeID(NodeFilho));
//		  		this->GroupElements.push_back(lst);
//		  	  }  // fecha if
//		    else // mais de um filho
//		         // notar que o metodo GetChildCount considera como filho o terminal mais os segmentos
//		    	{
//					vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
//					int NodeNumber = SegPai->GetNodeNumber();
//					vtkHMNodeData *Node = SegPai->GetNodeData(NodeNumber-1);
//					vtkHM1DMesh::ListOfInt lst;
//					lst.clear();
//					lst.push_back(this->GetTerminalID(id));
//					lst.push_back(this->GetNodeID(Node)); 
//					int Children =  parent->GetChildCount();
//					int i=1;
//					while (i<Children) // percorre todos segmentos filhos 
//						{
//						vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
//						vtkHMNodeData *Node = Seg->GetNodeData(0);
//						lst.push_back(this->GetNodeID(Node));
//						//if (i != 2) child2 = parent->GetNextChild();
//						child2 = parent->GetNextChild(); 
//						i++;	
//						}
//					this->GroupElements.push_back(lst);
//					parent = elem2->GetFirstParent();
//				  child2 = parent->GetFirstChild(); // retorna o terminal
//				  child2 = parent->GetNextChild();  
//					} // fecha else mais de um filho
		   
		   } // fecha if (child2)
		   
		  else // o terminal é folha
		  	{
//		   	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(parent);
//		   	int NodeNumber = Seg->GetNodeNumber();
//			  vtkHMNodeData *Node = Seg->GetNodeData(NodeNumber-1);
		    vtkHM1DMesh::ListOfInt lst;
		    lst.clear();
		    lst.push_back(this->GetTerminalID(id));
		    
		    while ( parent )
			   	{
			   	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
		     	int NodeNumber = SegPai->GetNodeNumber();
			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
			    lst.push_back(this->GetNodeID(NodePai));
			    parent = elem2->GetNextParent();
			   	}
		    
//		    lst.push_back(this->GetNodeID(NodePai));    
		    this->GroupElements.push_back(lst);  
		   	} // fecha else
	  	}  // fecha else se terminal tem pai
	 child = elem->GetFirstChild();
		} //-------------------------------- final do else --/ se atual elemento da arvore é um terminal
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->GenerateLists(id);
			}
		}	
}
//----------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::BuildElementArray()
{
	vtkHM1DMesh::ListOfInt lst;
  int i=0;
  
  // cria um array de int
  int *elementForGroup = new int[this->GroupElements.size()];
	  
  vtkHM1DMesh::VectorOfIntList::iterator it_vector;
  
  it_vector= this->GroupElements.begin();
   
  while (it_vector != this->GroupElements.end() )
  	{
  	lst=*it_vector;
  	vtkHM1DMesh::ListOfInt::iterator it;
	  it =lst.begin();
		// aqui eu posso construir a lista de *ELEMENT GROUPS
		while(it != lst.end())
			{
			it++;
			}
		elementForGroup[i]=lst.size();
		i++;
  	it_vector++;
  	}
  
  
  this->SetElementForGroup(elementForGroup);
  int *ElementType = new int[this->GroupElements.size()]; // vetor Element Type tem o mesmo tamanho do *ELEMENT GROUPS
  unsigned int z=0;
  while (z<this->GroupElements.size())
  	{
  	ElementType[z] = this->ElementTypeArray->GetValue(z);
  	z++;
  	}
  
  this->SetElementType(ElementType);
  
}

//----------------------------------------------------------------------------
vtkHM1DMesh::VectorOfIntList *vtkHM1DStraightModelWriterPreProcessor::GetGroupElements()
{
	return &GroupElements;
  
}


//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::CreateParam(vtkHM1DParam *param)
{
	vtkDebugMacro(<<"CreateParam");
	
	//Setando numero de grupos de parametros
	param->SetNumberOfGroups(this->points->GetNumberOfPoints()-this->NumberOfSegments);
	
	//Vetor para armazenar a quantidade parametros que cada grupo possui
	int n = param->GetNumberOfGroups();
	
	this->QuantityOfRealParameters = new int[n];
	this->QuantityOfIntegerParameters = new int[n];

	int pos=0;

	this->SetParameters(1, &pos);
	
	this->StraightModelVisited->Reset();
	
	//Setando os dados no objeto param
	param->SetQuantityOfRealParameters(this->QuantityOfRealParameters);
	param->SetQuantityOfIntegerParameters(this->QuantityOfIntegerParameters);
	param->SetRealParameters(&this->RealParameters);
	param->SetIntegerParameters(&this->IntegerParameters);
	
}

//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::SetParameters(int id, int *position)
{
	vtkDebugMacro(<<"SetParameters for data of the param file");
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
	{
//	cout << "  ::SetParameters -> Elemento já foi visitado: " << id << endl;
	return;
	}
//	cout << "vtkHM1DStraightModelWriterPreProcessor::SetParameters -> " << id << endl;
	this->StraightModelVisited->InsertNextId(id);
	
 	int pos = *position;
 	vtkHM1DTreeElement *elem;
 	vtkHM1DTreeElement *child;
 	
 	vtkHM1DTerminal *term;
 	vtkHM1DSegment *segm;
 	
 	if (this->StraightModel->IsSegment(id))
 		{
 		elem = segm = this->StraightModel->GetSegment(id);
 		child = elem->GetFirstChild();
 		this->SetSegmentData(segm, position);
 		
 		}
 	else if (this->StraightModel->IsTerminal(id))
 		{
 		elem = term = this->StraightModel->GetTerminal(id);
 		child = term->GetFirstChild();
 		this->SetTerminalData(term, position);
 		}
 	
 	if (!child)
	 	{
 		// É uma folha
 		return;
 		}
 	else
 		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
	 		{
	 		id = child->GetId();
	 		child = elem->GetNextChild();
	 		this->SetParameters(id, position);
	 		}
}

//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::SetTerminalData(vtkHM1DTerminal *term, int *position)
{
	vtkDebugMacro(<<"SetTerminalData for data of the param file");
	
	int numberOfParameters;
	int pos = *position;
	
	vtkHM1DParam::ListOfDouble lst;
	vtkHM1DParam::ListOfDouble::iterator it;
	
	vtkHM1DParam::ListOfInt listInt;
	vtkHM1DParam::ListOfInt::iterator itInt;
	
	//Declarando listas de dados e tempo do terminal
	vtkHM1DTerminal::ResistanceValuesList *r1;
	vtkHM1DTerminal::ResistanceValuesList::iterator itR;
	vtkHM1DTerminal::ResistanceTimeList *r1Time;
	vtkHM1DTerminal::ResistanceTimeList::iterator itRTime;
	vtkHM1DTerminal::ResistanceValuesList *r2;
	vtkHM1DTerminal::ResistanceTimeList *r2Time;
	vtkHM1DTerminal::CapacitanceValuesList *c;
	vtkHM1DTerminal::CapacitanceValuesList::iterator itC;
	vtkHM1DTerminal::CapacitanceTimeList *cTime;
	vtkHM1DTerminal::CapacitanceTimeList::iterator itCTime;
	vtkHM1DTerminal::PressureValuesList *p;
	vtkHM1DTerminal::PressureValuesList::iterator itP;
	vtkHM1DTerminal::PressureTimeList *pTime;
	vtkHM1DTerminal::PressureTimeList::iterator itPTime;
	
	//Obtendo listas de dados e tempo do terminal
	r1 = term->GetR1();
	r1Time = term->GetR1Time();
	r2 = term->GetR2();
	r2Time = term->GetR2Time();
	c = term->GetC();
	cTime = term->GetCTime();
	p = term->GetPt();
	pTime = term->GetPtTime();
	
	//Determinando a quantidade de parametros do terminal
	numberOfParameters = r1->size()+r2->size()+c->size()+p->size();
	
	vtkHM1DSegment *segm;
	
	//Caso a quantidade de parametros seja maior que quatro,
	//o terminal possui curva
	if ( numberOfParameters > 4 )
		{
		//Caso de um terminal comum que possui pai
		if ( term->GetFirstParent() )
			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e quantidade
			//de parametros de cada lista (r1, r2, c, p)
//			segm  = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
			
			
			//this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			
			if (r1->size() == 1) // se terminal possui valor fixo para R1
				this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			else // se tiver valor transiente, entao assumo que os demais parametros tambem sao transientes
				this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+ 4 ;
			
			
			for ( int i=0; i<term->GetNumberOfParents(); i++ )
				listInt.push_back(1);
			
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			
			listInt.push_back(r1->size());
			listInt.push_back(r2->size());
			listInt.push_back(c->size());
			listInt.push_back(p->size());
			}
		// Caso do coracao que nao possui pai
		else
			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e quantidade
			//de parametros de cada lista (r1, r2, c, p)
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			
			listInt.push_back(r1->size());
			listInt.push_back(r2->size());
			listInt.push_back(c->size());
			listInt.push_back(p->size());
			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+4;
			}
		
		//Adicionando a lista os 4 primeiros dados zerados
		//que sao descartados
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		
		//Adicionando os dados e tempo da lista de resistencia 1
		itRTime = r1Time->begin();
		for ( itR = r1->begin(); itR!=r1->end(); itR++ )
			{
			lst.push_back(*itRTime);
			itRTime++;
			lst.push_back(*itR);
			}
		
		//Adicionando os dados e tempo da lista de resistencia 2
		itRTime = r2Time->begin();
		for ( itR = r2->begin(); itR!=r2->end(); itR++ )
			{
			lst.push_back(*itRTime);
			itRTime++;
			lst.push_back(*itR);
			}
		
		//Adicionando os dados e tempo da lista de capacitancia
		itCTime = cTime->begin();
		for ( itC = c->begin(); itC!=c->end(); itC++ )
			{
			lst.push_back(*itCTime);
			itCTime++;
			lst.push_back(*itC);
			}
		
		//Adicionando os dados e tempo da lista de pressao
		itPTime = pTime->begin();
		for ( itP = p->begin(); itP!=p->end(); itP++ )
			{
			lst.push_back(*itPTime);
			itPTime++;
			lst.push_back(*itP);
			}
		
		//Conta realizada para contabilizar o total de parametros
		//que inclui tempo+dados+ os 4 valores descartados inseridos
		//no inicio da lista
		numberOfParameters = 2 * numberOfParameters + 4;
		}
	else
		{
//		if ( term->GetFirstParent() )
//			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e no final 0
			//pois, o terminal nao possui curva
//			segm  = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			for ( int i=0; i<term->GetNumberOfParents(); i++ )
				listInt.push_back(1);
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			listInt.push_back(0);
//			}
//		//Caso do coracao que nao possui pai
//		else
//			{
//			//Definindo a lista de parametros inteiros que sao:
//			//segmento de entrada (1), de saida (-1) e no final 0
//			//pois, o terminal nao possui curva
//			for ( int i=0; i<term->GetChildCount(); i++ )
//				listInt.push_back(-1);
//			listInt.push_back(0);
//			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+1;
//			}
		
		//Adicionando dados da lista de resistencia 1
		for ( itR = r1->begin(); itR!=r1->end(); itR++ )
			lst.push_back(*itR);
		
		//Adicionando dados da lista de resistencia 2
		for ( itR = r2->begin(); itR!=r2->end(); itR++ )
			lst.push_back(*itR);
		
		//Adicionando dados da lista de capacitancia
		for ( itC = c->begin(); itC!=c->end(); itC++ )
			lst.push_back(*itC);
		
		//Adicionando dados da lista de pressao
		for ( itP = p->begin(); itP!=p->end(); itP++ )
			lst.push_back(*itP);
		}
	
	//Setando quantidade de parametros do terminal
	this->QuantityOfRealParameters[pos] = numberOfParameters;
	//Adicionando ao vector double a lista de parametros reais
	this->RealParameters.push_back(lst);
	//Adicionando ao vector int a lista de parametros inteiros
	this->IntegerParameters.push_back(listInt);
	pos++;
	*position = pos;
	
}

//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::SetSegmentData(vtkHM1DSegment *segm, int *position)
{
	vtkDebugMacro(<<"SetSegmentData for data of the param file");
	
	int pos = *position;
	
	vtkHMElementData *elem;
	vtkHMNodeData *node1;
	vtkHMNodeData *node2;
	
	list<double> lst;
	list<double>::iterator it;
	
	//double *point;
	
	//Pegando dados do segmento para adicionar a lista de parametros reais
	for ( int i=0; i<segm->GetElementNumber(); i++ )
		{
		//Pegando elemento, node esquerdo e node direito
		elem = segm->GetElementData(i);
		node1 = segm->GetNodeData(i);
		node2 = segm->GetNodeData(i+1);
		
		//Adicionando na lista dados dos nodes
		lst.push_back(node1->area);
		lst.push_back(node2->area);
		lst.push_back(node1->alfa);
		lst.push_back(node2->alfa);
		
		//Adicionando na lista dados do elemento
		lst.push_back(elem->Elastin);
		lst.push_back(elem->Collagen);
		lst.push_back(elem->CoefficientA);
		lst.push_back(elem->CoefficientB);
		lst.push_back(elem->Viscoelasticity);
		lst.push_back(elem->ViscoelasticityExponent);
		lst.push_back(elem->ReferencePressure);
		lst.push_back(elem->InfiltrationPressure);
		lst.push_back(elem->Permeability);
		
		//Setando quantidade de parametros reais e inteiros
		this->QuantityOfRealParameters[pos] = 13;
		this->QuantityOfIntegerParameters[pos] = 0;
		pos++;
		
		//Adicionando ao vector double a lista de parametros do elemento
		this->RealParameters.push_back(lst);
		lst.clear();
		}
	
	*position = pos;
}

//-------------------------------------------------------------------------------------

int *vtkHM1DStraightModelWriterPreProcessor::GetElementForGroup()
{
	return this->nElementForGroup;
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetElementForGroup(int *elemForGroup)
{
	this->nElementForGroup = elemForGroup;
}
//-------------------------------------------------------------------------------------

int *vtkHM1DStraightModelWriterPreProcessor::GetElementType()
{
	return this->ElementType;
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetElementType(int *elemType)
{
	this->ElementType = elemType;
}



//----------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::BuildElementMat()
{

	 // verificar quando temos 2 ELEMENTOS somente !!!!!!!!!!!!!!!!!11
 
	 // cria o vetor
	 int *ElementMat = new int[this->GroupElements.size()];
	 
	 // pega a referencia do ElementType
	 //int *New = this->GetElementType();
	 
	 unsigned int i=0;
	 
	 while (i < this->GroupElements.size())
	 	{ 
	  if (this->GetElementType()[i] == 2) // || (this->GetElementType()[i] == 1)) 
	 		{
	 		if (!i) // para o coracao
	 		  ElementMat[i]=i+1;
	 	  else //demais terminais
	 		  ElementMat[i]=ElementMat[i-1]+1;
	 		}
	 	if (this->GetElementType()[i] == 1) 
	 		{
	 		ElementMat[i]=ElementMat[i-1]+1;
		 	}
	 	if (this->GetElementType()[i] == 3)
		 	{
	 		ElementMat[i]=ElementMat[i-1];
		 	}
	 	if (this->GetElementType()[i] == 4)
	 		{
	 		ElementMat[i]=ElementMat[i-1];
	 		}
	    i++;	
	 	}
	 this->SetElementMat(ElementMat);
}



//-------------------------------------------------------------------------------------

int *vtkHM1DStraightModelWriterPreProcessor::GetElementMat()
{
		return this->ElementMat;
}	
	
//-------------------------------------------------------------------------------------
	
void vtkHM1DStraightModelWriterPreProcessor::SetElementMat(int *elemMat)
{
		this->ElementMat = elemMat;
}

//-------------------------------------------------------------------------------------

int vtkHM1DStraightModelWriterPreProcessor::GetNumberOfElementGroups()
{
   return this->GroupElements.size();
}

//-------------------------------------------------------------------------------------
void vtkHM1DStraightModelWriterPreProcessor::GenerateDirichletsValues()
{
	vtkIntArray *DirichletArray= vtkIntArray::New();
	DirichletArray->SetNumberOfComponents(this->GetDOF());
	DirichletArray->SetNumberOfTuples(this->GetNumberOfPoints());
	int i,j;
	int *n;
	n = new int[this->GetDOF()];
	for (i=0; i<this->GetNumberOfPoints(); i++ )
		{
	  for(j=0; j<this->GetDOF(); j++)
			{
			n[j]=0;
			}
	  DirichletArray->SetTupleValue(i, n);
		}  
		
	this->SetDirichletsTag(DirichletArray);

	// geracao dos dos valores DirichLets
	// vetor de double
	vtkDoubleArray *DirichletDoubleArray= vtkDoubleArray::New();
	
	DirichletDoubleArray->SetNumberOfComponents(this->GetDOF());
	DirichletDoubleArray->SetNumberOfTuples(this->GetNumberOfPoints());
	
	
	double *n2;
	n2 = new double[this->GetDOF()];
	
	for ( i=0; i<this->GetNumberOfPoints(); i++ )
		{
	  for( j=0; j<this->GetDOF(); j++)
			{
	  	n2[j]=0.0;
	  	}
	  DirichletDoubleArray->SetTupleValue(i, n2);
		}
	
	this->SetDirichlets(DirichletDoubleArray);
	
	
	delete [] n;
	delete [] n2;
	
}


//----------------------------------------------------------------------------
vtkIntArray *vtkHM1DStraightModelWriterPreProcessor::GetDirichletsTag()
{
	return this->DirichletsTag;
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetDirichletsTag(vtkIntArray *d)
{
	this->DirichletsTag = d;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHM1DStraightModelWriterPreProcessor::GetDirichlets()
{
	return this->Dirichlets;
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetDirichlets(vtkDoubleArray *d)
{
	this->Dirichlets = d;
}
//-------------------------------------------------------------------------------------


vtkDoubleArray *vtkHM1DStraightModelWriterPreProcessor::GetIniFileArray()
{
	return this->IniFileDoubleArray;
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetIniFileArray(vtkDoubleArray *d)
{
	this->IniFileDoubleArray = d;
}
//-------------------------------------------------------------------------------------


void vtkHM1DStraightModelWriterPreProcessor::GenerateIniFile()
{
	vtkDoubleArray *IniFileDoubleArray2 = vtkDoubleArray::New();
	IniFileDoubleArray2->SetNumberOfComponents(this->GetDOF());
	IniFileDoubleArray2->SetNumberOfTuples(this->GetNumberOfPoints());

  	//**********************************
  	// 1D Tree DOF										
  	//           0  1 2 3
  	// DOF= 4 ---Q Pt A P
  	// DOF= 3 ---Q A  P
  	//**********************************

  // acessa o elemento 0 do primeiro segmento depois do coracao   
  // para pegar o valor da pressao de referencia e colocar 
  // como valor para as pressoes no inifile
	vtkHM1DTreeElement *tree 		= this->StraightModel->GetSegment(this->StraightModel->Get1DTreeRoot()->GetFirstChild()->GetId());
	vtkHM1DSegment *seg = vtkHM1DSegment::SafeDownCast(tree);
	vtkHMElementData *elem = seg->GetElementData(0);
		

  for(int i=0; i< this->GetNumberOfPoints() ; i++)
  	{
  	double *tupla = IniFileDoubleArray2->GetTuple(i);
  		
  	if (this->GetDOF()==4)
  		{
  		tupla[0]=0.0;
  		tupla[1]=elem->ReferencePressure;	// valor dummy para pressao inicial
  		tupla[2]=this->IniFileArray->GetValue(i); //// area do no
  		tupla[3]=elem->ReferencePressure; // valor dummy para pressao inicial
  		}
  	if (this->GetDOF()==3)
  		{
   		tupla[0]=0.0;
   		tupla[1]=this->IniFileArray->GetValue(i); // area do no
   		tupla[2]=elem->ReferencePressure; // valor dummy para pressao inicial
  		}
  	IniFileDoubleArray2->SetTupleValue(i,tupla);
  	}
  	this->SetIniFileArray(IniFileDoubleArray2);
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetDoubleParam(double DoubleParamArray[21])
{
  for (int i=0; i < 21; i++)
		this->DoubleParam[i] = DoubleParamArray[i];  	
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetIntParam(int IntParamArray[17])
{
  for (int i=0; i < 17; i++)
		this->IntParam[i] = IntParamArray[i];  	
}

//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::CreateNewBasParam(void)
{
	// definicoes retiradas de http://hemo01/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Basparam.txt
	
	
	this->BasParam->SetInitialTime(this->IntParam[7]);
	this->BasParam->SetRenumbering(this->IntParam[0]);
	this->BasParam->SetTimeQuantityScreen(this->IntParam[1]);
	this->BasParam->SetTimeQuantityFile(this->IntParam[2]);
	this->BasParam->SetMaxIterationOfResolver(this->IntParam[3]);
	this->BasParam->SetMaxIteration(this->IntParam[3]);
	this->BasParam->SetRalaxacao(1.000000e+000);
	this->BasParam->SetQuantityStep(1);
	
	string *name = new string[this->IntParam[4]];
	
	this->BasParamDOFNumber = this->IntParam[4];
	
	if (this->IntParam[4]==4)
		{
		name[0] = "'Caudal'";
		name[1] = "'Pt'";
		name[2] = "'Area'";
		name[3] = "'Presion'";
		}	 
	else
		{
		name[0] = "'Caudal'";
		name[1] = "'Area'";
		name[2] = "'Presion'";
		}
	       
	this->BasParam->SetNameDegreeOfFreedom(name);
	
	// sempre true *StepContinuationControl
	this->BasParam->SetConfigProcessExternal(1);
	this->BasParam->SetRalaxacao(this->DoubleParam[11]);
	
	double *norma = new double[this->IntParam[4]];
	if (this->IntParam[4]==4)
		{ 
		norma[0]= this->DoubleParam[3]; //ref flow
	  norma[1]= this->DoubleParam[5]; // ref elast pr
 	  norma[2]=this->DoubleParam[7]; // ref are
	  norma[3]=this->DoubleParam[9]; // ref pressure
		}
	else //dof = 3
		{
		norma[0]= this->DoubleParam[3]; //ref flow
	  norma[1]= this->DoubleParam[7]; // ref area
 	  norma[2]=this->DoubleParam[9]; // ref pressure
		}	
	this->BasParam->SetNorma(norma);
	
	
	
	double *Tolerance = new double[this->IntParam[4]];
	if (this->IntParam[4]==4)
		{ 
		Tolerance[0]= this->DoubleParam[4]; //conv flow
	  Tolerance[1]= this->DoubleParam[6]; // conv elast pr
 	  Tolerance[2]=this->DoubleParam[8]; // conv are
	  Tolerance[3]=this->DoubleParam[10]; // conv pressure
		}
	else //dof = 3
		{
		Tolerance[0]= this->DoubleParam[4]; //conv flow
	  Tolerance[1]= this->DoubleParam[8]; // conv area
 	  Tolerance[2]=this->DoubleParam[10]; // conv pressure
		}	
	this->BasParam->SetTolerance(Tolerance);
	
	
	int Nums[3]={0,0,0};
	//this->BasParam->SetNumsSubStep(Nums);
	this->BasParam->SetNums(Nums);
	
	
	this->BasParam->SetDelT(this->DoubleParam[0]);
	this->BasParam->SetTini(this->DoubleParam[1]);
	this->BasParam->SetTmax(this->DoubleParam[2]);
	
	
	if (this->IntParam[4]==4)
	  this->BasParam->SetQuantityOfDifferentElements(4);
	else
	  this->BasParam->SetQuantityOfDifferentElements(2);
	
	
	// no modelo 1D esse valor é sempre 14 - MaxLCommonPar do campo *ElementLibraryControl
	this->BasParam->SetMaximumQuantityOfParameters(14);
	
	
	//*ElementLibraryControl  (MaxElemLib)
	if (this->IntParam[4]==4) //se Least Squares ViscoELastic
	  this->BasParam->SetQuantityOfDifferentElements(4);
	else //se Least Squares
	  this->BasParam->SetQuantityOfDifferentElements(2);
	//************************************************
	
	// se o primeiro parametro de *StepContinuationControl é T os dois
	// primeiros parametros de SubStep são F e F
	
	// OpA
	this->BasParam->SetResultantMatrixIsSymmetrical(false);
	//OpB
	this->BasParam->SetCalculationStepIsLinear(false);
	
	// no caso de os dois serem False os parametros 1 e 2 sao dummy
	
	// pode ser dummy
	this->BasParam->SetMaxIterationSubStep(100);
	// pode ser dummy
	this->BasParam->SetRalaxacaoSubStep(1.000);
	
	
	// se o tipo de resolvedor for outro deve especifcar os demais parametros
	this->BasParam->SetResolveLibrary(this->IntParam[8]);

	this->BasParam->SetMaxIterationOfResolver(this->IntParam[9]);
	this->BasParam->SetToleranceOfConvergence(this->DoubleParam[17]);
	this->BasParam->SetSpacesOfKrylov(this->IntParam[10]);
	this->BasParam->SetNumberOfCoefficients(this->IntParam[11]);
	this->BasParam->SetToleranceOfExcuse(this->DoubleParam[18]);
	

	int *elementType = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			//elementType[i*5+j]=0;
			
			if (this->BasParam->GetQuantityOfDifferentElements() ==2)
				{
				if ((!j) && (!i)) elementType[i*5+j]=1;
				if ((!j) && (i==1)) elementType[i*5+j]=3;
				}
			else
				{
				if ((!j) && (!i)) elementType[i*5+j]=13;
				if ((!j) && (i==1)) elementType[i*5+j]=10;
				if ((!j) && (i==2)) elementType[i*5+j]=11;
				if ((!j) && (i==3)) elementType[i*5+j]=12;
				}	
			
			if (j==3) elementType[i*5+j]=((i+1)*-1);
			
			// terceira (j=2) e quinta (j=4) colunas usualmente zeros
			if (j==4 || j==2) elementType[i*5+j]=0;
			
			// segunda coluna dos elementos 11 e 12 sao 1 se elementos nao aditivos
			// 0 se aditivos
			if ((j==1) && (elementType[(i*5+j)-1]==12))  elementType[i*5+j]=!this->IntParam[5];
			if ((j==1) && (elementType[(i*5+j)-1]==11))  elementType[i*5+j]=!this->IntParam[5];
			
			// segunda coluna do elemento 1 -- sempre será zero
			if ((j==1) && (elementType[(i*5+j)-1]==1))  elementType[i*5+j]=0;
			
			
			
			// elemento 10 ou 3 o valor da coluna 2 depende de valor se aditivity =no valor é 1
			// se "yes" foi selecionado o valor do vetor int será 1
			if ((j==1) && ((elementType[(i*5+j)-1]==10) || (elementType[(i*5+j)-1]==3)))
			  elementType[i*5+j] = !this->IntParam[5];
			  
			  
			  
			
			// elemento 13 coluna 2 sempre 0
			if ((j==1) && ((elementType[i*5+j-1]==13)))  elementType[i*5+j]=0;
			}
		}
	this->BasParam->SetElementType(elementType);
	
	
	/*
	Definicao do CommomPar
	********************************
	para elementos:1, 11, 12 e 13 
	********************************
	CommonPar(1): Densidade do fluido.
	CommonPar(2): Viscosidade do fluido.
	CommonPar(3): Coeficiente indicando o perfil de velocidade que assume o modelo 1D (usualmente 1).
	CommonPar(4): Indicador da posição do grau de liberdade da pressão a partir do primeiro grau de liberdade (2-problema bidimensional, 3-problema tridimensional).
	CommonPar(5): Parâmetro de implicitude temporal do esquema theta (valor entre 0 e 1).
	CommonPar(6): Indicador do tipo de lei constitutiva usada no modelo 1D (usualmente 2).
	CommonPar(7): Parâmetro de gravidade na direção X (não usado, portanto vai valor dummy, usualmente 0).
	CommonPar(8): Parâmetro de gravidade na direção Y (não usado, portanto vai valor dummy, usualmente 0).
	CommonPar(9): Parâmetro de gravidade na direção Z (não usado, portanto vai valor dummy, usualmente 0). 
	*/
	
	/*
	********************************
	CommomPar para elementos: 10 e 3
	********************************
	CommonPar(1): Parâmetro de penalização. Só se o elemento for aditivo. Vale 0 se é não aditivo e -1 se é aditivo.
	CommonPar(2): Indicador da posição do grau de liberdade da pressão a partir do primeiro grau de liberdade (2-problema bidimensional, 3-problema tridimensional). 
	*/
	
	double *paramElementType = new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			{
			paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=0.0;
			
			//***************************
			//***************************
			// setando os elementos da segunda linha do CommonPar que correspondem ao elemento terminal
			// elementos 10 e 3
			
			// sempre segunda linha do commonpar corresponderá ao elemento terminal
			if ((i==1 && !j) && (this->IntParam[5])) // segunda linha e coluna 1
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->DoubleParam[12];
			
			if ((i==1 && !j) && (!this->IntParam[5])) //segunda linha e coluna 1
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 1.0;	
			
			if (i==1 && j==1) // segunda linha e coluna 2
				//paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 3;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= (this->IntParam[4] -1);
				
			//***************************
			//***************************
			
			
			
			//***************************
			// setando os elementos das linhas 1 (#2 elementos) e linhas 1, 3 e 4 (#4 elementos)
			// 
			// primeira coluna tem o valor Densidade do fluido
			if (!j && i!=1)  paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->DoubleParam[13];
						
			// segunda coluna : Viscosidade do fluido. 
			if (j==1 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->DoubleParam[14];
			
			// terceira coluna Velocity Profile;
			if (j==2 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->DoubleParam[16];
			
			// quarta coluna posicao da pressao em funcao do primeiro DOF
			if (j==3 && i!=1)
				{ 
				if (this->IntParam[4]==4)
				  paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 3;
				else
				  paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 2;
				}
			
			
			// coluna 5 parameter theta
			if (j==4 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->DoubleParam[15];
					
			// coluna 6 Artery Wall Law
			if (j==5 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=  this->IntParam[6];
			}
		}
	this->BasParam->SetParamElementType(paramElementType);
	this->BasParam->SetNormaSubStep(norma);
	this->BasParam->SetToleranceSubStep(Tolerance);
	this->BasParam->SetNameDegreeOfFreedomSubStep(name);
	this->BasParam->SetNumsSubStep(Nums);

	// deletando os arrays que chegam ao metodo
	//this->DoubleParam->Delete();
	//this->IntParam->Delete();
	
	this->BasParam->SetLogFilename(this->LogFilename);



	this->BasParam->SetSolverType(this->IntParam[12]); // Solver type (0: Sequential 1: Parallel));
	
	// opcoes utilizadas quando o tipo de solver é Paralelo
	
	this->BasParam->SetParallelSolverNumberOfProcessors(this->IntParam[13]);
	
	this->BasParam->SetParallelSolverRLS(this->IntParam[8]);
	
	this->BasParam->SetParallelSolverGMRESIterations(this->IntParam[14]);
	
	this->BasParam->SetParallelSolverMaxIterations(this->IntParam[15]);
	
	this->BasParam->SetParallelSolverPreConditioning(this->IntParam[16]);
	
	this->BasParam->SetParallelSolverRelativeConvError(this->DoubleParam[19]);
	
	this->BasParam->SetParallelSolverAbsConvError(this->DoubleParam[20]);
}


//-------------------------------------------------------------------------------------

//void vtkHM1DStraightModelWriterPreProcessor::MakePropertiesData()
//{
//	//Este método gera um map contendo dados da propriedade.
//	this->PropertyData->GeneratePropertiesMap(this->StraightModel);
//}



//-------------------------------------------------------------------------------------


int vtkHM1DStraightModelWriterPreProcessor::InitializeWriteProcess(char *path)
{
	// seta numero max de elem lib #2 ou #4
	// setando o numero de maximo de elementos (parametro num1 de ElementLibraryControl
	// do arquivo BasParam.txt) 
	// se formulation = least square entao Max Elem lib = 2
	// se formulation = least square viscoelastic entao Max Elem lib = 4
	this->SetMaxElemLib(this->BasParam->GetQuantityOfDifferentElements());
	
	this->BasParam->SetDoFNumber(this->BasParamDOFNumber);
	
	// seta dimension
	// sempre é 2 no caso 1D -- depois este valor deve ser determinado dinamicamente
	this->SetDimension(2);
		
	// seta DOF (Degree of Freedom number)
	// pega o valor de DOF do BasParam...
	if (this->BasParamDOFNumber != -1)
		this->SetDOF(this->BasParamDOFNumber);
	else
		vtkErrorMacro("DOF Number not defined in vtkHMStraightModelWidget");
	
	
	 
	// gera lista de coordenadas
	this->GenerateCoordinates(1);
	
	this->StraightModelVisited->Reset();
	
	// gera lista de incidencia de elementos
	this->GenerateLists(1);
	this->StraightModelVisited->Reset();
	
	
	this->BuildElementArray();
	this->BuildElementMat();
	
	this->GenerateDirichletsValues(); 
	
	// criar um novo Mesh Param IniFile
	
	//vtkHM1DMesh *Mesh 	= vtkHM1DMesh::New();
	vtkHM1DParam *Param 	= vtkHM1DParam::New();
	vtkHM1DIniFile *IniFile 	= vtkHM1DIniFile::New();
	
	
	// Updating Mesh
		
	this->Mesh->SetDegreeOfFreedom(this->GetDOF());
	this->Mesh->SetDimension(this->GetDimension());
	this->Mesh->SetNumberOfElementGroups(this->GetNumberOfElementGroups());
	this->Mesh->SetNumberOfPoints(this->GetNumberOfPoints());
	
	this->Mesh->SetElementType(this->GetElementType());
	this->Mesh->SetElementMat(this->GetElementMat());
					
	this->Mesh->SetElementForGroup(this->GetElementForGroup());
	
	
	this->Mesh->SetPoints(this->GetPoints());
	
	// vetor para construir a lista de *INCIDENCE
	this->Mesh->SetGroupElements(this->GetGroupElements());

		
	this->Mesh->SetDirichletsTag(this->GetDirichletsTag());
	this->Mesh->SetDirichlets(this->GetDirichlets());
	
  //vtkSMSourceProxy* sproxy = this->GetPVSource()->GetProxy();
	//vtkProcessModule* pm = vtkProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());
  
	//vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(sproxy->GetID(0)));
	  
	  
		
	
	this->CreateParam(Param);

	vtkHM1DData *Data = vtkHM1DData::New();
	
	Data->SetMesh(this->Mesh);
	

	Data->SetBasParam(this->BasParam);
	
	
	Data->SetParam(Param);
	
	
	
	if (this->IntParam[12]) // se solver type é paralelo.
		{
		vtkHMParallelSolverScriptWriter *ScriptWriter = vtkHMParallelSolverScriptWriter::New();
		if (!ScriptWriter->WriteParallelSolverScript(this->BasParam, path))
			vtkErrorMacro("Error on writing the Script for Parallel Solver!");
		ScriptWriter->Delete();
		}
	
	this->GenerateIniFile();
  IniFile->SetInitialConditions(this->GetIniFileArray());
	Data->SetIniFile(IniFile);

	vtkHM1DStraightModelWriter *writer = vtkHM1DStraightModelWriter::New();
	writer->SetWhichFile(this->WhichFile);
	writer->SetInput(Data);
	writer->SetFileName(path);
	writer->WriteData();
	
	//Mesh->Delete();
	Param->Delete();	
	writer->Delete();
	IniFile->Delete();
	Data->Delete();
//	PropertyWriter->Delete();
	
	//escrever arquivo no formato hemolab
	char fileName[256];
	sprintf(fileName, "%s/%s", path, "hemolab.hml1D");
	
	vtkHM1DHeMoLabWriter *hemolabWriter = vtkHM1DHeMoLabWriter::New();
	hemolabWriter->SetStraightModel(this->StraightModel);
	hemolabWriter->SetFileName(fileName);
	hemolabWriter->WriteData();
	
	hemolabWriter->Delete();
	
	return 1;
}


//-------------------------------------------------------------------------------------

void vtkHM1DStraightModelWriterPreProcessor::SetLogfilename (char *LogFilename)
{
		//this->LogFilename = LogFilename;   
	strcpy(this->LogFilename,LogFilename);	
}
//-------------------------------------------------------------------------------------


void vtkHM1DStraightModelWriterPreProcessor::SetWhichFile(int WhichFile[4])
{
  // o vetor WhichFile so esta sendo utilizado para que somente os arquivos selecionados sejam gerados
  // o processamento dos arquivos continua sendo realizado - diferente dos modulos 3D e acoplado
  // PZ - junho 2008
  for (int i=0; i < 4; i++)
		this->WhichFile[i] = WhichFile[i];  	

}
//-------------------------------------------------------------------------------------
