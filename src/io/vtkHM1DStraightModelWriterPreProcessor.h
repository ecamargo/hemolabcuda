
#ifndef VTKHM1DSTRAIGHTMODELWRITERPREPROCESSOR_H_
#define VTKHM1DSTRAIGHTMODELWRITERPREPROCESSOR_H_

#include "vtkObject.h"
#include "vtkHM1DStraightModel.h"
#include "vtkPoints.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DParam.h"
//#include "vtkHM1DPropertyData.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM1DStraightModelGrid;

#include <vector>
#include <list>
#include <string>
	
class VTK_EXPORT vtkHM1DStraightModelWriterPreProcessor : public vtkObject
{
public:
	
	//BTX
	typedef std::list<double> ListOfDouble;
	typedef std::vector<ListOfDouble*> VectorOfDoubleList;
	
	typedef	std::map<vtkHMNodeData *, int> NodeIDPairType;
	typedef std::map<int, int> TerminalIDPairType;
	//ETX
	
	
	static vtkHM1DStraightModelWriterPreProcessor *New();
	vtkTypeRevisionMacro(vtkHM1DStraightModelWriterPreProcessor,vtkObject);
	void PrintSelf(ostream& os, vtkIndent indent);

	
	// Description:	
  // set the straight model 
  //  void SetStraightModel(vtkHM1DStraightModel *STM);
	void SetStraightModel(vtkHM1DStraightModelGrid* grid);
 
  // Description:
	// recursive method that walks in the StraightModel tree (from heart id 1) and generates points list *COORDINATES and create the map between 
	// point and actors(segments) and between points with terminal-id which will be used later in order to create incidence list  
  void GenerateCoordinates(int id);
  
  // Description:
  // return Points instance
  vtkPoints *GetPoints();
  
  // Description:
  // return the number of points 
  int GetNumberOfPoints();
  
  
  // Description:
  // return the int ID from any node (the relation betwenn node id and node is created by GenerateCoordinates method) 
  int GetNodeID(vtkHMNodeData *Node);
  
  // Description:
  // return the int ID (order from each terminal or node is found in straight model tree ) from a terminal 
  // (the relation between terminal id and ID is created by GenerateCoordinates method) 
  int GetTerminalID(int id);
  
  // Description:
  // recursive method that walks in the straightModel and produces (using map created by GenerateCoordinates method)
  // the incidence list, fills the IniFileArray and Group Elements array; 
  void GenerateLists(int id);
    
  // Description:
  // Generates the integer arrays ELEMENT TYPE e ELEMENT GROUPS 
  void BuildElementArray();
  
  //BTX
	
	// Description:
  //
  vtkHM1DMesh::VectorOfIntList *GetGroupElements();
   
   
  //ETX 
	// Description:
	// set/get methods for the model's number of degrees of freedom 
	vtkSetMacro(DOF, int);
	vtkGetMacro(DOF, int);
	
	
	// Description:
	// set/get method for the dimension
	vtkSetMacro(Dimension, int);
	vtkGetMacro(Dimension, int);
	
	// Inicio dos metodos para geracao do param
	// Description:
	// Method for creation of the data for param file
	//BTX
	void CreateParam(vtkHM1DParam *param);
	
	// Description:
  // SetParameters for data of the param file
	void SetParameters(int id, int *position);
	
	// Description:
  //Set Terminal Data for data of the param file
	void SetTerminalData(vtkHM1DTerminal *term, int *position);
	
	// Description:
  // Set Segment Data for data of the param file
	void SetSegmentData(vtkHM1DSegment *segm, int *position);
	
	//ETX
	// Fim dos metodos para geracao do param

	
	// Description:
	// Set/Get nElementForGroup
	int *GetElementForGroup();
	void SetElementForGroup(int *elemForGroup);
	
	// Description:
	// Set/Get ElementType
	int *GetElementType();
	void SetElementType(int *elemType);
	
	
	// Description:
	// walk in the ElementType array and generates ELEMENT MAT integer array
	void BuildElementMat();
	
	// Description:
	// Set/Get Element mat
	int *GetElementMat();
	void SetElementMat(int *elemMat);
	
	// Description:
	// Set/Get MaxElemLib
	vtkSetMacro(MaxElemLib, int);
	vtkGetMacro(MaxElemLib, int);
	
	// Description:
	// return the number of element groups
	int GetNumberOfElementGroups();
	
	
	// Description:
	// Set/Get DirichletsTag
	vtkIntArray *GetDirichletsTag();
	void SetDirichletsTag(vtkIntArray *d);
	
	// Description:
	// Set/Get Dirichlets
	vtkDoubleArray *GetDirichlets();
	void SetDirichlets(vtkDoubleArray *d);
	
	// Description:
	// generate DirichletsTag and Dirichlets only with zeros 
	void GenerateDirichletsValues();
	
	// Description:
	// generate Infile array with area as only non zero value
  void GenerateIniFile();
  
  // Description:
	// Set /Get IniFileArray 
  vtkDoubleArray *GetIniFileArray();
  void SetIniFileArray(vtkDoubleArray *d);
  
  
  // Description:
  // covers current StraightModel tree in order to get data to  
  // create vtkHM1DMesh, vtkHM1DIniFile and vtkHM1DParam objects. 
  int InitializeWriteProcess(char *path);
  
  // Description:
  // create BasParam with values from HemoLab General Parameters setup interface
	void CreateNewBasParam(void);
		
	// Description:
  //	set double array parameters used in BasParam creation 
	void SetDoubleParam(double DoubleParamArray[21]);

	// Description:
  //	set integer array parameters used in BasParam creation
	void SetIntParam(int IntParamArray[17]);
		

	// Description:
	// set log file name
	void SetLogfilename (char *LogFilename);
	
	// Description:
	// Set whichFile array that selects which solver file will be generated
	void SetWhichFile(int WhichFile[4]);
	
	
	
	void SetMesh(vtkHM1DMesh *Mesh)
	{
	this->Mesh = Mesh;
	}
	
	
	void SetBasParam(  vtkHM1DBasParam *BasParam)
	{
	this->BasParam =  BasParam;
	}
	

	
//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


protected:
	vtkHM1DStraightModelWriterPreProcessor();
	~vtkHM1DStraightModelWriterPreProcessor();
	
	// Description:
	// reference to the 1D straightModel
	vtkHM1DStraightModel *StraightModel;
	
	//Description:
	//Reference to PropertyData.
//	vtkHM1DPropertyData* PropertyData;
	
	
	// Description:
	// object used in Mesh.txt creation 
  vtkPoints* points;
	
	//BTX
	
	// Description:
	// pair that store vtkHMNodeData and its id point order
	NodeIDPairType NodeIDPair;

	// Description:
	//pair that store vtkHMTerminal and its id point order
	TerminalIDPairType TerminalIDPair;
	
	// Description:
	// store the current number of visited points in the 1DStraightModel
	int point;
	
	
  // Description:
  // Vector with an list of the element groups
	vtkHM1DMesh::VectorOfIntList GroupElements;
	//ETX
	
	// Description:
	// number of Degree of Freedom from current model
	int DOF;
	
	// Description:
	// 2 if only X and Y coordinates used; 3 if XYZ
	int Dimension;
	
	// Description:
  // Number of elements for group
  int *nElementForGroup;
  	
  	
  // Description:
  // Types of elements of the 1D tree (terminal, segment, etc...)
  //*ELEMENT TYPE
  // if MaxElemLib from basparam is 4 
  // 2 heart / terminal
  // 1 elem
  // 3 elem in
  // 4 elem out
	int *ElementType;
	
	
	// Description:
	// object used to create Mesh's Element Type list
	vtkIntArray *ElementTypeArray;
	
	
	// Description:
  // Element mat of the 1D tree
  int *ElementMat;

	// Description:
  // default values of 2 or 4
	int MaxElemLib;
  
  //BTX
	// Description:
  // used to build param.txt list
  int *QuantityOfRealParameters;	

	// Description:
	// double array used in Param.txt file creation
	int *QuantityOfIntegerParameters;
	
	// Description:
	// vector of double list used in Param.txt file creation
	vtkHM1DParam::VectorOfDoubleList RealParameters;
	
	// Description:
	// vector of int list used in Param.txt file creation
	vtkHM1DParam::VectorOfIntList IntegerParameters;
	
	//ETX
	// Fim de declaracoes do arquivo param
	
	
	// Description:
	// object used in Mesh.txt creation
	vtkIntArray *DirichletsTag;
	
	// Description:
	// object used in Mesh.txt creation
	vtkDoubleArray *Dirichlets;
		
	// Description:
	// object used in IniFile.txt creation
	vtkDoubleArray *IniFileArray;
		
	// Description:
	// object used in IniFile.txt creation
	vtkDoubleArray *IniFileDoubleArray;
	
	// Description:
	// reference to BasParam object created by Reader
	vtkHM1DBasParam *BasParam;
	
	// Description:
	// reference to Mesh object created by reader
	vtkHM1DMesh *Mesh;
	
	// Description:
	// Number of Deegre of Freedom in BasParam
	int BasParamDOFNumber; 
	
	// Description:
	//  Double parameters used in solver files generation
	//  0, TimeStep
	//  1, InitialTime
	//  2, FinalTime
	//  3, ReferenceFlux
	//  4, ConvergenceFlux
	//  5, ElasticPressure
	//  6, ConvergenceElasticPressure
	//  7, Area
	//  8, ConvergenceArea
	//  9, Pressure
	//  10, ConvergencePressure
	//  11, SubRelaxationParameter
	//  12, Penalization
	//  13, Density
	//  14, Viscosity
	//  15, ParameterTheta
	//  16, VelocityProfile
	//  17, ConvergenceError
	//  18, DispTolerance
	//  19, ParallelRelativeError
	//	20, ParallelAbsError
	double DoubleParam[21];
	
	// Description:
	// Integer parameters used in solver files generation.
	//  0, Renumbering
	//  1, ScreenOutputControl
	//  2, FileOutputControl
	//  3, Iterations
	//  4, Formulation
	//  5, Aditivity
	//  6, ArteryWallLaw
	//  7, ResumeIniFile
	//  8, ResolutionSolverType
	//  9, entryIterationsBeforeRestart
	//  10,entrykrylov
	//  11,entryFParam
	//	12,SolverType
	//	13,ParallelNumberOfProcessors
	//	14,ParallelIterationsRestart
	//	15,ParallelMaxNumberIter
	//	16,ParallelPreConditioning
	int IntParam[17];
	
	
	// Description:
	//Number of segments of the tree, discard segments that are not connected the main tree
	int NumberOfSegments;
	
	// Description:
	// store log file name used in Basparam file
	char LogFilename[20];
	
	
	// Description:
	// List com ids of the elements visited on 1d tree
	vtkIdList *StraightModelVisited;
	
	
	int WhichFile[4];
	
	

	
	
}; //End class

#endif 
