/*
 * $Id: vtkHM3DBasParamReader.cxx 2402 2007-11-01 13:49:27Z eduardo $
 */
#include "vtkHM3DBasParamReader.h"
#include "vtkHM3DMeshReader.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DMesh.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>
#include <string>

#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHM3DBasParamReader, "$Revision: 2402 $");
vtkStandardNewMacro(vtkHM3DBasParamReader);

vtkHM3DBasParamReader::vtkHM3DBasParamReader()
{
	vtkDebugMacro(<<"Reading BasParam data...");
	this->BasParam = vtkHM1DBasParam::New();
}

vtkHM3DBasParamReader::~vtkHM3DBasParamReader()
{
	this->BasParam->Delete();
}

void vtkHM3DBasParamReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
//Open File
int vtkHM3DBasParamReader::OpenFile()
{
	if (this->ReadFromInputString)
	{
		if (this->InputArray)
		{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
		}
		else if (this->InputString)
		{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
		}
	}
	else
	{
		vtkDebugMacro(<< "Opening BasParam file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
		{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
		}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
			
		return 1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM3DBasParamReader::CloseFile()
{
	vtkDebugMacro(<<"Closing BasParam file");
	if ( this->IS != NULL )
	{
		delete this->IS;
	}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
// Read Initial Time tag in file.
int vtkHM3DBasParamReader::ReadInitialTime()
{
	vtkDebugMacro(<< "Reading BasParam file Initial Time");
	
	int iniTime;
	if(!this->Read(&iniTime))
	{
		vtkErrorMacro(<<"Can't read Initial Time!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetInitialTime(iniTime);
	return 1;
}

//----------------------------------------------------------------------------
// Find and Read Tags in file.
int vtkHM3DBasParamReader::FindReadTags(vtkHM1DMesh *mesh)
{
	char line[256];
	int i = 1;

	for(int z = 0; z < 256; z++)
		line[z] = 0;

	while( !this->IS->eof() )
		{	
		if (!this->ReadLine(line))
			{
			if(this->IS->eof())
				return 1;
			vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: " 
	                  << (this->FileName?this->FileName:"(Null FileName)"));
			this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
			return 0;
			}
		
		if( strncmp("*Param Write Swicht", line, 19) == 0 )
			{
			this->ReadParamWriteSwicht();
			}
		else
			{		
			if( strncmp("*Renumbering", line, 12) == 0 )
				{
				this->ReadHeader();
				}
			else
				{
				if( strncmp("*StepContinuationControl", line, 24) == 0 )
					{
					this->ReadStepContinuationControl(mesh);
					}
				else	
					{	
					if( strncmp("*TimeStep", line, 9) == 0 )		
						{
						this->ReadTimeStep();
						}
					else
						{
						if( strncmp("*OutputControl", line, 14) == 0 )
							{
							this->ReadOutputControl();
							}
						else
							{
							if( strncmp("*NodeOutputControl", line, 18) == 0 )	
								{
								this->ReadNodeOutputControl();
								}
							else
								{
								if( strncmp("*ElementLibraryControl", line, 22) == 0 )
									{
									this->ReadElementLibraryControl();
									}
								else
									{
									if( strncmp("*SubStep", line, 8) == 0 )
										{
										this->ReadSubStep(mesh);
										}
									else
										{
										if( strncmp("*Initial Time", line, 13) == 0 )
											{
											this->ReadInitialTime();
											}
//											else
//												{
//												if( strncmp("*LogFile", line, 8) == 0 )												
//												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	return 1;
}


//----------------------------------------------------------------------------
//Read BasParam File
int vtkHM3DBasParamReader::ReadBasParamFile(char *dir, vtkHM1DMesh *mesh)
{
	vtkDebugMacro(<< "Reading BasParam file");
	
	char diretorio[256];
	strcpy(diretorio, dir);
	
	//Concat dir with name of file
	this->SetFileName(strcat(diretorio,"Basparam.txt"));
	
	if ( !this->OpenFile())
	{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    	return 0;
	}
	
	// Find and read the tags in file
	if ( !this->FindReadTags(mesh) )
		{
		vtkErrorMacro(<<"Can't read file. " << this->GetFileName());
    	return 0;
		}
			
	this->CloseFile();
	
	return 1;
}

//----------------------------------------------------------------------------
//Read Header
int vtkHM3DBasParamReader::ReadHeader()
{
	vtkDebugMacro(<< "Reading BasParam file header");
	//
	// read header
	//

	int ren;
	if(!this->Read(&ren))
	{
		vtkErrorMacro(<<"Can't read Renumbering!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetRenumbering(ren);

	return 1;
}

//----------------------------------------------------------------------------
//Read Param Write Swicht
int vtkHM3DBasParamReader::ReadParamWriteSwicht()
{
	vtkDebugMacro(<< "Reading BasParam file Param Write Swicht");
	//
	// read Param Write Swicht
	//
		
	int paramWrite;
	if(!this->Read(&paramWrite))
	{
		vtkErrorMacro(<<"Can't read Renumbering!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetParamWriteSwicht(paramWrite);	
	
	return 1;
}


//----------------------------------------------------------------------------
//Read Step Continuation Control
int vtkHM3DBasParamReader::ReadStepContinuationControl(vtkHM1DMesh *mesh)
{
	char line[256];
	
	vtkDebugMacro(<< "Reading StepContinuationControl");

	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read option of more exterm loop!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	if(strcmp("T",line)==0)
		this->BasParam->SetConfigProcessExternal(true);
	else
		this->BasParam->SetConfigProcessExternal(false);

	
	
	int max;
	if(!this->Read(&max))
	{
		vtkErrorMacro(<<"Can't read maximum number of iterations!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetMaxIteration(max);
		
	
	
	double ral;
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read the parameter of ralaxação!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	ral = this->ScientificNotationToDouble(line);	
	this->BasParam->SetRalaxacao(ral);

	
	
	int quan;
	if(!this->Read(&quan))
	{
		vtkErrorMacro(<<"Can't read the quantity of time steps!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetQuantityStep(quan);
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	
	
	double *norma = new double[mesh->GetDegreeOfFreedom()];
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{
		if( !this->ReadString(line) )
		{
			vtkErrorMacro(<<"Can't read the vector Norma!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		norma[i] = this->ScientificNotationToDouble(line);
	}
	this->BasParam->SetNorma(norma);
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	
	
	double *tolerance = new double[mesh->GetDegreeOfFreedom()];
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{
		if( !this->ReadString(line) )
		{
			vtkErrorMacro(<<"Can't read the vector Tolerance!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		tolerance[i]= this->ScientificNotationToDouble(line);
	}
	this->BasParam->SetTolerance(tolerance);
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha

	
	
	string *nameDegreeOfFreedom = new string[mesh->GetDegreeOfFreedom()];
	char str[50];	
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{		
		if(!this->ReadString(str))
		{
			vtkErrorMacro(<<"Can't read the names of the degree of liberty!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		nameDegreeOfFreedom[i] = str;
	}
	this->BasParam->SetNameDegreeOfFreedom(nameDegreeOfFreedom);
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	
	
	int nums[3];
	for ( int i=0; i<3; i++)
	{
		if(!this->Read(&nums[i]))
		{
			vtkErrorMacro(<<"Can't read the vector of nums!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}		
	}
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	return 1;
}

//----------------------------------------------------------------------------
// Convert an char of Scientific Notation to Double value
double vtkHM3DBasParamReader::ScientificNotationToDouble(char *line)
{
	char *pch;
	if( (pch = strchr(line, 'd')) || (pch = strchr(line, 'D')) )
		line[pch-line] = 'e';
		
	return atof(line);	 
}


//----------------------------------------------------------------------------
//Read Time Step
int vtkHM3DBasParamReader::ReadTimeStep()
{
	char line[256];
	
	vtkDebugMacro(<< "Reading TimeStep");

	double delt;
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read time step!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	delt = this->ScientificNotationToDouble(line);
	this->BasParam->SetDelT(delt);

	
	double tini;
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read time of begin of the round!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	tini = this->ScientificNotationToDouble(line);
	this->BasParam->SetTini(tini);
	
	
	double tmax;
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read time of finishing of the round!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	tmax = this->ScientificNotationToDouble(line);
	this->BasParam->SetTmax(tmax);
	
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	return 1;
}

//----------------------------------------------------------------------------
//Read Output Control
int vtkHM3DBasParamReader::ReadOutputControl()
{
	char line[256];
	
	vtkDebugMacro(<< "Reading Output Control");
	
	int timeScreen;
	if(!this->Read(&timeScreen))
	{
		vtkErrorMacro(<<"Can't read steps of time between exits in the screen!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetTimeQuantityScreen(timeScreen);
	
	int timeFile;
	if(!this->Read(&timeFile))
	{
		vtkErrorMacro(<<"Can't read steps of time between exits in the file!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetTimeQuantityFile(timeFile);

	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	return 1;
}

//----------------------------------------------------------------------------
//Read Node Output Control
int vtkHM3DBasParamReader::ReadNodeOutputControl()
{
	char line[256];
	
	vtkDebugMacro(<< "Reading Node Output Control");
	
	int write;
	if(!this->Read(&write))
	{
		vtkErrorMacro(<<"Can't read option for which node goes to be carried through the writing of the results!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetWriterOutput(write);
	
	if(write > 0)
	{
		int *outputList = new int[write];
		for ( int i=0; i<write; i++)
		{
			if(!this->Read(&outputList[i]))
			{
				vtkErrorMacro(<<"Can't read list of the nodes that is asked for in the exit of the program !" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}
			if( i == write-1 )
				this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
		}
		this->BasParam->SetOutputList(outputList);
	}

	return 1;
}

//----------------------------------------------------------------------------
//Read Element Library Control
int vtkHM3DBasParamReader::ReadElementLibraryControl()
{
	char line[256];
	
	vtkDebugMacro(<< "Reading Element Library Control");

	int qtd;
	if(!this->Read(&qtd))
	{
		vtkErrorMacro(<<"Can't read the quantity of elements!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetQuantityOfDifferentElements(qtd);
	
	int max;
	if(!this->Read(&max))
	{
		vtkErrorMacro(<<"Can't read the maximum quantity of parameters of the elements!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetMaximumQuantityOfParameters(max);

	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	return 1;
}

//----------------------------------------------------------------------------
//Read Sub Step
int vtkHM3DBasParamReader::ReadSubStep(vtkHM1DMesh *mesh)
{
	char line[256];
	
	vtkDebugMacro(<< "Reading Sub Step");

	
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read if resultant matrix is symetrical!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	
	if(strcmp("T",line) == 0)
		this->BasParam->SetResultantMatrixIsSymmetrical(true);
	else
		this->BasParam->SetResultantMatrixIsSymmetrical(false);
	
	
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read if calculation step is linear!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	
	if(strcmp("T",line) == 0)
		this->BasParam->SetCalculationStepIsLinear(true);
	else
		this->BasParam->SetCalculationStepIsLinear(false);
	
	int max;
	if(!this->Read(&max))
	{
		vtkErrorMacro(<<"Can't read maximum number of iterations (SubStep)!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetMaxIterationSubStep(max);
	
	double ral;
	if(!this->ReadString(line))
	{
		vtkErrorMacro(<<"Can't read the parameter ralaxacao (SubStep)!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	ral = this->ScientificNotationToDouble(line);
	this->BasParam->SetRalaxacaoSubStep(ral);
	
	int resolve;
	if(!this->Read(&resolve))
	{
		vtkErrorMacro(<<"Can't read the parameter resolver of library!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->BasParam->SetResolveLibrary(resolve);
	
	this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	
	//Case the resolver of the library is different of zero,
	//read some parameters more
	if( (resolve != 0) && (resolve != 200) && (resolve != 20061129))  
	{
		if(!this->Read(&max))
		{
			vtkErrorMacro(<<"Can't read the maximum quantity of iterations!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		this->BasParam->SetMaxIterationOfResolver(max);
		
		double tol;
		if(!this->ReadString(line))
		{
			vtkErrorMacro(<<"Can't read the parameter convergence tolerance !" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		tol = this->ScientificNotationToDouble(line);
		this->BasParam->SetToleranceOfConvergence(tol);
		
		int space;
		if(!this->Read(&space))
		{
			vtkErrorMacro(<<"Can't read the parameter spaces of Krylov!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		this->BasParam->SetSpacesOfKrylov(space);
		
		int num;
		if(!this->Read(&num))
		{
			vtkErrorMacro(<<"can't read number of coefficients of the matrix!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		this->BasParam->SetNumberOfCoefficients(num);
		

		if(!this->ReadString(line))
		{
			vtkErrorMacro(<<"Can't read the parameter to excuse coefficients!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		tol = this->ScientificNotationToDouble(line);
		this->BasParam->SetToleranceOfExcuse(tol);
		
		this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	}
	
	int *elementType = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
	
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
	{
		for (int j=0; j<5; j++)
		{
			if(!this->Read(&elementType[i*5+j]))
			{
				vtkErrorMacro(<<"Can't read the type of element and its parametes!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}

			if(j == 4)
				this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
		}
	}

	this->BasParam->SetElementType(elementType);
	
	double *paramElementType = new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
	{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
		{
			if(!this->ReadString(line))
			{
				vtkErrorMacro(<<"Can't read the parameters for the types of elements!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}
			paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j] = this->ScientificNotationToDouble(line);
			if( j == (this->BasParam->GetMaximumQuantityOfParameters()-1) )
				this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
		}
	}
	this->BasParam->SetParamElementType(paramElementType);
	
	double *normaSubStep = new double[mesh->GetDegreeOfFreedom()];	
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{
		if(!this->ReadString(line))
		{
			vtkErrorMacro(<<"Can't read the vector Norma (SubStep)!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		normaSubStep[i] = this->ScientificNotationToDouble(line);
		if( i == (mesh->GetDegreeOfFreedom()-1) )
			this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	}
	this->BasParam->SetNormaSubStep(normaSubStep);
	
	double *toleranceSubStep = new double[mesh->GetDegreeOfFreedom()];
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{
		if(!this->ReadString(line))
		{
			vtkErrorMacro(<<"Can't read the vector Tolerance (SubStep)!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		toleranceSubStep[i] = this->ScientificNotationToDouble(line);
		if( i == (mesh->GetDegreeOfFreedom()-1) )
			this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	}
	this->BasParam->SetToleranceSubStep(toleranceSubStep);
	
	string *name = new string[mesh->GetDegreeOfFreedom()];
	char str[50];	
	for ( int i=0; i<mesh->GetDegreeOfFreedom(); i++)
	{		
		if(!this->ReadString(str))
		{
			vtkErrorMacro(<<"Can't read the names of the degrees of liberty (SubStep)!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		name[i] = str;
		if( i == (mesh->GetDegreeOfFreedom()-1) )
			this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	}
	this->BasParam->SetNameDegreeOfFreedomSubStep(name);
	
	int nums[3];
	for ( int i=0; i<3; i++)
	{
		if(!this->Read(&nums[i]))
		{
			vtkErrorMacro(<<"Can't read the vector of nums (SubStep)!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		if( i == 2 )
			this->ReadLine(line); //usado para pegar qualquer comentário no final da linha e pular de linha
	}
	this->BasParam->SetNumsSubStep(nums);

	return 1;
}

void vtkHM3DBasParamReader::SetBasParam(vtkHM1DBasParam *BP)
{
	this->BasParam = BP;
}

vtkHM1DBasParam *vtkHM3DBasParamReader::GetBasParam()
{
	return this->BasParam;
}
