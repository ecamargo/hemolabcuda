/*
 * $Id: vtkHM3DBasParamReader.h 405 2006-05-22 18:04:15Z igor $
 */
 
 // .NAME vtkHM3DBasParamReader - read BasParam data for 3D file 
// .SECTION Description
// vtkHM3DBasParamReader is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The superclass of this class, vtkDataReader, provides many methods for
// controlling the reading of the data file, see vtkDataReader for more
// information.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.
// .SECTION See Also
// vtkDataReader vtkHM3DMeshReader
 
#ifndef VTKHM3DBASPARAMREADER_H_
#define VTKHM3DBASPARAMREADER_H_

#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkDataReader.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;

#include <string>
using namespace std;
	
class VTK_EXPORT vtkHM3DBasParamReader : public vtkDataReader
{
public:
	static vtkHM3DBasParamReader *New();
	vtkTypeRevisionMacro(vtkHM3DBasParamReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Find and Read Tags in file.
	int FindReadTags(vtkHM1DMesh *mesh);
	
	// Description:
	// Read Initial Time tag in file.
	int ReadInitialTime();
	
	// Description:
	// Convert an char of Scientific Notation to Double value
	double ScientificNotationToDouble(char*);
		
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	// Read the header of a BasParam data file. Returns 0 if error.	
	int	ReadHeader();
	
	// Description:
	// Read the Param Write Swicht of a BasParam data file. Returns 0 if error.	
	int ReadParamWriteSwicht();
	
	// Description:
	//Read the remaining data of the BasParam file
	int ReadBasParamFile(char *dir, vtkHM1DMesh *mesh);
	
	// Description:
	//Read parameters of configuration of the
	//process of more external calculation
	int ReadStepContinuationControl(vtkHM1DMesh *mesh);
	
	// Description:
	//Read parameters that caracterize the problem transient
	int ReadTimeStep();
	
	// Description:
	//Read the quantity of steps that occur between two outputs
	int ReadOutputControl();
	
	// Description:
	//Read parameters for which nodos goes to be
	//made the writing of the results
	int ReadNodeOutputControl();
	
	// Description:
	//Read general information concerning the
	//elements used in the problem 
	int ReadElementLibraryControl();
	
	// Description:
	//Read general configuration of the
	//step of corresponding calculation
	int ReadSubStep(vtkHM1DMesh *mesh);
	
	// Description
	// SetGet BasParam
	vtkHM1DBasParam *GetBasParam();
	void SetBasParam(vtkHM1DBasParam *BP);
	
protected:
	vtkHM3DBasParamReader();
	~vtkHM3DBasParamReader();
	
	// Description
	// Object BasParam that contain data
	// of the BasParam file
	vtkHM1DBasParam *BasParam;
	
}; //End class

#endif /*VTKHM3DBASPARAMREADER_H_*/
