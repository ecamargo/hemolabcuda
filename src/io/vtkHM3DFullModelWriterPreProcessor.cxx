#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFile.h"
#include "vtkHM3DFullModelWriterPreProcessor.h"
#include "vtkProcessModule.h"
#include "vtkHM1DData.h"
#include "vtkHM1DStraightModelWriter.h"
#include "vtkHM3DFullModel.h"
#include "vtkSurfaceGen.h"
#include "SurfTriang.h"
#include "dyncoo3d.h"
#include "acdp.h"
#include "acdptype.h"
#include "vtkIdList.h"
#include "vtkCell.h"
#include <time.h>
#include <math.h>
#include <vtkHMParallelSolverScriptWriter.h>
//#include <omp.h>
#include <vtkTimerLog.h>

vtkCxxRevisionMacro(vtkHM3DFullModelWriterPreProcessor, "$Revision: 444 $");
vtkStandardNewMacro(vtkHM3DFullModelWriterPreProcessor);

//----------------------------------------------------------------------------
vtkHM3DFullModelWriterPreProcessor::vtkHM3DFullModelWriterPreProcessor()
{
	//this->DebugOn();
	this->points = vtkPoints::New();
	this->ElementsIncidence.clear(); 
	this->ElementTypeArray = vtkIntArray::New();
	this->IniFileArray = vtkDoubleArray::New();
	this->DirichletsTag = NULL;
	this->Dirichlets = NULL;
	this->IniFileDoubleArray = NULL;

	this->IntParamArray = vtkIntArray::New();
	this->TimeParamArray = vtkDoubleArray::New();
	this->ConvParamArray = vtkDoubleArray::New();
	this->ModelConfigParamArray = vtkDoubleArray::New();
	
	this->SolverConfigParamArray= vtkDoubleArray::New();

	this->IntParamArray->SetNumberOfValues(18);
	
	this->TimeParamArray->SetNumberOfValues(3);
	this->ConvParamArray->SetNumberOfValues(15);
	this->ModelConfigParamArray->SetNumberOfValues(5);
	
	this->SolverConfigParamArray->SetNumberOfValues(6);

	this->FullModel = NULL;
	
	this->SurfaceElementType = 57;
	this->CoverElementType   = 540;
	this->VolumeElementType  = 524;
	
	// 3D sempre 7 graus de liberdade
	this->BasParamDOFNumber = 7;
	
	this->Timer = vtkTimerLog::New();
	
}
//----------------------------------------------------------------------------

vtkHM3DFullModelWriterPreProcessor::~vtkHM3DFullModelWriterPreProcessor()
{
	this->points->Delete();
	this->ElementTypeArray->Delete();
	this->IniFileArray->Delete();
	
	if (this->DirichletsTag)
	  this->DirichletsTag->Delete();
	
	if (this->Dirichlets) 
	  this->Dirichlets->Delete();
	
	if (this->IniFileDoubleArray) 
	  this->IniFileDoubleArray->Delete(); 
	
	this->IntParamArray->Delete();
	this->TimeParamArray->Delete();
	this->ConvParamArray->Delete();
	this->ModelConfigParamArray->Delete();
	this->SolverConfigParamArray->Delete();
	
	this->Timer->Delete();
	
}
//----------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
//----------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::GenerateMeshLists()
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::GenerateMeshLists()");
	
	vtkSurfaceGen* surfGen = this->FullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
//  acDynCoord3D* Coord = surfGen->GetCoords(); //Obtendo as coordenadas dos pontos que compoem a malha.
//  acPoint3*  vc = Coord->GetVc();
//  this->NumGroups = surfGen->GetNumGroups(); //Obtendo o numero de grupos de vtkSurfaceGen.
//  int NumNodesSurface = Coord->GetNumNodes(); //Obtendo o numero de coordenadas de vtkSurfaceGen.
//  int NumNodesVolume = (int) this->FullModel->GetVolume()->GetNumberOfPoints(); //Obtendo o numero de coordenadas do volume.
//  this->nel = this->FullModel->GetVolume()->GetNumberOfCells(); //Obtendo o numero de elementos do volume.
//   
    

 	//***********************************************************
 	// cria lista de coordenadas do VOLUME
 	double point[3];
	
	// gera a lista de coordenadas dos pontos do volume
	
	for (long i = 0; i<this->NumNodesVolume; i++)
  	{	
  	this->FullModel->GetVolume()->GetPoint(i,point);
   	//cout << "Coordenadas do Volume " << point[0] << point[1] << point[2] << endl;
    
    
    
    // SCALEFACTOR3D -- usado como fator de scala para aumentar ou diminuir estrutura 3D
    if (this->ScaleFactor3D == 1.0)
    	this->points->InsertNextPoint(point[0], point[1], point[2]);
    else
    	this->points->InsertNextPoint(point[0]*this->ScaleFactor3D, point[1]*this->ScaleFactor3D, point[2]*this->ScaleFactor3D);
    
    
    //this->points->InsertNextPoint(point[0], point[1], point[2]);
  	}

	
	
	// inserindo os pontos que correspondem aos nos "ficticios" se presssao estiver habilitado nas tampas
  
  int CoverWithPression=0;
  for (int i=0; i < this->NumGroups-1; i++)
 		{
		// identifica se existe pressao constante definida nas tampas
		//if (this->FullModel->CoverConstantPressureIsSet(i))
		if (this->FullModel->GetCoverElementsBCType()->GetValue(i)!= 3) // velocidade 
		
		//if (this->FullModel->GetCoverPressure(i))
			{
			this->points->InsertNextPoint(0.0, 0.0 ,0.0);
			CoverWithPression++;
			}
    }

 	//***********************************************************
 	
	// vetor temporário que será usado para guardar os valores de *ELEMENT TYPE 
	vtkIntArray *ElementTypeIntArray = vtkIntArray::New();

	// vetor temporário que será usado para guardar os valores de *ELEMENT MAT 
	vtkIntArray *ElementMatIntArray = vtkIntArray::New();

 	
	//
	//
  //***********************************************************************
  // INCIDENCIA DOS TETRAEDROS  *************************
  //***********************************************************************
  //
 	vtkIdList *ids;
	vtkHM1DMesh::ListOfInt lst;
  lst.clear();
 	
  for (int i = 0; i< this->nel; i++)
		{
		ids = this->FullModel->GetVolume()->GetCell(i)->GetPointIds();
		lst.push_back(ids->GetId(0)+1);
		lst.push_back(ids->GetId(1)+1);
		lst.push_back(ids->GetId(2)+1);
		lst.push_back(ids->GetId(3)+1);
		this->ElementsIncidence.push_back(lst);
		lst.clear();
		//*Element Type  for volume elements
		ElementTypeIntArray->InsertNextValue(1); // supondo que elemento de volume é o primeiro na lista de elementos do BasParam
		
		//*Element Mat for volume elements
		ElementMatIntArray->InsertNextValue(i+1);
		}
		
		
		//
		//
    //***********************************************************************
    // INCIDENCIA DOS TRIANGULOS DA PAREDE E TAMPAS *************************
    //***********************************************************************
    //
    // 
    // percorre todos triangulos da surface incluindos os das tampas para criar a lista de incidencia 
    // os triangulos sao percorridos em ordem do grupo 0 ate o grupo n
    curr = first;
    while (curr)
    	{
      // para os elementos de tampa e que tenham pressao inserir o quarto nó 
      // se pressao constante ou pressao variante estiverem definidas
      if (curr->gr && (this->FullModel->GetCoverElementsBCType()->GetValue(curr->gr-1) != 3) )  
        lst.push_back(this->GetNumberOfPoints()-(CoverWithPression)+ curr->gr);
          
			lst.push_back(curr->n1+1);
			lst.push_back(curr->n2+1);
			lst.push_back(curr->n3+1);

			//*Element Type  for surface triangles
			ElementTypeIntArray->InsertNextValue(curr->gr+2); // element type relaciona o elemento atual com a identificacao dos elementos no Basparam
		  
		  //Element Mat for surface triangles
		  ElementMatIntArray->InsertNextValue(this->nel+curr->gr+1); // relaciona com o material do elemento descrito no arquivo Param.txt
      
      this->ElementsIncidence.push_back(lst); // se a tampa tiver como BC algum tipo de pressao esta lista terá 4 elementos 
      curr = curr->next;
      lst.clear();
    	}
 	
 	 // criacao do vetor que indica os parametros de cada elemento no arquivo Param.txt
	 // *ELEMENT MAT
	 int *ElementMat = new int[this->ElementsIncidence.size()];
	 
	 	for (int i=0; i<this->ElementsIncidence.size(); i++ )
			ElementMat[i] = ElementMatIntArray->GetValue(i);
	 
	 ElementMatIntArray->Delete();
	 
	 this->SetElementMat(ElementMat);

  //***********************************************************
  //***********************************************************
  // Criacao de *ELEMENT GROUPS  
  int *elementForGroup = new int[this->ElementsIncidence.size()];
  
	vtkHM1DMesh::VectorOfIntList::iterator it_vector;
  
  it_vector = this->ElementsIncidence.begin();
  int i=0; 
  while (it_vector != this->ElementsIncidence.end() )
  	{
  	lst=*it_vector;
		elementForGroup[i]=lst.size();
		i++;
  	it_vector++;
  	}  
  this->SetElementForGroup(elementForGroup);
  
  
  //***********************************************************
  //***********************************************************
  // Criacao de *ELEMENT TYPE 
  
  int *ElementType = new int[this->ElementsIncidence.size()];
	
	for (int i=0; i<this->ElementsIncidence.size(); i++ )
		ElementType[i] = ElementTypeIntArray->GetValue(i);
  
  ElementTypeIntArray->Delete();
  this->SetElementType(ElementType);

	 //***********************************************************

  //this->Timer->StartTimer();
  
  // Cria matriz de conectividade Transposta que é utilizada na geracao das condicoes de Dirichlets
  this->BuildTransposeConnectivity();
  
//  this->Timer->StopTimer();
//  cout << "GetElapsedTime from BuildTransposeConnectivity  " << this->Timer->GetElapsedTime()    << endl;
  
  
}

//----------------------------------------------------------------------------

vtkPoints *vtkHM3DFullModelWriterPreProcessor::GetPoints()
{
	return this->points;
}

//----------------------------------------------------------------------------

int vtkHM3DFullModelWriterPreProcessor::GetNumberOfPoints()
{
	return this->points->GetNumberOfPoints();
}



//----------------------------------------------------------------------------
vtkHM1DMesh::VectorOfIntList *vtkHM3DFullModelWriterPreProcessor::GetElementsIncidence()
{
	return &ElementsIncidence;
  
}


//-------------------------------------------------------------------------------------
void vtkHM3DFullModelWriterPreProcessor::CreateParam()
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::CreateParam()");
	
	int n = this->nel+this->NumGroups;
	
	//Vetor para armazenar a quantidade parametros que cada grupo possui

	
	this->QuantityOfRealParameters = new int[n];
	
	// parametros dos elementos de volume
	for (int i=0; i < this->nel; i++)
			this->QuantityOfRealParameters[i]=3;
	
	
	// inserindo o numero de parametros de parade (4) dado o numero de grupos de parede 
	for (int i=this->nel; i < this->nel + this->FullModel->GetNumberOfShells(); i++)
		{
		this->QuantityOfRealParameters[i]=4;
		}

	int Cover=0;
	for (int i= this->nel + this->FullModel->GetNumberOfShells(); i < n; i++)
		{
		// se tem curva de pressao para a tampa
		if (this->FullModel->GetCoverElementsBCType()->GetValue(Cover) == 2)
			{
			// se existe curva, pega o numero de elementos da curva do vetor
			this->QuantityOfRealParameters[i]= (this->FullModel->GetPressureCurve(Cover)->GetNumberOfTuples()*2);
			//Cover++;
			}
		else // se tem valor fixo de pressao ou velocidade
			this->QuantityOfRealParameters[i]=0;
		
		Cover++;
		}
		
	this->QuantityOfIntegerParameters = new int[n];

	this->RealParameters.clear();
	vtkHM1DParam::ListOfDouble lst;
//	vtkHM1DParam::ListOfDouble::iterator it;
//	lst.push_back(0.0);
//	lst.push_back(0.0);
//	lst.push_back(0.0);
	// insere lista vazia para indicar que nao existe curva
	//cout << "Size do Vetor de Parametros reais " << this->RealParameters.size() << endl;
	//cout << "tamanho da lista que esta sendo inserida " << lst.size() << endl;
	
	this->RealParameters.push_back(lst);
	//cout << "Size do Vetor de Parametros reais " << this->RealParameters.size() << endl;
	
	for (int i=0; i < this->nel; i++ )
		{
		lst.clear();	
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		this->RealParameters.push_back(lst);
		}
		//cout << "Size do Vetor de Parametros reais " << this->RealParameters.size() << endl;

	double Elastin,CurveRadius,Wallthickness, RefPressure, ViscoElasticity;
	
	// inserindo valor real dos parametros de Parede
	for (int i=0; i < this->FullModel->GetNumberOfShells(); i++ )
		{
		// se o modelo é rigid
		if (!this->IntParamArray->GetValue(0))
			{
			Elastin = 1.0;	
			CurveRadius = 1.0;	
			Wallthickness = 1.0;	
			RefPressure = 1.0;
			ViscoElasticity = 1.0;	
			}
		else
			{
			Elastin = this->FullModel->GetWallParameters(i)->GetValue(0);	
			CurveRadius = this->FullModel->GetWallParameters(i)->GetValue(1);	
			Wallthickness =this->FullModel->GetWallParameters(i)->GetValue(2);	
			RefPressure = this->FullModel->GetWallParameters(i)->GetValue(3);
			ViscoElasticity =this->FullModel->GetWallParameters(i)->GetValue(4);		
			}	
		
		lst.clear();			
		// elastina* (h/R)
		lst.push_back(Elastin*(Wallthickness/CurveRadius));		
				
		// raio de curvatura
		lst.push_back(CurveRadius);
				
		//pressão de referência do elemento
		lst.push_back(RefPressure);		

		//viscoelasticidade*(h/R)
		lst.push_back(ViscoElasticity*(Wallthickness/CurveRadius));				
			
		this->RealParameters.push_back(lst);	
		}	
	
	Cover=0;
	
	

	lst.clear();
	for (int i=0;  i< this->FullModel->GetNumberOfCoverGroups();i++ )// de 0 ate o numero de grupos de tampas
		{
		// se pressao constante ou velocidade definida para a tampa, insere lista vazia
		if (this->FullModel->GetCoverElementsBCType()->GetValue(i) != 2)
			{
			this->RealParameters.push_back(lst);
			lst.clear();	
			}	
		else // se lista com valores de p=f(t)  
			{
			for (int z=0; z< this->FullModel->GetPressureCurve(i)->GetNumberOfTuples(); z++)
					{
					lst.push_back(this->FullModel->GetPressureCurve(i)->GetTuple2(z)[0]);//tempo  							
					lst.push_back(this->FullModel->GetPressureCurve(i)->GetTuple2(z)[1]);// valor pressao  							
					}	
			this->RealParameters.push_back(lst);
			lst.clear();	
			}
		}	

	this->IntegerParameters.clear();
	vtkHM1DParam::ListOfInt listInt;
	
	
	//insere zeros para elementos volume e elementos de surface 
	for (int i=0; i< this->nel + this->FullModel->GetNumberOfShells(); i++ )
		this->QuantityOfIntegerParameters[i] = 0;
	
	for (int i = this->nel + this->FullModel->GetNumberOfShells(); i< n; i++)
		{
		if (this->QuantityOfRealParameters[i])		
			this->QuantityOfIntegerParameters[i] = 1;
		else
			this->QuantityOfIntegerParameters[i] = 0;	
		}	
	
		
	for (int i=0; i< (this->NumGroups-1); i++ )
		{
		
		if (this->FullModel->GetCoverElementsBCType()->GetValue(i)==2)
			{
			listInt.clear();
			listInt.push_back(this->FullModel->GetPressureCurve(i)->GetNumberOfTuples());
			this->IntegerParameters.push_back(listInt);
			}
		
		}
	

}

//-------------------------------------------------------------------------------------

int *vtkHM3DFullModelWriterPreProcessor::GetElementForGroup()
{
	return this->nElementForGroup;
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetElementForGroup(int *elemForGroup)
{
	this->nElementForGroup = elemForGroup;
}
//-------------------------------------------------------------------------------------

int *vtkHM3DFullModelWriterPreProcessor::GetElementType()
{
	return this->ElementType;
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetElementType(int *elemType)
{
	this->ElementType = elemType;
}
//-------------------------------------------------------------------------------------

int *vtkHM3DFullModelWriterPreProcessor::GetElementMat()
{
		return this->ElementMat;
}	
	
//-------------------------------------------------------------------------------------
	
void vtkHM3DFullModelWriterPreProcessor::SetElementMat(int *elemMat)
{
		this->ElementMat = elemMat;
}

//-------------------------------------------------------------------------------------

int vtkHM3DFullModelWriterPreProcessor::GetNumberOfElementGroups()
{
   return this->ElementsIncidence.size();
}

//-------------------------------------------------------------------------------------
void vtkHM3DFullModelWriterPreProcessor::GenerateDirichletsValues()
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::GenerateDirichletsValues()");
	
	
	/* O tamanho da lista A e B é o mesmo que o numero de pontos da lista de coordenadas
	
	
	 ListaA -- Integer Tags (0 or -1)
	 ListaB -- Real values
	
	 No 3D, sempre 2 Substeps	

	 ListaA
	 ListaA
	 ListaB
	 ListaB
	
	*/
	
	int NumberOfSubsteps = 2; // 3D sempre 2 substeps
	
  //int NumNodesSurface = this->FullModel->GetSurface()->GetCoords()->GetNumNodes(); //Obtendo o numero de coordenadas de vtkSurfaceGen.

	vtkIntArray *DirichletArray= vtkIntArray::New();
	vtkDoubleArray *DirichletDoubleArray= vtkDoubleArray::New();
	
	DirichletArray->SetNumberOfComponents(7);
	DirichletArray->SetNumberOfTuples(this->GetNumberOfPoints()*NumberOfSubsteps); // numero de pontos da estrutura 3D X numero de substeps

	DirichletDoubleArray->SetNumberOfComponents(7);
	DirichletDoubleArray->SetNumberOfTuples(this->GetNumberOfPoints()*NumberOfSubsteps);

	for (int i=0; i < this->GetNumberOfPoints()*NumberOfSubsteps; i++ )
		{
		DirichletArray->SetValue(i,0);
		DirichletDoubleArray->SetValue(i,0);
		}

	int i,j;
	int n[7] = {0};
	double n2[7] =  {0.0}; 

	double *velocity = new double[3];
	
	
	for (int i=this->NumNodesSurface; i < this->GetNumberOfPoints()*NumberOfSubsteps; i++ )
		{
		DirichletArray->SetTupleValue(i, n);
		DirichletDoubleArray->SetTupleValue(i, n2);
		}

	for (i=0; i < this->NumNodesSurface; i++ )
		{
  	// se o ponto analisado faz parte de algum grupo de tampa *
  	// * lembrando que o ponto com valor 0 é do volume ou parede
		int GroupPoint = this->GetGroupFromPoint(i);
		
  	if (GroupPoint)
  		{
      if (GroupPoint == -1)
        {
        vtkErrorMacro("Erro - could not determine the group of a given point");
        return;
        }
  		
  		cout << "this->GetGroupFromPoint(i) "<< GroupPoint << endl;

  		// se velocidade foi definida para a tampa a qual o ponto faz parte
  		if (this->FullModel->GetCoverElementsBCType()->GetValue(GroupPoint-1)== 3) // velocidade 
  			{
				n[0]=-1;
				n[1]=-1;
				n[2]=-1;
				n[3]= 0;
				n[4]= 0;
				n[5]= 0;
				n[6]= 0;

				velocity =  this->FullModel->GetCoverElementVelocity(GroupPoint-1);		
				
				n2[0]=velocity[0]; 	
				n2[1]=velocity[1]; 	
				n2[2]=velocity[2]; 	
				}
			else // pontos de tampa que tem pressao definida
				{
				for (int z=0; z < 7 ; z++)
					{
					n[z] = 0;
					n2[z] = 0.0;					
					}
				}
			}
		else
			{ // pontos da parede !
			  // se o ponto analisado é do grupo da parede e o modelo é rigido
			if (!this->IntParamArray->GetValue(0))
				{
				//cout<< "Parede rigida ponto "<< i << endl;
				n[0]=-1;
				n[1]=-1;
				n[2]=-1;
				n[3]= 0;
				n[4]=-1;
				n[5]=-1;
				n[6]=-1;
				}
			else // se o ponto é de parede e parede deformavel
				{
				for (int z=0; z < 7 ; z++)
					n[z] = 0;
				}
			
			for (int z=0; z < 7 ; z++)
				n2[z] = 0.0;
			}
	
		DirichletArray->SetTupleValue(i, n);
		DirichletArray->SetTupleValue(i+this->GetNumberOfPoints(), n);
		
		DirichletDoubleArray->SetTupleValue(i, n2);
		DirichletDoubleArray->SetTupleValue(i+this->GetNumberOfPoints(), n2);

		}

	this->SetDirichletsTag(DirichletArray);
	this->SetDirichlets(DirichletDoubleArray);
}


//----------------------------------------------------------------------------
vtkIntArray *vtkHM3DFullModelWriterPreProcessor::GetDirichletsTag()
{
	return this->DirichletsTag;
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetDirichletsTag(vtkIntArray *d)
{
	this->DirichletsTag = d;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHM3DFullModelWriterPreProcessor::GetDirichlets()
{
	return this->Dirichlets;
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetDirichlets(vtkDoubleArray *d)
{
	this->Dirichlets = d;
}
//-------------------------------------------------------------------------------------


vtkDoubleArray *vtkHM3DFullModelWriterPreProcessor::GetIniFileArray()
{
	return this->IniFileDoubleArray;
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetIniFileArray(vtkDoubleArray *d)
{
	this->IniFileDoubleArray = d;
}
//-------------------------------------------------------------------------------------


void vtkHM3DFullModelWriterPreProcessor::GenerateIniFile()
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::GenerateIniFile()");
	
	// aqui se tem 7 DOF
	// 0  1  2  3  4  5  6
	// Vx Vy Vz Pr Dx Dy Dz
	
	vtkDoubleArray *IniFileDoubleArray2 = vtkDoubleArray::New();
	IniFileDoubleArray2->SetNumberOfComponents(this->BasParamDOFNumber);
	
	
	int NumberOfMeshPoints = this->NumNodesVolume;
	
  for (int i=0; i < this->NumGroups-1; i++)
 		{
		if (this->FullModel->GetCoverElementsBCType()->GetValue(i)!= 3) // velocidade 
			{
			NumberOfMeshPoints++;
			}
 		}
	
	IniFileDoubleArray2->SetNumberOfTuples(NumberOfMeshPoints);
	
	
	
	
	//IniFileDoubleArray2->SetNumberOfTuples(this->GetNumberOfPoints());

	double IniArray[7] =  {0.0}; 

 	//int NumNodesSurface = this->FullModel->GetSurface()->GetCoords()->GetNumNodes(); //Obtendo o numero de coordenadas de vtkSurfaceGen.

	// pontos surface
  for(int i=0; i< this->NumNodesSurface ; i++)
  	{
  	//double *tupla = IniFileDoubleArray2->GetTuple(i);
  		
  	IniArray[0]=0.0;
  	IniArray[1]=0.0;	
  	IniArray[2]=0.0;
  	
//  	// se ponto pertence é da parede
//  	if (!this->GetGroupFromPoint(i))
//  		{
//	  	// se parede rigida P=0
//	  	if (!this->IntParamArray->GetValue(0))
//	  		IniArray[3]=0.0;
//	 	 	else // se parede é deformavel
//	 	 		// insere o valor da pressao de referencia da parede
//	 	 		IniArray[3]=this->FullModel->GetSurfaceElementParam()->GetValue(3);
//  		}
//  	else			
// 	 		IniArray[3]=0.0;
 	 		
 	 	
 	 	if (this->IntParamArray->GetValue(0)) // se parede é deformavel
	  	{
	  	if (this->FullModel->GetWallParameters(0)->GetValue(3)) // se tem valor definido para pressao de referencia
	  		IniArray[3]=this->FullModel->GetWallParameters(0)->GetValue(3);
	  	else // se PR nao definida usar valor default
	  		IniArray[3]=100000;
	  	}
	 	else // se parede é rigida
 	 		IniArray[3]=0.0;
 	 	
 	 	IniArray[4]=0.0;
  	IniArray[5]=0.0;
  	IniArray[6]=0.0;
  	
  	IniFileDoubleArray2->SetTupleValue(i,IniArray);
  	}
//  	this->SetIniFileArray(IniFileDoubleArray2);


	// resetando vetor auxiliar
  for(int i=0; i< 7 ; i++)
  	IniArray[i]=0.0;

  if (this->IntParamArray->GetValue(0)) // se parede é deformavel
		{
	  if (this->FullModel->GetWallParameters(0)->GetValue(3)) // se tem valor definido para pressao de referencia
	  	IniArray[3]=this->FullModel->GetWallParameters(0)->GetValue(3);
	  else // se PR nao definida usar valor default
	  	IniArray[3]=100000;
		}
	else // se parede é rigida
 		IniArray[3]=0.0;

  // demais pontos - volume
  for(int i=this->NumNodesSurface; i< this->GetNumberOfPoints() ; i++)
		IniFileDoubleArray2->SetTupleValue(i,IniArray);
	
	this->SetIniFileArray(IniFileDoubleArray2);

}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::ConfigureBasparam()
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::CreateNewBasParam()");
	// definicoes retiradas de http://hemo01/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Basparam.txt
	// e http://hemo01/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Biblioteca_de_Elementos

	/*******************************************************************
	/*******************************************************************
	 * Configurando os parametros de tempo do Basparam  
	/*******************************************************************
	/*******************************************************************
	*/
	this->BasParam->SetDelT(this->TimeParamArray->GetValue(0));
	this->BasParam->SetTini(this->TimeParamArray->GetValue(1));
	this->BasParam->SetTmax(this->TimeParamArray->GetValue(2));
	
	
	/*
	/*******************************************************************
	/*******************************************************************
	 * Configurando os parametros de tipo inteiros do Basparam  
	/*******************************************************************
	/*******************************************************************
	*/
	
	this->BasParam->SetInitialTime(0);
	this->BasParam->SetRenumbering(this->IntParamArray->GetValue(3));
	this->BasParam->SetTimeQuantityScreen(this->IntParamArray->GetValue(5));
	this->BasParam->SetTimeQuantityFile(this->IntParamArray->GetValue(4));
	string *name;
	bool CompliantModel = false;
	
	// se o modelo é Compliant
	if (this->IntParamArray->GetValue(0))
		CompliantModel = true;
	
	name = new string[this->BasParamDOFNumber];
	name[0] = "VX	";
	name[1] = "VY	";
	name[2] = "VZ	";
	name[3] = "P	";
	name[4] = "UX	";
	name[5] = "UY	";
	name[6] = "UZ	";

	this->BasParam->SetNameDegreeOfFreedom(name);
	
  //**************************************************************************************
	// definicao do *StepContinuationControl
  //**************************************************************************************
	// OpA 
	this->BasParam->SetConfigProcessExternal(0);
	// num1
	this->BasParam->SetMaxIteration(this->IntParamArray->GetValue(8));
	
	// num2	
	this->BasParam->SetRalaxacao(this->ConvParamArray->GetValue(14));

	//num3
	this->BasParam->SetQuantityStep(1);
	
	// norma
	double *norma = new double[this->BasParamDOFNumber];
	if (CompliantModel)
		{
		norma[0]= this->ConvParamArray->GetValue(0);
	  norma[1]= this->ConvParamArray->GetValue(1);
 	  norma[2]=	this->ConvParamArray->GetValue(2);
	  norma[3]=	this->ConvParamArray->GetValue(3);
	  norma[4]= this->ConvParamArray->GetValue(4);
 	  norma[5]=	this->ConvParamArray->GetValue(5);
	  norma[6]=	this->ConvParamArray->GetValue(6);
		}
	else  // se rigid entao os valores de referencia e tolerancia para dofs nao utilizados sao de 0,1 e 0,001 respectivamente
		{
		norma[0]= this->ConvParamArray->GetValue(0);
	  norma[1]= this->ConvParamArray->GetValue(1);
 	  norma[2]=	this->ConvParamArray->GetValue(2);
	  norma[3]=	this->ConvParamArray->GetValue(3);
		norma[4]= 0.1;
 	  norma[5]=	0.1;
	  norma[6]=	0.1;
		}
		
	this->BasParam->SetNorma(norma);
	
	// tolerancia
	double *Tolerance = new double[this->BasParamDOFNumber];
	if (CompliantModel)
		{
		Tolerance[0]= this->ConvParamArray->GetValue(7); 
	  Tolerance[1]= this->ConvParamArray->GetValue(8); 
 	  Tolerance[2]=this->ConvParamArray->GetValue(9); 
	  Tolerance[3]=this->ConvParamArray->GetValue(10); 
 	  Tolerance[4]= this->ConvParamArray->GetValue(11); 
 	  Tolerance[5]=this->ConvParamArray->GetValue(12); 
	  Tolerance[6]=this->ConvParamArray->GetValue(13);
		}
	else // se rigid entao os valores de referencia e tolerancia para dofs nao utilizados sao de 0,1 e 0,001 respectivamente
		{
		Tolerance[0]= this->ConvParamArray->GetValue(7); 
	  Tolerance[1]= this->ConvParamArray->GetValue(8); 
 	  Tolerance[2]=this->ConvParamArray->GetValue(9); 
	  Tolerance[3]=this->ConvParamArray->GetValue(10); 
 	  Tolerance[4]= 0.01; 
 	  Tolerance[5]= 0.01; 
	  Tolerance[6]= 0.01;
		}	 
	
	this->BasParam->SetTolerance(Tolerance);
	
	// nums
	int Nums[3];
	// se modelo compliant e Incremental version = yes 
	if (this->IntParamArray->GetValue(0) && this->IntParamArray->GetValue(7) )
		{
		Nums[0]=5;
		Nums[1]=6;
		Nums[2]=7;			
		}
	else
		{
		Nums[0]=0;
		Nums[1]=0;
		Nums[2]=0;
		}
	this->BasParam->SetNums(Nums);
	
		
	// definicao de *ElementLibraryControl
	this->BasParam->SetMaximumQuantityOfParameters(13);
  
  //elementos de tampa, surface e 1 correspondente aos elementos de volume
  this->BasParam->SetQuantityOfDifferentElements(this->FullModel->GetNumberOfCoverGroups() + this->FullModel->GetNumberOfShells() + 1);
	  
	  
	//*************************************************
	//*************************************************
	//*************************************************
	// 						*S  U  B  S  T  E  P 1
	// ************************************************
	//*************************************************
	//*************************************************
	
	//    * SubStep 
	//OpA OpB Num1 Num2 Num3
	//NumA NumB NumC NumD NumE (dependendo de Num3) 
	
	
	//opA
	this->BasParam->SetResultantMatrixIsSymmetrical(false);
	
	//opB
	this->BasParam->SetCalculationStepIsLinear(true);
	
	// Num1 se CalculationStepIsLinear é False este pode ser Dummy
	this->BasParam->SetMaxIterationSubStep(100);
	
	// Num2 se CalculationStepIsLinear é False este pode ser Dummy
	this->BasParam->SetRalaxacaoSubStep(1.000);
	
	// se solver é serial
	if (!this->IntParamArray->GetValue(13))
		this->BasParam->SetResolveLibrary(this->IntParamArray->GetValue(9)); // codigo do resolvedor
	else // se solver é paralelo
		this->BasParam->SetResolveLibrary(200);
	
	// esta variavel é usada para determinal se deve escrver os parametros do solver quando o mesmo é serial
	// *SubStep
  // F T 100 1 110 
  // 100 1e-10 60 20 1e-10 --> se é paralelo esta linha é removida
	this->BasParam->SetSolverType(this->IntParamArray->GetValue(13)); //13 - Solver type (0: Sequential 1: Parallel));
	
	// como OpB=T definir os NumA NumB NumC NumD NumE
	// numA
	this->BasParam->SetMaxIterationOfResolver(this->IntParamArray->GetValue(8));
	
	// numB
	this->BasParam->SetToleranceOfConvergence(this->SolverConfigParamArray->GetValue(2));
	
	//numC
	this->BasParam->SetSpacesOfKrylov(this->IntParamArray->GetValue(12));
	
	//numD - fill param
	this->BasParam->SetNumberOfCoefficients(this->IntParamArray->GetValue(10));
	
	//numE - drop tolerance
	this->BasParam->SetToleranceOfExcuse(this->SolverConfigParamArray->GetValue(3));
	
	// usado na identificacao de matriz de acoplamento simbólica 
	// de cada elemento
	// guarda o valor de elementos ativos do primeiro substep
	int LastCalculatedElement= -1; 

	int *elementType = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
	
	// Configurando para o Substep 1
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			// se Blood constutive law = Casson entao elemento de volume sera 5245
			// e elemento de parede sera 5707
			if (!this->IntParamArray->GetValue(1))
				{
				// somente no substep 1
				this->VolumeElementType = 5245;
				this->SurfaceElementType= 5705;
				}
			if ((!j) && (!i)) // elemento de volume somenete o primeiro
				{
				elementType[i*5+j]    = this->VolumeElementType;
				}
			else
				{
				if ((!j) && (i <= this->FullModel->GetNumberOfShells()))
					elementType[i*5+j]  = this->SurfaceElementType; // surface element
				
				if ((!j) && (i > this->FullModel->GetNumberOfShells()))
					elementType[i*5+j] = this->CoverElementType; // cover element
				}
			
			
			if (j==3) // coluna de habilitacao de elementos
				{
			 	// se esta é a linha do elemento de parede e se parede é rigida e elemento nao é de volume
			 	if (i <= this->FullModel->GetNumberOfShells() && !this->IntParamArray->GetValue(0) && i) 
					{
					elementType[i*5+j] = 0;  // desabilita elemento de parede
					}
				else
					{
					if (i > this->FullModel->GetNumberOfShells()) // seleciona elemento das tampas
						{
						if (this->FullModel->GetCoverElementsBCType()->GetValue(i-2)==3) // velocidade 
							{
							elementType[i*5+j]=0;  // desabilita elemento da tampa	
							}
						else
							{
							elementType[i*5+j]=LastCalculatedElement; // elemento habilitado no substep
			 				LastCalculatedElement--; 
							}
						}
					else
						{	
						elementType[i*5+j]=LastCalculatedElement; // elemento habilitado no substep
			 			LastCalculatedElement--;
						}
					}
				}
			
			// terceira (j=2) coluna usualmente zero
			if (j==2) elementType[i*5+j]=0;
			
			// quinta coluna indica numero de pontos do elemento
			// Usualmente nulo. As vezes é possível encontrar aqui a quantidade de nós
			// que o elemento possui, porém somente serve como ajuda, e não é 
			// utilizado no cálculo.
			if (j==4)
				elementType[i*5+j]=0;
//			 	if (!i)	 // primeira coluna
//			 		elementType[i*5+j]=5;
//			 	if (i==1)	 // segunda coluna
//			 		elementType[i*5+j]=3;
//			 	if (i > 1)	// demais colunas 
//			 		elementType[i*5+j]=4;
//				}
			
			//*************************************************
			// aditividades dos elementos
			
			// setando coluna 2 com o valor 1
			if (j==1)  elementType[i*5+j]=1;
			// segunda coluna do elemento 1 -- sempre será zero
			if ((j==1) && (!i))  elementType[i*5+j]=0;
			//*************************************************
			}
		}
	this->BasParam->SetElementType(elementType);
	
	double *paramElementType = new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	
	// seta todos elementos do vetor com valor nulo
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=0.0;
		}
	
	int cover = 0 ;
	int cover2 = 0;
	
	
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			{
			if (!i && !j)
				{
				//***************************
				// Configuracao do elementos volume 524 ou 5245
				//**************************				
				// coluna zero densidade
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->ModelConfigParamArray->GetValue(0);	
				
				// viscosidade
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1]= this->ModelConfigParamArray->GetValue(1);	
				
				// compressibilidade
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2]= this->SolverConfigParamArray->GetValue(1);	
				// coluna 4  theta
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3]= this->SolverConfigParamArray->GetValue(0);
				// coluna 6
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+5]= 1.0e0;
				
				// coluna 7 indicador de esquema incremental
					// se o modelo é rigido incremental version é nula
				if (!this->IntParamArray->GetValue(0))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+6]= 0;
				else // se modelo é deformavel
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+6]= this->IntParamArray->GetValue(7);
				
				// coluna 8
				if (!this->IntParamArray->GetValue(1))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+7]= 1.0e0;
				
				// coluna 9 limit Stress
				if (!this->IntParamArray->GetValue(1))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+8]= this->ModelConfigParamArray->GetValue(3);
				
				// coluna 10 Regulation Param
				if (!this->IntParamArray->GetValue(1))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+9]= this->ModelConfigParamArray->GetValue(4);
				
				
				}
				
		  //***************************
			// Configuracao do elementos parede 57, 5705
			//**************************
			
			//57: CommonPar -> 12, Param -> 4, JParam -> 0
			//CommonPar(1): Parâmetro de penalização. Só se o elemento for aditivo. Vale 0 se é não aditivo e -1 se é aditivo.
			//CommonPar(2): Coeficiente da normal (1-se as normais são externas, -1-se as normais são internas).
			//CommonPar(3): Parâmetro não usado.
			//CommonPar(4): Coeficiente para enrigecer a parede arterial (este multiplica a elastina indicada originalmente).
			//CommonPar(5): Indicador de simetria (0-não há simetria, 1-há simetria).
			//CommonPar(6): Indicador de eixo de simetria (1-eixo X é de simetria, 0-eixo Y é de simetria). Vale só se CommonPar(5)=1.
			//CommonPar(7): Valor de elastina vezes a relação espessura sobre raio do elemento (usualmente elastina*0.1). Só se CommonPar(11)=0.
			//CommonPar(8): Valor do raio de curvatura do elemento. Só se CommonPar(11)=0.
			//CommonPar(9): Valor de pressão de referência do elemento. Só se CommonPar(11)=0.
			//CommonPar(10): Valor de viscoelasticidade vezes a relação espessura sobre raio do elemento (usualmente viscoelasticidade*0.1). Só se CommonPar(11)=0.
			//CommonPar(11): Indicador de leitura em Basparam.txt ou em Param.txt dos parâmetros deste elemento (1-lê do Param.txt, 0-lê do Basparam.txt).
			//CommonPar(12): Indicador de esquema incremental, isto é, updated Lagrangian (0-não incremental, 1-incremental).  
			
			// Incremental Version = 0 nao incremental e 1 incremental
			
			if (i <= this->FullModel->GetNumberOfShells() && !j && i)
				{
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+0] = 1;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1] = -1; // normal
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2] = 0;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3] = 1;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+5] = 0;	
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+10]= 1;
				
				if (!this->IntParamArray->GetValue(0))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+11]= 0;
				else // se modelo é deformavel
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+11]= this->IntParamArray->GetValue(7);
				}
				
		  //***************************
			// Configuracao dos elementos de tampa 540
			//**************************
//			540: CommonPar -> 7, Param -> 2*NPf, JParam -> 1
//			CommonPar(1): Valor da pressão a ser imposta somente no caso de considerar pressão constante no tempo.
//			CommonPar(2): Quantidade de posições entre o primeiro nó, que é o terminal, e o primeiro nó do triângulo. Trivialmente vale sempre 1.
//			CommonPar(3): Coeficiente da normal (1-se as normais são externas, -1-se as normais são internas).
//			CommonPar(4): Coeficiente para indicar se há curva de pressão associada a estes elementos. Usa-se quando for necessário impor pressão 
//      variante no tempo. Se este valor é nulo então considera-se pressão constante e lê-se o primeiro elemento deste CommonPar como valor de
//      pressão. 
//			Se não for nulo então deve ser fornecida a curva da pressão através da estrutura Param.

//     	CommonPar(5): Indicador de esquema incremental, isto é, updated Lagrangian (0-não incremental, 1-incremental).
//			CommonPar(6): Indicador de simetria (0-não há simetria, 1-há simetria).
//			CommonPar(7): Indicador de eixo de simetria (1-eixo X é de simetria, 0-eixo Y é de simetria). Vale só se CommonPar(5)=1.
//			Param(Pf...): Lista de 2*NPf pontos que dão a informação das respectivas curvas de valores em função do tempo para Pf. Só se JParam(1)>0.
//			JParam(1): Número de pontos correspondentes na curva da pressão do da face Pf (NPf aqui é arbitrário). 
			
			if (!j && i > this->FullModel->GetNumberOfShells()) // coluna zero dos elementos de tampa : Valor da pressão a ser imposta somente no 
																													//	caso de considerar pressão constante no tempo.
				{
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->FullModel->GetCoverConstantPressure(cover);
			  cover++;		
				}
			
			// CommonPar(2): Quantidade de posições entre o primeiro nó, que é o terminal, e o primeiro nó do triângulo. Trivialmente vale sempre 1. 
			if (j==1 && i > this->FullModel->GetNumberOfShells()) 
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 1;
			
			// tampa CommonPar(3): Coeficiente da normal (1-se as normais são externas, -1-se as normais são internas).
			if (j==2 && i > this->FullModel->GetNumberOfShells()) 
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= -1;
		
			// curva associada a tampa (coluna 4)
			if (j==3 && i > this->FullModel->GetNumberOfShells())
				{
				// cover esta com valor errado!!!!!!!!!!!!!!!!1
				
				if (this->FullModel->GetCoverElementsBCType()->GetValue(cover2)==2)
					{
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 1;
					}
				else
					{
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 0;
					}
				cover2++;
				}
				
			// esquema incremental dos elementos das tampas (coluna 5)
			if (j==4 && i > this->FullModel->GetNumberOfShells())
				{
				if (!this->IntParamArray->GetValue(0))
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 0;
				else // se modelo é deformavel
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->IntParamArray->GetValue(7);
			  }
			}
		}
	
	
	// **************************************************************************************************
	// *************************************************************************************************
	// 						*Elementos e SolverConfig SUBSTEP Bubble
	// *************************************************************************************************
	// *************************************************************************************************
	
	
	this->BasParam->SetResultantMatrixIsSymmetricalSubStepBubble(true);
	this->BasParam->SetCalculationStepIsLinearSubStepBubble(false);
	
	// codigo do tipo de resolvedor
	if (!this->IntParamArray->GetValue(13))
		this->BasParam->SetResolveLibrarySubStepBubble(0);
	else // se solver é paralelo
		this->BasParam->SetResolveLibrarySubStepBubble(20061129);
	
	
	int *elementTypeBubble = new int[this->BasParam->GetQuantityOfDifferentElements()*5];

	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			elementTypeBubble[i*5+j] = elementType[i*5+j];

			// no segundo substep o elemento de volume sera sempre 525
			if (!i && !j)
				elementTypeBubble[i*5+j]= 525;
			
			// segundo substep elemento de parede sera 57 
			if (i <= this->FullModel->GetNumberOfShells() && !j && i) 
				elementTypeBubble[i*5+j]= 57;
				
			// somente o elemento de volume estara habilitado
			//if (!i && j==3)
			//	elementTypeBubble[i*5+j]= LastCalculatedElement;
		
			if (!i && j==2)
				elementTypeBubble[i*5+j]= 1;
			
			// os demais elementos ficam desabilitados neste substep Bubble	
			//if (i && j==3) elementTypeBubble[i*5+j]= 0;
			if (j==3) elementTypeBubble[i*5+j]= 0;
			}
		}

	this->BasParam->SetElementTypeSubStepBubble(elementTypeBubble);
	this->BasParam->SetParamElementType(paramElementType);
	this->BasParam->SetNormaSubStep(norma);
	this->BasParam->SetToleranceSubStep(Tolerance);
	this->BasParam->SetNameDegreeOfFreedomSubStep(name);

	Nums[0]=0;
	Nums[1]=0;
	Nums[2]=0;
	
	this->BasParam->SetNumsSubStep(Nums);
	this->BasParam->SetLogFilename(this->LogFilename);
	
	
	// OPCOES SETADAS SOMENTE QUANDO O SOLVER É PARALELLO
  this->BasParam->SetParallelSolverNumberOfProcessors(this->IntParamArray->GetValue(14));
  
  this->BasParam->SetParallelSolverRLS(this->IntParamArray->GetValue(9));
  
  this->BasParam->SetParallelSolverGMRESIterations(this->IntParamArray->GetValue(15));
  
  this->BasParam->SetParallelSolverMaxIterations(this->IntParamArray->GetValue(16));
  
  this->BasParam->SetParallelSolverPreConditioning(this->IntParamArray->GetValue(17));
  
  
  this->BasParam->SetParallelSolverRelativeConvError(this->SolverConfigParamArray->GetValue(4));
  
  this->BasParam->SetParallelSolverAbsConvError(this->SolverConfigParamArray->GetValue(5));
	
	
	
}

//*********************************************************************************

void vtkHM3DFullModelWriterPreProcessor::InitializeWriteProcess(char *path)
{
	vtkDebugMacro(<<"vtkHM3DFullModelWriterPreProcessor::InitializeWriteProcess(char *path)");

	this->BasParam 					= vtkHM1DBasParam::New();
	vtkHM1DMesh *Mesh 			= vtkHM1DMesh::New();
	vtkHM1DParam *Param 		= vtkHM1DParam::New();
	vtkHM1DIniFile *IniFile = vtkHM1DIniFile::New();
	
  this->NumGroups = this->FullModel->GetSurface()->GetNumGroups(); //Obtendo o numero de grupos de vtkSurfaceGen.
  this->NumNodesSurface = this->FullModel->GetSurface()->GetCoords()->GetNumNodes(); //Obtendo o numero de coordenadas de vtkSurfaceGen.
  this->NumNodesVolume = (int) this->FullModel->GetVolume()->GetNumberOfPoints(); //Obtendo o numero de coordenadas do volume.
  this->nel = this->FullModel->GetVolume()->GetNumberOfCells(); //Obtendo o numero de elementos do volume.

	if (this->WhichFile[0])
		{
		this->BasParam->SetDoFNumber(this->BasParamDOFNumber);
		// geracao BASPARAM.txt
		this->ConfigureBasparam();
		}
	

	if (this->WhichFile[1])
		{
		// geracao MESH.txt	
		// gera lista dos arquivo Mesh.txt
		
		this->GenerateMeshLists();
		this->GenerateDirichletsValues(); 
		
		// filling Mesh parameters
		Mesh->SetDegreeOfFreedom(this->BasParamDOFNumber); 
		Mesh->SetDimension(3);
		Mesh->SetNumberOfElementGroups(this->GetNumberOfElementGroups());
		Mesh->SetNumberOfPoints(this->GetNumberOfPoints());
		Mesh->SetElementType(this->GetElementType());
		Mesh->SetElementMat(this->GetElementMat());
		Mesh->SetElementForGroup(this->GetElementForGroup());
		Mesh->SetPoints(this->GetPoints());
		Mesh->SetGroupElements(this->GetElementsIncidence());
		Mesh->SetDirichletsTag(this->GetDirichletsTag());
		Mesh->SetDirichlets(this->GetDirichlets());
		}

	if (this->WhichFile[2])
		{
	  // geracao PARAM
		this->CreateParam();
		
		//Setando numero de grupos de parametros
		Param->SetNumberOfGroups(this->nel+this->NumGroups);
		
		Param->SetRealParameters(&this->RealParameters);
		Param->SetIntegerParameters(&this->IntegerParameters);

		Param->SetQuantityOfRealParameters(this->QuantityOfRealParameters);
		Param->SetQuantityOfIntegerParameters(this->QuantityOfIntegerParameters);
		}


	if (this->WhichFile[3])
		{
		// geracao Inifile
		this->GenerateIniFile();
  	IniFile->SetInitialConditions(this->GetIniFileArray());
		}
		
	// criando estrutura que é entrada do writer dos arquivos
	vtkHM1DData *Data = vtkHM1DData::New();

	if (this->WhichFile[0])
		Data->SetBasParam(this->BasParam);
	
	if (this->WhichFile[1])
		Data->SetMesh(Mesh);
	
	if (this->WhichFile[2])
		Data->SetParam(Param);
	
	if (this->WhichFile[3])
		Data->SetIniFile(IniFile);

  Data->SetModelType(1); // define como modelo 3d
  Data->Set3DFullModel(this->FullModel);

  //if (this->IntParamArray[13])
  if (this->IntParamArray->GetValue(13))
    {
    vtkHMParallelSolverScriptWriter *ScriptWriter = vtkHMParallelSolverScriptWriter::New();
    if (!ScriptWriter->WriteParallelSolverScript(BasParam, path))
      vtkErrorMacro("Error on writing the Script for Parallel Solver!");
    ScriptWriter->Delete();
    }

  
  
	vtkDebugMacro(<<"Escrevendo os Arquivos");

	vtkHM1DStraightModelWriter *writer = vtkHM1DStraightModelWriter::New();
	writer->SetWhichFile(this->WhichFile);
	
	
	writer->SetInput(Data);
	writer->SetFileName(path);
	writer->WriteData();

	this->BasParam->Delete();
	Mesh->Delete();
	Param->Delete();	
	IniFile->Delete();

	writer->Delete();
	Data->Delete();

}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetFullModel(vtkHM3DFullModel *FullModel)
{
 this->FullModel = FullModel;
}
//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetLogfilename (const char *LogFilename)
{
	this->LogFilename = LogFilename;   
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetTimeParam(double TimeParamArray[3])
{
  for (int i=0; i < 3; i++)
		this->TimeParamArray->SetValue(i, TimeParamArray[i]);  	
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetIntegerParam(int IntParamArray[18])
{

for (int i=0; i < 18; i++)
  this->IntParamArray->SetValue(i, IntParamArray[i]);   

}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetConvergenceParam(double ConvParamArray[15])
{
  for (int i=0; i < 15; i++)
		this->ConvParamArray->SetValue(i, ConvParamArray[i]);  	
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetSolverParam(double SolverConfigParamArray[6])
{
	for (int i=0; i < 6; i++)
		this->SolverConfigParamArray->SetValue(i, SolverConfigParamArray[i]);  	
}

//-------------------------------------------------------------------------------------

void vtkHM3DFullModelWriterPreProcessor::SetModelParam(double ModelConfigParamArray[5])
{
  for (int i=0; i < 5; i++)
		this->ModelConfigParamArray->SetValue(i, ModelConfigParamArray[i]);  	
}

//-------------------------------------------------------------------------------------

int vtkHM3DFullModelWriterPreProcessor::GetPointID(double x, double y, double z)
{
	for (int i=0; i < this->points->GetNumberOfPoints(); i++)
		{
		double *pointcoord = this->points->GetPoint(i);
		if (DoubleCompare(pointcoord[0],x) && DoubleCompare(pointcoord[1],y) && DoubleCompare(pointcoord[2],z))
			{
			return i+1;
			break;
			}
		}
	return 0;	 
}
//-------------------------------------------------------------------------------------

int vtkHM3DFullModelWriterPreProcessor::DoubleCompare(double one, double two)
{
	if (fabs(one - two) < 1E-5)
		return 1;
	else	
		return 0;
}

//-------------------------------------------------------------------------------------
int vtkHM3DFullModelWriterPreProcessor::SearchformCommonPoints(int point)
{
	vtkSurfaceGen* surfGen = this->FullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
 	
 	// percorre somente a parede arterial, grupo =0
  curr = first;
  while (curr)
  	{
    if (curr->gr == 0)
    	{
			if (point == curr->n1 || point == curr->n2 || point == curr->n3)
				return 1;
      }
      curr = curr->next;
    	}
    return 0;	
}

//-------------------------------------------------------------------------------------

  // Description:
	// builds the Transpose Connectivity matrix that stores the neighboors elements from a surface point.
	// 
 	// for instance:  node 1 --> elements: 2, 5 ,6 ----> node 1 is "used" by triangle elements number 2, 5 and 6
 	//                node 2 --> elements: 33 55 6 44 55
 	//								node n 


void vtkHM3DFullModelWriterPreProcessor::BuildTransposeConnectivity()
{
  vtkSurfaceGen* surfGen = this->FullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.


  //int NumNodesSurface = surfGen->GetCoords()->GetNumNodes();
    
  // constroi a conectividade transposta que relaciona nó de surface com os elementos vizinhos a este
  // por exemplo: nó 1 --> elementos viznhos: 2, 5 ,6
  //                 2 --> 33 55 6 44 55		

	int element=1;    
	curr = first;
	unsigned long int groups=1;
  
	ListOfInt lst;
  lst.clear();
	
  for (int i=0; i < this->NumNodesSurface; i++) // percorre todos nos de superficie
		{
//		ListOfInt lst;
//		lst.clear();
  	/*
  	Para cada no que percorrido, todos elementos da surface sao percorridos
  	em busca dos elementos triangulos que contenham aquele nó analisado
  	
  	Complexidade n^2
  	*/
  	
		while (curr)
  		{
    	if (curr->n1 == i || curr->n2 == i || curr->n3 == i) // verifica se no triangulo atual existe referencia ao nó atual 
    		{
    		lst.push_back(element); // se sim, insere o id do elemento na lista que relaciona os nós (pontos) com triangulos que este faz parte
    		//cout << i << " "<< element << " "; 
    		//cout << "grupo " << curr->gr << endl;
    		
    		// esta variavel armazena o grupo dos elementos da lista atual
    		// o qual sera usuada no metodo GetGroupFromPoint
    		// se por exemplo existe algum elemento que é de parede (grupo 0) o valor da variavel sera zero (por isso é usado multiplicacao)
    		// se for todos grupos forem 1, entao valor da variavel sera 1
    		// se tivermos 1 1 1 1 1 -- valor final =1 (nó esta no interior da tampa grupo 1)
    		// se tivermos 0 0 0 0 0 2 -- valor final =0 (no esta na fronteira - aqui consideramos que o no é de parede)
				// set tivermos 0 0 0 0 0 -- valor final=0 (nó estara na parede arterial)
//    		if (curr->gr < 0)
//    		  {
//    		  cout << "Valor negativo para grupo do elemento atual "<< curr->gr << endl;
//    		  }
    		
    		
    		groups= curr->gr * groups; 
    		
    		// se algum dos elementos triangulos for do grupo zero (surface) isso indica que este nó esta na área de fronteira
    		// entre parede e tampa --> para nossos propositos, nó na fronteira será nó da parede. 
    		
    		}      
    	curr = curr->next; // passa para o proximo triangulo
    	element++; 
  		}

		// no final do while tem uma lista de inteiros de tamanho variavel que descreve 
		// os elementos viznhos do ponto indicado por i 
		// ex: (23, 45 ,334, 6787)
		
		
		
		if (groups==0 || groups==1 ) 
			{
			//cout << "Dominante group >" << groups << endl;
			this->GroupFromPoint.push_back(groups);
			}
		else
			{
			// se tivermos 2 2 2 2 2 -- valor final da variavel "groups" será 32 (nó está dentro do tampa grupo 2)
			
			// para variaveis group que tem valor diferente de 0 ou 1
			// o seguinte processo é utilizado para descoberta do grupo: Suponto que tem-se o valor de 32 e tamanho da lista 5 elementos
			// para descobrir o grupo dos pontos é utilizada a formula: grupo_dominante= ( group^(1/numero_elem_lista) )
			int list_size =lst.size();
			double exp= 1.0/float(list_size);
			this->GroupFromPoint.push_back(int(pow (double(groups),exp)));

			// cout << "Lst.size " << lst.size() << endl;
			//cout << "Exp " << exp << endl;
			
//			cout << "groups "<< groups << endl;
//			cout << "list_size "<< list_size << endl;
//			cout << "Dominante group #####################=======>" << int(pow (double(groups),exp)) << endl;
			}
		//cout << endl;
		groups=1;
		curr = first;
		element=1;
		TransposeConnectivity.push_back(lst);          
		}
  //} // closes the parallel code
}


//-------------------------------------------------------------------------------------

int vtkHM3DFullModelWriterPreProcessor::GetGroupFromPoint(int point)
{
//	cout <<"buscando grupo ponto "<< point << endl;
//	
//	cout << "Size "<< GroupFromPoint.size() << endl;


  int i=0;
	ListOfInt::iterator it = GroupFromPoint.begin();
	for (; it !=  GroupFromPoint.end();it++ )
		{
		if (i==point)
		  {
//			cout << "retornando grupo "<< (*it) << endl;
		  return (*it);
		  }
		i++;
		}

  // caso não ache ira retornar -1 (erro)
//	cout << "grupo nao encontrado "<< endl;
	return -1;

}
//-------------------------------------------------------------------------------------


void vtkHM3DFullModelWriterPreProcessor::SetWhichFile(int WhichFile[4])
{
	for (int i=0; i < 4; i++)
		this->WhichFile[i]	=	WhichFile[i];
	
}
//-------------------------------------------------------------------------------------
