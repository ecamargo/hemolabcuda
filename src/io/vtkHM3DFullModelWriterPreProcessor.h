
#ifndef vtkHM3DFullModelWriterPreProcessor_h_
#define vtkHM3DFullModelWriterPreProcessor_h_

#include "vtkObject.h"
#include "vtkPoints.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DParam.h"
#include "vtkTimerLog.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM3DFullModel;

#include <vector>
#include <list>
#include <string>
	
class VTK_EXPORT vtkHM3DFullModelWriterPreProcessor : public vtkObject
{
public:
	
	//BTX
	//typedef std::list<int> ListOfDouble;
	typedef std::list<int> ListOfInt;
//	typedef std::vector<vtkIntArray*> VectorOfIntArray;
	typedef std::vector<ListOfInt> VectorOfIntArray;
	//ETX
	
	static vtkHM3DFullModelWriterPreProcessor *New();
	vtkTypeRevisionMacro(vtkHM3DFullModelWriterPreProcessor,vtkObject);
	void PrintSelf(ostream& os, vtkIndent indent);

	// Description:
	// Generates the Cordinates, Element Group, Incidence, Element Type and Element Mat list from Mesh.txt file.
  void GenerateMeshLists();
  
  // Description:
  // return Points instance
  vtkPoints *GetPoints();
  
  // Description:
  // return the number of points 
  int GetNumberOfPoints();
   
  //BTX
	
	// Description:
  // return a vector of integer list that describe the model incidence
  vtkHM1DMesh::VectorOfIntList *GetElementsIncidence();
   
   
  //ETX 
	// Description:
	// set/get methods for the model's number of degrees of freedom 
//	vtkSetMacro(DOF, int);
//	vtkGetMacro(DOF, int);
	
	
	// Description:
	// set/get method for the dimension
//	vtkSetMacro(Dimension, int);
//	vtkGetMacro(Dimension, int);
	
	// Description:
	// Method for creation of the data for param file
	//BTX
	void CreateParam();
	
	
	
	//ETX
	// Fim dos metodos para geracao do param

	
	// Description:
	// Set/Get nElementForGroup
	int *GetElementForGroup();
	void SetElementForGroup(int *elemForGroup);
	
	// Description:
	// Set/Get ElementType
	int *GetElementType();
	void SetElementType(int *elemType);
	
	
	// Description:
	// Set/Get Element mat
	int *GetElementMat();
	void SetElementMat(int *elemMat);
	
	// Description:
	// Set/Get MaxElemLib
	//vtkSetMacro(MaxElemLib, int);
	//vtkGetMacro(MaxElemLib, int);
	
	// Description:
	// return the number of element groups
	int GetNumberOfElementGroups();
	
	
	// Description:
	// Set/Get DirichletsTag
	vtkIntArray *GetDirichletsTag();
	void SetDirichletsTag(vtkIntArray *d);
	
	// Description:
	// Set/Get Dirichlets
	vtkDoubleArray *GetDirichlets();
	void SetDirichlets(vtkDoubleArray *d);
	
	// Description:
	// generate DirichletsTag and Dirichlets only with zeros 
	void GenerateDirichletsValues();
	
	// Description:
	// generate Infile array with area as only non zero value
  void GenerateIniFile();
  
  // Description:
	// Set /Get IniFileArray 
  vtkDoubleArray *GetIniFileArray();
  void SetIniFileArray(vtkDoubleArray *d);
  
  
  // Description:
  // covers current StraightModel tree in order to get data to  
  // create vtkHM1DMesh, vtkHM1DIniFile and vtkHM1DParam objects. 
  void InitializeWriteProcess(char *path);
  
  // Description:
  // create BasParam with values from HemoLab General Parameters setup interface
	void ConfigureBasparam();
		
		
	// Description:
	//	set the 3D FullModel from filter output	
	void SetFullModel(vtkHM3DFullModel *FullModel);
	
	// Description:
	// set the name of the Basparam log file
	void SetLogfilename (const char *LogFilename);
	
	// Description:
	// set time parameters used in files generation
	void SetTimeParam(double TimeParamArray[3]);
	
	// Description:
	// set integer parameters used in files generation
	void SetIntegerParam(int IntParamArray[18]);
	
	// Description:
	// set solver convergence parameters used in files generation
	void SetConvergenceParam(double ConvParamArray[15]);
	
	// Description:
	// set general model parameters used in files generation
	void SetModelParam(double ModelConfigParamArray[5]);

	// Description:
	// set general solver parameters used in files generation
	void SetSolverParam(double SolverConfigParamArray[6]);

  // Description:
  // given some point xyz coordeantes, this returns 
  // the id from the point in the mesh's coordinates list
	int GetPointID(double x, double y, double z);

  // Description:
	// como existe diferenca de arrendondamente entre valor de coordenada
	// de ponto do volume e ponto da superficie, entao é necessario implementar este metodo 
	// para realizar comparacao até o quinto digito depois da virgula 
	// por exemplo: 0.062667 e 0.0626666
	int DoubleCompare(double one, double two);	


  // Description:
  // returns true if a surface point is located in the border between cover and wall 
  // WARNING: this method is not currently being used.
	int SearchformCommonPoints(int point);

  // Description:
	// builds the Transpose Connectivity matrix that stores the neighboors elements from a surface point.
 	// for instance:  node 1 --> elements: 2, 5 ,6 ----> node 1 is "used" by triangle elements number 2, 5 and 6
 	//                node 2 --> elements: 33 55 6 44 55
 	//								node n 	 
	// ***********************************************
	// Este metodo constroi as duas estruturas:
	// TransposeConnectivity < numero do nó, lista de ids dos elementos que contem o nó>
	// GroupFromPoint <int, int> -- relaciona o nó e o grupos que os elementos que tem esse nó
	void BuildTransposeConnectivity();

  // Description:
	// returns the group of some surface point.
	// if the point is located into the cover, this method will return the number of the group
	// if the point is located in wall (including the points located in the border between cover and wall)
	// this method will return the group of surface, which here is zero. 
	int GetGroupFromPoint(int point);
	
	// Description:
	// set which int array with values from filter
	void SetWhichFile(int WhichFile[4]);
	
	
		// Description:
	// Set and Get method for ScaleFactor3D
	vtkSetMacro(ScaleFactor3D, double);
	vtkGetMacro(ScaleFactor3D, double);
	
	

	
//---------------------------------------------------------------	
protected:
	vtkHM3DFullModelWriterPreProcessor();
	~vtkHM3DFullModelWriterPreProcessor();
	
	// Description:
	// used to build coordinates list in Mesh.txt
  vtkPoints* 		points;
	
	//BTX
  // Description:
  // Vector with an list of the element groups
	vtkHM1DMesh::VectorOfIntList ElementsIncidence;
	//ETX
	
	// Description:
	// used in Basparam.txt
	//int DOF;
	
	// Description:
	// used in Mess.txt
	//int Dimension;
	
	// Description:
  // Number of elements for group
  int *nElementForGroup;
  	
  	
  // Description:
  // Types of elements of the 1D tree (terminal, segment, etc...)
  //*ELEMENT TYPE
  // se MaxElemLib do basparam é 4 
  // 2 heart / terminal
  // 1 elem
  // 3 elem in
  // 4 elem out
	int *ElementType;
	
	
	// Description:
	//
	vtkIntArray *ElementTypeArray;
	
	
	// Description:
  // Element mat of the 1D tree
  int *ElementMat;

	// Description:
  // valores padroes 2 ou 4
	//int MaxElemLib;
  
  //BTX
  // Variaveis do arquivo param
	// Description:
	// used in Param.txt
  int *QuantityOfRealParameters;	

	// Description:
	// used in Param.txt
	int *QuantityOfIntegerParameters;
	
	// Description:
	// used in Param.txt
	vtkHM1DParam::VectorOfDoubleList RealParameters;
	
	// Description:
	//used in Param.txt
	vtkHM1DParam::VectorOfIntList IntegerParameters;
	
	//ETX
	// Fim de declaracoes do arquivo param
	
	
	// Description:
	// used in mesh.txt
	vtkIntArray *DirichletsTag;
	
	// Description:
	//
	vtkDoubleArray *Dirichlets;
		
	// Description:
	//used in Inifile.txt
	vtkDoubleArray *IniFileArray;
		
	// Description:
	//used in Inifile.txt
	vtkDoubleArray *IniFileDoubleArray;
	
	// Description:
	// Basparam é atributo dessa classe pois diversos valores de seus atributos sao necessario para construcao das demais
	// estruturas Mesh Param e Inifile.
	vtkHM1DBasParam *BasParam;
	
	// Description:
	// used in Basparam.txt
	int BasParamDOFNumber; 
	
	// Description:
	// FullModel instance used in Files generation
	vtkHM3DFullModel *FullModel;
	
	// Description:
	// store the number of groups from current 3D model
	int NumGroups;

	// Description:
	// number of elements from volume
	int nel;
	
	
	// Description:
	// surface element type code, usually 57
	int	SurfaceElementType;

	// Description:
	// cover element type code, usually 54 or 540
	int CoverElementType;

	// Description:
	// volume element type code, usually 524 
	int VolumeElementType;
	
	// Description:
	//
	const char *LogFilename;
	
	// Description:	
	// hm3DModuleServer xml file property
	vtkIntArray *IntParamArray;
	
	// Description:	
	// hm3DModuleServer xml file property
	vtkDoubleArray *TimeParamArray;
	
	// Description:	
	// hm3DModuleServer xml file property
	vtkDoubleArray *ConvParamArray;
	
	// Description:	
	// hm3DModuleServer xml file property
	vtkDoubleArray *ModelConfigParamArray;
	
	// Description:	
	// hm3DModuleServer xml file property
	vtkDoubleArray *SolverConfigParamArray;
	
	
	// Description:
	// vector of int lists
	VectorOfIntArray TransposeConnectivity;
	
	// Description:
	//
	ListOfInt GroupFromPoint;
	
	// Description:
	// indicates which solver file will be generated
	int WhichFile[4];
	
	// Description:
	// number of surface nodes
	int NumNodesSurface;

	// Description:
	// number of volume nodes
	int NumNodesVolume;
	
	double ScaleFactor3D;
	
	// Description:
	// used to measure time interval solver files generation 
	vtkTimerLog *Timer;
	
	
}; //End class

#endif 
