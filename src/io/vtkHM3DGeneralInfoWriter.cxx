
#include "vtkHM3DGeneralInfoWriter.h"
#include "vtkHM3DFullModel.h"
#include "vtkHM1DMesh.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"



#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHM3DGeneralInfoWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHM3DGeneralInfoWriter);

//----------------------------------------------------------------------------
void vtkHM3DGeneralInfoWriter::Start3D(vtkHM3DFullModel *FullModel, char *dir)
{
  ostream *fp;
	
	char d[256];
	strcpy(d, dir);
	
	strcat(d, "/ModelInfo.hml3d");

	this->SetFileName(d);
	
    if ( !(fp=this->OpenFile(FullModel)) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  
	if ( !this->Write3DData(fp, FullModel) )
	 {
	 errorOccured = 1;
	 }
	
	*fp << buffer ;
	buffer.clear();

  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}

//----------------------------------------------------------------------------

void vtkHM3DGeneralInfoWriter::StartCoupled(vtkIdList *ModelInfo, vtkIntArray *elementType, vtkIntArray *elementMat, char *dir)
{
  ostream *fp;
	
	char d[256];
	strcpy(d, dir);
	
	strcat(d, "/ModelInfo.hml13d");
		
	this->SetFileName(d);
	
    if ( !(fp=this->OpenFile(NULL)) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return;
    }
    
  
  //
  // Write data owned by the dataset
  int errorOccured = 0;
  
//  if (this->ModelType == 1) //3d
//  	{
//	  if ( !this->WriteData(fp, FullModel) )
//	    {
//	    errorOccured = 1;
//	    }
//	  }
//	else //coupled model info
//		{
  vtkIntArray *elementList = NULL;
  vtkIntArray *terminalList = NULL;
  
  if ( elementType )
  	{
  	elementList = this->BuildNullElements(elementType, elementMat);
  	terminalList = this->BuildNullTerminals(elementType, elementMat);
  	}
  
  if ( !this->WriteCoupledData(fp, ModelInfo, elementList, terminalList) )
    {
    errorOccured = 1;
    }
	if ( elementList )
		elementList->Delete();
	if ( terminalList )
		terminalList->Delete();
//		}  

  
	*fp << buffer ;
	buffer.clear();

  if(errorOccured)
    {
    if(this->FileName)
      {
      vtkErrorMacro("Ran out of disk space; deleting file: " << this->FileName);
      this->CloseVTKFile(fp);
      unlink(this->FileName);
      }
    else
      {
      vtkErrorMacro("Error writting data set to memory");
      this->CloseVTKFile(fp);
      }
    return;
    }
  this->CloseFile(fp);
}



//----------------------------------------------------------------------------


ostream *vtkHM3DGeneralInfoWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening IniFile file for writing...");

//  if (this->WriteToOutputString)
//    {
//    // Get rid of any old output string.
//    if (this->OutputString)
//      {
//      delete [] this->OutputString;
//      this->OutputString = NULL;
//      this->OutputStringLength = 0;
//      this->OutputStringAllocatedLength = 0;
//      }
//    // Allocate the new output string. (Note: this will only work with binary).
//    if (input == NULL)
//      {
//      vtkErrorMacro(<< "No input! Can't write!");
//      return NULL;    
//      }
//    input->Update();
//    this->OutputStringAllocatedLength = (int) (500 
//      + 1000 * input->GetActualMemorySize());
//    this->OutputString = new char[this->OutputStringAllocatedLength];
//
//    fptr = new ostrstream(this->OutputString, 
//                          this->OutputStringAllocatedLength);
//    }
  //else 
   // {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
  //  }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
// Close a IniFile file.
void vtkHM3DGeneralInfoWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing IniFile file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}

//----------------------------------------------------------------------------
int vtkHM3DGeneralInfoWriter::Write3DData(ostream *fp, vtkHM3DFullModel *FullModel)
{
	//double *num;
	char str[1024];

	// numero de grupos de  tampas
  buffer.append("*Cover Groups\n");
  sprintf (str, "%i\n", FullModel->GetNumberOfCoverGroups());
  buffer.append(str);
  buffer.append("\n");

	// numero de grupos de parede
  buffer.append("*Shell Groups\n");
  sprintf (str, "%i\n", FullModel->GetNumberOfShells());
  buffer.append(str);
  buffer.append("\n");
  
  // parametros dos grupos de parede
  buffer.append("*Shell Parameters\n");
  for (int i = 0; i < FullModel->GetNumberOfShells(); ++i)
  	{
		vtkDoubleArray *WallParam = FullModel->GetWallParameters(i);
		
		for ( int i=0; i<5; i++ )
			{
			sprintf (str, "%.6f ", WallParam->GetValue(i));
			buffer.append(str);
			}
		buffer.append("\n");
		}
  buffer.append("\n");
  
  
  buffer.append("*Cover Parameters\n");
  
  for (int i = 0; i < FullModel->GetNumberOfCoverGroups(); ++i)
 		{
 		int BCType = FullModel->GetCoverElementsBCType()->GetValue(i);
 		
 		if (BCType == 1)//constant pressure 
 			{
 			sprintf (str, "%.6f\n", FullModel->GetCoverConstantPressure(i));
			buffer.append(str);
			buffer.append("\n");
			
 			}
 		if (BCType == 2)// variant pressure
 			{
 			vtkDoubleArray *PressureCurve =  FullModel->GetPressureCurve(i);
 			for (int i = 0; i < PressureCurve->GetNumberOfTuples(); ++i)
 				{
 				sprintf (str, "%.6f %.6f \n", PressureCurve->GetTuple2(i)[0], PressureCurve->GetTuple2(i)[1]);	
 				buffer.append(str);
 				}	
 			}

 		if (BCType == 3) //velocity
 			{
 			double *Velocity  = FullModel->GetCoverElementVelocity(i);
 			sprintf (str, "%.6f %.6f %.6f\n", Velocity[0], Velocity[1], Velocity[2]);
 			buffer.append("\n");
 			buffer.append(str);
 			}
		//
 		} 
  
  return 1;
}

//----------------------------------------------------------------------------

int vtkHM3DGeneralInfoWriter::WriteCoupledData(ostream *fp, vtkIdList *list, vtkIntArray *elementList, vtkIntArray *terminalList)
{
	char str[1024], str2[1024];
	int counter = 0;
	
	// numero de elementos 1D
  buffer.append("*1D ELEMENTS\n");
  sprintf (str, "%i\n", list->GetId(0));
  buffer.append(str);
  buffer.append("\n");

	// numero de elementos 1D diferentes (2 ou 4) 
  buffer.append("*NUMBER OF DIFFERENT 1D ELEMENTS\n");
  sprintf (str, "%i\n", list->GetId(1));
  buffer.append(str);
  buffer.append("\n");

	// numero de elementos 1D nulos
  buffer.append("*NULL 1D ELEMENTS\n");
  if ( elementList )
  	{
	  sprintf (str, "%i\n", elementList->GetNumberOfTuples());
	  buffer.append(str);
	  double *tuple;
	  
	  // escreve os elementos nulos do modelo 1D
	  for ( int i=0; i<elementList->GetNumberOfTuples(); i++ )
	  	{
	  	tuple = elementList->GetTuple3(i);
			sprintf (str, "%i %i %i\n", (int)tuple[0], (int)tuple[1], (int)tuple[2]);
			buffer.append(str);
	  	}
	  
  	}
  else
  	{
  	sprintf (str, "%i\n", 0);
  	buffer.append(str);
  	}
  buffer.append("\n");
	// numero de terminais 1D nulos
  buffer.append("*NULL 1D TERMINALS\n");
  if ( terminalList )
  	{
	  sprintf (str, "%i\n", terminalList->GetNumberOfTuples());
	  buffer.append(str);
	  double *tuple;
	  
	  // escreve os terminais nulos do modelo 1D
	  for ( int i=0; i<terminalList->GetNumberOfTuples(); i++ )
	  	{
	  	tuple = terminalList->GetTuple3(i);
	  	sprintf (str, "%i %i %i\n", (int)tuple[0], (int)tuple[1], (int)tuple[2]);
			buffer.append(str);
	  	}
//	  if ( terminalList->GetNumberOfTuples() > 0 )
//	  	buffer.append("\n");
  	}
  else
  	{
  	sprintf (str, "%i\n", 0);
  	buffer.append(str);
  	}
  buffer.append("\n");
  // numero de estruturas 3d
  buffer.append("*3D STRUCTURES\n");
  
  int NumberOf3Ds = list->GetId(2);
  sprintf (str, "%i\n", NumberOf3Ds);
  buffer.append(str);
  buffer.append("\n");
  
  counter = 3;
  
  buffer.append("*3D VOLUME ELEMENTS\n");
  
  // numeros de elementos de volume 
  for (int i = 0; i < NumberOf3Ds; ++i)
  	{
		sprintf (str, "%i ", list->GetId(i+3));
		buffer.append(str);
  	counter++;
  	}
  buffer.append("\n");
	
	
	
	
	// parametros de casca por 3d
	// numero de grupos de casca  numero de elementos de casca(1..n)
	// ex: 3 456 879 7373
  buffer.append("\n*3D SHELL PARAMETERS\n");
  for (int i = 0; i < NumberOf3Ds; ++i)
  	{
    
    int CurrentNumberOfShells = list->GetId(counter);
    
    sprintf (str, "%i ", CurrentNumberOfShells);
    
    buffer.append(str);
    
    counter++;
    
    for (int z = 0; z < CurrentNumberOfShells; ++z) 
    	{
			sprintf (str, "%i ", list->GetId(counter));
			buffer.append(str);
			counter++;
			}
    
  	buffer.append("\n");
  	}
  buffer.append("\n");
  

	// numero de elementos de casca por 3d
  buffer.append("*3D COUPLING PARAMETERS\n");
  for (int i = 0; i < NumberOf3Ds; ++i)
  	{
    int CurrentNumberOfCovers = list->GetId(counter);
    sprintf (str, "%i ", CurrentNumberOfCovers);
  	buffer.append(str);
  	counter++;
  	
  	for (int z = 0; z < CurrentNumberOfCovers ; ++z) 
    	{
			sprintf (str, "%i ", list->GetId(counter));
			buffer.append(str);
			counter++;
			}
  
  	buffer.append("\n");
  	}
  buffer.append("\n");


	// numero de substeps
  buffer.append("*SUBSTEPS NUMBER\n");
	sprintf (str, "%i ", list->GetId(counter));
	buffer.append(str);
  buffer.append("\n");
	
	
	return 1;
}	

//----------------------------------------------------------------------------
//PrintSelf
void vtkHM3DGeneralInfoWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM3DGeneralInfoWriter::BuildNullElements(vtkIntArray *array, vtkIntArray *elementMat)
{
	vtkIntArray *list = NULL;
	if ( array )
		{
		int n=0;
		list = vtkIntArray::New();
		list->SetNumberOfComponents(3);
		
		for ( int i=0; i<elementMat->GetNumberOfTuples(); i++ )
			{
			if ( (array->GetValue(i) == -1) || (array->GetValue(i) == -3) || (array->GetValue(i) == -4) )
				{
				list->InsertNextTuple3(i, array->GetValue(i)*(-1), elementMat->GetValue(i));
				n++;
				}
			}
		list->SetNumberOfTuples(n);
		}
	
	return list;
}

//----------------------------------------------------------------------------
vtkIntArray *vtkHM3DGeneralInfoWriter::BuildNullTerminals(vtkIntArray *array, vtkIntArray *elementMat)
{
	vtkIntArray *list = NULL;
	if ( array )
		{
		int n=0;
		list = vtkIntArray::New();
		list->SetNumberOfComponents(3);
		for ( int i=0; i<elementMat->GetNumberOfTuples(); i++ )
			{
			if ( array->GetValue(i) == -2 )
				{
				list->InsertNextTuple3(i, array->GetValue(i)*(-1), elementMat->GetValue(i));
				n++;
				}
			}
		list->SetNumberOfTuples(n);
		}
	
	return list;
}
