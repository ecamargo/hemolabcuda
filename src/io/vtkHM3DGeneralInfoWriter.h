/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHM3DGeneralInfoWriter.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM3DGeneralInfoWriter - 
// .SECTION Description
// vtkHM3DGeneralInfoWriter 
// .SECTION Caveats
// 
#ifndef vtkHM3DGeneralInfoWriter_H_
#define vtkHM3DGeneralInfoWriter_H_

#include "vtkDataWriter.h"
#include <string>
#include "vtkIntArray.h"
using namespace std;

class vtkHM3DFullModel;
class vtkDataObject;
class vtkIdList;


class VTK_EXPORT vtkHM3DGeneralInfoWriter : public vtkDataWriter
{
public:
  static vtkHM3DGeneralInfoWriter *New();
  vtkTypeRevisionMacro(vtkHM3DGeneralInfoWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // start the creation of 3d info file
  void Start3D(vtkHM3DFullModel *FullModel, char *dir);
  
  // start the creation of coupled model info file
  void StartCoupled(vtkIdList *ModelInfo, vtkIntArray *elementType, vtkIntArray *elementMat, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // close file
  void CloseFile(ostream *fp);
    
  // Description:
	// Write general information about 3D
	int Write3DData(ostream *fp, vtkHM3DFullModel *FullModel);
	
  // Description:
	// Write general information about Coupled model
	int WriteCoupledData(ostream *fp, vtkIdList *list, vtkIntArray *elementList, vtkIntArray *terminalList);
	
	// Description:
	// Build a list of the elements with id null.
	vtkIntArray *BuildNullElements(vtkIntArray *array, vtkIntArray *elementMat);
	
	// Description:
	// Build a list of the terminals with id null.
	vtkIntArray *BuildNullTerminals(vtkIntArray *array, vtkIntArray *elementMat);
  
protected:
  vtkHM3DGeneralInfoWriter() {};
  ~vtkHM3DGeneralInfoWriter() {};

  string	buffer;

};

#endif /*vtkHM3DGeneralInfoWriter_H_*/
