/*
 * $Id: vtkHM3DMeshReader.cxx 2402 2007-11-01 13:49:27Z eduardo $
 */
#include "vtkHM3DMeshReader.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"

#include "vtkHM1DMesh.h"

#include <fstream>
#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHM3DMeshReader, "$Revision: 2402 $");
vtkStandardNewMacro(vtkHM3DMeshReader);

vtkHM3DMeshReader::vtkHM3DMeshReader()
{
	vtkDebugMacro(<<"Reading mesh file...");
	this->Mesh = vtkHM1DMesh::New();
}

//----------------------------------------------------------------------------
vtkHM3DMeshReader::~vtkHM3DMeshReader()
{
	this->Mesh->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DMeshReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
	 os << indent << "Mesh "<< indent << *this->Mesh << "\n";
}

//----------------------------------------------------------------------------
//Open File
int vtkHM3DMeshReader::OpenFile()
{
	if (this->ReadFromInputString)
	{
		if (this->InputArray)
		{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
		}
		else if (this->InputString)
		{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
		}
	}
	else
	{
		vtkDebugMacro(<< "Opening Mesh file");
		
		if ( !this->FileName || (strlen(this->FileName) == 0))
		{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
		}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
			
		return 1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHM3DMeshReader::CloseFile()
{
	vtkDebugMacro(<<"Closing mesh file");
	if ( this->IS != NULL )
	{
		delete this->IS;
	}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Read Mesh File
int vtkHM3DMeshReader::ReadMeshFile(char *dir, int NumberOfSupSteps)
{	
//	char line[256];
	char diretorio[256];
	strcpy(diretorio, dir);
	
	vtkDebugMacro(<<"ReadMeshFile()...");

	this->SetFileName(strcat(diretorio,"Mesh.txt"));
	
	if ( !this->OpenFile()  || !this->ReadHeader() )
	{
		vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    	return 0;
	}
	
	if ( !this->ReadPoints() )
	{
		vtkErrorMacro(<<"Can't read points.");
		this->CloseFile ();
		return 0;
	}
	
	
	if ( !this->ReadElementGroups() )
	{
		vtkErrorMacro(<<"Can't read Element Groups.");
		this->CloseFile ();
		return 0;
	}
	
	if ( !this->ReadElementType() )
	{
		vtkErrorMacro(<<"Can't read element type.");
		this->CloseFile ();
		return 0;
	}
	
	if ( !this->ReadElementMat() )
	{
		vtkErrorMacro(<<"Can't read element mat.");
		this->CloseFile ();
		return 0;
	}
	
	while(!this->IS->eof())
	{
		if ( !this->ReadDirichletsConditionsTag(NumberOfSupSteps) )
		{
			vtkErrorMacro(<<"Can't read Dirichlets Conditions Tag.");
			this->CloseFile ();
			return 0;
		}
		
		if ( !this->ReadDirichletsConditions(NumberOfSupSteps) )
		{
			vtkErrorMacro(<<"Can't read Dirichlets Conditions.");
			this->CloseFile ();
			return 0;
		}
	}
	
	this->CloseFile();
		
	return 1;
}

//----------------------------------------------------------------------------
//Read Header
int vtkHM3DMeshReader::ReadHeader()
{
	char line[256];

	vtkDebugMacro(<< "Reading mesh file header");
	//
	// read header
	//
	
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*NODAL", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	this->ReadString(line);
	int df;
	if(!this->Read(&df))
	{
		vtkErrorMacro(<<"Can't read maximum number of degree of liberty!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->Mesh->SetDegreeOfFreedom(df);
	//read dimension of tree
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading dimension line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*DIMEN", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	int dimension;
	if(!this->Read(&dimension))
	{
		vtkErrorMacro(<<"Can't read number of dimension!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->Mesh->SetDimension(dimension);
	//read coordinates
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading coordinates line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*COORDINATES", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	int n;
	
	if(!this->Read(&n))
	{
		vtkErrorMacro(<<"Can't read number of cordinates!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->Mesh->SetNumberOfPoints(n);
	  
	return 1;
}

//----------------------------------------------------------------------------
// Read Points
int vtkHM3DMeshReader::ReadPoints()
{
	double x, y, z=0.0;

	vtkDebugMacro(<<"Reading mesh points data...");
	
	if ( this->Mesh->GetDimension() == 3 )
	{
		for ( int i=0; i<this->Mesh->GetNumberOfPoints(); i++ )
		{
			if ( !this->Read(&x) ||	!this->Read(&y) || !this->Read(&z) )
			{
				vtkErrorMacro(<<"Cannot read points data in line " << i+1 
							  << " of field 'points' for file: " 
							  << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}

			this->Mesh->SetPoints1D(x, y, z);		
		}

	}
	else if ( this->Mesh->GetDimension() == 2 )
	{
		for ( int i=0; i<this->Mesh->GetNumberOfPoints(); i++ )
		{
			if ( !this->Read(&x) ||	!this->Read(&y) )
			{
				vtkErrorMacro(<<"Cannot read points data in line " << i+1 
							  << " of field 'points' for file: " 
							  << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}

			this->Mesh->SetPoints1D(x, y, z);		
		}

	}
	
	return 1;
}

//----------------------------------------------------------------------------
// Read element group
int vtkHM3DMeshReader::ReadElementGroups()
{		
	char line[256];

	this->ReadLine(line);
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading element groups line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}

	if ( strncmp ("*ELEMENT", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		this->ReadLine(line);
		this->ReadString(line);
		if ( strncmp ("*ELEMENT", line, 20) )
		{
			vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
			this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		
			return 0;
		}
		return 0;
	}
	this->ReadString(line);
	int n;

	this->Read(&n);
	this->Read(&n);
	
	int elementGroups;
	if(!this->Read(&elementGroups))
	{
		vtkErrorMacro(<<"Can't read number of group elements!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->Mesh->SetNumberOfElementGroups(elementGroups);
	this->ReadString(line);
	
	vtkDebugMacro(<<"Reading Mesh element groups data...");
	
	int *elementForGroup = new int[this->Mesh->GetNumberOfElementGroups()];
	
	for ( int i=0; i<this->Mesh->GetNumberOfElementGroups(); i++ )
	{
		if ( !this->Read( &elementForGroup[i] ) )
		{
			vtkErrorMacro(<<"Cannot read number of elements for group " << " for file: " << (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	}
	this->Mesh->SetElementForGroup(elementForGroup);
	
	this->ReadString(line);
	int faceIndex;
	
	vtkHM1DMesh::VectorOfIntList groupElements;
	for ( int i=0; i<this->Mesh->GetNumberOfElementGroups(); i++ )
	{
		vtkHM1DMesh::ListOfInt lst;
		for ( int j=0; j<elementForGroup[i]; j++ )
		{
			if ( !this->Read( &faceIndex ) )
			{
				vtkErrorMacro(<<"Cannot read index " << j+1 << " in line " << i+1 << " of field 'element groups' for file: " << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}
			lst.push_back(faceIndex);			
			
		}
		groupElements.push_back(lst);
		
		lst.clear();
		
	}
	
	this->Mesh->SetGroupElements(&groupElements);
	
	return 1;
}

//-----------------------------------------------------------------------
//Read the types of elements
int vtkHM3DMeshReader::ReadElementType()
{
	char line[256];

	this->ReadLine(line);
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading element type line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	
	if ( strncmp ("*ELEMENT", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	this->ReadString(line);
	vtkDebugMacro(<<"Reading Mesh element type data...");
	
	
	int *elementType = new int[this->Mesh->GetNumberOfElementGroups()];  
	
	for ( int i=0; i<this->Mesh->GetNumberOfElementGroups(); i++ )
	{
		if ( !this->Read( &elementType[i] ) )
		{
			vtkErrorMacro(<<"Cannot read element type " << " for file: " << (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	}
	this->Mesh->SetElementType(elementType);
	
	return 1;
}

//-----------------------------------------------------------------------
//Read element mat
int vtkHM3DMeshReader::ReadElementMat()
{
	char line[256];

	this->ReadLine(line);
	if (!this->ReadString(line))
	{
		vtkErrorMacro(<<"Premature EOF reading element mat line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	
	if ( strncmp ("*ELEMENT", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	this->ReadString(line);
	
	vtkDebugMacro(<<"Reading Mesh element mat data...");
	
	int *elementMat = new int[this->Mesh->GetNumberOfElementGroups()];  
	
	for ( int i=0; i<this->Mesh->GetNumberOfElementGroups(); i++ )
	{
		if ( !this->Read( &elementMat[i] ) )
		{
			vtkErrorMacro(<<"Cannot read element mat " << 
			" for file: " << (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	}
	this->Mesh->SetElementMat(elementMat);
	
	return 1;
}

//------------------------------------------------------------------------
//Read tag of the dirichlets conditions
int vtkHM3DMeshReader::ReadDirichletsConditionsTag(int NumberOfSupSteps)
{
	char line[256];
	
	this->ReadLine(line);

	if(this->IS->eof())
		return 1;
	if (!this->ReadString(line))
	{
		if(this->IS->eof())
			return 1;

		vtkErrorMacro(<<"Premature EOF reading dirichlet conditions tag line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	
	if(this->Mesh->GetDegreeOfFreedom() == 7) // It`s a 3D file
	{		
		if ( strncmp ("*DIRICHLET", line, 20) )
		{
			vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
			                  << (this->FileName?this->FileName:"(Null FileName)"));
			    
			this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
			return 0;
		}
		this->ReadString(line);
		
		vtkDebugMacro(<<"Reading Mesh dirichlet conditions tag data...");
	
		vtkIntArray *dirichletsTag = vtkIntArray::New();
		int *n;
		n = new int[this->Mesh->GetDegreeOfFreedom()];
		
		dirichletsTag->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
		dirichletsTag->SetNumberOfTuples(this->Mesh->GetNumberOfPoints()*NumberOfSupSteps); // There are two substeps
		
		for ( int i=0; i<this->Mesh->GetNumberOfPoints()*NumberOfSupSteps; i++ ) // There are two substeps
		{
			for(int j=0; j<this->Mesh->GetDegreeOfFreedom(); j++)
			{
				if ( !this->Read( &n[j] ) )
				{
					vtkErrorMacro(<<"Cannot read dirichlet conditions tag " << 
					" for file: " << (this->FileName?this->FileName:"(Null FileName)"));
					return 0;
				}
			}
			dirichletsTag->SetTupleValue(i, n);
		}
		this->Mesh->SetDirichletsTag(dirichletsTag);
		
		dirichletsTag->Delete();
	}
	
	return 1;
}

//------------------------------------------------------------------------
//Read dirichlets conditions
int vtkHM3DMeshReader::ReadDirichletsConditions(int NumberOfSupSteps)
{
//	char line[256];
	
	vtkDebugMacro(<<"Reading Mesh dirichlet conditions data...");
	
	if(this->IS->eof())
		return 1;

	if( this->Mesh->GetDegreeOfFreedom() == 7 ) // It`s a 3D file
	{
		double *dirichlets = new double[this->Mesh->GetDegreeOfFreedom()]; 
		
		vtkDoubleArray *array = vtkDoubleArray::New();
		array->SetNumberOfComponents(this->Mesh->GetDegreeOfFreedom());
		array->SetNumberOfTuples(this->Mesh->GetNumberOfPoints()*NumberOfSupSteps); // There are two substeps

		for ( int i=0; i<this->Mesh->GetNumberOfPoints()*NumberOfSupSteps; i++ )	// There are two substeps
		{
			for(int j=0; j<this->Mesh->GetDegreeOfFreedom(); j++)
			{
				if ( !this->Read( &dirichlets[j] ) )
				{
					vtkErrorMacro(<<"Cannot read dirichlet conditions " << 
					" for file: " << (this->FileName?this->FileName:"(Null FileName)"));
					return 0;
				}
			}
			array->SetTuple(i, dirichlets);
		}
		this->Mesh->SetDirichlets(array);
		
		array->Delete();
	}
	
	return 1;
}

//----------------------------------------------------------------------------
vtkHM1DMesh *vtkHM3DMeshReader::GetMesh()
{
	return this->Mesh;
}

void vtkHM3DMeshReader::SetMesh(vtkHM1DMesh *m)
{
	this->Mesh = m;
}
