/*
 * $Id: vtkHM3DMeshReader.h 1330 2006-10-06 12:50:53Z igor $
 */

#ifndef VTKHM3DMESHREADER_H_
#define VTKHM3DMESHREADER_H_

#include "vtkPoints.h"
#include "vtkDataReader.h"

class vtkHM1DMesh;

#include <iostream>
	
class VTK_EXPORT vtkHM3DMeshReader : public vtkDataReader
{
public:
	static vtkHM3DMeshReader *New();
	vtkTypeRevisionMacro(vtkHM3DMeshReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	// Read the header of a Mesh data file. Returns 0 if error.	
	int	ReadHeader();
	
	// Description:
	// Receive full path of files and calls the other functions for read all file Mesh.
	int ReadMeshFile(char *dir, int NumberOfSupSteps);
		
	// Desccription:
	// Read points
	int ReadPoints();
			
	// Desccription:
	// Read element groups
	int ReadElementGroups();
	
	// Desccription:
	// Read types of elements of the 1D tree
	int ReadElementType();
	
	// Desccription:
	// Read data that indicate for each element which group
	// that has related in Param.txt
	int ReadElementMat();
	
	// Description:
	// Read which elements they will have dirichlet conditions
	int ReadDirichletsConditionsTag(int NumberOfSupSteps);
		
	// Description:
	// Read the parameters of the dirichlet conditions
	int ReadDirichletsConditions(int NumberOfSupSteps);
	
	// Description:
	// Set/Get vtkHM1DMesh
	vtkHM1DMesh *GetMesh();
	void SetMesh(vtkHM1DMesh *m);

protected:
	vtkHM3DMeshReader();
	~vtkHM3DMeshReader();
	
	vtkHM1DMesh *Mesh;
	
}; //End class

#endif /*VTKHM3DMESHREADER_H_*/
