/*
 * $Id: 
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DModelReader.cxx,v $

  Copyright (c) Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHM3DModelReader.h"
#include "vtkHM3DMeshReader.h"
#include "vtkHM1DMesh.h"
#include "vtkHM3DBasParamReader.h"
#include "vtkHM1DBasParam.h"
#include "vtkHMGeneralInfoReader.h"
#include "vtkHMGeneralInfo.h"
#include "vtkHMDataOutReader.h"
#include "vtkHMDataOut.h"

#include "vtkUnstructuredGrid.h"
#include "vtkHM3DModelGrid.h"
#include "vtkIdList.h"
#include "vtkCellType.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkExecutive.h"
#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkHM3DModelReader, "$Revision: 2355 $");
vtkStandardNewMacro(vtkHM3DModelReader);

//------------------------------------------------------------------
vtkHM3DModelReader::vtkHM3DModelReader()
{
	this->FileName = NULL;
  
  this->ExecutePiece = this->ExecuteNumberOfPieces = 0;
  this->ExecuteGhostLevel = 0;
  this->Selected3DObj = 0;
  this->NumberOf3DObj = 1;
  this->IsCoupleModel = 0;
  
	this->Mesh = NULL;
	this->MeshIdList = vtkIdList::New();
	this->BasParam = NULL;
	this->GeneralInfo = NULL;
	this->DataOut = NULL;
	
	vtkDebugMacro(<<"Reading Mesh, Param, IniFile, DataOut and BasParam file data...");
}

//------------------------------------------------------------------
vtkHM3DModelReader::~vtkHM3DModelReader()
{
	if(this->Mesh)
		this->Mesh->Delete();
	if(this->BasParam)
		this->BasParam->Delete();
	if(this->GeneralInfo)
			this->GeneralInfo->Delete();
	if(this->DataOut)
		this->DataOut->Delete();
	if(this->MeshIdList)
		this->MeshIdList->Delete();
}

//----------------------------------------------------------------------------
vtkHM3DModelGrid* vtkHM3DModelReader::GetOutput()
{
  return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkHM3DModelGrid* vtkHM3DModelReader::GetOutput(int idx)
{
  return vtkHM3DModelGrid::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetOutput(vtkDataObject *output)
{
  this->GetExecutive()->SetOutputData(0, output);
}

//----------------------------------------------------------------------------
int vtkHM3DModelReader::RequestUpdateExtent(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  int piece, numPieces, ghostLevel;

  piece = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  numPieces = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  ghostLevel = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS());
  
  // make sure piece is valid
  if (piece < 0 || piece >= numPieces)
    {
    return 1;
    }
  
  if (ghostLevel < 0)
    {
    return 1;
    }
  
  // Save the piece so execute can use this information.
  this->ExecutePiece = piece;
  this->ExecuteNumberOfPieces = numPieces;
  
  this->ExecuteGhostLevel = ghostLevel;

  return 1;
}

//----------------------------------------------------------------------------
int vtkHM3DModelReader::FillOutputPortInformation(int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkHM3DModelGrid");
  
  if(!this->ReadModelInfo())
  {
  	vtkErrorMacro(<<"Error reading ModelInfo.hml13d file");
  	return 0;
  }
  	
  return 1;
}

//------------------------------------------------------------------
void vtkHM3DModelReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::Modified()
{
	this->Superclass::Modified();
	this->InvokeEvent(vtkCommand::ModifiedEvent, this);
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetIsCoupleModel(int flag)
{
	this->IsCoupleModel = flag;
	this->Modified();
}

//----------------------------------------------------------------------------
int vtkHM3DModelReader::GetIsCoupleModel()
{
	return this->IsCoupleModel;
}

//------------------------------------------------------------------
void vtkHM3DModelReader::SetSelected3DObj(int i)
{
	this->Selected3DObj = i;	
	this->Modified();
}

//------------------------------------------------------------------
int vtkHM3DModelReader::GetSelected3DObj()
{
	return this->Selected3DObj;
}

//------------------------------------------------------------------
void vtkHM3DModelReader::SetNumberOf3DObj(int number)
{
	this->NumberOf3DObj = number;
	this->Modified();
}

//------------------------------------------------------------------
int vtkHM3DModelReader::GetNumberOf3DObj()
{
	return this->NumberOf3DObj;
}

//------------------------------------------------------------------
int vtkHM3DModelReader::ReadModelInfo()
{
	char str[256];
	
	strcpy(str, this->GetFileName());

	int n = strlen(str);

	//Test to verify if the directory has 
	// the bar in the end, in case that 
	// it does not have, adds 
	 
	while(str[n]!='/')
		n--;
	str[n+1]='\0';
	
	this->SetFileName(str);

	//Read file ModelInfo.hml13D
	if(!this->GeneralInfo)
	{
		vtkHMGeneralInfoReader *GIReader = vtkHMGeneralInfoReader::New();
		if (!GIReader->ReadGeneralInfoFile(this->GetFileName()))
			{
			//vtkErrorMacro(<<"Error reading ModelInfo file.");
			GIReader->CloseFile();
			GIReader->Delete();
			GIReader = NULL;
			}
		
		if(GIReader)
		{
		this->SetGeneralInfo(GIReader->GetGeneralInfo());
		this->GeneralInfo->Register(this);
		this->SetNumberOf3DObj(this->GeneralInfo->GetNumberOf3DObjects());
		this->SetIsCoupleModel(1);
		GIReader->Delete();
		}
	}

	return 1;
}

//------------------------------------------------------------------
int vtkHM3DModelReader::RequestData(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
	vtkDebugMacro(<<"Casting output to vtkHM3DModelGrid.");
	
	//get the info object
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the ouptut
	vtkHM3DModelGrid *output  = vtkHM3DModelGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	vtkHM3DModelGrid *grid = vtkHM3DModelGrid::New();	
		
	char str[256];
	
	strcpy(str, this->GetFileName());

	int n = strlen(str);

	//Test to verify if the directory has 
	// the bar in the end, in case that 
	// it does not have, adds 
	 
	while(str[n]!='/')
		n--;
	str[n+1]='\0';
	
	this->SetFileName(str);	
	
	//Read file Mesh.txt
	vtkHM3DMeshReader *MReader = vtkHM3DMeshReader::New();
	if(!this->Mesh)
	{		
		if(!this->GeneralInfo)
			{
			if (!MReader->ReadMeshFile( this->GetFileName(), 2 )) // SubSteps == 2 is a 3D Model
				{
				 vtkErrorMacro(<<"Error reading Mesh file.");
				 MReader->CloseFile();
				 MReader->Delete();
				 MReader = NULL;
				 return 0;
				}
			}
		else
			{
			if (!MReader->ReadMeshFile( this->GetFileName(), this->GeneralInfo->GetNumberOfSubSteps() )) // SubSteps >= 2 is a 1D-3D Model
				{
				 vtkErrorMacro(<<"Error reading Mesh file.");
				 MReader->CloseFile();
				 MReader->Delete();
				 MReader = NULL;
				 return 0;
				}	
			}
		this->SetMesh(MReader->GetMesh());
		this->Mesh->Register(this);
	}
	
	//Setting progress of the read
	float progress=this->GetProgress();
	this->SetProgress(progress + 0.05*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());
	MReader->Delete();


	//Read file BasParam.txt
	vtkHM3DBasParamReader *BPReader = vtkHM3DBasParamReader::New();
	if(!this->BasParam)
	{
		if (!BPReader->ReadBasParamFile(this->GetFileName(), this->Mesh))
			{
			vtkErrorMacro(<<"Error reading BasParam file.");
			BPReader->CloseFile();
			BPReader->Delete();
			BPReader = NULL;
			return 0;
			}
		this->SetBasParam(BPReader->GetBasParam());
		this->BasParam->Register(this);
	}
	
	//Setting progress of the read
	progress=this->GetProgress();
	this->SetProgress(progress + 0.06*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());
	BPReader->Delete();	

	// Alocando e populando estrutura de ids visitados
	this->visited = new VISITED[this->Mesh->GetNumberOfPoints()];	
	
	for(int i = 0; i < this->Mesh->GetNumberOfPoints(); i++)
		{
		this->MeshIdList->InsertNextId(-1);
		this->visited[i].newId = -1;
		this->visited[i].wasVisited = false;
		}
	
	// This part is used for select one object 3D (based in the *INCIDENCE tag of Mesh.txt and ModelInfo.hml13d), if there are more than one
	int initialVolumeCell = 0; // id da célula de volume inicial (referente a *INCIDENCE em Mesh.txt)
	int numberOfVolumeCells = 0; // qtd de células de volume

	int initialSurfaceCell = 0; // id da célula inicial da superfície (referente a *INCIDENCE em Mesh.txt)
	int numberOfSurfaceCells = 0; // qdt de células da superfície
	
	int initialCapsCell = 0; // id da célula inicial das tampas (referente a *INCIDENCE em Mesh.txt)
	int numberOfCapsCells = 0; // qdt de células das tampas

	if(this->GeneralInfo)
		{
		// Verifica se this->Selected3DObj é um valor entre 0 (zero) e total de objetos
		if( (this->Selected3DObj >= this->GeneralInfo->GetNumberOf3DObjects()) || (this->Selected3DObj < 0) )
			this->Selected3DObj = (this->GeneralInfo->GetNumberOf3DObjects()-1); 
				
		if(this->Selected3DObj == 0)
			{
			initialVolumeCell = this->GeneralInfo->GetNumberOf1DCells();	
			initialSurfaceCell = this->GeneralInfo->GetNumberOf1DCells();
			initialCapsCell = this->GeneralInfo->GetNumberOf1DCells();
			
			// Buscando a célula inicial de superfície e das tampas
			for(int i = 0; i < this->GeneralInfo->GetNumberOf3DObjects(); i++)
				{	
				initialSurfaceCell += this->GeneralInfo->GetNumberOfVolumeCells(i);
				initialCapsCell += this->GeneralInfo->GetNumberOfVolumeCells(i) + this->GeneralInfo->GetNumberOfShellCells(i);
				}		
			}
		else
			{
			// Posicionando na primeira célula volume da tag (*INCIDENCE)
			initialVolumeCell = this->GeneralInfo->GetNumberOf1DCells();
			initialSurfaceCell = this->GeneralInfo->GetNumberOf1DCells();
			initialCapsCell = this->GeneralInfo->GetNumberOf1DCells();
			
			// Posicionando na primeira célula superfície e das tampas da tag (*INCIDENCE)
			for(int i = 0; i < this->GeneralInfo->GetNumberOf3DObjects(); i++)
				{	
				initialSurfaceCell += this->GeneralInfo->GetNumberOfVolumeCells(i);
				initialCapsCell += this->GeneralInfo->GetNumberOfVolumeCells(i) + this->GeneralInfo->GetNumberOfShellCells(i);
				}
			
			// Buscando as células iniciais do volume, superfície e tampas do objeto selecionado
			for(int i = 0; i < this->Selected3DObj; i++)
				{
				initialVolumeCell += this->GeneralInfo->GetNumberOfVolumeCells(i);
				initialSurfaceCell += this->GeneralInfo->GetNumberOfShellCells(i);
				initialCapsCell += this->GeneralInfo->GetNumberOfCapsCells(i);
				}	
			}
		numberOfVolumeCells = this->GeneralInfo->GetNumberOfVolumeCells(this->Selected3DObj);
		numberOfSurfaceCells = this->GeneralInfo->GetNumberOfShellCells(this->Selected3DObj);
		numberOfCapsCells = this->GeneralInfo->GetNumberOfCapsCells(this->Selected3DObj);

		this->BuildGrid( grid, initialVolumeCell, numberOfVolumeCells ); //Devo informar a célula inicial do objeto e total de células		
		//Setting progress
		progress=this->GetProgress();
		this->SetProgress(progress + 0.06*(1.0 - progress));
		this->UpdateProgress(this->GetProgress());
		
		this->BuildGrid( grid, initialSurfaceCell, numberOfSurfaceCells ); //Devo informar a célula inicial do objeto e total de células
		//Setting progress
		progress=this->GetProgress();
		this->SetProgress(progress + 0.06*(1.0 - progress));
		this->UpdateProgress(this->GetProgress());
		
		this->BuildGrid( grid, initialCapsCell, numberOfCapsCells ); //Devo informar a célula inicial do objeto e total de células
		//Setting progress
		progress=this->GetProgress();
		this->SetProgress(progress + 0.06*(1.0 - progress));
		this->UpdateProgress(this->GetProgress());
		}
	else
		{
		numberOfVolumeCells = this->Mesh->GetNumberOfElementGroups();
		this->BuildGrid( grid, initialVolumeCell, numberOfVolumeCells ); //Devo informar a célula inicial do objeto e total de células
		//Setting progress
		progress=this->GetProgress();
		this->SetProgress(progress + 0.06*(1.0 - progress));
		this->UpdateProgress(this->GetProgress());
		}

	grid->Update();	

	// Move the virtual points for caps center 
	for(int i = 0; i < grid->GetVirtualPointsOfCaps()->GetNumberOfIds(); i++)
		{
		double p[3];
		int pointId = grid->GetVirtualPointsOfCaps()->GetId(i);
		this->BuildVirtualPointOfCap(pointId, p); // Muda X Y Z do ponta para uma possição sobre a tampa
		this->Mesh->GetPoints()->SetPoint( pointId, p	); // Seta a nova coordenada no MESH para evitar inconsistência nos dados
		grid->GetPoints()->SetPoint( this->visited[pointId].newId, p	); // Seta a nova coordenada no GRID
		}
	grid->SetMeshId(this->MeshIdList);
	grid->Update();	
	
	//Setting progress
	progress=this->GetProgress();
	this->SetProgress(progress + 0.07*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());
	

	//Read file DataOut.txt
	vtkHMDataOutReader *DOReader = vtkHMDataOutReader::New();
	if(!this->DataOut)
	{
		char file[256];
		sprintf(file, "%s%s", this->GetFileName(), "DataOut.txt");
		
		if (!DOReader->ReadDataOut(file, this->Mesh, this->BasParam, this))
			{
			// comentado por Paulo Ziemer pois a maioria dos modelos 1D sao abertos sem dados
			//vtkWarningMacro(<<"Error reading DataOut file or DataOut.txt not exists.");
			DOReader->CloseFile();
			DOReader->Delete();
			DOReader = NULL;
			}
		
		if(DOReader)
			{
			this->SetDataOut(DOReader->GetDataOut());
			this->DataOut->Register(this);
			
			vtkDoubleArray *time = this->DataOut->GetInstantOfTime();
			grid->SetInstantOfTime(time);
			
			grid->SetNumberOfBlocks(this->DataOut->GetNumberOfBlock());
			
			vtkDoubleArray *DegreeOfFreedom = this->DataOut->GetDegreeOfFreedomArray();
			grid->SetDegreeOfFreedom(DegreeOfFreedom);			
			}
	}
	DOReader->Delete();
	DOReader = NULL;	

	
	//Setting progress
	progress=this->GetProgress();
	this->SetProgress(progress + 0.08*(1.0 - progress));
	this->UpdateProgress(this->GetProgress());
	
		
	delete[] this->visited;
	grid->Update();
	output->DeepCopy(grid);
	
	grid->Delete();	
	
	return 1;
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::BuildVirtualPointOfCap(int virtualId, double p[3])
{
	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX
	
  VectorOfIntList *v;
  ListOfInt lst;
	ListOfInt::iterator ir;
	
	vtkIdList *idList = vtkIdList::New(); 
	vtkHM3DModelGrid *grid = vtkHM3DModelGrid::New(); 
	vtkPoints *points =	vtkPoints::New(); 
	
	v = this->Mesh->GetGroupElements();
	int *MeshType, *BasParamType, idBasParamType;

	for(int i = 0; i < this->Mesh->GetNumberOfElementGroups(); i++)
		{
		lst = v->at(i);
		ir = lst.begin();
						
		while( ir != lst.end() )
			{
			idList->InsertNextId(*ir-1);
			ir++;
			}
					
		if(idList->GetId(0) == virtualId)
			{
			MeshType = this->Mesh->GetElementType();
			BasParamType = this->BasParam->GetElementType();
			idBasParamType = BasParamType [ (MeshType[i]*5)-5 ];
			
			if(	(idList->GetNumberOfIds() == 4 ) && (idBasParamType == 54 || idBasParamType == 540 || idBasParamType == 541 || idBasParamType == 542 ) )
				{
				points->InsertNextPoint(this->Mesh->GetPoints()->GetPoint(idList->GetId(1)));
				points->InsertNextPoint(this->Mesh->GetPoints()->GetPoint(idList->GetId(2)));
				points->InsertNextPoint(this->Mesh->GetPoints()->GetPoint(idList->GetId(3)));
				idList->DeleteId( idList->GetId(0) );
				grid->InsertNextCell(VTK_TRIANGLE, idList);
				}
			}
		idList->Reset();
		}
	grid->SetPoints(points);
	grid->Update();
	grid->GetCenter(p);
	
	idList->Delete();
	points->Delete();
	grid->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::BuildGrid(vtkUnstructuredGrid *output, int initialCell, int NumberOfCells)
{
	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX
	
  VectorOfIntList *v;
  ListOfInt lst;
	ListOfInt::iterator ir;
	
	vtkIdList *idList = vtkIdList::New(); 
	vtkIdList *idListTemp = vtkIdList::New(); 
	vtkHM3DModelGrid *grid = vtkHM3DModelGrid::New(); 
	grid->DeepCopy(output);
		
	int lastId = -1;
	v = this->Mesh->GetGroupElements();
	int *MeshType, *BasParamType, idBasParamType;
	int MeshId = -1;
	int NewId = -1;
		
	// Se desloco o início para o primeiro ponto de volume, devo somar o valor do deslocamento
	// na quantidade de células do objeto para manter o total de células a serem lidas
	// Ex: 
	//	Objeto 3D inicia com célula 0 até 09, temos 10 células;
	//	Se existisse um objeto 1D de 15 (0 a 14) células antes do objeto 3D teremos o seguinte deslocamento:
	//	Objeto 3D inicia em 15 e termina em 24 (09 + 15), temos 10 células
	for(int i = initialCell; i < (NumberOfCells + initialCell); i++)
		{
		lst = v->at(i);
		ir = lst.begin();
		
		while( ir != lst.end() )
			{
			idList->InsertNextId(*ir-1);
			ir++;
			}
		
		MeshType = this->Mesh->GetElementType();
		BasParamType = this->BasParam->GetElementType();
		idBasParamType = BasParamType [ (MeshType[i]*5)-5 ];
		
		if(idList->GetNumberOfIds() == 4)	// Se for tetraedroinitialCell
			{
			if( idBasParamType == 54 || idBasParamType == 540 || idBasParamType == 541 || idBasParamType == 542 ) //Se for tampa
				{
//				cout << ":::Tampa" <<endl;
				for (int z = 0; z < 4; z++)
					{
					MeshId = idList->GetId(z);
					
					if( !this->visited[MeshId].wasVisited )
						{			
						NewId = grid->GetPoints()->InsertNextPoint( this->Mesh->GetPoints()->GetPoint(MeshId) );
						idListTemp->InsertNextId(NewId);
						this->MeshIdList->SetId( NewId, MeshId );
						this->visited[MeshId].wasVisited = true;
						this->visited[MeshId].newId = NewId;
						}
					else
						{
						idListTemp->InsertNextId( this->visited[MeshId].newId ); 
						}
					}
				if(lastId != idList->GetId(0)) // Se for uma tampa diferente
					// Popula o atributo virtualPointsOfCaps com os ID's (antigos) do pontos virtuais das tampas
					grid->GetVirtualPointsOfCaps()->InsertNextId( idList->GetId(0) );
				
				lastId = idList->GetId(0);				
				idListTemp->DeleteId( idListTemp->GetId(0) );				
				grid->InsertNextCell(VTK_TRIANGLE, idListTemp);
				}
			else if( idBasParamType == 524 || idBasParamType == 525 || idBasParamType == 5245 ) 	//Se for volume
				{
//				cout << ":::Volume" <<endl;
				for (int z = 0; z < 4; z++)
					{
					MeshId = idList->GetId(z);
					
					if( !this->visited[MeshId].wasVisited )
						{			
						NewId = grid->GetPoints()->InsertNextPoint( this->Mesh->GetPoints()->GetPoint(MeshId) );
						idListTemp->InsertNextId(NewId);
						this->MeshIdList->SetId( NewId, MeshId );
						this->visited[MeshId].wasVisited = true;
						this->visited[MeshId].newId = NewId;
						}
					else
						{
						idListTemp->InsertNextId( this->visited[MeshId].newId ); 
						}
					}
				grid->InsertNextCell(VTK_TETRA, idListTemp);
				}
			}
		else if(idList->GetNumberOfIds() == 3) // Se for triangulo
			{
		  if( idBasParamType == 57 || idBasParamType == 5705 ) // Se for superfície
		  	{
//		  	cout << ":::Superfície" <<endl;		
				for (int z = 0; z < 3; z++)
					{
					MeshId = idList->GetId(z);
					
					if( !this->visited[MeshId].wasVisited )
						{			
						NewId = grid->GetPoints()->InsertNextPoint( this->Mesh->GetPoints()->GetPoint(MeshId) );
						idListTemp->InsertNextId(NewId);
						this->MeshIdList->SetId( NewId, MeshId );
						this->visited[MeshId].wasVisited = true;
						this->visited[MeshId].newId = NewId;
						}
					else
						{
						idListTemp->InsertNextId( this->visited[MeshId].newId ); 
						}
					}				
				grid->InsertNextCell(VTK_TRIANGLE, idListTemp);
		  	}
			}	
		idListTemp->Reset();
		idList->Reset();
		}
	
	grid->Update();			
	output->DeepCopy(grid);
	
	grid->Delete();
	idList->Delete();	
	idListTemp->Delete();
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetMesh(vtkHM1DMesh *mesh)
{
	this->Mesh = mesh;
}

//----------------------------------------------------------------------------
vtkHM1DMesh *vtkHM3DModelReader::GetMesh()
{
	return this->Mesh;
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetGeneralInfo(vtkHMGeneralInfo *GInfo)
{
	this->GeneralInfo = GInfo;
}

//----------------------------------------------------------------------------
vtkHMGeneralInfo* vtkHM3DModelReader::GetGeneralInfo()
{
	return this->GeneralInfo;
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetBasParam(vtkHM1DBasParam *BP)
{
	this->BasParam = BP;
}

//----------------------------------------------------------------------------
vtkHM1DBasParam *vtkHM3DModelReader::GetBasParam()
{
	return this->BasParam;
}

//----------------------------------------------------------------------------
void vtkHM3DModelReader::SetDataOut(vtkHMDataOut *DO)
{
	this->DataOut = DO;
}

//------------------------------------------------------------------
vtkHMDataOut *vtkHM3DModelReader::GetDataOut()
{
	return this->DataOut;
}
