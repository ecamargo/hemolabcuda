/*
 * $Id: 
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DModelReader.h,v $

  Copyright (c) Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkHM3DModelReader - read ModelInfo, Mesh, BasParam 
// and DataOut data file and generate an vtkHM3DModelGrid
// .SECTION Description
// vtkHM3DModelReader is a source object that reads ASCII or binary 
// data files (see http://hemo01a.lncc.br/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional for format details).
// The output of this reader is a single vtkHM3DModelGrid data object.
// The superclass of this class, vtkDataReader, provides many methods for
// controlling the reading of the data file, see vtkDataReader for more
// information.
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.
// .SECTION See Also
// vtkPolyData vtkDataReader vtkHM3DModelGrid 

#ifndef VTKHM3DMODELREADER_H_
#define VTKHM3DMODELREADER_H_

#include "vtkDataReader.h"
#include "vtkUnstructuredGrid.h"
#include "vtkHM1DMesh.h"
#include "vtkHM3DModelGrid.h"


class vtkHM3DMeshReader;
class vtkHM3DBasParamReader;
class vtkHMGeneralInfo;
class vtkHMDataOutReader;
class vtkHM1DBasParam;
class vtkHMDataOut;

// BTX
typedef struct VISITED
{
	int newId;
	bool wasVisited;
};
// ETX

class VTK_EXPORT vtkHM3DModelReader : public vtkDataReader
{
public:
	static vtkHM3DModelReader *New();
	vtkTypeRevisionMacro(vtkHM3DModelReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
		
	// Description:
	// Get the output of this reader.
  vtkHM3DModelGrid *GetOutput();
  vtkHM3DModelGrid *GetOutput(int idx);
	void SetOutput(vtkDataObject *output);
 	
	// Description :
	// Set/Get MeshReader
	void SetMesh(vtkHM1DMesh *mesh);	
	vtkHM1DMesh *GetMesh();
	
	// Description :
	// Set/Get BasParamReader
	void SetBasParam(vtkHM1DBasParam *BP);	
	vtkHM1DBasParam *GetBasParam();
	
	// Description :
	// Set/Get ModelInfo.hml13d
	void SetGeneralInfo(vtkHMGeneralInfo *GInfo);	
	vtkHMGeneralInfo *GetGeneralInfo();
		
	// Description :
	// Set/Get DataOutReader
	void SetDataOut(vtkHMDataOut *DO);	
	vtkHMDataOut *GetDataOut();
	
	// Description :
	// Build vtkUnstructuredGrid
	void BuildGrid(vtkUnstructuredGrid *output, int initialCell, int NumberOfCells);
	
	// Description :
	// Build cover of vtkUnstructuredGrid and computes and returns your center
	void BuildVirtualPointOfCap(int virtualId, double p[3]);
	
	// Description :
	// Selects the number of object for visualization  
	void SetSelected3DObj(int i);
	int GetSelected3DObj();
	
	// Description :
	// Set/Get the number of 3D objects 
	int GetNumberOf3DObj();
	void SetNumberOf3DObj(int number);
	
	// Description :
	// Set/Get if it is CoupleModel or 3D Model 
	int GetIsCoupleModel();
	void SetIsCoupleModel(int flag);
	
protected:
	vtkHM3DModelReader();
	~vtkHM3DModelReader();
	virtual void Modified();
	virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);
  
	// Description:
	// Update extent of PolyData is specified in pieces.  
	// Since all DataObjects should be able to set UpdateExent as pieces,
	// just copy output->UpdateExtent  all Inputs.
	virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **,
                                  vtkInformationVector *);
  
	virtual int FillOutputPortInformation(int, vtkInformation*);
	
	VISITED* visited;
	
	vtkIdList *MeshIdList;
	
	// Description :
	// Read ModelInfo.hml13d 
	int ReadModelInfo();
  
	// Description :
	// If it is CoupleModel or 3D Model 
  int IsCoupleModel;
   
	// Description:
	// Used for selects the object for visualization
  int Selected3DObj;
  
	// Description:
	// Number of 3D Objects
  int NumberOf3DObj;
                               
	// Description:
	// Used by streaming: The extent of the output being processed
	// by the execute method. Set in the ComputeInputUpdateExtents method.
	int ExecutePiece;
  int ExecuteNumberOfPieces;
	int ExecuteGhostLevel;
	
	// Description:
	//Object for read data of the Mesh.txt file
	vtkHM1DMesh *Mesh;
	
	// Description:
	//Object for read data of the BasParam.txt file
	vtkHM1DBasParam *BasParam;
	
	// Description:
	//Object for read data of the BasParam.txt file
	vtkHMGeneralInfo *GeneralInfo;
	
	// Description:
	//Object for read data of the DataOut.txt file
	vtkHMDataOut *DataOut;	
};

#endif

