/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DParticleTracingWriter.h,v $

  Copyright (c) Rodrigo Luis de Souza da Silva and Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHM3DParticleTracingWriter.h"

#include "vtkToolkits.h" // for VTK_USE_PARALLEL
#ifdef VTK_USE_PARALLEL
# include "vtkMultiProcessController.h"
#endif

#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDataSet.h"
#include "vtkDoubleArray.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkHM3DModelGrid.h"
#include "vtkPoints.h"

#include <algorithm>
#include "stdio.h"
#include <vtkstd/vector>
#include <vtkstd/list>
#include <vtkstd/map>
#include "sys/time.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkHM3DParticleTracingWriter);
vtkCxxRevisionMacro(vtkHM3DParticleTracingWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
vtkHM3DParticleTracingWriter::vtkHM3DParticleTracingWriter()
{
  this->FileName = NULL;
  this->InitialPositionOfParticles = NULL;
	this->NumberOfParticles = 0;
	this->Delta = 0.005f;
	this->NumberOfRepeatTimeSteps = 1;
	this->IsRepeatPulse = false;

  //METACELLS
  this->distMeta = 0;

  //PARTICLES
  error = 0;			// error code
	currentMetacell = 0;// Current Metacell in memory
	currentTime = 0;	// Current Time of calculation
	fasterMode = false; // Copy all metacells in memory
	IsMetacellsInMemory = false; // Check if the metacells files are already in memory
}

//----------------------------------------------------------------------------
vtkHM3DParticleTracingWriter::~vtkHM3DParticleTracingWriter()
{
  this->SetFileName(NULL);

	if( this->InitialPositionOfParticles )
		this->InitialPositionOfParticles->Delete();

	delete [] this->LastPositions;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "FileName: "
    << (this->FileName ? this->FileName : "(none)") << "\n";
  os << indent << "Path: "
    << (this->Path ? this->Path : "(none)") << "\n";
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::WriteMetaCells()
{
	vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );

//********************** METACELLS CREATION - BEGIN ************************************
  int i;					// General counter
	this->distMeta = 0;		// Distancia da maior aresta (a ser utilizada no particleTracing.cpp)
	int numPoints = input->GetNumberOfPoints();
	int numCells = input->GetNumberOfCells();

	char indexName[512];	// Index File Name

	point	*pointList;		// Point List
	cell	*cellList;		// Cell List
	HMsort	*sortList;		// Auxiliar list to help sorting data

	pointList = new point[numPoints];
	for(i=0; i < numPoints; i++)
		pointList[i].pointId = -1;

	double p[3];
	for(i=0; i < numPoints; i++)
	{
		input->GetPoints()->GetPoint(i, p);
		pointList[i].pointId = i;
		pointList[i].x = p[0];
		pointList[i].y = p[1];
		pointList[i].z = p[2];
	}

	cellList = new cell[numCells];
	for(i=0; i < numCells; i++)
		cellList[i].cellId = -1;

	vtkIdList *list = vtkIdList::New();
	for(i=0; i < numCells; i++)
	{
		input->GetCellPoints(i, list);
		if( list->GetNumberOfIds() == 4 )
		{
			cellList[i].cellId = i;
			cellList[i].v[0] = list->GetId(0);
			cellList[i].v[1] = list->GetId(1);
			cellList[i].v[2] = list->GetId(2);
			cellList[i].v[3] = list->GetId(3);
		}
		list->Reset();

		this->setMaximumDistance(&this->distMeta, pointList, cellList[i].v[0], cellList[i].v[1], cellList[i].v[2], cellList[i].v[3]);
	}
	list->Delete();

	/* Computing Center Point  of each Cell */
//		printf("\nComputing Center Point of each Cell...");
		this->computingPointCenter(cellList, pointList, numCells);
//		printf("\t\t[ OK ]");

		/* Sorting the cells by the center points and Write the Metacells */
//		printf("\nSorting Data...");
		sortList = new HMsort[numCells];

		// Sorting the 'x' axis
		this->copyStructureX(sortList, cellList, 0, numCells);
		this->quicksort(sortList, 0, numCells-1);

		// Sorting the 'y' axis after first split (2 pieces)
		this->copyStructureY(sortList, cellList, 0, numCells);
		this->quicksort(sortList, 0, numCells/2);
		this->quicksort(sortList, (numCells/2) + 1, numCells-1);


		// Sorting the 'z' axis after second split (4 pieces)
		int n = numCells / 4;
		this->copyStructureZ(sortList, cellList, 0, numCells);
		this->quicksort(sortList,		   0,		n);
		this->quicksort(sortList,	   n + 1,	  2*n);
		this->quicksort(sortList,	 2*n + 1,     3*n);
		this->quicksort(sortList,  3*n + 1, 4*n - 1);
//		printf("\t\t\t\t\t[ OK ]");

		// Set index Name
		strcpy(indexName, this->Path);
		strcat(indexName, "_index.txt");

		// Delete the old File to build a new one
//		remove(indexName);
		this->writeMetaCell(sortList, cellList, pointList, numCells,			    0,           n/2, 1);
		this->writeMetaCell(sortList, cellList, pointList, numCells,		(n/2) + 1,             n, 2);
		this->writeMetaCell(sortList, cellList, pointList, numCells,		    n + 1, (n + 1) + n/2, 3);
		this->writeMetaCell(sortList, cellList, pointList, numCells,	(n + 2) + n/2,            2*n, 4);
		this->writeMetaCell(sortList, cellList, pointList, numCells,		  2*n + 1, 2*n + 1  + n/2, 5);
		this->writeMetaCell(sortList, cellList, pointList, numCells,	2*n + 2 + n/2,		  	  3*n, 6);
		this->writeMetaCell(sortList, cellList, pointList, numCells,		  3*n + 1,  3*n + 1 + n/2, 7);
		this->writeMetaCell(sortList, cellList, pointList, numCells,	3*n + 2 + n/2,		  4*n - 1, 8);

		// Writing Maximum distance between two vertices
		FILE *findex = fopen(indexName, "a+");
		fprintf(findex, "MaxDistance %f", this->distMeta);
		fprintf(findex, "\nVelocityVectorPosition:");
		for(i = 1; i <= 8; i++) fprintf(findex, "\n%d", this->localPosition[i]);
		fclose(findex);

		// Cell Intervals for each MetaCell //
//		printf("\n\nCell Intervals for each MetaCell:\n");
//		printf("\n%5d .. %5d",     0,           n/2, 1);
//		printf("\n%5d .. %5d",	(n/2) + 1,             n, 2);
//		printf("\n%5d .. %5d",	    n + 1, (n + 1) + n/2, 3);
//		printf("\n%5d .. %5d",	(n + 2) + n/2,            2*n, 4);
//		printf("\n%5d .. %5d",	  2*n + 1, 2*n + 1  + n/2, 5);
//		printf("\n%5d .. %5d",	2*n + 2 + n/2,		  	  3*n, 6);
//		printf("\n%5d .. %5d",	  3*n + 1,  3*n + 1 + n/2, 7);
//		printf("\n%5d .. %5d",	3*n + 2 + n/2,		  4*n - 1, 8);


		/* Reading Vector Field */
//		printf("\nAppending vector fields to Metacell files...\n");

		//int metaNumber, time, pos, meshId;
		int metaNumber, time, meshId;
		int metaNumPoints;
		//char metaName[256], aux1[30],aux2[30],aux3[30];
		char metaName[512],aux2[30];
		double vector[7];
		vtkDoubleArray *degreeOfFreedom = input->GetDegreeOfFreedom();

		int * binaryList;
		binaryList = new int[numPoints];
		this->timeSteps = input->GetNumberOfBlocks();

		for(metaNumber=1; metaNumber <= 8; metaNumber++) // Number of Metacells Files
		{
			this->setMetaCellFileName(metaName, metaNumber);
			FILE *fmeta = fopen(metaName, "a+");

			metaNumPoints = this->getNumPoinsFromFile(metaNumber);

//			printf("\nAppending vector fields data to file %s\n", metaName);
			this->setBinaryList(metaName, binaryList, numPoints, metaNumPoints);

			for(time = 0; time < this->timeSteps; time++) // Number of Times in the Main File
			{
				for (i = -1; i < numPoints; i++)	// Number of Points in each Time
				{
					if(i == -1)
					{
						sprintf(aux2, "%d%s", time, "Velocity/Pressure");
						fprintf(fmeta, "\n%s\n", aux2);
					}
					else
					{
						meshId = input->GetMeshId()->GetId( pointList[i].pointId );
						meshId = meshId + (input->GetNumberOfPoints() * time);
						degreeOfFreedom->GetTuple(meshId, vector);

						if(binaryList[i])
							{
							fprintf(fmeta, "%d %f %f %f %f\n",i, vector[0], vector[1],vector[2], vector[3]);
							}
					}
				}

			}
			fclose(fmeta);
		}

		delete [] binaryList;
		delete [] cellList;
		delete [] pointList;
		delete [] sortList;

//********************** METACELLS CREATION - END ************************************
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::WriteParticles()
{
	vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );

	//********************** PARTICLES CREATION - BEGIN ************************************
	int metaVec[9];

//	printf("\nPARTICLE TRACING");
//	printf("\n================\n\n");

	this->totalPoints = input->GetNumberOfPoints();
	this->timeSteps = input->GetNumberOfBlocks();

	// Divide-se por 4 para alocar praticamente o dobro do tamanho necessário
	this->maxMetaPoints = this->totalPoints / 4;

	// Initializing global List that loads all times for one metacell (not indexed)
	this->vecList = new point[this->timeSteps * this->maxMetaPoints];

	// Initializing global List that loads one time indexed (for search purposes)
	this->vecListSearch = new point[this->totalPoints];

	this->setMaximumDistancePart();

	int index = 1;
	this->startTime = 0;


//	printf("\nTempo de processamento em CPU:  ");
	// Usado para medição do tempo de processamento
  double ti,tf,tempo; // ti = tempo inicial // tf = tempo final
  ti = tf = tempo = 0;
  timeval tempo_inicio,tempo_fim;
  //inicializa contadores de tempo
  gettimeofday(&tempo_inicio,NULL);


	for(int j = 1; j <= this->NumberOfRepeatTimeSteps; j++)
	{
//		cout << "Cycle: " << j << " of " << this->NumberOfRepeatTimeSteps <<endl;

		for(int i = 0; i < this->NumberOfParticles; i++)
		{
//			printf("\tStart processing particle%03d.txt from time %d to %d\n", i+index, this->startTime, this->timeSteps-1);

			if(j==1)
				this->callMultiPart(i);
			else
				this->LoadLastPosition(i);

			this->currentMetacell = 0;
			this->currentTime = 0;
			this->partNumber = index+i;

			if(j==1)
				this->writeTimeInFile(this->startTime);

			this->computeTrace(metaVec);
		}

		this->IsRepeatPulse = true;
	}

//	printf("\n");

  //finaliza a contagem de tempo e imprime o tempo decorrido
  gettimeofday(&tempo_fim,NULL);
  tf = (double)tempo_fim.tv_usec + ((double)tempo_fim.tv_sec * (1000000.0));
  ti = (double)tempo_inicio.tv_usec + ((double)tempo_inicio.tv_sec * (1000000.0));
  tempo = (tf - ti) / 1000;
  printf("%.5f ",tempo); // Se quiser o tempo em segundos é só retirar o /1000 depois de (tf - ti).


	delete [] this->vecListSearch;
	delete [] this->vecList;
	this->WriteConfigFile();

	//********************** PARTICLES CREATION - END **************************************
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::SetPath(char* dir)
{
	sprintf(this->Path, "%s", dir);
}

//----------------------------------------------------------------------------
char* vtkHM3DParticleTracingWriter::GetPath()
{
	return this->Path;
}

//********************** PARTICLES VIEWER - BEGIN **************************************
//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::GetPositionOfParticlesInAllTimeSteps(point **vec2)
{
	int i, n, initialTime, fileTime, id;
	char caux[256], particleName[256];
	double bound[6];

	int steps = this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps * 2; // multiplicado por 2 para alocar o dobro do espaço necessário

	*vec2 = new point[steps];
	point *vec = *vec2;

	vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );
	input->GetBounds(bound);

	// Put particles out of bounds
	for(i = 0; i < steps; i++)
	{
		vec[i].x = (bound[1] - bound[0]) * 4; // (xMax - xMin) * 4
		vec[i].y = (bound[3] - bound[2]) * 4; // (yMax - yMin) * 4
		vec[i].z = (bound[5] - bound[4]) * 4; // (zMax - zMin) * 4

		vec[i].velocityX = 0.0;
		vec[i].velocityY = 0.0;
		vec[i].velocityZ = 0.0;
		vec[i].pressure  = 0.0;
		vec[i].time  = 0.0;
	}

	FILE *fp;
	// Read multiple files and load the result on a single vector
	for(n = 1; n <= this->NumberOfParticles; n++)
	{
		this->setParticleViewerFileName(particleName, n);
		fp = fopen(particleName, "r");
		if(!fp)
		{
			printf("\nError Reading File!!!\n\n");
			exit(1);
		}

		fscanf(fp, "%s %d", caux, &initialTime);
		fileTime = this->getNumberOfTimes(particleName);
		fileTime += initialTime;

//		if ( fileTime > (this->timeSteps * this->NumberOfRepeatTimeSteps) )
//			fileTime = (this->timeSteps * this->NumberOfRepeatTimeSteps);

//		for(i = 0; i < fileTime+1; i++)
		for(i = 0; i < fileTime-1; i++)
		{
			if(i >= initialTime)
			{
				id = i + (n-1) * this->timeSteps * this->NumberOfRepeatTimeSteps;

				if(id >= steps) //teste para segmentation fault
					cout << "id: " << id << "\tmp: " << steps <<endl;

				fscanf(fp, "%f %f %f %f %f %f %f %f", &vec[id].x, &vec[id].y, &vec[id].z, &vec[id].velocityX, &vec[id].velocityY,
																	&vec[id].velocityZ, &vec[id].pressure, &vec[id].time);
			}
		}
		fclose(fp);
	}
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriter::getNumberOfTimes(char *file)
{
	char c;
	int count = 0;
	FILE *fcount;
	fcount = fopen(file, "r");

	while((c=getc(fcount))!=EOF)
		if(c == '\n') count++;

	return count;
}

// Set Particle File Name
//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::setParticleViewerFileName(char *nome, int n)
{
	char num[6];
	sprintf(num, "%03d", n);
	strcpy(nome, this->GetPath());
	strcat(nome, "particle");
	strcat(nome, num);
	strcat(nome, ".txt");
}
//********************** PARTICLES VIEWER - END **************************************

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::SetNumberOfMetaCells(int n)
{
	this->NumberOfMetaCells = n;
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriter::GetNumberOfMetaCells()
{
	return this->NumberOfMetaCells;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::SetParticles(vtkPoints* pos)
{
	if(!this->InitialPositionOfParticles)
		{
		this->InitialPositionOfParticles = vtkPoints::New();
		this->InitialPositionOfParticles->DeepCopy(pos);
		}
	else
		{
		this->InitialPositionOfParticles->Reset();
		this->InitialPositionOfParticles->DeepCopy(pos);
		}

	this->LastPositions = new point[this->InitialPositionOfParticles->GetNumberOfPoints()];
}

//----------------------------------------------------------------------------
vtkPoints* vtkHM3DParticleTracingWriter::GetParticles()
{
	return this->InitialPositionOfParticles;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::WriteConfigFile()
{
	char configName[512];
	int initialTime = 1;
	int finalTime = this->NumberOfParticles;

	strcpy(configName, this->GetPath());
	strcat(configName, "InformationFile.par");

//	remove(configName);
	FILE *fpar = fopen(configName, "w");

	fprintf(fpar, "*NUMBER OF TIME STEPS\n");
	fprintf(fpar, "%3d\n\n", this->timeSteps);

	fprintf(fpar, "*INITIAL PARTICLE NUMBER FILE\n");
	fprintf(fpar, "%3d\n\n", initialTime);

	fprintf(fpar, "*FINAL PARTICLE NUMBER FILE\n");
	fprintf(fpar, "%3d\n\n", finalTime);

	fclose(fpar);
}

//----------------------------------------------------------------------------
/*******************************************************************************/
/* Functions - MetaCells */
/*******************************************************************************/
// Auxiliar structure that receive information of valid points
// of each metacell (Vector Field Stage)
void vtkHM3DParticleTracingWriter::setBinaryList(char *metaName, int *binaryList, int numPoints, int metaNumPoints)
{
	int i, pId;
	float aux;

	FILE *f = fopen(metaName, "r");

	for(i = 0; i < numPoints; i++)
		binaryList[i] = 0;

	for(i = 0; i < metaNumPoints; i++)
	{
		fscanf(f, "%d %f %f %f",&pId, &aux, &aux,&aux);
		binaryList[pId] = 1;
	}

	fclose(f);
}

//----------------------------------------------------------------------------
// Error message for open File error operation
void vtkHM3DParticleTracingWriter::openFileError(FILE *findex)
{
	if(!findex)
	{
		printf("\n\n ** Error when open file!!! ** \n\n");
		exit(1);
	}
}

//----------------------------------------------------------------------------
// Find the "number of points" information in the _index.txt file
int vtkHM3DParticleTracingWriter::getNumPoinsFromFile(int metaNumber)
{
	int num,i;
	char aux[512], indexName[512];

	strcpy(indexName, this->GetPath());
	strcat(indexName, "_index.txt");

	FILE *findex = fopen(indexName, "r");
	this->openFileError(findex);

	for(i = 1; i <= 8; i++)
	{
		fscanf(findex, "%s %d", aux, &num);
		if(i == metaNumber)
		{
			fclose(findex);
			return num;
		}
		fscanf(findex, "%s %s %s %s %s %s %s", &aux, &aux, &aux, &aux, &aux, &aux, &aux);
	}
	fclose(findex); //add
	return 0;
}

//----------------------------------------------------------------------------
// Set MetaCell File Name
void vtkHM3DParticleTracingWriter::setMetaCellFileName(char *nome, int n)
{
	char num[10];
	sprintf(num, "%d", n);
	strcpy(nome, this->GetPath());
	strcat(nome, "metacell");
	strcat(nome, "00");
	strcat(nome, num);
	strcat(nome, ".txt");
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::writeMetaCell(HMsort *sortList, cell * cellList, point *pointList, int num, int begin, int end, int metaNumber)
{
	char metaName1[512];
	this->setMetaCellFileName(metaName1, metaNumber);

	FILE *fmeta = fopen(metaName1,  "w+");

//	printf("\nCreating file %s", metaName1);

	int i, j, id, v[4], localNumberOfPoints = 0, localNumberOfCells;
	float xmin, xmax, ymin, ymax, zmin, zmax;
	int * binaryList;
//	binaryList = (int *) malloc(num * sizeof(int));
	binaryList = new int[num];

	// Set binaryList to zero in each position
	for(i = 0; i < num; i++) binaryList[i] = 0;

	// Fill binaryList avoiding duplication
	for( i = begin; i <= end; i++)
	{
		id = sortList[i].cellId;
		for(j = 0; j < 4; j++)
		{
			v[j] = cellList[ id ].v[j];
			if( !binaryList[ v[j] ] ) binaryList[ v[j] ] = 1;
		}
	}

	int firstMinMax = 0;

	// Put all marked points in the metacell File
	for(i = 0; i < num; i++)
	{
		if(binaryList[i])
		{
			localNumberOfPoints++;
			this->setMinMaxValues(firstMinMax, i, pointList, &xmin, &xmax, &ymin, &ymax, &zmin, &zmax);
			fprintf(fmeta, "\n%d %f %f %f", pointList[i].pointId, pointList[i].x, pointList[i].y, pointList[i].z);
			if(!firstMinMax) firstMinMax = 1;
		}
	}

	fprintf(fmeta, "\n%s %d\n\n","Total:", localNumberOfPoints);

	// Put all cells in the metacell file
	for( i = begin; i <= end; i++)
	{
		id = sortList[i].cellId;
		fprintf(fmeta, "\n%d %d %d %d", cellList[ id ].v[0], cellList[id ].v[1], cellList[ id ].v[2], cellList[ id ].v[3]);
	}
	localNumberOfCells = end - begin + 1;
	fprintf(fmeta, "\n%s %d\n\n", "Total:", localNumberOfCells); // Number of Cells of the file

	this->localPosition[metaNumber] = ftell(fmeta);

	delete [] binaryList;

	this->writeMetaIndexFile(metaName1,localNumberOfPoints, localNumberOfCells, xmin, xmax, ymin, ymax, zmin, zmax);
	fclose(fmeta);

//	printf("\t\t\t[ OK ]");
}

//----------------------------------------------------------------------------
// Compute and Write Metacells Index File
void vtkHM3DParticleTracingWriter::writeMetaIndexFile(char *metaName, int nPoints, int nCells, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax)
{
	char indexName[512];
	strcpy(indexName, this->GetPath());
	strcat(indexName, "_index.txt");
	FILE *findex = fopen(indexName, "a+");
	fprintf(findex, "%s\t%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\n", metaName, nPoints, nCells, xmin, xmax, ymin, ymax, zmin, zmax);
	fclose(findex);
}

//----------------------------------------------------------------------------
// Set Mininum and Maximum value for each Metacell
void vtkHM3DParticleTracingWriter::setMinMaxValues(int first, int i, point* pointList, float *xmin, float *xmax, float *ymin, float *ymax, float *zmin, float *zmax)
{
	if(!first) // First Time
	{
		* xmin = pointList[i].x;
		* xmax = pointList[i].x;
		* ymin = pointList[i].y;
		* ymax = pointList[i].y;
		* zmin = pointList[i].z;
		* zmax = pointList[i].z;
	}
	else
	{
		* xmin = (pointList[i].x < * xmin) ? pointList[i].x : * xmin;
		* xmax = (pointList[i].x > * xmax) ? pointList[i].x : * xmax;
		* ymin = (pointList[i].y < * ymin) ? pointList[i].y : * ymin;
		* ymax = (pointList[i].y > * ymax) ? pointList[i].y : * ymax;
		* zmin = (pointList[i].z < * zmin) ? pointList[i].z : * zmin;
		* zmax = (pointList[i].z > * zmax) ? pointList[i].z : * zmax;

	}

}

//----------------------------------------------------------------------------
// First step to build Metacell - Copying the X coordenates to sort
void vtkHM3DParticleTracingWriter::copyStructureX(HMsort *sortList, cell * cellList, int begin, int end)
{
	int i;
	for( i = begin; i < end; i++)
	{
		sortList[i].cellId = cellList[i].cellId;
		sortList[i].vertex = cellList[i].center.x;
	}
}

//----------------------------------------------------------------------------
// Second step to build Metacell - Copying the Y coordenates to sort
void vtkHM3DParticleTracingWriter::copyStructureY(HMsort *sortList, cell * cellList, int begin, int end)
{
	int i, id;
	for( i = begin; i < end; i++)
	{
		id = sortList[i].cellId;
		sortList[i].vertex = cellList[ id ].center.y;
	}
}

//----------------------------------------------------------------------------
// Third step to build Metacell - Copying the Z coordenates to sort
void vtkHM3DParticleTracingWriter::copyStructureZ(HMsort *sortList, cell * cellList, int begin, int end)
{
	int i, id;
	for( i = begin; i < end; i++)
	{
		id = sortList[i].cellId;
		sortList[i].vertex = cellList[ id ].center.z;
	}
}

//----------------------------------------------------------------------------
// Computing the center points of the cells (tetrahedron)
void vtkHM3DParticleTracingWriter::computingPointCenter(cell * cellList, point * pList, int numCells)
{
	int i;
	int p0, p1, p2, p3;
	for(i=0; i < numCells; i++)
	{
		cellList[i].center.pointId = i;
		p0 = cellList[i].v[0];
		p1 = cellList[i].v[1];
		p2 = cellList[i].v[2];
		p3 = cellList[i].v[3];

		cellList[i].center.x = ( pList[ p0 ].x + pList[ p1 ].x + pList[ p2 ].x + pList[ p3 ].x) / 4;
		cellList[i].center.y = ( pList[ p0 ].y + pList[ p1 ].y + pList[ p2 ].y + pList[ p3 ].y) / 4;
		cellList[i].center.z = ( pList[ p0 ].z + pList[ p1 ].z + pList[ p2 ].z + pList[ p3 ].z) / 4;
	}
}

//----------------------------------------------------------------------------
// Compute Euclidean Distance
float vtkHM3DParticleTracingWriter::computeDistance(point p1, point p2)
{
	float x1, y1, z1, x2, y2, z2;
	x1 = p1.x; 	y1 = p1.y; 	z1 = p1.z;
	x2 = p2.x; 	y2 = p2.y; 	z2 = p2.z;

	return (float) sqrt(pow(x1-x2,2) + pow(y1-y2,2) + pow(z1-z2,2));
}

//----------------------------------------------------------------------------
// Encontra distancia da maior aresta de cada célula, para auxiliar na busca
// dos tetraedros de interesse no momento de calcular o traçado da uma partícula
void vtkHM3DParticleTracingWriter::setMaximumDistance(float *d, point *pList, int v0, int v1, int v2, int v3)
{
	float aux;
	aux = this->computeDistance(pList[ v0 ], pList[ v1 ]); if(aux > *d) *d = aux;
	aux = this->computeDistance(pList[ v0 ], pList[ v2 ]); if(aux > *d) *d = aux;
	aux = this->computeDistance(pList[ v0 ], pList[ v3 ]); if(aux > *d) *d = aux;
	aux = this->computeDistance(pList[ v1 ], pList[ v2 ]); if(aux > *d) *d = aux;
	aux = this->computeDistance(pList[ v1 ], pList[ v3 ]); if(aux > *d) *d = aux;
	aux = this->computeDistance(pList[ v2 ], pList[ v3 ]); if(aux > *d) *d = aux;
}

//----------------------------------------------------------------------------
/*******************************************************************************/
/* QuickSort */
/*******************************************************************************/
void vtkHM3DParticleTracingWriter::swap(HMsort *a, int i, int j)
{
	float tmp = a[i].vertex;
    a[i].vertex = a[j].vertex;
    a[j].vertex = tmp;

    int cellId = a[i].cellId;
    a[i].cellId = a[j].cellId;
    a[j].cellId = cellId;
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriter::Random(int i, int j)
{
    return i + rand() % (j-i+1);
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::quicksort(HMsort *a, int left, int right)
{
    int last = left, i;

    if (left >= right) return;

    this->swap(a,left,this->Random(left,right));
    for (i = left + 1; i <= right; i++)
        if (a[i].vertex < a[left].vertex)
           this->swap(a,++last,i);
    this->swap(a,left,last);
    this->quicksort(a,left,last-1);
    this->quicksort(a,last+1,right);
}

//----------------------------------------------------------------------------
/*******************************************************************************/
/* Functions - Particles */
/*******************************************************************************/
// Write the starting time on the particle file
void vtkHM3DParticleTracingWriter::writeTimeInFile(int startTime)
{
	FILE *fpar;
	char particleName[256];
	this->setFileName(particleName, partNumber, 2);

	fpar = fopen(particleName, "w");

	fprintf(fpar, "TIME %d\n", startTime); // Writing Time
	fprintf(fpar, "%f %f %f %f %f %f %f %f\n", this->part.x, this->part.y, this->part.z, this->part.velocityX, this->part.velocityY,
												this->part.velocityZ, this->part.pressure, this->part.time); // Writing First Position

	fclose(fpar);
}

//----------------------------------------------------------------------------
//Check if a metacell is inside or outside an interval
int vtkHM3DParticleTracingWriter::checkMetacellIntervals(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax)
{
	point p = this->part;
	if((p.x >= xmin && p.x <= xmax) && (p.y >= ymin && p.y <= ymax) && (p.z >= zmin && p.z <= zmax))
		return 1; // Particle is inside
	else
		return 0; // Particle is outside
}

//----------------------------------------------------------------------------
// Print the Metacells Candidates
void vtkHM3DParticleTracingWriter::printfCandidates(point p, int * metaVec)
{
	printf("\nParticula pode estar nas seguintes Metacelulas: ");
	for(int i = 1; i <= 8; i++)
	{
		if(metaVec[i])
			printf(" %d", i);
	}
}

//----------------------------------------------------------------------------
// Set MetaCell File Name
void vtkHM3DParticleTracingWriter::setFileName(char *nome, int n, int op)
{
	char num[6];
	sprintf(num, "%03d", n);
	strcpy(nome,this->GetPath());
	if(op == 1) strcat(nome, "metacell");
	if(op == 2) strcat(nome, "particle");
	if(op == 3)
	{
		strcat(nome, "_index.txt");
		return;
	}
	strcat(nome, num);
	strcat(nome, ".txt");
}

//----------------------------------------------------------------------------
// Return the number of points and cells written in the _index.txt file
void vtkHM3DParticleTracingWriter::getNumberOfPointsAndCells(int metaNumber, int *numPoints, int *numCells)
{
	char indexName[256];
	this->setFileName(indexName, 0, 3);
	FILE *f = fopen(indexName, "r");

	char aux[256];
	float faux;

	for(int i = 1; i <= 8; i++)
	{
		//fprintf("\nMetaCellFile	Number_Points	Number_Cells	xmin		xmax		ymin		ymax		zmin		zmax");
		fscanf(f, "%s %d %d %f %f %f %f %f %f", aux, numPoints, numCells, &faux, &faux, &faux, &faux, &faux, &faux);
		if(i == metaNumber) break;
	}

	fclose(f);
}

//----------------------------------------------------------------------------
// Set global variable "dist" for general purposes
void vtkHM3DParticleTracingWriter::setMaximumDistancePart()
{
	char indexName[256];
	this->setFileName(indexName, 0, 3);
	FILE *findex = fopen(indexName, "r");

	int i;
	float faux;
	char aux[256];

	for(i = 1; i <= 8; i++)
		fscanf(findex, "%s %s %s %f %f %f %f %f %f", aux, aux, aux, &faux, &faux, &faux, &faux, &faux, &faux);

	fscanf(findex, "%s %f", aux, &this->distPart);
	fscanf(findex, "%s", aux); // Reading VelocityVectorPositionLabel

	for(i = 1; i <= 8; i++)
		fscanf(findex, "%d", &this->localPosition[i]); // Load Global Vector

	fclose(findex);

//	printfCandidates(p, metaVec);
}

//----------------------------------------------------------------------------
// Compute Euclidean Distance between two points and check if
// that value is lower than the minimum 'distance'
int vtkHM3DParticleTracingWriter::checkMinimumDistance(float x, float y,float z)
{
	float d;
	d = float(pow(x - this->part.x,2) + pow(y - this->part.y,2) + pow(z - this->part.z,2) - pow(distPart,2));
	if(d <= 0.0)
		return 1;
	else
		return 0;
}

//----------------------------------------------------------------------------
// Get Cells by the Euclidean Distance of the Particle coordinate
int vtkHM3DParticleTracingWriter::getCloserCells(point* pointList, cell c)
{
	float x, y, z;
	int res[4];		// load the result for each vertex
	for(int i = 0; i <=3; i++)
	{
		if(pointList[ c.v[i] ].pointId)
		{
			x = pointList[ c.v[i] ].x;
			y = pointList[ c.v[i] ].y;
			z = pointList[ c.v[i] ].z;
			res[i] = checkMinimumDistance(x,y,z);
		}
		else
		{
			printf("\n\nCheck getCloserCells Function!!!\n\n");
			system("PAUSE");
			exit(1);
		}
	}
	if( res[0] && res[1] && res[2] && res[3])
		return 1;
	else
		return 0;
}

//----------------------------------------------------------------------------
// Set Point Values
void vtkHM3DParticleTracingWriter::setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, point* plist, cell c)
{
	*p0 = part;

	*p1 = plist[ c.v[0] ];
	*p2 = plist[ c.v[1] ];
	*p3 = plist[ c.v[2] ];
	*p4 = plist[ c.v[3] ];
}

//----------------------------------------------------------------------------
// Compute subtration operation for points
void vtkHM3DParticleTracingWriter::computeVectorOperationSub(point *v, point pa, point pb)
{
	(*v).x = pa.x - pb.x;
	(*v).y = pa.y - pb.y;
	(*v).z = pa.z - pb.z;
}

//----------------------------------------------------------------------------
// Set Vector Values
void vtkHM3DParticleTracingWriter::setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4)
{
	computeVectorOperationSub(v1, p0, p1);
	computeVectorOperationSub(v2, p2, p1);
	computeVectorOperationSub(v3, p3, p1);
	computeVectorOperationSub(v4, p4, p1);
}

//----------------------------------------------------------------------------
// Calculates Cross Product between two vector v = pa x pb
void vtkHM3DParticleTracingWriter::computeCrossProd(point *v, point va, point vb)
{
	(*v).x = (va.y * vb.z) - (va.z * vb.y);
	(*v).y = (va.z * vb.x) - (va.x * vb.z);
	(*v).z = (va.x * vb.y) - (va.y * vb.x);
}

//----------------------------------------------------------------------------
// Set the 'n' Values to compute inside/outside points
void vtkHM3DParticleTracingWriter::setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4)
{
	computeCrossProd(n1, v2, v3);
	computeCrossProd(n2, v2, v4);
	computeCrossProd(n3, v3, v4);
}

//----------------------------------------------------------------------------
// Calculates Scalar Product between two vector r = v1. v2
float vtkHM3DParticleTracingWriter::computeScalarProd(point va, point vb)
{
	return float(va.x * vb.x + va.y * vb.y + va.z * vb.z);
}

//----------------------------------------------------------------------------
// Set the alpha values to compute inside/outside points
void vtkHM3DParticleTracingWriter::setAlphaValues(float *alpha, point n, point va, point vb)
{
	float val1, val2;
	val1 = float(computeScalarProd(n, va));
	val2 = float(computeScalarProd(n, vb));

	if(val2)
		*alpha = float(val1 / val2);
	else
	{
		printf("\nCheck 'setAlphaValues' function!!!\n");
		system("PAUSE");
		*alpha = 0;
	}

}

//----------------------------------------------------------------------------
// Check if the particle is inside a given cell (tetraedro).
int vtkHM3DParticleTracingWriter::isInsideCell(point* pointList, cell c)
{
	point v1, v2, v3, v4;
	point n1, n2, n3;
	point p0, p1, p2, p3, p4;

	setPoints(&p0, &p1, &p2, &p3, &p4, pointList, c);
	setVector(&v1, &v2, &v3, &v4, p0, p1, p2, p3, p4);
	setNValues(&n1, &n2, &n3, v2, v3, v4);

	setAlphaValues(&a4, n1, v1, v4);
	setAlphaValues(&a3, n2, v1, v3);
	setAlphaValues(&a2, n3, v1, v2);

	a1 = 1 - a2 - a3 - a4;

	if((a1>=0  && a1<=1) && (a2>=0  && a2<=1) && (a3>=0  && a3<=1) && (a4>=0  && a4<=1))
		return 1; // Point defined by global variable 'part' is inside the cell 'c'
	else
		return 0; // Point is outside the cell 'c'
}

//----------------------------------------------------------------------------
// Load Global Velocity Vector
void vtkHM3DParticleTracingWriter::loadVelocityVectorFromFile(int metaNumber, int numPoints)
{
	int t,i, id, fullId;
	char aux[256], metaName[256];

	currentMetacell = metaNumber;

	setFileName(metaName, metaNumber, 1);
	FILE *fvec = fopen(metaName,  "r");

	fseek(fvec, localPosition[metaNumber], SEEK_SET); // Set File Position

	// Load the full List for the 'metaNumber' metacell
	for(t = 0; t < timeSteps; t++)
	{
		fscanf(fvec, "%s", aux);
		for(i = 0; i < numPoints; i++)
		{
			id = i + t * numPoints;
			fscanf(fvec, "%d %f %f %f %f", &vecList[id].pointId, &vecList[id].x, &vecList[id].y, &vecList[id].z, &vecList[id].pressure);
		}
	}

	// Load the search list for the time needed
	for(i = 0; i < numPoints; i++)
	{
		fullId = i + numPoints * currentTime; // Point to First element of each time in the main vector
		id = vecList[fullId].pointId;
		vecListSearch[ id ] = vecList[fullId];
	}
	fclose(fvec);
}

//----------------------------------------------------------------------------
// Load Local Velocity Vector from Global Velocity Vector
void vtkHM3DParticleTracingWriter::loadVelocityVectorFromMemory(int numPoints)
{
	int i, id, fullId;

	for(i = 0; i < numPoints; i++)
		{
			fullId = i + numPoints * currentTime; // Point to First element of each time in the main vector
			id = vecList[fullId].pointId;
			vecListSearch[ id ] = vecList[fullId];
		}
}

//----------------------------------------------------------------------------
// Procedures that computes the velocity of a particle inside a cell. Metacells are read here.
void vtkHM3DParticleTracingWriter::computeVelocity(cell c, int metaNumber, int numPoints)
{
	FILE *fpar;
//	float delta = 0.005f;
	float delta = this->Delta;
	char particleName[256];
	float vec[4];

	// Se a meta célula carregada em memória for diferente da meta célula corrente, carregar a metacécula e velocidade
	// Se não carregar somente a velocidade para o timestep
	if(metaNumber != currentMetacell)
		this->loadVelocityVectorFromFile(metaNumber, numPoints);
	else
		this->loadVelocityVectorFromMemory(numPoints);

	this->setFileName(particleName, partNumber, 2);

	// New
	fpar = fopen(particleName, "a+");

	double tmpPart[4];
	tmpPart[0] = this->part.x;
	tmpPart[1] = this->part.y;
	tmpPart[2] = this->part.z;
	tmpPart[3] = this->part.time;

	// Computes the vector
	this->part.x = this->part.x + delta * (a1 * this->vecListSearch[ c.v[0] ].x + a2 * this->vecListSearch[ c.v[1] ].x +
	                           a3 * this->vecListSearch[ c.v[2] ].x + a4 * this->vecListSearch[ c.v[3] ].x);
	this->part.y = this->part.y + delta * (a1 * this->vecListSearch[ c.v[0] ].y + a2 * this->vecListSearch[ c.v[1] ].y +
	                           a3 * this->vecListSearch[ c.v[2] ].y + a4 * this->vecListSearch[ c.v[3] ].y);
	this->part.z = this->part.z + delta * (a1 * this->vecListSearch[ c.v[0] ].z + a2 * this->vecListSearch[ c.v[1] ].z +
	                           a3 * this->vecListSearch[ c.v[2] ].z + a4 * this->vecListSearch[ c.v[3] ].z);

	this->part.velocityX = (a1 * this->vecListSearch[ c.v[0] ].x + a2 * this->vecListSearch[ c.v[1] ].x +
														a3 * this->vecListSearch[ c.v[2] ].x + a4 * this->vecListSearch[ c.v[3] ].x);
	this->part.velocityY = (a1 * this->vecListSearch[ c.v[0] ].y + a2 * this->vecListSearch[ c.v[1] ].y +
														a3 * this->vecListSearch[ c.v[2] ].y + a4 * this->vecListSearch[ c.v[3] ].y);
	this->part.velocityZ = (a1 * this->vecListSearch[ c.v[0] ].z + a2 * this->vecListSearch[ c.v[1] ].z +
      											a3 * this->vecListSearch[ c.v[2] ].z + a4 * this->vecListSearch[ c.v[3] ].z);


	this->part.pressure = ( this->vecListSearch[ c.v[0] ].pressure + this->vecListSearch[ c.v[1] ].pressure +
      											 this->vecListSearch[ c.v[2] ].pressure + this->vecListSearch[ c.v[3] ].pressure) / 4 ;

	this->part.time = this->part.time + delta;



	//armazena as coordenadas do tempo zero mas com velocidade e pressão do passo 1 ..........o tempo será o do passo zero.
	if( !this->IsRepeatPulse && this->currentTime == 0 )
		{
		//fecha o arquivo
		fclose(fpar);

		//apaga conteudo o abre o arquivo para leitura e escrita
		fpar = fopen(particleName, "w+");

		fprintf(fpar, "TIME %d\n", this->currentTime);

		//coordenadas do passo zero com velocidade, pressão e tempo do passo 1
		fprintf(fpar, "%f %f %f %f %f %f %f %f\n", tmpPart[0], tmpPart[1], tmpPart[2],
						this->part.velocityX, this->part.velocityY,	this->part.velocityZ, this->part.pressure, tmpPart[3]);

		//fecha o arquivo
		fclose(fpar);

		//abre o arquivo para leitura e adição
		fpar = fopen(particleName, "a+");
		}




	//Se as coordenadas de x, y e z forem diferentes ao passo anterior, houve movimento
	if( (this->part.x != this->LastPositions[partNumber-1].x) &&
			(this->part.y != this->LastPositions[partNumber-1].y) &&
			(this->part.z != this->LastPositions[partNumber-1].z) )
		{

//		if( this->currentTime > 0)
//			{
			//armazena as coordenadas, velocidade, pressão e tempo do passo
			fprintf(fpar, "%f %f %f %f %f %f %f %f\n", this->part.x, this->part.y, this->part.z, this->part.velocityX, this->part.velocityY,
													this->part.velocityZ, this->part.pressure, this->part.time);
//			}


		// Armazenando a última posição calculada para a partícula corrente
		this->LastPositions[partNumber-1].x = this->part.x;
		this->LastPositions[partNumber-1].y = this->part.y;
		this->LastPositions[partNumber-1].z = this->part.z;
		this->LastPositions[partNumber-1].velocityX = this->part.velocityX;
		this->LastPositions[partNumber-1].velocityY = this->part.velocityY;
		this->LastPositions[partNumber-1].velocityZ = this->part.velocityZ;
		this->LastPositions[partNumber-1].pressure = this->part.pressure;
		this->LastPositions[partNumber-1].time = this->part.time;
		}


	this->currentTime++;
	fclose(fpar);
}

//----------------------------------------------------------------------------
// Procedures that computes the velocity of a particle inside a cell already allocated in memory
void vtkHM3DParticleTracingWriter::computeVelocityFaster(cell c, int metaNumber, int numPoints)
{
	FILE *fpar;
//	float delta = 0.005f;
	float delta = this->Delta;
	char particleName[256];
	float vec[4];

	setFileName(particleName, partNumber, 2);

	fpar = fopen(particleName, "a+");

	int i  = metaNumber;

	// Computes the vector
	part.x = part.x + delta * (a1 * metaCells[i].vecListSearch[ c.v[0] ].x + a2 * metaCells[i].vecListSearch[ c.v[1] ].x +
	                           a3 * metaCells[i].vecListSearch[ c.v[2] ].x + a4 * metaCells[i].vecListSearch[ c.v[3] ].x);
	part.y = part.y + delta * (a1 * metaCells[i].vecListSearch[ c.v[0] ].y + a2 * metaCells[i].vecListSearch[ c.v[1] ].y +
	                           a3 * metaCells[i].vecListSearch[ c.v[2] ].y + a4 * metaCells[i].vecListSearch[ c.v[3] ].y);
	part.z = part.z + delta * (a1 * metaCells[i].vecListSearch[ c.v[0] ].z + a2 * metaCells[i].vecListSearch[ c.v[1] ].z +
	                           a3 * metaCells[i].vecListSearch[ c.v[2] ].z + a4 * metaCells[i].vecListSearch[ c.v[3] ].z);
	part.pressure = 0.2;

	currentTime++;

	fprintf(fpar, "%f %f %f %f\n", part.x, part.y, part.z, part.pressure);
	fclose(fpar);
}

//----------------------------------------------------------------------------
// Read All Metacells
void vtkHM3DParticleTracingWriter::readAllMetaCells()
{
	char metaName[256], aux[256];
	int numPoints, numCells, i, j, t;

	printf("\nReading all Metacells in memory...\n");

	for(i = 1; i <= 8; i++)
	{
		setFileName(metaName, i, 1);
		FILE *fin = fopen(metaName,  "r");

		if(!fin)
		{
			printf("\n\nError: File \"%s\" not found!\n\n", metaName);
			exit(1);
		}

		getNumberOfPointsAndCells(i, &numPoints, &numCells);

		metaCells[i].numPoints = numPoints;
		metaCells[i].numCells  = numCells;
//		metaCells[i].pointList = (point *) malloc (10 * numPoints * sizeof(point));// Multiply by 10 to allow direct seach (any index)
		metaCells[i].pointList = new point[10 * numPoints];// Multiply by 10 to allow direct seach (any index)

		// Set all Values to NULL
		for(j=0; j < numPoints * 10; j++) metaCells[i].pointList[j].pointId = 0;

		int id;
		for(j=0; j < numPoints; j++)
		{
			fscanf(fin, "%d",&id);
			metaCells[i].pointList[ id ].pointId = 1;
			fscanf(fin, "%f",&metaCells[i].pointList[ id ].x);
			fscanf(fin, "%f",&metaCells[i].pointList[ id ].y);
			fscanf(fin, "%f",&metaCells[i].pointList[ id ].z);
		}
		fscanf(fin, "%s %s",aux, aux);

//		metaCells[i].cellList = (cell *) malloc (numCells * sizeof(cell));
		metaCells[i].cellList = new cell[numCells];

		/* Reading cells */
		//printf("\nReading %d from %s...", numCells, metaName);
		for(j=0; j < numCells; j++)
		{
			metaCells[i].cellList[j].cellId = j;
			fscanf(fin, "%d %d %d %d",&metaCells[i].cellList[j].v[0],&metaCells[i].cellList[j].v[1],
		   	                       &metaCells[i].cellList[j].v[2],&metaCells[i].cellList[j].v[3]);
		}
		fscanf(fin, "%s %s",aux, aux);

		// Initializing global List that loads all times for one metacell (not indexed)
//		metaCells[i].vecList = (point *) malloc(timeSteps * maxMetaPoints * sizeof(point));
		metaCells[i].vecList = new point[timeSteps * maxMetaPoints];

		// Initializing global List that loads one time indexed (for search purposes)
//		metaCells[i].vecListSearch = (point *) malloc(totalPoints * sizeof(point));
		metaCells[i].vecListSearch = new point [totalPoints];

		// Load the full List for the 'metaNumber' metacell
		for(t = 0; t < timeSteps; t++)
		{
			fscanf(fin, "%s", aux);
			for(j = 0; j < metaCells[i].numPoints; j++)
			{
				id = j + t * metaCells[i].numPoints;
				fscanf(fin, "%d %f %f %f", &metaCells[i].vecList[id].pointId, &metaCells[i].vecList[id].x,
				                           &metaCells[i].vecList[id].y,       &metaCells[i].vecList[id].z);
			}
		}

		int fullId;
		// Load the search list for the time needed
		for(j = 0; j < metaCells[i].numPoints; j++)
		{
			fullId = j + metaCells[i].numPoints * currentTime; // Point to First element of each time in the main vector
			id = metaCells[i].vecList[fullId].pointId;
			metaCells[i].vecListSearch[ id ] = metaCells[i].vecList[fullId];
		}

		fclose(fin);
	}
	//printf("\t\t\t\t[ OK ]");
}

//----------------------------------------------------------------------------
// Find Cells by the Euclidean Distance of the Particle coordinate
// All metacells are in memory
void vtkHM3DParticleTracingWriter::findCloserCellsFaster(int metaNumber)
{
	//printf("\nMetanumber: %d NumCells: %d\n", metaNumber, metaCells[ metaNumber ].numCells);
	// Finding an inside point
	for(int i=0; i < metaCells[ metaNumber ].numCells; i++)
	{
		if(currentTime >= startTime)
		{

			if(getCloserCells(metaCells[ metaNumber ].pointList, metaCells[ metaNumber ].cellList[i]))
			{
				if(isInsideCell(metaCells[ metaNumber ].pointList, metaCells[ metaNumber ].cellList[i]))
				{
					if(!fasterMode)
						computeVelocity(metaCells[ metaNumber ].cellList[i], metaNumber, metaCells[ metaNumber ].numPoints);
					else
						computeVelocityFaster(metaCells[ metaNumber ].cellList[i], metaNumber, metaCells[ metaNumber ].numPoints);
					break; // If a cell was found, end the search
				}
			}
		}
		else
			currentTime++;
	}
}

//----------------------------------------------------------------------------
// Find Cells by the Euclidean Distance of the Particle coordinate
void vtkHM3DParticleTracingWriter::findCloserCells(int metaNumber)
{
	char metaName[256], aux[256];
	int numPoints, numCells, i;
	setFileName(metaName, metaNumber, 1);
	FILE *fin = fopen(metaName,  "r");

	getNumberOfPointsAndCells(metaNumber, &numPoints, &numCells);

	point	*pointList;		// Point List
	cell	*cellList;		// Cell List

	//if(currentTime >= startTime) // Just print if its the right time
	//	printf("\nReading %s in time %d...", metaName, currentTime);

//	pointList = (point *) malloc (10 * numPoints * sizeof(point));// Multiply by 10 to allow direct search
	pointList = new point[10 * numPoints];// Multiply by 10 to allow direct search

	// Set all Values to NULL
	for(i=0; i < numPoints; i++) pointList[i].pointId = 0;

	int id;
	for(i=0; i < numPoints; i++)
	{
		fscanf(fin, "%d",&id);
		pointList[ id ].pointId = 1;
		fscanf(fin, "%f",&pointList[ id ].x);
		fscanf(fin, "%f",&pointList[ id ].y);
		fscanf(fin, "%f",&pointList[ id ].z);
	}
	fscanf(fin, "%s %s",aux, aux);

//	cellList = (cell *) malloc (numCells * sizeof(cell));
	cellList = new cell[numCells];

	/* Reading cells */
	//printf("\nReading %d from %s...", numCells, metaName);
	for(i=0; i < numCells; i++)
	{
		cellList[i].cellId = i;
		fscanf(fin, "%d %d %d %d",&cellList[i].v[0],&cellList[i].v[1],&cellList[i].v[2],&cellList[i].v[3]);
	}
	fscanf(fin, "%s %s",aux, aux);
	//printf("\t\t\t\t[ OK ]");

	// Finding an inside point
	for(i=0; i < numCells; i++)
	{
		if(getCloserCells(pointList, cellList[i]))
		{
			if(isInsideCell(pointList, cellList[i]))
			{
//				if(currentTime >= startTime)
				if( (currentTime >= startTime) && (currentTime < this->timeSteps) )
				{
					computeVelocity(cellList[i], metaNumber, numPoints);
					break; // If a cell was found, end the search
				}
				else
					currentTime++;
			}
		}
	}

	/*
	if(inside>1)
	{
		printf("\nCheck findCloserCells\n!");
		exit(1);
	}
	*/

	delete [] pointList;
	delete [] cellList;

//	free(pointList);
//	free(cellList);

	fclose(fin);
}

//----------------------------------------------------------------------------
// Start Computing trace by finding the possible Metacells that contains the particle
// The MetaVec vector set the candidates (1)
void vtkHM3DParticleTracingWriter::computeTrace(int *metaVec)
{
	point p = part;
	char indexName[256];

	setFileName(indexName, 0, 3);
	FILE *findex = fopen(indexName, "r");
	int i,t;
	float xmin[9], xmax[9], ymin[9], ymax[9], zmin[9], zmax[9];
	char aux[256];

	for(i = 1; i <= 8; i++)
	{
		//fprintf("\nMetaCellFile	Number_Points	Number_Cells	xmin		xmax		ymin		ymax		zmin		zmax");
		fscanf(findex, "%s %s %s %f %f %f %f %f %f", aux, aux, aux, &xmin[i], &xmax[i], &ymin[i], &ymax[i], &zmin[i], &zmax[i]);
	}

	for(t = 0; t < timeSteps; t++)
	{
		for(i = 1; i <= 8; i++)
		{
			//fprintf("\nMetaCellFile	Number_Points	Number_Cells	xmin		xmax		ymin		ymax		zmin		zmax");
			metaVec[i] = checkMetacellIntervals(xmin[i], xmax[i], ymin[i], ymax[i], zmin[i], zmax[i]);
			if(metaVec[i])
			{
				if(!fasterMode) findCloserCells(i);
				else			    findCloserCellsFaster(i);
			}
		}
	}
	fclose(findex);

//	printfCandidates(p, metaVec);
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::callMultiPart(int op)
{
	double point[3];
	this->InitialPositionOfParticles->GetPoint(op, point);

	this->part.x = float(point[0]);
	this->part.y = float(point[1]);
	this->part.z = float(point[2]);

	this->part.velocityX = 0.0;
	this->part.velocityY = 0.0;
	this->part.velocityZ = 0.0;

	this->part.pressure = 0.0;

	this->part.time = 0.0;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriter::LoadLastPosition(int op)
{
	this->part.x = this->LastPositions[op].x;
	this->part.y = this->LastPositions[op].y;
	this->part.z = this->LastPositions[op].z;

	this->part.velocityX = this->LastPositions[op].velocityX;
	this->part.velocityY = this->LastPositions[op].velocityY;
	this->part.velocityZ = this->LastPositions[op].velocityZ;

	this->part.pressure = this->LastPositions[op].pressure;

	this->part.time = this->LastPositions[op].time;
}

/*******************************************************************************/
