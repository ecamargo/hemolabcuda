/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DParticleTracingWriter.h,v $

  Copyright (c) Rodrigo Luis de Souza da Silva and Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkHM3DParticleTracingWriter - write metacells and particles files
// .SECTION Description
// vtkHM3DParticleTracingWriter is a source object that builds
// unstructured grid data files with particle tracing.

// .SECTION Caveats


/// vtkHM3DParticleTracingWriterCUDA é um escritor de trajetória de partículas que utiliza somente CPU

#ifndef vtkHM3DParticleTracingWriter_H_
#define vtkHM3DParticleTracingWriter_H_

#include "vtkDataWriter.h"
#include "vtkPoints.h"
#include "vtkHMUtils.h" // Adiciona as estruturas usadas


class VTK_EXPORT vtkHM3DParticleTracingWriter : public vtkDataWriter
{

public:
  vtkTypeRevisionMacro(vtkHM3DParticleTracingWriter, vtkDataWriter);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkHM3DParticleTracingWriter *New();

  // Description
  // Writes description files of metacells
  void WriteMetaCells();

  // Description
  // Writes description files of particles
  void WriteParticles();

  // Description
  // Set/Get output path for files
  void SetPath(char* dir);
  char* GetPath();

  // Description
  // Set/Get initial coordinates for particles
  void SetParticles(vtkPoints* pos);
  vtkPoints* GetParticles();

  // Description
  // Set/Get number of meta cells
  void SetNumberOfMetaCells(int n);
  int GetNumberOfMetaCells();

  void WriteConfigFile();

  // Description
  // Get particles position in all timesteps
  void GetPositionOfParticlesInAllTimeSteps(point **vec);

  vtkSetMacro(NumberOfParticles, int);
  vtkGetMacro(NumberOfParticles, int);

  vtkSetMacro(Delta, float);
  vtkGetMacro(Delta, float);

  // Description:
  // Set/Get number of repetitons of cardiac pulse
  void SetNumberOfRepeatTimeSteps(int arg)
	{
  this->NumberOfRepeatTimeSteps = arg;
	}

  int GetNumberOfRepeatTimeSteps(){return this->NumberOfRepeatTimeSteps;}


protected:
  vtkHM3DParticleTracingWriter();
  virtual ~vtkHM3DParticleTracingWriter();

	// Description:
	// Number of repetitons of cardiac pulse
  int NumberOfRepeatTimeSteps;

  bool IsRepeatPulse;

  // Description
  // Load local Velocity Vector from Global Velocity Vector. Used for particleTrancing`s method computeVelocity
  void loadVelocityVectorFromMemory(int numPoints);

  // Description
  // Output path for files
  char Path[256];

  // Description
  // Number of meta cells
	int NumberOfMetaCells;

  // Description
  // Number of Particles
	int NumberOfParticles;

  // Description
  // Particles coordinates in the all timeSteps. If we have 10 particles and 5 timeSteps, the first 5 elements
	// represents particle 01 for all timeSteps.
	point *PositionOfParticlesInAllTimeSteps;

  // Description
	//
	point *LastPositions;

  // Description
	//
	void LoadLastPosition(int part);


  // Description
  // Initial coordinates for particles
	vtkPoints* InitialPositionOfParticles;



	/**************************************************************************************/
  /* Global Variables */
  /**************************************************************************************/
	// ***************** PARTICLES
	MetaCell metaCells[9];

	/* Globals */
	int totalPoints;		// Total number of points of the structure
	int maxMetaPoints; 	// Max number of points of each metacell
	// O nome original desta variável é dist
	float distPart;				// Global maximum distance between 2 points
	point part;				// Calculated Particle
	int partNumber;			// Particle Number to que used in the file name
	int error;			// error code
	int currentMetacell;// Current Metacell in memory
	int currentTime;	// Current Time of calculation
	int startTime;			// Time to start the simulation
	float a1;
	float a2;
	float a3;
	float a4;	// Global alphas to enable interpolation
	bool fasterMode; // Copy all metacells in memory
	bool IsMetacellsInMemory; // Check if the metacells files are already in memory

	/* Global Lists */
	// Global List that loads all times for one metacell (not indexed)
	point	*vecList;
	// Global List that loads one time indexed (for search purposes)
	point	*vecListSearch;


	// ***************** METACELLS
	// Description
  // Vector that saves the Begining of the velocity
  // vector for each meta file
  int localPosition[9];

  // Description
  // Number of Simulation Steps
  int timeSteps;

  double Delta;

  // O nome original desta variável é dist
  float distMeta;		// Distancia da maior aresta (a ser utilizado no particleTracing.cpp)


  /*******************************************************************************/
  /* Internal Functions - Particles */
  /*******************************************************************************/
  int   checkMetacellIntervals(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);
  void 	printfCandidates(point p, int * metaVec);
  void 	setFileName(char *nome, int n, int op);
  void 	getNumberOfPointsAndCells(int metaNumber, int *numPoints, int *numCells);
  void 	setMaximumDistancePart();
  int 	checkMinimumDistance(float x, float y,float z);
  int 	getCloserCells(point* pointList, cell c);
  void 	setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, point* plist, cell c);
  void 	computeVectorOperationSub(point *v, point pa, point pb);
  void 	setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4);
  void 	computeCrossProd(point *v, point va, point vb);
  void 	setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4);
  float computeScalarProd(point va, point vb);
  void 	setAlphaValues(float *alpha, point n, point va, point vb);
  int 	isInsideCell(point* pointList, cell c);
  void 	loadVelocityVectorFromFile(int metaNumber, int numPoints);
  void 	writeTimeInFile(int startTime);
  void 	computeVelocity(cell c, int metaNumber, int numPoints);
  void 	computeVelocityFaster(cell c, int metaNumber, int numPoints);
  void 	findCloserCells(int metaNumber);
  void 	findCloserCellsFaster(int metaNumber);
  void 	computeTrace(int *metaVec);
  void 	callMultiPart(int op);
  void  readAllMetaCells();
  /*******************************************************************************/

  /**************************************************************************************/
  /* Internal Functions - ParticleViewer */
  /**************************************************************************************/
  int getNumberOfTimes(char *file);
  void setParticleViewerFileName(char *nome, int n);

  /**************************************************************************************/
  /* Internal Functions - MetaCells */
  /**************************************************************************************/
  void setBinaryList(char *metaName, int *binaryList, int numPoints, int metaNumPoints);
  void openFileError(FILE *findex);
  int getNumPoinsFromFile(int metaNumber);
  void setMetaCellFileName(char *nome, int n);
  void writeMetaCell(HMsort *sortList, cell * cellList, point *pointList, int num, int begin, int end, int metaNumber);
  void writeMetaIndexFile(char *metaName, int nPoints, int nCells, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);
  void setMinMaxValues(int first, int i, point* pointList, float *xmin, float *xmax, float *ymin, float *ymax, float *zmin, float *zmax);
  void copyStructureX(HMsort *sortList, cell * cellList, int begin, int end);
  void copyStructureY(HMsort *sortList, cell * cellList, int begin, int end);
  void copyStructureZ(HMsort *sortList, cell * cellList, int begin, int end);
  void computingPointCenter(cell * cellList, point * pList, int numCells);
  float computeDistance(point p1, point p2);
  void setMaximumDistance(float *d, point *pList, int v0, int v1, int v2, int v3);
  /**************************************************************************************/



  /**************************************************************************************/
  /* QuickSort Functions */
  /**************************************************************************************/
  void swap(HMsort *a, int i, int j);
  int Random(int i, int j);
  void quicksort(HMsort *a, int left, int right);
  //**************************************************************************************/


  vtkHM3DParticleTracingWriter(const vtkHM3DParticleTracingWriter&);  // Not implemented.
  void operator=(const vtkHM3DParticleTracingWriter&);  // Not implemented.

};
#endif /*vtkHM3DParticleTracingWriter_H_*/
