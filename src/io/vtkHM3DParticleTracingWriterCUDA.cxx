/*=========================================================================

Program:   HeMoLab
Module:    $RCSfile: vtkHM3DParticleTracingWriterCUDA.h,v $

Copyright (c) Rodrigo Luis de Souza da Silva and Eduardo Camargo
All rights reserved.
See Copyright.txt or http://hemo01a.lncc.br for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHM3DParticleTracingWriterCUDA.h"

#include "vtkToolkits.h" // for VTK_USE_PARALLEL
#ifdef VTK_USE_PARALLEL
# include "vtkMultiProcessController.h"
#endif

#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDataSet.h"
#include "vtkDoubleArray.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkHM3DModelGrid.h"
#include "vtkPoints.h"


#include "sys/time.h"
#include <algorithm>
#include "stdio.h"
#include <vtkstd/vector>
#include <vtkstd/list>
#include <vtkstd/map>



#include <cuda.h>
#include <cuda_runtime.h>

#include "external_dependency.h"


extern int ExecuteKernel_DispatcherThreadsGPU(point *Device_ParticlePath, gpuMetaCell *Device_metaCells, point *Device_vecListSearch,
                                              uint nParticles, uint nSteps, float maxDistance, float delta, cell *Device_fullCells,
                                              point *Device_fullPoints, uint cycle_Id, uint startCycle, uint nblocks);



//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkHM3DParticleTracingWriterCUDA);
vtkCxxRevisionMacro(vtkHM3DParticleTracingWriterCUDA, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
vtkHM3DParticleTracingWriterCUDA::vtkHM3DParticleTracingWriterCUDA()
{
  this->metaCells = new MetaCell[9];
  this->FileName = NULL;
  this->InitialPositionOfParticles = NULL;
  this->NumberOfParticles = 0;
  this->Delta = 0.005f;
  this->NumberOfRepeatTimeSteps = 1;
  this->IsRepeatPulse = false;
  this->LastPositions = NULL;

  //METACELLS
  this->distMeta = 0;

  //PARTICLES
  error = 0;      // error code
  currentMetacell = 0;// Current Metacell in memory
  currentTime = 0;  // Current Time of calculation
//  fasterMode = false; // Copy all metacells in memory
//  IsMetacellsInMemory = false; // Check if the metacells files are already in memory

  this->Host_fullPoints = NULL;
  this->Host_fullCells = NULL;
  this->Host_metaCells = new gpuMetaCell[9];
  this->Host_ParticlePath = NULL;
}

//----------------------------------------------------------------------------
vtkHM3DParticleTracingWriterCUDA::~vtkHM3DParticleTracingWriterCUDA()
{
  this->SetFileName(NULL);

  if( this->InitialPositionOfParticles )
    this->InitialPositionOfParticles->Delete();

  if( this->LastPositions )
    delete [] this->LastPositions;

  if( this->metaCells )
    delete [] this->metaCells;

  if( this->Host_metaCells )
    delete [] this->Host_metaCells;

  if( this->Host_ParticlePath )
    delete [] this->Host_ParticlePath;

  if( this->Host_fullPoints )
    delete [] this->Host_fullPoints;

  if( this->Host_fullCells )
    delete [] this->Host_fullCells;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "FileName: "
      << (this->FileName ? this->FileName : "(none)") << "\n";
  os << indent << "Path: "
      << (this->Path ? this->Path : "(none)") << "\n";
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::WriteMetaCells()
{
  vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );

  //********************** METACELLS CREATION - BEGIN ************************************
  int i;          // General counter
  this->distMeta = 0;   // Distancia da maior aresta (a ser utilizada no particleTracing.cpp)
  int numPoints = input->GetNumberOfPoints();
  int numCells = input->GetNumberOfCells();

  char indexName[512];  // Index File Name

  point *pointList;   // Point List
  cell  *cellList;    // Cell List
  HMsort  *sortList;    // Auxiliar list to help sorting data

  pointList = new point[numPoints];
  for(i=0; i < numPoints; i++)
    pointList[i].pointId = -1;

  double p[3];
  for(i=0; i < numPoints; i++)
    {
    input->GetPoints()->GetPoint(i, p);
    pointList[i].pointId = i;
    pointList[i].x = p[0];
    pointList[i].y = p[1];
    pointList[i].z = p[2];
    }

  cellList = new cell[numCells];
  for(i=0; i < numCells; i++)
    cellList[i].cellId = -1;

  vtkIdList *list = vtkIdList::New();
  for(i=0; i < numCells; i++)
    {
    input->GetCellPoints(i, list);
    if( list->GetNumberOfIds() == 4 )
      {
      cellList[i].cellId = i;
      cellList[i].v[0] = list->GetId(0);
      cellList[i].v[1] = list->GetId(1);
      cellList[i].v[2] = list->GetId(2);
      cellList[i].v[3] = list->GetId(3);
      }
    list->Reset();

    this->setMaximumDistance(&this->distMeta, pointList, cellList[i].v[0], cellList[i].v[1], cellList[i].v[2], cellList[i].v[3]);
    }
  list->Delete();

  /* Computing Center Point  of each Cell */
//  printf("\nComputing Center Point of each Cell...");
  this->computingPointCenter(cellList, pointList, numCells);
//  printf("\t\t[ OK ]");

  /* Sorting the cells by the center points and Write the Metacells */
//  printf("\nSorting Data...");
  sortList = new HMsort[numCells];

  // Sorting the 'x' axis
  this->copyStructureX(sortList, cellList, 0, numCells);
  this->quicksort(sortList, 0, numCells-1);

  // Sorting the 'y' axis after first split (2 pieces)
  this->copyStructureY(sortList, cellList, 0, numCells);
  this->quicksort(sortList, 0, numCells/2);
  this->quicksort(sortList, (numCells/2) + 1, numCells-1);


  // Sorting the 'z' axis after second split (4 pieces)
  int n = numCells / 4;
  this->copyStructureZ(sortList, cellList, 0, numCells);
  this->quicksort(sortList,      0,   n);
  this->quicksort(sortList,    n + 1,   2*n);
  this->quicksort(sortList,  2*n + 1,     3*n);
  this->quicksort(sortList,  3*n + 1, 4*n - 1);
//  printf("\t\t\t\t\t[ OK ]");

  // Set index Name
  strcpy(indexName, this->Path);
  strcat(indexName, "_index.txt");

  // Delete the old File to build a new one
  //    remove(indexName);
  this->writeMetaCell(sortList, cellList, pointList, numCells,          0,           n/2, 1);
  this->writeMetaCell(sortList, cellList, pointList, numCells,    (n/2) + 1,             n, 2);
  this->writeMetaCell(sortList, cellList, pointList, numCells,        n + 1, (n + 1) + n/2, 3);
  this->writeMetaCell(sortList, cellList, pointList, numCells,  (n + 2) + n/2,            2*n, 4);
  this->writeMetaCell(sortList, cellList, pointList, numCells,      2*n + 1, 2*n + 1  + n/2, 5);
  this->writeMetaCell(sortList, cellList, pointList, numCells,  2*n + 2 + n/2,          3*n, 6);
  this->writeMetaCell(sortList, cellList, pointList, numCells,      3*n + 1,  3*n + 1 + n/2, 7);
  this->writeMetaCell(sortList, cellList, pointList, numCells,  3*n + 2 + n/2,      4*n - 1, 8);

  // Writing Maximum distance between two vertices
  FILE *findex = fopen(indexName, "a+");
  fprintf(findex, "MaxDistance %f", this->distMeta);
  fprintf(findex, "\nVelocityVectorPosition:");
  for(i = 1; i <= 8; i++) fprintf(findex, "\n%d", this->localPosition[i]);
  fclose(findex);

/*
  // Cell Intervals for each MetaCell //
  printf("\n\nCell Intervals for each MetaCell:\n");
  printf("\n%5d .. %5d",     0,           n/2, 1);
  printf("\n%5d .. %5d",  (n/2) + 1,             n, 2);
  printf("\n%5d .. %5d",      n + 1, (n + 1) + n/2, 3);
  printf("\n%5d .. %5d",  (n + 2) + n/2,            2*n, 4);
  printf("\n%5d .. %5d",    2*n + 1, 2*n + 1  + n/2, 5);
  printf("\n%5d .. %5d",  2*n + 2 + n/2,          3*n, 6);
  printf("\n%5d .. %5d",    3*n + 1,  3*n + 1 + n/2, 7);
  printf("\n%5d .. %5d",  3*n + 2 + n/2,      4*n - 1, 8);
*/

  /* Reading Vector Field */
//  printf("\n\nAppending vector fields to Metacell files...\n");

  //int metaNumber, time, pos, meshId;
  int metaNumber, time, meshId;
  int metaNumPoints;
  //char metaName[256], aux1[30],aux2[30],aux3[30];
  char metaName[512],aux2[30];
  double vector[7];
  vtkDoubleArray *degreeOfFreedom = input->GetDegreeOfFreedom();

  int * binaryList;
  binaryList = new int[numPoints];
  this->timeSteps = input->GetNumberOfBlocks();

  for(metaNumber=1; metaNumber <= 8; metaNumber++) // Number of Metacells Files
    {
    this->setMetaCellFileName(metaName, metaNumber);
    FILE *fmeta = fopen(metaName, "a+");

    metaNumPoints = this->getNumPoinsFromFile(metaNumber);

//    printf("Appending vector fields data to file %s\n", metaName);
    this->setBinaryList(metaName, binaryList, numPoints, metaNumPoints);

    for(time = 0; time < this->timeSteps; time++) // Number of Times in the Main File
      {
      for (i = -1; i < numPoints; i++)  // Number of Points in each Time
        {
        if(i == -1)
          {
          sprintf(aux2, "%d%s", time, " Velocity/Pressure");
          fprintf(fmeta, "\n%s\n", aux2);
          }
        else
          {
          meshId = input->GetMeshId()->GetId( pointList[i].pointId );
          meshId = meshId + (input->GetNumberOfPoints() * time);
          degreeOfFreedom->GetTuple(meshId, vector);

          if(binaryList[i])
            {
            fprintf(fmeta, "%d %f %f %f %f\n",i, vector[0], vector[1],vector[2], vector[3]);
            }
          }
        }

      }
    fclose(fmeta);
    }

  delete [] binaryList;
  delete [] cellList;
  delete [] pointList;
  delete [] sortList;

  //********************** METACELLS CREATION - END ************************************
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriterCUDA::WriteParticles()
{
  double timeInitialization; // Tempo gasto para inicialização e carga da geometria em memória da CPU
  double timeCopyInitialDataMemToDevice; // Tempo gasto para a cópia da geometria e vetor de trajetória para a GPU
  double timeCopyStepsMemToDevice; // Tempo gasto para a cópia de velocidade e pressão para a GPU (incrementado a cada step)
  double timeCopyResultsDeviceToMem; // Tempo gasto para a cópia dos resultados da GPU para CPU
  double timeProcessing; // Tempo gasto para a calcular as trajetórias (todos os steps)
  double timeRealeaseVariables; // Tempo gasto para desalocar variáveis CUDA e locais

  double ti, tf, ti_proc, tf_proc; // ti = tempo inicial  tf = tempo final
  timeval time_ini, time_end,
          time_ini_proc, time_end_proc;

  timeInitialization = timeCopyInitialDataMemToDevice =
  timeCopyStepsMemToDevice = timeCopyResultsDeviceToMem =
  timeProcessing = timeRealeaseVariables = 0;

  ti = tf = ti_proc = tf_proc = 0;



  // Inicialização de variáveis e carga da geometria para a memória da CPU
  //--------------------------------------------------------------------------------------------------
  gettimeofday(&time_ini,NULL);


  vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );

  this->totalPoints = input->GetNumberOfPoints();
  this->timeSteps = input->GetNumberOfBlocks();
  this->totalCells = input->GetNumberOfCells();

  // Divide-se por 4 para alocar praticamente o dobro do tamanho necessário
  this->maxMetaPoints = this->totalPoints / 4;


  // Initializing global List that loads one time indexed (for search purposes)
  this->vecListSearch = new point[this->totalPoints];

  this->setMaximumDistancePart();

  int size, tmp, startCycle;
  this->startTime = 0;

  double bounds[6];
  input->GetBounds(bounds);

  // Aloca e inicializa o vetor de trajetórias
  size = this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps;
  this->Host_ParticlePath = new point[ size ];

  for(int i=0; i < size; i++)
    {
    double point[3];

    // Putting point position out of bounds
    point[0] = (bounds[1] - bounds[0]) * 4; // (xMax - xMin) * 4
    point[1] = (bounds[3] - bounds[2]) * 4; // (yMax - yMin) * 4
    point[2] = (bounds[5] - bounds[4]) * 4; // (zMax - zMin) * 4


    if( i < this->NumberOfParticles )
      {
      this->InitialPositionOfParticles->GetPoint(i, point);
      this->Host_ParticlePath[ i ].pointId = 1;// Setting valid position
      }
    else
      {
      this->Host_ParticlePath[ i ].pointId = 0; // Setting invalid position
      }


    this->Host_ParticlePath[ i ].x = float(point[0]);
    this->Host_ParticlePath[ i ].y = float(point[1]);
    this->Host_ParticlePath[ i ].z = float(point[2]);

    this->Host_ParticlePath[ i ].velocityX = 0.0;
    this->Host_ParticlePath[ i ].velocityY = 0.0;
    this->Host_ParticlePath[ i ].velocityZ = 0.0;
    this->Host_ParticlePath[ i ].pressure = 0.0;
    this->Host_ParticlePath[ i ].time = 0.0;
    }


  //Lê a geometria das meta células
  this->readMetaCellsGeometry();




  // Alocações de memória em GPU
  cudaMalloc((void**)&this->Device_ParticlePath,
              sizeof(point) * (this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps));
  CHECK_CUDA_ERROR();

  cudaMalloc((void**)&this->Device_fullPoints, sizeof(point) * this->totalPoints);
  CHECK_CUDA_ERROR();

  cudaMalloc((void**)&this->Device_fullCells, sizeof(cell) * this->totalCells);
  CHECK_CUDA_ERROR();

  cudaMalloc((void**)&this->Device_metaCells, sizeof(gpuMetaCell) * 9);
  CHECK_CUDA_ERROR();

  cudaMalloc((void**)&this->Device_vecListSearch, sizeof(point) * this->totalPoints);
  CHECK_CUDA_ERROR();

  gettimeofday(&time_end, NULL);
  tf = (double)time_end.tv_usec + ((double)time_end.tv_sec * (1000000.0));
  ti = (double)time_ini.tv_usec + ((double)time_ini.tv_sec * (1000000.0));
  timeInitialization = (tf - ti) / 1000;
  //--------------------------------------------------------------------------------------------------





  // Usado para medição do tempo de cópia de dados para a GPU
  //--------------------------------------------------------------------------------------------------
  gettimeofday(&time_ini,NULL);

  cudaMemcpy( this->Device_ParticlePath, this->Host_ParticlePath,
              sizeof(point) * (this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps), cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();

  cudaMemcpy( this->Device_fullPoints, this->Host_fullPoints, sizeof(point) * this->totalPoints, cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();

  cudaMemcpy( this->Device_fullCells, this->Host_fullCells, sizeof(cell) * this->totalCells, cudaMemcpyHostToDevice );
  CHECK_CUDA_ERROR();

  cudaMemcpy(this->Device_metaCells, this->Host_metaCells, sizeof(gpuMetaCell) * 9, cudaMemcpyHostToDevice);
  CHECK_CUDA_ERROR();

  gettimeofday(&time_end, NULL);
  tf = (double)time_end.tv_usec + ((double)time_end.tv_sec * (1000000.0));
  ti = (double)time_ini.tv_usec + ((double)time_ini.tv_sec * (1000000.0));
  timeCopyInitialDataMemToDevice = (tf - ti) / 1000;
  //--------------------------------------------------------------------------------------------------




  // Usado para medição do tempo de processamento
  //--------------------------------------------------------------------------------------------------
  gettimeofday(&time_ini_proc,NULL);

  startCycle = 0;
  for(int r=1; r <= this->NumberOfRepeatTimeSteps; r++)
    {
    for(int t=0; t < (this->timeSteps-1); t++)
      {
      //carrega velocidade e pressão para o step corrente
      this->loadMetaCellsVectors(t);

      gettimeofday(&time_ini, NULL);

      // Envia dados para a GPU
      cudaMemcpy( this->Device_vecListSearch, this->vecListSearch, sizeof(point) * this->totalPoints, cudaMemcpyHostToDevice );
      CHECK_CUDA_ERROR();

      gettimeofday(&time_end, NULL);
      tf = (double)time_end.tv_usec + ((double)time_end.tv_sec * (1000000.0));
      ti = (double)time_ini.tv_usec + ((double)time_ini.tv_sec * (1000000.0));
      timeCopyStepsMemToDevice += (tf - ti) / 1000;



      // Call the kernel
      tmp = t + ( (r-1) * this->timeSteps );
      ExecuteKernel_DispatcherThreadsGPU(this->Device_ParticlePath,
                                         this->Device_metaCells,
                                         this->Device_vecListSearch,
                                         this->NumberOfParticles,
                                         tmp,
                                         this->distPart,
                                         this->Delta,
                                         this->Device_fullCells,
                                         this->Device_fullPoints,
                                         r,
                                         startCycle,
                                         this->nBlocks);
      CHECK_CUDA_ERROR();

      startCycle = 0;
      }
    startCycle = 1;
    }

  gettimeofday(&time_end_proc,NULL);
  tf_proc = (double)time_end_proc.tv_usec + ((double)time_end_proc.tv_sec * (1000000.0));
  ti_proc = (double)time_ini_proc.tv_usec + ((double)time_ini_proc.tv_sec * (1000000.0));
  timeProcessing = (tf_proc - ti_proc) / 1000;
  timeProcessing = timeProcessing - timeCopyStepsMemToDevice;
  //--------------------------------------------------------------------------------------------------



  // Usado para medição do tempo de cópia de dados GPU to Host
  //--------------------------------------------------------------------------------------------------
  gettimeofday(&time_ini, NULL);

  cudaMemcpy(this->Host_ParticlePath, this->Device_ParticlePath,
             sizeof(point) * (this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps), cudaMemcpyDeviceToHost);
  CHECK_CUDA_ERROR();

  gettimeofday(&time_end,NULL);
  tf = (double)time_end.tv_usec + ((double)time_end.tv_sec * (1000000.0));
  ti = (double)time_ini.tv_usec + ((double)time_ini.tv_sec * (1000000.0));
  timeCopyResultsDeviceToMem = (tf - ti) / 1000;
  //--------------------------------------------------------------------------------------------------






  //Desaloca os vetores
  //--------------------------------------------------------------------------------------------------
  gettimeofday(&time_ini, NULL);

  cudaFree(this->Device_ParticlePath);
  CHECK_CUDA_ERROR();

  cudaFree((void*)this->Device_metaCells);
  CHECK_CUDA_ERROR();

  cudaFree((void*)this->Device_vecListSearch);
  CHECK_CUDA_ERROR();

  cudaFree((void*)this->Device_fullPoints);
  CHECK_CUDA_ERROR();

  cudaFree((void*)this->Device_fullCells);
  CHECK_CUDA_ERROR();


  delete [] this->vecListSearch;


  gettimeofday(&time_end,NULL);
  tf = (double)time_end.tv_usec + ((double)time_end.tv_sec * (1000000.0));
  ti = (double)time_ini.tv_usec + ((double)time_ini.tv_sec * (1000000.0));
  timeRealeaseVariables = (tf - ti) / 1000;
  //--------------------------------------------------------------------------------------------------


//  printf("\t");
  printf("%.5f ", timeInitialization);
  printf("%.5f ", timeCopyInitialDataMemToDevice);
  printf("%.5f ", timeCopyStepsMemToDevice);
  printf("%d ", this->nBlocks);
  printf("%.5f ", timeProcessing);
  printf("%.5f ", timeCopyResultsDeviceToMem);
  printf("%.5f ", timeRealeaseVariables);
  printf("%.5f ", timeInitialization + timeCopyInitialDataMemToDevice +
                   timeCopyStepsMemToDevice + timeProcessing +
                   timeCopyResultsDeviceToMem + timeRealeaseVariables);


//  printf("\n");
//  printf("1- Inicialização e carga da geometria na memória da CPU: %.5f ms\n", timeInitialization);
//  printf("2- Cópia de dados para a GPU (dados iniciais): %.5f ms\n", timeCopyInitialDataMemToDevice);
//  printf("3- Cópia de dados para a GPU (steps): %.5f ms\n", timeCopyStepsMemToDevice);
//  printf("4- Blocos de processamento na GPU: %2d\n", this->nBlocks);
//  printf("5- Total de processamento dos steps: %.5f ms\n", timeProcessing);
//  printf("6- Cópia de dados para a CPU (trajetórias): %.5f ms\n", timeCopyResultsDeviceToMem);
//  printf("7- Liberação de memória GPU e CPU: %.5f ms\n", timeRealeaseVariables);
//  printf("8- Total de processamento (1+2+3+5+6+7): %.5f ms\n", timeInitialization +
//                                                               timeCopyInitialDataMemToDevice +
//                                                               timeCopyStepsMemToDevice +
//                                                               timeProcessing +
//                                                               timeCopyResultsDeviceToMem +
//                                                               timeRealeaseVariables);
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::SetPath(char* dir)
{
  sprintf(this->Path, "%s", dir);
}

//----------------------------------------------------------------------------
char* vtkHM3DParticleTracingWriterCUDA::GetPath()
{
  return this->Path;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::GetPositionOfParticlesInAllTimeSteps(point **vec2)
{
  int part_Id, step_Id, vec_id, host_id, size, totalSteps;


  size = this->NumberOfParticles * this->timeSteps * this->NumberOfRepeatTimeSteps;
  totalSteps = this->timeSteps * this->NumberOfRepeatTimeSteps;



  *vec2 = new point[ size ];
  point *vec = *vec2;


  //Conversão para o formato de posições usado pelo filtro para escrever os arquivos EnSight
  //Para cada partícula são informados sequencialmente todas suas posições em todos os steps
  // part01_Step0 | part01_Step1 | part01_Step2 | part02_Step0 | part02_Step1 | part02_Step2 |
  for(part_Id = 0; part_Id < this->NumberOfParticles; part_Id++)
    {
    for(step_Id = 0; step_Id < totalSteps; step_Id++)
      {
      vec_id = step_Id + part_Id * this->timeSteps * this->NumberOfRepeatTimeSteps;
      host_id = part_Id + (step_Id * this->NumberOfParticles);

//      printf("vec_id: %d \t host_id: %d\n", vec_id, host_id);


      // No step 0 os vetores (velocidade e pressão) foram inicializados com zero.
      // Por sugestão do Pablo os valores destes vetores no step 0 devem ser substítuídos pelos calculados no step 1.
      // Pois as simulações não tem pontos com valores zerados e sim inicializados com os valores de referência
      // da literatura no caso de não existirem medições coletadas do paciente.
      if( step_Id == 0 )
        {
        vec[ vec_id ].pointId =     this->Host_ParticlePath[ host_id ].pointId;
        vec[ vec_id ].x =           this->Host_ParticlePath[ host_id ].x;
        vec[ vec_id ].y =           this->Host_ParticlePath[ host_id ].y;
        vec[ vec_id ].z =           this->Host_ParticlePath[ host_id ].z;

        vec[ vec_id ].time =        this->Host_ParticlePath[ host_id ].time;


        host_id = part_Id + ( (step_Id+1) * this->NumberOfParticles );

        vec[ vec_id ].velocityX =   this->Host_ParticlePath[ host_id ].velocityX;
        vec[ vec_id ].velocityY =   this->Host_ParticlePath[ host_id ].velocityY;
        vec[ vec_id ].velocityZ =   this->Host_ParticlePath[ host_id ].velocityZ;
        vec[ vec_id ].pressure =    this->Host_ParticlePath[ host_id ].pressure;
        }
      else
        {
        vec[ vec_id ].pointId =     this->Host_ParticlePath[ host_id ].pointId;
        vec[ vec_id ].x =           this->Host_ParticlePath[ host_id ].x;
        vec[ vec_id ].y =           this->Host_ParticlePath[ host_id ].y;
        vec[ vec_id ].z =           this->Host_ParticlePath[ host_id ].z;

        vec[ vec_id ].velocityX =   this->Host_ParticlePath[ host_id ].velocityX;
        vec[ vec_id ].velocityY =   this->Host_ParticlePath[ host_id ].velocityY;
        vec[ vec_id ].velocityZ =   this->Host_ParticlePath[ host_id ].velocityZ;
        vec[ vec_id ].pressure =    this->Host_ParticlePath[ host_id ].pressure;
        vec[ vec_id ].time =        this->Host_ParticlePath[ host_id ].time;
        }
      }
    }
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::SetNumberOfMetaCells(int n)
{
  this->NumberOfMetaCells = n;
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriterCUDA::GetNumberOfMetaCells()
{
  return this->NumberOfMetaCells;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::SetParticles(vtkPoints* pos)
{
  if(!this->InitialPositionOfParticles)
    {
    this->InitialPositionOfParticles = vtkPoints::New();
    this->InitialPositionOfParticles->DeepCopy(pos);
    }
  else
    {
    this->InitialPositionOfParticles->Reset();
    this->InitialPositionOfParticles->DeepCopy(pos);
    }

  this->LastPositions = new point[this->InitialPositionOfParticles->GetNumberOfPoints()];
}

//----------------------------------------------------------------------------
vtkPoints* vtkHM3DParticleTracingWriterCUDA::GetParticles()
{
  return this->InitialPositionOfParticles;
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::WriteConfigFile()
{
  char configName[512];
  int initialTime = 1;
  int finalTime = this->NumberOfParticles;

  strcpy(configName, this->GetPath());
  strcat(configName, "InformationFile.par");

  //  remove(configName);
  FILE *fpar = fopen(configName, "w");

  fprintf(fpar, "*NUMBER OF TIME STEPS\n");
  fprintf(fpar, "%3d\n\n", this->timeSteps);

  fprintf(fpar, "*INITIAL PARTICLE NUMBER FILE\n");
  fprintf(fpar, "%3d\n\n", initialTime);

  fprintf(fpar, "*FINAL PARTICLE NUMBER FILE\n");
  fprintf(fpar, "%3d\n\n", finalTime);

  fclose(fpar);
}

//----------------------------------------------------------------------------
/*******************************************************************************/
/* Functions - MetaCells */
/*******************************************************************************/
// Auxiliar structure that receive information of valid points
// of each metacell (Vector Field Stage)
void vtkHM3DParticleTracingWriterCUDA::setBinaryList(char *metaName, int *binaryList, int numPoints, int metaNumPoints)
{
  int i, pId;
  float aux;

  FILE *f = fopen(metaName, "r");

  for(i = 0; i < numPoints; i++)
    binaryList[i] = 0;

  for(i = 0; i < metaNumPoints; i++)
    {
    fscanf(f, "%d %f %f %f",&pId, &aux, &aux,&aux);
    binaryList[pId] = 1;
    }

  fclose(f);
}

//----------------------------------------------------------------------------
// Error message for open File error operation
void vtkHM3DParticleTracingWriterCUDA::openFileError(FILE *findex)
{
  if(!findex)
    {
    printf("\n\n ** Error when open file!!! ** \n\n");
    exit(1);
    }
}

//----------------------------------------------------------------------------
// Find the "number of points" information in the _index.txt file
int vtkHM3DParticleTracingWriterCUDA::getNumPoinsFromFile(int metaNumber)
{
  int num,i;
  char aux[512], indexName[512];

  strcpy(indexName, this->GetPath());
  strcat(indexName, "_index.txt");

  FILE *findex = fopen(indexName, "r");
  this->openFileError(findex);

  for(i = 1; i <= 8; i++)
    {
    fscanf(findex, "%s %d", aux, &num);
    if(i == metaNumber)
      {
      fclose(findex);
      return num;
      }
    fscanf(findex, "%s %s %s %s %s %s %s", &aux, &aux, &aux, &aux, &aux, &aux, &aux);
    }
  fclose(findex); //add
  return 0;
}

//----------------------------------------------------------------------------
// Set MetaCell File Name
void vtkHM3DParticleTracingWriterCUDA::setMetaCellFileName(char *nome, int n)
{
  char num[10];
  sprintf(num, "%d", n);
  strcpy(nome, this->GetPath());
  strcat(nome, "metacell");
  strcat(nome, "00");
  strcat(nome, num);
  strcat(nome, ".txt");
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::writeMetaCell(HMsort *sortList, cell * cellList, point *pointList, int num, int begin, int end, int metaNumber)
{
  char metaName1[512];
  this->setMetaCellFileName(metaName1, metaNumber);

  FILE *fmeta = fopen(metaName1,  "w+");

  //printf("\nCreating file %s", metaName1);

  int i, j, id, v[4], localNumberOfPoints = 0, localNumberOfCells;
  float xmin, xmax, ymin, ymax, zmin, zmax;
  int * binaryList;
  //  binaryList = (int *) malloc(num * sizeof(int));
  binaryList = new int[num];

  // Set binaryList to zero in each position
  for(i = 0; i < num; i++) binaryList[i] = 0;

  // Fill binaryList avoiding duplication
  for( i = begin; i <= end; i++)
    {
    id = sortList[i].cellId;
    for(j = 0; j < 4; j++)
      {
      v[j] = cellList[ id ].v[j];
      if( !binaryList[ v[j] ] ) binaryList[ v[j] ] = 1;
      }
    }

  int firstMinMax = 0;

  // Put all marked points in the metacell File
  for(i = 0; i < num; i++)
    {
    if(binaryList[i])
      {
      localNumberOfPoints++;
      this->setMinMaxValues(firstMinMax, i, pointList, &xmin, &xmax, &ymin, &ymax, &zmin, &zmax);
      fprintf(fmeta, "\n%d %f %f %f", pointList[i].pointId, pointList[i].x, pointList[i].y, pointList[i].z);
      if(!firstMinMax) firstMinMax = 1;
      }
    }

  fprintf(fmeta, "\n%s %d\n\n","Total:", localNumberOfPoints);

  // Put all cells in the metacell file
  for( i = begin; i <= end; i++)
    {
    id = sortList[i].cellId;
    fprintf(fmeta, "\n%d %d %d %d", cellList[ id ].v[0], cellList[id ].v[1], cellList[ id ].v[2], cellList[ id ].v[3]);
    }
  localNumberOfCells = end - begin + 1;
  fprintf(fmeta, "\n%s %d\n\n", "Total:", localNumberOfCells); // Number of Cells of the file

  this->localPosition[metaNumber] = ftell(fmeta);

  delete [] binaryList;

  this->writeMetaIndexFile(metaName1,localNumberOfPoints, localNumberOfCells, xmin, xmax, ymin, ymax, zmin, zmax);
  fclose(fmeta);

//  printf("\t\t\t[ OK ]");
}

//----------------------------------------------------------------------------
// Compute and Write Metacells Index File
void vtkHM3DParticleTracingWriterCUDA::writeMetaIndexFile(char *metaName, int nPoints, int nCells, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax)
{
  char indexName[512];
  strcpy(indexName, this->GetPath());
  strcat(indexName, "_index.txt");
  FILE *findex = fopen(indexName, "a+");
  fprintf(findex, "%s\t%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\n", metaName, nPoints, nCells, xmin, xmax, ymin, ymax, zmin, zmax);
  fclose(findex);
}

//----------------------------------------------------------------------------
// Set Mininum and Maximum value for each Metacell
void vtkHM3DParticleTracingWriterCUDA::setMinMaxValues(int first, int i, point* pointList, float *xmin, float *xmax, float *ymin, float *ymax, float *zmin, float *zmax)
{
  if(!first) // First Time
    {
    * xmin = pointList[i].x;
    * xmax = pointList[i].x;
    * ymin = pointList[i].y;
    * ymax = pointList[i].y;
    * zmin = pointList[i].z;
    * zmax = pointList[i].z;
    }
  else
    {
    * xmin = (pointList[i].x < * xmin) ? pointList[i].x : * xmin;
    * xmax = (pointList[i].x > * xmax) ? pointList[i].x : * xmax;
    * ymin = (pointList[i].y < * ymin) ? pointList[i].y : * ymin;
    * ymax = (pointList[i].y > * ymax) ? pointList[i].y : * ymax;
    * zmin = (pointList[i].z < * zmin) ? pointList[i].z : * zmin;
    * zmax = (pointList[i].z > * zmax) ? pointList[i].z : * zmax;

    }

}

//----------------------------------------------------------------------------
// First step to build Metacell - Copying the X coordenates to sort
void vtkHM3DParticleTracingWriterCUDA::copyStructureX(HMsort *sortList, cell * cellList, int begin, int end)
{
  int i;
  for( i = begin; i < end; i++)
    {
    sortList[i].cellId = cellList[i].cellId;
    sortList[i].vertex = cellList[i].center.x;
    }
}

//----------------------------------------------------------------------------
// Second step to build Metacell - Copying the Y coordenates to sort
void vtkHM3DParticleTracingWriterCUDA::copyStructureY(HMsort *sortList, cell * cellList, int begin, int end)
{
  int i, id;
  for( i = begin; i < end; i++)
    {
    id = sortList[i].cellId;
    sortList[i].vertex = cellList[ id ].center.y;
    }
}

//----------------------------------------------------------------------------
// Third step to build Metacell - Copying the Z coordenates to sort
void vtkHM3DParticleTracingWriterCUDA::copyStructureZ(HMsort *sortList, cell * cellList, int begin, int end)
{
  int i, id;
  for( i = begin; i < end; i++)
    {
    id = sortList[i].cellId;
    sortList[i].vertex = cellList[ id ].center.z;
    }
}

//----------------------------------------------------------------------------
// Computing the center points of the cells (tetrahedron)
void vtkHM3DParticleTracingWriterCUDA::computingPointCenter(cell * cellList, point * pList, int numCells)
{
  int i;
  int p0, p1, p2, p3;
  for(i=0; i < numCells; i++)
    {
    cellList[i].center.pointId = i;
    p0 = cellList[i].v[0];
    p1 = cellList[i].v[1];
    p2 = cellList[i].v[2];
    p3 = cellList[i].v[3];

    cellList[i].center.x = ( pList[ p0 ].x + pList[ p1 ].x + pList[ p2 ].x + pList[ p3 ].x) / 4;
    cellList[i].center.y = ( pList[ p0 ].y + pList[ p1 ].y + pList[ p2 ].y + pList[ p3 ].y) / 4;
    cellList[i].center.z = ( pList[ p0 ].z + pList[ p1 ].z + pList[ p2 ].z + pList[ p3 ].z) / 4;
    }
}

//----------------------------------------------------------------------------
// Compute Euclidean Distance
float vtkHM3DParticleTracingWriterCUDA::computeDistance(point p1, point p2)
{
  float x1, y1, z1, x2, y2, z2;
  x1 = p1.x;  y1 = p1.y;  z1 = p1.z;
  x2 = p2.x;  y2 = p2.y;  z2 = p2.z;

  return (float) sqrt(pow(x1-x2,2) + pow(y1-y2,2) + pow(z1-z2,2));
}

//----------------------------------------------------------------------------
// Encontra distancia da maior aresta de cada célula, para auxiliar na busca
// dos tetraedros de interesse no momento de calcular o traçado da uma partícula
void vtkHM3DParticleTracingWriterCUDA::setMaximumDistance(float *d, point *pList, int v0, int v1, int v2, int v3)
{
  float aux;
  aux = this->computeDistance(pList[ v0 ], pList[ v1 ]); if(aux > *d) *d = aux;
  aux = this->computeDistance(pList[ v0 ], pList[ v2 ]); if(aux > *d) *d = aux;
  aux = this->computeDistance(pList[ v0 ], pList[ v3 ]); if(aux > *d) *d = aux;
  aux = this->computeDistance(pList[ v1 ], pList[ v2 ]); if(aux > *d) *d = aux;
  aux = this->computeDistance(pList[ v1 ], pList[ v3 ]); if(aux > *d) *d = aux;
  aux = this->computeDistance(pList[ v2 ], pList[ v3 ]); if(aux > *d) *d = aux;
}

//----------------------------------------------------------------------------
/*******************************************************************************/
/* QuickSort */
/*******************************************************************************/
void vtkHM3DParticleTracingWriterCUDA::swap(HMsort *a, int i, int j)
{
  float tmp = a[i].vertex;
  a[i].vertex = a[j].vertex;
  a[j].vertex = tmp;

  int cellId = a[i].cellId;
  a[i].cellId = a[j].cellId;
  a[j].cellId = cellId;
}

//----------------------------------------------------------------------------
int vtkHM3DParticleTracingWriterCUDA::Random(int i, int j)
{
  return i + rand() % (j-i+1);
}

//----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::quicksort(HMsort *a, int left, int right)
{
  int last = left, i;

  if (left >= right) return;

  this->swap(a,left,this->Random(left,right));
  for (i = left + 1; i <= right; i++)
    if (a[i].vertex < a[left].vertex)
      this->swap(a,++last,i);
  this->swap(a,left,last);
  this->quicksort(a,left,last-1);
  this->quicksort(a,last+1,right);
}


//----------------------------------------------------------------------------
// Set MetaCell File Name
void vtkHM3DParticleTracingWriterCUDA::setFileName(char *nome, int n, int op)
{
  char num[6];
  sprintf(num, "%03d", n);
  strcpy(nome,this->GetPath());
  if(op == 1) strcat(nome, "metacell");
  if(op == 2) strcat(nome, "particle");
  if(op == 3)
    {
    strcat(nome, "_index.txt");
    return;
    }
  strcat(nome, num);
  strcat(nome, ".txt");
}

//----------------------------------------------------------------------------
// Return the number of points and cells written in the _index.txt file
void vtkHM3DParticleTracingWriterCUDA::getNumberOfPointsAndCells(int metaNumber, int *numPoints, int *numCells)
{
  char indexName[256];
  this->setFileName(indexName, 0, 3);
  FILE *f = fopen(indexName, "r");

  char aux[256];
  float faux;

  for(int i = 1; i <= 8; i++)
    {
    //fprintf("\nMetaCellFile Number_Points Number_Cells  xmin    xmax    ymin    ymax    zmin    zmax");
    fscanf(f, "%s %d %d %f %f %f %f %f %f", aux, numPoints, numCells, &faux, &faux, &faux, &faux, &faux, &faux);
    if(i == metaNumber) break;
    }

  fclose(f);
}

//----------------------------------------------------------------------------
// Set global variable "dist" for general purposes
void vtkHM3DParticleTracingWriterCUDA::setMaximumDistancePart()
{
  char indexName[256];
  this->setFileName(indexName, 0, 3);
  FILE *findex = fopen(indexName, "r");

  int i;
  float faux;
  char aux[256];

  for(i = 1; i <= 8; i++)
    fscanf(findex, "%s %s %s %f %f %f %f %f %f", aux, aux, aux, &faux, &faux, &faux, &faux, &faux, &faux);

  fscanf(findex, "%s %f", aux, &this->distPart);
  fscanf(findex, "%s", aux); // Reading VelocityVectorPositionLabel

  for(i = 1; i <= 8; i++)
    fscanf(findex, "%d", &this->localPosition[i]); // Load Global Vector

  fclose(findex);

  //  printfCandidates(p, metaVec);
}

//----------------------------------------------------------------------------
// Read all Metacells geometry
void vtkHM3DParticleTracingWriterCUDA::readMetaCellsGeometry()
{
  char metaName[256], aux[256];
  int numPoints, numCells, cellId, pointId;


 // printf("\nReading all Metacells geometry ...");


  this->Host_fullPoints = new point[ this->totalPoints ]; // To allow direct search (any index).
  this->Host_fullCells = new cell[ this->totalCells ]; // To allow direct search (any index).



  for(int i = 0; i < this->totalPoints; i++)
    this->Host_fullPoints[i].pointId = 0;

  for(int i = 0; i < this->totalCells; i++)
    this->Host_fullCells[i].cellId = 0;


  for(int i = 1; i <= 8; i++)  // Como utilizo somente as metacells de 1 até 8, desconsidero a posição 0.
    {
    this->setFileName(metaName, i, 1);
    FILE *fin = fopen(metaName,  "r");

    if(!fin)
      {
      printf("\n\nError: File \"%s\" not found!\n\n", metaName);
      return;
      }

    this->getNumberOfPointsAndCells(i, &numPoints, &numCells);

    this->Host_metaCells[i].numPoints = numPoints;
    this->Host_metaCells[i].numCells  = numCells;


    int pointId = -1;
    for(int j=0; j < numPoints; j++)
      {
      fscanf(fin, "%d",&pointId);
      this->Host_fullPoints[ pointId ].pointId = 1;

      fscanf(fin, "%f",&this->Host_fullPoints[ pointId ].x);
      fscanf(fin, "%f",&this->Host_fullPoints[ pointId ].y);
      fscanf(fin, "%f",&this->Host_fullPoints[ pointId ].z);
      }
    fscanf(fin, "%s %s",aux, aux);



    /* Reading cells */
    //printf("\nReading %d from %s...", numCells, metaName);
    cellId = (i-1)*numCells;
    for(int j=0; j < numCells; j++)
      {
      this->Host_fullCells[ cellId+j ].cellId = cellId+j;
      fscanf(fin, "%d %d %d %d",&this->Host_fullCells[ cellId+j ].v[0],
                                &this->Host_fullCells[ cellId+j ].v[1],
                                &this->Host_fullCells[ cellId+j ].v[2],
                                &this->Host_fullCells[ cellId+j ].v[3]);
      }
    fscanf(fin, "%s %s",aux, aux);


    fclose(fin);
    }



  char indexName[256];
  this->setFileName(indexName, 0, 3);
  FILE *findex = fopen(indexName, "r");
  float xmin[9], xmax[9], ymin[9], ymax[9], zmin[9], zmax[9];

  for(int i = 1; i <= 8; i++)
    {
    //fprintf("\nMetaCellFile Number_Points Number_Cells  xmin    xmax    ymin    ymax    zmin    zmax");
    fscanf(findex, "%s %s %s %f %f %f %f %f %f", aux, aux, aux, &xmin[i], &xmax[i], &ymin[i], &ymax[i], &zmin[i], &zmax[i]);

    this->Host_metaCells[i].bounds[0] = xmin[i];
    this->Host_metaCells[i].bounds[1] = xmax[i];
    this->Host_metaCells[i].bounds[2] = ymin[i];
    this->Host_metaCells[i].bounds[3] = ymax[i];
    this->Host_metaCells[i].bounds[4] = zmin[i];
    this->Host_metaCells[i].bounds[5] = zmax[i];
    }

  fclose(findex);

//  printf("\t\t\t\t[ OK ]\n");
}

//----------------------------------------------------------------------------
// Read only vector`s (velocity and pressure) Metacells for one timeStep
bool vtkHM3DParticleTracingWriterCUDA::loadMetaCellsVectors(int time )
{
  vtkHM3DModelGrid *input = vtkHM3DModelGrid::SafeDownCast( this->GetInput() );


  int meshId, i, j, k, pointId, numCells, initialCell, finalCell;
  double vector[7];


  if( (time < 0) || (time > this->timeSteps) )
    {
    return false;
    }


  for(i=1; i <= 8; i++ )
    {
    numCells = this->Host_metaCells[ i ].numCells;

    initialCell = (i-1) * numCells;
    finalCell = initialCell + numCells;

    for(j = initialCell; j < finalCell; j++)
      {
      for(k=0; k <= 3; k++)
        {
        pointId = this->Host_fullCells[ j ].v[ k ];

        meshId = input->GetMeshId()->GetId( pointId );
        meshId = meshId + (input->GetNumberOfPoints() * time);
        input->GetDegreeOfFreedom()->GetTuple(meshId, vector);

//        cout << "id: " << id << "  meshId: " << meshId << "  i: " << i << "  j: " << j << "  k: " << k << endl;

        this->vecListSearch[ pointId ].pointId = 1;
        this->vecListSearch[ pointId ].velocityX = vector[0];
        this->vecListSearch[ pointId ].velocityY = vector[1];
        this->vecListSearch[ pointId ].velocityZ = vector[2];
        this->vecListSearch[ pointId ].pressure  = vector[3];
        this->vecListSearch[ pointId ].time = time;
        }
      }
    }

  return true;
}

//-----------------------------------------------------------------------------
void vtkHM3DParticleTracingWriterCUDA::printHour()
{
  time_t     now;
  struct tm  *ts;
  char       buf[80];

  // Obtem o tempo corrente
  time(&now);

  // Formata e imprime o tempo, "ddd yyyy-mm-dd hh:mm:ss zzz"
  ts = localtime(&now);
  strftime (buf, 80,"%H:%M:%S.",ts);
  printf("%s\n", buf);
}

//-----------------------------------------------------------------------------
bool vtkHM3DParticleTracingWriterCUDA::HostHasGPUDevice()
{
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, 0);

  // Esta função retorna 9999 para ambos os campos "major" e "minor", caso não exista um device que suporte CUDA
  if (deviceProp.major == 9999 && deviceProp.minor == 9999)
    return false;
  else
    return true;
}

