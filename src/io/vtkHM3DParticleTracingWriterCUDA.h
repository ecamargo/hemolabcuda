/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHM3DParticleTracingWriterCUDA.h,v $

  Copyright (c) Rodrigo Luis de Souza da Silva and Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/// vtkHM3DParticleTracingWriterCUDA é um escritor de trajetória de partículas que utiliza a arquitetura CUDA


#ifndef vtkHM3DParticleTracingWriterCUDA_H_
#define vtkHM3DParticleTracingWriterCUDA_H_

#include "vtkDataWriter.h"
#include "vtkPoints.h"
#include "vtkHMUtils.h" // Adiciona as estruturas usadas


class VTK_EXPORT vtkHM3DParticleTracingWriterCUDA : public vtkDataWriter
{

public:
  vtkTypeRevisionMacro(vtkHM3DParticleTracingWriterCUDA, vtkDataWriter);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  /// Construtor
  static vtkHM3DParticleTracingWriterCUDA *New();

  /// Escreve em disco os arquivos que descrevem as meta-células
  void WriteMetaCells();

  /// Escreve em disco os arquivos que descrevem as partículas
  int WriteParticles();

  /// Configura o diretório de saída dos arquivos
  void SetPath(char* dir);

  /// Recupera o diretório de saída dos arquivos
  char* GetPath();

  /// Set/Get as coordenadas iniciais das partículas
  void SetParticles(vtkPoints* pos);
  vtkPoints* GetParticles();

  /// Configura o número de meta-células
  void SetNumberOfMetaCells(int n);

  /// Recupera o número de meta-células
  int GetNumberOfMetaCells();

  /// Escreve o arquivo de configuração (arquivo auxiliar) em disco
  void WriteConfigFile();

  /// Recupera a posição das partículas em todos os instantes de tempo.
  /// Para cada partícula são informados sequencialmente todas suas posições em todos os steps (e.g part01_Step0 | part01_Step1 | part01_Step2 | part02_Step0 | part02_Step1 | part02_Step2 |)
  void GetPositionOfParticlesInAllTimeSteps(point **vec);

  /// Configura o número de partículas
  vtkSetMacro(NumberOfParticles, int);

  /// Recupera o número de partículas
  vtkGetMacro(NumberOfParticles, int);

  /// Configura a duração do passo (instantes) de tempo.
  vtkSetMacro(Delta, float);

  /// Recupera a duração do passo (instantes) de tempo.
  vtkGetMacro(Delta, float);

  /// Configura o número de repetições do pulso cardíaco.
  void SetNumberOfRepeatTimeSteps(int arg){this->NumberOfRepeatTimeSteps = arg;}

  /// Recupera o número de repetições do pulso cardíaco.
  int GetNumberOfRepeatTimeSteps(){return this->NumberOfRepeatTimeSteps;}

  /// Verifica se o computador onde o programa esta sendo executado possui uma GPU compatível com a arquitetura CUDA
  bool HostHasGPUDevice();

  /// Configura o número de blocos CUDA para execução - usado para testes de desempenho
  bool SetNumBlocks(int arg){ this->nBlocks = arg; }

protected:
  vtkHM3DParticleTracingWriterCUDA();
  virtual ~vtkHM3DParticleTracingWriterCUDA();

  /// Número de repetições do pulso cardíaco.
  int NumberOfRepeatTimeSteps;

  /// O pulso cardíaco será repetido ou não.
  bool IsRepeatPulse;

  /// Diretório de saída dos arquivos
  char Path[256];

  /// Número de meta-células
  int NumberOfMetaCells;

  /// Número de partículas
  int NumberOfParticles;

  /// Armazena as coordenadas imediatamente anteriores de cada partículas.
  /// Usado para saber se uma partícula se movimentou ou não.
  point *LastPositions;

  /// Coordenadas iniciais das partículas
  vtkPoints* InitialPositionOfParticles;

  /// Meta-células
  MetaCell *metaCells;

  /// Número total de pontos da estrutura
  int totalPoints;

  /// Número máximo de pontos para cada meta-célula
  int maxMetaPoints;

  /// Maior distância entre 2 pontos da estrutura.
  float distPart;

  /// Partícula com o cálculo em andamento
  point part;

  /// Número que é usado no nome do arquivo temporáro que descreve a trajetória da partícula
  int partNumber;

  /// Código de erro.
  int error;

  /// Meta-célula em memória.
  int currentMetacell;

  /// Instante de tempo corrente em cálculo.
  int currentTime;

  /// Instante de início da simulação
  int startTime;

  /// Lista global para carregar todos os instantes de tempo para uma meta-célula
  point *vecList;

  /// Lista global para carregar 1 instante de tempo de todas as meta-células
  point *vecListSearch;

  /// Vetor que armazena a posição de início do vetor velocidade em cada arquivo descritor das meta-células
  int localPosition[9];

  /// Número de passos da simulação
  int timeSteps;

  /// Duração de cada passo (instante de tempo) da simulação
  double Delta;

  ///
  float distMeta;

  /// Método auxiliar que configura os nomes de arquivos temporários
  void  setFileName(char *nome, int n, int op);

  /// Recupera a quantidade de pontos e células da meta-célula "metaNumber"
  void  getNumberOfPointsAndCells(int metaNumber, int *numPoints, int *numCells);

  /// Configura as variáveis globais "distPart" e "localPosition"
  void  setMaximumDistancePart();

  /// Lê e armazena na variável 'vecListSearch' os valores de velocidade e pressão de todas as meta-células para um dado instante de tempo
  bool  loadMetaCellsVectors(int timeStep);

  /// Lê e carrega para a memória (CPU) a geometria (pontos e células) de todas as meta-células
  void  readMetaCellsGeometry();


//  // Alphas globais para habilitar a interpolação
//  float a1;
//  float a2;
//  float a3;
//  float a4;
//  /// Carrega todas as meta-células para a memória (CPU)
//  bool fasterMode;
//  /// Verifica se as meta-células já estão em memória (CPU)
//  bool IsMetacellsInMemory;


//  /// Carrega o vetor de velocidade local a partir do vetor de velocidade global. Usado pelo método vtkHM3DParticleTracingWriterCUDA::computeVelocity
//  void loadVelocityVectorFromMemory(int numPoints);
//  /// Carrega a partículas "part" para as variáveis internas a fim de calcular sua trajetória
//  void LoadLastPosition(int part);
//  /// Verifica se a partícula esta dentro (retorna 1) ou fora do intervalo
//  int   checkMetacellIntervals(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);
//  /// Para debug
//  void  printfCandidates(point p, int * metaVec);
//  /// Usa a distância Euclidiana para verificar se a distância entre as coordenadas informadas e a partícula corrente é menor do que "distPart".
//  int   checkMinimumDistance(float x, float y,float z);
//  /// Usa a distância Euclidiana para verificar se a partícula esta próxima da célula informada.
//  int   getCloserCells(point* pointList, cell c);
//  /// Configura os valores dos vértices da célula informada
//  void  setPoints(point* p0, point* p1, point* p2, point* p3, point* p4, point* plist, cell c);
//  /// Calcula a diferença (subtração) entre dois pontos (pa e pb).
//  void  computeVectorOperationSub(point *v, point pa, point pb);
//  /// Configura vetores
//  void  setVector(point* v1, point* v2, point* v3, point* v4, point p0, point p1, point p2, point p3, point p4);
//  /// Cálcula o produto vetorial entre va e vb
//  void  computeCrossProd(point *v, point va, point vb);
//  /// Calcula os pontos 'n' para verificar se a partícula esta dentro ou fora de uma célula
//  void  setNValues(point* n1, point* n2, point* n3, point v2, point v3, point v4);
//  /// Calcula o produto escalar entre va e vb
//  float computeScalarProd(point va, point vb);
//  /// Calcula os valores alpha para verificar se a partícula esta dentro ou fora de uma célula
//  void  setAlphaValues(float *alpha, point n, point va, point vb);
//  /// A partícula esta dentro da célula?
//  int   isInsideCell(point* pointList, cell c);
//  /// Carrega do arquivo o vetor velocidade da meta-célula 'metaNumber'
//  void  loadVelocityVectorFromFile(int metaNumber, int numPoints);
//
//  void  writeTimeInFile(int startTime);
//  /// Calcula a próxima coordenada, velocidad, pressão e tempo da partícula.
//  void  computeVelocity(cell c, int metaNumber, int numPoints);
//  void  computeVelocityFaster(cell c, int metaNumber, int numPoints);
//  /// Encontra células próximas da partícula
//  void  findCloserCells(int metaNumber);
//  void  findCloserCellsFaster(int metaNumber);
//  void  computeTrace(int *metaVec);
//  /// Configura a partícula corrente em sua posição inicial
//  void  callMultiPart(int op);
//  void  readAllMetaCells();
//  /// Para debug
//  void PrintParticlePath(point *path, int nPart, int nSteps, int nRepetitions);
//  void PrintMetaCells(MetaCell *meta, int arg);

  /// Método auxiliar que recebe informação dos pontos válidos de cada meta-célula
  void setBinaryList(char *metaName, int *binaryList, int numPoints, int metaNumPoints);

  /// Mensagem de erro na abertura de um arquivo
  void openFileError(FILE *findex);

  /// Recupera do arquivo _index.txt o número de pontos da meta-célula
  int getNumPoinsFromFile(int metaNumber);

  /// Configura o nome de arquivo para uma meta-célula
  void setMetaCellFileName(char *nome, int n);

  /// Escreve em disco a geometria das meta-células
  void writeMetaCell(HMsort *sortList, cell * cellList, point *pointList, int num, int begin, int end, int metaNumber);

  /// Configura e escreve o arquivo Index das meta-células
  void writeMetaIndexFile(char *metaName, int nPoints, int nCells, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);

  /// Configura os limites (bounds) de cada meta-célula
  void setMinMaxValues(int first, int i, point* pointList, float *xmin, float *xmax, float *ymin, float *ymax, float *zmin, float *zmax);

  /// Primeiro passo para construir as meta-células - Copiar as cooordenadas x para ordenar
  void copyStructureX(HMsort *sortList, cell * cellList, int begin, int end);

  /// Segundo passo para construir as meta-células - Copiar as cooordenadas y para ordenar
  void copyStructureY(HMsort *sortList, cell * cellList, int begin, int end);

  /// Terceiro passo para construir as meta-células - Copiar as cooordenadas z para ordenar
  void copyStructureZ(HMsort *sortList, cell * cellList, int begin, int end);

  /// Calcular o ponto central de cada meta-célula
  void computingPointCenter(cell * cellList, point * pList, int numCells);

  /// Calcula a distância Euclidiana entre 2 pontos
  float computeDistance(point p1, point p2);

  /// Encontra distancia da maior aresta de cada célula, para auxiliar na busca dos tetraedros de interesse no momento de calcular o traçado da uma partícula
  void setMaximumDistance(float *d, point *pList, int v0, int v1, int v2, int v3);



  /**************************************************************************************/
  /* QuickSort Functions */
  /**************************************************************************************/
  /// Auxiliar do quicksort
  void swap(HMsort *a, int i, int j);

  /// Auxiliar do quicksort
  int Random(int i, int j);

  /// Implementa o algoritimo de ornadação Quicksort
  void quicksort(HMsort *a, int left, int right);
  //**************************************************************************************/


  vtkHM3DParticleTracingWriterCUDA(const vtkHM3DParticleTracingWriterCUDA&);  // Not implemented.
  void operator=(const vtkHM3DParticleTracingWriterCUDA&);  // Not implemented.

  /// Total de células da malha de entrada
  int totalCells;

  /// Vetor para armazenar todos os pontos na memória do Host (CPU)
  point *Host_fullPoints;

  /// Vetor para armazenar todos os pontos na memória do Device (GPU)
  point *Device_fullPoints;

  /// Vetor para armazenar todas as células na memória do Host (CPU)
  cell *Host_fullCells;

  /// Vetor para armazenar todas as células na memória do Device (GPU)
  cell *Device_fullCells;

  /// Vetor para armazenar velocidade, pressão e tempo de 1 passo de tempo para todos os pontos na memória do Device (GPU)
  point *Device_vecListSearch;

  /// Estrutra que representa as meta-células em CPU
  gpuMetaCell *Host_metaCells;

  /// Estrutra que representa as meta-células em GPU
  gpuMetaCell *Device_metaCells;

  /// Vetor para armazenar todas as trajetórias das partículas na memória do Host (CPU)
  point *Host_ParticlePath;

  /// Vetor para armazenar todas as trajetórias das partículas na memória do Device (GPU)
  point *Device_ParticlePath;




  //para teste de desempenho
  int nBlocks;


  void printHour();
};
#endif /*vtkHM3DParticleTracingWriterCUDA_H_*/
