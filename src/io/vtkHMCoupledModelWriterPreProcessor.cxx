#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHM1DIniFile.h"
#include "vtkHMCoupledModelWriterPreProcessor.h"
#include "vtkHM1DData.h"
#include "vtkHM1DStraightModelWriter.h"
#include "vtkHM3DFullModel.h"
#include "vtkSurfaceGen.h"
#include "SurfTriang.h"
#include "dyncoo3d.h"
#include "acdp.h"
#include "acdptype.h"
#include "vtkIdList.h"
#include "vtkCell.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DTreeElement.h"
#include "vtkMeshData.h"
#include "vtkHMParallelSolverScriptWriter.h"
#include "vtkPVConnectivityFilter.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include <fstream> 
#include <iostream>

using namespace std;
 
vtkCxxRevisionMacro(vtkHMCoupledModelWriterPreProcessor, "$Revision: 444 $");
vtkStandardNewMacro(vtkHMCoupledModelWriterPreProcessor);

//----------------------------------------------------------------------------
vtkHMCoupledModelWriterPreProcessor::vtkHMCoupledModelWriterPreProcessor()
{
	//this->DebugOn();
	
	this->StraightModel = NULL;
	this->DirichletsTag = NULL;
	this->Dirichlets = NULL;
	this->IniFileDoubleArray = NULL;
	
	this->points = vtkPoints::New();
	this->ElementsIncidence.clear(); 
	this->ElementTypeArray = vtkIntArray::New();
	this->IncidenceElements1D.clear(); 
	this->IniFileArray = vtkDoubleArray::New();
	//this->BasParam = vtkHM1DBasParam::New();
	
	this->NumberOfPoints1D =0;
	this->Max1DElemLib= 0;  
	this->SurfaceElementType = 0;
	this->CoverElementType = 0;
	this->VolumeElementType =0;
	//this->NumGroups = 0;
	this->NumberOfSegments = 0;
	this->point = 0;
	this->BasParamDOFNumber = 7;
	
	this->MediumValuePressure = 0.0;

	this->InFlowElementSet = false;
	
	this->CouplingArrayInfo = NULL;
	
	this->StraightModelVisited = vtkIdList::New();
	
	this->StraightModelVisited2 = vtkIdList::New();
	
	this->FullModelList.clear();
	
	this->FullModelNumber = 0;
	
	this->FullModelNumberOfPoints = vtkIntArray::New();
	
	this->SelectedCouplingInfo = NULL;
	
	this->BasParamElementList = vtkIntArray::New();
	
	this->ScaleFactor3D = 0;
	
	
	this->CouplingTerminalNumber = 0;
	
	this->NumberOfRegionsEachFullModel = vtkIdList::New();
	this->NumberOfRegionsEachFullModel->Reset();
	
	
	this->TerminalFlowDirection = vtkIdList::New();
	this->TerminalFlowDirection->Reset();
	
	this->ModelInfo = vtkIdList::New();
	this->ModelInfo->Reset();
	
	this->ElementMat1D = NULL;
	
	this->SegmentIDList = vtkIdList::New();
	
	this->IDsVisited = vtkIdList::New();
	this->IDsVisited->Reset();
	
	this->Inner1DElements = vtkIdList::New();
	this->Inner1DElements->Reset();
	
	
	this->TotalNumberOfCoordinates = 0;
	
	
}

//----------------------------------------------------------------------------

vtkHMCoupledModelWriterPreProcessor::~vtkHMCoupledModelWriterPreProcessor()
{
	this->points->Delete();
	
	this->ElementTypeArray->Delete();
	
	this->IniFileArray->Delete();
	
	if (this->DirichletsTag) 
		this->DirichletsTag->Delete();
	
	if (this->Dirichlets)
		this->Dirichlets->Delete();
	
	if (this->IniFileDoubleArray)
		this->IniFileDoubleArray->Delete(); 
	
	this->BasParam->Delete();
	
	this->ElementsIncidence.clear();
	
	this->IncidenceElements1D.clear(); 
	
	this->StraightModelVisited->Delete();
	
	this->StraightModelVisited2->Delete();
	
	this->FullModelNumberOfPoints->Delete();
	
	if (this->SelectedCouplingInfo)
		this->SelectedCouplingInfo->Delete();	
		
	this->BasParamElementList->Delete();	
	
	this->NumberOfRegionsEachFullModel->Delete(); 
	
	this->TerminalFlowDirection->Delete();

	this->ModelInfo->Delete();
	
	if ( this->ElementMat1D )
		this->ElementMat1D->Delete();

	this->ElementMat1D = NULL;

	this->SegmentIDList->Delete();
	
	this->IDsVisited->Delete();
	
	this->Inner1DElements->Delete();
	

	

	


}
//----------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
//----------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::InitializeWriteProcess(char *path)
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::InitializeWriteProcess(char *path)");
	
	this->BasParam 						= vtkHM1DBasParam::New();
	vtkHM1DMesh *Mesh 				= vtkHM1DMesh::New();
	vtkHM1DParam *Param 			= vtkHM1DParam::New();
  vtkHM1DIniFile *IniFile 	= vtkHM1DIniFile::New();
  
  
  // gerar coordenadas do modelo 1D indenpendete se mesh sera gerado!
  this->GenerateCoordinates1D(1);
  this->StraightModelVisited->Reset();
  
  this->NumberOfPoints1D = this->GetNumberOfPoints(); // pega o numero de pontos apos arvore 1D tiver sido caminhada
  
  	
	// calculados em generatecoordinates1D
	int NumberOf1DElementsParam = this->NumberOfPoints1D - this->NumberOfSegments;

	// corresponde aos elementos de volume e 1 elem de surface (supondo que a superficie tem as mesmas propriedades )
	this->NumberOf3DElements = this->GetTotalNumberOfVolumeElements(); 
	
	//int NumberOf3DSurfaces = this->GetNumberOf3DFullModels();
	this->NumberOf3DSurfaces = this->GetTotalNumberOfSurfaceGroups();
	
	// elem de acoplamento 54 e 23 (nao tem elementos no Param) é igual ao numero de elementos de tampa
	this->NumberOfCouplingElem = this->GetTotalNumberOfCovers()*2;
	
	
	// elementos 99
	this->NumberOfNullElements = 1;
  
  this->TotalNumberOfElements = NumberOf1DElementsParam + this->NumberOf3DElements + this->NumberOf3DSurfaces + this->NumberOfCouplingElem + this->NumberOfNullElements;
  
	// gera lista de incidencia de elementos 1D
	vtkDebugMacro(<<"Gerando Listas 1d");
	this->Generate1DLists(1);
	
	this->NumberOf1DElements = this->ElementsIncidence.size();
	this->StraightModelVisited->Reset();

	int VolumeElem = this->GetNumberOf3DFullModels();//this->GetNumberOfVolumeElements();
	int SurfElem = this->GetTotalNumberOfSurfaceGroups();
	//int CoverElem = this->GetTotalNumberOfCovers();
	int CouplingElem = this->GetTotalNumberOfCovers();
	int TermCouplingElem = this->GetTotalNumberOfCovers();
	int NullElem = 1;


	// this->Max1DElemLib é atualizado em vtkHMCoupledModelWriterPreProcessor::Generate1DLists(int id)
	
	this->SetQuantityOfDifferentElements(this->Max1DElemLib + VolumeElem + SurfElem + CouplingElem + TermCouplingElem + NullElem);
	










	// se Blood constutive law = Casson entao elemento de volume sera 5245
	// e elemento de parede sera 5707
	if (!this->IntParamArray[2])
		{
		// somente no substep 1
		this->VolumeElementType = 5245;
		this->SurfaceElementType= 5705;
		}
	else // se Blood constutive law = Newton
		{	
		// somente no substep 1
		this->VolumeElementType = 524;
		this->SurfaceElementType= 57;
		}

	
	if (this->Max1DElemLib == 2)
		{
		this->BasParamElementList->InsertNextValue(1); 
		this->BasParamElementList->InsertNextValue(3);
		}
	else
		{	
		this->BasParamElementList->InsertNextValue(13); 
		this->BasParamElementList->InsertNextValue(10);
		this->BasParamElementList->InsertNextValue(11); 
		this->BasParamElementList->InsertNextValue(12);	
		}
	
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->BasParamElementList->InsertNextValue(this->VolumeElementType);
		 
		
		for (int z = 0; z < this->GetFullModel(i)->GetNumberOfShells(); ++z)
			this->BasParamElementList->InsertNextValue(this->SurfaceElementType);
		
		for (int z = 0; z < this->GetNumberOfCovers(i); ++z)
			if (!this->IntParamArray[7])
				this->BasParamElementList->InsertNextValue(54); // se monolitico usamos no substep 1 elem. 54
			else
				this->BasParamElementList->InsertNextValue(542); // se monolitico usamos no substep 1 elem. 542
				
		for (int z = 0; z < this->GetNumberOfCovers(i); ++z)
			this->BasParamElementList->InsertNextValue(23);
		
		}
	 
	this->BasParamElementList->InsertNextValue(99);	
	this->BasParamElementList->Resize(this->GetQuantityOfDifferentElements());	
	




	
	
	// Obtendo o numero total de coordenadas do modelo - para ser usado na geracao do Inifile.txt
	
	this->TotalNumberOfCoordinates = this->TotalNumberOfCoordinates + this->GetNumberOfPoints();
	
	it = this->FullModelList.begin();
	// percorre a lista de fullmodels
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		this->CurrentFullModel = (*it);
		this->TotalNumberOfCoordinates = this->TotalNumberOfCoordinates + this->CurrentFullModel->GetVolume()->GetNumberOfPoints(); //Obtendo o numero de coordenadas do volume atual.
		it++;
		}
	
	it = this->FullModelList.begin();
	
	// inserindo os pontos que correspondem aos nós "ficticios" de presssao
	// para cada tampa de cada estrtura 3D
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		this->CurrentFullModel = (*it);	
		
  	for (int i=0; i < (this->CurrentFullModel->GetNumberOfCoverGroups()); i++)
			this->TotalNumberOfCoordinates = this->TotalNumberOfCoordinates + 1;
		
		it++;
		}



	
	  // geracao BASPARAM ****************
	if (this->WhichFile[0])
		{
  	this->BasParam->SetDoFNumber(7);
  	this->ConfigureBasparam();
		}
	
	// seta dimension
	//this->SetDimension(3);

	// seta DOF (Degree of Freedom number)
	// pega o valor de DOF do BasParam...
//	if (this->BasParamDOFNumber != -1)
//		this->SetDOF(this->BasParamDOFNumber);
//	else
//		vtkErrorMacro("DOF Number not defined in vtkHMStraightModelWidget");
	
	// a compatibilizacao de raio entre os modelos 
//	if (this->IntParamArray[35])
//		this->CompatibilizeCouplingPoints(this->IntParamArray[36]);

//	if (!this->TestCouplingPointsAccuracy())
//		return 0; // se erro aborta geracao dos arquivos

  //Construcao Mesh.txt
	if (this->WhichFile[1])
		{
		// geracao MESH.txt	

	  //vtkDebugMacro(<<"Gerando Coordenadas 1d");
		//this->GenerateCoordinates1D(1);
		//this->StraightModelVisited->Reset();
		// gera as coordenadas dos pontos da estrutura 3D
		vtkDebugMacro(<<"Gerando Coordenadas 3d");
		this->GenerateCoordinates3D();
		
		
		// a compatibilizacao de raio entre os modelos  -deve ser feita apos o aumento da escala do 3D que é realizado no metodo GenerateCoordinates3D
		if (this->IntParamArray[35])
			this->CompatibilizeCouplingPoints(this->IntParamArray[36]);
		
		
//		// gera lista de incidencia de elementos 1D
//	  vtkDebugMacro(<<"Gerando Listas 1d");
//		this->Generate1DLists(1);
//		this->StraightModelVisited->Reset();
		// gera lista de incidencia dos elementos 3D
		vtkDebugMacro(<<"Gerando Listas 3d");
		this->GenerateIncidence3D();
		
		vtkDebugMacro(<<"Construindo ElementArray");
		this->BuildElementArray();
		
		vtkDebugMacro(<<"Gerando ElementMat");
		this->BuildElementMat();
		this->InsertNullElements();
		vtkDebugMacro(<<"Gerando DirichLet values");
		this->GenerateDirichletsValues(); 
		Mesh->SetDegreeOfFreedom(7); 
		Mesh->SetDimension(3); 
		Mesh->SetCoupledModel(1);
		
		//this->SubStepNumber é definido no metodo GenerateDirichletsValues
		Mesh->SetSubStepNumber(this->SubStepNumber);
		
		
		Mesh->SetNumberOfElementGroups(this->GetNumberOfElementGroups());
		Mesh->SetNumberOfPoints(this->GetNumberOfPoints());
		Mesh->SetElementType(this->GetElementType());
		Mesh->SetElementMat(this->GetElementMat());
		Mesh->SetElementForGroup(this->GetElementForGroup());
		Mesh->SetPoints(this->GetPoints());
		Mesh->SetGroupElements(this->GetElementsIncidence());
		Mesh->SetDirichletsTag(this->GetDirichletsTag());
		Mesh->SetDirichlets(this->GetDirichlets());
		}
	
	
  //Construcao PARAM.txt
	if (this->WhichFile[2])
		{	
		vtkDebugMacro(<<"Gerando Param");
		this->CreateParam();
		
		Param->SetNumberOfGroups(this->TotalNumberOfElements);
		
		Param->SetRealParameters(&this->RealParameters);
		Param->SetIntegerParameters(&this->IntegerParameters);
	
		//Setando os dados no objeto param
		Param->SetQuantityOfRealParameters(this->QuantityOfRealParameters);
		Param->SetQuantityOfIntegerParameters(this->QuantityOfIntegerParameters);
	
	
		}
	
  //Construcao INIFILE.txt
	if (this->WhichFile[3])
		{	
		this->GenerateIniFile();
  	IniFile->SetInitialConditions(this->GetIniFileArray());
		}


	vtkHM1DData *Data = vtkHM1DData::New();
	Data->SetMesh(Mesh);
	Data->SetBasParam(this->BasParam);
	Data->SetParam(Param);
	Data->SetIniFile(IniFile);
	
	Data->SetModelType(2); // define como modelo acoplado
	
	Data->SetModelInfo(this->ModelInfo);
	Data->SetElementTypeArray(this->ElementTypeArray);
	Data->SetElementMatArray(this->ElementMat1D);
	
	// escreve arquivo script para execucao do Solver Paralelo somente se Solver Type for Paralelo
	if (this->IntParamArray[30])
		{
		vtkHMParallelSolverScriptWriter *ScriptWriter = vtkHMParallelSolverScriptWriter::New();
		if (!ScriptWriter->WriteParallelSolverScript(BasParam, path))
			vtkErrorMacro("Error on writing the Script for Parallel Solver!");
		ScriptWriter->Delete();
		}

  vtkDebugMacro(<<"Escrevendo Solver files");
	vtkHM1DStraightModelWriter *writer = vtkHM1DStraightModelWriter::New();
	writer->SetWhichFile(this->WhichFile);
	writer->SetInput(Data);
	writer->SetFileName(path);
	writer->WriteData();

	Mesh->Delete();
	Param->Delete();	
	writer->Delete();
	IniFile->Delete();
	Data->Delete();
	
	return 1;

}

//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::GenerateCoordinates1D(int id)
{
 	vtkHM1DTreeElement *elem = NULL;
	vtkHM1DTreeElement *child = NULL;
	vtkHM1DTreeElement *parent = NULL;
	double x,y,z;
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
		return;
	
	this->StraightModelVisited->InsertNextId(id);
	
	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
		{
		this->NumberOfSegments++;
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
		int NodeNumber = Seg->GetNodeNumber();
		int i=0;
		while (i < NodeNumber) // visita cada nó do segmento
			{
		  vtkHMNodeData *Node = Seg->GetNodeData(i);
		  this->point++;
		  // fills the "Node-Point Number" Pair map
		  NodeIDPair.insert(pair<vtkHMNodeData *, int>(Node,this->point));
		  x=Node->coords[0];
		  y=Node->coords[1];
		  z=Node->coords[2];
		  this->points->InsertNextPoint(x,y,z);
		  i++;
			}
		}
	else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
		{
		elem = this->StraightModel->GetTerminal(id);
		this->point++;
		// fills the "terminal id-Point number" map
		TerminalIDPair.insert(pair<int, int>(id,this->point));
		if (!elem->GetFirstParent())	// se é o coracao
  		{
	    child = elem->GetFirstChild();
	    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child);
	    if (Seg)
	    	{     
	     	vtkHMNodeData *Node = Seg->GetNodeData(0);
	     	if (Node)  
	     		{
	     	  x=Node->coords[0];
	  			y=Node->coords[1];
	  			z=Node->coords[2];
	  			this->points->InsertNextPoint(x,y,z);
	  		  }
	      }
  		}
		else // se o terminal tem pai
			{
		  parent = elem->GetFirstParent();
//		  child = parent->GetFirstChild(); // retorna o terminal
//		  child = parent->GetNextChild();  // retorna o primeiro segmento filho
		  child = elem->GetFirstChild();  // retorna o primeiro segmento filho
		  if (child)
		  	{
		    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child);
		   	vtkHMNodeData *Node = Seg->GetNodeData(0);
		    x=Node->coords[0];
		  	y=Node->coords[1];
		  	z=Node->coords[2];
		  	this->points->InsertNextPoint(x,y,z);
		   	}
		  else // o terminal é folha
		  	{
		   	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(parent);
		   	int NumberOfNodes = Seg->GetNodeNumber();
		   	vtkHMNodeData *Node = Seg->GetNodeData(NumberOfNodes-1);
		    x=Node->coords[0];
		  	y=Node->coords[1];
		  	z=Node->coords[2];
		  	this->points->InsertNextPoint(x,y,z);
		    }
			}  
		child = elem->GetFirstChild();
		} // fim else
	
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->GenerateCoordinates1D(id);
			}
		}	
}

//----------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::GenerateCoordinates3D()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::GenerateCoordinates3D()");
	
	// vetor utilizado para acessar cada ponto (coordenadas x y z)
	// GetVolume()->GetPoint(posicao, double*) 
	double VolumePoint[3];
	
	this->FullModelNumberOfPoints->SetNumberOfValues(this->GetNumberOf3DFullModels());
	
	it = this->FullModelList.begin();
	// percorre a lista de fullmodels
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);
		
		int NumberOfVolumePoint = this->CurrentFullModel->GetVolume()->GetNumberOfPoints(); //Obtendo o numero de coordenadas do volume atual.
		
		// preenche este vetor de int o qual será utilizado na criacao da lista de incidencia
		// no metodo GenerateIncidence3D para fazer o deslocamento dos valores de cordenada de cada volume para 
		// as coordenadas globais de todos elementos do modelo acoplado
		this->FullModelNumberOfPoints->SetValue(i, NumberOfVolumePoint);
		
 		// armazenando o numero de pontos do 1D
		// na primeira vez que passa por aqui
		if (!this->NumberOfPoints1D)
			this->NumberOfPoints1D = this->GetNumberOfPoints();
			
		if (!this->ScaleFactor3D)	
			vtkErrorMacro("3D Scale Factor is Null!!!");
	
		// copiando as coordenadas do volume que englobam as coordenadas da surface e depois dos pontos de volume
		for (int i = 0; i < NumberOfVolumePoint; i++)
  		{	
  		this->CurrentFullModel->GetVolume()->GetPoint(i,VolumePoint);
    	
    	if (this->ScaleFactor3D == 1.0)
    		this->points->InsertNextPoint(VolumePoint[0], VolumePoint[1], VolumePoint[2]);
    	else
    		this->points->InsertNextPoint(VolumePoint[0]*this->ScaleFactor3D, VolumePoint[1]*this->ScaleFactor3D, VolumePoint[2]*this->ScaleFactor3D);
  		}
		it++;
		}
	
	it = this->FullModelList.begin();
	
	// inserindo os pontos que correspondem aos nós "ficticios" de presssao
	// para cada tampa de cada estrtura 3D
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);	
		
		// elementos de 4 pontos (elemento 54, 541, 542...) 
  	for (int i=0; i < (this->CurrentFullModel->GetNumberOfCoverGroups()); i++)
			this->points->InsertNextPoint(0, 0 ,0);
		
		it++;
		}
}


//----------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::Generate1DLists(int id)
{
	vtkHM1DTreeElement *elem 		= NULL;
	vtkHM1DTreeElement *child 	= NULL;
	vtkHM1DTreeElement *parent 	= NULL;
	vtkHM1DTreeElement *child2 	= NULL;
	vtkHM1DTreeElement *elem2 	= NULL;
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
		return;
	
	this->StraightModelVisited->InsertNextId(id);
	
	
	// posicao do elemento 99 no Basparam
	//int NullElement = this->Max1DElemLib + 2 + 2*(this->FullModel->GetNumberOfCoverGroups()) + 1;
	int NullElement = this->GetQuantityOfDifferentElements();
	
	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
		{ //******************************************************************************
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
		int NodeNumber = Seg->GetNodeNumber();
		int i=0;
		while (i < NodeNumber) // visita cada nó do segmento
			{
		  vtkHMNodeData *Node = Seg->GetNodeData(i);
		  	
		  if (!i) //i==0   //acessa primeiro no do segmento
		 		{
		  	vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
		  	if (!this->Max1DElemLib)
		  		{
		  		vtkErrorMacro("Max Element libs not defined!!");
		  		}
		  	else
		  		{
	  			if (this->Max1DElemLib==4)
		  			{
		  			if (NodeNumber==2) // se o Segmento tem somente um ELEMENTO
		  				{
		  				vtkHM1DMesh::ListOfInt lst; 
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->ElementsIncidence.push_back(lst);
				  		this->ElementsIncidence.push_back(lst);
				  		this->ElementsIncidence.push_back(lst);
			  			
			  			// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
			  				//cout << "Elemento dentro 3D "<< id <<" "<< i << endl;
			  				
								//*****************************************
								//*****************************************
                // !!!!!! IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!
                // Inserindo os ElementType em negativo para poder saber o tipo de elemento
                // e ao mesmo tipo fazer uma maracao para informar que o elemento negativo esta dentro da estrutura 3D
								//*****************************************
								//*****************************************

			  				this->ElementTypeArray->InsertNextValue(-1); // elem
			  				this->ElementTypeArray->InsertNextValue(-3); // elem in
			  				this->ElementTypeArray->InsertNextValue(-4); // elem out
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  			//	cout << "Elemento fora 3D "<< id <<" "<< i << endl;

			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				this->ElementTypeArray->InsertNextValue(3); // elem in
			  				this->ElementTypeArray->InsertNextValue(4); // elem out
			  				}
			  			
			  			
			  			//  quando temos segmentos somente com um elemento ai teremos elementos: 1 3 4
			  			//  lista de incidencia tambem será modificada
			  			// elem ex:  1 2
			  			// elem in:  1 2
			  			// elem out: 1 2
			  			
			  			// geracao do INIFILE
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				}
		  			else  // para segmentos com mais de um elemento
		  				{
		  				
		  				// visita o primeiro elemento do segmento
		  				// e visita o nó 0 do elemento
							// por exemplo, se o segmento tem 8 nós, sera visitados o nó 0 
							// assumindo que o primeiro nó é o 0 e o último é o nó 7
		  				vtkHM1DMesh::ListOfInt lst;
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->ElementsIncidence.push_back(lst);
				  		this->ElementsIncidence.push_back(lst);

			  			// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
								//*****************************************
								//*****************************************
                // !!!!!! IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!
                // Inserindo os ElementType em negativo para poder saber o tipo de elemento
                // e ao mesmo tipo fazer uma maracao para informar que o elemento negativo esta dentro da estrutura 3D
								//*****************************************
								//*****************************************
			  				this->ElementTypeArray->InsertNextValue(-1); // elem
			  				this->ElementTypeArray->InsertNextValue(-3); // elem in
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  				//cout << "Elemento fora 3D "<< id <<" "<< i << endl;
			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				this->ElementTypeArray->InsertNextValue(3); // elem in
			  				}
			  			
			  			
			  			// geracao do INIFILE
		  				this->IniFileArray->InsertNextValue(Node->area);
		  				}
		  			}
	  				if (this->Max1DElemLib==2)
		  				{
			  			vtkHM1DMesh::ListOfInt lst;
		  				lst.clear();
		  				lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->ElementsIncidence.push_back(lst);
				  		
				  		// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
			  				//cout << "Elemento dentro 3D "<< id <<" "<< i << endl;
			  				//this->ElementTypeArray->InsertNextValue(NullElement);
			  				this->ElementTypeArray->InsertNextValue(-1); // elem
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  				//cout << "Elemento fora 3D "<< id <<" "<< i << endl;
			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				}
				  		
				  		// geracao do INIFILE
			  			this->IniFileArray->InsertNextValue(Node->area);
			  			}
		  		} // fim else
		  	}  //fim if (!i)
		  else
		  	{
		  	if (i== NodeNumber-2) // penultimo nó do segmento
		  		{
					// visita o penultimo e o último nó do segmento
					// por exemplo, se o segmento tem 8 nós, serão visitados os nós 6 e 7 
					// assumindo que o primeiro nó é o 0 e o último é o nó 7
					vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
	  			if (!this->Max1DElemLib)
	  				{
	  				vtkErrorMacro("Max Element libs not definied!!");
	  				}
	  			else
	  				{
	  				if (this->Max1DElemLib==4)
		  				{
					  	vtkHM1DMesh::ListOfInt lst;
				  		lst.clear();
				  		lst.push_back(this->GetNodeID(Node));
				  		lst.push_back(this->GetNodeID(Node2));
				  		this->ElementsIncidence.push_back(lst);
				  		this->ElementsIncidence.push_back(lst);
					  	
		  				// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
			  				//cout << "Elemento dentro 3D "<< id <<" "<< i << endl;
//			  				this->ElementTypeArray->InsertNextValue(NullElement);
//			  				this->ElementTypeArray->InsertNextValue(NullElement);
			  				this->ElementTypeArray->InsertNextValue(-1);
			  				this->ElementTypeArray->InsertNextValue(-4);
			  				
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  			//	cout << "Elemento fora 3D "<< id <<" "<< i << endl;
			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				this->ElementTypeArray->InsertNextValue(4); // elem in
			  				}
		  				
		  				
		  				// geracao do INIFILE
							this->IniFileArray->InsertNextValue(Node->area);
							
							// como o último nó não é visitado, o mesmo deve ser visitado para que sua área seja inserida no IniFileArray
							this->IniFileArray->InsertNextValue(Node2->area); 
							}
	  				if (this->Max1DElemLib==2)
		  				{
		  				vtkHM1DMesh::ListOfInt lst;
			  			lst.clear();
			  			lst.push_back(this->GetNodeID(Node));
			  			lst.push_back(this->GetNodeID(Node2));
			  			this->ElementsIncidence.push_back(lst);
			  			
			  			// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
			  				//cout << "Elemento dentro 3D "<< id <<" "<< i << endl;
//			  				this->ElementTypeArray->InsertNextValue(NullElement);
			  				this->ElementTypeArray->InsertNextValue(-1);
			  				
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  				//cout << "Elemento fora 3D "<< id <<" "<< i << endl;
			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				}
			  			
		  				// geracao do INIFILE
							this->IniFileArray->InsertNextValue(Node->area);
							
							// como o último nó não é visitado, o mesmo deve ser visitado para que sua área seja inserida no IniFileArray
							this->IniFileArray->InsertNextValue(Node2->area); 
		  				}
	  				}
		  		} // fecha if if (i== NodeNumber-2)
		  	else
		  		{
		  		if (i != NodeNumber -1) // nao preciso acessar informacao do ultimo no do segmento
		  			{
						// visita todos nós do segmento entre o primeiro nó (Zero) e o penultimo nó (excluindo estes)
						// por exemplo, se o segmento tem 8 nós, serão visitados o nó 1, 2, 3, 4, 5 
						// assumindo que o primeiro nó é o 0 e o último é o nó 7
						vtkHMNodeData *Node2 = Seg->GetNodeData(i+1);
						vtkHM1DMesh::ListOfInt lst;
						lst.clear();
						lst.push_back(this->GetNodeID(Node));
						lst.push_back(this->GetNodeID(Node2));
						this->ElementsIncidence.push_back(lst);
							
						// geracao do INIFILE
						this->IniFileArray->InsertNextValue(Node->area);
						
						if (!this->Max1DElemLib)
							{
		  				vtkErrorMacro("Max Element libs not definied!!");
							}
		  			else
		  				{
		  				// consulta para saber se este elemento esta englobado pela estrutura 3D 
			  			if (this->TreeElementInside3D(id, i))
			  				{
			  				//cout << "Elemento dentro 3D "<< id <<" "<< i << endl;
			  				//this->ElementTypeArray->InsertNextValue(NullElement);
			  				this->ElementTypeArray->InsertNextValue(-1);
			  				}
			  			else // nao esta dentro da estrtura 3D
			  				{
			  				//cout << "Elemento fora 3D "<< id <<" "<< i << endl;
			  				this->ElementTypeArray->InsertNextValue(1); // elem
			  				}		
		  				}
				 		} //fecha  if (i != NodeNumber -1)
					} //fecha else	
				}	//fecha else
		  i++;
			} // FECHA WHILE  
		} // fecha if se elemento é segmento  //******************************************************************************
	
	else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
		{
		
		//cout << "id do Terminal ***************" << id << endl;
		
		
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
		
		// geracao do INIFILE
		// **************************************
		// area do no associado ao terminal é nula
		// **************************************
		this->IniFileArray->InsertNextValue(0);
		
		if (!this->Max1DElemLib)
			{
			vtkErrorMacro("Max Element libs not definied!!");
			}
		else
			{
			//this->ElementTypeArray->InsertNextValue(2); // terminal or heart
			
			// consulta para saber se este elemento esta englobado pela estrutura 3D 
			if (this->TreeElementInside3D(id, -1))
				{
				//cout << "Elemento dentro 3D "<< id <<" "<< -1 << endl;
				//this->ElementTypeArray->InsertNextValue(NullElement);
				this->ElementTypeArray->InsertNextValue(-2);
				}
			else // nao esta dentro da estrtura 3D
				{
				//cout << "Elemento fora 3D "<< id <<" "<< -1 << endl;
				this->ElementTypeArray->InsertNextValue(2); // terminal or heart
				}
			
			
			}
		elem2 = elem;
		
		if (!elem2->GetFirstParent())	// se é o coracao pois o mesmo nao tem elemento pai
  		{
  	  child2 = elem2->GetFirstChild();   //retorna o segmento filho
	  	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
	  	if (Seg)
		  	{      
		    vtkHMNodeData *Node = Seg->GetNodeData(0);
		    vtkHM1DMesh::ListOfInt lst;
		  	lst.clear();
		  	lst.push_back(this->GetTerminalID(id));
		  	lst.push_back(this->GetNodeID(Node));
		  	this->ElementsIncidence.push_back(lst);
		  	}
			}// fecha if se é coracao
		
		//******************************************************************************************************************
	  else // se o terminal tem pai
	  	{
		  // pegar o no zero do segmento filho
		  parent = elem2->GetFirstParent();
//		  child2 = parent->GetFirstChild(); // retorna o terminal
//		  child2 = parent->GetNextChild();  // retorna o primeiro segmento filho
		  child2 = elem2->GetFirstChild(); // retorna o primeiro segmento filho
		  if (child2)
		  	{
		    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
		   	vtkHMNodeData *Node = Seg->GetNodeData(0);
		   	
		   	vtkHM1DMesh::ListOfInt lst;
		   	lst.clear();
		   	//inseri na lista de grupo de elementos o id do ponto do terminal
		   	lst.push_back(this->GetTerminalID(id));
		   	
		   	while ( parent )
			   	{
			   	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
		     	int NodeNumber = SegPai->GetNodeNumber();
			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
			    lst.push_back(this->GetNodeID(NodePai));
			    parent = elem2->GetNextParent();
			   	}
			  
			  while ( child2 )
				  {
				  vtkHM1DSegment *SegFilho = vtkHM1DSegment::SafeDownCast(child2);
			    vtkHMNodeData *NodeFilho = SegFilho->GetNodeData(0);
			    lst.push_back(this->GetNodeID(NodeFilho));
				  child2 = elem2->GetNextChild();
				  }
		   	this->ElementsIncidence.push_back(lst);
		   	
//		    if (parent->GetChildCount()==2) // se o terminal tem um pai e este segmento tem somente 2 filhos (seg+terminal)
//		    	{
//		     	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
//		     	int NodeNumber = SegPai->GetNodeNumber();
//			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
//			    vtkHM1DSegment *SegFilho = vtkHM1DSegment::SafeDownCast(child2);
//			    vtkHMNodeData *NodeFilho = SegFilho->GetNodeData(0);
//			    vtkHM1DMesh::ListOfInt lst;
//		  		lst.clear();
//		  		lst.push_back(this->GetTerminalID(id));
//		  		lst.push_back(this->GetNodeID(NodePai));
//		  		lst.push_back(this->GetNodeID(NodeFilho));
//		  		this->ElementsIncidence.push_back(lst);
//		  	  }  // fecha if
//		    else // mais de um filho
//		         // notar que o metodo GetChildCount considera como filho o terminal mais os segmentos
//		    	{
//					vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
//					int NodeNumber = SegPai->GetNodeNumber();
//					vtkHMNodeData *Node = SegPai->GetNodeData(NodeNumber-1);
//					vtkHM1DMesh::ListOfInt lst;
//					lst.clear();
//					lst.push_back(this->GetTerminalID(id));
//					lst.push_back(this->GetNodeID(Node)); 
//					int Children =  parent->GetChildCount();
//					int i=1;
//					while (i<Children) // percorre todos segmentos filhos 
//						{
//						vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child2);
//						vtkHMNodeData *Node = Seg->GetNodeData(0);
//						lst.push_back(this->GetNodeID(Node));
//						//if (i != 2) child2 = parent->GetNextChild();
//						child2 = parent->GetNextChild(); 
//						i++;	
//						}
//					this->ElementsIncidence.push_back(lst);
//					parent = elem2->GetFirstParent();
//				  child2 = parent->GetFirstChild(); // retorna o terminal
//				  child2 = parent->GetNextChild();  
//					} // fecha else mais de um filho
		   } // fecha if (child)
		   
		  else // o terminal é folha
		  	{
//		   	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(parent);
//		   	int NodeNumber = Seg->GetNodeNumber();
//			  vtkHMNodeData *Node = Seg->GetNodeData(NodeNumber-1);
		    vtkHM1DMesh::ListOfInt lst;
		    lst.clear();
		    lst.push_back(this->GetTerminalID(id));
		    
		    while ( parent )
			   	{
			   	vtkHM1DSegment *SegPai = vtkHM1DSegment::SafeDownCast(parent);
		     	int NodeNumber = SegPai->GetNodeNumber();
			    vtkHMNodeData *NodePai = SegPai->GetNodeData(NodeNumber-1);
			    lst.push_back(this->GetNodeID(NodePai));
			    parent = elem2->GetNextParent();
			   	}
		    
//		    lst.push_back(this->GetNodeID(Node));    
		    this->ElementsIncidence.push_back(lst);  
		   	} // fecha else
	  	}  // fecha else se terminal tem pai
	 child = elem->GetFirstChild();
		} //-------------------------------- final do else --/ se atual elemento da arvore é um terminal
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->Generate1DLists(id);
			}
		}	
}


//----------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::Build1DElementArray()
{
	vtkHM1DMesh::ListOfInt lst;
  int i=0;
  
  // cria um array de int
  int *elementForGroup = new int[this->ElementsIncidence.size()];
	  
  vtkHM1DMesh::VectorOfIntList::iterator it_vector;
  
  it_vector= this->ElementsIncidence.begin();
   
  while (it_vector != this->ElementsIncidence.end() )
  	{
  	lst=*it_vector;
  	vtkHM1DMesh::ListOfInt::iterator it;
	  it =lst.begin();
		// aqui eu posso construir a lista de *ELEMENT GROUPS
		while(it != lst.end())
			{
			it++;
			}
		elementForGroup[i]=lst.size();
//		cout << "lst.size() " << lst.size() << endl;
		i++;
  	it_vector++;
  	}
  
  
  // seta o vetor *ELEMENT GROUP
  this->SetElementForGroup(elementForGroup);
  
  
  
}

//----------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNodeID(vtkHMNodeData *Node)
{
	std::map<vtkHMNodeData *, int>::iterator it = NodeIDPair.find(Node);
 	return (*it).second ;
}

//----------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetTerminalID(int id)
{
  std::map<int, int>::iterator it = TerminalIDPair.find(id);
  return (*it).second ;
}


//----------------------------------------------------------------------------


vtkPoints *vtkHMCoupledModelWriterPreProcessor::GetPoints()
{
	return this->points;
}

//----------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNumberOfPoints()
{
	return this->points->GetNumberOfPoints();
}



//----------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::BuildElementArray()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::BuildElementArray()");
	
	vtkHM1DMesh::ListOfInt lst;
  int i=0;
  
  // cria um array de int
  int *elementForGroup = new int[this->ElementsIncidence.size()];
	  
  vtkHM1DMesh::VectorOfIntList::iterator it_vector;
  
  it_vector= this->ElementsIncidence.begin();
   
  while (it_vector != this->ElementsIncidence.end() )
  	{
  	lst=*it_vector;
  	vtkHM1DMesh::ListOfInt::iterator it;
	  it =lst.begin();
		// aqui eu posso construir a lista de *ELEMENT GROUPS
		while(it != lst.end())
			{
			it++;
			}
		elementForGroup[i]=lst.size();
		i++;
  	it_vector++;
  	}
  
  
  this->SetElementForGroup(elementForGroup);
  int *ElementType = new int[this->ElementsIncidence.size()]; // vetor Element Type tem o mesmo tamanho do *ELEMENT GROUPS
  int z=0;
  while (z < this->ElementsIncidence.size())
  	{
  	ElementType[z] = this->ElementTypeArray->GetValue(z);
  	z++;
  	}
  
  // setar somente no final do processamento do Element Type
   this->SetElementType(ElementType);
  
}

//----------------------------------------------------------------------------
vtkHM1DMesh::VectorOfIntList *vtkHMCoupledModelWriterPreProcessor::GetElementsIncidence()
{
	return &ElementsIncidence;
}


//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::CreateParam()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::CreateParam(vtkHM1DParam *param)");
	
	/*
	Numero de Grupos é assim definido:
	Numero de Elementos 1D, Numero Total de elementos de volumes de todos 3D envolvidos, Numero de elementos de parede 
	com parametros diferente (se assume que todos elementos da parede tem as mesmas propriedades - entao cada 3D tem um 1 parametro), 
	Numero de elementos de acoplamento 23 e 54 e elemento nulo 99.
	*/
	
	
	// numero de elementos 1D que tenham parametros terminais e elementos
	// numero de terminais + numero de elementos de cada segmento
	int NumberOf1DElementsWithParam = this->NumberOfPoints1D - this->NumberOfSegments;

	
	
	//Vetor para armazenar a quantidade parametros que cada grupo possui
	//int n = param->GetNumberOfGroups();
	int n = NumberOf1DElementsWithParam + this->NumberOf3DElements + this->NumberOf3DSurfaces + this->NumberOfCouplingElem + this->NumberOfNullElements;
	
	// Estrtura do vetor QuantityOfRealParameters
	// 1d numero de parametros:  ex: 40 13 13 13 13 13 13 4 13 13 13 4 13 13 1
	// 3D volume "1" numero: 3 3 3 3 3 3 3 3 3 3
	// 3D volume "n" numero: 3 3 3 3 3 3 3 3 3 3
	// 3D surface: 4 para cada parede
	// 3D Acoplamento(elem 54) 0 para cada tampa
	// 3D Acoplamento(elem 23) 0 para cada tampa
	// Elemento Nulo: 0 (somente uma vez)
	
	
	// se temos 1 carotida (3 tampas) e 1 tubo (2 tampas) tem-se a seguinte configuracao do vetor QuantityOfRealParameters
	/*
	1d: 40 13 13 ...
	3d 1 volume 1:3 3 3 3 3 3 3 3 3 3
	3d 2 volume 1:3 3 3 3 3 3 3 3 3 3
	parede 1 e parede 2: 4 4
	elem 54 v1: 0 0 0
	elem 54 v2: 0 0
	elem 23 v1: 0 0 0
	elem 23 v2: 0 0
	elem 99: 0
	*/
	
	this->QuantityOfRealParameters = new int[n];
	this->QuantityOfIntegerParameters = new int[n];

	// inicializa vetor em zero 
	for (int i = 0; i < n; i++)
			this->QuantityOfRealParameters[i] = 0;

	int pos=0;

	// parametros elementos do modelo 1D
	this->SetParameters(1, &pos);
	
	this->StraightModelVisited->Reset();
	
	int counter;
	
  // elementos volume   
	for (int i = NumberOf1DElementsWithParam; i < this->NumberOf3DElements + NumberOf1DElementsWithParam; i++)
			{
			this->QuantityOfRealParameters[i]=3;
			counter = i;
			}
	
	counter++; // 
  // o numero de elementos de superficie, aqueles que tem 4 parametros reais	
	for (int i = counter; i < (counter + this->NumberOf3DSurfaces); ++i)
		{
		this->QuantityOfRealParameters[i] = 4;
		}




	vtkHM1DParam::ListOfDouble lst;

	for (int i=NumberOf1DElementsWithParam; i < NumberOf1DElementsWithParam + this->NumberOf3DElements; i++ )
		{
		lst.clear();	
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		this->RealParameters.push_back(lst);
		//counter = i;
		}

	double Elastin,CurveRadius,Wallthickness, RefPressure, ViscoElasticity;
	// inserindo os pontos que correspondem aos nós "ficticios" de presssao
	// para cada tampa de cada estrtura 3D
	
	it = this->FullModelList.begin();
	
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);	
		
		for (int z= 0; z < this->CurrentFullModel->GetNumberOfShells(); ++z)
			{
			if (!this->IntParamArray[1])
				{
				Elastin 				= 1.0;	
				CurveRadius 		= 1.0;	
				Wallthickness 	= 1.0;
				RefPressure     = 1.0;	
				ViscoElasticity = 1.0;	
				}
			else
				{
				Elastin 				= this->CurrentFullModel->GetWallParameters(z)->GetValue(0);	
				CurveRadius 		= this->CurrentFullModel->GetWallParameters(z)->GetValue(1);	
				Wallthickness 	=	this->CurrentFullModel->GetWallParameters(z)->GetValue(2);
				RefPressure     = this->CurrentFullModel->GetWallParameters(z)->GetValue(3);	
				ViscoElasticity =	this->CurrentFullModel->GetWallParameters(z)->GetValue(4);	
				}			
			lst.clear();			
			lst.push_back(Elastin*(Wallthickness/CurveRadius));		
	
			// raio de curvatura
			lst.push_back(CurveRadius);
	
			//pressão de referência do elemento
			lst.push_back(RefPressure);		
		
			//viscoelasticidade*(h/R)
			lst.push_back(ViscoElasticity*(Wallthickness/CurveRadius));				
		
		  // inserindo lista de parametros reais do elemento correspondente a parede arterial
			this->RealParameters.push_back(lst);
			}
		it++; // passa iterador para proximo 3DFullModel
		}	
		
		


	// insere listas vazias para os elementos que nao tem parametros no Param como os elementos 54 23 e 99
	for (int i=0; i < (this->NumberOfCouplingElem + this->NumberOfNullElements); i++ )
		{
		lst.clear();	
		this->RealParameters.push_back(lst);
		}

	// Preechendo parametros da lista A de parametros inteiros que especificam o numero de 
	// parametros inteiros que devem ser lidos na lista B dos elementos de volume
	
	for (int i=NumberOf1DElementsWithParam; i < n; i++ )
		this->QuantityOfIntegerParameters[i] = 0; // elementos Volume
  

}

//-----------------------------------------------------------------------------------

int *vtkHMCoupledModelWriterPreProcessor::GetElementForGroup()
{
	return this->nElementForGroup;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetElementForGroup(int *elemForGroup)
{
	this->nElementForGroup = elemForGroup;
}
//-------------------------------------------------------------------------------------

int *vtkHMCoupledModelWriterPreProcessor::GetElementType()
{
	return this->ElementType;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetElementType(int *elemType)
{
	this->ElementType = elemType;
}
//-------------------------------------------------------------------------------------

int *vtkHMCoupledModelWriterPreProcessor::GetElementMat()
{
		return this->ElementMat;
}	
	
//-------------------------------------------------------------------------------------
	
void vtkHMCoupledModelWriterPreProcessor::SetElementMat(int *elemMat)
{
		this->ElementMat = elemMat;
}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNumberOfElementGroups()
{
   return this->ElementsIncidence.size();
	//return this->IncidenceElements1D.size();
}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::GenerateDirichletsValues()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::GenerateDirichletsValues()");
	
	
	/* O tamanho da lista A é o mesmo que o numero de pontos da lista de coordenadas
	
	
	
	 ListaA -- Integer Tags (0 or -1)
	 ListaB -- Real values
	
	
	 se acoplamento é monolitico entao 2 substeps 
	 ListaA
	 ListaA
	 ListaB
	 ListaB

	 para acoplamento segregado tem-se 2 opcoes: Segregado e Super-Segregado
	 Segregado 3 substeps
	 1D
	 3D
	 Bubble
	
	 Super-Segregado
	 Numero de substeps 2 + numero de 3Ds
	 1D
	 3D-1
	 3D-n
	 Bubble
	
	 Se Segregado
	 ListaA
	 ListaA
	 ListaA
	 ListaB
	 ListaB
	 ListaB
	
	*/
	
	int n[7] = {0};
	double n2[7] =  {0.0}; 
	
	vtkIntArray *DirichletIntArray= vtkIntArray::New();
	vtkDoubleArray *DirichletDoubleArray= vtkDoubleArray::New();

	if (this->IntParamArray[7] == 1)
		// acoplamento Segregado "Normal" teremos apenas 3 substeps independente do numero de 3ds 
		this->SubStepNumber = 3;
	
	if (this->IntParamArray[7] == 2)
		// acoplamento super segregado
		this->SubStepNumber = this->GetNumberOf3DFullModels() + 2; // numero de 3ds + 2 substeps (1d e bubble)
	
	if (!this->IntParamArray[7])
		// se acoplamento monolitico teremos apenas 2 substeps independente do numero de 3ds
		this->SubStepNumber = 2;

	//************************************************************
	//************************************************************
  // COLETANDO INFORMACAO SOBRE NUMERO DE SUBSTEPS DO MODELO
  // PARA SER USADA NO ARQUIVO DE DESCRICAO GERAL DO MODELO
	this->ModelInfo->InsertNextId(this->SubStepNumber);
  //************************************************************
  //************************************************************
	
	DirichletIntArray->SetNumberOfComponents(7);

	DirichletDoubleArray->SetNumberOfComponents(7);

	int Counter = 0;
	//cout << "Tamanho Lista A " << this->GetNumberOfPoints() << endl;

	// Configurnado a lista A (valores inteiros)
	vtkIntArray *TempIntArray = vtkIntArray::New();
	vtkDoubleArray *TempDoubleArray = vtkDoubleArray::New();
	
	TempIntArray->SetNumberOfComponents(7);
	TempDoubleArray->SetNumberOfComponents(7);
	//TempIntArray->SetNumberOfTuples(this->GetNumberOfPoints());
	
	// percorre de 0 até o número de pontos 1d e insere zeros
	// 1d nao tem condicoes de DirichLet 
	
	vtkDebugMacro(<<"GenerateDirichletsValues do modelo 1D");
	
	for (int i = 0; i < this->NumberOfPoints1D; i++ ) 
		{	
		TempIntArray->InsertNextTupleValue(n);
		TempDoubleArray->InsertNextTupleValue(n2);
		Counter++;
		}
	it = this->FullModelList.begin();
  for (int z = 0; z < this->GetNumberOf3DFullModels(); ++z)
		{
		
		this->CurrentFullModel = (*it);	
		
		this->BuildTransposeConnectivity(this->CurrentFullModel);		
		int NumPointsSurface = this->CurrentFullModel->GetSurface()->GetCoords()->GetNumNodes();
		int NumPointsVolume = this->CurrentFullModel->GetVolume()->GetNumberOfPoints();
		 
		
		vtkDebugMacro(<<"GenerateDirichletsValues() pontos de surface");
		
		
		for (int i = 0; i < NumPointsSurface; i++ ) // percorre somente pontos da parede e tampas do modelo 3D
			{
			if (!this->GetGroupFromPoint(i)) // se o ponto é da parede arterial
	  		{
				// se o modelo é rigido
				if (!this->IntParamArray[1])
					{
					n[0]=-1;
					n[1]=-1;
					n[2]=-1;
					n[3]= 0;
					n[4]=-1;
					n[5]=-1;
					n[6]=-1;
					}
	  		}
	  	else // se o ponto esta localizado nas tampas
	  		{
	  		for (int y=0; y < 7 ; y++)
						n[y] = 0;	
	  		}				
			TempIntArray->InsertNextTupleValue(n);
			TempDoubleArray->InsertNextTupleValue(n2);
			Counter++;
			}
		
		TempIntArray->Squeeze();
		TempDoubleArray->Squeeze();
		
		
		
			
			
		for (int i=0; i < 7 ; i++)
						n[i] = 0;	
		
		vtkDebugMacro(<<"GenerateDirichletsValues() pontos volume");
			
		for (int i = 0; i < (NumPointsVolume - NumPointsSurface) ; ++i)
			{
			TempIntArray->InsertNextTupleValue(n);
			TempDoubleArray->InsertNextTupleValue(n2);
			Counter++;
			}	
		
		TempIntArray->Squeeze();
		TempDoubleArray->Squeeze();
		
			
		it++; //pega o proximo FullModel	
		}

	// inserindo vetores com relacao aos pontos de terminais de acoplamento
	for (int i = 0; i < this->GetTotalNumberOfCovers() ; ++i)
		{
		TempIntArray->InsertNextTupleValue(n);
		TempDoubleArray->InsertNextTupleValue(n2);
		Counter++;
		}	
	

 vtkDebugMacro(<<"GenerateDirichletsValues() - Resize dos vtkDouble e Int Arrays");
	
	TempIntArray->Resize(this->GetNumberOfPoints());
	TempDoubleArray->Resize(this->GetNumberOfPoints());
	
	
	
	DirichletIntArray->SetNumberOfTuples(this->GetNumberOfPoints()*this->SubStepNumber);	
	DirichletDoubleArray->SetNumberOfTuples(this->GetNumberOfPoints()*this->SubStepNumber);	
	
	
	
	
	int IntTemp[7];
	double DoubleTemp[7];
	
	int counter = 0;
	
	for (int i = 0; i < this->SubStepNumber; ++i)
		{
		for (int z = 0; z < TempIntArray->GetNumberOfTuples(); ++z)
			{
			TempIntArray->GetTupleValue(z, IntTemp);
			TempDoubleArray->GetTupleValue(z, DoubleTemp);
			//DirichletIntArray->InsertNextTupleValue(IntTemp);
			//DirichletDoubleArray->InsertNextTupleValue(DoubleTemp);
			
			/*
			 * antes utilizava o metodo insertnexttuple para inserir nos objetos 
			 * DirichletIntArray e DirichletDoubleArray mas pelo que parece a cada insertNextTuple o tamanho maximo do DataArray é duplicado
			 * para valores muito grande de numeros de pontos ocorria overflow entao foi necessario setar o tamanho dos data array a priori
			 * e inserir os tuples atraves do uso de uma variavel counter
			 * */
			
			DirichletIntArray->InsertTupleValue(counter, IntTemp);
			DirichletDoubleArray->InsertTupleValue(counter, DoubleTemp);
			counter++;
			
			}
	
		} 
	//DirichletIntArray->Resize(this->GetNumberOfPoints()*SubStepNumber);	

  
	this->SetDirichletsTag(DirichletIntArray);
	this->SetDirichlets(DirichletDoubleArray);
	
	
	TempIntArray->Delete();
	TempDoubleArray->Delete();
		
	
}


//----------------------------------------------------------------------------
vtkIntArray *vtkHMCoupledModelWriterPreProcessor::GetDirichletsTag()
{
	return this->DirichletsTag;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetDirichletsTag(vtkIntArray *d)
{
	this->DirichletsTag = d;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkHMCoupledModelWriterPreProcessor::GetDirichlets()
{
	return this->Dirichlets;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetDirichlets(vtkDoubleArray *d)
{
	this->Dirichlets = d;
}
//-------------------------------------------------------------------------------------


vtkDoubleArray *vtkHMCoupledModelWriterPreProcessor::GetIniFileArray()
{
	return this->IniFileDoubleArray;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetIniFileArray(vtkDoubleArray *d)
{
	this->IniFileDoubleArray = d;
}
//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::GenerateIniFile()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::GenerateIniFile()");
	
	vtkDoubleArray *IniFileDoubleArray2 = vtkDoubleArray::New();
	//IniFileDoubleArray2->SetNumberOfComponents(this->GetDOF());
	
	IniFileDoubleArray2->SetNumberOfComponents(7);
	
	//IniFileDoubleArray2->SetNumberOfTuples(this->GetNumberOfPoints());
	IniFileDoubleArray2->SetNumberOfTuples(this->TotalNumberOfCoordinates);

	double IniArray[7] =  {0.0}; 

	// Inifile parte 1D ##############################################
	//**********************************
	// 1D Tree DOF										
	//           0  1 2 3
	// DOF= 4 ---Q Pt A P
	// DOF= 3 ---Q A  P
	//**********************************

  // acessa o elemento 0 do primeiro segmento depois do coracao   
  // para pegar o valor da pressao de referencia e colocar 
  // como valor para as pressoes no inifile
	vtkHM1DTreeElement *tree 		= this->StraightModel->GetSegment(this->StraightModel->Get1DTreeRoot()->GetFirstChild()->GetId());
	vtkHM1DSegment *seg = vtkHM1DSegment::SafeDownCast(tree);
	vtkHMElementData *elem = seg->GetElementData(0);
	
	  
  for(int i=0; i< this->NumberOfPoints1D ; i++)
  	{
  	// analisar a formulacao utilizada no 1D para obter o numero de graus de liberdade da parte 1D	
  	if (this->IntParamArray[0]) // 4 DOFS
  		{
  		IniArray[0]=0.0;
  		IniArray[1]=elem->ReferencePressure;	// valor dummy para pressao inicial
  		IniArray[2]=this->IniFileArray->GetValue(i); // area do no
  		IniArray[3]=elem->ReferencePressure; // valor dummy para pressao inicial
  		}
  	else	 // 3 DOFS
  		{
   		IniArray[0]=0.0;
   		IniArray[1]=0.0;
   		IniArray[2]=this->IniFileArray->GetValue(i); // area do no
   		IniArray[3]=elem->ReferencePressure; // valor dummy para pressao inicial
  		}
  	IniFileDoubleArray2->SetTupleValue(i,IniArray);
  	}


  for(int i=0; i< 7 ; i++)
  	IniArray[i]=0.0;
  
  IniArray[3]=elem->ReferencePressure;	
  	


 // pontos do volume 	
  
  for(int i=NumberOfPoints1D; i< this->TotalNumberOfCoordinates ; i++)
  //for(int i=NumberOfPoints1D; i< this->GetNumberOfPoints() ; i++)
  	{
		IniFileDoubleArray2->SetTupleValue(i,IniArray);
  	}
	this->SetIniFileArray(IniFileDoubleArray2);

}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::ConfigureBasparam()
{
//	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::CreateNewBasParam()");
//	// definicoes retiradas de http://hemo01/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Basparam.txt
//	// e http://hemo01/wiki/index.php/HemoWiki:Community_Portal:Modelagem_Computacional:Biblioteca_de_Elementos
//

	/*******************************************************************
	/*******************************************************************
	 * Configurando os parametros de tempo do Basparam  
	/*******************************************************************
	/*******************************************************************
	*/
	this->BasParam->SetDelT(this->TimeParamArray[0]);
	this->BasParam->SetTini(this->TimeParamArray[1]);
	this->BasParam->SetTmax(this->TimeParamArray[2]);
//	
//	
	/*
	/*******************************************************************
	/*******************************************************************
	 * Configurando os parametros de tipo inteiros do Basparam  
	/*******************************************************************
	/*******************************************************************
	*/
	// definir
	//this->BasParam->SetInitialTime(this->TimeParamArray[0]);
	
	
	this->BasParam->SetRenumbering(this->IntParamArray[13]);
	this->BasParam->SetTimeQuantityScreen(this->IntParamArray[6]);
	this->BasParam->SetTimeQuantityFile(this->IntParamArray[5]);
	string *name;
	bool CompliantModel = false;
	
	// se o modelo é Compliant
	if (this->IntParamArray[1])
		CompliantModel = true;
	
	// acoplamento o numero de graus de liberdade é sempre 7	
	this->BasParamDOFNumber = 7;
	name = new string[this->BasParamDOFNumber];
	name[0] = "VX	";
	name[1] = "VY	";
	name[2] = "VZ	";
	name[3] = "P	";
	name[4] = "UX	";
	name[5] = "UY	";
	name[6] = "UZ	";

	this->BasParam->SetNameDegreeOfFreedom(name);
//	
  //**************************************************************************************
	// definicao do *StepContinuationControl
  //**************************************************************************************

	// se acoplamento é monolitico
	if (!this->IntParamArray[7])
		{
		// OpA  -- monolitico é sempre False
		this->BasParam->SetConfigProcessExternal(0);
		
		// num1
		this->BasParam->SetMaxIteration(100);
	
		// num2	
		this->BasParam->SetRalaxacao(1);

		//num3 -- sempre 1
		this->BasParam->SetQuantityStep(1);
		}
	else // se acoplamento é segregated 
		{
		if (!this->IntParamArray[8]) // se linear
			{
			this->BasParam->SetConfigProcessExternal(0);
			this->BasParam->SetMaxIteration(100);
			this->BasParam->SetRalaxacao(1);
			this->BasParam->SetQuantityStep(1);
			}
		else // se acoplamento nao linear
			{
			this->BasParam->SetConfigProcessExternal(1);
			this->BasParam->SetMaxIteration(this->IntParamArray[9]);
			this->BasParam->SetRalaxacao(this->SegregatedSubRelaxation);
			this->BasParam->SetQuantityStep(1);
			}
	
		}

	
	// geting the number of degree of freedom of each model
	int DOF1D, DOF3D;

	if (!IntParamArray[0])  // 0 - Least Square / 1= ViscoElatsic (1D)
		DOF1D = 3;
	else
		DOF1D = 4;	
	 
	if (!IntParamArray[1])  // deformable compliant model (0= no/1 =yes) (3D)
		DOF3D = 4;
	else
		DOF3D = 7;	
	

	// norma
	double *norma = new double[this->BasParamDOFNumber];
	
	double *norma1D = new double[this->BasParamDOFNumber];

	double *norma3D = new double[this->BasParamDOFNumber];


	// tolerancia
	double *Tolerance = new double[this->BasParamDOFNumber];

	double *Tolerance1D = new double[this->BasParamDOFNumber];

	double *Tolerance3D = new double[this->BasParamDOFNumber];


	
	// filling the norma and tolerance arrays
	// IF COUPLING TYPE IS MONOLITHIC --> comparing the reference values between the 1D and 3D values, the bigger value is chosen
	// and the associated convergence value is picked
	for (int i = 0; i < 4; ++i)
		{
		if (ConvergenceParam3D[i] > ConvergenceParam1D[i])
			{
			norma[i]= ConvergenceParam3D[i];
			Tolerance[i]= ConvergenceParam3D[i+7];
			}

		if (ConvergenceParam3D[i] < ConvergenceParam1D[i])
			{
			norma[i]= ConvergenceParam1D[i];
			Tolerance[i]= ConvergenceParam1D[i+4];
			}
			
		if (ConvergenceParam3D[i] == ConvergenceParam1D[i])
			{
			norma[i]= ConvergenceParam3D[i];
			
			// se valores de referencia sao iguais, escolhe-se o menor valor de convergencia	
			if (ConvergenceParam3D[i+7] > ConvergenceParam1D[i+4])
				Tolerance[i]= ConvergenceParam3D[i+7];
			
			if (ConvergenceParam3D[i+7] < ConvergenceParam1D[i+4])
				Tolerance[i]= ConvergenceParam1D[i+4];

			if (ConvergenceParam3D[i+7] == ConvergenceParam1D[i+4])
				Tolerance[i]= ConvergenceParam1D[i+4];
			}
		}
	
	// se o numero de graus de liberdade de 1D é 3, entao o campo de Elastic Pressure fica desabilitado
	// portanto o valor para norma e tolerance deve ser igual aos valores de conv. de 3D
	if (DOF1D == 3)
		{
		norma[1]= ConvergenceParam3D[1];
		Tolerance[1]= ConvergenceParam3D[8];
		}
		
	
		
	for (int i = 4; i < 7; ++i)
		{
		norma[i]= ConvergenceParam3D[4];
		Tolerance[i]= ConvergenceParam3D[11];
		}

	
	for (int i = 0; i < 7; ++i)
		{
		norma3D[i]= ConvergenceParam3D[i];
		Tolerance3D[i]= ConvergenceParam3D[i+7];
		}
	
	// zerando o double array
	for (int i = 0; i < 7; ++i)
		{
		//norma1D[i]= 0.0;
		//q[i]= 0.0;
		// para valores nao ficaream zerados no substep 1D segregado
		norma1D[i]= 0.01;
		Tolerance1D[i]= 1.0;
		}
	
	
	for (int i = 0; i < 4; ++i)
		{
		norma1D[i]= ConvergenceParam1D[i];
		Tolerance1D[i]= ConvergenceParam1D[i+4];
		}
	
	
	
	// valores utilizados quando tem-se modelo monolitico
	// escolhem-se os maiores valores de convergencia 
	this->BasParam->SetNorma(norma);
	this->BasParam->SetTolerance(Tolerance);
	
	// valores utilizados nos substeps 3D quando tem-se modelo segregado (normal ou super)
	this->BasParam->SetNormaSubStep3D(norma3D);
	this->BasParam->SetToleranceSubStep3D(Tolerance3D);
	
	// valores utilizados no substep 1D quando tem-se modelo segregado (normal ou super)
	this->BasParam->SetNormaSubStep1D(norma1D);
	this->BasParam->SetToleranceSubStep1D(Tolerance1D);



	int Nums[3];
	
	if (!this->IntParamArray[16])  //		16 -Incremental Version 0= no/1 =yes (3D))
		{
		Nums[0]=0;
		Nums[1]=0;
		Nums[2]=0;
		}
	else
		{
		Nums[0]=5;
		Nums[1]=6;
		Nums[2]=7;
		}
	this->BasParam->SetNums(Nums);

	// definicao de *ElementLibraryControl
	this->BasParam->SetMaximumQuantityOfParameters(13);
	//                                                 1D             vol - surface - covers - coupling - coupling term - null elem
//  this->BasParam->SetQuantityOfDifferentElements(this->Max1DElemLib + (3*this->FullModel->GetNumberOfCoverGroups())+1);
//  this->BasParam->SetQuantityOfDifferentElements(this->Max1DElemLib + 2 + (2*this->FullModel->GetNumberOfCoverGroups()) +1);


	//this->BasParam->SetQuantityOfDifferentElements(this->Max1DElemLib + 2*(this->GetNumberOfVolumeElements()) + (2*this->FullModel->GetNumberOfCoverGroups()) +1);


	int VolumeElem = this->GetNumberOf3DFullModels();//this->GetNumberOfVolumeElements();
	int SurfElem = this->GetTotalNumberOfSurfaceGroups();
	//int CoverElem = this->GetTotalNumberOfCovers();
	int CouplingElem = this->GetTotalNumberOfCovers();
	int TermCouplingElem = this->GetTotalNumberOfCovers();
	int NullElem = 1;


	
	this->BasParam->SetQuantityOfDifferentElements(this->Max1DElemLib + VolumeElem + SurfElem + CouplingElem + TermCouplingElem + NullElem);

	  
	//*************************************************
	//*************************************************
	//*************************************************
	// 						*S  U  B  S  T  E  P 1
	// ************************************************
	//*************************************************
	//*************************************************
	
	//    * SubStep 
	//OpA OpB Num1 Num2 Num3
	//NumA NumB NumC NumD NumE (dependendo de Num3) 
	
		// se acoplamento é monolitico
	if (!this->IntParamArray[7])
		{
		//opA
		this->BasParam->SetResultantMatrixIsSymmetrical(false);
		
		//opB
		this->BasParam->SetCalculationStepIsLinear(true);
		
		this->BasParam->SetMaxIterationSubStep(this->IntParamArray[10]);
		
		this->BasParam->SetRalaxacaoSubStep(this->MonolithicSubRelaxation);
		
		// num3 codigo do tipo de resolvedor
		
		// se paralelo
		if (this->IntParamArray[30]) 
			//this->BasParam->SetResolveLibrary(0);
			this->BasParam->SetResolveLibrary(200);
		else
			this->BasParam->SetResolveLibrary(this->IntParamArray[17]);
		
		
		//this->BasParam->SetResolveLibrary(this->IntParamArray[17]);
	
		// como OpB=T definir os NumA NumB NumC NumD NumE
		// numA
		this->BasParam->SetMaxIterationOfResolver(this->IntParamArray[19]);
		
		// numB
		this->BasParam->SetToleranceOfConvergence(this->SolverConfigParamArray[5]); 
		
		//numC
		this->BasParam->SetSpacesOfKrylov(this->IntParamArray[20]);
		
		//numD - fill param
		this->BasParam->SetNumberOfCoefficients(this->IntParamArray[18]);
		
		//numE - drop tolerance
		this->BasParam->SetToleranceOfExcuse(this->SolverConfigParamArray[6]); 
		}
	else // se acopĺamento é segregated
		{
		//*******************************************************
		//*******************************************************
		// substep 1 é relacionado com configuracao do modelo 1D
		//*******************************************************
		//*******************************************************
		//opA sempre F
		this->BasParam->SetResultantMatrixIsSymmetrical(false);
		
		//opB sempre T
		this->BasParam->SetCalculationStepIsLinear(true);
		
		this->BasParam->SetMaxIterationSubStep(this->IntParamArray[12]);
		
		this->BasParam->SetRalaxacaoSubStep(this->ConvergenceParam1D[8]);
		
		// num3 codigo do tipo de resolvedor do substep 1D
		
		// se paralelo
		if (this->IntParamArray[30]) 
			this->BasParam->SetResolveLibrary(0);
		else
			this->BasParam->SetResolveLibrary(this->IntParamArray[21]);
		
		
		// como OpB=T definir os NumA NumB NumC NumD NumE
		// numA
		this->BasParam->SetMaxIterationOfResolver(this->IntParamArray[23]);
		
		// numB
		this->BasParam->SetToleranceOfConvergence(this->SolverConfigParamArray[7]); 
		
		//numC
		this->BasParam->SetSpacesOfKrylov(this->IntParamArray[24]);
		
		//numD - fill param
		this->BasParam->SetNumberOfCoefficients(this->IntParamArray[22]);
		
		//numE - drop tolerance
		this->BasParam->SetToleranceOfExcuse(this->SolverConfigParamArray[8]); 
		}
	
		
	// criando lista de Elementos
	//vtkIntArray *TempArray = vtkIntArray::New(); //[this->BasParam->GetQuantityOfDifferentElements()];	

	// lista de elementos habilitados
	vtkIntArray *Temp2Array = vtkIntArray::New();
	Temp2Array->SetNumberOfTuples(this->BasParam->GetQuantityOfDifferentElements());	
	
//	// se Blood constutive law = Casson entao elemento de volume sera 5245
//	// e elemento de parede sera 5707
//	if (!this->IntParamArray[2])
//		{
//		// somente no substep 1
//		this->VolumeElementType = 5245;
//		this->SurfaceElementType= 5705;
//		}
//	else // se Blood constutive law = Newton
//		{	
//		// somente no substep 1
//		this->VolumeElementType = 524;
//		this->SurfaceElementType= 57;
//		}
	
	
	
//		if (this->Max1DElemLib == 2)
//			{
//			this->BasParamElementList->InsertNextValue(1); 
//			this->BasParamElementList->InsertNextValue(3);
//			}
//		else
//			{	
//			this->BasParamElementList->InsertNextValue(13); 
//			this->BasParamElementList->InsertNextValue(10);
//			this->BasParamElementList->InsertNextValue(11); 
//			this->BasParamElementList->InsertNextValue(12);	
//			}
//	
//	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
//		{
//		
//		this->BasParamElementList->InsertNextValue(this->VolumeElementType);
//		 
//		
//		for (int z = 0; z < this->GetFullModel(i)->GetNumberOfShells(); ++z)
//			this->BasParamElementList->InsertNextValue(this->SurfaceElementType);
//		
//		for (int z = 0; z < this->GetNumberOfCovers(i); ++z)
//			if (!this->IntParamArray[7])
//				this->BasParamElementList->InsertNextValue(54); // se monolitico usamos no substep 1 elem. 54
//			else
//				this->BasParamElementList->InsertNextValue(542); // se monolitico usamos no substep 1 elem. 542
//				
//		for (int z = 0; z < this->GetNumberOfCovers(i); ++z)
//			this->BasParamElementList->InsertNextValue(23);
//		
//		}
//	 
//		this->BasParamElementList->InsertNextValue(99);	
//		this->BasParamElementList->Resize(this->BasParam->GetQuantityOfDifferentElements());	
	
	// usado na identificacao de matriz de acoplamento simbólica de cada elemento
	// guarda o valor de elementos ativos do primeiro substep
	int LastCalculatedElement = -1;
	
	for (int i = 0; i < Temp2Array->GetNumberOfTuples(); ++i)
		{
		if (this->BasParamElementList->GetValue(i) == 99) // ELEMENTO NULO 99 
			Temp2Array->SetValue(i,0);
		else
			{
			// se monolitico todos elementos neste substep estarão habilitados
			if (!this->IntParamArray[7])
				{
				// se o elemento é de parede e parede é rigida
				if (this->BasParamElementList->GetValue(i) == this->SurfaceElementType &&  !this->IntParamArray[1] ) 	
					Temp2Array->SetValue(i,0);
				else
					{
					Temp2Array->SetValue(i,LastCalculatedElement);
					LastCalculatedElement--;
					}
					
				}
			else // se acoplamento segregado (qualquer dos dois tipo) somente os elementos 1D e elementos de Acoplamento estarao habilitados
				{
				if (i < this->Max1DElemLib)
					{
					Temp2Array->SetValue(i,LastCalculatedElement);
					LastCalculatedElement--;	
					}
				else // demais elementos 3D
					{
					if (this->BasParamElementList->GetValue(i) == 542 || this->BasParamElementList->GetValue(i) == 23) // ELEMENTO Acoplamento 542 e terminais de acoplamento 23
						{
						Temp2Array->SetValue(i,LastCalculatedElement);
						LastCalculatedElement--;
						}
					else
						Temp2Array->SetValue(i,0);	// elementos 57, 524, 99
					}	
				}
			}
		}
	
		
	int *elementType = new int[this->BasParam->GetQuantityOfDifferentElements()*5];

	// inicializa a matriz com zeros
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		for (int j=0; j<5; j++)
			elementType[i*5+j] = 0;


	// Configurando para o primeiro Substep --> 1D
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			
			if (!j) elementType[i*5+j] = this->BasParamElementList->GetValue(i);
			
			if (j == 3) elementType[i*5+j] = Temp2Array->GetValue(i);
			
			
			//*************************************************
			// aditividades dos elementos
			
			if (j==1 && i < this->Max1DElemLib && i) // elementos 1D -- // segunda coluna do elemento 1 -- sempre será zero
				elementType[i*5+j] = (!this->IntParamArray[14]);
			else //demais elementos
				{
				// setando coluna 2   --> Aditividade dos Elementos 3D
				if (j==1 && i >= this->Max1DElemLib && i < this->BasParam->GetQuantityOfDifferentElements()-1) // ultima condicao do IF trata do elem. 99 que tem sempre adtividade Nula
					{
					elementType[i*5+j] = (!this->IntParamArray[15]);
					}
				}	
			 
			}
		}
	
	// setando aditividade dos elementos de volume que é sempre zero
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		if (!this->GetElementTypeFromCode(elementType[i*5]))
			elementType[i*5+1] = 0;
		}		
	
	
		
	Temp2Array->Delete();	
		
	this->BasParam->SetElementType(elementType);
//	
	double *paramElementType = new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	
	// seta todos elementos do vetor com valor nulo
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=0.0;
		}



	/*
	
	****************************************** Elementos 1D ******************************************
	*******************************************************************************************************************************
	* 
	
	Definicao do CommomPar --
	********************************
	para elementos:1, 11, 12 e 13 
	********************************
	CommonPar(1): Densidade do fluido.
	CommonPar(2): Viscosidade do fluido.
	CommonPar(3): Coeficiente indicando o perfil de velocidade que assume o modelo 1D (usualmente 1).
	CommonPar(4): Indicador da posição do grau de liberdade da pressão a partir do primeiro grau de liberdade 
	(2-problema bidimensional, 3-problema tridimensional).
	
	CommonPar(5): Parâmetro de implicitude temporal do esquema theta (valor entre 0 e 1).
	CommonPar(6): Indicador do tipo de lei constitutiva usada no modelo 1D (usualmente 2).
	
	CommonPar(7): Parâmetro de gravidade na direção X (não usado, portanto vai valor dummy, usualmente 0).
	CommonPar(8): Parâmetro de gravidade na direção Y (não usado, portanto vai valor dummy, usualmente 0).
	CommonPar(9): Parâmetro de gravidade na direção Z (não usado, portanto vai valor dummy, usualmente 0). 
	*/
	
	/*
	********************************
	CommomPar para elementos: 10 e 3
	********************************
	CommonPar(1): Parâmetro de penalização. Só se o elemento for aditivo. Vale 0 se é não aditivo e -1 se é aditivo.
	CommonPar(2): Indicador da posição do grau de liberdade da pressão a partir do primeiro grau de liberdade 
	(2-problema bidimensional, 3-problema tridimensional). 
	*/
	
	//double *paramElementType = new double[this->BasParam->GetQuantityOfDifferentElements()*this->BasParam->GetMaximumQuantityOfParameters()];
	
	
		for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
			{
			for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=0.0;
			}
	
	
	
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<this->BasParam->GetMaximumQuantityOfParameters() ; j++)
			{
			//paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=0.0;
			
			//***************************
			//***************************
			// setando os elementos da segunda linha do CommonPar que correspondem ao elemento terminal
			// elementos TERMINAIS 10 e 3
			
			// sempre segunda linha do commonpar corresponderá ao elemento terminal
			// se Aditivity = yes entao seta valor de penalization para terminal
			if ((i==1 && !j) && (this->IntParamArray[14])) // segunda linha e coluna 1
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->SolverConfigParamArray[1];
			//else
			//
			if ((i==1 && !j) && (!this->IntParamArray[14])) //segunda linha e coluna 1
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 1.0;	
			
			if (i==1 && j==1) // segunda linha e coluna 2   aqui no acoplamento sempre tem-se problema 3D
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 3; //(this->IntParam->GetValue(4) -1);
				
			//***************************
			//***************************
			
			
			
			if (i < Max1DElemLib)
				{
			
				//***************************
				// setando os elementos das linhas 1 (#2 elementos) e linhas 1, 3 e 4 (#4 elementos)
				// 
				// primeira coluna tem o valor Densidade do fluido
				if (!j && i!=1)  paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->ModelConfigParamArray[0];
							
				// segunda coluna : Viscosidade do fluido. 
				if (j==1 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->ModelConfigParamArray[1];
				
				// terceira coluna Velocity Profile;
				if (j==2 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->ModelConfigParamArray[2];
				
				// quarta coluna posicao da pressao em funcao do primeiro DOF
				if (j==3 && i!=1)
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= 3;
				
				// coluna 5 parameter theta
				if (j==4 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]= this->SolverConfigParamArray[0];
						
				// coluna 6 Artery Wall Law
				if (j==5 && i!=1) paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+j]=  2;
			
				}
			
	
	//****************************************** Elementos 3D ******************************************
	//**************************************************************************************************
			if ( (elementType[i*5] == 524 || elementType[i*5] == 5245) && (!j)) 
				{
				//***************************
				// Configuracao do elementos volume 524 ou 5245
				//**************************				
				
				
				//Elemento de Volume 5245
				//CommonPar(1): Densidade do fluido.
				//CommonPar(2): Viscosidade do fluido (é a viscosidade assintótica do modelo não linear caso CoommonPar(8)=1).
				//CommonPar(3): Compresibilidade artificial do fluido (usualmente 1.0E-05).
				//CommonPar(4): Parâmetro de implicitude temporal do esquema theta (valor entre 0 e 1).
				//CommonPar(5): Indicador de simetria (0-não há simetria, 1-há simetria).
				//CommonPar(6): Indicador de eixo de simetria (1-eixo X é de simetria, 0-eixo Y é de simetria). Vale só se CommonPar(5)=1.
				//CommonPar(7): Indicador de esquema incremental, isto é, updated Lagrangian (0-não incremental, 1-incremental).
				//CommonPar(8): Indicador de lei constitutiva do fluido não linear (0-Newtoniano, 1-Casson).
				//CommonPar(9): Parâmetro de tensão limite da lei constitutiva não linear. Só se CommonPar(8)=1.
				//CommonPar(10): Parâmetro de regularização da lei constitutiva não linear (usualmente 0.1 ou 0.01). Só se CommonPar(8)=1.
				//CommonPar(11): Parâmetro de gravidade na direção X.
				//CommonPar(12): Parâmetro de gravidade na direção Y.
				//CommonPar(13): Parâmetro de gravidade na direção Z. Só se forem três dimensões. 
				
				
				// coluna zero densidade - COMMONPAR 1
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+ 0]= this->ModelConfigParamArray[3];	
				
				// viscosidade - COMMONPAR 2
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1]= this->ModelConfigParamArray[4];	
				
				// compressibilidade // geralmente  1.0E-05) - COMMONPAR 3
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2]= 1.0E-05;	

				// coluna 4  theta - COMMONPAR 4
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3]= this->SolverConfigParamArray[2];
				
				// coluna 6 -- sempre Zero - COMMONPAR 6
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+5]= 0.0;
				
				// coluna 7 indicador de esquema incremental
//					// se o modelo é rigido incremental version é nula
//				if (!this->IntParamArray[1]) 
//					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+6]= 0;
//				else // se modelo é deformavel
//					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+6]= 1;

				// - COMMONPAR 7 
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+6]= this->IntParamArray[16];  //		16 -Incremental Version 0= no/1 =yes (3D)
					
				// coluna 8 - COMMONPAR 8
				if (!this->IntParamArray[2])
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+7]= !this->IntParamArray[2];
				
				// coluna 9 limit Stress  - COMMONPAR 9
				if (!this->IntParamArray[2])
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+8]= this->ModelConfigParamArray[6];
				
				// coluna 10 Regulation Param - COMMONPAR 10
				if (!this->IntParamArray[2])
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+9]= this->ModelConfigParamArray[7];
				
				}
				
				
		   //***************************
			 // Configuracao do elementos parede 57, 5705
			 //**************************
			 
			//57: CommonPar -> 12, Param -> 4, JParam -> 0
			//CommonPar(1): Parâmetro de penalização. Só se o elemento for aditivo. Vale 0 se é não aditivo e -1 se é aditivo.
			//CommonPar(2): Coeficiente da normal (1-se as normais são externas, -1-se as normais são internas).
			//CommonPar(3): Parâmetro não usado.
			//CommonPar(4): Coeficiente para enrigecer a parede arterial (este multiplica a elastina indicada originalmente).
			//CommonPar(5): Indicador de simetria (0-não há simetria, 1-há simetria).
			//CommonPar(6): Indicador de eixo de simetria (1-eixo X é de simetria, 0-eixo Y é de simetria). Vale só se CommonPar(5)=1.
			//CommonPar(7): Valor de elastina vezes a relação espessura sobre raio do elemento (usualmente elastina*0.1). Só se CommonPar(11)=0.
			//CommonPar(8): Valor do raio de curvatura do elemento. Só se CommonPar(11)=0.
			//CommonPar(9): Valor de pressão de referência do elemento. Só se CommonPar(11)=0.
			//CommonPar(10): Valor de viscoelasticidade vezes a relação espessura sobre raio do elemento (usualmente viscoelasticidade*0.1). Só se CommonPar(11)=0.
			//CommonPar(11): Indicador de leitura em Basparam.txt ou em Param.txt dos parâmetros deste elemento (1-lê do Param.txt, 0-lê do Basparam.txt).
			//CommonPar(12): Indicador de esquema incremental, isto é, updated Lagrangian (0-não incremental, 1-incremental).  
			
			// Incremental Version = 0 nao incremental e 1 incremental
			 
			if ( (elementType[i*5] == 57 || elementType[i*5] == 5705) && (!j) )
				{
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+0] = 1;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1] = -1; // normal
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2] = 0;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3] = 1;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+5] = 0;	
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+10]= 1;
					
//					if (!this->IntParamArray[1]) // se é rigido
//						paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+11]= 0;
//					else // se modelo é deformavel
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+11]= this->IntParamArray[16];  //		16 -Incremental Version 0= no/1 =yes (3D)
				}


			  //***************************
				// Configuracao dos elementos Acoplamento tampa 54 541 542
				//**************************
			//54: CommonPar -> 6, Param -> 0, JParam -> 0
			//CommonPar(1): Parâmetro real de penalização. Se o elemento for aditivo (segunda coluna da lista de identificação dos elementos) valor de penalização alto. Se elemento não aditivo valor de penalização é 1.0
			//CommonPar(2): Coeficiente da normal (1-se as normais são externas, -1-se as normais são internas).
			//CommonPar(3): Indicador de esquema incremental, isto é, updated Lagrangian (0-não incremental, 1-incremental).
			//CommonPar(4): Parâmetro de implicitude temporal do esquema theta (valor entre 0 e 1).
			//CommonPar(5): Indicador de simetria (0-não há simetria, 1-há simetria).
			//CommonPar(6): Indicador de eixo de simetria (1-eixo X é de simetria, 0-eixo Y é de simetria). Vale só se CommonPar(5)=1. 

			if ( (elementType[i*5] == 54 || elementType[i*5] == 541 || elementType[i*5] == 542) && (!j) )
				{
				
				double Penalization = 1.0;
				
				//se é aditivo
				if (this->IntParamArray[15])
				 Penalization = SolverConfigParamArray[3];
				
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+0] = Penalization;
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1] = -1; //normal
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2] = this->IntParamArray[16];
				paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3] = 1.0; //sempre 1
				
				
				}
				
			//***************************
			// Configuracao dos elementos Terminais de Acoplamento tampa 23
			
			//23: CommonPar -> 4, Param -> 0, JParam -> 0
			//CommonPar(1): Parâmetro real de penalização. Se o elemento for aditivo (segunda coluna da lista de identificação dos elementos) valor de penalização alto. Se elemento não aditivo valor de penalização é 1.0.
			//CommonPar(2): Valor da pressão do terminal associado a este elemento acoplamento. Usualmente 0.
			//CommonPar(3): Valor da capacitância do terminal associado a este elemento acoplamento. Usualmente 0.
			//CommonPar(4): Direção do fluxo. Indica-se com 1/-1 (1-nó de entrada, -1-nó de saída). 

				if ( (elementType[i*5] == 23) && (!j) ) 
					{
					
					double Penalization = 1.0;
					
					//cout << "Setando param elem 23 linha " << i << endl;
					
					//se é aditivo
					if (this->IntParamArray[15])
					  Penalization = SolverConfigParamArray[3];
					
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+0] = Penalization;
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+1] = 0;
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+2] = 0;
					
					//CouplingTerminalNumber indica a ordem (em relacao a todos elementos de acoplamento) do elemento de acoplamento 23 
					paramElementType[i*this->BasParam->GetMaximumQuantityOfParameters()+3] = this->TerminalFlowDirection->GetId(this->CouplingTerminalNumber);
					this->CouplingTerminalNumber++;
					}

			}
		}			



	//*************************************************
	// 	*Elementos e SolverConfig SUBSTEP 3D do Acoplamento Normal Segregated
	// ************************************************
	
	// se o acoplamento é monolitico, nao calcula o substep 3D
	if (this->IntParamArray[7] == 1)
		{
		this->BasParam->SetResultantMatrixIsSymmetricalSubStep3D(false);

		this->BasParam->SetCalculationStepIsLinearSubStep3D(true);
		
		this->BasParam->SetMaxIterationSubStep3D(this->IntParamArray[11]);

		this->BasParam->SetRalaxacaoSubStep3D(this->ConvergenceParam3D[14]);
		
		// se paralelo
		if (this->IntParamArray[30]) 
			this->BasParam->SetResolveLibrarySubStep3D(200);
		else
			this->BasParam->SetResolveLibrarySubStep3D(this->IntParamArray[25]);
		
		// como OpB=T definir os NumA NumB NumC NumD NumE
		// numA
		this->BasParam->SetMaxIterationOfResolver3D(this->IntParamArray[27]);
		
		// numB
		this->BasParam->SetToleranceOfConvergence3D(this->SolverConfigParamArray[9]); 
		
		//numC
		this->BasParam->SetSpacesOfKrylov3D(this->IntParamArray[28]);
		
		//numD - fill param
		this->BasParam->SetNumberOfCoefficients3D(this->IntParamArray[26]);
		
		//numE - drop tolerance
		this->BasParam->SetToleranceOfExcuse3D(this->SolverConfigParamArray[10]); 
		
		
	
		int *elementType3D = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
	
		for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
			{
			for (int j=0; j<5; j++)
				{
				elementType3D[i*5+j] = elementType[i*5+j];
				
				if (elementType[i*5+j] == 542)
					elementType3D[i*5+j] = 541;
	
	
				// se é segregado somente os elementos 3D e acoplamento estarao habilitados
				if (j==3 && this->IntParamArray[7])
					{
				 	if (i < this->Max1DElemLib) // && i <= this->Max1DElemLib+1 ) 
				 		{
						elementType3D[i*5+j]=0;  // desabilita elementos 1D
						}
					else
						{
						if (i==this->BasParam->GetQuantityOfDifferentElements()-1)
							elementType3D[i*5+j]=0; // ELEMENTO NULO 99 
						else
							{
							// demais elementos 3D e acoplamento
							elementType3D[i*5+j]=LastCalculatedElement;
				 			LastCalculatedElement--;
							}
						}
					}
	
	
				}
			}
		//cout << "Last elemento volume " << LastCalculatedElement << endl;
		this->BasParam->SetElementTypeSubStep3D(elementType3D);
	
		}
	// ************************************************
	// ************************************************
	//*************************************************
	// 	*Elementos e SolverConfig SUBSTEP 3D - SuperSegregated Model
	// ************************************************
	// ************************************************
	// ************************************************
	
	
	// se o acoplamento é monolitico, nao calcula o substep 3D SuperSegregated
	if (this->IntParamArray[7] == 2)
		{
		this->BasParam->SetResultantMatrixIsSymmetricalSubStep3D(false);

		this->BasParam->SetCalculationStepIsLinearSubStep3D(true);

		// se paralelo
		if (this->IntParamArray[30]) 
			this->BasParam->SetResolveLibrarySubStep3D(200);
		else
			this->BasParam->SetResolveLibrarySubStep3D(this->IntParamArray[25]);

		this->BasParam->SetMaxIterationSubStep3D(this->IntParamArray[11]);

		this->BasParam->SetRalaxacaoSubStep3D(this->ConvergenceParam3D[14]);
		
		// como OpB=T definir os NumA NumB NumC NumD NumE
		// numA
		this->BasParam->SetMaxIterationOfResolver3D(this->IntParamArray[27]);
		
		// numB
		this->BasParam->SetToleranceOfConvergence3D(this->SolverConfigParamArray[9]); 
		
		//numC
		this->BasParam->SetSpacesOfKrylov3D(this->IntParamArray[28]);
		
		//numD - fill param
		this->BasParam->SetNumberOfCoefficients3D(this->IntParamArray[26]);
		
		//numE - drop tolerance
		this->BasParam->SetToleranceOfExcuse3D(this->SolverConfigParamArray[10]); 
		
		
		for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
			{ // A CADA iteracao de i habilita um grupo de elementos 3D

			// Cria lista de Index dos elementos de volume
			vtkIntArray *VolumeElementsList = vtkIntArray::New();


			// Copia o Array
			int *elementType3DSuperSegregated = new int[this->BasParam->GetQuantityOfDifferentElements()*5];
			for ( int z=0; z<this->BasParam->GetQuantityOfDifferentElements(); z++ )
				{
					for (int j=0; j<5; j++)
						{
						// copia array
						elementType3DSuperSegregated[z*5+j] = elementType[z*5+j];
						
						// substep 3d elemento de acoplamento 541 
						if (elementType[z*5+j] == 542)
							elementType3DSuperSegregated[z*5+j] = 541;
					
						if (j == 3)
							elementType3DSuperSegregated[z*5+j] = 0;
						}
				}

			int size = 0;
			for (int z = 0; z < this->BasParam->GetQuantityOfDifferentElements(); ++z) 
				{
				for (int j=0; j<5; j++)
						{
						if (elementType3DSuperSegregated[z*5+j] == 5245 || elementType3DSuperSegregated[z*5+j] == 524 || elementType3DSuperSegregated[z*5+j] == 99)
							{
							VolumeElementsList->InsertNextValue(z);
							size++;
							}
						}
				}			
				
			VolumeElementsList->Resize(size);		

			for (int z = 0; z < this->BasParam->GetQuantityOfDifferentElements(); ++z) 
				{
				for (int j=0; j<5; j++)
					{
					if (j==3 && (z >= VolumeElementsList->GetValue(i)) && (z <= (VolumeElementsList->GetValue(i+1)-1)) )
						{
						elementType3DSuperSegregated[z*5+j] = LastCalculatedElement;
						LastCalculatedElement--;	
						}
					}	
				}
			VolumeElementsList->Delete();
			this->Substeps3DElementList.push_back(elementType3DSuperSegregated);
			}
		this->BasParam->SetSubStepSuperSegregated(&this->Substeps3DElementList);
		
		}



	//*************************************************
	// 	*Elementos e SolverConfig SUBSTEP BUBBLE
	// ************************************************
	
	this->BasParam->SetResultantMatrixIsSymmetricalSubStepBubble(true);
	this->BasParam->SetCalculationStepIsLinearSubStepBubble(false);

	// codigo do tipo de resolvedor
	
	// se paralelo
	if (this->IntParamArray[30]) 
		this->BasParam->SetResolveLibrarySubStepBubble(20061129);
	else
		this->BasParam->SetResolveLibrarySubStepBubble(0);
		
	
	int *elementTypeBubble = new int[this->BasParam->GetQuantityOfDifferentElements()*5];

	// copia array
	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			elementTypeBubble[i*5+j] = elementType[i*5+j];
			if (j==3)
				elementTypeBubble[i*5+j] = 0;
			
			if (elementType[i*5+j] == 541 || elementType[i*5+j] == 542)
				elementTypeBubble[i*5+j] = 54;	
			}
		}

	for ( int i=0; i<this->BasParam->GetQuantityOfDifferentElements(); i++ )
		{
		for (int j=0; j<5; j++)
			{
			if (elementTypeBubble[i*5+j]==  524 || elementTypeBubble[i*5+j]== 5245)
				{
				// se paralelo
				if (this->IntParamArray[30]) 
					{
					elementTypeBubble[i*5+j]= 525;
					// j assume valor de 2 pois corresponde a coluna que esta habilitada somente quando temos solver paralelo
					elementTypeBubble[i*5+2] = 1;
					}
				else	
					{
					elementTypeBubble[i*5+j]= 525;
					// j assume valor de 3 pois corresponde a coluna de habilitacao
					elementTypeBubble[i*5+3] = LastCalculatedElement;
					LastCalculatedElement--;
					}
				}
			if (elementTypeBubble[i*5+j]==  5705)
				elementTypeBubble[i*5+j]= 57;
			}
		}

	this->BasParam->SetElementTypeSubStepBubble(elementTypeBubble);
	this->BasParam->SetParamElementType(paramElementType);
	this->BasParam->SetNormaSubStep(norma);
	this->BasParam->SetToleranceSubStep(Tolerance);
	this->BasParam->SetNameDegreeOfFreedomSubStep(name);

//	Nums[0]=0;
//	Nums[1]=0;
//	Nums[2]=0;
	
	this->BasParam->SetNumsSubStep(Nums);
	this->BasParam->SetLogFilename(this->LogFileName);
	
	
	
	this->BasParam->SetCoupledModel(1);
	
	this->BasParam->SetNumberOfSubsteps(this->GetNumberOf3DFullModels() + 2);// numero de 3Ds + 1 substep 1D e 1 substep Bublle
	
	
	
	// modelo segregado ou monolitico
	// 0 -- monolitico e 1 - N. segregated 2- Super Segregado 
	this->BasParam->SetCouplingScheme(this->IntParamArray[7]);
	

	this->BasParam->SetSegregatedSubStepOrder(this->IntParamArray[29]);
	
	
	
	this->BasParam->SetSolverType(this->IntParamArray[30]); //30 - Solver type (0: Sequential 1: Parallel));
	
	// OPCOES SETADAS SOMENTE QUANDO O SOLVER É PARALELLO
	
	
	this->BasParam->SetParallelSolverNumberOfProcessors(this->IntParamArray[31]);
	
	
	this->BasParam->SetParallelSolverRLS(this->IntParamArray[17]);
	
	this->BasParam->SetParallelSolverGMRESIterations(this->IntParamArray[32]);
	
	this->BasParam->SetParallelSolverMaxIterations(this->IntParamArray[33]);
	
	
	this->BasParam->SetParallelSolverPreConditioning(this->IntParamArray[34]);
	
	
	this->BasParam->SetParallelSolverRelativeConvError(this->SolverConfigParamArray[11]);
	
	this->BasParam->SetParallelSolverAbsConvError(this->SolverConfigParamArray[12]);

	
		
}

//*********************************************************************************


void vtkHMCoupledModelWriterPreProcessor::SetElementType(int SurfaceElementType, int CoverElementType, int VolumeElementType)
{
	this->SurfaceElementType = SurfaceElementType;
	this->CoverElementType   = CoverElementType;
	this->VolumeElementType  = VolumeElementType;
} 

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::SetLogFileName (const char *LogFileName)
{
	strcpy(this->LogFileName, LogFileName);
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetTimeParam(double *TimeParamArray)
{
	this->TimeParamArray = TimeParamArray;  	
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetIntegerParam(int *IntParamArray)
{
	this->IntParamArray =  IntParamArray;  	
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetSegregatedSubRelaxation(double value)
{
this->SegregatedSubRelaxation = value;
}

//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::SetMonolithicSubRelaxation(double value)
{
this->MonolithicSubRelaxation = value;
}

//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::Set3DConvergenceParam(double *array)
{
this->ConvergenceParam3D = array;
}

//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::Set1DConvergenceParam(double *array)
{
	this->ConvergenceParam1D = array;
	
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetSolverParam(double *array)
{
	this->SolverConfigParamArray = array;  	
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetModelConfigParam(double *ModelConfigParamArray)
{
	this->ModelConfigParamArray = ModelConfigParamArray;  	
}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetPointID(double x, double y, double z)
{
	for (int i=0; i < this->points->GetNumberOfPoints(); i++)
		{
		double *pointcoord = this->points->GetPoint(i);
		if (DoubleCompare(pointcoord[0],x) && DoubleCompare(pointcoord[1],y) && DoubleCompare(pointcoord[2],z))
			{
			return i+1;
			break;
			}
		}
	return 0;	 
}
//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::DoubleCompare(double one, double two)
{
	if (fabs(one - two) < 1E-5)
		return 1;
	else	
		return 0;
}

//-------------------------------------------------------------------------------------



void vtkHMCoupledModelWriterPreProcessor::BuildTransposeConnectivity(vtkHM3DFullModel *FullModel)
{
	this->TransposeConnectivity.clear(); 
	this->GroupFromPoint.clear();
	
	vtkSurfaceGen* surfGen = FullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  SurfTriang* curr; // = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.


  int NumNodesSurface = surfGen->GetCoords()->GetNumNodes();
    
  // constroi a conectividade transposta que relaciona nó de surface com os elementos vizinhos a este
  // por exemplo: nó 1 --> elementos viznhos: 2, 5 ,6
  //                 2 --> 33 55 6 44 55		

	int element=1;    
	curr = first;
	int groups=1;

	for (int i=0; i < NumNodesSurface; i++) // percorre todos nos de superficie
		{
		ListOfInt lst;
		lst.clear();
  	while (curr)
  		{
    	if (curr->n1 == i || curr->n2 == i || curr->n3 == i) // verifica se no triangulo atual existe referencia ao nó atual 
    		{
    		lst.push_back(element); // se sim, insere o id do elemento na lista de inteiro 
    		//cout << i << " "<< element << " "; 
    		//cout << "grupo " << curr->gr << endl;
    		
    		// esta variavel armazena o grupo dos elementos da lista atual
    		// o qual sera usuada no metodo GetGroupFromPoint
    		// se por exemplo existe algum elemento que é de parede (grupo 0) o valor da variavel sera zero (por isso é usado multiplicacao)
    		// se for todos grupos forem 1, entao valor da variavel sera 1
    		// se tivermos 1 1 1 1 1 -- valor final =1 (nó esta no interior da tampa grupo 1)
    		// se tivermos 0 0 0 0 0 2 -- valor final =0 (no esta na fronteira - aqui consideramos que o no é de parede)
				// se tivermos 2 2 2 2 2 -- valor final =32 (nó está dentro do tampa grupo 2)
				// set tivermos 0 0 0 0 0 -- valor final=0 (nó estara na parede arterial)
    		groups= curr->gr*groups; 
    		}      
    	curr = curr->next; // passa para o proximo triangulo
    	element++; 
  		}
		// no final do while tem uma lista de inteiros de tamanho variavel que descreve 
		// os elementos viznhos do ponto indicado por i 
		// ex: (23, 45 ,334, 6787)
		
		if (groups==0 || groups==1 )
			//cout << "Dominante group >" << groups << endl;
			this->GroupFromPoint.push_back(groups);
			
		else
			{
			// para variaveis group que tem valor diferente de 0 ou 1
			// o seguinte processo é utilizado para descoberta do grupo
			// suponto que tem-se o valor de 32 e tamanho da lista 5 elementos
			// para descobrir o grupo dos pontos é utilizada a formula
			// grupo_dominante= ( group^(1/numero_elem_lista) )
			int list_size =lst.size();
			double exp= 1.0/float(list_size);
			this->GroupFromPoint.push_back(int(pow (double(groups),exp)));

			// cout << "Lst.size " << lst.size() << endl;
			//cout << "Exp " << exp << endl;
			//cout << "Dominante group #####################=======>" << pow (double(groups),exp) << endl;
			}
		//cout << endl;
		groups=1;
		curr = first;
		element=1;
		TransposeConnectivity.push_back(lst);          
		}

}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetGroupFromPoint(int point)
{
	//cout << "Point " << point << endl;
	//point--;
	
	int i=0;
	ListOfInt::iterator it = GroupFromPoint.begin();
	for (; it !=  GroupFromPoint.end();it++ )
		{
		if (i==point)
			return (*it);
		i++;
		}
}

//-------------------------------------------------------------------------------------

vtkHM1DMesh::VectorOfIntList *vtkHMCoupledModelWriterPreProcessor::GetIncidenceElements1D()
{
	return &IncidenceElements1D;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::GenerateIncidence3D()
{
	vtkDebugMacro(<<"vtkHMCoupledModelWriterPreProcessor::GenerateCoordinates()");
  
  if (!this->NumberOfPoints1D)
  	vtkErrorMacro(<<"Number of points from 1D not defined yet");

  //this->NumberOf1DElements = this->ElementsIncidence.size();
  
  // existe diferenca entre esses 2 valores!!!!!!!!
  
  
  //**************************************************
  this->ModelInfo->InsertNextId(this->NumberOf1DElements);
  
  
  //0 - Least Square / 1= ViscoElatsic (1D)
  if (this->IntParamArray[0])
  	this->ModelInfo->InsertNextId(4);
  else
  	this->ModelInfo->InsertNextId(2);
  //**************************************************	
  	
  vtkIdList *ids;
	vtkHM1DMesh::ListOfInt lst;
  lst.clear();
  
  // valor para deslocar o referenciamento dos pontos 3D com relacao
  // a posicao na lista de coordenadas 
  int ShifitingValue = 0; 
  
  it = this->FullModelList.begin();
  
  
  int NumberOf3DsModels = this->GetNumberOf3DFullModels();
  
  this->ModelInfo->InsertNextId(NumberOf3DsModels);
  
  // percorre a lista de FullModels envolvidos no modelo acoplado
  // e cria a lista de incidencia dos elementos de volume 
  for (int i = 0; i < NumberOf3DsModels; ++i)
		{
		
		this->CurrentFullModel = (*it);
		
		//Obtendo o numero de elementos do volume atual.
		int NumberOfVolumeElements = this->CurrentFullModel->GetVolume()->GetNumberOfCells();
		
		this->ModelInfo->InsertNextId(NumberOfVolumeElements);
		
  	
  	if (!i)
  		ShifitingValue = this->NumberOfPoints1D + 1;
  	else
  		//ShifitingValue = this->FullModelNumberOfPoints->GetValue(i-1) +  this->NumberOfPoints1D + 1 ;	 
  	  ShifitingValue = this->GetNumberOfVolumePoints(i) +  this->NumberOfPoints1D + 1 ;  
  	
  	
  	int VolumeElement = this->GetVolumeElementPosition(i);
  	
  	// percorre todos elementos de volume do FullModel atual
  	for (int z = 0; z < NumberOfVolumeElements; z++)
			{
			ids = this->CurrentFullModel->GetVolume()->GetCell(z)->GetPointIds();
			lst.push_back(ids->GetId(0) + ShifitingValue);
			lst.push_back(ids->GetId(1) + ShifitingValue);
			lst.push_back(ids->GetId(2) + ShifitingValue);
			lst.push_back(ids->GetId(3) + ShifitingValue);
			this->ElementsIncidence.push_back(lst);
			lst.clear();
		
			// ELEMENT TYPE dos elementos de VOLUME
			this->ElementTypeArray->InsertNextValue(VolumeElement); //this->Max1DElemLib+1);  
			}	
		
		it++;
		}	
 	
//***********************************************************************
//***********************************************************************
		// INCIDENCIA DOS TRIANGULOS DE SUPERFICIE
 		// Obs===> Aqui irão somente os elementos que compoe a casca da superficie
 		// os elementos das tampas irao na incidencia dos elementos de acoplamento 	
//***********************************************************************
//*********************************************************************** 	
 	it = this->FullModelList.begin();
 	// percorre a lista de FullModels envolvidos no modelo acoplado
  // e cria a lista de incidencia dos elementos de surface  	
	for (int i = 0; i < NumberOf3DsModels; ++i)
		{
		this->CurrentFullModel = (*it);
		
		int CurrentNumberOfShells = this->CurrentFullModel->GetNumberOfShells();
		
		this->ModelInfo->InsertNextId(CurrentNumberOfShells); // informacao de numeros de cascas por fullmodel para ser usada na escrita de arquivo de descricao do modelo
		
		
		// acessa referencia do primeiro triangulo da superficie
  	vtkSurfaceGen* surfGen = this->CurrentFullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  	SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  	SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
		
		
		// inserindo informacao dos numeros de elementos dos grupos de casca
		for (int z = 0; z < CurrentNumberOfShells; ++z)
			this->ModelInfo->InsertNextId(surfGen->GetNumElsGroups(z)); 
		//!!!!!!!!!!!!!!!!!!!!!!
		
		if (!i)
  		ShifitingValue = this->NumberOfPoints1D + 1;
  	else
  		//ShifitingValue = this->FullModelNumberOfPoints->GetValue(i-1) + this->NumberOfPoints1D + 1;
  		ShifitingValue = this->GetNumberOfVolumePoints(i) +  this->NumberOfPoints1D + 1 ; 
		
		//int SurfaceElemPosition = this->GetVolumeElementPosition(i) + 1;
		
    curr = first;
    while (curr)
    	{
      if (curr->gr < CurrentNumberOfShells) // so quero elementos que sejam do grupo de parede
       	{
        
        lst.push_back(curr->n1 + ShifitingValue);
        lst.push_back(curr->n2 + ShifitingValue);
        lst.push_back(curr->n3 + ShifitingValue);

        // ELEMENT TYPE DOs elementos de Surface (relacao com Basparam.txt)
        //ElementTypeArray->InsertNextValue(SurfaceElemPosition);
        
        ElementTypeArray->InsertNextValue(this->GetVolumeElementPosition(i) + curr->gr + 1);
        
        this->ElementsIncidence.push_back(lst);
        }
      curr = curr->next;
      lst.clear();
    	}
		it++;
		}

//	//*************************************************************
//	// BLOCO DE CODIGO PARA COLETAR INFORMACAO SOBRE GRUPOS CASCA
//  // pega informacao do numero de elementos de casca por fullmodel
// 	it = this->FullModelList.begin();
//	for (int i = 0; i < NumberOf3DsModels; ++i)
//		{
//		this->CurrentFullModel = (*it);
//		int CurrentNumberOfShells = this->CurrentFullModel->GetNumberOfShells();
//		
//		// acessa referencia do primeiro triangulo da superficie
//  	vtkSurfaceGen* surfGen = this->CurrentFullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
//  	
//		for (int i = 0; i < CurrentNumberOfShells; ++i)
//			this->ModelInfo->InsertNextId(surfGen->GetNumElsGroups(i)); 			
//		}
//	//*************************************************************
	

		
	// ***********************************************************
 	// percorre a lista de FullModels envolvidos no modelo acoplado
  // e cria a lista de incidencia dos INCIDENCIA DOS ELEMENTOS de Acoplamento Interface 1D-3D
	// ***********************************************************

	it = this->FullModelList.begin();
	for (int i = 0; i < NumberOf3DsModels; ++i)
		{
		
		this->CurrentFullModel = (*it);
		
		
		int CurrentNumberOfCovers = this->CurrentFullModel->GetNumberOfCoverGroups();
		
		this->ModelInfo->InsertNextId(CurrentNumberOfCovers); // informacao de numeros de tampas por fullmodel para ser usada na escrita de arquivo de descricao do modelo
		
		
		
		// acessa referencia do primeiro triangulo da superficie
  	vtkSurfaceGen* surfGen = this->CurrentFullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
  	SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  	SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
		
		
		
		
	 int FirstCover = this->CurrentFullModel->GetNumberOfShells(); 
  	
		for (int z = 0; z < CurrentNumberOfCovers; ++z)
			{
			this->ModelInfo->InsertNextId(surfGen->GetNumElsGroups(FirstCover));
			FirstCover++; 			
			}
		
		
		
		// INCIDENCIA DOS ELEMENTOS de Acoplamento Interface 1D-3D
		// ponto de acoplamento com os pontos dos triangulos da face
		// Exemplo:
		// 500 2 6 3           **face 1 # ponto de acoplamento e pontos que compoe triangulo da tampa
		// 500 10 56 30        **face 1
		// 1500 6553 35455 326 **face 2
		// 1500 63 3555 26     **face 2
		
		
		if (!i)
  		ShifitingValue = this->NumberOfPoints1D + 1;
  	else
  		//ShifitingValue = this->FullModelNumberOfPoints->GetValue(i-1) + this->NumberOfPoints1D + 1;
		  ShifitingValue = this->GetNumberOfVolumePoints(i) +  this->NumberOfPoints1D + 1 ; 
		
		vtkIntArray *temp = this->GetCouplingInfoFrom3D(i);
		
		int CouplingElemPosition = this->GetVolumeElementPosition(i) + this->CurrentFullModel->GetNumberOfShells() +1;

		for (int z = 0; z < CurrentNumberOfCovers; z++) 
			{
			// pega o id do no 1d de acoplamento
			int NodeId = this->GetNodePointID( int(temp->GetTuple4(z)[0]), int(temp->GetTuple4(z)[1]) ); // busca segmento e node info
			curr = first;  
			while (curr)
	  		{
	  		// procura por pontos do grupo que esta ligado ao ponto 1D NodeID
	  		if (curr->gr == temp->GetTuple(z)[2])
	    		{		      
					lst.push_back(NodeId);// ponto de acoplamento
					lst.push_back(curr->n1 + ShifitingValue);
	        lst.push_back(curr->n2 + ShifitingValue);
	        lst.push_back(curr->n3 + ShifitingValue);
	      	this->ElementsIncidence.push_back(lst);
	      	lst.clear();
					
					// ELEMENT TYPE dos elementos de Acoplamento
				  this->ElementTypeArray->InsertNextValue(CouplingElemPosition + z);
	    		}
				curr = curr->next; // passa para o proximo triangulo
	  		}
			}
		it++;
		}	
	
	
	
//	//*************************************************************
//	// BLOCO DE CODIGO PARA COLETAR INFORMACAO SOBRE TAMPAS
//	// pega informacao do numero de elementos de tampa/acoplamento por fullmodel
// 	it = this->FullModelList.begin();
//	for (int i = 0; i < NumberOf3DsModels; ++i)
//		{
//		this->CurrentFullModel = (*it);
//		int CurrentNumberOfCovers = this->CurrentFullModel->GetNumberOfCoverGroups();
//		
//		// acessa referencia do primeiro triangulo da superficie
//  	vtkSurfaceGen* surfGen = this->CurrentFullModel->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
//  	
//  	
//  	int FirstCover = this->CurrentFullModel->GetNumberOfShells(); 
//  	
//		for (int i = 0; i < CurrentNumberOfCovers; ++i)
//			{
//			this->ModelInfo->InsertNextId(surfGen->GetNumElsGroups(FirstCover));
//			FirstCover++; 			
//			}
//		
//		}
	//*************************************************************
	
	
	
	
			
  ///*****************************************************
	// INCIDENCIA DOS ELEMENTOS de Acoplamento Terminais
	//
 	// percorre a lista de FullModels envolvidos no modelo acoplado
  // e cria a lista de incidencia dos elementos de surface  	
  ///*****************************************************
  
  it = this->FullModelList.begin();
	for (int i = 0; i < NumberOf3DsModels; ++i)
		{
		this->CurrentFullModel = (*it);
		
		int TerminalCouplingElemPosition = this->GetVolumeElementPosition(i) + this->CurrentFullModel->GetNumberOfShells() + this->CurrentFullModel->GetNumberOfCoverGroups() + 1;
		
		vtkIntArray *temp = this->GetCouplingInfoFrom3D(i);
		for (int z = 0; z < this->CurrentFullModel->GetNumberOfCoverGroups(); z++) 
				{
				int NodeId = this->GetNodePointID( int(temp->GetTuple(z)[0]), int(temp->GetTuple(z)[1]) );
				lst.push_back(NodeId);
				this->ElementsIncidence.push_back(lst);
		   	
		   	// ELEMENT TYPE DOs elementos de acoplamento
				this->ElementTypeArray->InsertNextValue(TerminalCouplingElemPosition + z);
		   	lst.clear();
				}	
		//temp->Delete();
		it++;
		}

	// Construcao do Vetor ELEMENT TYPE
	// o vetor ElementTypeArray é um vetor auxiliar
	
//	int *ElementType = new int[this->ElementsIncidence.size()];
//
//	for (int i=0; i<this->ElementsIncidence.size(); i++ )
//			{
//			//cout << i << " " << this->ElementTypeArray->GetValue(i) << endl;
//			ElementType[i] = this->ElementTypeArray->GetValue(i);
//			}
	 
}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNodePointID(int id, int order)
{
 	vtkHM1DTreeElement *elem = NULL;
	double x,y,z;
	elem = this->StraightModel->GetSegment(id);
	vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(elem);
	int NodeNumber = Seg->GetNodeNumber();
	vtkHMNodeData *Node = Seg->GetNodeData(order);
	x=Node->coords[0];
	y=Node->coords[1];
	z=Node->coords[2];
	return this->GetPointID(x,y,z);
}

//-------------------------------------------------------------------------------------

bool vtkHMCoupledModelWriterPreProcessor::TreeElementInside3D(int TreeElementID, int ElemOrder)
{
	vtkIntArray *IntArray;
	it = this->FullModelList.begin();
	
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		if (this->GetNumberOfDifferentRegions(i) > 1) // SE FullModel tem mais de 1 estrutura 3D - exemplo: pericalosa (2 3ds em um FullModel)
			{
			for (int z = 0; z < this->GetNumberOfDifferentRegions(i); ++z)
				{
				//IntArray = this->GetCouplingInfoFromSelectedRegion(z, i);
				
				cout << "Modelo 3D com 2 regioes - Marcacao de elementos internos ao 3D desabilitada" << endl;
				
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				// IMPLEMENTAR PARA ESTE CASO
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				}	
			}
		else // apenas 1 regiao
			{
			//cout << "Coletando Coupling info do 3D "<< i << endl;
			
			// coleta informacao de acoplamento do 3D
			IntArray = this->GetCouplingInfoFrom3D(i);
			
			// checar se tem 2 ou mais grupos e acoplamento esta em um mesmo segmento
			if (IntArray->GetNumberOfTuples() == 2 && (IntArray->GetTuple(0)[0] == IntArray->GetTuple(1)[0]) )
				{
				int CouplingSegment = int(IntArray->GetTuple(0)[0]);
				
				if (CouplingSegment != TreeElementID)
					{
					//return false;
					}
				else
					{
					int InFlowNode, OutFlowNode;
					// aqui os pontos de acoplamento sao identificados
					for (int i = 0; i < IntArray->GetNumberOfTuples(); ++i)
						{
						if (int(IntArray->GetTuple(i)[3]) == 1)
							InFlowNode = int(IntArray->GetTuple(i)[1]);

						if (int(IntArray->GetTuple(i)[3]) == -1)
							OutFlowNode = int(IntArray->GetTuple(i)[1]);
						}
					
					if (ElemOrder >= InFlowNode &&  ElemOrder < OutFlowNode )
						{
						//cout << "Segmento ID " <<  TreeElementID << " NN dentro 3D  " << ElemOrder << endl; 
						return true;
						}
//					else	
//						{
//						cout << "Segmento ID " <<  TreeElementID << " NN fora 3D " << ElemOrder << endl; 
//						return false;	
//						}
					}
				}
			else // demais casos: com mais de 2 tampas ou 2 tampas com acoplamento em segmentos diferentes
				{
				for (int i = 0; i < IntArray->GetNumberOfTuples(); ++i)
					{
					if (TreeElementID == int(IntArray->GetTuple(i)[0]) ) // compara se segmento é de acoplamento
						{
						// se segmento é entrada e NN em teste é maior que nó de acoplamento
						if (int(IntArray->GetTuple(i)[3]) == 1 && ElemOrder >= int(IntArray->GetTuple(i)[1]) )
							{
							//cout << "Segmento entrada ID " <<  TreeElementID << " NN dentro 3D " << ElemOrder << endl; 
							return true;	
							}
						// se segmento é Saida e NN em teste é menor que nó de acoplamento
						if (int(IntArray->GetTuple(i)[3]) == -1 && ElemOrder < int(IntArray->GetTuple(i)[1]))
							{
							//cout << "Segmento saida ID " <<  TreeElementID << " NN dentro 3D " << ElemOrder << endl;
							return true;	
							}
						// retornar false para elementos que estejam fora do 3D, apesar de estar no mesmo segmento de acoplamento
					
						//cout << "Segmento entrada ou saida ID " <<  TreeElementID << " NN fora 3D " << ElemOrder << endl;
						return false;			
						}
					}	
				}
			}
		} // final For
	
	
	// se o ID estiver na lista de IDS internos ao 3D e ID não for de acoplamento
	if ( this->Inner1DElements->IsId(TreeElementID) != -1 && (!this->VerifyCoupledSegment(TreeElementID) ) )
		{
		//cout << "Segmento dentro 3D " <<  TreeElementID  << endl;
		return true;
		}
	else
		{
		return false;
		}

}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::CreateCouplingInfoIndex()
{
	vtkIntArray *IntArray;
	it = this->FullModelList.begin();
	
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		if (this->GetNumberOfDifferentRegions(i) > 1) // SE FullModel tem mais de 1 estrutura 3D - exemplo: pericalosa (2 3ds em um FullModel)
			{
			for (int z = 0; z < this->GetNumberOfDifferentRegions(i); ++z)
				{
				IntArray = this->GetCouplingInfoFromSelectedRegion(z, i);
				
				for (int j = 0; j < IntArray->GetNumberOfTuples(); ++j)
					this->TerminalFlowDirection->InsertNextId(int(IntArray->GetTuple4(j)[3]));
				}
			}
		else // para FullModels com somente 1 "Regiao Diferente" --> 1 3D em por FullModel
			{	
			IntArray = this->GetCouplingInfoFrom3D(i); // coleta informacao de acoplamento de um determinado 3D
				
			for (int j = 0; j < IntArray->GetNumberOfTuples(); ++j)
				this->TerminalFlowDirection->InsertNextId(int(IntArray->GetTuple4(j)[3]));

		 this->StraightModelVisited->Reset();
		 this->TempCouplingInfo = IntArray;
		 this->CreateInnerElementsList( this->GetIdFromInFlowSegment(IntArray)); 
		 this->StraightModelVisited->Reset();
			}
		}

}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetParameters(int id, int *position)
{
	vtkDebugMacro(<<"SetParameters for data of the param file");
	
 	int pos = *position;
 	vtkHM1DTreeElement *elem;
 	vtkHM1DTreeElement *child;
 	
 	vtkHM1DTerminal *term;
 	vtkHM1DSegment *segm;
 	
 	if ( this->StraightModelVisited->IsId(id) != -1 )
		return;
	
	this->StraightModelVisited->InsertNextId(id);
 	
 	if (this->StraightModel->IsSegment(id))
 		{
 		elem = segm = this->StraightModel->GetSegment(id);
 		child = elem->GetFirstChild();
 		this->SetSegmentData(segm, position);
		//cout << "Position " << position[0] << endl;
 
 		
 		}
 	else if (this->StraightModel->IsTerminal(id))
 		{
 		elem = term = this->StraightModel->GetTerminal(id);
 		child = term->GetFirstChild();
 		this->SetTerminalData(term, position);
 		//cout << "Position " << position[0] << endl;
 		}
 	
 	if (!child)
	 	{
 		// É uma folha
 		return;
 		}
 	else
 		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
	 		{
	 		id = child->GetId();
	 		child = elem->GetNextChild();
	 		this->SetParameters(id, position);
	 		}
}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::SetTerminalData(vtkHM1DTerminal *term, int *position)
{
	vtkDebugMacro(<<"SetTerminalData for data of the param file");
	
	int numberOfParameters;
	int pos = *position;
	
	vtkHM1DParam::ListOfDouble lst;
	vtkHM1DParam::ListOfDouble::iterator it;
	
	vtkHM1DParam::ListOfInt listInt;
	vtkHM1DParam::ListOfInt::iterator itInt;
	
	//Declarando listas de dados e tempo do terminal
	vtkHM1DTerminal::ResistanceValuesList *r1;
	vtkHM1DTerminal::ResistanceValuesList::iterator itR;
	vtkHM1DTerminal::ResistanceTimeList *r1Time;
	vtkHM1DTerminal::ResistanceTimeList::iterator itRTime;
	vtkHM1DTerminal::ResistanceValuesList *r2;
	vtkHM1DTerminal::ResistanceTimeList *r2Time;
	vtkHM1DTerminal::CapacitanceValuesList *c;
	vtkHM1DTerminal::CapacitanceValuesList::iterator itC;
	vtkHM1DTerminal::CapacitanceTimeList *cTime;
	vtkHM1DTerminal::CapacitanceTimeList::iterator itCTime;
	vtkHM1DTerminal::PressureValuesList *p;
	vtkHM1DTerminal::PressureValuesList::iterator itP;
	vtkHM1DTerminal::PressureTimeList *pTime;
	vtkHM1DTerminal::PressureTimeList::iterator itPTime;
	
	//Obtendo listas de dados e tempo do terminal
	r1 = term->GetR1();
	r1Time = term->GetR1Time();
	r2 = term->GetR2();
	r2Time = term->GetR2Time();
	c = term->GetC();
	cTime = term->GetCTime();
	p = term->GetPt();
	pTime = term->GetPtTime();
	
	//Determinando a quantidade de parametros do terminal
	numberOfParameters = r1->size()+r2->size()+c->size()+p->size();
	
	vtkHM1DSegment *segm;
	
	//Caso a quantidade de parametros seja maior que quatro,
	//o terminal possui curva
	if ( numberOfParameters > 4 )
		{
		//Caso de um terminal comum que possui pai
		if ( term->GetFirstParent() )
			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e quantidade
			//de parametros de cada lista (r1, r2, c, p)
//			segm  = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
			//this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			
			
			if (r1->size() == 1) // se terminal possui valor fixo para R1
				this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			else // se tiver valor transiente, entao assumo que os demais parametros tambem sao transientes
				this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+ 4 ;
			
			
			for ( int i=0; i<term->GetNumberOfParents(); i++ )
				listInt.push_back(1);
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			listInt.push_back(r1->size());
			listInt.push_back(r2->size());
			listInt.push_back(c->size());
			listInt.push_back(p->size());
			}
		// Caso do coracao que nao possui pai
		else
			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e quantidade
			//de parametros de cada lista (r1, r2, c, p)
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			listInt.push_back(r1->size());
			listInt.push_back(r2->size());
			listInt.push_back(c->size());
			listInt.push_back(p->size());
			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+4;
			}
		
		//Adicionando a lista os 4 primeiros dados zerados
		//que sao descartados
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		lst.push_back(0.0);
		
		//Adicionando os dados e tempo da lista de resistencia 1
		itRTime = r1Time->begin();
		for ( itR = r1->begin(); itR!=r1->end(); itR++ )
			{
			lst.push_back(*itRTime);
			itRTime++;
			lst.push_back(*itR);
			}
		
		//Adicionando os dados e tempo da lista de resistencia 2
		itRTime = r2Time->begin();
		for ( itR = r2->begin(); itR!=r2->end(); itR++ )
			{
			lst.push_back(*itRTime);
			itRTime++;
			lst.push_back(*itR);
			}
		
		//Adicionando os dados e tempo da lista de capacitancia
		itCTime = cTime->begin();
		for ( itC = c->begin(); itC!=c->end(); itC++ )
			{
			lst.push_back(*itCTime);
			itCTime++;
			lst.push_back(*itC);
			}
		
		//Adicionando os dados e tempo da lista de pressao
		itPTime = pTime->begin();
		for ( itP = p->begin(); itP!=p->end(); itP++ )
			{
			lst.push_back(*itPTime);
			itPTime++;
			lst.push_back(*itP);
			}
		
		//Conta realizada para contabilizar o total de parametros
		//que inclui tempo+dados+ os 4 valores descartados inseridos
		//no inicio da lista
		numberOfParameters = 2 * numberOfParameters + 4;
		}
	else
		{
//		if ( term->GetFirstParent() )
//			{
			//Definindo a lista de parametros inteiros que sao:
			//segmento de entrada (1), de saida (-1) e no final 0
			//pois, o terminal nao possui curva
//			segm  = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+term->GetNumberOfParents()+1;
			for ( int i=0; i<term->GetNumberOfParents(); i++ )
				listInt.push_back(1);
			for ( int i=0; i<term->GetChildCount(); i++ )
				listInt.push_back(-1);
			listInt.push_back(0);
//			}
//		//Caso do coracao que nao possui pai
//		else
//			{
//			//Definindo a lista de parametros inteiros que sao:
//			//segmento de entrada (1), de saida (-1) e no final 0
//			//pois, o terminal nao possui curva
//			for ( int i=0; i<term->GetChildCount(); i++ )
//				listInt.push_back(-1);
//			listInt.push_back(0);
//			this->QuantityOfIntegerParameters[pos] = term->GetChildCount()+1;
//			}
		
		//Adicionando dados da lista de resistencia 1
		for ( itR = r1->begin(); itR!=r1->end(); itR++ )
			lst.push_back(*itR);
		
		//Adicionando dados da lista de resistencia 2
		for ( itR = r2->begin(); itR!=r2->end(); itR++ )
			lst.push_back(*itR);
		
		//Adicionando dados da lista de capacitancia
		for ( itC = c->begin(); itC!=c->end(); itC++ )
			lst.push_back(*itC);
		
		//Adicionando dados da lista de pressao
		for ( itP = p->begin(); itP!=p->end(); itP++ )
			lst.push_back(*itP);
		}
	
	//Setando quantidade de parametros do terminal
	this->QuantityOfRealParameters[pos] = numberOfParameters;
	//Adicionando ao vector double a lista de parametros reais
	this->RealParameters.push_back(lst);
	//Adicionando ao vector int a lista de parametros inteiros
	this->IntegerParameters.push_back(listInt);
	pos++;
	*position = pos;
	
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetSegmentData(vtkHM1DSegment *segm, int *position)
{
	vtkDebugMacro(<<"SetSegmentData for data of the param file");
	
	int pos = *position;
	
	vtkHMElementData *elem;
	vtkHMNodeData *node1;
	vtkHMNodeData *node2;
	
	list<double> lst;
	list<double>::iterator it;
	
	//double *point;
	
	//Pegando dados do segmento para adicionar a lista de parametros reais
	for ( int i=0; i<segm->GetElementNumber(); i++ )
		{
		//Pegando elemento, node esquerdo e node direito
		elem = segm->GetElementData(i);
		node1 = segm->GetNodeData(i);
		node2 = segm->GetNodeData(i+1);
		
		//Adicionando na lista dados dos nodes
		lst.push_back(node1->area);
		lst.push_back(node2->area);
		lst.push_back(node1->alfa);
		lst.push_back(node2->alfa);
		
		//Adicionando na lista dados do elementog
		lst.push_back(elem->Elastin);
		lst.push_back(elem->Collagen);
		lst.push_back(elem->CoefficientA);
		lst.push_back(elem->CoefficientB);
		lst.push_back(elem->Viscoelasticity);
		lst.push_back(elem->ViscoelasticityExponent);
		lst.push_back(elem->ReferencePressure);
		lst.push_back(elem->InfiltrationPressure);
		lst.push_back(elem->Permeability);
		
		//Setando quantidade de parametros reais e inteiros
		this->QuantityOfRealParameters[pos] = 13;
		this->QuantityOfIntegerParameters[pos] = 0;
		pos++;
		
		//Adicionando ao vector double a lista de parametros do elemento
		this->RealParameters.push_back(lst);
		lst.clear();
		}
	
	*position = pos;
}

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::BuildElementMat()
{
	/*
	* OBS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	*
	* quando os pontos de acoplamento estao mal definidos o vetor ElementMat fica com lixo, geralmente nso ultimos 
	* elementos.
	*
	*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	*/
	
	int NumberOfElements = this->ElementsIncidence.size();
	
	// cria o vetores
	int *ElementMat = new int[NumberOfElements];

	// vetor auxiliar 
	int *ElementTypeOriginal = new int[NumberOfElements];
	
	int LastValue;

	// pega a lista element type original
	// sem valores negativos
	for (int i = 0; i < NumberOfElements; ++i)
		{
		if (this->GetElementType()[i] < 0)
			ElementTypeOriginal[i] = (-1*this->GetElementType()[i]); 
		else
			ElementTypeOriginal[i] = this->GetElementType()[i];
		}
	
	//int NumberOf1DElements = this->NumberOf1DElements;
	vtkIntArray *temp = vtkIntArray::New();
	
	// *****************************************
	//	0 - Least Square / 1= ViscoElatsic (1D)
	if (this->IntParamArray[0]) // 1D ViscoElastico - 4 Elementos
		{
		// !!!!!!!!!!!!!!!
		// Construcao da lista Element Mat somenente dos elementos 1D
		// !!!!!!!!!!!!!!! 
		
		int i = 0;
		int LastValue1D;
		
		// quando 1D Least Square Viscolelastic temos 4 elementos  
		// 1 e 3 correspondem a mesma linha de parametros no Param.txt
		// 1 e 4 correspondem a mesma linha de parametros no Param.txt
		 
		while (i < NumberOf1DElements)
		 {
		 if (ElementTypeOriginal[i] == 2) // se elemento é Terminal
		   {		
		 	 if (!i) // para o coracao
		 		 {
		 		 temp->InsertNextValue(i+1);
		 		 LastValue1D = i + 1;
		 		 }
		 	 else //demais terminais
		 		 {
		 		 temp->InsertNextValue(LastValue1D + 1);
		 		 LastValue1D = LastValue1D + 1;
		 		 }
		 	 }
		 if (ElementTypeOriginal[i] == 1)  // elemento "normal"
		 	 {
		 	 temp->InsertNextValue(LastValue1D + 1);
			 LastValue1D = LastValue1D + 1;
			 }
		 if (ElementTypeOriginal[i] == 3) // elemento in
			 {
		 	 temp->InsertNextValue(LastValue1D);
			 //LastValue1D = LastValue1D;
			 }
		 if (ElementTypeOriginal[i] == 4) // elemento out
		 	 {
		 	 temp->InsertNextValue(LastValue1D);
		 	 //LastValue1D = LastValue1D;
		 	 }
		 i++;	
		 }
		
		//Setar os valores originais de element mat do 1D.
		//Estes dados são salvos para posterior leitura dos dados 1D.
		this->SetElementMat1D(temp);
		
		// incrementa para que o 1o elemento do bloco de elementos 3d nao repita o valor do ultimo elemento do 1D 
		LastValue1D++;
		
		// elementos de volume de todos FullModels envolvidos
		for (int i = 0; i < this->GetTotalNumberOfVolumeElements() ; ++i)
			temp->InsertNextValue(LastValue1D + i);
			
		LastValue = LastValue1D + this->GetTotalNumberOfVolumeElements();
		}
	else
		{	
		// quando 1D Least Square temos 2 elementos e cada elemento do 1D corresposnde a uma linha de parametros no Param.txt
		
		//Adiciona primeiro os elementos 1D ao array
		for (int i = 0; i < NumberOf1DElements; ++i)
			temp->InsertNextValue(i+1);
		
		//Setar os valores originais de element mat do 1D.
		//Estes dados são salvos para posterior leitura dos dados 1D.
		this->SetElementMat1D(temp);
		
		//Adiciona os elementos do volume
		for (int i = NumberOf1DElements; i < this->GetTotalNumberOfVolumeElements() ; ++i)
			temp->InsertNextValue(i+1);
		
		LastValue = NumberOf1DElements + this->GetTotalNumberOfVolumeElements() +1; //Adciona 1 para que os elementos de parede nao comecem com o ultimo valor dos Elementos Bubble
		}

	
// 	//elemento de parede
//	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
//		{
//		for (int z = 0; z < this->GetNumberOfTriangElements(i, 0); ++z)
//			{
//			temp->InsertNextValue(LastValue);
//			}
//		LastValue++;
//		}
	
	// Elementos de Parede
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		for (int y = 0; y < this->GetFullModel(i)->GetNumberOfShells(); ++y)	
			{
			for (int z = 0; z < this->GetNumberOfTriangElements(i, y); ++z)	
				{
				temp->InsertNextValue(LastValue);	
				}
			LastValue++;
			
			}
		}
	
	
 // elementos de acoplamento 54
 for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		for (int y = 0; y < this->GetFullModel(i)->GetNumberOfCoverGroups(); ++y)
			{
			for (int z = 0; z < this->GetNumberOfTriangElements(i, y + this->GetFullModel(i)->GetNumberOfShells()); ++z)
				{
				temp->InsertNextValue(LastValue);
				}
			LastValue++;
			}
		}
	
  // elementos de acoplamento 23
	for (int i = 0; i < this->GetTotalNumberOfCovers(); ++i)
		{
		temp->InsertNextValue(LastValue);
		LastValue++;
		}


	// resize no tamanho do vetor
	temp->Resize(NumberOfElements);

	for (int i = 0; i < NumberOfElements; ++i)
		{
		ElementMat[i] = temp->GetValue(i);
		}


	//	 percorre a lista element type com valores negativos e 
	//	 atualiza na lista element mat os valores dos elementos nulos 
	for (int i = 0; i < NumberOfElements; ++i)
		{
		if (this->GetElementType()[i] < 0)
			ElementMat[i] = this->TotalNumberOfElements;
		}

	 this->SetElementMat(ElementMat);
	 temp->Delete();
	 
	// desalocando os vetores	 
//	delete [] ElementMat;
	delete [] ElementTypeOriginal; 
	 
	 
}


//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::InsertNullElements()
{
	int NumberOfElements = this->ElementsIncidence.size();
	
	// pega a lista element type original
	// sem valores negativos
	for (int i = 0; i < NumberOfElements; ++i)
		{
		if (this->GetElementType()[i] < 0)
			this->GetElementType()[i] = this->GetQuantityOfDifferentElements(); 
		}

}

//-------------------------------------------------------------------------------------
int vtkHMCoupledModelWriterPreProcessor::GetNumberOf3DFullModels()
{
	return this->FullModelList.size();
}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::Insert3DFullModel(vtkHM3DFullModel *model)
{
	this->FullModelList.push_back(model);
}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::SetFullModelNumber(int Number)
{
	this->FullModelNumber = Number;
}

//-------------------------------------------------------------------------------------

vtkIntArray *vtkHMCoupledModelWriterPreProcessor::GetCouplingInfoFrom3D(int FullModelNumber)
{
	if (FullModelNumber > (this->GetNumberOf3DFullModels()-1))
		vtkErrorMacro("There is something wrong. I dont have the Coupling information for the FullModel " << FullModelNumber);


	if (!this->SelectedCouplingInfo)
		{
		this->SelectedCouplingInfo = vtkIntArray::New();
		this->SelectedCouplingInfo->SetNumberOfComponents(4);
		}

	
	this->SelectedCouplingInfo->SetNumberOfTuples(this->GetNumberOfCovers(FullModelNumber));  
	
	int ArrayPosition = 0;
	
	for (int i = 0; i < this->CouplingArrayInfo->GetNumberOfTuples(); ++i)
		{
		if (this->CouplingArrayInfo->GetTuple4(i)[3] == FullModelNumber)
			{
			this->SelectedCouplingInfo->InsertTuple(ArrayPosition, this->CouplingArrayInfo->GetTuple4(i));
			ArrayPosition++;
			}
		}
	
	
	// A seguir codigo que percorre o vetor e coleciona todos valores de segmento ID e Node Order em duas listas especificas
	std::list<int> ListOfSegmentIds;
	std::list<int> ListOfNodeOrder;
	
	for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
		{
		// constroi a lista de segment Ids
		ListOfSegmentIds.push_back(int(this->SelectedCouplingInfo->GetTuple4(i)[0]) );
		// lista de node orders
		ListOfNodeOrder.push_back(int(this->SelectedCouplingInfo->GetTuple4(i)[1]) );	
		}
	// sorts the list into ascending order; that way I can have the lower value
	ListOfSegmentIds.sort();
	ListOfNodeOrder.sort();
	
	
	// agora tenho que saber quem é o menor segmento ID para marcar este como entrada de fluxo
	// se o 3D tem 2 faces e estes estao no mesmo segmento tenho que entao comparar que tem o menor Node Number
	
	if (this->SelectedCouplingInfo->GetNumberOfTuples() == 2 && (ListOfSegmentIds.front() == ListOfSegmentIds.back()) )
		{
		// se o 3D esta em um mesmo segmento entao testa-se para saber qual tem o menor node number
		for (int z = 0; z < this->SelectedCouplingInfo->GetNumberOfTuples(); ++z)
			{
			double *Tuple4 = this->SelectedCouplingInfo->GetTuple4(z);
				
			if (this->SelectedCouplingInfo->GetTuple4(z)[1] > ListOfNodeOrder.front())
				{
				Tuple4[3] = -1.0; // zero segmento de saida
				this->SelectedCouplingInfo->InsertTuple(z, Tuple4);
				}
			else
				{
				Tuple4[3] = 1.0; // segmento de entrada de fluxo
				this->SelectedCouplingInfo->InsertTuple(z, Tuple4);
				}
			}	
			
		}
	else // para um 3D com 2 tampas em diferentes segmentos ou com 3D com mais de 2 tampas
		{
		for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
			{
			double *Tuple4 = this->SelectedCouplingInfo->GetTuple4(i);
			
			
			if (this->SelectedCouplingInfo->GetTuple4(i)[0] > ListOfSegmentIds.front())
				{
				Tuple4[3] = -1.0;
				this->SelectedCouplingInfo->InsertTuple(i, Tuple4);
				}
			else
				{
				Tuple4[3] = 1.0;
				this->SelectedCouplingInfo->InsertTuple(i, Tuple4);
				}
			}
		}
	
//	for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
//		cout << " pos 0:  " << this->SelectedCouplingInfo->GetTuple4(i)[0] << " pos 1:  " << this->SelectedCouplingInfo->GetTuple4(i)[1] << " pos 2:  " << this->SelectedCouplingInfo->GetTuple4(i)[2] << " pos 3:  " << this->SelectedCouplingInfo->GetTuple4(i)[3] << endl;
	
	
	return this->SelectedCouplingInfo;
	
}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNumberOfVolumeElements(int FullModelNumber)
{	
	it = this->FullModelList.begin();
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);	
		if (i == FullModelNumber)
			return this->CurrentFullModel->GetVolume()->GetNumberOfCells();
		it++;
		}

}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetTotalNumberOfVolumeElements()
{	
	it = this->FullModelList.begin();
	int VolumeElements = 0;
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);	
		VolumeElements = VolumeElements +	this->CurrentFullModel->GetVolume()->GetNumberOfCells();
		it++;
		}
	return VolumeElements;
}



//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNumberOfCovers(int FullModelNumber)
{	
	it = this->FullModelList.begin();
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		
		this->CurrentFullModel = (*it);
		
		if (i == FullModelNumber)
			return this->CurrentFullModel->GetNumberOfCoverGroups();
		it++;
		}
	// em caso de nao achar o FullModel especificado
	return 0;
}
//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetTotalNumberOfCovers()
{	
	it = this->FullModelList.begin();
	int TotalCovers = 0;
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		this->CurrentFullModel = (*it);	
		TotalCovers = TotalCovers +	this->CurrentFullModel->GetNumberOfCoverGroups();
		it++;
		}
	return TotalCovers;
}

//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetVolumeElementPosition(int FullModelNumber)
{
	int Found = 0;
	for (int i = 0; i < this->BasParamElementList->GetNumberOfTuples(); ++i)
		{
		// elementos de volume
		if (this->BasParamElementList->GetValue(i) == 524 || this->BasParamElementList->GetValue(i) == 5245)
			{
			if (Found == FullModelNumber)
				return i+1;
			
			Found++;
			}
		}
	// if I did not find something
	return 0;
}

//-------------------------------------------------------------------------------------
int vtkHMCoupledModelWriterPreProcessor::GetNumberOfTriangElements(int FullModelNumber, int Group)
{
	vtkSurfaceGen* surfGen = this->GetFullModel(FullModelNumber)->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
	return  surfGen->GetNumElsGroups(Group);	
 	
}

//-------------------------------------------------------------------------------------

vtkHM3DFullModel *vtkHMCoupledModelWriterPreProcessor::GetFullModel(int FullModelNumber)
{	
	it = this->FullModelList.begin();
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		if (FullModelNumber == i)
			return (*it);
		
		it++;
		}
	return NULL;
}

//-------------------------------------------------------------------------------------


int vtkHMCoupledModelWriterPreProcessor::GetElementTypeFromCode(int ElementCode)
{
	if (ElementCode == 524)
		return VOLUME;

	if (ElementCode == 525)
		return VOLUME;

	if (ElementCode == 5245)
		return VOLUME;

	if (ElementCode == 5705)
		return SURFACE;

	if (ElementCode == 57)
		return SURFACE;

	if (ElementCode == 54)
		return COUPLING_ELEMENT;

	if (ElementCode == 541)
		return COUPLING_ELEMENT;

	if (ElementCode == 542)
		return COUPLING_ELEMENT;

	if (ElementCode == 23)
		return COUPLING_TERMINAL;

	if (ElementCode == 99)
		return NULL_ELEMENT;

}


//-------------------------------------------------------------------------------------

bool vtkHMCoupledModelWriterPreProcessor::CheckIfCouplingPointIsProximal(int FullModelNumber, int CouplingPointOrder)
{
	
	// pega informacao de pontos de acoplamento referente ao FullModel dado por FullModelNumber
	vtkIntArray *LocalInfoArray = this->GetCouplingInfoFrom3D(FullModelNumber);
	
	if ( int(LocalInfoArray->GetTuple4(CouplingPointOrder)[3]) == 1 )
		return true;
	else
		return false;	
}

//-------------------------------------------------------------------------------------

double vtkHMCoupledModelWriterPreProcessor::GetCoverArea(int Group, int FullModelNumber)
{
	double CoverArea = 0.0;
	
	vtkSurfaceGen* surfGen = this->GetFullModel(FullModelNumber)->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
	
	
	SurfTriang* first = surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
	SurfTriang* curr = surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
	curr = first;  
		while (curr)
  		{
  		if (curr->gr == Group)
    		{		      
				CoverArea = CoverArea + surfGen->GetTriangleArea(curr->n1, curr->n2, curr->n3);
    		}
        
			curr = curr->next; // passa para o proximo triangulo
  		}
	
	// se o fator de escala é utilizado tem-se que obter a nova area
	// que é dada pela formula NewArea = OldArea * fator^2 
	return (CoverArea * (this->ScaleFactor3D * this->ScaleFactor3D));
	
}

//-------------------------------------------------------------------------------------

double vtkHMCoupledModelWriterPreProcessor::GetCoverRadius(int Group, int FullModelNumber)
{
	//calculo do raio
	return sqrt( (this->GetCoverArea(Group, FullModelNumber)/vtkMath::Pi()) );
}


//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::CompatibilizeCouplingPoints(int Mode)
{
	// Mode 1 - Constant
	// Mode 0 - Linear
		
	// percorre todos FullModels do modelo
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		if (this->GetNumberOfDifferentRegions(i) > 1)
			{
			for (int z = 0; z < this->GetNumberOfDifferentRegions(i); ++z)
				{
				vtkIntArray *LocalInfoArray = this->GetCouplingInfoFromSelectedRegion(z, i);
				this->CompatibilizeCouplingPointsInternal(LocalInfoArray, i, Mode);
				}
			}
		else
			{
			// pega informacao dos pontos de acoplamento de cada modelo 3D
			vtkIntArray *LocalInfoArray = this->GetCouplingInfoFrom3D(i);
			this->CompatibilizeCouplingPointsInternal(LocalInfoArray, i, Mode);
			}
		}
}		

//-------------------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::CompatibilizeCouplingPointsInternal(vtkIntArray *LocalInfoArray, int FullModelNumber, int Mode)		
{		
	int SegId,NodeNumber, Cover;	
	
	for (int z = 0; z < LocalInfoArray->GetNumberOfTuples(); ++z)
		{
		SegId = int(LocalInfoArray->GetTuple4(z)[0]);
		NodeNumber = int(LocalInfoArray->GetTuple4(z)[1]);
		Cover = int(LocalInfoArray->GetTuple4(z)[2]);
		
		vtkHM1DStraightModel::TreeNodeDataMap::iterator it;
		it = this->StraightModel->GetSegmentsMap().find(SegId);
		vtkHM1DSegment *s = vtkHM1DSegment::SafeDownCast(it->second);
		int SegmentNodeNumber = s->GetNodeNumber();
		
		
		if (int(LocalInfoArray->GetTuple4(z)[3]) == 1 )// se o segmento é de entrada
			{
			
			// Variaveis utilizadas quando compatibilizacao é constante
			double NewNodeRadiusConstant =  this->GetCoverRadius(Cover, FullModelNumber);
			double NewNodeAreaConstant =  this->GetCoverArea(Cover, FullModelNumber);
			
			double A, B; // valores dos coeficientes da equacao da reta y=Ax+B
			A =  ( NewNodeRadiusConstant - s->GetNodeData(0)->radius)  /  (this->Distance2BetweenPoints(s->GetNodeData(0)->coords,  s->GetNodeData(NodeNumber)->coords) );
			B =  s->GetNodeData(0)->radius;
			
			// **************************
			// Segmento ou ponto de entrada de Fluxo
			// **************************
			
			for (int y = 0; y < NodeNumber+1; ++y) // percorre do no 0 ate o nó em que o ponto de acoplamento esta localizado
			//for (int y = 0; y < SegmentNodeNumber; ++y) // percorre todos nós do segmento de entrada
				{
				
				if (Mode) // Compatibilizacao constante
					{
					s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusConstant/s->GetNodeData(y)->radius) * (NewNodeRadiusConstant/s->GetNodeData(y)->radius) );
					s->GetNodeData(y)->radius = NewNodeRadiusConstant;
					s->GetNodeData(y)->area = NewNodeAreaConstant;
					}
				else     // Compatibilizacao linear
					{
					if (y < NodeNumber+1) // do nó Zero até ao ponto de acoplamento
						{
						double NewNodeRadiusLinear = A* (this->Distance2BetweenPoints(s->GetNodeData(0)->coords,  s->GetNodeData(y)->coords)) + B;
						
						s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusLinear/s->GetNodeData(y)->radius) * (NewNodeRadiusLinear/s->GetNodeData(y)->radius) );
						s->GetNodeData(y)->radius = NewNodeRadiusLinear;	
						s->GetNodeData(y)->area = s->GetNodeData(y)->radius * s->GetNodeData(y)->radius * vtkMath::Pi();	
						}
					else // do ponto de acoplamento até o final do segmento
						{
						s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusConstant/s->GetNodeData(y)->radius) * (NewNodeRadiusConstant/s->GetNodeData(y)->radius) );
						s->GetNodeData(y)->radius = NewNodeRadiusConstant;
						s->GetNodeData(y)->area = NewNodeAreaConstant;	
						}	
					}
				
				}
			
			if (LocalInfoArray->GetNumberOfTuples() > 2) 
				{
				for (int y = NodeNumber+1; y < SegmentNodeNumber; ++y) // percorre do no do ponto de acoplamento até o finbal do segmento
					{
					// PARTE INTERNA SEMPRE CONSTANTE QUANDO MAIS DE 2 TAMPAS 
					//cout << "Seta Nó Entrada " << y << endl;
					s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusConstant/s->GetNodeData(y)->radius) * (NewNodeRadiusConstant/s->GetNodeData(y)->radius) );
					s->GetNodeData(y)->radius = NewNodeRadiusConstant;
					s->GetNodeData(y)->area = NewNodeAreaConstant;
					}
				}		
		
			}
		else // se segmento é saida seta nós entre o ponto de acoplamento até final do segmento
			{
			
			double NewNodeRadiusConstant =  this->GetCoverRadius(Cover, FullModelNumber);
			double NewNodeAreaConstant =  this->GetCoverArea(Cover, FullModelNumber);
			
			
			double A, B; // valores dos coeficientes da equacao da reta y=Ax+B
			// A = (Rf -Ri)/ Distancia (RF Ri)
			// Raio Final será dado pelo raio do ultimo no do segmento e * Raio inicial sera o raio da tampa de saida da estrutura 3D
			A =  (s->GetNodeData(SegmentNodeNumber -1)->radius - NewNodeRadiusConstant)  /  (this->Distance2BetweenPoints(s->GetNodeData(SegmentNodeNumber -1)->coords, s->GetNodeData(NodeNumber)->coords) );
			
			// B = Raio inicial
			B =  NewNodeRadiusConstant;
			
			
			
			for (int y = NodeNumber; y < SegmentNodeNumber; ++y)
				{
				if (Mode) // Compatibilizacao constante
					{
					s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusConstant/s->GetNodeData(y)->radius) * (NewNodeRadiusConstant/s->GetNodeData(y)->radius) );
					s->GetNodeData(y)->radius = NewNodeRadiusConstant;
					s->GetNodeData(y)->area = NewNodeAreaConstant;
					}
				else     // Compatibilizacao linear
					{
					double NewNodeRadiusLinear = A* (this->Distance2BetweenPoints(s->GetNodeData(NodeNumber)->coords,  s->GetNodeData(y)->coords)) + B;
					s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusLinear/s->GetNodeData(y)->radius) * (NewNodeRadiusLinear/s->GetNodeData(y)->radius) );
					s->GetNodeData(y)->radius = NewNodeRadiusLinear;	
					s->GetNodeData(y)->area = s->GetNodeData(y)->radius * s->GetNodeData(y)->radius * vtkMath::Pi();	
					}
				}	
			
			
			if (LocalInfoArray->GetNumberOfTuples() > 2) 
				{
				for (int y = 0; y < NodeNumber; ++y) // percorre do no 0 ATÉ ao ponto de acoplamento 
					{
					// PARTE INTERNA SEMPRE CONSTANTE QUANDO MAIS DE 2 TAMPAS 
					//cout << "Seta Nó Saida " << y << endl;
					s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusConstant/s->GetNodeData(y)->radius) * (NewNodeRadiusConstant/s->GetNodeData(y)->radius) );
					s->GetNodeData(y)->radius = NewNodeRadiusConstant;
					s->GetNodeData(y)->area = NewNodeAreaConstant;
					}
				}		
			} // final Else
		} //*******Final do For
	
	
	// *****************************************************************
	//
	// COMPATIBILIZACAO PARTE INTERNA AO 3D (ENTRE PONTOS DE ACOPLAMENTO)
	//
	// *****************************************************************
		
	// apos o raio das partes nao englobadas pela estrutura 3D já tiverem sido modificadas, aplicar a variacao linear nos pontos internos ao 3D
	// somente quando a estrutura tem 2 tampas e os pontos de acoplamento estao em um mesmo segmento	
	if ( (LocalInfoArray->GetNumberOfTuples() == 2) && this->CheckIfCouplingIsLocatedInSameSegment(LocalInfoArray) )
		{
		
		vtkHM1DStraightModel::TreeNodeDataMap::iterator it;
		it = this->StraightModel->GetSegmentsMap().find(int(LocalInfoArray->GetTuple4(0)[0]));
		vtkHM1DSegment *s = vtkHM1DSegment::SafeDownCast(it->second);// identificando segmento no qual estao localizados os pontos de acoplamento
		int SegmentNodeNumber = s->GetNodeNumber(); // numero de nodes do segmento
		
		double NewNodeRadiusConstantProximal,NewNodeRadiusConstantDistal; //variaveis para armazenar valor dos raios dos pontos de acoplamento
		int InNode, OutNode; // no de entrada e saida de fluxo, respectivamente		
		
		for (int i = 0; i < LocalInfoArray->GetNumberOfTuples(); ++i)
			{
			if (int(LocalInfoArray->GetTuple4(i)[3]) == 1) // Localiza ponto proximal 					
				{
				NewNodeRadiusConstantProximal =  this->GetCoverRadius(int(LocalInfoArray->GetTuple4(i)[2]), FullModelNumber);
				InNode = int(LocalInfoArray->GetTuple4(i)[1]);	
				}
			else // se é distal
				{
				NewNodeRadiusConstantDistal =  this->GetCoverRadius(int(LocalInfoArray->GetTuple4(i)[2]), FullModelNumber);	
				OutNode = int(LocalInfoArray->GetTuple4(i)[1]);	
				}	
			}
		
//		cout << "Valor Raio Proximal " << NewNodeRadiusConstantProximal << endl;
//		cout << "Valor Raio Distal " << NewNodeRadiusConstantDistal << endl;
		
				
		double A, B; // valores dos coeficientes da equacao da reta y=Ax+B
		A =  ( NewNodeRadiusConstantDistal - NewNodeRadiusConstantProximal)  /  (this->Distance2BetweenPoints(s->GetNodeData(InNode)->coords,  s->GetNodeData(OutNode)->coords) );
		B =  NewNodeRadiusConstantProximal;
				
		for (int y = InNode+1; y < OutNode; ++y) 
			{
			//cout << "Seta Nó " << y << endl;
			double NewNodeRadiusLinear = A* (this->Distance2BetweenPoints(s->GetNodeData(InNode)->coords,  s->GetNodeData(y)->coords)) + B;
			s->GetNodeData(y)->alfa = s->GetNodeData(y)->alfa * ( (NewNodeRadiusLinear/s->GetNodeData(y)->radius) * (NewNodeRadiusLinear/s->GetNodeData(y)->radius) );
			s->GetNodeData(y)->radius = NewNodeRadiusLinear;	
			s->GetNodeData(y)->area = s->GetNodeData(y)->radius * s->GetNodeData(y)->radius * vtkMath::Pi();			
			}
		}
	else if ( (LocalInfoArray->GetNumberOfTuples() == 2) && !this->CheckIfCouplingIsLocatedInSameSegment(LocalInfoArray) )  // ************ Para estruturas 3D com 2 tampas em segmentos Diferentes ***************
		{
		// a equacao da reta é a mesma se as 2 tampas estivessem em um mesmo segmento	
		
		double NewNodeRadiusConstantProximal,NewNodeRadiusConstantDistal; //variaveis para armazenar valor dos raios dos pontos de acoplamento
		int InNode, OutNode; // no de entrada e saida de fluxo, respectivamente		
		int InSeg, OutSeg; // id do segmento de entrada / saida de fluxo
		
		
		for (int i = 0; i < LocalInfoArray->GetNumberOfTuples(); ++i)
			{
			if (int(LocalInfoArray->GetTuple4(i)[3]) == 1) // Localiza ponto proximal 					
				{
				NewNodeRadiusConstantProximal =  this->GetCoverRadius(int(LocalInfoArray->GetTuple4(i)[2]), FullModelNumber);
				InNode = int(LocalInfoArray->GetTuple4(i)[1]);
				InSeg = int(LocalInfoArray->GetTuple4(i)[0]);	
				}
			else // se é distal
				{
				NewNodeRadiusConstantDistal =  this->GetCoverRadius(int(LocalInfoArray->GetTuple4(i)[2]), FullModelNumber);	
				OutNode = int(LocalInfoArray->GetTuple4(i)[1]);
				OutSeg 	= int(LocalInfoArray->GetTuple4(i)[0]);
				}	
			}
		
		vtkHM1DStraightModel::TreeNodeDataMap::iterator it;
		it = this->StraightModel->GetSegmentsMap().find(InSeg);
		vtkHM1DSegment *s1 = vtkHM1DSegment::SafeDownCast(it->second);// identificando segmento no qual estao localizados os pontos de acoplamento
		int SegmentNodeNumber = s1->GetNodeNumber(); // numero de nodes do segmento
		
		
		it = this->StraightModel->GetSegmentsMap().find(OutSeg);
		vtkHM1DSegment *s2 = vtkHM1DSegment::SafeDownCast(it->second);// identificando segmento no qual estao localizados os pontos de acoplamento
		//int SegmentNodeNumber = s2->GetNodeNumber(); // numero de nodes do segmento
		
			
			
		double A, B; // valores dos coeficientes da equacao da reta y=Ax+B
		A =  ( NewNodeRadiusConstantDistal - NewNodeRadiusConstantProximal)  /  (this->Distance2BetweenPoints(s1->GetNodeData(InNode)->coords,  s2->GetNodeData(OutNode)->coords) );
		B =  NewNodeRadiusConstantProximal;
		
		// de no de entrada até fim de segmento		
		for (int y = InNode+1; y < SegmentNodeNumber; ++y) 
			{
			//cout << "Seta Nó " << y << endl;
			double NewNodeRadiusLinear = A* (this->Distance2BetweenPoints(s1->GetNodeData(InNode)->coords,  s1->GetNodeData(y)->coords)) + B;
			s1->GetNodeData(y)->alfa = s1->GetNodeData(y)->alfa * ( (NewNodeRadiusLinear/s1->GetNodeData(y)->radius) * (NewNodeRadiusLinear/s1->GetNodeData(y)->radius) );
			s1->GetNodeData(y)->radius = NewNodeRadiusLinear;	
			s1->GetNodeData(y)->area = s1->GetNodeData(y)->radius * s1->GetNodeData(y)->radius * vtkMath::Pi();			
			}
		
		//no no zero até o no de saida do segmento
		for (int y = 0; y < OutNode; ++y) 
			{
			//cout << "Seta Nó " << y << endl;
			double NewNodeRadiusLinear = A* (this->Distance2BetweenPoints(s1->GetNodeData(InNode)->coords,  s2->GetNodeData(y)->coords)) + B;
			s2->GetNodeData(y)->alfa = s2->GetNodeData(y)->alfa * ( (NewNodeRadiusLinear/s2->GetNodeData(y)->radius) * (NewNodeRadiusLinear/s2->GetNodeData(y)->radius) );
			s2->GetNodeData(y)->radius = NewNodeRadiusLinear;	
			s2->GetNodeData(y)->area = s2->GetNodeData(y)->radius * s2->GetNodeData(y)->radius * vtkMath::Pi();			
			}	
		}

}

//-------------------------------------------------------------------------------------
int vtkHMCoupledModelWriterPreProcessor::TestCouplingPointsAccuracy()
{
	vtkIdList *IdList = vtkIdList::New();
	IdList->Reset();
	int NodeOrder, SegId, Groups;
	
	
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		vtkIntArray *LocalInfoArray = this->GetCouplingInfoFrom3D(i);
		
		// primeiro testa se os grupos 3d sao iguais
		for (int z = 0; z < LocalInfoArray->GetNumberOfTuples(); ++z)
			{
			Groups = int(LocalInfoArray->GetTuple4(z)[2]);
			IdList->InsertUniqueId(Groups);
			}
					
		if (IdList->GetNumberOfIds() != LocalInfoArray->GetNumberOfTuples())
			{
			vtkErrorMacro("Coupling Points Bad Defined: Coupling points with the same 3D group");	
			return 0; //error
			}
		
		IdList->Reset();
		
		
		if (LocalInfoArray->GetNumberOfTuples() == 2)
			{
			// se ids sao o mesmo, entao estrutura 3d esta no mesmo segmento
			if ( int(LocalInfoArray->GetTuple4(0)[0]) == int(LocalInfoArray->GetTuple4(1)[0]) )
				{
				// testar se node orders sao iguais ou nao- se forem tem problemas nos pontos de acoplamento
				for (int z = 0; z < LocalInfoArray->GetNumberOfTuples(); ++z)
					{
					NodeOrder = int(LocalInfoArray->GetTuple4(z)[1]);
					IdList->InsertUniqueId(NodeOrder);
					}
					
				if (IdList->GetNumberOfIds() != 	LocalInfoArray->GetNumberOfTuples())
					{
					vtkErrorMacro("Coupling Points Bad Defined: Coupling points on the same 1D node");	
					return 0; //error
					}
				}
			}
		else
			{
			// para estruturas com mais de 2 tampas, todos ids devem ser diferentes
			
			for (int z = 0; z < LocalInfoArray->GetNumberOfTuples(); ++z)
				{
				SegId = int(LocalInfoArray->GetTuple4(z)[0]);
				IdList->InsertUniqueId(SegId);
				}
				
			if (IdList->GetNumberOfIds() != 	LocalInfoArray->GetNumberOfTuples())
				{
				vtkErrorMacro("Coupling Points Bad Defined: There are two or more coupling points in the same segment");
				return 0; //error
				}
			}
		IdList->Reset();
		}	
	IdList->Delete();	
	return 1; //points ok
}



//-------------------------------------------------------------------------------------
double vtkHMCoupledModelWriterPreProcessor::Distance2BetweenPoints(const double X[3], 
                                              const double Y[3])
{
  
  return sqrt( ((Y[0] -X[0])*(Y[0] -X[0]))  +  ((Y[1] -X[1])*(Y[1] -X[1])) );
  
          
}          
//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetNumberOfDifferentRegions(vtkHM3DFullModel *FullModel)
{
	int NumberOfRegions;
	vtkPVConnectivityFilter *filter = vtkPVConnectivityFilter::New();
	filter->SetInput(FullModel);
	filter->Update();
	NumberOfRegions = filter->GetNumberOfExtractedRegions();
	filter->Delete();
	return NumberOfRegions;
}


//-------------------------------------------------------------------------------------

vtkIntArray *vtkHMCoupledModelWriterPreProcessor::GetCouplingInfoFromSelectedRegion(int Region, int FullModelNumber)
{
	
//	if (FullModelNumber > (this->GetNumberOf3DFullModels()-1))
//		vtkErrorMacro("There is something wrong. I dont have the Coupling information for the FullModel " << FullModelNumber);

	cout << "Buscando Coupling Info from Region "<< Region << " no fullModel " << FullModelNumber << endl;


	if (!this->SelectedCouplingInfo)
		{
		this->SelectedCouplingInfo = vtkIntArray::New();
		this->SelectedCouplingInfo->SetNumberOfComponents(4);
		}

	
//	this->SelectedCouplingInfo->SetNumberOfTuples(this->GetNumberOfCovers(FullModelNumber));  
	
	this->SelectedCouplingInfo->Reset();
	int ArrayPosition = 0;
	
	
	
	
	for (int i = 0; i < this->CouplingArrayInfo->GetNumberOfTuples(); ++i)
		{
		
		if (int(this->CouplingArrayInfo->GetTuple4(i)[3]) == FullModelNumber)
			{
		
			if (this->CheckGroupFromRegion(int(this->CouplingArrayInfo->GetTuple4(i)[2]), Region, FullModelNumber))
				{
				this->SelectedCouplingInfo->InsertNextTuple(this->CouplingArrayInfo->GetTuple4(i));
				ArrayPosition++;				
				}
			}
		}
		
	this->SelectedCouplingInfo->SetNumberOfTuples(ArrayPosition);
	

//**************************************************************************
// Procedimento para identificar nos de entrada de fluxo


	// A seguir codigo que percorre o vetor e coleciona todos valores de segmento ID e Node Order em duas listas especificas
	std::list<int> ListOfSegmentIds;
	std::list<int> ListOfNodeOrder;
	
	for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
		{
		// constroi a lista de segment Ids
		ListOfSegmentIds.push_back(int(this->SelectedCouplingInfo->GetTuple4(i)[0]) );
		// lista de node orders
		ListOfNodeOrder.push_back(int(this->SelectedCouplingInfo->GetTuple4(i)[1]) );	
		}
	// sorts the list into ascending order; that way I can have the lower value
	ListOfSegmentIds.sort();
	ListOfNodeOrder.sort();
	
	
	// agora tenho que saber quem é o menor segmento ID para marcar este como entrada de fluxo
	// se o 3D tem 2 faces e estes esato no mesmo segmento tenho que entao comparar que tem o menor Node Number
	
	if (this->SelectedCouplingInfo->GetNumberOfTuples() == 2 && (ListOfSegmentIds.front() == ListOfSegmentIds.back()) )
		{
		// se o 3D esta em um mesmo segmento entao testa-se para saber qual tem o menor node number
		for (int z = 0; z < this->SelectedCouplingInfo->GetNumberOfTuples(); ++z)
			{
			double *Tuple4 = this->SelectedCouplingInfo->GetTuple4(z);
				
			if (this->SelectedCouplingInfo->GetTuple4(z)[1] > ListOfNodeOrder.front())
				{
				Tuple4[3] = -1.0; // zero segmento de saida
				this->SelectedCouplingInfo->InsertTuple(z, Tuple4);
				}
			else
				{
				Tuple4[3] = 1.0; // segmento de entrada de fluxo
				this->SelectedCouplingInfo->InsertTuple(z, Tuple4);
				}
			}	
			
		}
	else // para um 3D com 2 tampas em diferentes segmentos ou com 3D com mais de 2 tampas
		{
		for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
			{
			double *Tuple4 = this->SelectedCouplingInfo->GetTuple4(i);
			
			
			if (this->SelectedCouplingInfo->GetTuple4(i)[0] > ListOfSegmentIds.front())
				{
				Tuple4[3] = -1.0;
				this->SelectedCouplingInfo->InsertTuple(i, Tuple4);
				}
			else
				{
				Tuple4[3] = 1.0;
				this->SelectedCouplingInfo->InsertTuple(i, Tuple4);
				}
			}
		}
//*********************************************************************************************

	double *temp;  
	
	for (int i = 0; i < this->SelectedCouplingInfo->GetNumberOfTuples(); ++i)
		{
		temp = 	this->SelectedCouplingInfo->GetTuple(i);
		
		//cout << "Selected Region Coupling Info from Region "<< Region << " :: "  << temp[0] << " " << temp[1]<< " "<< temp[2]<< " "<< temp[3] << endl;
		}


	return this->SelectedCouplingInfo;

}
	

//-------------------------------------------------------------------------------------

bool	vtkHMCoupledModelWriterPreProcessor::CheckGroupFromRegion(int Group, int Region, int FullModelNumber)
{		
	//cout << "Testando se Grupo "<< Group << " esta na regiao " << Region << endl; 
	
	// este filtro retorna o numero distinto de regioes em um grid nao estruturado
	vtkPVConnectivityFilter *filter = vtkPVConnectivityFilter::New();
	
	filter->SetInput(this->GetFullModel(FullModelNumber));
	filter->Update();
	
	vtkIdList *CellPoints = vtkIdList::New();
	CellPoints->Reset();
	
	for (int i = 0; i < filter->GetOutput()->GetNumberOfCells(); ++i) 
		{
		// testa a primeira celula que for do grupo desejado
		if (Group == int(filter->GetOutput()->GetCellData()->GetScalars("Groups")->GetTuple1(i)) )
			{
			//cout << "Achei celula com grupo desejado" << endl;
			
			//testa somente o ponto 0 do triangulo - poderia testar os 2 outro pontos para aumentar confiabilidade, mas isto nao esta sendo feito :-)	
			filter->GetOutput()->GetCellPoints(i,CellPoints);
			
			// RegionId é uma valor associado a cada ponto indicando a estrutura do qual aquele ponto faz parte
			if ( int(filter->GetOutput()->GetPointData()->GetScalars("RegionId")->GetTuple1(CellPoints->GetId(0))) == Region )
				{
				//cout << "Sim!" << endl;
				filter->Delete();
				CellPoints->Delete();
				return true;
				}
			else
				{
				//cout << "Nao!" << endl;
				filter->Delete();
				CellPoints->Delete();
				return false;	
				}
			}
		
		}
		

}

//-------------------------------------------------------------------------------------


void vtkHMCoupledModelWriterPreProcessor::CreateNumberOfDifferentRegionsList()
{
	it = this->FullModelList.begin();
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		if ((*it)->GetNumberOfCoverGroups() > 3) // calcula o numero de regions somente para FullModels com mais de 3 covers
			this->NumberOfRegionsEachFullModel->InsertNextId(this->GetNumberOfDifferentRegions(*it));
		else 
			this->NumberOfRegionsEachFullModel->InsertNextId(1);// estrturas com menos de 3 covers possuem somente 1 regiao
		it++;
		}

}
//-------------------------------------------------------------------------------------


int vtkHMCoupledModelWriterPreProcessor::GetNumberOfDifferentRegions(int FullModelNumber)
{
	return this->NumberOfRegionsEachFullModel->GetId(FullModelNumber);
}

//-------------------------------------------------------------------------------------

bool vtkHMCoupledModelWriterPreProcessor::CheckIfCouplingIsLocatedInSameSegment(vtkIntArray *LocalInfoArray)
{
	if (LocalInfoArray->GetNumberOfTuples() != 2 )
		return false;
	
	if ( int(LocalInfoArray->GetTuple4(0)[0]) == int(LocalInfoArray->GetTuple4(1)[0]) )
		return true;
	else
		return false;	

}


//-------------------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetTotalNumberOfSurfaceGroups()
{	
	it = this->FullModelList.begin();
	int TotalSurfaceGroups = 0;
	for (int i = 0; i < this->GetNumberOf3DFullModels(); ++i)
		{
		this->CurrentFullModel = (*it);	
		TotalSurfaceGroups = TotalSurfaceGroups +	this->CurrentFullModel->GetNumberOfShells();
		it++;
		}
	return TotalSurfaceGroups;
}

//-------------------------------------------------------------------------------------


int vtkHMCoupledModelWriterPreProcessor::GetNumberOfVolumePoints(int VolumeNumber)
{

int TotalVolumeElements = 0;

for (int i = 0; i < VolumeNumber; ++i)
	{
	TotalVolumeElements = TotalVolumeElements + this->FullModelNumberOfPoints->GetValue(i);
	}

return TotalVolumeElements;


}

//-------------------------------------------------------------------------------------
vtkIntArray *vtkHMCoupledModelWriterPreProcessor::GetElementMat1D()
{
	return this->ElementMat1D;
}

//-------------------------------------------------------------------------------------
void vtkHMCoupledModelWriterPreProcessor::SetElementMat1D(vtkIntArray *array)
{
	if ( array )
		{
		if ( !this->ElementMat1D )
			this->ElementMat1D = vtkIntArray::New();
		this->ElementMat1D->DeepCopy(array);
		}
	else
		if ( this->ElementMat1D )
			{
			this->ElementMat1D->Delete();
			this->ElementMat1D = NULL;
			}
}

//-------------------------------------------------------------------------------------

// para cada ID que este metodo recebe como parametro uma busca (BuildIDsList) é realizada e a lista de IDs filhos é criada
// se nessa lista algum segmento de acoplamento estiver presente, isso indicara que este ID é interno ao 3D
//
// duas busca sao feitas de forma paralela na arvore 1d - CreateInnerElementsList e BuildIdList
// CreateInnerElementsList: busca um ID e passa para BuildIDlist que percorre todos filhos daquele ID 
// e para cada lista de filhos, busca se se existe algum segmento de acoplamento. 
//
/*
 * Supondo que temos a seguinte arvore
 * 
 *           (0)
 *            |
 * 						|1
 * 						|
 *           (2) 		 
 * 					  /\				
 * 				  9/  \ 3
 *  				/    \
 *        (10)   (4)
 * 								/\
 * 							7/  \5
 * 							/  	 \
 * 						(8)		(6)
 * 
 * 
 * Segmentos de Acoplamento: 1, 9 e 3
 * 
 * CreateInnerElementsList(1)
 * BuildIdList(1): 1 2 3 4 5 6 7 8 9 10 -- Existem segmentos de acoplamento? Sim entao este ID=1 é interno ao 3D
 * !!! Essa lista gerada por BuildIdlist  é armazenda em this->IDsVisited
 * 
 * 
 ** CreateInnerElementsList(2)
 * BuildIdList(2): 2 3 4 5 6 7 8 9 10 -- Existem segmentos de acoplamento? Sim entao este ID=2 é interno ao 3D
 * 
 * * CreateInnerElementsList(3)
 * BuildIdList(1): 3 4 5 6 7 8 -- Existem segmento de acoplamento? Sim entao este ID=3 é interno ao 3D
 * 
 * * CreateInnerElementsList(4)
 * BuildIdList(1): 4 5 6 7 8 -- Existem segmento de acoplamento? Não entao este ID=4 não é interno ao 3D 
 *
 * 
 * * CreateInnerElementsList(5)
 * BuildIdList(1): 5 6 -- Existem segmento de acoplamento? Não entao este ID=5 não é interno ao 3D 
 * 
 * * CreateInnerElementsList(6)
 * BuildIdList(1): 6 -- Existem segmento de acoplamento? Não entao este ID=6 não é interno ao 3D  
 * 
 * * CreateInnerElementsList(7)
 * BuildIdList(1): 7 8 -- Existem segmento de acoplamento? Não entao este ID=7 não é interno ao 3D 
 
 * * * CreateInnerElementsList(8)
 * BuildIdList(1): 8 -- Existem segmento de acoplamento? Não entao este ID=8 não é interno ao 3D 
 
 * * * CreateInnerElementsList(9)
 * BuildIdList(1): 9 10 -- Existem segmento de acoplamento? Sim entao este ID=9 é interno ao 3D 

 * * * CreateInnerElementsList(10)
 * BuildIdList(1): 10 -- Existem segmento de acoplamento? Não entao este ID=10 não é interno ao 3D 
 * 
 * **************************************************************************************
 * No final a lista (this->Inner1DElements) de elementos internos ao 3D será: 1 2 3 4 9
 * ************************************************************************************** 
 */
void vtkHMCoupledModelWriterPreProcessor::CreateInnerElementsList(int id)
{
 	vtkHM1DTreeElement *elem = NULL;
	vtkHM1DTreeElement *child = NULL;
	vtkHM1DTreeElement *parent = NULL;
	//double x,y,z;
	
	if ( this->StraightModelVisited->IsId(id) != -1 )
		return;
	
	this->StraightModelVisited->InsertNextId(id);
	
	if (this->CheckCouplingSons(id))
		{
		this->Inner1DElements->InsertNextId(id);
		//cout << "Visitando id englobado por 3D "<< id << endl;
		}
	
	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
		{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		}
	else if (this->StraightModel->IsTerminal(id))// se atual elemento da arvore é um terminal
		{
		elem = this->StraightModel->GetTerminal(id);
		
		if (!elem->GetFirstParent())	// se é o coracao
  		{
	    child = elem->GetFirstChild();
	    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(child);
  		}
		else // se o terminal tem pai
			{
		  parent = elem->GetFirstParent();
		  child = elem->GetFirstChild();  // retorna o primeiro segmento filho
			}  
		child = elem->GetFirstChild();
		} // fim else
	
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->CreateInnerElementsList(id);
			}
		}	
}

//----------------------------------------------------------------------------

// checa se o ID está na lista de segmentos de acoplamento 
bool vtkHMCoupledModelWriterPreProcessor::CheckCouplingID(int id)
{  
  for (int i = 0; i < this->TempCouplingInfo->GetNumberOfTuples(); ++i)
  	{
  	if (int(this->TempCouplingInfo->GetTuple(i)[0]) == id)
  		return true;
  	}
  return false;
  
}

//----------------------------------------------------------------------------

// verifica se algum descendente é segmento de acoplamento
// requista criacao da lista de filhos e checa se nessa lista existe algum segmento de acoplamento
bool vtkHMCoupledModelWriterPreProcessor::CheckCouplingSons(int id)
{

	// reseta lista
	this->StraightModelVisited2->Reset();

  this->BuildIDsList(id); // constroi a lista de descendentes deste segmento / terminal analisado
  // constroi lista em this->IDsVisited

 // se encontrar segmento de acoplamento este elemento esta dentro do 3D
 for (int i = 0; i < this->IDsVisited->GetNumberOfIds(); ++i)
 	{
	if (this->CheckCouplingID(this->IDsVisited->GetId(i)) ) // checa se na lista de filhos existe algum segmento de acoplamento
		{
		this->IDsVisited->Reset();
		return true;	
		}
	}
 
	// apaga lista para novo calculo
	this->IDsVisited->Reset();
	return false;
}


//----------------------------------------------------------------------------

// cria lista de segmentos filhos, netos,... (this->IDsVisited) de um determinado ID
// como esse metodo é recurssivo entao lista é criada em uma atributo de classe
void vtkHMCoupledModelWriterPreProcessor::BuildIDsList(int id)
{
 	vtkHM1DTreeElement *elem = NULL;
	vtkHM1DTreeElement *child = NULL;
	vtkHM1DTreeElement *parent = NULL;
	//double x,y,z;
	
	if ( this->StraightModelVisited2->IsId(id) != -1 )
		return;
	
	this->StraightModelVisited2->InsertNextId(id);
	
	this->IDsVisited->InsertNextId(id);


	if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
		{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		}
	else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
		{
		elem = this->StraightModel->GetTerminal(id);
		if (!elem->GetFirstParent())	// se é o coracao
  		{
	    child = elem->GetFirstChild();
  		}
		else // se o terminal tem pai
			{
		  parent = elem->GetFirstParent();
		  child = elem->GetFirstChild();  // retorna o primeiro segmento filho
			}  
		child = elem->GetFirstChild();
		} // fim else
	
	if (!child)
		{
		// É uma folha
		return;
		}
	else
		{
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->BuildIDsList(id);
			}
		}	
}

//----------------------------------------------------------------------------

int vtkHMCoupledModelWriterPreProcessor::GetIdFromInFlowSegment(vtkIntArray *LocalCouplingInfoArray)
{
  int SegmentID = 0;
  
  // identifica o id do segmento proximal
  for (int i = 0; i < LocalCouplingInfoArray->GetNumberOfTuples(); ++i)
  	{
  	if ( int(LocalCouplingInfoArray->GetTuple(i)[3])==1)
  		SegmentID = int(LocalCouplingInfoArray->GetTuple(i)[0]);
  	}
	
	if (!SegmentID)
		vtkErrorMacro("Could not find InFlow segment.  vtkHMCoupledModelWriterPreProcessor::GetIdFromInFlowSegment(vtkIntArray *LocalCouplingInfoArray)");
	
	return SegmentID;
}

//----------------------------------------------------------------------------

bool vtkHMCoupledModelWriterPreProcessor::VerifyCoupledSegment(int id)
{
	for (int i = 0; i < this->CouplingArrayInfo->GetNumberOfTuples(); ++i)
  	{
  	if (this->CouplingArrayInfo->GetTuple(i)[0]==id)
  		return true;	
  	}
	
	return false;	
	
}

//----------------------------------------------------------------------------

void vtkHMCoupledModelWriterPreProcessor::SetWhichFile(int WhichFile[4])
{
	for (int i=0; i < 4; i++)
		this->WhichFile[i]	=	WhichFile[i];
	
}
//-------------------------------------------------------------------------------------
