
#ifndef vtkHMCoupledModelWriterPreProcessor_h_
#define vtkHMCoupledModelWriterPreProcessor_h_

#include "vtkObject.h"
#include "vtkPoints.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DParam.h"
#include "vtkHM3DFullModel.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DSegment.h"
#include "vtkConnectivityFilter.h"
#include "vtkIntArray.h"

class vtkHM1DMesh;
class vtkHM1DBasParam;
class vtkHM1DParam;
class vtkHM3DFullModel;
class vtkHM1DStraightModel;
class vtkHM1DSegment;

#include <vector>
#include <list>
#include <string>



#define VOLUME							0
#define SURFACE 						1
#define COUPLING_ELEMENT 		2
#define COUPLING_TERMINAL 	3
#define NULL_ELEMENT 				4


class VTK_EXPORT vtkHMCoupledModelWriterPreProcessor : public vtkObject
{
public:
	
	//BTX
	typedef std::list<double> ListOfDouble;
	typedef std::vector<ListOfDouble*> VectorOfDoubleList;
	typedef	std::map<vtkHMNodeData *, int> NodeIDPairType;
	typedef std::map<int, int> TerminalIDPairType;
	typedef std::list<int> ListOfInt;
	typedef std::vector<ListOfInt> VectorOfIntArray;
	typedef std::list<vtkHM3DFullModel*> ListOf3DFullModels;
	typedef std::list<int *> ListOfIntArray;
	//ETX
	
	static vtkHMCoupledModelWriterPreProcessor *New();
	vtkTypeRevisionMacro(vtkHMCoupledModelWriterPreProcessor,vtkObject);
	void PrintSelf(ostream& os, vtkIndent indent);

	
	// Description:
  // covers current StraightModel tree in order to get data to  
  // create vtkHM1DMesh, vtkHM1DIniFile and vtkHM1DParam objects.
  // return 1 if sucess; otherwise 0 if some error 
  int InitializeWriteProcess(char *path);
	
	// Description:
	// recursive method that generates the xyz coordinates from the 1D Straight Model Tree.
  // Bellow, the strucuture is explicated in more detailed way:
  // *COORDINATES
  // 1D points coordinates
	void GenerateCoordinates1D(int id);
	
	// Description:
  // generate the coordinates list from the 3D models and coupling terminals and append it with 1D coordinates points
  // Bellow, the strucuture is explicated in more detailed way:
  // *COORDINATES
  // 3D #1 points coordinates
  // ...
  // 3D #n points coordinates
  // Coupling Points:
  // insert a null coordinate for each coupling point in each 3D.
  // if we have two 3d structures one with 3 covers and the other with 2 covers
  // we will have here 5 points with null coordinates 
  void GenerateCoordinates3D();
  
  // Description:
	//
  void Generate1DLists(int id);
  
  // Description:
	//
  void Build1DElementArray();
  
  
  // Description:
	// return the point id from some node
  int GetNodeID(vtkHMNodeData *Node);
  
  // Description:
	// return the point id from the terminal tree id 
  int GetTerminalID(int id);
  
  
  // Description:
  // return Points instance
  // 1D points
  // 3D surface points
  // 3D volume 
  // coupling points
  vtkPoints *GetPoints();
  
  // Description:
  // return the number of points 
  int GetNumberOfPoints();
   
  // Description:
  // Generates the integer arrays ELEMENT TYPE e ELEMENT GROUPS 
  void BuildElementArray();
  
  //BTX
	
	// Description:
  //
  vtkHM1DMesh::VectorOfIntList *GetElementsIncidence();
   
   
  //ETX 
	// Description:
	// set/get methods for the model's number of degrees of freedom 
//	vtkSetMacro(DOF, int);
//	vtkGetMacro(DOF, int);
	
	
	// Description:
	// set/get method for the dimension
//	vtkSetMacro(Dimension, int);
//	vtkGetMacro(Dimension, int);
	
	// Inicio dos metodos para geracao do param
	// Description:
	// Method for creation of the data for param file
	//BTX
	void CreateParam();
	
	
	
	//ETX
	// Fim dos metodos para geracao do param

	
	// Description:
	// Set/Get nElementForGroup
	int *GetElementForGroup();
	void SetElementForGroup(int *elemForGroup);
	
	// Description:
	// Set/Get ElementType
	int *GetElementType();
	void SetElementType(int *elemType);
	
	
	// Description:
	// Set/Get Element mat
	int *GetElementMat();
	void SetElementMat(int *elemMat);
	
	// Description:
	// Set/Get Max1DElemLib
	vtkSetMacro(Max1DElemLib, int);
	vtkGetMacro(Max1DElemLib, int);
	
	// Description:
	// return the number of element groups
	int GetNumberOfElementGroups();
	
	
	// Description:
	// Set/Get DirichletsTag
	vtkIntArray *GetDirichletsTag();
	void SetDirichletsTag(vtkIntArray *d);
	
	// Description:
	// Set/Get Dirichlets
	vtkDoubleArray *GetDirichlets();
	void SetDirichlets(vtkDoubleArray *d);
	
	// Description:
	// generate DirichletsTag and Dirichlets used in Mesh.txt *DIRICHLET CONDITIONS
	void GenerateDirichletsValues();
	
	// Description:
	// generate Infile array with area as only non zero value
	void GenerateIniFile();
  
  // Description:
	// Set /Get IniFileArray 
  vtkDoubleArray *GetIniFileArray();
  void SetIniFileArray(vtkDoubleArray *d);
  
  
  // Description:
  // create BasParam with values from HemoLab General Parameters setup interface
	void ConfigureBasparam(void);
		
		
	// Description:
	//
	void SetElementType(int SurfaceElementType, int CoverElementType, int VolumeElementType);
	
	// Description:
	// set the name of the Basparam log file
	void SetLogFileName (const char *LogFileName);

	// Description:
	// Set method for the interger parameters used in pre processing of solver files structures
	void SetIntegerParam(int *IntParamArray);
	
	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void SetModelConfigParam(double *ModelConfigParamArray);

	// Description:
	// Set method for the time parameters used in pre processing of Basparam solver files structures
	void SetTimeParam(double *TimeParamArray);

	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void SetSegregatedSubRelaxation(double value);
	
	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void SetMonolithicSubRelaxation(double value); 
	
	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void Set3DConvergenceParam(double *array);
	
	
	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void Set1DConvergenceParam(double *array);
	
	
	// Description:
	// Set method for parameters used in pre processing of Basparam solver files structures
	void SetSolverParam(double *array);

  // Description:
  // given some point xyz coordeantes, this returns 
  // the id from the point in the mesh's coordinates list
	int GetPointID(double x, double y, double z);

  // Description:
	// como existe diferenca de arrendondamente entre valor de coordenada
	// de ponto do volume e ponto da superficie, entao é necessario implementar este metodo 
	// para realizar comparacao até o quinto digito depois da virgula 
	// por exemplo: 0.062667 e 0.0626666
	int DoubleCompare(double one, double two);	


  // Description:
	// buids the Transpose Connectivity matrix that stores the neighboors elements from a surface point.
 	// for instance:  node 1 --> elements: 2, 5 ,6
 	//                node 2 --> elements: 33 55 6 44 55
 	//								node n 	 
	void BuildTransposeConnectivity(vtkHM3DFullModel *FullModel);

  // Description:
	// returns the group of some surface point.
	// if the point is located into the cover, this method will return the number of the group
	// if the point is located in wall (including the points located in the border between cover and wall)
	// this method will return the group of surface, which here is zero. 
	int GetGroupFromPoint(int point);
	
	
	// Description:
	// 3DFullModel Local reference set method 
	//vtkSetObjectMacro(FullModel, vtkHM3DFullModel);

	// Description:
	// 1DStraightModel Local reference set method 
	vtkSetObjectMacro(StraightModel, vtkHM1DStraightModel);

  //BTX

	// Description:
	//
	// retorna o vetor de lista de int que guarda a informacao de incidencia 1D
	vtkHM1DMesh::VectorOfIntList *GetIncidenceElements1D();
		
 //ETX
	
	// Description:
	// cria as *INCIDENCE e *ELEMENT TYPE dos elementos 3D
	// n is the number of 3d structures
	// 
	// 1D elements
	// 3D 1 volume elements (assuming we have one volume group ever)
	// 3D n volume elements
	// 3D 1 surface (wall) (from group 0 to maximum number of wall groups - in growing order) 
	// 3D n surface (wall)
	// 3D 1 Coupling elements (4 points) 
	// 3D n Coupling elements (4 points)
	// 3D 1 Terminal Coupling elements (1 point) 
	// 3D n Terminal Coupling elements (1 point) 
	void GenerateIncidence3D();
	
  // Description:
	// Get and Set methods for the interger array that store information related to the
	// coupling points
	vtkSetObjectMacro(CouplingArrayInfo, vtkIntArray);
	vtkGetObjectMacro(CouplingArrayInfo, vtkIntArray);
	
  // Description:
	// return the point id from some node
	int GetNodePointID(int id, int order);
	
  // Description:
	// Given a tree element ID (Terminal or Segment) and a element order (for a Terminal, element order is -1)
	// this method returns True if the tree element is inside the 3D structure; otherwise returns False.
	bool TreeElementInside3D(int SegmentID, int ElemOrder);
	
	
	// Description:
	// Creates the internal database that will be used to determine if some 1d point is inside the 3d structure.
	// For each 3D structure (with 2 covers) this method will return a pair of values indicating the localization from the 3D.
	// Example: If the 3D is connected in segment Id 2 and the Group 1 is linked with the node number 1 and the group 2 is linked with the node
	// number 9, then we will have two points: 2,1 and 2,9. The calculated points are inserted in the vtkDoubleArray this->CouplingInfoIndex.
	// If the 3D structure has more than 2 covers (e.g carotide) this will insert n + 1 duples, being n the number of output flow segments. 
	void CreateCouplingInfoIndex();
	
  
  // Description:
	// Used to create the lists needed to build the Param.txt file	
	void SetParameters(int id, int *position);
	
  // Description:
	// used to create the lists needed to build the Param.txt file	
	void SetTerminalData(vtkHM1DTerminal *term, int *position);
	
  // Description:
	// used to create the lists needed to build the Param.txt file	
	void SetSegmentData(vtkHM1DSegment *segm, int *position);

  // Description:
	// Percorre a lista Element Type e cria a lista *ELEMENT MAT	
	void BuildElementMat();

  // Description:
	// percorre lista *ELEMENT TYPE e troca os valores negativos pelo numero do elemento nulo (99) 
	// na lista de elementos do arquivo BasParam
	void InsertNullElements();
	
	// Description:
	// Value to be used to fill the value of pressure in Inifile for the 1D elements under the 3D structure
	vtkSetMacro(MediumValuePressure, double);	
	vtkGetMacro(MediumValuePressure, double);	
	
	// Description:
	// return the number of 3D FullModels used in the coupled model.
	int GetNumberOf3DFullModels();
	
	
	// Description:
	// Insert a 3D Fullmodel in the Coupled Model.
	void Insert3DFullModel(vtkHM3DFullModel *model);
	
	// Description:
	// set the number of 3d fullmodels
	void SetFullModelNumber(int Number);
	
	// Description:
	// Generates a vtkIntArray with the coupling information from some FullModel envolved in the coupled model
	// with the following structure:
	// 0: Segment Id   1: Node number   2: 3D Groups   3: If current segment is inflow segment (1 otherwise -1)
	vtkIntArray *GetCouplingInfoFrom3D(int FullModelNumber);
	
	// Description:
	// return the number of volume elements from the 3DFullModel number "i" envolved in the model 
	int GetNumberOfVolumeElements(int FullModelNumber);

	// Description:
	// return the number of volume elements from all the 3DFullModels envolved in the model
	int GetTotalNumberOfVolumeElements();
	
	// Description:
	// Returns the number of covers from a 3D FullModel envolved in the model.
	int GetNumberOfCovers(int FullModelNumber);

	// Description:
	// return the total number of cover from all 3D models envolved in the system
	int GetTotalNumberOfCovers();
	
	// Description:
	// return the order from the volume element from some FullModel in the Element Code list
	int GetVolumeElementPosition(int FullModelNumber);
	
	// Description:
	//	return the number of triangules from somegroup from some fullModel
	int GetNumberOfTriangElements(int FullModelNumber, int Group);

	// Description:
	//  return a specific FullModel from list 
	vtkHM3DFullModel *GetFullModel(int FullModelNumber);
	
	
	// Description:
	// Return the type from some given element code
	// 0 volume element
	// 1 surface
	// 2 Coupling Element
	// 3 Coupling Terminal
	int GetElementTypeFromCode(int ElementCode);
	
	// Description:
	// return true if given element terminal coupling is located in proximal segment or proximal element; otherwise returns false 
	bool CheckIfCouplingPointIsProximal(int FullModelNumber, int CouplingPointOrder);
	
	// Description:
	// Set and Get method for ScaleFactor3D
	vtkSetMacro(ScaleFactor3D, double);
	vtkGetMacro(ScaleFactor3D, double);
	
	// Description:
	// Returns the area from a cover from a 3d surface
	double GetCoverArea(int Group, int FullModelNumber);
	
	// Description:
		// Returns the radius from a cover from a 3d surface
	double GetCoverRadius(int Group, int FullModelNumber);
	
	// Description:
	// Compatibilizes the dimensions of 3D model with 1D model; 1D Coupling points are set with the dimension of the 3D cover 
	// radius which the node is attached.   
	// this method can make linear (mode 0) or constant compatibilization (mode 1).
	//
	// General Compatibilization Scheme:
	//
	// If the 3D has 2 covers on the same segment the following scheme is used (assuming P1 is proximal aond P2...Pn are distal):
	// 
	// -------------------P1|||||||||||||||||||||||||||||||||P2------------------
	//  liner/ constant         always linear (R1 to R2)         liner/ constant
	//
	//
	// If the 3D (2 covers) is located in 2 segments with a terminal in middle --> TODO: more than on terminal in milddle of 3D structure
	//
	//----------------------P1||||||||||||||||(Term)||||||||||||||||P2------------------
	//  liner/ constant         always linear (R1 to R2)               liner/ constant
	//
	//
	// If 3D has more than 2 covers:
	//
	//
	//----------------------P1||||||||||||||||(Term)||||||||||||||||P2------------------
	//                                         ||
	//                                         ||	
	//                                         ||
	//                                         || 
	//																				 P3
	//
	// * Linear / Constant from segment begin until P1. From P1 to T Constant R1
	// * Linear / Constant from P2 to segment end. From T until P2 - Constant R2
	// * Linear / Constant from P3 to segment end. From T until P3 - Constant R3
	//
	void CompatibilizeCouplingPoints(int Mode);
	
	void CompatibilizeCouplingPointsInternal(vtkIntArray *LocalInfoArray, int FullModelNumber, int Mode);	
	
	// Description:
	// Test for coupling points accuracy
	// if a error is found return 0; otherwise returns 1.
	int TestCouplingPointsAccuracy();
	
	
	// Description:
	// computes the distance between two points;
	double Distance2BetweenPoints(const double X[3],const double Y[3]);
	
	// Description:
	// return the number of different regions from a selected FullModel. this method uses the filter vtkPVConnectivityFilter
	int GetNumberOfDifferentRegions(vtkHM3DFullModel *FullModel);
	
	// Description:
	// if the Fullmodel has more than one distinct region this will return the Coupling Info the from the selected region
	//vtkIntArray *GetCouplingInfoFromSelectedRegion(int Region);
	vtkIntArray *GetCouplingInfoFromSelectedRegion(int Region, int FullModelNumber);
	
	
	// Description:
	// verifies in a FullModel if some given group is located at some especifique Regions returning true if so.
	bool	CheckGroupFromRegion(int Group, int Region, int FullModelNumber);
	
	// Description:
	// creates a list with the values of different regions from each FullModel.
	void CreateNumberOfDifferentRegionsList();
	
	// Description:
	// return the number of different regions from a selected FullModel.
	int GetNumberOfDifferentRegions(int FullModelNumber);
	
	// Description:
	// verifies if coupling points from a 2 covers 3D structure are located in one same segment.
	bool CheckIfCouplingIsLocatedInSameSegment(vtkIntArray *LocalInfoArray);
	
	
	// Description:
	// return the number of surface groups from all FullModels in coupled model
	int GetTotalNumberOfSurfaceGroups();
	
	// Description:
	// used to compute correct shift value to calculate 3Ds elements incidence * used by GenerateIncidence3D method 
	int GetNumberOfVolumePoints(int VolumeNumber);
	
	// Description:
  // inicia a criacao da lista de elementos internos ao 3D
  // o parametro ID é o segmento onde sera iniciada a busca
  void CreateInnerElementsList(int id);
  
  // Description:
  // checa se id esta na lista de segmentos de acoplamento
  bool CheckCouplingID(int id);
  
  // Description:
  // verifica se algum descendente é segmento de acoplamento
	// requista criacao da lista de filhos e checa se nessa lista existe algum segmento de acoplamento
  bool CheckCouplingSons(int id);
  
  // Description:
  // cria lista de segmentos filhos, netos,... (this->IDsVisited) de um determinado ID
 	// como esse metodo é recursivo entao lista é criada em um atributo de classe
	void BuildIDsList(int id);
  
  // Description:
  // return ID from inflow segment 
  int GetIdFromInFlowSegment(vtkIntArray *LocalCouplingInfoArray);
  
  // Description:
  // returns true if id is a coupling segment (all coupling segments are tested)
  bool VerifyCoupledSegment(int id);
  
  // Description:
	// set which int array with values from filter
	void SetWhichFile(int WhichFile[4]);
	
	vtkSetMacro(QuantityOfDifferentElements, int);
	vtkGetMacro(QuantityOfDifferentElements, int);
	

//======================================================================================	
//======================================================================================	
//======================================================================================	
//======================================================================================	
	
	// Description:
	// Set/Get ElementMat1D.
	vtkIntArray *GetElementMat1D();
	void SetElementMat1D(vtkIntArray *array);

protected:
	vtkHMCoupledModelWriterPreProcessor();
	~vtkHMCoupledModelWriterPreProcessor();
	
  // Description:
  // used to build coordinates list
  vtkPoints* 		points;

	//BTX
  
  // Description:
  //pair: node; coordinates list point id
  NodeIDPairType NodeIDPair;
	
	// Description:
	// pair: tree id; coordinates list point id
	TerminalIDPairType TerminalIDPair;
	
	// Description:
	// used tu build TerminalIDPair and NodeIDPair pairs
	int point;
	
		
  // Description:
  // Vector with an list of the element groups
  // vetor de lista de int que guarda a informacao de incidencia 1D
	vtkHM1DMesh::VectorOfIntList IncidenceElements1D;


  // Description:
  // Vector with an list of the element groups
	vtkHM1DMesh::VectorOfIntList ElementsIncidence;
	//ETX
	
	// Description:
	//
	//int DOF;
	
	// Description:
	//
	//int Dimension;
	
	// Description:
  // Number of elements for group
  int *nElementForGroup;
  	
  	
  // Description:
  // Types of elements of the 1D tree (terminal, segment, etc...)
  //*ELEMENT TYPE
  // se Max1DElemLib do basparam é 4 
  // 2 heart / terminal
  // 1 elem
  // 3 elem in
  // 4 elem out
	int *ElementType;
	
	
	// Description:
	// vetor auxiliar usuado na construcao da lista *Element Type
	// este é preenchido nos metodos: Generate1DList e GenerateIncidence3D
	/*
	 vol 1
	 vol n
	 wall 1
	 wall 2
	 Coupling elem 1 (vol 1)
	 Coupling elem n (vol 1)
	 Coupling elem 1 (vol n)
	 Coupling elem n (vol n)
	 Coupling Term elem 1 (vol 1)
	 Coupling Term elem n (vol 1)
	 Coupling Term elem 1 (vol n)
	 Coupling Term elem n (vol n)
	 Null Elem
	
	*/ 
	vtkIntArray *ElementTypeArray;
	
	
	// Description:
  // Element mat of the 1D tree
  int *ElementMat;

	// Description:
  // Maximum elements used in 1D Model (deafult values are 2 or 4)
	int Max1DElemLib;
  
  //BTX
  // Variaveis do arquivo param
  int *QuantityOfRealParameters;	

	// Description:
	// used to build list of Integer param for Param.txt file
	int *QuantityOfIntegerParameters;
	
	// Description:
	// used to build list to be used in Param.txt file
	vtkHM1DParam::VectorOfDoubleList RealParameters;
	
	// Description:
	//used to build list to be used in Param.txt file
	vtkHM1DParam::VectorOfIntList IntegerParameters;
	
	
	// Description:
	// list of FullModels from the current coupled model
	ListOf3DFullModels FullModelList;
	
	//ETX
	// Fim de declaracoes do arquivo param
	
	
	// Description:
	// object used in building of Mesh file
	vtkIntArray *DirichletsTag;
	
	// Description:
	//object used in building of Mesh file
	vtkDoubleArray *Dirichlets;
		
	// Description:
	//object used in building of Inifile file
	vtkDoubleArray *IniFileArray;
		
	// Description:
	//object used in building of Inifile file
	vtkDoubleArray *IniFileDoubleArray;
	
	// Description:
	// object used in building of Basparam file
	vtkHM1DBasParam *BasParam;
	
	// Description:
	// basparam degree of freedom value
	int BasParamDOFNumber; 
	
	// Description:
	// current FullModel reference - used when the list of 3D models is being passed through
	vtkHM3DFullModel *CurrentFullModel;
	
	// Description:
	// local reference to 1D model
	vtkHM1DStraightModel *StraightModel;
	
	// Description:
	//
	//int NumGroups;

	// Description:
	//
	//int nel;
	
	// Description:
	// surface element type used in basparam
	int	SurfaceElementType;

	// Description:
	// cover element type used in basparam
	int CoverElementType;

	// Description:
	// volume code used in Basparam
	int VolumeElementType;
	
	// Description:
	// Log file name used in Basparam
	char LogFileName[50];
	
	// Description:
	// properties values from solver files configuration window.
	int *IntParamArray;
	
	// Description:
	// properties values from solver files configuration window. 
	double *ModelConfigParamArray;

	// Description:
	// properties values from solver files configuration window.
	double *TimeParamArray;
	

	// Description:
	// properties values from solver files configuration window.
	double SegregatedSubRelaxation;
	
	// Description:
	// properties values from solver files configuration window.
	double MonolithicSubRelaxation; 

	// Description:
	// properties values from solver files configuration window.
	double *ConvergenceParam3D;

	// Description:
	//properties values from solver files configuration window.
	double *ConvergenceParam1D;
	
	
	// Description:
	//properties values from solver files configuration window.
	double *SolverConfigParamArray;
	
	// Description:
	//	vector used in vtkHMCoupledModelWriterPreProcessor::BuildTransposeConnectivity(vtkHM3DFullModel *FullModel)
	VectorOfIntArray TransposeConnectivity;
	
	// Description:
	//list used by vtkHMCoupledModelWriterPreProcessor::GetGroupFromPoint(int) method	
	ListOfInt GroupFromPoint;
	
	// Description:		
	//Number of segments of the tree, discard segments that are not connected the main tree
	int NumberOfSegments;
	
	// Description:	
	// track the number of 1D elements - used only internally
  int NumberOfPoints1D;
 
	// Description:	
 	// array that store the information related to each coupling point
 	// #1(id_segment, node number, 3d group, FullModel number);
 	// #2(id_segment, node number, 3d group, FullModel number);
 	vtkIntArray *CouplingArrayInfo;

	// Description:
	//
	double MediumValuePressure;	
	
	// Description:
	//
	bool InFlowElementSet;
	
	// Description:	
	// track the number of 3D thetraedrum elements - used only internally
	int NumberOfVolumePoints;
	
	// Description:
	// List com ids of the elements visited on 1d tree
	vtkIdList *StraightModelVisited;
	
	//
	vtkIdList *StraightModelVisited2;
	
	// Description:
	//
	int FullModelNumber;
	
	// Description:
	// guarda o numero de pontos de volume de cada FullModel para posterior utilizacao na criacao 
	// da lista de incidencia
	vtkIntArray *FullModelNumberOfPoints;
	
	
	// Description:
	//
	vtkIntArray *SelectedCouplingInfo;
	
	//BTX
	// Description:
	// iterator for the 3DFullModel list
	ListOf3DFullModels::iterator it;
	//ETX

	// Description:
	// store the element list used in Basparam
	vtkIntArray *BasParamElementList;
	
	
	//BTX
	// Description:
	// used to store integer list from substep basparam file
	ListOfIntArray Substeps3DElementList;
	//ETX
	
	
	// Description:
	// store the number of elements from 1D model
	int NumberOf1DElements;
	
	
	// Description:
	// used to count the current coupling terminal element in the element list in the substep
	// this variable is used to determine if current element is in the proximal or distal position
	int CouplingTerminalNumber;


	// Description:
	// used to compatibilize scale between 1D and 3D model (if difference between scale is big simulation might fail )
	double ScaleFactor3D;
	
	// Description:
	// list that indicates the number of different regions for each FullModel
	// ex: 1 2 2 1 --> 4 FullModels: the second and third have two regions.
	vtkIdList *NumberOfRegionsEachFullModel;
	
	// Description:
	// used only to the tags that indicate flow direction from coupling terminals
	// store the flow direction (1 or -1) from all coupling points * USED in the Basparam configuration
	vtkIdList *TerminalFlowDirection;
	
	// Description:
	// used to create file that describe general model info like number of 1d points, number of 3d strucutures and so on...
	// # of 1d elem; # of 3ds structs; # 3d volume elem of 3d 1; # 3d volume elem of 3d n....
	vtkIdList *ModelInfo;
	
	// Description:
	//
	int SubStepNumber;
	
	// Description:
	// Element mat for 1D data. Data without null element.
	vtkIntArray *ElementMat1D;
	
	// Description:
	// used to collect ids from segments which are covered by the 3d structure
	vtkIdList *SegmentIDList;
	
	
	// Description:
	//
	vtkIntArray *TempCouplingInfo;
	
	// Description:
	// id of tree elements that have been visited
	vtkIdList *IDsVisited;
	
	// Description:
	// ids of tree elements (terminals or segments) which are under 3D structure
	vtkIdList *Inner1DElements;
	
	// Description:
	// indicates which solver file will be generated
	int WhichFile[4];
	
	
	
	
	//int NumberOf1DElements;
	int NumberOf3DElements; 
	
	int NumberOf3DSurfaces;
	
	int NumberOfCouplingElem;
	
	
	int NumberOfNullElements;
	
		
		
	int TotalNumberOfElements;
	
	
	int QuantityOfDifferentElements;
	
	int TotalNumberOfCoordinates;
	
	
}; //End class

#endif 
