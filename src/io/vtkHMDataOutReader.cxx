/*
 * $Id: vtkHMDataOutReader.cxx 2371 2007-10-19 19:50:54Z igor $
 */
#include "vtkHMDataOutReader.h"
#include "vtkHMDataOut.h"
#include "vtkHM1DMesh.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM3DModelReader.h"

#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

#include <fstream>

#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHMDataOutReader, "$Revision: 2371 $");
vtkStandardNewMacro(vtkHMDataOutReader);

vtkHMDataOutReader::vtkHMDataOutReader()
{
	vtkDebugMacro(<<"Reading DataOut data...");
	this->DataOut = vtkHMDataOut::New();

	this->CoupledModel = false;

	this->NumberOfPointsOf1DTree = 0;
}

//----------------------------------------------------------------------------
vtkHMDataOutReader::~vtkHMDataOutReader()
{
	if( this->DataOut )
		this->DataOut->Delete();
}

//----------------------------------------------------------------------------
void vtkHMDataOutReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
// Read DataOut file
int vtkHMDataOutReader::ReadDataOut(char *file, vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkDataReader *reader)
{
	vtkDebugMacro(<<"Reading DataOut file");
	
	this->SetFileName(file);
	
	if ( !this->OpenFile() )
		{
		return 0;
		}
	
	if ( !this->ReadData(mesh, BasParam, reader) )
		{
		vtkErrorMacro(<<"Can't read data. " << this->GetFileName());
    return 0;
		}
	
	this->CloseFile();
	
	return 1;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHMDataOutReader::CloseFile()
{
	vtkDebugMacro(<<"Closing DataOut file");
	if ( this->IS != NULL )
		{
		delete this->IS;
		}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Open File
int vtkHMDataOutReader::OpenFile()
{
	if (this->ReadFromInputString)
		{
		if (this->InputArray)
			{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
			}
		else if (this->InputString)
			{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
			}
		}
	else
		{
		vtkDebugMacro(<< "Opening DataOut file");

		if ( !this->FileName || (strlen(this->FileName) == 0))
			{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
			}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
			{
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
			}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
			{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
			}
			
		return 1;
		}
	
	return 0;
}

//----------------------------------------------------------------------------
//ReadData
int vtkHMDataOutReader::ReadData(vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkDataReader *reader)
{
	vtkDebugMacro(<< "Reading Data of the DataOut File");
	
	char line[256];
	float progress;
	
	int TimeQuantityFile = int (BasParam->GetTimeQuantityFile());
	int DelT = int (BasParam->GetDelT());
	int Tmax = int (BasParam->GetTmax());
	
	//calculate the number of blocks
	int numberOfBlock = int(BasParam->GetTmax() / (BasParam->GetDelT() * BasParam->GetTimeQuantityFile()));
	// calculo do numero de blocos especifico para arquivos gerados pelo HeMoLab
	
	numberOfBlock += 1;
	
	this->DataOut->SetNumberOfBlock(numberOfBlock);
	
	double *results;
	int numDegree;
	
	numDegree = mesh->GetDegreeOfFreedom();
	
	results = new double[numDegree];
	this->Max = new double[numDegree];
	this->Min = new double[numDegree];
	for ( int i=0; i<numDegree; i++ )
		{
		this->Max[i] = -10000000;
		this->Min[i] = 10000000;
		}
	
	//Setting number of components and number of tuples of the vtkDoubleArray
	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfComponents(numDegree);
	
	vtkIntArray *timeStep = vtkIntArray::New();
	vtkDoubleArray *instantOfTime = vtkDoubleArray::New();
	vtkDoubleArray *timeStepUsed = vtkDoubleArray::New();
	
	// se nao estiver no modo acoplado, uso o numero de pontos lido no arquivo mesh,
	// caso contrario uso o numero setado de acordo com a arvore 1D do arquivo mesh 1D3D.
	// Esse valor eh setado no vtkHM1DStraightModelReader::ReadSimulationData.
	if ( !this->CoupledModel )
	  this->NumberOfPointsOf1DTree = mesh->GetNumberOfPoints();

	//Readind data
	for ( int i=0; i<numberOfBlock; i++)
		{
		if (!this->ReadString(line))
			{
			numberOfBlock = i; // o número de blocos será o total encontrado na leitura e não o do BasParam
			this->DataOut->SetNumberOfBlock(i);
			break;
			}

		if ( strncmp ("*Time", line, 30) )
			{
			vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
			                  << (this->FileName?this->FileName:"(Null FileName)"));
			    
			this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
			timeStep->Delete();
			instantOfTime->Delete();
			timeStepUsed->Delete();
			array->Delete();
			return 0;
			}
		
		this->ReadLine(line);
		int time;
		//Read TimeStep
		if(!this->Read(&time))
			{
			vtkErrorMacro(<<"Can't read time step" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			timeStep->Delete();
			instantOfTime->Delete();
			timeStepUsed->Delete();
			array->Delete();
			return 0;
			}
		timeStep->InsertNextValue(time);
		
		double instant;
		//Read InstantOfTime
		if(!this->Read(&instant))
			{
			vtkErrorMacro(<<"Can't read instant of time" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			timeStep->Delete();
			instantOfTime->Delete();
			timeStepUsed->Delete();
			array->Delete();
			return 0;
			}
		instantOfTime->InsertNextValue(instant);
		
		double used;
		//Read TimeStepUsed
		if(!this->Read(&used))
			{
			vtkErrorMacro(<<"Can't read time step used for the calculation" << "! for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			timeStep->Delete();
			instantOfTime->Delete();
			timeStepUsed->Delete();
			array->Delete();
			return 0;
			}
		timeStepUsed->InsertNextValue(used);
		
		this->ReadLine(line);
		
		//Read data of the degree of freedom
    for ( int k=0; k<this->NumberOfPointsOf1DTree; k++)
			{
			for (int j=0 ; j<numDegree; j++)
				{				
				if(!this->Read(&results[j]))
					{
					vtkErrorMacro(<<"Can't read the Data" << "! for file: " 
									<< (this->FileName?this->FileName:"(Null FileName)"));
					timeStep->Delete();
					instantOfTime->Delete();
					timeStepUsed->Delete();
					array->Delete();
					return 0;
					}
				}

			this->SetMax(results, numDegree);
			this->SetMin(results, numDegree);
			array->InsertNextTuple(results);
			}
		

    // se estiver no modo acoplado, pulo a leitura dos dados 3D.
    if ( this->CoupledModel )
      {
      this->ReadLine(line);
      this->ReadLine(line);

      string str(line);

      //conta quantos espaços tem na linha, assim eu se o arquivo esta
      //em uma coluna ou mais.
      int pos = str.find(" ");

      int count=0;
      while(pos != -1)
        {
        count++;
        pos = str.find(" ", pos+1);
        }

      int nBytes=0;

      //verifica se o dataout é escrito em coluna unica ou
      //igual ao numero de graus de liberdade
      if ( count <= 2 )
        {
        // calculo de quantos bytes serao pulados, caso em que o arquivo é
        // escrito em apenas uma coluna.

        // multiplico o numero de pontos 3D pelo numero de graus de liberdade e
        // pelo tamanho da linha lida, somando tambem as quebras de linha
        // que não são contabilizadas na string, no final diminuo a
        // string lida logo acimo para saber o tamanho da linha.
        nBytes = (mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) * mesh->GetDegreeOfFreedom() * str.size()
            + ((mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) *mesh->GetDegreeOfFreedom()) - str.size()-2;

//        nBytes = (mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) * mesh->GetDegreeOfFreedom()*sizeof(double) * 2
//            + ((mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) *mesh->GetDegreeOfFreedom() *2) - str.size()-2;
        }
      else
        {
        //calculo de quantos bytes serao pulados, caso em que o arquivo é
        // escrito em varias colunas, de acordo com o numero de graus de liberdade.

        //multiplico o numero de pontos 3D pelo tamanho da linha lida e
        // somo tambem as quebras de linha que não são contabilizadas na string,
        // no final diminuo a string lida logo acimo para saber o tamanho da linha.
        nBytes = (mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) * str.size()
            + ((mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree)) - str.size()-2;

//        nBytes = (mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) * mesh->GetDegreeOfFreedom()*sizeof(double) * 2
//            + ((mesh->GetNumberOfPoints() - this->NumberOfPointsOf1DTree) *mesh->GetDegreeOfFreedom() *2) - str.size()-2;
        }

      //seto o ponteiro do leitor na posicao de interesse.
      this->IS->seekg(nBytes, ios::cur);
      }

		//Setting progress of the read
		progress=reader->GetProgress();
		reader->SetProgress(progress + 0.001*(1.0 - progress));
		reader->UpdateProgress(reader->GetProgress());
		
		}
  array->SetNumberOfTuples(numberOfBlock*this->NumberOfPointsOf1DTree);
	timeStep->SetNumberOfTuples(numberOfBlock);
	instantOfTime->SetNumberOfTuples(numberOfBlock);
	timeStepUsed->SetNumberOfTuples(numberOfBlock);
	
	this->DataOut->SetMax(this->Max);
	this->DataOut->SetMin(this->Min);
	this->DataOut->SetTimeStep(timeStep);
	this->DataOut->SetInstantOfTime(instantOfTime);
	this->DataOut->SetTimeStepUsed(timeStepUsed);
	this->DataOut->SetDegreeOfFreedomArray(array);
	
	timeStep->Delete();
	instantOfTime->Delete();
	timeStepUsed->Delete();
	
	return 1;
}

void vtkHMDataOutReader::SetDataOut(vtkHMDataOut *DO)
{
	this->DataOut = DO;
}

vtkHMDataOut *vtkHMDataOutReader::GetDataOut()
{
	return this->DataOut;
}

//---------------------------------------------------------------------
void vtkHMDataOutReader::SetMax(double *m, int t)
{
	for (int i=0; i<t; i++)
		if ( this->Max[i] < m[i] )
			this->Max[i] = m[i];
}

double *vtkHMDataOutReader::GetMax()
{
	return this->Max;
}

//---------------------------------------------------------------------
void vtkHMDataOutReader::SetMin(double *m, int t)
{
	for (int i=0; i<t; i++)
		if ( this->Min[i] > m[i] )
			this->Min[i] = m[i];
}

double *vtkHMDataOutReader::GetMin()
{
	return this->Min;
}

//---------------------------------------------------------------------
void vtkHMDataOutReader::CoupledModelOn()
{
  this->CoupledModel = true;
}

//---------------------------------------------------------------------
void vtkHMDataOutReader::CoupledModelOff()
{
  this->CoupledModel = false;
}
