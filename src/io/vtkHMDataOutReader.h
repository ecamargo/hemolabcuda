/*
 * $Id:
 */
#ifndef VTKHMDATAOUTREADER_H_
#define VTKHMDATAOUTREADER_H_

#include "vtkDataReader.h"

#include "vtkDoubleArray.h"

#include <string>

class vtkHM1DMesh;
class vtkHM1DBasParam;
class vtkHMDataOut;
class vtkHM3DModelReader;
class vtkHM1DStraightModelReader;

using namespace std;
	
class VTK_EXPORT vtkHMDataOutReader : public vtkDataReader
{
public:
	static vtkHMDataOutReader *New();
	vtkTypeRevisionMacro(vtkHMDataOutReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	//Read the data of the DataOut.txt file for 3D model.
	int ReadDataOut(char *file, vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkDataReader *reader);
	
	// Description:
	//Read the data of the DataOut.txt file for 1D model.
//	int ReadDataOut(char *file, vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkHM1DStraightModelReader *reader);
	
	// Description:
	//Read the results in DataOut file for 3D model
	int ReadData(vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkDataReader *reader);
	
	// Description:
	//Read the results in DataOut file for 1D model
//	int ReadData(vtkHM1DMesh *mesh, vtkHM1DBasParam *BasParam, vtkHM1DStraightModelReader *reader);
	
	void SetDataOut(vtkHMDataOut *DO);
	vtkHMDataOut *GetDataOut();
	
	double *Max;
	double *Min;
	
	void SetMax(double *m, int t);
	double *GetMax();
	
	void SetMin(double *m, int t);
	double *GetMin();
	
	// Description:
	// Set coupled mode.
	void CoupledModelOn();
	void CoupledModelOff();

  vtkSetMacro(NumberOfPointsOf1DTree,int);
  vtkGetMacro(NumberOfPointsOf1DTree,int);

protected:
	vtkHMDataOutReader();
	~vtkHMDataOutReader();
	
	vtkHMDataOut *DataOut;
	
	// Description:
	// File is coupled mode. Default is false.
	bool CoupledModel;

	// Description:
	// Number of points of 1D tree. Used in case of read coupled model.
	// In this case read only 1D data to optimize time of reading.
	int NumberOfPointsOf1DTree;

}; //End class

#endif /*VTKHMDATAOUTREADER_H_*/
