/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkHMEnSightWriter.h,v $

  Copyright (c) Eduardo Camargo
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkHMEnSightWriter - write vtk unstructured grid data as an EnSight file
// .SECTION Description
// vtkHMEnSightWriter is a source object that writes binary 
// unstructured grid data files in EnSight format. See EnSight Manual for 
// format details or vtkEnSightWriter class

// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.
// Be sure to specify the endian-ness of the file when reading it into EnSight

#ifndef VTKHMENSIGHTWRITER_H_
#define VTKHMENSIGHTWRITER_H_

#include "vtkWriter.h"
#include "vtkEnSightWriter.h"

class vtkUnstructuredGrid;
class vtkModelMetadata;

class VTK_EXPORT vtkHMEnSightWriter : public vtkEnSightWriter
{

public:
  vtkTypeRevisionMacro(vtkHMEnSightWriter, vtkEnSightWriter);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkHMEnSightWriter *New();

  // Description
  // Writes the case file that EnSight is capable of reading
  // The other data files must be written before the case file
  // and the input must be one of the time steps
  // variables must be the same for all time steps or the case file will be 
  // missing variables (over write WriteCaseFile method of vtkEnSightWriter class
  virtual void WriteCaseFile(int TotalTimeSteps);
  
  // Description:
  // Define byte order for output files. Little Endian = 0 and Big Endian = 1, default value is 1.
//	void SetByteOrder(int byte) {this->ByteOrder = byte;}
//	int GetByteOrder() {return this->ByteOrder;}
  vtkSetMacro(ByteOrder, int);
	vtkGetMacro(ByteOrder, int);
  
protected:
  vtkHMEnSightWriter();
  virtual ~vtkHMEnSightWriter();

  virtual void WriteData();
  virtual void WriteIntToFile(const int i,FILE* file);
  virtual void WriteFloatToFile(const float, FILE*);
  
  // Description:
  // Define byte order for output files. Little Endian = 0 and Big Endian = 1, default value is 1.
  int ByteOrder;
  
  vtkHMEnSightWriter(const vtkHMEnSightWriter&);  // Not implemented.
  void operator=(const vtkHMEnSightWriter&);  // Not implemented.

};

#endif /*VTKHMENSIGHTWRITER_H_*/
