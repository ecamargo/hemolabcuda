
#include "vtkHMGeneralInfoReader.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkIntArray.h"
#include "vtkDoubleArray.h"
#include "vtkErrorCode.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"

#include "vtkHMGeneralInfo.h"

#include <fstream>
#include <sys/stat.h>

vtkCxxRevisionMacro(vtkHMGeneralInfoReader, "$Revision: 2402 $");
vtkStandardNewMacro(vtkHMGeneralInfoReader);

//----------------------------------------------------------------------------
vtkHMGeneralInfoReader::vtkHMGeneralInfoReader()
{
	vtkDebugMacro(<<"Reading GeneralInfo file...");
	this->GInfo = vtkHMGeneralInfo::New();
}

//----------------------------------------------------------------------------
vtkHMGeneralInfoReader::~vtkHMGeneralInfoReader()
{
	this->GInfo->Delete();
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfoReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
	 os << indent << "GeneralInfo "<< indent << *this->GInfo << "\n";
}

//----------------------------------------------------------------------------
//Open File
int vtkHMGeneralInfoReader::OpenFile()
{
	if (this->ReadFromInputString)
	{
		if (this->InputArray)
		{
			vtkDebugMacro(<< "Reading from InputArray");
			this->IS = new istrstream(this->InputArray->GetPointer(0), 
			                                this->InputArray->GetNumberOfTuples()*
			this->InputArray->GetNumberOfComponents());
			return 1;
		}
		else if (this->InputString)
		{
			vtkDebugMacro(<< "Reading from InputString");
			this->IS = new istrstream(this->InputString, this->InputStringLength);
			return 1;
		}
	}
	else
	{
		vtkDebugMacro(<< "Opening GeneralInfo file");
		
		if ( !this->FileName || (strlen(this->FileName) == 0))
		{
			vtkErrorMacro(<< "No file specified!");
			this->SetErrorCode( vtkErrorCode::NoFileNameError );
			return 0;
		}

		// first make sure the file exists, this prevents an empty file from
		// being created on older compilers
		struct stat fs;
		if (stat(this->FileName, &fs) != 0) 
		{
			//vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
		this->IS = new ifstream(this->FileName, ios::in);
		if (this->IS->fail())
		{
			vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
			delete this->IS;
			this->IS = NULL;
			this->SetErrorCode( vtkErrorCode::CannotOpenFileError );
			return 0;
		}
			
		return 1;
	}
	
	return 0;
}

//----------------------------------------------------------------------------
// Close file.
void vtkHMGeneralInfoReader::CloseFile()
{
	vtkDebugMacro(<<"Closing GeneralInfo file");
	if ( this->IS != NULL )
	{
		delete this->IS;
	}
	this->IS = NULL;
}

//----------------------------------------------------------------------------
//Read GeneralInfo File
int vtkHMGeneralInfoReader::ReadGeneralInfoFile(char *dir)
{	
	char line[256];
	char diretorio[256];
	strcpy(diretorio, dir);
	
	vtkDebugMacro(<<"ReadGeneralInfoFile()...");

	this->SetFileName(strcat(diretorio,"ModelInfo.hml13d"));
	
	if ( !this->OpenFile() )
	{
		//vtkErrorMacro(<<"Can't open file. " << this->GetFileName());
    return 0;
	}
	
	if( !this->ReadHeader() )
	{
		vtkErrorMacro(<<"Can't read header file. " << this->GetFileName());
    return 0;
	}
	
	this->ReadLine(line);
	
	if( !this->Read3DStructures() )
	{
		vtkErrorMacro(<<"Can't read 3D STRUCTURES tag. " << this->GetFileName());
    	return 0;
	}
	
	this->ReadLine(line);
	
	if( !this->Read3DVolumeElements() )
	{
		vtkErrorMacro(<<"Can't read 3D VOLUME ELEMENTS tag. " << this->GetFileName());
    	return 0;
	}
	
	this->ReadLine(line);
	
	if( !this->Read3DShellParameters() )
	{
		vtkErrorMacro(<<"Can't read 3D SHELL PARAMETERS tag. " << this->GetFileName());
    	return 0;
	}	

	this->ReadLine(line);
	
	if( !this->Read3DCapsElements() )
	{
		vtkErrorMacro(<<"Can't read 3D COUPLING PARAMETERS tag. " << this->GetFileName());
    	return 0;
	}	
	
	this->ReadLine(line);
	
	if( !this->ReadNumberOfSubSteps() )
	{
		vtkErrorMacro(<<"Can't read NUMBER OF SUBSTEPS tag. " << this->GetFileName());
    	return 0;
	}
	
	this->CloseFile();
		
	return 1;
}

//----------------------------------------------------------------------------
//Read Header
int vtkHMGeneralInfoReader::ReadHeader()
{
	char line[256];

	vtkDebugMacro(<< "Reading GeneralInfo file header");

	// read header
	
	if (!this->ReadLine(line))
		{
		vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
		}
	if ( strncmp ("*1D ELEMENTS", line, 20) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
		}
	int NumberOf1DCells;
	if(!this->Read(&NumberOf1DCells))
		{
		vtkErrorMacro(<<"Can't read number of 1D points!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	
	this->GInfo->SetNumberOf1DCells(NumberOf1DCells);
	this->ReadLine(line); // To take the blank line at the end of the tag
	
	if (!this->ReadLine(line))
		{
		vtkErrorMacro(<<"Premature EOF! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
		}
	
	this->ReadLine(line); // To take the blank line at the end of the tag
	
	if ( strncmp ("*NUMBER OF DIFFERENT 1D ELEMENTS", line, 35) )
		{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
		}
	
	int NumberOfElementType;
	if(!this->Read(&NumberOfElementType))
		{
		vtkErrorMacro(<<"Can't read number of element type of 1D model!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	
	this->GInfo->SetNumberOfElementType(NumberOfElementType);
	
	while ( strncmp ("*NULL 1D ELEMENTS", line, 20) )
		{
		if (!this->ReadLine(line))
			{
				vtkErrorMacro(<<"Premature EOF reading *NULL 1D ELEMENTS line! " << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
				this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
				return 0;
			}
		}
	
	int NumberOfNullElement;
	if(!this->Read(&NumberOfNullElement))
		{
		vtkErrorMacro(<<"Can't read number of null elements of 1D model!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}

	vtkIntArray *elementList = NULL;
	if ( NumberOfNullElement > 0 )
		{
		int id, type, mat;
		elementList = vtkIntArray::New();
		elementList->SetNumberOfComponents(3);
		for ( int i=0; i<NumberOfNullElement; i++ )
			{
			if(!this->Read(&id) || !this->Read(&type) || !this->Read(&mat))
				{
				vtkErrorMacro(<<"Can't read of null element of 1D model!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			elementList->InsertNextTuple3(id, type, mat);
			}
		}
	
	this->GInfo->SetNullElementList(elementList);
	if ( elementList )
		elementList->Delete();

	while ( strncmp ("*NULL 1D TERMINALS", line, 20) )
		{
		if (!this->ReadLine(line))
			{
				vtkErrorMacro(<<"Premature EOF reading *NULL 1D TERMINALS line! " << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
				this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
				return 0;
			}
		}
	
	int NumberOfNullTerminal;
	if(!this->Read(&NumberOfNullTerminal))
		{
		vtkErrorMacro(<<"Can't read number of null terminals of 1D model!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
		}
	
	vtkIntArray *terminalList = NULL;
	if ( NumberOfNullTerminal > 0 )
		{
		int id, type, mat;
		terminalList = vtkIntArray::New();
		terminalList->SetNumberOfComponents(3);
		for ( int i=0; i<NumberOfNullTerminal; i++ )
			{
			if(!this->Read(&id) || !this->Read(&type) || !this->Read(&mat))
				{
				vtkErrorMacro(<<"Can't read of null terminal of 1D model!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			terminalList->InsertNextTuple3(id, type, mat);
			}
		}
	
	this->GInfo->SetNullTerminalList(terminalList);
	if ( terminalList )
		terminalList->Delete();
	
	this->ReadLine(line); // To take the blank line at the end of the tag
	return 1;
}

//----------------------------------------------------------------------------
//Read 3D Volume Elements
int vtkHMGeneralInfoReader::Read3DVolumeElements()
{
		char line[256];

		vtkDebugMacro(<< "Reading GeneralInfo file 3D STRUCTURES");
	
	//read number of volume points
	
	if (!this->ReadLine(line))
	{
		vtkErrorMacro(<<"Premature EOF reading number of volume points line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	if ( strncmp ("*3D VOLUME ELEMENTS", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	vtkIntArray *NumberOfVolumeCells = vtkIntArray::New();
	int temp;
	for(int i = 0; i < this->GInfo->GetNumberOf3DObjects(); i++)
	{
		if(!this->Read(&temp))
		{
			vtkErrorMacro(<<"Can't read number of volume points!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
		NumberOfVolumeCells->InsertNextValue(temp);
	}
	this->GInfo->SetNumberOfVolumeCells(NumberOfVolumeCells);
	NumberOfVolumeCells->Delete();
	this->ReadLine(line); // To take the blank line at the end of the tag
	
	return 1;
}

//----------------------------------------------------------------------------
//Read 3D Structures
int vtkHMGeneralInfoReader::Read3DStructures()
{
	char line[256];

	vtkDebugMacro(<< "Reading GeneralInfo file 3D STRUCTURES");
	
	//read number of 3D objects
	
	if (!this->ReadLine(line))
	{
		vtkErrorMacro(<<"Premature EOF reading 3D Structures line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	
	if ( strncmp ("*3D STRUCTURES", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	int NumberOf3DObjects;
	if(!this->Read(&NumberOf3DObjects))
	{
		vtkErrorMacro(<<"Can't read number of 3D objects!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->GInfo->SetNumberOf3DObjects(NumberOf3DObjects);
	this->ReadLine(line); // To take the blank line at the end of the tag

	return 1;
}

//----------------------------------------------------------------------------
//Read 3D SHELL PARAMETERS
int vtkHMGeneralInfoReader::Read3DShellParameters()
{
	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX
	
	char line[256];

	vtkDebugMacro(<< "Reading GeneralInfo file 3D SHELL PARAMETERS");
	
	//read number of Shell cells
	
	if (!this->ReadLine(line))
	{
		vtkErrorMacro(<<"Premature EOF reading 3D SHELL PARAMETERS line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	if ( strncmp ("*3D SHELL PARAMETERS", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
			
	vtkHMGeneralInfo::VectorOfIntList shellParameters;
	vtkHMGeneralInfo::ListOfInt lst;
	int temp, var;
	for(int i=0; i < this->GInfo->GetNumberOf3DObjects(); i++)
	{
		if ( !this->Read( &temp ) )
			{
			vtkErrorMacro(<<"Can't read number of SHELL PARAMETERS!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		lst.push_back(temp);
		
		for ( int j = 0; j < temp; j++ )
		{
			if ( !this->Read( &var ) )
				{
				vtkErrorMacro(<<"Can't read number of SHELL PARAMETERS!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			lst.push_back(var);
		}
		shellParameters.push_back(lst);
		lst.clear();
	}
		
	this->GInfo->SetNumberOfShellCells(&shellParameters);
	
//	VectorOfIntList *v;
//	ListOfInt::iterator ir;
//	v = this->GInfo->GetNumberOfShellCells();
//	
//	for(int i = 0; i < v->size(); i++)		
//	{
//		lst.clear();
//		lst = v->at(i);
//		ir = lst.begin();
//		cout << "i: " << i <<endl;
//		while(ir != lst.end())
//		{
//			cout << "   ::::::" << *ir <<endl;
//			ir++;
//		}
//	}
	
	this->ReadLine(line); // To take the blank line at the end of the tag

	return 1;
}

//----------------------------------------------------------------------------
//Read 3D COUPLING PARAMETERS
int vtkHMGeneralInfoReader::Read3DCapsElements()
{
	//BTX
	typedef std::list<int> ListOfInt; 
	typedef std::vector<ListOfInt> VectorOfIntList;
	//ETX
	
	char line[256];

	vtkDebugMacro(<< "Reading GeneralInfo file 3D COUPLING PARAMETERS");
	
	//read number of caps and their number of cells
	
	if (!this->ReadLine(line))
	{
		vtkErrorMacro(<<"Premature EOF reading 3D COUPLING PARAMETERS line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	if ( strncmp ("*3D COUPLING PARAMETERS", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
			
	vtkHMGeneralInfo::VectorOfIntList couplingParameters;
	vtkHMGeneralInfo::ListOfInt lst;
	int temp, var;
	for(int i=0; i < this->GInfo->GetNumberOf3DObjects(); i++)
	{
		if ( !this->Read( &temp ) )
			{
			vtkErrorMacro(<<"Can't read number of COUPLING PARAMETERS!" << " for file: " 
							<< (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
			}
		lst.push_back(temp);
		
		for ( int j = 0; j < temp; j++ )
		{
			if ( !this->Read( &var ) )
				{
				vtkErrorMacro(<<"Can't read number of SHELL PARAMETERS!" << " for file: " 
								<< (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
				}
			lst.push_back(var);
		}
		couplingParameters.push_back(lst);
		lst.clear();
	}
		
	this->GInfo->SetNumberOfCapsCells(&couplingParameters);
	this->ReadLine(line); // To take the blank line at the end of the tag
	
	return 1;
}

//----------------------------------------------------------------------------
//Read SUBSTEPS NUMBER
int vtkHMGeneralInfoReader::ReadNumberOfSubSteps()
{
	char line[256];

	vtkDebugMacro(<< "Reading GeneralInfo file Number Of SubSteps");

	if (!this->ReadLine(line))
	{
		vtkErrorMacro(<<"Premature EOF reading first line! " << " for file: " 
                  << (this->FileName?this->FileName:"(Null FileName)"));
		this->SetErrorCode( vtkErrorCode::PrematureEndOfFileError );
		return 0;
	}
	if ( strncmp ("*SUBSTEPS NUMBER", line, 20) )
	{
		vtkErrorMacro(<< "Unrecognized file type: "<< line << " for file: " 
		                  << (this->FileName?this->FileName:"(Null FileName)"));
		    
		this->SetErrorCode( vtkErrorCode::UnrecognizedFileTypeError );
		return 0;
	}
	int NumberOfSubSteps;
	if(!this->Read(&NumberOfSubSteps))
	{
		vtkErrorMacro(<<"Can't read number of SubSteps!" << " for file: " 
						<< (this->FileName?this->FileName:"(Null FileName)"));
		return 0;
	}
	this->GInfo->SetNumberOfSubSteps(NumberOfSubSteps);
	this->ReadLine(line); // To take the blank line at the end of tag
	
	return 1;
}

//----------------------------------------------------------------------------
vtkHMGeneralInfo *vtkHMGeneralInfoReader::GetGeneralInfo()
{
	return this->GInfo;
}

//----------------------------------------------------------------------------
void vtkHMGeneralInfoReader::SetGeneralInfo(vtkHMGeneralInfo *m)
{
	this->GInfo = m;
}
