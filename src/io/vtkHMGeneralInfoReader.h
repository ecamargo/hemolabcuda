#ifndef VTKHMGENERALINFOREADER_H_
#define VTKHMGENERALINFOREADER_H_

#include "vtkPoints.h"
#include "vtkDataReader.h"

class vtkHMGeneralInfo;

#include <iostream>
	
class VTK_EXPORT vtkHMGeneralInfoReader : public vtkDataReader
{
public:
	static vtkHMGeneralInfoReader *New();
	vtkTypeRevisionMacro(vtkHMGeneralInfoReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Open a data file. Returns zero if error.
	int OpenFile();
		
	// Description:
	// Close a data file. 
	void CloseFile();
	
	// Description:
	// Read the header of a GeneralInfo data file. Returns 0 if error.	
	int	ReadHeader();
		
	// Description:
	// Read the 3D Structures (number of 3D objects) of a GeneralInfo data file. Returns 0 if error.
	int Read3DStructures();
	
	// Description:
	// Read the 3D Volume Elements (number of cells) of a GeneralInfo data file. Returns 0 if error.	
	int Read3DVolumeElements();
	
	// Description:
	// Read the 3D Shell Elements (number of cells) of a GeneralInfo data file. Returns 0 if error.	
	int Read3DShellParameters();
	
	// Description:
	// Read the 3D Caps Elements (number of caps and cells) of a GeneralInfo data file. Returns 0 if error.	
	int Read3DCapsElements();
	
	// Description:
	// Read the number of SubSteps of a GeneralInfo data file. Returns 0 if error.
	int ReadNumberOfSubSteps();
	
	// Description:
	// Receive full path of files and calls the other functions for read all file ModelInfo.hml13d.
	int ReadGeneralInfoFile(char *dir);
		
	vtkHMGeneralInfo *GetGeneralInfo();
	void SetGeneralInfo(vtkHMGeneralInfo *GI);

protected:
	vtkHMGeneralInfoReader();
	~vtkHMGeneralInfoReader();
	
	vtkHMGeneralInfo *GInfo;
	
}; //End class

#endif /*VTKHMGENERALINFOREADER_H_*/
