/*
 * $Id:$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMMeshDataReader.cpp,v $
  Author: 	 Jan Palach

=========================================================================*/

#include "vtkHMMeshDataReader.h"
#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"
#include "vtkMesh3d.h"
#include "vtkParamMesh.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkFieldData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkUnstructuredGrid.h"

#include <fstream>
#include <string>
#include <sys/stat.h>

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>

#ifdef read
#undef read
#endif

vtkCxxRevisionMacro(vtkHMMeshDataReader, "$Revision: 1.28 $");
vtkStandardNewMacro(vtkHMMeshDataReader);

vtkHMMeshDataReader::vtkHMMeshDataReader()
{
	this->readMeshData = NULL;
	outputType = 0; //The default output is a surface.
}

vtkHMMeshDataReader::~vtkHMMeshDataReader()
{
	if(this->readMeshData) this->readMeshData->Delete();
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMMeshDataReader::GetOutput()
{
	return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMMeshDataReader::GetOutput(int idx)
{
  return vtkMeshData::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHMMeshDataReader::SetOutput(vtkMeshData *output)
{
	this->GetExecutive()->SetOutputData(0, output);
}

//----------------------------------------------------------------------------
int vtkHMMeshDataReader::ReadSurface()
{
	return this->readMeshData->Read(this->FileName);			
}

//----------------------------------------------------------------------------
void vtkHMMeshDataReader::ReadVolume()
{
	int numPts=0, numCells=0;
  long NumberOfPoints = 0;
  long NumberOfTetra = 0;
  vtkCellArray *cells = NULL;
	vtkPoints *points = NULL;
  int *types=NULL;
  char str[255];
	
	// Final da leitura de superfície------------------------------------------
	vtkUnstructuredGrid *grid = this->readMeshData->GetVolume();
	
	// Iniciando a leitura do volume.
  vtkDebugMacro(<<"Reading Volume File...");
	
	// Leitura dos dados de volume.
	if (!this->OpenVTKFile())
    return;
    
  // Reading "*Coordinates" String
  while(strcmp(str,"*VOLUME_COORDINATES"))
		this->ReadLine(str);
		
	float x, y, z;
	int index;	
	vtkDebugMacro(<<"Reading Volume Points...");
	this->Read(&NumberOfPoints);

	points	= vtkPoints::New();
	for ( int i=0 ; i < NumberOfPoints; i++ )
		{
		this->Read(&index);
		if ( !this->Read(&x) ||	!this->Read(&y) || !this->Read(&z) )
			{
			vtkErrorMacro(<<"Cannot read points data in line " << i+1 << " of field 'points' for file: " << (this->FileName?this->FileName:"(Null FileName)"));
			return;
			}		
		points->InsertNextPoint(x, y, z);				
		}
	grid->SetPoints(points);
  points->Delete();
   
  // reading "*Element Groups" String
	while(strcmp(str,"*ELEMENT_VOLUME_GROUPS"))
		this->ReadLine(str);

	int tetraIndex[4];
	int numgroups;          // numero de grupos
	int totelem, aux, elem;
	this->Read(&numgroups);
	
	totelem =  0;

	// Verifica o número de grupos do volume
	for (int i = 0; i < numgroups; i++)
		{
		this->Read(&aux);
		this->Read(&elem);
		this->ReadString(str);
    totelem += elem;
		}
	NumberOfTetra = totelem;
	
	numgroups = 4;

	//reading "*Incidente" String
	cells =  vtkCellArray::New();
	// reading "*Incidente" String
	this->ReadString(str);

	vtkDebugMacro(<<"Reading Volume Tetraedra Data...");

	types = new int[ NumberOfTetra ];	
	
	for ( int i=0; i < NumberOfTetra; i++ )
		{
		for ( int j=0; j<4; j++ )
			{
			if ( !this->Read( &tetraIndex[j] ) )
				{
				vtkErrorMacro(<<"Cannot read indices " << j+1 << " in line " << i+1 << " of field 'INCIDENCE' for file: " << (this->FileName?this->FileName:"(Null FileName)"));
				return;
				}
			// Arquivos surf começam de 1 e não de 0. Temos que decrementar em uma unidade
			tetraIndex[j]--;
			}
		types[i] = 10;		
		cells->InsertNextCell( 4, tetraIndex );
		}
	grid->SetCells(types, cells );	// 10 é para tetraedros

  delete []types;
  cells->Delete();

  this->CloseVTKFile();
}


int vtkHMMeshDataReader::RequestData(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
	vtkInformation* outInfo = outputVector->GetInformationObject(0);
	
	if(!this->readMeshData) 
		{	
		// Cria novo meshdata
		this->readMeshData = vtkMeshData::New();			
		// Le superfície
		this->ReadSurface();
		// Le Volume
		this->ReadVolume();
		}

	vtkMeshData* meshdata  = vtkMeshData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
  meshdata->CopyStructure(readMeshData);

  if(outputType == 0) meshdata->SetOutputAsSurface();  
  if(outputType == 1) meshdata->SetOutputAsVolume();
    
  cout << *meshdata << endl;
  return 1;
}


void vtkHMMeshDataReader::SetOutputType(int outputSelected)
{
  outputType = outputSelected;
  this->Modified();
};

//----------------------------------------------------------------------------
int vtkHMMeshDataReader::FillOutputPortInformation(int, vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
	return 1;
}

void vtkHMMeshDataReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}
