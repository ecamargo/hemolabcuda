/*
 * $Id:$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMMeshDataReader.h,v $
  Author: 	 Jan Palach, Rodrigo

=========================================================================*/
// .NAME vtkHMMeshDataReader - Read .meshData files
// .SECTION Description
// Read meshData files

#ifndef __vtkUnstructuredGridReader_h
#define __vtkUnstructuredGridReader_h

#include "vtkDataReader.h"
#include "vtkUnstructuredGrid.h"

class vtkPolyData;
class vtkPoints;
class vtkCellArray;
class vtkMeshData;

class VTK_EXPORT vtkHMMeshDataReader : public vtkDataReader
{
public:
	static vtkHMMeshDataReader *New();
	vtkTypeRevisionMacro(vtkHMMeshDataReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Get the output of this reader.
	vtkMeshData *GetOutput();
	vtkMeshData *GetOutput(int idx);
	void SetOutput(vtkMeshData *output);
	void SetOutputType(int outputSelected);
	
	// Read Surface
	int  ReadSurface();			

	// Read Volume
	void ReadVolume();	
	
protected:
	vtkHMMeshDataReader();
	~vtkHMMeshDataReader();

	virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);
  
	virtual int FillOutputPortInformation(int, vtkInformation*);

	// Used by streaming: The extent of the output being processed
	// by the execute method. Set in the ComputeInputUpdateExtents method.
	int ExecutePiece;
	int ExecuteNumberOfPieces;
	int ExecuteGhostLevel;
	int outputType;

	// MeshData to be readed	
	vtkMeshData *readMeshData;
  
private:
  vtkHMMeshDataReader(const vtkHMMeshDataReader&);  // Not implemented.
  void operator=(const vtkHMMeshDataReader&);  // Not implemented.
};


#endif //__vtkHMMeshDataReader__
