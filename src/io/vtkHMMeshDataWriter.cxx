/*
 * $Id:$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMMeshDataWriter.cpp,v $
  Author: 	 Jan Palach

=========================================================================*/
#include "vtkHMMeshDataWriter.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkIdList.h"
#include "vtkCell.h"

#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"
#include "SurfTriang.h"
#include "dyncoo3d.h"
#include "acdp.h"
#include "acdptype.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHMMeshDataWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHMMeshDataWriter);

void vtkHMMeshDataWriter::WriteData()
{
	FILE *fp;

	// Pega a superfície de input
  vtkMeshData *meshdata = this->GetInput();
    
  if(!meshdata)
		vtkErrorMacro("Unable to get Surface Input");

	// Abre arquivo onde superfície será escrita
	fp = fopen(this->FileName, "w");
  if (!fp)
		vtkErrorMacro("Can't open surface (.mesh) output file");

  //Escrevendo dados de superficie e de volume.
  //Este metodo recebe como parametros, um ponteiro para um arquivo e uma referencia para um tipo vtkMeshData.
  //de onde serao extraidos os dados da superficie e do volume.
  this->Print(fp, meshdata);

	// Fechando arquivo de escrita...
  fclose(fp);	
}


//Metodo para escrita do arquivo .meshData.
void vtkHMMeshDataWriter::Print(FILE *fp, vtkMeshData *meshdata)
{
  int i;
  long NumGroups = 0, NumNodes = 0, nod = 0, totalCoords = 0, nel = 0;
	SurfTriang* first 	= NULL;
	SurfTriang* curr  	= NULL;
	acDynCoord3D* Coord	= NULL;
	acPoint3*  vc				= NULL;

  // Pegando dados da superfície
  vtkSurfaceGen* surfGen = meshdata->GetSurface(); //Trabalhando a referencia vtkSurfGen contida em vtkMeshData.
	if(surfGen)
		{
	  first 		= surfGen->GetFirstTriang(); //Obtendo o primeiro triangulo.
  	curr 			= surfGen->GetCurrTriang(); //Obtendo o triangulo corrente.
	  Coord 		= surfGen->GetCoords(); //Obtendo as coordenadas dos pontos que compoem a malha.
	 	NumGroups = surfGen->GetNumGroups(); //Obtendo o numero de grupos de vtkSurfaceGen.	   
	  if(Coord)
	  	{
		  vc 				= Coord->GetVc();
			NumNodes 	= Coord->GetNumNodes(); //Obtendo o numero de coordenadas de vtkSurfaceGen.		  
	  	}
		}
  
  // Escrevendo informacao de grupos...
  long *nelg = new long[NumGroups];    
  for (i=0; i<NumGroups; i++)
    nelg[i]=0;

  curr=first;
  while (curr)
  {
    nelg[curr->gr]++;
    curr = curr->next;
  }

  //Gravando os elementos da superfície.
  fprintf(fp, "*ELEMENT GROUPS\n %d\n",NumGroups);
  for (i=0; i<NumGroups; i++)
    fprintf(fp, "%d %ld Tri3\n", i+1, nelg[i]);
  delete []nelg;
  
  //Gravando informações a respeito do número de grupos da casca
  fprintf(fp, "*SHELL GROUPS\n %d\n",surfGen->GetNumberOfShells() );
  
  //Escrevendo a conectividade ou incidencia da superficie.
  fprintf(fp,"*INCIDENCE\n");
  for (i=0; i<NumGroups; i++)
  {
    curr = first;
    while (curr)
    {
        if (curr->gr == i)
          fprintf(fp,"%ld %ld %ld\n",curr->n1+1, curr->n2+1, curr->n3+1);
        curr = curr->next;
    }
  }
  
  //Escrevendo as coordenadas da superficie.
  fprintf(fp, "\n*COORDINATES\n %ld\n",NumNodes);
  for (long i = 0; i<NumNodes; i++)
    fprintf(fp, " %ld %lg %lg %lg\n", i+1, vc[i].x, vc[i].y, vc[i].z);
    
  //Escrevendo o volume
  if(Coord)
  	{	
  	acLimit3 vol = Coord->Box();
	  fprintf(fp, "*FRONT_VOLUME\n %lf %lf %lf %lf %lf %lf\n", vol.xmin,vol.xmax,vol.ymin,vol.ymax,vol.zmin,vol.zmax);
	  }

  // Pegando dados do volume
  if( meshdata->GetVolume() )
	  {
		nod = (int) meshdata->GetVolume()->GetNumberOfPoints(); //Obtendo o numero de coordenadas do volume.
	  nel = meshdata->GetVolume()->GetNumberOfCells(); //Obtendo o numero de elementos do volume. 
	  }  

  //Escrevendo as coordenadas do volume.
  double point[3];
  fprintf (fp,"*VOLUME_COORDINATES\n");
  fprintf (fp,"%d\n", nod );
  for (long i = 0; i<nod; i++)
  {	
  	meshdata->GetVolume()->GetPoint(i,point);
    fprintf (fp," %d %.16lf %.16lf %.16lf\n", i+1, point[0], point[1], point[2]);
  }
  
  fprintf(fp, "*ELEMENT_VOLUME_GROUPS\n");
  fprintf (fp," 1\n");
  fprintf (fp," 1 %ld LINEAR_TETRAHEDRUM\n", nel);
  
  //Escreve conectividade (Incidência) do volume.
  vtkIdList *ids;
  fprintf(fp,"*VOLUME_INCIDENCE\n");
  for (int i = 0; i<nel; i++)
		{
		ids = meshdata->GetVolume()->GetCell(i)->GetPointIds();
		fprintf (fp, "%d %d %d %d\n",
			ids->GetId(0)+1, ids->GetId(1)+1, ids->GetId(2)+1, ids->GetId(3)+1);
		}
  
  fprintf (fp,"*END\n");
}





int vtkHMMeshDataWriter::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMeshData");
  return 1;
}

vtkMeshData* vtkHMMeshDataWriter::GetInput()
{
  return vtkMeshData::SafeDownCast(this->Superclass::GetInput());
}

vtkMeshData* vtkHMMeshDataWriter::GetInput(int port)
{
  return vtkMeshData::SafeDownCast(this->Superclass::GetInput(port));
}

void vtkHMMeshDataWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
