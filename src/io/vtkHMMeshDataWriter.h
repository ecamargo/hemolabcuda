/*
 * $Id:$
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMMeshDataWriter.h,v $
  Author: 	 Jan Palach

=========================================================================*/
// .NAME vtkHMMeshDataWriter - Write .meshData files
// .SECTION Description
// Write Volume  and surface data in  one file.

#ifndef __vtkHMMeshDataWriter_h
#define __vtkHMMeshDataWriter_h

#include "vtkInformation.h"
#include "vtkObjectFactory.h"

#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"
#include "SurfTriang.h"
#include "dyncoo3d.h"
#include "acdp.h"
#include "acdptype.h"

#include "vtkDataWriter.h"

class vtkUnstructuredGrid;

class VTK_EXPORT vtkHMMeshDataWriter : public vtkDataWriter
{
public:
  static vtkHMMeshDataWriter *New();
  vtkTypeRevisionMacro(vtkHMMeshDataWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  vtkMeshData* GetInput();
  vtkMeshData* GetInput(int port);

  void  Print(FILE *fp, vtkMeshData *surface);

protected:
  vtkHMMeshDataWriter() {};
  ~vtkHMMeshDataWriter() {};

  void WriteData();

  virtual int FillInputPortInformation(int port, vtkInformation *info);

private:
  vtkHMMeshDataWriter(const vtkHMMeshDataWriter&);  // Not implemented.
  void operator=(const vtkHMMeshDataWriter&);  // Not implemented.
};

#endif

