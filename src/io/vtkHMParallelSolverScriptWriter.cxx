
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkHM1DBasParam.h"


#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

#include "vtkHMParallelSolverScriptWriter.h"

vtkCxxRevisionMacro(vtkHMParallelSolverScriptWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHMParallelSolverScriptWriter);

//----------------------------------------------------------------------------
int vtkHMParallelSolverScriptWriter::WriteParallelSolverScript(vtkHM1DBasParam *BasParam, char *dir)
{
  ostream *fp;
	
	char d[256];
	char aux[500];
	
	strcpy(d, dir);
	strcat(d, "/Submit-altix.job");
	this->SetFileName(d);
	
  vtkDebugMacro(<<"Writing BasParam data...");

  if ( !(fp=this->OpenFile(BasParam)) )
    {
    if (fp)
      {
      if(this->FileName)
        {
        vtkErrorMacro("Ran out of disk space; deleting file: "
                      << this->FileName);
        this->CloseFile(fp);
        unlink(this->FileName);
        }
      else
        {
        this->CloseFile(fp);
        vtkErrorMacro("Could not read memory header. ");
        }
      }
    return 0;
    }
	

	
	*fp << "#PBS -N SolverGP-fem\n";
	*fp << "#PBS -S /bin/bash\n";
	*fp << "#PBS -q prjhemo\n";

	
	sprintf(aux,"#PBS -l ncpus=%d\n", BasParam->GetParallelSolverNumberOfProcessors()) ;
	
	*fp << aux;
	
	
	*fp << "cd SolverGP07/\n";
	*fp << "echo \"Current directory:\"\n";
	*fp << "pwd\n";
	*fp << "#\n";
	*fp << "echo \"Running Navier Stokes code\"\n";
	*fp << "#\n";
	*fp << "cp IniFile000.txt IniFile.txt\n";
	*fp << "cp Param000.txt Param.txt\n";
	*fp << "#\n";
	sprintf(aux,"mpirun -np %d /prj/prjhemo/enzodari/SolverGP07/bin/linux-ia64-opt/SolverGP.x \\\n", BasParam->GetParallelSolverNumberOfProcessors()) ;
	*fp << aux;
	
	*fp << "-mat_partitioning_type parmetis \\\n";
	
	// cgs
	if (BasParam->GetParallelSolverRLS() == 110)
		*fp << "-ksp_type cgs \\\n";
	// gmres
	if (BasParam->GetParallelSolverRLS() == 107)
		{
		sprintf(aux,"-ksp_type gmres -ksp_gmres_restart %d \\\n", BasParam->GetParallelSolverGMRESIterations()) ;	
		*fp << aux;
		
		}

	
	if (BasParam->GetParallelSolverPreConditioning())
		{
		*fp << "-pc_type BJacobi \\\n";	
		}

	sprintf(aux,"-ksp_max_it %d \\\n", BasParam->GetParallelSolverMaxIterations()) ;	
	*fp << aux;
		
	sprintf(aux,"-ksp_rtol %e \\\n", BasParam->GetParallelSolverRelativeConvError()) ;	
	*fp << aux;

	sprintf(aux,"-ksp_atol %e \\\n", BasParam->GetParallelSolverAbsConvError()) ;	
	*fp << aux;

	sprintf(aux,"-ksp_monitor -log_summary < /dev/null>& run.p%d.log\n", BasParam->GetParallelSolverNumberOfProcessors()) ;	
	*fp << aux;

	fp->flush();
  if (fp->fail())
    {
    this->SetErrorCode(vtkErrorCode::OutOfDiskSpaceError);
    return 0;
    }
  
  return 1;
	
}

//----------------------------------------------------------------------------
// Open a Mesh data file. Returns NULL if error.
ostream *vtkHMParallelSolverScriptWriter::OpenFile(vtkDataObject *input)
{
  ostream *fptr;
  
  if ((!this->WriteToOutputString) && ( !this->FileName ))
    {
    vtkErrorMacro(<< "No FileName specified! Can't write!");
    this->SetErrorCode(vtkErrorCode::NoFileNameError);
    return NULL;
    }
  
  vtkDebugMacro(<<"Opening Properties file for writing...");

  if (this->WriteToOutputString)
    {
    // Get rid of any old output string.
    if (this->OutputString)
      {
      delete [] this->OutputString;
      this->OutputString = NULL;
      this->OutputStringLength = 0;
      this->OutputStringAllocatedLength = 0;
      }
    // Allocate the new output string. (Note: this will only work with binary).
    if (input == NULL)
      {
      vtkErrorMacro(<< "No input! Can't write!");
      return NULL;    
      }
    input->Update();
    this->OutputStringAllocatedLength = (int) (500 
      + 1000 * input->GetActualMemorySize());
    this->OutputString = new char[this->OutputStringAllocatedLength];

    fptr = new ostrstream(this->OutputString, 
                          this->OutputStringAllocatedLength);
    }
  else 
    {
    if ( this->FileType == VTK_ASCII )
      {
      fptr = new ofstream(this->FileName, ios::out);
      }
    else
      { 
#ifdef _WIN32
      fptr = new ofstream(this->FileName, ios::out | ios::binary);
#else
      fptr = new ofstream(this->FileName, ios::out);
#endif
      }
    }

  if (fptr->fail())
    {
    vtkErrorMacro(<< "Unable to open file: "<< this->FileName);
    this->SetErrorCode(vtkErrorCode::CannotOpenFileError);
    delete fptr;
    return NULL;
    }

  return fptr;
}

//----------------------------------------------------------------------------
void vtkHMParallelSolverScriptWriter::CloseFile(ostream *fp)
{
  vtkDebugMacro(<<"Closing Properties file\n");
  
  if ( fp != NULL )
    {
    if (this->WriteToOutputString)
      {
      char *tmp;
      ostrstream *ostr = (ostrstream*)(fp);
      this->OutputStringLength = ostr->pcount();

      if (this->OutputStringLength == this->OutputStringAllocatedLength)
        {
        vtkErrorMacro("OutputString was not long enough.");
        }
      // Sanity check.
      tmp = ostr->str();
      if (tmp != this->OutputString)
        {
        vtkErrorMacro("String mismatch");
        }
      this->OutputString = tmp;
      }
    delete fp;
    }

}


//----------------------------------------------------------------------------
//PrintSelf
void vtkHMParallelSolverScriptWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

