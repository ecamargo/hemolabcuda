/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHMParallelSolverScriptWriter.h,v $

=========================================================================*/
// .NAME vtkHMParallelSolverScriptWriter -
// .SECTION Description
//
// .SECTION Caveats
// Binary files written on one system may not be readable on other systems.

#ifndef vtkHMParallelSolverScriptWriter_H_
#define vtkHMParallelSolverScriptWriter_H_


#include "vtkDataWriter.h"
#include "vtkDataObject.h"
class vtkHM1DBasParam;

class VTK_EXPORT vtkHMParallelSolverScriptWriter : public vtkDataWriter
{
public:
  static vtkHMParallelSolverScriptWriter *New();
  vtkTypeRevisionMacro(vtkHMParallelSolverScriptWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
	// Write the Solver parallel script
  int WriteParallelSolverScript(vtkHM1DBasParam *BasParam, char *dir);
  
  // Description:
	// Open a data file. Returns NULL if error.
	ostream *OpenFile(vtkDataObject *input);
		
	// Description:
  // Close a Mesh file.
  void CloseFile(ostream *fp);
  


protected:
  vtkHMParallelSolverScriptWriter() {};
  ~vtkHMParallelSolverScriptWriter() {};



};


#endif 
