/*
 * $Id: vtkHMSurfaceReader.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMSurfaceReader.cpp,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/

#include "vtkHMSurfaceReader.h"
#include "vtkMeshData.h"

#include "vtkCellArray.h"
#include "vtkCharArray.h"
#include "vtkErrorCode.h"
#include "vtkFieldData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkStreamingDemandDrivenPipeline.h"

#include <fstream>
#include <string>
#include <sys/stat.h>

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>

vtkCxxRevisionMacro(vtkHMSurfaceReader, "$Revision: 1.28 $");
vtkStandardNewMacro(vtkHMSurfaceReader);

vtkHMSurfaceReader::vtkHMSurfaceReader()
{
	vtkMeshData *output = vtkMeshData::New();	
	this->SetOutput(output);
	
	// Releasing data for pipeline parallism.
	// Filters will know it is empty. 
	output->ReleaseData();
	output->Delete();
	
	this->ExecutePiece = this->ExecuteNumberOfPieces = 0;
	this->ExecuteGhostLevel = 0;
	
}

vtkHMSurfaceReader::~vtkHMSurfaceReader()
{
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMSurfaceReader::GetOutput()
{
	return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMSurfaceReader::GetOutput(int idx)
{
  return vtkMeshData::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHMSurfaceReader::SetOutput(vtkMeshData *output)
{
	this->GetExecutive()->SetOutputData(0, output);
}


//----------------------------------------------------------------------------
int vtkHMSurfaceReader::RequestUpdateExtent(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  int piece, numPieces, ghostLevel;

  piece = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  numPieces = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
  ghostLevel = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS());
  
  // make sure piece is valid
  if (piece < 0 || piece >= numPieces)
    {
    return 1;
    }
  
  if (ghostLevel < 0)
    {
    return 1;
    }
  
  // Save the piece so execute can use this information.
  this->ExecutePiece = piece;
  this->ExecuteNumberOfPieces = numPieces;
  
  this->ExecuteGhostLevel = ghostLevel;

  return 1;
}

int vtkHMSurfaceReader::RequestData(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
	vtkInformation* outInfo = outputVector->GetInformationObject(0);
	vtkMeshData*		output  = vtkMeshData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkDebugMacro(<<"Reading .sur polygonal data...");
	
	// Passa o nome do arquivo pois a leitura é feita por métodos de vtkMesh3d
	// Este método atualiza estruturas internas utilizadas pelo trisurf
	output->Read(this->FileName);

	// Atualiza UnstructuredGrid interno com a superfície lida
	output->SetOutputAsSurface();
	return 1;
}

//----------------------------------------------------------------------------
int vtkHMSurfaceReader::FillOutputPortInformation(int, vtkInformation* info)
{
	info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
	return 1;
}


void vtkHMSurfaceReader::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
}

