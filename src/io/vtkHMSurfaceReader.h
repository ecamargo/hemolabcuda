/*
 * $Id: vtkHMSurfaceReader.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMSurfaceReader.h,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMSurfaceReader - Read .suf files
// .SECTION Description
// Read surface files

#ifndef __VTKHMSURFACEREADER__
#define __VTKHMSURFACEREADER__

#include "vtkDataReader.h"

class vtkPolyData;
class vtkPoints;
class vtkCellArray;
class vtkMeshData;

class VTK_EXPORT vtkHMSurfaceReader : public vtkDataReader
{
public:
	static vtkHMSurfaceReader *New();
	vtkTypeRevisionMacro(vtkHMSurfaceReader,vtkDataReader);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Get the output of this reader.
	vtkMeshData *GetOutput();
	vtkMeshData *GetOutput(int idx);
	void SetOutput(vtkMeshData *output);
	
protected:
	vtkHMSurfaceReader();
	~vtkHMSurfaceReader();

	virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);

	// Update extent of PolyData is specified in pieces.  
	// Since all DataObjects should be able to set UpdateExent as pieces,
	// just copy output->UpdateExtent  all Inputs.
	virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **,
                                  vtkInformationVector *);
  
	virtual int FillOutputPortInformation(int, vtkInformation*);

	// Used by streaming: The extent of the output being processed
	// by the execute method. Set in the ComputeInputUpdateExtents method.
	int ExecutePiece;
	int ExecuteNumberOfPieces;
	int ExecuteGhostLevel;
  
private:
  vtkHMSurfaceReader(const vtkHMSurfaceReader&);  // Not implemented.
  void operator=(const vtkHMSurfaceReader&);  // Not implemented.
};


#endif //__vtkHMSurfaceReader__
