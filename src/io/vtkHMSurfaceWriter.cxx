/*
 * $Id: vtkHMSurfaceWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMSurfaceWriter.cpp,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
#include "vtkHMSurfaceWriter.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"

#include "vtkMeshData.h"
#include "vtkSurfaceGen.h"

#if !defined(_WIN32) || defined(__CYGWIN__)
# include <unistd.h> /* unlink */
#else
# include <io.h> /* unlink */
#endif

vtkCxxRevisionMacro(vtkHMSurfaceWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHMSurfaceWriter);

void vtkHMSurfaceWriter::WriteData()
{
	FILE *fp;

	// Pega a superfície de input
  vtkMeshData *surface = this->GetInput();
  if(!surface)
		vtkErrorMacro("Unable to get Surface Input");

	// Abre arquivo onde superfície será escrita
	fp = fopen(this->FileName, "w");
  if (!fp)
		vtkErrorMacro("Can't open surface (.sur) output file");

	// Pega o SurfaceGen da superfície e chama método apropriado de escrita
  surface->GetSurface()->Print(fp);
  fclose(fp);	
}

int vtkHMSurfaceWriter::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMeshData");
  return 1;
}

vtkMeshData* vtkHMSurfaceWriter::GetInput()
{
  return vtkMeshData::SafeDownCast(this->Superclass::GetInput());
}

vtkMeshData* vtkHMSurfaceWriter::GetInput(int port)
{
  return vtkMeshData::SafeDownCast(this->Superclass::GetInput(port));
}

void vtkHMSurfaceWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
