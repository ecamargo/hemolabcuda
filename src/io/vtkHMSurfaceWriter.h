/*
 * $Id: vtkHMSurfaceWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMSurfaceWriter.h,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMSurfaceWriter - Write .suf files
// .SECTION Description
// Write surface files

#ifndef __vtkHMSurfaceWriter_h
#define __vtkHMSurfaceWriter_h

#include "vtkDataWriter.h"

class vtkMeshData;

class VTK_EXPORT vtkHMSurfaceWriter : public vtkDataWriter
{
public:
  static vtkHMSurfaceWriter *New();
  vtkTypeRevisionMacro(vtkHMSurfaceWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  vtkMeshData* GetInput();
  vtkMeshData* GetInput(int port);

protected:
  vtkHMSurfaceWriter() {};
  ~vtkHMSurfaceWriter() {};

  void WriteData();

  virtual int FillInputPortInformation(int port, vtkInformation *info);

private:
  vtkHMSurfaceWriter(const vtkHMSurfaceWriter&);  // Not implemented.
  void operator=(const vtkHMSurfaceWriter&);  // Not implemented.
};

#endif

