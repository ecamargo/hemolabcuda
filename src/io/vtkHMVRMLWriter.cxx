/*
 * $Id: vtkHMVRMLWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVRMLWriter.cpp,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkDataSet.h"
#include "vtkHMVRMLWriter.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkDataSetMapper.h"
#include "vtkLODActor.h"
#include "vtkVRMLExporter.h"
#include "vtkProperty.h"
#include "vtkPointData.h"
#include "vtkLookupTable.h"
#include "vtkDataArray.h"
#include "vtkDoubleArray.h"

vtkCxxRevisionMacro(vtkHMVRMLWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHMVRMLWriter);

void vtkHMVRMLWriter::SetLookupTableColors(vtkLookupTable *lut)
{
	int i;
	double size = 64.0;
	for(i = 0; i < int(size); i++)
		lut->SetTableValue(i, 	 		0.0, double(i)/size, 1);
	for(i = 0; i < int(size); i++)
		lut->SetTableValue(i+ 1*size, 0.0, 1.0, 1 - double(i)/size);
	for(i = 0; i < int(size); i++)
		lut->SetTableValue(i+ 2*size, double(i)/size, 1.0, 0.0);
	for(i = 0; i < int(size); i++)
		lut->SetTableValue(i+ 3*size, 1.0, 1 - double(i)/size, 0.0);
}

void vtkHMVRMLWriter::WriteData()
{ 
	// Pega a superfície de input
  	vtkDataSet *grid = this->GetInput();
  	
  	int numComponents = grid->GetPointData()->GetArray(0)->GetNumberOfComponents();  	
		
  	// Se for um campo escalar, tem apenas um componente 
  	if(numComponents == 1)
	{
		grid->GetPointData()->SetScalars(grid->GetPointData()->GetArray(0));  	
	}
	else
	{ 
		// Se tiver mais de um componente, calcula a magnitude
		vtkDoubleArray *array 	 = vtkDoubleArray::New();
		for(int i = 0; i < grid->GetPointData()->GetNumberOfTuples(); i++)
		{
			double * tuple = grid->GetPointData()->GetArray(0)->GetTuple(i);
			double * coord = grid->GetPoint(i);
			double magnitude = sqrt( pow(tuple[0],2) + pow(tuple[1],2) + pow(tuple[2],2));			
			array->InsertNextTuple1(magnitude);
		}

		grid->GetPointData()->SetScalars( (vtkDataArray *) array);
		array->Delete();
	}

  	double range[2];
  	grid->GetPointData()->GetScalars()->GetRange(range);

  	vtkLookupTable *lut = vtkLookupTable::New();
  		lut->Allocate();  		
  		lut->SetVectorModeToMagnitude(); 
  		lut->SetNumberOfTableValues(256);
  		lut->SetTableRange(0, 255);
  	this->SetLookupTableColors(lut);  		
  		lut->Build();

	vtkDataSetMapper *camMapper = vtkDataSetMapper::New();
		camMapper->SetInput(grid);
		camMapper->SetLookupTable(lut);				
		camMapper->SetScalarModeToUsePointData();	
		camMapper->ScalarVisibilityOn();
		camMapper->SetScalarRange(range[0], range[1] );
			    
	vtkLODActor *camActor = vtkLODActor::New();
		camActor->SetMapper(camMapper);

	vtkRenderer *ren1 = vtkRenderer::New();
		ren1->AutomaticLightCreationOff();
		ren1->AddActor(camActor);
	
	vtkRenderWindow *renWin = vtkRenderWindow::New();
		renWin->AddRenderer(ren1);
		renWin->SetWindowName("Generating VRML File...");
		renWin->SetSize(256, 128);
		renWin->Render();
	
	vtkVRMLExporter *vrml = vtkVRMLExporter::New();
		vrml->SetInput(renWin);
		vrml->SetFileName(this->FileName);		
		vrml->Write();

	vrml->Delete();
	renWin->Delete();
	ren1->Delete();
	camActor->Delete();
	camMapper->Delete();			
	lut->Delete();	
}


int vtkHMVRMLWriter::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  return 1;
}

vtkDataSet* vtkHMVRMLWriter::GetInput()
{
  return vtkDataSet::SafeDownCast(this->Superclass::GetInput());
}

vtkDataSet* vtkHMVRMLWriter::GetInput(int port)
{
  return vtkDataSet::SafeDownCast(this->Superclass::GetInput(port));
}

void vtkHMVRMLWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
