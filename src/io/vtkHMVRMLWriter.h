/*
 * $Id: vtkHMVRMLWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVRMLWriter.h,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMVRMLWriter - Write .vrml files
// .SECTION Description
// Write VRML Files to be visualize in the CAVE environment

#ifndef __vtkHMVRMLWriter_h
#define __vtkHMVRMLWriter_h

#include "vtkDataWriter.h"
#include "vtkLookupTable.h"

class vtkDataSet;

class VTK_EXPORT vtkHMVRMLWriter : public vtkDataWriter
{
public:
  static vtkHMVRMLWriter *New();
  vtkTypeRevisionMacro(vtkHMVRMLWriter,vtkDataWriter)
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  vtkDataSet* GetInput();
  vtkDataSet* GetInput(int port);
  void SetLookupTableColors(vtkLookupTable *);
  
protected:
  vtkHMVRMLWriter() {};
  ~vtkHMVRMLWriter() {};
  
  void WriteData();

  virtual int FillInputPortInformation(int port, vtkInformation *info);

private:
  vtkHMVRMLWriter(const vtkHMVRMLWriter&);  // Not implemented.
  void operator=(const vtkHMVRMLWriter&);  // Not implemented.
};

#endif

