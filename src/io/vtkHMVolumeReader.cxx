/*
 * $Id: vtkHMVolumeReader.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVolumeReader.cpp,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
#include "vtkHMVolumeReader.h"

#include "vtkByteSwap.h"
#include "vtkCharArray.h"
#include "vtkCellArray.h"
#include "vtkFieldData.h"
#include "vtkPoints.h"
#include "vtkInformation.h"
#include "vtkErrorCode.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMeshData.h"

vtkCxxRevisionMacro(vtkHMVolumeReader, "$Revision: 1.70 $");
vtkStandardNewMacro(vtkHMVolumeReader);

#ifdef read
#undef read
#endif

vtkHMVolumeReader::vtkHMVolumeReader()
{
  vtkMeshData *output = vtkMeshData::New();
  this->SetOutput(output);
  // Releasing data for pipeline parallism.
  // Filters will know it is empty. 
  output->ReleaseData();
  output->Delete();
}

vtkHMVolumeReader::~vtkHMVolumeReader()
{
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMVolumeReader::GetOutput()
{
  return this->GetOutput(0);
}

//----------------------------------------------------------------------------
vtkMeshData* vtkHMVolumeReader::GetOutput(int idx)
{
  return vtkMeshData::SafeDownCast(this->GetOutputDataObject(idx));
}

//----------------------------------------------------------------------------
void vtkHMVolumeReader::SetOutput(vtkMeshData *output)
{
  this->GetExecutive()->SetOutputData(0, output);
}

//----------------------------------------------------------------------------
// I do not think this should be here, but I do not want to remove it now.
int vtkHMVolumeReader::RequestUpdateExtent(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  int piece, numPieces;

  piece = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER());
  numPieces = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES());
    
  // make sure piece is valid
  if (piece < 0 || piece >= numPieces)
    {
    return 1;
    }

  return 1;
}

int vtkHMVolumeReader::RequestData(
  vtkInformation *,
  vtkInformationVector **,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  int numPts=0, numCells=0;
  int NumberOfPoints = 0;
  int NumberOfTetra = 0;
  vtkCellArray *cells=NULL;
  int *types=NULL;
	char str[255];  
  vtkMeshData *meshdata = vtkMeshData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	vtkUnstructuredGrid *output = vtkUnstructuredGrid::New();
	
  // All of the data in the first piece.
  if (outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER()) > 0)
    {
    return 1;
    }
  
  vtkDebugMacro(<<"Reading Volume File...");

  if (!this->OpenVTKFile())
    {
    return 1;
    }

	// reading "*Coordinates" String
	this->ReadString(str);	
	float x, y, z;
	int index;	

	vtkDebugMacro(<<"Reading Volume Points...");
	this->Read(&NumberOfPoints);

	vtkPoints *points	= vtkPoints::New();
	for ( int i=0 ; i < NumberOfPoints; i++ )
	{
		this->Read(&index);
		if ( !this->Read(&x) ||	!this->Read(&y) || !this->Read(&z) )
		{
			vtkErrorMacro(<<"Cannot read points data in line " << i+1 << " of field 'points' for file: " << (this->FileName?this->FileName:"(Null FileName)"));
			return 0;
		}
	
		points->InsertNextPoint(x, y, z);				
	}
	output->SetPoints(points);
  points->Delete();


	// reading "*Element Groups" String
	while(strcmp(str,"*ELEMENT GROUPS"))
		this->ReadLine(str);
	int tetraIndex[4];
	int numgroups;          // numero de grupos
	int totelem, aux, elem;
	
	this->Read(&numgroups);
	totelem =  0;
	
	// Verifica o número de grupos do volume
	for (int i = 0; i < numgroups; i++)
		{
		this->Read(&aux);
		this->Read(&elem);
		this->ReadString(str);
    totelem += elem;
		}
	NumberOfTetra = totelem;
	
	cells =  vtkCellArray::New();

	// reading "*Incidente" String
	this->ReadString(str);

	vtkDebugMacro(<<"Reading Volume Tetraedra Data...");

	types = new int[ NumberOfTetra ];	
	
	for ( int i=0; i < NumberOfTetra; i++ )
	{
		for ( int j=0; j<4; j++ )
		{
			if ( !this->Read( &tetraIndex[j] ) )
			{
				vtkErrorMacro(<<"Cannot read indices " << j+1 << " in line " << i+1 << " of field 'INCIDENCE' for file: " << (this->FileName?this->FileName:"(Null FileName)"));
				return 0;
			}
			// Arquivos surf começam de 1 e não de 0. Temos que decrementar em uma unidade
			tetraIndex[j]--;
		}
		types[i] = 10;		
		cells->InsertNextCell( 4, tetraIndex );
	}
	output->SetCells(types, cells );	// 10 é para tetraedros

  if (types) delete [] types;
  if (cells) cells->Delete();

  this->CloseVTKFile (); 
  
  // Preenche o objecto MeshData com os dados lidos pelo grid não estruturado
  meshdata->GetVolume()->CopyStructure( output );

	// Set the volume information to be visualized
	meshdata->SetOutputAsVolume();
	
	// Remove objeto temporário
	output->Delete();  

	cout << *meshdata << endl;
 
  return 1;
}


//----------------------------------------------------------------------------
int vtkHMVolumeReader::FillOutputPortInformation(int,vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkMeshData");
  return 1;
}

void vtkHMVolumeReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
