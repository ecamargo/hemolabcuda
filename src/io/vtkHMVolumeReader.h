/*
 * $Id: vtkHMVolumeReader.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVolumeReader.h,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/


#ifndef __vtkUnstructuredGridReader_h
#define __vtkUnstructuredGridReader_h

#include "vtkDataReader.h"

class vtkMeshData;
class vtkPoints;
class vtkCellArray;

class VTK_EXPORT vtkHMVolumeReader : public vtkDataReader
{
public:
  static vtkHMVolumeReader *New();
  vtkTypeRevisionMacro(vtkHMVolumeReader,vtkDataReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the output of this reader.
  vtkMeshData *GetOutput();
  vtkMeshData *GetOutput(int idx);
  void SetOutput(vtkMeshData *output);
  
protected:
  vtkHMVolumeReader();
  ~vtkHMVolumeReader();

  virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);

  // Since the Outputs[0] has the same UpdateExtent format
  // as the generic DataObject we can copy the UpdateExtent
  // as a default behavior.
  virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **,
                                  vtkInformationVector *);

  virtual int FillOutputPortInformation(int, vtkInformation*);
    
private:
  vtkHMVolumeReader(const vtkHMVolumeReader&);  // Not implemented.
  void operator=(const vtkHMVolumeReader&);  // Not implemented.
};

#endif
