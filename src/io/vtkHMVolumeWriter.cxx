/*
 * $Id: vtkHMVolumeWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVolumeWriter.cpp,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
#include "vtkHMVolumeWriter.h"
#include "vtkSurfaceGen.h"

#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkUnstructuredGrid.h"
#include "vtkCell.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkHMVolumeWriter, "$Revision: 1.26 $");
vtkStandardNewMacro(vtkHMVolumeWriter);

void vtkHMVolumeWriter::WriteData()
{
	FILE *fp;

	// Pega a superfície de input
  vtkUnstructuredGrid *volume = this->GetInput();
  if(!volume)
		vtkErrorMacro("Unable to get Surface Input");

	// Abre arquivo onde superfície será escrita
	fp = fopen(this->FileName, "w");
  if (!fp)
		vtkErrorMacro("Can't open surface (.sur) output file");

	// Escreve coordenadas em arquivo	
  fprintf (fp,"*COORDINATES\n");
  int nod = (int) volume->GetNumberOfPoints();
  fprintf (fp,"%d\n", nod );
  double point[3];
  for (int i = 0; i<nod; i++)
  	{	
  	volume->GetPoint(i,point);
    fprintf (fp,"%d %.16lf %.16lf %.16lf\n", i+1, point[0], point[1], point[2]);
  	}
  
  // Escreve conectividade (Incidência) em arquivo
  int nel = volume->GetNumberOfCells();
  fprintf (fp,"*ELEMENT GROUPS\n");
  fprintf (fp," 1\n");
  fprintf (fp," 1 %ld LINEAR_TETRAHEDRUM\n", nel);
  fprintf (fp,"*INCIDENCE\n");
  vtkIdList *ids;
  for (int i = 0; i<nel; i++)
		{
		ids = volume->GetCell(i)->GetPointIds();
		fprintf (fp, "%d %d %d %d\n",
			ids->GetId(0)+1, ids->GetId(1)+1, ids->GetId(2)+1, ids->GetId(3)+1);
		}
  fprintf (fp,"*END\n");

	// Finaliza arquivo
  fclose(fp);	
}

int vtkHMVolumeWriter::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMeshData");
  return 1;
}

vtkUnstructuredGrid* vtkHMVolumeWriter::GetInput()
{
  return vtkUnstructuredGrid::SafeDownCast(this->Superclass::GetInput());
}

vtkUnstructuredGrid* vtkHMVolumeWriter::GetInput(int port)
{
  return vtkUnstructuredGrid::SafeDownCast(this->Superclass::GetInput(port));
}

void vtkHMVolumeWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
