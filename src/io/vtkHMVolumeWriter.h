/*
 * $Id: vtkHMVolumeWriter.h 690 2006-07-19 17:27:26Z rodrigo $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkHMVolumeWriter.h,v $
  Author: 	 Rodrigo L. S. Silva

=========================================================================*/
// .NAME vtkHMVolumeWriter - Write .vwm files
// .SECTION Description
// Write Volume files

#ifndef __vtkHMVolumeWriter_h
#define __vtkHMVolumeWriter_h

#include "vtkDataWriter.h"

class vtkUnstructuredGrid;

class VTK_EXPORT vtkHMVolumeWriter : public vtkDataWriter
{
public:
  static vtkHMVolumeWriter *New();
  vtkTypeRevisionMacro(vtkHMVolumeWriter,vtkDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the input to this writer.
  vtkUnstructuredGrid* GetInput();
  vtkUnstructuredGrid* GetInput(int port);

protected:
  vtkHMVolumeWriter() {};
  ~vtkHMVolumeWriter() {};

  void WriteData();

  virtual int FillInputPortInformation(int port, vtkInformation *info);

private:
  vtkHMVolumeWriter(const vtkHMVolumeWriter&);  // Not implemented.
  void operator=(const vtkHMVolumeWriter&);  // Not implemented.
};

#endif

