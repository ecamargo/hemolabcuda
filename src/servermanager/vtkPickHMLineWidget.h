
#ifndef __vtkPickHMLineWidget_h
#define __vtkPickHMLineWidget_h

#include "vtkHMStraightModelWidget.h"
class vtkSMRenderModuleProxy;


class VTK_EXPORT vtkPickHMLineWidget : public vtkHMStraightModelWidget
{
public:
  static vtkPickHMLineWidget* New();
  vtkTypeRevisionMacro(vtkPickHMLineWidget, vtkHMStraightModelWidget);

  void PrintSelf(ostream& os, vtkIndent indent);
    
  // Description:
  // The render module is for picking.
  void SetRenderModuleProxy(vtkSMRenderModuleProxy* rm){ this->RenderModuleProxy = rm; }
  vtkGetObjectMacro(RenderModuleProxy, vtkSMRenderModuleProxy);

  // Description:
  // We have to look for key press events too.
  virtual void SetEnabled(int);

protected:
  vtkPickHMLineWidget();
  ~vtkPickHMLineWidget();

  // For picking.  Use a proxy in the future.
  vtkSMRenderModuleProxy* RenderModuleProxy;

  virtual void OnChar();

  // Handles the events
  static void ProcessEvents(vtkObject* object, 
                            unsigned long event,
                            void* clientdata, 
                            void* calldata);

  // For toggling the pick between the two end points.
  int LastPicked;

private:
  vtkPickHMLineWidget(const vtkPickHMLineWidget&); // Not implemented
  void operator=(const vtkPickHMLineWidget&); // Not implemented

  int LastY;
};

#endif
