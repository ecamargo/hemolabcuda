#include "vtkPickHMPointWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkCellPicker.h"
#include "vtkCommand.h"
#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCamera.h"
#include "vtkSMRenderModuleProxy.h"

vtkStandardNewMacro(vtkPickHMPointWidget);
vtkCxxRevisionMacro(vtkPickHMPointWidget, "$Revision: 1.2 $");

//----------------------------------------------------------------------------
vtkPickHMPointWidget::vtkPickHMPointWidget()
{
	this->EventCallbackCommand->SetCallback(vtkPickHMPointWidget::ProcessEvents);
  this->RenderModuleProxy = 0;
}

//----------------------------------------------------------------------------
vtkPickHMPointWidget::~vtkPickHMPointWidget()
{
  this->SetRenderModuleProxy(NULL);
}

//----------------------------------------------------------------------------
void vtkPickHMPointWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "RenderModuleProxy: " << this->RenderModuleProxy << endl;
}


//----------------------------------------------------------------------------
void vtkPickHMPointWidget::SetEnabled(int enabling)
{
  if ( ! this->Interactor )
    {
    vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
    return;
    }

  if ( enabling && ! this->Enabled)
    {
    // listen for the following events
    vtkRenderWindowInteractor *i = this->Interactor;
    i->AddObserver(vtkCommand::KeyPressEvent, 
                   this->EventCallbackCommand, this->Priority);
    }

  this->Superclass::SetEnabled(enabling);
}

//----------------------------------------------------------------------------
void vtkPickHMPointWidget::ProcessEvents(vtkObject* object, 
                                       unsigned long event,
                                       void* clientdata, 
                                       void* calldata)
{
  vtkInteractorObserver* self 
    = reinterpret_cast<vtkInteractorObserver *>( clientdata );

  vtkHMPointWidget::ProcessEvents(object, event, clientdata, calldata);

  //look for char and delete events
  switch(event)
    {
    case vtkCommand::CharEvent:
      self->OnChar();
      break;
    }
}

//----------------------------------------------------------------------------
void vtkPickHMPointWidget::OnChar()
{
  if (this->Interactor->GetKeyCode() == 'p' ||
      this->Interactor->GetKeyCode() == 'P' )
    {
    if (this->RenderModuleProxy == NULL)
      {
      vtkErrorMacro("Cannot pick without a render module.");
      return;
      }
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];
    float z = this->RenderModuleProxy->GetZBufferValue(X, Y);
    double pt[4];
    this->ComputeDisplayToWorld(double(X),double(Y),double(z),pt);
    this->Cursor3D->SetFocalPoint(pt);
    this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
    return;
    }
}



