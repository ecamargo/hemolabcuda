
#ifndef __vtkPickHMPointWidget_h
#define __vtkPickHMPointWidget_h

#include "vtkHMPointWidget.h"

class vtkSMRenderModuleProxy;


class VTK_EXPORT vtkPickHMPointWidget : public vtkHMPointWidget
{
public:
  static vtkPickHMPointWidget* New();
  vtkTypeRevisionMacro(vtkPickHMPointWidget, vtkHMPointWidget);

  void PrintSelf(ostream& os, vtkIndent indent);
    
  // Description:
  // The render module is for picking.
  void SetRenderModuleProxy(vtkSMRenderModuleProxy* rm)
    { this->RenderModuleProxy = rm; }
  
  vtkGetObjectMacro(RenderModuleProxy, vtkSMRenderModuleProxy);

  // Description:
  // We have to look for key press events too.
  virtual void SetEnabled(int);

protected:
  vtkPickHMPointWidget();
  ~vtkPickHMPointWidget();

  // For picking.  Use a proxy in the future.
  vtkSMRenderModuleProxy* RenderModuleProxy;

  virtual void OnChar();

  // Handles the events
  static void ProcessEvents(vtkObject* object, 
                            unsigned long event,
                            void* clientdata, 
                            void* calldata);

private:
  vtkPickHMPointWidget(const vtkPickHMPointWidget&); // Not implemented
  void operator=(const vtkPickHMPointWidget&); // Not implemented

  int LastY;
};

#endif
