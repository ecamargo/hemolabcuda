/*
 * $Id: vtkSMHM1DSegmentProxy.cxx 286 2006-05-05 19:43:09Z ziemer $
 */
#include "vtkSMHM1DSegmentProxy.h"
#include "vtkHMUtils.h"

#include "vtkIdList.h"

#define M_PI 3.14159265358979323846

vtkStandardNewMacro(vtkSMHM1DSegmentProxy);
vtkCxxRevisionMacro(vtkSMHM1DSegmentProxy, "$Rev: 286 $");

//----------------------------------------------------------------------------
vtkSMHM1DSegmentProxy::vtkSMHM1DSegmentProxy()
{
	//this->DebugOn();
	this->CurrentElement=NULL;
	this->CurrentNode=NULL;
	this->ElemOrder = 0;
 	this->NodeOrder = 0;
 	this->NumberOfNodes = 0;
 	this->NumberOfElements = 0;
}

//----------------------------------------------------------------------------

vtkSMHM1DSegmentProxy::~vtkSMHM1DSegmentProxy()
{

}

//----------------------------------------------------------------------------


int vtkSMHM1DSegmentProxy::GetNumberOfElements(void)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		      << "GetElementNumber"
      		  << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
	  return result;
   	}
   else
   	{
	  vtkErrorMacro("Number of elements of Segment not defined. Data Server has not returned any data in stream");
	  return 0;
   	}
}

//----------------------------------------------------------------------------

vtkHMElementData *vtkSMHM1DSegmentProxy::GetElementData(int ElemOrder)
{
  vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetElementData");

  if ( ElemOrder == -1 )
    return NULL;

  // creates the elementData which will be filled by information brought by streams
  vtkHMElementData *Elem = new vtkHMElementData();

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		      << "GetElementArray" << ElemOrder
      		  << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  const char* Data;

  if (res.GetArgument(0,0,&Data))
  	{
	  if (!strlen(Data)) return NULL;
	  int LastPosition =0, CurrentPosition=0, ElemNumber=0, z=0;
	  char temp[50];
   	// resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************

	  vtkDoubleArray *Array = vtkDoubleArray::New();
	  Array->SetNumberOfValues(14);

	  while (Data[CurrentPosition])
	  	{
	   	if (Data[CurrentPosition] == ';')
	   		{
	   	  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
//	   	  if (z == 12 || z == 13) // os dois ultimos valores do string são inteiros
//	   	  	{
//	   	  	if (z==10)
//	   	  		Elem->idNode[0] = atoi(temp);
//	   	  	else
//	   	  		Elem->idNode[1] = atoi(temp);
//	   	  	}
//	   	 	else
//	   	 		{
	   	  	Array->InsertValue(z, atof(temp));
//	   	 		}
	   	  z++;
	   	  LastPosition =CurrentPosition+1;

	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 				//*************************

	   	  }
	   	CurrentPosition++;
 	   	}

   	Elem->Elastin											= Array->GetValue(0);
	 	Elem->Collagen										= Array->GetValue(1);
		Elem->CoefficientA								= Array->GetValue(2);
	 	Elem->CoefficientB								= Array->GetValue(3);
	 	Elem->Viscoelasticity							= Array->GetValue(4);
	 	Elem->ViscoelasticityExponent			= Array->GetValue(5);
		Elem->InfiltrationPressure				= Array->GetValue(6);
		Elem->ReferencePressure						= Array->GetValue(7);
		Elem->Permeability								= Array->GetValue(8);
		Elem->WallThickness								= Array->GetValue(9);
		Elem->ElastinStentFactor					= Array->GetValue(10);
		Elem->ViscoelasticityStentFactor	= Array->GetValue(11);
		Elem->idNode[0] 									= (int)Array->GetValue(12);
		Elem->idNode[1] 									= (int)Array->GetValue(13);
		Elem->meshID 											= (int)Array->GetValue(14);

	  if (Array) Array->Delete();
   	} // final do if
	else
  	{
	  vtkErrorMacro("vtkSMHM1DSegmentProxy::GetElementData:: Data Server has not returned any data in stream");
	  return NULL;
   	}

	return Elem;
}

//----------------------------------------------------------------------------

vtkHMNodeData *vtkSMHM1DSegmentProxy::GetNodeData(int NodeOrder)
{
	double pi = M_PI;
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetNodeData" );
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy:: Node Order " << NodeOrder );

  if ( NodeOrder == -1 )
    return NULL;

 	vtkHMNodeData *Node = new vtkHMNodeData();
 	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		      << "GetNodeArray" << NodeOrder
      		  << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  const char* Data;

  if (res.GetArgument(0,0,&Data))
  	{
	  if (!strlen(Data)) return NULL;
	  int LastPosition=0, CurrentPosition=0, ElemNumber=0, z = 0;
	  char temp[50];

	  // resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************

	  vtkDoubleArray *Array = vtkDoubleArray::New();
//	  Array->SetNumberOfValues(6);
	  Array->SetNumberOfValues(7);



	  while (Data[CurrentPosition])
	  	{
	   	if (Data[CurrentPosition] == ';')
	   		{
	   	  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
	   	  Array->InsertValue(z, atof(temp));
	   	  z++;
	   	  LastPosition =CurrentPosition+1;
	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 				//*************************
	   	  }
	   	CurrentPosition++;
 	   	}
		Node->coords[0]				= Array->GetValue(0);
		Node->coords[1]				= Array->GetValue(1);
		Node->coords[2]				= Array->GetValue(2);
		Node->area						= Array->GetValue(3);
		Node->radius					= sqrt(Node->area/pi);
		Node->alfa						= Array->GetValue(5);
		Node->meshID				= int(Array->GetValue(6));


	 	if (Array) Array->Delete();
   	}
   else
   	{
	  vtkErrorMacro(" vtkSMHM1DSegmentProxy::GetNodeData::  Data Server has not returned any data in stream");
	  return NULL;
   	}
  return Node;
}

//----------------------------------------------------------------------------

void vtkSMHM1DSegmentProxy::SetSegmentCSID(int CurrentSegment, int ElemOrder, int NodeOrder, int NumberOfNodes, int NumberOfElements)
{
	vtkDebugMacro(<< "SetSegmentCSID");
	vtkDebugMacro(<< "Elem em SetSegmentCSID: "<< ElemOrder);
	vtkDebugMacro(<< "NodeOrder em SetSegmentCSID: " << NodeOrder);
	vtkDebugMacro(<< "Number of nodes  "<< NumberOfNodes);
	vtkDebugMacro(<< "Number of Elements  "<< NumberOfElements);

	vtkClientServerID csid;
	csid.ID=CurrentSegment;
	this->SetID(0,csid);

	this->ElemOrder = ElemOrder;
	this->NodeOrder = NodeOrder;
	this->NumberOfNodes = NumberOfNodes;
	this->NumberOfElements = NumberOfElements;

	vtkDebugMacro(<< "SegmentProxy gerencia segmento com CSID " << this->GetID(0));
}

//----------------------------------------------------------------------------


vtkDoubleArray *vtkSMHM1DSegmentProxy::GetNodeInfo(void)
{
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetNodeInfo");

  if ( this->NodeOrder == -1 )
    return NULL;

	CurrentNode=this->GetNodeData(this->NodeOrder);
	double pi = M_PI;
	// calculo da espessura
	double wallthickness= ((CurrentNode->alfa) / (pi*CurrentNode->radius));
	vtkDebugMacro(<< " Valor de Espessura e raio do no : " << wallthickness << " " << CurrentNode->radius);
	vtkDebugMacro(<< " Valor de alfa do no : " << CurrentNode->alfa);
	vtkDoubleArray *node = vtkDoubleArray::New();
	node->InsertValue(0, wallthickness);
	node->InsertValue(1, CurrentNode->radius);
	node->InsertValue(2, CurrentNode->coords[0]);
	node->InsertValue(3, CurrentNode->coords[1]);
	node->InsertValue(4, CurrentNode->coords[2]);
	node->InsertValue(5, CurrentNode->meshID);



	return node;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHM1DSegmentProxy::GetNodeInfo(int nodeOrder)
{
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetNodeInfo(int nodeOrder)");

  if ( nodeOrder == -1 )
    return NULL;

	CurrentNode=this->GetNodeData(nodeOrder);
	double pi = M_PI;
	// calculo da espessura
	double wallthickness= ((CurrentNode->alfa) / (pi*CurrentNode->radius));
	vtkDebugMacro(<< " Valor de Espessura e raio do no : " << wallthickness << " " << CurrentNode->radius);
	vtkDebugMacro(<< " Valor de alfa do no : " << CurrentNode->alfa);
	vtkDoubleArray *node = vtkDoubleArray::New();
	node->InsertValue(0, wallthickness);
	node->InsertValue(1, CurrentNode->radius);
	node->InsertValue(2, CurrentNode->coords[0]);
	node->InsertValue(3, CurrentNode->coords[1]);
	node->InsertValue(4, CurrentNode->coords[2]);
	node->InsertValue(5, CurrentNode->meshID);



	return node;
}

//----------------------------------------------------------------------------


vtkDoubleArray *vtkSMHM1DSegmentProxy::GetElementInfo(void)
{
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetElementInfo");

  if ( this->ElemOrder == -1 )
    return NULL;

	CurrentElement=this->GetElementData(this->ElemOrder);
  vtkDoubleArray *Elem = vtkDoubleArray::New();
  Elem->InsertValue(0,CurrentElement->Elastin);
  Elem->InsertValue(1,CurrentElement->Collagen);
	Elem->InsertValue(2,CurrentElement->CoefficientA);
	Elem->InsertValue(3,CurrentElement->CoefficientB);
	Elem->InsertValue(4,CurrentElement->Viscoelasticity);
	Elem->InsertValue(5,CurrentElement->ViscoelasticityExponent);
	Elem->InsertValue(6,CurrentElement->InfiltrationPressure);
	Elem->InsertValue(7,CurrentElement->ReferencePressure);
	Elem->InsertValue(8,CurrentElement->Permeability);
	Elem->InsertValue(9,CurrentElement->WallThickness);
	Elem->InsertValue(10,CurrentElement->meshID);
	return Elem;
}


//----------------------------------------------------------------------------

void vtkSMHM1DSegmentProxy::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
	os << indent << "Current Segment csid : " << this->GetID(0).ID << "\n";
	os << indent << "Current Element order: " << this->ElemOrder << "\n";
	os << indent << "Current Node order: " << this->NodeOrder << "\n";
	os << indent << "Current segment number of nodes: " << this->NumberOfNodes << "\n";
	os << indent << "Current segment number of elements: " << this->NumberOfElements << "\n";

}


//----------------------------------------------------------------------------

vtkDoubleArray *vtkSMHM1DSegmentProxy::GetSegmentBasicInfo(void)
{

 	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetSegmentBasicInfo");
  vtkDebugMacro(<< " Numero de elementos deste segmento " << this->NumberOfElements );
  double pi = M_PI;
  // pegar as coordenadas do no 0 e no n do segmento atual para calcular o comprimento
  vtkHMNodeData *Node = this->GetNodeData(0); //acessa no 0 do segmento
  vtkHMElementData *Element = this->GetElementData(0); // acessa o elemento proximal do segmento

  if (!Node || !Element)
  	{
  	vtkErrorMacro(<< " Could not get information from the node or element... ");
  	}

  double Proximal_Radius = Node->radius;
  double Proximal_Wallthickness = ((Node->alfa) / (pi*Node->radius));
  double Proximal_Elastin = Element->Elastin;
  double Proximal_Permeability = Element->Permeability;
  double Proximal_InfiltrationPressure = Element->InfiltrationPressure;
  double Proximal_ReferencePressure =  Element->ReferencePressure;
  double coordenadas1[3], coordenadas2[3];
  coordenadas1[0]=Node->coords[0];
  coordenadas1[1]=Node->coords[1];
  coordenadas1[2]=Node->coords[2];


  // acessa info do nó distal
  Node = this->GetNodeData(NumberOfNodes - 1); // acessa ultimo nó do segmento - diminui-se 1 pois os nos comecam de zero
  Element = this->GetElementData((NumberOfElements -1)); // acessa o elemento distal do segmento - diminui-se 1 pois os elementos comecam de zero
  double Distal_Radius = Node->radius;
  double Distal_Wallthickness = ((Node->alfa) / (pi*Node->radius));
  double Distal_Elastin = Element->Elastin;
  double Distal_Permeability = Element->Permeability;
  double Distal_InfiltrationPressure = Element->InfiltrationPressure;
  double Distal_ReferencePressure =  Element->ReferencePressure;

  coordenadas2[0]=Node->coords[0];
  coordenadas2[1]=Node->coords[1];
  coordenadas2[2]=Node->coords[2];
  double lenght, cat1, cat2, cat3;
  cat1=(coordenadas2[0] - coordenadas1[0]); // cateto 1
  cat2=(coordenadas2[1] - coordenadas1[1]); // cateto 2
  cat3=(coordenadas2[2] - coordenadas1[2]); // cateto 3

  vtkHMNodeData *no;
  Node = this->GetNodeData(0);
  lenght = 0;

  for ( int i=1; i<this->NumberOfNodes; i++ )
	  {
	  no = this->GetNodeData(i);
	  cat1 = no->coords[0] - Node->coords[0];
	  cat2 = no->coords[1] - Node->coords[1];
	  cat3 = no->coords[2] - Node->coords[2];

	  lenght += sqrt((cat1*cat1) + (cat2*cat2) + (cat3*cat3));  // calculo da distancia entre os pontos

	  Node = this->GetNodeData(i);
	  }

//  lenght=sqrt((cat1*cat1) + (cat2*cat2) + (cat3*cat3));  // calculo da distancia entre os pontos

  
  
  double Compliance, AverageRadius=0.0, AverageAlfa = 0.0, AverageWall = 0.0, AverageElastine=0.0;
  
  for ( int i=0; i<this->NumberOfNodes; i++ )
     {
     AverageRadius =AverageRadius + this->GetNodeData(i)->radius;
     AverageAlfa = AverageAlfa + this->GetNodeData(i)->alfa; 
     }
     
  AverageRadius = AverageRadius/NumberOfNodes;
  AverageAlfa = AverageAlfa/NumberOfNodes;
  
  
  AverageWall = ((AverageAlfa) / (pi*AverageRadius));
     
  
  for ( int i=0; i<this->NumberOfNodes-1; i++ )
     {
     AverageElastine = AverageElastine + this->GetElementData(i)->Elastin;
     }
  
  AverageElastine = AverageElastine/(NumberOfNodes-1);
  
  Compliance = (2*lenght*pi*AverageRadius*AverageRadius*AverageRadius)/(AverageElastine*AverageWall);
  
  
  double DX = (lenght/this->NumberOfElements);

  double volume;

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "SetLength" << lenght
      		    << vtkClientServerStream::End;

  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetVolume"
      		    << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);

  res.GetArgument(0,0,&volume);

  vtkDoubleArray *SegBasicInfo = vtkDoubleArray::New();
  SegBasicInfo->InsertValue(0, lenght);
  SegBasicInfo->InsertValue(1, DX);
  SegBasicInfo->InsertValue(2, this->NumberOfElements);
  SegBasicInfo->InsertValue(3, -1); // definir o tipo de interpolacao
  SegBasicInfo->InsertValue(4, Proximal_Radius);
  SegBasicInfo->InsertValue(5, Distal_Radius);
  SegBasicInfo->InsertValue(6, Proximal_Wallthickness);
  SegBasicInfo->InsertValue(7, Distal_Wallthickness);
  SegBasicInfo->InsertValue(8, Proximal_Elastin);
  SegBasicInfo->InsertValue(9, Distal_Elastin);
  SegBasicInfo->InsertValue(10, Proximal_Permeability);
  SegBasicInfo->InsertValue(11, Distal_Permeability);
  SegBasicInfo->InsertValue(12, Proximal_InfiltrationPressure);
  SegBasicInfo->InsertValue(13, Distal_InfiltrationPressure);
  SegBasicInfo->InsertValue(14, Proximal_ReferencePressure);
  SegBasicInfo->InsertValue(15, Distal_ReferencePressure);
  SegBasicInfo->InsertValue(16, volume);
  
  SegBasicInfo->InsertValue(17, Compliance);
  

  return SegBasicInfo;

}

//-----------------------------------------------------------------------------------------------
vtkDoubleArray *vtkSMHM1DSegmentProxy::GetSegmentAdvancedInfo(void)
{
   	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetSegmentAdvancedInfo");
  // pegar as coordenadas do no 0 e no n do segmento atual para calcular o comprimento
  //vtkHMNodeData *Node = this->GetNodeData(0); //acessa no 0 do segmento
  vtkHMElementData *Element = this->GetElementData(0); // acessa o elemento proximal do segmento


  if (!Element)
  	{
  	vtkErrorMacro(<< " Could not get advanced information from proximal element... ");
  	}


  double Proximal_Collagen = Element->Collagen;
  double Proximal_CoefficientA = Element->CoefficientA;
  double Proximal_CoefficientB = Element->CoefficientB;
  double Proximal_Viscoelasticity = Element->Viscoelasticity;
  double Proximal_ViscoelasticityExponent = Element->ViscoelasticityExponent;


  Element=NULL;
  // sempre o numero do ultimo elemento é igual ao Numero de node menos 1;
  Element = this->GetElementData((this->NumberOfElements-1)); // acessa o elemento distal do segmento
  if (!Element)
  	{
  	vtkErrorMacro(<< " Could not get advanced information from distal element... ");
  	}
  double Distal_Collagen = Element->Collagen;
  double Distal_CoefficientA = Element->CoefficientA;
  double Distal_CoefficientB = Element->CoefficientB;
  double Distal_Viscoelasticity = Element->Viscoelasticity;
  double Distal_ViscoelasticityExponent = Element->ViscoelasticityExponent;


  vtkDoubleArray *SegAdvancedInfo = vtkDoubleArray::New();
  SegAdvancedInfo->InsertValue(0, Proximal_Collagen);
  SegAdvancedInfo->InsertValue(1, Distal_Collagen);
  SegAdvancedInfo->InsertValue(2, Proximal_CoefficientA);
  SegAdvancedInfo->InsertValue(3, Distal_CoefficientA);
  SegAdvancedInfo->InsertValue(4, Proximal_CoefficientB);
  SegAdvancedInfo->InsertValue(5, Distal_CoefficientB);
  SegAdvancedInfo->InsertValue(6, Proximal_Viscoelasticity);
  SegAdvancedInfo->InsertValue(7, Distal_Viscoelasticity);
  SegAdvancedInfo->InsertValue(8, Proximal_ViscoelasticityExponent);
  SegAdvancedInfo->InsertValue(9, Distal_ViscoelasticityExponent);

  return SegAdvancedInfo;

}

//----------------------------------------------------------------------------


void vtkSMHM1DSegmentProxy::SetSegmentNodeProps(double ProximalRadius, double DistalRadius, double InterpolationType, double ProximalWall, double DistalWall)
{
  vtkDoubleArray *SegBasicInfo = this->GetSegmentBasicInfo();
  double node[7];
  node[0]=ProximalRadius;
  node[1]=DistalRadius;
  node[2]=InterpolationType;
  node[3]=ProximalWall;
  node[4]=DistalWall;
  node[5]=SegBasicInfo->GetValue(0); // lenght
  node[6]=SegBasicInfo->GetValue(1); //dx

  SegBasicInfo->Delete();

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			  << "SetNodesProps"
   			  << vtkClientServerStream::InsertArray(node,7)
      		<< vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

}

//----------------------------------------------------------------------------

void vtkSMHM1DSegmentProxy::SetSegmentProps(int numberOfElements, double length)
{
	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::SetSegmentProps");

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			  << "SetNumberOfElements" << numberOfElements
      		<< vtkClientServerStream::End;

  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			  << "SetLength" << length
      		<< vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

  this->NumberOfElements = numberOfElements;
  this->NumberOfNodes = numberOfElements+1;
}

//----------------------------------------------------------------------------


void vtkSMHM1DSegmentProxy::SetSingleNode(double radius, double wall)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			  << "SetSingleNodeProp" << radius << wall << this->NodeOrder
       		<< vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

}

//----------------------------------------------------------------------------


void vtkSMHM1DSegmentProxy::SetSingleElem(vtkDoubleArray *array, vtkDoubleArray *Geometrical_Element_Info)
{
	double Elem[11];

	for (int i=0;i<11;i++)
		Elem[i] = array->GetValue(i);

	// Inserindo a informacao da ordem do elemento a ser editado no segmento.
  // Isso é feito, pois o ProcessModule aparentemente não entende quando se envia junto do Stream
  // um vetor e um inteiro como parametro de um metodo.
  // O metodo nao é achado na classe no servidor,
  // enviando somente o vetor, funciona ok;
  Elem[10] = this->ElemOrder;

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			      << "SetSingleElem"
   			      << vtkClientServerStream::InsertArray(Elem,11)
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);



  if (Geometrical_Element_Info) // se tenho vetor para atualizar os valores dos nos distal e proximal
  															// pode se querer apenas atualizar os valores do elemento
  	{
	  double ProximalWallThickness = Geometrical_Element_Info->GetValue(0);
	  double ProximalRadius = Geometrical_Element_Info->GetValue(2);
	  double DistalWallThickness = Geometrical_Element_Info->GetValue(1);
	  double DistalRadius = Geometrical_Element_Info->GetValue(3);
	  int ProximalNodeOrder = this->ElemOrder;
	  int DistalNodeOrder = this->ElemOrder +1 ;

	  vtkClientServerStream new_stream2;
	  new_stream2  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
	   			       << "SetSingleNodeProp" << ProximalRadius << ProximalWallThickness << ProximalNodeOrder
	     		       << vtkClientServerStream::End;
		pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream2);

		vtkClientServerStream new_stream3;
		new_stream3  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
	 	       		   << "SetSingleNodeProp" << DistalRadius << DistalWallThickness << DistalNodeOrder
	       		     << vtkClientServerStream::End;
	  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream3);
  	}
}

//----------------------------------------------------------------------------

void vtkSMHM1DSegmentProxy::SetSegment(vtkDoubleArray *proxElem, vtkDoubleArray *distalElem, int InterpolationType)
{
  double ProximalElem[10],DistalElem[10];

 for (int i=0;i<10;i++)
   ProximalElem[i] = proxElem->GetValue(i);

 for (int i=0;i<10;i++)
 	 DistalElem[i] = distalElem->GetValue(i);

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream1;
  new_stream1 <<  vtkClientServerStream::Invoke <<  this->GetID(0)
       			  << "SetProxElem"
   		        << vtkClientServerStream::InsertArray(ProximalElem,10)
   			      << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream1);

  vtkClientServerStream new_stream2;
  new_stream2  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			       << "SetDistalElem"
   		         << vtkClientServerStream::InsertArray(DistalElem,10)
   			       << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream2);

  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke <<  this->GetID(0)
   			      << "SetElementsFromSegment" << InterpolationType
   			      << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

}

//----------------------------------------------------------------------------


vtkDoubleArray *vtkSMHM1DSegmentProxy::GetElementGeometricalInfo(void)
{
  vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetElementGeometricalInfo");
  double pi = M_PI;
  int ProximalNodeOrder = this->ElemOrder;
  int DistalNodeOrder = this->ElemOrder +1 ;

  vtkHMNodeData *ProximalNode = this->GetNodeData(ProximalNodeOrder);
  vtkHMNodeData *DistalNode = this->GetNodeData(DistalNodeOrder);

  double Proximal_Radius = ProximalNode->radius;
  double Proximal_Wallthickness = ((ProximalNode->alfa) / (pi*ProximalNode->radius));

  double Distal_Radius = DistalNode->radius;
  double Distal_Wallthickness = ((DistalNode->alfa) / (pi*DistalNode->radius));


  vtkDoubleArray *Elem = vtkDoubleArray::New();
  Elem->SetNumberOfValues(4);


	Elem->InsertValue(0, Proximal_Radius);
	Elem->InsertValue(1, Proximal_Wallthickness);
	Elem->InsertValue(2, Distal_Radius);
	Elem->InsertValue(3, Distal_Wallthickness);

	return Elem;
}


//----------------------------------------------------------------------------



vtkDoubleArray *vtkSMHM1DSegmentProxy::GetNodeResolutionArray()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();


  int NewNodeOrder =0;
  char type;  //Tipo de objeto clicado, node 'N' ou elemento 'E'
  if (this->NodeOrder == -1) // se elemento é selecionado
  	{
    NewNodeOrder = this->ElemOrder;
    type = 'E';
  	}
  else // se node é selecionado
  	{
  	NewNodeOrder = this->NodeOrder;
  	type = 'N';
  	}

  vtkClientServerStream stream1;
  stream1  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionArrayNumberOfComponents" << NewNodeOrder
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream1);

  int ComponentsNumber=0;
  const vtkClientServerStream ServerAnswer1= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  if (!ServerAnswer1.GetArgument(0,0,&ComponentsNumber))
  	vtkErrorMacro("Couldn't Get Resolution Array Component Number");


  vtkClientServerStream stream;
  stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionNumberOfTuples" << NewNodeOrder
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream);

  int TupleNumber=0;
  const vtkClientServerStream ServerAnswer= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  if (!ServerAnswer.GetArgument(0,0,&TupleNumber))
  	vtkErrorMacro("Couldn't Get Resolution Array Tuples Number");

  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionArrayAsChar" << NewNodeOrder << type
      		    << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
//  pm->SendStream(this->GetServers(), new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
//  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());

  const char *Data;

  if (res.GetArgument(0,0,&Data))
  	{
  	vtkDoubleArray *array = vtkHMUtils::ConvertCharToDoubleArray((char*)Data);
  	return array;
  	}
   else
   	{
	  vtkErrorMacro("vtkSMHM1DSegmentProxy::GetNodeResolutionArray :: Data Server has not returned Resolution array char in stream");
	  return NULL;
   	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::SetSegmentName(const char *n)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream <<  vtkClientServerStream::Invoke <<  this->GetID(0)
       			  << "SetSegmentName" << n
   			      << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
}

//---------------------------------------------------------------------
char *vtkSMHM1DSegmentProxy::GetSegmentName()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetSegmentName"
      		    << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  char *Data;

  if (res.GetArgument(0,0,&Data))
  	{
	  if (!Data)
	  	return NULL;
	  else
	  	{
		  strcpy(this->SegmentName, Data);
		  return this->SegmentName;
	  	}
  	}
}

//---------------------------------------------------------------------
char *vtkSMHM1DSegmentProxy::GetSegmentName(vtkIdType ID)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetSegmentName" << ID
      		    << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  char *Data;

  if (res.GetArgument(0,0,&Data))
  	{
	  if (!Data)
	  	return NULL;
	  else
	  	{
		  strcpy(this->SegmentName, Data);
		  return this->SegmentName;
	  	}
  	}
}

//---------------------------------------------------------------------
double vtkSMHM1DSegmentProxy::GetLength()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream new_stream;
	new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
			        << "GetLength"
	    		    << vtkClientServerStream::End;

	pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
	const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
	double length = 0;

	if (res.GetArgument(0,0,&length))
		{
		return length;
	  }
}

//---------------------------------------------------------------------
int vtkSMHM1DSegmentProxy::InvertFlowDirection()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "InvertFlowDirection"
      		    << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);

  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  int Data = 0;

  if (res.GetArgument(0,0,&Data))
  	{
	  if (!Data)
	  	return 0;
	  else
		  return 1;
  	}
}

vtkDoubleArray *vtkSMHM1DSegmentProxy::GetSurgicalInterventionsInfo()
{
 	vtkDebugMacro(<< "vtkSMHM1DSegmentProxy::GetSurgicalInterventionsInfo");

 	if ( (this->ElemOrder == -1) && ( this->NodeOrder != -1 ) )
 	  this->ElemOrder = this->NodeOrder;
 	else if ( (this->ElemOrder == -1) && ( this->NodeOrder == -1 ) )
 	  return NULL;

  // pegar as coordenadas dos nos.
  vtkHMNodeData *node1, *node2;
  vtkHMElementData *element = this->GetElementData(this->ElemOrder); // acessa o elemento proximal do segmento

//  node1 = this->GetNodeData(element->idNode[0]);
//  node2 = this->GetNodeData(element->idNode[1]);
  node1 = this->GetNodeData(this->ElemOrder);
  node2 = this->GetNodeData(this->ElemOrder+1);

  double squarelen;
  double length, p1[3], p2[3];

  p1[0] = node1->coords[0];
  p1[1] = node1->coords[1];
  p1[2] = node1->coords[2];

  p2[0] = node2->coords[0];
  p2[1] = node2->coords[1];
  p2[2] = node2->coords[2];

  double cat = (p2[0] - p1[0]);
  squarelen = cat * cat;

  cat = (p2[1] - p1[1]);
  squarelen += cat * cat;

  cat = (p2[2] - p1[2]);
  squarelen += cat * cat;

  length = sqrt(squarelen);

  vtkDoubleArray *SegBasicInfo = vtkDoubleArray::New();
  SegBasicInfo->InsertValue(0, length);
  SegBasicInfo->InsertValue(1, element->Elastin/element->ElastinStentFactor);
  SegBasicInfo->InsertValue(2, element->Viscoelasticity/element->ViscoelasticityStentFactor);

  return SegBasicInfo;
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::AddStent(vtkIdList *elemList, double elastinFactor, double viscoelasticityFactor)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<elemList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream new_stream;
    new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "AddStent" << elemList->GetId(i)
     			      << elastinFactor
     			      << viscoelasticityFactor
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::RemoveStent(vtkIdList *elemList)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<elemList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream stream;
    stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "RemoveStent" << elemList->GetId(i)
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream);
  	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::AddStenosis(vtkIdList *nodeList, double percentage)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<nodeList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream new_stream;
    new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "AddStenosis" << nodeList->GetId(i)
     			      << percentage
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::RemoveStenosis(vtkIdList *nodeList)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<nodeList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream stream;
    stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "RemoveStenosis" << nodeList->GetId(i)
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream);
  	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::AddAneurysm(vtkIdList *nodeList, double percentage)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<nodeList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream new_stream;
    new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "AddAneurysm" << nodeList->GetId(i)
     			      << percentage
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  	}
}

//---------------------------------------------------------------------
void vtkSMHM1DSegmentProxy::RemoveAneurysm(vtkIdList *nodeList)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	for ( int i=0; i<nodeList->GetNumberOfIds(); i++ )
  	{
    vtkClientServerStream stream;
    stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
     			      << "RemoveAneurysm" << nodeList->GetId(i)
        		    << vtkClientServerStream::End;
    pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream);
  	}
}
