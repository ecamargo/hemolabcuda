/*
 * $Id: vtkSMHM1DSegmentProxy.h 286 2006-05-05 19:43:09Z ziemer $
 */

// .NAME vtkSMHM1DSegmentProxy 
// .SECTION Description
// 

#ifndef _vtkSMHM1DSegmentProxy_h_
#define _vtkSMHM1DSegmentProxy_h_

#include "vtkSMProxy.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkHM1DSegment.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"
#include "vtkSMDoubleVectorProperty.h"
#include<math.h> 


class VTK_EXPORT vtkSMHM1DSegmentProxy:public vtkSMProxy
{
public:

  vtkTypeRevisionMacro(vtkSMHM1DSegmentProxy,vtkSMProxy);
  static vtkSMHM1DSegmentProxy *New(); 
  void PrintSelf(ostream& os, vtkIndent indent);

 
  // Description:  
  // returns the number of elements from the current Segment.  
  int GetNumberOfElements(void);
  
  
  // Description:  
  // set the CSID (after user interaction) of the current segment managed by this proxy
  void SetSegmentCSID(int CurrentSegment, int ElemOrder, int NodeOrder, int NumberOfNodes, int NumberOfElements);
  
  
  // Description:
  // returns the vtkHMElementData from a given segment
  vtkHMElementData *GetElementData(int ElemOrder);
  
  
  // Description:
  // returns the vtkHMNodeData from a user selected node
  vtkHMNodeData *GetNodeData(int NodeOrder);
  
  //Description:
  // returns a vtkDoubleArray (size 2) with info (wallthickness and radius) from current selected node
  vtkDoubleArray *GetNodeInfo(void);
  vtkDoubleArray *GetNodeInfo(int nodeOrder);
  
  //Description:
  // returns a vtkDoubleArray (size 10) from current selected element
  //0,->Elastin
  //1,->Collagen
	//2,->CoefficientA
	//3,->CoefficientB
	//4,->Viscoelasticity
	//5,->ViscoelasticityExponent
	//6,->InfiltrationPressure
	//7,->ReferencePressure
	//8,->Permeability
	//9,->WallThickness
  vtkDoubleArray *GetElementInfo(void);
    	
  
  // Description:
  // retorna um vtkDoubleArray com as informacoes basicas de um segmento
  // (0, lenght);
  // (1, DX);
  // (2, this->NumberOfElements);
  // (3, -1); // definir o tipo de interpolacao
  // (4, Proximal_Radius);
  // (5, Distal_Radius);
  // (6, Proximal_Wallthickness);
  // (7, Distal_Wallthickness);
  // (8, Proximal_Elastin);
  // (9, Distal_Elastin);
  // (10, Proximal_Permeability);
  // (11, Distal_Permeability);
  // (12, Proximal_InfiltrationPressure);
  // (13, Distal_InfiltrationPressure);
  // (14, Proximal_ReferencePressure);
  // (15, Distal_ReferencePressure);
  // (16, volume);
  // (17, Compliance);
 	vtkDoubleArray *GetSegmentBasicInfo(void);
 
 
 	//Description:
  // Retorna um vtkDoubleArray com as informacoes avancadas de um segmento.
 	//	0- Elastina do colageno proximal	(colagen de elementData)	
	//	1- Elastina do colageno distal		
	//	2- Valor medio de enrolamento proximal	Coef. A
	//	3- Valor medio de enrolamento distal	Coef. A
	//	4- Dispersao do enrolamento proximal	Coef.B
	//	5- Dispersao do enrolamento distal	    Coef. B
	//	6- Fluidez da parede arterial proximal	Viscoelasticity
	//	7- Fluidez da parede arterial distal	Viscoelasticity
	//	8- Expoente viscoelastico	proximal	
	//	9- Expoente viscoelastico	distal	
 	vtkDoubleArray *GetSegmentAdvancedInfo(void);
 
  //Description:
  // Updates the nodes (interpolation ou constant) from current segment
  void SetSegmentNodeProps(double ProximalRadius, double DistalRadius, double InterpolationType, double ProximalWall, double DistalWall);

  //Description:
  // Update current node properties values
  void SetSingleNode(double radius, double wall);

  //Description:
  // Update current element properties values 
  void SetSingleElem(vtkDoubleArray *array, vtkDoubleArray *Geometrical_Element_Info);

  //Description:
  // Update every element from current segment. In order to do that, two array must
  // be provided. One with info from proximal element basic info an the other with
  // info from distal element. An integer var sets the interpolation type. 
  void SetSegment(vtkDoubleArray *proxElem, vtkDoubleArray *distalElem, int InterpolationType);

  //Description:
  // returns a vtkDoubleArray with four elements with the proximal and distal geometrical info (radius and wallthickness)
	//(0, Proximal_Radius);
	//(1, Proximal_Wallthickness);
	//(2, Distal_Radius);
	//(3, Distal_Wallthickness);
	vtkDoubleArray *GetElementGeometricalInfo(void);
	
	
	//Description:
  // return current Node resolution array 
	vtkDoubleArray *GetNodeResolutionArray();
	
	//Description:
  // Updates number of elements and lenght of the current segment
  void SetSegmentProps(int numberOfElements, double lenght);
	
  // Description:
  // Set/Get Segment name
	void SetSegmentName(const char *n);
	char *GetSegmentName();
	char *GetSegmentName(vtkIdType);
	
	// Description:
	// Return length.
	double GetLength();
	
	// Description:
	// Invert flow direction of the segment.
	int InvertFlowDirection();
	
	// Description:
	// return a vtkDoubleArray with informations of the properties about surgical interventions
	// (0, Size);
	// (1, Elastin);
	// (2, Viscoelasticity);
	vtkDoubleArray *GetSurgicalInterventionsInfo();
	
	// Description:
	// Add stent.
	void AddStent(vtkIdList *elemList, double elastinFactor, double viscoelasticityFactor);
	
	// Description:
	// Remove stent.
	void RemoveStent(vtkIdList *elemList);
	
	// Description:
	// Add stenosis.
	void AddStenosis(vtkIdList *nodeList, double percentage);
	
	// Description:
	// Remove stenosis.
	void RemoveStenosis(vtkIdList *nodeList);

	// Description:
	// Add Aneurysm.
	void AddAneurysm(vtkIdList *nodeList, double percentage);
	
	// Description:
	// Remove Aneurysm.
	void RemoveAneurysm(vtkIdList *nodeList);
	
protected:
  vtkSMHM1DSegmentProxy();
  ~vtkSMHM1DSegmentProxy();
  

  // Description:
  // keeps a reference to the Element data from current segment e element wich is selected
  vtkHMElementData *CurrentElement;
  
  // Description:
  // keeps a reference to the Node data from current segment e node wich is selected
  vtkHMNodeData *CurrentNode;
	
	// Description:
  // // order of the selected element inside the segment
  int ElemOrder;
  
  // Description:
  // order of the selected node inside the segment 
  int NodeOrder; 
  
  // Description:
  // number of nodes from current segment
  int NumberOfNodes; 
  
  // Description:
  // number of elements from current segment
  int NumberOfElements;


  char SegmentName[25];
  
};


#endif /*_vtkSMHM1DSegmentProxy_h_*/
