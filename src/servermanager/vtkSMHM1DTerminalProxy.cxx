/*
 * $Id: vtkSMHM1DTerminalProxy.cxx 286 2006-05-05 19:43:09Z ziemer $
 */
#include "vtkSMHM1DTerminalProxy.h"
#include "vtkHMUtils.h"
#include "vtkIdList.h"

vtkStandardNewMacro(vtkSMHM1DTerminalProxy);
vtkCxxRevisionMacro(vtkSMHM1DTerminalProxy, "$Rev: 286 $");

//----------------------------------------------------------------------------

vtkSMHM1DTerminalProxy::vtkSMHM1DTerminalProxy()
{
	this->R1=NULL;
	this->R2=NULL;
	this->Cap=NULL;
	this->P=NULL;
	this->AssNodes.clear();
}


//----------------------------------------------------------------------------

vtkSMHM1DTerminalProxy::~vtkSMHM1DTerminalProxy()
{
 
	if (R1) R1->Delete();
	if (R2) R2->Delete();
	if (Cap) Cap->Delete();
	if (P)	P->Delete();
  this->R1=NULL;
  this->R2=NULL;
  this->Cap=NULL;
  this->P=NULL;
  this->AssNodes.clear();
}


//----------------------------------------------------------------------------

vtkDoubleArray *vtkSMHM1DTerminalProxy::GetTerminalProp(char elem, int option)
{
	if  (!this->GetID(0).ID )
  	{
		vtkErrorMacro("TerminalProxy is not attached to any vtkObject.");
		return NULL;
   	} 
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
  	{
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return NULL;
    }
	vtkClientServerStream initStream;
  initStream << vtkClientServerStream::Invoke << this->GetID(0) 
                    << "GetArray" << elem << option
                    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, initStream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
   const char* Data;
  if (res.GetArgument(0,0,&Data))
  	{
	  if (!strlen(Data)) return NULL;
	  vtkDoubleArray *array = vtkDoubleArray::New();
	  int LastPosition =0, CurrentPosition=0, ElemNumber=0, z=0, ArraySize=0;
	  char temp[50];
   	// resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************	
	  
	  
	  // pegando o primeiro elemento do char - que indica o tamanho que o doubleArray deve ter
	  while (Data[CurrentPosition])
	  {
	   	if (Data[CurrentPosition] == ';')
	   		{
	   			  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
	   				ArraySize= atoi(temp);
	   				LastPosition = CurrentPosition+1;
	   				CurrentPosition++;
	   				//CurrentPosition =0;
	   				// resetar array **********
  					for (int i=0; i < 50 ;i++)
 							temp[i]=0;
 						//*************************	
	   				break;
	   		}
	  CurrentPosition++;
	  }
	  
	  array->SetNumberOfValues(ArraySize);
	  
	    
	  while (Data[CurrentPosition])
	  	{
	   	if (Data[CurrentPosition] == ';')
	   		{
	   	  
	   	  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
	   	  array->InsertValue(z, atof(temp));
	   	  z++;
	   	  LastPosition =CurrentPosition+1;
	   	  
	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 				//*************************	
	  
	   	  }
	   	CurrentPosition++;
 	   	}
  
	  return array;
	  //return NULL;
   	} 
	else
  	{
	  vtkErrorMacro("vtkDoubleArray empty. Data Server has not returned any data in stream");
	  return NULL;
   	}
}

//----------------------------------------------------------------------------

vtkDoubleArray *vtkSMHM1DTerminalProxy::GetTerminalTimeProp(char elem, int option)
{
	if  (!this->GetID(0).ID )
  	{
		vtkErrorMacro("TerminalProxy is not attached to any vtkObject.");
		return NULL;
   	} 
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  if (!pm)
  	{
    vtkErrorMacro("Can not fully initialize without a global "
                  "ProcessModule. This object will not be fully "
                  "functional.");
    return NULL;
    }
	vtkClientServerStream initStream;
  initStream << vtkClientServerStream::Invoke << this->GetID(0) 
                    << "GetTimeArray" << elem << option
                    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, initStream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
   const char* Data;
  if (res.GetArgument(0,0,&Data))
  	{
	  if (!strlen(Data)) return NULL;
	  vtkDoubleArray *array = vtkDoubleArray::New();
	  int LastPosition =0, CurrentPosition=0, ElemNumber=0, z=0, ArraySize=0;
	  char temp[50];
   	// resetar array **********
  	for (int i=0; i < 50 ;i++)
 			temp[i]=0;
 		//*************************	
	  
	  
	  // pegando o primeiro elemento do char - que indica o tamanho que o doubleArray deve ter
	  while (Data[CurrentPosition])
	  {
	   	if (Data[CurrentPosition] == ';')
	   		{
	   			  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
	   				ArraySize= atoi(temp);
	   				LastPosition = CurrentPosition+1;
	   				CurrentPosition++;
	   				//CurrentPosition =0;
	   				// resetar array **********
  					for (int i=0; i < 50 ;i++)
 							temp[i]=0;
 						//*************************	
	   				break;
	   		}
	  CurrentPosition++;
	  }
	  
	  array->SetNumberOfValues(ArraySize);
	  
	    
	  while (Data[CurrentPosition])
	  	{
	   	if (Data[CurrentPosition] == ';')
	   		{
	   	  
	   	  memcpy(temp, Data+LastPosition, CurrentPosition-LastPosition);
	   	  array->InsertValue(z, atof(temp));
	   	  z++;
	   	  LastPosition =CurrentPosition+1;
	   	  
	   	  // resetar array **********
  			for (int i=0; i < 50 ;i++)
 					temp[i]=0;
 				//*************************	
	  
	   	  }
	   	CurrentPosition++;
 	   	}
	  return array;
	  //return NULL;
   	} 
	else
  	{
	  vtkErrorMacro("vtkDoubleArray empty. Data Server has not returned any data in stream");
	  return NULL;
   	}
}


//----------------------------------------------------------------------------
 
int vtkSMHM1DTerminalProxy::SetTerminalProp(vtkDoubleArray *array, char elem, int option)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	if (!pm)
		{
		vtkErrorMacro("Can not fully initialize without a global "
				"ProcessModule. This object will not be fully "
				"functional.");
		return 0;
		}
	if (!this->GetID(0).ID)
		{
		vtkErrorMacro("TerminalProxy is not attached to any vtkObject.");
		return 0;
		}
	
	vtkClientServerStream initStream;
	initStream << vtkClientServerStream::Invoke << this->GetID(0) 
			<< "SetArray" << array << elem << option
			<< vtkClientServerStream::End;
	
//	 cout << "Setando elem  option) "<< elem << " "<< option <<  endl;
//	  for (int i = 0; i < array->GetNumberOfTuples(); ++i)
//	  	{
//			cout << "Valor de Param Terminal " << array->GetValue(i) << endl;
//			}
	  		
			
	
	pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, initStream);
	return 1;
}

//----------------------------------------------------------------------------
 
int vtkSMHM1DTerminalProxy::SetTerminalTimeProp(vtkDoubleArray *array, char elem, int option)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	if (!pm)
		{
		vtkErrorMacro("Can not fully initialize without a global "
				"ProcessModule. This object will not be fully "
				"functional.");
		return 0;
		}
	if (!this->GetID(0).ID)
		{
		vtkErrorMacro("TerminalProxy is not attached to any vtkObject.");
		return 0;
		}
	
	vtkClientServerStream initStream;
	initStream << vtkClientServerStream::Invoke << this->GetID(0) 
			<< "SetTimeArray" << array << elem << option
			<< vtkClientServerStream::End;


//	 cout << "Setando elem  option) "<< elem << " "<< option <<  endl;
//	  for (int i = 0; i < array->GetNumberOfTuples(); ++i)
//	  	{
//			cout << "Valor de TIME Param Terminal " << array->GetValue(i) << endl;
//			}


	
	pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, initStream);
	return 1;
}

//----------------------------------------------------------------------------

void vtkSMHM1DTerminalProxy::SetTerminalCSID(int terminalCSID)
{
	vtkClientServerID csid;
	csid.ID=terminalCSID;
	this->SetID(0,csid);
	vtkDebugMacro(<< "TerminalProxy gerencia terminal com CSID " << this->GetID(0));
}

//----------------------------------------------------------------------------

vtkDoubleArray *vtkSMHM1DTerminalProxy::GetTerminalSingleParam()
{		
	// deleta os objetos antigos do TerminalProxy
	if (R1)
	  R1->Delete();
 	
	if (R2)
 	  R2->Delete();
 	
	if (Cap)
 	  Cap->Delete();
 	
	if (P)
 	  P->Delete();
 	
	this->R1=this->GetTerminalProp('R',1);
	this->R2=this->GetTerminalProp('R',2);
	this->Cap=this->GetTerminalProp('C',1);
	this->P=this->GetTerminalProp('P', 1);
	
	// -1 indica que o tamanho da f(t) é maior que 1 
	// se valor é diferente de -1 aquele é o valor constante
	vtkDoubleArray *array = vtkDoubleArray::New();
	
	
	if (this->R1->GetNumberOfTuples() != 1)
	  {
		if ( (this->R1->GetNumberOfTuples() == 2) && (this->R1->GetValue(0) == this->R1->GetValue(1)) )
		  array->InsertValue(0, this->R1->GetValue(0));
		else
	    array->InsertValue(0, -1);
	  }
	else // se R1 tem apenas um valor
	  {
	  array->InsertValue(0,this->R1->GetValue(0));
	  }

	//*********************
	// Resistance 2
	
	if (this->R2->GetNumberOfTuples() != 1)
	  {
	  if ( (this->R2->GetNumberOfTuples() == 2) && (this->R2->GetValue(0) == this->R2->GetValue(1)) )
	    {
      array->InsertValue(1, this->R2->GetValue(0));
      }
    else
      {
      array->InsertValue(1, -1);
      }
	  }
	else
    {
    array->InsertValue(1,this->R2->GetValue(0));
    }
	
	
	// ************************************
	// Capacitance
	
	if (this->Cap->GetNumberOfTuples() != 1)
	  {
	  if ( (this->Cap->GetNumberOfTuples() == 2) && (this->Cap->GetValue(0) == this->Cap->GetValue(1)) )
	    {
	    array->InsertValue(2,this->Cap->GetValue(0));	    
	    }
	  else
	    {
	    array->InsertValue(2, -1);  
	    }
	  }
	else	
	  {
	  array->InsertValue(2,this->Cap->GetValue(0));
	  }
	
	
	this->P->ComputeRange(0);
	double *r;
	r = this->P->GetRange();
	
	
	// se é coracao devolver o valor máximo da pressao
	// se for um terminal qualuqre devolver -1 ou valor constante
	
	if (!this->GetTerminalParentCount()) // coracao
	  {
	  array->InsertValue(3, r[1]);
	  }
	else // demais terminais
	  {
	  if (this->Cap->GetNumberOfTuples() != 1) // se numero de pontos maior que um
	    {
      if ( (this->P->GetNumberOfTuples() == 2) && (this->P->GetValue(0) == this->P->GetValue(1)) )
        {
        array->InsertValue(3, this->P->GetValue(0));     
        }
      else // se valores nao-constantes
        {
        array->InsertValue(3, -1);
        }
	    }
	  else // se tem apenas um valor constante
	    {
	    array->InsertValue(3, this->P->GetValue(0));
	    }
	  }

	vtkDoubleArray *time = this->GetTerminalTimeProp('R', 1);
	array->InsertValue(4, time->GetValue(time->GetNumberOfTuples()-1));
	
	int meshID = this->GetTerminalMeshID();
	
	array->InsertValue(5, r[0]);
	array->InsertValue(6, r[1]);
	array->InsertValue(7, meshID);
	time->Delete();
	return array;
	  		
}


//----------------------------------------------------------------------------

void vtkSMHM1DTerminalProxy::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
int vtkSMHM1DTerminalProxy::GetTerminalChildCount(void)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  	   	      << "GetTerminalNumberOfSons"
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
	  return result;
    } 
    else
    {
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetTerminalChildCount :: Can not get the children number from this Terminal");
	  return 0;
    }
}

//----------------------------------------------------------------------------
int vtkSMHM1DTerminalProxy::GetTerminalParentCount(void)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  	   	      << "GetNumberOfParents"
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
	  return result;
    } 
    else
    {
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetTerminalParentCount :: Can not get the parents number from this Terminal");
	  return 0;
    }
}

//----------------------------------------------------------------------------
vtkIdType vtkSMHM1DTerminalProxy::GetFirstChildId()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  	   	      << "GetFirstChildId"
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
	  return result;
    } 
    else
    {
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetFirstChildId :: Can not get child this Terminal");
	  return 0;
    }
}


vtkIdType vtkSMHM1DTerminalProxy::GetTerminalId()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  	   	      << "GetTerminalId"
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);
  
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
	  return result;
    } 
    else
    {
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetTerminalId :: Can not get id this Terminal");
	  return 0;
    }
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHM1DTerminalProxy::GetTerminalResolutionArray()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  
  
  vtkClientServerStream stream1;
  stream1  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionArrayNumberOfComponents"  
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream1);    		    
      		    
  int ComponentsNumber=0;
  const vtkClientServerStream ServerAnswer1= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  if (!ServerAnswer1.GetArgument(0,0,&ComponentsNumber))
  	vtkErrorMacro("Couldn't Get Terminal Resolution Array Component Number");	
   
  
  vtkClientServerStream stream;
  stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionNumberOfTuples" 
      		    << vtkClientServerStream::End;
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream);    		    
      		    
  int TupleNumber=0;
  const vtkClientServerStream ServerAnswer= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  if (!ServerAnswer.GetArgument(0,0,&TupleNumber))
  	vtkErrorMacro("Couldn't Get Terminal Resolution Array Tuples Number");	
  
  vtkClientServerStream new_stream;
  new_stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
  		        << "GetResolutionArrayAsChar"  
      		    << vtkClientServerStream::End;
 
  pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, new_stream);
  const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
  
  const char *Data;


  if (res.GetArgument(0,0,&Data))
  	{
    vtkDoubleArray *array = vtkHMUtils::ConvertCharToDoubleArray((char*)Data);
		return array;  
  	} 
	else
   	{
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetTerminalResolutionArray ::Data Server has not returned Resolution array char in stream");
	  return NULL;
   	}
}


//----------------------------------------------------------------------------

int vtkSMHM1DTerminalProxy::GetTerminalMeshID()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	
	
	vtkClientServerStream stream1;
	stream1  <<  vtkClientServerStream::Invoke << this->GetID(0)
			        << "GetmeshID"  
	    		    << vtkClientServerStream::End;
	pm->SendStream(vtkProcessModule::DATA_SERVER_ROOT, stream1); 
	
	int Data;
	const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER_ROOT);
	
	if (res.GetArgument(0,0,&Data))
		{
		return Data;  
		} 
	else
	 	{
	  vtkErrorMacro("vtkSMHM1DTerminalProxy::GetTerminalMeshID :: Data Server has not returned int meshID in stream");
	  return -1;
	 	}
}

//vtkIdList *vtkSMHM1DTerminalProxy::GetAssNodesMeshID(int id)
string vtkSMHM1DTerminalProxy::GetAssNodesMeshID()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	
	
	vtkClientServerStream stream;
	stream  <<  vtkClientServerStream::Invoke << this->GetID(0)
			        << "GetAssNodesMeshID" 
	    		    << vtkClientServerStream::End;
	pm->SendStream(vtkProcessModule::DATA_SERVER, stream); 
	
	const vtkClientServerStream res= pm->GetLastResult(vtkProcessModule::DATA_SERVER);//Pega a ultima stream entre cliente e servidor
	const char *Result;
	//for (int i=0; i < 20 ;i++)
 // 	  Result[i]=0;
	if (res.GetArgument(0,0,&Result))
		{
			this->AssNodes = Result;
			//char teste[50];
			//strcpy(teste, Result);
//		vtkIdList *l = vtkHMUtils::ConvertCharToIdList(Result);
			return this->AssNodes;
//		return l;
		}
	
	return NULL;
}
