/*
 * $Id: vtkSMHM1DTerminalProxy.h 286 2006-05-05 19:43:09Z ziemer $
 */
#ifndef _vtkSMHM1DTerminalProxy_h_
#define _vtkSMHM1DTerminalProxy_h_

#include "vtkSMProxy.h"
#include "vtkDoubleArray.h"
#include "vtkIntArray.h"
#include "vtkProcessModule.h"
#include "vtkObjectFactory.h"
#include "vtkClientServerStream.h"

#include <string>
using namespace std;


class VTK_EXPORT vtkSMHM1DTerminalProxy:public vtkSMProxy
{
public:

  vtkTypeRevisionMacro(vtkSMHM1DTerminalProxy,vtkSMProxy);
  static vtkSMHM1DTerminalProxy *New(); 
  void PrintSelf(ostream& os, vtkIndent indent);

  
  //Description:
  // returns a vtkDoubleArray with the values of time of ResistanceValues, 
  // Capacitance value or Pressure value list. The element is selected by the char var
  // elem. (R or r --> resistance; C or c --> capacitance; P or p --> pressure).
  // R1 is selected by the ivar option with value of 1; R2 option=2
  // for Cap or Pressure properties the ivar option can have any dummy value
  vtkDoubleArray *GetTerminalProp(char elem, int option);
  
  //Description:
  // returns a vtkDoubleArray with the values of time of ResistanceValues, 
  // Capacitance value or Pressure value list. The element is selected by the char var
  // elem. (R or r --> resistance; C or c --> capacitance; P or p --> pressure).
  // R1 is selected by the ivar option with value of 1; R2 option=2
  // for Cap or Pressure properties the ivar option can have any dummy value
  vtkDoubleArray *GetTerminalTimeProp(char elem, int option);
 
  //Description:
  // Set the values of R(1 or 2) resistance, capacitance or pressure list from 
  // vtkDoubleArray got from Terminal Client Widget
  int SetTerminalProp(vtkDoubleArray *array, char elem, int option);
  
  //Description:
  // Set the time of R(1 or 2) resistance, capacitance or pressure list from 
  // vtkDoubleArray got from Terminal Client Widget
  int SetTerminalTimeProp(vtkDoubleArray *array, char elem, int option);

  // Description:
  // set the CSID from the terminal managed by this proxy
  void SetTerminalCSID(int terminalCSID);
  
  
  // Description:
  // return a vtkDoubleArray (size 8) indicating the constant value of some terminal properties.
  // 0 - constant value of R1 (-1 if not constant value)  
  // 1 - constant value of R2 (-1 if not constant value)
  // 2 - constant value of Cap (-1 if not constant value)
  // 3 - constant value of P (-1 if not constant value)
  // 4 - Final Time 
  // 5 - Min. Flow
  // 6 - Max. Flow
  // 7 - Mesh ID
	vtkDoubleArray *GetTerminalSingleParam();

  // Description:
  // returns the child number from current terminal
  int GetTerminalChildCount(void);
  
  // Description:
  // returns the parent number from current terminal
  int GetTerminalParentCount(void);
  
  // Description:
  // Return current terminal resolution array
  vtkDoubleArray *GetTerminalResolutionArray();
  
  // Description:
  // Get terminal id.
  vtkIdType GetTerminalId();
  
  // Description:
  // Get id of the first child
  vtkIdType GetFirstChildId();

  // Description:
  // Get terminal meshID
  int GetTerminalMeshID();
  
	// Description:
	// Get terminal meshID
//	vtkIdList *GetAssNodesMeshID(int id);
	string GetAssNodesMeshID();
  
protected:

  vtkSMHM1DTerminalProxy();
  ~vtkSMHM1DTerminalProxy();
   
  
  // Description:
  //  store the property values from the current terminal managed by this proxy 
  vtkDoubleArray *R1;
  vtkDoubleArray *R2;
  vtkDoubleArray *Cap;
  vtkDoubleArray *P;

  string AssNodes;
 	
};


#endif /*_vtkSMHM1DTerminalProxy_h_*/
