/*
 * vtkSMHM3DCenterlinesProxy.cxx
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */

#include "vtkSMHM3DCenterlinesProxy.h"
#include "vtkHMPolyDataCenterlines.h"
#include "vtkHM3DCenterlinesWidget.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"
#include "vtkSMSourceProxy.h"
#include "vtkClientServerID.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkPolyData.h"
#include "vtkIdList.h"

#include "vtkCommand.h"

vtkCxxRevisionMacro(vtkSMHM3DCenterlinesProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHM3DCenterlinesProxy);

vtkSMHM3DCenterlinesProxy::vtkSMHM3DCenterlinesProxy()
{
  this->SourceProxy = NULL;
}

vtkSMHM3DCenterlinesProxy::~vtkSMHM3DCenterlinesProxy()
{
}

//----------------------------------------------------------------------------
int vtkSMHM3DCenterlinesProxy::SetSourceProxy(vtkSMSourceProxy *sp)
{

  if (!this->SourceProxy)
    {
    this->SourceProxy = sp;
    this->SourceCSID = sp->GetID(0);
    }
  else
    {
    return 0;
    }

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

  vtkPolyData *polydata = vtkPolyData::SafeDownCast(vtkHMPolyDataCenterlines::SafeDownCast(pm->GetObjectFromID(sp->GetID(0)))->GetInput());

  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetInput" << polydata
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  int result;

  if (res.GetArgument(0,0,&result))
     return result;
  else
    return 0;

  return 1;
}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::ExecuteEvent(vtkObject *wdg, unsigned long event,void *p)
{
  vtkHM3DCenterlinesWidget* widget = vtkHM3DCenterlinesWidget::SafeDownCast(wdg);

  if (!widget)
  {
    vtkErrorMacro(<<"There isn't a widget!");
    return;
  }

  int pointId = widget->GetPointIdSelected();

  //set point clicked
  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("PointId"));

  smSelect->SetElement(0, pointId);

  //set widget state
  int state = widget->GetState();

  vtkSMIntVectorProperty* smState = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("WidgetState"));

  smState->SetElement(0, state);

  if ( event != vtkCommand::EndInteractionEvent )
    this->Superclass::ExecuteEvent(wdg, event, p);
}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::ExecuteFilter()
{
cout << "vtkSMHM3DCenterlinesProxy::ExecuteFilter" << endl;
//  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
//  vtkClientServerStream stream;
//
//  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
//         << "ExecuteCenterlines" << vtkClientServerStream::End;
//
//  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SendSourceSeed()
{
  int pointId;

  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("PointId"));

  pointId = smSelect->GetElement(0);

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "AddSourceSeed" << pointId
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SendTargetSeed()
{
  int pointId;

  vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("PointId"));

  pointId = smSelect->GetElement(0);

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "AddTargetSeed" << pointId
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SetSeedLists(vtkIdList *sourceList, vtkIdList *targetList)
{
  this->SetSourceSeedList(sourceList);
  this->SetTargetSeedList(targetList);

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkHMPolyDataCenterlines *filter = vtkHMPolyDataCenterlines::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
  filter->SetRadiusArrayName("Radius");

}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SetSourceSeedList(vtkIdList *list)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkHMPolyDataCenterlines *filter = vtkHMPolyDataCenterlines::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
  filter->SetSourceSeedIds(list);
}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SetTargetSeedList(vtkIdList *list)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkHMPolyDataCenterlines *filter = vtkHMPolyDataCenterlines::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
  filter->SetTargetSeedIds(list);
}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SetViewMode(int mode)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetViewMode" << mode
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------
void vtkSMHM3DCenterlinesProxy::SetResamplingStepLength(double value)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkHMPolyDataCenterlines *filter = vtkHMPolyDataCenterlines::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
  filter->SetResamplingStepLength(value);
}
