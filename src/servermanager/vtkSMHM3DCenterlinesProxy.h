/*
 * vtkSMHM3DCenterlinesProxy.h
 *
 *  Created on: Aug 12, 2009
 *      Author: igor
 */

#ifndef VTKSMHM3DCENTERLINESPROXY_H_
#define VTKSMHM3DCENTERLINESPROXY_H_

#include "vtkSM3DWidgetProxy.h"

class vtkSMSourceProxy;
class vtkIdList;

struct vtkClientServerID;

class VTK_EXPORT vtkSMHM3DCenterlinesProxy : public vtkSM3DWidgetProxy
{
public:

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkSMHM3DCenterlinesProxy, vtkSM3DWidgetProxy);

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkSMHM3DCenterlinesProxy* New();

  // Description:
  // Execute event of the 3D Widget.
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);

  // Description:
  // Invokes the filter execution on the Data Server object.
  void ExecuteFilter();

  // Description:
  // Sends the passed seed to the widget.
  void SendSourceSeed();

  // Description:
  // Sends the passed seed to the widget.
  void SendTargetSeed();

  int SetSourceProxy( vtkSMSourceProxy *sp);

  void SetSeedLists(vtkIdList *sourceList, vtkIdList *targetList);
  void SetSourceSeedList(vtkIdList *list);
  void SetTargetSeedList(vtkIdList *list);

  void SetViewMode(int mode);

  void SetResamplingStepLength(double value);

protected:

  // Description:
  // Class Constructor
  vtkSMHM3DCenterlinesProxy();

  // Description:
  // Class Destructor
  ~vtkSMHM3DCenterlinesProxy();

  vtkSMSourceProxy *SourceProxy;
  vtkClientServerID SourceCSID;

private:
  vtkSMHM3DCenterlinesProxy(const vtkSMHM3DCenterlinesProxy&);// Not implemented
  void operator=(const vtkSMHM3DCenterlinesProxy&); // Not implemented

};

#endif /* VTKSMHM3DCENTERLINESPROXY_H_ */
