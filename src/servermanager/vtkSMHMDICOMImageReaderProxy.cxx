#include "vtkSMHMDICOMImageReaderProxy.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"

#include "vtkClientServerID.h"

vtkCxxRevisionMacro(vtkSMHMDICOMImageReaderProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHMDICOMImageReaderProxy);

vtkSMHMDICOMImageReaderProxy::vtkSMHMDICOMImageReaderProxy()
{
}

vtkSMHMDICOMImageReaderProxy::~vtkSMHMDICOMImageReaderProxy()
{
}

void vtkSMHMDICOMImageReaderProxy::SetDirectoryName(char *DirectoryName)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "SetDirectoryName" << DirectoryName << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}
/*
char* vtkSMHMDICOMImageReaderProxy::GetPatientName()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  
  this->PatientName = "Couldn't get Patient's name";


  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "GetPatientName"
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);

  vtkClientServerStream res = pm->GetLastResult(vtkProcessModule::DATA_SERVER);

  if (res.GetArgument(0, 0, &this->PatientName))
  {
  	return this->PatientName;
  }
  else
  {
    vtkErrorMacro("HeMoLab was unable to retrieve the patient's name from DataServer");
    return 0;
  }	
}
*/
