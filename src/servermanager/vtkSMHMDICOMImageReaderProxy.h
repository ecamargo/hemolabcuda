#ifndef __vtkSMHMDICOMImageReaderProxy_h
#define __vtkSMHMDICOMImageReaderProxy_h

#include "vtkSMSourceProxy.h"

struct vtkClientServerID;

class VTK_EXPORT vtkSMHMDICOMImageReaderProxy : public vtkSMSourceProxy
{
public:

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkSMHMDICOMImageReaderProxy, vtkSMSourceProxy);

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkSMHMDICOMImageReaderProxy* New();

  // Description:
  // Sets the vtkDICOMImageReader DataServer's object directory.
  void SetDirectoryName(char *DirectoryName);

  // Description:
  // Gets the vtkDICOMImageReader DataServer's object PatientName.
//  char* GetPatientName();
  
protected:
//BTX
  // Description:
  // Class Constructor
  vtkSMHMDICOMImageReaderProxy();

  // Description:
  // Class Destructor
  ~vtkSMHMDICOMImageReaderProxy();
   
  // Description:
  // Stores the PatientName aquired from GetPatientName method, so it can return the *char and delete it at this class's destructor.
  char *PatientName;

private:
  vtkSMHMDICOMImageReaderProxy(const vtkSMHMDICOMImageReaderProxy&);// Not implemented
  void operator=(const vtkSMHMDICOMImageReaderProxy&); // Not implemented
//ETX
};  

#endif
