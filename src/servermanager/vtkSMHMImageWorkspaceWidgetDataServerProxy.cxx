#include "vtkSMHMImageWorkspaceWidgetDataServerProxy.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"

#include "vtkClientServerID.h"

vtkCxxRevisionMacro(vtkSMHMImageWorkspaceWidgetDataServerProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHMImageWorkspaceWidgetDataServerProxy);

vtkSMHMImageWorkspaceWidgetDataServerProxy::vtkSMHMImageWorkspaceWidgetDataServerProxy()
{
	
  this->WidgetInputSource = 1;
  
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

    this->ImageWorkspaceID = pm->GetUniqueID();

    stream << vtkClientServerStream::New 
             << "vtkHMImageWorkspace" << this->ImageWorkspaceID
             << vtkClientServerStream::End;

    pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

vtkSMHMImageWorkspaceWidgetDataServerProxy::~vtkSMHMImageWorkspaceWidgetDataServerProxy()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Delete
		 << this->ImageWorkspaceID
	   	 << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetDataServerProxy::UpdateVTKObjects()
{
  this->Superclass::UpdateVTKObjects();
}

void vtkSMHMImageWorkspaceWidgetDataServerProxy::Update()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  
  stream << vtkClientServerStream::Invoke
         << this->ImageWorkspaceID << "Update"
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetDataServerProxy::SetWidgetInputSource(int PassedWidgetInputSource)
{
  this->WidgetInputSource = PassedWidgetInputSource;
}

void vtkSMHMImageWorkspaceWidgetDataServerProxy::SetInput(int InputServerObjectID)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  this->SourceID.ID = InputServerObjectID;

  if (!this->WidgetInputSource)
  {
    stream << vtkClientServerStream::Invoke
           << this->SourceID << "GetImageDataInput" << 0
           << vtkClientServerStream::End
           << vtkClientServerStream::Invoke
           << this->ImageWorkspaceID << "SetInput"
           << vtkClientServerStream::LastResult
           << vtkClientServerStream::End;
  }
  else
  {
    stream << vtkClientServerStream::Invoke
           << this->SourceID << "GetOutput"
           << vtkClientServerStream::End
           << vtkClientServerStream::Invoke
           << this->ImageWorkspaceID << "SetInput"
           << vtkClientServerStream::LastResult
           << vtkClientServerStream::End;
  }

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetDataServerProxy::CreateVTKObjects(int numObjects)
{
  if(this->ObjectsCreated)
  {
    return;
  }

  this->Superclass::CreateVTKObjects(numObjects);
}

int vtkSMHMImageWorkspaceWidgetDataServerProxy::GetImageWorkspaceID()
{
  return this->ImageWorkspaceID.ID;
}
