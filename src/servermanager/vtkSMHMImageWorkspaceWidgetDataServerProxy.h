#ifndef __vtkSMHMImageWorkspaceWidgetDataServerProxy_h
#define __vtkSMHMImageWorkspaceWidgetDataServerProxy_h

#include "vtkSMSourceProxy.h"

struct vtkClientServerID;
class vtkSMRenderModuleProxy;

class VTK_EXPORT vtkSMHMImageWorkspaceWidgetDataServerProxy : public vtkSMSourceProxy
{
public:

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkSMHMImageWorkspaceWidgetDataServerProxy, vtkSMSourceProxy);

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkSMHMImageWorkspaceWidgetDataServerProxy* New();

  // Description:
  // Updates DataServer side objects.  
  virtual void UpdateVTKObjects();
  
  virtual void Update();

  // Description:
  // Creates DataServer side objects.
  virtual void CreateVTKObjects(int numObjects);

  // Description:
  // Sets the input object on DataServer side.
  void SetInput(int InputServerObjectID);

  // Description:
  // Returns the ImageWorkspace DataServer object ID.
  int GetImageWorkspaceID();

  // Description:
  // Sets the WidgetInputSource attribute.
  void SetWidgetInputSource(int PassedWidgetInputSource);

protected:

  // Description:
  // Constructor.
  vtkSMHMImageWorkspaceWidgetDataServerProxy();
  
  // Description:
  // Destructor.
  ~vtkSMHMImageWorkspaceWidgetDataServerProxy();

  // Description:
  // ImageWorkspace DataServer object's ID.
  vtkClientServerID ImageWorkspaceID;

  // Description:
  // ID of the DataServer input for this filter / widget.
  vtkClientServerID SourceID;
  
  // Description:
  // Stores 0 if the sources input is selected, and 1 for the output.
  int WidgetInputSource;

private:
  vtkSMHMImageWorkspaceWidgetDataServerProxy(const vtkSMHMImageWorkspaceWidgetDataServerProxy&);// Not implemented
  void operator=(const vtkSMHMImageWorkspaceWidgetDataServerProxy&); // Not implemented
};  

#endif
