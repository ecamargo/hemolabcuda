#include "vtkSMHMImageWorkspaceWidgetProxy.h"
#include "vtkSMHMImageWorkspaceWidgetDataServerProxy.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"
#include "vtkCommand.h"
#include "vtkSMProxyManager.h"

vtkCxxRevisionMacro(vtkSMHMImageWorkspaceWidgetProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHMImageWorkspaceWidgetProxy);

vtkSMHMImageWorkspaceWidgetProxy::vtkSMHMImageWorkspaceWidgetProxy()
{
  this->ImageWorkspaceWidgetDataServerProxy = vtkSMHMImageWorkspaceWidgetDataServerProxy::New();
}

vtkSMHMImageWorkspaceWidgetProxy::~vtkSMHMImageWorkspaceWidgetProxy()
{
  this->ImageWorkspaceWidgetDataServerProxy->Delete();
}

void vtkSMHMImageWorkspaceWidgetProxy::SetImageWorkspaceWidgetDataServerProxy()
{
}

vtkSMHMImageWorkspaceWidgetDataServerProxy *vtkSMHMImageWorkspaceWidgetProxy::GetImageWorkspaceWidgetDataServerProxy()
{
  return this->ImageWorkspaceWidgetDataServerProxy;
}

void vtkSMHMImageWorkspaceWidgetProxy::UpdateVTKObjects()
{
}

void vtkSMHMImageWorkspaceWidgetProxy::CreateVTKObjects(int numObjects)
{
  if(this->ObjectsCreated)
  {
    return;
  }

  this->Superclass::CreateVTKObjects(numObjects);

  this->SetImageWorkspaceWidgetID();

  this->ImageWorkspaceWidgetDataServerProxy->CreateVTKObjects(numObjects);
}

void vtkSMHMImageWorkspaceWidgetProxy::SendSetCenterStream(char *method, int center)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << method << center << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetPlaneCenter(char *method, int center)
{
  this->SendSetCenterStream(method, center);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetPlaneEnable(char *method, int status)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << method << status << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}

void vtkSMHMImageWorkspaceWidgetProxy::SetWindowAndColorLevels(double windowLevel, double colorLevel)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetWindowAndColorLevels" << windowLevel << colorLevel << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}

void vtkSMHMImageWorkspaceWidgetProxy::SetVisibility(int val)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetVisibility" << val << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}

void vtkSMHMImageWorkspaceWidgetProxy::SetEnabled(int enabling)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetEnabled" << enabling << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetImageWorkspaceWidgetID()
{
  this->ImageWorkspaceWidgetID = this->GetID(0);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetInput(int PassedDataServerCutterObjectID)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  this->DataServerCutterObjectID.ID = PassedDataServerCutterObjectID;
  
  stream << vtkClientServerStream::Invoke
         << this->DataServerCutterObjectID << "GetOutput"
         << vtkClientServerStream::End
         << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "SetInputFromDataServer"
         << vtkClientServerStream::LastResult
         << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "CreateImagesWorkspace" << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetInputFromImageDataInput(int PassedDataServerCutterObjectID)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  this->DataServerCutterObjectID.ID = PassedDataServerCutterObjectID;

  stream << vtkClientServerStream::Invoke
         << this->DataServerCutterObjectID << "GetImageDataInput"
//         << this->DataServerCutterObjectID << "GetOutput"
         << 0 << vtkClientServerStream::End
//         << vtkClientServerStream::End
         << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "SetInputFromDataServer"
         << vtkClientServerStream::LastResult
         << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "CreateImagesWorkspace" << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::Render()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "Render"
         << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetDimensions(int *PassedDimensions)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "SetDimensions"
         << PassedDimensions[0] << PassedDimensions[1]
         << PassedDimensions[2] << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}
  
void vtkSMHMImageWorkspaceWidgetProxy::SetSpacing(double *PassedSpacing)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "SetSpacing"
         << PassedSpacing[0] << PassedSpacing[1]
         << PassedSpacing[2] << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}


void vtkSMHMImageWorkspaceWidgetProxy::ManipulateObservers(int Flag)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke
         << this->ImageWorkspaceWidgetID << "ManipulateObservers"
         << Flag << vtkClientServerStream::End;
  
  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);
}

void vtkSMHMImageWorkspaceWidgetProxy::SetSeedsVisibility(int val)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetSeedsVisibility" << val << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}

void vtkSMHMImageWorkspaceWidgetProxy::SetSeedsAutoResize(int val)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetSeedsAutoResize" << val << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}

void vtkSMHMImageWorkspaceWidgetProxy::SetIntersectionLinesVisibility(int val)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->ImageWorkspaceWidgetID
         << "SetIntersectionLinesVisibility" << val << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::RENDER_SERVER, stream);  
}
