#ifndef __vtkSMHMImageWorkspaceWidgetProxy_h
#define __vtkSMHMImageWorkspaceWidgetProxy_h

#include "vtkSM3DWidgetProxy.h"

class vtkSMHMImageWorkspaceWidgetDataServerProxy;
class vtkSMSourceProxy;

class VTK_EXPORT vtkSMHMImageWorkspaceWidgetProxy : public vtkSM3DWidgetProxy
{
public:
  // Description:
  // Macro required by VTK when extending its classes.
  vtkTypeRevisionMacro(vtkSMHMImageWorkspaceWidgetProxy, vtkSM3DWidgetProxy);

  // Description:
  // Static Method used on instanciating objects of this class.
  static vtkSMHMImageWorkspaceWidgetProxy* New();

  // Description:
  // Updates DataServer side objects.
  virtual void UpdateVTKObjects();

  // Description:
  // Sets the DataServer proxy for the widget.
  void SetImageWorkspaceWidgetDataServerProxy();

  // Description:
  // Returns this DataServer proxy's reference
  vtkSMHMImageWorkspaceWidgetDataServerProxy *GetImageWorkspaceWidgetDataServerProxy();

  // Description:
  // Sets the plane center passed from the sliders.
  void SetPlaneCenter(char *method, int center);  

  // Description:
  // Enables / Disables the texture of the reespective plane.
  void SetPlaneEnable(char *method, int status);

  // Description:
  // Sets the X, Y and Z planes window and color levels.
  void SetWindowAndColorLevels(double windowLevel, double colorLevel);

  // Description:
  // Enables / Disables the widget's visibility.
  virtual void SetVisibility(int val);

  // Description:
  // Enables / Disables the widget.
  void SetEnabled(int enabling);
  
  // Description:
  // Set the widget's input from a data server (usually from it's data server object's output).
  void SetInput(int DataServerCutterObjectID);

  // Description:
  // Set's the widget's data server object's input (usually from it's data server object's input).  
  void SetInputFromImageDataInput(int PassedDataServerCutterObjectID);

  // Description:
  // Renders the current renderer.
  void Render();
  
  // Description:
  // Sets the widget's dimensions array.
  void SetDimensions(int *PassedDimensions);

  // Description:
  // Sets the widget's spacing array.  
  void SetSpacing(double *PassedSpacing);

  // Description:
  // Add / Remove the widget's observer.
  void ManipulateObservers(int Flag);

  // Description:
  // Enables / Disables the widget's seeds visibility.
  void SetSeedsVisibility(int val);

  // Description:
  // Enables / Disables the widget's seeds auto resize mode.
  void SetSeedsAutoResize(int val);

  // Description:
  // Enables / Disables the planes intersection lines visibility.  
  void SetIntersectionLinesVisibility(int val);

protected:

  // Description:
  // Constructor
  vtkSMHMImageWorkspaceWidgetProxy();
  
  // Description:
  // Destructor
  ~vtkSMHMImageWorkspaceWidgetProxy();

  // Description:
  // Creates the widget's objects.
  virtual void CreateVTKObjects(int numObjects);
  
  // Description:
  // Data-Server cutter Proxy.
  vtkSMHMImageWorkspaceWidgetDataServerProxy *ImageWorkspaceWidgetDataServerProxy;

  // Descrition:
  // Render Server widget's object ID.
  vtkClientServerID ImageWorkspaceWidgetID;

  // Descrition:
  // Data Server widget's object ID.
  vtkClientServerID DataServerCutterObjectID;

  // Descrition:
  // Method responsible for sending the "set center" streams to the Render Server widget's object.
  void SendSetCenterStream(char *method, int center);

  // Descrition:
  // Sets the Render Server widget's object ID.
  void SetImageWorkspaceWidgetID();
  
private:
  vtkSMHMImageWorkspaceWidgetProxy(const vtkSMHMImageWorkspaceWidgetProxy&);// Not implemented
  void operator=(const vtkSMHMImageWorkspaceWidgetProxy&); // Not implemented

};  

#endif /*_vtkSMHMImageWorkspaceWidgetProxy_h_*/
