#ifndef __vtkSMHMPointWidgetProxy_h
#define __vtkSMHMPointWidgetProxy_h

#include "vtkSM3DWidgetProxy.h"

class VTK_EXPORT vtkSMHMPointWidgetProxy : public vtkSM3DWidgetProxy
{
public:
  static vtkSMHMPointWidgetProxy* New();
  vtkTypeRevisionMacro(vtkSMHMPointWidgetProxy, vtkSM3DWidgetProxy);
  void PrintSelf(ostream &os,vtkIndent indent);

  vtkSetVector3Macro(Position,double);
  vtkGetVector3Macro(Position,double);
 
  virtual void SaveInBatchScript(ofstream *file);

  virtual void UpdateVTKObjects();
protected:
//BTX
  vtkSMHMPointWidgetProxy();
  ~vtkSMHMPointWidgetProxy();

  // Description:
  // Overloaded to update the property values before saving state
  virtual void SaveState(const char* name, ostream* file, vtkIndent indent);
  
  // Description:
  // Execute event of the 3D Widget.
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);
  virtual void CreateVTKObjects(int numObjects);

  double Position[3];
  
private:
  vtkSMHMPointWidgetProxy(const vtkSMHMPointWidgetProxy&);// Not implemented
  void operator=(const vtkSMHMPointWidgetProxy&); // Not implemented
//ETX
};

#endif
