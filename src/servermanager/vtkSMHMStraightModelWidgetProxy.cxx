#include "vtkSMHMStraightModelWidgetProxy.h"
#include "vtkHMStraightModelWidget.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModelSource.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DBasParam.h"
#include "vtkHM1DParam.h"
#include "vtkHMDataOut.h"
#include "vtkHMDefineVariables.h"
#include "vtkHMUtils.h"
#include "vtkObjectFactory.h"
#include "vtkPVProcessModule.h"
#include "vtkClientServerStream.h"
#include "vtkCommand.h"
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMStringVectorProperty.h"
#include "vtkSMSourceProxy.h"
#include "vtkPVDataInformation.h"
#include "vtkPVDataSetAttributesInformation.h"
#include "vtkPVArrayInformation.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "vtkStringArray.h"
#include "vtkIdList.h"

#include <algorithm>

#include "vtkHM1DKeepRemoveFilter.h"

vtkStandardNewMacro(vtkSMHMStraightModelWidgetProxy);
vtkCxxRevisionMacro(vtkSMHMStraightModelWidgetProxy, "$Revision: 1.7 $");
//----------------------------------------------------------------------------
vtkSMHMStraightModelWidgetProxy::vtkSMHMStraightModelWidgetProxy()
{
	this->StraightModelElementSelected = 0;

	// Este StraightModel é um ponteiro para o StraightModel da classe Widget
	// Ele será atribuido no método ExecuteEvent
	this->StraightModel = NULL;

	// Este Widget é um ponteiro para a classe Widget referente a este objeto
	this->Widget = NULL;

	// No momento não há nenhum objeto do StraightModel selecionado
	this->StraightModelObjectSelected = -1;


	this->StraightModelSource = NULL;
	this->SourceProxy = NULL;
	this->Reader = NULL;

	this->SegmentProxy = vtkSMHM1DSegmentProxy::New();
	this->TerminalProxy = vtkSMHM1DTerminalProxy::New();

	this->PlotArray = vtkIntArray::New();

	this->IDList = vtkIdList::New();
}

//----------------------------------------------------------------------------
vtkSMHMStraightModelWidgetProxy::~vtkSMHMStraightModelWidgetProxy()
{
	this->StraightModelSource = NULL;
	this->StraightModel = NULL;
	this->Widget = NULL;
	this->SourceProxy = NULL;
	this->Reader = NULL;

	this->SegmentProxy->Delete();
	this->SegmentProxy = NULL;
	this->TerminalProxy->Delete();
	this->TerminalProxy = NULL;

	PlotArray->Delete();
	PlotArray = NULL;

	this->IDList->Delete();
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::UpdateVTKObjects()
{
  this->Superclass::UpdateVTKObjects();
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::CreateVTKObjects(int numObjects)
{


  if(this->ObjectsCreated)
    {


    return;
    }

  unsigned int cc;

  //superclass will create the stream objects
  this->Superclass::CreateVTKObjects(numObjects);

  //now do additional initialization on the streamobjects
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  for(cc=0; cc < this->GetNumberOfIDs(); cc++)
    {
    vtkClientServerID id = this->GetID(cc);
    stream << vtkClientServerStream::Invoke <<  id
           << "SetAlignToNone" << vtkClientServerStream::End;
    pm->SendStream(this->GetServers(), stream);
    }
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetNumberGraphs(int value)
{

    vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

	vtkClientServerStream stream;

    vtkClientServerID id = this->GetID(0);

    stream << vtkClientServerStream::Invoke <<  id
           << "SetMaxCurves" << value
           << vtkClientServerStream::End;

    pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::SetSourceProxy(vtkSMSourceProxy *sp)
{
  if (!this->SourceProxy)
    {
    this->SourceProxy = sp;
	  this->SourceCSID = sp->GetID(0);
    }
  else
  	{
  	//vtkErrorMacro(<< "Source proxy already setted! this->Superclass::CreateVTKObjects(1)");
  	return 0;
  	}

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetSourceCSID" << sp->GetID(0).ID
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  int result;
  this->StraightModel = this->Widget->GetStraightModel();
  if (res.GetArgument(0,0,&result))
     return result;
  else
  	return 0;

	vtkDebugMacro( << " Straight model source succesfully connected to straight model source");
  return 1;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ExecuteEvent(vtkObject *wdg, unsigned long event,void *p)
{
  vtkHMStraightModelWidget* widget = vtkHMStraightModelWidget::SafeDownCast(wdg);
  if (!widget)
  {
  	vtkErrorMacro(<<"There isn't a widget!");
    return;
  }

	if ( !this->Widget )
		this->Widget = widget;

	// Pega informações sobre geometria inicial (raio e resolução de esferas etc).
//	vtkSMDoubleVectorProperty* geo = vtkSMDoubleVectorProperty::SafeDownCast(
//		      		this->GetProperty("GeometryValuesProperty"));

//	double values[4];
//	this->Widget->GetWidgetHandleValues(values);
//	if (geo)
//		for(int i = 0; i < 4; i++)
//			geo->SetElement(i, values[i]);

	// Aqui já posso acessar as propriedades para ver qual dos graficos
	// que foram selecionados para visualização
	vtkSMIntVectorProperty* plot = vtkSMIntVectorProperty::SafeDownCast(
		      		this->GetProperty("SelectedGraphic"));

	if (plot)
		{
		for(int i = 0; i < 6; i++)
			PlotArray->InsertValue(i, plot->GetElement(i));

		this->Widget->SetPlotType(PlotArray);
		}

  this->StraightModelElementSelected = this->Widget->GetStraightModelElementSelected();

  if (event != vtkCommand::PlaceWidgetEvent || !this->IgnorePlaceWidgetChanges)
  	{
		// As atualizações que são feitas no StraightModel dentro da classe widget são
		// refletidas no StraightModel do SM
		this->StraightModel = this->Widget->GetStraightModel();

		this->StraightModelObjectSelected = this->Widget->GetStraightModelObjectSelected();

		vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
      this->GetProperty("StraightModelObjectSelected"));

		smSelect->SetElement(0, this->StraightModelObjectSelected);
	  // pegar o valor da propriedade que indica o modo de operacao (selecionado pelo usuario)
	  // se for Input Visualization ou Edition, atualizar os valores das propriedades
    this->UpdateWidgetProperties();

  	}

  vtkSMIntVectorProperty* smWidgetSelect = vtkSMIntVectorProperty::SafeDownCast(
  		this->GetProperty("StraightModelWidgetEvent"));
	smWidgetSelect->SetElement(0, event);
  this->GetTreeGeneralProperties();
  this->Superclass::ExecuteEvent(wdg, event, p);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SaveState(const char* name,ostream* file,
  vtkIndent indent)
{
  this->Superclass::SaveState(name,file,indent);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SaveInBatchScript(ofstream *file)
{
  this->Superclass::SaveInBatchScript(file);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::UpdateWidgetProperties(void)
{
	vtkDebugMacro(<< "vtkSMHMStraightModelWidgetProxy::UpdateWidgetProperties(void)" );

	// ************************************************************************************
	// ************************************************************************************
	// Se um NÓ foi Selecionado
	// ************************************************************************************
	// ************************************************************************************
	if (this->StraightModelObjectSelected == NODE) // seta prop do no
		{
		vtkIntArray *array = this->StraightModel->GetSegmentCSID(this->Widget->GetSegmentSelected());

		this->SegmentProxy->SetSegmentCSID(array->GetValue(0), -1, this->Widget->GetNodeSelected(), array->GetValue(1), array->GetValue(2));

		vtkDoubleArray *BasicSeg = this->SegmentProxy->GetSegmentBasicInfo();
    vtkSMDoubleVectorProperty* node0 = vtkSMDoubleVectorProperty::SafeDownCast(
		this->GetProperty("BasicSegmentProperty"));
		if (!node0)
			{
			vtkErrorMacro("Can not find the property: BasicSegmentProperty");
			}
		else
			{
      node0->SetElement(0, BasicSeg->GetValue(0));
      node0->SetElement(1, BasicSeg->GetValue(1));
      node0->SetElement(2, BasicSeg->GetValue(2));
      node0->SetElement(3, BasicSeg->GetValue(3));
      node0->SetElement(4, BasicSeg->GetValue(4));
      node0->SetElement(5, BasicSeg->GetValue(5));
      node0->SetElement(6, BasicSeg->GetValue(6));
      node0->SetElement(7, BasicSeg->GetValue(7));
      node0->SetElement(8, BasicSeg->GetValue(8));
      node0->SetElement(9, BasicSeg->GetValue(9));
      node0->SetElement(10, BasicSeg->GetValue(10));
      node0->SetElement(11, BasicSeg->GetValue(11));
      node0->SetElement(12, BasicSeg->GetValue(12));
      node0->SetElement(13, BasicSeg->GetValue(13));
      node0->SetElement(14, BasicSeg->GetValue(14));
      node0->SetElement(15, BasicSeg->GetValue(15));
  		}

		vtkDoubleArray *AdvancedSeg = this->SegmentProxy->GetSegmentAdvancedInfo();
    vtkSMDoubleVectorProperty* ASeg = vtkSMDoubleVectorProperty::SafeDownCast(
		this->GetProperty("AdvancedSegmentProperty"));
		if (!node0)
			{
			vtkErrorMacro("Can not find the property: AdvancedSegmentProperty");
			}
		else
			{
      ASeg->SetElement(0, AdvancedSeg->GetValue(0));
      ASeg->SetElement(1, AdvancedSeg->GetValue(1));
      ASeg->SetElement(2, AdvancedSeg->GetValue(2));
      ASeg->SetElement(3, AdvancedSeg->GetValue(3));
      ASeg->SetElement(4, AdvancedSeg->GetValue(4));
      ASeg->SetElement(5, AdvancedSeg->GetValue(5));
      ASeg->SetElement(6, AdvancedSeg->GetValue(6));
      ASeg->SetElement(7, AdvancedSeg->GetValue(7));
      ASeg->SetElement(8, AdvancedSeg->GetValue(8));
      ASeg->SetElement(9, AdvancedSeg->GetValue(9));
		 	}

    vtkDoubleArray *BasicNode = this->SegmentProxy->GetNodeInfo();
    vtkSMDoubleVectorProperty* node2 = vtkSMDoubleVectorProperty::SafeDownCast(
		this->GetProperty("NodeProperty"));
    if (!node2)  {vtkErrorMacro("Can not find the property: NodeProperty");}
		else
			{
      node2->SetElement(0, BasicNode->GetValue(0));
      node2->SetElement(1, BasicNode->GetValue(1));
      node2->SetElement(2, BasicNode->GetValue(2));
      node2->SetElement(3, BasicNode->GetValue(3));
      node2->SetElement(4, BasicNode->GetValue(4));
      node2->SetElement(5, BasicNode->GetValue(5));
			}

    array->Delete();
    BasicNode->Delete();
    BasicSeg->Delete();
    AdvancedSeg->Delete();
//    this->IDList.clear();
    this->IDList->Reset();
	  }

	// ************************************************************************************
	// ************************************************************************************
	// Se um ELEMENTO foi Selecionado
	// ************************************************************************************
	// ************************************************************************************

	else if (this->StraightModelObjectSelected == SEGMENT) // seta prop do elemento
		{
		vtkIntArray *array = this->StraightModel->GetSegmentCSID(this->Widget->GetSegmentSelected());

		// se a tecla CRTL estiver pressionada enquanto um elemento for selecionado
		// entao o id do segmento será inserido na lista dos segmentos atualmente
		// selecionados
		if (this->CrtlKeyPressed()&&(this->Widget->GetStraightModelMode()!=MODE_TODETACHSUBTREE_VAL))
			{
			if (!this->IDList->GetNumberOfIds()) // renderiza atores novamente com cores padroes
			   this->Widget->RenderSegmentsWithDefaultColors();

			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
		  }
		else if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			this->IDList->Reset();
			vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();

			if ( subTreeListSegments )
				this->IDList->DeepCopy(subTreeListSegments);
			subTreeListSegments->Delete();
			}
		else //if ( !this->CrtlKeyPressed() )
			{
			this->IDList->Reset();
			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
			}

		this->SegmentProxy->SetSegmentCSID(array->GetValue(0), this->Widget->GetElementSelected(), -1, array->GetValue(1),array->GetValue(2));

		vtkDoubleArray *BasicSeg = this->SegmentProxy->GetSegmentBasicInfo();
		vtkSMDoubleVectorProperty* node0 = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("BasicSegmentProperty"));

	  if (!node0)
	  	{
	    vtkErrorMacro("Can not find the property: BasicSegmentProperty");
	    }
	  else
	  	{
      node0->SetElement(0, BasicSeg->GetValue(0));
      node0->SetElement(1, BasicSeg->GetValue(1));
      node0->SetElement(2, BasicSeg->GetValue(2));
      node0->SetElement(3, BasicSeg->GetValue(3));
      node0->SetElement(4, BasicSeg->GetValue(4));
      node0->SetElement(5, BasicSeg->GetValue(5));
      node0->SetElement(6, BasicSeg->GetValue(6));
      node0->SetElement(7, BasicSeg->GetValue(7));
      node0->SetElement(8, BasicSeg->GetValue(8));
      node0->SetElement(9, BasicSeg->GetValue(9));
      node0->SetElement(10, BasicSeg->GetValue(10));
      node0->SetElement(11, BasicSeg->GetValue(11));
      node0->SetElement(12, BasicSeg->GetValue(12));
      node0->SetElement(13, BasicSeg->GetValue(13));
      node0->SetElement(14, BasicSeg->GetValue(14));
      node0->SetElement(15, BasicSeg->GetValue(15));
      node0->SetElement(16, BasicSeg->GetValue(16));
      node0->SetElement(17, BasicSeg->GetValue(17));


		  }

		vtkDoubleArray *AdvancedSeg = this->SegmentProxy->GetSegmentAdvancedInfo();
		vtkSMDoubleVectorProperty* ASeg2 = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("AdvancedSegmentProperty"));
	  if (!node0)
	  	{
	    vtkErrorMacro("Can not find the property: AdvancedSegmentProperty");
	    }
	  else
	  	{
			ASeg2->SetElement(0, AdvancedSeg->GetValue(0));
      ASeg2->SetElement(1, AdvancedSeg->GetValue(1));
      ASeg2->SetElement(2, AdvancedSeg->GetValue(2));
      ASeg2->SetElement(3, AdvancedSeg->GetValue(3));
      ASeg2->SetElement(4, AdvancedSeg->GetValue(4));
      ASeg2->SetElement(5, AdvancedSeg->GetValue(5));
      ASeg2->SetElement(6, AdvancedSeg->GetValue(6));
      ASeg2->SetElement(7, AdvancedSeg->GetValue(7));
      ASeg2->SetElement(8, AdvancedSeg->GetValue(8));
      ASeg2->SetElement(9, AdvancedSeg->GetValue(9));
      }
		vtkDoubleArray *Elem = this->SegmentProxy->GetElementInfo();

		vtkSMDoubleVectorProperty* node2 = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("ElementProperty"));
		if (!node2)
			{
		  vtkErrorMacro("Can not find the property: ElementProperty");
		  }
	  else
	    {
      node2->SetElement(0, Elem->GetValue(0));
      node2->SetElement(1, Elem->GetValue(1));
      node2->SetElement(2, Elem->GetValue(2));
      node2->SetElement(3, Elem->GetValue(3));
      node2->SetElement(4, Elem->GetValue(4));
      node2->SetElement(5, Elem->GetValue(5));
      node2->SetElement(6, Elem->GetValue(6));
      node2->SetElement(7, Elem->GetValue(7));
      node2->SetElement(8, Elem->GetValue(8));
      node2->SetElement(9, Elem->GetValue(9));
      node2->SetElement(10, Elem->GetValue(10));
	    }
	  vtkDoubleArray *ElementGeometricalInfoArray = this->SegmentProxy->GetElementGeometricalInfo();
		vtkSMDoubleVectorProperty* ElementGeometricalInfo = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("GeometricalElementProperty"));
		if (!node2) { vtkErrorMacro("Can not find the property: GeometricalElementProperty");}
	  else
	  	{
			ElementGeometricalInfo->SetElement(0, ElementGeometricalInfoArray->GetValue(0));
			ElementGeometricalInfo->SetElement(1, ElementGeometricalInfoArray->GetValue(1));
			ElementGeometricalInfo->SetElement(2, ElementGeometricalInfoArray->GetValue(2));
			ElementGeometricalInfo->SetElement(3, ElementGeometricalInfoArray->GetValue(3));
			}
	  Elem->Delete();
		BasicSeg->Delete();
		array->Delete();
		AdvancedSeg->Delete();
		ElementGeometricalInfoArray->Delete();
		} //fim if seta prop do elemento

	// ************************************************************************************
	// ************************************************************************************
	// Se um TERMINAL foi Selecionado
	// ************************************************************************************
	// ************************************************************************************

	else if (this->StraightModelObjectSelected == TERMINAL || this->StraightModelObjectSelected == HEART) // seta prop de um terminal
		{
//	  vtkIntArray *array = this->StraightModel->GetTerminalCSID(this->Widget->GetTerminalSelected());
	  vtkIdType id = this->StraightModel->GetTerminalCSID(this->Widget->GetTerminalSelected());
//    this->TerminalProxy->SetTerminalCSID(array->GetValue(1));
		this->TerminalProxy->SetTerminalCSID(id);

    vtkDoubleArray *array2 = this->TerminalProxy->GetTerminalSingleParam();
	  vtkSMDoubleVectorProperty* TerminalProp = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("TerminalProperties"));
	  if (!TerminalProp) { vtkErrorMacro("Can not find the property: terminalProperty");}
	  else
	  	{
	    TerminalProp->SetElement(0, array2->GetValue(0));
	    TerminalProp->SetElement(1, array2->GetValue(1));
	    TerminalProp->SetElement(2, array2->GetValue(2));
	    TerminalProp->SetElement(3, array2->GetValue(3));
	    TerminalProp->SetElement(4, array2->GetValue(4));
	    TerminalProp->SetElement(5, array2->GetValue(5));
	    TerminalProp->SetElement(6, array2->GetValue(6));
	    TerminalProp->SetElement(7, array2->GetValue(7));
	    }

	  array2->Delete();

	  this->IDList->Reset();
		if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			this->IDList->Reset();
			vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();

			if ( subTreeListSegments )
				this->IDList->DeepCopy(subTreeListSegments);
			subTreeListSegments->Delete();
			}
	  }
	// ************************************************************************************
	// ************************************************************************************
	// Se um STENT foi Selecionado
	// ************************************************************************************
	// ************************************************************************************

	else if (this->StraightModelObjectSelected == STENT) // seta prop do elemento
		{
		vtkIntArray *array = this->StraightModel->GetSegmentCSID(this->Widget->GetSegmentSelected());

		// se a tecla CRTL estiver pressionada enquanto um elemento for selecionado
		// entao o id do segmento será inserido na lista dos segmentos atualmente
		// selecionados
		if (this->CrtlKeyPressed()&&(this->Widget->GetStraightModelMode()!=MODE_TODETACHSUBTREE_VAL))
			{
			if (!this->IDList->GetNumberOfIds()) // renderiza atores novamente com cores padroes
			   this->Widget->RenderSegmentsWithDefaultColors();

			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
		  }
		else if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			this->IDList->Reset();
			vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();

			if ( subTreeListSegments )
				this->IDList->DeepCopy(subTreeListSegments);
			subTreeListSegments->Delete();
			}
		else
			{
			this->IDList->Reset();
			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
			}

		this->SegmentProxy->SetSegmentCSID(array->GetValue(0), this->Widget->GetElementSelected(), this->Widget->GetNodeSelected(), array->GetValue(1),array->GetValue(2));

		vtkDoubleArray *BasicSeg = this->SegmentProxy->GetSurgicalInterventionsInfo();
		vtkSMDoubleVectorProperty* elem = vtkSMDoubleVectorProperty::SafeDownCast(
	  this->GetProperty("InterventionsProperty"));

	  if ( !elem || !BasicSeg )
	  	{
	    vtkErrorMacro("Can not find the property: InterventionsProperty");
	    }
	  else
	  	{
      elem->SetElement(0, BasicSeg->GetValue(0));
      elem->SetElement(1, BasicSeg->GetValue(1));
      elem->SetElement(2, BasicSeg->GetValue(2));
      BasicSeg->Delete();
		  }

		array->Delete();
		} //fim if seta prop do elemento
	// ************************************************************************************
	// ************************************************************************************
	// Se um STENOSIS foi Selecionado
	// ************************************************************************************
	// ************************************************************************************
	else if (this->StraightModelObjectSelected == STENOSIS) // seta prop do elemento
		{
		vtkIntArray *array = this->StraightModel->GetSegmentCSID(this->Widget->GetSegmentSelected());

		// se a tecla CRTL estiver pressionada enquanto um elemento for selecionado
		// entao o id do segmento será inserido na lista dos segmentos atualmente
		// selecionados
		if (this->CrtlKeyPressed()&&(this->Widget->GetStraightModelMode()!=MODE_TODETACHSUBTREE_VAL))
			{
			if (!this->IDList->GetNumberOfIds()) // renderiza atores novamente com cores padroes
			   this->Widget->RenderSegmentsWithDefaultColors();

			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
		  }
		else if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			this->IDList->Reset();
			vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();

			if ( subTreeListSegments )
				this->IDList->DeepCopy(subTreeListSegments);
			subTreeListSegments->Delete();
			}
		else
			{
			this->IDList->Reset();
			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
			}

		this->SegmentProxy->SetSegmentCSID(array->GetValue(0), this->Widget->GetElementSelected(), this->Widget->GetNodeSelected(), array->GetValue(1),array->GetValue(2));

		array->Delete();
		} //fim if seta prop do elemento
	// ************************************************************************************
	// ************************************************************************************
	// Se um ANEURYSM foi Selecionado
	// ************************************************************************************
	// ************************************************************************************
	else if (this->StraightModelObjectSelected == ANEURYSM) // seta prop do elemento
		{
		vtkIntArray *array = this->StraightModel->GetSegmentCSID(this->Widget->GetSegmentSelected());

		// se a tecla CRTL estiver pressionada enquanto um elemento for selecionado
		// entao o id do segmento será inserido na lista dos segmentos atualmente
		// selecionados
		if (this->CrtlKeyPressed()&&(this->Widget->GetStraightModelMode()!=MODE_TODETACHSUBTREE_VAL))
			{
			if (!this->IDList->GetNumberOfIds()) // renderiza atores novamente com cores padroes
			   this->Widget->RenderSegmentsWithDefaultColors();

			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
		  }
		else if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			this->IDList->Reset();
			vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();

			if ( subTreeListSegments )
				this->IDList->DeepCopy(subTreeListSegments);
			subTreeListSegments->Delete();
			}
		else
			{
			this->IDList->Reset();
			this->IDList->InsertUniqueId(this->Widget->GetSegmentSelected());
			}

		this->SegmentProxy->SetSegmentCSID(array->GetValue(0), this->Widget->GetElementSelected(), this->Widget->GetNodeSelected(), array->GetValue(1),array->GetValue(2));

		array->Delete();
		} //fim if seta prop do elemento
	else if ( !this->CrtlKeyPressed() )
		{
		this->IDList->Reset();
		}

}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::GetTreeGeneralProperties(void)
{
	if ( !this->StraightModel )
		return;

	// first thing: to acess the desired property
	vtkSMIntVectorProperty* tree = vtkSMIntVectorProperty::SafeDownCast(
		this->GetProperty("TreeGeneralProperty"));

    // ok, the property exists...
    if (tree)
    	{
			tree->SetElement(0, this->StraightModel->GetNumberOfSegments());
			tree->SetElement(1, this->StraightModel->GetNumberOfTerminals());
			tree->SetElement(2, this->StraightModel->GetNumberOfNodes());
		}
    else
    	{
    	vtkErrorMacro("Can not find the property: TreeGeneralProperty");
    	}
}


//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::SelectTreeElement(int element, int segment)
{
	this->Widget->SetElement_Selected(element);
  this->Widget->SetSegment_Selected(segment);
}

//----------------------------------------------------------------------------

vtkSMHM1DSegmentProxy *vtkSMHMStraightModelWidgetProxy::GetSegmentProxy(void)
{
 return this->SegmentProxy;
}

//----------------------------------------------------------------------------

vtkSMHM1DTerminalProxy *vtkSMHMStraightModelWidgetProxy::GetTerminalProxy(void)
{
 	return this->TerminalProxy;
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::GenerateSolverFiles(vtkDoubleArray *DoubleParam, vtkIntArray *IntParam, const char *path, const char *LogFilename, vtkIntArray *WhichFile)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
 	vtkClientServerStream stream, stream2;

	double DoubleParamArray[21];
	int IntParamArray[17];
	int WhichFileArray[4];


	for (int i=0; i < 4; i++)
		WhichFileArray[i] = WhichFile->GetValue(i);

	for (int i=0; i < 21; i++)
		{
		//cout << "Valor de Param Double "<< i << " " << DoubleParam->GetValue(i) << endl;
		DoubleParamArray[i] = DoubleParam->GetValue(i);
		}

  for (int i=0; i < 17; i++)
		{
		//cout << "Valor de Int Param "<< i << " " << IntParam->GetValue(i) << endl;
		IntParamArray[i] = IntParam->GetValue(i);
		}

	vtkClientServerID id = pm->GetUniqueID();

	// instancia um objeto do tipo vtkHM1DStraightModelWriterPreProcessor
	stream << vtkClientServerStream::New
         << "vtkHM1DStraightModelWriterPreProcessor" << id
         << vtkClientServerStream::End;

	pm->SendStream(vtkProcessModule::DATA_SERVER, stream);


  //pegando o reader
  vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));

  if ( !reader )
    {
    vtkClientServerID newId = pm->GetUniqueID();

    stream << vtkClientServerStream::New
           << "vtkHM1DStraightModelReader" << newId
           << vtkClientServerStream::End;

    vtkHM1DBasParam *BasParam = vtkHM1DBasParam::New();
    vtkHM1DMesh *Mesh = vtkHM1DMesh::New();

    stream << vtkClientServerStream::Invoke
              << newId << "SetMesh" << Mesh
              << vtkClientServerStream::End;


    stream << vtkClientServerStream::Invoke
                << newId << "SetBasParam" << BasParam
                << vtkClientServerStream::End;

    stream << vtkClientServerStream::Invoke
              << newId << "GetMesh"
              << vtkClientServerStream::End
              << vtkClientServerStream::Invoke
              << id << "SetMesh"
              << vtkClientServerStream::LastResult
              << vtkClientServerStream::End;

    stream << vtkClientServerStream::Invoke
                << newId << "GetBasParam"
                << vtkClientServerStream::End
                << vtkClientServerStream::Invoke
                << id << "SetBasParam"
                << vtkClientServerStream::LastResult
                << vtkClientServerStream::End;

    pm->SendStream(vtkProcessModule::DATA_SERVER, stream);

    BasParam->Delete();
    Mesh->Delete();
    }
  else
    {
    // setando vtkHM1DMesh e vtkHM1DBasParam para o vtkHM1DStraightModelWriterPreProcessor
    // que sao construidos no processo de leitura. O preProcessor deve atualizar o Mesh e BasParam
    // para evitar que esses objetos fiquem desatualizados e no processo de leitura do DataOut
    // informacao nao atualizada seja utilizada. Exemplo: numero de nodes de um segmento foi mudado e na leitura do
    // DataOut.txt o numero de nodes antigo tenta ser lido e assim um erro na leitura ocorre.

    stream << vtkClientServerStream::Invoke
              << this->SourceCSID << "GetMesh"
              << vtkClientServerStream::End
              << vtkClientServerStream::Invoke
              << id << "SetMesh"
              << vtkClientServerStream::LastResult
              << vtkClientServerStream::End;

    stream << vtkClientServerStream::Invoke
                << this->SourceCSID << "GetBasParam"
                << vtkClientServerStream::End
                << vtkClientServerStream::Invoke
                << id << "SetBasParam"
                << vtkClientServerStream::LastResult
                << vtkClientServerStream::End;

    pm->SendStream(vtkProcessModule::DATA_SERVER, stream);

    }

  //*****************************************************************************************

	stream << vtkClientServerStream::Invoke
				 << this->SourceCSID << "GetOutput"
				 << vtkClientServerStream::End
				 << vtkClientServerStream::Invoke
				 << id << "SetStraightModel"
				 << vtkClientServerStream::LastResult
				 << vtkClientServerStream::End;

	pm->SendStream(vtkProcessModule::DATA_SERVER, stream);


	//seta parametros reais
	stream << vtkClientServerStream::Invoke << id
         << "SetDoubleParam"
         << vtkClientServerStream::InsertArray(DoubleParamArray, 21)
         << vtkClientServerStream::End;

  // seta parametros inteiros
  stream << vtkClientServerStream::Invoke << id
         << "SetIntParam"
         << vtkClientServerStream::InsertArray(IntParamArray, 17)
         << vtkClientServerStream::End;

 	//seta o nome do arquivo de log do Basparam
	stream << vtkClientServerStream::Invoke << id
         << "SetLogfilename" << LogFilename
         << vtkClientServerStream::End;


  // seta vetpor que descreve quais arquivos do solver serao gerados
  stream << vtkClientServerStream::Invoke << id
         << "SetWhichFile"
         << vtkClientServerStream::InsertArray(WhichFileArray, 4)
         << vtkClientServerStream::End;

	// cria um novo objeto BasParam
	stream << vtkClientServerStream::Invoke << id
         << "CreateNewBasParam"
         << vtkClientServerStream::End;

	pm->SendStream(this->GetServers(), stream);


	// manda stream para percorrer o StraightModel
	// para gerar os arquivos
	stream2 << vtkClientServerStream::Invoke << id
         << "InitializeWriteProcess" << path
         << vtkClientServerStream::End;

	pm->SendStream(this->GetServers(), stream2);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  int result=0;
  res.GetArgument(0,0,&result);

 	vtkClientServerStream stream3;
	// deleta o vtkHM1DStraightModelWriterPreProcessor
	stream3 << vtkClientServerStream::Delete << id
         << vtkClientServerStream::End;

	// envia stream para o servidor
	pm->SendStream(this->GetServers(), stream3);

	this->UpdateWidget();

	return result;
}

//----------------------------------------------------------------------------
double vtkSMHMStraightModelWidgetProxy::GetIniFileCurrentTime()
{

 vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
 vtkClientServerStream stream;

 vtkClientServerID id = this->GetID(0);


  stream << vtkClientServerStream::Invoke <<  id
         << "GetIniFileCurrentTime"
         << vtkClientServerStream::End;

 pm->SendStream(this->GetServers(), stream);

    const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
    double result=0.0;
    if (res.GetArgument(0,0,&result))
    {
       return result;
    }
    else
    {
	     vtkErrorMacro("double var result empty. Data Server has not returned any data in stream");
	     return 0;
    }
}
//----------------------------------------------------------------------------
double vtkSMHMStraightModelWidgetProxy::GetHeartFinalTime()
{

	return this->Widget->GetHeartFinalTime();


}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ColorTree()
{
	vtkPVDataInformation* info = this->SourceProxy->GetDataInformation();

	vtkHM1DStraightModelGrid *grid;

	this->Widget->ColorTree();

	// Pegar informacoes dos arrays relacionados com as linhas
	vtkPVDataSetAttributesInformation *dataSetInfo = info->GetCellDataInformation();

	vtkPVArrayInformation *arrayInfo = dataSetInfo->GetArrayInformation("Elastin");

	grid = this->Widget->GetGridLine();

	//Pegando range e array de cada propriedade para atualizar o
	//range do array de informação da barra de cores
	double *range;
	range = arrayInfo->GetComponentRange(0);

	vtkDoubleArray *property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Elastin"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("Collagen");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Collagen"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("CoefficientA");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("CoefficientA"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("CoefficientB");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("CoefficientB"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("Viscoelasticity");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Viscoelasticity"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("ViscoelasticityExponent");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("ViscoelasticityExponent"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("InfiltrationPressure");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("InfiltrationPressure"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("ReferencePressure");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("ReferencePressure"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	arrayInfo = dataSetInfo->GetArrayInformation("Permeability");
	property = vtkDoubleArray::SafeDownCast(grid->GetCellData()->GetArray("Permeability"));
	range = property->GetRange();
	arrayInfo->SetComponentRange(0, range[0], range[1]);

	// Pegar informacoes dos arrays relacionados com os pontos
	dataSetInfo = info->GetPointDataInformation();

	arrayInfo = dataSetInfo->GetArrayInformation("Radius");
	property = vtkDoubleArray::SafeDownCast(grid->GetPointData()->GetArray("Radius"));
	range = property->GetRange();

	arrayInfo->SetComponentRange(0, range[0], range[1]);
}
//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetTerminalProxyFromList(vtkDoubleArray *DoubleArray, bool factor, int SubTree )
{
	if (SubTree == MODE_TODETACHSUBTREE_VAL)
		{
		// referencia direta ao Widget!!!!!!!!!!!!!!!!!!!
	  // nao funciona distribuido pois o Widget esta no Render Server
		vtkIdList *SubList = this->GetSubTreeListTerminals();
//		this->IDList.clear();
		this->IDList->Reset();
		//Copia a SubList para a IDList para atualizar os propriedades dos segmentos
		for (int i=0; i< SubList->GetNumberOfIds();i++ )
			this->IDList->InsertUniqueId(SubList->GetId(i));
//			this->IDList.push_back(SubList->GetId(i));
		SubList->Delete();
		}

//	vtkSMHMStraightModelWidgetProxy::ListOfInt::iterator it = this->IDList.begin();

	// for que percorre todos elementos da lista
//	for (it = this->IDList.begin() ; it!=this->IDList.end() ; it++)
	for (int j=0 ; j<this->IDList->GetNumberOfIds(); j++)
		{

		vtkHM1DTreeElement *elem = this->StraightModel->GetTerminal(this->IDList->GetId(j));
		if(elem->IsLeaf())
			{
//			cout << "O ID: " << elem->GetId() << " e uma folha" << endl;

//			vtkIdType id = this->StraightModel->GetTerminalCSID(*it);
			vtkIdType id = this->StraightModel->GetTerminalCSID(this->IDList->GetId(j));

			this->TerminalProxy->SetTerminalCSID(id);
			vtkDoubleArray *ListaR1 = this->TerminalProxy->GetTerminalProp('R',1);
			vtkDoubleArray *ListaR2 = this->TerminalProxy->GetTerminalProp('R',2);
			vtkDoubleArray *ListaC = this->TerminalProxy->GetTerminalProp('C',1);
			vtkDoubleArray *ListaPt = this->TerminalProxy->GetTerminalProp('P',1);

	    if (factor) // se atualizacao é por fator multiplicativo
	    	{
	      for(int i=0; i<ListaR1->GetNumberOfTuples(); i++)
		    	ListaR1->SetValue(i, ListaR1->GetValue(i)*DoubleArray->GetValue(0));

	      for(int i=0; i<ListaR2->GetNumberOfTuples(); i++)
	      	ListaR2->SetValue(i, ListaR2->GetValue(i)*DoubleArray->GetValue(1));

	      for(int i=0; i<ListaC->GetNumberOfTuples(); i++)
	      	ListaC->SetValue(i, ListaC->GetValue(i)*DoubleArray->GetValue(2));

	      for(int i=0; i<ListaPt->GetNumberOfTuples(); i++)
	      	ListaPt->SetValue(i, ListaPt->GetValue(i)*DoubleArray->GetValue(3));
	      }

	    else // se atualizacao é por valor de prop. fixo
	     	{
	     	// valores nao definidos pelo usuario chegam aqui como -1
	     	// entao os valores dos elementos nao devem ser modificados
	     	if (DoubleArray->GetValue(0) != -1)
	     	  {
	     	  ListaR1->Reset();
	     	  ListaR1->SetNumberOfValues(1);
	     	  ListaR1->SetValue(0, DoubleArray->GetValue(0));
	     	  }

	     	if (DoubleArray->GetValue(1) != -1)
	     	  {
	     	  ListaR2->Reset();
	     	  ListaR2->SetNumberOfValues(1);
					ListaR2->SetValue(0, DoubleArray->GetValue(1));
	     	  }

	     	if (DoubleArray->GetValue(2) != -1)
	     	  {
	     	  ListaC->Reset();
	     	  ListaC->SetNumberOfValues(1);
					ListaC->SetValue(0, DoubleArray->GetValue(2));
	     	  }

	     	if (DoubleArray->GetValue(3) != -1)
	     	  {
	        ListaPt->Reset();
	        ListaPt->SetNumberOfValues(1);
					ListaPt->SetValue(0, DoubleArray->GetValue(3));
	     	  }
	     	}

	    this->TerminalProxy->SetTerminalProp(ListaR1, 'R', 1);
	    this->TerminalProxy->SetTerminalProp(ListaR2, 'R', 2);
	    this->TerminalProxy->SetTerminalProp(ListaC, 'C', 1);
	    this->TerminalProxy->SetTerminalProp(ListaPt, 'P', 1);
			ListaR1->Delete();
			ListaR2->Delete();
			ListaC->Delete();
			ListaPt->Delete();
			}
		}
	DoubleArray->Delete();
//	this->IDList.clear();
	this->IDList->Reset();
}
//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::SetSegmentProxyFromList(vtkDoubleArray *DoubleArray, bool factor, bool wholeTree, int SubTree )
{
    if (SubTree == MODE_TODETACHSUBTREE_VAL)
    	{
    	// referencia direta ao Widget!!!!!!!!!!!!!!!!!!!
	    // nao funciona distribuido pois o Widget esta no Render Server
    	vtkIdList *SubList = this->GetSubTreeListSegments();
//    	this->IDList.clear();
    	this->IDList->Reset();
    	//Copia a SubList para a IDList para atualizar as propriedades dos Terminais
    	for (int i=0; i< SubList->GetNumberOfIds();i++ )
    		this->IDList->InsertUniqueId(SubList->GetId(i));
//    		this->IDList.push_back(SubList->GetId(i));
    	SubList->Delete();
    	}

		vtkIntArray *CurrentSegmentInfoArray;
//	  vtkSMHMStraightModelWidgetProxy::ListOfInt::iterator it = this->IDList.begin();

	  // se todos terminais devem ser atualizados, o metodo abaixo pega do
	  // Straightmodel a lista de id de todos terminais da arvore
	  if (wholeTree)
	    this->GetSegmentsIDs();


	  // for que percorre todos elementos da lista
//	  for (it = this->IDList.begin() ; it!=this->IDList.end() ; it++)
	  for ( int j=0; j<this->IDList->GetNumberOfIds(); j++)
	  	{
	    // referencia direta ao StraightModel!!!!!!!!!!!!!!!!!!!
	    // nao funciona distribuido pois o 1DSTM esta no Data Server

//	    CurrentSegmentInfoArray = this->StraightModel->GetSegmentCSID(*it);
	    CurrentSegmentInfoArray = this->StraightModel->GetSegmentCSID(this->IDList->GetId(j));

	    int ElementOrder = 0;
	    int NodeOrder = 0;

	    while (ElementOrder <  CurrentSegmentInfoArray->GetValue(2))
	    	{
		    this->SegmentProxy->SetSegmentCSID(CurrentSegmentInfoArray->GetValue(0), ElementOrder, NodeOrder, CurrentSegmentInfoArray->GetValue(1),CurrentSegmentInfoArray->GetValue(2));

		    vtkHMElementData *Element = this->SegmentProxy->GetElementData(ElementOrder);

		    if (factor) // se atualizacao é por fator multiplicativo
		    	{
		      Element->Elastin = Element->Elastin*DoubleArray->GetValue(0);
					Element->Collagen = Element->Collagen*DoubleArray->GetValue(1);
					Element->CoefficientA = Element->CoefficientA*DoubleArray->GetValue(2);
					Element->CoefficientB = Element->CoefficientB*DoubleArray->GetValue(3);
					Element->Viscoelasticity = Element->Viscoelasticity*DoubleArray->GetValue(4);
					Element->ViscoelasticityExponent = Element->ViscoelasticityExponent*DoubleArray->GetValue(5);
					Element->InfiltrationPressure = Element->InfiltrationPressure*DoubleArray->GetValue(6);
					Element->ReferencePressure = Element->ReferencePressure*DoubleArray->GetValue(7);
					Element->Permeability = Element->Permeability*DoubleArray->GetValue(8);
		      }
		    else // se atualizacao é por valor de prop. fixo
		     	{
		     	// valores nao definidos pelo usuario chegam aqui como -1
		     	// entao os valores dos elementos nao devem ser modificados
		     	if (DoubleArray->GetValue(0) != -1)
		     		Element->Elastin = DoubleArray->GetValue(0);

		     	if (DoubleArray->GetValue(1) != -1)
						Element->Collagen = DoubleArray->GetValue(1);

		     	if (DoubleArray->GetValue(2) != -1)
						Element->CoefficientA = DoubleArray->GetValue(2);

		     	if (DoubleArray->GetValue(3) != -1)
						Element->CoefficientB = DoubleArray->GetValue(3);

		     	if (DoubleArray->GetValue(4) != -1)
						Element->Viscoelasticity = DoubleArray->GetValue(4);

		     	if (DoubleArray->GetValue(5) != -1)
						Element->ViscoelasticityExponent = DoubleArray->GetValue(5);

		     	if (DoubleArray->GetValue(6) != -1)
						Element->InfiltrationPressure = DoubleArray->GetValue(6);

		     	if (DoubleArray->GetValue(7) != -1)
						Element->ReferencePressure = DoubleArray->GetValue(7);

		     	if (DoubleArray->GetValue(8) != -1)
						Element->Permeability = DoubleArray->GetValue(8);
		     	}


		    vtkDoubleArray *array = vtkDoubleArray::New();
		    array->SetNumberOfValues(10);
		    array->SetValue(0, Element->Elastin);
		    array->SetValue(1, Element->Collagen);
		    array->SetValue(2, Element->CoefficientA);
		    array->SetValue(3, Element->CoefficientB);
		    array->SetValue(4, Element->Viscoelasticity);
		    array->SetValue(5, Element->ViscoelasticityExponent);
		    array->SetValue(6, Element->InfiltrationPressure);
		    array->SetValue(7, Element->ReferencePressure);
		    array->SetValue(8, Element->Permeability);
		    array->SetValue(9, 0); // wall thickness

		    this->SegmentProxy->SetSingleElem(array, NULL);


		    // ATUALIZACAO DOS NOS DOS SEGMENTOS

		    vtkHMNodeData *Node = this->SegmentProxy->GetNodeData(NodeOrder);


		    if (!factor)  // atualizacao por valor fixo
			    {
			    double Radius, Wall;

			    if (DoubleArray->GetValue(9) != -1) //se valor é diferente de -1 atualizar o valor do raio
			    	Radius = 	DoubleArray->GetValue(9);
			    else
			    	Radius = Node->radius;

			    if (DoubleArray->GetValue(10) != -1)
			    	Wall = DoubleArray->GetValue(10);
			    else
			    	Wall= (Node->alfa) / (vtkMath::Pi() * Node->radius);

			    this->SegmentProxy->SetSingleNode(Radius, Wall);
			    }
		    else // atualizacao por fator multiplicativo
		    	{
		    	double NewRadius, NewWall, OriginalWall;

		    	NewRadius = Node->radius *	DoubleArray->GetValue(9); //atualiza valor de prop
			    OriginalWall= (Node->alfa) / (vtkMath::Pi() * Node->radius);
		    	NewWall = OriginalWall * DoubleArray->GetValue(10); //atualiza valor prop
			    this->SegmentProxy->SetSingleNode(NewRadius, NewWall);
		    	}
		    NodeOrder++;
		    ElementOrder++;

	    	array->Delete();
	    	}
	    	CurrentSegmentInfoArray->Delete();
	  	}
	  	DoubleArray->Delete();

//	  	this->IDList.clear();
	  	this->IDList->Reset();
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RefineSubTreeMesh(int method, int factor, int mode)
{
  if ( mode == MODE_TODETACHSUBTREE_VAL )
    {
    vtkIdList *SubList = this->GetSubTreeListSegments();

    this->IDList->Reset();
    //Copia a SubList para a IDList para atualizar as propriedades dos Terminais
    for (int i=0; i< SubList->GetNumberOfIds();i++ )
      this->IDList->InsertUniqueId(SubList->GetId(i));

    SubList->Delete();
    }

  this->StraightModel->RefineSegment(this->IDList, method, factor);

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  vtkClientServerID id = this->GetID(0);
  stream << vtkClientServerStream::Invoke <<  id
         << "ConfigureLengthOfElements"
         << vtkClientServerStream::End;
  pm->SendStream(this->GetServers(), stream);

//  this->IDList->Reset();
}

//----------------------------------------------------------------------------


int vtkSMHMStraightModelWidgetProxy::CheckForNullViscoElasticElements()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  vtkClientServerID id = this->GetID(0);
  stream << vtkClientServerStream::Invoke <<  id
         << "CheckForNullViscoElasticElements"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
    return result;
    }
    else
    {
	  vtkErrorMacro("int var result empty. Data Server has not returned any data in stream");
	  return 0;
    }
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::CrtlKeyPressed()
{

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "CrtlKeyPressed"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  int result=0;
  if (res.GetArgument(0,0,&result))
  	{
    return result;
    }
    else
    {
	  vtkErrorMacro("int var result empty. Data Server has not returned any data in stream");
	  return 0;
    }
}

////----------------------------------------------------------------------------
//int vtkSMHMStraightModelWidgetProxy::SearchID(int NewID)
//{
////	vtkSMHMStraightModelWidgetProxy::ListOfInt::iterator it = this->IDList.begin();
////	for (it = this->IDList.begin();it!=this->IDList.end();it++)
////		{
////	    if (NewID==*it)
////	    	{
////	    	return 1;
////	    	break;
////	    	}
////	  }
//	return 0;
//}
////----------------------------------------------------------------------------
//
//int vtkSMHMStraightModelWidgetProxy::RemoveID(int id)
//{
////	vtkSMHMStraightModelWidgetProxy::ListOfInt::iterator it = this->IDList.begin();
////	for (it = this->IDList.begin();it!=this->IDList.end();it++)
////		{
////	    if (id==*it)
////	    	{
////	    	this->IDList.erase(it);
////	    	return 1;
////	    	break;
////	    	}
////	  }
//	return 0;
//}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetNumberOfGraphicsSum(int n)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetNumberOfGraphicsSum" << n
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetNumberOfGraphics(int n)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetNumberOfGraphics" << n
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------


vtkDoubleArray *vtkSMHMStraightModelWidgetProxy::GetDataOutInstantOfTime(int TimeStep)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

//  vtkClientServerID id = this->GetID(0);
  vtkClientServerID id = this->SourceProxy->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "GetDataOutTimeArray"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  const char *Result;
  if (res.GetArgument(0,0,&Result))
  	{
  	vtkDoubleArray *array = vtkHMUtils::ConvertCharToDoubleArray((char*)Result);
   	return array;
  	}
  else
    {
	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::GetDataOutInstantOfTime:: Data Server has not returned any data in stream");
	  return NULL;
   	}
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ColorPlot(int type)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "ColorPlot" << type
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetStraightModelMode(int mode)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
 vtkClientServerStream stream;

 vtkClientServerID id = this->GetID(0);


  stream << vtkClientServerStream::Invoke <<  id
         << "SetStraightModelMode" << mode
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::SetEditionMode(vtkIdType mode)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
 vtkClientServerStream stream;

 vtkClientServerID id = this->GetID(0);


  stream << vtkClientServerStream::Invoke <<  id
         << "SetEditionModeSelected" << mode
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  // As atualizações que são feitas no StraightModel dentro da classe widget são
	// refletidas no StraightModel do SM
	this->StraightModelObjectSelected = this->Widget->GetStraightModelObjectSelected();

	vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("StraightModelObjectSelected"));

	smSelect->SetElement(0, this->StraightModelObjectSelected);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetSurgicalInterventionsMode(vtkIdType mode)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;

	vtkClientServerID id = this->GetID(0);

	if ( mode == vtkHMStraightModelWidget::DeleteIntervention )
		{
		this->StraightModelObjectSelected = this->Widget->GetStraightModelObjectSelected();
		if ( this->StraightModelObjectSelected == CLIP )
			{
			this->RemoveClip();
			}
		else if ( this->StraightModelObjectSelected == STENT )
			{
			this->RemoveStent();
			}
		else if ( this->StraightModelObjectSelected == STENOSIS )
			{
			this->RemoveStenosis();
			}
		else if ( this->StraightModelObjectSelected == ANEURYSM )
			{
			this->RemoveAneurysm();
			}
		}

  stream << vtkClientServerStream::Invoke <<  id
         << "SetSurgicalInterventionsModeSelected" << mode
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  // As atualizações que são feitas no StraightModel dentro da classe widget são
	// refletidas no StraightModel do SM
	this->StraightModelObjectSelected = this->Widget->GetStraightModelObjectSelected();

	vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
    this->GetProperty("StraightModelObjectSelected"));

	smSelect->SetElement(0, this->StraightModelObjectSelected);
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::SetSegmentData(int numberOfElements, double length)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;

	vtkClientServerID id = this->GetID(0);


  stream << vtkClientServerStream::Invoke <<  id
         << "SetSegmentData" << numberOfElements << length
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());

  int result;
  res.GetArgument(0,0,&result);

  return result;
}

//----------------------------------------------------------------------------
vtkIdType vtkSMHMStraightModelWidgetProxy::GetStraightModelNodeNumber()
{
	if(this->Widget)
		return this->Widget->GetNodeSelected();
	else
		return -1;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetTreePressure(int pressure, double value)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;

	vtkClientServerID id = this->GetID(0);


  stream << vtkClientServerStream::Invoke <<  id
         << "SetTreePressure" << pressure << value
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

//  vtkSMStringVectorProperty* node0 = vtkSMStringVectorProperty::SafeDownCast(
//		this->GetProperty("BasicSegmentProperty"));

}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::GetSegmentsIDs()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "GetSegmentsID"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  const char *Result;
  if (res.GetArgument(0,0,&Result))
  	{
			//if (!strlen(Result)) return NULL;

//			this->IDList.clear();
  	this->IDList->Reset();

			int LastPosition =0, CurrentPosition=0, ElemNumber=0;
//	    vtkDoubleArray *array = vtkDoubleArray::New();
// 			array->SetNumberOfValues(TimeStep);
  		char temp[20];
  		// resetando a string auxiliar ************
		  for (int i=0; i < 20 ;i++)
		  	  temp[i]=0;
		  //*****************************************

	    while (Result[CurrentPosition])
	   		{
	   	  if (Result[CurrentPosition] == ';')
	   	  	{
	   	  	memcpy(temp, Result+LastPosition, CurrentPosition-LastPosition);
//	   	  	this->IDList.push_back(atoi(temp));
	   	  	this->IDList->InsertUniqueId(atoi(temp));

	   	  	LastPosition =CurrentPosition+1;
	   	  	ElemNumber++;
	   	  	//***************************
	   	  	for (int i=0; i < 20 ;i++)
     				temp[i]=0;
     			//***************************
	   	  	}
	   	  CurrentPosition++;
	   		}
	   	//return array;
  	}
  else
    {
	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::GetSegmentsCSID:: Data Server has not returned any data in stream");
	  //return NULL;
   	}

}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::CloneSegment(vtkStringArray *origArray, vtkStringArray *targetArray)
{

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	const char *orig, *target;

	for ( int i=0; i<origArray->GetNumberOfValues(); i++ )
		{
		orig = origArray->GetValue(i);
		target = targetArray->GetValue(i);

	  stream << vtkClientServerStream::Invoke <<  id
	         << "CloneSegment" << (char*)orig << (char*)target
	         << vtkClientServerStream::End;
		}

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
vtkStringArray* vtkSMHMStraightModelWidgetProxy::GenerateListOfSegmentName()
{
	vtkHM1DStraightModel::SegmentInformationMap segMap = this->StraightModel->GetSegmentNamesMap();
	vtkHM1DStraightModel::SegmentInformationMap::iterator it = segMap.begin();

	string s;
	//char *names;
	vtkStringArray *array = vtkStringArray::New();
	array->SetNumberOfValues(segMap.size());
	int i=0;

	std::list<string> listName;

	while ( it != segMap.end() )
		{
		array->InsertValue(i, it->first);
		it++;
		i++;
		}

	char aux[256], aux2[256];
	for ( i=0; i<array->GetNumberOfValues()-1; i++ )
		{
		strcpy(aux, array->GetValue(i));
		strcpy(aux2, array->GetValue(i+1));

		if ( strcmp(aux, aux2) == 1 )
			{
			array->SetValue(i, aux2);
			array->SetValue(i+1, aux);
			for ( int j=0; j<i; j++ )
				{
				strcpy(aux, array->GetValue(j));
				strcpy(aux2, array->GetValue(i));
				if ( strcmp(aux, aux2) == 1  )
					{
				array->SetValue(j, aux2);
				array->SetValue(i, aux);
					}
				}
			}
		}

	return array;
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::ConfigureScale(double v, char n)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "ConfigureScale" << v << n
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------

double *vtkSMHMStraightModelWidgetProxy::GetSize()
{
	vtkHM1DStraightModelGrid *grid = this->Widget->GetGridLine();
	double *size = new double[3];

	if(grid)
		{
		double *bds = grid->GetBounds();

		//calcula o tamanho em cada eixo
		size[0] = sqrt(pow(bds[1]-bds[0],2));
		size[1] = sqrt(pow(bds[3]-bds[2],2));
		size[2] = sqrt(pow(bds[5]-bds[4],2));
		}

	return size;
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::ConfigureScale(double *size)
{
	vtkHM1DStraightModelGrid *grid = this->Widget->GetGridLine();

	double *origSize = this->Widget->GetSizePerAxle();
	double *newSize = new double[3];

	if (origSize[0]==0)
		origSize[0]=1;
	if (origSize[1]==0)
		origSize[1]=1;
	if (origSize[2]==0)
		origSize[2]=1;

	//Calcula o tamanho de cada eixo em relação ao tamanho original.
	//O parametro size é passado em cm
	newSize[0] = size[0] / origSize[0];
	newSize[1] = size[1] / origSize[1];
	newSize[2] = size[2] / origSize[2];

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "ConfigureScale" << newSize[0] << newSize[1] << newSize[2]
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::InvertFlowDirection()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "InvertFlowDirection"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::TerminalsUpdateProps(int option, double R1, double R2, double Cap, double Pr)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "SetTerminals" << option << R1 << R2 << Cap << Pr
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::GenerateCouplingRepresentation(int NumberOfActors)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "GenerateCouplingRepresentation" << NumberOfActors
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::IncrementCouplingVar(int var)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "IncrementCouplingVar" << var
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::RemoveCouplingActor(int ActorNumber)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "RemoveCouplingActor" << ActorNumber
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::RemoveAllCouplingActors()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "RemoveAllCouplingActors"
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------

void vtkSMHMStraightModelWidgetProxy::SetFullModelCode(int code)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
         << "SetFullModelCode" << code
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::AddClip()
{
	vtkIdList *l = this->GetSegmentsWithClip();

	if ( l )
		this->StraightModel->AddClip(l->GetId(0), l->GetId(1));
	l->Delete();
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RemoveClip()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idClip;

  //Pegar o elemento selecionado
  stream << vtkClientServerStream::Invoke <<  id
             << "GetStraightModelElementSelected"
             << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  res.GetArgument(0,0,&idClip);

  //Verifica se tem clip selecionado
  if ( idClip != -1 )
  	{
  	//Remove o clip selecionado
    this->StraightModel->RemoveClip(idClip);
  	} // Fim if ( idClip != -1 )
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::AddStent(vtkDoubleArray *array)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idStent;
//  this->RemoveStent();

  vtkClientServerStream stream2;
  //Chama no widget o método para adicionar stent
	stream2 << vtkClientServerStream::Invoke <<  id
         << "AddStent" << array->GetValue(0)
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream2);

  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());


  res2.GetArgument(0,0,&idStent);

  vtkClientServerStream stream3;
	//pegar os ids dos elementos
	stream3 << vtkClientServerStream::Invoke <<  id
         << "GetElementsWithStent" << idStent
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream3);

  const vtkClientServerStream res3= pm->GetLastResult(this->GetServers());
  char *Result;
  //Pega as informações que chegam como string e converte para ids
  if (res3.GetArgument(0,0,&Result))
  	{
  	vtkIdList *elemList = vtkHMUtils::ConvertCharToIdList(Result);

  	if ( elemList )
  		{
	    //Adiciona stent
  		this->StraightModel->AddStent(this->Widget->GetSegmentSelected(), idStent, elemList);
	    this->SegmentProxy->AddStent(elemList, array->GetValue(1), array->GetValue(2));
	    elemList->Delete();
  		}
  	}
  else
    {
	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::AddStent:: Data Server has not returned any data in stream");
   	}

}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RemoveStent()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idStent;

  //Pegar o elemento selecionado
  stream << vtkClientServerStream::Invoke <<  id
             << "GetStraightModelElementSelected"
             << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  res.GetArgument(0,0,&idStent);

  //Verifica se tem stent selecionado, caso tenha, devo pegar os elementos do
  //stent, para poder remove-los.
  if ( idStent != -1 )
  	{
  	//Pega os ids dos elementos
  	vtkClientServerStream stream2;
	  stream2 << vtkClientServerStream::Invoke <<  id
	           << "GetElementsWithStent" << idStent
	           << vtkClientServerStream::End;

	  pm->SendStream(this->GetServers(), stream2);

	  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());
    char *Result;

    //Pega as informações que chegam como string e converte para ids
    if (res2.GetArgument(0,0,&Result))
    	{
    	vtkIdList *elemList = vtkHMUtils::ConvertCharToIdList(Result);
    	if ( elemList )
    		{
	      //Remove o stent selecionado
    		this->StraightModel->RemoveStent(this->Widget->GetSegmentSelected(), idStent);
	      this->SegmentProxy->RemoveStent(elemList);
	      elemList->Delete();
    		}
    	}

    else
      {
  	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::RemoveStent:: Data Server has not returned any data in stream");
     	}

  	} // Fim if ( idStent != -1 )
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::AddStenosis(vtkDoubleArray *array)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idStenosis;
//  this->RemoveStenosis();

  vtkClientServerStream stream2;
  //Chama no widget o método para adicionar stenosis
	stream2 << vtkClientServerStream::Invoke <<  id
         << "AddStenosis" << array->GetValue(0)
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream2);

  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());


  res2.GetArgument(0,0,&idStenosis);

  vtkClientServerStream stream3;
	//pegar os ids dos elementos
	stream3 << vtkClientServerStream::Invoke <<  id
         << "GetNodesWithStenosis" << idStenosis
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream3);

  const vtkClientServerStream res3= pm->GetLastResult(this->GetServers());
  char *Result;
  //Pega as informações que chegam como string e converte para ids
  if (res3.GetArgument(0,0,&Result))
  	{
  	vtkIdList *nodeList = vtkHMUtils::ConvertCharToIdList(Result);

  	if ( nodeList )
  		{
	    //Adiciona stenosis
  		this->StraightModel->AddStenosis(this->Widget->GetSegmentSelected(), idStenosis, nodeList);
	    this->SegmentProxy->AddStenosis(nodeList, array->GetValue(1));
	    nodeList->Delete();
  		}
  	}
  else
    {
	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::AddStenosis:: Data Server has not returned any data in stream");
   	}

}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RemoveStenosis()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idStenosis;

  //Pegar o elemento selecionado
  stream << vtkClientServerStream::Invoke <<  id
             << "GetStraightModelElementSelected"
             << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  res.GetArgument(0,0,&idStenosis);

  //Verifica se tem stenosis selecionado, caso tenha, devo pegar os nodes do
  //stenosis para poder remove-los.
  if ( idStenosis != -1 )
  	{
  	//Pega os ids dos elementos
  	vtkClientServerStream stream2;
	  stream2 << vtkClientServerStream::Invoke <<  id
	           << "GetNodesWithStenosis" << idStenosis
	           << vtkClientServerStream::End;

	  pm->SendStream(this->GetServers(), stream2);

	  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());
    char *Result;

    //Pega as informações que chegam como string e converte para ids
    if (res2.GetArgument(0,0,&Result))
    	{
    	vtkIdList *nodeList = vtkHMUtils::ConvertCharToIdList(Result);
    	if ( nodeList )
    		{
	      //Remove o stenosis selecionado
	      this->SegmentProxy->RemoveStenosis(nodeList);
	      nodeList->Delete();
    		}
    	}

    else
      {
  	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::RemoveStenosis:: Data Server has not returned any data in stream");
     	}

  	} // Fim if ( idStenosis != -1 )
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::AddAneurysm(vtkDoubleArray *array)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idAneurysm;
//  this->RemoveAneurysm();

  vtkClientServerStream stream2;
  //Chama no widget o método para adicionar Aneurysm
	stream2 << vtkClientServerStream::Invoke <<  id
         << "AddAneurysm" << array->GetValue(0)
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream2);

  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());


  res2.GetArgument(0,0,&idAneurysm);

  vtkClientServerStream stream3;
	//pegar os ids dos elementos
	stream3 << vtkClientServerStream::Invoke <<  id
         << "GetNodesWithAneurysm" << idAneurysm
         << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream3);

  const vtkClientServerStream res3= pm->GetLastResult(this->GetServers());
  char *Result;
  //Pega as informações que chegam como string e converte para ids
  if (res3.GetArgument(0,0,&Result))
  	{
  	vtkIdList *nodeList = vtkHMUtils::ConvertCharToIdList(Result);

  	if ( nodeList )
  		{
	    //Adiciona Aneurysm
  		this->StraightModel->AddAneurysm(this->Widget->GetSegmentSelected(), idAneurysm, nodeList);
	    this->SegmentProxy->AddAneurysm(nodeList, array->GetValue(1));
	    nodeList->Delete();
  		}
  	}
  else
    {
	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::AddAneurysm:: Data Server has not returned any data in stream");
   	}

}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RemoveAneurysm()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  vtkClientServerID id = this->GetID(0);

  vtkIdType idAneurysm;

  //Pegar o elemento selecionado
  stream << vtkClientServerStream::Invoke <<  id
             << "GetStraightModelElementSelected"
             << vtkClientServerStream::End;

  pm->SendStream(this->GetServers(), stream);

  const vtkClientServerStream res= pm->GetLastResult(this->GetServers());
  res.GetArgument(0,0,&idAneurysm);

  //Verifica se tem Aneurysm selecionado, caso tenha, devo pegar os nodes do
  //Aneurysm para poder remove-los.
  if ( idAneurysm != -1 )
  	{
  	//Pega os ids dos elementos
  	vtkClientServerStream stream2;
	  stream2 << vtkClientServerStream::Invoke <<  id
	           << "GetNodesWithAneurysm" << idAneurysm
	           << vtkClientServerStream::End;

	  pm->SendStream(this->GetServers(), stream2);

	  const vtkClientServerStream res2= pm->GetLastResult(this->GetServers());
    char *Result;

    //Pega as informações que chegam como string e converte para ids
    if (res2.GetArgument(0,0,&Result))
    	{
    	vtkIdList *nodeList = vtkHMUtils::ConvertCharToIdList(Result);
    	if ( nodeList )
    		{
	      //Remove o Aneurysm selecionado
	      this->SegmentProxy->RemoveAneurysm(nodeList);
	      nodeList->Delete();
    		}
    	}

    else
      {
  	  vtkErrorMacro("vtkSMHMStraightModelWidgetProxy::RemoveAneurysm:: Data Server has not returned any data in stream");
     	}

  	} // Fim if ( idAneurysm != -1 )
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::AddAging(vtkDoubleArray *array)
{
	this->StraightModel->SetAgingFactor(array->GetValue(0), array->GetValue(1));

//	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
//  vtkClientServerStream stream;
//
//  vtkClientServerID id = this->GetID(0);
//
//  this->RemoveAging();
//
//  //Chama no widget o método para adicionar Aging
//	stream << vtkClientServerStream::Invoke <<  id
//         << "AddAging" << array->GetValue(0) << array->GetValue(1)
//         << vtkClientServerStream::End;
//
//	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RemoveAging()
{
	this->StraightModel->RemoveAging();
//	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
//  vtkClientServerStream stream;
//
//  vtkClientServerID id = this->GetID(0);
//
//  stream << vtkClientServerStream::Invoke <<  id
//             << "RemoveAging"
//             << vtkClientServerStream::End;
//
//  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::RenderWidgetsWithDefaultColors()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;

	vtkClientServerID id = this->GetID(0);

	stream << vtkClientServerStream::Invoke <<  id
	       << "RenderSegmentsWithDefaultColors"
	       << vtkClientServerStream::End;

	pm->SendStream(this->GetServers(), stream);

	vtkClientServerStream stream2;

	stream2 << vtkClientServerStream::Invoke <<  id
	       << "ClearSubTreeLists"
	       << vtkClientServerStream::End;

	pm->SendStream(this->GetServers(), stream2);
}
//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ClearElementSelectedProxy()
{
	///////////////////////Stream
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "ClearElementSelected"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

	///////////////////////Stream
	vtkClientServerStream stream2;
	stream2 << vtkClientServerStream::Invoke <<  id
	       << "GetStraightModelObjectSelected"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream2);

	//Setar novas propriedades no XML
	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	vtkIdType Result;
	if (res.GetArgument(0,0,&Result))
		this->StraightModelObjectSelected = Result;

	vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
	  this->GetProperty("StraightModelObjectSelected"));

	smSelect->SetElement(0, this->StraightModelObjectSelected);

}

//----------------------------------------------------------------------------
vtkIdList *vtkSMHMStraightModelWidgetProxy::GetSegmentsWithClip()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "GetSegmentsWithClip" << this->StraightModelElementSelected
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
//	cout << "  ::GetSegmentsWithClip(): ElementSelected -> " << this->StraightModelElementSelected << endl;
	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	char *Result;
	if (res.GetArgument(0,0,&Result))
		{
		vtkIdList *l = vtkHMUtils::ConvertCharToIdList(Result);

		return l;
		}

	return NULL;
}

//----------------------------------------------------------------------------
vtkIdType vtkSMHMStraightModelWidgetProxy::GetSegmentCSID(vtkIdType id)
{
  vtkIntArray *array = this->StraightModel->GetSegmentCSID(id);
  vtkIdType csid = 0;
  if ( array )
  	{
  	csid = array->GetValue(0);
  	array->Delete();
  	}

  return csid;
}

//----------------------------------------------------------------------------
vtkIdType vtkSMHMStraightModelWidgetProxy::GetTerminalCSID(vtkIdType id)
{
  return this->StraightModel->GetTerminalCSID(id);
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::DeleteTerminalSelected()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "DeleteTerminalSelected"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	int Result;
	if (res.GetArgument(0,0,&Result))
		return Result;

	return 0;
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::DeleteSegmentSelected()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "DeleteSegmentSelected"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	int Result;
	if (res.GetArgument(0,0,&Result))
		return Result;

	return 0;
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::DeleteSegment(vtkIdType ID)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "DeleteSegment" << ID
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	int Result;
	if (res.GetArgument(0,0,&Result))
		return Result;

	return 0;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::DeleteSegment(vtkIdList *list)
{
	string array = vtkHMUtils::ConvertIdListToChar(list);

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "DeleteSegment" << array.c_str()
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ClearPlot()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "ClearPlot"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ReloadPlotActors()
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "ReloadPlotActors"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
int vtkSMHMStraightModelWidgetProxy::ReadSimulationData(const char *fileName)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;

  //pegando o reader
  vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));

  if ( !reader )
  	{
  	vtkClientServerID newId = pm->GetUniqueID();

  	stream << vtkClientServerStream::New
           << "vtkHM1DStraightModelReader" << newId
           << vtkClientServerStream::End;

  	stream << vtkClientServerStream::Invoke <<  newId
  	  	   << "SetFileName" << fileName
  	  	   << vtkClientServerStream::End;

  	stream << vtkClientServerStream::Invoke <<  newId
  	  	   << "Update" << vtkClientServerStream::End;

    pm->SendStream(this->GetServers(), stream);

    vtkHM1DKeepRemoveFilter *source = vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
    vtkHM1DStraightModelReader *newReader = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(newId));

    int n1 = source->GetOutput(0)->GetNumberOfPoints();
    int n2 = newReader->GetOutput()->GetNumberOfPoints();

    if ( n1 != n2 )
      {
      vtkErrorMacro(<< "Error in simulation data. Data not correspond the tree.")
      return 0;
      }
  	stream << vtkClientServerStream::Invoke <<  newId
  	       << "ReadSimulationData" << fileName
  	       << vtkClientServerStream::End;

    pm->SendStream(this->GetServers(), stream);

    const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
    int Result=0;

    if (res.GetArgument(0,0,&Result))
      Result = 1;

    stream << vtkClientServerStream::Invoke <<  newId
           << "GetDataOutTimeArray"
           << vtkClientServerStream::End
           << vtkClientServerStream::Invoke
           << this->SourceProxy->GetID(0) << "SetDataOutTimeArray"
           << vtkClientServerStream::LastResult
           << vtkClientServerStream::End;

    pm->SendStream(this->GetServers(), stream);

  	stream << vtkClientServerStream::Invoke
          << newId << "GetOutput"
          << vtkClientServerStream::End
          << vtkClientServerStream::Invoke
          << this->SourceProxy->GetID(0) << "SetOutput"
          << vtkClientServerStream::LastResult
          << vtkClientServerStream::End;

  	pm->SendStream(this->GetServers(), stream);

  	this->UpdateWidget();

  	return Result;

  	}
  else
  	{
		vtkClientServerID id = this->SourceProxy->GetID(0);
		stream << vtkClientServerStream::Invoke <<  id
		       << "ReadSimulationData" << fileName
		       << vtkClientServerStream::End;
		pm->SendStream(this->GetServers(), stream);
  	}

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	int Result;
	if (res.GetArgument(0,0,&Result))
		{
		return Result;
		}

	return 0;
}


double vtkSMHMStraightModelWidgetProxy::CalculateTreeVolume()
{
  if ( !this->StraightModel )
    return 0;

	double volume = this->StraightModel->CalculateTreeVolume();

//	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
//	vtkClientServerStream stream;
//	vtkClientServerID id = this->SourceProxy->GetID(0);
//	stream << vtkClientServerStream::Invoke <<  id
//	       << "ReadSimulationData" << fileName
//	       << vtkClientServerStream::End;
//	pm->SendStream(this->GetServers(), stream);
//
//	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
//	int Result;
//	if (res.GetArgument(0,0,&Result))
//		{
//		return Result;
//		}

	return volume;
}

//----------------------------------------------------------------------------
bool vtkSMHMStraightModelWidgetProxy::HasNullElements1D3D()
{
	return this->StraightModel->HasNullElements1D3D();
}

//----------------------------------------------------------------------------
vtkIdType vtkSMHMStraightModelWidgetProxy::GetSmallerSegment()
{

	vtkIdType id = this->StraightModel->GetSmallerSegment();

	vtkIntArray *array = this->StraightModel->GetSegmentCSID(id);

	this->SegmentProxy->SetSegmentCSID(array->GetValue(0), 0, 0, array->GetValue(1),array->GetValue(2));

	array->Delete();

	return id;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::HighlightSegment(vtkIdType idSegment)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "HighlightSegment" << idSegment
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::HighlightSegment(char *Name)
{
	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "HighlightSegment" << Name
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ConfigureDefaultLengthOfElements(double length)
{
	this->StraightModel->ConfigureDefaultLengthOfElements(length);

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "ConfigureDefaultLengthOfElements"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::CopySegment()
{
	if ( this->IDList->GetNumberOfIds() > 0 )
		{
		vtkIdList * newSegmentsList = this->StraightModel->CopySegments(this->IDList);

		vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

		string array = vtkHMUtils::ConvertIdListToChar(newSegmentsList);

		vtkClientServerStream stream;
		vtkClientServerID id = this->GetID(0);
		stream << vtkClientServerStream::Invoke <<  id
		       << "CopySegments" << array.c_str()
		       << vtkClientServerStream::End;
		pm->SendStream(this->GetServers(), stream);


		if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			vtkClientServerStream stream2;

			stream2 << vtkClientServerStream::Invoke <<  id
			       << "RenderSegmentsWithDefaultColors"
			       << vtkClientServerStream::End;

			stream2 << vtkClientServerStream::Invoke <<  id
			       << "ToDetachSubTree"
			       << vtkClientServerStream::End;

			pm->SendStream(this->GetServers(), stream2);
			}
		else
			{
			vtkClientServerStream stream2;

			for ( int i=0; i<this->IDList->GetNumberOfIds(); i++ )
				stream2 << vtkClientServerStream::Invoke <<  id
				       << "HighlightSegment" << this->IDList->GetId(i)
				       << vtkClientServerStream::End;

			pm->SendStream(this->GetServers(), stream2);
			}

		newSegmentsList->Delete();
		}
}
//----------------------------------------------------------------------------
vtkIdList *vtkSMHMStraightModelWidgetProxy::GetSubTreeListSegments()
{

vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
vtkClientServerStream stream;
vtkClientServerID id = this->GetID(0);
stream << vtkClientServerStream::Invoke <<  id
       << "GetSubTreeListSegments"
       << vtkClientServerStream::End;
pm->SendStream(this->GetServers(), stream);

const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
char *Result;

if (res.GetArgument(0,0,&Result))
	{
	vtkIdList *l = vtkHMUtils::ConvertCharToIdList(Result);

	return l;
	}

return NULL;

//return this->Widget->GetSubTreeListSegment();
}

//----------------------------------------------------------------------------
vtkIdList *vtkSMHMStraightModelWidgetProxy::GetSubTreeListTerminals()
{

vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
vtkClientServerStream stream;
vtkClientServerID id = this->GetID(0);
stream << vtkClientServerStream::Invoke <<  id
       << "GetSubTreeListTerminals"
       << vtkClientServerStream::End;
pm->SendStream(this->GetServers(), stream);

const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
char *Result;

if (res.GetArgument(0,0,&Result))
	{
	vtkIdList *l = vtkHMUtils::ConvertCharToIdList(Result);

	return l;
	}

return NULL;

//return this->Widget->GetSubTreeListSegment();
}

void *vtkSMHMStraightModelWidgetProxy::SetSegment(vtkIdType element)
{
//this->SegmentProxy->SetSegmentCSID(array->GetValue(0), this->Widget->GetElementSelected(), -1, array->GetValue(1),array->GetValue(2));
vtkIntArray *array = this->StraightModel->GetSegmentCSID(element);
this->SegmentProxy->SetSegmentCSID(array->GetValue(0), 0, -1, array->GetValue(1),array->GetValue(2));
array->Delete();
return NULL;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::FlipSegments()
{
	if ( this->IDList->GetNumberOfIds() > 0 )
		{
		vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

		this->StraightModel->FlipSegments(this->IDList);

		string array = vtkHMUtils::ConvertIdListToChar(this->IDList);

		vtkClientServerStream stream;
		vtkClientServerID id = this->GetID(0);
		stream << vtkClientServerStream::Invoke <<  id
		       << "FlipSegments" << array.c_str()
		       << vtkClientServerStream::End;
		pm->SendStream(this->GetServers(), stream);


		if ( this->Widget->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
			vtkClientServerStream stream2;

			stream2 << vtkClientServerStream::Invoke <<  id
			       << "RenderSegmentsWithDefaultColors"
			       << vtkClientServerStream::End;

			stream2 << vtkClientServerStream::Invoke <<  id
			       << "ToDetachSubTree"
			       << vtkClientServerStream::End;

			pm->SendStream(this->GetServers(), stream2);
			}
		else
			{
			vtkClientServerStream stream2;

			for ( int i=0; i<this->IDList->GetNumberOfIds(); i++ )
				stream2 << vtkClientServerStream::Invoke <<  id
				       << "HighlightSegment" << this->IDList->GetId(i)
				       << vtkClientServerStream::End;

			pm->SendStream(this->GetServers(), stream2);
			}

		}
}

//----------------------------------------------------------------------------
double vtkSMHMStraightModelWidgetProxy::GetEquivalentResistance()
{
//Retorna a Resistência Equivalente da sub-árvore
	vtkIdList *subTreeListTerminals = this->GetSubTreeListTerminals();
	double eqRes=0;
	double r1=0, r2=0;
	for(int i=0; i<subTreeListTerminals->GetNumberOfIds(); i++)//percorre a lista de segmentos
		{
//		if(this->IsLeaf(subTreeListTerminals->GetId(i)))//verifica se é uma folha
//			{
		vtkIdType id = this->StraightModel->GetTerminalCSID(subTreeListTerminals->GetId(i));//pega o CSID do id
		this->TerminalProxy->SetTerminalCSID(id);//Seta o CSID -> Faz com que o id seja marcado como selecionado
		vtkDoubleArray *R1 = this->TerminalProxy->GetTerminalProp('R',1);
		vtkDoubleArray *R2 = this->TerminalProxy->GetTerminalProp('R',2);
		r1 = R1->GetValue(0);
		r2 = R2->GetValue(0);
		eqRes += 1/(r1+r2);
		R1->Delete();
		R2->Delete();
//			}
		}
//	eqRes += 1/(r1+r2);
	subTreeListTerminals->Delete();
	return eqRes;
}

//----------------------------------------------------------------------------
double vtkSMHMStraightModelWidgetProxy::GetEquivalentCapacitance()
{
//Retorna a Capacitancia Equivalente da sub-árvore
	vtkIdList *subTreeListTerminals = this->GetSubTreeListTerminals();
	vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();
	double cap=0;

	//soma todas as capacitâncias dos terminais
	for(int i=0; i<subTreeListTerminals->GetNumberOfIds(); i++)
		{
//		if(this->IsLeaf(subTreeListTerminals->GetId(i)))
//			{
		vtkIdType id = this->StraightModel->GetTerminalCSID(subTreeListTerminals->GetId(i));
		this->TerminalProxy->SetTerminalCSID(id);
		vtkDoubleArray *Capacitance = this->TerminalProxy->GetTerminalProp('C',1);
		cap += Capacitance->GetValue(0);
		Capacitance->Delete();
//			}
		}

	//A capacitância do segmento chama-se compliance
	//soma a compliance dos segmentos à capacitância dos terminais.
	for(int i=0; i<subTreeListSegments->GetNumberOfIds(); i++)
		{
		vtkIntArray *id = this->StraightModel->GetSegmentCSID(subTreeListSegments->GetId(i));
		this->SegmentProxy->SetSegmentCSID(id->GetValue(0), -1, -1, id->GetValue(1), id->GetValue(2));
		vtkDoubleArray *Compliance = this->SegmentProxy->GetSegmentBasicInfo();
		cap += Compliance->GetValue(17);
		Compliance->Delete();
		id->Delete();
		}

	subTreeListTerminals->Delete();
	subTreeListSegments->Delete();
	return cap;
}

//----------------------------------------------------------------------------
bool vtkSMHMStraightModelWidgetProxy::IsLeaf(vtkIdType terminal)
{
//Retorna TRUE se um terminal for uma folha e FALSE caso contrário.
	vtkHM1DTreeElement *elem = this->StraightModel->GetTerminal(terminal);
	if(elem->IsLeaf())
		return true;
	return false;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHMStraightModelWidgetProxy::GetMaxRadiusInSubTree()
{
//Retorna um double array com:
//maior Raio da sub-arvore
//o ID do segmento em que se encontra o maior raio
//o numero do nó do segmento em que se encontra o maior raio
	vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();
	double maxRadius = 0;
	int idSeg, idNode;

//	for para percorrer os segmentos da lista da sub-arvore
	for(int i=0; i<subTreeListSegments->GetNumberOfIds(); i++)
		{
		//		Retorna um int array com as informações de:
		//		id->GetValue(0) = Id do segmento que está selecionado
		//		id->GetValue(1) = numero de nós
		//		id->GetValue(2) = numero de elementos
		vtkIntArray *id = this->StraightModel->GetSegmentCSID(subTreeListSegments->GetId(i));

		this->SegmentProxy->SetSegmentCSID(id->GetValue(0), -1, -1, id->GetValue(1), id->GetValue(2));
		//		for para percorrer os nós dos segmentos da lista de sub-arvore
		for(int j=0; j<id->GetValue(1); j++)
			{
			//			Retorna um DoubleArray com as informações do nó:
			//			Node->GetValue(0) = wallthickness
			//			Node->GetValue(1) = CurrentNode->radius
			//			Node->GetValue(2) = CurrentNode->coords[0]
			//			Node->GetValue(3) = CurrentNode->coords[1]
			//			Node->GetValue(4) = CurrentNode->coords[2]
			//			Node->GetValue(5) = CurrentNode->meshID
			vtkDoubleArray *Node = this->SegmentProxy->GetNodeInfo(j);
			if(maxRadius < Node->GetValue(1))
				{
				maxRadius = Node->GetValue(1);
				idSeg = subTreeListSegments->GetId(i);
				idNode = j;
				}
			Node->Delete();
			}
		id->Delete();
		}
	subTreeListSegments->Delete();

	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfValues(3);

	array->InsertValue(0, maxRadius);
	array->InsertValue(1, double(idSeg));
	array->InsertValue(2, double(idNode));

	return array;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHMStraightModelWidgetProxy::GetMinRadiusInSubTree()
{
//Retorna um double array com:
//maior Raio da sub-arvore
//o ID do segmento em que se encontra o maior raio
//o numero do nó do segmento em que se encontra o maior raio
	vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();
	double minRadius = 10000000;
	int idSeg, idNode;

//	for para percorrer os segmentos da lista da sub-arvore
	for(int i=0; i<subTreeListSegments->GetNumberOfIds(); i++)
		{
		//		Retorna um int array com as informações de:
		//		id->GetValue(0) = Id do segmento que está selecionado
		//		id->GetValue(1) = numero de nós
		//		id->GetValue(2) = numero de elementos
		vtkIntArray *id = this->StraightModel->GetSegmentCSID(subTreeListSegments->GetId(i));

		this->SegmentProxy->SetSegmentCSID(id->GetValue(0), -1, -1, id->GetValue(1), id->GetValue(2));
		//		for para percorrer os nós dos segmentos da lista de sub-arvore
		for(int j=0; j<id->GetValue(1); j++)
			{
			//			Retorna um DoubleArray com as informações do nó:
			//			Node->GetValue(0) = wallthickness
			//			Node->GetValue(1) = CurrentNode->radius
			//			Node->GetValue(2) = CurrentNode->coords[0]
			//			Node->GetValue(3) = CurrentNode->coords[1]
			//			Node->GetValue(4) = CurrentNode->coords[2]
			//			Node->GetValue(5) = CurrentNode->meshID
			vtkDoubleArray *Node = this->SegmentProxy->GetNodeInfo(j);
			if(minRadius > Node->GetValue(1))
				{
				minRadius = Node->GetValue(1);
				idSeg = subTreeListSegments->GetId(i);
				idNode = j;
				}
			Node->Delete();
			}
		id->Delete();
		}
	subTreeListSegments->Delete();

	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfValues(3);

	array->InsertValue(0, minRadius);
	array->InsertValue(1, double(idSeg));
	array->InsertValue(2, double(idNode));

	return array;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHMStraightModelWidgetProxy::GetMaxWallThicknessInSubTree()
{
//Retorna um double array com:
//maior WallThickness da sub-arvore
//o ID do segmento em que se encontra o maior WallThickness
//o numero do nó do segmento em que se encontra o maior WallThickness
	vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();
	double maxWall = 0;
	int idSeg, idNode;

//	for para percorrer os segmentos da lista da sub-arvore
	for(int i=0; i<subTreeListSegments->GetNumberOfIds(); i++)
		{
		//		Retorna um int array com as informações de:
		//		id->GetValue(0) = Id do segmento que está selecionado
		//		id->GetValue(1) = numero de nós
		//		id->GetValue(2) = numero de elementos
		vtkIntArray *id = this->StraightModel->GetSegmentCSID(subTreeListSegments->GetId(i));

		this->SegmentProxy->SetSegmentCSID(id->GetValue(0), -1, -1, id->GetValue(1), id->GetValue(2));
		//		for para percorrer os nós dos segmentos da lista de sub-arvore
		for(int j=0; j<id->GetValue(1); j++)
			{
			//			Retorna um DoubleArray com as informações do nó:
			//			Node->GetValue(0) = wallthickness
			//			Node->GetValue(1) = CurrentNode->radius
			//			Node->GetValue(2) = CurrentNode->coords[0]
			//			Node->GetValue(3) = CurrentNode->coords[1]
			//			Node->GetValue(4) = CurrentNode->coords[2]
			//			Node->GetValue(5) = CurrentNode->meshID
			vtkDoubleArray *Node = this->SegmentProxy->GetNodeInfo(j);
			if(maxWall < Node->GetValue(0))
				{
				maxWall = Node->GetValue(0);
				idSeg = subTreeListSegments->GetId(i);
				idNode = j;
				}
			Node->Delete();
			}
		id->Delete();
		}
	subTreeListSegments->Delete();

	vtkDoubleArray *array = vtkDoubleArray::New();
	array->SetNumberOfValues(3);

	array->InsertValue(0, maxWall);
	array->InsertValue(1, double(idSeg));
	array->InsertValue(2, double(idNode));

	return array;
}

//----------------------------------------------------------------------------
vtkDoubleArray *vtkSMHMStraightModelWidgetProxy::GetMinWallThicknessInSubTree()
{
//Retorna um double array com:
//maior WallThickness da sub-arvore
//o ID do segmento em que se encontra o maior WallThickness
//o numero do nó do segmento em que se encontra o maior WallThickness
	vtkIdList *subTreeListSegments = this->GetSubTreeListSegments();
	double minWall = 10000000;
	int idSeg, idNode;

//	for para percorrer os segmentos da lista da sub-arvore
	for(int i=0; i<subTreeListSegments->GetNumberOfIds(); i++)
		{
		//		Retorna um int array com as informações de:
		//		id->GetValue(0) = Id do segmento que está selecionado
		//		id->GetValue(1) = numero de nós
		//		id->GetValue(2) = numero de elementos
		vtkIntArray *id = this->StraightModel->GetSegmentCSID(subTreeListSegments->GetId(i));

		this->SegmentProxy->SetSegmentCSID(id->GetValue(0), -1, -1, id->GetValue(1), id->GetValue(2));
		//		for para percorrer os nós dos segmentos da lista de sub-arvore
		for(int j=0; j<id->GetValue(1); j++)
			{
			//			Retorna um DoubleArray com as informações do nó:
			//			Node->GetValue(0) = wallthickness
			//			Node->GetValue(1) = CurrentNode->radius
			//			Node->GetValue(2) = CurrentNode->coords[0]
			//			Node->GetValue(3) = CurrentNode->coords[1]
			//			Node->GetValue(4) = CurrentNode->coords[2]
			//			Node->GetValue(5) = CurrentNode->meshID
			vtkDoubleArray *Node = this->SegmentProxy->GetNodeInfo(j);
			if(minWall > Node->GetValue(0))
				{
				minWall = Node->GetValue(0);
				idSeg = subTreeListSegments->GetId(i);
				idNode = j;
				}
			Node->Delete();
			}
		id->Delete();
		}
	subTreeListSegments->Delete();

	vtkDoubleArray *array = vtkDoubleArray::New();

	array->SetNumberOfValues(3);

	array->InsertValue(0, minWall);
	array->InsertValue(1, double(idSeg));
	array->InsertValue(2, double(idNode));

	return array;
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::HighlightSelectedActors(int opcao)
{
vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
vtkClientServerStream stream;

vtkClientServerID id = this->GetID(0);

stream << vtkClientServerStream::Invoke <<  id
       << "HighlightSelectedActors" << opcao
       << vtkClientServerStream::End;

pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
vtkIdList *vtkSMHMStraightModelWidgetProxy::RestartSubTree()
{
//vtkIdType id = this->StraightModel->GetTerminalCSID(this->StraightModel->Get1DTreeRoot()->GetId());
//this->TerminalProxy->SetTerminalCSID(id);

//vtkSMIntVectorProperty* smSelect = vtkSMIntVectorProperty::SafeDownCast(
//  this->GetProperty("StraightModelObjectSelected"));
//
//smSelect->SetElement(0, this->StraightModelObjectSelected);

	vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
	vtkClientServerStream stream;
	vtkClientServerID id = this->GetID(0);
	stream << vtkClientServerStream::Invoke <<  id
	       << "RestartSubTree"
	       << vtkClientServerStream::End;
	pm->SendStream(this->GetServers(), stream);

	const vtkClientServerStream res= pm->GetLastResult(this->GetServers());//Pega a ultima stream entre cliente e servidor
	char *Result;

	if (res.GetArgument(0,0,&Result))
		{
		vtkIdList *l = vtkHMUtils::ConvertCharToIdList(Result);

		return l;
		}

	return NULL;

}

//-----------------------------------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::SetTermPropAfterDelSubTree(vtkHM1DTreeElement *segmParent, double capEq, double resEq)
{
//	cout << "segmParent: " << segmParent->GetId() << endl;
//	cout << "termParent: " << segmParent->GetFirstChild()->GetId() << endl;
	//pego o CSID do terminal parent
	vtkIdType id = this->StraightModel->GetTerminalCSID(segmParent->GetFirstChild()->GetId());
	//seto o terminal parent como selecionado
	this->TerminalProxy->SetTerminalCSID(id);

	vtkDoubleArray *capacitance = this->TerminalProxy->GetTerminalProp('C', 0);

	//coloco o valor da capacitancia em um DoubleArray para poder setar na funcao
	vtkDoubleArray *Cap = vtkDoubleArray::New();
	Cap->InsertValue(0, capEq+capacitance->GetValue(0));
	//seto o valor da capacitancia no terminal selecionado
	this->TerminalProxy->SetTerminalProp(Cap, 'C', 0);
	Cap->Delete();


	vtkDoubleArray *R1Terminal = this->TerminalProxy->GetTerminalProp('R', 1);
	vtkDoubleArray *R2Terminal = this->TerminalProxy->GetTerminalProp('R', 2);

	double rTerminal = 1 / (R1Terminal->GetValue(0)+R2Terminal->GetValue(0));

	resEq = 1 / (rTerminal + 1/resEq);

	//pego o valor de resistencia equivalente
	//coloco 20% desse valor para r1 e 80% para r2
	double r1 = resEq * 0.2;
	double r2 = resEq * 0.8;

	//coloco os valores de Resistencia em dois DoubleArrays para poder setar nas funcoes
	vtkDoubleArray *R1 = vtkDoubleArray::New();
	vtkDoubleArray *R2 = vtkDoubleArray::New();
	R1->InsertValue(0, r1);
	R2->InsertValue(0, r2);
	//seto os valores de R1 e R2 no terminal selecionado
	this->TerminalProxy->SetTerminalProp(R1, 'R', 1);
	this->TerminalProxy->SetTerminalProp(R2, 'R', 2);

	R1->Delete();
	R2->Delete();
	R1Terminal->Delete();
	R2Terminal->Delete();
	capacitance->Delete();
}

//-----------------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkSMHMStraightModelWidgetProxy::GetSegment(vtkIdType subTree)
{
return this->StraightModel->GetSegment(subTree);
}

//-----------------------------------------------------------------------------------------------------
vtkHM1DTreeElement *vtkSMHMStraightModelWidgetProxy::GetTerminal(vtkIdType subTree)
{
return this->StraightModel->GetTerminal(subTree);
}


int *vtkSMHMStraightModelWidgetProxy::GetPathTerminals()
{

terminals[0] = this->Widget->GetBeginTerminal();
terminals[1] = this->Widget->GetEndTerminal();

return terminals;


}



//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::ResetPathTree()
{

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  vtkClientServerID id = this->GetID(0);

  stream << vtkClientServerStream::Invoke <<  id
         << "ResetPathTree"
         << vtkClientServerStream::End;
  pm->SendStream(this->GetServers(), stream);

  pm->SendStream(this->GetServers(), stream);
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::KeepRemoveSegments(int mode)
{
  vtkIdList *list = this->GetSubTreeListSegments();

  if ( list )
    {
    vtkProcessModule* pm = vtkProcessModule::GetProcessModule();

    vtkHM1DKeepRemoveFilter *source = vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));
    source->SetSegmentsSelected(list);
    source->SetKeepRemoveMode(mode);

    double finalTime = this->GetHeartFinalTime();
    source->SetCardiacCycleTime(finalTime);

    double resistance = this->GetEquivalentResistance();
    double capacitance = this->GetEquivalentCapacitance();

    source->SetEquivalentResistance(1/resistance);
    source->SetEquivalentCapacitance(capacitance);

    list->Delete();
    }
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::UpdateWidget()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkHM1DKeepRemoveFilter *source = vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(this->SourceProxy->GetID(0)));

  this->Widget->SetGridLine(source->GetOutput(0));

  this->Widget->ClearSubTreeLists();
  this->Widget->RenderSegmentsWithDefaultColors();
  this->Widget->ClearElementSelected();

  this->StraightModel = this->Widget->GetStraightModel();
}

//----------------------------------------------------------------------------
void vtkSMHMStraightModelWidgetProxy::KeepSubTree(vtkIdList *list)
{
  string array = vtkHMUtils::ConvertIdListToChar(list);

  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;
  vtkClientServerID id = this->GetID(0);
  stream << vtkClientServerStream::Invoke <<  id
         << "KeepSubTree" << array.c_str()
         << vtkClientServerStream::End;
  pm->SendStream(this->GetServers(), stream);
}
