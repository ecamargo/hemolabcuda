#ifndef __vtkSMHMStraightModelWidgetProxy_h
#define __vtkSMHMStraightModelWidgetProxy_h

#include "vtkSM3DWidgetProxy.h"
#include "vtkSMHM1DSegmentProxy.h"
#include "vtkSMHM1DTerminalProxy.h"
#include <list>


class vtkHMStraightModelWidget;
class vtkHM1DStraightModel;
class vtkHM1DStraightModelSource;
class vtkHM1DStraightModelReader;

class vtkSMSourceProxy;
class vtkIntArray;
class vtkStringArray;

class VTK_EXPORT vtkSMHMStraightModelWidgetProxy : public vtkSM3DWidgetProxy
{
public:
	//BTX
	typedef std::list<int> ListOfInt;
	//ETX


  static vtkSMHMStraightModelWidgetProxy* New();
  vtkTypeRevisionMacro(vtkSMHMStraightModelWidgetProxy, vtkSM3DWidgetProxy);
  void PrintSelf(ostream &os,vtkIndent indent);

  vtkIdType GetStraightModelElementSelected() {return this->StraightModelElementSelected;};
  vtkSetMacro(StraightModelElementSelected, vtkIdType);

  vtkIdType GetStraightModelObjectSelected() {return this->StraightModelObjectSelected;};
  vtkSetMacro(StraightModelObjectSelected, vtkIdType);

  // Description:
  // Returns the number of the selected node
  vtkIdType GetStraightModelNodeNumber();

  virtual void SaveInBatchScript(ofstream *file);
  virtual void UpdateVTKObjects();

  int SetSourceProxy(vtkSMSourceProxy *sp);

  //Description:
  // after any  event, this method is called
  // in order to update the properties from the selected tree element (node, terminal or element )
  void UpdateWidgetProperties(void);

 //Description:
 // method used to result the number of graphs
 void SetNumberGraphs(int value);

  //Description:
  // Get and updates the properties number of terminals, segments and nodes.
  void GetTreeGeneralProperties(void);

 // Description:
  // Geometry Methods
//  void AutoResize();
//	void GetHandleValues(double *values);

	// Description:
	// Update Sphere Resolution and Radius
//	void ChangeSphereSettings(double, double);

	// Description:
	// Update Tube Sides and Radius
//	void ChangeTubeSettings(double, double);

	// Description:
	// used to set widget visualization /edition mode
	// when element is set only the current element is highlighted
	// when the segment mode is selected, all the elements from
	// the current segment will be highlighted
	void SelectTreeElement(int element, int segment);

  // Description:
	// return reference to the Segment Proxy
  vtkSMHM1DSegmentProxy *GetSegmentProxy(void);


  // Description:
	// return reference to the Terminal Proxy
	vtkSMHM1DTerminalProxy *GetTerminalProxy(void);

  // Description:
	// Initializes the 1D Solver files writing process. This method receives a double and a integer array with the values
	// from Configuration window. The parameter Path indicates where the solver files will be genereated
  int GenerateSolverFiles(vtkDoubleArray *DoubleParam, vtkIntArray *IntParam, const char *path, const char *LogFilename, vtkIntArray *WhichFile);


  // Description:
	//
  double GetIniFileCurrentTime();


  // Description:
	//
  double GetHeartFinalTime();

	// Description:
	// Method for color tree in function of the property selected
	// (Elastin, Collagen, Viscoelasticity, etc).
	void ColorTree();

	// Description:
	// Method to color node, terminal or segment in the same color of the curve of plot
	//control a type of the graphic: normal or animated plot graphic.
	void ColorPlot(int type);

	void SetNumberOfGraphicsSum(int n);
	void SetNumberOfGraphics(int n);

	// Description:
	//
  //void SetSegmentProxyFromList(vtkDoubleArray *DoubleArray, bool factor);
  void SetSegmentProxyFromList(vtkDoubleArray *DoubleArray, bool factor, bool wholeTree, int SubTree );

	// Description:
	//
  //void SetTerminalProxyFromList(vtkDoubleArray *DoubleArray, bool factor);
  void SetTerminalProxyFromList(vtkDoubleArray *DoubleArray, bool factor, int SubTree );

	// Description:
	// returns 1 if some element in the StraightModel tree has null viscoelasticity
  int CheckForNullViscoElasticElements();

	// Description:
	// checks if Ctrl key is currentlly pressed in vtkRenderWindowInteractor
  int CrtlKeyPressed();

//  // Description:
//	//
//  int SearchID(int NewID);
//
//  // Description:
//	//
//  int RemoveID(int id);


  // Description:
	//
  vtkDoubleArray *GetDataOutInstantOfTime(int TimeStep);

	// Description:
	//
	void SetStraightModelMode(int mode);

	// Description:
	//
	void SetEditionMode(vtkIdType mode);

	// Description:
	//
	void SetSurgicalInterventionsMode(vtkIdType mode);

	// Description:
	//
	int SetSegmentData(int numberOfElements, double length);

	// Description:
	//
	void SetTreePressure(int pressure, double value);

	// Description:
	//
	void GetSegmentsIDs();

	// Description:
	// Clone all properties of one segment to other.
	void CloneSegment(vtkStringArray *origArray, vtkStringArray *targetArray);

	// Description:
	// Generate list of segment name
	vtkStringArray* GenerateListOfSegmentName();

	// Description:
	// Configure scale of the grid based in coordinates
	// v = value
	// n = axle 'x', 'y', 'z'
	void ConfigureScale(double v, char n);

	// Description:
	// Configure scale of the grid based in size
	void ConfigureScale(double *size);

	// Description:
	// Return size of the grid.
	// size[0] = axle x.
	// size[1] = axle y.
	// size[2] = axle z.
	double *GetSize();

	// Description:
	// Inver flow direction
	void InvertFlowDirection();

	// Description:
	//
	void TerminalsUpdateProps(int option, double R1, double R2, double Cap, double Pr);

	// Description:
	//	Generate the representation of coupling spheres  -- *** USED ONLY WHEN THE 1D MODELS IS BEING COUPLED WITH A 3D MODEL ***
	void GenerateCouplingRepresentation(int NumberOfActors);

	// Description:
	//	Increments the ivar that counts the number of coupling actors -- *** USED ONLY WHEN THE 1D MODELS IS BEING COUPLED WITH A 3D MODEL ***
	void IncrementCouplingVar(int var);

	// Description:
	//	Remove some coupling actor -- *** USED ONLY WHEN THE 1D MODELS IS BEING COUPLED WITH A 3D MODEL ***
	void RemoveCouplingActor(int ActorNumber);

	// Description:
	//	Remove all coupling actors -- *** USED ONLY WHEN THE 1D MODELS IS BEING COUPLED WITH A 3D MODEL ***
	void RemoveAllCouplingActors();

	// Description:
	//	Set the color code of coupling spheres -- *** USED ONLY WHEN THE 1D MODELS IS BEING COUPLED WITH A 3D MODEL ***
	void SetFullModelCode(int code);

	// Description:
	// Add clip.
	void AddClip();

	// Description:
	// Remove clip.
	void RemoveClip();

	// Description:
	// Add stent.
	void AddStent(vtkDoubleArray *array);

	// Description:
	// Remove stent.
	void RemoveStent();

	// Description:
	// Add stenosis.
	void AddStenosis(vtkDoubleArray *array);

	// Description:
	// Remove stenosis.
	void RemoveStenosis();

	// Description:
	// Add Aneurysm.
	void AddAneurysm(vtkDoubleArray *array);

	// Description:
	// Remove Aneurysm.
	void RemoveAneurysm();

	// Description:
	// Add aging.
	void AddAging(vtkDoubleArray *array);

	// Description:
	// Remove aging.
	void RemoveAging();

	// Description:
	//
	void RenderWidgetsWithDefaultColors();

	// Description:
	//
	void ClearElementSelectedProxy();

	// Description;
	// Return ids of the segments with clip.
	vtkIdList *GetSegmentsWithClip();

	// Description:
	// Return segment vtkClientServerID.
	vtkIdType GetSegmentCSID(vtkIdType id);

	// Description:
	// Return terminal vtkClientServerID.
	vtkIdType GetTerminalCSID(vtkIdType id);

	// Description:
	// Delete terminal selected.
	int DeleteTerminalSelected();

	// Description:
	// Delete segment selected.
	int DeleteSegmentSelected();

	// Description:
	// Delete segment to the past ID.
	int DeleteSegment(vtkIdType ID);

	// Description:
	// Delete segment to the past ID.
	void DeleteSegment(vtkIdList *list);

//***************************************************************************************
//***************************************************************************************
//***************************************************************************************
//***************************************************************************************

	// Description:
	// Clear render with select button clear.
	void ClearPlot();

	// Description:
	// When select Plot mode reload actors in render.
	void ReloadPlotActors();

	// Description:
	// Read simulation da file. DataOut.txt file
	int ReadSimulationData(const char *fileName);

	// Description:
	// Calculate volume of the tree.
	double CalculateTreeVolume();

	// Description:
	// Has null elements of the coupled model.
	bool HasNullElements1D3D();

	// Description:
	// Return id of the smaller segment.
	vtkIdType GetSmallerSegment();

	// Description:
	// Highlight segment.
	void HighlightSegment(vtkIdType idSegment);
	void HighlightSegment(char *Name);

	// Description:
	// Configure a default value to all elements of the segment in the tree. This value
	// can be specified for the user or based in the length of the element of the
	// smaller segment in the tree.
	void ConfigureDefaultLengthOfElements(double length);

	// Description:
	// Copy segment selected.
	void CopySegment();

	// Description:
	//
	void *SetSegment(vtkIdType element);

	// Description:
	// Return IdList with SubTreeListSegments
	vtkIdList *GetSubTreeListSegments();

	// Description:
	// Return IdList with SubTreeListTerminals
	vtkIdList *GetSubTreeListTerminals();

	// Description:
	// Flip segments.
	void FlipSegments();

	//Description:
	//Returns Equivalent Resistance of the sub-tree
	double GetEquivalentResistance();

	//Description:
	//Returns Equivalent Capacitance of the sub-tree
	double GetEquivalentCapacitance();

	//Description:
	//Returns if the terminal is a leaf or not
	bool IsLeaf(vtkIdType terminal);

  //Description:
  //Returns a vtkDoubleArray (size 3) with sub-tree radius informations
  //0,->Greather radius
  //1,->Id of segment where the greather radius is
	//2,->Number of the node where the greather radius is
	vtkDoubleArray *GetMaxRadiusInSubTree();

  //Description:
  //Returns a vtkDoubleArray (size 3) with sub-tree radius informations
  //0,->Smaller radius
  //1,->Id of segment where the smaller radius is
	//2,->Number of the node where the smaller radius is
	vtkDoubleArray *GetMinRadiusInSubTree();

  //Description:
  //Returns a vtkDoubleArray (size 3) with sub-tree wallthickness informations
  //0,->Smaller wallthickness
  //1,->Id of segment where the smaller wallthickness is
	//2,->Number of the node where the smaller wallthickness is
	vtkDoubleArray *GetMaxWallThicknessInSubTree();

  //Description:
  //Returns a vtkDoubleArray (size 3) with sub-tree wallthickness informations
  //0,->Smaller wallthickness
  //1,->Id of segment where the smaller wallthickness is
	//2,->Number of the node where the smaller wallthickness is
	vtkDoubleArray *GetMinWallThicknessInSubTree();

//	double GetSegmentDistRadius(vtkIdType segment);
//	double GetSegmentProxWall(vtkIdType segment);
//	double GetSegmentDistWall(vtkIdType segment);

	void HighlightSelectedActors(int opcao);

  //Description:
  //Returns a list of segments for restart sub-tree
	vtkIdList *RestartSubTree();

	void SetTermPropAfterDelSubTree(vtkHM1DTreeElement *segmParent, double capEq, double resEq);

	vtkHM1DTreeElement *GetSegment(vtkIdType subTree);

	vtkHM1DTreeElement *GetTerminal(vtkIdType subTree);

	// Descriprion:
	// Refine sub-tree mesh.
	// method = "Refine" or "De-Refine".
	// factor = 1 <= factor <= 10
	void RefineSubTreeMesh(int method, int factor, int mode);


	int *GetPathTerminals();

	void ResetPathTree();

	// Description:
	// Set segments selected.
	// mode = keep or remove mode.
	void KeepRemoveSegments(int mode);

	void UpdateWidget();

	// Description:
	// Keep a sub tree selected.
	void KeepSubTree(vtkIdList *list);

protected:
//BTX
  vtkSMHMStraightModelWidgetProxy();
  ~vtkSMHMStraightModelWidgetProxy();

  // Description:
  // Overloaded to update the property values before saving state
  virtual void SaveState(const char* name, ostream* file, vtkIndent indent);

  // Description:
  // Execute event of the 3D Widget.
  virtual void ExecuteEvent(vtkObject*, unsigned long, void*);

  virtual void CreateVTKObjects(int numObjects);

  vtkHM1DStraightModelSource* StraightModelSource;
  vtkHM1DStraightModelReader* Reader;
  vtkHM1DStraightModel* 			StraightModel;
  vtkIdType 									StraightModelElementSelected;
  vtkHMStraightModelWidget* Widget;

  vtkIdType StraightModelObjectSelected;

  vtkSMSourceProxy *SourceProxy;

  // Descrition:
  // CSId of the server side HM1DStraightModelSource.
  vtkClientServerID SourceCSID;

  vtkSMHM1DSegmentProxy *SegmentProxy;
  vtkSMHM1DTerminalProxy *TerminalProxy;
  vtkIntArray *PlotArray;


//  ListOfInt IDList;
  vtkIdList *IDList;

  // used to store the boundary terminals from a path
  int terminals[2];



private:
  vtkSMHMStraightModelWidgetProxy(const vtkSMHMStraightModelWidgetProxy&);// Not implemented
  void operator=(const vtkSMHMStraightModelWidgetProxy&); // Not implemented
//ETX
};

#endif
