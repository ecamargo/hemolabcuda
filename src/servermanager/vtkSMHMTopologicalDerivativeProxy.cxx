#include "vtkSMHMTopologicalDerivativeProxy.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"

#include "vtkClientServerID.h"

vtkCxxRevisionMacro(vtkSMHMTopologicalDerivativeProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHMTopologicalDerivativeProxy);

vtkSMHMTopologicalDerivativeProxy::vtkSMHMTopologicalDerivativeProxy()
{
}

vtkSMHMTopologicalDerivativeProxy::~vtkSMHMTopologicalDerivativeProxy()
{
}

void vtkSMHMTopologicalDerivativeProxy::SendStream(char *Method, int Parameter)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << Method << Parameter << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMTopologicalDerivativeProxy::ExecuteFilter()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "ExecuteTopologicalDerivative" << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}
