#ifndef __vtkSMHMTopologicalDerivativeProxy_h
#define __vtkSMHMTopologicalDerivativeProxy_h

#include "vtkSMSourceProxy.h"

struct vtkClientServerID;

class VTK_EXPORT vtkSMHMTopologicalDerivativeProxy : public vtkSMSourceProxy
{
public:

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkSMHMTopologicalDerivativeProxy, vtkSMSourceProxy);

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkSMHMTopologicalDerivativeProxy* New();

  // Description:
  // Sends a generic stream to its DataServer Object.
  void SendStream(char *Method, int Parameter);

  // Description:
  // Invokes the filter execution on the Data Server object.  
  void ExecuteFilter();

protected:
//BTX
  // Description:
  // Class Constructor
  vtkSMHMTopologicalDerivativeProxy();

  // Description:
  // Class Destructor
  ~vtkSMHMTopologicalDerivativeProxy();
   
private:
  vtkSMHMTopologicalDerivativeProxy(const vtkSMHMTopologicalDerivativeProxy&);// Not implemented
  void operator=(const vtkSMHMTopologicalDerivativeProxy&); // Not implemented
//ETX
};  

#endif
