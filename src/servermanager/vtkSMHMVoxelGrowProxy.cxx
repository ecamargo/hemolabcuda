#include "vtkSMHMVoxelGrowProxy.h"

#include "vtkObjectFactory.h"
#include "vtkProcessModule.h"
#include "vtkClientServerStream.h"

#include "vtkClientServerID.h"

vtkCxxRevisionMacro(vtkSMHMVoxelGrowProxy, "$Rev$");
vtkStandardNewMacro(vtkSMHMVoxelGrowProxy);

vtkSMHMVoxelGrowProxy::vtkSMHMVoxelGrowProxy()
{
}

vtkSMHMVoxelGrowProxy::~vtkSMHMVoxelGrowProxy()
{
}

void vtkSMHMVoxelGrowProxy::SendStream(char *Method, int Parameter)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << Method << Parameter << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMVoxelGrowProxy::ExecuteFilter()
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "ExecuteVoxelGrow" << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMVoxelGrowProxy::SendSeed(double *PassedSeed)
{
  this->SendSeed(PassedSeed[0],  PassedSeed[1], PassedSeed[2], PassedSeed[3]);
}

void vtkSMHMVoxelGrowProxy::SendSeed(double PassedSeedX, double PassedSeedY, double PassedSeedZ, double PassedSeedIntensity)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "AddSeed" << PassedSeedX << PassedSeedY << PassedSeedZ
         << PassedSeedIntensity << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);  
}

void vtkSMHMVoxelGrowProxy::SetThreshold(int LowerThreshold, int UpperThreshold)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "SetThreshold" << LowerThreshold << UpperThreshold
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMVoxelGrowProxy::AddSeedThresholds(int LowerThreshold, int UpperThreshold)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "AddSeedThresholds" << LowerThreshold << UpperThreshold
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMVoxelGrowProxy::SetSeedThresholds(int SeedID, int LowerThreshold, int UpperThreshold)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "SetSeedThresholds" << SeedID << LowerThreshold << UpperThreshold
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}

void vtkSMHMVoxelGrowProxy::RemoveSeedThresholds(int SeedID)
{
  vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
  vtkClientServerStream stream;

  stream << vtkClientServerStream::Invoke <<  this->GetID(0)
         << "RemoveSeedThresholds" << SeedID
         << vtkClientServerStream::End;

  pm->SendStream(vtkProcessModule::DATA_SERVER, stream);
}
