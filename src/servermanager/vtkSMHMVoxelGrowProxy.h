#ifndef __vtkSMHMVoxelGrowProxy_h
#define __vtkSMHMVoxelGrowProxy_h

#include "vtkSMSourceProxy.h"

struct vtkClientServerID;

class VTK_EXPORT vtkSMHMVoxelGrowProxy : public vtkSMSourceProxy
{
public:

  // Description:
  // Macro required by VTK when extending its classes
  vtkTypeRevisionMacro(vtkSMHMVoxelGrowProxy, vtkSMSourceProxy);

  // Description:
  // Static Method used for instanciating objects of this class.
  static vtkSMHMVoxelGrowProxy* New();

  // Description:
  // Sends a generic stream to its DataServer Object.
  void SendStream(char *Method, int Parameter);

  // Description:
  // Invokes the filter execution on the Data Server object.  
  void ExecuteFilter();

  // Description:
  // Sends the passed seed to the Data Server Filter.
  void SendSeed(double *PassedSeed);

  // Description:
  // Sends the passed seed to the Data Server Filter.
  void SendSeed(double PassedSeedX, double PassedSeedY, double PassedSeedZ, double PassedSeedIntensity);
  
  void SetThreshold(int LowerThreshold, int UpperThreshold);

  void AddSeedThresholds(int LowerThreshold, int UpperThreshold);

  void SetSeedThresholds(int SeedID, int LowerThreshold, int UpperThreshold);
    
  void RemoveSeedThresholds(int SeedID);
  
protected:
//BTX
  // Description:
  // Class Constructor
  vtkSMHMVoxelGrowProxy();

  // Description:
  // Class Destructor
  ~vtkSMHMVoxelGrowProxy();
   
private:
  vtkSMHMVoxelGrowProxy(const vtkSMHMVoxelGrowProxy&);// Not implemented
  void operator=(const vtkSMHMVoxelGrowProxy&); // Not implemented
//ETX
};  

#endif
