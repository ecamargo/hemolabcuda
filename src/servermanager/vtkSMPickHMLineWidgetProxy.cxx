#include "vtkSMPickHMLineWidgetProxy.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkCommand.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSMRenderModuleProxy.h"
#include "vtkProcessModule.h"
#include "vtkHMStraightModelWidget.h"

vtkStandardNewMacro(vtkSMPickHMLineWidgetProxy);
vtkCxxRevisionMacro(vtkSMPickHMLineWidgetProxy, "$Revision: 1.2 $");
//-----------------------------------------------------------------------------
vtkSMPickHMLineWidgetProxy::vtkSMPickHMLineWidgetProxy()
{
  this->EventTag = 0;
  this->Interactor = 0;
  this->EventCallbackCommand = vtkCallbackCommand::New();
  this->EventCallbackCommand->SetClientData(this);
  this->EventCallbackCommand->SetCallback(vtkSMPickHMLineWidgetProxy::ProcessEvents);
  this->LastPicked = 0;
   
}

//-----------------------------------------------------------------------------
vtkSMPickHMLineWidgetProxy::~vtkSMPickHMLineWidgetProxy()
{
  this->EventCallbackCommand->Delete();
}

//-----------------------------------------------------------------------------
/*static*/
void vtkSMPickHMLineWidgetProxy::ProcessEvents(vtkObject* vtkNotUsed(object), 
                                          unsigned long event,
                                          void* clientdata, 
                                          void* vtkNotUsed(calldata))
{
  vtkSMPickHMLineWidgetProxy* self = reinterpret_cast<vtkSMPickHMLineWidgetProxy*>(
    clientdata);
  if (!self)
    {
    vtkGenericWarningMacro("ProcessEvents received from unknown object.");
    return;
    }

  switch (event) 
    {
  case vtkCommand::CharEvent:
    self->OnChar();
    break;
    }
}

//-----------------------------------------------------------------------------
void vtkSMPickHMLineWidgetProxy::OnChar()
{
  if (!this->ObjectsCreated || this->GetNumberOfIDs() < (unsigned int)1)
    {
    vtkErrorMacro("HMLineWidgetProxy not created yet.");
    return;
    }

  vtkRenderer* ren = this->CurrentRenderModuleProxy->GetRenderer();
  
  if (ren && this->Interactor->GetKeyCode() == 'p' || 
    this->Interactor->GetKeyCode() == 'P' )
    {
    if (this->CurrentRenderModuleProxy == NULL)
      {
      vtkErrorMacro("Cannot pick without a render module.");
      return;
      }
    int X = this->Interactor->GetEventPosition()[0];
    int Y = this->Interactor->GetEventPosition()[1];
    float z = this->CurrentRenderModuleProxy->GetZBufferValue(X, Y);
    double pt[4];

    // ComputeDisplayToWorld
    ren->SetDisplayPoint(double(X), double(Y), z);
    ren->DisplayToWorld();
    ren->GetWorldPoint(pt);

//    if (this->LastPicked == 0)
//      { // Choose the closest point.
//      const double *pt1 = this->GetPoint1();
//      const double *pt2 = this->GetPoint2();
//      double d1, d2, tmp[3];
//      tmp[0] = pt1[0]-pt[0]; 
//      tmp[1] = pt1[1]-pt[1]; 
//      tmp[2] = pt1[2]-pt[2];
//      d1 = tmp[0]*tmp[0] + tmp[1]*tmp[1] + tmp[2]*tmp[2];
//      tmp[0] = pt2[0]-pt[0]; 
//      tmp[1] = pt2[1]-pt[1]; 
//      tmp[2] = pt2[2]-pt[2];
//      d2 = tmp[0]*tmp[0] + tmp[1]*tmp[1] + tmp[2]*tmp[2];
//      this->LastPicked = 1;
//      if (d2 < d1)
//        {
//        this->LastPicked = 2;
//        }
//      }
//    else
//      { // toggle point
//      if (this->LastPicked == 1)
//        {
//        this->LastPicked = 2;
//        }
//      else
//        {
//        this->LastPicked = 1;
//        }
//      }

//    if (this->LastPicked == 1)
//      {
//      this->SetPoint1(pt[0], pt[1], pt[2]);
//      }
//    else
//      {
//      this->SetPoint2(pt[0], pt[1], pt[2]);
//      }
    this->UpdateVTKObjects(); // This will push down the values on to the
      // server objects (and client objects).
    this->InvokeEvent(vtkCommand::WidgetModifiedEvent); //So that the GUI
      // knows that the widget has been modified.
    this->Interactor->Render();
    }
}

//-----------------------------------------------------------------------------
void vtkSMPickHMLineWidgetProxy::AddToRenderModule(vtkSMRenderModuleProxy* rm)
{
  this->Superclass::AddToRenderModule(rm);
  if (this->Interactor || !this->ObjectsCreated || this->GetNumberOfIDs() < 1)
    {
    // already added to a render module.
    return;
    }
  this->Interactor = rm->GetInteractor();
  
  if (this->Interactor)
    {
    vtkProcessModule* pm = vtkProcessModule::GetProcessModule();
    vtkHMStraightModelWidget* wdg = vtkHMStraightModelWidget::SafeDownCast(
      pm->GetObjectFromID(this->GetID(0)));
    
    this->EventTag = this->Interactor->AddObserver(vtkCommand::CharEvent,
      this->EventCallbackCommand, wdg->GetPriority()); 
    }
}


//-----------------------------------------------------------------------------
void vtkSMPickHMLineWidgetProxy::RemoveFromRenderModule(
  vtkSMRenderModuleProxy* rm)
{
  this->Superclass::RemoveFromRenderModule(rm);

  if (this->Interactor && this->EventTag)
    {
    this->Interactor->RemoveObserver(this->EventTag);
    this->EventTag = 0;
    }
  this->Interactor = 0;
}
//-----------------------------------------------------------------------------
void vtkSMPickHMLineWidgetProxy::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
