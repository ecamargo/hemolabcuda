#ifndef __vtkSMPickHMLineWidgetProxy_h
#define __vtkSMPickHMLineWidgetProxy_h

#include "vtkSMHMStraightModelWidgetProxy.h"

class vtkCallbackCommand;
class vtkRenderWindowInteractor;

class VTK_EXPORT vtkSMPickHMLineWidgetProxy : public vtkSMHMStraightModelWidgetProxy
{
public:
  static vtkSMPickHMLineWidgetProxy* New();
  vtkTypeRevisionMacro(vtkSMPickHMLineWidgetProxy, vtkSMHMStraightModelWidgetProxy);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Called when the display is added/removed to/from a RenderModule.
  // Overridden to bind OnChar event on the Interactor (for picking).
  virtual void AddToRenderModule(vtkSMRenderModuleProxy*);
  virtual void RemoveFromRenderModule(vtkSMRenderModuleProxy*);
protected:
  vtkSMPickHMLineWidgetProxy();
  ~vtkSMPickHMLineWidgetProxy();

  // Determines the position of the widget end poins based on the 
  // pointer positions.
  void OnChar();

  // Static method for vtkCallbackCommand.
  static void ProcessEvents(vtkObject* vtkNotUsed(object), 
                                          unsigned long event,
                                          void* clientdata, 
                                          void* vtkNotUsed(calldata));
  
  unsigned long EventTag;
  vtkCallbackCommand* EventCallbackCommand;
  vtkRenderWindowInteractor* Interactor;
  int LastPicked; // identifier for the last picked line end point.

private:
  vtkSMPickHMLineWidgetProxy(const vtkSMPickHMLineWidgetProxy&); // Not implemented.
  void operator=(const vtkSMPickHMLineWidgetProxy&); // Not implemented.
};

#endif
