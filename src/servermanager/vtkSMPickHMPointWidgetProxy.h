#ifndef __vtkSMPickHMPointWidgetProxy_h
#define __vtkSMPickHMPointWidgetProxy_h

#include "vtkSMHMPointWidgetProxy.h"

class vtkCallbackCommand;
class vtkRenderWindowInteractor;

class VTK_EXPORT vtkSMPickHMPointWidgetProxy : public vtkSMHMPointWidgetProxy
{
public:
  static vtkSMPickHMPointWidgetProxy* New();
  vtkTypeRevisionMacro(vtkSMPickHMPointWidgetProxy, vtkSMHMPointWidgetProxy);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Called when the display is added/removed to/from a RenderModule.
  // Overridden to bind OnChar event on the Interactor (for picking).
  virtual void AddToRenderModule(vtkSMRenderModuleProxy*);
  virtual void RemoveFromRenderModule(vtkSMRenderModuleProxy*);
protected:
  vtkSMPickHMPointWidgetProxy();
  ~vtkSMPickHMPointWidgetProxy();

  // Determines the position of the widget end poins based on the 
  // pointer positions.
  void OnChar();

  // Static method for vtkCallbackCommand.
  static void ProcessEvents(vtkObject* vtkNotUsed(object), 
                                          unsigned long event,
                                          void* clientdata, 
                                          void* vtkNotUsed(calldata));
  
  unsigned long EventTag;
  vtkCallbackCommand* EventCallbackCommand;
  vtkRenderWindowInteractor* Interactor;

private:
  vtkSMPickHMPointWidgetProxy(const vtkSMPickHMPointWidgetProxy&); // Not implemented.
  void operator=(const vtkSMPickHMPointWidgetProxy&); // Not implemented.
    
};



#endif
