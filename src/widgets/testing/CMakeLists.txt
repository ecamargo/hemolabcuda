ADD_EXECUTABLE( testHM1DWidgetReadingSource testHM1DWidgetReadingSource.cxx)

TARGET_LINK_LIBRARIES(testHM1DWidgetReadingSource
	vtkCommon
	vtkRendering 
	vtkGraphics	
	vtkGraphicsCS
	vtkKWParaView		
	vtkWidgets
	vtkWidgetsCS		
  vtkhmCommon
	vtkhmGraphics 
  vtkhmIO	
  vtkhmWidgets	
)


ADD_EXECUTABLE( testHM1DWidgetGridTest testHM1DWidgetGridTest.cxx)

TARGET_LINK_LIBRARIES(testHM1DWidgetGridTest
	vtkCommon
	vtkRendering 
	vtkGraphics	
	vtkGraphicsCS
	vtkKWParaView		
	vtkWidgets
	vtkWidgetsCS		
  vtkhmCommon
	vtkhmGraphics 
  vtkhmIO	
  vtkhmWidgets	
)

#ADD_EXECUTABLE( testHMImageWorkspaceWidget testHMImageWorkspaceWidget.cxx)

#TARGET_LINK_LIBRARIES(testHMImageWorkspaceWidget
#	vtkCommon
#	vtkRendering 
#	vtkGraphics	
#	vtkGraphicsCS
#	vtkKWParaView		
#	vtkWidgets
#	vtkWidgetsCS		
#  vtkhmCommon
#	vtkhmGraphics 
#  vtkhmIO	
#  vtkhmWidgets	
#)