/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: testHM1DWidgetGridTest.cxx,v $
	Date:			 02/05/2006
	Updates:   
=========================================================================*/
// .SECTION Description
// Test reading files using the vtkHM1DStraightModelReader and
// and passing the 1DStraightModelGrid directly


#include "vtkCellArray.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleImage.h"
    
#include "vtkHMCallback.h"      
#include "vtkHMActorTable.h"      
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DStraightModelWidget.h"


int main( int argc, char *argv[] )
{
	// ATENÇÃO!!!
	// PARA ESTE TESTE FUNCIONAR, DEVE-SE PASSAR DIRETÓRIO COM 
	// ARQUIVO DA ÁRVORE 1D COMO PARÂMETRO DA FUNÇÃO MAIN!
	
	// Testa se diretório foi ou não passado
	// Testado: ./app /home/workspace/HeMoLab/data/Case1/v1
	//                            ou
	//          ./app /home/workspace/HeMoLab/data/v1
	if (argc <= 1)
		{
	  std::cerr << "Falta Parametros." << std::endl;
	  std::cerr << "Usando exemplo default..." << std::endl;
	  argv[1] = new char[100];
	  strcpy(argv[1], "/home/workspace/HeMoLab/data/Case1/v1");
	  }
	
  // Lendo arquivos com informações da árvore
	vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::New();
		reader->SetFileName(argv[1]);
		reader->Update();
	
	// Recebe da classe de leitura o StraightModelGrid preenchido
	vtkHM1DStraightModelGrid *poly = reader->GetOutput(0);

	vtkRenderer *renderer = vtkRenderer::New();
	vtkRenderWindow *renWin = vtkRenderWindow::New();
	renWin->AddRenderer(renderer);

	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
	iren->SetRenderWindow(renWin);

	renderer->SetBackground(0,0,0);
	renWin->SetSize(400,400);	 

	// Criação da Widget para o modelo 1D com informações do polydata (grid)
	vtkHM1DStraightModelWidget *model = vtkHM1DStraightModelWidget::New();
  	model->SetInteractor(iren);
		model->SetStraightModelGrid(poly);
  		
	// Modificando tamanho dos terminais
	model->SizeHandles();  		

	vtkHMCallback *callback = vtkHMCallback::New();
		iren->AddObserver(vtkCommand::LeftButtonPressEvent, callback, 2); 
		iren->AddObserver(vtkCommand::EndPickEvent, callback,2);	
		callback->SetTable(model->GetTable()); 	

	// Define estilo tipo imagem para prevenir rotações no modelo
  vtkInteractorStyleImage *isi = vtkInteractorStyleImage::New();
		iren->SetInteractorStyle(isi);
            	                
	renWin->Render();
	iren->Start();

	renderer->Delete();
	renWin->Delete();
	iren->Delete();
	isi->Delete();
	poly->Delete();	
 	reader->Delete();	
 	model->Delete();  
	callback->Delete();
	
	return 0;
}


