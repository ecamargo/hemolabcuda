/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: testHM1DWidgetReadingSource.cxx,v $
	Date:			 13/04/2006
	Updates:   13/04/2006 - Rodrigo
						 27/04/2006 - Replace Left Mouse Button and Keyboard Callback 
						 							for an unique Callback
=========================================================================*/
// .SECTION Description
// Test reading files using the vtkHM1DStraightModelReader and
// the visualization is done by the vtkHM1DStraightModelWidget


#include "vtkCellArray.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleImage.h"
    
#include "vtkHMCallback.h"      
#include "vtkHMActorTable.h"      
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DStraightModelWidget.h"


int main( int argc, char *argv[] )
{
/*	
	vtkRenderer *renderer = vtkRenderer::New();
	vtkRenderWindow *renWin = vtkRenderWindow::New();
	renWin->AddRenderer(renderer);

	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
	iren->SetRenderWindow(renWin);

	renderer->SetBackground(0,0,0);
	renWin->SetSize(400,400);	 

	// Criação da Widget para o modelo 1D com informações do polydata (grid)
	vtkHM1DStraightModelWidget *model = vtkHM1DStraightModelWidget::New();
  	model->SetInteractor(iren);
		model->SetNumberOfSegments(5);
		model->SetNumberOfPoints(4);		
		model->BuildActorTable();

	renderer->Delete();
	renWin->Delete();
	iren->Delete();
 	model->Delete();
*/

	// ATENÇÃO!!!
	// PARA ESTE TESTE FUNCIONAR, DEVE-SE PASSAR DIRETÓRIO COM 
	// ARQUIVO DA ÁRVORE 1D COMO PARÂMETRO DA FUNÇÃO MAIN!
	
	// Testa se diretório foi ou não passado
	// Testado: ./app /home/workspace/HeMoLab/data/Case1/v1
	//                            ou
	//          ./app /home/workspace/HeMoLab/data/v1
	if (argc <= 1)
		{
	  std::cerr << "Falta Parametros." << std::endl;
	  std::cerr << "Usando exemplo default..." << std::endl;
	  argv[1] = new char[100];
	  strcpy(argv[1], "/home/workspace/HeMoLab/data/Case1/v1");
	  }
	
  // Lendo arquivos com informações da árvore
	vtkHM1DStraightModelReader *reader = vtkHM1DStraightModelReader::New();
		reader->SetFileName(argv[1]);
		reader->Update();
	
	// Recebe da classe de leitura o StraightModelGrid preenchido
	vtkHM1DStraightModelGrid *poly = reader->GetOutput(0);

	vtkRenderer *renderer = vtkRenderer::New();
	vtkRenderWindow *renWin = vtkRenderWindow::New();
	renWin->AddRenderer(renderer);

	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
	iren->SetRenderWindow(renWin);

	renderer->SetBackground(0,0,0);
	renWin->SetSize(400,400);	 

	// Criação da Widget para o modelo 1D com informações do polydata (grid)
	vtkHM1DStraightModelWidget *model = vtkHM1DStraightModelWidget::New();
  	model->SetInteractor(iren);
		model->SetNumberOfSegments(poly->GetNumberOfCells());
		model->SetNumberOfPoints(poly->GetNumberOfPoints());		
		model->BuildActorTable();
  	
	// Lendo do StraightModelGrid quais nós são terminais
	vtkCellArray *terminals = poly->GetVerts();
	int id = 1;
	int idHeart;
	int numberOfTerminals = poly->GetNumberOfPoints();
	int *isTerminal = new int[numberOfTerminals];
	for( int i = 0; i < terminals->GetNumberOfCells(); i++)
	{
		if(i==0) idHeart = isTerminal[terminals->GetData()->GetValue(id)];
		isTerminal[terminals->GetData()->GetValue(id)] = 1;
		id+=2; // Pois o primeiro dado é o índice do próprio CellArray		
	}

 	isTerminal[idHeart] = 3;
  // Lendo do StraightModelGrid posição no espaço dos nós
	for(int i = 0; i < poly->GetNumberOfPoints(); i++)
		model->SetPoint(poly->GetPoint(i), isTerminal[i]);	
		
  // Lendo do StraightModelGrid conectividade dos pontos (segmentos)
	int edge[2];
	vtkCellArray *cell = poly->GetLines();
	id = 1;
	for( int i = 0; i < cell->GetNumberOfCells(); i++)
	{
		edge[0] = cell->GetData()->GetValue(id++);
		edge[1] = cell->GetData()->GetValue(id++);		
		model->AddSegment(edge, isTerminal);		
		id++;
	}
  		
	// Modificando tamanho dos terminais
	model->SizeHandles();  		

	vtkHMCallback *callback = vtkHMCallback::New();
		iren->AddObserver(vtkCommand::LeftButtonPressEvent, callback, 2); 
		iren->AddObserver(vtkCommand::EndPickEvent, callback,2);	
		callback->SetTable(model->GetTable()); 	

	// Define estilo tipo imagem para prevenir rotações no modelo
  vtkInteractorStyleImage *isi = vtkInteractorStyleImage::New();
		iren->SetInteractorStyle(isi);
            	                
	renWin->Render();
	iren->Start();

 	delete [] isTerminal;
	renderer->Delete();
	renWin->Delete();
	iren->Delete();
	isi->Delete();
	poly->Delete();	
 	reader->Delete();	
//	cell->Delete();
//  terminals->Delete();
 	model->Delete();  
	callback->Delete();
 	
	
	return 0;
}


