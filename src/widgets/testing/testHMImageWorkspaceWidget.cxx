#include "vtkHMImageWorkspaceWidget.h"

#include "vtkDICOMImageReader.h"
#include "vtkRenderer.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkCellPicker.h"
#include "vtkImagePlaneWidget.h"
#include "vtkImageData.h"
#include "vtkProperty.h"

#include "vtkRenderWindowInteractor.h"
#include "vtkCamera.h"

#include "vtkRenderWindow.h";


int main( int argc, char *argv[] )
{
  vtkDICOMImageReader *reader = vtkDICOMImageReader::New();
  vtkCamera *camera = vtkCamera::New();
  vtkImageData *img = vtkImageData::New();
  vtkOutlineFilter *outline = vtkOutlineFilter::New();
  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
  vtkCellPicker *picker = vtkCellPicker::New();
  vtkRenderer *render = vtkRenderer::New();
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();  
  vtkRenderWindow *renderWindow = vtkRenderWindow::New();

  vtkImagePlaneWidget *xPlane = vtkImagePlaneWidget::New();
  vtkImagePlaneWidget *yPlane = vtkImagePlaneWidget::New();
  vtkImagePlaneWidget *zPlane = vtkImagePlaneWidget::New();
  
  reader->SetDirectoryName("/home/vinicius/images/dicom/Ricardo/");

  img = reader->GetOutput();
  img->Update();

  outline->SetInput(img);

  mapper->SetInput(outline->GetOutput());

  vtkActor *actor = vtkActor::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(0,0,0);

  picker->SetTolerance(0.005);

  xPlane->SetInteractor(iren);
  xPlane->SetInput(img);
  xPlane->SetKeyPressActivationValue('x');
  xPlane->SetPicker(picker);
  xPlane->GetPlaneProperty()->SetColor(0,0,1);
  xPlane->TextureInterpolateOn();
  xPlane->SetResliceInterpolateToCubic();
  xPlane->SetPlaneOrientationToXAxes();
  xPlane->SetSliceIndex(0);
  xPlane->SetTextureVisibility(1);
  xPlane->DisplayTextOn();

  yPlane->SetInteractor(iren);
  yPlane->SetInput(img);
  yPlane->SetKeyPressActivationValue('y');
  yPlane->SetPicker(picker);
  yPlane->GetPlaneProperty()->SetColor(0,0,1);
  yPlane->TextureInterpolateOn();
  yPlane->SetResliceInterpolateToCubic();
  yPlane->SetPlaneOrientationToYAxes();
  yPlane->SetSliceIndex(0);
  yPlane->SetTextureVisibility(1);
  yPlane->DisplayTextOn();
  
  zPlane->SetInteractor(iren);
  zPlane->SetInput(img);
  zPlane->SetKeyPressActivationValue('z');
  zPlane->SetPicker(picker);
  zPlane->GetPlaneProperty()->SetColor(0,0,1);
  zPlane->TextureInterpolateOn();
  zPlane->SetResliceInterpolateToCubic();
  zPlane->SetPlaneOrientationToZAxes();
  zPlane->SetSliceIndex(0);
  zPlane->SetTextureVisibility(0);
  zPlane->DisplayTextOn();

  camera->SetViewUp(0,0,-1);
  camera->SetPosition(0,1,0);
  camera->SetFocalPoint(0,0,0);
  camera->ComputeViewPlaneNormal();
  

  render->AddActor(actor);
  render->SetActiveCamera(camera);
  render->ResetCamera();
  render->SetBackground(1,1,1);
  
  renderWindow->AddRenderer(render);
  renderWindow->SetSize(800,600);
  
  iren->SetRenderWindow(renderWindow);

  iren->Start();


  //vtkHMImageWorkspaceWidget *widget = vtkHMImageWorkspaceWidget::New();
  
  return 1;
}
