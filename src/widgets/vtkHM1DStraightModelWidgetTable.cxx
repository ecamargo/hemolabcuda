/*
 * $Id:vtkHM1DStraightModelWidgetTable.cxx 506 2006-06-08 19:35:06Z diego $
 */

#include "vtkObjectFactory.h"
#include "vtkHM1DStraightModelWidgetTable.h"

vtkCxxRevisionMacro(vtkHM1DStraightModelWidgetTable, "$Rev:$");
vtkStandardNewMacro(vtkHM1DStraightModelWidgetTable);



vtkHM1DStraightModelWidgetTable::vtkHM1DStraightModelWidgetTable()
{
}



vtkHM1DStraightModelWidgetTable::~vtkHM1DStraightModelWidgetTable()
{
  WidgetTable.erase(WidgetTable.begin(), WidgetTable.end());		
}


//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::InsertArrayId(vtkIdType smId, vtkIdType arrayId)
{
 	WidgetTable.insert(WidgetTablePair(smId, arrayId));
}

//--------------------------------------------------------------------------------------------
int vtkHM1DStraightModelWidgetTable::GetArrayId(vtkIdType smId)
{
	WidgetTableMap::iterator it = WidgetTable.find(smId);
	return (*it).second;
}



//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::InsertSphereWidgetId(vtkIdType smId, vtkIdType widgetId)
{
 	SphereWidgetTable.insert(WidgetTablePair(smId, widgetId));
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::RemoveSphereWidgetId(vtkIdType smId, vtkIdType widgetId)
{
	SphereWidgetTableMap::iterator it, it2, it3;
	
	it  = SphereWidgetTable.lower_bound(smId);
	it2 = SphereWidgetTable.upper_bound(smId);
	
	while ( it != it2 )
	{
		if ( it->second == widgetId )
		{
			it3 = it;
			it3++;
			SphereWidgetTable.erase(it);

			while ( it3 != this->SphereWidgetTable.end() )
				{
				it3->second--;
				it3++;
				}
			break;
		}
		it++;
	}
	
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::RemoveReferenceSphereWidgetId(vtkIdType smId, vtkIdType widgetId)
{
	SphereWidgetTableMap::iterator it, it2, it3;
	
	it  = SphereWidgetTable.lower_bound(smId);
	it2 = SphereWidgetTable.upper_bound(smId);
	
	while ( it != it2 )
	{
		if ( it->second == widgetId )
		{
//			it3 = it;
//			it3++;
			SphereWidgetTable.erase(it);

//			while ( it3 != this->SphereWidgetTable.end() )
//				{
//				it3->second--;
//				it3++;
//				}
			break;
		}
		it++;
	}
	
}

//--------------------------------------------------------------------------------------------
vtkIdType vtkHM1DStraightModelWidgetTable::GetSphereWidgetId(vtkIdType smId)
{
	SphereWidgetTableMap::iterator it = SphereWidgetTable.find(smId);
	return (*it).second;
}

//--------------------------------------------------------------------------------------------
vtkIdType vtkHM1DStraightModelWidgetTable::GetSphereWidgetId(vtkIdType smId, vtkIdType widgetId)
{
	
	//Seta na primeira esfera do segmento, no primeiro node.
	SphereWidgetTableMap::iterator it  = this->SphereWidgetTable.lower_bound(smId);
	SphereWidgetTableMap::iterator it2 = this->SphereWidgetTable.upper_bound(smId);
	
	while ( it != it2 )
	{
		if ( it->second == widgetId )
		{
			return (*it).second;
		}
		it++;
	}

	return 0;
}


//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::InsertLineWidgetId(vtkIdType smId, vtkIdType widgetId)
{
 	LineWidgetTable.insert(WidgetTablePair(smId, widgetId));
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::RemoveLineWidgetId(vtkIdType smId, vtkIdType widgetId)
{
	LineWidgetTableMap::iterator it, it2, it3;
	
	it  = this->LineWidgetTable.lower_bound(smId);
	it2 = this->LineWidgetTable.upper_bound(smId);
	
	while ( (it != it2) )
	{
		if ( it->second == widgetId )
		{
			it3 = it;
			it3++;
			this->LineWidgetTable.erase(it);
			
			while ( it3 != this->LineWidgetTable.end() )
				{
				it3->second--;
				it3++;
				}
			break;
		}
		it++;
	}
	
}

//--------------------------------------------------------------------------------------------
vtkIdType vtkHM1DStraightModelWidgetTable::GetLineWidgetId(vtkIdType smId)
{
	LineWidgetTableMap::iterator it = LineWidgetTable.find(smId);
	return (*it).second;
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::OrderSphereTable(vtkIdType smId, vtkIdType widgetId)
{
	//Seta na primeira esfera do segmento, no primeiro node.
	SphereWidgetTableMap::iterator it  = this->SphereWidgetTable.lower_bound(smId);
	SphereWidgetTableMap::iterator it2 = this->SphereWidgetTable.upper_bound(smId);
	
	while ( it != this->SphereWidgetTable.end() )
	{
		if ( it->second == widgetId )
		{
			
		}
		it++;
	}
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::OrderLineTable(vtkIdType smId, vtkIdType widgetId)
{
}

//--------------------------------------------------------------------------------------------
void vtkHM1DStraightModelWidgetTable::RemoveSegmentWidgetId(vtkIdType smId, int numberOfLines, int numberOfSpheres)
{
	SphereWidgetTableMap::iterator itSphereBegin = this->SphereWidgetTable.lower_bound(smId);
	SphereWidgetTableMap::iterator itSphereEnd 	 = this->SphereWidgetTable.upper_bound(smId);
	SphereWidgetTableMap::iterator itSphere = itSphereEnd;
	
	LineWidgetTableMap::iterator itLineBegin = this->LineWidgetTable.lower_bound(smId);
	LineWidgetTableMap::iterator itLineEnd 	 = this->LineWidgetTable.upper_bound(smId);
	LineWidgetTableMap::iterator itLine = itLineEnd;
	
	while ( itLine != this->LineWidgetTable.end() )
		{
		itLine->second = itLine->second - numberOfLines;
		itLine++;
		}
	
	while ( itSphere != this->SphereWidgetTable.end() )
		{
		itSphere->second = itSphere->second - numberOfSpheres;
		itSphere++;
		}
	
	this->LineWidgetTable.erase(itLineBegin, itLineEnd);
	this->SphereWidgetTable.erase(itSphereBegin, itSphereEnd);
	
	
}
