/*
 * $Id:vtkHM1DStraightModelWidgetTable.h 506 2006-06-08 19:35:06Z diego $
 */
/*=========================================================================
=========================================================================*/


#ifndef __vtkHM1DStraightModelWidgetTable_h
#define __vtkHM1DStraightModelWidgetTable_h

#include "vtkObject.h"
#include "vtkActor.h"

#define NODE	0
#define TERMINAL 1
#define HEART 2
#define SEGMENT 3

#include <list> 
#include <map>

// Description:
// Table in which the relation between actor
// and properties ID is made
class VTK_EXPORT vtkHM1DStraightModelWidgetTable: public vtkObject
{
	
public:
//BTX
  typedef std::pair<vtkIdType, vtkIdType> WidgetTablePair;  
  typedef std::map <vtkIdType, vtkIdType> WidgetTableMap;
  
  typedef std::multimap <vtkIdType, vtkIdType> SphereWidgetTableMap;
//  typedef std::map <vtkIdType, vtkIdType> LineWidgetTableMap;  
  typedef std::multimap <vtkIdType, vtkIdType> LineWidgetTableMap;  
//ETX

  static vtkHM1DStraightModelWidgetTable *New();
  vtkTypeRevisionMacro(vtkHM1DStraightModelWidgetTable,vtkObject);

  // Description:
  // Class Constructor/Destructor
	vtkHM1DStraightModelWidgetTable();
  ~vtkHM1DStraightModelWidgetTable();	
	
  // Description:
  // Insert an actor and its type	
	void InsertArrayId(vtkIdType smId, vtkIdType arrayId);	
		
  // Description:
  // Get type of an actor
	int  GetArrayId(vtkIdType smId);	
	
	
	// Description:
  // Insert an StraightModel Id and its widget id	
	void InsertSphereWidgetId(vtkIdType smId, vtkIdType widgetId);	
	
	// Description:
  // Remove an StraightModel Id and its widget id	
	void RemoveSphereWidgetId(vtkIdType smId, vtkIdType widgetId);
	
	// Description:
  // Remove reference of the StraightModel id with widget id	
	void RemoveReferenceSphereWidgetId(vtkIdType smId, vtkIdType widgetId);
	
  // Description:
  // Get Widget Id of an StraightModel Id
	vtkIdType  GetSphereWidgetId(vtkIdType smId);
	
	// Description:
  // Get Widget Id of an StraightModel Id
	vtkIdType  GetSphereWidgetId(vtkIdType smId, vtkIdType widgetId);	

	// Description:
  // Insert an StraightModel Id and its widget id	
	void InsertLineWidgetId(vtkIdType smId, vtkIdType widgetId);	
	
	// Description:
  // Remove an StraightModel Id and its widget id	
	void RemoveLineWidgetId(vtkIdType smId, vtkIdType widgetId);
	
  // Description:
  // Get Widget Id of an StraightModel Id
	vtkIdType  GetLineWidgetId(vtkIdType smId);	
	
	void OrderSphereTable(vtkIdType smId, vtkIdType widgetId);
	
	void OrderLineTable(vtkIdType smId, vtkIdType widgetId);
	
	// Description:
	// Remove all information of the segments with sphere and line widgets
	void RemoveSegmentWidgetId(vtkIdType smId, int numberOfLines, int numberOfSpheres);
	
//BTX
  // Description:
  // Establish StraightModel Id and Widget Id relation
  WidgetTableMap WidgetTable;
  
  // Description:
  // Establish Sphere Widget Ids and StraightModel Ids relation
  SphereWidgetTableMap SphereWidgetTable;
  
  // Description:
  // Establish Line Widget Ids and StraightModel Ids relation
  LineWidgetTableMap LineWidgetTable;
//ETX	

private:

};


#endif
