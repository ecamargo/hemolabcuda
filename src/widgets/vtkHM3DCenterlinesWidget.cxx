/*
 * vtkHM3DCenterlinesWidget.cxx
 *
 *  Created on: Aug 18, 2009
 *      Author: igor
 */

#include "vtkHM3DCenterlinesWidget.h"
#include "vtkObjectFactory.h"

#include "vtkHMSeed3DWidget.h"

#include "vtkAssemblyPath.h"
#include "vtkLODActor.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkCellPicker.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkCommand.h"
#include "vtkPolyDataMapper.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkIdList.h"
#include "vtkMath.h"
#include "vtkCallbackCommand.h"
#include "vtkProp.h"

#include "vtkCollection.h"

vtkCxxRevisionMacro(vtkHM3DCenterlinesWidget, "$Rev: 563 $");
vtkStandardNewMacro(vtkHM3DCenterlinesWidget);

vtkHM3DCenterlinesWidget::vtkHM3DCenterlinesWidget()
{
  this->State = vtkHM3DCenterlinesWidget::Start;

  this->EventCallbackCommand->SetCallback(vtkHM3DCenterlinesWidget::ProcessEvents);

  this->Surface = vtkPolyData::New();
  this->SurfaceActor = vtkLODActor::New();

  this->SurfaceProperty = vtkProperty::New();

  this->SurfaceViewProperty = vtkProperty::New();
  this->SurfaceViewProperty->SetSpecular( .4);
  this->SurfaceViewProperty->SetSpecularPower( 80);
  this->SurfaceViewProperty->SetDiffuse( .8);
  this->SurfaceViewProperty->SetOpacity (0.25);

  this->SurfacePicker = vtkCellPicker::New();
  this->SurfacePicker->SetTolerance(0.004);

  this->SourcePicker = vtkCellPicker::New();
  this->SourcePicker->SetTolerance(0.004);
  this->SourcePicker->PickFromListOn();

  this->TargetPicker = vtkCellPicker::New();
  this->TargetPicker->SetTolerance(0.004);
  this->TargetPicker->PickFromListOn();

  this->SourceSeedsCollection = vtkCollection::New();
  this->TargetSeedsCollection = vtkCollection::New();

  this->SelectedSourceSeed = -1;
  this->SelectedTargetSeed = -1;

  this->SourceSeedProperty = vtkProperty::New();
  this->SourceSeedProperty->SetColor(1, 0, 0);

  this->TargetSeedProperty = vtkProperty::New();
  this->TargetSeedProperty->SetColor(0, 0, 1);

  this->SelectedSeedProperty = vtkProperty::New();
  this->SelectedSeedProperty->SetColor(0, 1, 0);

  this->PreAddSeedProperty = vtkProperty::New();
  this->PreAddSeedProperty->SetColor(1, 1, 0);

  this->PointIdSelected = -1;

  this->SetEnabled(0);

  this->CurrentSeed = NULL;
  this->PreAddSeed = NULL;
}

//------------------------------------------------------------------------------------------
vtkHM3DCenterlinesWidget::~vtkHM3DCenterlinesWidget()
{
  this->SetEnabled(0);

  this->Surface->Delete();
  this->SurfaceActor->Delete();

  this->SurfaceProperty->Delete();
  this->SurfaceViewProperty->Delete();

  this->SurfacePicker->Delete();
  this->SourcePicker->Delete();
  this->TargetPicker->Delete();

  if ( this->PreAddSeed )
    this->PreAddSeed->Delete();

  int NumberOfItems = this->SourceSeedsCollection->GetNumberOfItems();
  vtkHMSeed3DWidget *TempSeed3D;

  for (int i=0; i < NumberOfItems; i++)
  {
    TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i));

    TempSeed3D->Delete();
    TempSeed3D = NULL;
  }

  this->SourceSeedsCollection->Delete();

  NumberOfItems = this->TargetSeedsCollection->GetNumberOfItems();

  for (int i=0; i < NumberOfItems; i++)
  {
    TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i));

    TempSeed3D->Delete();
    TempSeed3D = NULL;
  }

  this->TargetSeedsCollection->Delete();

  this->SourceSeedProperty->Delete();
  this->TargetSeedProperty->Delete();
  this->SelectedSeedProperty->Delete();
  this->PreAddSeedProperty->Delete();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::ProcessEvents(vtkObject* vtkNotUsed(object), unsigned long event, void* clientdata, void* vtkNotUsed(calldata))
{
  vtkHM3DCenterlinesWidget* self = reinterpret_cast<vtkHM3DCenterlinesWidget *>( clientdata );
  switch(event)
    {
    case vtkCommand::LeftButtonPressEvent:
      self->OnLeftButtonPressEvent();
      break;

    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonReleaseEvent();
      break;

    case vtkCommand::RightButtonPressEvent:
      self->OnRightButtonPressEvent();
      break;

    case vtkCommand::RightButtonReleaseEvent:
      self->OnRightButtonReleaseEvent();
      break;

    case vtkCommand::MouseMoveEvent:
      self->OnMouseMoveEvent();
      break;
    }
}

//------------------------------------------------------------------------------------------
int vtkHM3DCenterlinesWidget::HighlightSourceSeed(vtkProp *prop)
{
  int n = this->SourceSeedsCollection->GetNumberOfItems();

  vtkHMSeed3DWidget *TempSeed3D;

  // First unhighlight anything picked
  for ( int i = 0; i < n; ++i ) // find handle
    {
    TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i));
    TempSeed3D->GetActor()->SetProperty(this->SourceSeedProperty);
    }

  if ( prop )
    {
    for ( int i = 0; i < n; ++i ) // find handle
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i));
      if ( prop == TempSeed3D->GetActor() )
        {
        this->CurrentSeed = TempSeed3D;
        this->ValidPick = 1;
        this->SourcePicker->GetPickPosition(this->LastPickPosition);
        this->CurrentSeed->GetActor()->SetProperty(this->SelectedSeedProperty);
        return i;
        }
      }
    }

  this->CurrentSeed = NULL;

  return -1;
}

//------------------------------------------------------------------------------------------
int vtkHM3DCenterlinesWidget::HighlightTargetSeed(vtkProp *prop)
{
  int n = this->TargetSeedsCollection->GetNumberOfItems();

  vtkHMSeed3DWidget *TempSeed3D;

  // First unhighlight anything picked
  for ( int i = 0; i < n; ++i ) // find handle
    {
    TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i));
    TempSeed3D->GetActor()->SetProperty(this->TargetSeedProperty);
    }

  if ( prop )
    {
    for ( int i = 0; i < n; ++i ) // find handle
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i));
      if ( prop == TempSeed3D->GetActor() )
        {
        this->CurrentSeed = TempSeed3D;
        this->ValidPick = 1;
        this->TargetPicker->GetPickPosition(this->LastPickPosition);
        this->CurrentSeed->GetActor()->SetProperty(this->SelectedSeedProperty);
        return i;
        }
      }
    }

  this->CurrentSeed = NULL;

  return -1;
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::OnLeftButtonPressEvent()
{
  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Okay, make sure that the pick is in the current renderer
  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
    this->State = vtkHM3DCenterlinesWidget::Outside;
    return;
    }

  this->HighlightSourceSeed(NULL);
  this->HighlightTargetSeed(NULL);

  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then try to pick the line.
  vtkAssemblyPath *path;

  this->SourcePicker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->SourcePicker->GetPath();

  if ( path != NULL )
    {
    if ( this->PreAddSeed )
      this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());

    this->HighlightSourceSeed(path->GetFirstNode()->GetViewProp());

    this->State = vtkHM3DCenterlinesWidget::SourceSelected;

    double *point;
    point = this->CurrentSeed->GetActor()->GetCenter();

    this->PointIdSelected = this->Surface->FindPoint(point);

    this->EventCallbackCommand->SetAbortFlag(1);
    this->StartInteraction();
    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
    this->Interactor->Render();
    }
  else
    {
    this->TargetPicker->Pick(X,Y,0.0,this->CurrentRenderer);
    path = this->TargetPicker->GetPath();

    if ( path != NULL )
      {
      if ( this->PreAddSeed )
        this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());

      this->HighlightTargetSeed(path->GetFirstNode()->GetViewProp());

      this->State = vtkHM3DCenterlinesWidget::TargetSelected;

      double *point;
      point = this->CurrentSeed->GetActor()->GetCenter();

      this->PointIdSelected = this->Surface->FindPoint(point);

      this->EventCallbackCommand->SetAbortFlag(1);
      this->StartInteraction();
      this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
      this->Interactor->Render();
      }

    else
      {
      this->SurfacePicker->Pick(X,Y,0.0,this->CurrentRenderer);
      path = this->SurfacePicker->GetPath();

      if ( path != NULL )
        {
        //Pego a celula clicada.
        vtkIdType cellId = this->SurfacePicker->GetCellId();

        //Pego os pontos da celula.
        vtkIdList *ptIds = vtkIdList::New();
        this->Surface->GetCellPoints(cellId, ptIds);

        double center[3], pickPosition[3];

        //Pego o ponto clicado
        this->SurfacePicker->GetPickPosition(pickPosition);

        vtkIdType pointId = ptIds->GetId(0);
        double distance = sqrt(vtkMath::Distance2BetweenPoints(pickPosition, this->Surface->GetPoint(pointId)));
        double minorDistance = distance;

        //Verifico qual ponto da celula esta mais proximo do ponto clicado
        for ( int i=1; i<ptIds->GetNumberOfIds(); i++ )
          {
          distance = sqrt(vtkMath::Distance2BetweenPoints(pickPosition, this->Surface->GetPoint(ptIds->GetId(i))));

          if ( distance < minorDistance )
            {
            minorDistance = distance;
            pointId = ptIds->GetId(i);
            }
          }

        ptIds->Delete();
        this->PointIdSelected = pointId;

        this->ValidPick = 1;

        if ( !this->PreAddSeed )
          {
          this->PreAddSeed = vtkHMSeed3DWidget::New();
          this->PreAddSeed->Create();
          }

        this->CurrentSeed = this->PreAddSeed;

        double *point = this->Surface->GetPoint(pointId);

        this->PreAddSeed->SetCenter(point);
//        double radius = this->vtk3DWidget::SizeHandles(0.5);


        this->SizeHandles();

//        this->PreAddSeed->SetRadius(radius);

        this->CurrentRenderer->AddActor(this->PreAddSeed->GetActor());
        this->PreAddSeed->GetActor()->SetProperty(this->PreAddSeedProperty);
        this->State = vtkHM3DCenterlinesWidget::Adding;


        this->EventCallbackCommand->SetAbortFlag(1);
        this->StartInteraction();
        this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
        this->Interactor->Render();
        }
      else
        {
        if ( this->PreAddSeed )
          this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());

        this->PointIdSelected = -1;

        this->EventCallbackCommand->SetAbortFlag(0);
        this->StartInteraction();
        this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
        this->State = vtkHM3DCenterlinesWidget::Outside;
        this->TargetPicker->GetPickPosition(this->LastPickPosition);
        return;
        }
      }
    }
  this->TargetPicker->GetPickPosition(this->LastPickPosition);
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::OnLeftButtonReleaseEvent()
{
  if ( (this->State == vtkHM3DCenterlinesWidget::Outside) || (this->State == vtkHM3DCenterlinesWidget::Start) )
    return;

  this->State = vtkHM3DCenterlinesWidget::Start;
  this->TargetPicker->GetPickPosition(this->LastPickPosition);
  this->EventCallbackCommand->SetAbortFlag(1);
  this->EndInteraction();
  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::OnRightButtonReleaseEvent()
{
  this->SizeHandles();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::OnRightButtonPressEvent()
{

}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::OnMouseMoveEvent()
{
  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  if ( (this->State == vtkHM3DCenterlinesWidget::Outside) || (this->State == vtkHM3DCenterlinesWidget::Start) ||
       (this->State == vtkHM3DCenterlinesWidget::SourceSelected) || (this->State == vtkHM3DCenterlinesWidget::TargetSelected) )
    return;

  // Okay, make sure that the pick is in the current renderer
  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
    this->State = vtkHM3DCenterlinesWidget::Outside;
    return;
    }

  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then try to pick the line.
  vtkAssemblyPath *path;

  this->SurfacePicker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->SurfacePicker->GetPath();

  if ( path != NULL )
    {
    //Pego a celula clicada.
    vtkIdType cellId = this->SurfacePicker->GetCellId();

    //Pego os pontos da celula.
    vtkIdList *ptIds = vtkIdList::New();
    this->Surface->GetCellPoints(cellId, ptIds);

    double center[3], pickPosition[3];

    //Pego o ponto clicado
    this->SurfacePicker->GetPickPosition(pickPosition);

    vtkIdType pointId = ptIds->GetId(0);
    double distance = sqrt(vtkMath::Distance2BetweenPoints(pickPosition, this->Surface->GetPoint(pointId)));
    double minorDistance = distance;

    //Verifico qual ponto da celula esta mais proximo do ponto clicado
    for ( int i=1; i<ptIds->GetNumberOfIds(); i++ )
      {
      distance = sqrt(vtkMath::Distance2BetweenPoints(pickPosition, this->Surface->GetPoint(ptIds->GetId(i))));

      if ( distance < minorDistance )
        {
        minorDistance = distance;
        pointId = ptIds->GetId(i);
        }
      }
    ptIds->Delete();
    this->PointIdSelected = pointId;

    this->CurrentSeed->SetCenter(this->Surface->GetPoint(pointId));

//    this->SizeHandles();

    }

  this->EventCallbackCommand->SetAbortFlag(1);
//    this->StartInteraction();
  this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SizeHandles()
{
//  double radius = this->vtk3DWidget::SizeHandles(0.5);
  double factor = 0.5;
  double radius;
  int i;
  vtkRenderer *renderer;

  if ( !this->ValidPick || !(renderer=this->CurrentRenderer) ||
       !renderer->GetActiveCamera() )
    {
    radius = (this->HandleSize * factor * this->InitialLength);
    }
  else
    {
    double z;
    double windowLowerLeft[4], windowUpperRight[4];
    double *viewport = renderer->GetViewport();
    int *winSize = renderer->GetRenderWindow()->GetSize();
    double focalPoint[4];

    this->ComputeWorldToDisplay(this->LastPickPosition[0],
                                this->LastPickPosition[1],
                                this->LastPickPosition[2], focalPoint);
    z = focalPoint[2];
    z = fabs(z);

    double x = winSize[0] * viewport[0];
    double y = winSize[1] * viewport[1];
    this->ComputeDisplayToWorld(x,y,z,windowLowerLeft);

    x = winSize[0] * viewport[2];
    y = winSize[1] * viewport[3];
    this->ComputeDisplayToWorld(x,y,z,windowUpperRight);

    for (radius=0.0, i=0; i<3; i++)
      {
      radius += (windowUpperRight[i] - windowLowerLeft[i]) *
        (windowUpperRight[i] - windowLowerLeft[i]);
      }

    radius = (sqrt(radius) * factor * this->HandleSize);
    }

  for (int i=0; i < this->SourceSeedsCollection->GetNumberOfItems(); i++)
  {
    vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i))->SetRadius(radius);
  }

  for (int i=0; i < this->TargetSeedsCollection->GetNumberOfItems(); i++)
  {
    vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i))->SetRadius(radius);
  }

  if ( this->PreAddSeed )
    this->PreAddSeed->SetRadius(radius);
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::AddObservers()
{
  if (this->Interactor)
    {
    this->Interactor->AddObserver(vtkCommand::LeftButtonPressEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::LeftButtonReleaseEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::RightButtonReleaseEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::RightButtonPressEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::MouseMoveEvent, this->EventCallbackCommand, 1.0);
    }
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::RemoveObservers()
{
  this->Interactor->RemoveObserver(this->EventCallbackCommand);
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetInput(vtkPolyData *polyData)
{
  this->Surface->DeepCopy(polyData);

  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
  mapper->SetInput(this->Surface);

  this->SurfaceActor->SetMapper(mapper);

  this->SurfacePicker->SetTolerance(0.004);
  this->SurfacePicker->AddPickList(this->SurfaceActor);
  this->SurfacePicker->PickFromListOn();

  this->SetEnabled(1);

  mapper->Delete();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::PlaceWidget(double bounds[6])
{

}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetEnabled(int enabling)
{
  if ( ! this->Interactor )
    {
    return;
    }

  if ( enabling )
    {
    vtkDebugMacro(<<"Enabling ImageWorkspace Widget");

    if ( this->Enabled ) //already enabled, just return
      {
      return;
      }

    if ( ! this->CurrentRenderer )
      {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(0 , 0));

      if (this->CurrentRenderer == NULL)
        {
        return;
        }
      }

    this->Enabled = 1;

    this->AddObservers();

    this->CurrentRenderer->AddActor(this->SurfaceActor);

    if ( !this->ViewMode )
      this->SetSeedsVisibility(1);

    this->InvokeEvent(vtkCommand::EnableEvent,NULL);
    }

  else //disabling----------------------------------------------------------
    {
    vtkDebugMacro(<<"Disabling Widget");

    if ( ! this->Enabled ) //already disabled, just return
      {
      return;
      }
    this->Enabled = 0;

    this->RemoveObservers();

    this->CurrentRenderer->RemoveActor(this->SurfaceActor);

    this->SetSeedsVisibility(0);

    this->CurrentSeed = NULL;
    this->InvokeEvent(vtkCommand::DisableEvent,NULL);
    this->SetCurrentRenderer(NULL);
    }

  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetSeedsVisibility(int v)
{
  this->SetSourceSeedsVisibility(v);
  this->SetTargetSeedsVisibility(v);

  if ( this->PreAddSeed )
    {
    if ( v )
      {
      this->CurrentRenderer->AddActor(this->PreAddSeed->GetActor());
      this->PreAddSeed->GetActor()->SetProperty(this->PreAddSeedProperty);
      }
    else
      {
      this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());
      }
    }
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetSourceSeedsVisibility(int v)
{
  vtkHMSeed3DWidget *TempSeed3D;
  double n;

  if ( v )
    {
    n = this->SourceSeedsCollection->GetNumberOfItems();

    for (int i=0; i < n; i++)
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i));

      this->CurrentRenderer->AddActor(TempSeed3D->GetActor());
      TempSeed3D->GetActor()->SetProperty(this->SourceSeedProperty);
      }
    }
  else
    {
    n = this->SourceSeedsCollection->GetNumberOfItems();

    for (int i=0; i < n; i++)
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(i));

      this->CurrentRenderer->RemoveActor(TempSeed3D->GetActor());
      }
    }
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetTargetSeedsVisibility(int v)
{
  vtkHMSeed3DWidget *TempSeed3D;
  double n;

  if ( v )
    {
    n = this->TargetSeedsCollection->GetNumberOfItems();

    for (int i=0; i < n; i++)
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i));

      this->CurrentRenderer->AddActor(TempSeed3D->GetActor());
      TempSeed3D->GetActor()->SetProperty(this->TargetSeedProperty);
      }
    }
  else
    {
    n = this->TargetSeedsCollection->GetNumberOfItems();

    for (int i=0; i < n; i++)
      {
      TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(i));

      this->CurrentRenderer->RemoveActor(TempSeed3D->GetActor());
      }
    }
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::AddSourceSeed(int newSeed)
{
  if ( this->PreAddSeed )
    {
    this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());
    this->PreAddSeed->Delete();
    this->PreAddSeed = NULL;
    }
  this->SourceSeedsCollection->AddItem(vtkHMSeed3DWidget::New());

  this->CurrentSeed = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(this->SourceSeedsCollection->GetNumberOfItems()-1));

  this->CurrentSeed->Create();


  this->SourcePicker->AddPickList(this->CurrentSeed->GetActor());
  this->SourcePicker->PickFromListOn();

  double *point = this->Surface->GetPoint(newSeed);

  this->CurrentSeed->SetCenter(point);

  this->SizeHandles();

  this->PointIdSelected = newSeed;

  this->CurrentRenderer->AddActor(this->CurrentSeed->GetActor());
  this->CurrentSeed->GetActor()->SetProperty(this->SourceSeedProperty);
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::AddTargetSeed(int newSeed)
{
  if ( this->PreAddSeed )
    {
    this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());
    this->PreAddSeed->Delete();
    this->PreAddSeed = NULL;
    }
  this->TargetSeedsCollection->AddItem(vtkHMSeed3DWidget::New());

  this->CurrentSeed = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(this->TargetSeedsCollection->GetNumberOfItems()-1));

  this->CurrentSeed->Create();


  this->TargetPicker->AddPickList(this->CurrentSeed->GetActor());
  this->TargetPicker->PickFromListOn();

  double *point = this->Surface->GetPoint(newSeed);

  this->CurrentSeed->SetCenter(point);

  this->SizeHandles();

  this->PointIdSelected = newSeed;

  this->CurrentRenderer->AddActor(this->CurrentSeed->GetActor());
  this->CurrentSeed->GetActor()->SetProperty(this->TargetSeedProperty);
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::RemoveSourceSeed(int seedID)
{
  if ( (seedID < 0) || (seedID > this->SourceSeedsCollection->GetNumberOfItems()) )
    {
    vtkErrorMacro("Source seed is out of array");
    return;
    }
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(seedID));

  this->SourcePicker->DeletePickList(TempSeed3D->GetActor());
  this->CurrentRenderer->RemoveActor(TempSeed3D->GetActor());
  this->Interactor->Render();

  this->SourceSeedsCollection->RemoveItem(seedID);

  TempSeed3D->Delete();

  TempSeed3D = NULL;
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::RemoveTargetSeed(int seedID)
{
  if ( (seedID < 0) || (seedID > this->TargetSeedsCollection->GetNumberOfItems()) )
    {
    vtkErrorMacro("Target seed is out of array");
    return;
    }
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(seedID));

  this->TargetPicker->DeletePickList(TempSeed3D->GetActor());
  this->CurrentRenderer->RemoveActor(TempSeed3D->GetActor());
  this->Interactor->Render();

  this->TargetSeedsCollection->RemoveItem(seedID);

  TempSeed3D->Delete();

  TempSeed3D = NULL;
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SelectSourceSeed(int seedID)
{
  if ( this->PreAddSeed )
    this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());

  this->HighlightTargetSeed(NULL);
  this->HighlightSourceSeed(vtkHMSeed3DWidget::SafeDownCast(this->SourceSeedsCollection->GetItemAsObject(seedID))->GetActor());
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SelectTargetSeed(int seedID)
{
  if ( this->PreAddSeed )
    this->CurrentRenderer->RemoveActor(this->PreAddSeed->GetActor());

  this->HighlightSourceSeed(NULL);
  this->HighlightTargetSeed(vtkHMSeed3DWidget::SafeDownCast(this->TargetSeedsCollection->GetItemAsObject(seedID))->GetActor());
  this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
void vtkHM3DCenterlinesWidget::SetViewMode(int mode)
{
  this->ViewMode = mode;

  if ( mode )
    {
    this->SurfaceActor->SetProperty(this->SurfaceViewProperty);
    this->SetSeedsVisibility(0);
    this->RemoveObservers();
    }
  else
    {
    this->SurfaceActor->SetProperty(this->SurfaceProperty);
    this->SetSeedsVisibility(1);
    this->AddObservers();
    }

  this->Interactor->Render();
}
