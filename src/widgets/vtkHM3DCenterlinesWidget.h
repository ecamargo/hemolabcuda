/*
 * vtkHM3DCenterlinesWidget.h
 *
 *  Created on: Aug 18, 2009
 *      Author: igor
 */

#ifndef VTKHM3DCENTERLINESWIDGET_H_
#define VTKHM3DCENTERLINESWIDGET_H_

#include "vtk3DWidget.h"

class vtkLODActor;
class vtkCellPicker;
class vtkProp;
class vtkPolyData;
class vtkProperty;
class vtkCollection;

class vtkHMSeed3DWidget;

class VTK_EXPORT vtkHM3DCenterlinesWidget : public vtk3DWidget
{
public:
  vtkTypeRevisionMacro(vtkHM3DCenterlinesWidget,vtk3DWidget);

  // Description:
  // Instantiates new objects.
  static vtkHM3DCenterlinesWidget *New();

  // Description:
  // Executes method when mouse's left button is released.
  static void ProcessEvents(vtkObject* object,
                          unsigned long event,
                          void* clientdata,
                          void* calldata);

  // Description:
  // Pure virtual in Superclass. I don't actually get to use it, but it's pure virtual.
  virtual void PlaceWidget(double bounds[6]);

  // Description:
  // Sets the Input that will feed the widget.
  void SetInput(vtkPolyData *polyData);

  // Description:
  // Enables / Disables this widget.
  void SetEnabled(int enabling);

  // Description:
  // Add / Remove the widget's observer.
  void AddObservers();
  void RemoveObservers();

  // Description:
  // Adds a 3D Seed.
  void AddSourceSeed(int newSeed);
  void AddTargetSeed(int newSeed);

  // Description:
  // Remove source seed.
  void RemoveSourceSeed(int seedID);

  // Description:
  // Remove target seed.
  void RemoveTargetSeed(int seedID);

  // Description:
  // Select source seed.
  void SelectSourceSeed(int seedID);

  // Description:
  // Select target seed.
  void SelectTargetSeed(int seedID);


  //BTX
  int State;
  enum WidgetState
  {
    Start=0,
    Adding,
    Moving,
    SourceSelected,
    TargetSelected,
    Outside
  };
  //ETX

  vtkGetMacro(State, int);

  vtkSetMacro(PointIdSelected, int);
  vtkGetMacro(PointIdSelected, int);

  void SetSeedsVisibility(int v);
  void SetSourceSeedsVisibility(int v);
  void SetTargetSeedsVisibility(int v);

  // Description:
  // Highlight source seed and return index.
  int HighlightSourceSeed(vtkProp *prop);

  // Description:
  // Highlight target seed and return index.
  int HighlightTargetSeed(vtkProp *prop);

  void SetViewMode(int mode);

protected:

  // Description:
  // Constructor.
  vtkHM3DCenterlinesWidget();

  // Description:
  // Destructor.
  ~vtkHM3DCenterlinesWidget();

  // Description:
  // Updates the size of the widget resizable components.
  void SizeHandles();

  // Description:
  // LOD Actor that represents the surface.
  vtkLODActor *SurfaceActor;
  vtkPolyData *Surface;
  vtkProperty *SurfaceProperty;
  vtkProperty *SurfaceViewProperty;

  // Description:
  // Picker that allows the interaction commands.
  vtkCellPicker *SourcePicker;
  vtkCellPicker *TargetPicker;
  vtkCellPicker *SurfacePicker;

  vtkProperty *SourceSeedProperty;
  vtkProperty *TargetSeedProperty;
  vtkProperty *SelectedSeedProperty;
  vtkProperty *PreAddSeedProperty;

  // Description:
  // Stores the selected source seed.
  int SelectedSourceSeed;

  // Description:
  // Stores the selected target seed.
  int SelectedTargetSeed;

  // Description:
  // Collection that stores all the 3DSeeds.
  vtkCollection *SourceSeedsCollection;
  vtkCollection *TargetSeedsCollection;

  //EVENT METHODS ---------------------------------------------------------|

  // Description:
  // Executed when the mouse's left button is press.
  void OnLeftButtonPressEvent();

  // Description:
  // Executed when the mouse's left button is released.
  void OnLeftButtonReleaseEvent();

  // Description:
  // Executed when the mouse's right button is pressed.
  void OnRightButtonPressEvent();

  // Description:
  // Executed when the mouse's right button is released.
  void OnRightButtonReleaseEvent();

  void OnMouseMoveEvent();

  //-----------------------------------------------------------------------|



  // Description:
  // Current seed selected.
  vtkHMSeed3DWidget *CurrentSeed;

  // Description:
  // Seed before add source or target seed. Pre seed is added when click in the surface.
  vtkHMSeed3DWidget *PreAddSeed;

  // Description:
  // Point id selected.
  int PointIdSelected;

  // Description:
  // View mode.
  int ViewMode;

private:
  vtkHM3DCenterlinesWidget(const vtkHM3DCenterlinesWidget&);  //Not implemented
  void operator=(const vtkHM3DCenterlinesWidget&);  //Not implemented
};

#endif /* VTKHM3DCENTERLINESWIDGET_H_ */
