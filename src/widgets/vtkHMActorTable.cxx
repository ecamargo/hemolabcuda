/*
 * $Id: vtkHMActorTable.cxx 1899 2007-03-09 19:46:23Z igor $
 */

#include "vtkActor.h"
#include "vtkActor2D.h"
#include "vtkObjectFactory.h"
#include "vtkHMActorTable.h"
#include "vtkProperty.h"

#include "vtkHM1DStraightModelWidgetTable.h"

vtkCxxRevisionMacro(vtkHMActorTable, "$Rev: 1899 $");
vtkStandardNewMacro(vtkHMActorTable);


//---------------------------------------------------------------------------

vtkHMActorTable::vtkHMActorTable()
{
	this->current_seg=0;
	this->elem=0;
	this->node=0;
	this->aux=0;
	this->Last_Position=NULL;
}

//---------------------------------------------------------------------------

vtkHMActorTable::~vtkHMActorTable()
{
  ActorTable.erase(ActorTable.begin(), ActorTable.end());		
  ActorID.erase(ActorID.begin(), ActorID.end());		  
}


//---------------------------------------------------------------------------

void vtkHMActorTable::InsertType(vtkActor *ac, int type)	
{
  	ActorTable.insert(ActorTablePair(ac, type));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveType(vtkActor *ac)
{
	ActorTableMap::iterator it = this->ActorTable.find(ac);
	
	if ( it != this->ActorTable.end() )
		this->ActorTable.erase(it);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------


int vtkHMActorTable::GetActorType(vtkActor *ac)
{
		ActorTableMap::iterator it = ActorTable.find(ac);
		return (*it).second;
}


//---------------------------------------------------------------------------

void vtkHMActorTable::InsertID(vtkActor *ac, int id)	
{
	this->CountElement(ac, id);
	this->CountNode(ac, id);
		
	ActorID.insert(ActorTablePair(ac, id));
}

//---------------------------------------------------------------------------

void vtkHMActorTable::RemoveID(vtkActor *ac)
{
	ActorIDMap::iterator it = this->ActorID.find(ac);
	
	if ( it != this->ActorID.end() )
		this->ActorID.erase(it);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
	
int vtkHMActorTable::GetActorID(vtkActor *ac)
{
		ActorIDMap::iterator it = ActorID.find(ac);
		return (*it).second;
}	
	
//---------------------------------------------------------------------------

int vtkHMActorTable::GetElementOrder(vtkActor *ac)
{
	ActorElemMap::iterator it = ActorElem.find(ac);
		return (*it).second;
}

//---------------------------------------------------------------------------

int vtkHMActorTable::GetNodeOrder(vtkActor *ac)
{
	ActorNodeMap::iterator it = ActorNode.find(ac);
	return (*it).second;
}

//---------------------------------------------------------------------------

int vtkHMActorTable::GetNodeSegment(vtkActor *ac)
{
	ActorSegmentMap::iterator it = ActorSeg.find(ac);
	return (*it).second;
}

//---------------------------------------------------------------------------

void  vtkHMActorTable::CountElement(vtkActor *ac, int id)
{
    if (this->GetActorType(ac) == 3)
    {
		// somentre a primeira vez entra aqui
		if (this->current_seg==0)
		{ 
			this->current_seg=id;
		}
			
		if (id != this->current_seg)	
		{
			this->elem=0; // é outro segmento!
			this->current_seg=id;
			ActorElem.insert(ActorTablePair(ac,this->elem));
			this->elem++;
		}
		else 
		{
			ActorElem.insert(ActorTablePair(ac,this->elem));
			this->elem++;
		}
    }
}

//---------------------------------------------------------------------------

void  vtkHMActorTable::CountNode(vtkActor *ac, int id)
{
	if ((this->GetActorType(ac) == 0) && (id ==-1))
	{
		if ((aux != current_seg) && (aux != 0))
		{
			// houve mudanca de segmento
			// na analise do primeiro segmento nao entra aqui
			node=0;
			aux=current_seg;
		}
		
	double *CurrentPosition=ac->GetBounds();
	
	if (Last_Position != NULL)
		{
			// como 2 atores para cada nó são criados na  mesma posição, somente o ultimo é inserido na tabela
			// se x é igual a x ator antigo e y é igual a y ator antigo --> atores na mesma posicao 
			if ((CurrentPosition[0] == Last_Position[0]) && (CurrentPosition[1] == Last_Position[1]) && (CurrentPosition[2] == Last_Position[2] && (CurrentPosition[3] == Last_Position[3])  )  )
			     {
			      node++;
			      ActorNode.insert(ActorTablePair(ac,node));
			      ActorSeg.insert(ActorTablePair(ac,current_seg));	
			     }
			aux=current_seg;
		}
	Last_Position=CurrentPosition;
	}
}

//---------------------------------------------------------------------------

vtkActor *vtkHMActorTable::GetActorfromID(int id, int interaction)
{
	// interaction--> indica o numero de ator que este metodo deve buscar (tabela ACtorID) no momento.
	// por exemplo: um segmento pode ter 3 elementos sendo representado por tres atores.  
	ActorIDMap::iterator it = ActorID.begin();
	int i=0;	
	for (;it!=ActorID.end();it++)
	{
		 if ((*it).second==id)
		 	{
			 	i++;
			 	if 	(i==interaction)
			 	{
				 return (*it).first;
			 	 break;
			 	}
		 	}
	}
	return NULL;
}	

//---------------------------------------------------------------------------
void vtkHMActorTable::InsertArrayLineId(vtkActor *ac, int id)
{
 	this->ActorArrayLineId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::InsertArraySphereId(vtkActor *ac, int id)
{
 	this->ActorArraySphereId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveArrayLineId(vtkActor *ac)
{
	ActorArrayIdMap::iterator it = this->ActorArrayLineId.begin();
	ActorArrayIdMap::iterator remove = this->ActorArrayLineId.find(ac);
	
	if ( remove != this->ActorArrayLineId.end() )
		{
		while ( it != this->ActorArrayLineId.end() )
			{
			if ( it->second > remove->second )
				it->second--;
			it++;
			}
		
		this->ActorArrayLineId.erase(remove);
		}
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveArraySphereId(vtkActor *ac)
{
	ActorArrayIdMap::iterator it = this->ActorArraySphereId.begin();
	ActorArrayIdMap::iterator remove = this->ActorArraySphereId.find(ac);
	
	if ( remove != this->ActorArraySphereId.end() )
		{
		while ( it != this->ActorArraySphereId.end() )
			{
			if ( it->second > remove->second )
				it->second--;
			
			it++;
			}
			
		this->ActorArraySphereId.erase(remove);
		}
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
int  vtkHMActorTable::GetActorArrayLineId(vtkActor *ac)
{
	ActorArrayIdMap::iterator it = this->ActorArrayLineId.find(ac);
	return (*it).second;
}	

//---------------------------------------------------------------------------
int  vtkHMActorTable::GetActorArraySphereId(vtkActor *ac)
{
	ActorArrayIdMap::iterator it = this->ActorArraySphereId.find(ac);
	return (*it).second;
}	

//---------------------------------------------------------------------------

void vtkHMActorTable::InsertTerminalId(vtkActor *ac, vtkIdType id)	
{
 	ActorTerminalId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveTerminalId (vtkActor *ac)
{
	ActorTerminalIdMap::iterator it = this->ActorTerminalId.find(ac);
	
	if ( it != this->ActorTerminalId.end() )
		this->ActorTerminalId.erase(it);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
	
vtkIdType vtkHMActorTable::GetTerminalId(vtkActor *ac)
{
	ActorTerminalIdMap::iterator it = ActorTerminalId.find(ac);
	if ( it == ActorTerminalId.end() )
		return -1;
	else
		return (*it).second;
}	

//---------------------------------------------------------------------------	
	
void vtkHMActorTable::InsertSegmentId(vtkActor *ac, vtkIdType id)	
{
 	ActorSegmentId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------

void vtkHMActorTable::RemoveSegmentId(vtkActor *ac)
{
	ActorSegmentIdMap::iterator it = this->ActorSegmentId.find(ac);
	ActorSegmentIdMap::iterator remove = this->ActorSegmentId.find(ac);
	
	if ( it != this->ActorSegmentId.end() )
		this->ActorSegmentId.erase(remove);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------

vtkIdType vtkHMActorTable::GetSegmentId(vtkActor *ac)
{
	ActorSegmentIdMap::iterator it = ActorSegmentId.find(ac);
	if ( it == ActorSegmentId.end() )
		return -1;
	else
		return (*it).second;
}	

//---------------------------------------------------------------------------

void vtkHMActorTable::InsertNodeSegmentId(vtkActor *ac, vtkIdType id)	
{
 	ActorNodeSegmentId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveNodeSegmentId	(vtkActor *ac)
{
	ActorNodeSegmentIdMap::iterator it = this->ActorNodeSegmentId.find(ac);
	
	if ( it != this->ActorNodeSegmentId.end() )
		this->ActorNodeSegmentId.erase(it);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
	
vtkIdType vtkHMActorTable::GetNodeSegmentId(vtkActor *ac)
{
	ActorNodeSegmentIdMap::iterator it = ActorNodeSegmentId.find(ac);
	if ( it == ActorNodeSegmentId.end() )
		return -1;
	else
		return (*it).second;
}	

//---------------------------------------------------------------------------
void vtkHMActorTable::InsertElementSegmentId(vtkActor *ac, vtkIdType id)	
{
 	ActorElementSegmentId.insert(ActorTablePair(ac, id));		
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveElementSegmentId(vtkActor *ac)
{
	ActorElementSegmentIdMap::iterator it = this->ActorElementSegmentId.find(ac);
	
	if ( it != this->ActorElementSegmentId.end() )
		this->ActorElementSegmentId.erase(it);
	else
		vtkWarningMacro(<<"The object not exists in table.");
}

//---------------------------------------------------------------------------
	
vtkIdType vtkHMActorTable::GetElementSegmentId(vtkActor *ac)
{
	ActorElementSegmentIdMap::iterator it = ActorElementSegmentId.find(ac);
	if ( it == ActorElementSegmentId.end() )
		return -1;
	else
		return (*it).second;
}

//---------------------------------------------------------------------------
void vtkHMActorTable::RemoveAllInformationFromTables(vtkActor *ac)
{
	if ( this->GetActorType(ac) == SEGMENT )
		{
		this->RemoveArrayLineId(ac);
		this->RemoveElementSegmentId(ac);
		}
	else if ( this->GetActorType(ac) == NODE )
		{
		this->RemoveArraySphereId(ac);
		this->RemoveNodeSegmentId(ac);
		}
	else if ( (this->GetActorType(ac) == TERMINAL) || (this->GetActorType(ac) == HEART) )
		{
		this->RemoveArraySphereId(ac);
		this->RemoveTerminalId(ac);
		}
	
	this->RemoveType(ac);
	this->RemoveID(ac);
	this->RemoveSegmentId(ac);
	
}
