/*
 * $Id: vtkHMActorTable.h 1899 2007-03-09 19:46:23Z igor $
 */
/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHMActorTable.h,v $

=========================================================================*/
// .NAME vtkHM1DStraightModelWidget - 3D widget ....
// .SECTION Description
// Stores relationship between actor on the scene and the associated 
// elements like terminals, segments etc.

#ifndef __vtkHMActorTable_h
#define __vtkHMActorTable_h

#include "vtkObject.h"
#include "vtkActor.h"
#include "vtkActor2D.h"

#include <list> 
#include <map> 	

// Description:
// Table in which the relation between actor 
// and properties ID is made
class VTK_EXPORT vtkHMActorTable: public vtkObject
{
	
public:
//BTX
  typedef std::pair<vtkActor *,int> ActorTablePair;  
  typedef std::map<vtkActor *,int> ActorTableMap;
  typedef std::map<vtkActor *,int> ActorIDMap;    

  typedef std::map<vtkActor *,int > ActorElemMap; 
  typedef std::map<vtkActor *,int > ActorNodeMap; 
  typedef std::map<vtkActor *,int > ActorSegmentMap;
  
  
  typedef std::map<vtkActor *, vtkIdType> ActorTerminalIdMap;  
  typedef std::map<vtkActor *, vtkIdType> ActorSegmentIdMap;
  typedef std::map<vtkActor *, vtkIdType> ActorNodeSegmentIdMap;  
  typedef std::map<vtkActor *, vtkIdType> ActorElementSegmentIdMap;  
  typedef std::map<vtkActor *, vtkIdType> ActorArrayIdMap;
  
  //map<const char*, int> ages;
//ETX

  static vtkHMActorTable *New();
  vtkTypeRevisionMacro(vtkHMActorTable,vtkObject);

  // Description:
  // Class Constructor/Destructor
	vtkHMActorTable();
  ~vtkHMActorTable();	
	
  // Description:
  // Insert an actor and its type	
	void InsertType(vtkActor *ac, int type);
	
	// Description:
  // Remove an actor and its type
	void RemoveType(vtkActor *ac);
		
  // Description:
  // Get type of an actor
	int  GetActorType(vtkActor *ac);	

  // Description:
  // Insert an actor and its ID
	void InsertID(vtkActor *ac, int id);
	
	// Description:
  // Remove an actor and its ID
	void RemoveID(vtkActor *ac);
	
  // Description:
  // Get ID of an actor
	int  GetActorID(vtkActor *ac);
	
 // Description:
 // Returns the element order of one given actor	
	int GetElementOrder(vtkActor *ac);		
	
 // Description:	
 // Returns the node order (inside one segment) of one given node actor	
	int GetNodeOrder(vtkActor *ac);
	
 // Description:
 // returns the ID from the segment which the selected node is located
	int GetNodeSegment(vtkActor *ac);
				
 // Description:	
 // this method creates a map between the actor and the number of elements in a given segment. 	
	void  CountElement(vtkActor *ac, int id);
	
	
 // Description:
 // calculates the order of the node in one segment
    void  CountNode(vtkActor *ac, int id);
    
 // Description:
 // returns the actor number n (n is given by the ivar interaction) from a given id
    vtkActor *GetActorfromID(int id, int interaction);
   
  // Description:
  // Insert an actor and its array Id
//	void InsertArrayId(vtkActor *ac, int id);
	
	// Description:
  // Insert an actor and its array Id
	void InsertArrayLineId(vtkActor *ac, int id);
	
	// Description:
  // Insert an actor and its array Id
	void InsertArraySphereId(vtkActor *ac, int id);
	
	// Description:
  // Remove an actor and its array Id
//	void RemoveArrayId(vtkActor *ac);
	
	// Description:
  // Remove an actor and its array Id
	void RemoveArrayLineId(vtkActor *ac);
	
	// Description:
  // Remove an actor and its array Id
	void RemoveArraySphereId(vtkActor *ac);
	
	// Description:
  // Get ID of an actor
//	int  GetActorArrayId(vtkActor *ac);
	
	// Description:
  // Get ID of an actor
	int  GetActorArrayLineId(vtkActor *ac);
	
	// Description:
  // Get ID of an actor
	int  GetActorArraySphereId(vtkActor *ac);

	// Description:
  // Insert an actor and its terminal Id
	void InsertTerminalId	(vtkActor *ac, vtkIdType terminalId);
	
	// Description:
  // Remove an actor and its terminal Id
	void RemoveTerminalId (vtkActor *ac);

	// Description:
  // Get terminal Id of an actor
	vtkIdType  GetTerminalId(vtkActor *ac);
	
	
	// Description:
  // Insert an actor and its segment Id
	void InsertSegmentId	(vtkActor *ac, vtkIdType segmentId);
	
	// Description:
  // Remove an actor and its segment Id
	void RemoveSegmentId	(vtkActor *ac);

	// Description:
  // Get segment Id of an actor
	vtkIdType  GetSegmentId(vtkActor *ac);

	// Description:
  // Insert an actor and its node segment Id
	void InsertNodeSegmentId	(vtkActor *ac, vtkIdType nodeSegmentId);
	
	// Description:
  // Remove an actor and its node segment Id
	void RemoveNodeSegmentId	(vtkActor *ac);

	// Description:
  // Get Node Id in a Segment of an actor
	vtkIdType  GetNodeSegmentId(vtkActor *ac);


	// Description:
  // Insert an actor and its element segment Id
	void InsertElementSegmentId	(vtkActor *ac, vtkIdType elementSegmentId);
	
	// Description:
  // Remove an actor and its element segment Id
	void RemoveElementSegmentId	(vtkActor *ac);

	// Description:
  // Get Element Id in a Segment of an actor
	vtkIdType  GetElementSegmentId(vtkActor *ac);

	
	void RemoveAllInformationFromTables(vtkActor *ac);
	
//************************************************************ 
 
 
 
 
//BTX
  // Description:
  // Establish actors and types relation
  ActorTableMap ActorTable;

  // Description:
  // Description:
  // Establish actors and IDs relation
  ActorIDMap		ActorID;
  
 // Description:
 // Establish actors and element number in one segment
  ActorElemMap  ActorElem;
  // Description:
  // stores the relation between Actor and Node Order in one segment
  ActorNodeMap  ActorNode;
 
 // Description:
 // stores the relation between Node Actor and Segment
  ActorSegmentMap ActorSeg;
  
//************************************************************   
  // Description:
  // Establish actors and array Ids relation
//  ActorArrayIdMap		ActorArrayId;
  
  // Description:
  // Set the ids of lines in collection.
  ActorArrayIdMap		ActorArrayLineId;
  
  // Description:
  // Set the ids of spheres in collection.
  ActorArrayIdMap		ActorArraySphereId;
  
  ActorTerminalIdMap 				ActorTerminalId;
  ActorSegmentIdMap 				ActorSegmentId;
  ActorNodeSegmentIdMap 		ActorNodeSegmentId;
  ActorElementSegmentIdMap 	ActorElementSegmentId;  
//************************************************************   
//ETX	

private:

int current_seg;
int elem;
int node;
int aux;
double *Last_Position;

};


#endif
