#include "vtkHMBoxWidget.h"
#include "vtkObjectFactory.h"

#include "vtkRenderer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCellPicker.h"

#include "vtkPolyData.h"

vtkCxxRevisionMacro(vtkHMBoxWidget, "$Rev: 563 $");
vtkStandardNewMacro(vtkHMBoxWidget);

vtkHMBoxWidget::vtkHMBoxWidget()
{
  for (int i=0; i<6; i++)
  {
  	this->Bounds[i] = 0;
  }
  this->Clicked = 0;
}

vtkHMBoxWidget::~vtkHMBoxWidget()
{
}

double vtkHMBoxWidget::GetBound0()
{
  this->CalculateBounds();
  
  return this->Bounds[0];
}

double vtkHMBoxWidget::GetBound1()
{
  this->CalculateBounds();
  
  return this->Bounds[1];
}

double vtkHMBoxWidget::GetBound2()
{
  this->CalculateBounds();
  
  return this->Bounds[2];
}

double vtkHMBoxWidget::GetBound3()
{
  this->CalculateBounds();
  
  return this->Bounds[3];
}

double vtkHMBoxWidget::GetBound4()
{
  this->CalculateBounds();
  
  return this->Bounds[4];
}

double vtkHMBoxWidget::GetBound5()
{
  this->CalculateBounds();
  
  return this->Bounds[5];
}

void vtkHMBoxWidget::CalculateBounds()
{
  vtkPolyData *PolyData = vtkPolyData::New();
  this->GetPolyData(PolyData);
  
  PolyData->GetBounds(this->Bounds);
  
  PolyData->Delete();
}

//Event Methods ----------------------------------------------|
void vtkHMBoxWidget::OnMouseMove()
{
//  if(this->Interactor->GetShiftKey())
//  {
    if (this->Clicked)
    {
      this->CalculateBounds();
    }

    this->Superclass::OnMouseMove();
//  }
}

void vtkHMBoxWidget::OnLeftButtonDown()
{
  //Must click to know if a handle was picked.
//  this->Superclass::OnLeftButtonDown();

  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then pick the bounding box.
  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
    this->State = vtkBoxWidget::Outside;
    return;
    }
  
  vtkAssemblyPath *path;
  this->HandlePicker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->HandlePicker->GetPath();

  if ( path != NULL )
  {
  	this->Clicked = 1;
    this->CalculateBounds();
  	
    this->Superclass::OnLeftButtonDown();
  }
  else
  {
    if(this->Interactor->GetShiftKey())
    {
      this->Clicked = 1;
      this->CalculateBounds();
      this->Superclass::OnLeftButtonDown();
    }
  }
}

void vtkHMBoxWidget::OnLeftButtonUp()
{
//  if(this->Interactor->GetShiftKey())
//  {
    this->CalculateBounds();


      this->Superclass::OnLeftButtonUp();
/*
    else
    {
    //Using the Left mouse button to translate box.  
      this->Superclass::OnMiddleButtonUp();
    }
*/
    this->Clicked = 0;
//  }
}

void vtkHMBoxWidget::OnMiddleButtonDown()
{
  if(this->Interactor->GetShiftKey())
  {
    this->Clicked = 1;

    this->CalculateBounds();
  
    this->Superclass::OnMiddleButtonDown();
  }    
}

void vtkHMBoxWidget::OnMiddleButtonUp()
{
//  if(this->Interactor->GetShiftKey())
//  {
    this->CalculateBounds();
  
    this->Superclass::OnMiddleButtonUp();
  
    this->Clicked = 0;
//  }
}

void vtkHMBoxWidget::OnRightButtonDown()
{
  if(this->Interactor->GetShiftKey())
  {
    this->Clicked = 1;

    this->CalculateBounds();
  
    this->Superclass::OnRightButtonDown();
  }
}

void vtkHMBoxWidget::OnRightButtonUp()
{
  if(this->Interactor->GetShiftKey())
  {
    this->CalculateBounds();
  
    this->Superclass::OnRightButtonUp();
  
    this->Clicked = 0;
  }
}
//------------------------------------------------------------|

int vtkHMBoxWidget::GetClicked()
{
  return this->Clicked;
}
