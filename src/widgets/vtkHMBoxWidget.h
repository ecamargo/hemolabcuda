#ifndef __vtkHMBoxWidget_h
#define __vtkHMBoxWidget_h

#include "vtkBoxWidget.h"

class VTK_EXPORT vtkHMBoxWidget : public vtkBoxWidget
{
public:

  vtkTypeRevisionMacro(vtkHMBoxWidget, vtkBoxWidget);

  // Description:
  // Instantiates new objects.
  static vtkHMBoxWidget *New();
  
  virtual double GetBound0();
  virtual double GetBound1();
  virtual double GetBound2();
  virtual double GetBound3();
  virtual double GetBound4();
  virtual double GetBound5();

  int GetClicked();

protected:

  // Description:
  // Constructor.
  vtkHMBoxWidget();
  
  // Description:
  // Destructor.
  ~vtkHMBoxWidget();
  
  virtual void CalculateBounds();

  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnMiddleButtonDown();
  virtual void OnMiddleButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();
  
  double Bounds[6];
  
  int Clicked;


private:
  vtkHMBoxWidget(const vtkHMBoxWidget&);  //Not implemented
  void operator=(const vtkHMBoxWidget&);  //Not implemented
};

#endif
