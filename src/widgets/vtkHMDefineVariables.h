#ifndef VTKHMDEFINEVARIABLES_H_
#define VTKHMDEFINEVARIABLES_H_

//Define mode de uso selecionado na interface do paraview
//Usado em: vtkPVHMStraightModelWidget
#define MODE_EDITION				"Edition"
#define MODE_VISUALIZATION  "Input Visualization"
#define MODE_PLOT  					"Output Visualization"
#define MODE_TODETACHSUBTREE	"To Detach Sub-Tree"
#define MODE_ESPECIAL_TOOLS	"Especial Tools"

//Define mode de uso selecionado na interface do paraview
//Usado em: vtkHMStraightModelWidget e vtkPVHMStraightModelWidget
#define MODE_EDITION_VAL					0
#define MODE_VISUALIZATION_VAL		1
#define MODE_PLOT_VAL							2
#define MODE_TODETACHSUBTREE_VAL	3
#define MODE_ESPECIAL_TOOLS_VAL		4
#define MODE_SET_PATH_VAL         5 

//Define tipo de interpolação
//Usado em: vtkPVHMStraightModelWidget
#define INTERPOLATION_CONSTANT 	"Constant"
#define INTERPOLATION_LINEAR		"Linear"

//Define tipos de objetos existentes no widget
//Usado em: vtkHMStraightModelWidget, vtkPVHMStraightModelWidget e vtkSMHMStraightModelWidgetProxy
#define NODE			0
#define TERMINAL 	1
#define HEART 		2
#define SEGMENT 	3
#define CLIP 			4
#define STENT			5
#define STENOSIS	6
#define ANEURYSM	7
#define AGING			8

#endif /*VTKHMDEFINEVARIABLES_H_*/
