#include "vtkHMImagePlaneWidget.h"
#include "vtkObjectFactory.h"

#include "vtkImagePlaneWidget.h"
#include "vtkLODActor.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkCellPicker.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkCommand.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"

#include "vtkProperty.h"
#include "vtkImageData.h"

vtkCxxRevisionMacro(vtkHMImagePlaneWidget, "$Rev: 563 $");
vtkStandardNewMacro(vtkHMImagePlaneWidget);

vtkHMImagePlaneWidget::vtkHMImagePlaneWidget()
{
  this->Dimensions[0] = 0;
  this->Dimensions[1] = 0;
  
  this->Clicked = 0;
  
  this->OldPCoords[0] = 0;
  this->OldPCoords[1] = 0;
  this->OldPCoords[2] = 0;
}

vtkHMImagePlaneWidget::~vtkHMImagePlaneWidget()
{
}

void vtkHMImagePlaneWidget::OnMiddleButtonDown()
{
}

void vtkHMImagePlaneWidget::OnRightButtonDown()
{
}

void vtkHMImagePlaneWidget::SetDimensions(int *PassedDimensions)
{
  this->Dimensions[0] = PassedDimensions[0];
  this->Dimensions[1] = PassedDimensions[1];
}

void vtkHMImagePlaneWidget::SetDimensions(int PassedDimensions0, int PassedDimensions1)
{
  this->Dimensions[0] = PassedDimensions0;
  this->Dimensions[1] = PassedDimensions1;
}

void vtkHMImagePlaneWidget::OnLeftButtonDown()
{
  if(this->Interactor->GetShiftKey() || this->Interactor->GetControlKey())
  {
    this->Superclass::OnLeftButtonDown();
  
    this->Clicked = 0;

    double PCoords[3];
    this->PlanePicker->GetPCoords(PCoords);
  
    if (!(PCoords[0] == 0 && PCoords[1] == 0 && PCoords [2] == 0) || (PCoords[0] == this->OldPCoords[0] && PCoords[1] == this->OldPCoords[1] && PCoords[2] == this->OldPCoords[2]))
    {
   	  this->Clicked = 1;
    }

    this->OldPCoords[0] = PCoords[0];
    this->OldPCoords[1] = PCoords[1];
    this->OldPCoords[2] = PCoords[2];
  }
}

void vtkHMImagePlaneWidget::OnLeftButtonUp()
{
  this->Superclass::OnLeftButtonUp();

  this->Clicked = 0;
}

int vtkHMImagePlaneWidget::GetClicked()
{
  return this->Clicked;
}

void vtkHMImagePlaneWidget::SetClicked(int Flag)
{
  this->Clicked = Flag;
}
