#ifndef __vtkHMImagePlaneWidget_h
#define __vtkHMImagePlaneWidget_h

#include "vtkImagePlaneWidget.h"

class VTK_EXPORT vtkHMImagePlaneWidget : public vtkImagePlaneWidget
{
public:

  vtkTypeRevisionMacro(vtkHMImagePlaneWidget,vtkImagePlaneWidget);

  // Description:
  // Instantiates new objects.
  static vtkHMImagePlaneWidget *New();

  // Description:
  // Set the plane dimensions.
  void SetDimensions(int *PassedDimensions);

  // Description:
  // Set the plane dimensions.  
  void SetDimensions(int PassedDimensions0, int PassedDimensions1);

  // Description:
  // Set as the currently clicked plane.
  void SetClicked(int Flag);

  // Description:
  // Get the clicked status.
  int GetClicked();

protected:

  // Description:
  // Constructor.
  vtkHMImagePlaneWidget();
  
  // Description:
  // Destructor.
  ~vtkHMImagePlaneWidget();

  // Description:
  // Method invoked when the mouse's middle button is pressed while it's cursor is over that plane.
  virtual void OnMiddleButtonDown();

  // Description:
  // Method invoked when the mouse's left button is pressed.
  virtual void OnLeftButtonDown();

  // Description:
  // Method invoked when the mouse's right button is pressed.
  virtual void OnRightButtonDown();

  // Description:
  // Method invoked when the mouse's left button is released.
  virtual void OnLeftButtonUp();
  
  // Description:
  // Stores the plane pixel dimensions.
  int Dimensions[2];

  // Description:
  // Setted as 1 while this plane is clicked.
  int Clicked;

  // Description:
  // Setted as 1 while this plane is clicked.  
  double OldPCoords[3];

private:
  vtkHMImagePlaneWidget(const vtkHMImagePlaneWidget&);  //Not implemented
  void operator=(const vtkHMImagePlaneWidget&);  //Not implemented
};

#endif
