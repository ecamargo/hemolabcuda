#include "vtkHMImageWorkspaceWidget.h"
#include "vtkObjectFactory.h"

#include "vtkHMImagePlaneWidget.h"
#include "vtkHMSeed3DWidget.h"

#include "vtkLineSource.h"

#include "vtkLODActor.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkCellPicker.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkCommand.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"

#include "vtkProperty.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

#include "vtkCallbackCommand.h"

#include "vtkCollection.h"

vtkCxxRevisionMacro(vtkHMImageWorkspaceWidget, "$Rev: 563 $");
vtkStandardNewMacro(vtkHMImageWorkspaceWidget);

vtkHMImageWorkspaceWidget::vtkHMImageWorkspaceWidget()
{
  this->XPlane = vtkHMImagePlaneWidget::New();
  this->YPlane = vtkHMImagePlaneWidget::New();
  this->ZPlane = vtkHMImagePlaneWidget::New();
    
  this->XPlaneActor = vtkLODActor::New();
  this->YPlaneActor = vtkLODActor::New();
  this->ZPlaneActor = vtkLODActor::New();

  this->XPicker = vtkCellPicker::New();
  this->YPicker = vtkCellPicker::New();
  this->ZPicker = vtkCellPicker::New();  

  this->XIntersectionLine = vtkLineSource::New();
  this->YIntersectionLine = vtkLineSource::New();
  this->ZIntersectionLine = vtkLineSource::New();

  this->XIntersectionLineActor = vtkActor::New();
  this->YIntersectionLineActor = vtkActor::New();
  this->ZIntersectionLineActor = vtkActor::New();
  
  this->XIntersectionLineMapper = vtkPolyDataMapper::New();
  this->YIntersectionLineMapper = vtkPolyDataMapper::New();
  this->ZIntersectionLineMapper = vtkPolyDataMapper::New();

  this->img = vtkImageData::New();
  
  this->SeedsCollection = vtkCollection::New();

  this->Enabled = 0;
  
  this->WindowAndColorLevels[0] = 0;
  this->WindowAndColorLevels[1] = 0;
  
  this->EventCallbackCommand->SetCallback(vtkHMImageWorkspaceWidget::ProcessEvents);
  
  this->LastClickedVoxel[0] = 0;
  this->LastClickedVoxel[1] = 0;
  this->LastClickedVoxel[2] = 0;
  
  this->SelectedSeed3D = -1;
  
  this->SeedsAutoResizeFlag = 1;
  
  this->XAxeColor[0] = 1;
  this->XAxeColor[1] = 0;
  this->XAxeColor[2] = 0;

  this->YAxeColor[0] = 0.95;
  this->YAxeColor[1] = 0.95;
  this->YAxeColor[2] = 0;

  this->ZAxeColor[0] = 0;
  this->ZAxeColor[1] = 1;
  this->ZAxeColor[2] = 0;
  
  this->ActivePlaneColor[0] = 0;
  this->ActivePlaneColor[1] = 0;
  this->ActivePlaneColor[2] = 1;
}

vtkHMImageWorkspaceWidget::~vtkHMImageWorkspaceWidget()
{
  this->XPicker->Delete();
  this->YPicker->Delete();
  this->ZPicker->Delete();

  this->XPlaneActor->Delete();
  this->YPlaneActor->Delete();
  this->ZPlaneActor->Delete();

  this->XPlane->Delete();
  this->YPlane->Delete();
  this->ZPlane->Delete();

  this->XIntersectionLine->Delete();
  this->YIntersectionLine->Delete();
  this->ZIntersectionLine->Delete();

  this->XIntersectionLineActor->Delete();
  this->YIntersectionLineActor->Delete();
  this->ZIntersectionLineActor->Delete();

  this->XIntersectionLineMapper->Delete();
  this->YIntersectionLineMapper->Delete();
  this->ZIntersectionLineMapper->Delete();

  this->img->Delete();

  int NumberOfItems = this->SeedsCollection->GetNumberOfItems();
  vtkHMSeed3DWidget *TempSeed3D;

  for (int i=0; i < NumberOfItems; i++)
  {
    TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(i));
  
    TempSeed3D->Delete();
    TempSeed3D = NULL;
  }
  
  this->SeedsCollection->Delete();

  for (int i=0; i<3; i++)
  {
    this->XAxeColor[i] = 0;
    this->YAxeColor[i] = 0;
    this->ZAxeColor[i] = 0;
    this->ActivePlaneColor[i] = 0;
  }
}

void vtkHMImageWorkspaceWidget::ProcessEvents(vtkObject* vtkNotUsed(object), unsigned long event, void* clientdata, void* vtkNotUsed(calldata))
{
  vtkHMImageWorkspaceWidget* self = reinterpret_cast<vtkHMImageWorkspaceWidget *>( clientdata );
  switch(event)
  {
    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonReleaseEvent();
      break;

    case vtkCommand::RightButtonPressEvent:
      self->OnRightButtonPressEvent();
      break;
      
    case vtkCommand::RightButtonReleaseEvent:
      self->OnRightButtonReleaseEvent();
      break;
  }
}

void vtkHMImageWorkspaceWidget::OnLeftButtonReleaseEvent()
{
  double ClickedVoxel[4];
  int XFlag = 0, YFlag = 0, ZFlag = 0;

  for (int i = 0; i < 4; i ++)
  {
    ClickedVoxel[i] = 0;
  }

  if(this->XPlane->GetClicked())
  {
    XFlag = this->XPlane->GetCursorData(ClickedVoxel);

    if (ClickedVoxel[0]!=0 || ClickedVoxel[1]!=0 || ClickedVoxel[2]!=0 || ClickedVoxel[3]!=0)
    {
      for (int i = 0; i < 4; i++)
      {
        this->LastClickedVoxel[i] = ClickedVoxel[i];
      }
    }
  }

  if(this->YPlane->GetClicked())
  {
    YFlag = this->YPlane->GetCursorData(ClickedVoxel);

    if (ClickedVoxel[0]!=0 || ClickedVoxel[1]!=0 || ClickedVoxel[2]!=0 || ClickedVoxel[3]!=0)
    {
      for (int i = 0; i < 4; i++)
      {
        this->LastClickedVoxel[i] = ClickedVoxel[i];
      }
    }
  }

  if(this->ZPlane->GetClicked())
  {
    ZFlag = this->ZPlane->GetCursorData(ClickedVoxel);

    if (ClickedVoxel[0]!=0 || ClickedVoxel[1]!=0 || ClickedVoxel[2]!=0 || ClickedVoxel[3]!=0)
    {
      for (int i = 0; i < 4; i++)
      {
        this->LastClickedVoxel[i] = ClickedVoxel[i];
      }
    }
  }

  if (this->LastClickedVoxel[0] == 0 && this->LastClickedVoxel[1] == 0 && this->LastClickedVoxel[2] == 0)
  {
  	this->LastClickedVoxel[3] = 0;
  }
    
  this->StartInteraction();
  this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
}

void vtkHMImageWorkspaceWidget::OnRightButtonReleaseEvent()
{
  this->SizeHandles();
}

void vtkHMImageWorkspaceWidget::OnRightButtonPressEvent()
{
//  cout << "Right Button pressed" << endl;
}

void vtkHMImageWorkspaceWidget::SizeHandles()
{
  if (this->SeedsAutoResizeFlag)
  {
    double CameraPosition[3];
    this->CurrentRenderer->GetActiveCamera()->GetPosition(CameraPosition);
    double RealSize = 3.0;

    double NewDistance = (CameraPosition[0] * CameraPosition[0]) + (CameraPosition[1] * CameraPosition[1]) + (CameraPosition[2] * CameraPosition[2]);
    double NewSize = RealSize * (NewDistance / 300000);

    if (NewSize > 3.0)
    {
      NewSize = 3.0;
    }
  
    for (int i=0; i < this->SeedsCollection->GetNumberOfItems(); i++)
    {
      vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(i))->SetRadius(NewSize);
    }
  }
}

void vtkHMImageWorkspaceWidget::ManipulateObservers(int flag)
{
  if (flag == 1)
  {
    this->Interactor->AddObserver(vtkCommand::LeftButtonReleaseEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::RightButtonReleaseEvent, this->EventCallbackCommand, 1.0);
    this->Interactor->AddObserver(vtkCommand::RightButtonPressEvent, this->EventCallbackCommand, 1.0);
  }
  else if (flag == 0)
  {
        this->Interactor->RemoveObserver(this->EventCallbackCommand);
  }
  else
    vtkWarningMacro(<< "Wrong Flag. Couldn't add or remove any observer.");
  
}

void vtkHMImageWorkspaceWidget::SetWidgetInput(int Input)
{
//  cout << "  Server - WidgetInput: " << Input << endl;
}

void vtkHMImageWorkspaceWidget::SetInputFromDataServer(vtkImageData *PassedImageData)
{
  PassedImageData->Update();

  this->img->DeepCopy(PassedImageData);
  
  this->img->GetDimensions(this->Dimensions);
  this->img->GetSpacing(this->Spacing);
  this->img->GetExtent(this->Extent);

  double XYOrigin = (this->Dimensions[0] * Spacing[0] / 2) *-1;
  double ZOrigin = ((this->Dimensions[2] * Spacing[0]) * (Spacing[2] / Spacing[0])) /2 * -1;

  if (this->GetInput())
  {
    vtkImageData::SafeDownCast(this->GetInput())->SetExtent(this->Extent);
    vtkImageData::SafeDownCast(this->GetInput())->SetWholeExtent(this->Extent);
    vtkImageData::SafeDownCast(this->GetInput())->SetUpdateExtent(this->Extent);
  }

  this->SetInput(this->img);
  PassedImageData = NULL;
}

void vtkHMImageWorkspaceWidget::CreateImagesWorkspace()
{
  vtkOutlineFilter *outline = vtkOutlineFilter::New();
  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();

  this->img = vtkImageData::SafeDownCast(this->GetInput());
  this->img->Update();
  this->img->GetDimensions(this->Dimensions);
  this->img->GetSpacing(this->Spacing);
  this->img->GetExtent(this->Extent);

  this->CreateXPlane();
  this->CreateYPlane();
  this->CreateZPlane();

  this->CreateIntersectionLines();
  
  outline->SetInput(this->img);
  
  mapper->SetInput(outline->GetOutput());

  this->XPlaneActor->SetMapper(mapper);
  this->XPlaneActor->GetProperty()->SetColor(1, 1, 1);
  
  this->YPlaneActor->SetMapper(mapper);
  this->YPlaneActor->GetProperty()->SetColor(1, 1, 1);

  this->ZPlaneActor->SetMapper(mapper);
  this->ZPlaneActor->GetProperty()->SetColor(1, 1, 1);

  this->XPicker->SetTolerance(0.005);
  this->YPicker->SetTolerance(0.005);
  this->ZPicker->SetTolerance(0.005);

  this->XPlane->GetWindowLevel(this->WindowAndColorLevels);

  outline->Delete();
  mapper->Delete();

  this->Render();
  this->StartInteraction();
}

void vtkHMImageWorkspaceWidget::CreateXPlane()
{
  this->XPlane->SetInteractor(this->Interactor);
  this->XPlane->SetInput(this->img);
  this->XPlane->SetPicker(this->XPicker);
  this->XPlane->GetPlaneProperty()->SetColor(this->XAxeColor);
  this->XPlane->TextureInterpolateOn();
  this->XPlane->SetResliceInterpolateToCubic();
  this->XPlane->SetPlaneOrientationToXAxes();
  this->XPlane->SetSliceIndex(0);
  this->XPlane->SetTextureVisibility(1);
  this->XPlane->DisplayTextOn();
  this->XPlane->SetRightButtonAction(0);

  this->XPlane->GetSelectedPlaneProperty()->SetColor(this->ActivePlaneColor);
  this->XPlane->GetCursorProperty()->SetColor(this->ActivePlaneColor);
    
  this->XPlane->SetDimensions(this->Dimensions[1], this->Dimensions[2]);
}

void vtkHMImageWorkspaceWidget::CreateYPlane()
{
  this->YPlane->SetInteractor(this->Interactor);
  this->YPlane->SetInput(this->img);
  this->YPlane->SetPicker(this->YPicker);
  this->YPlane->GetPlaneProperty()->SetColor(this->YAxeColor);
  this->YPlane->TextureInterpolateOn();
  this->YPlane->SetResliceInterpolateToCubic();
  this->YPlane->SetPlaneOrientationToYAxes();
  this->YPlane->SetSliceIndex(0);
  this->YPlane->SetTextureVisibility(1);
  this->YPlane->DisplayTextOn();
  this->YPlane->SetRightButtonAction(0);

  this->YPlane->GetSelectedPlaneProperty()->SetColor(this->ActivePlaneColor);
  this->YPlane->GetCursorProperty()->SetColor(this->ActivePlaneColor);

  this->YPlane->SetDimensions(this->Dimensions[2], this->Dimensions[0]);
}

void vtkHMImageWorkspaceWidget::CreateZPlane()
{
  this->ZPlane->SetInteractor(this->Interactor);
  this->ZPlane->SetInput(this->img);
  this->ZPlane->SetPicker(this->ZPicker);
  this->ZPlane->GetPlaneProperty()->SetColor(0, 1, 0);
  this->ZPlane->GetPlaneProperty()->SetAmbientColor(this->ZAxeColor);
  this->ZPlane->TextureInterpolateOn();
  this->ZPlane->SetResliceInterpolateToCubic();
  this->ZPlane->SetPlaneOrientationToZAxes();
  this->ZPlane->SetSliceIndex(0);
  this->ZPlane->SetTextureVisibility(1);
  this->ZPlane->DisplayTextOn();
  this->ZPlane->SetRightButtonAction(0);

  this->ZPlane->GetSelectedPlaneProperty()->SetColor(this->ActivePlaneColor);
  this->ZPlane->GetCursorProperty()->SetColor(0, 0, 1);

  this->ZPlane->SetDimensions(this->Dimensions[0], this->Dimensions[1]);
}

void vtkHMImageWorkspaceWidget::CreateIntersectionLines()
{
  this->CreateXIntersectionLine();
  this->CreateYIntersectionLine();
  this->CreateZIntersectionLine();
}

void vtkHMImageWorkspaceWidget::CreateXIntersectionLine()
{
  this->XIntersectionLine->SetResolution(10);

  this->UpdateXIntersectionLinePosition();
}

void vtkHMImageWorkspaceWidget::CreateYIntersectionLine()
{
  this->YIntersectionLine->SetResolution(10);

  this->UpdateYIntersectionLinePosition();
}

void vtkHMImageWorkspaceWidget::CreateZIntersectionLine()
{
  this->ZIntersectionLine->SetResolution(10);

  this->UpdateZIntersectionLinePosition();
}

void vtkHMImageWorkspaceWidget::UpdateXIntersectionLinePosition()
{
  double XCenter[3], YCenter[3], ZCenter[3];
  double Point1[3], Point2[3];
  
  this->XPlane->GetCenter(XCenter);
  this->YPlane->GetCenter(YCenter);
  this->ZPlane->GetCenter(ZCenter);

  this->YPlane->GetPoint1(Point1);
  this->YPlane->GetPoint2(Point2);

  this->XIntersectionLine->SetPoint1(Point1[0], YCenter[1], ZCenter[2]);
  this->XIntersectionLine->SetPoint2(Point2[0], YCenter[1], ZCenter[2]);
}

void vtkHMImageWorkspaceWidget::UpdateYIntersectionLinePosition()
{
  double XCenter[3], YCenter[3], ZCenter[3];
  double Point1[3], Point2[3];
  
  this->XPlane->GetCenter(XCenter);
  this->YPlane->GetCenter(YCenter);
  this->ZPlane->GetCenter(ZCenter);
 
  this->ZPlane->GetPoint1(Point1);
  this->ZPlane->GetPoint2(Point2);
 
  this->YIntersectionLine->SetPoint1(XCenter[0], Point1[1], ZCenter[2]);
  this->YIntersectionLine->SetPoint2(XCenter[0], Point2[1], ZCenter[2]);
}

void vtkHMImageWorkspaceWidget::UpdateZIntersectionLinePosition()
{
  double XCenter[3], YCenter[3], ZCenter[3];
  double Point1[3], Point2[3];

  this->XPlane->GetCenter(XCenter);
  this->YPlane->GetCenter(YCenter);
  this->ZPlane->GetCenter(ZCenter);
  
  this->XPlane->GetPoint1(Point1);
  this->XPlane->GetPoint2(Point2);
  
  this->ZIntersectionLine->SetPoint1(XCenter[0], YCenter[1], Point1[2]);
  this->ZIntersectionLine->SetPoint2(XCenter[0], YCenter[1], Point2[2]);
}

void vtkHMImageWorkspaceWidget::PlaceWidget(double bounds[6])
{
  this->XPlane->PlaceWidget(bounds);
  this->YPlane->PlaceWidget(bounds);
  this->ZPlane->PlaceWidget(bounds);
}

void vtkHMImageWorkspaceWidget::SetXPlaneCenter(int slice)
{
  this->XPlane->SetSliceIndex(slice);

  this->UpdateYIntersectionLinePosition();
  this->UpdateZIntersectionLinePosition();

  this->Render();
}

void vtkHMImageWorkspaceWidget::SetYPlaneCenter(int slice)
{
  this->YPlane->SetSliceIndex(slice);

  this->UpdateXIntersectionLinePosition();
  this->UpdateZIntersectionLinePosition();
  
  this->Render();
}

void vtkHMImageWorkspaceWidget::SetZPlaneCenter(int slice)
{
  this->ZPlane->SetSliceIndex(slice);

  this->UpdateXIntersectionLinePosition();
  this->UpdateYIntersectionLinePosition();

  this->Render();
}

void vtkHMImageWorkspaceWidget::EnableXPlane(int status)
{
  this->XPlane->SetTextureVisibility(status);
  this->Render();
}

void vtkHMImageWorkspaceWidget::EnableYPlane(int status)
{
  this->YPlane->SetTextureVisibility(status);
  this->Render();
}

void vtkHMImageWorkspaceWidget::EnableZPlane(int status)
{
  this->ZPlane->SetTextureVisibility(status);
  this->Render();
}

void vtkHMImageWorkspaceWidget::SetWindowAndColorLevels(double windowLevel, double colorLevel)
{
  this->XPlane->SetWindowLevel(windowLevel, colorLevel);
  this->YPlane->SetWindowLevel(windowLevel, colorLevel);
  this->ZPlane->SetWindowLevel(windowLevel, colorLevel);
  
  this->Render();
}

void vtkHMImageWorkspaceWidget::SetVisibility (int val)
{
  if (val)
  {
  	this->XPlane->On();
  	this->YPlane->On();
  	this->ZPlane->On();
  }
  else
  {
  	this->XPlane->Off();
  	this->YPlane->Off();
  	this->ZPlane->Off();
  }
  
  this->Render();
}

void vtkHMImageWorkspaceWidget::SetSeedsVisibility(int val)
{
  if (val)
  {
  	for (int i=0; i < this->SeedsCollection->GetNumberOfItems(); i++)
  	{
  	  this->CurrentRenderer->AddActor(vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(i))->GetActor());
  	}
  }
  else
  {
  	for (int i=0; i < this->SeedsCollection->GetNumberOfItems(); i++)
  	{
  	  this->CurrentRenderer->RemoveActor(vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(i))->GetActor());
  	}    
  }
  this->Render();
}

void vtkHMImageWorkspaceWidget::SetIntersectionLinesVisibility(int val)
{
  if (val)
  {
    this->GetCurrentRenderer()->AddActor(this->XIntersectionLineActor);
    this->GetCurrentRenderer()->AddActor(this->YIntersectionLineActor);
    this->GetCurrentRenderer()->AddActor(this->ZIntersectionLineActor);
  }
  else
  {
    this->GetCurrentRenderer()->RemoveActor(this->XIntersectionLineActor);
    this->GetCurrentRenderer()->RemoveActor(this->YIntersectionLineActor);
    this->GetCurrentRenderer()->RemoveActor(this->ZIntersectionLineActor);
  }
  this->Render();
}

void vtkHMImageWorkspaceWidget::Render()
{
  this->GetCurrentRenderer()->GetRenderWindow()->Render();
}

double vtkHMImageWorkspaceWidget::GetWindowLevel()
{
  return this->WindowAndColorLevels[0];
}

double vtkHMImageWorkspaceWidget::GetColorLevel()
{
  return this->WindowAndColorLevels[1];
}

void vtkHMImageWorkspaceWidget::SetEnabled(int enabling)
{
  if ( ! this->Interactor )
  {
    return;
  }
  
  if ( enabling )
  {
    vtkDebugMacro(<<"Enabling ImageWorkspace Widget");

    if ( this->Enabled ) //already enabled, just return
    {
      return;
    }
    
    if ( ! this->CurrentRenderer )
    {
      this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(0 , 0));

      if (this->CurrentRenderer == NULL)
      {
        return;
      }
    }
 
    this->Enabled = 1;

    this->Interactor = this->GetCurrentRenderer()->GetRenderWindow()->GetInteractor();
  
    this->Interactor->SetRenderWindow(this->GetCurrentRenderer()->GetRenderWindow());

    this->Interactor->Enable();

    this->XPlane->SetInteractor(this->Interactor);
    this->XPlane->On();
    this->GetCurrentRenderer()->AddActor(this->XPlaneActor);
  
    this->YPlane->SetInteractor(this->Interactor);
    this->YPlane->On();
    this->GetCurrentRenderer()->AddActor(this->YPlaneActor);
    
    this->ZPlane->SetInteractor(this->Interactor);
    this->ZPlane->On();
    this->GetCurrentRenderer()->AddActor(this->ZPlaneActor);


    this->XIntersectionLineMapper->SetInput(this->XIntersectionLine->GetOutput());
    this->XIntersectionLineActor->SetMapper(this->XIntersectionLineMapper);
    this->XIntersectionLineActor->GetProperty()->SetColor(this->XAxeColor);
    this->GetCurrentRenderer()->AddActor(this->XIntersectionLineActor);

    this->YIntersectionLineMapper->SetInput(this->YIntersectionLine->GetOutput());
    this->YIntersectionLineActor->SetMapper(this->YIntersectionLineMapper);
    this->YIntersectionLineActor->GetProperty()->SetColor(this->YAxeColor);
    this->GetCurrentRenderer()->AddActor(this->YIntersectionLineActor);

    this->ZIntersectionLineMapper->SetInput(this->ZIntersectionLine->GetOutput());
    this->ZIntersectionLineActor->SetMapper(this->ZIntersectionLineMapper);
    this->ZIntersectionLineActor->GetProperty()->SetColor(this->ZAxeColor);
    this->GetCurrentRenderer()->AddActor(this->ZIntersectionLineActor);
  
    this->Interactor->Start();
  }

  else //disabling----------------------------------------------------------
  {
    vtkDebugMacro(<<"Disabling ImageWorkspace Widget");

    if ( ! this->Enabled ) //already disabled, just return
    {
      return;
    }
    this->Enabled = 0;

    this->CurrentRenderer->RemoveActor(this->XPlaneActor);
    this->CurrentRenderer->RemoveActor(this->YPlaneActor);
    this->CurrentRenderer->RemoveActor(this->ZPlaneActor);
    
    this->GetCurrentRenderer()->RemoveActor(this->XIntersectionLineActor);
    this->GetCurrentRenderer()->RemoveActor(this->YIntersectionLineActor);
    this->GetCurrentRenderer()->RemoveActor(this->ZIntersectionLineActor);

    this->SetSeedsVisibility(0);

    this->SetCurrentRenderer(NULL);

    this->XPlane->GetInteractor()->Disable();
    this->XPlane->SetInteractor(NULL);

    this->YPlane->GetInteractor()->Disable();
    this->YPlane->SetInteractor(NULL);

    this->ZPlane->GetInteractor()->Disable();
    this->ZPlane->SetInteractor(NULL);

    this->Interactor->Disable();
  }
  this->Interactor->Render();
}

void vtkHMImageWorkspaceWidget::SetDimensions(int XDimension, int YDimension, int ZDimension)
{
  this->Dimensions[0] = XDimension;
  this->Dimensions[1] = YDimension;
  this->Dimensions[2] = ZDimension;
}

void vtkHMImageWorkspaceWidget::SetSpacing(double XSpacing, double YSpacing, double ZSpacing)
{
  this->Spacing[0] = XSpacing;
  this->Spacing[1] = YSpacing;
  this->Spacing[2] = ZSpacing;
}

int vtkHMImageWorkspaceWidget::GetExtent0()
{
  return this->Extent[0];
}

int vtkHMImageWorkspaceWidget::GetExtent1()
{
  return this->Extent[1];
}

int vtkHMImageWorkspaceWidget::GetExtent2()
{
  return this->Extent[2];
}

int vtkHMImageWorkspaceWidget::GetExtent3()
{
  return this->Extent[3];
}

int vtkHMImageWorkspaceWidget::GetExtent4()
{
  return this->Extent[4];
}

int vtkHMImageWorkspaceWidget::GetExtent5()
{
  return this->Extent[5];
}

int *vtkHMImageWorkspaceWidget::GetDimensionsFromOutput()
{
  return this->img->GetDimensions();
}

int vtkHMImageWorkspaceWidget::GetDimension(int Index)
{
  int *TempDimensions;
  TempDimensions = this->GetDimensionsFromOutput();

  return TempDimensions[Index];
}

int vtkHMImageWorkspaceWidget::GetXDimension()
{
  return this->GetDimension(0);
}

int vtkHMImageWorkspaceWidget::GetYDimension()
{
  return this->GetDimension(1);
}

int vtkHMImageWorkspaceWidget::GetZDimension()
{
  return this->GetDimension(2);
}

double *vtkHMImageWorkspaceWidget::GetSpacingsFromOutput()
{
  return this->img->GetSpacing();
}

double vtkHMImageWorkspaceWidget::GetSpacing(int Index)
{
  double *TempSpacings;
  TempSpacings = this->GetSpacingsFromOutput();
  return TempSpacings[Index];
}

double vtkHMImageWorkspaceWidget::GetXSpacing()
{
  return this->GetSpacing(0);
}

double vtkHMImageWorkspaceWidget::GetYSpacing()
{
  return this->GetSpacing(1);
}

double vtkHMImageWorkspaceWidget::GetZSpacing()
{
  return this->GetSpacing(2);
}

int *vtkHMImageWorkspaceWidget::GetDimensions()
{
  return this->Dimensions;
}

double *vtkHMImageWorkspaceWidget::GetSpacing()
{
  return this->Spacing;
}

double vtkHMImageWorkspaceWidget::GetXOfLastClickedVoxel()
{
  return this->LastClickedVoxel[0];
}

double vtkHMImageWorkspaceWidget::GetYOfLastClickedVoxel()
{
  return this->LastClickedVoxel[1];
}

double vtkHMImageWorkspaceWidget::GetZOfLastClickedVoxel()
{
  return this->LastClickedVoxel[2];
}

double vtkHMImageWorkspaceWidget::GetIntensityOfLastClickedVoxel()
{
  return this->LastClickedVoxel[3];
}

void vtkHMImageWorkspaceWidget::AddSeedFromLastClickedVoxel(int Flag)
{
  this->AddSeed(this->LastClickedVoxel);
}

void vtkHMImageWorkspaceWidget::AddSeed(double *PassedSeed)
{
  double CameraPosition[3];
  this->CurrentRenderer->GetActiveCamera()->GetPosition(CameraPosition);
  
  this->SeedsCollection->AddItem(vtkHMSeed3DWidget::New());
  
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(this->SeedsCollection->GetNumberOfItems()-1));

  double Center[3];
  
  Center[0] = PassedSeed[0] * this->Spacing[0];
  Center[1] = PassedSeed[1] * this->Spacing[1];
  Center[2] = PassedSeed[2] * this->Spacing[2];

  TempSeed3D->SetCenter(Center);
  TempSeed3D->SetRadius(3.0);
  this->SizeHandles();
  TempSeed3D->Create();

  this->CurrentRenderer->AddActor(TempSeed3D->GetActor());
  this->Render();
  
  TempSeed3D = NULL;
}
  
void vtkHMImageWorkspaceWidget::RemoveSeed(int PassedID)
{
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(PassedID));

  this->CurrentRenderer->RemoveActor(TempSeed3D->GetActor());
  this->Render();
  
  this->SeedsCollection->RemoveItem(PassedID);
  
  TempSeed3D->Delete();

  TempSeed3D = NULL;
}

vtkHMSeed3DWidget *vtkHMImageWorkspaceWidget::GetSeed(int PassedID)
{
  return vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(PassedID));
}

void vtkHMImageWorkspaceWidget::SelectSeed(int PassedID)
{
	
  if (this->SelectedSeed3D != -1 && this->SelectedSeed3D < this->SeedsCollection->GetNumberOfItems())
  {
  	this->DeselectSeed3D(this->SelectedSeed3D);
  }
  
  this->SelectedSeed3D = PassedID;  
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(PassedID));
  
  TempSeed3D->SetSelected(1);
  
  TempSeed3D = NULL;
  
  this->Render();
}

void vtkHMImageWorkspaceWidget::DeselectSeed3D(int PassedID)
{
  vtkHMSeed3DWidget *TempSeed3D = vtkHMSeed3DWidget::SafeDownCast(this->SeedsCollection->GetItemAsObject(PassedID));
  
  TempSeed3D->SetSelected(0);
  
  TempSeed3D = NULL;
  
  this->Render();  
}

void vtkHMImageWorkspaceWidget::SetSeedsAutoResize(int val)
{
  this->SeedsAutoResizeFlag = val;
  this->SizeHandles();
}

double vtkHMImageWorkspaceWidget::GetMinDataRange()
{
  double Range[2] = {0,0};
  this->img->GetPointData()->GetScalars()->GetRange(Range);
  
  return Range[0];
}

double vtkHMImageWorkspaceWidget::GetMaxDataRange()
{
  double Range[2] = {0,0};
  this->img->GetPointData()->GetScalars()->GetRange(Range);
  
  return Range[1];
}
