#ifndef __vtkHMImageWorkspaceWidget_h
#define __vtkHMImageWorkspaceWidget_h

#include "vtk3DWidget.h"

struct vtkClientServerID;

class vtkHMImagePlaneWidget;
class vtkActor;
class vtkLODActor;
class vtkCellPicker;

class vtkLineSource;

class vtkImageData;
class vtkPolyData;

class vtkCollection;
class vtkHMSeed3DWidget;

class vtkPolyDataMapper;

class VTK_EXPORT vtkHMImageWorkspaceWidget : public vtk3DWidget
{
public:
  vtkTypeRevisionMacro(vtkHMImageWorkspaceWidget,vtk3DWidget);

  // Description:
  // Instantiates new objects.
  static vtkHMImageWorkspaceWidget *New();

  // Description:
  // Executes method when mouse's left button is released.
  static void ProcessEvents(vtkObject* object, 
                          unsigned long event,
                          void* clientdata, 
                          void* calldata);

  // Description:
  // Returns this object's interactor.
  vtkRenderWindowInteractor* GetInteractor(){return this->Interactor;}

  void SetWidgetInput(int Input);

  // Description:
  // Pure virtual in Superclass. I don't actually get to use it, but it's pure virtual.
  virtual void PlaceWidget(double bounds[6]);

  // Description:
  // Sets the ImageDataInput that will feed the widget.
  void SetInputFromDataServer(vtkImageData *PassedImageData);

  // Description:
  // Method that creates the 3 planes and all other components of the widget.
  void CreateImagesWorkspace();

  // Description:
  // Enables / Disables this widget.
  void SetEnabled(int enabling);

  // Description:
  // Moves the widget's X plane its respective PV slider.
  void SetXPlaneCenter(int slice);

  // Description:
  // Moves the widget's Y plane its respective PV slider.
  void SetYPlaneCenter(int slice);

  // Description:
  // Moves the widget's Z plane its respective PV slider.
  void SetZPlaneCenter(int slice);

  // Description:
  // Enables / Disables the widget's X plane texture (volume slice).
  void EnableXPlane(int status);

  // Description:
  // Enables / Disables the widget's Y plane texture (volume slice).
  void EnableYPlane(int status);
  
  // Description:
  // Enables / Disables the widget's Z plane texture (volume slice).
  void EnableZPlane(int status);
  
  // Description:
  // Sets the WindowAndColorLevels class double array from X plane (could be readed from any of the two other planes) after readed from the volume.
  void SetWindowAndColorLevels(double windowLevel, double colorLevel);

  // Description:
  // Shows / hides the widget.
  void SetVisibility(int val);
  
  // Description:
  // Shows / hides the widget's seeds.
  void SetSeedsVisibility(int val);

  // Description:
  // Shows / hides the planes intersection lines.  
  void SetIntersectionLinesVisibility(int val);
  
  // Description:
  // Executes the Render() on the current renderer.
  void Render();

  // Description:
  // Returns the WindowLevel readed from the X plane.
  double GetWindowLevel();
  
  // Description:
  // Returns the ColorLevel readed from the X plane.
  double GetColorLevel();

  // Description:
  // Sets this widget's dimensions.
  void SetDimensions(int XDimension, int YDimension, int ZDimension);

  // Description:
  // Sets this widget's spacing.
  void SetSpacing(double XSpacing, double YSpacing, double ZSpacing);

  // Description:
  // Returns the first value of the widget's Extent array.
  int GetExtent0();

  // Description:
  // Returns the second value of the widget's Extent array.
  int GetExtent1();
  
  // Description:
  // Returns the third value of the widget's Extent array.
  int GetExtent2();

  // Description:
  // Returns the fourth value of the widget's Extent array.
  int GetExtent3();

  // Description:
  // Returns the fifth value of the widget's Extent array.
  int GetExtent4();

  // Description:
  // Returns the sixth value of the widget's Extent array.
  int GetExtent5();

  // Description:
  // Returns and three dimension array with the image data dimensions extracted directly from the output of this widget.
  int *GetDimensionsFromOutput();

  // Description:
  // Returns this class three dimension array that contains the image data dimensions.
  int *GetDimensions();
  
  // Description:
  // Returns X, Y or Z dimension, depending on the passed index.
  int GetDimension(int Index);

  // Description:
  // Returns the X dimensions of the image data.
  int GetXDimension();

  // Description:
  // Returns the Y dimensions of the image data.
  int GetYDimension();

  // Description:
  // Returns the Z dimensions of the image data.
  int GetZDimension();

  // Description:
  // Returns and three dimensions array with the image data spacing extracted directly from the output of this widget.
  double *GetSpacingsFromOutput();

  // Description:
  // Returns this class three dimensions array that contains the image data spacing.
  double *GetSpacing();

  // Description:
  // Returns X, Y or Z spacing, depending on the passed index.
  double GetSpacing(int Index);

  // Description:
  // Returns the X spacing of the image data.
  double GetXSpacing();

  // Description:
  // Returns the Y spacing of the image data.
  double GetYSpacing();
  
  // Description:
  // Returns the Z spacing of the image data.
  double GetZSpacing();

  // Description:
  // Returns the X coordinate of the last clicked voxel.
  double GetXOfLastClickedVoxel();

  // Description:
  // Returns the Y coordinate of the last clicked voxel.
  double GetYOfLastClickedVoxel();

  // Description:
  // Returns the Z coordinate of the last clicked voxel.
  double GetZOfLastClickedVoxel();

  // Description:
  // Returns the intensity of the last clicked voxel.
  double GetIntensityOfLastClickedVoxel();

  // Description:
  // Add / Remove the widget's observer.
  void ManipulateObservers(int flag);

  // Description:
  // Adds a 3D Seed from the last clicked Voxel attribute.  
  void AddSeedFromLastClickedVoxel(int Flag);

  // Description:
  // Adds a 3D Seed.  
  void AddSeed(double *PassedSeed);
  
  // Description:
  // Removes a 3D Seed.  
  void RemoveSeed(int PassedID);

  // Description:
  // Gets a 3D Seed from its collection.  
  vtkHMSeed3DWidget *GetSeed(int PassedID);

  // Description:
  // Select a seed from the passed ID.
  void SelectSeed(int PassedID);

  // Description:
  // Deselect a seed from the passed ID.
  void DeselectSeed3D(int PassedID);
  
  // Description:
  // Enables / Disables the widget's seeds auto resize mode.
  virtual void SetSeedsAutoResize(int val);
  
  // Description:
  // Gets the output minimum Intensity.
  double GetMinDataRange();

  // Description:
  // Gets the output maximum Intensity.
  double GetMaxDataRange();  
  
protected:

  // Description:
  // Constructor.
  vtkHMImageWorkspaceWidget();
  
  // Description:
  // Destructor.
  ~vtkHMImageWorkspaceWidget();

  // Description:
  // Creates and configurates the widget's X Plane.
  void CreateXPlane();

  // Description:
  // Creates and configurates the widget's Y Plane.
  void CreateYPlane();

  // Description:
  // Creates and configurates the widget's Z Plane.
  void CreateZPlane();
  
  // Description:
  // Creates the planes intersection lines.
  void CreateIntersectionLines();

  // Description:
  // Create and configurates the X planes intersection lines.
  void CreateXIntersectionLine();

  // Description:
  // Create and configurates the Y planes intersection lines.
  void CreateYIntersectionLine();

  // Description:
  // Create and configurates the Z planes intersection lines.
  void CreateZIntersectionLine();

  void UpdateXIntersectionLinePosition();
  void UpdateYIntersectionLinePosition();
  void UpdateZIntersectionLinePosition();

  // Description:
  // Updates the size of the widget resizable components.
  void SizeHandles();

  // Description:
  // VTK plane widget for reslicing data on the X axe.
  vtkHMImagePlaneWidget *XPlane;

  // Description:
  // VTK plane widget for reslicing data on the Y axe.
  vtkHMImagePlaneWidget *YPlane;
  
  // Description:
  // VTK plane widget for reslicing data on the Z axe.
  vtkHMImagePlaneWidget *ZPlane;

  // Description:
  // LOD Actor that represents the X plane on the ViewPort.
  vtkLODActor *XPlaneActor;

  // Description:
  // LOD Actor that represents the Y plane on the ViewPort.
  vtkLODActor *YPlaneActor;

  // Description:
  // LOD Actor that represents the Z plane on the ViewPort.
  vtkLODActor *ZPlaneActor;

  // Description:
  // Actor that represents the X planes intersection line.  
  vtkActor *XIntersectionLineActor;

  // Description:
  // Actor that represents the Y planes intersection line.  
  vtkActor *YIntersectionLineActor;

  // Description:
  // Actor that represents the Z planes intersection line.  
  vtkActor *ZIntersectionLineActor;

  // Description:
  // Picker that allows the planes interaction commands.
  vtkCellPicker *XPicker;
  vtkCellPicker *YPicker;
  vtkCellPicker *ZPicker;

  // Description:
  // X axe planes intersection line.
  vtkLineSource *XIntersectionLine;

  // Description:
  // X axe planes intersection line.
  vtkLineSource *YIntersectionLine;
  
  // Description:
  // X axe planes intersection line.
  vtkLineSource *ZIntersectionLine;

  // Description:
  // X intersection line polydata mapper.
  vtkPolyDataMapper *XIntersectionLineMapper;
  
  // Description:
  // Y intersection line polydata mapper.
  vtkPolyDataMapper *YIntersectionLineMapper;
  
  // Description:
  // Z intersection line polydata mapper.
  vtkPolyDataMapper *ZIntersectionLineMapper;

  // Description:
  // Contains the axes respective RGB color representations for this widget.
  double XAxeColor[3];
  double YAxeColor[3];
  double ZAxeColor[3];

  // Description:
  // Contains the RGB color for the active plane.
  double ActivePlaneColor[3];
  
  // Description:
  // Image Data that stores a copy of the Image Data source / filter output. This is used on feeding the widget at the Render Server.
  vtkImageData *img;

  // Description:
  // Integer 6 dimension array that stores the Image Data extent information.
  int Extent[6];

  // Description:
  // Integer 3 dimension array that stores the Image Data dimensions information.
  int Dimensions[3];

  // Description:
  // Double 3 dimension array that stores the Image Data dimensions information.
  double Spacing[3];

  // Description:
  // Double two dimension array that stores the Window and color levels initially extracted from the planes.
  double WindowAndColorLevels[2];

  // Description:
  // Stores the Last Clicked Voxel (-1 value if no voxel's found).
  double LastClickedVoxel[4];
  
  // Description:
  // Stores the selected seed ID.
  int SelectedSeed3D;

  // Description:
  // Stores the selected seed ID.
  int SeedsAutoResizeFlag;
  
  // Description:
  // Collection that stores all the 3DSeeds.
  //BTX
  vtkCollection *SeedsCollection;
  //ETX
  
  //EVENT METHODS ---------------------------------------------------------|
  
  // Description:
  // Executed when the mouse's left button is released.
  void OnLeftButtonReleaseEvent();

  // Description:
  // Executed when the mouse's right button is pressed.   
  void OnRightButtonPressEvent();

  // Description:
  // Executed when the mouse's right button is released.  
  void OnRightButtonReleaseEvent();

  //-----------------------------------------------------------------------|
  
private:
  vtkHMImageWorkspaceWidget(const vtkHMImageWorkspaceWidget&);  //Not implemented
  void operator=(const vtkHMImageWorkspaceWidget&);  //Not implemented
};

#endif
