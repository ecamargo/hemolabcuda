#ifndef __vtkHMLineSource_h
#define __vtkHMLineSource_h

#include "vtkPolyDataAlgorithm.h"

class VTK_EXPORT vtkHMLineSource : public vtkPolyDataAlgorithm 
{
public:
  static vtkHMLineSource *New();
  vtkTypeRevisionMacro(vtkHMLineSource,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set position of first end point.
  vtkSetVector3Macro(Point1,double);
  vtkGetVectorMacro(Point1,double,3);

  // Description:
  // Set position of other end point.
  vtkSetVector3Macro(Point2,double);
  vtkGetVectorMacro(Point2,double,3);

  // Description:
  // Divide line into resolution number of pieces.
  vtkSetClampMacro(Resolution,int,1,VTK_LARGE_INTEGER);
  vtkGetMacro(Resolution,int);

protected:
  vtkHMLineSource(int res=1);
  ~vtkHMLineSource() {};

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  int RequestInformation(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  double Point1[3];
  double Point2[3];
  int Resolution;
private:
  vtkHMLineSource(const vtkHMLineSource&);  // Not implemented.
  void operator=(const vtkHMLineSource&);  // Not implemented.
};

#endif
