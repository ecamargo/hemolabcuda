#ifndef __vtkHMPointWidget_h
#define __vtkHMPointWidget_h

#include "vtk3DWidget.h"
#include "vtkCursor3D.h" // Needed for faster access to the Cursor3D

class vtkActor;
class vtkPolyDataMapper;
class vtkCellPicker;
class vtkPolyData;
class vtkProperty;

class VTK_EXPORT vtkHMPointWidget : public vtk3DWidget
{
public:
  // Description:
  // Instantiate this widget
  static vtkHMPointWidget *New();

  vtkTypeRevisionMacro(vtkHMPointWidget,vtk3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Methods that satisfy the superclass' API.
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}

  // Description:
  // Grab the polydata (including points) that defines the point. A
  // single point and a vertex compose the vtkPolyData.
  void GetPolyData(vtkPolyData *pd);
  
  // Description:
  // Set/Get the position of the point. Note that if the position is set
  // outside of the bounding box, it will be clamped to the boundary of
  // the bounding box.
  void SetPosition(double x, double y, double z)
    {this->Cursor3D->SetFocalPoint(x,y,z);}
  void SetPosition(double x[3])
    {this->SetPosition(x[0],x[1],x[2]);}
  double* GetPosition() 
    {return this->Cursor3D->GetFocalPoint();}
  void GetPosition(double xyz[3]) 
    {this->Cursor3D->GetFocalPoint(xyz);}

  // Description:
  // Turn on/off the wireframe bounding box.
  void SetOutline(int o)
    {this->Cursor3D->SetOutline(o);}
  int GetOutline()
    {return this->Cursor3D->GetOutline();}
  void OutlineOn()
    {this->Cursor3D->OutlineOn();}
  void OutlineOff()
    {this->Cursor3D->OutlineOff();}

  // Description:
  // Turn on/off the wireframe x-shadows.
  void SetXShadows(int o)
    {this->Cursor3D->SetXShadows(o);}
  int GetXShadows()
    {return this->Cursor3D->GetXShadows();}
  void XShadowsOn()
    {this->Cursor3D->XShadowsOn();}
  void XShadowsOff()
    {this->Cursor3D->XShadowsOff();}

  // Description:
  // Turn on/off the wireframe y-shadows.
  void SetYShadows(int o)
    {this->Cursor3D->SetYShadows(o);}
  int GetYShadows()
    {return this->Cursor3D->GetYShadows();}
  void YShadowsOn()
    {this->Cursor3D->YShadowsOn();}
  void YShadowsOff()
    {this->Cursor3D->YShadowsOff();}

  // Description:
  // Turn on/off the wireframe z-shadows.
  void SetZShadows(int o)
    {this->Cursor3D->SetZShadows(o);}
  int GetZShadows()
    {return this->Cursor3D->GetZShadows();}
  void ZShadowsOn()
    {this->Cursor3D->ZShadowsOn();}
  void ZShadowsOff()
    {this->Cursor3D->ZShadowsOff();}

  // Description:
  // If translation mode is on, as the widget is moved the bounding box,
  // shadows, and cursor are all translated simultaneously as the point
  // moves.
  void SetTranslationMode(int mode)
    { this->Cursor3D->SetTranslationMode(mode); this->Cursor3D->Update(); }
  int GetTranslationMode()
    { return this->Cursor3D->GetTranslationMode(); }
  void TranslationModeOn()
    { this->SetTranslationMode(1); }
  void TranslationModeOff()
    { this->SetTranslationMode(0); }
  
  // Description:
  // Convenience methods to turn outline and shadows on and off.
  void AllOn()
    {
      this->OutlineOn();
      this->XShadowsOn();
      this->YShadowsOn();
      this->ZShadowsOn();
    }
  void AllOff()
    {
      this->OutlineOff();
      this->XShadowsOff();
      this->YShadowsOff();
      this->ZShadowsOff();
    }

  // Description:
  // Get the handle properties (the little balls are the handles). The 
  // properties of the handles when selected and normal can be 
  // set.
  vtkGetObjectMacro(Property,vtkProperty);
  vtkGetObjectMacro(SelectedProperty,vtkProperty);
  
  // Description:
  // Set the "hot spot" size; i.e., the region around the focus, in which the
  // motion vector is used to control the constrained sliding action. Note the
  // size is specified as a fraction of the length of the diagonal of the 
  // point widget's bounding box.
  vtkSetClampMacro(HotSpotSize,double,0.0,1.0);
  vtkGetMacro(HotSpotSize,double);
  
protected:
  vtkHMPointWidget();
  ~vtkHMPointWidget();

//BTX - manage the state of the widget
  friend class vtkHMStraightModelWidget;
  
  int State;
  enum WidgetState
  {
    Start=0,
    Moving,
    Scaling,
    Translating,
    Outside
  };
//ETX
    
  // Handles the events
  static void ProcessEvents(vtkObject* object, 
                            unsigned long event,
                            void* clientdata, 
                            void* calldata);

  // ProcessEvents() dispatches to these methods.
  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnMiddleButtonDown();
  virtual void OnMiddleButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();
  
  // the cursor3D
  vtkActor          *Actor;
  vtkPolyDataMapper *Mapper;
  vtkCursor3D       *Cursor3D;
  void Highlight(int highlight);

  // Do the picking
  vtkCellPicker *CursorPicker;
  
  // Methods to manipulate the cursor
  int ConstraintAxis;
  void Translate(double *p1, double *p2);
  void Scale(double *p1, double *p2, int X, int Y);
  void MoveFocus(double *p1, double *p2);
  int TranslationMode;

  // Properties used to control the appearance of selected objects and
  // the manipulator in general.
  vtkProperty *Property;
  vtkProperty *SelectedProperty;
  void CreateDefaultProperties();
  
  // The size of the hot spot.
  double HotSpotSize;
  int DetermineConstraintAxis(int constraint, double *x);
  int WaitingForMotion;
  int WaitCount;
  
private:
  vtkHMPointWidget(const vtkHMPointWidget&);  //Not implemented
  void operator=(const vtkHMPointWidget&);  //Not implemented
};

#endif
