#include "vtkHMSeed3DWidget.h"
#include "vtkObjectFactory.h"

#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"

#include "vtkProperty.h"

vtkCxxRevisionMacro(vtkHMSeed3DWidget, "$Rev: 563 $");
vtkStandardNewMacro(vtkHMSeed3DWidget);

vtkHMSeed3DWidget::vtkHMSeed3DWidget()
{
  this->SphereSource = vtkSphereSource::New();
  this->SphereMapper = vtkPolyDataMapper::New();
  this->SphereActor = vtkActor::New();
}

vtkHMSeed3DWidget::~vtkHMSeed3DWidget()
{
  this->SphereSource->Delete();
  this->SphereMapper->Delete();
  this->SphereActor->Delete();
}

void vtkHMSeed3DWidget::PlaceWidget(double bounds[6])
{
}

void vtkHMSeed3DWidget::Create()
{
  this->SphereSource->SetThetaResolution(72);
  this->SphereSource->SetPhiResolution(72);

  this->SphereMapper->SetInput(SphereSource->GetOutput());
  this->SphereActor->SetMapper(SphereMapper);
  this->SphereActor->GetProperty()->SetColor(1.0, 0, 0);
}

vtkActor *vtkHMSeed3DWidget::GetActor()
{
  return this->SphereActor;
}

void vtkHMSeed3DWidget::SetCenter(double *PassedSeed)
{
  this->SphereSource->SetCenter(PassedSeed[0], PassedSeed[1], PassedSeed[2]);
}

void vtkHMSeed3DWidget::SetSelected(int Flag)
{
  if(Flag)
  {
  	this->SphereActor->GetProperty()->SetColor(0.0, 0.8, 0.8);
  }
  else
  {
    this->SphereActor->GetProperty()->SetColor(1.0, 0, 0);
  }
}

void vtkHMSeed3DWidget::SetRadius(double PassedRadius)
{
  this->SphereSource->SetRadius(PassedRadius);
}

void vtkHMSeed3DWidget::SetSeedColor(double r, double g, double b)
{
  this->SphereActor->GetProperty()->SetColor(r, g, b);
}
