#ifndef __vtkHMSeed3DWidget_h
#define __vtkHMSeed3DWidget_h

#include "vtk3DWidget.h"

class vtkActor;
class vtkSphereSource;
class vtkPolyDataMapper;
class vtkActor;

class VTK_EXPORT vtkHMSeed3DWidget : public vtk3DWidget
{
public:
  vtkTypeRevisionMacro(vtkHMSeed3DWidget, vtk3DWidget);

  // Description:
  // Instantiates new objects.
  static vtkHMSeed3DWidget *New();

  // Description:
  // Pure virtual in Superclass. I don't actually get to use it, but it's pure virtual.
  virtual void PlaceWidget(double bounds[6]);

  // Description:
  // Sets seed's (sphere) center.
  void SetCenter(double *PassedCenter);

  // Description:
  // Creates the seed from previously informed parameters.
  void Create();

  // Description:
  // Returns the seed's actor.
  vtkActor *GetActor();

  // Description:
  // Set's the selected / deselected mode for this seed.
  void SetSelected(int Flag);

  // Description:
  // Sets the seed radius.
  void SetRadius(double PassedRadius);

  void SetSeedColor(double r, double g, double b);

protected:

  // Description:
  // Constructor.
  vtkHMSeed3DWidget();

  // Description:
  // Destructor.
  ~vtkHMSeed3DWidget();

  vtkSphereSource *SphereSource;
  vtkPolyDataMapper *SphereMapper;
  vtkActor *SphereActor;


private:
  vtkHMSeed3DWidget(const vtkHMSeed3DWidget&);  //Not implemented
  void operator=(const vtkHMSeed3DWidget&);  //Not implemented
};

#endif
