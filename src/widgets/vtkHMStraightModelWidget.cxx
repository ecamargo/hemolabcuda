#include "vtkHMStraightModelWidget.h"
#include "vtkHM1DIniFile.h"
#include "vtkHMPointWidget.h"
#include "vtkHMDataOut.h"
#include "vtkHM1DSegment.h"
#include "vtkHM1DStraightModel.h"
#include "vtkHM1DStraightModelReader.h"
#include "vtkHM1DHeMoLabReader.h"
#include "vtkHM1DStraightModelSource.h"
#include "vtkHM1DTerminal.h"
#include "vtkHM1DTreeElement.h"
#include "vtkHMGraphicProperty.h"
#include "vtkHM1DStraightModelWriterPreProcessor.h"
#include "vtkHMUnstructuredGridTo1DModel.h"
#include "vtkHM1DStentPolyData.h"
#include "vtkHM1DStenosisPolyData.h"
#include "vtkHM1DAneurysmPolyData.h"
#include "vtkHMUtils.h"
#include "vtkHM1DKeepRemoveFilter.h"

#include "vtkActor.h"
#include "vtkAssemblyNode.h"
#include "vtkAssemblyPath.h"
#include "vtkCallbackCommand.h"
#include "vtkCamera.h"
#include "vtkCellArray.h"
#include "vtkCellPicker.h"
#include "vtkCommand.h"
#include "vtkDoubleArray.h"
#include "vtkFloatArray.h"
#include "vtkLineSource.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSphereSource.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkProcessModule.h"
#include "vtkCollection.h"
#include "vtkActorCollection.h"
#include "vtkMapperCollection.h"
#include "vtkConeSource.h"
#include "vtkVectorText.h"
#include<math.h>


vtkCxxRevisionMacro(vtkHMStraightModelWidget, "$Revision:$");
vtkStandardNewMacro(vtkHMStraightModelWidget);

// This class is used to coordinate the interaction between the point widget
// at the center of the line and the line widget. When the line is selected
// (as compared to the handles), a point widget appears at the selection
// point, which can be manipulated in the usual way.
class vtkHMPWCallback : public vtkCommand
{
	/*
	 * Esta classe eh a callback das linhas, elementos.
	 */
public:
  static vtkHMPWCallback *New()
    { return new vtkHMPWCallback; }
  virtual void Execute(vtkObject *vtkNotUsed(caller), unsigned long, void*)
    {
      double x[3];
      this->hmPointWidget->GetPosition(x);
      this->hmLineWidget->SetLinePosition(x);
    }
  vtkHMPWCallback():hmLineWidget(0),hmPointWidget(0) {}
  vtkHMStraightModelWidget  *hmLineWidget;
  vtkHMPointWidget *hmPointWidget;
};

// This class is used to coordinate the interaction between the point widget
// and the line widget.
class vtkHMPointWidgetCallback : public vtkCommand
{
	/*
	 * Esta classe eh a callback das esferas, terminais e nodes.
	 */
public:
  static vtkHMPointWidgetCallback *New()
    { return new vtkHMPointWidgetCallback; }
  virtual void Execute(vtkObject *vtkNotUsed(caller), unsigned long, void*)
    {
      double x[3];
      this->hmPointWidget->GetPosition(x);
      this->hmLineWidget->SetPoints(x);
      this->hmLineWidget->SetSubTreePoints(x);
    }
  vtkHMPointWidgetCallback():hmLineWidget(0),hmPointWidget(0) {}
  vtkHMStraightModelWidget  *hmLineWidget;
  vtkHMPointWidget *hmPointWidget;
};

// Begin the definition of the vtkHMStraightModelWidget methods
//
vtkHMStraightModelWidget::vtkHMStraightModelWidget()
{
	this->KeyPressActivationOn();

	this->StraightModel = NULL;
	this->StraightModelSource = NULL;
	this->StraightModelFilter = NULL;
	this->HeMoLabReader = NULL;
	this->StraightModelKeepRemoveFilter = NULL;
	this->NumberOfSegments = 0;

	//Array que aponta para os atores selecionados e
	//Array que aponta para os sources dos atores selecionados
	//quando está selecionado modo de visualização de gráficos
	for(int i=0; i<MAX_CURVES; i++)
		{
			this->SelectedActors[i] = NULL;
			this->PlotSource[i] = NULL;
		}

	this->AnimatedSelectedActor = NULL;

	//Array de propriedades dos atores selecionados no
	//modo de visualização de gráficos
	for(int i=0; i<MAX_CURVES; i++)
		this->SelectedActorsProperty[i] = vtkProperty::New();

	this->AnimatedSelectedActorProperty = vtkProperty::New();

	//Seta as cores de cada ator selecionado.
	//O vetor "PlotColor[][]", está definido no
	//arquivo gui/widgets/vtkHMGraphicProperty.h
	for (int i=0; i<MAX_CURVES; i++)
		{
		this->SelectedActorsProperty[i]->SetColor(PlotColor[i][0],PlotColor[i][1],PlotColor[i][2]);
		this->SelectedActorsProperty[i]->SetLineWidth(3.0);
		}

	//Seta as cores de cada ator selecionado.
	//O vetor "AnimatedPlotColor[][]", está definido no
	//arquivo gui/widgets/vtkHMGraphicProperty.h
	this->AnimatedSelectedActorProperty->SetColor(AnimatedPlotColor[0],AnimatedPlotColor[1],AnimatedPlotColor[2]);
	this->AnimatedSelectedActorProperty->SetLineWidth(3.0);

	this->StraightModelObjectSelected = -1;

	// Neste modo, não apresenta gráficos
	this->StraightModelMode	= MODE_VISUALIZATION_VAL;
	this->PlotType = NULL;

  this->HandleHeartProperty = NULL;
	this->HandleTerminalProperty = NULL;
	this->HandleNodeProperty = NULL;
	this->SelectedHandleProperty = NULL;
	this->LineProperty = NULL;
	this->SelectedLineProperty = NULL;
	this->MainStraightModelProperty = NULL;
	this->SubTreeHandleProperty = NULL;

	this->Interactor = NULL;

	this->Element_Selected=0;
  this->Segment_Selected=1;

	this->NumberOfGraphicsSum = 1;
	this->NumberOfGraphics = 1;

	this->EditionModeSelected = 0;

  this->GridNode		 = NULL;
  this->GridTerminal = NULL;

  this->GridLineMapper = vtkPolyDataMapper::New();
	this->GridLineActor = vtkActor::New();
	this->GridLineProperty = vtkProperty::New();
	this->GridLineProperty->SetLineWidth(3.0);
	this->GridLineProperty->SetColor(1.0,1.0,1.0);

	this->GridNodeMapper = vtkPolyDataMapper::New();
	this->GridNodeActor = vtkActor::New();
	this->GridNodeProperty = vtkProperty::New();
	this->GridNodeProperty->SetPointSize(4.5);
	this->GridNodeProperty->SetColor(0.1,0.5,0.1);

	this->GridTerminalMapper = vtkPolyDataMapper::New();
	this->GridTerminalActor = vtkActor::New();
	this->GridTerminalProperty = vtkProperty::New();
	this->GridTerminalProperty->SetPointSize(5.5);
	this->GridTerminalProperty->SetColor(1.0,1.0,0.0);

	this->GridNullElements1D3D = NULL;
	this->GridNullElements1D3DMapper = vtkPolyDataMapper::New();
	this->GridNullElements1D3DActor = vtkActor::New();
	this->GridNullElements1D3DProperty = vtkProperty::New();
	this->GridNullElements1D3DProperty->SetPointSize(4.5);
	this->GridNullElements1D3DProperty->SetLineWidth(3);
	this->GridNullElements1D3DProperty->SetColor(0.0,0.0,0.0);

	this->SphereActorSelected = vtkActor::New();
  this->SphereMapperSelected = vtkPolyDataMapper::New();
  this->SphereSourceSelected = vtkSphereSource::New();

  this->GridLineSelected = vtkHM1DStraightModelGrid::New();
  this->GridLineSelectedMapper = vtkPolyDataMapper::New();
	this->GridLineSelectedActor = vtkActor::New();

  this->GridTerminalSelected = vtkHM1DStraightModelGrid::New();
  this->GridTerminalSelectedMapper = vtkPolyDataMapper::New();
	this->GridTerminalSelectedActor = vtkActor::New();

	this->HeartActor = NULL;
	this->HeartMapper = NULL;
	this->HeartGeometry = NULL;

	this->NodePicker = NULL;
	this->TerminalPicker = NULL;
	this->LinePicker = NULL;
	this->ClipPicker = NULL;
	this->StentPicker = NULL;
	this->StenosisPicker = NULL;
	this->AneurysmPicker = NULL;

	this->GridLineWidget = NULL;
	this->GridNodeWidget = NULL;
	this->GridTerminalWidget = NULL;
	this->GridClipWidget = NULL;
	this->GridStentWidget = NULL;
	this->GridStenosisWidget = NULL;
	this->GridAneurysmWidget = NULL;

	this->GridLineCallback = NULL;
	this->GridNodeCallback = NULL;
	this->GridTerminalCallback = NULL;
	this->GridClipCallback = NULL;
	this->GridStentCallback = NULL;
	this->GridStenosisCallback = NULL;
	this->GridAneurysmCallback = NULL;

	this->Points = NULL;

	this->BeginSphereActorSelected = vtkActor::New();
  this->BeginSphereMapperSelected = vtkPolyDataMapper::New();
  this->BeginSphereSourceSelected = vtkSphereSource::New();

  this->EndSphereActorSelected = vtkActor::New();
  this->EndSphereMapperSelected = vtkPolyDataMapper::New();
  this->EndSphereSourceSelected = vtkSphereSource::New();

	this->ClipSegments = vtkIntArray::New();
	this->ClipSegments->SetNumberOfComponents(2);

	this->GridClip 				= vtkHM1DStraightModelGrid::New();
	this->GridClipActor 	= vtkActor::New();
	this->GridClipMapper 	= vtkPolyDataMapper::New();
	vtkPoints *p = vtkPoints::New();
	vtkCellArray *a = vtkCellArray::New();
	this->GridClipProperty = vtkProperty::New();
	this->GridClipProperty->SetPointSize(9.5);
	this->GridClipProperty->SetColor(0.0,0.0,1.0);

	this->GridClip->SetPoints(p);
	this->GridClip->SetVerts(a);
	p->Delete();
	a->Delete();
	this->GridClipMapper->SetInput(this->GridClip);

	this->GridClipActor->SetMapper(this->GridClipMapper);
  this->GridClipActor->SetProperty(this->GridClipProperty);

  this->GridStent 				= vtkHM1DStentPolyData::New();
	this->GridStentActor	 	= vtkActor::New();
	this->GridStentMapper 	= vtkPolyDataMapper::New();
	p = vtkPoints::New();
	a = vtkCellArray::New();
	this->GridStentProperty = vtkProperty::New();
	this->GridStentProperty->SetLineWidth(3.2);
	this->GridStentProperty->SetColor(1.0,0.5,0.0);

	this->GridStent->SetPoints(p);
	this->GridStent->SetLines(a);

	p->Delete();
	a->Delete();
	this->GridStentMapper->SetInput(this->GridStent);

	this->GridStentActor->SetMapper(this->GridStentMapper);
  this->GridStentActor->SetProperty(this->GridStentProperty);

  this->GridStenosis 				= vtkHM1DStenosisPolyData::New();
	this->GridStenosisActor	 	= vtkActor::New();
	this->GridStenosisMapper 	= vtkPolyDataMapper::New();
	p = vtkPoints::New();
	a = vtkCellArray::New();
	this->GridStenosisProperty = vtkProperty::New();
	this->GridStenosisProperty->SetLineWidth(3.2);
	this->GridStenosisProperty->SetColor(0.5,1.0,0.0);

	this->GridStenosis->SetPoints(p);
	this->GridStenosis->SetLines(a);

	p->Delete();
	a->Delete();
	this->GridStenosisMapper->SetInput(this->GridStenosis);

	this->GridStenosisActor->SetMapper(this->GridStenosisMapper);
  this->GridStenosisActor->SetProperty(this->GridStenosisProperty);

  this->GridAneurysm 				= vtkHM1DAneurysmPolyData::New();
	this->GridAneurysmActor	 	= vtkActor::New();
	this->GridAneurysmMapper 	= vtkPolyDataMapper::New();
	p = vtkPoints::New();
	a = vtkCellArray::New();
	this->GridAneurysmProperty = vtkProperty::New();
	this->GridAneurysmProperty->SetLineWidth(3.2);
	this->GridAneurysmProperty->SetColor(0.89,0.89,0.2);

	this->GridAneurysm->SetPoints(p);
	this->GridAneurysm->SetLines(a);

	p->Delete();
	a->Delete();
	this->GridAneurysmMapper->SetInput(this->GridAneurysm);

	this->GridAneurysmActor->SetMapper(this->GridAneurysmMapper);
  this->GridAneurysmActor->SetProperty(this->GridStenosisProperty);

  this->SurgicalInterventionsModeSelected = vtkHMStraightModelWidget::none;


	this->TextSource = vtkVectorText::New();
	this->TextMapper = vtkPolyDataMapper::New();
	this->TextActor = vtkActor::New();

	this->TextCollection  = vtkCollection::New();
	this->MapperCollection = vtkCollection::New();
	this->ActorCollection = vtkActorCollection::New();

	this->CouplingSphereMapperCollection =  vtkCollection::New();
	this->CouplingSphereSourceCollection =  vtkCollection::New();
	this->SphereActorCollection =  vtkActorCollection::New();

	this->CouplingActorCounter = 0;
	this->FullModelCode = 0;

	this->SubTreeListTerminals = vtkIdList::New();
	this->SubTreeListSegments = vtkIdList::New();

	this->AnimatedSphereActor = vtkActor::New();
	this->AnimatedSphereSource = vtkSphereSource::New();
	this->AnimatedSphereMapper = vtkPolyDataMapper::New();

	this->AnimatedLineActor = vtkActor::New();
	this->AnimatedLineSource = vtkLineSource::New();
	this->AnimatedLineMapper = vtkPolyDataMapper::New();

	this->MoveElement=false;

	this->TreeListChar = NULL;
	this->TreeListChar2 = NULL;

	this->BeginTerminal = 0;
	this->EndTerminal = 0;

}

vtkHMStraightModelWidget::~vtkHMStraightModelWidget()
{
	for(int i=0; i<MAX_CURVES; i++)
		{
		this->SelectedActorsProperty[i]->Delete();

		if(this->SelectedActors[i])
			{
			this->SelectedActors[i]->GetMapper()->Delete();
			this->SelectedActors[i]->Delete();
			this->PlotSource[i]->Delete();
			}
		}

	this->AnimatedSelectedActorProperty->Delete();

	this->AnimatedSelectedActor = NULL;

	this->DestructorRepresentation();

	this->StraightModelSource = NULL;
	this->StraightModelFilter = NULL;
	this->HeMoLabReader = NULL;
	this->StraightModelKeepRemoveFilter = NULL;
	this->StraightModel = NULL;

	this->PlotType = NULL;

	this->Interactor = NULL;

  this->GridLineMapper->Delete();
  this->GridLineMapper = NULL;
	this->GridLineActor->Delete();
	this->GridLineActor = NULL;
	this->GridLineProperty->Delete();
	this->GridLineProperty = NULL;

	this->GridNodeMapper->Delete();
	this->GridNodeMapper = NULL;
	this->GridNodeActor->Delete();
	this->GridNodeActor = NULL;
	this->GridNodeProperty->Delete();
	this->GridNodeProperty = NULL;

	if ( this->GridNode )
		this->GridNode->Delete();
	this->GridNode = NULL;

	this->GridTerminalMapper->Delete();
	this->GridTerminalMapper = NULL;
	this->GridTerminalActor->Delete();
	this->GridTerminalActor = NULL;
	this->GridTerminalProperty->Delete();
	this->GridTerminalProperty = NULL;

	if ( this->GridTerminal )
		this->GridTerminal->Delete();
	this->GridTerminal = NULL;

	this->GridNullElements1D3DMapper->Delete();
	this->GridNullElements1D3DMapper = NULL;
	this->GridNullElements1D3DActor->Delete();
	this->GridNullElements1D3DActor = NULL;
	this->GridNullElements1D3DProperty->Delete();
	this->GridNullElements1D3DProperty = NULL;

	if ( this->GridNullElements1D3D )
		this->GridNullElements1D3D->Delete();
	this->GridNullElements1D3D = NULL;

	this->SphereActorSelected->Delete();
	this->SphereActorSelected = NULL;
  this->SphereMapperSelected->Delete();
  this->SphereMapperSelected = NULL;
  this->SphereSourceSelected->Delete();
	this->SphereSourceSelected = NULL;

  this->GridLineSelected->Delete();
  this->GridLineSelected = NULL;

  this->GridLineSelectedMapper->Delete();
	this->GridLineSelectedActor->Delete();
	this->GridLineSelectedMapper = NULL;
	this->GridLineSelectedActor = NULL;

  this->GridTerminalSelected->Delete();
  this->GridTerminalSelected = NULL;

  this->GridTerminalSelectedMapper->Delete();
	this->GridTerminalSelectedActor->Delete();
	this->GridTerminalSelectedMapper = NULL;
	this->GridTerminalSelectedActor = NULL;

	if ( this->Points )
		this->Points->Delete();
	this->Points = NULL;

	this->BeginSphereActorSelected->Delete();
	this->BeginSphereActorSelected = NULL;
  this->BeginSphereMapperSelected->Delete();
  this->BeginSphereMapperSelected = NULL;
  this->BeginSphereSourceSelected->Delete();
	this->BeginSphereSourceSelected = NULL;

	this->EndSphereActorSelected->Delete();
	this->EndSphereActorSelected = NULL;
  this->EndSphereMapperSelected->Delete();
  this->EndSphereMapperSelected = NULL;
  this->EndSphereSourceSelected->Delete();
	this->EndSphereSourceSelected = NULL;

	this->ClipSegments->Delete();

	this->GridClip->Delete();
	this->GridClipActor->Delete();
	this->GridClipMapper->Delete();
	this->GridClipProperty->Delete();

	this->GridStent->Delete();
	this->GridStentActor->Delete();
	this->GridStentMapper->Delete();
	this->GridStentProperty->Delete();

	this->GridStenosis->Delete();
	this->GridStenosisActor->Delete();
	this->GridStenosisMapper->Delete();
	this->GridStenosisProperty->Delete();

	this->GridAneurysm->Delete();
	this->GridAneurysmActor->Delete();
	this->GridAneurysmMapper->Delete();
	this->GridAneurysmProperty->Delete();

	this->TextSource->Delete();
	this->TextMapper->Delete();
	this->TextActor->Delete();

	this->TextCollection->Delete();
	this->MapperCollection->Delete();
	this->ActorCollection->Delete();

	this->CouplingSphereMapperCollection->Delete();
	this->CouplingSphereSourceCollection->Delete();
	this->SphereActorCollection->Delete();

	this->SubTreeListTerminals->Delete();
	this->SubTreeListSegments->Delete();

	this->AnimatedSphereActor->Delete();
	this->AnimatedSphereSource->Delete();
	this->AnimatedSphereMapper->Delete();

	this->AnimatedLineActor->Delete();
	this->AnimatedLineSource->Delete();
	this->AnimatedLineMapper->Delete();

	if (this->TreeListChar)
		delete this->TreeListChar;
	if (this->TreeListChar2)
		delete this->TreeListChar2;
}


void vtkHMStraightModelWidget::SetEnabled(int enabling)
{
  if ( ! this->Interactor )
    {
    vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
    return;
    }

	if ( enabling ) //-----------------------------------------------------------
    {
    if(!this->StraightModelFilter)
    	{
      if(!this->StraightModelKeepRemoveFilter)
        {
        if( !this->StraightModelSource )
          {
          if ( !this->HeMoLabReader )
            return;
          else if ( !this->HeMoLabReader->GetFileName() )
            return;
          }
        else	if( !this->StraightModelSource->GetFileName() )
          return;
        }
    	}

    vtkDebugMacro(<<"Enabling line widget");

    if ( this->Enabled ) //already enabled, just return
      {
      return;
      }

    if ( ! this->CurrentRenderer )
      {
      this->SetCurrentRenderer(
        this->Interactor->FindPokedRenderer(
          this->Interactor->GetLastEventPosition()[0],
          this->Interactor->GetLastEventPosition()[1]));
      if (this->CurrentRenderer == NULL)
        {
        return;
        }
      }

    this->GridLineWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridNodeWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridTerminalWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridClipWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridStentWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridStenosisWidget->SetCurrentRenderer(this->CurrentRenderer);
    this->GridAneurysmWidget->SetCurrentRenderer(this->CurrentRenderer);

    this->Enabled = 1;

    // listen for the following events
    vtkRenderWindowInteractor *it = this->Interactor;
//    it->AddObserver(vtkCommand::MouseMoveEvent,
//                   this->EventCallbackCommand, this->Priority);
    it->AddObserver(vtkCommand::LeftButtonPressEvent,
                   this->EventCallbackCommand, this->Priority);
    it->AddObserver(vtkCommand::LeftButtonReleaseEvent,
                   this->EventCallbackCommand, this->Priority);
//    it->AddObserver(vtkCommand::MiddleButtonPressEvent,
//                   this->EventCallbackCommand, this->Priority);
//    it->AddObserver(vtkCommand::MiddleButtonReleaseEvent,
//                   this->EventCallbackCommand, this->Priority);
//    it->AddObserver(vtkCommand::RightButtonPressEvent,
//                   this->EventCallbackCommand, this->Priority);
    it->AddObserver(vtkCommand::RightButtonReleaseEvent,
                   this->EventCallbackCommand, this->Priority);

    //Adicionar o grid
    this->CurrentRenderer->AddActor(this->GridLineActor);
    this->GridLineActor->SetProperty(this->GridLineProperty);

    this->CurrentRenderer->AddActor(this->GridClipActor);
    this->GridClipActor->SetProperty(this->GridClipProperty);

    this->CurrentRenderer->AddActor(this->GridStentActor);
    this->GridStentActor->SetProperty(this->GridStentProperty);

    this->CurrentRenderer->AddActor(this->GridStenosisActor);
    this->GridStenosisActor->SetProperty(this->GridStenosisProperty);

    this->CurrentRenderer->AddActor(this->GridAneurysmActor);
    this->GridAneurysmActor->SetProperty(this->GridAneurysmProperty);

    if ( !this->GetSegment_Selected() )
	    {
	    this->CurrentRenderer->AddActor(this->GridNodeActor);
	    this->GridNodeActor->SetProperty(this->GridNodeProperty);
	    this->NodePicker->PickFromListOn();
	    }

    this->CurrentRenderer->AddActor(this->GridTerminalActor);
    this->GridTerminalActor->SetProperty(this->GridTerminalProperty);

    this->CurrentRenderer->AddActor(this->GridNullElements1D3DActor);
    this->GridNullElements1D3DActor->SetProperty(this->GridNullElements1D3DProperty);

    //Adicionar o coracao
    this->CurrentRenderer->AddActor(this->HeartActor);
    this->HeartActor->SetProperty(this->HandleHeartProperty);

    if ( (this->TerminalSelected != -1) || (this->NodeSelected != -1) )
    	{
    	if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
    		{
    		this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
    		this->CurrentRenderer->AddActor(this->GridTerminalSelectedActor);
    		}
    	else
    		this->CurrentRenderer->AddActor(this->SphereActorSelected);
			}

    if ( (this->SegmentSelected != -1) && (this->StraightModelObjectSelected == SEGMENT) )
    	{

    	if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
    		{
    		this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
    		this->CurrentRenderer->AddActor(this->GridTerminalSelectedActor);
    		}
    	else
    		{
	    	this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
	    	if ( this->GetSegment_Selected() )
	    		{

	    		this->CurrentRenderer->AddActor(this->BeginSphereActorSelected);
	    		this->CurrentRenderer->AddActor(this->EndSphereActorSelected);
	    		}
	    	}
    	}
    //Adicionando os atores do modo plot novamente
		for ( int i=0; i<this->NumberOfGraphics; i++ )
			this->CurrentRenderer->AddActor(this->SelectedActors[i]);
		this->CurrentRenderer->AddActor(this->AnimatedSelectedActor);

    //adcionando os atores referentes aos pontos de acoplamento
    for (int i = 0; i < this->CouplingActorCounter; ++i)
			{
			vtkActor *a = NULL;
			// adcionando numeros
			a = vtkActor::SafeDownCast(this->ActorCollection->GetItemAsObject(i));
			if (a)
				this->CurrentRenderer->AddActor(a);
//			else
//				vtkErrorMacro("Can not find the selected Number Actor in the List");

			a = NULL;

			// adcionando esferas
			a = vtkActor::SafeDownCast(this->SphereActorCollection->GetItemAsObject(i));

			if (a)
				this->CurrentRenderer->AddActor(a);
//			else
//				vtkErrorMacro("Can not find the selected Sphere Actor in the List");
			}

    this->InvokeEvent(vtkCommand::EnableEvent,NULL);
    }

  else //disabling----------------------------------------------------------
    {
    vtkDebugMacro(<<"Disabling line widget");

    if ( ! this->Enabled ) //already disabled, just return
      {
      return;
      }

    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

		//Remover o grid
		this->CurrentRenderer->RemoveActor(this->GridLineActor);
		this->CurrentRenderer->RemoveActor(this->GridNodeActor);
		this->CurrentRenderer->RemoveActor(this->GridTerminalActor);
		this->CurrentRenderer->RemoveActor(this->HeartActor);
		this->CurrentRenderer->RemoveActor(this->SphereActorSelected);
		this->CurrentRenderer->RemoveActor(this->GridLineSelectedActor);
		this->CurrentRenderer->RemoveActor(this->GridTerminalSelectedActor);

		this->CurrentRenderer->RemoveActor(this->BeginSphereActorSelected);
		this->CurrentRenderer->RemoveActor(this->EndSphereActorSelected);
		this->CurrentRenderer->RemoveActor(this->GridClipActor);
		this->CurrentRenderer->RemoveActor(this->GridStentActor);
		this->CurrentRenderer->RemoveActor(this->GridStenosisActor);
		this->CurrentRenderer->RemoveActor(this->GridAneurysmActor);
		this->CurrentRenderer->RemoveActor(this->GridNullElements1D3DActor);

		this->HighlightSelectedActors(0);

    if (this->CurrentPointWidget)
      {
      this->CurrentPointWidget->EnabledOff();
      }

    //removendo os atores referentes aos pontos de acoplamento
    for (int i = 0; i < this->ActorCollection->GetNumberOfItems(); ++i)
			{
			vtkActor *a = NULL;
			// removendo numeros
			a = vtkActor::SafeDownCast(this->ActorCollection->GetItemAsObject(i));
			if (a)
				this->CurrentRenderer->RemoveActor(a);
//			else
//				vtkErrorMacro("Can not find the selected Number Actor in the List");

			a = NULL;

			// removendo esferas
			a = vtkActor::SafeDownCast(this->SphereActorCollection->GetItemAsObject(i));

			if (a)
				this->CurrentRenderer->RemoveActor(a);
//			else
//				vtkErrorMacro("Can not find the selected Sphere Actor in the List");
			}




    this->CurrentHandle = NULL;
    this->InvokeEvent(vtkCommand::DisableEvent,NULL);
    this->SetCurrentRenderer(NULL);
    }

  this->Interactor->Render();
}

void vtkHMStraightModelWidget::ProcessEvents(vtkObject* vtkNotUsed(object),
                                  unsigned long event,
                                  void* clientdata,
                                  void* vtkNotUsed(calldata))
{
  vtkHMStraightModelWidget* self = reinterpret_cast<vtkHMStraightModelWidget *>( clientdata );

  //okay, let's do the right thing
  switch(event)
    {
    case vtkCommand::LeftButtonPressEvent:
      self->OnLeftButtonDown();
      break;

    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonUp();
      break;
/*    case vtkCommand::MiddleButtonPressEvent:
      self->OnMiddleButtonDown();
      break;
    case vtkCommand::MiddleButtonReleaseEvent:
      self->OnMiddleButtonUp();
      break;
*/
    case vtkCommand::RightButtonPressEvent:
      self->OnRightButtonDown();
      break;

    case vtkCommand::RightButtonReleaseEvent:
      self->OnRightButtonUp();
      break;

    case vtkCommand::MouseMoveEvent:
      self->OnMouseMove();
      break;


    }
}

void vtkHMStraightModelWidget::SizeHandles()
{
	this->RadiusTerminal = this->vtk3DWidget::SizeHandles(1.0);
	this->RadiusNode			= 2 * (this->RadiusTerminal/3);
	double radiusTube		  = 3 * (this->RadiusNode/4);

	this->HeartGeometry->SetRadius(this->RadiusNode);
	this->SphereSourceSelected->SetRadius(this->RadiusNode);

	for (int i=0; i<this->GetNumberOfGraphics(); i++)
		if(vtkSphereSource::SafeDownCast(this->PlotSource[i]))
			vtkSphereSource::SafeDownCast(this->PlotSource[i])->SetRadius(this->RadiusNode);

	this->BeginSphereSourceSelected->SetRadius(this->RadiusNode);
	this->EndSphereSourceSelected->SetRadius(this->RadiusNode);


	// ajusta o tamanho das esferas de representacao dos pontos de acoplamento
	for (int i = 0; i < this->ActorCollection->GetNumberOfItems(); ++i)
		{
		vtkSphereSource *s = vtkSphereSource::SafeDownCast(this->CouplingSphereSourceCollection->GetItemAsObject(i));
		s->SetRadius(this->RadiusNode);
		}

}

//-------------------------------------------------------------------------
//Colore os atores selecionados, caso a opcao de plot esteja selecionada
void vtkHMStraightModelWidget::HighlightSelectedActors(int op)
{
this->RenderSegmentsWithDefaultColors();
//	se op != 0 pinta o ator com a cor especcifica da curva plotada
//	se op == 0 pinta o ator com a cor padrao
	if ( op )//colorir todo mundo
		{
	//pinta todo mundo
		if (!PlotMode)//Se estiver no modo NormalPlot
			for ( int i=0; i<this->GetNumberOfGraphics(); i++ )
				this->SelectedActors[i]->SetProperty(this->SelectedActorsProperty[i]);
		else // se estiver no modo AnimatedPlot
			this->AnimatedSelectedActor->SetProperty(this->AnimatedSelectedActorProperty);

		//adiciona todos os atores existentes
		for ( int i=0; i<this->GetNumberOfGraphics(); i++ )
			if (this->SelectedActors[i])
				this->CurrentRenderer->AddActor(this->SelectedActors[i]);
		if (this->AnimatedSelectedActor)
			this->CurrentRenderer->AddActor(this->AnimatedSelectedActor);

		}
	else//descolorir todo mundo
			{
				for ( int i=0; i<this->GetNumberOfGraphics(); i++ )
					if (this->SelectedActors[i])
						this->CurrentRenderer->RemoveActor(this->SelectedActors[i]);

				if (this->AnimatedSelectedActor)
					this->CurrentRenderer->RemoveActor(this->AnimatedSelectedActor);
			}
}

int vtkHMStraightModelWidget::ForwardEvent(unsigned long event)
{
  if ( !this->CurrentPointWidget )
    {
    return 0;
    }

  this->CurrentPointWidget->ProcessEvents(this,event,this->CurrentPointWidget,NULL);

  return 1;
}

// assumed current handle is set
void vtkHMStraightModelWidget::EnablePointWidget()
{
	// Set up the point widgets
  double x[3];

  if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))
  	{
  	this->CurrentPointWidget = this->GridTerminalWidget;

    this->TerminalPicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];

  	}
  else if ( this->StraightModelObjectSelected == NODE )
	  {
	  this->CurrentPointWidget = this->GridNodeWidget;

    this->NodePicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
	  }
  else if ( this->StraightModelObjectSelected == SEGMENT )
	  {
	  this->CurrentPointWidget = this->GridLineWidget;
    this->LinePicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
	  }
  else if ( this->StraightModelObjectSelected == CLIP )
  	{
  	this->CurrentPointWidget = this->GridClipWidget;
    this->ClipPicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
  	}
  else if ( this->StraightModelObjectSelected == STENT )
  	{
  	this->CurrentPointWidget = this->GridStentWidget;
    this->StentPicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
  	}
  else if ( this->StraightModelObjectSelected == STENOSIS )
  	{
  	this->CurrentPointWidget = this->GridStenosisWidget;
    this->StenosisPicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
  	}
  else if ( this->StraightModelObjectSelected == ANEURYSM )
  	{
  	this->CurrentPointWidget = this->GridAneurysmWidget;
    this->AneurysmPicker->GetPickPosition(x);

    this->LastPosition[0] = x[0];
    this->LastPosition[1] = x[1];
    this->LastPosition[2] = x[2];
  	}

  double bounds[6];
  for (int i=0; i<3; i++)
    {
    bounds[2*i] = x[i] - 0.1*this->InitialLength;
    bounds[2*i+1] = x[i] + 0.1*this->InitialLength;
    }

  // Note: translation mode is disabled and enabled to control
  // the proper positioning of the bounding box.
  this->CurrentPointWidget->SetInteractor(this->Interactor);
  this->CurrentPointWidget->TranslationModeOff();
  this->CurrentPointWidget->SetPlaceFactor(1.0);
  this->CurrentPointWidget->PlaceWidget(bounds);
  this->CurrentPointWidget->TranslationModeOn();
  this->CurrentPointWidget->SetPosition(x);
  this->CurrentPointWidget->SetCurrentRenderer(this->CurrentRenderer);
  this->CurrentPointWidget->On();
}

// assumed current handle is set
void vtkHMStraightModelWidget::DisablePointWidget()
{
  if (this->CurrentPointWidget)
    {
    this->CurrentPointWidget->Off();
    }
  this->CurrentPointWidget = NULL;
}

void vtkHMStraightModelWidget::HighlightHandles(int highlight)
{
  if ( highlight )
    {
    this->ValidPick = 1;

    vtkIdType *info;
    double *p;
    if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))
	    info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
	  else if ( this->StraightModelObjectSelected == NODE )
	  	info = this->GridLine->GetNodeInformation(this->SegmentSelected, this->NodeSelected);

    if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
    	{
    	this->GridTerminalSelectedMapper->SetInput(this->GridTerminalSelected);
  	 	//Ator
  	 	this->GridTerminalSelectedActor->SetMapper(this->GridTerminalSelectedMapper);

  		this->CurrentRenderer->AddActor(this->GridTerminalSelectedActor);
  		this->GridTerminalSelectedActor->SetProperty(this->SubTreeHandleProperty);

  		this->Interactor->Render();
    	}
    else  if ( this->GetStraightModelMode() == MODE_SET_PATH_VAL && this->StraightModelObjectSelected == TERMINAL )
      {
      p = this->GridLine->GetPoints()->GetPoint(info[0]);
      this->GridTerminalSelectedMapper->SetInput(this->GridTerminalSelected);
      //Ator
      this->GridTerminalSelectedActor->SetMapper(this->GridTerminalSelectedMapper);

      this->CurrentRenderer->AddActor(this->GridTerminalSelectedActor);
      this->GridTerminalSelectedActor->SetProperty(this->SubTreeHandleProperty);

      this->Interactor->Render();
      }



    else
    	{
    	p = this->GridLine->GetPoints()->GetPoint(info[0]);

	    this->SphereSourceSelected->SetCenter(p);
	    this->CurrentRenderer->AddActor(this->SphereActorSelected);
	    this->SphereActorSelected->SetProperty(this->SelectedHandleProperty);
	    this->SizeHandles();
    	}

    // se TextCollection tem algum item, entao o 1D Widget esta sendo utilizado em um modelo acoplado
    if (this->TextCollection->GetNumberOfItems() &&  (this->StraightModelObjectSelected == NODE || this->GetStraightModelMode() == MODE_SET_PATH_VAL) )
    	{
	    if (CouplingActorCounter >= this->TextCollection->GetNumberOfItems())
	    	return;

	    vtkActor *a = vtkActor::SafeDownCast(this->ActorCollection->GetItemAsObject(CouplingActorCounter));
	    a->GetProperty()->SetColor(vtkMath::HSVToRGB(double(this->FullModelCode)/5.0, 0.3, 0.75));
			a->SetPosition(p);

			vtkSphereSource *s = vtkSphereSource::SafeDownCast(this->CouplingSphereSourceCollection->GetItemAsObject(CouplingActorCounter));
			s->SetRadius(this->RadiusNode);

	    vtkActor *sa = vtkActor::SafeDownCast(this->SphereActorCollection->GetItemAsObject(CouplingActorCounter));
	    sa->GetProperty()->SetColor(vtkMath::HSVToRGB(double(this->FullModelCode)/5.0, 0.3, 0.75));
			sa->SetPosition(p);

			// cada clique vai adcionar um ator
			if (!this->CheckIfActorIsOnTheRender(a) && this->GetStraightModelMode() != MODE_SET_PATH_VAL)
				this->CurrentRenderer->AddActor(a);

			if (!this->CheckIfActorIsOnTheRender(sa))
				this->CurrentRenderer->AddActor(sa);
    	}
    }
  else

  	{
  	if (!this->Interactor->GetControlKey())
	  	{
			this->CurrentRenderer->RemoveActor(this->GridTerminalSelectedActor);
			vtkPoints *point = vtkPoints::New();
			vtkCellArray *verts = vtkCellArray::New();

			//Setar os pontos vazios
			this->GridTerminalSelected->SetPoints(point);
			this->GridTerminalSelected->SetVerts(verts);
			this->GridTerminalSelected->RemoveAllInformation();

			verts->Delete();
			point->Delete();
	  	}
  	if (this->Interactor->GetControlKey()&&(this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL))
	  	{
	  	this->CurrentRenderer->RemoveActor(this->GridTerminalSelectedActor);
			vtkPoints *point = vtkPoints::New();
			vtkCellArray *verts = vtkCellArray::New();

			//Setar os pontos vazios
			this->GridTerminalSelected->SetPoints(point);
			this->GridTerminalSelected->SetVerts(verts);
			this->GridTerminalSelected->RemoveAllInformation();

			verts->Delete();
			point->Delete();
	  	}
  	this->CurrentRenderer->RemoveActor(this->SphereActorSelected);
  	}

}

void vtkHMStraightModelWidget::HighlightLine(int highlight)
{
  if ( highlight )
  	{
    this->ValidPick = 1;
    this->LinePicker->GetPickPosition(this->LastPickPosition);
    if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
    	{
    	this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
  	 	//Ator
  	 	this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

  		this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
  		this->GridLineSelectedActor->SetProperty(this->MainStraightModelProperty);

  		this->Interactor->Render();
    	}


    else if ( this->GetStraightModelMode() == MODE_SET_PATH_VAL)
      {
      this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
      //Ator
      this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

      this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
      this->GridLineSelectedActor->SetProperty(this->MainStraightModelProperty);

      this->Interactor->Render();
      }


    else if ( this->EditionModeSelected == vtkHMStraightModelWidget::CheckConsistence)
    	{
    	this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
  	 	//Ator
  	 	this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

  		this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
  		this->GridLineSelectedActor->SetProperty(this->MainStraightModelProperty);

//  		this->EditionModeSelected = vtkHMStraightModelWidget::EditGeometry;
  		this->Interactor->Render();
    	}
    //Se modo de visualizacao de segmento, colorir todo o segmento
    else if ( this->GetSegment_Selected() )
	    {
	    if ( this->GridLineSelected->GetNodeInformation(this->SegmentSelected, 0) )
				return;

	    double p1[3];
	    vtkIdType *info, idNode[3], idCellNode;

			int numberOfPoints = this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeNumber();
			idNode[2] = this->SegmentSelected;

			vtkIdType idPts[2];
			vtkCellArray *line = this->GridLineSelected->GetLines();
			vtkPoints *point = this->GridLineSelected->GetPoints();
			vtkCellArray *nodes	= this->GridLineSelected->GetNodes();

			if ( (!point) || (!line) || (!nodes) )
				{
				line = vtkCellArray::New();
				point = vtkPoints::New();
				nodes	= vtkCellArray::New();

				this->GridLineSelected->SetPoints(point);
				this->GridLineSelected->SetLines(line);
				this->GridLineSelected->SetNodes(nodes);

				point->Delete();
   			line->Delete();
				nodes->Delete();

				line = this->GridLineSelected->GetLines();
				point = this->GridLineSelected->GetPoints();
				nodes	= this->GridLineSelected->GetNodes();
				}

			//Pego do grid informacoes do primeiro node, primeiro ponto
			info = this->GridLine->GetNodeInformation(this->SegmentSelected, 0);
			//Pego do grid o ponto relacionado com o node
			this->GridLine->GetPoints()->GetPoint(info[0], p1);

			idNode[0] = idPts[0] = point->InsertNextPoint(p1);
			idNode[1] = 0;

			idCellNode = nodes->InsertNextCell(3, idNode);
			vtkIdList *nodeList = vtkIdList::New();
			nodeList->InsertNextId(idCellNode);

			for ( int i=1; i<numberOfPoints; i++ )
				{
				//Pego do grid informacoes do primeiro node, primeiro ponto
				info = this->GridLine->GetNodeInformation(this->SegmentSelected, i);

				//Pego do grid o ponto relacionado com o node
				this->GridLine->GetPoints()->GetPoint(info[0], p1);

				idNode[0] = idPts[1] = point->InsertNextPoint(p1);
				idNode[1] = i;

				idCellNode = nodes->InsertNextCell(3, idNode);

				nodeList->InsertNextId(idCellNode);

				line->InsertNextCell(2, idPts);
				idPts[0] = idPts[1];
				}

			//Remove do map as informacoes das celulas associadas ao segmento.
			this->GridLineSelected->RemoveNodeInformation(this->SegmentSelected);
			//Inserir no map informacoes das celulas associadas ao segmento para que
			//facilite e agilize a busca pelo elemento.
			this->GridLineSelected->InsertNodeInformation(this->SegmentSelected,nodeList);

			this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
		 	//Ator
		 	this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

			this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
   		this->GridLineSelectedActor->SetProperty(this->SelectedLineProperty);

	    //Pego do grid informacoes do primeiro node, primeiro ponto
			info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(this->SegmentSelected)->GetFirstParent()->GetId());

			this->GridTerminal->GetPoints()->GetPoint(info[0], p1);
			this->BeginSphereSourceSelected->SetCenter(p1);

			double p2[3];
			info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(this->SegmentSelected)->GetFirstChild()->GetId());
			//Pego do grid o ponto relacionado com o node
			this->GridTerminal->GetPoints()->GetPoint(info[0], p2);

	    this->EndSphereSourceSelected->SetCenter(p2);
	    this->BeginSphereMapperSelected->SetInput(this->BeginSphereSourceSelected->GetOutput());
      //Actor
      this->BeginSphereActorSelected->SetMapper(this->BeginSphereMapperSelected);
    	this->BeginSphereActorSelected->GetProperty()->SetColor(0,0,1);

    	this->EndSphereMapperSelected->SetInput(this->EndSphereSourceSelected->GetOutput());
      //Actor
      this->EndSphereActorSelected->SetMapper(this->EndSphereMapperSelected);
    	this->EndSphereActorSelected->GetProperty()->SetColor(0,1,0);

	    this->CurrentRenderer->AddActor(this->BeginSphereActorSelected);
	    this->CurrentRenderer->AddActor(this->EndSphereActorSelected);
	    this->SizeHandles();
	    } // End if ( this->GetSegment_Selected() )

	  //Se modo de visualizacao de elemento, colorir somente o elemento clicado
    else
	    {
	    double *p1, *p2;

			vtkIdType *info, idNode[3], idCellNode;

			vtkIdType idPts[2];
			vtkCellArray *line = vtkCellArray::New();
			vtkPoints *point = vtkPoints::New();
//			vtkCellArray *nodes = vtkCellArray::New();
//			vtkIdList *nodeList = vtkIdList::New();
//			if ( this->StraightModelObjectSelected == STENT )
//				{
//				vtkIdList *list = this->GridStent->GetCell(this->StraightModelElementSelected)->GetPointIds();
//
//				for ( int i=0; i<list->GetNumberOfIds(); i++ )
//					{
//					p1 = this->GridStent->GetPoints()->GetPoint(i);
//
//					idPts[1] = point->InsertNextPoint(p1);
//					idNode[1] = i;
//
//					idCellNode = nodes->InsertNextCell(3, idNode);
//
//					nodeList->InsertNextId(idCellNode);
//
//					line->InsertNextCell(2, idPts);
//					idPts[0] = idPts[1];
//					}
//				this->GridLineSelected->SetPoints(point);
//				this->GridLineSelected->SetLines(line);
//				this->GridLineSelected->SetNodes(nodes);
//				//Remove do map as informacoes das celulas associadas ao segmento.
//				this->GridLineSelected->RemoveNodeInformation(this->SegmentSelected);
//				//Inserir no map informacoes das celulas associadas ao segmento para que
//				//facilite e agilize a busca pelo elemento.
//				this->GridLineSelected->InsertNodeInformation(this->SegmentSelected,nodeList);
//				}
//			else
//				{
			//Pego do grid informacoes do elemento clicado
			info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

			//Pego do grid o ponto 1 associado com o elemento
			p1 = this->GridLine->GetPoint(info[0]);
			idPts[0] = point->InsertNextPoint(p1);

			//Pego do grid o ponto 2 associado com o elemento
			p2 = this->GridLine->GetPoint(info[1]);
			idPts[1] = point->InsertNextPoint(p2);

			line->InsertNextCell(2, idPts);

			this->GridLineSelected->SetPoints(point);
			this->GridLineSelected->SetLines(line);
//				}
			this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
		 	//Ator
		 	this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

			this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
   		this->GridLineSelectedActor->SetProperty(this->SelectedLineProperty);

   		point->Delete();
   		line->Delete();
	    }
  	} // End if ( highlight )

  //Remover o ator da renderer
  else
  	{
  	if (!this->Interactor->GetControlKey())
	  	{
			this->CurrentRenderer->RemoveActor(this->GridLineSelectedActor);
			vtkPoints *point = vtkPoints::New();
			vtkCellArray *line = vtkCellArray::New();
			vtkCellArray *nodes = vtkCellArray::New();

			//Setar os pontos e linhas vazios
			this->GridLineSelected->SetPoints(point);
			this->GridLineSelected->SetLines(line);
			this->GridLineSelected->SetNodes(nodes);
			this->GridLineSelected->RemoveAllInformation();

			point->Delete();
	 		line->Delete();
	 		nodes->Delete();
	  	}
  	if (this->Interactor->GetControlKey()&&(this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL))
	  	{
	  	this->CurrentRenderer->RemoveActor(this->GridLineSelectedActor);
			vtkPoints *point = vtkPoints::New();
			vtkCellArray *line = vtkCellArray::New();
			vtkCellArray *nodes = vtkCellArray::New();

			//Setar os pontos e linhas vazios
			this->GridLineSelected->SetPoints(point);
			this->GridLineSelected->SetLines(line);
			this->GridLineSelected->SetNodes(nodes);
			this->GridLineSelected->RemoveAllInformation();

			point->Delete();
	 		line->Delete();
	 		nodes->Delete();
	  	}
  	this->CurrentRenderer->RemoveActor(this->BeginSphereActorSelected);
  	this->CurrentRenderer->RemoveActor(this->EndSphereActorSelected);
  	}

}


//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::HighlightSegment(vtkIdType idSegment)
{
	//limpa qualque outro objeto selecionado.
	this->HighlightLine(0);

  if ( this->GridLineSelected->GetNodeInformation(idSegment, 0) )
		return;

	this->StraightModelElementSelected 	= idSegment;
	//  this->LineSelectedId 								= l;
	this->SegmentSelected								= idSegment;
	this->TerminalSelected							= -1;
	this->NodeSelected									= -1;
	this->ElementSelected								= 0;
	this->LineWidgetSelected 						= this->LineSelectedId;
	this->SphereWidgetSelected 					= -1;
	this->StraightModelObjectSelected 	= SEGMENT;

  double p1[3];
  vtkIdType *info, idNode[3], idCellNode;

	int numberOfPoints = this->StraightModel->GetSegment(idSegment)->GetNodeNumber();
	idNode[2] = idSegment;

	vtkIdType idPts[2];
	vtkCellArray *line = this->GridLineSelected->GetLines();
	vtkPoints *point = this->GridLineSelected->GetPoints();
	vtkCellArray *nodes	= this->GridLineSelected->GetNodes();

	if ( (!point) || (!line) || (!nodes) )
		{
		line = vtkCellArray::New();
		point = vtkPoints::New();
		nodes	= vtkCellArray::New();

		this->GridLineSelected->SetPoints(point);
		this->GridLineSelected->SetLines(line);
		this->GridLineSelected->SetNodes(nodes);

		point->Delete();
		line->Delete();
		nodes->Delete();

		line = this->GridLineSelected->GetLines();
		point = this->GridLineSelected->GetPoints();
		nodes	= this->GridLineSelected->GetNodes();
		}

	//Pego do grid informacoes do primeiro node, primeiro ponto
	info = this->GridLine->GetNodeInformation(idSegment, 0);
	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], p1);

	idNode[0] = idPts[0] = point->InsertNextPoint(p1);
	idNode[1] = 0;

	idCellNode = nodes->InsertNextCell(3, idNode);
	vtkIdList *nodeList = vtkIdList::New();
	nodeList->InsertNextId(idCellNode);

	for ( int i=1; i<numberOfPoints; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetNodeInformation(idSegment, i);

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[0], p1);

		idNode[0] = idPts[1] = point->InsertNextPoint(p1);
		idNode[1] = i;

		idCellNode = nodes->InsertNextCell(3, idNode);

		nodeList->InsertNextId(idCellNode);

		line->InsertNextCell(2, idPts);
		idPts[0] = idPts[1];
		}

	//Remove do map as informacoes das celulas associadas ao segmento.
	this->GridLineSelected->RemoveNodeInformation(idSegment);
	//Inserir no map informacoes das celulas associadas ao segmento para que
	//facilite e agilize a busca pelo elemento.
	this->GridLineSelected->InsertNodeInformation(idSegment,nodeList);

	this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
 	//Ator
 	this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

	this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
	this->GridLineSelectedActor->SetProperty(this->SelectedLineProperty);

//  //Pego do grid informacoes do primeiro node, primeiro ponto
//	info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(idSegment)->GetFirstParent()->GetId());
//
//	this->GridTerminal->GetPoints()->GetPoint(info[0], p1);
//	this->BeginSphereSourceSelected->SetCenter(p1);
//
//	double p2[3];
//	info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(idSegment)->GetFirstChild()->GetId());
//	//Pego do grid o ponto relacionado com o node
//	this->GridTerminal->GetPoints()->GetPoint(info[0], p2);
//
//  this->EndSphereSourceSelected->SetCenter(p2);
//  this->BeginSphereMapperSelected->SetInput(this->BeginSphereSourceSelected->GetOutput());
//  //Actor
//  this->BeginSphereActorSelected->SetMapper(this->BeginSphereMapperSelected);
//	this->BeginSphereActorSelected->GetProperty()->SetColor(0,0,1);
//
//	this->EndSphereMapperSelected->SetInput(this->EndSphereSourceSelected->GetOutput());
//  //Actor
//  this->EndSphereActorSelected->SetMapper(this->EndSphereMapperSelected);
//	this->EndSphereActorSelected->GetProperty()->SetColor(0,1,0);
//
//  this->CurrentRenderer->AddActor(this->BeginSphereActorSelected);
//  this->CurrentRenderer->AddActor(this->EndSphereActorSelected);
//  this->SizeHandles();

	this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::HighlightSegment(char *Name)
{
	//limpa qualque outro objeto selecionado.
	this->HighlightLine(0);

	vtkIdType idSegment = this->StraightModel->GetSegment(Name)->GetId();

	if ( this->GridLineSelected->GetNodeInformation(idSegment, 0) )
		return;

	this->StraightModelElementSelected 	= idSegment;
//  this->LineSelectedId 								= l;
	this->SegmentSelected								= idSegment;
	this->TerminalSelected							= -1;
	this->NodeSelected									= -1;
	this->ElementSelected								= 0;
//	this->LineWidgetSelected 						= this->LineSelectedId;
	this->SphereWidgetSelected 					= -1;
	this->StraightModelObjectSelected 	= SEGMENT;

	double p1[3];
	vtkIdType *info, idNode[3], idCellNode;

	int numberOfPoints = this->StraightModel->GetSegment(idSegment)->GetNodeNumber();
	idNode[2] = idSegment;

	vtkIdType idPts[2];
	vtkCellArray *line = this->GridLineSelected->GetLines();
	vtkPoints *point = this->GridLineSelected->GetPoints();
	vtkCellArray *nodes	= this->GridLineSelected->GetNodes();

	if ( (!point) || (!line) || (!nodes) )
		{
		line = vtkCellArray::New();
		point = vtkPoints::New();
		nodes	= vtkCellArray::New();

		this->GridLineSelected->SetPoints(point);
		this->GridLineSelected->SetLines(line);
		this->GridLineSelected->SetNodes(nodes);

		point->Delete();
		line->Delete();
		nodes->Delete();

		line = this->GridLineSelected->GetLines();
		point = this->GridLineSelected->GetPoints();
		nodes	= this->GridLineSelected->GetNodes();
		}

	//Pego do grid informacoes do primeiro node, primeiro ponto
	info = this->GridLine->GetNodeInformation(idSegment, 0);
	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], p1);

	idNode[0] = idPts[0] = point->InsertNextPoint(p1);
	idNode[1] = 0;

	idCellNode = nodes->InsertNextCell(3, idNode);
	vtkIdList *nodeList = vtkIdList::New();
	nodeList->InsertNextId(idCellNode);

	for ( int i=1; i<numberOfPoints; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetNodeInformation(idSegment, i);

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[0], p1);

		idNode[0] = idPts[1] = point->InsertNextPoint(p1);
		idNode[1] = i;

		idCellNode = nodes->InsertNextCell(3, idNode);

		nodeList->InsertNextId(idCellNode);

		line->InsertNextCell(2, idPts);
		idPts[0] = idPts[1];
		}

	//Remove do map as informacoes das celulas associadas ao segmento.
	this->GridLineSelected->RemoveNodeInformation(idSegment);
	//Inserir no map informacoes das celulas associadas ao segmento para que
	//facilite e agilize a busca pelo elemento.
	this->GridLineSelected->InsertNodeInformation(idSegment,nodeList);

	this->GridLineSelectedMapper->SetInput(this->GridLineSelected);
		//Ator
		this->GridLineSelectedActor->SetMapper(this->GridLineSelectedMapper);

	this->CurrentRenderer->AddActor(this->GridLineSelectedActor);
	this->GridLineSelectedActor->SetProperty(this->SelectedLineProperty);

	//  //Pego do grid informacoes do primeiro node, primeiro ponto
	//	info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(idSegment)->GetFirstParent()->GetId());
	//
	//	this->GridTerminal->GetPoints()->GetPoint(info[0], p1);
	//	this->BeginSphereSourceSelected->SetCenter(p1);
	//
	//	double p2[3];
	//	info = this->GridTerminal->GetTerminalInformation(this->StraightModel->GetSegment(idSegment)->GetFirstChild()->GetId());
	//	//Pego do grid o ponto relacionado com o node
	//	this->GridTerminal->GetPoints()->GetPoint(info[0], p2);
	//
	//  this->EndSphereSourceSelected->SetCenter(p2);
	//  this->BeginSphereMapperSelected->SetInput(this->BeginSphereSourceSelected->GetOutput());
	//  //Actor
	//  this->BeginSphereActorSelected->SetMapper(this->BeginSphereMapperSelected);
	//	this->BeginSphereActorSelected->GetProperty()->SetColor(0,0,1);
	//
	//	this->EndSphereMapperSelected->SetInput(this->EndSphereSourceSelected->GetOutput());
	//  //Actor
	//  this->EndSphereActorSelected->SetMapper(this->EndSphereMapperSelected);
	//	this->EndSphereActorSelected->GetProperty()->SetColor(0,1,0);
	//
	//  this->CurrentRenderer->AddActor(this->BeginSphereActorSelected);
	//  this->CurrentRenderer->AddActor(this->EndSphereActorSelected);
	//  this->SizeHandles();

	this->Interactor->Render();

}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnLeftButtonDown()
{
  int forward=0;

  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Desmarcando os elementos antes selecionados
  if((this->GetStraightModelMode() != MODE_PLOT_VAL) && (!this->Interactor->GetControlKey())&&(this->GetStraightModelMode() != MODE_TODETACHSUBTREE_VAL)
  		&& (this->EditionModeSelected != vtkHMStraightModelWidget::CheckConsistence))
		{
		this->RenderSegmentsWithDefaultColors();
		}

	 	// Okay, make sure that the pick is in the current renderer
  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
    {
    this->State = vtkHMStraightModelWidget::Outside;
    return;
    }

  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then try to pick the line.
  vtkAssemblyPath *path;

  this->ClipPicker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->ClipPicker->GetPath();

  if ( path != NULL )
  	{
  	vtkIdType cellId = this->ClipPicker->GetCellId();
		cout << "\nClip cell: " << cellId << endl;
		double *tuple = this->ClipSegments->GetTuple(cellId);

		// Acertou um segmento
		this->StraightModelElementSelected 	= cellId;
    this->LineSelectedId 								= -1;
		this->SegmentSelected								= (int)tuple[0];
		this->TerminalSelected							= -1;
		this->NodeSelected									= -1;
		this->ElementSelected								= -1;
		this->LineWidgetSelected 						= this->LineSelectedId;
		this->SphereWidgetSelected 					= -1;
 		this->StraightModelObjectSelected 	= CLIP;

		if(this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
			{
				this->EnablePointWidget();
				forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
		  	this->State = vtkHMStraightModelWidget::MovingHandle;
			}  // End if(this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)

		if(this->GetStraightModelMode() == MODE_EDITION_VAL)
			{
			if (this->MoveElement == true)
				{
				this->EnablePointWidget();
		  	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
		  	this->State = vtkHMStraightModelWidget::MovingHandle;
				}
			}  // End if(this->GetStraightModelMode() == MODE_EDITION_VAL)

	  this->EventCallbackCommand->SetAbortFlag(1);
	  this->StartInteraction();
	  this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);

  	} //Fim if ( path != NULL ) -> clip

	else
	  {
	  this->TerminalPicker->Pick(X,Y,0.0,this->CurrentRenderer);
	  path = this->TerminalPicker->GetPath();

	  if ( path != NULL )
	  	{
	  	this->HighlightSelectedActors(0);
	   	// Acertou um terminal ou node

			vtkIdType cellId = this->TerminalPicker->GetCellId();
			cout << "\nTerminal cell: " << cellId << endl;
			vtkIdType *pt;

			pt = this->GridLine->GetTerminal(cellId);
			cout << "Point: " << pt[0] << "\tTerminal: " << pt[1] << "\tSegm: " << pt[2] << endl;

			this->StraightModelElementSelected 	= pt[1];
	    this->PointSelectedId 							= pt[0];
			this->SegmentSelected								= -1;
			this->TerminalSelected							= pt[1];
			this->NodeSelected									= -1;
			this->ElementSelected								= -1;
			this->LineWidgetSelected 						= -1;
			this->SphereWidgetSelected 					= this->PointSelectedId;

			if ( this->TerminalSelected == 1 )
				this->StraightModelObjectSelected = HEART;
			else
				this->StraightModelObjectSelected = TERMINAL;

			if(this->GetStraightModelMode() != MODE_PLOT_VAL)
	    	this->HighlightHandles(1);

			//Se clicar em um terminal colorir todos os filhos dele inclusive ele.
			//Modo ToDetachSubTree
			if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
				{
		    	this->EnablePointWidget();
		    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
		    	this->State = vtkHMStraightModelWidget::MovingHandle;
				if(!this->Interactor->GetShiftKey())
					{
					this->RenderSegmentsWithDefaultColors();
					this->ToDetachSubTree();
					}
				else
					{
					this->RenderSegmentsWithDefaultColors();
					this->ToDetachSubTreeErase();
					}
				}
			else if ( this->GetStraightModelMode() == MODE_SET_PATH_VAL)
        {
          forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
          this->RenderSegmentsWithDefaultColors();
          this->VisualizePath();
        }

			// Se habilitado modo de edicao da arvore
			else if ( (this->GetStraightModelMode() == MODE_EDITION_VAL) && this->MoveElement)
				{
				//Se click for em um terminal e modo de adicao de segmento estiver selecionado
				if ( ( this->StraightModelObjectSelected == TERMINAL ) && ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment ) )
	  			{
					this->HighlightHandles(0);
	  			double x[3];
					this->GridLine->GetPoint(pt[0], x);
	  			vtkHM1DTerminal *term = this->StraightModel->GetTerminal(this->TerminalSelected);
	  			if ( term->IsLeaf() )
	  				{
	  				//Setar valores default de terminal não folha
  					vtkHM1DTerminal::ResistanceValuesList R;
  					R.push_back(100000000);
  					term->SetR1(&R);
  					term->SetR2(&R);
  					R.clear();
  					R.push_back(0);
  					term->SetC(&R);
  					term->SetPt(&R);
  					R.clear();
	  				}
	  			//Se terminal possui pai, adicionar segmento como filho do terminal clicado
	  			this->AddLineWidget(x, 2, this->StraightModelElementSelected);

					vtkIdType *pt;
					pt = this->GridLine->GetTerminal(this->NumberOfTerminals-1);

	  			this->StraightModelElementSelected 	= pt[1];
			    this->PointSelectedId 							= pt[0];
					this->SegmentSelected								= pt[2];
					this->TerminalSelected							= pt[1];
					this->NodeSelected									= -1;
					this->ElementSelected								= -1;
					this->LineWidgetSelected 						= -1;
					this->SphereWidgetSelected 					= this->PointSelectedId;
	  			this->StraightModelObjectSelected = TERMINAL;
	  			}  // End if ( ( this->StraightModelObjectSelected == TERMINAL ) && ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment ) )

//				if (this->MoveElement == true)
//					{
		    	this->EnablePointWidget();
		    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
		    	this->State = vtkHMStraightModelWidget::MovingHandle;
//					}
				}  // End if(this->GetStraightModelMode() == MODE_EDITION_VAL)

			// Se habilitado modo de edicao da arvore
			else if(this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL)
				{
				//Se click for em um terminal e modo de adicao de segmento estiver selecionado
//				if ( ( this->StraightModelObjectSelected == TERMINAL )
//				    && ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment )
//				    && this->MoveElement )
//	  			{
//					this->HighlightHandles(0);
//	  			double x[3];
//					this->GridLine->GetPoint(pt[0], x);
//
//	  			//Se terminal possui pai, adicionar segmento como filho do segmento pai do terminal
//	  			this->AddLineWidget(x, 2, this->StraightModelElementSelected);
//					vtkIdType *pt;
//					pt = this->GridLine->GetTerminal(this->NumberOfTerminals-1);
//
//	  			this->StraightModelElementSelected 	= pt[1];
//			    this->PointSelectedId 							= pt[0];
//					this->SegmentSelected								= pt[2];
//					this->TerminalSelected							= pt[1];
//					this->NodeSelected									= -1;
//					this->ElementSelected								= -1;
//					this->LineWidgetSelected 						= -1;
//					this->SphereWidgetSelected 					= this->PointSelectedId;
//	  			this->StraightModelObjectSelected = TERMINAL;
//	  			}  // End if ( ( this->StraightModelObjectSelected == TERMINAL ) && ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment ) )
//
//	    	this->EnablePointWidget();
//	    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
//	    	this->State = vtkHMStraightModelWidget::MovingHandle;
				}

	    this->EventCallbackCommand->SetAbortFlag(1);
	    this->StartInteraction();
	    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);

	  	this->State = vtkHMStraightModelWidget::MovingHandle;
//	  	this->HighlightHandles(1);
	  	} // Fim if ( path != NULL ) -> comparação do terminal picker
	  else
    	{
    	this->StenosisPicker->Pick(X,Y,0.0,this->CurrentRenderer);
    	path = this->StenosisPicker->GetPath();
     	if ( path != NULL )
   	  	{
   	  	//  	this->HighlightSelectedActors(0);
   	   	// Acertou um stenosis
   			vtkIdType cellId = this->StenosisPicker->GetCellId();
   			cout << "\nStenosis cell: " << cellId << endl;
   			vtkIdType *pt;

   			vtkIdType d = this->StenosisPicker->GetSubId();
   	  	vtkIdList *idList = this->GridStenosis->GetStenosis(cellId);

   			cout << "Elem: " << idList->GetId(d) << "\tSeg: " << idList->GetId(idList->GetNumberOfIds()-1) << endl;

      	// Acertou um stenosis
   			this->StraightModelElementSelected 	= cellId;
        this->LineSelectedId 								= cellId;
   			this->SegmentSelected								= idList->GetId(idList->GetNumberOfIds()-1);
   			this->TerminalSelected							= -1;
   			this->NodeSelected									= idList->GetId(d);
   			this->ElementSelected								= idList->GetId(d);
      	this->StraightModelObjectSelected 	= STENOSIS;
      	this->HighlightLine(1);

    		//Se clicar em um terminal colorir todos os filhos dele inclusive ele.
    		//Modo ToDetachSubTree
    		if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
    			{
	  	    	this->EnablePointWidget();
	  	    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
	  	    	this->State = vtkHMStraightModelWidget::MovingLine;
  				if(!this->Interactor->GetShiftKey())
  					{
  					this->RenderSegmentsWithDefaultColors();
  					this->ToDetachSubTree();
  					}
  				else
  					{
  					this->RenderSegmentsWithDefaultColors();
  					this->ToDetachSubTreeErase();
  					}
    	    }

   			else if ( this->GetStraightModelMode() == MODE_EDITION_VAL )
   				{
   				if (this->MoveElement)
   					{
			    	this->EnablePointWidget();
			    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
				  	this->State = vtkHMStraightModelWidget::MovingLine;
   					}
   				}
         	this->EventCallbackCommand->SetAbortFlag(1);
         	this->StartInteraction();
         	this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
//   				this->State = vtkHMStraightModelWidget::MovingLine;
   	  	}
     	else
     		{
      	this->AneurysmPicker->Pick(X,Y,0.0,this->CurrentRenderer);
      	path = this->AneurysmPicker->GetPath();
       	if ( path != NULL )
     	  	{
     	  	//  	this->HighlightSelectedActors(0);
     	   	// Acertou um aneurisma
     			vtkIdType cellId = this->AneurysmPicker->GetCellId();
     			cout << "\nAneurysm cell: " << cellId << endl;
     			vtkIdType *pt;

     			vtkIdType d = this->AneurysmPicker->GetSubId();
     	  	vtkIdList *idList = this->GridAneurysm->GetAneurysm(cellId);

     			cout << "Elem: " << idList->GetId(d) << "\tSeg: " << idList->GetId(idList->GetNumberOfIds()-1) << endl;

        	// Acertou um aneurisma
     			this->StraightModelElementSelected 	= cellId;
          this->LineSelectedId 								= cellId;
     			this->SegmentSelected								= idList->GetId(idList->GetNumberOfIds()-1);
     			this->TerminalSelected							= -1;
     			this->NodeSelected									= idList->GetId(d);
     			this->ElementSelected								= idList->GetId(d);
        	this->StraightModelObjectSelected 	= ANEURYSM;
        	this->HighlightLine(1);

      		//Se clicar em um terminal colorir todos os filhos dele inclusive ele.
      		//Modo ToDetachSubTree
      		if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
      			{
	    	    	this->EnablePointWidget();
	    	    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
	    	    	this->State = vtkHMStraightModelWidget::MovingLine;
    				if(!this->Interactor->GetShiftKey())
    					{
    					this->RenderSegmentsWithDefaultColors();
    					this->ToDetachSubTree();
    					}
    				else
    					{
    					this->RenderSegmentsWithDefaultColors();
    					this->ToDetachSubTreeErase();
    					}
      	    }

     			else if ( this->GetStraightModelMode() == MODE_EDITION_VAL )
     				{
     				if (this->MoveElement)
     					{
				    	this->EnablePointWidget();
				    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
					  	this->State = vtkHMStraightModelWidget::MovingLine;

     					}
     				}
           this->EventCallbackCommand->SetAbortFlag(1);
           this->StartInteraction();
           this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
//     			 this->State = vtkHMStraightModelWidget::MovingLine;
     	  	}
       	else
	     		{
		     	this->StentPicker->Pick(X,Y,0.0,this->CurrentRenderer);
		     	path = this->StentPicker->GetPath();
		     	if ( path != NULL )
		   	  	{
		   	  	//  	this->HighlightSelectedActors(0);
		   	   	// Acertou um stent
		   			vtkIdType cellId = this->StentPicker->GetCellId();
		   			cout << "\nStent cell: " << cellId << endl;
		   			vtkIdType *pt;

		   			vtkIdType d = this->StentPicker->GetSubId();
		   	  	vtkIdList *idList = this->GridStent->GetStent(cellId);

		   			cout << "Elem: " << idList->GetId(d) << "\tSeg: " << idList->GetId(idList->GetNumberOfIds()-1) << endl;

		      	// Acertou um stent
		   			this->StraightModelElementSelected 	= cellId;
		        this->LineSelectedId 								= cellId;
		   			this->SegmentSelected								= idList->GetId(idList->GetNumberOfIds()-1);
		   			this->TerminalSelected							= -1;
		   			this->NodeSelected									= -1;
		   			this->ElementSelected								= idList->GetId(d);
		      	this->StraightModelObjectSelected 	= STENT;
		      	this->HighlightLine(1);

		    		//Se clicar em um terminal colorir todos os filhos dele inclusive ele.
		    		//Modo ToDetachSubTree
		    		if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
		    			{
			  	    	this->EnablePointWidget();
			  	    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
			  	    	this->State = vtkHMStraightModelWidget::MovingLine;
		  				if(!this->Interactor->GetShiftKey())
		  					{
		  					this->RenderSegmentsWithDefaultColors();
		  					this->ToDetachSubTree();
		  					}
		  				else
		  					{
		  					this->RenderSegmentsWithDefaultColors();
		  					this->ToDetachSubTreeErase();
		  					}
		    	    }

		   			else if ( this->GetStraightModelMode() == MODE_EDITION_VAL )
		   				{
		   				if (this->MoveElement)
		   					{
					    	this->EnablePointWidget();
					    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
						  	this->State = vtkHMStraightModelWidget::MovingLine;

		   					}
		   				}
		        this->EventCallbackCommand->SetAbortFlag(1);
		        this->StartInteraction();
		        this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
//		   			this->State = vtkHMStraightModelWidget::MovingLine;
		   	  	}
				  else
				  	{
				  	path = NULL;
						if ( this->NodePicker )
							{
							this->NodePicker->Pick(X,Y,0.0,this->CurrentRenderer);
					    path = this->NodePicker->GetPath();
							}

			    	if ( path != NULL )
					    {
					    this->HighlightSelectedActors(0);

					    vtkIdType no = this->NodePicker->GetCellId();
				    	cout << "\nNode cell: " << no << endl;

				    	vtkIdType *pt;

					 		pt = this->GridLine->GetNode(no);
					 		cout << "Point: " << pt[0] << "\tNo: " << pt[1] << "\tSeg: " << pt[2] << endl;

					 		this->StraightModelElementSelected 	= pt[2];
					    this->PointSelectedId 							= pt[0];
							this->SegmentSelected								= pt[2];
							this->TerminalSelected							= -1;
							this->NodeSelected									= pt[1];
							this->ElementSelected								= -1;
							this->LineWidgetSelected 						= -1;
							this->StraightModelObjectSelected 	= NODE;
							this->SphereWidgetSelected 					= this->PointSelectedId;
							this->HighlightHandles(1);

							if ( (this->GetStraightModelMode() == MODE_EDITION_VAL) && this->MoveElement )
								{
								// Se modo de adicao de segmento
								if ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment )
					  			{
									this->HighlightHandles(0);
					  			double x[3];

									// pega a coordenada da esfera clicada para setar o novo segmento
									this->GridLine->GetPoint(this->PointSelectedId, x);

					  			//Pega o segmento clicado e o terminal filho
									vtkHM1DSegment *segm 	= this->StraightModel->GetSegment(this->SegmentSelected);
									vtkHM1DTreeElement *elem;

									elem = segm->GetFirstChild();

									//Procura pelo terminal filho do segmento
									while ( !this->StraightModel->IsTerminal(elem->GetId()) )
										elem = segm->GetNextChild();

									//Dividir o segmento em dois
									this->DivideSegment(this->SegmentSelected, this->NodeSelected);

									//Verifica se no segmento existe algum clip
									vtkIdType pos = this->FindClipIds(this->SegmentSelected);
									if ( pos != -1 )
										{
										double *tupla = this->ClipSegments->GetTuple(pos);
										//verifica se o segmento selecionado é a parte antes do clip,
										//caso seja, altera o segmento associado com o clip para o novo
										//segmento.
										if ( this->SegmentSelected == tupla[0] )
											{
											tupla[0] = elem->GetFirstChild()->GetId();
											this->ClipSegments->SetTuple(pos, tupla);
											}
										}

									//Adiciona o novo segmento
									vtkIdType id = this->AddLineWidget(x, 2, elem->GetId());

									this->GridLine->UpdateGrid();

									pt = this->GridLine->GetTerminal(this->NumberOfTerminals-1);

					  			this->StraightModelElementSelected 	= pt[1];
							    this->PointSelectedId 							= pt[0];
									this->SegmentSelected								= -1;
									this->TerminalSelected							= pt[1];
									this->NodeSelected									= -1;
									this->ElementSelected								= -1;
									this->LineWidgetSelected 						= -1;
									this->SphereWidgetSelected 					= this->PointSelectedId;
					  			this->StraightModelObjectSelected 	= TERMINAL;

					  			}  // End if ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment )
					  		//Se modo de edicao for o de adicao de terminal
					  		else if ( this->EditionModeSelected == vtkHMStraightModelWidget::AddTerminal )
					  			{
									this->HighlightHandles(0);
					  			double x[3];

									// pega a coordenada da esfera clicada para setar o novo segmento
									this->GridLine->GetPoint(this->PointSelectedId, x);

					  			//Pega o segmento clicado e o terminal filho
									vtkHM1DSegment *segm 	= this->StraightModel->GetSegment(this->SegmentSelected);
									vtkHM1DTreeElement *elem;

									elem = segm->GetFirstChild();

									//Procura pelo terminal filho do segmento
									while ( !this->StraightModel->IsTerminal(elem->GetId()) )
										elem = segm->GetNextChild();

									this->DivideSegment(this->SegmentSelected, this->NodeSelected);

									//Verifica se no segmento existe algum clip
									vtkIdType pos = this->FindClipIds(this->SegmentSelected);
									if ( pos != -1 )
										{
										double *tupla = this->ClipSegments->GetTuple(pos);
										//verifica se o segmento selecionado é a parte antes do clip,
										//caso seja, altera o segmento associado com o clip para o novo
										//segmento.
										if ( this->SegmentSelected == tupla[0] )
											{
											tupla[0] = elem->GetFirstChild()->GetId();
											this->ClipSegments->SetTuple(pos, tupla);
											}
										}

					  			this->GridLine->UpdateGrid();
									this->UpdateGrid();

									pt = this->GridLine->GetTerminalInformation(elem->GetId());

					  			this->StraightModelElementSelected 	= pt[1];
							    this->PointSelectedId 							= pt[0];
									this->SegmentSelected								= -1;
									this->TerminalSelected							= pt[1];
									this->NodeSelected									= -1;
									this->ElementSelected								= -1;
									this->LineWidgetSelected 						= -1;
									this->SphereWidgetSelected 					= this->PointSelectedId;
					  			this->StraightModelObjectSelected 	= TERMINAL;

					  			}  // End if ( this->EditionModeSelected == vtkHMStraightModelWidget::AddTerminal )
//								if (this->MoveElement == true)
//									{
							 		this->EnablePointWidget();
						    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
						    	this->State = vtkHMStraightModelWidget::MovingHandle;
//									}
					    	this->HighlightHandles(1);
								}  // End if(this->GetStraightModelMode() == MODE_EDITION_VAL)

							else if ( this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL )
								{
								this->HighlightHandles(1);

								if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Clip )
									{
									this->HighlightHandles(0);

									vtkIdType pos = this->FindClipIds(this->SegmentSelected);
									if ( pos == -1 )
										{
										this->AddClip(this->SegmentSelected, this->NodeSelected);
										this->StraightModelElementSelected = this->ClipSegments->GetNumberOfTuples()-1;
										this->StraightModelObjectSelected = CLIP;
										}
									else
										vtkWarningMacro(<<"This segment already has clip");
									} // fim if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Clip )
								else if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stent )
                  {
                  this->StraightModelElementSelected  = -1;
                  this->StraightModelObjectSelected   = STENT;
                  }
								else if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stenosis )
									{
//									this->HighlightHandles(0);
									this->StraightModelElementSelected 	= -1;
									this->StraightModelObjectSelected 	= STENOSIS;
									} // fim if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stenosis )

								else if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Aneurysm )
									{
//									this->HighlightHandles(0);
									this->StraightModelElementSelected 	= -1;
									this->StraightModelObjectSelected 	= ANEURYSM;
									} // fim if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Aneurysm )

								// Se modo de adicao de segmento
//								else if ( (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment) && this->MoveElement )
//					  			{
//									this->HighlightHandles(0);
//					  			double x[3];
//
//									// pega a coordenada da esfera clicada para setar o novo segmento
//									this->GridLine->GetPoint(this->PointSelectedId, x);
//
//					  			//Pega o segmento clicado e o terminal filho
//									vtkHM1DSegment *segm 	= this->StraightModel->GetSegment(this->SegmentSelected);
//									vtkHM1DTreeElement *elem;
//
//									elem = segm->GetFirstChild();
//
//									//Procura pelo terminal filho do segmento
//									while ( !this->StraightModel->IsTerminal(elem->GetId()) )
//										elem = segm->GetNextChild();
//
//									//Dividir o segmento em dois
//									this->DivideSegment(this->SegmentSelected, this->NodeSelected);
//
//									//Verifica se no segmento existe algum clip
//									vtkIdType pos = this->FindClipIds(this->SegmentSelected);
//									if ( pos != -1 )
//										{
//										double *tupla = this->ClipSegments->GetTuple(pos);
//										//verifica se o segmento selecionado é a parte antes do clip,
//										//caso seja, altera o segmento associado com o clip para o novo
//										//segmento.
//										if ( this->SegmentSelected == tupla[0] )
//											{
//											tupla[0] = elem->GetFirstChild()->GetId();
//											this->ClipSegments->SetTuple(pos, tupla);
//											}
//										}
//
//									//Adiciona o novo segmento
//									vtkIdType id = this->AddLineWidget(x, 2, elem->GetId());
//
//									this->GridLine->UpdateGrid();
//
//									pt = this->GridLine->GetTerminal(this->NumberOfTerminals-1);
//
//					  			this->StraightModelElementSelected 	= pt[1];
//							    this->PointSelectedId 							= pt[0];
//									this->SegmentSelected								= -1;
//									this->TerminalSelected							= pt[1];
//									this->NodeSelected									= -1;
//									this->ElementSelected								= -1;
//									this->LineWidgetSelected 						= -1;
//									this->SphereWidgetSelected 					= this->PointSelectedId;
//					  			this->StraightModelObjectSelected 	= TERMINAL;
//
//							 		this->EnablePointWidget();
//						    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
//						    	this->State = vtkHMStraightModelWidget::MovingHandle;
//
//					  			}  // End if ( this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment )
//								else
//                  {
//                  this->EnablePointWidget();
//                  forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
//                  this->State = vtkHMStraightModelWidget::MovingHandle;
//                  }
								}  // End else if ( this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL )

					    this->EventCallbackCommand->SetAbortFlag(1);
					    this->StartInteraction();
					    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);

					  	this->State = vtkHMStraightModelWidget::MovingHandle;

					    }  //Fim if ( path != NULL ) -> comparacao do node picker

						else
					  	{
					  	this->LinePicker->Pick(X,Y,0.0,this->CurrentRenderer);
					    path = this->LinePicker->GetPath();

					    if ( path != NULL )
					    	{
					    	this->HighlightSelectedActors(0);

					    	vtkIdType l = this->LinePicker->GetCellId();
					    	cout << "\nLine cell: " << l << endl;
					    	vtkIdType *pt;

						 		pt = this->GridLine->GetSegment(l);
						 		cout << "Points: " << pt[0] << "\t" << pt[1] << "\tElem: " << pt[2] << "\tSeg: " << pt[3] << endl;

					   		// Acertou um segmento
								this->StraightModelElementSelected 	= pt[3];
					      this->LineSelectedId 								= l;
								this->SegmentSelected								= pt[3];
								this->TerminalSelected							= -1;
								this->NodeSelected									= -1;
								this->ElementSelected								= pt[2];
								this->LineWidgetSelected 						= this->LineSelectedId;
								this->SphereWidgetSelected 					= -1;
					   		this->StraightModelObjectSelected 	= SEGMENT;

					   		this->HighlightLine(1);

					  		//Se clicar em um terminal colorir todos os filhos dele inclusive ele.
					  		//Modo ToDetachSubTree
					  		if ( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
					  			{
							    	this->EnablePointWidget();
							    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
							    	this->State = vtkHMStraightModelWidget::MovingLine;
									if(!this->Interactor->GetShiftKey())
										{
										this->RenderSegmentsWithDefaultColors();
										this->ToDetachSubTree();
										}
									else
										{
										this->RenderSegmentsWithDefaultColors();
										this->ToDetachSubTreeErase();
										}
					  	    }

								else if ( this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL )
									{
									if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stent )
										{
										this->StraightModelElementSelected 	= -1;
										this->StraightModelObjectSelected 	= STENT;
										}
		              else if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stenosis )
		                {
                    this->StraightModelElementSelected  = -1;
                    this->StraightModelObjectSelected   = STENOSIS;
                    } // fim if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Stenosis )
                  else if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Aneurysm )
                    {
                    this->StraightModelElementSelected  = -1;
                    this->StraightModelObjectSelected   = ANEURYSM;
                    } // fim if ( this->SurgicalInterventionsModeSelected == vtkHMStraightModelWidget::Aneurysm )
//									else
//                    {
//									  this->EnablePointWidget();
//                    forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
//                    this->State = vtkHMStraightModelWidget::MovingLine;
//                    }
									}
								else if ( this->GetStraightModelMode() == MODE_EDITION_VAL )
									{
									if (this->MoveElement == true)
										{
							    	this->EnablePointWidget();
							    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
								  	this->State = vtkHMStraightModelWidget::MovingLine;
										}
									}
					      this->EventCallbackCommand->SetAbortFlag(1);
					      this->StartInteraction();
					      this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
//								this->State = vtkHMStraightModelWidget::MovingLine;
					    	}// Fim if ( path != NULL ) -> comparação do line picker
						  else
							  {
					    	// Nao acertou nada!
					  	  // Adiciona um segmento na render
						    if( (this->GetStraightModelMode() == MODE_EDITION_VAL)
						        && (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment)
						        && this->MoveElement )
					  			{
					  			double x[3];

					  			this->TerminalPicker->GetPickPosition(x);

					  			// Do different things depending on state
								  // Calculations everybody does
								  double focalPoint[4], pickPoint[4];
								  double z=0;

									// Compute the two points defining the motion vector
								  this->ComputeWorldToDisplay(this->LastPickPosition[0], this->LastPickPosition[1],
								                              this->LastPickPosition[2], focalPoint);
								  z = focalPoint[2];

								  this->ComputeDisplayToWorld(double(X), double(Y), z, pickPoint);

					  			this->AddLineWidget(pickPoint, 2, 0);

					  			vtkIdType *pt;
									pt = this->GridLine->GetTerminal(this->NumberOfTerminals-1);

					  			this->StraightModelElementSelected 	= pt[1];
							    this->PointSelectedId 							= pt[0];
									this->SegmentSelected								= pt[2];
									this->TerminalSelected							= pt[1];
									this->NodeSelected									= -1;
									this->ElementSelected								= -1;
									this->LineWidgetSelected 						= -1;
									this->SphereWidgetSelected 					= this->PointSelectedId;
					  			this->StraightModelObjectSelected = TERMINAL;

//									if (this->MoveElement == true)
//										{
							    	this->EnablePointWidget();
							    	forward = this->ForwardEvent(vtkCommand::LeftButtonPressEvent);
								  	this->State = vtkHMStraightModelWidget::MovingHandle;
//										}

								  this->EventCallbackCommand->SetAbortFlag(1);
							    this->StartInteraction();
							    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
//							  	this->State = vtkHMStraightModelWidget::MovingHandle;

					  			this->Interactor->Render();
					  			} // Fim if( (this->GetStraightModelMode() == MODE_EDITION_VAL) && (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment) )
								else if( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
									{
									this->StartInteraction();
							    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
									this->State = vtkHMStraightModelWidget::Outside;

						      return;
									}
								else
							    {
							    this->ClearElementSelected();
						      if ( this->GetStraightModelMode() != MODE_PLOT_VAL )
										this->HighlightSelectedActors(0);


						      //////////////////////////////////////////////////////////////////////////
						      //Verificar quando nada é acertado para limpar a interface e não exibir
						      //informações de nenhum elemento da árvore.
						      //////////////////////////////////////////////////////////////////////////

						      this->EventCallbackCommand->SetAbortFlag(0);
							    this->StartInteraction();
							    this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
									this->State = vtkHMStraightModelWidget::Outside;

						      return;
							    }
					    	} //Fim else não acertou nada
					  	} //Fim else node
				  	} // Fim else stent
	     		} //Fim else stent
     		} //Fim else aneurysm
		  } //Fim else stenosis
	  } //Fime else clip

	if (!forward)
    {
    this->EventCallbackCommand->SetAbortFlag(1);
    this->Interactor->Render();
    }
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnLeftButtonUp()
{
	int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];
	int Z = this->Interactor->GetEventPosition()[2];

	if(this->GetStraightModelMode() == MODE_PLOT_VAL)
		{
		this->SizeHandles();
		this->State = vtkHMStraightModelWidget::Start;
		this->DisablePointWidget();
	  this->EndInteraction();
	  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
	  this->Interactor->Render();
	  return;
		}

	//se modo de destaque de sub-arvore
	if( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL )
		{
		if ( this->EditionModeSelected == vtkHMStraightModelWidget::Move )
			{
			if ( this->StraightModelObjectSelected == SEGMENT )
		  	{
				//Pego o segmento selecionado
				vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegmentSelected);
				//Pego o ponto clicado no segmento
				double *pcoord = this->LinePicker->GetPickPosition();
				//calculo a distancia entre o ponto clicado e o ponto do ultimo evento ocorrido
				double distance = this->ComputeVectorLength(pcoord, this->LastPosition);
				//se a distancia entre eles for maior que o raio dos terminais, desconecto o segmento
				if ( distance > 0.2 )
					{
					this->DisconnectSubTree();
					this->GridLine->UpdateGrid();
					this->UpdateGrid();
					//verifico se o terminal pai colide com outro terminal
					vtkHM1DTreeElement *parent = segm->GetFirstParent();
					vtkIdType idParent = this->DetectCollisionBetweenTerminals(parent->GetId());
					//Se terminal pai for diferente do coração
					if ( parent->GetId() != 1 )
						{
						//Verificar se terminal pai colide com outro terminal diferente do coração
						if ( idParent != -1 )
							{
							if ( idParent != 1 )
								this->ConnectElement(parent->GetId(), idParent);
							else
								vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
							}
						}
					//verifico de o terminal filho colide com outro terminal
					vtkHM1DTreeElement *child = segm->GetFirstChild();
					vtkIdType idChild = this->DetectCollisionBetweenTerminals(child->GetId());
					//Verificar se terminal filho colide com outro terminal diferente do coração
					if ( idChild != -1 )
						{
						if ( idChild != 1 )
							this->ConnectElement(child->GetId(), idChild);
						else
							vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
						}
						this->HighlightLine(0);
						this->HighlightHandles(0);
						for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
							this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
				//		for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro a lista global de TERMINAIS
						this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid
						this->HighlightLine(1);
						this->HighlightHandles(1);
		  		}
			  } //Fim if ( this->StraightModelObjectSelected == SEGMENT ))
	  	else if ( this->StraightModelObjectSelected == TERMINAL )
	  		{
	  		//Pego o ponto clicado no segmento
				double *pcoord = this->TerminalPicker->GetPickPosition();
				//calculo a distancia entre o ponto clicado e o ponto do ultimo evento ocorrido
				double distance = this->ComputeVectorLength(pcoord, this->LastPosition);
				//se a distancia entre eles for maior que o raio dos terminais, desconecto o segmento
				if ( distance > 0.2 )
					{
		  		vtkHM1DTerminal *termSelected = this->StraightModel->GetTerminal(this->TerminalSelected);
		  		vtkHM1DSegment *segmChild = this->StraightModel->GetSegment(termSelected->GetId());
					//Verifica se ha colisao entre o terminal selecionado e algum outro terminal
					vtkIdType termId = this->DetectCollisionBetweenTerminals(termSelected->GetId());
					if ( termId == -1 )
						{
						this->HighlightLine(0);
						this->HighlightHandles(0);
						for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
							this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
				//		for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro a lista global de TERMINAIS
						this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid
						this->HighlightLine(1);
						this->HighlightHandles(1);
						}
					else if ( termId != -1 )
						{
	  				if ( termId != 1 )
							{
		  				this->ConnectElement(this->TerminalSelected, termId);
			  			this->ClearElementSelected();
			  			this->ClearSubTreeLists();
			  			this->HighlightHandles(0);
			  			this->HighlightLine(0);
							this->Interactor->Render();
							}
	  				else
		  				{
		  				vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
		  				}
						} //Fim if ( termId != -1 )
	  			}
	  		} //Fim if ( this->StraightModelObjectSelected == TERMINAL )
			this->UpdateActors();
			this->UpdateGridSelected();
			} //Fim do if ( this->EditionModeSelected == vtkHMStraightModelWidget::Move )
		else if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
			{
			double distance;
			if ( this->StraightModelObjectSelected == SEGMENT )
				{
				//Pego o ponto clicado no segmento
				double *pcoord = this->LinePicker->GetPickPosition();
				//calculo a distancia entre o ponto clicado e o ponto do ultimo evento ocorrido
				distance = this->ComputeVectorLength(pcoord, this->LastPosition);
				}
			else if ( this->StraightModelObjectSelected == TERMINAL )
				{
				//Pego o ponto clicado no segmento
				double *pcoord = this->TerminalPicker->GetPickPosition();
				//calculo a distancia entre o ponto clicado e o ponto do ultimo evento ocorrido
				distance = this->ComputeVectorLength(pcoord, this->LastPosition);
				}
			// Se houve um movimento, atualiza todo mundo
			if (distance > 0.2)
				{
				this->HighlightLine(0);
				this->HighlightHandles(0);
				for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
					this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
				this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid
				this->HighlightLine(1);
				this->HighlightHandles(1);
				this->UpdateActors();
				this->UpdateGridSelected();
				}
			} //fim do if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
		} //fim do if( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL )
		//Se modo de edição

	else if ( this->GetStraightModelMode() == MODE_EDITION_VAL )
		{
		this->Interactor->SetKeyCode('q');
		this->MoveElement=false;

		vtkIdType *info, *info2;

		//Se modo de adicao de segmento estiver selecionado
		if ( (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment) && (this->StraightModelObjectSelected == TERMINAL) )
			{
		  // Okay, make sure that the pick is in the current renderer
		  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
		    {
		    this->State = vtkHMStraightModelWidget::Outside;
		    return;
		    }
		  // Okay, we can process this. Try to pick handles first;
		  // if no handles picked, then try to pick the line.
		  vtkAssemblyPath *path = NULL;

		  this->TerminalPicker->Pick(X,Y,0.0,this->CurrentRenderer);
		  path = this->TerminalPicker->GetPath();

		  if ( path != NULL )
		  	{
		  	vtkIdType cellId = this->TerminalPicker->GetCellId();

		  	vtkIdType *pt;
				pt = this->GridLine->GetTerminal(cellId);
		  	vtkIdType termId = pt[1];

				if ( (termId != -1) && (termId != this->TerminalSelected) && (termId != this->StraightModel->GetTerminal(this->TerminalSelected)->GetFirstParent()->GetFirstParent()->GetId()) )
					{
					if ( termId == 1 )
						{
						this->UpdateGrid();
						vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
						}
					else
						{
						vtkHM1DTerminal *term = this->StraightModel->GetTerminal(termId);
						vtkHM1DTerminal *termSelected = this->StraightModel->GetTerminal(this->TerminalSelected);

						double p[3], pt1[3], pt2[3];
						info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
						info2 = this->GridLine->GetTerminalInformation(termId);

						this->GridLine->GetPoints()->GetPoint(info[0], pt1);
						this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

						p[0] = pt2[0] - pt1[0];
						p[1] = pt2[1] - pt1[1];
						p[2] = pt2[2] - pt1[2];

						vtkHM1DTreeElement *segParent = termSelected->GetFirstParent();
						segParent->AddChild(term);

						this->GridLine->RemoveTerminalCell(this->TerminalSelected);
						this->GridTerminal->RemoveTerminalCell(this->TerminalSelected);
						this->StraightModel->RemoveTerminal(this->TerminalSelected, 0);

						// Setting number of Points, Segments and Terminals
						this->NumberOfPoints 		= this->GridLine->GetPoints()->GetNumberOfPoints();
						this->NumberOfSegments  = this->GridLine->GetSegments()->GetNumberOfCells();
						this->NumberOfTerminals = this->StraightModel->GetNumberOfTerminals();
						this->NumberOfNodes			= this->StraightModel->GetNumberOfNodes();
						this->NumberOfHandles		= this->NumberOfTerminals+this->NumberOfNodes;

						this->MoveParent(p, segParent->GetId());
						this->UpdateGrid();

						this->ClearElementSelected();
						this->HighlightHandles(0);

						} //End else
					}  //End if ( termId != -1 )
				else
					{
					vtkIdType *info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
					double *p = this->GridLine->GetPoint(info[0]);

					//Adicionar o terminal filho do segmento no widget de terminais
					this->AddTerminalWidget(p, this->TerminalSelected);
					this->UpdateGrid();
					}
		  	} // End if ( path != NULL )
		  else
				{
				vtkIdType *info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
				double *p = this->GridLine->GetPoint(info[0]);

				//Adicionar o terminal filho do segmento no widget de terminais
				this->AddTerminalWidget(p, this->TerminalSelected);

				this->UpdateGrid();
				}
			} // Fim if ( (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment ) && (this->StraightModelObjectSelected == TERMINAL) )

		//Se segmento foi movido
		else if ( this->EditionModeSelected == vtkHMStraightModelWidget::Move )
			{
			if ( this->StraightModelObjectSelected == SEGMENT )
		  	{
				//Pego o segmento selecionado
				vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegmentSelected);
				//Verifico se o segmento possui alguma conexão com outro segmento
				if ( !segm->GetFirstChild()->IsLeaf() || (segm->GetFirstParent()->GetNumberOfParents() > 0) )
					{
					//Pego o ponto clicado no segmento
					double *pcoord = this->LinePicker->GetPickPosition();

					//calculo a distancia entre o ponto clicado e o ponto do ultimo evento ocorrido
					double distance = this->ComputeVectorLength(pcoord, this->LastPosition);
					this->SizeHandles();

					//se a distancia entre eles for maior que o raio dos terminais, desconecto o segmento
					if ( distance > this->RadiusNode )
						{
						this->DisconnectElement();
						this->GridLine->UpdateGrid();
						this->UpdateGrid();
            this->HighlightLine(0);
            this->HighlightLine(1);
						}
					//caso o movimento realizado nao seja suficiente para desconectar, mover o segmento
					//para seu local de origem
					else
						{
						double v[3];
						v[0] = this->LastPosition[0] - pcoord[0];
						v[1] = this->LastPosition[1] - pcoord[1];
						v[2] = this->LastPosition[2] - pcoord[2];
						this->MoveSegment(v, this->SegmentSelected);
						this->HighlightLine(0);
						this->HighlightLine(1);
						}
					} // fim if ( !segm->GetFirstChild()->IsLeaf() || (segm->GetFirstParent()->GetNumberOfParents() > 0) )

				vtkHM1DTreeElement *parent = segm->GetFirstParent();

				vtkIdType idParent = this->DetectCollisionBetweenTerminals(parent->GetId());

//				while ( idParent != -1 )
//					{
					//Se terminal pai for diferente do coração
					if ( parent->GetId() != 1 )
						{
						//Verificar se terminal pai colide com outro terminal diferente do coração
						if ( idParent != -1 )
							{
							if ( idParent != 1 )
								this->ConnectElement(parent->GetId(), idParent);
							else
								vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
							}
						}
//					parent = segm->GetFirstParent();
//					idParent = this->DetectCollisionBetweenTerminals(parent->GetId());
//					}

				vtkHM1DTreeElement *child = segm->GetFirstChild();
				vtkIdType idChild = this->DetectCollisionBetweenTerminals(child->GetId());

				//Verificar se terminal filho colide com outro terminal diferente do coração
				if ( idChild != -1 )
					{
					if ( idChild != 1 )
						this->ConnectElement(child->GetId(), idChild);
					else
						vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");

//					child = segm->GetFirstChild();
//					idChild = this->DetectCollisionBetweenTerminals(child->GetId());
					}
			  } //Fim if ( this->StraightModelObjectSelected == SEGMENT ))

	  	else if ( this->StraightModelObjectSelected == TERMINAL )
	  		{
	  		vtkHM1DTerminal *termSelected = this->StraightModel->GetTerminal(this->TerminalSelected);

  			//Verifica se ha colisao entre o terminal selecionado e algum outro terminal
  			vtkIdType termId = this->DetectCollisionBetweenTerminals();

  			if ( termId != -1 )
  				{
	  			if ( termId != this->TerminalSelected )
	  				{
	  				if ( termId == 1 )
							{
							vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
							}
	  				else
		  				{
		  				this->ConnectElement(this->TerminalSelected, termId);

		  				this->ClearElementSelected();
				      this->HighlightHandles(0);

							this->Interactor->Render();
							this->TerminalSelected = termId;
		  				termId = this->DetectCollisionBetweenTerminals();
		  				}
	  				} //Fim if ( (termId != -1) && (termId != this->TerminalSelected))
  				} //Fim while ( termId != -1 )
	  		} //Fim if ( this->StraightModelObjectSelected == TERMINAL ))
	  	} //Fim else if ( this->EditionModeSelected == vtkHMStraightModelWidget::Move )

		this->UpdateActors();
		this->UpdateGridSelected();
		}  // Fim if ( this->GetStraightModelMode() == MODE_EDITION_VAL )

	else if ( this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL )
		{
		vtkIdType *info, *info2;

		//Se modo de adicao de segmento estiver selecionado
		if ( (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment) && (this->StraightModelObjectSelected == TERMINAL) )
			{
		  // Okay, make sure that the pick is in the current renderer
		  if (!this->CurrentRenderer || !this->CurrentRenderer->IsInViewport(X, Y))
		    {
		    this->State = vtkHMStraightModelWidget::Outside;
		    return;
		    }
		  // Okay, we can process this. Try to pick handles first;
		  // if no handles picked, then try to pick the line.
		  vtkAssemblyPath *path = NULL;

		  this->TerminalPicker->Pick(X,Y,0.0,this->CurrentRenderer);
		  path = this->TerminalPicker->GetPath();

		  if ( path != NULL )
		  	{
		  	vtkIdType cellId = this->TerminalPicker->GetCellId();

		  	vtkIdType *pt;
				pt = this->GridLine->GetTerminal(cellId);
		  	vtkIdType termId = pt[1];

				if ( (termId != -1) && (termId != this->TerminalSelected) && (termId != this->StraightModel->GetTerminal(this->TerminalSelected)->GetFirstParent()->GetFirstParent()->GetId()) )
					{
					if ( termId == 1 )
						{
						this->UpdateGrid();
						vtkWarningMacro(<<"This is heart and it cannot be connected to another segment or terminal");
						}
					else
						{
						vtkHM1DTerminal *term = this->StraightModel->GetTerminal(termId);
						vtkHM1DTerminal *termSelected = this->StraightModel->GetTerminal(this->TerminalSelected);

						double p[3], pt1[3], pt2[3];
						info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
						info2 = this->GridLine->GetTerminalInformation(termId);

						this->GridLine->GetPoints()->GetPoint(info[0], pt1);
						this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

						p[0] = pt2[0] - pt1[0];
						p[1] = pt2[1] - pt1[1];
						p[2] = pt2[2] - pt1[2];

						vtkHM1DTreeElement *segParent = termSelected->GetFirstParent();
						segParent->AddChild(term);

						this->GridLine->RemoveTerminalCell(this->TerminalSelected);
						this->GridTerminal->RemoveTerminalCell(this->TerminalSelected);
						this->StraightModel->RemoveTerminal(this->TerminalSelected, 0);

						// Setting number of Points, Segments and Terminals
						this->NumberOfPoints 		= this->GridLine->GetPoints()->GetNumberOfPoints();
						this->NumberOfSegments  = this->GridLine->GetSegments()->GetNumberOfCells();
						this->NumberOfTerminals = this->StraightModel->GetNumberOfTerminals();
						this->NumberOfNodes			= this->StraightModel->GetNumberOfNodes();
						this->NumberOfHandles		= this->NumberOfTerminals+this->NumberOfNodes;

						this->MoveParent(p, segParent->GetId());
						this->UpdateGrid();

						this->ClearElementSelected();

						this->HighlightHandles(0);

						} //End else
					}  //End if ( termId != -1 )
				else
					{
					vtkIdType *info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
					double *p = this->GridLine->GetPoint(info[0]);

					//Adicionar o terminal filho do segmento no widget de terminais
					this->AddTerminalWidget(p, this->TerminalSelected);
					this->UpdateGrid();
					}
		  	} // End if ( path != NULL )

		  else
				{
				vtkIdType *info = this->GridLine->GetTerminalInformation(this->TerminalSelected);
				double *p = this->GridLine->GetPoint(info[0]);

				//Adicionar o terminal filho do segmento no widget de terminais
				this->AddTerminalWidget(p, this->TerminalSelected);

				this->UpdateGrid();
				}
			} // Fim if ( (this->EditionModeSelected == vtkHMStraightModelWidget::AddSegment ) && (this->StraightModelObjectSelected == TERMINAL) )
		this->UpdateGridSelected();
		this->UpdateActors();
		} // Fim else if ( this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL )

	if ( this->State == vtkHMStraightModelWidget::Outside ||
       this->State == vtkHMStraightModelWidget::Start )
    {
    return;
    }

	this->State = vtkHMStraightModelWidget::Start;
	this->DisablePointWidget();
  this->EventCallbackCommand->SetAbortFlag(1);
  this->EndInteraction();
  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  this->SizeHandles();
  this->Interactor->Render();
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnMiddleButtonDown()
{
  int forward=0;
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnMiddleButtonUp()
{
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnRightButtonDown()
{
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnRightButtonUp()
{
	this->ValidPick = 1;
	this->SizeHandles();
}


//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::OnMouseMove()
{
}

//----------------------------------------------------------------------------

void vtkHMStraightModelWidget::OnChar()
{
	// Se habilitado modo de edicao da arvore
	if((this->GetStraightModelMode() == MODE_EDITION_VAL) || (this->GetStraightModelMode() == MODE_ESPECIAL_TOOLS_VAL))
		{
		if ( (this->Interactor->GetKeyCode() == 'D') && (this->StraightModelObjectSelected == SEGMENT) )
			{
			if ( (this->SegmentSelected <= 0) || (this->SegmentSelected == 2) )
				{
				vtkWarningMacro("Segment not selected or segment cannot be removed!");
				return;
				}
			//Chama o evento para no PV, retirar o nome do segmento deletado
			//da lista de nomes de clonagem de propriedades do segmento
			this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
			this->DeleteSegmentSelected();
			this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
			}
		if ( (this->Interactor->GetKeyCode() == 'D') && (this->StraightModelObjectSelected == TERMINAL) )
			{
			vtkHM1DTerminal *term = this->StraightModel->GetTerminal(this->TerminalSelected);
			if ( (this->TerminalSelected <= 1) || (term->GetNumberOfParents() != 1) || (term->GetChildCount() != 1) )
				{
				vtkWarningMacro("Terminal not selected or cannot be removed!");
				return;
				}

			if ( (term->GetNumberOfParents() == 1) && (term->GetChildCount() == 1) )
				{
				//Chama o evento para no PV, retirar o nome do segmento deletado
				//da lista de nomes de clonagem de propriedades do segmento
				this->InvokeEvent(vtkCommand::InteractionEvent,NULL);

				this->DeleteTerminalSelected();
				}
			}
		if( (this->Interactor->GetKeyCode() == 'A') || (this->Interactor->GetKeyCode() == 'a') )
			this->MoveElement = true;
		}
}

void vtkHMStraightModelWidget::Scale(double *p1, double *p2, int vtkNotUsed(X), int Y)
{
}

void vtkHMStraightModelWidget::CreateDefaultProperties()
{
  // Handle properties
  this->HandleHeartProperty = vtkProperty::New();
  this->HandleHeartProperty->SetColor(1,0,0);

  this->HandleTerminalProperty = vtkProperty::New();
  this->HandleTerminalProperty->SetColor(1,1,0);

  this->HandleNodeProperty = vtkProperty::New();
  this->HandleNodeProperty->SetColor(0,1,0);

  this->SelectedHandleProperty = vtkProperty::New();
  this->SelectedHandleProperty->SetColor(1,0,1);

  // Line properties
  this->LineProperty = vtkProperty::New();
  this->LineProperty->SetColor(1,1,1);

  this->SubTreeHandleProperty = vtkProperty::New();
  this->SubTreeHandleProperty->SetColor(0.6,0.0,0.0);
	this->SubTreeHandleProperty->SetPointSize(6);

  this->SelectedLineProperty = vtkProperty::New();
  this->SelectedLineProperty->SetLineWidth(3.0);
  // quando o usuario selecionar algum segment, este tera sua cor alterada para vermelho
  this->SelectedLineProperty->SetColor(1,0,0);

  //Propriedade para colorir a arvore principal no momento da checagem
  //de consistencia.
  this->MainStraightModelProperty = vtkProperty::New();
  this->MainStraightModelProperty->SetLineWidth(3.0);
  this->MainStraightModelProperty->SetColor(1,0,0);
}

void vtkHMStraightModelWidget::DestructorDefaultProperties()
{
	if ( this->HandleHeartProperty )
		this->HandleHeartProperty->Delete();
  if ( this->HandleTerminalProperty )
  	this->HandleTerminalProperty->Delete();
  if ( this->HandleNodeProperty )
  	this->HandleNodeProperty->Delete();
  if ( this->SelectedHandleProperty )
  	this->SelectedHandleProperty->Delete();
  if ( this->LineProperty )
  	this->LineProperty->Delete();
  if ( this->SelectedLineProperty )
  	this->SelectedLineProperty->Delete();
  if ( this->MainStraightModelProperty )
  	this->MainStraightModelProperty->Delete();
  if ( this->SubTreeHandleProperty )
  	this->SubTreeHandleProperty->Delete();

  this->HandleHeartProperty = NULL;
  this->HandleTerminalProperty = NULL;
  this->HandleNodeProperty = NULL;
  this->SelectedHandleProperty = NULL;
  this->LineProperty = NULL;
  this->SelectedLineProperty = NULL;
  this->MainStraightModelProperty = NULL;
  this->SubTreeHandleProperty = NULL;
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::PlaceWidget(double bds[6])
{
	if( this->StraightModelSource )
		{
		if( this->StraightModelSource->GetFileName() )
			{
		  double pt1[3], pt2[3];

		  this->GridLine->GetPoint(0, pt1);
		  this->GridLine->GetPoint(this->GridLine->GetNumberOfPoints()-1, pt2);

		  double bounds[6];
		  bounds[0] = pt1[0];
		  bounds[1] = pt1[1];
		  bounds[2] = pt1[2];
		  bounds[3] = pt2[0];
		  bounds[4] = pt2[1];
		  bounds[5] = pt2[2];

		//  this->AdjustBounds(bds, bounds, center);

		  for (int i=0; i<6; i++)
		    {
		    this->InitialBounds[i] = bounds[i];
		    }
		  this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
		                             (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
		                             (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

			}
		}
	else 	if( this->HeMoLabReader)
		{
		if( this->HeMoLabReader->GetFileName() )
			{
		  double pt1[3], pt2[3];

		  this->GridLine->GetPoint(0, pt1);
		  this->GridLine->GetPoint(this->GridLine->GetNumberOfPoints()-1, pt2);

		  double bounds[6];
		  bounds[0] = pt1[0];
		  bounds[1] = pt1[1];
		  bounds[2] = pt1[2];
		  bounds[3] = pt2[0];
		  bounds[4] = pt2[1];
		  bounds[5] = pt2[2];

		//  this->AdjustBounds(bds, bounds, center);

		  for (int i=0; i<6; i++)
		    {
		    this->InitialBounds[i] = bounds[i];
		    }
		  this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
		                             (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
		                             (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

			}
		}
	else if(this->StraightModelFilter)
		{
			double pt1[3], pt2[3];
		  double bounds[6];

		  this->GridLine->GetPoint(0, pt1);
		  this->GridLine->GetPoint(this->GridLine->GetNumberOfPoints()-1, pt2);

		  bounds[0] = pt1[0];
		  bounds[1] = pt1[1];
		  bounds[2] = pt1[2];
		  bounds[3] = pt2[0];
		  bounds[4] = pt2[1];
		  bounds[5] = pt2[2];

		//  this->AdjustBounds(bds, bounds, center);

		  for (int i=0; i<6; i++)
		    {
		    this->InitialBounds[i] = bounds[i];
		    }
		  this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
		                             (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
		                             (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

		}
  else if(this->StraightModelKeepRemoveFilter)
    {
      double pt1[3], pt2[3];
      double bounds[6];

      this->GridLine->GetPoint(0, pt1);
      this->GridLine->GetPoint(this->GridLine->GetNumberOfPoints()-1, pt2);

      bounds[0] = pt1[0];
      bounds[1] = pt1[1];
      bounds[2] = pt1[2];
      bounds[3] = pt2[0];
      bounds[4] = pt2[1];
      bounds[5] = pt2[2];

    //  this->AdjustBounds(bds, bounds, center);

      for (int i=0; i<6; i++)
        {
        this->InitialBounds[i] = bounds[i];
        }
      this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
                                 (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
                                 (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

    }
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::SetPoints(double x[3])
{
	double p1[3];
	double v[3];
	int id;
	int numberOfElements;

	v[0] = x[0] - this->LastPosition[0];
  v[1] = x[1] - this->LastPosition[1];
  v[2] = x[2] - this->LastPosition[2];

	//Verifica se o elemento clicado do straightModel é um terminal
	if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))
		{
		vtkHM1DTerminal* term = (vtkHM1DTerminal*)this->StraightModel->GetTerminal(this->StraightModelElementSelected);
		vtkHM1DTreeElement* parent = term->GetFirstParent();
		vtkHM1DTreeElement *child = NULL;
		vtkHM1DTreeElement *elem = NULL;
		vtkHM1DSegment* segm = NULL;

		this->SphereSourceSelected->GetCenter(p1);
		//modifica o ponto
		p1[0] += v[0];
		p1[1] += v[1];
		p1[2] += v[2];
		this->SphereSourceSelected->SetCenter(p1);

		//Se parent existe, o elemento clicado é um terminal comum,
		//senão é o coração.
		if ( parent )
			{
			while ( parent )
				{
				this->MoveParent(v, parent->GetId());
				parent = term->GetNextParent();
				}
			elem = term;
			child = term->GetFirstChild();
			}
		else
			{
			//Pega ponto do terminal clicado
			vtkPoints *points = this->GridLine->GetPoints();
			points->GetPoint(this->PointSelectedId, p1);

			//modifica o ponto
			p1[0] += v[0];
			p1[1] += v[1];
			p1[2] += v[2];

			//Seta o ponto
			points->SetPoint(this->PointSelectedId, p1);

			vtkPoints *pointsTerminal = this->GridTerminal->GetPoints();

			vtkIdType *info = this->GridTerminal->GetTerminalInformation(this->StraightModelElementSelected);
			//Seta o ponto
			pointsTerminal->SetPoint(info[0], p1);

			if ( this->StraightModelObjectSelected == HEART )
				this->HeartGeometry->SetCenter(p1);

			elem = term;
			child = term->GetFirstChild();
			}

		while ( child )
			{
			id = child->GetId();

			//Se filho for igual a terminal, pego o próximo para movimenta-lo
			if ( this->StraightModel->IsTerminal(id) )
				{
				child = elem->GetNextChild();
				}
			else
				{
				segm = (vtkHM1DSegment*)child;

				this->MoveChild(v, id);
				child = elem->GetNextChild();
				} // Fim else

			} // Fim While(child)

		} //Fim if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))

	else if ( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT) ||
						(this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
		{
		vtkHM1DTreeElement *child = NULL;
		vtkHM1DTreeElement *elem = NULL;
		vtkHM1DSegment* segm = (vtkHM1DSegment*)this->StraightModel->GetSegment(this->SegmentSelected);
		vtkHM1DTreeElement* parent = segm->GetFirstParent();
		vtkHM1DSegment *segmParent; //Segmento pai

		//Verifica se o parent é diferente de terminal, caso seja,
		//movimento o segmento pai junto com o segmento que esta
		//sendo deslocado
		if ( this->StraightModel->IsSegment(parent->GetId()) )
			{
			elem = segm;
			segmParent = (vtkHM1DSegment*)parent;
		 	numberOfElements = segmParent->GetElementNumber();

		 	id = parent->GetId();

		 	this->MoveParent(v, id);
		 	//Movimentar os elementos filhos do segmento selecionado
		 	child = segm->GetFirstChild();
			while ( child )
				{
				this->MoveChild(v, child->GetId());
				child = segm->GetNextChild();
				}

			} // Fim if ( parent )

		//Se pai é um terminal, os
		//outros segmentos filhos também devem ser movimentados,
		else if ( this->StraightModel->IsTerminal(parent->GetId()) )
			{
			elem = segm;
			segmParent = (vtkHM1DSegment*)parent->GetFirstParent();

		 	if ( segmParent )
			 	{
			 	while ( segmParent )
				 	{
				 	id = segmParent->GetId();
				 	this->MoveParent(v, id);
				 	segmParent = (vtkHM1DSegment*)parent->GetNextParent();
				 	}
			 	}
			//Movimentar filhos do terminal pai
			child = parent->GetFirstChild();
			while ( child )
				{
				if ( child->GetId() != segm->GetId() )
					this->MoveChild(v, child->GetId());
				child = parent->GetNextChild();
				}


			//Encontrar o terminal filho do segmento selecionado
		 	elem = segm->GetFirstChild();
		 	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		 		elem = segm->GetNextChild();

		 	//Movimentar os elementos parents do terminal filho do segmento selecionado
		 	segmParent = (vtkHM1DSegment*)elem->GetFirstParent();
		 	while ( segmParent )
			 	{
			 	id = segmParent->GetId();
			 	if ( segmParent->GetId() != segm->GetId() )
			 		this->MoveParent(v, id);
			 	segmParent = (vtkHM1DSegment*)elem->GetNextParent();
			 	}

		 	//Movimentar os elementos filhos do terminal filho do segmento selecionado
		 	child = elem->GetFirstChild();
			while ( child )
				{
				this->MoveChild(v, child->GetId());
				child = elem->GetNextChild();
				}

			} // Fim else if ( this->StraightModel->IsTerminal(parent->GetId()) )
		} // Fim else if ( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT) ||
			//(this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
	 //Elemento clicado é um node
	else //if ( this->NodeSelected > -1)
		{
		//Pega ponto do node clicado
		vtkPoints *pointsNode = this->GridNode->GetPoints();
		vtkPoints *pointsLine = this->GridLine->GetPoints();
		pointsNode->GetPoint(this->PointSelectedId, p1);

		//modifica o ponto
		p1[0] += v[0];
		p1[1] += v[1];
		p1[2] += v[2];
		this->SphereSourceSelected->SetCenter(p1);
		//Seta o ponto
		pointsNode->SetPoint(this->PointSelectedId, p1);
		pointsLine->SetPoint(this->PointSelectedId, p1);
		this->Points->SetPoint(this->PointSelectedId, p1);

		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[0] = p1[0];
		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[1] = p1[1];
		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[2] = p1[2];

		}

	// remember last position
  this->LastPosition[0] = x[0];
  this->LastPosition[1] = x[1];
  this->LastPosition[2] = x[2];
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::MoveParent(double v[3], int id)
{
	int numberOfElements;
	double p1[3], p2[3];
	double move[3];

	vtkIdType *p;
	vtkIdList *elementList;

	vtkPoints *pointsLine = this->GridLine->GetPoints();
	vtkPoints *pointsNode = this->GridNode->GetPoints();


	if ( this->StraightModel->IsSegment(id) )
		{
		vtkHM1DSegment *segm = this->StraightModel->GetSegment(id);
		numberOfElements = segm->GetElementNumber();

	 	elementList = this->GridLine->GetSegmentInformation(id);

		//pega o movimento realizado e divide pelo numero de elementos do segmento
	 	//para calcular quanto cada elemento e node do segmento vai ser deslocado
		move[0] = v[0] / numberOfElements;
	 	move[1] = v[1] / numberOfElements;
	 	move[2] = v[2] / numberOfElements;

	 	//Calcula o deslocamento para os elementos e nodes intermediarios ao segmento
	 	for ( int j=1; j<=numberOfElements; j++ )
			{
			//Pega a coordenada do node do segmento
			p2[0] = segm->GetNodeData(j)->coords[0];
			p2[1] = segm->GetNodeData(j)->coords[1];
			p2[2] = segm->GetNodeData(j)->coords[2];

			for (int i=0; i<3; i++)
		    p1[i] = p2[i] + (move[i] * (j));

		  //Seta a coordenada do node do segmento
			segm->GetNodeData(j)->coords[0] = p1[0];
			segm->GetNodeData(j)->coords[1] = p1[1];
			segm->GetNodeData(j)->coords[2] = p1[2];

		  p = this->GridLine->GetSegment(elementList->GetId(j-1));

		  //Seta os pontos
		  pointsLine->SetPoint(p[1], p1);
		 	pointsNode->SetPoint(p[1], p1);
		  this->Points->SetPoint(p[1], p1);
			}

		///////////////////////////////////////////////////////////
	 	//Seta o ponto do terminal filho
	 	vtkPoints *pointsTerminal = this->GridTerminal->GetPoints();
		p = this->GridTerminal->GetTerminalInformation(segm->GetFirstChild()->GetId());

		if ( p )
			pointsTerminal->SetPoint(p[0], p1);

		}  // Fim if ( this->StraightModel->IsSegment(id) )
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::MoveChild(double v[3], int id)
{
	int numberOfElements;
	double p1[3], p2[3];
	double move[3];

	vtkIdType *p;
	vtkIdList *elementList;

	vtkPoints *pointsLine = this->GridLine->GetPoints();
	vtkPoints *pointsNode = this->GridNode->GetPoints();

	if ( this->StraightModel->IsSegment(id) )
		{
		vtkHM1DSegment *segm = this->StraightModel->GetSegment(id);
		numberOfElements = segm->GetElementNumber();

	 	elementList = this->GridLine->GetSegmentInformation(id);

		//pega o movimento realizado e divide pelo numero de elementos do segmento
 		//para calcular quanto cada elemento e node do segmento vai ser deslocado
	 	move[0] = v[0] / numberOfElements;
	 	move[1] = v[1] / numberOfElements;
	 	move[2] = v[2] / numberOfElements;

	 	//Calcula o deslocamento para os elementos e nodes intermediarios ao segmento
	 	for ( int j=numberOfElements; j>0; j-- )
			{
		  p = this->GridLine->GetSegment(elementList->GetId(numberOfElements - j));

			//Pega a coordenada do node do segmento
			p2[0] = segm->GetNodeData(p[2])->coords[0];
			p2[1] = segm->GetNodeData(p[2])->coords[1];
			p2[2] = segm->GetNodeData(p[2])->coords[2];

			for (int i=0; i<3; i++)
		    p1[i] = p2[i] + (move[i] * (j));

		  //Seta a coordenada do node do segmento
			segm->GetNodeData(p[2])->coords[0] = p1[0];
			segm->GetNodeData(p[2])->coords[1] = p1[1];
			segm->GetNodeData(p[2])->coords[2] = p1[2];

		  //Seta os pontos
		  pointsLine->SetPoint(p[0], p1);
		 	pointsNode->SetPoint(p[0], p1);
		 	this->Points->SetPoint(p[0], p1);

			}
		}

}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::SetLines(double x[3])
{
  double v[3];

  v[0] = x[0] - this->LastPosition[0];
  v[1] = x[1] - this->LastPosition[1];
  v[2] = x[2] - this->LastPosition[2];

	if(this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL)
		{
		this->MoveSubTree(v);
		this->SetSubTreePoints(x);
		}
	else
		{
		this->MoveSegment(v, this->SegmentSelected);
		this->SetPoints(x);
		}
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::MoveSegment(double x[3], vtkIdType idSegment)
{
	double p1[3], p2[3], v[3];
	int numberOfElements;

	vtkPoints *pointsTerminal = this->GridTerminal->GetPoints();
	vtkPoints *pointsLine = this->GridLine->GetPoints();
	vtkPoints *pointsNode = this->GridNode->GetPoints();

	vtkIdType *p;
	vtkIdList *elementList;

	vtkHM1DSegment *segm = this->StraightModel->GetSegment(idSegment);
	numberOfElements = segm->GetElementNumber();

	vtkHM1DTreeElement *parent = segm->GetFirstParent();

	//Se pai do segmento for um terminal, o segmento possui um terminal a mais relacionado a ele.
	if ( this->StraightModel->IsTerminal(parent->GetId()) )
		{
		if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry)
			{
			//Pega informacao do ponto do terminal
			p = this->GridTerminal->GetTerminalInformation(parent->GetId());

			//Pega a coordenada
			pointsTerminal->GetPoint(p[0], p1);

			//Soma ao ponto o movimento
			p1[0] += x[0];
			p1[1] += x[1];
			p1[2] += x[2];

			//Seta o ponto
			pointsTerminal->SetPoint(p[0], p1);

			//Caso o parent seja o coracao devo setar a coordenada da esfera
			if ( parent->GetId() == 1 )
				this->HeartGeometry->SetCenter(p1);

			}
		else if ( parent->GetChildCount() <= 1 )
			{
			//Pega informacao do ponto do terminal
			p = this->GridTerminal->GetTerminalInformation(parent->GetId());

			//Pega a coordenada
			pointsTerminal->GetPoint(p[0], p1);

			//Soma ao ponto o movimento
			p1[0] += x[0];
			p1[1] += x[1];
			p1[2] += x[2];

			//Seta o ponto
			pointsTerminal->SetPoint(p[0], p1);

			//Caso o parent seja o coracao devo setar a coordenada da esfera
			if ( parent->GetId() == 1 )
				this->HeartGeometry->SetCenter(p1);
			}
		}

	elementList = this->GridLine->GetSegmentInformation(idSegment);

	//Calcula o deslocamento para os elementos e nodes intermediarios ao segmento
 	for ( int j=0; j<numberOfElements; j++ )
		{
		//Pega a coordenada do node do segmento
		p2[0] = segm->GetNodeData(j)->coords[0];
		p2[1] = segm->GetNodeData(j)->coords[1];
		p2[2] = segm->GetNodeData(j)->coords[2];

		for (int i=0; i<3; i++)
	    p2[i] += x[i];

	  //Seta a coordenada do node do segmento
		segm->GetNodeData(j)->coords[0] = p2[0];
		segm->GetNodeData(j)->coords[1] = p2[1];
		segm->GetNodeData(j)->coords[2] = p2[2];

	  p = this->GridLine->GetSegment(elementList->GetId(j));

	  //Seta os pontos
	  pointsLine->SetPoint(p[0], p2);
	 	pointsNode->SetPoint(p[0], p2);
	 	this->Points->SetPoint(p[0], p2);

		}

	//////////////////////////////////////////////////////////////
	//Seta o ponto do ultimo node do segmento

	p = this->GridLine->GetSegment(elementList->GetId(numberOfElements-1));
	//Pega a coordenada do node do segmento
	p2[0] = segm->GetNodeData(numberOfElements)->coords[0];
	p2[1] = segm->GetNodeData(numberOfElements)->coords[1];
	p2[2] = segm->GetNodeData(numberOfElements)->coords[2];

	for (int i=0; i<3; i++)
    p2[i] += x[i];

  //Seta a coordenada do node do segmento
	segm->GetNodeData(numberOfElements)->coords[0] = p2[0];
	segm->GetNodeData(numberOfElements)->coords[1] = p2[1];
	segm->GetNodeData(numberOfElements)->coords[2] = p2[2];

  //Seta os pontos
  pointsLine->SetPoint(p[1], p2);
 	pointsNode->SetPoint(p[1], p2);
 	this->Points->SetPoint(p[1], p2);

 	///////////////////////////////////////////////////////////
 	//Seta o ponto do terminal filho
	p = this->GridTerminal->GetTerminalInformation(segm->GetFirstChild()->GetId());

	//Pega o ponto
	pointsTerminal->GetPoint(p[0], p1);

	//Soma ao ponto o movimento
	p1[0] += x[0];
	p1[1] += x[1];
	p1[2] += x[2];

	//Seta o ponto
	pointsTerminal->SetPoint(p[0], p1);
}

//--------------------------------------------------------------------------------------------------------
//Move o segmento com clip. Esse movimento é diferente porque é preciso movimentar a primeira e a segunda
//parte do segmento que foi dividido em dois e junto com ele os segmentos pais e filhos
//
// direção do fluxo -->
//
//      1º segmento		  clip		 2º segmento
//  t__________________t| |t____________________t
//
// t = terminal
//o segmento é dividido em dois e é adionado terminais para separa-los
//quando algum desses segmento ou o clip for selecionado, ambos devem ser movimentados e seus respectivos
//pais e filhos
void vtkHMStraightModelWidget::MoveSegmentWithClip(double x[3], vtkIdType position)
{
	double p[3];
	double v[3];

	//calcula o movimento realizado
	v[0] = x[0] - this->LastPosition[0];
  v[1] = x[1] - this->LastPosition[1];
  v[2] = x[2] - this->LastPosition[2];

  //pega a coordenada do clip
  this->GridClip->GetPoints()->GetPoint(position, p);

  p[0] += v[0];
  p[1] += v[1];
  p[2] += v[2];
  //seta a coordenada do clip
  this->GridClip->GetPoints()->SetPoint(position, p);

	double *tuple;

	//pega os ids dos segmentos associados com o clip
	tuple = this->ClipSegments->GetTuple(position);

	//movimenta os dois segmentos juntos do clip
	this->MoveSegment(v, (int)tuple[0]);
	this->MoveSegment(v, (int)tuple[1]);

	//pega o terminal pai do primeiro segmento
	vtkHM1DTreeElement *parent = this->StraightModel->GetSegment((int)tuple[0])->GetFirstParent();
	//pega o terminal filho do segundo segmento
	vtkHM1DTreeElement *child = this->StraightModel->GetSegment((int)tuple[1])->GetFirstChild();

	vtkHM1DTreeElement *elem = parent->GetFirstParent();

	//move todos os segmentos pais do primeiro segmento do clip
	while ( elem )
		{
		this->MoveParent(v, elem->GetId());
		elem = parent->GetNextParent();
		}

	//pego os filhos do terminal pai para movimenta-los
	elem = parent->GetFirstChild();
	while ( elem )
		{
		if ( elem->GetId() != (int)tuple[0] ) //se o segmento for diferente do segmento associado ao clip
			this->MoveChild(v, elem->GetId());
		elem = parent->GetNextChild();
		}

	//pega os filhos do terminal filho do segundo segmento do clip
	elem = child->GetFirstChild();
	while ( elem )
		{
		this->MoveChild(v, elem->GetId());
		elem = child->GetNextChild();
		}

	//movimenta os pais do terminal filho do segundo segmento
	elem = child->GetFirstParent();
	while ( elem )
		{
		if ( elem->GetId() != (int)tuple[1] ) //se o segmento for diferente do segmento associado ao clip
			this->MoveParent(v, elem->GetId());
		elem = child->GetNextParent();
		}
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::SetLinePosition(double x[3])
{
  double v[3];
  vtkIdType position;

  // vector of motion
  v[0] = x[0] - this->LastPosition[0];
  v[1] = x[1] - this->LastPosition[1];
  v[2] = x[2] - this->LastPosition[2];

  if ( this->StraightModelObjectSelected == SEGMENT )
  	{
	  position = this->FindClipIds(this->SegmentSelected);

	  //se modo de edição estiver selecionado
	  if ( position == -1 )
	  	{
			if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
				this->SetLines(x);
			else if ( this->EditionModeSelected == vtkHMStraightModelWidget::Move )
				{
				if(this->GetStraightModelMode() != MODE_TODETACHSUBTREE_VAL)
					this->MoveSegment(v, this->SegmentSelected);
				else
					this->MoveSubTree(v);
				}
	  	}
	  else
	  	{
	  	if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
	  		{
	  		this->MoveSegmentWithClip(x, position);
	  		}
	  	}
  	}
  else if ( this->StraightModelObjectSelected == CLIP )
  	{
  	position = this->StraightModelElementSelected;
  	this->MoveSegmentWithClip(x, position);
  	}
  else if ( this->StraightModelObjectSelected == STENT )
  	{
  	position = this->FindClipIds(this->SegmentSelected);

	  //se modo de edição estiver selecionado
	  if ( position == -1 )
	  	{
	  	this->SetLines(x);
	  	}
	  else
	  	{
	  	if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
	  		{
	  		this->MoveSegmentWithClip(x, position);
	  		}
	  	}
  	}
  else if ( this->StraightModelObjectSelected == STENOSIS )
  	{
  	position = this->FindClipIds(this->SegmentSelected);

	  //se modo de edição estiver selecionado
	  if ( position == -1 )
	  	{
	  	this->SetLines(x);
	  	}
	  else
	  	{
	  	if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
	  		{
	  		this->MoveSegmentWithClip(x, position);
	  		}
	  	}
  	}
  else if ( this->StraightModelObjectSelected == ANEURYSM )
  	{
  	position = this->FindClipIds(this->SegmentSelected);

	  //se modo de edição estiver selecionado
	  if ( position == -1 )
	  	{
	  	this->SetLines(x);
	  	}
	  else
	  	{
	  	if ( this->EditionModeSelected == vtkHMStraightModelWidget::EditGeometry )
	  		{
	  		this->MoveSegmentWithClip(x, position);
	  		}
	  	}
  	}

  // remember last position
  this->LastPosition[0] = x[0];
  this->LastPosition[1] = x[1];
  this->LastPosition[2] = x[2];

}

//------------------------------------------------------------------------
//Procura se o segment está relacionado com algum
//clip e retorna a posição no array, caso não encontre retorna -1
vtkIdType vtkHMStraightModelWidget::FindClipIds(vtkIdType idSegment)
{
	double *tuple;
	for ( int i=0; i<this->ClipSegments->GetNumberOfTuples(); i++ )
		{
		tuple = this->ClipSegments->GetTuple(i);
		if ( (tuple[0] == idSegment) || (tuple[1] == idSegment) )
			return i;
		}
	return -1;
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::ClampPosition(double x[3])
{
  for (int i=0; i<3; i++)
    {
    if ( x[i] < this->InitialBounds[2*i] )
      {
      x[i] = this->InitialBounds[2*i];
      }
    if ( x[i] > this->InitialBounds[2*i+1] )
      {
      x[i] = this->InitialBounds[2*i+1];
      }
    }
}

int vtkHMStraightModelWidget::InBounds(double x[3])
{
  for (int i=0; i<3; i++)
    {
    if ( x[i] < this->InitialBounds[2*i] ||
         x[i] > this->InitialBounds[2*i+1] )
      {
      return 0;
      }
    }
  return 1;
}

//----------------------------------------------------------------------------
// Cria o StraightModelReader segundo o caminha dado
int vtkHMStraightModelWidget::UpdateStraightModelReader()
{
	if ( this->StraightModelSource )
		{
		// Setando o StraightModel desta classe
		this->StraightModel = this->StraightModelSource->GetOutput()->GetStraightModel();

		this->GridLine = this->StraightModelSource->GetOutput();
		}
	else if( this->StraightModelFilter )
		{
		// Setando o StraightModel desta classe
		this->GridLine = vtkHM1DStraightModelGrid::SafeDownCast(this->StraightModelFilter->GetOutput(0));

		this->StraightModel = this->GridLine->GetStraightModel();

		}
  else if( this->StraightModelKeepRemoveFilter )
    {
    // Setando o StraightModel desta classe
    this->GridLine = vtkHM1DStraightModelGrid::SafeDownCast(this->StraightModelKeepRemoveFilter->GetInput());

    this->StraightModel = this->GridLine->GetStraightModel();

    }
	else
		{
		// Setando o StraightModel desta classe
		this->StraightModel = this->HeMoLabReader->GetOutput()->GetStraightModel();

		this->GridLine = this->HeMoLabReader->GetOutput();

		int numberOfClips = this->StraightModel->GetNumberOfClips();
		//inserir clips
		for ( int i=0; i<numberOfClips; i++ )
			{
			vtkIdType ids[2];
			vtkIdType *info;
			this->StraightModel->GetClip(i, ids);

			this->ClipSegments->InsertNextTuple2(ids[0], ids[1]);

			info = this->GridLine->GetTerminalInformation(this->StraightModel->GetSegment(ids[0])->GetFirstChild()->GetId());

			double x[3];
			this->GridLine->GetPoint(info[0], x);

			vtkPoints *points = this->GridClip->GetPoints();

			vtkIdType idPt = points->InsertNextPoint(x);
			this->GridClip->SetPoints(points);

			vtkCellArray *verts = this->GridClip->GetVerts();

			verts->InsertNextCell(1, &idPt);
			this->GridClip->SetVerts(verts);
			}

		//inserir stent
		for ( int i=0; i<this->HeMoLabReader->GetNumberOfStent(); i++ )
			{
			vtkIdList *l = this->HeMoLabReader->GetStent(i);
			vtkIdType idSegment = l->GetId(l->GetNumberOfIds()-1);
			vtkIdList *list = vtkIdList::New();

			for ( int j=0; j<l->GetNumberOfIds()-1; j++ )
				list->InsertNextId(l->GetId(j));

			this->AddStent(idSegment, list);
			l->Delete();
			}

		//inserir stenosis
		for ( int i=0; i<this->HeMoLabReader->GetNumberOfStenosis(); i++ )
			{
			vtkIdList *l = this->HeMoLabReader->GetStenosis(i);
			vtkIdType idSegment = l->GetId(l->GetNumberOfIds()-1);
			vtkIdList *list = vtkIdList::New();

			for ( int j=0; j<l->GetNumberOfIds()-2; j++ )
				list->InsertNextId(l->GetId(j));

			this->AddStenosis(idSegment, list);
			l->Delete();
			}

		//inserir aneurysm
		for ( int i=0; i<this->HeMoLabReader->GetNumberOfAneurysm(); i++ )
			{
			vtkIdList *l = this->HeMoLabReader->GetAneurysm(i);
			vtkIdType idSegment = l->GetId(l->GetNumberOfIds()-1);
			vtkIdList *list = vtkIdList::New();

			for ( int j=0; j<l->GetNumberOfIds()-2; j++ )
				list->InsertNextId(l->GetId(j));

			this->AddAneurysm(idSegment, list);
			l->Delete();
			}
		} //Fim else

	if ( !this->GridLine->GetPoints() )
		return 0;

	this->Points = vtkPoints::New();
	this->UpdateGrid();

	this->GridLineMapper->SetInput(this->GridLine);

 	//Ator
 	this->GridLineActor->SetMapper(this->GridLineMapper);

 	this->GridNodeMapper->SetInput(this->GridNode);
 	//Ator
 	this->GridNodeActor->SetMapper(this->GridNodeMapper);

 	double *bds = this->GridLine->GetBounds();

	this->SizePerAxle[0] = sqrt(pow(bds[1]-bds[0],2));
	this->SizePerAxle[1] = sqrt(pow(bds[3]-bds[2],2));
	this->SizePerAxle[2] = sqrt(pow(bds[5]-bds[4],2));

	return 1;
}

//--------------------------------------------------------------------
int vtkHMStraightModelWidget::SetSourceCSID(int csId)
{
	this->csId.ID = csId;
	vtkProcessModule* pm = vtkProcessModule::SafeDownCast(vtkProcessModule::GetProcessModule());

	if (vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(this->csId)))
		{
		this->StraightModelSource = vtkHM1DStraightModelReader::SafeDownCast(pm->GetObjectFromID(this->csId));

		if ( !this->UpdateStraightModelReader() )
			return 0;
		this->GenerateRepresentation();
		this->SetEnabled(1);

		}
	else if (vtkHMUnstructuredGridTo1DModel::SafeDownCast(pm->GetObjectFromID(this->csId)))
		{
		this->StraightModelFilter = vtkHMUnstructuredGridTo1DModel::SafeDownCast(pm->GetObjectFromID(this->csId));

		if ( !this->UpdateStraightModelReader() )
			return 0;
		this->GenerateRepresentation();
		this->SetEnabled(1);
		}
  else if (vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(this->csId)))
    {
    this->StraightModelKeepRemoveFilter = vtkHM1DKeepRemoveFilter::SafeDownCast(pm->GetObjectFromID(this->csId));

    if ( !this->UpdateStraightModelReader() )
      return 0;
    this->GenerateRepresentation();
    this->SetEnabled(1);
    }
	else 	if (vtkHM1DHeMoLabReader::SafeDownCast(pm->GetObjectFromID(this->csId)))
		{
		this->HeMoLabReader = vtkHM1DHeMoLabReader::SafeDownCast(pm->GetObjectFromID(this->csId));

		if ( !this->UpdateStraightModelReader() )
			return 0;
		this->GenerateRepresentation();
		this->SetEnabled(1);

		}
//	this->ValidPick = 1;
//	this->SizeHandles();
	return 1;
}

//----------------------------------------------------------------------------
// Percorrer o StraightModel para compor todas as linhas e terminais que o representam
void vtkHMStraightModelWidget::GenerateRepresentation()
{
	// No início não há nenhum elemento selecionado
  this->SegmentSelected	= this->TerminalSelected = this->NodeSelected = this->ElementSelected = -1;
	this->LineWidgetSelected 		=	this->SphereWidgetSelected 	= -1;

	// Atualmente nenhum objeto do StraightModel está selecionado
	this->StraightModelObjectSelected = -1;
	this->StraightModelElementSelected = this->LineSelectedId =	this->PointSelectedId = 0;
	this->CurrentLineActor = this->CurrentHandleActor = 0;
	this->CurrentTerminal = this->CurrentSegment = 	this->CurrentNode	= 0;

	this->State = vtkHMStraightModelWidget::Start;
  this->EventCallbackCommand->SetCallback(vtkHMStraightModelWidget::ProcessEvents);
  this->Align = vtkHMStraightModelWidget::XAxis;

  vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
	camera->ParallelProjectionOn();

  //Configuracao da linha
  //Mapper
  this->SphereMapperSelected->SetInput(this->SphereSourceSelected->GetOutput());
  //Actor
  this->SphereActorSelected->SetMapper(this->SphereMapperSelected);
	this->SphereActorSelected->SetProperty(this->SelectedHandleProperty);

	this->BeginSphereMapperSelected->SetInput(this->BeginSphereSourceSelected->GetOutput());
  //Actor
  this->BeginSphereActorSelected->SetMapper(this->BeginSphereMapperSelected);
	this->BeginSphereActorSelected->GetProperty()->SetColor(0,0,1);

	this->EndSphereMapperSelected->SetInput(this->EndSphereSourceSelected->GetOutput());
  //Actor
  this->EndSphereActorSelected->SetMapper(this->EndSphereMapperSelected);
	this->EndSphereActorSelected->GetProperty()->SetColor(0,1,0);

	int rootId = 1;
	// Set up the initial properties
  this->CreateDefaultProperties();

	this->GenerateRepresentationOfHeart();
	this->BuildHeartRepresentation();

	  // Define the point coordinates
  double bounds[6];
  bounds[0] = -0.5;
  bounds[1] = 0.5;
  bounds[2] = -0.5;
  bounds[3] = 0.5;
  bounds[4] = -0.5;
  bounds[5] = 0.5;
  this->PlaceFactor = 1.0; //overload parent's value

  // Initial creation of the widget, serves to initialize it
  this->PlaceWidget(bounds);
  this->ClampToBounds = 0;

	// Gerando os Pickers
	this->GenerateHandlePicker();
	this->GenerateLinePicker();
  this->CurrentHandle = NULL;

  // Gerando os point widgets
  this->GeneratePointWidget();

  // Gerando e configurando as callbacks
  this->GenerateCallbacks();

  this->CurrentPointWidget = NULL;
}


//----------------------------------------------------------------------------
// destruir toda a representação
void vtkHMStraightModelWidget::DestructorRepresentation()
{
	if ( this->HeartActor )
		this->HeartActor->Delete();
	if ( this->HeartMapper )
		this->HeartMapper->Delete();
	if ( this->HeartGeometry )
		this->HeartGeometry->Delete();

	this->HeartActor = NULL;
	this->HeartMapper = NULL;
	this->HeartGeometry = NULL;

	// Destruindo todas representações de segmentos
	this->DestructorLinePicker();

	// Destruindo todas representações de terminais
	this->DestructorHandlePicker();

	this->DestructorDefaultProperties();

	this->DestructorCallbacks();
}

//----------------------------------------------------------------------------
// Gerar o widget para o Coração
void vtkHMStraightModelWidget::GenerateRepresentationOfHeart()
{
	this->HeartGeometry = vtkSphereSource::New();
  this->HeartMapper = vtkPolyDataMapper::New();
  this->HeartActor = vtkActor::New();

  this->HeartGeometry->SetThetaResolution(8);
  this->HeartGeometry->SetPhiResolution(8);

  //Mapper
  this->HeartMapper->SetInput(this->HeartGeometry->GetOutput());

  //Actor
  this->HeartActor->SetMapper(this->HeartMapper);

	this->HeartActor->SetProperty(this->HandleHeartProperty);

}


//----------------------------------------------------------------------------
// Gerar a representação do coração
void vtkHMStraightModelWidget::BuildHeartRepresentation()
{
	double pt[3];
	this->GridLine->GetPoints()->GetPoint(0, pt);

	this->HeartGeometry->SetCenter(pt);
}

//----------------------------------------------------------------------------
// Gerar os Handles Pickers
void vtkHMStraightModelWidget::GenerateHandlePicker()
{
	this->NodePicker = NULL;
	this->TerminalPicker = vtkCellPicker::New();
	this->TerminalPicker->SetTolerance(0.004);
	this->TerminalPicker->AddPickList(this->GridTerminalActor);
	this->TerminalPicker->PickFromListOn();
}

//----------------------------------------------------------------------------
// Destruir os Handles Pickers
void vtkHMStraightModelWidget::DestructorHandlePicker()
{
  if ( this->NodePicker )
  	this->NodePicker->Delete();
  this->NodePicker = NULL;

	if ( this->TerminalPicker )
		this->TerminalPicker->Delete();
	this->TerminalPicker = NULL;
}


//----------------------------------------------------------------------------
// Gerar os Lines Pickers
void vtkHMStraightModelWidget::GenerateLinePicker()
{
	this->LinePicker = vtkCellPicker::New();
  this->LinePicker->SetTolerance(0.003);
  this->LinePicker->AddPickList(this->GridLineActor);
  this->LinePicker->PickFromListOn();

  this->ClipPicker = vtkCellPicker::New();
  this->ClipPicker->SetTolerance(0.005);
  this->ClipPicker->AddPickList(this->GridClipActor);
  this->ClipPicker->PickFromListOn();

  this->StentPicker = vtkCellPicker::New();
  this->StentPicker->SetTolerance(0.005);
  this->StentPicker->AddPickList(this->GridStentActor);
  this->StentPicker->PickFromListOn();

  this->StenosisPicker = vtkCellPicker::New();
  this->StenosisPicker->SetTolerance(0.005);
  this->StenosisPicker->AddPickList(this->GridStenosisActor);
  this->StenosisPicker->PickFromListOn();

  this->AneurysmPicker = vtkCellPicker::New();
  this->AneurysmPicker->SetTolerance(0.005);
  this->AneurysmPicker->AddPickList(this->GridAneurysmActor);
  this->AneurysmPicker->PickFromListOn();
}

//----------------------------------------------------------------------------
// Destruir os Lines Pickers
void vtkHMStraightModelWidget::DestructorLinePicker()
{
	// Destruindo todas representações de segmentos
  if ( this->LinePicker )
  	this->LinePicker->Delete();
  this->LinePicker = NULL;

  if ( this->ClipPicker )
  	this->ClipPicker->Delete();
  this->ClipPicker = NULL;

  if ( this->StentPicker )
  	this->StentPicker->Delete();
  this->StentPicker = NULL;

  if ( this->StenosisPicker )
  	this->StenosisPicker->Delete();
  this->StenosisPicker = NULL;

  if ( this->AneurysmPicker )
  	this->AneurysmPicker->Delete();
  this->AneurysmPicker = NULL;
}

//----------------------------------------------------------------------------
// Gerar os Point Widgets
void vtkHMStraightModelWidget::GeneratePointWidget()
{
	//Point widget for grids
	this->GridLineWidget = vtkHMPointWidget::New();
	this->GridLineWidget->AllOff();
	this->GridLineWidget->SetHotSpotSize(0.5);

  this->GridNodeWidget = vtkHMPointWidget::New();
  this->GridNodeWidget->AllOff();
  this->GridNodeWidget->SetHotSpotSize(0.5);

  this->GridTerminalWidget = vtkHMPointWidget::New();
  this->GridTerminalWidget->AllOff();
  this->GridTerminalWidget->SetHotSpotSize(0.5);

  this->GridClipWidget = vtkHMPointWidget::New();
	this->GridClipWidget->AllOff();
	this->GridClipWidget->SetHotSpotSize(0.5);

	this->GridStentWidget = vtkHMPointWidget::New();
	this->GridStentWidget->AllOff();
	this->GridStentWidget->SetHotSpotSize(0.5);

	this->GridStenosisWidget = vtkHMPointWidget::New();
	this->GridStenosisWidget->AllOff();
	this->GridStenosisWidget->SetHotSpotSize(0.5);

	this->GridAneurysmWidget = vtkHMPointWidget::New();
	this->GridAneurysmWidget->AllOff();
	this->GridAneurysmWidget->SetHotSpotSize(0.5);
}

//----------------------------------------------------------------------------
// Gerar as Callbacks
void vtkHMStraightModelWidget::GenerateCallbacks()
{
	//Callback for grids
  this->GridLineCallback = vtkHMPWCallback::New();
  this->GridLineCallback->hmLineWidget = this;
  this->GridLineCallback->hmPointWidget = this->GridLineWidget;

  this->GridNodeCallback = vtkHMPointWidgetCallback::New();
  this->GridNodeCallback->hmLineWidget = this;
  this->GridNodeCallback->hmPointWidget = this->GridNodeWidget;

  this->GridTerminalCallback = vtkHMPointWidgetCallback::New();
	this->GridTerminalCallback->hmLineWidget = this;
	this->GridTerminalCallback->hmPointWidget = this->GridTerminalWidget;

	this->GridClipCallback = vtkHMPWCallback::New();
  this->GridClipCallback->hmLineWidget = this;
  this->GridClipCallback->hmPointWidget = this->GridClipWidget;

  this->GridStentCallback = vtkHMPWCallback::New();
  this->GridStentCallback->hmLineWidget = this;
  this->GridStentCallback->hmPointWidget = this->GridStentWidget;

  this->GridStenosisCallback = vtkHMPWCallback::New();
  this->GridStenosisCallback->hmLineWidget = this;
  this->GridStenosisCallback->hmPointWidget = this->GridStenosisWidget;

  this->GridAneurysmCallback = vtkHMPWCallback::New();
  this->GridAneurysmCallback->hmLineWidget = this;
  this->GridAneurysmCallback->hmPointWidget = this->GridAneurysmWidget;

	this->GridLineWidget->AddObserver(vtkCommand::InteractionEvent,this->GridLineCallback, 0.0);
	this->GridNodeWidget->AddObserver(vtkCommand::InteractionEvent,this->GridNodeCallback, 0.0);
	this->GridTerminalWidget->AddObserver(vtkCommand::InteractionEvent,this->GridTerminalCallback, 0.0);
	this->GridClipWidget->AddObserver(vtkCommand::InteractionEvent,this->GridClipCallback, 0.0);
	this->GridStentWidget->AddObserver(vtkCommand::InteractionEvent,this->GridStentCallback, 0.0);
	this->GridStenosisWidget->AddObserver(vtkCommand::InteractionEvent,this->GridStenosisCallback, 0.0);
	this->GridAneurysmWidget->AddObserver(vtkCommand::InteractionEvent,this->GridAneurysmCallback, 0.0);
}

void vtkHMStraightModelWidget::DestructorCallbacks()
{
	if ( this->GridLineWidget )
		{
		this->GridLineWidget->RemoveObserver(this->GridLineCallback);
		this->GridLineWidget->Delete();
		}
	this->GridLineWidget = NULL;

	if ( this->GridLineCallback )
		this->GridLineCallback->Delete();
	this->GridLineCallback = NULL;

	if ( this->GridNodeWidget )
		{
		this->GridNodeWidget->RemoveObserver(this->GridNodeCallback);
		this->GridNodeWidget->Delete();
		}
	this->GridNodeWidget = NULL;

	if ( this->GridNodeCallback )
		this->GridNodeCallback->Delete();
	this->GridNodeCallback = NULL;

	if ( this->GridTerminalWidget )
		{
		this->GridTerminalWidget->RemoveObserver(this->GridTerminalCallback);
		this->GridTerminalWidget->Delete();
		}
	this->GridTerminalWidget = NULL;

	if ( this->GridTerminalCallback )
		this->GridTerminalCallback->Delete();
	this->GridTerminalCallback = NULL;

	if ( this->GridClipWidget )
		{
		this->GridClipWidget->RemoveObserver(this->GridClipCallback);
		this->GridClipWidget->Delete();
		}
	this->GridClipWidget = NULL;

	if ( this->GridClipCallback )
		this->GridClipCallback->Delete();
	this->GridClipCallback = NULL;

	if ( this->GridStentWidget )
		{
		this->GridStentWidget->RemoveObserver(this->GridStentCallback);
		this->GridStentWidget->Delete();
		}
	this->GridStentWidget = NULL;

	if ( this->GridStentCallback )
		this->GridStentCallback->Delete();
	this->GridStentCallback = NULL;

	if ( this->GridStenosisWidget )
		{
		this->GridStenosisWidget->RemoveObserver(this->GridStenosisCallback);
		this->GridStenosisWidget->Delete();
		}
	this->GridStenosisWidget = NULL;

	if ( this->GridStenosisCallback )
		this->GridStenosisCallback->Delete();
	this->GridStenosisCallback = NULL;

	if ( this->GridAneurysmWidget )
		{
		this->GridAneurysmWidget->RemoveObserver(this->GridAneurysmCallback);
		this->GridAneurysmWidget->Delete();
		}
	this->GridAneurysmWidget = NULL;

	if ( this->GridAneurysmCallback )
		this->GridAneurysmCallback->Delete();
	this->GridAneurysmCallback = NULL;
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  if ( this->HandleTerminalProperty )
    {
    os << indent << "Handle Property: " << this->HandleTerminalProperty << "\n";
    }
  else
    {
    os << indent << "Handle Property: (none)\n";
    }
  if ( this->SelectedHandleProperty )
    {
    os << indent << "Selected Handle Property: "
       << this->SelectedHandleProperty << "\n";
    }
  else
    {
    os << indent << "Selected Handle Property: (none)\n";
    }

  if ( this->LineProperty )
    {
    os << indent << "Line Property: " << this->LineProperty << "\n";
    }
  else
    {
    os << indent << "Line Property: (none)\n";
    }
  if ( this->SelectedLineProperty )
    {
    os << indent << "Selected Line Property: "
       << this->SelectedLineProperty << "\n";
    }
  else
    {
    os << indent << "Selected Line Property: (none)\n";
    }

  os << indent << "Constrain To Bounds: "
     << (this->ClampToBounds ? "On\n" : "Off\n");

  os << indent << "Align with: ";
  switch ( this->Align )
    {
    case XAxis:
      os << "X Axis";
      break;
    case YAxis:
      os << "Y Axis";
      break;
    case ZAxis:
      os << "Z Axis";
      break;
    default:
      os << "None";
    }

}

void vtkHMStraightModelWidget::SetPlotType(vtkIntArray *PlotArray)
{
	this->PlotType = PlotArray;
}

vtkIntArray *vtkHMStraightModelWidget::GetPlotType(void)
{
	return this->PlotType;
}

//----------------------------------------------------------------------
double vtkHMStraightModelWidget::GetIniFileCurrentTime()
{
  if (!this->GetStraightModelReader()->GetIniFile())
    return -1; // No IniFile found!
  else
  	 return this->GetStraightModelReader()->GetIniFile()->GetCurrentTimeStep();
}

//----------------------------------------------------------------------
double vtkHMStraightModelWidget::GetIniFileDeltaT()
{

  return this->GetStraightModelReader()->GetIniFile()->GetDeltaT();
}

//----------------------------------------------------------------------
void vtkHMStraightModelWidget::SetTreePressure(int pressure, double value)
{
  this->GetStraightModelReader()->GetOutput()->GetStraightModel()->SetTreePressure(pressure, value);
}

//----------------------------------------------------------------------------
double vtkHMStraightModelWidget::GetHeartFinalTime()
{
	vtkHM1DTreeElement *Elem = this->GetStraightModel()->Get1DTreeRoot();
	vtkHM1DTerminal *heart = vtkHM1DTerminal::SafeDownCast(Elem);

	if (heart->GetHeartFinalTime())
		return heart->GetHeartFinalTime();
	else
		return -1;
}

//----------------------------------------------------------------------------
// Method for color tree in function of the property selected
void vtkHMStraightModelWidget::ColorTree()
{
	if ( this->SegmentSelected < 1 )
		return;

	//Pegando os arrays do grid
	vtkDoubleArray *Radius	 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetPointData()->GetArray("Radius"));
	vtkDoubleArray *Elastin 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Elastin"));
	vtkDoubleArray *Collagen 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Collagen"));
	vtkDoubleArray *CoefficientA							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("CoefficientA"));
	vtkDoubleArray *CoefficientB							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("CoefficientB"));
	vtkDoubleArray *Viscoelasticity						= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Viscoelasticity"));
	vtkDoubleArray *ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("ViscoelasticityExponent"));
	vtkDoubleArray *InfiltrationPressure			= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("InfiltrationPressure"));
	vtkDoubleArray *ReferencePressure					= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("ReferencePressure"));
	vtkDoubleArray *Permeability							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Permeability"));

	vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegmentSelected);
	vtkIdType *ptId;

	if ( this->GetSegment_Selected() )
		{
		vtkIdList *list = this->GridLine->GetSegmentInformation(this->SegmentSelected);

		for ( int i=0; i<list->GetNumberOfIds(); i++ )
			{
			ptId = this->GridLine->GetNodeInformation(this->SegmentSelected, i);
			//Trocando os valores das propriedades
			Radius->SetValue(ptId[0], segm->GetNodeData(i)->radius);

			Elastin->SetValue(list->GetId(i), segm->GetElementData(i)->Elastin);
		  Collagen->SetValue(list->GetId(i), segm->GetElementData(i)->Collagen);
		  CoefficientA->SetValue(list->GetId(i), segm->GetElementData(i)->CoefficientA);
		  CoefficientB->SetValue(list->GetId(i), segm->GetElementData(i)->CoefficientB);
		  Viscoelasticity->SetValue(list->GetId(i), segm->GetElementData(i)->Viscoelasticity);
		  ViscoelasticityExponent->SetValue(list->GetId(i), segm->GetElementData(i)->ViscoelasticityExponent);
		  InfiltrationPressure->SetValue(list->GetId(i), segm->GetElementData(i)->InfiltrationPressure);
		  ReferencePressure->SetValue(list->GetId(i), segm->GetElementData(i)->ReferencePressure);
		  Permeability->SetValue(list->GetId(i), segm->GetElementData(i)->Permeability);
			}

		ptId = this->GridLine->GetNodeInformation(this->SegmentSelected, list->GetNumberOfIds());
		//Trocando os valores das propriedades
		Radius->SetValue(ptId[0], segm->GetNodeData(list->GetNumberOfIds())->radius);
		}
	else if ( this->StraightModelObjectSelected == SEGMENT )
		{
		//Trocando os valores das propriedades
		Elastin->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->Elastin);
	  Collagen->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->Collagen);
	  CoefficientA->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->CoefficientA);
	  CoefficientB->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->CoefficientB);
	  Viscoelasticity->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->Viscoelasticity);
	  ViscoelasticityExponent->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->ViscoelasticityExponent);
	  InfiltrationPressure->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->InfiltrationPressure);
	  ReferencePressure->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)-> ReferencePressure);
	  Permeability->SetValue(this->LineWidgetSelected, segm->GetElementData(this->ElementSelected)->Permeability);

	  ptId = this->GridLine->GetSegment(this->LineSelectedId);
	  Radius->SetValue(ptId[0], segm->GetNodeData(this->ElementSelected)->radius);
	  Radius->SetValue(ptId[1], segm->GetNodeData(this->ElementSelected+1)->radius);
		}
	else
		{
		//Trocando os valores das propriedades
		Radius->SetValue(this->PointSelectedId, segm->GetNodeData(this->NodeSelected)->radius);
		}

	Radius->Modified();
	Elastin->Modified();
	Collagen->Modified();
	CoefficientA->Modified();
	CoefficientB->Modified();
	Viscoelasticity->Modified();
	ViscoelasticityExponent->Modified();
	InfiltrationPressure->Modified();
	ReferencePressure->Modified();
	Permeability->Modified();

	this->GridLine->Update();
}

//----------------------------------------------------------------------------

int vtkHMStraightModelWidget::CheckForNullViscoElasticElements()
{
	return this->GetStraightModel()->CheckForNullViscoElasticElements();
}

//----------------------------------------------------------------------------
int vtkHMStraightModelWidget::CrtlKeyPressed()
{
	if ( this->Interactor )
		return this->Interactor->GetControlKey();
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::RenderSegmentsWithDefaultColors()
{
	this->HighlightLine(0);
	this->HighlightHandles(0);

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void vtkHMStraightModelWidget::ColorPlot(int type)
{
	PlotMode = type;
	this->RenderSegmentsWithDefaultColors();
	if (type == 0)//Se estiver no modo NormalPlot
	{
		if ( this->StraightModelObjectSelected == SEGMENT )
			{
			vtkActor *lineActor = vtkActor::New();

			vtkLineSource *lineSource = vtkLineSource::New();

			vtkPolyDataMapper *lineMapper = vtkPolyDataMapper::New();

			vtkIdType *info;
			double point1[3], point2[3];

			//Pego do grid informacoes do elemento clicado
			info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

			//Pego do grid o ponto 1 associado com o elemento
			this->GridLine->GetPoint(info[0],point1);
			lineSource->SetPoint1(point1);//setando o ponto 1 no source

			//Pego do grid o ponto 2 associado com o elemento
			this->GridLine->GetPoint(info[1], point2);
			lineSource->SetPoint2(point2);//setando o ponto 2 no source

	//		this->PlotMapper[NumberOfGraphicsInstance]->SetInput(lineSource->GetOutput());//GetOutput retorna um polydata para o mapper
			lineMapper->SetInput(lineSource->GetOutput());//GetOutput retorna um polydata para o mapper
	//		this->PlotLineActor[NumberOfGraphicsInstance]->SetMapper(this->PlotMapper[NumberOfGraphicsInstance]);//setando o ator no mapper
			lineActor->SetMapper(lineMapper);//setando o ator no mapper

			//apagando os atores e o mapper maiores do que o número de gráficos
			if(this->GetNumberOfGraphicsSum()>MAX_CURVES)
				{
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->GetMapper()->Delete();
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->Delete();
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]=NULL;

				this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->Delete();
				this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]=NULL;
				}

	//		this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = this->PlotLineActor[NumberOfGraphicsInstance];
			this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = lineActor;

			this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = lineSource;

			}
		else
		{
		vtkActor *sphereActor = vtkActor::New();
		vtkSphereSource *sphereSource = vtkSphereSource::New();
		vtkPolyDataMapper *sphereMapper = vtkPolyDataMapper::New();
		vtkIdType *info;
		double point1[3];
			if ( this->StraightModelObjectSelected == NODE )
				//Pego do grid informacoes do elemento clicado
				info = this->GridLine->GetNodeInformation(this->SegmentSelected, this->NodeSelected);
			else
				info = this->GridLine->GetTerminalInformation(this->TerminalSelected);

			//Pego do grid o ponto 1 associado com o elemento
			this->GridLine->GetPoint(info[0],point1);
			sphereSource->SetCenter(point1);//setando o ponto 1 no source
			sphereSource->SetRadius(this->RadiusNode);
			this->SizeHandles();

	//		this->PlotMapper[NumberOfGraphicsInstance]->SetInput(sphereSource->GetOutput());//GetOutput retorna um polydata para o mapper
			sphereMapper->SetInput(sphereSource->GetOutput());//GetOutput retorna um polydata para o mapper
	//		this->PlotSphereActor[NumberOfGraphicsInstance]->SetMapper(this->PlotMapper[NumberOfGraphicsInstance]);//setando o mapper
			sphereActor->SetMapper(sphereMapper);//setando o mapper

			if(this->GetNumberOfGraphicsSum()>MAX_CURVES)
				{
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->GetMapper()->Delete();
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->Delete();
				this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]=NULL;

				this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]->Delete();
				this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES]=NULL;
				}

	//		this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = this->PlotSphereActor[NumberOfGraphicsInstance];
			this->SelectedActors[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = sphereActor;

			this->PlotSource[(this->GetNumberOfGraphicsSum()-1)%MAX_CURVES] = sphereSource;

		}
	}
	else if (type == 1)//Se estiver no modo AnimatedPlot
	{
		if ( this->StraightModelObjectSelected == SEGMENT )
			{

			vtkIdType *info;
			double point1[3], point2[3];

			//Pego do grid informacoes do elemento clicado
			info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

			//Pego do grid o ponto 1 associado com o elemento
			this->GridLine->GetPoint(info[0],point1);
			this->AnimatedLineSource->SetPoint1(point1);//setando o ponto 1 no source

			//Pego do grid o ponto 2 associado com o elemento
			this->GridLine->GetPoint(info[1], point2);
			this->AnimatedLineSource->SetPoint2(point2);//setando o ponto 2 no source

			this->AnimatedLineMapper->SetInput(this->AnimatedLineSource->GetOutput());//GetOutput retorna um polydata para o mapper
			this->AnimatedLineActor->SetMapper(this->AnimatedLineMapper);//setando o ator no mapper

			this->AnimatedSelectedActor = this->AnimatedLineActor;

			}
		else
		{

		vtkIdType *info;
		double point1[3];
			if ( this->StraightModelObjectSelected == NODE )
				//Pego do grid informacoes do elemento clicado
				info = this->GridLine->GetNodeInformation(this->SegmentSelected, this->NodeSelected);
			else
				info = this->GridLine->GetTerminalInformation(this->TerminalSelected);

			//Pego do grid o ponto 1 associado com o elemento
			this->GridLine->GetPoint(info[0],point1);
			this->AnimatedSphereSource->SetCenter(point1);//setando o ponto 1 no source
			this->AnimatedSphereSource->SetRadius(this->RadiusNode);
			this->SizeHandles();

			this->AnimatedSphereMapper->SetInput(this->AnimatedSphereSource->GetOutput());//GetOutput retorna um polydata para o mapper
			this->AnimatedSphereActor->SetMapper(this->AnimatedSphereMapper);//setando o mapper

			this->AnimatedSelectedActor = this->AnimatedSphereActor;

		}
	}
	this->SizeHandles();
	this->HighlightSelectedActors(1);
}

//----------------------------------------------------------------------------
vtkIdType vtkHMStraightModelWidget::AddLineWidget(double xyz[3], int numberOfElements, vtkIdType idParent)
{
	int i=0;
	vtkIdType idPts[4];
	vtkIdType idNode[3];
	int nPoints = numberOfElements+1; // Number of nodes
	int lastNode = nPoints - 1 ;
	double pt1[3], pt2[3];

	vtkDebugMacro(<<"AddLineWidget.");

	vtkPoints *points 			= this->GridLine->GetPoints();
	vtkCellArray *Segments	= this->GridLine->GetSegments();
	vtkCellArray *Lines 		= this->GridLine->GetLines();
	vtkCellArray *GridNodes	= this->GridLine->GetNodes();

	vtkCellArray *Vertices 		= this->GridNode->GetVerts();
	vtkPoints *pointsNode 		= this->GridNode->GetPoints();
	vtkCellArray *Nodes 			= this->GridNode->GetNodes();

	idNode[0] = idPts[0] = points->InsertNextPoint(xyz);
	pointsNode->InsertNextPoint(xyz);
	this->Points->InsertNextPoint(xyz);
	Vertices->InsertNextCell(1, &idNode[0]);

	vtkDoubleArray *Elastin 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Elastin"));
	vtkDoubleArray *Collagen 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Collagen"));
	vtkDoubleArray *CoefficientA							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("CoefficientA"));
	vtkDoubleArray *CoefficientB							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("CoefficientB"));
	vtkDoubleArray *Viscoelasticity						= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Viscoelasticity"));
	vtkDoubleArray *ViscoelasticityExponent		= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("ViscoelasticityExponent"));
	vtkDoubleArray *InfiltrationPressure			= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("InfiltrationPressure"));
	vtkDoubleArray *ReferencePressure					= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("ReferencePressure"));
	vtkDoubleArray *Permeability							= vtkDoubleArray::SafeDownCast(this->GridLine->GetCellData()->GetArray("Permeability"));

	pt1[0] = xyz[0];
	pt1[1] = xyz[1];
	pt1[2] = xyz[2];

	vtkIdType idTerminal = idParent;
	//Se não possui parent, adionar um terminal como pai do segmento
	if ( !idParent )
		{
		idTerminal = this->AddTerminalWidget(xyz, 0, idPts[0]);
		this->AddTerminalWidget(xyz, idTerminal);
		}

	vtkIdType idSegment = this->StraightModel->AddSegment(idTerminal);

	idNode[2] = idSegment;

	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(idSegment);
	Segm->SetNumberOfElements(numberOfElements);

	vtkIdList *elementList = vtkIdList::New();
	vtkIdList *nodeList = vtkIdList::New();
	vtkIdType idLine, idCellNode;

	for ( i=0; i<numberOfElements; i++ )
		{
		vtkDebugMacro(<<"Set Element Data.");

		vtkHMElementData *elem = new vtkHMElementData();

		//Setting default data
		elem->idNode[0] = i;
		elem->idNode[1] = i+1;

		Elastin->InsertNextValue(elem->Elastin);
		Collagen->InsertNextValue(elem->Collagen);
		CoefficientA->InsertNextValue(elem->CoefficientA);
		CoefficientB->InsertNextValue(elem->CoefficientB);
		Viscoelasticity->InsertNextValue(elem->Viscoelasticity);
		ViscoelasticityExponent->InsertNextValue(elem->ViscoelasticityExponent);
		InfiltrationPressure->InsertNextValue(elem->InfiltrationPressure);
		ReferencePressure->InsertNextValue(elem->ReferencePressure);
		Permeability->InsertNextValue(elem->Permeability);

		Segm->SetElementData(i, *elem);

		//Alterar o ponto dois para adiconar os nodes e o terminal pois,
		//caso fiquem todos no mesmo ponto inicialmente, ocorre um erro
		//no calculo das normais das esferas
		pt2[0] = pt1[0]+0.0001;
		pt2[1] = pt1[1];
		pt2[2] = pt1[2];

		//Alterando o StraightModelGrid
		idPts[1] = points->InsertNextPoint(pt2);
		pointsNode->InsertNextPoint(xyz);
		this->Points->InsertNextPoint(pt2);
		idPts[3] = idSegment;

		idPts[2] = i;

		idNode[1] = i; // id do node

		idCellNode = GridNodes->InsertNextCell(3, idNode);
		Nodes->InsertNextCell(3, idNode);
		nodeList->InsertNextId(idCellNode);

		idNode[0] = idPts[1];
		Vertices->InsertNextCell(1, &idNode[0]);

		// Adiciona ao vetor de linhas do PolyData
		idLine = Lines->InsertNextCell(2,idPts);
		elementList->InsertNextId(idLine);

		// Adiciona ao vetor de segmentos do PolyData para o StraightModel
		Segments->InsertNextCell(4,idPts);

		vtkDebugMacro(<<"Create Element Widget " << i);

  	//Adicionar novo node
		this->AddNodeWidget(pt1, idSegment, i);

		pt1[0] = pt2[0];
		pt1[1] = pt2[1];
		pt1[2] = pt2[2];

		idPts[0] = idPts[1];

		this->CurrentSegment++;
		this->CurrentLineActor++;
		this->NumberOfSegments++;

		}

	//Adicionar ultimo node
	this->AddNodeWidget(pt1, idSegment, i);
	idNode[1] = i; // id do node
	idNode[0] = idPts[1];

	idCellNode = GridNodes->InsertNextCell(3, idNode);
	nodeList->InsertNextId(idCellNode);

	//Adiciona o terminal somente no straightModel
	idTerminal = this->AddTerminalWidget(pt1, idSegment, idPts[1]);

	//Remove do map as informacoes das celulas associadas ao segmento.
	this->GridLine->RemoveSegmentInformation(Segm->GetId());
	this->GridLine->RemoveNodeInformation(Segm->GetId());
	//Inserir no map informacoes das celulas associadas ao segmento para que
	//facilite e agilize a busca pelo elemento.
	this->GridLine->InsertSegmentInformation(Segm->GetId(),elementList);
	this->GridLine->InsertNodeInformation(Segm->GetId(),nodeList);

	// Setting number of Points, Segments and Terminals
	this->NumberOfPoints 		= this->GridLine->GetPoints()->GetNumberOfPoints();
	this->NumberOfSegments  = this->GridLine->GetSegments()->GetNumberOfCells();
	this->NumberOfTerminals = this->StraightModel->GetNumberOfTerminals();
	this->NumberOfNodes			= this->StraightModel->GetNumberOfNodes();
	this->NumberOfHandles		= this->NumberOfTerminals+this->NumberOfNodes;

	this->UpdateActors();

	return idSegment;
}

//----------------------------------------------------------------------------
// Adiciona um node ao Widget
void vtkHMStraightModelWidget::AddNodeWidget(double xyz[3], vtkIdType idSegment, vtkIdType idNode)
{
	vtkDebugMacro(<<"AddNodeWidget " << idNode);

	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(idSegment);

	vtkDoubleArray *radius	 									= vtkDoubleArray::SafeDownCast(this->GridLine->GetPointData()->GetArray("Radius"));

	vtkDebugMacro(<<"Setting default data of the node.");
	vtkHMNodeData *node = new vtkHMNodeData();

	node->coords[0] 			= xyz[0];
	node->coords[1] 			= xyz[1];
	node->coords[2] 			= xyz[2];

	radius->InsertNextValue(node->radius);

	Segm->SetNodeData(idNode, *node);
	delete node;

	this->NumberOfHandles++;
	this->NumberOfNodes++;
	this->NumberOfPoints++;
}

//------------------------------------------------------------------------------------------
//Adiciona um terminal no straightModel e no grid de linhas
vtkIdType vtkHMStraightModelWidget::AddTerminalWidget(double xyz[3], vtkIdType idSegment, vtkIdType idPoint)
{
	vtkDebugMacro(<<"AddTerminalWidget ");

	vtkCellArray *TerminalsGrid = this->GridLine->GetTerminals();
	vtkIdType idTerm[3];

	vtkIdType idTerminal;
	//Se idSegment for diferente de zero, o terminal possui pai,
	//sendo assim, não se torna necessário adicionar terminal no straightModel
	if ( idSegment )
		idTerminal = this->StraightModel->GetSegment(idSegment)->GetFirstChild()->GetId();
	else
		idTerminal = this->StraightModel->AddTerminal(idSegment);

	//Insere dados do terminal no grid de linhas
	idTerm[0] = idPoint;
	idTerm[1] = idTerminal;
	idTerm[2] = idSegment;

	vtkHM1DTerminal *Term = this->StraightModel->GetTerminal(idTerminal);

	vtkIdType idCell = TerminalsGrid->InsertNextCell(3,idTerm);
	this->GridLine->InsertTerminalInformation(idTerminal, idCell);

	vtkDebugMacro(<<"Setting default data of the terminal " << idTerminal);
	//Declaracao de listas de parametros do terminal
	vtkHM1DTerminal::ResistanceValuesList r1;
	vtkHM1DTerminal::ResistanceTimeList r1Time;
	vtkHM1DTerminal::ResistanceValuesList r2;
	vtkHM1DTerminal::ResistanceTimeList r2Time;
	vtkHM1DTerminal::CapacitanceValuesList c;
	vtkHM1DTerminal::CapacitanceTimeList cTime;
	vtkHM1DTerminal::PressureValuesList p;
	vtkHM1DTerminal::PressureValuesList pTime;

	//Adicionar zero as listas
	r1.push_back(0);
	r2.push_back(0);
	c.push_back(0);
	p.push_back(0);
	r1Time.push_back(0);
	r2Time.push_back(0);
	cTime.push_back(0);
	pTime.push_back(0);

	//Setar as listas
	Term->SetR1(&r1);
	Term->SetR1Time(&r1Time);
	Term->SetR2(&r2);
	Term->SetR2Time(&r2Time);
	Term->SetC(&c);
	Term->SetCTime(&cTime);
	Term->SetPt(&p);
	Term->SetPtTime(&pTime);

	this->CurrentTerminal++;
	this->CurrentHandleActor++;
	this->NumberOfHandles++;
	this->NumberOfTerminals++;

	// Setting number of Points, Segments and Terminals
	this->NumberOfPoints 		= this->GridLine->GetPoints()->GetNumberOfPoints();
	this->NumberOfSegments  = this->GridLine->GetSegments()->GetNumberOfCells();
	this->NumberOfTerminals = this->StraightModel->GetNumberOfTerminals();
	this->NumberOfNodes			= this->StraightModel->GetNumberOfNodes();
	this->NumberOfHandles		= this->NumberOfTerminals+this->NumberOfNodes;

  return idTerminal;
}

//------------------------------------------------------------------------------------------
//Adiciona um terminal no grid de terminais
void vtkHMStraightModelWidget::AddTerminalWidget(double xyz[3], vtkIdType idTerm)
{
	vtkCellArray *TerminalsTerm = this->GridTerminal->GetTerminals();

	vtkCellArray *VerticesTerm 		= this->GridTerminal->GetVerts();
	vtkPoints *pointsTerminal 		= this->GridTerminal->GetPoints();

	vtkIdType id[3];

	id[0] = pointsTerminal->InsertNextPoint(xyz);
	id[1] = idTerm;
	id[2] = 0;

	TerminalsTerm->InsertNextCell(3,id);
	VerticesTerm->InsertNextCell(1, id);
	this->GridTerminal->InsertTerminalInformation(idTerm, id[0]);

	this->UpdateActors();
}

//------------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::AddClip(vtkIdType idSegment, vtkIdType idNode)
{
	double x[3];
	vtkIdType *pt;

	vtkDebugMacro(<<"AddClip.");

	pt = this->GridLine->GetNodeInformation(idSegment, idNode);

	this->GridLine->GetPoint(pt[0], x);

	this->HighlightHandles(0);

	//Pega o segmento clicado e o terminal filho
	vtkHM1DSegment *segm 	= this->StraightModel->GetSegment(idSegment);
	vtkHM1DTreeElement *elem;

	elem = segm->GetFirstChild();

	//Procura pelo terminal filho do segmento
	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		elem = segm->GetNextChild();

	//Dividir o segmento em dois
	this->DivideSegment(idSegment, idNode);

	//Atualiza as informações de pontos dos grids
	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	//pega o segmento filho do terminal do segmento selecionado
	//Esse segmento é a segunda parte do segmento que fo dividido
	vtkHM1DTreeElement *elem2 = elem->GetFirstChild();
	//pega informações do primeiro node do segmento, a segunda parte do segmento que foi dividido
	vtkIdType *info = this->GridLine->GetNodeInformation(elem2->GetId(), 0);
	vtkIdType newTerm = this->AddTerminalWidget(x, 0, info[0]);

	this->AddTerminalWidget(x, newTerm);

	//Pego o novo terminal adicionado
	vtkHM1DTerminal *newTerminal = this->StraightModel->GetTerminal(newTerm);

	vtkHM1DTerminal::ResistanceValuesList R1;
	R1.push_back(1.0E+12);

	//Seta propriedade R1 e R2 default do novo terminal
	newTerminal->SetR1(&R1);
	newTerminal->SetR2(&R1);
	//Seta propriedade R1 e R2 default do terminal filho do segmento primeiro segmento.
	vtkHM1DTerminal::SafeDownCast(elem)->SetR1(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetR2(&R1);

	//Limpa a lista e adiciona o valor zero
	R1.clear();
	R1.push_back(0);
	//Seta propriedade default do novo terminal
	newTerminal->SetC(&R1);
	newTerminal->SetPt(&R1);
	newTerminal->SetR1Time(&R1);
	newTerminal->SetR2Time(&R1);
	newTerminal->SetCTime(&R1);
	newTerminal->SetPtTime(&R1);
	//Seta propriedade default do terminal filho do segmento primeiro segmento.
	vtkHM1DTerminal::SafeDownCast(elem)->SetC(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetPt(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetR1Time(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetR2Time(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetCTime(&R1);
	vtkHM1DTerminal::SafeDownCast(elem)->SetPtTime(&R1);

	//adiciona como filho do novo terminal o segundo segmento.
	newTerminal->AddChild(elem2);

	//remove do terminal o filho segundo segmento
	elem->RemoveChild(elem2);
	//remove do segundo segmento o pai terminal
	elem2->RemoveParent(elem);

	this->ClipSegments->InsertNextTuple2(idSegment, elem2->GetId());

	vtkPoints *points = this->GridClip->GetPoints();

	vtkIdType idPt = points->InsertNextPoint(x);
	this->GridClip->SetPoints(points);

	vtkCellArray *verts = this->GridClip->GetVerts();

	verts->InsertNextCell(1, &idPt);
	this->GridClip->SetVerts(verts);

	this->UpdateActors();
}


//---------------------------------------------------------------------------------------
//Adiciona um stent no segmento com os ids dos elementos na lista
vtkIdType vtkHMStraightModelWidget::AddStent(vtkIdType idSegment, vtkIdList *elementList)
{
	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		return -1;

	//Armazena as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//Adiciona o stent
	vtkIdType idCell = this->GridStent->AddStent(elementList, pointArray);

	pointArray->Delete();
//	this->UpdateActors();

	return idCell;
}

//---------------------------------------------------------------------------------------
//Adiciona um stent no segmento selecionado, varre para ambos os lados do segmento, a partir
//do elemento selecionado, buscando no segmento os elementos que serão afetados pelo stent.
//O stent será adicionado no elemento clicado e do tamanho passado como parametro será subtraído
//o tamanho do elemento, o tamanho total será dividido por dois, que será o tamanho a percorrer
//para cada lado a partir do elemento clicado, se este tamanho for maior ou igual ao tamanho do
//elemento, o stent será adicionado neste elemento também.
vtkIdType vtkHMStraightModelWidget::AddStent(double size)
{
	if ( (this->SegmentSelected == -1) )
		return -1;

	double pt1[3], pt2[3];
	vtkIdType *info;
	vtkIdList *elementList;
	vtkIdList *list1;

	int numberOfElements = this->StraightModel->GetSegment(this->SegmentSelected)->GetElementNumber();

	//calcula o tamanho do elemento selecionado
	double length;

	double size1, size2;

	//Se click foi em um elemento e nao em um node
	if ( this->ElementSelected != -1 )
	  {
    info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

	  this->GridLine->GetPoints()->GetPoint(info[0], pt1);
	  this->GridLine->GetPoints()->GetPoint(info[1], pt2);

    length = this->ComputeVectorLength(pt1,pt2);

    //se elemento clicado for o primeiro do segmento
    if ( this->ElementSelected == 0 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo stent a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stent, inseri na lista
        //o id do elemento e subtrai do tamanho do stent o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }
      } // Fim if ( this->ElementSelected == 0 )
    //se elemento clicado for o último do segmento
    else if ( this->ElementSelected == numberOfElements-1 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo stent a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stent, inseri na lista
        //o id do elemento e subtrai do tamanho do stent o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo stent
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);
      } // Fim else if ( this->ElementSelected == numberOfElements-1 )
    //se elemento clicado for do meio do segmento
    else
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;
      size1 = size2 = size/2.0;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo stent a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size1);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stent, inseri na lista
        //o id do elemento e subtrai do tamanho do stent o tamanho do elemento para nova comparação
        if ( (length <= size1) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size1 -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo stent
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo stent a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size2);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stent, inseri na lista
        //o id do elemento e subtrai do tamanho do stent o tamanho do elemento para nova comparação
        if ( (length <= size2) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size2 -= length;
          }
        else
          break;
        }

      } // Fim else
	  } // Fim if ( this->ElementSelected != -1 )

	//Se click foi em um node e nao em um elemento
	else if ( this->NodeSelected != -1 )
	  {
	  size1 = size2 = size/2.0;

    elementList = vtkIdList::New();

    list1 = vtkIdList::New();

    //Percorre os elementos da primeira parte que serão afetados pelo stent a partir do node selecionado
    for ( int i=this->NodeSelected-1; i>=0; i-- )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size1);

      //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stenosis, inserir na lista
      //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
      if ( (length <= size1) || (dif < 0.001) )
       {
       list1->InsertUniqueId(i);
       size1 -= length;
       }
      else
       break;
      }

    //inseri na lista de forma ordenada os elementos afetados pelo stenosis
    for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
      elementList->InsertUniqueId(list1->GetId(i));
    list1->Delete();

    //Percorre os elementos da segunda parte que serão afetados pelo stenosis a partir do elemento selecionado
    for ( int i=this->NodeSelected; i<numberOfElements; i++ )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size2);

      //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stenosis, inserir na lista
      //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
      if ( (length <= size2) || (dif < 0.001) )
       {
       elementList->InsertUniqueId(i);
       size2 -= length;
       }
      else
       break;
      }
	  } // Fim else if ( this->NodeSelected != -1 )


	if ( this->GridStent->GetStent(this->StraightModelElementSelected) )
		this->ReplaceStent(this->SegmentSelected, elementList, this->StraightModelElementSelected);
	else
		this->StraightModelElementSelected = this->AddStent(this->SegmentSelected, elementList);

	this->UpdateActors();

	return this->StraightModelElementSelected;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ReplaceStent(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idStent)
{
	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		return;

	//Armazenar as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//refaz o grid de stent
	this->GridStent->ReplaceStent(idStent, elementList, pointArray);

	pointArray->Delete();
	this->UpdateActors();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::RemoveStent(vtkIdType idStent)
{
	this->GridStent->RemoveStent(idStent);
	this->UpdateActors();
	this->ClearElementSelected();
	this->RenderSegmentsWithDefaultColors();
}

//---------------------------------------------------------------------------------------
//Adiciona um stenosis no segmento selecionado, varre para ambos os lados do segmento, a partir
//do node selecionado, buscando no segmento os elementos que serão afetados pelo stenosis.
//O stenosis será adicionado no elemento clicado e do tamanho passado como parametro será subtraído
//o tamanho do elemento, o tamanho total será dividido por dois, que será o tamanho a percorrer
//para cada lado a partir do elemento clicado, se este tamanho for maior ou igual ao tamanho do
//elemento, o stenosis será adicionado neste elemento também.
vtkIdType vtkHMStraightModelWidget::AddStenosis(double size)
{
	if ( this->SegmentSelected == -1 )
		return -1;

	double pt1[3], pt2[3];
	vtkIdType *info;
	vtkIdList *elementList;
	vtkIdList *list1;

	int numberOfElements = this->StraightModel->GetSegment(this->SegmentSelected)->GetElementNumber();

	//calcula o tamanho do elemento selecionado
	double length;

	double size1, size2;

  //Se click foi em um node e nao em um elemento
	if ( this->NodeSelected != -1 )
	  {
    size1 = size2 = size/2.0;

    elementList = vtkIdList::New();

    list1 = vtkIdList::New();

    //Percorre os elementos da primeira parte que serão afetados pelo stenosis a partir do node selecionado
    for ( int i=this->NodeSelected-1; i>=0; i-- )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size1);

      //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stenosis, inserir na lista
      //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
      if ( (length <= size1) || (dif < 0.001) )
        {
        list1->InsertUniqueId(i);
        size1 -= length;
        }
      else
        break;
      }

    //inseri na lista de forma ordenada os elementos afetados pelo stenosis
    for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
      elementList->InsertUniqueId(list1->GetId(i));
    list1->Delete();

    //Percorre os elementos da segunda parte que serão afetados pelo stenosis a partir do elemento selecionado
    for ( int i=this->NodeSelected; i<numberOfElements; i++ )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size2);

      //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stenosis, inserir na lista
      //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
      if ( (length <= size2) || (dif < 0.001) )
        {
        elementList->InsertUniqueId(i);
        size2 -= length;
        }
      else
        break;
      }
	  } // Fim if ( this->NodeSelected != -1 )

  //Se click foi em um elemento e nao em um node
	else if ( this->ElementSelected != -1 )
	  {
	  info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

	  this->GridLine->GetPoints()->GetPoint(info[0], pt1);
	  this->GridLine->GetPoints()->GetPoint(info[1], pt2);

	  //calcula o tamanho do elemento selecionado
	  length = this->ComputeVectorLength(pt1,pt2);

    //se elemento clicado for o primeiro do segmento
    if ( this->ElementSelected == 0 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo stenosis a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stenosis, inseri na lista
        //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }
      } // Fim if ( this->ElementSelected == 0 )
    //se elemento clicado for o último do segmento
    else if ( this->ElementSelected == numberOfElements-1 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo stenosis a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stenosis, inseri na lista
        //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo stenosis
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);
      } // Fim else if ( this->ElementSelected == numberOfElements-1 )
    //se elemento clicado for do meio do segmento
    else
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;
      size1 = size2 = size/2.0;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo stenosis a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size1);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do stenosis, inseri na lista
        //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
        if ( (length <= size1) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size1 -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo stenosis
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo stenosis a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size2);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do stenosis, inseri na lista
        //o id do elemento e subtrai do tamanho do stenosis o tamanho do elemento para nova comparação
        if ( (length <= size2) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size2 -= length;
          }
        else
          break;
        }

      } // Fim else
    } // Fim if ( this->ElementSelected != -1 )

	if ( this->GridStenosis->GetStenosis(this->StraightModelElementSelected) )
		this->ReplaceStenosis(this->SegmentSelected, elementList, this->StraightModelElementSelected);
	else
		this->StraightModelElementSelected = this->AddStenosis(this->SegmentSelected, elementList);

	this->UpdateActors();

	return this->StraightModelElementSelected;
}

//---------------------------------------------------------------------------------------
//Adiciona um stenosis no segmento com os ids dos elementos na lista
vtkIdType vtkHMStraightModelWidget::AddStenosis(vtkIdType idSegment, vtkIdList *elementList)
{
  vtkWarningMacro(<<"vtkHMStraightModelWidget::AddStenosis - idSegment: " << idSegment);
  for (int i=0; i<elementList->GetNumberOfIds(); i++ )
    vtkWarningMacro(<<"  ::AddStenosis - element: " << elementList->GetId(i));

	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		return -1;

	//Armazena as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//Adiciona o stenosis
	vtkIdType idCell = this->GridStenosis->AddStenosis(elementList, pointArray);

	pointArray->Delete();
//	this->UpdateActors();

	return idCell;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ReplaceStenosis(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idStenosis)
{
	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		return;

	//Armazenar as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//refaz o grid de stenosis
	this->GridStenosis->ReplaceStenosis(idStenosis, elementList, pointArray);

	pointArray->Delete();
	this->UpdateActors();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::RemoveStenosis(vtkIdType idStenosis)
{
	this->GridStenosis->RemoveStenosis(idStenosis);
	this->UpdateActors();
	this->ClearElementSelected();
	this->RenderSegmentsWithDefaultColors();
}

//---------------------------------------------------------------------------------------
//Adiciona um aneurisma no segmento selecionado, varre para ambos os lados do segmento, a partir
//do node selecionado, buscando no segmento os elementos que serão afetados pelo aneurisma.
//O aneurisma será adicionado no elemento clicado e do tamanho passado como parametro será subtraído
//o tamanho do elemento, o tamanho total será dividido por dois, que será o tamanho a percorrer
//para cada lado a partir do elemento clicado, se este tamanho for maior ou igual ao tamanho do
//elemento, o aneurisma será adicionado neste elemento também.
vtkIdType vtkHMStraightModelWidget::AddAneurysm(double size)
{
	if ( this->SegmentSelected == -1 )
		return -1;

	double pt1[3], pt2[3];
	vtkIdType *info;
	vtkIdList *elementList;
	vtkIdList *list1;

	int numberOfElements = this->StraightModel->GetSegment(this->SegmentSelected)->GetElementNumber();

	//calcula o tamanho do elemento selecionado
	double length;

	double size1, size2;

  //Se click foi em um node e nao em um elemento
  if ( this->NodeSelected != -1 )
    {
    size1 = size2 = size/2.0;

    elementList = vtkIdList::New();

    list1 = vtkIdList::New();

    //Percorre os elementos da primeira parte que serão afetados pelo aneurisma a partir do node selecionado
    for ( int i=this->NodeSelected-1; i>=0; i-- )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size1);

      //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do aneurisma, inserir na lista
      //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
      if ( (length <= size1) || (dif < 0.001) )
        {
        list1->InsertUniqueId(i);
        size1 -= length;
        }
      else
        break;
      }

    //inseri na lista de forma ordenada os elementos afetados pelo aneurisma
    for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
      elementList->InsertUniqueId(list1->GetId(i));
    list1->Delete();

    //Percorre os elementos da segunda parte que serão afetados pelo aneurisma a partir do elemento selecionado
    for ( int i=this->NodeSelected; i<numberOfElements; i++ )
      {
      //pega informação do elemento.
      info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

      this->GridLine->GetPoints()->GetPoint(info[0], pt1);
      this->GridLine->GetPoints()->GetPoint(info[1], pt2);

      //calcula o tamanho do elemtno
      length = this->ComputeVectorLength(pt1,pt2);

      //calcula a diferença entre o tamanho do elemento e o restante do tamanho
      double dif = fabs(length - size2);

      //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do aneurisma, inserir na lista
      //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
      if ( (length <= size2) || (dif < 0.001) )
        {
        elementList->InsertUniqueId(i);
        size2 -= length;
        }
      else
        break;
      }
    } // Fim if ( this->NodeSelected != -1 )

  //Se click foi em um elemento e nao em um node
  else if ( this->ElementSelected != -1 )
    {
	  info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

	  this->GridLine->GetPoints()->GetPoint(info[0], pt1);
	  this->GridLine->GetPoints()->GetPoint(info[1], pt2);

	  //calcula o tamanho do elemento selecionado
	  length = this->ComputeVectorLength(pt1,pt2);

    //se elemento clicado for o primeiro do segmento
    if ( this->ElementSelected == 0 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo aneurisma a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do aneurisma, inseri na lista
        //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }
      } // Fim if ( this->ElementSelected == 0 )
    //se elemento clicado for o último do segmento
    else if ( this->ElementSelected == numberOfElements-1 )
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo aneurisma a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do aneurisma, inseri na lista
        //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
        if ( (length <= size) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo aneurisma
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);
      } // Fim else if ( this->ElementSelected == numberOfElements-1 )
    //se elemento clicado for do meio do segmento
    else
      {
      //subtrai do tamanho total, o tamanho do elemento e divide por dois
      size -= length;
      size1 = size2 = size/2.0;

      elementList = vtkIdList::New();

      list1 = vtkIdList::New();

      //Percorre os elementos da primeira parte que serão afetados pelo aneurisma a partir do elemento selecionado
      for ( int i=this->ElementSelected-1; i>=0; i-- )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size1);

        //se o tamanho do elemento for maior ou igual ao tamanho da primeira parte do aneurisma, inseri na lista
        //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
        if ( (length <= size1) || (dif < 0.001) )
          {
          list1->InsertUniqueId(i);
          size1 -= length;
          }
        else
          break;
        }

      //inseri na lista de forma ordenada os elementos afetados pelo aneurisma
      for ( int i=list1->GetNumberOfIds()-1; i>=0; i-- )
        elementList->InsertUniqueId(list1->GetId(i));
      list1->Delete();

      //inseri o elemento selecionado
      elementList->InsertUniqueId(this->ElementSelected);

      //Percorre os elementos da segunda parte que serão afetados pelo aneurisma a partir do elemento selecionado
      for ( int i=this->ElementSelected+1; i<numberOfElements; i++ )
        {
        //pega informação do elemento.
        info = this->GridLine->GetElementInformation(this->SegmentSelected, i);

        this->GridLine->GetPoints()->GetPoint(info[0], pt1);
        this->GridLine->GetPoints()->GetPoint(info[1], pt2);

        //calcula o tamanho do elemtno
        length = this->ComputeVectorLength(pt1,pt2);

        //calcula a diferença entre o tamanho do elemento e o restante do tamanho
        double dif = fabs(length - size2);

        //se o tamanho do elemento for maior ou igual ao tamanho da segunda parte do aneurisma, inseri na lista
        //o id do elemento e subtrai do tamanho do aneurisma o tamanho do elemento para nova comparação
        if ( (length <= size2) || (dif < 0.001) )
          {
          elementList->InsertUniqueId(i);
          size2 -= length;
          }
        else
          break;
        }

      } // Fim else
    } // Fim if ( this->ElementSelected != -1 )

	if ( this->GridAneurysm->GetAneurysm(this->StraightModelElementSelected) )
		this->ReplaceAneurysm(this->SegmentSelected, elementList, this->StraightModelElementSelected);
	else
		this->StraightModelElementSelected = this->AddAneurysm(this->SegmentSelected, elementList);

	this->UpdateActors();

	return this->StraightModelElementSelected;
}

//---------------------------------------------------------------------------------------
//Adiciona um aneurisma no segmento com os ids dos elementos na lista
vtkIdType vtkHMStraightModelWidget::AddAneurysm(vtkIdType idSegment, vtkIdList *elementList)
{
	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		return -1;

	//Armazena as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do node
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//Adiciona o stenosis
	vtkIdType idCell = this->GridAneurysm->AddAneurysm(elementList, pointArray);

	pointArray->Delete();
//	this->UpdateActors();

	return idCell;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ReplaceAneurysm(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idAneurysm)
{
	double pt[3];
	vtkIdType *info;

	int numberOfElements = elementList->GetNumberOfIds();

	if ( numberOfElements <= 0 )
		{
		elementList->Delete();
		return;
		}

	//Armazenar as coordenadas dos elementos
	vtkDoubleArray *pointArray = vtkDoubleArray::New();
	pointArray->SetNumberOfComponents(3);

	info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(0));

	//Pego do grid o ponto relacionado com o node
	this->GridLine->GetPoints()->GetPoint(info[0], pt);
	pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

	for ( int i=0; i<numberOfElements; i++ )
		{
		//Pego do grid informacoes do primeiro node, primeiro ponto
		info = this->GridLine->GetElementInformation(idSegment, elementList->GetId(i));

		//Pego do grid o ponto relacionado com o node
		this->GridLine->GetPoints()->GetPoint(info[1], pt);
		pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
		}

	elementList->InsertNextId(idSegment);

	//refaz o grid de aneurisma
	this->GridAneurysm->ReplaceAneurysm(idAneurysm, elementList, pointArray);

	pointArray->Delete();
	this->UpdateActors();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::RemoveAneurysm(vtkIdType idAneurysm)
{
	this->GridAneurysm->RemoveAneurysm(idAneurysm);
	this->UpdateActors();
	this->ClearElementSelected();
	this->RenderSegmentsWithDefaultColors();
}

//------------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::SetEditionModeSelected(vtkIdType mode)
{
	if ( mode == vtkHMStraightModelWidget::DelElementInTree )
		{
		if ( this->StraightModelObjectSelected == SEGMENT )
			this->DeleteSegmentSelected();
		else if ( this->StraightModelObjectSelected == TERMINAL )
			this->DeleteTerminalSelected();
		this->EditionModeSelected = vtkHMStraightModelWidget::EditGeometry;
		}
	else if ( mode == vtkHMStraightModelWidget::CheckConsistence )
		{
		this->ClearElementSelected();

    this->RenderSegmentsWithDefaultColors();

    //Chama o método para colorir a arvore principal
    vtkIdList *list = vtkIdList::New();
		this->MakeMainTreeColor(this->StraightModel->Get1DTreeRoot()->GetId(), list);
		list->Delete();

		this->EditionModeSelected = mode;
		this->HighlightLine(1);
		}
	else
		this->EditionModeSelected = mode;
}

//------------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::SetSurgicalInterventionsModeSelected(vtkIdType mode)
{
	if ( mode == vtkHMStraightModelWidget::DeleteIntervention )
		{
		if ( this->StraightModelObjectSelected == CLIP )
			this->RemoveClip(this->StraightModelElementSelected);
		if ( this->StraightModelObjectSelected == STENT )
			this->RemoveStent(this->StraightModelElementSelected);
		if ( this->StraightModelObjectSelected == STENOSIS )
			this->RemoveStenosis(this->StraightModelElementSelected);
		if ( this->StraightModelObjectSelected == ANEURYSM )
			this->RemoveAneurysm(this->StraightModelElementSelected);
		mode = vtkHMStraightModelWidget::none;
		}

	this->SurgicalInterventionsModeSelected = mode;
}

//------------------------------------------------------------------------------------------
int vtkHMStraightModelWidget::DeleteSegmentSelected()
{
	vtkDebugMacro(<<"DeleteSegmentSelected.");
	if ( (this->SegmentSelected <= 0) || (this->SegmentSelected == 2) )
		{
		vtkWarningMacro("Segment not selected or segment cannot be removed!");
		return 0;
		}

	if ( this->StraightModelObjectSelected != SEGMENT )
		return 0;

	vtkIdType pos = this->FindClipIds(this->SegmentSelected);
	if ( pos != -1 )
		{
		vtkWarningMacro("This segment has a clip and cannot be removed!");
		return 0;
		}

	this->GridStent->DeleteStentsInSegment(this->SegmentSelected);
	this->GridStenosis->DeleteStenosisInSegment(this->SegmentSelected);
	this->GridAneurysm->DeleteAneurysmInSegment(this->SegmentSelected);

	this->StraightModel->RemoveSegment(this->SegmentSelected, 0);

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	this->HighlightLine(0);
	this->ClearElementSelected();
	this->State = vtkHMStraightModelWidget::Outside;

	this->Interactor->Render();
	return 1;
}

//------------------------------------------------------------------------------------------
int vtkHMStraightModelWidget::DeleteSegment(vtkIdType id)
{
	vtkDebugMacro(<<"DeleteSegment.");

	if ( (id <= 0) || (id == 2) )
		{
		vtkWarningMacro("Segment not selected or segment cannot be removed!");
		return 0;
		}

	vtkIdType clipId = this->FindClipIds(id);
	if ( clipId != -1 )
		{
		this->RemoveClip(clipId);
//		vtkWarningMacro("This segment has a clip and cannot be removed!");
//		return 0;
		}
//	else
//		{
//		this->RemoveClip(clipId);
////		double *segmId = this->GetClipSegments()->GetTuple(clipId);
////		this->StraightModel->RemoveSegment(segmId[0], 0);
////		this->StraightModel->RemoveSegment(segmId[1], 0);
//		}

	this->GridStent->DeleteStentsInSegment(id);
	this->GridStenosis->DeleteStenosisInSegment(id);
	this->GridAneurysm->DeleteAneurysmInSegment(id);

	this->StraightModel->RemoveSegment(id, 0);
	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	this->HighlightLine(0);
//	this->ClearSubTreeLists();
	this->RenderSegmentsWithDefaultColors();
	this->ClearElementSelected();
	this->State = vtkHMStraightModelWidget::Outside;

	this->Interactor->Render();
	return 1;
}

//------------------------------------------------------------------------------------------
int vtkHMStraightModelWidget::DeleteSegment(const char *text)
{
	vtkDebugMacro(<<"DeleteSegment.");

	vtkIdList *list = vtkHMUtils::ConvertCharToIdList((char*)text);
//	if(list)
	vtkIdType clipId;

	for(int i=0; i <= list->GetNumberOfIds(); i++)
		{
		clipId = this->FindClipIds(list->GetId(i));
		if ( clipId != -1 )
			this->RemoveClip(clipId);

		this->GridStent->DeleteStentsInSegment(list->GetId(i));
		this->GridStenosis->DeleteStenosisInSegment(list->GetId(i));
		this->GridAneurysm->DeleteAneurysmInSegment(list->GetId(i));
//			this->StraightModel->RemoveSegment(list->GetId(i), 0);
		}


	this->StraightModel->RemoveSegmentsInSubTree(list,  0);

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	this->HighlightLine(0);
	this->ClearSubTreeLists();
	this->RenderSegmentsWithDefaultColors();
	this->ClearElementSelected();
	this->State = vtkHMStraightModelWidget::Outside;

	this->Interactor->Render();
	list->Delete();
	return 1;
}

int vtkHMStraightModelWidget::DeleteTerminalSelected()
{
	vtkDebugMacro(<<"DeleteTerminalSelected.");

	if ( (this->TerminalSelected <= 0) || (this->TerminalSelected == 1) )
		{
		vtkWarningMacro("Terminal not selected or cannot be removed!");
		return 0;
		}

	if ( this->StraightModelObjectSelected != TERMINAL )
		return 0;

	vtkHM1DTerminal *term = this->StraightModel->GetTerminal(this->TerminalSelected);

	if ( (term->GetNumberOfParents() != 1) || (term->GetChildCount() != 1) )
		{
		vtkWarningMacro("Terminal cannot be removed!");
		return 0;
		}

	vtkHM1DSegment *parent = vtkHM1DSegment::SafeDownCast(term->GetFirstParent());
	vtkHM1DSegment *child = vtkHM1DSegment::SafeDownCast(term->GetFirstChild());

	double pt[3];
	vtkIdType *info;

	int numberOfElements = child->GetElementNumber();
	numberOfElements += parent->GetElementNumber();

	int i=parent->GetElementNumber();

	//Pega a lista de stents existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *stentList = this->GridStent->GetStentsInSegment(child->GetId());

	if ( stentList )
		{
		for ( int k=0; k<stentList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do stent
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do stent
			vtkIdList *elemList = this->GridStent->GetStent(stentList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do stent
			info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do stent
				newList->InsertUniqueId(elemList->GetId(l)+parent->GetElementNumber());
				info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(parent->GetId());
			//refaz o grid de stent
			this->GridStent->ReplaceStent(stentList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		stentList->Delete();
		}

	/*
	 * Atualiza lista de stenosis contidos na segunda parte de um segmento com clip
	 */
	//Pega a lista de stenosis existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *stenosisList = this->GridStenosis->GetStenosisInSegment(child->GetId());

	if ( stenosisList )
		{
		for ( int k=0; k<stenosisList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do stenosis
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do stenosis
			vtkIdList *elemList = this->GridStenosis->GetStenosis(stenosisList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do stenosis
			info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do stenosis
				newList->InsertUniqueId(elemList->GetId(l)+parent->GetElementNumber());
				info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(parent->GetId());
			//refaz o grid de stenosis
			this->GridStenosis->ReplaceStenosis(stenosisList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		stenosisList->Delete();
		}

	/*
	 * Atualiza lista de aneurysm contidos na segunda parte de um segmento com clip
	 */
	//Pega a lista de aneurysm existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *aneurysmList = this->GridAneurysm->GetAneurysmInSegment(child->GetId());

	if ( aneurysmList )
		{
		for ( int k=0; k<aneurysmList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do aneurysm
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do aneurysm
			vtkIdList *elemList = this->GridAneurysm->GetAneurysm(aneurysmList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do aneurysm
			info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do aneurysm
				newList->InsertUniqueId(elemList->GetId(l)+parent->GetElementNumber());
				info = this->GridLine->GetElementInformation(child->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(parent->GetId());
			//refaz o grid de aneurysm
			this->GridAneurysm->ReplaceAneurysm(aneurysmList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		aneurysmList->Delete();
		}

	int j=1;

	parent->SetNumberOfElements(numberOfElements);
	for ( ; i<numberOfElements; i++ )
		{
		vtkHMElementData *elem = new vtkHMElementData();

		//Setting default data
		elem->idNode[0] = i;
		elem->idNode[1] = i+1;

		parent->SetElementData(i, *elem);

		info = this->GridLine->GetNodeInformation(child->GetId(), j);

		this->GridLine->GetPoints()->GetPoint(info[0], pt);

  	//Adicionar novo node
		this->AddNodeWidget(pt, parent->GetId(), i+1);

		j++;
		}

	term->RemoveParent(parent);
	parent->RemoveChild(term);
	parent->AddChild(child->GetFirstChild());

	//Remove do straightModel o segundo segmento
	this->StraightModel->RemoveSegment(child->GetId(), 0);

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	this->ClearElementSelected();
	this->HighlightHandles(0);

	this->State = vtkHMStraightModelWidget::Outside;

	this->Interactor->Render();

	return 1;
}

//------------------------------------------------------------------------------------------
char *vtkHMStraightModelWidget::GetSegmentsWithClip(vtkIdType idClip)
{
	double *tuple = this->ClipSegments->GetTuple(idClip);

	char *s = new char[15];
	sprintf(s, "%0.f;%0.f;", tuple[0], tuple[1]);

	return s;
}

//------------------------------------------------------------------------------------------
//Remove o clip e junta os dois segmentos em um
void vtkHMStraightModelWidget::RemoveClip(vtkIdType idClip)
{
	double *tuple = this->ClipSegments->GetTuple(idClip);

	//pega os segmentos associados com o clip
	vtkHM1DSegment *firstSegm = this->StraightModel->GetSegment((int)tuple[0]);
	vtkHM1DSegment *secondSegm = this->StraightModel->GetSegment((int)tuple[1]);

	int numberOfElements = secondSegm->GetElementNumber();
	numberOfElements += firstSegm->GetElementNumber();

	int i=firstSegm->GetElementNumber();
	int nPoints = numberOfElements + 1; // Number of nodes

	double pt[3];
	vtkIdType *info;

	vtkDebugMacro(<<"RemoveClip.");

	/*
	 * Atualiza lista de stent contidos na segunda parte de um segmento com clip
	 */
	//Pega a lista de stents existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *stentList = this->GridStent->GetStentsInSegment(secondSegm->GetId());

	if ( stentList )
		{
		for ( int k=0; k<stentList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do stent
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do stent
			vtkIdList *elemList = this->GridStent->GetStent(stentList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do stent
			info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do stent
				newList->InsertUniqueId(elemList->GetId(l)+firstSegm->GetElementNumber());
				info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(firstSegm->GetId());
			//refaz o grid de stent
			this->GridStent->ReplaceStent(stentList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		stentList->Delete();
		}

	/*
	 * Atualiza lista de stenosis contidos na segunda parte de um segmento com clip
	 */
	//Pega a lista de stenosis existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *stenosisList = this->GridStenosis->GetStenosisInSegment(secondSegm->GetId());

	if ( stenosisList )
		{
		for ( int k=0; k<stenosisList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do stenosis
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do stenosis
			vtkIdList *elemList = this->GridStenosis->GetStenosis(stenosisList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do stenosis
			info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do stenosis
				newList->InsertUniqueId(elemList->GetId(l)+firstSegm->GetElementNumber());
				info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(firstSegm->GetId());
			//refaz o grid de stenosis
			this->GridStenosis->ReplaceStenosis(stenosisList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		stenosisList->Delete();
		}

	/*
	 * Atualiza lista de aneurysm contidos na segunda parte de um segmento com clip
	 */
	//Pega a lista de aneurysm existentes no segundo segmento para atualiza-los com o primeiro segmento
	vtkIdList *aneurysmList = this->GridAneurysm->GetAneurysmInSegment(secondSegm->GetId());

	if ( aneurysmList )
		{
		for ( int k=0; k<aneurysmList->GetNumberOfIds(); k++ )
			{
			//lista para armazenar os novos ids dos elementos do aneurysm
			vtkIdList *newList = vtkIdList::New();

			//pega lista com os ids dos elementos do aneurysm
			vtkIdList *elemList = this->GridAneurysm->GetAneurysm(aneurysmList->GetId(k));

			vtkDoubleArray *pointArray = vtkDoubleArray::New();
			pointArray->SetNumberOfComponents(3);
			//pega informação do primeiro elemento do aneurysm
			info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(0));

			this->GridLine->GetPoints()->GetPoint(info[0], pt);
			pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);

			for ( int l=0; l<elemList->GetNumberOfIds()-1; l++ )
				{
				//inseri na nova lista o novo id do elemento do aneurysm
				newList->InsertUniqueId(elemList->GetId(l)+firstSegm->GetElementNumber());
				info = this->GridLine->GetElementInformation(secondSegm->GetId(), elemList->GetId(l));

				this->GridLine->GetPoints()->GetPoint(info[1], pt);
				pointArray->InsertNextTuple3(pt[0], pt[1], pt[2]);
				}
			//inseri o id do primeiro segmento
			newList->InsertNextId(firstSegm->GetId());
			//refaz o grid de aneurysm
			this->GridAneurysm->ReplaceAneurysm(aneurysmList->GetId(k), newList, pointArray);
			pointArray->Delete();
			}
		aneurysmList->Delete();
		}

	int j=1;

	firstSegm->SetNumberOfElements(numberOfElements);
	for ( ; i<numberOfElements; i++ )
		{
		vtkHMElementData *elem = new vtkHMElementData();

		//Setting default data
		elem->idNode[0] = i;
		elem->idNode[1] = i+1;

		firstSegm->SetElementData(i, *elem);

		info = this->GridLine->GetNodeInformation(secondSegm->GetId(), j);

		this->GridLine->GetPoints()->GetPoint(info[0], pt);

  	//Adicionar novo node
		this->AddNodeWidget(pt, firstSegm->GetId(), i+1);

		j++;
		}

	//Remove do grid de clips o ponto referente ao clip
	this->GridClip->RemovePoint(idClip, 0);
	//Remove do array a tupla de associação dos segmentos com o clip
	this->ClipSegments->RemoveTuple(idClip);

	vtkHM1DTreeElement *child = secondSegm->GetFirstChild();
	vtkHM1DTreeElement *elem = child->GetFirstChild();

	vtkHM1DTreeElement *parent = child->GetFirstParent();

	while ( parent )
		{
		if ( parent->GetId() != secondSegm->GetId() )
			{
			parent->AddChild(firstSegm->GetFirstChild());
			child->RemoveParent(parent);
			parent->RemoveChild(child);
			parent = child->GetFirstParent();
			}
		else
			parent = child->GetNextParent();
		}

	//Passar os filhos do segundo segmento para o primeiro
	while ( elem )
		{
		firstSegm->GetFirstChild()->AddChild(elem);
		child->RemoveChild(elem);
		elem->RemoveParent(child);
		elem = child->GetFirstChild();
		}

	//Remove do straightModel o segundo segmento
	this->StraightModel->RemoveSegment(secondSegm->GetId(), 0);

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	this->ClearElementSelected();

	this->State = vtkHMStraightModelWidget::Outside;

	this->Interactor->Render();
}

//------------------------------------------------------------------------------------------
int vtkHMStraightModelWidget::SetSegmentData(int numberOfElements, double length)
{
	double x[3], diff[3], df[3], oldPt[3];
	int i=0;
	int nPoints = numberOfElements+1; // Number of nodes
	double pt1[3], pt2[3], p1[3], p2[3];

	vtkIdType *info;

	vtkDebugMacro(<<"SetSegmentData.");

	vtkIdList *stentList = this->GridStent->GetStentsInSegment(this->SegmentSelected);

	if ( stentList )
		{
		for (int i=0; i<stentList->GetNumberOfIds(); i++ )
			{
			vtkIdList *elemList = this->GridStent->GetStent(stentList->GetId(i));
			if ( numberOfElements < (elemList->GetId(elemList->GetNumberOfIds()-2)+1) )
				{
				vtkWarningMacro("This segment has one stent in the last elements and the number of elements cannot be lesser that "<<elemList->GetId(elemList->GetNumberOfIds()-2)+1);
				stentList->Delete();
				return 0;
				}
			}
		stentList->Delete();
		}

	vtkIdList *stenosisList = this->GridStenosis->GetStenosisInSegment(this->SegmentSelected);

	if ( stenosisList )
		{
		for (int i=0; i<stenosisList->GetNumberOfIds(); i++ )
			{
			vtkIdList *elemList = this->GridStenosis->GetStenosis(stenosisList->GetId(i));
			if ( numberOfElements < (elemList->GetId(elemList->GetNumberOfIds()-2)+1) )
				{
				vtkWarningMacro("This segment has one stenosis in the last elements and the number of elements cannot be lesser that "<<elemList->GetId(elemList->GetNumberOfIds()-2)+1);
				stenosisList->Delete();
				return 0;
				}
			}
		stenosisList->Delete();
		}

	vtkIdList *aneurysmList = this->GridAneurysm->GetAneurysmInSegment(this->SegmentSelected);

	if ( aneurysmList )
		{
		for (int i=0; i<aneurysmList->GetNumberOfIds(); i++ )
			{
			vtkIdList *elemList = this->GridAneurysm->GetAneurysm(aneurysmList->GetId(i));
			if ( numberOfElements < (elemList->GetId(elemList->GetNumberOfIds()-2)+1) )
				{
				vtkWarningMacro("This segment has one aneurysm in the last elements and the number of elements cannot be lesser that "<<elemList->GetId(elemList->GetNumberOfIds()-2)+1);
				aneurysmList->Delete();
				return 0;
				}
			}
		aneurysmList->Delete();
		}

	if ( this->GridNullElements1D3D )
		{
		this->GridNullElements1D3D->Delete();
		this->StraightModel->ClearNullElements1D3DMap();
		}
	this->GridNullElements1D3D = NULL;

	//Pega a lista de linhas do segmento
	vtkIdList *list = this->GridLine->GetSegmentInformation(this->SegmentSelected);

	//Pega o primeiro elemento
	info = this->GridLine->GetLine(list->GetId(0));
	this->GridLine->GetPoint(info[0], p1);

	//Pega o ultimo elemento
	info = this->GridLine->GetLine(list->GetId(list->GetNumberOfIds()-1));
	this->GridLine->GetPoint(info[1], oldPt);

	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(this->SegmentSelected);
	int nElements = Segm->GetElementNumber();

	//Calcular as novas coordenadas baseado no novo tamanho do segmento
	x[0] = (oldPt[0] - p1[0]) / Segm->GetLength();
	x[1] = (oldPt[1] - p1[1]) / Segm->GetLength();
	x[2] = (oldPt[2] - p1[2]) / Segm->GetLength();

	p2[0] = p1[0] + length * x[0];
	p2[1] = p1[1] + length * x[1];
	p2[2] = p1[2] + length * x[2];

	//Diferença entre o ponto distal antigo e o novo ponto do segmento
	//Essa diferença só acontece caso o tamanho do segmento seja alterado
	//para calcular o quanto os filhos do segmento serão deslocados.
	df[0] = p2[0] - oldPt[0];
	df[1] = p2[1] - oldPt[1];
	df[2] = p2[2] - oldPt[2];


	//Reseta a lista de elementos visitados na árvore.
	this->StraightModel->ResetElementVisitedList();
	//se o tamanho do segmento for alterado, desloca os elementos filhos do segmento.
	double d = length - Segm->GetLength();
	if ( (fabs(d) >= 0.0001) )
		{
		this->StraightModel->MoveChildren(Segm->GetFirstChild()->GetId(), df);
//		//pega os filhos do terminal filho do segmento com o tamanho alterado
//		vtkHM1DTreeElement *term = Segm->GetFirstChild();
//		vtkHM1DTreeElement *elem = term->GetFirstChild();
//		while ( elem )
//			{
//			this->MoveChild(df, elem->GetId());
//			elem = term->GetNextChild();
//			}
		}

	//calcula a diferenca entre o primeiro e o ultimo ponto
	diff[0] = p2[0] - p1[0];
	diff[1] = p2[1] - p1[1];
	diff[2] = p2[2] - p1[2];

	//calcula a distancia entre cada nó do segmento
	x[0] = diff[0] / numberOfElements;
	x[1] = diff[1] / numberOfElements;
	x[2] = diff[2] / numberOfElements;

	vtkDebugMacro(<<"AddLine.");

	vtkPoints *points = this->GridLine->GetPoints();

	pt1[0] = p1[0];
	pt1[1] = p1[1];
	pt1[2] = p1[2];

	//se novo numero de elementos for maior que o atual, devo adicionar novos elementos
	if ( numberOfElements > nElements )
		{
		//setar as coordenadas dos atuais nodes
		for ( i=0; i<nElements; i++ )
			{
			//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
			pt2[0] = pt1[0] + x[0];
			pt2[1] = pt1[1] + x[1];
			pt2[2] = pt1[2] + x[2];

	  	//Adicionar novo node
			Segm->GetNodeData(i)->coords[0] = pt1[0];
			Segm->GetNodeData(i)->coords[1] = pt1[1];
			Segm->GetNodeData(i)->coords[2] = pt1[2];

			pt1[0] = pt2[0];
			pt1[1] = pt2[1];
			pt1[2] = pt2[2];
			}

		Segm->SetNumberOfElements(numberOfElements);
		//Adicionar novos elementos e nodes
		for ( i=nElements; i<numberOfElements; i++ )
			{
			vtkDebugMacro(<<"Set Element Data.");

			vtkHMElementData *elem = new vtkHMElementData();

			//Setting default data
			elem->idNode[0] = i;
			elem->idNode[1] = i+1;

			Segm->SetElementData(i, *elem);

			//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
			pt2[0] = pt1[0] + x[0];
			pt2[1] = pt1[1] + x[1];
			pt2[2] = pt1[2] + x[2];

	  	//Adicionar novo node
			this->AddNodeWidget(pt1, this->SegmentSelected, i);

			pt1[0] = pt2[0];
			pt1[1] = pt2[1];
			pt1[2] = pt2[2];

			this->CurrentSegment++;
			this->CurrentLineActor++;
			this->NumberOfSegments++;
			}
		//Adicionar ultimo node
		this->AddNodeWidget(pt1, this->SegmentSelected, i);

		Segm->SetNumberOfElements(numberOfElements);
		}
	else
		{
		//remover os nodes e elementos
		Segm->RemoveElementData(numberOfElements, nElements);
		Segm->RemoveNodeData(numberOfElements+1, nElements+1);
		Segm->SetNumberOfElements(numberOfElements);

		//setar as coordenadas dos nodes
		for ( i=0; i<numberOfElements+1; i++ )
			{
			//Atribuir ao ponto dois o atual ponto mais a distancia para o proximo ponto
			pt2[0] = pt1[0] + x[0];
			pt2[1] = pt1[1] + x[1];
			pt2[2] = pt1[2] + x[2];

	  	//Adicionar novo node
			Segm->GetNodeData(i)->coords[0] = pt1[0];
			Segm->GetNodeData(i)->coords[1] = pt1[1];
			Segm->GetNodeData(i)->coords[2] = pt1[2];

			pt1[0] = pt2[0];
			pt1[1] = pt2[1];
			pt1[2] = pt2[2];
			}
		}

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	return 1;
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::SetSegment_Selected(int s)
{
	if ( !this->Enabled ) //already enabled, just return
	  {
	  return;
	  }

	vtkDebugMacro(<<"SetSegment_Selected.");

	this->RenderSegmentsWithDefaultColors();

	if ( s )
		{
		this->CurrentRenderer->RemoveActor(this->GridNodeActor);
		if ( this->NodePicker )
			{
			this->NodePicker->Delete();
			this->NodePicker = NULL;
			}
		}
	else
		{
		this->CurrentRenderer->AddActor(this->GridNodeActor);
	  this->GridNodeActor->SetProperty(this->GridNodeProperty);
	  if ( !this->NodePicker )
		  {
		  this->NodePicker = vtkCellPicker::New();
			this->NodePicker->SetTolerance(0.003);
			this->NodePicker->AddPickList(this->GridNodeActor);
		  }
			this->NodePicker->PickFromListOn();

		}
	this->Segment_Selected = s;
	this->Interactor->Render();
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::UpdateGrid()
{
	vtkDebugMacro(<<"UpdateGrid.");

	// Setting number of Points, Segments and Terminals
	this->NumberOfPoints 		= this->GridLine->GetPoints()->GetNumberOfPoints();
	this->NumberOfSegments  = this->GridLine->GetSegments()->GetNumberOfCells();
	this->NumberOfTerminals = this->StraightModel->GetNumberOfTerminals();
	this->NumberOfNodes			= this->StraightModel->GetNumberOfNodes();
	this->NumberOfHandles		= this->NumberOfTerminals+this->NumberOfNodes;

	///////////////////////////////////////////
 	//Atualiza o grid de nodes
	vtkDebugMacro(<<"Update grid of nodes.");

 	if ( !this->GridNode )
 		this->GridNode = vtkHM1DStraightModelGrid::New();

 	vtkPoints *points = this->GridLine->GetPoints();
 	this->Points->DeepCopy(points);
 	vtkPoints *newPoints = vtkPoints::New();

 	vtkCellArray *verts = vtkCellArray::New();
 	vtkCellArray *nodes = vtkCellArray::New();

 	nodes->DeepCopy(this->GridLine->GetNodes());

 	// Inseri os pontos e os vertices
 	for ( int i=0; i<this->NumberOfPoints; i++ )
	 	{
	 	newPoints->InsertNextPoint(points->GetPoint(i));
	 	verts->InsertNextCell(1,&i);
	 	}

	// Seta o tamanho dos arrays
	verts->SetNumberOfCells(this->NumberOfPoints);
	newPoints->SetNumberOfPoints(this->NumberOfPoints);

 	this->GridNode->SetPoints(newPoints);
 	this->GridNode->SetVerts(verts);
 	this->GridNode->SetNodes(nodes);

 	verts->Delete();
 	newPoints->Delete();
 	nodes->Delete();

 	///////////////////////////////////////////
 	//Atualiza o grid de terminais
 	vtkDebugMacro(<<"Update grid of terminals.");

 	if ( !this->GridTerminal )
 		this->GridTerminal = vtkHM1DStraightModelGrid::New();

 	vtkCellArray *term = this->GridLine->GetTerminals();
 	vtkCellArray *newTerminal = vtkCellArray::New();

 	newPoints = vtkPoints::New();

 	verts = vtkCellArray::New();

 	vtkIdType id[3];

	vtkIdType *p;
	this->GridTerminal->RemoveTerminalInformation();
	int numberOfTerminals = this->GridLine->GetTerminals()->GetNumberOfCells();

 	// Inseri os pontos e os vertices
 	for ( int i=0; i<numberOfTerminals; i++ )
	 	{
	 	id[0] = i;
	 	p = this->GridLine->GetTerminal(i);

	 	newPoints->InsertNextPoint(points->GetPoint(p[0]));
	 	id[1] = p[1];
	 	id[2] = p[2];
	 	verts->InsertNextCell(1,id);
	 	newTerminal->InsertNextCell(3,id);

	 	this->GridTerminal->InsertTerminalInformation(id[1],id[0]);
	 	}

	// Seta o tamanho dos arrays
	verts->SetNumberOfCells(numberOfTerminals);
	newPoints->SetNumberOfPoints(numberOfTerminals);
	newTerminal->SetNumberOfCells(numberOfTerminals);

 	this->GridTerminal->SetPoints(newPoints);
 	this->GridTerminal->SetVerts(verts);
 	this->GridTerminal->SetTerminals(newTerminal);

 	verts->Delete();
 	newPoints->Delete();
 	newTerminal->Delete();

 	///////////////////////////////////////////
 	//Atualiza o grid de null elements 1D 3D
 	vtkDebugMacro(<<"Update grid of null element 1D 3D.");

 	if ( !this->GridNullElements1D3D )
 		this->GridNullElements1D3D = vtkHM1DStraightModelGrid::New();

 	vtkCellArray *lines = vtkCellArray::New();

 	newPoints = vtkPoints::New();

 	verts = vtkCellArray::New();

 	vtkIdType ids[4];

 	vtkHM1DStraightModel::NullElementsMap nullElements = this->StraightModel->GetNullElements1D3DMap();
 	vtkHM1DStraightModel::NullElementsMap::iterator it = nullElements.begin();

	vtkHM1DSegment *segm;
	vtkIdList *list;
	double pt[3];
	while ( it != nullElements.end() )
		{
		segm = this->StraightModel->GetSegment(it->first);

		if ( segm )
			{
			list = this->StraightModel->GetNullElements1D3DList(it->first);
			pt[0] = segm->GetNodeData(list->GetId(0))->coords[0];
			pt[1] = segm->GetNodeData(list->GetId(0))->coords[1];
			pt[2] = segm->GetNodeData(list->GetId(0))->coords[2];

			ids[0] = newPoints->InsertNextPoint(pt);
			verts->InsertNextCell(1,ids);

			for ( int i=1; i<list->GetNumberOfIds(); i++ )
				{
				pt[0] = segm->GetNodeData(list->GetId(i))->coords[0];
				pt[1] = segm->GetNodeData(list->GetId(i))->coords[1];
				pt[2] = segm->GetNodeData(list->GetId(i))->coords[2];

				ids[1] = newPoints->InsertNextPoint(pt);
				verts->InsertNextCell(1,ids);
				lines->InsertNextCell(2,ids);
				ids[0] = ids[1];
				}
			pt[0] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[0];
			pt[1] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[1];
			pt[2] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[2];

			ids[1] = newPoints->InsertNextPoint(pt);
			verts->InsertNextCell(1,ids);
			lines->InsertNextCell(2,ids);
			}
		it++;
		}

	// Seta o tamanho dos arrays
//	verts->SetNumberOfCells(numberOfTerminals);
//	newPoints->SetNumberOfPoints(numberOfTerminals);
//	newTerminal->SetNumberOfCells(numberOfTerminals);

 	this->GridNullElements1D3D->SetPoints(newPoints);
 	this->GridNullElements1D3D->SetVerts(verts);
 	this->GridNullElements1D3D->SetLines(lines);

 	verts->Delete();
 	newPoints->Delete();
 	lines->Delete();

 	this->HighlightLine(0);

	this->UpdateActors();
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::UpdateGridSelected()
{
	vtkIdType *info;
	double p1[3], p2[3];

	vtkDebugMacro(<<"UpdateGridSelected.");

	vtkHM1DStraightModelGrid::InformationMap nodeMap = this->GridLineSelected->GetNodeInformation();
	vtkHM1DStraightModelGrid::InformationMap::iterator it = nodeMap.begin();
	vtkIdList *list;

	//Se modo de visualizacao de segmento
	if ( this->GetSegment_Selected() )
		{
		if ( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT) )
			{
			info = this->GridLine->GetTerminalInformation(this->StraightModel->GetSegment(this->SegmentSelected)->GetFirstParent()->GetId());
			this->GridLine->GetPoints()->GetPoint(info[0], p1);

			this->BeginSphereSourceSelected->SetCenter(p1);

			info = this->GridLine->GetTerminalInformation(this->StraightModel->GetSegment(this->SegmentSelected)->GetFirstChild()->GetId());
			this->GridLine->GetPoints()->GetPoint(info[0], p1);
			this->EndSphereSourceSelected->SetCenter(p1);
			}
		}
	//Se modo de visualizacao de elemento e segmento selecionado
	else if (( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT)
			|| (this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM))
			&& (this->StraightModelElementSelected != -1))
		{
		//Pego informacoes do elemento clicado, primeiro e segundo ponto relacionado com o elemento
		info = this->GridLine->GetElementInformation(this->SegmentSelected, this->ElementSelected);

		//Pego o primeiro ponto
		this->GridLine->GetPoints()->GetPoint(info[0], p1);
		this->GridLineSelected->GetPoints()->SetPoint(0, p1);

		//Pego o segundo ponto
		this->GridLine->GetPoints()->GetPoint(info[1], p2);
		this->GridLineSelected->GetPoints()->SetPoint(1, p2);
		}

  this->GridLineSelected->GetPoints()->Reset();
  this->GridLineSelected->GetLines()->Reset();
  this->GridLineSelected->GetNodes()->Reset();

	while ( it != nodeMap.end() )
		{
    this->AddSegmentInMainTreeColor(it->first);
		it++;
		}

  if( this->GetStraightModelMode() == MODE_TODETACHSUBTREE_VAL )
    {
    for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
      this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid

    this->HighlightLine(1);
    }

	this->GridLineSelected->GetCellData()->Update();
	this->GridLineSelected->Modified();
	this->GridLineSelected->Update();
	this->GridLineSelected->BuildCells();
	this->GridLineSelected->BuildLinks();

	this->GridTerminalSelected->GetCellData()->Update();
	this->GridTerminalSelected->Modified();
	this->GridTerminalSelected->Update();
	this->GridTerminalSelected->BuildCells();
	this->GridTerminalSelected->BuildLinks();
}

//----------------------------------------------------------------
char *vtkHMStraightModelWidget::GetSegmentsID()
{
	return this->StraightModel->GetSegmentsID();
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::CloneSegment(char *orig, char *target)
{
	vtkHM1DStraightModel::TreeNodeDataMap segMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it = segMap.begin();

	vtkHM1DSegment *segOrig, *segTarget;

	vtkDebugMacro(<<"CloneSegment.");

	while ( it != segMap.end() )
		{
		segOrig = vtkHM1DSegment::SafeDownCast(it->second);

		if ( !strcmp(segOrig->GetSegmentName(), orig) )
			break;

		it++;
		}

	if ( it == segMap.end() )
		{
		vtkWarningMacro("Original segment not found!");
		return;
		}

	it = segMap.begin();

	while ( it != segMap.end() )
		{
		segTarget = vtkHM1DSegment::SafeDownCast(it->second);

		if ( !strcmp(segTarget->GetSegmentName(), target) )
			break;

		it++;
		}

	if ( it == segMap.end() )
		{
		vtkWarningMacro("Target segment not found!");
		return;
		}

	int numberOfElements;

	if ( segOrig->GetElementNumber() > segTarget->GetElementNumber() )
		numberOfElements = segTarget->GetElementNumber();
	else
		numberOfElements = segOrig->GetElementNumber();

	vtkHMElementData *elemOrig, *elemTarget;
	vtkHMNodeData *nodeOrig, *nodeTarget;

	for ( int i=0; i<numberOfElements; i++ )
		{
		//copy element
		elemOrig = segOrig->GetElementData(i);
		elemTarget = segTarget->GetElementData(i);

		elemTarget->Elastin									= elemOrig->Elastin;
   	elemTarget->Collagen								= elemOrig->Collagen;
	 	elemTarget->CoefficientA						= elemOrig->CoefficientA;
		elemTarget->CoefficientB						= elemOrig->CoefficientB;
		elemTarget->Viscoelasticity					= elemOrig->Viscoelasticity;
		elemTarget->ViscoelasticityExponent	= elemOrig->ViscoelasticityExponent;
		elemTarget->InfiltrationPressure		= elemOrig->InfiltrationPressure;
		elemTarget->ReferencePressure				= elemOrig->ReferencePressure;
		elemTarget->Permeability						= elemOrig->Permeability;
		elemTarget->WallThickness						= elemOrig->WallThickness;

		//Copy node
		nodeOrig = segOrig->GetNodeData(i);
		nodeTarget = segTarget->GetNodeData(i);

		nodeTarget->area          = nodeOrig->area;
  	nodeTarget->radius        = nodeOrig->radius;
		nodeTarget->alfa      		= nodeOrig->alfa;
		}

	nodeOrig = segOrig->GetNodeData(numberOfElements);
	nodeTarget = segTarget->GetNodeData(numberOfElements);

	nodeTarget->area          = nodeOrig->area;
	nodeTarget->radius        = nodeOrig->radius;
	nodeTarget->alfa      		= nodeOrig->alfa;

	segTarget->SetisViscoelastic(segOrig->GetisViscoelastic());
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::ConfigureScale(double v, char n)
{
	vtkDebugMacro(<<"ConfigureScale.");
	int numberOfPts = this->GridLine->GetNumberOfPoints();
	int numberOfNodes;

	double *pts, *pts2;
	double p1[3], p2[3];

	vtkPoints *points = this->GridLine->GetPoints();
	vtkPoints *pointsOfNode = this->GridNode->GetPoints();
	vtkPoints *pointsOfTerminal = this->GridTerminal->GetPoints();

	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap segMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	vtkIdType *info, *info2;

	switch (n)
		{
		case 'x':
			vtkDebugMacro(<<"Configure scale x.");
			it = segMap.begin();
			while ( it != segMap.end() )
				{
				numberOfNodes = vtkHM1DSegment::SafeDownCast(it->second)->GetNodeNumber();

				for ( int i=0; i<numberOfNodes; i++ )
					{
					//Pega informação do ponto ao qual o no esta relacionado
					info = this->GridLine->GetNodeInformation(it->first, i);

					//Pega o ponto original
					pts = this->Points->GetPoint(info[0]);

					//Pega o ponto atual do grid
					pts2 = points->GetPoint(info[0]);
					pts[0] = pts[0] * v; //multiplica pelo fator de escala para o eixo x
					pts[1] = pts2[1];
					pts[2] = pts2[2];
					points->SetPoint(info[0], pts);
					pointsOfNode->SetPoint(info[0], pts);

					//Seta a nova coordenada no node do segmento
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[0] = pts[0];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[1] = pts[1];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[2] = pts[2];
					}

				it++;
				}
			it = termMap.begin();
			//Configura o centro da esfera do coração
			info = this->GridLine->GetTerminalInformation(it->first);
			info2 = this->GridTerminal->GetTerminalInformation(it->first);
			pts = this->GridLine->GetPoint(info[0]);
			this->HeartGeometry->SetCenter(pts);

			//Atualiza os pontos do grid de terminais
			while ( it != termMap.end() )
				{
				info = this->GridLine->GetTerminalInformation(it->first);
				info2 = this->GridTerminal->GetTerminalInformation(it->first);

				pts = this->GridLine->GetPoint(info[0]);
				pointsOfTerminal->SetPoint(info2[0], pts);
				it++;
				}

			break;

		case 'y':
			vtkDebugMacro(<<"Configure scale y.");
			it = segMap.begin();
			while ( it != segMap.end() )
				{
				numberOfNodes = vtkHM1DSegment::SafeDownCast(it->second)->GetNodeNumber();

				for ( int i=0; i<numberOfNodes; i++ )
					{
					//Pega informação do ponto ao qual o no esta relacionado
					info = this->GridLine->GetNodeInformation(it->first, i);

					//Pega o ponto original
					pts = this->Points->GetPoint(info[0]);
					//Pega o ponto atual do grid
					pts2 = points->GetPoint(info[0]);
					pts[1] = pts[1] * v; //multiplica pelo fator de escala para o eixo y
					pts[0] = pts2[0];
					pts[2] = pts2[2];
					points->SetPoint(info[0], pts);
					pointsOfNode->SetPoint(info[0], pts);

					//Seta a nova coordenada no node do segmento
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[0] = pts[0];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[1] = pts[1];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[2] = pts[2];
					}

				it++;
				}
			it = termMap.begin();
			//Configura o centro da esfera do coração
			info = this->GridLine->GetTerminalInformation(it->first);
			info2 = this->GridTerminal->GetTerminalInformation(it->first);
			pts = this->GridLine->GetPoint(info[0]);
			this->HeartGeometry->SetCenter(pts);

			//Atualiza os pontos do grid de terminais
			while ( it != termMap.end() )
				{
				info = this->GridLine->GetTerminalInformation(it->first);
				info2 = this->GridTerminal->GetTerminalInformation(it->first);

				pts = this->GridLine->GetPoint(info[0]);
				pointsOfTerminal->SetPoint(info2[0], pts);
				it++;
				}

			break;

		case 'z':
			vtkDebugMacro(<<"Configure scale z.");
			it = segMap.begin();
			while ( it != segMap.end() )
				{
				numberOfNodes = vtkHM1DSegment::SafeDownCast(it->second)->GetNodeNumber();

				for ( int i=0; i<numberOfNodes; i++ )
					{
					//Pega informação do ponto ao qual o no esta relacionado
					info = this->GridLine->GetNodeInformation(it->first, i);

					//Pega o ponto original
					pts = this->Points->GetPoint(info[0]);
					//Pega o ponto atual do grid
					pts2 = points->GetPoint(info[0]);
					pts[2] = pts[2] * v; //multiplica pelo fator de escala para o eixo z
					pts[0] = pts2[0];
					pts[1] = pts2[1];
					points->SetPoint(info[0], pts);
					pointsOfNode->SetPoint(info[0], pts);

					//Seta a nova coordenada no node do segmento
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[0] = pts[0];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[1] = pts[1];
					vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[2] = pts[2];
					}

				it++;
				}
			it = termMap.begin();

			//Configura o centro da esfera do coração
			info = this->GridLine->GetTerminalInformation(it->first);
			info2 = this->GridTerminal->GetTerminalInformation(it->first);
			pts = this->GridLine->GetPoint(info[0]);
			this->HeartGeometry->SetCenter(pts);

			//Atualiza os pontos do grid de terminais
			while ( it != termMap.end() )
				{
				info = this->GridLine->GetTerminalInformation(it->first);
				info2 = this->GridTerminal->GetTerminalInformation(it->first);

				pts = this->GridLine->GetPoint(info[0]);
				pointsOfTerminal->SetPoint(info2[0], pts);
				it++;
				}

			break;

		} // End swicth (n)

	this->UpdateActors();
	this->UpdateGridSelected();
}

//----------------------------------------------------------------
void vtkHMStraightModelWidget::ConfigureScale(double sizeX, double sizeY, double sizeZ)
{
	vtkDebugMacro(<<"ConfigureScale.");

	int numberOfPts = this->GridLine->GetNumberOfPoints();
	int numberOfNodes;

	double *pts, *pts2;
	vtkPoints *points = this->GridLine->GetPoints();
	vtkPoints *pointsOfNode = this->GridNode->GetPoints();
	vtkPoints *pointsOfTerminal = this->GridTerminal->GetPoints();

	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap segMap =  this->StraightModel->GetSegmentsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	vtkIdType *info, *info2;

	it = segMap.begin();
	while ( it != segMap.end() )
		{
		numberOfNodes = vtkHM1DSegment::SafeDownCast(it->second)->GetNodeNumber();

		for ( int i=0; i<numberOfNodes; i++ )
			{
			//Pega informação do ponto ao qual o no esta relacionado
			info = this->GridLine->GetNodeInformation(it->first, i);
			//Pega o ponto original
			pts = this->Points->GetPoint(info[0]);
			//Pega o ponto atual do grid
			pts2 = points->GetPoint(info[0]);
			pts[0] = pts[0] * sizeX;
			pts[1] = pts[1] * sizeY;
			pts[2] = pts[2] * sizeZ;

			points->SetPoint(info[0], pts);
			pointsOfNode->SetPoint(info[0], pts);

			//Seta a nova coordenada no node do segmento
			vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[0] = pts[0];
			vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[1] = pts[1];
			vtkHM1DSegment::SafeDownCast(it->second)->GetNodeData(i)->coords[2] = pts[2];
			}

		it++;
		}
	it = termMap.begin();

	//Configura o centro da esfera do coração
	info = this->GridLine->GetTerminalInformation(it->first);
	info2 = this->GridTerminal->GetTerminalInformation(it->first);
	pts = this->GridLine->GetPoint(info[0]);
	this->HeartGeometry->SetCenter(pts);

	//Atualiza os pontos do grid de terminais
	while ( it != termMap.end() )
		{
		info = this->GridLine->GetTerminalInformation(it->first);
		info2 = this->GridTerminal->GetTerminalInformation(it->first);

		pts = this->GridLine->GetPoint(info[0]);
		pointsOfTerminal->SetPoint(info2[0], pts);
		it++;
		}

	this->UpdateActors();
	this->UpdateGridSelected();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::SetSizePerAxle(double size[3])
{
	this->SizePerAxle[0] = size[0];
	this->SizePerAxle[1] = size[1];
	this->SizePerAxle[2] = size[2];
}

//---------------------------------------------------------------------------------------
double *vtkHMStraightModelWidget::GetSizePerAxle()
{
	return this->SizePerAxle;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::SetTerminals(int option, double R1, double R2, double Cap, double Pr)
{
	if (!option)
		this->StraightModel->SetNonLeafTerminalsDefaultValues(R1, R2, Cap, Pr);
	else
		this->StraightModel->SetLeafTerminalsDefaultValues(R1, R2, Cap, Pr);
}

//---------------------------------------------------------------------------------------
//Desconectar elementos da arvore no caso de arraste de algum elemento
void vtkHMStraightModelWidget::DisconnectElement()
{
	//Segmento selecionado
	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(this->StraightModelElementSelected);
	//terminal parent
	vtkHM1DTreeElement *parent = Segm->GetFirstParent();

	double p1[3];
	vtkIdType newTerminal;

	//Pego o terminal parent
	while ( !this->StraightModel->IsTerminal(parent->GetId()) )
		parent = Segm->GetNextParent();

  //Se parent for um terminal com mais de um filho, ou o parent tiver parent,
	//adicionar um terminal como pai do segmento arrastado.
  if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )
  	{
	  //Seta no primeiro node do segmento.
	  p1[0] = Segm->GetNodeData(0)->coords[0];
		p1[1] = Segm->GetNodeData(0)->coords[1];
		p1[2] = Segm->GetNodeData(0)->coords[2];

		vtkIdType *info;
		//Pego informação do primeiro node do segmento
		info = this->GridLine->GetNodeInformation(Segm->GetId(), 0);

  	//Adiciona um terminal na coordenada do primeiro node
		newTerminal = this->AddTerminalWidget(p1, 0, info[0]);
		this->AddTerminalWidget(p1, newTerminal);

	  vtkHM1DTerminal *Term = this->StraightModel->GetTerminal(newTerminal);

	  //remove o segmento como filho do parent e adiciona o novo terminal como parent
	  parent->RemoveChild(Segm);
	  Segm->RemoveParent(parent);
	  Term->AddChild(Segm);

	  if ( parent->GetFirstParent() )
		  {
		  //Pega a ultima coordenada do segmento parent
		  int n = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeNumber();
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[2];
		  }
	  else
		  {
		  //Pega a primeira coordenada do segmento filho
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[2];

		  info = this->GridLine->GetNodeInformation(parent->GetFirstChild()->GetId(), 0);
		  this->GridLine->SetTerminalPoint(parent->GetId(), info[0]);
		  }

		//Seta a coordenada do terminal
	  info = this->GridTerminal->GetTerminalInformation(parent->GetId());
	  this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	} //Fim if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )

	//Pego o terminal filho
	vtkHM1DTreeElement *elem = Segm->GetFirstChild();
	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		elem = Segm->GetNextChild();

	//Se o terminal filho possui algum filho e algum parent, ele possui segmento(s) como filho e como pai,
	//sendo assim, é adicionado um novo terminal como filho do segmento arrastado
  if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )
  	{
  	//Remove o segm como parent do terminal e o terminal como filho do segmento selecionado
  	elem->RemoveParent(Segm);
  	Segm->RemoveChild(elem);

  	//Troca o elemento selecionado para no momento da adição do novo terminal
  	//a associação na tabela seja com o segmento movido.
  	this->StraightModelElementSelected = 0;

  	vtkIdType *info;
  	//Pega informação do ultimo node do segmento arrastado para que o
  	//novo terminal seja adicionado no mesmo ponto
  	info = this->GridLine->GetNodeInformation(Segm->GetId(), Segm->GetElementNumber());

  	//Pega a coordenada
  	double *pt = this->GridLine->GetPoint(info[0]);

  	//Adiciona o terminal no ultimo ponto do segmento
  	newTerminal = this->AddTerminalWidget(pt, 0, info[0]);
  	this->AddTerminalWidget(pt, newTerminal);

  	//Adiciona o novo terminal como filho do segmento e segmento como parent do terminal
  	Segm->AddChild(this->StraightModel->GetTerminal(newTerminal));

		vtkHM1DTreeElement *elem2 = elem->GetFirstParent();
  	vtkIdType *info2;

  	//Se o terminal não possui mais parent, pego o segmento filho e a
  	//primeira coordenada e o id do ponto
  	if ( !elem2 )
	  	{
	  	elem2 = elem->GetFirstChild();

	  	p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[0];
			p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[1];
			p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[2];

	  	info2 = this->GridLine->GetNodeInformation(elem2->GetId(), 0);
	  	}
	  //Se o terminal possui parent, pego a ultima coordenada e o id do ponto
	  else
	  	{
	  	int n = vtkHM1DSegment::SafeDownCast(elem2)->GetElementNumber();
	  	//Seta no ultimo node do segmento.
		  p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[0];
			p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[1];
			p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[2];

			info2 = this->GridLine->GetNodeInformation(elem2->GetId(), n);
	  	}

		//Pego informações do terminal no grid de terminais para setar a coordenada dele
  	info = this->GridTerminal->GetTerminalInformation(elem->GetId());
  	this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	//Pego informações do terminal no grid de linhas para setar o id do ponto
  	//que esta relacionado com ele
  	info = this->GridLine->GetTerminalInformation(elem->GetId());
  	this->GridLine->SetTerminalPoint(elem->GetId(), info2[0]);

  	}  //Fim if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )

  //Se o terminal filho não possui filhos mas possui mais que um parent, adicionar
  //um novo terminal como filho do segmento arrastado
  else if ( (elem->GetChildCount() == 0) && (elem->GetNumberOfParents() > 1) )
  	{
  	//Remove o segm como parent do terminal e o terminal como filho do segmento selecionado
  	elem->RemoveParent(Segm);
  	Segm->RemoveChild(elem);

  	//Troca o elemento selecionado para no momento da adição do novo terminal
  	//a associação na tabela seja com o segmento movido.
  	this->StraightModelElementSelected = 0;

  	vtkIdType *info;
  	//Pega informação do ultimo node do segmento arrastado para que o
  	//novo terminal seja adicionado no mesmo ponto
  	info = this->GridLine->GetNodeInformation(Segm->GetId(), Segm->GetElementNumber());

  	//Pega a coordenada
  	double *pt = this->GridLine->GetPoint(info[0]);

  	//Adiciona o terminal no ultimo ponto do segmento
  	newTerminal = this->AddTerminalWidget(pt, 0, info[0]);
  	this->AddTerminalWidget(pt, newTerminal);

  	//Adiciona o novo terminal como filho do segmento e segmento como parent do terminal
  	Segm->AddChild(this->StraightModel->GetTerminal(newTerminal));

		vtkHM1DTreeElement *elem2;
  	vtkIdType *info2;

  	elem2 = elem->GetFirstParent();

  	//Seta no ultimo node do segmento.
	  int n = vtkHM1DSegment::SafeDownCast(elem2)->GetElementNumber();
	  p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[0];
		p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[1];
		p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[2];

		//pego a ultima coordenada e o id do ponto
		info2 = this->GridLine->GetNodeInformation(elem2->GetId(), n);

  	//Pego informações do terminal no grid de terminais para setar a coordenada dele
  	info = this->GridTerminal->GetTerminalInformation(elem->GetId());
  	this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	//Pego informações do terminal no grid de linhas para setar o id do ponto
  	//que esta relacionado com ele
  	info = this->GridLine->GetTerminalInformation(elem->GetId());
  	this->GridLine->SetTerminalPoint(elem->GetId(), info2[0]);

  	}  //Fim if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )
}

//---------------------------------------------------------------------------------------
//Desconectar elementos da arvore no caso de arraste de algum elemento
void vtkHMStraightModelWidget::DisconnectElement(vtkIdType idSegment)
{
	//Segmento selecionado
	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(idSegment);
	//terminal parent
	vtkHM1DTreeElement *parent = Segm->GetFirstParent();

	double p1[3];
	vtkIdType newTerminal;

	//Pego o terminal parent
	while ( !this->StraightModel->IsTerminal(parent->GetId()) )
		parent = Segm->GetNextParent();

  //Se parent for um terminal com mais de um filho, ou o parent tiver parent,
	//adicionar um terminal como pai do segmento arrastado.
  if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )
  	{
	  //Seta no primeiro node do segmento.
	  p1[0] = Segm->GetNodeData(0)->coords[0];
		p1[1] = Segm->GetNodeData(0)->coords[1];
		p1[2] = Segm->GetNodeData(0)->coords[2];

		vtkIdType *info;
		//Pego informação do primeiro node do segmento
		info = this->GridLine->GetNodeInformation(Segm->GetId(), 0);

  	//Adiciona um terminal na coordenada do primeiro node
		newTerminal = this->AddTerminalWidget(p1, 0, info[0]);
		this->AddTerminalWidget(p1, newTerminal);

	  vtkHM1DTerminal *Term = this->StraightModel->GetTerminal(newTerminal);

	  //remove o segmento como filho do parent e adiciona o novo terminal como parent
	  parent->RemoveChild(Segm);
	  Segm->RemoveParent(parent);
	  Term->AddChild(Segm);

	  if ( parent->GetFirstParent() )
		  {
		  //Pega a ultima coordenada do segmento parent
		  int n = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeNumber();
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[2];
		  }
	  else
		  {
		  //Pega a primeira coordenada do segmento filho
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[2];

		  info = this->GridLine->GetNodeInformation(parent->GetFirstChild()->GetId(), 0);
		  this->GridLine->SetTerminalPoint(parent->GetId(), info[0]);
		  }

		//Seta a coordenada do terminal
	  info = this->GridTerminal->GetTerminalInformation(parent->GetId());
	  this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	} //Fim if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )

	//Pego o terminal filho
	vtkHM1DTreeElement *elem = Segm->GetFirstChild();
	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		elem = Segm->GetNextChild();

	//Se o terminal filho possui algum filho e algum parent, ele possui segmento(s) como filho e como pai,
	//sendo assim, é adicionado um novo terminal como filho do segmento arrastado
  if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )
  	{
  	//Remove o segm como parent do terminal e o terminal como filho do segmento selecionado
  	elem->RemoveParent(Segm);
  	Segm->RemoveChild(elem);

  	//Troca o elemento selecionado para no momento da adição do novo terminal
  	//a associação na tabela seja com o segmento movido.
  	this->StraightModelElementSelected = 0;

  	vtkIdType *info;
  	//Pega informação do ultimo node do segmento arrastado para que o
  	//novo terminal seja adicionado no mesmo ponto
  	info = this->GridLine->GetNodeInformation(Segm->GetId(), Segm->GetElementNumber());

  	//Pega a coordenada
  	double *pt = this->GridLine->GetPoint(info[0]);

  	//Adiciona o terminal no ultimo ponto do segmento
  	newTerminal = this->AddTerminalWidget(pt, 0, info[0]);
  	this->AddTerminalWidget(pt, newTerminal);

  	//Adiciona o novo terminal como filho do segmento e segmento como parent do terminal
  	Segm->AddChild(this->StraightModel->GetTerminal(newTerminal));

		vtkHM1DTreeElement *elem2 = elem->GetFirstParent();
  	vtkIdType *info2;

  	//Se o terminal não possui mais parent, pego o segmento filho e a
  	//primeira coordenada e o id do ponto
  	if ( !elem2 )
	  	{
	  	elem2 = elem->GetFirstChild();

	  	p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[0];
			p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[1];
			p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(0)->coords[2];

	  	info2 = this->GridLine->GetNodeInformation(elem2->GetId(), 0);
	  	}
	  //Se o terminal possui parent, pego a ultima coordenada e o id do ponto
	  else
	  	{
	  	int n = vtkHM1DSegment::SafeDownCast(elem2)->GetElementNumber();
	  	//Seta no ultimo node do segmento.
		  p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[0];
			p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[1];
			p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[2];

			info2 = this->GridLine->GetNodeInformation(elem2->GetId(), n);
	  	}

		//Pego informações do terminal no grid de terminais para setar a coordenada dele
  	info = this->GridTerminal->GetTerminalInformation(elem->GetId());
  	this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	//Pego informações do terminal no grid de linhas para setar o id do ponto
  	//que esta relacionado com ele
  	info = this->GridLine->GetTerminalInformation(elem->GetId());
  	this->GridLine->SetTerminalPoint(elem->GetId(), info2[0]);

  	}  //Fim if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )

  //Se o terminal filho não possui filhos mas possui mais que um parent, adicionar
  //um novo terminal como filho do segmento arrastado
  else if ( (elem->GetChildCount() == 0) && (elem->GetNumberOfParents() > 1) )
  	{
  	//Remove o segm como parent do terminal e o terminal como filho do segmento selecionado
  	elem->RemoveParent(Segm);
  	Segm->RemoveChild(elem);

  	//Troca o elemento selecionado para no momento da adição do novo terminal
  	//a associação na tabela seja com o segmento movido.
  	this->StraightModelElementSelected = 0;

  	vtkIdType *info;
  	//Pega informação do ultimo node do segmento arrastado para que o
  	//novo terminal seja adicionado no mesmo ponto
  	info = this->GridLine->GetNodeInformation(Segm->GetId(), Segm->GetElementNumber());

  	//Pega a coordenada
  	double *pt = this->GridLine->GetPoint(info[0]);

  	//Adiciona o terminal no ultimo ponto do segmento
  	newTerminal = this->AddTerminalWidget(pt, 0, info[0]);
  	this->AddTerminalWidget(pt, newTerminal);

  	//Adiciona o novo terminal como filho do segmento e segmento como parent do terminal
  	Segm->AddChild(this->StraightModel->GetTerminal(newTerminal));

		vtkHM1DTreeElement *elem2;
  	vtkIdType *info2;

  	elem2 = elem->GetFirstParent();

  	//Seta no ultimo node do segmento.
	  int n = vtkHM1DSegment::SafeDownCast(elem2)->GetElementNumber();
	  p1[0] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[0];
		p1[1] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[1];
		p1[2] = vtkHM1DSegment::SafeDownCast(elem2)->GetNodeData(n)->coords[2];

		//pego a ultima coordenada e o id do ponto
		info2 = this->GridLine->GetNodeInformation(elem2->GetId(), n);

  	//Pego informações do terminal no grid de terminais para setar a coordenada dele
  	info = this->GridTerminal->GetTerminalInformation(elem->GetId());
  	this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	//Pego informações do terminal no grid de linhas para setar o id do ponto
  	//que esta relacionado com ele
  	info = this->GridLine->GetTerminalInformation(elem->GetId());
  	this->GridLine->SetTerminalPoint(elem->GetId(), info2[0]);

  	}  //Fim if ( (elem->GetChildCount() >= 1) && (elem->GetNumberOfParents() >= 1) )
}

//---------------------------------------------------------------------------------------
//Conecta terminais no caso de movimentação de um terminal e este seja solto em cima
//de outro terminal ou movimento de um segmento e o terminal pai e/ou filho esteja
//colidindo com outro terminal
//t1 = terminal que esta sendo movimentado
//t2 = terminal parado
void vtkHMStraightModelWidget::ConnectElement(vtkIdType t1, vtkIdType t2)
{
	vtkIdType *info, *info2;

	vtkHM1DTerminal *term1 = this->StraightModel->GetTerminal(t1);
	vtkHM1DTerminal *term2 = this->StraightModel->GetTerminal(t2);

	double p[3], pt1[3], pt2[3];
	//Pego informacoes do terminal movimentado e o terminal ao qual será conectado
	info = this->GridLine->GetTerminalInformation(t1);
	info2 = this->GridLine->GetTerminalInformation(t2);

	//Pego as coordenadas dos terminais
	this->GridLine->GetPoints()->GetPoint(info[0], pt1);
	this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

	//calcula o deslocamento que será realizado pelo segmento até o
	//terminal, para ajustar os segmentos e o terminal no mesmo ponto
	p[0] = pt2[0] - pt1[0];
	p[1] = pt2[1] - pt1[1];
	p[2] = pt2[2] - pt1[2];

	vtkHM1DTreeElement *elem = term1->GetFirstParent();

	while ( elem )
		{
		//retirar do segmento o filho terminal
		elem->RemoveChild(term1);
		term1->RemoveParent(elem);

		//adicionar como filho do segmento o terminal conectado
		elem->AddChild(term2);
		this->MoveParent(p, elem->GetId());

		elem = term1->GetFirstParent();
		}

	elem = term1->GetFirstChild();
	while ( elem )
		{
		elem->RemoveParent(term1);
		term1->RemoveChild(elem);
		term2->AddChild(elem);

		this->MoveChild(p, elem->GetId());
		elem = term1->GetFirstChild();
		}

	//remover o terminal selecionado do straightModelGrid e do straightModel
	this->GridLine->RemoveTerminalCell(t1);
	this->GridTerminal->RemoveTerminalCell(t1);
	this->StraightModel->RemoveTerminal(t1, 0);
}

//---------------------------------------------------------------------------------------
//Verifica se ao soltar o botao do mouse, existe colisao entre o terminal selecionado
//e os demais terminais
vtkIdType vtkHMStraightModelWidget::DetectCollisionBetweenTerminals()
{
	vtkIdType *info, *info2;
	double distance = 100000, currentDistance;
	double pt1[3], pt2[3];
	vtkIdType idTerminal = -1;

	if ( !this->StraightModel->IsTerminal(this->TerminalSelected) )
		return -1;

	//Pego informacoes do terminal selecionado
	info = this->GridLine->GetTerminalInformation(this->TerminalSelected);

	this->GridLine->GetPoints()->GetPoint(info[0],pt1);

	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	it = termMap.begin();
	//Procurar se existe colisao entre terminais
	while ( it != termMap.end() )
		{
		//Verifica se o terminal verificado e o mesmo selecionado, se forem diferentes,
		//verifico se ha colisao
		if ( (it->first != this->TerminalSelected) )
			{
			info2 = this->GridLine->GetTerminalInformation(it->first);

			this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

			currentDistance = this->ComputeVectorLength(pt1, pt2);

			if ( currentDistance < (2*this->RadiusNode) )
				{
				if ( currentDistance < distance )
					{
					idTerminal = it->first;
					distance = currentDistance;
					}
				}

			} //End if ( it->first != this->TerminalSelected )

		it++;
		} //End while ( it != termMap.end() )

	return idTerminal;
}

//---------------------------------------------------------------------------------------
vtkIdType vtkHMStraightModelWidget::DetectCollisionBetweenTerminals(vtkIdType t)
{
	vtkIdType *info, *info2;
	double distance = 100000, currentDistance;
	double pt1[3], pt2[3];
	vtkIdType idTerminal = -1;

	if ( !this->StraightModel->IsTerminal(t) )
		return -1;

	//Pego informacoes do terminal recebido como parametro
	info = this->GridLine->GetTerminalInformation(t);

	this->GridLine->GetPoints()->GetPoint(info[0],pt1);

	vtkHM1DStraightModel::TreeNodeDataMap termMap =  this->StraightModel->GetTerminalsMap();
	vtkHM1DStraightModel::TreeNodeDataMap::iterator it;

	it = termMap.begin();
	//Procurar se existe colisao entre terminais
	while ( it != termMap.end() )
		{
		//Verifica se o terminal verificado é o mesmo recebido, se forem diferentes,
		//verifico se ha colisao
		if ( (it->first != t) )
			{
			info2 = this->GridLine->GetTerminalInformation(it->first);

			this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

			currentDistance = this->ComputeVectorLength(pt1, pt2);

			if ( currentDistance < (2*this->RadiusNode) )
				{
				if ( currentDistance < distance )
					{
					idTerminal = it->first;
					distance = currentDistance;
					}
				}

			} //End if ( it->first != this->TerminalSelected )

		it++;
		} //End while ( it != termMap.end() )

	return idTerminal;
}

//---------------------------------------------------------------------------------------
//Verifica se existe colisao entre os terminais
bool vtkHMStraightModelWidget::DetectCollisionBetweenTerminals(vtkIdType t1, vtkIdType t2)
{
	vtkIdType *info, *info2;
	double distance;
	double pt1[3], pt2[3];

	//Verifica se os dois ids recebido como parametros, sao terminais
	if ( !this->StraightModel->IsTerminal(t1) || !this->StraightModel->IsTerminal(t2) )
		return false;

	if ( t1 == t2 )
		return false;

	//Pego informacoes do terminal
	info = this->GridLine->GetTerminalInformation(t1);
	this->GridLine->GetPoints()->GetPoint(info[0],pt1);

	//Pego informacoes do terminal
	info2 = this->GridLine->GetTerminalInformation(t2);
	this->GridLine->GetPoints()->GetPoint(info2[0], pt2);

	distance = this->ComputeVectorLength(pt1, pt2);

	if ( distance < (2*this->RadiusNode) )
		{
		return true;
		}
	else
		return false;
}

//---------------------------------------------------------------------------------------
//Colorir a sub-arvore selecionada, a partir do segmento ou terminal selecionado
void vtkHMStraightModelWidget::MakeSubTreeColor(vtkIdType id, vtkIdList *idListSegm, vtkIdList *idListTerm)
{
	vtkHM1DTreeElement *elem;
	vtkHM1DTreeElement *child;

	if ( !idListSegm )
		idListSegm = vtkIdList::New();

	if ( !idListTerm )
		idListTerm = vtkIdList::New();

	if (( idListSegm->IsId(id) != -1 )||( idListTerm->IsId(id) != -1 ))
		return;

	if (this->StraightModel->IsSegment(id))
		{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		idListSegm->InsertNextId(id);
		}
	else if (this->StraightModel->IsTerminal(id))
		{
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
		if (id!=1)
			idListTerm->InsertNextId(id);
		}

	if (!child)
		{
		// É uma folha
		return;
		}
	else
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->MakeSubTreeColor(id, idListSegm, idListTerm);
			}

}

//---------------------------------------------------------------------------------------
//Colorir a arvore principal, para destacar caso tenha sub-arvore que nao sera salva.
void vtkHMStraightModelWidget::ToDetachSubTree()
{
	vtkIdList *subListSegm = vtkIdList::New();
	vtkIdList *subListTerm = vtkIdList::New();

	//Se for Stent passa o objeto stent senão passa o objeto de segmento ou terminal
	if ( (this->StraightModelObjectSelected == STENT) || (this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
		this->MakeSubTreeColor(this->SegmentSelected, subListSegm, subListTerm);
	else
		this->MakeSubTreeColor(this->StraightModelElementSelected, subListSegm, subListTerm);
	this->HighlightLine(0);
	this->HighlightHandles(0);

  // Se as duas listas estiverem vazias enche as duas
	if((this->SubTreeListSegments->GetNumberOfIds() == 0)&&(this->SubTreeListTerminals->GetNumberOfIds() == 0))
		{
		this->SubTreeListSegments->DeepCopy(subListSegm);// copia as sublistas para as listas globais
		this->SubTreeListTerminals->DeepCopy(subListTerm);
		}
	//Se uma das duas estiver vazia e a outra cheia, adiciona apenas os que não tem na lista
	else if( (this->SubTreeListSegments->GetNumberOfIds() == 0)||(this->SubTreeListTerminals->GetNumberOfIds() == 0) )
		{
		if(!this->Interactor->GetControlKey())//Se Control NÃO Pressionado
			this->ClearSubTreeLists();

		for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
			this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o SEGMENTO que ainda não tem
		for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
			this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
		}
	else//Se nenhuma lista está vazia
		{
		if(this->Interactor->GetControlKey())//Se Control Pressionado
			{
			//Se for um SEGMENTO e ele estiver na lista
			if((this->SegmentSelected != -1)&&(this->SubTreeListSegments->IsId(this->SegmentSelected) != -1))
				{
				this->SubTreeListSegments->DeleteId(this->SegmentSelected);//Deleto o SEGMENTO selecionado na lista global
				subListSegm->DeleteId(this->SegmentSelected);//Deleto o SEGMENTO selecionado na lista auxiliar
				}
			//Se for um SEGMENTO e ele NÃO estiver na lista
			else if((this->SegmentSelected != -1)&&(this->SubTreeListSegments->IsId(this->SegmentSelected) == -1))
				{
				for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
					this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o SEGMENTO que ainda não tem
				for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
					this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
				}
			//Se for um TERMINAL e ele estiver na lista
			else if((this->TerminalSelected != -1)&&(this->SubTreeListTerminals->IsId(this->TerminalSelected) != -1))
				{
				this->SubTreeListTerminals->DeleteId(this->TerminalSelected);//Deleto o TERMINAL selecionado na lista global
				subListTerm->DeleteId(this->TerminalSelected);//Deleto o TERMINAL selecionado na lista auxiliar
				}
			//Se for um TERMINAL e ele NÃO estiver na lista
			else if((this->TerminalSelected != -1)&&(this->SubTreeListTerminals->IsId(this->TerminalSelected) == -1))
				{
				for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
					this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
				for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
					this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
				}
			}
		//Se control NÃO pressionado
		else if(!this->Interactor->GetControlKey())
			{
			this->ClearSubTreeLists();
			this->SubTreeListSegments->DeepCopy(subListSegm);// copia a sublista de SEGMENTOS para a lista global
			this->SubTreeListTerminals->DeepCopy(subListTerm);// copia a sublista de TERMINAIS para a lista global
			}
		}
	for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
		this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
	//for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro a lista global de TERMINAIS
	this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid

	subListSegm->Delete();
	subListTerm->Delete();

	this->HighlightLine(1);
	this->HighlightHandles(1);
}

//---------------------------------------------------------------------------------------
//De-selecionar uma sub-árvore de uma sub-árvore selecionada.
//Após selecionar uma sub-árvore(A) esse método de-seleciona uma sub-árvore(B) dentro da sub-árvore(A)
void vtkHMStraightModelWidget::ToDetachSubTreeErase()
{
	//se uma das listas estiver vazia não executa o método
	if((this->SubTreeListSegments->GetNumberOfIds() == 0)||(this->SubTreeListTerminals->GetNumberOfIds() == 0))
		return;

	//criando listas auxiliares
	vtkIdList *subListSegm = vtkIdList::New();
	vtkIdList *subListTerm = vtkIdList::New();

	//Se for Stent passa o objeto stent senão passa o objeto de segmento ou terminal
	if ( (this->StraightModelObjectSelected == STENT) || (this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
		this->MakeSubTreeColor(this->SegmentSelected, subListSegm, subListTerm);
	else
		this->MakeSubTreeColor(this->StraightModelElementSelected, subListSegm, subListTerm);

	//Me certifico de descolorir todo mundo
	this->HighlightLine(0);
	this->HighlightHandles(0);

	//Preenchendo as listas globais
	for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
		this->SubTreeListSegments->DeleteId(subListSegm->GetId(i));
	for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
		this->SubTreeListTerminals->DeleteId(subListTerm->GetId(i));

	//Adicionando os elementos das listas globais nos respectivos grids
	for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
		this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
	//for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro a lista global de TERMINAIS
	this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid

	//Deletando as listas auxiliares
	subListSegm->Delete();
	subListTerm->Delete();

	//Colorir todo mundo
	this->HighlightLine(1);
	this->HighlightHandles(1);
}

//---------------------------------------------------------------------------------------
//Colorir a arvore principal, para destacar caso tenha sub-arvore que nao sera salva.
void vtkHMStraightModelWidget::MakeMainTreeColor(vtkIdType id, vtkIdList *idList)
{
	vtkHM1DTreeElement *elem;
	vtkHM1DTreeElement *child;

	if ( !idList )
		idList = vtkIdList::New();

	if ( idList->IsId(id) != -1 )
		return;

	if (this->StraightModel->IsSegment(id))
		{
		elem = this->StraightModel->GetSegment(id);
		child = elem->GetFirstChild();
		this->AddSegmentInMainTreeColor( elem->GetId() );
		idList->InsertNextId(id);
		}
	else if (this->StraightModel->IsTerminal(id))
		{
		elem = this->StraightModel->GetTerminal(id);
		child = elem->GetFirstChild();
		idList->InsertNextId(id);
		}

	if (!child)
		{
		// É uma folha
		return;
		}
	else
		while (child) // Caso o child possua irmãos, a função deverá ser chamada para cada um deles.
			{
			id = child->GetId();
			child = elem->GetNextChild();
			this->MakeMainTreeColor(id, idList);
			}
}

//---------------------------------------------------------------------------------------
//Adicionar um novo segmento para colorir a arvore principal
void vtkHMStraightModelWidget::AddSegmentInMainTreeColor(vtkIdType idSegment)
{
	double *p1;
  double p[3];
  vtkIdType *info, idNode[3], idCellNode;

	int numberOfPoints = this->StraightModel->GetSegment(idSegment)->GetNodeNumber();
	idNode[2] = idSegment;

	vtkIdType idPts[2];
	vtkCellArray *line = this->GridLineSelected->GetLines();
	vtkPoints *point = this->GridLineSelected->GetPoints();
	vtkCellArray *nodes	= this->GridLineSelected->GetNodes();

	if ( (!point) || (!line) || (!nodes) )
		{
		line = vtkCellArray::New();
		point = vtkPoints::New();
		nodes	= vtkCellArray::New();

		this->GridLineSelected->SetPoints(point);
		this->GridLineSelected->SetLines(line);
		this->GridLineSelected->SetNodes(nodes);

		point->Delete();
		line->Delete();
		nodes->Delete();

		line = this->GridLineSelected->GetLines();
		point = this->GridLineSelected->GetPoints();
		nodes	= this->GridLineSelected->GetNodes();
		}

	p[0] = this->StraightModel->GetSegment(idSegment)->GetNodeData(0)->coords[0];
	p[1] = this->StraightModel->GetSegment(idSegment)->GetNodeData(0)->coords[1];
	p[2] = this->StraightModel->GetSegment(idSegment)->GetNodeData(0)->coords[2];

	idNode[0] = idPts[0] = point->InsertNextPoint(p);
	idNode[1] = 0;

	idCellNode = nodes->InsertNextCell(3, idNode);
	vtkIdList *nodeList = vtkIdList::New();
	nodeList->InsertNextId(idCellNode);

	for ( int i=1; i<numberOfPoints; i++ )
		{
		p[0] = this->StraightModel->GetSegment(idSegment)->GetNodeData(i)->coords[0];
		p[1] = this->StraightModel->GetSegment(idSegment)->GetNodeData(i)->coords[1];
		p[2] = this->StraightModel->GetSegment(idSegment)->GetNodeData(i)->coords[2];

		idNode[0] = idPts[1] = point->InsertNextPoint(p);
		idNode[1] = i;

		idCellNode = nodes->InsertNextCell(3, idNode);

		nodeList->InsertNextId(idCellNode);

		line->InsertNextCell(2, idPts);
		idPts[0] = idPts[1];
		}

	//Remove do map as informacoes das celulas associadas ao segmento.
	this->GridLineSelected->RemoveNodeInformation(idSegment);
	//Inserir no map informacoes das celulas associadas ao segmento para que
	//facilite e agilize a busca pelo elemento.
	this->GridLineSelected->InsertNodeInformation(idSegment,nodeList);
}

//---------------------------------------------------------------------------------------
//Adicionar um novo terminal para colorir a arvore principal
void vtkHMStraightModelWidget::AddTerminalInMainTreeColor(vtkIdType idTerminal)
{

	if ( !this->GridTerminalSelected )
		this->GridTerminalSelected = vtkHM1DStraightModelGrid::New();

	vtkCellArray *newTerminal = vtkCellArray::New();     // Armazenará os novos terminais que serão incluidos
	vtkCellArray *verts = vtkCellArray::New(); //armazena os vertices dos novos terminais
	vtkPoints *points = this->GridLine->GetPoints();//Armazena os ids dos pontos
	vtkPoints *newPoints = vtkPoints::New(); //armazena os pontos dos novos terminais
	vtkIdType id[3];//informações do id dentro do grid 0 = id do ponto no grid, 1 = id real do ponto(chave), 2 = id do pai
	vtkIdType *p;//armazenará informações do terminal que será inserido no grid

	this->GridTerminalSelected->RemoveTerminalInformation();

	for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro s lista global
		{
	 	p = this->GridLine->GetTerminalInformation(this->SubTreeListTerminals->GetId(i));
	 	id[0] = newPoints->InsertNextPoint(points->GetPoint(p[0]));//retorna o id do ponto. InsertNextPoint gera coordenadas X,Y,Z retorna um id
	 	id[1] = p[1];//id do terminal
	 	id[2] = p[2];//id do pai para uma possível busca
	 	verts->InsertNextCell(1,id);
	 	newTerminal->InsertNextCell(3,id);

	 	this->GridTerminalSelected->InsertTerminalInformation(id[1],id[0]);//inserindo no MAP
		}

	// Seta o tamanho dos arrays
	verts->SetNumberOfCells(this->SubTreeListTerminals->GetNumberOfIds());
	newPoints->SetNumberOfPoints(this->SubTreeListTerminals->GetNumberOfIds());
	newTerminal->SetNumberOfCells(this->SubTreeListTerminals->GetNumberOfIds());

	this->GridTerminalSelected->SetPoints(newPoints);
	this->GridTerminalSelected->SetVerts(verts);
	this->GridTerminalSelected->SetTerminals(newTerminal);

	verts->Delete();
	//points->Delete();
	newPoints->Delete();
	newTerminal->Delete();

	this->UpdateActors();
}


//---------------------------------------------------------------------------------------
// Calcula comprimento do vetor
double vtkHMStraightModelWidget::ComputeVectorLength(double *p1, double *p2)
{
  double squarelen;
  double length;

  double cat = (p2[0] - p1[0]);
  squarelen = cat * cat;

  cat = (p2[1] - p1[1]);
  squarelen += cat * cat;

  cat = (p2[2] - p1[2]);
  squarelen += cat * cat;

  length = sqrt(squarelen);
  return length;
}

//---------------------------------------------------------------------------------------
//Inverter a direcao do fluxo
void vtkHMStraightModelWidget::InvertFlowDirection()
{
	vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SegmentSelected);

	vtkHM1DTreeElement *parent = segm->GetFirstParent();
	vtkHM1DTreeElement *child = segm->GetFirstChild();

	segm->RemoveParent(parent);
	parent->RemoveChild(segm);
	segm->RemoveChild(child);
	child->RemoveParent(segm);
	segm->AddChild(parent);
	child->AddChild(segm);

	this->GridLine->UpdateGrid();
	this->UpdateGrid();
}

//---------------------------------------------------------------------------------------
//Dividir um segmento em dois a partir do node passado.
void vtkHMStraightModelWidget::DivideSegment(vtkIdType idSegment, vtkIdType idNode)
{
	double x[3], p[3];

	vtkCellArray *Lines = this->GridLine->GetLines();
	vtkIdType *pt2, *info;

	// pega a coordenada da node clicado para setar o novo segmento
	info = this->GridLine->GetNodeInformation(idSegment, idNode);
	this->GridLine->GetPoint(info[0], x);

	//Pega o segmento clicado e o terminal filho
	vtkHM1DSegment *segm 	= this->StraightModel->GetSegment(idSegment);
	vtkHM1DTerminal *term;
	vtkHM1DTreeElement *elem;

	elem = segm->GetFirstChild();

	//Procura pelo terminal filho do segmento
	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		elem = segm->GetNextChild();

	term = vtkHM1DTerminal::SafeDownCast(elem);

	int numberOfElements 	= segm->GetElementNumber();
	int numberOfNodes 		= segm->GetNodeNumber();

	//Adicionar um segmento como filho do terminal filho do segmento, o novo segmento
	//representa o restante do segmento após o node clicado.
	vtkIdType newSeg = this->AddLineWidget(x, numberOfElements-idNode, term->GetId());

	//Pega o novo segmento e o terminal filho
	vtkHM1DSegment *newSegment 	= this->StraightModel->GetSegment(newSeg);
	vtkHM1DTreeElement *newTerminal = newSegment->GetFirstChild();

	vtkHM1DTerminal::SafeDownCast(newTerminal)->DeepCopy(term);

	//Setar valores default de terminal não folha
	vtkHM1DTerminal::ResistanceValuesList R;
	R.push_back(100000000);
	term->SetR1(&R);
	term->SetR2(&R);
	R.clear();
	R.push_back(0);
	term->SetC(&R);
	term->SetPt(&R);
	R.clear();

	int nodeRemove = idNode+1; //node que sera removido do segmento
	int elementRemove = idNode; //elemento que sera removido do segmento

	//Copia os dados do node clicado para o primeiro node do novo segmento.
	newSegment->GetNodeData(0)->area          = segm->GetNodeData(idNode)->area;
	newSegment->GetNodeData(0)->radius        = segm->GetNodeData(idNode)->radius;
	newSegment->GetNodeData(0)->alfa     			= segm->GetNodeData(idNode)->alfa;
	newSegment->GetNodeData(0)->meshID      	= segm->GetNodeData(idNode)->meshID;
	newSegment->GetNodeData(0)->PercentageToCalculateStenosis      = segm->GetNodeData(idNode)->PercentageToCalculateStenosis;
	newSegment->GetNodeData(0)->PercentageToCalculateAneurysm      = segm->GetNodeData(idNode)->PercentageToCalculateAneurysm;

	for ( int i=0; i<numberOfElements-idNode; i++ )
		{
		//Setar o id do ponto a ser removido
		pt2 = this->GridLine->GetNodeInformation(idSegment, nodeRemove);

		this->GridLine->GetPoints()->GetPoint(pt2[0], p);

		pt2 = this->GridLine->GetNodeInformation(newSeg, i+1);

		this->GridLine->GetPoints()->SetPoint(pt2[0], p);

		//copiando dados dos nodes
		newSegment->GetNodeData(i+1)->coords[0] = p[0];
		newSegment->GetNodeData(i+1)->coords[1] = p[1];
		newSegment->GetNodeData(i+1)->coords[2] = p[2];
		newSegment->GetNodeData(i+1)->area          = segm->GetNodeData(nodeRemove)->area;
		newSegment->GetNodeData(i+1)->radius        = segm->GetNodeData(nodeRemove)->radius;
		newSegment->GetNodeData(i+1)->alfa     			= segm->GetNodeData(nodeRemove)->alfa;
		newSegment->GetNodeData(i+1)->meshID      	= segm->GetNodeData(nodeRemove)->meshID;
		newSegment->GetNodeData(i+1)->PercentageToCalculateStenosis      = segm->GetNodeData(nodeRemove)->PercentageToCalculateStenosis;
		newSegment->GetNodeData(i+1)->PercentageToCalculateAneurysm      = segm->GetNodeData(nodeRemove)->PercentageToCalculateAneurysm;

		//copiando dados dos elementos
		newSegment->GetElementData(i)->Elastin = segm->GetElementData(elementRemove)->Elastin;
		newSegment->GetElementData(i)->Collagen = segm->GetElementData(elementRemove)->Collagen;
		newSegment->GetElementData(i)->CoefficientA = segm->GetElementData(elementRemove)->CoefficientA;
		newSegment->GetElementData(i)->CoefficientB = segm->GetElementData(elementRemove)->CoefficientB;
		newSegment->GetElementData(i)->Viscoelasticity = segm->GetElementData(elementRemove)->Viscoelasticity;
		newSegment->GetElementData(i)->ViscoelasticityExponent = segm->GetElementData(elementRemove)->ViscoelasticityExponent;
		newSegment->GetElementData(i)->InfiltrationPressure = segm->GetElementData(elementRemove)->InfiltrationPressure;
		newSegment->GetElementData(i)->ReferencePressure = segm->GetElementData(elementRemove)->ReferencePressure;
		newSegment->GetElementData(i)->Permeability = segm->GetElementData(elementRemove)->Permeability;
		newSegment->GetElementData(i)->WallThickness = segm->GetElementData(elementRemove)->WallThickness;
		newSegment->GetElementData(i)->idNode[0] = segm->GetElementData(elementRemove)->idNode[0];
		newSegment->GetElementData(i)->idNode[1] = segm->GetElementData(elementRemove)->idNode[1];
		newSegment->GetElementData(i)->meshID = segm->GetElementData(elementRemove)->meshID;
		newSegment->GetElementData(i)->ElastinStentFactor = segm->GetElementData(elementRemove)->ElastinStentFactor;
		newSegment->GetElementData(i)->ViscoelasticityStentFactor = segm->GetElementData(elementRemove)->ViscoelasticityStentFactor;

		segm->RemoveNodeData(nodeRemove);
		segm->RemoveElementData(elementRemove);

		nodeRemove++;
		elementRemove++;
		}

	//Adiciona o terminal no grid de terminais no ultimo ponto do segmento
	this->AddTerminalWidget(p, newTerminal->GetId());

	elem = term->GetFirstChild();

	vtkIdList *elemRemove = vtkIdList::New();  //Lista para remocao de elementos do straightModel

	//pega os elementos filhos do segmento clicado e os adiciona como
	//filhos do novo segmento
	while ( elem )
		{
		if ( elem->GetId() != newSeg )
			{
			newTerminal->AddChild(elem);
			elemRemove->InsertNextId(elem->GetId());
			}
		elem = term->GetNextChild();
		}

	//remove os filhos deixando apenas o terminal e o novo segmento
	for( int i=0; i<elemRemove->GetNumberOfIds(); i++ )
		{
		term->GetChild(elemRemove->GetId(i))->RemoveParent(term);
		term->RemoveChild(term->GetChild(elemRemove->GetId(i)));
		}

	elemRemove->Reset();
	elem = term->GetFirstParent();
	//pego os parents e adiciono como filhos o novo terminal, é o caso
	//de um termial com mais de um pai
	while ( elem )
		{
		if ( elem->GetId() != idSegment )
			{
			elem->AddChild(newTerminal);
			elemRemove->InsertNextId(elem->GetId());
			}
		elem = term->GetNextParent();
		}

	//remove do terminal os parents que foram adicionado ao terminal logo acima
	for( int i=0; i<elemRemove->GetNumberOfIds(); i++ )
		{
		term->GetParent(elemRemove->GetId(i))->RemoveChild(term);
		term->RemoveParent(term->GetParent(elemRemove->GetId(i)));
		}

	elemRemove->Delete();

	/************************************************************************************
	 *
	 * Verifica se no segmento dividido tem algum stent, caso tenha algum stent depois
	 * do node de divisao, atualizar as informações do(s) stent(s) que irão ficar no
	 * novo segmento.
	 *
	 ************************************************************************************/
	//Pega stents existentes no segmento
	vtkIdList *stentList = this->GridStent->GetStentsInSegment(idSegment);
	//Verifica se a lista com os ids dos stent está cheia
	if ( stentList )
		{
		vtkIdList *elementList;
		//Percorre a lista  verificando se cada stent está depois do node clicado
		for ( int i=0; i<stentList->GetNumberOfIds(); i++ )
			{
			elementList = this->GridStent->GetStent(stentList->GetId(i));
			//Verifica se o id do elemento do stent é maior que o id do node
			if ( elementList->GetId(0) > idNode )
				{
				vtkIdList *newList = vtkIdList::New();
				vtkDoubleArray *pointArray = vtkDoubleArray::New();
				pointArray->SetNumberOfComponents(3);
				//Petar o id do ponto
				pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(0)-idNode);
				this->GridLine->GetPoints()->GetPoint(pt2[0], p);
				pointArray->InsertNextTuple3(p[0], p[1], p[2]);

				for ( int j=0; j<elementList->GetNumberOfIds()-1; j++ )
					{
					//Petar o id do ponto
					pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(j)-idNode);

					this->GridLine->GetPoints()->GetPoint(pt2[0], p);
					pointArray->InsertNextTuple3(p[0], p[1], p[2]);

					newList->InsertUniqueId(elementList->GetId(j)-idNode);
					}
				newList->InsertNextId(newSeg);
				this->GridStent->ReplaceStent(stentList->GetId(i), newList, pointArray);
				pointArray->Delete();
				}
			}
		stentList->Delete();
		}

	/************************************************************************************
	 *
	 * Verifica se no segmento dividido tem algum estenoses, caso tenha algum estenoses depois
	 * do node de divisao, atualizar as informações do(s) estenoses que irão ficar no
	 * novo segmento.
	 *
	 ************************************************************************************/
	//Pega stenosis existentes no segmento
	vtkIdList *stenosisList = this->GridStenosis->GetStenosisInSegment(idSegment);
	//Verifica se a lista com os ids dos stenosis está cheia
	if ( stenosisList )
		{
		vtkIdList *elementList;
		//Percorre a lista  verificando se cada stenosis está depois do node clicado
		for ( int i=0; i<stenosisList->GetNumberOfIds(); i++ )
			{
			elementList = this->GridStenosis->GetStenosis(stenosisList->GetId(i));
			//Verifica se o id do elemento do stenosis é maior que o id do node
			if ( elementList->GetId(0) > idNode )
				{
				vtkIdList *newList = vtkIdList::New();
				vtkDoubleArray *pointArray = vtkDoubleArray::New();
				pointArray->SetNumberOfComponents(3);
				//Petar o id do ponto
				pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(0)-idNode);
				this->GridLine->GetPoints()->GetPoint(pt2[0], p);
				pointArray->InsertNextTuple3(p[0], p[1], p[2]);

				for ( int j=0; j<elementList->GetNumberOfIds()-1; j++ )
					{
					//Petar o id do ponto
					pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(j)-idNode);

					this->GridLine->GetPoints()->GetPoint(pt2[0], p);
					pointArray->InsertNextTuple3(p[0], p[1], p[2]);

					newList->InsertUniqueId(elementList->GetId(j)-idNode);
					}
				newList->InsertNextId(newSeg);
				this->GridStenosis->ReplaceStenosis(stenosisList->GetId(i), newList, pointArray);
				pointArray->Delete();
				}
			}
		stenosisList->Delete();
		}

	/************************************************************************************
	 *
	 * Verifica se no segmento dividido tem algum aneurisma, caso tenha algum aneurisma
	 * depois do node de divisao, atualizar as informações do(s) aneurisma(s) que irão
	 * ficar no novo segmento.
	 *
	 ************************************************************************************/
	//Pega aneurismas existentes no segmento
	vtkIdList *aneurysmList = this->GridAneurysm->GetAneurysmInSegment(idSegment);
	//Verifica se a lista com os ids dos aneurismas está cheia
	if ( aneurysmList )
		{
		vtkIdList *elementList;
		//Percorre a lista  verificando se cada aneurisma está depois do node clicado
		for ( int i=0; i<aneurysmList->GetNumberOfIds(); i++ )
			{
			elementList = this->GridAneurysm->GetAneurysm(aneurysmList->GetId(i));
			//Verifica se o id do elemento do aneurisma é maior que o id do node
			if ( elementList->GetId(0) > idNode )
				{
				vtkIdList *newList = vtkIdList::New();
				vtkDoubleArray *pointArray = vtkDoubleArray::New();
				pointArray->SetNumberOfComponents(3);
				//Petar o id do ponto
				pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(0)-idNode);
				this->GridLine->GetPoints()->GetPoint(pt2[0], p);
				pointArray->InsertNextTuple3(p[0], p[1], p[2]);

				for ( int j=0; j<elementList->GetNumberOfIds()-1; j++ )
					{
					//Petar o id do ponto
					pt2 = this->GridLine->GetElementInformation(newSeg, elementList->GetId(j)-idNode);

					this->GridLine->GetPoints()->GetPoint(pt2[0], p);
					pointArray->InsertNextTuple3(p[0], p[1], p[2]);

					newList->InsertUniqueId(elementList->GetId(j)-idNode);
					}
				newList->InsertNextId(newSeg);
				this->GridAneurysm->ReplaceAneurysm(aneurysmList->GetId(i), newList, pointArray);
				pointArray->Delete();
				}
			}
		aneurysmList->Delete();
		}
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::GenerateCouplingRepresentation(int NumberOfActors)
{
	// se a representacao dos atores de acoplamento já foi feita; retornar
	if (this->TextCollection->GetNumberOfItems())
		return;

	for (int i = 0; i < NumberOfActors; ++i)
		{
		vtkActor *a = vtkActor::New();
		vtkPolyDataMapper *m = vtkPolyDataMapper::New();
		vtkVectorText *t 	 = vtkVectorText::New();
		a->GetProperty()->SetColor(1,0,0);

		vtkSphereSource *s = vtkSphereSource::New();
		vtkActor *sa = vtkActor::New();
		vtkPolyDataMapper *ms = vtkPolyDataMapper::New();

		s->SetRadius(1.0);
	  ms->SetInput(s->GetOutput());
		sa->SetMapper(ms);

		char temp[50];
		sprintf(temp, "%d", i+1);
		t->SetText(temp);
	  m->SetInput(t->GetOutput());
		a->SetMapper(m);

		this->CouplingSphereSourceCollection->AddItem(s);
		this->SphereActorCollection->AddItem(sa);
		this->CouplingSphereMapperCollection->AddItem(ms);

		this->TextCollection->AddItem(t);
		this->MapperCollection->AddItem(m);
		this->ActorCollection->AddItem(a);

		t->Delete();
		m->Delete();
		a->Delete();

		sa->Delete();
		ms->Delete();
		s->Delete();
		}
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::IncrementCouplingVar(int var)
{
	this->CouplingActorCounter++;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::RemoveCouplingActor(int ActorNumber)
{
	vtkActor *a = vtkActor::SafeDownCast(this->ActorCollection->GetItemAsObject(ActorNumber));

	vtkActor *sa = vtkActor::SafeDownCast(this->SphereActorCollection->GetItemAsObject(ActorNumber));

	if (this->CheckIfActorIsOnTheRender(sa) && this->CurrentRenderer)
		this->CurrentRenderer->RemoveActor(sa);

	if (this->CheckIfActorIsOnTheRender(a) && this->CurrentRenderer)
		this->CurrentRenderer->RemoveActor(a);

	this->Interactor->Render();

	if (this->CouplingActorCounter > 0)
		this->CouplingActorCounter--;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::RemoveAllCouplingActors()
{
	for (int i = 0; i < this->ActorCollection->GetNumberOfItems(); ++i)
		{
		vtkActor *a = NULL;
		// removendo numeros
		a = vtkActor::SafeDownCast(this->ActorCollection->GetItemAsObject(i));
		if (this->CheckIfActorIsOnTheRender(a) && this->CurrentRenderer)
			this->CurrentRenderer->RemoveActor(a);

		a = NULL;

		// removendo esferas
		a = vtkActor::SafeDownCast(this->SphereActorCollection->GetItemAsObject(i));

		if (this->CheckIfActorIsOnTheRender(a) && this->CurrentRenderer)
			this->CurrentRenderer->RemoveActor(a);
		}

	// já deleta todos objetos interiores
	this->ActorCollection->RemoveAllItems();
	this->TextCollection->RemoveAllItems();
	this->MapperCollection->RemoveAllItems();

	this->CouplingSphereSourceCollection->RemoveAllItems();
	this->SphereActorCollection->RemoveAllItems();
	this->CouplingSphereMapperCollection->RemoveAllItems();

	this->CouplingActorCounter = 0;

	this->FullModelCode = 0;

	this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
bool vtkHMStraightModelWidget::CheckIfActorIsOnTheRender(vtkActor *a)
{
	if (!this->CurrentRenderer)
		return false;

	vtkActorCollection *RenderActorCollection = this->CurrentRenderer->GetActors();
	for (int i = 0; i < RenderActorCollection->GetNumberOfItems(); ++i)
		{
		if (a == vtkActor::SafeDownCast(RenderActorCollection->GetItemAsObject(i)))
			return true;
		}
	return false;
}

//---------------------------------------------------------------------------------------
//Atualiza os grids e atores
void vtkHMStraightModelWidget::UpdateActors()
{
	//Atualiza os grids
	this->GridLine->GetCellData()->Update();
	this->GridLine->Modified();
  this->GridLine->Update();

  this->GridNode->GetCellData()->Update();
	this->GridNode->Modified();
  this->GridNode->Update();

  this->GridTerminal->GetCellData()->Update();
	this->GridTerminal->Modified();
  this->GridTerminal->Update();

  this->GridLine->BuildCells();
	this->GridLine->BuildLinks();
	this->GridNode->BuildCells();
	this->GridNode->BuildLinks();
	this->GridTerminal->BuildCells();
	this->GridTerminal->BuildLinks();

  this->GridTerminalMapper->SetInput(this->GridTerminal);
 	//Ator
 	this->GridTerminalActor->SetMapper(this->GridTerminalMapper);

	this->GridTerminalActor->SetProperty(this->GridTerminalProperty);

	this->UpdateGridNullElements1D3D();
	if ( this->GridNullElements1D3D )
		{
	  this->GridNullElements1D3D->GetCellData()->Update();
		this->GridNullElements1D3D->Modified();
	  this->GridNullElements1D3D->Update();
		this->GridNullElements1D3D->BuildCells();
		this->GridNullElements1D3D->BuildLinks();
		}
  this->GridNullElements1D3DMapper->SetInput(this->GridNullElements1D3D);
 	//Ator
 	this->GridNullElements1D3DActor->SetMapper(this->GridNullElements1D3DMapper);

	this->GridNullElements1D3DActor->SetProperty(this->GridNullElements1D3DProperty);

	this->GridClip->GetCellData()->Update();
	this->GridClip->Modified();
	this->GridClip->Update();
	this->GridClip->BuildCells();
	this->GridClip->BuildLinks();

	this->UpdateGridStent();
	this->GridStent->GetCellData()->Update();
	this->GridStent->Modified();
	this->GridStent->Update();
	this->GridStent->BuildCells();
	this->GridStent->BuildLinks();

	//Grid Stenosis
	this->UpdateGridStenosis();
	this->GridStenosis->GetCellData()->Update();
	this->GridStenosis->Modified();
	this->GridStenosis->Update();
	this->GridStenosis->BuildCells();
	this->GridStenosis->BuildLinks();


	//Grid Aneurysm
	this->UpdateGridAneurysm();
	this->GridAneurysm->GetCellData()->Update();
	this->GridAneurysm->Modified();
	this->GridAneurysm->Update();
	this->GridAneurysm->BuildCells();
	this->GridAneurysm->BuildLinks();

	this->GridLineSelected->GetCellData()->Update();
	this->GridLineSelected->Modified();
  this->GridLineSelected->Update();
  this->GridLineSelected->BuildCells();
	this->GridLineSelected->BuildLinks();


  this->GridTerminalSelected->GetCellData()->Update();
	this->GridTerminalSelected->Modified();
  this->GridTerminalSelected->Update();
	this->GridTerminalSelected->BuildCells();
	this->GridTerminalSelected->BuildLinks();

	this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
//Atualiza as coordenadas do grid de stent
void vtkHMStraightModelWidget::UpdateGridStent()
{
	vtkIdType *info, *info2;
	double p1[3], p2[3];

	vtkIdList *list, *listPoint;

	if ( this->GridStent->GetNumberOfStent() > 0 )
		{
		for ( int i=0; i<this->GridStent->GetNumberOfStent(); i++ )
			{
			//Pegar informações dos elementos associados com o stent
			list = this->GridStent->GetStent(i);
			//Pega lista com os ids dos pontos do stent
			listPoint = this->GridStent->GetCell(i)->GetPointIds();
			for ( int j=0; j<list->GetNumberOfIds()-1; j++ )
				{
				//Pegar informações dos pontos, elemento e segmento do grid de linhas
				info2 = this->GridLine->GetElementInformation(list->GetId(list->GetNumberOfIds()-1), list->GetId(j));

				//Pegar coordenadas 1 e 2 do elemento
				this->GridLine->GetPoints()->GetPoint(info2[0], p1);
				this->GridLine->GetPoints()->GetPoint(info2[1], p2);

				//Setar as coordenadas no grid de stent
				this->GridStent->GetPoints()->SetPoint(listPoint->GetId(j), p1);
				this->GridStent->GetPoints()->SetPoint(listPoint->GetId(j+1), p2);
				}
			}
		}
}

//---------------------------------------------------------------------------------------
//Atualiza as coordenadas do grid de stenosis
void vtkHMStraightModelWidget::UpdateGridStenosis()
{
	vtkIdType *info, *info2;
	double p1[3], p2[3];

	vtkIdList *list, *listPoint;

	if ( this->GridStenosis->GetNumberOfStenosis() > 0 )
		{
		for ( int i=0; i<this->GridStenosis->GetNumberOfStenosis(); i++ )
			{
			//Pegar informações dos elementos associados com o stenosis
			list = this->GridStenosis->GetStenosis(i);
			//Pega lista com os ids dos pontos do stenosis
			listPoint = this->GridStenosis->GetCell(i)->GetPointIds();
			for ( int j=0; j<list->GetNumberOfIds()-1; j++ )
				{
				//Pegar informações dos pontos, elemento e segmento do grid de linhas
				info2 = this->GridLine->GetElementInformation(list->GetId(list->GetNumberOfIds()-1), list->GetId(j));

				//Pegar coordenadas 1 e 2 do elemento
				this->GridLine->GetPoints()->GetPoint(info2[0], p1);
				this->GridLine->GetPoints()->GetPoint(info2[1], p2);

				//Setar as coordenadas no grid de stenosis
				this->GridStenosis->GetPoints()->SetPoint(listPoint->GetId(j), p1);
				this->GridStenosis->GetPoints()->SetPoint(listPoint->GetId(j+1), p2);
				}
			}
		}
}

//---------------------------------------------------------------------------------------
//Atualiza as coordenadas do grid de aneurysm
void vtkHMStraightModelWidget::UpdateGridAneurysm()
{
	vtkIdType *info, *info2;
	double p1[3], p2[3];

	vtkIdList *list, *listPoint;

	if ( this->GridAneurysm->GetNumberOfAneurysm() > 0 )
		{
		for ( int i=0; i<this->GridAneurysm->GetNumberOfAneurysm(); i++ )
			{
			//Pegar informações dos elementos associados com o aneurysm
			list = this->GridAneurysm->GetAneurysm(i);
			//Pega lista com os ids dos pontos do aneurysm
			listPoint = this->GridAneurysm->GetCell(i)->GetPointIds();
			for ( int j=0; j<list->GetNumberOfIds()-1; j++ )
				{
				//Pegar informações dos pontos, elemento e segmento do grid de linhas
				info2 = this->GridLine->GetElementInformation(list->GetId(list->GetNumberOfIds()-1), list->GetId(j));

				//Pegar coordenadas 1 e 2 do elemento
				this->GridLine->GetPoints()->GetPoint(info2[0], p1);
				this->GridLine->GetPoints()->GetPoint(info2[1], p2);

				//Setar as coordenadas no grid de aneurysm
				this->GridAneurysm->GetPoints()->SetPoint(listPoint->GetId(j), p1);
				this->GridAneurysm->GetPoints()->SetPoint(listPoint->GetId(j+1), p2);
				}
			}
		}
}

//---------------------------------------------------------------------------------------
//Atualiza as coordenadas do grid de elementos nulos do modelo acoplado
void vtkHMStraightModelWidget::UpdateGridNullElements1D3D()
{
	///////////////////////////////////////////
 	//Atualiza o grid de null elements 1D 3D
 	vtkDebugMacro(<<"Update grid of null element 1D 3D.");

 	if ( !this->GridNullElements1D3D )
 		this->GridNullElements1D3D = vtkHM1DStraightModelGrid::New();

 	vtkCellArray *lines = vtkCellArray::New();

 	vtkPoints *newPoints = vtkPoints::New();

 	vtkCellArray *verts = vtkCellArray::New();

 	vtkIdType ids[4];

 	vtkHM1DStraightModel::NullElementsMap nullElements = this->StraightModel->GetNullElements1D3DMap();
 	vtkHM1DStraightModel::NullElementsMap::iterator it = nullElements.begin();

	vtkHM1DSegment *segm;
	vtkIdList *list;
	double pt[3];
	while ( it != nullElements.end() )
		{
		segm = this->StraightModel->GetSegment(it->first);

		if ( segm )
			{
			list = this->StraightModel->GetNullElements1D3DList(it->first);
			pt[0] = segm->GetNodeData(list->GetId(0))->coords[0];
			pt[1] = segm->GetNodeData(list->GetId(0))->coords[1];
			pt[2] = segm->GetNodeData(list->GetId(0))->coords[2];

			ids[0] = newPoints->InsertNextPoint(pt);
			verts->InsertNextCell(1,ids);

			for ( int i=1; i<list->GetNumberOfIds(); i++ )
				{
				pt[0] = segm->GetNodeData(list->GetId(i))->coords[0];
				pt[1] = segm->GetNodeData(list->GetId(i))->coords[1];
				pt[2] = segm->GetNodeData(list->GetId(i))->coords[2];

				ids[1] = newPoints->InsertNextPoint(pt);
				verts->InsertNextCell(1,ids);
				lines->InsertNextCell(2,ids);
				ids[0] = ids[1];
				}
			pt[0] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[0];
			pt[1] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[1];
			pt[2] = segm->GetNodeData(list->GetId(list->GetNumberOfIds()-1)+1)->coords[2];

			ids[1] = newPoints->InsertNextPoint(pt);
			verts->InsertNextCell(1,ids);
			lines->InsertNextCell(2,ids);
			}
		it++;
		}

	// Seta o tamanho dos arrays
//	verts->SetNumberOfCells(numberOfTerminals);
//	newPoints->SetNumberOfPoints(numberOfTerminals);
//	newTerminal->SetNumberOfCells(numberOfTerminals);

 	this->GridNullElements1D3D->SetPoints(newPoints);
 	this->GridNullElements1D3D->SetVerts(verts);
 	this->GridNullElements1D3D->SetLines(lines);

 	verts->Delete();
 	newPoints->Delete();
 	lines->Delete();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetElementsWithStent(vtkIdType idStent)
{
	vtkIdList *elementList = this->GridStent->GetStent(idStent);

	if ( !elementList )
		return NULL;

	vtkIdList *List = vtkIdList::New();
	//menos um porque o ultimo id da lista é o id do segmento
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		List->InsertNextId(elementList->GetId(i));
	string listChar = vtkHMUtils::ConvertIdListToChar(List);

	List->Delete();
	return listChar.c_str();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetElementsWithStenosis(vtkIdType idStenosis)
{
	vtkIdList *elementList = this->GridStenosis->GetStenosis(idStenosis);

	if ( !elementList )
		return NULL;

	vtkIdList *List = vtkIdList::New();
	//menos um porque o ultimo id da lista é o id do segmento
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		List->InsertNextId(elementList->GetId(i));

	string listChar = vtkHMUtils::ConvertIdListToChar(List);
	List->Delete();
	return listChar.c_str();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetNodesWithStenosis(vtkIdType idStenosis)
{
	vtkIdList *elementList = this->GridStenosis->GetStenosis(idStenosis);

	if ( !elementList )
		return NULL;

	vtkIdList *List = vtkIdList::New();
	//menos um porque o ultimo id da lista é o id do segmento
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		List->InsertNextId(elementList->GetId(i));

	List->InsertUniqueId(List->GetId(List->GetNumberOfIds()-1)+1);
	string listChar = vtkHMUtils::ConvertIdListToChar(List);
	List->Delete();
	return listChar.c_str();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetElementsWithAneurysm(vtkIdType idAneurysm)
{
	vtkIdList *elementList = this->GridAneurysm->GetAneurysm(idAneurysm);

	if ( !elementList )
		return NULL;

	vtkIdList *List = vtkIdList::New();
	//menos um porque o ultimo id da lista é o id do segmento
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
		List->InsertNextId(elementList->GetId(i));
	string listChar = vtkHMUtils::ConvertIdListToChar(List);
	List->Delete();
	return listChar.c_str();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetNodesWithAneurysm(vtkIdType idAneurysm)
{
	vtkIdList *elementList = this->GridAneurysm->GetAneurysm(idAneurysm);

	if ( !elementList )
		return NULL;

	vtkIdList *List = vtkIdList::New();
	//menos um porque o ultimo id da lista é o id do segmento
	for ( int i=0; i<elementList->GetNumberOfIds()-1; i++ )
			List->InsertNextId(elementList->GetId(i));
	List->InsertUniqueId(List->GetId(List->GetNumberOfIds()-1)+1);
	string listChar = vtkHMUtils::ConvertIdListToChar(List);
	List->Delete();
	return listChar.c_str();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ClearSubTreeLists ()
{
	this->SubTreeListTerminals->Reset();
	this->SubTreeListSegments->Reset();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ClearElementSelected ()
{
	this->SegmentSelected		= -1;
	this->TerminalSelected	= -1;
	this->NodeSelected			= -1;
	this->ElementSelected		= -1;

	this->LineWidgetSelected 		= -1;
	this->SphereWidgetSelected 	= -1;

	this->LineSelectedId = 0;
	this->PointSelectedId = 0;

	this->StraightModelObjectSelected = -1;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ClearPlot ()
{
	for ( int i=0; i<this->GetNumberOfGraphics(); i++ )
		if (this->SelectedActors[i])
			{
			this->CurrentRenderer->RemoveActor(this->SelectedActors[i]);
			this->SelectedActors[i]->GetMapper()->Delete();
			this->SelectedActors[i]->Delete();
			this->SelectedActors[i]=NULL;

			this->PlotSource[i]->Delete();
			this->PlotSource[i]=NULL;
			}
	this->NumberOfGraphics=0;
	this->NumberOfGraphicsSum=0;
	this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ReloadPlotActors ()
{
	for(int i=0; i<GetNumberOfGraphics(); i++)
		this->CurrentRenderer->AddActor(this->SelectedActors[i]);
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::ConfigureLengthOfElements()
{
	this->GridLine->UpdateGrid();
	this->UpdateGrid();
  this->UpdateGridSelected();

  this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::CopySegments(char *array)
{
	this->SizeHandles();
	double x[3];
	x[0] = -this->RadiusTerminal*20;
	x[1] = -this->RadiusTerminal;
	x[2] = 0;

	this->GridLine->UpdateGrid();
	this->UpdateGrid();

	vtkIdList *list = vtkHMUtils::ConvertCharToIdList(array);

	for ( int i=0; i<list->GetNumberOfIds(); i++ )
		{
		this->MoveSegment(x, list->GetId(i));
		}
	vtkHM1DSegment *segm1;
	vtkHM1DSegment *segm2;

	for ( int i=0; i<list->GetNumberOfIds(); i++ )
		{
		segm1 = this->StraightModel->GetSegment(list->GetId(i));

		for ( int j=i+1; j<list->GetNumberOfIds(); j++ )
			{
			segm2 = this->StraightModel->GetSegment(list->GetId(j));
			if ( this->DetectCollisionBetweenTerminals(segm1->GetFirstParent()->GetId(), segm2->GetFirstParent()->GetId()) )
				{
				this->ConnectElement(segm1->GetFirstParent()->GetId(), segm2->GetFirstParent()->GetId());
				}
			if ( this->DetectCollisionBetweenTerminals(segm1->GetFirstChild()->GetId(), segm2->GetFirstParent()->GetId()) )
				{
				this->ConnectElement(segm2->GetFirstParent()->GetId(), segm1->GetFirstChild()->GetId());
				}
			if ( this->DetectCollisionBetweenTerminals(segm1->GetFirstParent()->GetId(), segm2->GetFirstChild()->GetId()) )
				{
				this->ConnectElement(segm1->GetFirstParent()->GetId(), segm2->GetFirstChild()->GetId());
				}
			if ( this->DetectCollisionBetweenTerminals(segm1->GetFirstChild()->GetId(), segm2->GetFirstChild()->GetId()) )
				{
				this->ConnectElement(segm1->GetFirstChild()->GetId(), segm2->GetFirstChild()->GetId());
				}
			}
		}

	list->Delete();

	this->GridLine->UpdateGrid();
	this->UpdateGrid();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetSubTreeListSegments()
{
	string listChar = vtkHMUtils::ConvertIdListToChar(this->SubTreeListSegments);
	if (this->TreeListChar)
		delete [] this->TreeListChar;
	this->TreeListChar = new char[listChar.size()+10];
	strcpy(this->TreeListChar, (char *)listChar.c_str());
	return (const char*)this->TreeListChar;
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::GetSubTreeListTerminals()
{
	string listChar = vtkHMUtils::ConvertIdListToChar(this->SubTreeListTerminals);
	if (this->TreeListChar2)
		delete [] this->TreeListChar2;
	this->TreeListChar2 = new char[listChar.size()+10];
	strcpy(this->TreeListChar2, (char *)listChar.c_str());
	return (const char*)this->TreeListChar2;
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::FlipSegments(char *array)
{
//	this->SizeHandles();
//	double x[3];
//	x[0] = -this->RadiusTerminal;
//	x[1] = -this->RadiusTerminal;
//	x[2] = 0;

//	this->GridLine->UpdateGrid();
//	this->UpdateGrid();

//	vtkIdList *list = vtkHMUtils::ConvertCharToIdList(array);
//
////	for ( int i=0; i<list->GetNumberOfIds(); i++ )
////		{
////		this->MoveSegment(x, list->GetId(i));
////		}
//	vtkHM1DSegment *segm;
//
//	for ( int i=0; i<list->GetNumberOfIds(); i++ )
//		{
//		segm = this->StraightModel->GetSegment(list->GetId(i));
//
//
//		}
//
//	list->Delete();
//
	this->GridLine->UpdateGrid();
	this->UpdateGrid();
}

//---------------------------------------------------------------------------------------
const char *vtkHMStraightModelWidget::RestartSubTree()
{
	vtkIdList *segm = vtkIdList::New();
	vtkIdList *term = vtkIdList::New();

	this->MakeSubTreeColor(1, segm, term);

	segm->DeleteId(2);//porque o segmento 2 não pode ser apagado

	string listCharSegm = vtkHMUtils::ConvertIdListToChar(segm);
	string listCharTerm = vtkHMUtils::ConvertIdListToChar(term);

	segm->Delete();
	term->Delete();

	return listCharSegm.c_str();
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::MoveSubTree(double x[3])
{
	double p1[3], p2[3], v[3];
	int numberOfElements, numberOfNodes;

	vtkPoints *pointsTerminal = this->GridTerminal->GetPoints();
	vtkPoints *pointsLine = this->GridLine->GetPoints();
	vtkPoints *pointsNode = this->GridNode->GetPoints();

	vtkIdType *p;
	vtkIdList *elementList;

	for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)
		{
		vtkHM1DTerminal *term = this->StraightModel->GetTerminal(this->SubTreeListTerminals->GetId(i));

		p = this->GridTerminal->GetTerminalInformation(term->GetId());

		//Pega o ponto
		pointsTerminal->GetPoint(p[0], p1);

		//Soma ao ponto o movimento
		p1[0] += x[0];
		p1[1] += x[1];
		p1[2] += x[2];

		//Seta o ponto
		pointsTerminal->SetPoint(p[0], p1);
		}

	for (int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)
		{
		vtkHM1DSegment *segm = this->StraightModel->GetSegment(this->SubTreeListSegments->GetId(i));
		numberOfElements = segm->GetElementNumber();
		numberOfNodes = segm->GetNodeNumber();

		elementList = this->GridLine->GetSegmentInformation(segm->GetId());

		//Calcula o deslocamento para os elementos e nodes intermediarios ao segmento
	 	for ( int j=0; j<numberOfElements; j++ )
			{
			//Pega a coordenada do node do segmento
			p2[0] = segm->GetNodeData(j)->coords[0];
			p2[1] = segm->GetNodeData(j)->coords[1];
			p2[2] = segm->GetNodeData(j)->coords[2];

			for (int k=0; k<3; k++)
		    p2[k] += x[k];

		  //Seta a coordenada do node do segmento
			segm->GetNodeData(j)->coords[0] = p2[0];
			segm->GetNodeData(j)->coords[1] = p2[1];
			segm->GetNodeData(j)->coords[2] = p2[2];

		  p = this->GridLine->GetSegment(elementList->GetId(j));

		  //Seta os pontos
		  pointsLine->SetPoint(p[0], p2);
		 	pointsNode->SetPoint(p[0], p2);
		 	this->Points->SetPoint(p[0], p2);

			}

		//////////////////////////////////////////////////////////////
		//Seta o ponto do ultimo node do segmento
		//Pega a coordenada do node do segmento
		p2[0] = segm->GetNodeData(numberOfElements)->coords[0];
		p2[1] = segm->GetNodeData(numberOfElements)->coords[1];
		p2[2] = segm->GetNodeData(numberOfElements)->coords[2];

		for (int j=0; j<3; j++)
	    p2[j] += x[j];

	  //Seta a coordenada do node do segmento
		segm->GetNodeData(numberOfElements)->coords[0] = p2[0];
		segm->GetNodeData(numberOfElements)->coords[1] = p2[1];
		segm->GetNodeData(numberOfElements)->coords[2] = p2[2];

		p = this->GridLine->GetSegment(elementList->GetId(numberOfElements-1));

	  //Seta os pontos
	  pointsLine->SetPoint(p[1], p2);
	 	pointsNode->SetPoint(p[1], p2);
	 	this->Points->SetPoint(p[1], p2);
		}
}


//---------------------------------------------------------------------------------------
//Desconectar elementos da arvore no caso de arraste de algum elemento
void vtkHMStraightModelWidget::DisconnectSubTree()
{
	//Segmento selecionado
	vtkHM1DSegment *Segm = this->StraightModel->GetSegment(this->StraightModelElementSelected);
	//terminal parent
	vtkHM1DTreeElement *parent = Segm->GetFirstParent();

	double p1[3];
	vtkIdType newTerminal;

	//Pego o terminal parent
	while ( !this->StraightModel->IsTerminal(parent->GetId()) )
		parent = Segm->GetNextParent();

  //Se parent for um terminal com mais de um filho, ou o parent tiver parent,
	//adicionar um terminal como pai do segmento arrastado.
  if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )
  	{
	  //Seta no primeiro node do segmento.
	  p1[0] = Segm->GetNodeData(0)->coords[0];
		p1[1] = Segm->GetNodeData(0)->coords[1];
		p1[2] = Segm->GetNodeData(0)->coords[2];

		vtkIdType *info;
		//Pego informação do primeiro node do segmento
		info = this->GridLine->GetNodeInformation(Segm->GetId(), 0);

  	//Adiciona um terminal na coordenada do primeiro node
		newTerminal = this->AddTerminalWidget(p1, 0, info[0]);
		this->AddTerminalWidget(p1, newTerminal);

	  vtkHM1DTerminal *Term = this->StraightModel->GetTerminal(newTerminal);

	  //remove o segmento como filho do parent e adiciona o novo terminal como parent
	  parent->RemoveChild(Segm);
	  Segm->RemoveParent(parent);
	  Term->AddChild(Segm);

	  if ( parent->GetFirstParent() )
		  {
		  //Pega a ultima coordenada do segmento parent
		  int n = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeNumber();
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstParent())->GetNodeData(n-1)->coords[2];
		  }
	  else
		  {
		  //Pega a primeira coordenada do segmento filho
		  p1[0] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[0];
		  p1[1] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[1];
		  p1[2] = vtkHM1DSegment::SafeDownCast(parent->GetFirstChild())->GetNodeData(0)->coords[2];

		  info = this->GridLine->GetNodeInformation(parent->GetFirstChild()->GetId(), 0);
		  this->GridLine->SetTerminalPoint(parent->GetId(), info[0]);
		  }

		//Seta a coordenada do terminal
	  info = this->GridTerminal->GetTerminalInformation(parent->GetId());
	  this->GridTerminal->GetPoints()->SetPoint(info[0], p1);

  	} //Fim if ( (parent->GetChildCount() > 1) || (parent->GetFirstParent()) )
}

//--------------------------------------------------------------------
void vtkHMStraightModelWidget::SetSubTreePoints(double x[3])
{
	double p1[3];
	double v[3];
	int id;
	int numberOfElements;

	v[0] = x[0] - this->LastPosition[0];
  v[1] = x[1] - this->LastPosition[1];
  v[2] = x[2] - this->LastPosition[2];

	//Verifica se o elemento clicado do straightModel é um terminal
	if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))
		{
		vtkHM1DTerminal* term = (vtkHM1DTerminal*)this->StraightModel->GetTerminal(this->StraightModelElementSelected);
		vtkHM1DTreeElement* parent = term->GetFirstParent();
		vtkHM1DTreeElement *child = NULL;
		vtkHM1DTreeElement *elem = NULL;
		vtkHM1DSegment* segm = NULL;

		this->SphereSourceSelected->GetCenter(p1);
		//modifica o ponto
		p1[0] += v[0];
		p1[1] += v[1];
		p1[2] += v[2];
		this->SphereSourceSelected->SetCenter(p1);

		//Se parent existe, o elemento clicado é um terminal comum,
		//senão é o coração.
		if ( parent )
			{
			while ( parent )
				{
				this->MoveParent(v, parent->GetId());
				parent = term->GetNextParent();
				}
			elem = term;
			child = term->GetFirstChild();
			}
		else
			{
			//Pega ponto do terminal clicado
			vtkPoints *points = this->GridLine->GetPoints();
			points->GetPoint(this->PointSelectedId, p1);

			//modifica o ponto
			p1[0] += v[0];
			p1[1] += v[1];
			p1[2] += v[2];

			//Seta o ponto
			points->SetPoint(this->PointSelectedId, p1);

			vtkPoints *pointsTerminal = this->GridTerminal->GetPoints();

			vtkIdType *info = this->GridTerminal->GetTerminalInformation(this->StraightModelElementSelected);
			//Seta o ponto
			pointsTerminal->SetPoint(info[0], p1);

			if ( this->StraightModelObjectSelected == HEART )
				this->HeartGeometry->SetCenter(p1);

			elem = term;
			child = term->GetFirstChild();
			}

		while ( child )
			{
			id = child->GetId();

			//Se filho for igual a terminal, pego o próximo para movimenta-lo
			if ( this->StraightModel->IsTerminal(id) )
				{
				child = elem->GetNextChild();
				}
			else
				{
				segm = (vtkHM1DSegment*)child;

				this->MoveChild(v, id);
				child = elem->GetNextChild();
				} // Fim else

			} // Fim While(child)

		} //Fim if (( this->StraightModelObjectSelected == TERMINAL ) || ( this->StraightModelObjectSelected == HEART ))

	else if ( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT) ||
						(this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
		{
		vtkHM1DTreeElement *child = NULL;
		vtkHM1DTreeElement *elem = NULL;
		vtkHM1DSegment* segm = (vtkHM1DSegment*)this->StraightModel->GetSegment(this->SegmentSelected);
		vtkHM1DTreeElement* parent = segm->GetFirstParent();
		vtkHM1DSegment *segmParent; //Segmento pai

		//Verifica se o parent é diferente de terminal, caso seja,
		//movimento o segmento pai junto com o segmento que esta
		//sendo deslocado
		if ( this->StraightModel->IsSegment(parent->GetId()) )
			{
			elem = segm;
			segmParent = (vtkHM1DSegment*)parent;
		 	numberOfElements = segmParent->GetElementNumber();

		 	id = parent->GetId();
		 	this->MoveParent(v, id);

		 	//Movimentar os elementos filhos do segmento selecionado
		 	child = segm->GetFirstChild();
			while ( child )
				{
				this->MoveChild(v, child->GetId());
				child = segm->GetNextChild();
				}
			} // Fim if ( parent )

		//Se pai é um terminal, os
		//outros segmentos filhos também devem ser movimentados,
		else if ( this->StraightModel->IsTerminal(parent->GetId()) )
			{
			elem = segm;
			segmParent = (vtkHM1DSegment*)parent->GetFirstParent();

		 	if ( segmParent )
			 	{
			 	while ( segmParent )
				 	{
				 	id = segmParent->GetId();
				 	this->MoveParent(v, id);
				 	segmParent = (vtkHM1DSegment*)parent->GetNextParent();
				 	}
			 	}
			//Movimentar filhos do terminal pai
			child = parent->GetFirstChild();
			while ( child )
				{
				if ( child->GetId() != segm->GetId() )
					this->MoveChild(v, child->GetId());
				child = parent->GetNextChild();
				}


			//Encontrar o terminal filho do segmento selecionado
		 	elem = segm->GetFirstChild();
		 	while ( !this->StraightModel->IsTerminal(elem->GetId()) )
		 		elem = segm->GetNextChild();

		 	//Movimentar os elementos parents do terminal filho do segmento selecionado
		 	segmParent = (vtkHM1DSegment*)elem->GetFirstParent();
		 	while ( segmParent )
			 	{
			 	id = segmParent->GetId();
			 	if ( segmParent->GetId() != segm->GetId() )
			 		this->MoveParent(v, id);
			 	segmParent = (vtkHM1DSegment*)elem->GetNextParent();
			 	}

		 	//Movimentar os elementos filhos do terminal filho do segmento selecionado
//		 	child = elem->GetFirstChild();
//			while ( child )
//				{
//				this->MoveChild(v, child->GetId());
//				child = elem->GetNextChild();
//				}

			} // Fim else if ( this->StraightModel->IsTerminal(parent->GetId()) )
		} // Fim else if ( (this->StraightModelObjectSelected == SEGMENT) || (this->StraightModelObjectSelected == STENT) ||
			//(this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
	 //Elemento clicado é um node
	else //if ( this->NodeSelected > -1)
		{
		//Pega ponto do node clicado
		vtkPoints *pointsNode = this->GridNode->GetPoints();
		vtkPoints *pointsLine = this->GridLine->GetPoints();
		pointsNode->GetPoint(this->PointSelectedId, p1);

		//modifica o ponto
		p1[0] += v[0];
		p1[1] += v[1];
		p1[2] += v[2];
		this->SphereSourceSelected->SetCenter(p1);
		//Seta o ponto
		pointsNode->SetPoint(this->PointSelectedId, p1);
		pointsLine->SetPoint(this->PointSelectedId, p1);
		this->Points->SetPoint(this->PointSelectedId, p1);

		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[0] = p1[0];
		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[1] = p1[1];
		this->StraightModel->GetSegment(this->SegmentSelected)->GetNodeData(this->NodeSelected)->coords[2] = p1[2];

		}

	// remember last position
  this->LastPosition[0] = x[0];
  this->LastPosition[1] = x[1];
  this->LastPosition[2] = x[2];
}


//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::MakePathTreeColor(vtkIdType id, vtkIdList *idListSegm, vtkIdList *idListTerm)
{
//cout << "vtkHMStraightModelWidget::MakePathTreeColor Visitando ID "<< id << endl;


  if (this->StraightModel->IsSegment(id)) // se atual elemento da arvore é um segmento
    {
    idListSegm->InsertNextId(id);

    if (id == BeginTerminal)
      {
      //this->CouplingActorCounter++;
      //cout << "Achei ponto inicial entao oincrmento valor do CopulingActor" << endl;
      return;
      }

    vtkHM1DSegment *Seg = vtkHM1DSegment::SafeDownCast(this->StraightModel->GetSegment(id));

    this->MakePathTreeColor(Seg->GetFirstParent()->GetId(), idListSegm, idListTerm);
    }
  else if (this->StraightModel->IsTerminal(id)) // se atual elemento da arvore é um terminal
    {
    vtkHM1DTerminal *term = this->StraightModel->GetTerminal(id);

    idListTerm->InsertNextId(id);

    if (id == BeginTerminal)
      {
      this->CouplingActorCounter = 0;
      //cout << "Achei ponto inicial entao o incrmento valor do CopulingActor" << endl;
      return;
      }

    if (!term->GetFirstParent())  // se é o coracao
      {
      //cout << "Cheguei ao coracao e este é o primeiro ponto" << endl;
      // se chegou no coracao entao o elemento clicado é o primeiro nó a ser inserido
      BeginTerminal = this->StraightModelElementSelected;
      idListTerm->Reset();
      idListSegm->Reset();
      return;
      }
    else // se o terminal tem pai
      {
      this->MakePathTreeColor(term->GetFirstParent()->GetId(), idListSegm, idListTerm);
      }
    } // fim else
}


//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::VisualizePath()
{
if (this->StraightModelObjectSelected  != TERMINAL && this->StraightModelObjectSelected  != HEART)
  return;
//cout << "void vtkHMStraightModelWidget::VisualizePath()" << endl;



  vtkIdList *subListSegm = vtkIdList::New();
  vtkIdList *subListTerm = vtkIdList::New();

//  //Se for Stent passa o objeto stent senão passa o objeto de segmento ou terminal
//  if ( (this->StraightModelObjectSelected == STENT) || (this->StraightModelObjectSelected == STENOSIS) || (this->StraightModelObjectSelected == ANEURYSM) )
//    this->MakePathTreeColor(this->SegmentSelected, subListSegm, subListTerm);
//  else

  this->EndTerminal = this->StraightModelElementSelected;

  this->MakePathTreeColor(this->StraightModelElementSelected, subListSegm, subListTerm);
  this->HighlightLine(0);
  this->HighlightHandles(0);

  // Se as duas listas estiverem vazias enche as duas
  if((this->SubTreeListSegments->GetNumberOfIds() == 0)&&(this->SubTreeListTerminals->GetNumberOfIds() == 0))
    {
    this->SubTreeListSegments->DeepCopy(subListSegm);// copia as sublistas para as listas globais
    this->SubTreeListTerminals->DeepCopy(subListTerm);
    }
  //Se uma das duas estiver vazia e a outra cheia, adiciona apenas os que não tem na lista
  else if( (this->SubTreeListSegments->GetNumberOfIds() == 0)||(this->SubTreeListTerminals->GetNumberOfIds() == 0) )
    {
    if(!this->Interactor->GetControlKey())//Se Control NÃO Pressionado
      this->ClearSubTreeLists();

    for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
      this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o SEGMENTO que ainda não tem
    for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
      this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
    }
  else//Se nenhuma lista está vazia
    {
    if(this->Interactor->GetControlKey())//Se Control Pressionado
      {
      //Se for um SEGMENTO e ele estiver na lista
      if((this->SegmentSelected != -1)&&(this->SubTreeListSegments->IsId(this->SegmentSelected) != -1))
        {
        this->SubTreeListSegments->DeleteId(this->SegmentSelected);//Deleto o SEGMENTO selecionado na lista global
        subListSegm->DeleteId(this->SegmentSelected);//Deleto o SEGMENTO selecionado na lista auxiliar
        }
      //Se for um SEGMENTO e ele NÃO estiver na lista
      else if((this->SegmentSelected != -1)&&(this->SubTreeListSegments->IsId(this->SegmentSelected) == -1))
        {
        for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
          this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o SEGMENTO que ainda não tem
        for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
          this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
        }
      //Se for um TERMINAL e ele estiver na lista
      else if((this->TerminalSelected != -1)&&(this->SubTreeListTerminals->IsId(this->TerminalSelected) != -1))
        {
        this->SubTreeListTerminals->DeleteId(this->TerminalSelected);//Deleto o TERMINAL selecionado na lista global
        subListTerm->DeleteId(this->TerminalSelected);//Deleto o TERMINAL selecionado na lista auxiliar
        }
      //Se for um TERMINAL e ele NÃO estiver na lista
      else if((this->TerminalSelected != -1)&&(this->SubTreeListTerminals->IsId(this->TerminalSelected) == -1))
        {
        for(int i=0; i<subListTerm->GetNumberOfIds(); i++)
          this->SubTreeListTerminals->InsertUniqueId(subListTerm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
        for(int i=0; i<subListSegm->GetNumberOfIds(); i++)
          this->SubTreeListSegments->InsertUniqueId(subListSegm->GetId(i));//Só adiciona o TERMINAL que ainda não tem
        }
      }
    //Se control NÃO pressionado
    else if(!this->Interactor->GetControlKey())
      {
      this->ClearSubTreeLists();
      this->SubTreeListSegments->DeepCopy(subListSegm);// copia a sublista de SEGMENTOToDetachPathS para a lista global
      this->SubTreeListTerminals->DeepCopy(subListTerm);// copia a sublista de TERMINAIS para a lista global
      }
    }
  for(int i=0; i < this->SubTreeListSegments->GetNumberOfIds(); i++)//percorro a lista global de SEGMENTOS
    this->AddSegmentInMainTreeColor(this->SubTreeListSegments->GetId(i));//adiciono SEGMENTOS no grid
  //for(int i=0; i < this->SubTreeListTerminals->GetNumberOfIds(); i++)//percorro a lista global de TERMINAIS
  this->AddTerminalInMainTreeColor(this->SubTreeListTerminals->GetId(0));//adiciono TERMINAIS no grid

  subListSegm->Delete();
  subListTerm->Delete();

  this->HighlightLine(1);
  this->HighlightHandles(1);
}

//---------------------------------------------------------------------------------------

void vtkHMStraightModelWidget::ResetPathTree()
{
  this->BeginTerminal = 0;
  this->EndTerminal = 0;
  this->HighlightLine(0);
  this->HighlightHandles(0);
  this->Interactor->Render();
}

//---------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::SetGridLine(vtkHM1DStraightModelGrid *grid)
{
  this->GridLine = grid;
  this->StraightModel = grid->GetStraightModel();

  this->UpdateGrid();
  this->BuildHeartRepresentation();
  this->GridLineMapper->SetInput(this->GridLine);

  //Ator
  this->GridLineActor->SetMapper(this->GridLineMapper);

  this->GridNodeMapper->SetInput(this->GridNode);
  //Ator
  this->GridNodeActor->SetMapper(this->GridNodeMapper);

  double *bds = this->GridLine->GetBounds();

  this->SizePerAxle[0] = sqrt(pow(bds[1]-bds[0],2));
  this->SizePerAxle[1] = sqrt(pow(bds[3]-bds[2],2));
  this->SizePerAxle[2] = sqrt(pow(bds[5]-bds[4],2));
//  this->GenerateRepresentation();

  this->SetEnabled(1);
}

//------------------------------------------------------------------------------------------
void vtkHMStraightModelWidget::KeepSubTree(const char *text)
{
  vtkDebugMacro(<<"KeepSubTree.");

  //pega todos os segmentos da arvore
  char *allSegments = this->StraightModel->GetSegmentsID();

  vtkIdList *AllSegmentsIds = vtkHMUtils::ConvertCharToIdList(allSegments);

  vtkIdList *list = vtkHMUtils::ConvertCharToIdList((char*)text);

  vtkIdType clipId;

  //deleta da lista de todos os segmentos os selecionados
  for ( int i=0; i<list->GetNumberOfIds(); i++ )
    {
    AllSegmentsIds->DeleteId(list->GetId(i));
    }

  for(int i=0; i <= AllSegmentsIds->GetNumberOfIds(); i++)
    {
    clipId = this->FindClipIds(AllSegmentsIds->GetId(i));
    if ( clipId != -1 )
      this->RemoveClip(clipId);

    this->GridStent->DeleteStentsInSegment(AllSegmentsIds->GetId(i));
    this->GridStenosis->DeleteStenosisInSegment(AllSegmentsIds->GetId(i));
    this->GridAneurysm->DeleteAneurysmInSegment(AllSegmentsIds->GetId(i));
    }

  vtkHM1DTerminal *heart = vtkHM1DTerminal::SafeDownCast(this->StraightModel->Get1DTreeRoot());
  vtkHM1DSegment *segm = this->StraightModel->GetSegment(list->GetId(0));

  vtkHM1DTreeElement *parent = segm->GetFirstParent();
  vtkHM1DTreeElement *child = parent->GetFirstChild();

  // pegar o array de dados lido do DataOut do primeiro node do segmento.
  vtkDoubleArray *array = segm->GetResolutionArray(0);

  // Remover todos os segmentos não selecionados
  this->StraightModel->RemoveSegmentsInSubTree(AllSegmentsIds,  0);

  // se tenho dados lidos do DataOut, setar a curva do coração com
  // os dados lidos. Esses dados são do primeiro node do segmento
  // filho do terminal que ficará como coração.
  if ( array )
    {
    //seta o tempo e a curva do coração
    vtkDoubleArray *timeArray = this->StraightModel->GetInstantOfTime();
    vtkDoubleArray *newTimeArray = vtkDoubleArray::New();

    // tempo total da simulação
    double totalTime = timeArray->GetValue(timeArray->GetNumberOfTuples()-1);

    // numero de ciclos processados completamente
    int numberOfCardiacCycle = (float)totalTime / (float)this->GetHeartFinalTime();

    // numero de pontos de cada ciclo cardiaco
    int nPoints = (float)this->GetHeartFinalTime() / ((float)timeArray->GetValue(1) - (float)timeArray->GetValue(0));

    // ponto inicial na curva.
    int initial = (numberOfCardiacCycle-1) * nPoints;

    vtkDoubleArray *p = vtkDoubleArray::New();

    std::list<double> *R1 = heart->GetR1();
    std::list<double> *R2 = heart->GetR2();

    std::list<double>::iterator it = R1->begin();

    double r1Value = *it;

    it = R2->begin();
    double r2Value = *it;

    double sumResistance = r1Value+r2Value;

    int n = array->GetNumberOfTuples();
    p->SetNumberOfValues(nPoints+1);
    newTimeArray->SetNumberOfValues(nPoints+1);

    // se a soma das resistencias for maior que zero, calculo o valor da pressão
    // multiplicando o fluxo pela soma das resistencias
    if ( sumResistance > 0 )
      {
      int j=0;
      for ( int i=initial; i<initial+nPoints+1; i++ )
        {
        double *tuple = array->GetTuple(i);
        p->SetValue(j, tuple[0]*sumResistance);
        newTimeArray->SetValue(j, timeArray->GetValue(j));

        j++;
        }
      }
    else
      {
      int j=0;
      for ( int i=initial; i<initial+nPoints+1; i++ )
        {
        double *tuple = array->GetTuple(i);
        p->SetValue(j, tuple[0]);
        newTimeArray->SetValue(j, timeArray->GetValue(j));

        j++;
        }
      }

    heart->SetTimeArray(newTimeArray, 'P', 1);
    heart->SetArray(p, 'P', 1);
    p->Delete();
    newTimeArray->Delete();
    }

  //coloca o coração na posição correta
  while (child)
    {
    heart->AddChild(child);
    child->RemoveParent(parent);
    parent->RemoveChild(child);
    child = parent->GetFirstChild();
    }

  // Atualizar o grid
  this->GridLine->UpdateGrid();
  this->UpdateGrid();
  this->BuildHeartRepresentation();

  this->HighlightLine(0);
  this->ClearSubTreeLists();
  this->RenderSegmentsWithDefaultColors();
  this->ClearElementSelected();
  this->State = vtkHMStraightModelWidget::Outside;

  this->Interactor->Render();
  AllSegmentsIds->Delete();
  list->Delete();
}
