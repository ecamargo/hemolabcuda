// .NAME vtkHM1DStraightModelWidget - Class to representation of a 1D tree.
// .SECTION Description
// vtkHM1DStraightModelWidget is the class that manipula the way a 1D tree is created and modified
// by its users.

#ifndef __vtkHMStraightModelWidget_h
#define __vtkHMStraightModelWidget_h

#include "vtk3DWidget.h"
#include "vtkHMLineSource.h" // For passing calls to it
#include "vtkHMGraphicProperty.h"
#include "vtkClientServerID.h"
#include "vtkHM1DStraightModelGrid.h"
#include "vtkHMDefineVariables.h"

class vtkActor;
class vtkPolyDataMapper;
class vtkPoints;
class vtkPolyData;
class vtkProp;
class vtkProperty;
class vtkLineSource;
class vtkSphereSource;
class vtkCellPicker;
class vtkHMPointWidget;
class vtkHMPWCallback;
class vtkHMPointWidgetCallback;
class vtkHM1DSegment;
class vtkHM1DStraightModel;
class vtkHM1DStraightModelSource;
class vtkHM1DStraightModelReader;
class vtkHM1DHeMoLabReader;
class vtkHMUnstructuredGridTo1DModel;
class vtkHM1DStentPolyData;
class vtkHM1DStenosisPolyData;
class vtkHM1DAneurysmPolyData;
class vtkHM1DKeepRemoveFilter;

#include "vtkCollection.h"
class vtkActorCollection;
class vtkMapperCollection;
class vtkStringArray;
#include <vector>

#include "vtkArrowSource.h"
#include "vtkGlyphSource2D.h"

class vtkVectorText;
#include <string>
using namespace std;

class VTK_EXPORT vtkHMStraightModelWidget : public vtk3DWidget
{
public:
  // Description:
  // Instantiate the object.
  vtkTypeRevisionMacro(vtkHMStraightModelWidget,vtk3DWidget);
  static vtkHMStraightModelWidget *New();
  void PrintSelf(ostream& os, vtkIndent indent);

	int SetSourceCSID(int csId);

  // Description:
  // Methods that satisfy the superclass' API.
  virtual void SetEnabled(int);
  virtual void PlaceWidget(double bounds[6]);
  void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  void PlaceWidget(double xmin, double xmax, double ymin, double ymax,
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}

  // Description:
  // Set points of the segments parent and childs of the terminal clicked,
  // moving all nodes and elements of the segments adjacents. Ex: Click in
  // the terminal and moves, it will move all segments or then click and
  // moves one segment then son will be moved the terminals adjacents.
	void SetPoints(double x[3]);

  // Description:
	// Similar to the method SETPOINTS, but not move the children,
	// because they move whole sub-tree together
	void SetSubTreePoints(double x[3]);

	// Description:
	// Set points of the segment clicked, moving all nodes and elements of the
	// segment. Ex: Click in segment and move.
	void SetLines(double x[3]);

	// Description:
	// Move segment child of the element clicked. Ex: Click and moves the
	// terminal and the segment child son will be moved
	void MoveChild(double v[3], int id);

	// Description:
	// Move segment parent of the element clicked. Ex: Click and moves the
	// terminal and the segment parent son will be moved
	void MoveParent(double v[3], int id);

	// Description:
	// Move only segment selected.
	void MoveSegment(double x[3], vtkIdType idSegment);

	// Description:
	// Move segment with clip. Move first and second piece of the segment divided in two.
	// position = id of the clip in array.
	void MoveSegmentWithClip(double x[3], vtkIdType position);

	// Description:
	// Return ids of the segments with clip.
	char *GetSegmentsWithClip(vtkIdType idClip);

  // Description:
  // Force the line widget to be aligned with one of the x-y-z axes.
  // Remember that when the state changes, a ModifiedEvent is invoked.
  // This can be used to snap the line to the axes if it is orginally
  // not aligned.
  vtkSetClampMacro(Align, int, XAxis, None);
  vtkGetMacro(Align, int);
  void SetAlignToXAxis() { this->SetAlign(XAxis); }
  void SetAlignToYAxis() { this->SetAlign(YAxis); }
  void SetAlignToZAxis() { this->SetAlign(ZAxis); }
  void SetAlignToNone()  { this->SetAlign(None); }

  // Description:
  // Enable/disable clamping of the point end points to the bounding box
  // of the data. The bounding box is defined from the last PlaceWidget()
  // invocation, and includes the effect of the PlaceFactor which is used
  // to gram/shrink the bounding box.
  vtkSetMacro(ClampToBounds,int);
  vtkGetMacro(ClampToBounds,int);
  vtkBooleanMacro(ClampToBounds,int);

  // Description:
  // Grab the polydata (including points) that defines the line.  The
  // polydata consists of n+1 points, where n is the resolution of the
  // line. These point values are guaranteed to be up-to-date when either the
  // InteractionEvent or EndInteraction events are invoked. The user provides
  // the vtkPolyData and the points and polyline are added to it.
//  void GetPolyData(vtkPolyData *pd);

  // Description:
  // Get the handle properties (the little balls are the handles). The
  // properties of the handles when selected and normal can be
  // manipulated.
  vtkGetObjectMacro(HandleTerminalProperty,vtkProperty);
  vtkGetObjectMacro(HandleNodeProperty,vtkProperty);
//  vtkGetObjectMacro(SelectedHandleProperty,vtkProperty);

  // Description:
  // Get the line properties. The properties of the line when selected
  // and unselected can be manipulated.
  vtkGetObjectMacro(LineProperty,vtkProperty);
  vtkGetObjectMacro(SelectedLineProperty,vtkProperty);

  // Description:
  // get the last StraightModel Element Selected
  vtkGetMacro(StraightModelElementSelected,vtkIdType);
  vtkSetMacro(StraightModelElementSelected,vtkIdType);


	vtkHM1DStraightModel* GetStraightModel() {return this->StraightModel;};

	// Description:
  // Set/Get the selected object in StraightModel
 	vtkSetMacro(StraightModelObjectSelected, vtkIdType);
	vtkGetMacro(StraightModelObjectSelected, vtkIdType);

	// Description:
  // Set/Get the Straight Model Mode (vis. input, edition, vis. output)
  vtkGetMacro(StraightModelMode, int);
  vtkSetMacro(StraightModelMode, int);


   vtkGetMacro(NodeSelected, vtkIdType);
   vtkGetMacro(SegmentSelected, vtkIdType);
   vtkGetMacro(ElementSelected, vtkIdType);
   vtkGetMacro(TerminalSelected, vtkIdType);

   // Description:
   // sets the plot type array
   void SetPlotType(vtkIntArray *PlotArray);

   // Description:
   // returns a reference to the plot type array
   vtkIntArray *GetPlotType(void);

   // Description:
   // Geometry Methods
//	 void ChangeGeometricProperties();
//	 void GetWidgetHandleValues(double *values);

  // Description:
  // Highlights the proximal and distal segments
//  void HighLightProximalDistal(int id);

  // Description:
  // paints a segment with identifier given by id with a
  // color given by the variables r, g and b.
//  void PaintSegment(int id, double r, double g, double b);

  // Description:
  //
  double GetIniFileCurrentTime();

  // Description:
  //
  void SetTreePressure(int pressure, double value);


  // Description:
  //
  double GetIniFileDeltaT();


  // Description:
  //
  double GetHeartFinalTime();


  // Description:
  //Set and Get for XYPlotWidget (O nome deve ser Reader e não Source como foi declarado.)
  //vtkGetObjectMacro(StraightModelReader,vtkHM1DStraightModelReader);
  vtkHM1DStraightModelReader* GetStraightModelReader()
  {
  	return StraightModelSource;
  }

	vtkSetMacro(Element_Selected, int);
  vtkGetMacro(Element_Selected, int);
  vtkGetMacro(Segment_Selected, int);
  void SetSegment_Selected(int s);

	// Description:
	// Method for color tree in function of the property selected
	// (Elastin, Collagen, Viscoelasticity, etc).
	void ColorTree();

//	vtkSetObjectMacro(GridLine, vtkHM1DStraightModelGrid);
	vtkGetObjectMacro(GridLine, vtkHM1DStraightModelGrid);
	void SetGridLine(vtkHM1DStraightModelGrid *grid);

	// Description:
	// returns 1 if some element in the StraightModel tree has null viscoelasticity
	int CheckForNullViscoElasticElements();

	// Description:
	// return 1 if Crtl key is currently pressed;
	// this is used in multiple selection of segments.
	int CrtlKeyPressed();

	// Description:
	//
	void RenderSegmentsWithDefaultColors();

	// Description:
	// Set/Get NumberOfGraphicsSum
	vtkSetMacro(NumberOfGraphicsSum, int);
  vtkGetMacro(NumberOfGraphicsSum, int);

  // Description:
	// Set/Get NumberOfGraphics
  vtkSetMacro(NumberOfGraphics, int);
  vtkGetMacro(NumberOfGraphics, int);

  // Description:
	// Method to select actor clicked
  void ColorPlot(int type);

  // Description:
	// Method to color node or terminal or segment in the same color of the curve of plot
  void HighlightSelectedActors(int op);

  // Description:
	// Set/Get EditionModeSelected
  vtkGetMacro(EditionModeSelected, vtkIdType);
  void SetEditionModeSelected(vtkIdType mode);

  // Description:
	// Set/Get SurgicalInterventionsModeSelected
  vtkGetMacro(SurgicalInterventionsModeSelected, vtkIdType);
  void SetSurgicalInterventionsModeSelected(vtkIdType mode);

  // Description:
  // Detach sub tree elements
  void ToDetachSubTree();

  // Description:
  // Erase sub tree elements in the sub tree
  void ToDetachSubTreeErase();

  // Description:
  // Detach sub tree elements
  int DeleteSegment(vtkIdType id);

  // Description:
  // Detach sub tree elements
  int DeleteSegment(const char *text);

  // Description:
  // Delete segment selected
  int DeleteSegmentSelected();

  // Description:
  // Delete terminal selected
  int DeleteTerminalSelected();

//  // Description:
//  // Delete node widget
//  void DeleteNodeWidget(vtkIdType idSegm, vtkIdType idNode);
//
//  // Description:
//  // Delete element widget
//  void DeleteElementWidget(vtkIdType idSegm, vtkIdType idElement);


	// Description:
	// Update informations of the segment selected. Update number of elements and length,
	int SetSegmentData(int numberOfElements, double length);

	// Description:
	// Update the grids of terminals and nodes
	void UpdateGrid();
	// Atualiza os grids de terminais e nodes.
	// Quando algum segmento, ou elemento precisa ser deletado, fica mais rápido
	// refazer os grids de terminais e nodes do que atualiza-los.

	// Description:
	// Update coordinates of the grid selected
	void UpdateGridSelected();
	// Metodo para atualizar as coordenadas do grid que representa o segmento selecionado

	// Description:
	// just call the GetSegmentsID method from StraightModel
	char *GetSegmentsID();

	// Description:
	// Clone all properties of one segment to other.
	void CloneSegment(char *origArray, char *targetArray);

	// Description:
	// Configure scale of the grid
	// v = value
	// n = axle 'x', 'y', 'z'
	void ConfigureScale(double v, char n);

	// Description:
	// Configure scale of the grid
	// Size in cm.
	void ConfigureScale(double sizeX, double sizeY, double sizeZ);

	// Description:
	// Set/Get Size, of the tree, per axle
	void SetSizePerAxle(double size[3]);
	double *GetSizePerAxle();

	void SetTerminals(int option, double R1, double R2, double Cap, double Pr);

	// Description:
	// Disconnect element selected of the tree in case of movement
	void DisconnectElement();

	void DisconnectSubTree();

	// Description:
	// Disconnect element of the tree in case of movement
	void DisconnectElement(vtkIdType idSegment);

	// Description:
	// Connect any element of the tree in case of movement of terminal
	// or segment and it collides with another terminal
	// t1 = terminal moved
	// t2 = terminal stopped
	void ConnectElement(vtkIdType t1, vtkIdType t2);

	// Description:
	// It detects collision between the terminal selected and the all terminals
	vtkIdType DetectCollisionBetweenTerminals();

	// Description:
	// It detects collision between the terminal and the all terminals
	vtkIdType DetectCollisionBetweenTerminals(vtkIdType t);

	// Description:
	// It detects collision between two terminals
	bool DetectCollisionBetweenTerminals(vtkIdType t1, vtkIdType t2);

	// Description:
	// It color the main tree
	void MakeSubTreeColor(vtkIdType id, vtkIdList *idListSegm, vtkIdList *idListTerm);

	// Description:
	// It color the main tree
	void MakeMainTreeColor(vtkIdType id, vtkIdList *idList);

	// Description:
	// Add segment in main tree color
	void AddSegmentInMainTreeColor(vtkIdType idSegment);

	// Description:
	// Add terminal in main tree color
	void AddTerminalInMainTreeColor(vtkIdType idTerminal);

	// Description:
	// Invert flow direction
	void InvertFlowDirection();

	// Description:
	// Return a char* with ids of elements with stent.
	const char *GetElementsWithStent(vtkIdType idStent);

	// Description:
	// Return a char* with ids of elements with stenosis.
	const char *GetElementsWithStenosis(vtkIdType idStenosis);

	// Description:
	// Return a char* with ids of nodes with stenosis.
	const char *GetNodesWithStenosis(vtkIdType idStenosis);

	// Description:
	// Return a char* with ids of elements with aneurysm.
	const char *GetElementsWithAneurysm(vtkIdType idAneurysm);

	// Description:
	// Return a char* with ids of nodes with aneurysm.
	const char *GetNodesWithAneurysm(vtkIdType idAneurysm);

	// Description:
	// Add clip in segment.
	void AddClip(vtkIdType idSegment, vtkIdType idNode);

	//Description:
	// Remove clip
	void RemoveClip(vtkIdType idClip);

	// Description:
	// Find ids of the segments associated with clip
	// and return position in array
	vtkIdType FindClipIds(vtkIdType idSegment);

	// Description:
	// Add stent in segment selected. Pass like parameter the size
	// of the effected area and return id of the stent
	vtkIdType AddStent(double size);

	// Description:
	// Add stent in segment. Parameter is id of the segment and
	// list of the elements that stent will be added
	vtkIdType AddStent(vtkIdType idSegment, vtkIdList *elementList);

	// Description:
	// Replace a stent in case of the change in some property
	void ReplaceStent(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idStent);

	// Description:
	// Remove stent.
	void RemoveStent(vtkIdType idStent);

	// Description:
	// Add stenosis in segment selected. Pass like parameter the size
	// of the effected area and return id of the stenosis
	vtkIdType AddStenosis(double size);

	// Description:
	// Add stenosis in segment. Parameter is id of the segment and
	// list of the elements that stenosis will be added
	vtkIdType AddStenosis(vtkIdType idSegment, vtkIdList *elementList);

	// Description:
	// Replace a stenosis in case of the change in some property
	void ReplaceStenosis(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idStenosis);

	// Description:
	// Remove stenosis.
	void RemoveStenosis(vtkIdType idStenosis);

	// Description:
	// Add aneurysm in segment selected. Pass like parameter the size
	// of the effected area and return id of the aneurysm
	vtkIdType AddAneurysm(double size);

	// Description:
	// Add aneurysm in segment. Parameter is id of the segment and
	// list of the elements that aneurysm will be added
	vtkIdType AddAneurysm(vtkIdType idSegment, vtkIdList *elementList);

	// Description:
	// Replace a aneurysm in case of the change in some property
	void ReplaceAneurysm(vtkIdType idSegment, vtkIdList *elementList, vtkIdType idAneurysm);

	// Description:
	// Remove aneurysm.
	void RemoveAneurysm(vtkIdType idAneurysm);

	//Description:
	// Starts the creation of actor that represent the coupling points	- USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	void GenerateCouplingRepresentation(int NumberOfActors);

	//Description:
	//	Increments the variable that controls the number of coupling actors - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	void IncrementCouplingVar(int var);

	//Description:
	// Remove a coupling actor from render - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	void RemoveCouplingActor(int ActorNumber);

	//Description:
	// Remove all coupling actors from render - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	void RemoveAllCouplingActors();

	//Description:
	//	- USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	vtkSetMacro(FullModelCode,int);
	vtkGetMacro(FullModelCode,int);

	//Description:
	//Return char of the IdList with SubTreeListSegments
	const char *GetSubTreeListSegments();

	//Description:
	//Return char of the IdList with SubTreeListTerminals
	const char *GetSubTreeListTerminals();

	//Description:
	//return true if some given actor is currently in the render window.
	bool CheckIfActorIsOnTheRender(vtkActor *a);

	// Descriptions:
	// Update grid and actors.
	void UpdateActors();

	// Descriptions:
	// Update grid stent coordinates
	void UpdateGridStent();

	// Descriptions:
	// Update grid stenosis coordinates
	void UpdateGridStenosis();

	// Descriptions:
	// Update grid aneurysm coordinates
	void UpdateGridAneurysm();

	// Descriptions:
	// Update grid of null elements 1D 3D
	void UpdateGridNullElements1D3D();

	// Descriptions:
	// Clear the list of terminals and segments in the module to detach sub-tree
	void ClearSubTreeLists ();

	// Descriptions:
	// Clear any segments selected
	void ClearElementSelected ();

	//BTX
	enum SurgicalInterventions
		{
		Clip=0,
		Stent,
		Stenosis,
		Aneurysm,
		Aging,
		DeleteIntervention,
		none
		};
	//ETX

	// Descriptions:
	// Clear render with select button clear
	void ClearPlot ();

	// Description:
	// When select Plot mode reload actors in render.
	void ReloadPlotActors ();

	// Description:
	// Highlight segment.
	void HighlightSegment(vtkIdType idSegment);
	void HighlightSegment(char *Name);

	// Description:
	// Configure a default value to all elements of the segment in the tree. This value
	// can be specified for the user or based in the length of the element of the
	// smaller segment in the tree.
	void ConfigureLengthOfElements();

	// Description:
	// Copy segments selected.
	void CopySegments(char *array);

	// Description:
	// Flip segments selected.
	void FlipSegments(char *array);

	// Description:
	// Return a list of segments for restart tree.
	const char *RestartSubTree();

	void MoveSubTree(double x[3]);

	// Description:
	// Recurssive method that create a list with the ids from terminals and
	// segments along a path specified by two terminals
	void MakePathTreeColor(vtkIdType id, vtkIdList *idListSegm, vtkIdList *idListTerm);

	// Description:
	// this paints the segments from segment list created by MakePathTreeColor
	void VisualizePath();

	// return the end and begin path from a selected path
  vtkGetMacro(BeginTerminal, int);
  vtkGetMacro(EndTerminal, int);

  // reset current path
  void ResetPathTree();

  // Description:
  // Keep a sub tree selected.
  void KeepSubTree(const char *text);

protected:
  vtkHMStraightModelWidget();
  ~vtkHMStraightModelWidget();

//BTX - manage the state of the widget
  friend class vtkHMPWCallback;
//  friend class vtkHMPointWidgetCallback;

  int State;
  enum WidgetState
  {
    Start=0,
    MovingHandle,
    MovingLine,
    Scaling,
    Outside
  };
//ETX

  //handles the events
  static void ProcessEvents(vtkObject* object,
                            unsigned long event,
                            void* clientdata,
                            void* calldata);

  // ProcessEvents() dispatches to these methods.
  void OnLeftButtonDown();
  void OnLeftButtonUp();
  void OnMiddleButtonDown();
  void OnMiddleButtonUp();
  void OnRightButtonDown();
  void OnRightButtonUp();
  void OnChar();
  virtual void OnMouseMove();

  // controlling ivars
  int Align;

//BTX
  enum AlignmentState {
    XAxis,
    YAxis,
    ZAxis,
    None
  };
//ETX

  vtkIdType					LineSelectedId;
  vtkIdType					PointSelectedId;
  void HighlightLine(int highlight);

	virtual void SizeHandles();
  void HighlightHandles(int highlight);

  // Do the picking
  vtkActor *CurrentHandle;
  vtkActor *CurrentLine;
  double LastPosition[3];
  void  SetLinePosition(double x[3]);

  // Methods to manipulate the hexahedron.
  void Scale(double *p1, double *p2, int X, int Y);

  // Initial bounds
  int   ClampToBounds;
  void  ClampPosition(double x[3]);
  int   InBounds(double x[3]);

  // Properties used to control the appearance of selected objects and
  // the manipulator in general.
  vtkProperty *HandleHeartProperty;
  vtkProperty *HandleTerminalProperty;
  vtkProperty *HandleNodeProperty;
  vtkProperty *SelectedHandleProperty;
  vtkProperty *LineProperty;
  vtkProperty *StraightModelSourceProperty;
  vtkProperty *SelectedLineProperty;
  vtkProperty *MainStraightModelProperty;
  vtkProperty *SubTreeHandleProperty;
  void CreateDefaultProperties();
  void DestructorDefaultProperties();

  vtkHMPointWidget *CurrentPointWidget;
  void EnablePointWidget();
  void DisablePointWidget();
  int ForwardEvent(unsigned long event);

  int UpdateStraightModelReader();
	void GenerateHandlePicker();
	void DestructorHandlePicker();
	void GenerateLinePicker();
	void DestructorLinePicker();
	void GeneratePointWidget();
	void GenerateCallbacks();
	void DestructorCallbacks();
	void GenerateRepresentationOfHeart();
	void GenerateRepresentation();
	void DestructorRepresentation();

	void BuildHeartRepresentation();

	// the StraightModelSource
//	vtkHM1DStraightModelSource* StraightModelSource;
	vtkHM1DStraightModelReader* StraightModelSource;
	vtkHMUnstructuredGridTo1DModel *StraightModelFilter;
	vtkHM1DHeMoLabReader *HeMoLabReader;
	vtkHM1DKeepRemoveFilter *StraightModelKeepRemoveFilter;

	vtkHM1DStraightModel* StraightModel;

	int	NumberOfPoints;
	int NumberOfTerminals;
	int NumberOfNodes;
	int NumberOfSegments;
	int NumberOfHandles;
	int CurrentNode;
	int CurrentTerminal;
	int CurrentSegment;
	int CurrentHandleActor;
	int CurrentLineActor;

	vtkIdType StraightModelElementSelected;

	// Passa qual o tipo de objeto está selecionado atualmente
	// TERMINAL, NODE, SEGMENT, HEART, ELEMENT
	vtkIdType StraightModelObjectSelected;

	vtkIdType TerminalSelected;
	vtkIdType SegmentSelected;
	vtkIdType ElementSelected;
	vtkIdType NodeSelected;

	vtkIdType LineWidgetSelected;
	vtkIdType SphereWidgetSelected;

  char* FileName;

  vtkClientServerID csId;

  // reference to the array that indicates the selected plot by the user
  vtkIntArray *PlotType;

  // Description:
  // this variable control a type of plot mode: normal or animated plot mode
  int PlotMode;

  // Variável utilizada o modo do StraightModel
  // É aqui que definimos se iremos ou não apresentar o plot ao clicarmos em
  // um elemento da interface. (StraightModelMode = 2 para mostrar gŕafico).
  int StraightModelMode;

  int Element_Selected;
  int Segment_Selected;

  // Description:
  // Actors selected in mode plot
  vtkActor *SelectedActors[MAX_CURVES];

  // Description:
  // Actor selected in mode Animatedplot
  vtkActor *AnimatedSelectedActor;

  // Description:
  // Propertys of the actors in mode plot
  vtkProperty *SelectedActorsProperty[MAX_CURVES];

  // Description:
  // Propertys of the actors in mode Animatedplot
  vtkProperty *AnimatedSelectedActorProperty;

  // Description:
  // Sum of the number of the graphics ploted
  int NumberOfGraphicsSum;

  // Description:
  // Number of the graphics in plot
  int NumberOfGraphics;

  // Description:
  // Mode of edition
  vtkIdType EditionModeSelected;
//BTX
  enum EditionSelected
  {
    EditGeometry=0,
    AddSegment,
    AddTerminal,
    DelElementInTree,
    Move,
    CheckConsistence,
    Out
  };
//ETX

	// Description:
	// Add new line widget and new points and lines in vtkHM1DStraightModelGrid.
	// Return id of the segment added.
	// xyz 								= coordinate of the points of the line;
	// numberOfElements 	= number of elements of the segment;
	// idParent 					= id of the parent of the segment;
//	void AddLineWidget(vtkIdType idSegment);
	vtkIdType AddLineWidget(double xyz[3], int numberOfElements, vtkIdType idParent);

	// Description:
	// Add new node widget.
	// xyz 				= coordinate of the node;
	// idSegment 	= id of the segment parent;
	// idNode 		= id of the node;
	void AddNodeWidget(double xyz[3], vtkIdType idSegment, vtkIdType idNode);

	// Description:
	// Add new terminal widget. Return id of the terminal added.
	// xyz 				= coordinate of the terminal;
	// idSegment 	= id of the segment parent;
	// idPoint 		= id of the point associated with terminal;
	vtkIdType AddTerminalWidget(double xyz[3], vtkIdType idSegment, vtkIdType idPoint);

	// Description:
	// Add new terminal in the terminal grid.
	// xyz		= coordinate of the terminal;
	// idTerm = id of the terminal in straightmodel.
	void AddTerminalWidget(double xyz[3], vtkIdType idTerm);

	// Description:
	// Divide one segment in two
	void DivideSegment(vtkIdType idSegment, vtkIdType idNode);

	////////////////////////////////////////////////////////
	//Property for the lines

	// Description:
  // Reference to vtkHM1DStraightModelGrid
  vtkHM1DStraightModelGrid* GridLine;

	// Description:
	// Properties for the grid of the line
	vtkPolyDataMapper *GridLineMapper;
	vtkActor *GridLineActor;
	vtkProperty *GridLineProperty;

	////////////////////////////////////////////////////////
	//Grid and property for the nodes

	// Description:
	// Grid for show nodes of the grid
	vtkHM1DStraightModelGrid *GridNode;

	// Description:
	// Properties for the grid of the nodes
	vtkPolyDataMapper *GridNodeMapper;
	vtkActor *GridNodeActor;
	vtkProperty *GridNodeProperty;

	////////////////////////////////////////////////////////
	//Grid and property for the terminals

	// Description:
	// Grid for show terminals of the grid
	vtkHM1DStraightModelGrid *GridTerminal;

	// Description:
	// Properties for the grid of the terminals
	vtkPolyDataMapper *GridTerminalMapper;
	vtkActor *GridTerminalActor;
	vtkProperty *GridTerminalProperty;

	////////////////////////////////////////////////////////
	//Property for the null elements of the coupled model

	// Description:
  // Reference to vtkHM1DStraightModelGrid
  vtkHM1DStraightModelGrid* GridNullElements1D3D;

	// Description:
	// Properties for the grid of the line
	vtkPolyDataMapper *GridNullElements1D3DMapper;
	vtkActor *GridNullElements1D3DActor;
	vtkProperty *GridNullElements1D3DProperty;

	// Description:
	// Point widget for grids
	vtkHMPointWidget *GridLineWidget;
  vtkHMPointWidget *GridNodeWidget;
  vtkHMPointWidget *GridTerminalWidget;
  vtkHMPointWidget *GridClipWidget;
  vtkHMPointWidget *GridStentWidget;
  vtkHMPointWidget *GridStenosisWidget;
  vtkHMPointWidget *GridAneurysmWidget;

  // Description:
  // Callback for grids
  vtkHMPWCallback  *GridLineCallback;
  vtkHMPointWidgetCallback *GridNodeCallback;
  vtkHMPointWidgetCallback *GridTerminalCallback;
  vtkHMPWCallback  *GridClipCallback;
  vtkHMPWCallback  *GridStentCallback;
  vtkHMPWCallback  *GridStenosisCallback;
  vtkHMPWCallback  *GridAneurysmCallback;

  // Description:
	// Pickers for grids
	vtkCellPicker *NodePicker;
	vtkCellPicker *TerminalPicker;
  vtkCellPicker *LinePicker;
  vtkCellPicker *ClipPicker;
  vtkCellPicker *StentPicker;
  vtkCellPicker *StenosisPicker;
  vtkCellPicker *AneurysmPicker;

  // Description:
  // Properties for heart representation.
  vtkActor *HeartActor;
  vtkPolyDataMapper *HeartMapper;
  vtkSphereSource *HeartGeometry;

	double RadiusTerminal;
	double RadiusNode;
	double RadiusLine;

	// Description:
	// Properties for sphere selected representation
	vtkActor *SphereActorSelected;
  vtkPolyDataMapper *SphereMapperSelected;
  vtkSphereSource *SphereSourceSelected;

  // Description:
  // Grid line selected representation
	vtkHM1DStraightModelGrid *GridLineSelected;

	// Description:
	// Mapper and actor for line selected representation
	vtkPolyDataMapper *GridLineSelectedMapper;
	vtkActor *GridLineSelectedActor;

  // Description:
  // Grid terminal selected representation
	vtkHM1DStraightModelGrid *GridTerminalSelected;

	// Description:
	// Mapper, Actor and Property for terminal selected representation
	vtkPolyDataMapper *GridTerminalSelectedMapper;
	vtkActor *GridTerminalSelectedActor;
	vtkProperty *GridTerminalSelectedProperty;

	// Description:
	// Original points of grid, used for calculate scale
	vtkPoints *Points;

	// Description:
	// Original size of each axle, used for calculate scale
	double SizePerAxle[3];

//	void CalculeAngle(double p1[3], double p2[3], double degrees[2]);

	// Description:
	// Return distance between two points
	double ComputeVectorLength(double *p1, double *p2);

	// Description:
	// Actor, mapper and source that represent the begin of the segment
	vtkActor *BeginSphereActorSelected;
	vtkPolyDataMapper *BeginSphereMapperSelected;
	vtkSphereSource *BeginSphereSourceSelected;

	// Description:
	// Actor, mapper and source that represent the end of the segment
	vtkActor *EndSphereActorSelected;
	vtkPolyDataMapper *EndSphereMapperSelected;
	vtkSphereSource *EndSphereSourceSelected;

	// Description:
	// Mode of surgical interventions selected
	int SurgicalInterventionsModeSelected;

	// Description:
	// Id of the segments associated with clip, first is segment before the clip
	// and second is segment after the clip
	vtkIntArray *ClipSegments;

	// Description:
  // Grid with clips representation
	vtkHM1DStraightModelGrid *GridClip;
	vtkActor *GridClipActor;
	vtkPolyDataMapper *GridClipMapper;

	// Description:
	// Grid clip properties
	vtkProperty *GridClipProperty;

	// Description:
  // Grid with stent representation
	vtkHM1DStentPolyData *GridStent;
	vtkActor *GridStentActor;
	vtkPolyDataMapper *GridStentMapper;

	// Description:
	// Grid stent properties
	vtkProperty *GridStentProperty;

	// Description:
  // Grid with stenosis representation
	vtkHM1DStenosisPolyData *GridStenosis;
	vtkActor *GridStenosisActor;
	vtkPolyDataMapper *GridStenosisMapper;

	// Description:
	// Grid stenosis properties
	vtkProperty *GridStenosisProperty;

	// Description:
  // Grid with aneurysm representation
	vtkHM1DAneurysmPolyData *GridAneurysm;
	vtkActor *GridAneurysmActor;
	vtkPolyDataMapper *GridAneurysmMapper;

	// Description:
	// Grid aneurysm properties
	vtkProperty *GridAneurysmProperty;

	// Description:
	// Used to create Coupling Actors when this 1D model is being used to crete a coupled model
	// - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	vtkVectorText *TextSource;
	vtkPolyDataMapper *TextMapper;
	vtkActor *TextActor;
	vtkCollection *TextCollection;
	vtkCollection *CouplingSphereSourceCollection;
	vtkCollection *CouplingSphereMapperCollection;
	vtkCollection *MapperCollection;
	vtkActorCollection *ActorCollection;
	vtkActorCollection *SphereActorCollection;

	// Description:
	// Keeps track of how many Coupling Actor are in the render
	// - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	int CouplingActorCounter;

	// Description:
	// Used to paint the coupling actor with a color related to the 3D currentely selected
  // - USED ONLY WHEN THE 1D MODEL IS BEING USED IN A COUPLED MODEL **************
	int FullModelCode;

	// Description:
  // List of terminals and segments using in ToDetachSubTree mode
	vtkIdList *SubTreeListTerminals;
	vtkIdList *SubTreeListSegments;

	// Description:
  // String that will be returned when the values of resolution data out time array to be sent to the client
  // used only by char *vtkHMStraightModelWidget::GetDataOutTimeArray(int TimeStepNumber)
  string Result;

  // Description:
  // Store information of the souce that sphereSource in plot mode
  vtkPolyDataAlgorithm *PlotSource[MAX_CURVES];

	vtkActor *AnimatedSphereActor;
	vtkSphereSource *AnimatedSphereSource;
	vtkPolyDataMapper *AnimatedSphereMapper;

	vtkActor *AnimatedLineActor;
	vtkLineSource *AnimatedLineSource;
	vtkPolyDataMapper *AnimatedLineMapper;

	//Description:
	//for control element's movement in edition mode;
	bool MoveElement;

	char *TreeListChar;
	char *TreeListChar2;

	// used to store the begin and end terminal from a selected path
	int BeginTerminal;
	int EndTerminal;




private:
  vtkHMStraightModelWidget(const vtkHMStraightModelWidget&);  //Not implemented
  void operator=(const vtkHMStraightModelWidget&);  //Not implemented
};

#endif



