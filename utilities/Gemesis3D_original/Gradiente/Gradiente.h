// Gradiente.h : main header file for the GRADIENTE application
//

#if !defined(AFX_GRADIENTE_H__277EB396_E015_4FCC_A885_2F3533448333__INCLUDED_)
#define AFX_GRADIENTE_H__277EB396_E015_4FCC_A885_2F3533448333__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGradienteApp:
// See Gradiente.cpp for the implementation of this class
//

class CGradienteApp : public CWinApp
{
public:
	CGradienteApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradienteApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGradienteApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADIENTE_H__277EB396_E015_4FCC_A885_2F3533448333__INCLUDED_)
