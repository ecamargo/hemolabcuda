// GradienteDoc.cpp : implementation of the CGradienteDoc class
//

#include "stdafx.h"
#include "Gradiente.h"

#include "GradienteDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradienteDoc

IMPLEMENT_DYNCREATE(CGradienteDoc, CDocument)

BEGIN_MESSAGE_MAP(CGradienteDoc, CDocument)
	//{{AFX_MSG_MAP(CGradienteDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGradienteDoc construction/destruction

CGradienteDoc::CGradienteDoc()
{
	// TODO: add one-time construction code here

}

CGradienteDoc::~CGradienteDoc()
{
}

BOOL CGradienteDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGradienteDoc serialization

void CGradienteDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGradienteDoc diagnostics

#ifdef _DEBUG
void CGradienteDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGradienteDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGradienteDoc commands
