// GradienteDoc.h : interface of the CGradienteDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRADIENTEDOC_H__3BD06F5A_A822_437F_BF38_CBBFC347C5A8__INCLUDED_)
#define AFX_GRADIENTEDOC_H__3BD06F5A_A822_437F_BF38_CBBFC347C5A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGradienteDoc : public CDocument
{
protected: // create from serialization only
	CGradienteDoc();
	DECLARE_DYNCREATE(CGradienteDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradienteDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGradienteDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGradienteDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADIENTEDOC_H__3BD06F5A_A822_437F_BF38_CBBFC347C5A8__INCLUDED_)
