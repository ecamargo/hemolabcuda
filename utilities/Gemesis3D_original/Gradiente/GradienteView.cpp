// GradienteView.cpp : implementation of the CGradienteView class
//

#include "stdafx.h"
#include "Gradiente.h"

#include "GradienteDoc.h"
#include "GradienteView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradienteView

IMPLEMENT_DYNCREATE(CGradienteView, CView)

BEGIN_MESSAGE_MAP(CGradienteView, CView)
	//{{AFX_MSG_MAP(CGradienteView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGradienteView construction/destruction

CGradienteView::CGradienteView()
{
	// TODO: add construction code here

}

CGradienteView::~CGradienteView()
{
}

BOOL CGradienteView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGradienteView drawing

void CGradienteView::OnDraw(CDC* pDC)
{
	CGradienteDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++) {
			int c;
			c=setOrientacion(i,j,0);
//			c=reverseColor(c);
			if(i<255)
				pDC->SetPixel(i,j,RGB(i,0,255-i));
			else if(i<512)
				pDC->SetPixel(i,j,RGB(255,i-255,0));
			else 
				pDC->SetPixel(i,j,RGB(-i+512,255,i-512));
//			pDC->SetPixel(i,j,RGB(c*N/(2*N),0,0));
		}
}

int CGradienteView::reverseColor(int c) {
	return 255-c;
}

int CGradienteView::setOrientacion(int x,int y,int orientacion) {
	if(orientacion==0) {
		return x;
	} else if(orientacion==1) {
		return y;
	} else if(orientacion==2) {
		return (int)(x+y)/2;
	} else if(orientacion==3) {
		return (int)((-x+y)/2+N/2);
	} else if(orientacion==4) {
		return (int)sqrt((x-N/2)*(x-N/2)+(y-N/2)*(y-N/2));
	}
	return 0;
}
/////////////////////////////////////////////////////////////////////////////
// CGradienteView printing

BOOL CGradienteView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CGradienteView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CGradienteView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CGradienteView diagnostics

#ifdef _DEBUG
void CGradienteView::AssertValid() const
{
	CView::AssertValid();
}

void CGradienteView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGradienteDoc* CGradienteView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGradienteDoc)));
	return (CGradienteDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGradienteView message handlers
