// GradienteView.h : interface of the CGradienteView class
//
/////////////////////////////////////////////////////////////////////////////
#include <math.h>
#if !defined(AFX_GRADIENTEVIEW_H__6B513979_637A_4C91_A535_A567ED289F49__INCLUDED_)
#define AFX_GRADIENTEVIEW_H__6B513979_637A_4C91_A535_A567ED289F49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
const int N = 3*255;

class CGradienteView : public CView
{
protected: // create from serialization only
	CGradienteView();
	DECLARE_DYNCREATE(CGradienteView)

// Attributes
public:
	CGradienteDoc* GetDocument();
	int setOrientacion(int x,int y,int orientacion);
	int reverseColor(int c);  

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradienteView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGradienteView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGradienteView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GradienteView.cpp
inline CGradienteDoc* CGradienteView::GetDocument()
   { return (CGradienteDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADIENTEVIEW_H__6B513979_637A_4C91_A535_A567ED289F49__INCLUDED_)
