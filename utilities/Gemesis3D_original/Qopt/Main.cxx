#include "openf.h"
#include "acdp.h"
#include "relaxmsh.h"

int main()
    {
	StartACDP(0, "Qopt.msg", "trace.txt");

    RelaxMesh *rmesh;
    char filename[255];
    FILE *fcfg, *fp;

    TraceOn("main");

    printf("                            QOPT - GEMESIS3D\n");
    printf("            Optimize element's quality on a tetraedral mesh\n\n");
    printf("Configuration file (enter for open_dialog):");
    gets(filename);
    if (strlen(filename) < 1)
        {
        char *filedef="*.*";
        if (!acOpenDialog(0,filename,filedef)) exit(0);
        }
    fcfg = fopen(filename, "r");
    if (!fcfg) Error (FATAL_ERROR, 1, "Can't open configuration file");

// LECTURA DE LA MALLA
    TimeOn();
    char *kwd = "INPUT_MESH";
    if (FindKeyWord(kwd, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nInput mesh from file: %s", filename);
        fp = fopen(filename, "r");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
        }
    else
        Error(FATAL_ERROR, 2, "Can't find INPUT_MESH keyword");
    Mesh3d *mesh;
    mesh = new Mesh3d;
    mesh->Read(fp);
    fclose(fp);
    TimeOff("Mesh reading", 3);

//  CALCULO DE ESTRUCTURAS AUXILIARES
    printf ("\nComputing auxiliary structures...\n");
    TimeOn();
    rmesh = new RelaxMesh(mesh);
    delete mesh;
    printf ("\nTesting input mesh...\n");
    rmesh->TestStruct();
    TimeOff("Auxiliary structures", 3);

//  LECTURA DE PARAMETROS DEL ARCHIVO DE CONFIGURACION
    long nczmax;
    char *kwd3 = "MAX_CLUSTER_SIZE";
    if (FindKeyWord(kwd3, fcfg))
        fscanf(fcfg,"%ld",&nczmax);
    else
        nczmax = 200;
    rmesh->SetNumMaxCluster(nczmax);

    int flag;
    char *kwd4 = "ADD_NODES_FLAG";
    if (FindKeyWord(kwd4, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 1;
    rmesh->SetAddNodesFlag (flag);

    char *kwd7 = "QUALITY_FLAG";
    if (FindKeyWord(kwd7, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 0;
    rmesh->SetQualityFlag (flag);

	char *kwd8 = "SEE_ALL_FLAG";
	if (FindKeyWord(kwd8, fcfg))
		fscanf(fcfg,"%d",&flag);
	else
		flag = 1;
	rmesh->SetSeeAllFlag (flag);

	char *kwd5 = "ANTI_ELEM_FLAG";
    if (FindKeyWord(kwd5, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 0;
    rmesh->SetAntiElemFlag (flag);

    long ntloop,nitnod,nitedg,nitfac;
    char *kwd2 = "MAX_ITER";
    if (FindKeyWord(kwd2, fcfg))
        fscanf(fcfg,"%ld %ld %ld %ld",&ntloop,&nitnod,&nitedg,&nitfac);
    else
        {nitnod = nitedg = nitfac = 1;}

    long nloop=1,ntch=1;


    while(ntch && nloop<ntloop+1)
        {
        ntch=0;
    //  LOOP SOBRE LOS NODOS
        printf ("\nAnalyzing node_clusters...\n");
        TimeOn();
        long nch,iter;
        nch = iter = 1;
        while (nch && iter<nitnod+1)
            {
            nch = rmesh->AnalizeAllNodes();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }
        TimeOff("Node_clusters analysis", 3);

    //  LOOP SOBRE LOS EDGES
        printf ("\nAnalyzing edge_clusters...\n");
        TimeOn();
        nch = iter = 1;
        while (nch && iter<nitedg+1)
            {
            nch = rmesh->AnalizeAllEdges();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }

        TimeOff("Edgde_clusters analysis", 3);

    //  LOOP SOBRE LAS CARAS
        printf ("\nAnalyzing face_clusters...\n");
        TimeOn();
        nch = iter = 1;
        while (nch && iter<nitfac+1)
            {
            nch = rmesh->AnalizeAllFaces();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }
        TimeOff("Face_clusters analysis", 3);

//    printf ("\nDeleting Null Elements...\n");
//    nch = rmesh->RemoveNullElements();
//    printf (" %ld elements removed",nch);

        nloop++;
    }
//  IMPRESION DE LA MALLA
    printf("Writing output mesh...\n");
    char *kwd6 = "OUTPUT_MESH";
    TimeOn();
    if (FindKeyWord(kwd6, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput mesh to file: %s", filename);
        fp = fopen(filename, "w");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
        }
    else
        {
        printf("Can't find OUTPUT_MESH keyword");
        printf("\n\nOutput mesh to file: relax3d.vwm");
        fp = fopen("relax3d.vwm", "w");
        }
    if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
    rmesh->Print(fp);
    TimeOff("Mesh writing", 3);
    fclose(fp);
    delete rmesh;
    printf("\nPress any key to quit");
    gets(filename);
    return (1);
    }

