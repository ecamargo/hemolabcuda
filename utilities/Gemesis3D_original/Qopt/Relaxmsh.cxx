#include "relaxmsh.h"
#include "aclist.h"
#include <math.h>

typedef TetElem * pTetElem;

int Edgecmp(void *a,void *b)
    {
    Edge *aa,*bb;
    aa = (Edge *)a;
    bb = (Edge *)b;
    if (aa->n1==bb->n1 && aa->n2==bb->n2) return 0;
    return 1;
    }

RelaxMesh::RelaxMesh(Mesh3d *mesh)
    {
    pTetElem  *vd;
    SparseElNo  *El  = mesh->NewElNo();

    vd = (pTetElem  *)mMalloc(sizeof(pTetElem)*El->GetTIndex());
    if(!vd) printf("Not enought memory\n");
//                                  Coloca los nodos
    for (long i=0; i<El->GetTIndex(); i++)
       {
       curre = new TetElem;
       vd[i]  = curre;
       curre->n1 = El->GetElem(i, 0);
       curre->n2 = El->GetElem(i, 1);
       curre->n3 = El->GetElem(i, 2);
       curre->n4 = El->GetElem(i, 3);
       }

    delete El;
    SparseElElV *ElV = mesh->NewElElV();
//                                  Coloca los vecinos
    for (i=0; i<ElV->GetTIndex(); i++)
        {
        curre = vd[i];
        if (ElV->GetElem(i, 0) >= 0)
            curre->v4 = vd[ElV->GetElem(i, 0)];
        else
            curre->v4 = NULL;
        if (ElV->GetElem(i, 1) >= 0)
            curre->v3 = vd[ElV->GetElem(i, 1)];
        else
            curre->v3 = NULL;
        if (ElV->GetElem(i, 2) >= 0)
            curre->v1 = vd[ElV->GetElem(i, 2)];
        else
            curre->v1 = NULL;
        if (ElV->GetElem(i, 3) >= 0)
            curre->v2 = vd[ElV->GetElem(i, 3)];
        else
            curre->v2 = NULL;
        }

//                                  Engancha la lista
    firste = vd[0];
    firste->prev = NULL;
    firste->next = vd[1];
    for (i=1; i<ElV->GetTIndex()-1; i++)
        {
        curre = vd[i];
        curre->prev = vd[i-1];
        curre->next = vd[i+1];
        }
    curre = vd[ElV->GetTIndex()-1];
    curre->prev = vd[ElV->GetTIndex()-2];
    curre->next = NULL;

//                                  Borra estructuras auxiliares
    mFree(vd);
    delete ElV;

//                                  Copia las coordenadas
    acCoordinate3D &c = *mesh->GetCoords();
    DCoord = new acVSCoord3D(mesh->GetNumNodes());
    acVSCoord3D &cv = *DCoord;
    acPoint3 p,pv;
    for (long n=0; n<mesh->GetNumNodes(); n++)
        {
        p = c[n];
        cv[n]=p;
        }
    int flagig=0;
    for (n=0; n<mesh->GetNumNodes(); n++)
        {
        p = c[n]; pv = cv[n];
        if (p.x!=pv.x || p.y!=pv.y || p.z!=pv.z) flagig=1;
        }

//												Calcula la calidad inicial
    Qmin = CalcMinQuality();
    long nodc = mesh->GetNumNodes();
    long nodv = DCoord->GetNumNodes();
    if (nodv != nodc || flagig) printf("No esta funcando...");
    }

RelaxMesh::~RelaxMesh()
    {
    TetElem *curra;
    curre=firste;
    while(curre)
        {
        curra = curre->next;
        delete curre;
        curre = curra;
        }
    delete DCoord;
    }

void RelaxMesh::TestStruct()
    {
    curre = firste;
    while (curre)
        {
        if(curre->n1<0) printf("(Test) Nodo: %ld",curre->n1);
        if(curre->n2<0) printf("(Test) Nodo: %ld",curre->n2);
        if(curre->n3<0) printf("(Test) Nodo: %ld",curre->n3);
        if(curre->n4<0) printf("(Test) Nodo: %ld",curre->n4);
        if (curre->fgc)
            printf("Quedo un flag cluster en 1\n");
        if (curre->fgm)
            printf("Quedo un flag marca en 1\n");
        if (curre->v1)
            if (curre->v1->v1!=curre &&
                curre->v1->v2!=curre &&
                curre->v1->v3!=curre &&
                curre->v1->v4!=curre)
                printf("(Test) Problemas en la estrucutura...\n");
        if (curre->v2)
            if (curre->v2->v1!=curre &&
                curre->v2->v2!=curre &&
                curre->v2->v3!=curre &&
                curre->v2->v4!=curre)
                printf("(Test) Problemas en la estrucutura...\n");
        if (curre->v3)
            if (curre->v3->v1!=curre &&
                curre->v3->v2!=curre &&
                curre->v3->v3!=curre &&
                curre->v3->v4!=curre)
                printf("(Test) Problemas en la estrucutura...\n");
        if (curre->v4)
            if (curre->v4->v1!=curre &&
                curre->v4->v2!=curre &&
                curre->v4->v3!=curre &&
                curre->v4->v4!=curre)
                printf("(Test) Problemas en la estrucutura...\n");
        curre = curre->next;
        }
/*
	Marca(firste);
	curre = firste;
	while(curre)
		{
		if (!curre->fgm) printf("Test: Mas de una componente conexa!");
		curre->fgm=0;
		curre=curre->next;
		}
*/
    }

void RelaxMesh::Marca(TetElem *el)
    {
    if(el->fgm) return;
    el->fgm=1;
    if (el->v1) Marca(el->v1);
    if (el->v2) Marca(el->v2);
    if (el->v3) Marca(el->v3);
    if (el->v4) Marca(el->v4);
    }

long RelaxMesh::RemoveNullElements()
    {
    acVSCoord3D &rcoord = *DCoord;
    acPoint3 p1,p2,p3,p4;
    acPoint2 qlt;
    long numch=0;
    curre=firste;
    while(curre)
        {
        p1 = rcoord[curre->n1];
        p2 = rcoord[curre->n2];
        p3 = rcoord[curre->n3];
        p4 = rcoord[curre->n4];
        qlt=Quality(p1,p2,p3,p4);
        if(qlt.x==0)
            {
            TetElem *ev = curre->v1;
            if(ev)
                {
                if     (ev->v1==curre) ev->v1=NULL;
                else if(ev->v2==curre) ev->v2=NULL;
                else if(ev->v3==curre) ev->v3=NULL;
                else if(ev->v4==curre) ev->v4=NULL;
                }
            ev = curre->v2;
            if(ev)
                {
                if     (ev->v1==curre) ev->v1=NULL;
                else if(ev->v2==curre) ev->v2=NULL;
                else if(ev->v3==curre) ev->v3=NULL;
                else if(ev->v4==curre) ev->v4=NULL;
                }
            ev = curre->v3;
            if(ev)
                {
                if     (ev->v1==curre) ev->v1=NULL;
                else if(ev->v2==curre) ev->v2=NULL;
                else if(ev->v3==curre) ev->v3=NULL;
                else if(ev->v4==curre) ev->v4=NULL;
                }
            ev = curre->v4;
            if(ev)
                {
                if     (ev->v1==curre) ev->v1=NULL;
                else if(ev->v2==curre) ev->v2=NULL;
                else if(ev->v3==curre) ev->v3=NULL;
                else if(ev->v4==curre) ev->v4=NULL;
                }
            numch++;
            TetElem *curra=curre->prev;
            DeleteElem(curre);
            if (!curra) {curre=firste; continue;}
            curre=curra;
            }
        curre=curre->next;
        }
    return(numch);
    }

long RelaxMesh::AnalizeAllNodes()
    {
    long numch=0;
    long NumNodes = DCoord->GetNumNodes();
    edges = 0;
    faces = 0;

    int  *msw = (int  *)mMalloc(NumNodes*sizeof(int));
    if (!msw)
        {
        printf("Error (AnalizeAllNodes): Not enought memory\n");
        exit(1);
        }

    for(long i=0;i<NumNodes;i++)
        msw[i]=0;
    curre=firste;
    while(curre)
        {
        node = curre->n1;  // Nodo 1
        if (node<NumNodes && !msw[node])
            {
            msw[node]=1;
            if (AnalizeNode()) numch++;
            }
        if (!curre)
            printf("AnalizeAllNodes-Error\n");
        node = curre->n2;  // Nodo 2
        if (node<NumNodes && !msw[node])
            {
            msw[node]=1;
            if (AnalizeNode()) numch++;
            }
        if (!curre)
            printf("AnalizeAllNodes-Error\n");
        node = curre->n3;  // Nodo 3
        if (node<NumNodes && !msw[node])
            {
            msw[node]=1;
            if (AnalizeNode()) numch++;
            }
        if (!curre)
            printf("AnalizeAllNodes-Error\n");
        node = curre->n4;  // Nodo 4
        if (node<NumNodes && !msw[node])
            {
            msw[node]=1;
            if (AnalizeNode()) numch++;
            }
        if (!curre)
            printf("AnalizeAllNodes-Error\n");
        else
            {
            curre = curre->next;   // OJO!, curre debe ser siempre != NULL
            }                      // Responsabilidad de GenerateNewElements
        }
    mFree(msw);
    long nr;
    RemoveFreeNodes();
    nr = DCoord->GetNumNodes();
	MQuality Qminn = CalcMinQuality();
	printf("Initial abs(volume)      : %lg\n"
            "New abs(volume)          : %lg\n"
            "Real volume              : %lg\n"
            "Initial quality          : %lg\n"
            "New quality              : %lg\n"
            "Initial no-positive elem.: %ld\n"
            "New no-positive elem.    : %ld\n"
            "Clusters changed         : %ld\n"
            "Old number of nodes      : %ld\n"
            "New number of nodes      : %ld\n"
            ,Qmin.avol,Qminn.avol,Qmin.vol
            ,Qmin.qlty,Qminn.qlty,Qmin.num0,Qminn.num0
            ,numch,NumNodes,nr);
	Qmin = Qminn;
    return(numch);
    }


long RelaxMesh::AnalizeAllFaces()
    {
    long numch=0;
    long NumNodes = DCoord->GetNumNodes();
    TetElem *ev;
    edges=0;
    faces=1;

    curre=firste;
    while(curre)
        {
        ev=curre->v1;
        if(ev)
            if(AnalizeFace(ev))numch++;

        ev=curre->v2;
        if(ev)
            if(AnalizeFace(ev))numch++;

        ev=curre->v3;
        if(ev)
            if(AnalizeFace(ev))numch++;

        ev=curre->v4;
        if(ev)
            if(AnalizeFace(ev))numch++;

        curre=curre->next;
        }

    long nr;
    nr = DCoord->GetNumNodes();
    MQuality Qminn = CalcMinQuality();
	printf("Initial abs(volume)      : %lg\n"
            "New abs(volume)          : %lg\n"
            "Real volume              : %lg\n"
            "Initial quality          : %lg\n"
            "New quality              : %lg\n"
            "Initial no-positive elem.: %ld\n"
            "New no-positive elem.    : %ld\n"
            "Clusters changed         : %ld\n"
            "Old number of nodes      : %ld\n"
            "New number of nodes      : %ld\n"
            ,Qmin.avol,Qminn.avol,Qmin.vol
            ,Qmin.qlty,Qminn.qlty,Qmin.num0,Qminn.num0
            ,numch,NumNodes,nr);
    Qmin = Qminn;
    faces=0;
    return(numch);
    }


long RelaxMesh::AnalizeAllEdges()
    {
    long numch=0;
    long NumNodes = DCoord->GetNumNodes();

    edges = 1;
    faces= 0;

    curre=firste;
    while(curre)
        {
        curre->edges=0;
        curre=curre->next;
        }

    int aux;
    curre=firste;
    while(curre)
        {
        aux = curre->edges;
        if ( !(aux&=1) )
            {
            node1 = curre->n1; node2 = curre->n2; //Arista n1-n2
            if (AnalizeEdge()) numch++;
            }
        aux = curre->edges;
        if ( !(aux&=2) )
            {
            node1 = curre->n1; node2 = curre->n3; //Arista n1-n3
            if (AnalizeEdge()) numch++;
            }
        aux = curre->edges;
        if ( !(aux&=4) )
            {
           node1 = curre->n1; node2 = curre->n4; //Arista n1-n4
            if (AnalizeEdge()) numch++;
            }
        aux = curre->edges;
        if ( !(aux&=8) )
            {
            node1 = curre->n2; node2 = curre->n3; //Arista n2-n3
            if (AnalizeEdge()) numch++;
            }
        aux = curre->edges;
        if ( !(aux&=16) )
            {
            node1 = curre->n2; node2 = curre->n4; //Arista n2-n4
            if (AnalizeEdge()) numch++;
            }
        aux = curre->edges;
        if ( !(aux&=32) )
            {
            node1 = curre->n3; node2 = curre->n4; //Arista n3-n4
            if (AnalizeEdge()) numch++;
            }
        if (!curre)
            printf("AnalizeAllEdges-Error\n");
        else
            curre = curre->next;   // OJO!, curre debe ser siempre != NULL
                                   // Responsabilidad de GenerateNewElements
        }
    long nr;
    nr = DCoord->GetNumNodes();
    MQuality Qminn = CalcMinQuality();
	printf("Initial abs(volume)      : %lg\n"
            "New abs(volume)          : %lg\n"
            "Real volume              : %lg\n"
            "Initial quality          : %lg\n"
            "New quality              : %lg\n"
            "Initial no-positive elem.: %ld\n"
            "New no-positive elem.    : %ld\n"
            "Clusters changed         : %ld\n"
            "Old number of nodes      : %ld\n"
            "New number of nodes      : %ld\n"
            ,Qmin.avol,Qminn.avol,Qmin.vol
            ,Qmin.qlty,Qminn.qlty,Qmin.num0,Qminn.num0
            ,numch,NumNodes,nr);
    Qmin = Qminn;
    return(numch);
    }

int RelaxMesh::AnalizeEdge()
    {
    int flag=0;
    NumFaces=0;
    currf = new FacElem;
    firstf= currf;

    CalcEdEl(curre);     // Se genera la lista del cluster

    currf = firstf;
    firstf= currf->next;
    delete currf;

    FacElem *fa=firstf;
    TetElem *el;
    int aux;
    while(fa)
        {
        el = fa->el;
        if(el)
            {
            el->fgc=1;
            el->fgm=0;
            if      ((el->n1==node1||el->n1==node2)&&(el->n2==node1||el->n2==node2))
                aux=1;
            else if ((el->n1==node1||el->n1==node2)&&(el->n3==node1||el->n3==node2))
                aux=2;
            else if ((el->n1==node1||el->n1==node2)&&(el->n4==node1||el->n4==node2))
                aux=4;
            else if ((el->n2==node1||el->n2==node2)&&(el->n3==node1||el->n3==node2))
                aux=8;
            else if ((el->n2==node1||el->n2==node2)&&(el->n4==node1||el->n4==node2))
                aux=16;
            else if ((el->n3==node1||el->n3==node2)&&(el->n4==node1||el->n4==node2))
                aux=32;
            else
                printf("Problemas en AnalizeEdge");
            el->edges|=aux;
            }
        fa = fa->next;
        }

    if (NumFaces<NumMaxCluster)
        flag = AnalizeConfig();// Se analiza se hay mejor configuracion
    else
        {
        printf("Cluster too big (%ld). Not analyzed",node);
        }

    fa = firstf; // se borra la lista del cluster
    while(fa)
        {
        if (!flag && fa->el) fa->el->fgc=0;
        firstf=fa->next;
        delete fa;
        fa = firstf;
        }

    return(flag);
    }

int RelaxMesh::AnalizeNode()
    {
    int flag=0;
    NumFaces=0;
    currf = new FacElem;
    firstf= currf;

    CalcNoEl(curre);     // Se genera la lista del cluster

    currf = firstf;
    firstf= currf->next;
    delete currf;

    FacElem *fa=firstf;
    TetElem *el;
    while(fa)
        {
        el = fa->el;
        if(el)
            {
            el->fgc=1;
            el->fgm=0;
            }
        fa = fa->next;
        }

    if (NumFaces<NumMaxCluster)
        flag = AnalizeConfig();// Se analiza se hay mejor configuracion
    else
        {
        printf("Cluster too big (%ld). Not analyzed\n",node);
        }

    fa = firstf; // se borra la lista del cluster
    while(fa)
        {
        if (!flag && fa->el) fa->el->fgc=0;
        firstf=fa->next;
        delete fa;
        fa = firstf;
        }

    return(flag);
    }

int RelaxMesh::AnalizeFace(TetElem * el)
    {
    FacElem *fa;
    int flag=0;
    long n1,n2;

    curre->fgc=1;
    el->fgc=1;

    if      (curre->v1==el)  n1=curre->n1;
    else if (curre->v2==el)  n1=curre->n2;
    else if (curre->v3==el)  n1=curre->n3;
    else    n1=curre->n4;

    if      (el->v1==curre)  n2=el->n1;
    else if (el->v2==curre)  n2=el->n2;
    else if (el->v3==curre)  n2=el->n3;
    else    n2=el->n4;

    //Creo cluster de los 2 elementos
    currf=NULL;
    if (curre->n1!=n1)
        {
        firstf= new FacElem;
        firstf->n1=curre->n2;
        firstf->n2=curre->n4;
        firstf->n3=curre->n3;
        firstf->el=curre;
        firstf->next=currf;
        currf=firstf;
        }

    if (curre->n2!=n1)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=curre->n3;
        firstf->n2=curre->n4;
        firstf->n3=curre->n1;
        firstf->el=curre;
        currf=firstf;
        }

    if (curre->n3!=n1)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=curre->n1;
        firstf->n2=curre->n4;
        firstf->n3=curre->n2;
        firstf->el=curre;
        currf=firstf;
        }

    if (curre->n4!=n1)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=curre->n1;
        firstf->n2=curre->n2;
        firstf->n3=curre->n3;
        firstf->el=curre;
        currf=firstf;
        }

    if (el->n1!=n2)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=el->n2;
        firstf->n2=el->n4;
        firstf->n3=el->n3;
        firstf->el=el;
        currf=firstf;
        }


    if (el->n2!=n2)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=el->n3;
        firstf->n2=el->n4;
        firstf->n3=el->n1;
        firstf->el=el;
        currf=firstf;
        }


    if (el->n3!=n2)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=el->n1;
        firstf->n2=el->n4;
        firstf->n3=el->n2;
        firstf->el=el;
        currf=firstf;
        }


    if (el->n4!=n2)
        {
        firstf= new FacElem;
        firstf->next=currf;
        firstf->n1=el->n1;
        firstf->n2=el->n2;
        firstf->n3=el->n3;
        firstf->el=el;
        currf=firstf;
        }

    flag=AnalizeConfig();

    fa = firstf; // se borra la lista del cluster
    while(fa)
        {
        if (!flag && fa->el) fa->el->fgc=0;
        firstf=fa->next;
        delete fa;
        fa = firstf;
        }

    return(flag);
    }

int RelaxMesh::AnalizeConfig()
    {
    acVSCoord3D &rcoord = *DCoord;
    acPoint3 p1,p2,p3,p4;
    TetElem *el;
    acPoint2 qlt;
    double qlto,qlta;
    double volo,vola;
    long nodom =-1;
    int delflg =1;
    int vpos   =0;
    int vneg   =0;
    long onum0 = 0;
// Calculo del volumen y calidad original
    qlto = 1.1;
    volo = 0.0;
    currf = firstf;
	while (currf)
		{
		el = currf->el;
		if(el)
			{
			if(el->fgc==1)
				{
				if (edges) el->fgc=2;
				else if (faces) el->fgc=3;
				p1 = rcoord[el->n1];
				p2 = rcoord[el->n2];
				p3 = rcoord[el->n3];
				p4 = rcoord[el->n4];
				qlt=Quality(p1,p2,p3,p4);
				if (qlt.x>0)vpos=1;
				else
                    {
                    onum0++;
                    if (qlt.x<0) vneg=1;
                    }
				volo+=fabs(qlt.x);
				if (qlt.y<qlto) qlto=qlt.y;
				}
            }
        else delflg = 0;
        currf = currf->next;
        }
	if (!vpos) return(0);
	if (!SeeAllFlag && qlto>0) return(0); // Solo modifica en los pliegues

// Agrando un epsilon para evitar ciclos
    qlto = qlto+0.00001*fabs(qlto);
    volo = volo-0.00001*fabs(volo);

// Se genera la lista de nodos de la cavidad
    NodElem *firstn,*currn;
    firstn = MakeNodeList();

// Analizo las configuraciones alternativas (nodos de la cavidad)
// Cuadratica en numero de caras de la cavidad!!!
    FacElem *suel;
    acPoint3 pm;
    long nodo;
    long nn=0;
    long nvneg;
    long num0;
    pm.x = pm.y = pm.z = 0.0;
    currn = firstn;
    while(currn)
        {
        nodo = currn->n;
        p4   = rcoord[nodo];
        nn++;
        pm.x+= p4.x;
        pm.y+= p4.y;
        pm.z+= p4.z;
        suel = firstf;
        qlta = 1.1;
        vola = 0.0;
        nvneg= 0;
        num0 = 0;
        while (suel)    // Toma todas las caras de la cavidad
            {
            if (suel->n1!=nodo &&     // Verifica que la cara
                suel->n2!=nodo &&     // no contenga al nodo
                suel->n3!=nodo)
                {
                p1 = rcoord[suel->n1];
                p2 = rcoord[suel->n2];
                p3 = rcoord[suel->n3];
                qlt=Quality(p1,p2,p3,p4);
                if(qlt.x<=0)
                    {
                    num0++;
                    if(qlt.x<0) nvneg=1;
                    }
                vola+=fabs(qlt.x);
                if (qlta>qlt.y) qlta=qlt.y;
                }
			suel = suel->next;
			}

/*        double CG,fvola;

        if (Qmin.avol==Qmin.vol) fvola=1e-30;
        else  fvola=Qmin.avol - Qmin.vol;

        CG=1000000*(volo - vola)/fvola +100*(onum0 - num0)/(onum0 + 1)+1*(qlta - qlto);

        if (CG>0)
            {
            nodom = nodo;
            volo  = vola;
            qlto  = qlta;
            vneg  = nvneg;
            onum0 = num0;
            }

*/
/*        double error= Qmin.avol-Qmin.vol;

			if(((error<0.0000001) && (qlta>qlto || (qlta==qlto && num0<onum0)))||
				((error>0.0000001)&& (vola<volo || (vola==volo && num0<onum0))))
				{
				nodom = nodo;
				volo  = vola;
				qlto  = qlta;
				vneg  = nvneg;
				onum0 = num0;
				}


*/

			if(((QualityFlag==1||!vneg) && (qlta>qlto || (qlta==qlto && num0<onum0)))||
				(QualityFlag==0 && (vola<volo  || (vola==volo && num0<onum0)))||
				(QualityFlag==2 && (num0<onum0 || (vola<volo && num0==onum0)))||
				(QualityFlag==3 && (num0<onum0 || (qlta>qlto && num0==onum0)))  )
				{
				nodom = nodo;
				volo  = vola;
				qlto  = qlta;
				vneg  = nvneg;
				onum0 = num0;
				}

		currn=currn->next;
		}

	currn = firstn; // se borra la lista de nodos
	while(currn)
		{
		firstn=currn->next;
		delete currn;
		currn = firstn;
		}

// Analizo configuracion alternativa con nuevo nodo al centro
	if (AddNodesFlag)
		{
        pm.x/=nn;    pm.y/=nn;    pm.z/=nn;
        suel = firstf;
        qlta = 1.1;
        vola = 0.0;
        num0 = 0;
        while (suel)    // Toma todas las caras de la cavidad
            {
            p1 = rcoord[suel->n1];
            p2 = rcoord[suel->n2];
            p3 = rcoord[suel->n3];
            qlt=Quality(p1,p2,p3,pm);
            vola+=fabs(qlt.x);
            if (qlt.x<=0)
					{
					num0++;
					if(qlt.x<0) nvneg=1;
					}
			if (qlta>qlt.y) qlta=qlt.y;
			suel = suel->next;
			}

/*        double CG,fvola;

        if (Qmin.avol==Qmin.vol) fvola=1e-30;
        else                     fvola=Qmin.avol - Qmin.vol;

        CG=1000000*(volo - vola)/(fvola) +100*(onum0 - num0)/(onum0 + 1)+1*(qlta - qlto);

        if (CG>0)
            {
			nodom = DCoord->AddCoord(pm);
            volo  = vola;
            qlto  = qlta;
            vneg  = nvneg;
            onum0 = num0;
            }
*/
/*        double error=Qmin.avol-Qmin.vol;

			if(((error<0.0000001)&& (qlta>qlto || (qlta==qlto && num0<onum0)))||
				((error>0.0000001)&& (vola<volo  || (vola==volo && num0<onum0))))
				{
				nodom = DCoord->AddCoord(pm);
				volo  = vola;
				qlto  = qlta;
				vneg  = nvneg;
				onum0 = num0;
				}
*/
			if(((QualityFlag==1||!vneg) && (qlta>qlto || (qlta==qlto && num0<onum0)))||
				(QualityFlag==0 && (vola<volo  || (vola==volo && num0<onum0)))||
				(QualityFlag==2 && (num0<onum0 || (vola<volo && num0==onum0)))||
				(QualityFlag==3 && (num0<onum0 || (qlta>qlto && num0==onum0)))  )
				{
				nodom = DCoord->AddCoord(pm);
				volo  = vola;
				qlto  = qlta;
				vneg  = nvneg;
				onum0 = num0;
				}


		if (QualityFlag || !vneg)
			{
			if (qlta > qlto || (qlta==qlto && num0<onum0))
				{
				nodom = DCoord->AddCoord(pm);
				qlto  = qlta;
				onum0 = num0;
				}
			}
		else if (vola < volo || (vola==volo && num0<onum0))
				{
				nodom = DCoord->AddCoord(pm);
				volo  = vola;
				onum0 = num0;
				}
		}

	int flag;
	if(nodom < 0) return(0);
    if      (AntiElemFlag==0) flag = 1;
    else if (AntiElemFlag==1) flag = TestLHard(nodom);
    else                      flag = TestSHard(nodom);
    if (!flag) return(0);
    GenerateNewElements(nodom);       // Se cambia la configuracion
    if (delflg && !edges && !faces) DCoord->DelCoord(node);
    return(1);
    }

NodElem *RelaxMesh::MakeNodeList()// Cuadratica en numero
                                  // de caras de la cavidad!!!
    {
    NodElem *firstn = new NodElem;
    NodElem *currn  = new NodElem;
    currn->next     = new NodElem;
    firstn->n       = firstf->n1;
	currn->n        = firstf->n2;
    currn->next->n  = firstf->n3;
    firstn->next    = currn;
    currn->next->next = NULL;
    currf = firstf->next;
    while (currf)
        {
        currn = firstn;
        while (currn)
            {
            if(currf->n1==currn->n) break;
            currn=currn->next;
            }
        if (!currn)
            {
            currn= new NodElem;
            currn->n = currf->n1;
            currn->next = firstn;
            firstn = currn;
            }
        currn = firstn;
        while (currn)
            {
            if(currf->n2==currn->n) break;
            currn=currn->next;
            }
        if (!currn)
            {
            currn= new NodElem;
            currn->n = currf->n2;
            currn->next = firstn;
            firstn = currn;
            }
        currn = firstn;
        while (currn)
            {
            if(currf->n3==currn->n) break;
            currn=currn->next;
            }
        if (!currn)
            {
            currn= new NodElem;
            currn->n = currf->n3;
            currn->next = firstn;
            firstn = currn;
            }
        currf = currf->next;
        }
    return(firstn);
    }

void RelaxMesh::CalcEdEl(TetElem *ela)
    {
    if (ela->fgm) return;
    if (NumFaces>NumMaxCluster)return;
    NumFaces ++;
    FacElem *currfa;
    ela->fgm=1;
    currfa = new FacElem;
    if(!currfa) printf("Memory exhausted for CalcEdEl");
    currf->next = currfa;
    currfa->el  = ela;
    currfa->next = new FacElem;
    if(!currfa->next) printf("Memory exhausted for CalcEdEl");
    currf = currfa->next;
    currf->el  = ela;

    if      ((ela->n1==node1||ela->n1==node2) &&
             (ela->n2==node1||ela->n2==node2))       // caso edge = n1-n2
        {
        currf->n1 = ela->n2;
        currf->n2 = ela->n4;
        currf->n3 = ela->n3;
        currfa->n1= ela->n1;
        currfa->n2= ela->n3;
        currfa->n3= ela->n4;
        if (ela->v3) CalcEdEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n4;
            currf->n3 = ela->n2;
            }
        if (ela->v4) CalcEdEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n2;
            currf->n3 = ela->n3;
            }
        }
    else if ((ela->n1==node1||ela->n1==node2) &&
             (ela->n3==node1||ela->n3==node2))       // caso edge = n1-n3
        {
        currf->n1 = ela->n1;
        currf->n2 = ela->n4;
        currf->n3 = ela->n2;
        currfa->n1= ela->n4;
        currfa->n2= ela->n3;
        currfa->n3= ela->n2;
        if (ela->v2) CalcEdEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n3;
            currf->n3 = ela->n4;
            }
        if (ela->v4) CalcEdEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n2;
            currf->n3 = ela->n3;
            }
        }
    else if ((ela->n1==node1||ela->n1==node2) &&
             (ela->n4==node1||ela->n4==node2))       // caso edge = n1-n4
        {
        currf->n1 = ela->n4;
        currf->n2 = ela->n3;
        currf->n3 = ela->n2;
        currfa->n1= ela->n1;
        currfa->n2= ela->n2;
        currfa->n3= ela->n3;
        if (ela->v3) CalcEdEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n4;
            currf->n3 = ela->n2;
            }
        if (ela->v2) CalcEdEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n3;
            currf->n3 = ela->n4;
            }
        }
    else if ((ela->n2==node1||ela->n2==node2) &&
             (ela->n3==node1||ela->n3==node2))       // caso edge = n2-n3
        {
        currf->n1 = ela->n2;
        currf->n2 = ela->n1;
        currf->n3 = ela->n4;
        currfa->n1= ela->n1;
        currfa->n2= ela->n3;
        currfa->n3= ela->n4;
        if (ela->v1) CalcEdEl(ela->v1);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n4;
            currf->n2 = ela->n3;
            currf->n3 = ela->n2;
            }
        if (ela->v4) CalcEdEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n2;
            currf->n3 = ela->n3;
            }
        }
    else if ((ela->n2==node1||ela->n2==node2) &&
             (ela->n4==node1||ela->n4==node2))       // caso edge = n2-n4
        {
        currf->n1 = ela->n1;
        currf->n2 = ela->n2;
        currf->n3 = ela->n3;
        currfa->n1= ela->n1;
        currfa->n2= ela->n3;
        currfa->n3= ela->n4;
        if (ela->v3) CalcEdEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n1;
            currf->n2 = ela->n4;
            currf->n3 = ela->n2;
            }
        if (ela->v1) CalcEdEl(ela->v1);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n4;
            currf->n2 = ela->n3;
            currf->n3 = ela->n2;
            }
        }
    else if ((ela->n3==node1||ela->n3==node2) &&
             (ela->n4==node1||ela->n4==node2))       // caso edge = n3-n4
        {
        currf->n1 = ela->n4;
        currf->n2 = ela->n2;
        currf->n3 = ela->n1;
        currfa->n1= ela->n1;
        currfa->n2= ela->n2;
        currfa->n3= ela->n3;
        if (ela->v1) CalcEdEl(ela->v1);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n4;
            currf->n2 = ela->n3;
            currf->n3 = ela->n2;
            }
        if (ela->v2) CalcEdEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem;
            if(!currf->next) printf("Memory exhausted for CalcEdEl");
            currf=currf->next;
            currf->el=NULL;
            currf->n1 = ela->n4;
            currf->n2 = ela->n1;
            currf->n3 = ela->n3;
            }
        }
    }

void RelaxMesh::CalcNoEl(TetElem *ela)// Crea la lista de elementos
    {                                // asociados a un nodo (recursiva)
    if (ela->fgm) return;
    if (NumFaces>NumMaxCluster)return;
    NumFaces ++;

    currf->next = new FacElem;
    if(!currf->next) printf("Memory exhausted for CalcNoEl");

    currf = currf->next;
    currf->el  = ela;
    ela->fgm = 1;
    if (ela->n1 == node)      // caso node = n1
        {
        currf->n1  = ela->n3;
        currf->n2  = ela->n2;
        currf->n3  = ela->n4;
        if (ela->v3)
            CalcNoEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n4,ela->n2);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");

            currf = currf->next;
            }
        if (ela->v2)
                CalcNoEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n3,ela->n4);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v4)
            CalcNoEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n2,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        }

    else if (ela->n2 == node) // caso node = n2
        {
        currf->n1  = ela->n1;
        currf->n2  = ela->n3;
        currf->n3  = ela->n4;
        if (ela->v1)
            CalcNoEl(ela->v1);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n2,ela->n4,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
    	if (ela->v3)
            CalcNoEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n4,ela->n2);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v4)
            CalcNoEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n2,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        }

    else if (ela->n3 == node) // caso node = n3
        {
        currf->n1  = ela->n2;
	    currf->n2  = ela->n1;
        currf->n3  = ela->n4;
	    if (ela->v2)
            CalcNoEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
	        currf->next = new FacElem(ela->n1,ela->n3,ela->n4);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v1)
            CalcNoEl(ela->v1);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n2,ela->n4,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v4)
            CalcNoEl(ela->v4);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n2,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        }

    else if (ela->n4 == node) // caso node = n4
        {
     currf->n1  = ela->n1;
		  currf->n2  = ela->n2;
		  currf->n3  = ela->n3;
		  if (ela->v1)
				CalcNoEl(ela->v1);
		  else
				{
				if (NumFaces>NumMaxCluster)return;
				NumFaces ++;
            currf->next = new FacElem(ela->n2,ela->n4,ela->n3);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v2)
            CalcNoEl(ela->v2);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
            currf->next = new FacElem(ela->n1,ela->n3,ela->n4);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
        if (ela->v3)
            CalcNoEl(ela->v3);
        else
            {
            if (NumFaces>NumMaxCluster)return;
            NumFaces ++;
    	    currf->next = new FacElem(ela->n1,ela->n4,ela->n2);
            if(!currf->next) printf("Memory exhausted for CalcNoEl");
            currf = currf->next;
            }
	    }
    else
        printf("Problemas al calcular NoEl...\n");
    }

int RelaxMesh::TestLHard(long nodom)
    {
    FacElem *fa,*vfa;
    TetElem *el;

// Copiar la lista del cluster
    FacElem *vfirst = new FacElem;
    *vfirst = *firstf;
    vfa = vfirst;
    fa  = firstf->next;
    while (fa)
        {
        vfa->next=new FacElem;
        vfa= vfa->next;
        *vfa=*fa;
        fa = fa->next;
        }
    vfa->next=NULL;

    vfa = firstf; firstf = vfirst; vfirst = vfa;

	 int saco = 1;   // Se barre el cluster buscando potenciales antielementos
	 while (saco)
       {
       saco = 0;
       fa = firstf;
       while (fa)
           {
           el = fa->el;
           if (el && fa->n2!=-1 && fa->n1!=nodom && fa->n2!=nodom && fa->n3!=nodom)
               {
               if      (el->n1!=fa->n1 && el->n1!=fa->n2 && el->n1!=fa->n3) el = el->v1;
               else if (el->n2!=fa->n1 && el->n2!=fa->n2 && el->n2!=fa->n3) el = el->v2;
               else if (el->n3!=fa->n1 && el->n3!=fa->n2 && el->n3!=fa->n3) el = el->v3;
               else if (el->n4!=fa->n1 && el->n4!=fa->n2 && el->n4!=fa->n3) el = el->v4;
               else    printf("Algo anda mal en TestLHard");
               if (el && (el->n1==nodom||el->n2==nodom||el->n3==nodom||el->n4==nodom))
                   {
                   FacElem *nf1 = new FacElem;
                   nf1->n1 = el->n3; nf1->n2 = el->n2; nf1->n3 = el->n4;
                   FacElem *esta1 = FindFace(nf1, firstf);
                   FacElem *nf2 = new FacElem;
                   nf2->n1 = el->n1; nf2->n2 = el->n3; nf2->n3 = el->n4;
                   FacElem *esta2 = FindFace(nf2, firstf);
                   FacElem *nf3 = new FacElem;
                   nf3->n1 = el->n2; nf3->n2 = el->n1; nf3->n3 = el->n4;
                   FacElem *esta3 = FindFace(nf3, firstf);
                   FacElem *nf4 = new FacElem;
                   nf4->n1 = el->n1; nf4->n2 = el->n2; nf4->n3 = el->n3;
                   FacElem *esta4 = FindFace(nf4, firstf);

                   int esta=0;
                   if (esta1) esta++;
                   if (esta2) esta++;
                   if (esta3) esta++;
                   if (esta4) esta++;
                   if (esta > 1)
                       {
                       saco = 1;
                       if (esta1)
                           { esta1->n2=-1; delete nf1; }
                       else
                           { nf1->next=firstf; firstf = nf1; nf1->el=el; el->fgc++; }
                       if (esta2)
                           { esta2->n2=-1; delete nf2; }
                       else
                           { nf2->next=firstf; firstf = nf2; nf2->el=el;  el->fgc++;}
                       if (esta3)
                           { esta3->n2=-1; delete nf3; }
                       else
                           { nf3->next=firstf; firstf = nf3; nf3->el=el;  el->fgc++;}
                       if (esta4)
                           { esta4->n2=-1; delete nf4; }
                       else
                           { nf4->next=firstf; firstf = nf4; nf4->el=el;  el->fgc++;}
                       }
                   else
                       {
                       delete nf1;
                       delete nf2;
                       delete nf3;
                       delete nf4;
                       }
                   }
               }
           fa=fa->next;
           }
       }

    int ok= TestSHard(nodom);
    if (ok)                                     //borrar lista vieja
        {
//        if (saco) printf("Saque algun antielemento...\n");
        DeleteList(vfirst);
        fa = firstf;
        while(fa)
           {
           if (fa->el && fa->n2==-1)
               {
               FacElem *fau = firstf;      // verifica que el elemento no este 2 veces
               while (fau)
                   {
                   if (fa!=fau && fa->el==fau->el) break;
                   fau=fau->next;
                   }
               if (!fau) DeleteElem(fa->el); // Si estaba 1 vez lo borra
               else fa->el->fgc--;
               if (fa!=firstf)
                   {
                   vfa->next=fa->next;
                   delete fa;
                   fa = vfa;
                   }
               else
                   {
                   firstf=fa->next;
                   delete fa;
                   fa = firstf;
                   continue;
                   }
               }
           vfa=fa;
           fa = fa->next;
           }
        }
    else                                        //borrar lista nueva
        {
        fa = firstf;
        while (fa)
            {
            if (fa->el) fa->el->fgc=0;
            fa=fa->next;
            }
        DeleteList(firstf);
        firstf=vfirst;
        }
    return(ok);
    }

FacElem *RelaxMesh::FindFace(FacElem *nfa, FacElem *fa)
    {
    while (fa)
        {
        if ((fa->n1==nfa->n1 || fa->n2==nfa->n1 || fa->n3==nfa->n1 )&&
            (fa->n1==nfa->n2 || fa->n2==nfa->n2 || fa->n3==nfa->n2 )&&
            (fa->n1==nfa->n3 || fa->n2==nfa->n3 || fa->n3==nfa->n3 )) break;
        fa=fa->next;
        }
    return(fa);
    }

int RelaxMesh::TestSHard(long nodom)
    {
    FacElem *First=firstf;
    long     Node = node;
    FacElem *fa,*fan;
    TetElem *el;

    fa = firstf;             //buscamos viejo elemento que tenga a nodom
    while (fa)
        {
        el = fa->el;
        if (el && (el->n1==nodom||el->n2==nodom||el->n3==nodom||el->n4==nodom))
            break;
        fa = fa->next;
        }
    if(!fa) return(1);

    node=nodom;
    NumFaces=0;
    currf = new FacElem;
    firstf= currf;
    CalcNoEl(el);     // Se genera la lista del cluster
    currf = firstf;
    firstf= currf->next;
    delete currf;

    node = Node;
    fa = First; First = firstf; firstf = fa;
    if (NumFaces>NumMaxCluster)
        { DeleteList(First); return(0); }

    fa = First;
    while(fa)
       {
       if (fa->el) fa->el->fgm=0;
       fa = fa->next;
       }

    fan= First; // Se borran elementos internos al cluster original
    while(fan)
        {
        if(fan->el && fan->el->fgc)
            {
            if (fan!=First)
                { fa->next=fan->next; delete fan; fan = fa; }
            else
                { First = fan->next; delete fan; fan = First; continue; }
            }
        fa =fan;
        fan=fan->next;
        }

    // Se crean las listas de aristas
    int ok=1;
    Edge ed;
    acList *ednode,*ednodom;
    ednode = new acList(sizeof(Edge),Edgecmp);
    fa = firstf;                            // Lista asociada a node
    while(fa)
        {
        if (!(fa->n1==nodom||fa->n2==nodom||fa->n3==nodom||fa->n2==-1))
            {
            if (fa->n1<fa->n2)
                {
                ed.n1=fa->n1; ed.n2=fa->n2;
                if (ednode->SearchSeq(&ed)) {ok=0; break;} // arista repetida
                ednode->InsertFirst(&ed);
                }
            if (fa->n2<fa->n3)
                {
                ed.n1=fa->n2; ed.n2=fa->n3;
                if (ednode->SearchSeq(&ed)) {ok=0; break;} // arista repetida
                ednode->InsertFirst(&ed);
                }
            if (fa->n3<fa->n1)
                {
                ed.n1=fa->n3; ed.n2=fa->n1;
                if (ednode->SearchSeq(&ed)) {ok=0; break;} // arista repetida
                ednode->InsertFirst(&ed);
                }
            }
        fa = fa->next;
        }

    if (!ok)
        {
        delete ednode;
        DeleteList(First);
        return(0);
        }

    fa = firstf;  // se borran algunas
    while(fa)
        {
        if (fa->n2!=-1)
            {
            if      (fa->n1==nodom && fa->n3<fa->n2)
                {
                ed.n1=fa->n3; ed.n2=fa->n2;
                ednode->DelNode(&ed);
                }
            else if (fa->n2==nodom && fa->n1<fa->n3)
                {
                ed.n1=fa->n1; ed.n2=fa->n3;
                ednode->DelNode(&ed);
                }
            else if (fa->n3==nodom && fa->n2<fa->n1)
                {
                ed.n1=fa->n2; ed.n2=fa->n1;
                ednode->DelNode(&ed);
                }
            }
        fa = fa->next;
        }

    ednodom= new acList(sizeof(Edge),Edgecmp);
    fa = First;
    while(fa)                               // Lista asociada a nodom
        {
        if (fa->n1<fa->n2)
            {
            ed.n1=fa->n1; ed.n2=fa->n2;
            ednodom->InsertFirst(&ed);
            }
        if (fa->n2<fa->n3)
            {
            ed.n1=fa->n2; ed.n2=fa->n3;
            ednodom->InsertFirst(&ed);
            }
        if (fa->n3<fa->n1)
            {
            ed.n1=fa->n3; ed.n2=fa->n1;
            ednodom->InsertFirst(&ed);
            }
        fa = fa->next;
        }

    acNodeList *nl= ednode->GetHead();
    while (nl)
        {
        ed = *((Edge *)nl->GetDados());
        if (ednodom->SearchSeq(&ed)) {ok=0; break;}
        nl = nl->next;
        }

    delete ednode;
    delete ednodom;
    DeleteList(First);
    return(ok);
    }

void RelaxMesh::DeleteList(FacElem *first)
    {
    FacElem *fa=first;
    FacElem *fant;
    while (fa)
        {
        if(fa->el) fa->el->fgm=0;
        fant = fa;
        fa = fa->next;
        delete fant;
        }
    }

void RelaxMesh::GenerateNewElements(long nodom)
    {
    FacElem *fa;
    TetElem *el,*elf;
    elf = new TetElem;
    nfirst = new FacElem;
    nfirst->el=NULL;
    currf = firstf;
    while (currf)
        {
        el = currf->el;
        if (currf->n1 == nodom || currf->n2 == nodom || currf->n3 == nodom)
            {                    //No hay que hacer elemento
            if(el)
                {
                fa = new FacElem;
                fa->next=nfirst; nfirst=fa; fa->n1=-1;
                if      (el->n1 != currf->n1 && // busco al vecino
                         el->n1 != currf->n2 &&
                         el->n1 != currf->n3)    fa->el = el->v1;
                else if (el->n2 != currf->n1 &&
                         el->n2 != currf->n2 &&
                         el->n2 != currf->n3)    fa->el = el->v2;
                else if (el->n3 != currf->n1 &&
                         el->n3 != currf->n2 &&
                         el->n3 != currf->n3)    fa->el = el->v3;
                else if (el->n4 != currf->n1 &&
                         el->n4 != currf->n2 &&
                         el->n4 != currf->n3)    fa->el = el->v4;
                else
                        {
                        printf("Error en la GenerateNewElement\n");
                        exit(1);
                        }
                // Marco la cara en cuestion en el vecino
                if (fa->el)
                    {
                    if     (fa->el->v1==el) fa->el->v1=elf;
                    else if(fa->el->v2==el) fa->el->v2=elf;
                    else if(fa->el->v3==el) fa->el->v3=elf;
                    else if(fa->el->v4==el) fa->el->v4=elf;
                    else
                        {
                        printf("Palmo en GenerateNewElements\n");
                        exit(1);
                        }

                    // si ya estaba en la lista lo borro para que no este dos veces
                    FacElem *fprev=fa;
                    FacElem *faux=fa->next;
                    while(faux)
                        {
                        if (faux->el == fa->el)
                            {
                            fprev->next=faux->next;
                            delete faux;
                            break;
                            }
                        fprev= faux;
                        faux = faux->next;
                        }
                    }
                }
            }
        else            // Creo un nuevo elemento
            {
            TetElem *eln = new TetElem;
            fa  = new FacElem;
            fa->el  = eln;
            fa->next= nfirst;
            fa->n1=1;
            nfirst = fa;
            eln->n1 = currf->n1;
            eln->n2 = currf->n2;
            eln->n3 = currf->n3;
            eln->n4 = nodom;
    if(eln->n1<0||eln->n2<0||eln->n3<0||eln->n4<0)
        printf("%ld %ld %ld %ld",eln->n1,eln->n2,eln->n3,eln->n4);
            if (!el) eln->v4 = NULL;
            else if (el->n1!=currf->n1 && el->n1!=currf->n2 && el->n1!=currf->n3)
                     eln->v4 = el->v1;
            else if (el->n2!=currf->n1 && el->n2!=currf->n2 && el->n2!=currf->n3)
                     eln->v4 = el->v2;
            else if (el->n3!=currf->n1 && el->n3!=currf->n2 && el->n3!=currf->n3)
                     eln->v4 = el->v3;
            else if (el->n4!=currf->n1 && el->n4!=currf->n2 && el->n4!=currf->n3)
                     eln->v4 = el->v4;
            if (eln->v4)
                {
                if      (eln->v4->v1==el) eln->v4->v1=eln;
                else if (eln->v4->v2==el) eln->v4->v2=eln;
                else if (eln->v4->v3==el) eln->v4->v3=eln;
                else if (eln->v4->v4==el) eln->v4->v4=eln;
                else printf("la cagamos en Generate");
                }
            eln->v1=elf;
            eln->v2=elf;
            eln->v3=elf;
                                                    //enganchar (antes de firste)
            eln->next=firste;
            eln->prev=NULL;
            firste->prev=eln;
            firste=eln;
            }
        currf = currf->next;
        }

    //buscar los vecinos internos (marcados con elf)
    currf = nfirst;
    while (currf)
        {
        el = currf->el;
        if(el) FindVecs(el, elf);
        currf = currf->next;
        }
    delete elf;

    //borrar viejos elementos
    fa = firstf;
    while (fa)
        {
        el = fa->el;
        if(el)
            {
            if (el->fgc>1) el->fgc--;
            else         DeleteElem(el);
            }
        fa = fa->next;
        }

    fa = nfirst; // se borra la lista del cluster
    while(fa)
        {
        nfirst=fa->next;
        delete fa;
        fa = nfirst;
        }
    }

void RelaxMesh::DeleteElem(TetElem *el)
    {
    if (el==curre)
        {
        if (curre->next) curre = curre->next;
        else             curre = curre->prev;
        }
    if (el->prev) el->prev->next = el->next;
    else firste = el->next;
    if (el->next) el->next->prev = el->prev;
    delete el;
    }

void RelaxMesh::FindVecs(TetElem *el, TetElem *elf)
    {
    TetElem *elv;
    FacElem *fa;
    if (el->v1==elf)				     // CARA 1 (n2,n4,n3)
        {
        fa = currf->next;
        while (fa)
            {
            elv = fa->el;
            if (elv)
                if (el->n2==elv->n1 || el->n2==elv->n2 ||
                    el->n2==elv->n3 || el->n2==elv->n4)
                    if (el->n4==elv->n1 || el->n4==elv->n2 ||
                        el->n4==elv->n3 || el->n4==elv->n4)
                        if (el->n3==elv->n1 || el->n3==elv->n2 ||
                            el->n3==elv->n3 || el->n3==elv->n4)
                            {
                            el->v1=elv;
                            break;
                            }
            fa = fa->next;
            }
        if (el->v1==elf) el->v1=NULL;
        else
            {
            if     (elv->n1!=el->n2 && elv->n1!=el->n4 && elv->n1!=el->n3)
                {
                if (elv->v1 != elf) printf("Mas de dos elementos en una cara");
                elv->v1 = el;
                }
            else if(elv->n2!=el->n2 && elv->n2!=el->n4 && elv->n2!=el->n3)
                {
                if (elv->v2 != elf) printf("Mas de dos elementos en una cara");
                elv->v2 = el;
                }
            else if(elv->n3!=el->n2 && elv->n3!=el->n4 && elv->n3!=el->n3)
                {
                if (elv->v3 != elf) printf("Mas de dos elementos en una cara");
                elv->v3 = el;
                }
            else if(elv->n4!=el->n2 && elv->n4!=el->n4 && elv->n4!=el->n3)
                {
                if (elv->v4 != elf) printf("Mas de dos elementos en una cara");
                elv->v4 = el;
                }
            }
        }

    if (el->v2==elf)				     // CARA 2 (n1,n3,n4)
        {
        fa = currf->next;
        while (fa)
            {
            elv = fa->el;
            if (elv)
                if (el->n1==elv->n1 || el->n1==elv->n2 ||
                    el->n1==elv->n3 || el->n1==elv->n4)
                    if (el->n3==elv->n1 || el->n3==elv->n2 ||
                        el->n3==elv->n3 || el->n3==elv->n4)
                        if (el->n4==elv->n1 || el->n4==elv->n2 ||
                            el->n4==elv->n3 || el->n4==elv->n4)
                            {
                            el->v2=elv;
                            break;
                            }
            fa = fa->next;
            }
        if (el->v2==elf) el->v2=NULL;
        else
            {
            if     (elv->n1!=el->n1 && elv->n1!=el->n3 && elv->n1!=el->n4)
                {
                if (elv->v1 != elf) printf("Mas de dos elementos en una cara");
                elv->v1 = el;
                }
            else if(elv->n2!=el->n1 && elv->n2!=el->n3 && elv->n2!=el->n4)
                {
                if (elv->v2 != elf) printf("Mas de dos elementos en una cara");
                elv->v2 = el;
                }
            else if(elv->n3!=el->n1 && elv->n3!=el->n3 && elv->n3!=el->n4)
                {
                if (elv->v3 != elf) printf("Mas de dos elementos en una cara");
                elv->v3 = el;
                }
            else if(elv->n4!=el->n1 && elv->n4!=el->n3 && elv->n4!=el->n4)
                {
                if (elv->v4 != elf) printf("Mas de dos elementos en una cara");
                elv->v4 = el;
                }
            }
        }

    if (el->v3==elf)				     // CARA 3 (n1,n4,n2)
        {
        fa = currf->next;
        while (fa)
            {
            elv = fa->el;
            if (elv)
                if (el->n1==elv->n1 || el->n1==elv->n2 ||
                    el->n1==elv->n3 || el->n1==elv->n4)
                    if (el->n4==elv->n1 || el->n4==elv->n2 ||
                        el->n4==elv->n3 || el->n4==elv->n4)
                        if (el->n2==elv->n1 || el->n2==elv->n2 ||
                            el->n2==elv->n3 || el->n2==elv->n4)
                            {
                            el->v3=elv;
                            break;
                            }
            fa = fa->next;
            }
        if (el->v3==elf) el->v3=NULL;
        else
            {
            if     (elv->n1!=el->n1 && elv->n1!=el->n4 && elv->n1!=el->n2)
                {
                if (elv->v1 != elf) printf("Mas de dos elementos en una cara");
                elv->v1 = el;
                }
            else if(elv->n2!=el->n1 && elv->n2!=el->n4 && elv->n2!=el->n2)
                {
                if (elv->v2 != elf) printf("Mas de dos elementos en una cara");
                elv->v2 = el;
                }
            else if(elv->n3!=el->n1 && elv->n3!=el->n4 && elv->n3!=el->n2)
                {
                if (elv->v3 != elf) printf("Mas de dos elementos en una cara");
                elv->v3 = el;
                }
            else if(elv->n4!=el->n1 && elv->n4!=el->n4 && elv->n4!=el->n2)
                {
                if (elv->v4 != elf) printf("Mas de dos elementos en una cara");
                elv->v4 = el;
                }
            }
        }

    if (el->v4==elf)				     // CARA 4 (n1,n2,n3)
        {
        fa = currf->next;
        while (fa)
            {
            elv = fa->el;
            if (elv)
                if (el->n1==elv->n1 || el->n1==elv->n2 ||
                    el->n1==elv->n3 || el->n1==elv->n4)
                    if (el->n2==elv->n1 || el->n2==elv->n2 ||
                        el->n2==elv->n3 || el->n2==elv->n4)
                        if (el->n3==elv->n1 || el->n3==elv->n2 ||
                            el->n3==elv->n3 || el->n3==elv->n4)
                            {
                            el->v4=elv;
                            break;
                            }
            fa = fa->next;
            }
        if (el->v4==elf) el->v4=NULL;
        else
            {
            if     (elv->n1!=el->n1 && elv->n1!=el->n2 && elv->n1!=el->n3)
                {
                if (elv->v1 != elf) printf("Mas de dos elementos en una cara");
                elv->v1 = el;
                }
            else if(elv->n2!=el->n1 && elv->n2!=el->n2 && elv->n2!=el->n3)
                {
                if (elv->v2 != elf) printf("Mas de dos elementos en una cara");
                elv->v2 = el;
                }
            else if(elv->n3!=el->n1 && elv->n3!=el->n2 && elv->n3!=el->n3)
                {
                if (elv->v3 != elf) printf("Mas de dos elementos en una cara");
                elv->v3 = el;
                }
            else if(elv->n4!=el->n1 && elv->n4!=el->n2 && elv->n4!=el->n3)
                {
                if (elv->v4 != elf) printf("Mas de dos elementos en una cara");
                elv->v4 = el;
                }
            }
        }
    }

MQuality RelaxMesh::CalcMinQuality()
    {
    acPoint2 qlty;
    MQuality qmesh;
    acVSCoord3D &rcoord = *DCoord;
    acPoint3 p1,p2,p3,p4;
    TetElem *el;

    qmesh.avol=0.0;
    qmesh.vol =0.0;
    qmesh.qlty=1.1;
    qmesh.num0=  0;
    el=firste;
    while(el)
        {
        p1 = rcoord[el->n1];
        p2 = rcoord[el->n2];
        p3 = rcoord[el->n3];
        p4 = rcoord[el->n4];
        qlty=Quality(p1,p2,p3,p4);
        qmesh.avol+= fabs(qlty.x);
        qmesh.vol += qlty.x;
        if (qlty.x <= 0) qmesh.num0++;
        if (qlty.y<qmesh.qlty) qmesh.qlty = qlty.y;
        el = el->next;
        }
    qmesh.qlty*=72.0;
//    return(qmesh.qlty*1832.8208);
    return(qmesh);
    }

acPoint2 RelaxMesh::Quality(acPoint3 &p1,acPoint3 &p2,acPoint3 &p3,acPoint3 &p4)
    {
    acPoint2 qlty;
    // Calculo el volumen
    double x21,y21,z21,x31,y31,z31,x41,y41,z41;
    double d1,d2,d3,vol;

    x21 = p2.x-p1.x; y21 = p2.y-p1.y; z21 = p2.z-p1.z;
    x31 = p3.x-p1.x; y31 = p3.y-p1.y; z31 = p3.z-p1.z;
    x41 = p4.x-p1.x; y41 = p4.y-p1.y; z41 = p4.z-p1.z;
    d1 = y31*z41 - z31*y41;
    d2 = y41*z21 - z41*y21;
    d3 = y21*z31 - z21*y31;
    vol = (x21 * d1 + x31 * d2 + x41 * d3) / 6.0;
    qlty.x = vol;

    // Calculo la calidad
    double dx,dy,dz;
    double maxleng2, leng2;

    dx = p2.x-p1.x; dy = p2.y-p1.y; dz = p2.z-p1.z;
    maxleng2=dx*dx+dy*dy+dz*dz;
    dx = p3.x-p1.x; dy = p3.y-p1.y; dz = p3.z-p1.z;
    leng2=dx*dx+dy*dy+dz*dz;
    if(leng2 > maxleng2) maxleng2=leng2;
    dx = p4.x-p1.x; dy = p4.y-p1.y; dz = p4.z-p1.z;
    leng2=dx*dx+dy*dy+dz*dz;
    if(leng2 > maxleng2) maxleng2=leng2;
    dx = p3.x-p2.x; dy = p3.y-p2.y; dz = p3.z-p2.z;
    leng2=dx*dx+dy*dy+dz*dz;
    if(leng2 > maxleng2) maxleng2=leng2;
    dx = p4.x-p2.x; dy = p4.y-p2.y; dz = p4.z-p2.z;
    leng2=dx*dx+dy*dy+dz*dz;
    if(leng2 > maxleng2) maxleng2=leng2;
    dx = p4.x-p3.x; dy = p4.y-p3.y; dz = p4.z-p3.z;
    leng2=dx*dx+dy*dy+dz*dz;
    if(leng2 > maxleng2) maxleng2=leng2;
    qlty.y=(vol*fabs(vol))/(maxleng2*maxleng2*maxleng2);

    return(qlty);  //calcula fast quality
    }
/*
double RelaxMesh::Quality(acPoint3 &p1,acPoint3 &p2,acPoint3 &p3,acPoint3 &p4)
    {
    double dx,dy,dz;
    double maxleng2;

    dx = p2.x-p1.x; dy = p2.y-p1.y; dz = p2.z-p1.z;
    maxleng2 =sqrt(dx*dx+dy*dy+dz*dz);
    dx = p3.x-p1.x; dy = p3.y-p1.y; dz = p3.z-p1.z;
    maxleng2+=sqrt(dx*dx+dy*dy+dz*dz);
    dx = p4.x-p1.x; dy = p4.y-p1.y; dz = p4.z-p1.z;
    maxleng2+=sqrt(dx*dx+dy*dy+dz*dz);
    dx = p3.x-p2.x; dy = p3.y-p2.y; dz = p3.z-p2.z;
    maxleng2+=sqrt(dx*dx+dy*dy+dz*dz);
    dx = p4.x-p2.x; dy = p4.y-p2.y; dz = p4.z-p2.z;
    maxleng2+=sqrt(dx*dx+dy*dy+dz*dz);
    dx = p4.x-p3.x; dy = p4.y-p3.y; dz = p4.z-p3.z;
    maxleng2+=sqrt(dx*dx+dy*dy+dz*dz);

    return(Vol/(maxleng2*maxleng2*maxleng2));  //calcula fast quality
    }
*/

void RelaxMesh::RemoveFreeNodes()
    {
    if (DCoord->CompactCoord())
        {
        curre = firste;
        while (curre)
            {
            if(curre->n1<0) printf("Nodo: %ld",curre->n1);
            if(curre->n2<0) printf("Nodo: %ld",curre->n2);
            if(curre->n3<0) printf("Nodo: %ld",curre->n3);
            if(curre->n4<0) printf("Nodo: %ld",curre->n4);
            curre->n1 = DCoord->NewIndex(curre->n1);
            curre->n2 = DCoord->NewIndex(curre->n2);
            curre->n3 = DCoord->NewIndex(curre->n3);
            curre->n4 = DCoord->NewIndex(curre->n4);
            curre = curre->next;
            }
        }
    }

void RelaxMesh::Print(FILE *fp)
    {
    DCoord->Print(fp);
    long nel=0;
    curre=firste;
    while(curre)
        {
        nel++;
        curre = curre->next;
        }
    fprintf(fp,"*ELEMENT GROUPS\n 1\n 1 %ld Tetra4\n",nel);
    curre=firste;
    fprintf(fp,"*INCIDENCE\n");
    while(curre)
        {
        fprintf(fp,"%ld %ld %ld %ld\n",
            curre->n1+1,curre->n2+1,curre->n3+1,curre->n4+1);
        curre = curre->next;
        }
    fprintf(fp,"*END\n");
    }
