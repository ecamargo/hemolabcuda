/*
** vscoo3D.cxx - Enzo et al, 8/95
*/

#include "vscoo3d.h"
#include "rotation.h"

// ************
// Constructors
// ************

acVSCoord3D::acVSCoord3D(long nod)
    {
    static char *nome = "acVSCoord3D::acVSCoord3D";
    long i, NeededVectors;
    TraceOn(nome);

    NNodes = nod;
    NeededVectors = nod / VSC3D_ELEMS_PER_VECTOR + 1;
    NVectors = NeededVectors + VSC3D_GROWTH;
    VCoords = (acPoint3l **) mMalloc (NVectors*sizeof(acPoint3l *));
    if (!VCoords)
    	ErrMem();

    for (i = 0; i < NeededVectors; i++)
	    {
	    VCoords[i] = (acPoint3l *)
	        mMalloc (VSC3D_ELEMS_PER_VECTOR * sizeof (acPoint3l));
	    if (!VCoords[i])
	        ErrMem();
	    }
    for (; i < NVectors; i++)
	    VCoords[i] = NULL;

    for (i = 0; i < NNodes; i++)
	    this->GetP(i)->l = -1;
    FirstFree = i;	// NNodes
    while (i < NeededVectors * VSC3D_ELEMS_PER_VECTOR)
	    {
	    this->GetP(i)->l = i+1;  // Next Free Node
	    i++;
	    }
    Perm = NULL;
    TraceOff(nome);
    }

// **********
// Destructor
// **********
void acVSCoord3D::Free()
    {
    if (NNodes == 0) return;
    for (long i = 0; i < NVectors && VCoords[i]; i++)
	mFree(VCoords[i]);
    mFree(VCoords);
    VCoords = NULL;
    NNodes = NVectors = 0;
    if (Perm) mFree(Perm);
    Perm = NULL;
    SizePerm = 0;
    }

// ***
// I/O
// ***
void acVSCoord3D::Read(FILE *fp)
    {
    static char *nome = "acVSCoord3D::Read";
    long i, NeededVectors;
    char buf[80];
    long dummy;
    static char *palcha = "COORDINATES";

    TraceOn(nome);

    if (NNodes)
	{
	Error(WARNING, 1, "An object of class Coordinate exist and will be eliminated");
	Free();
	}

    if (!FindKeyWord(palcha, fp))
	{//  27, "Keyword |%s| Not Found."
	GetError(27,buf);
	Error(FATAL_ERROR, 2, buf, palcha);
	}

    fscanf(fp, "%ld", &NNodes);
    if (NNodes < 1)
	Error(FATAL_ERROR, 3, "Number of nodes negative or null");
    NeededVectors = NNodes / VSC3D_ELEMS_PER_VECTOR + 1;
    NVectors = NeededVectors + VSC3D_GROWTH;
    VCoords = (acPoint3l **) mMalloc (NVectors*sizeof(acPoint3l *));
    if (!VCoords)
	ErrMem();

    for (i = 0; i < NeededVectors; i++)
	{
	VCoords[i] = (acPoint3l *)
	    mMalloc (VSC3D_ELEMS_PER_VECTOR * sizeof (acPoint3l));
	if (!VCoords[i])
	    ErrMem();
	}
    for (; i < NVectors; i++)
	VCoords[i] = NULL;

    for (i = 0; i < NNodes; i++)
	{
	double x, y, z;
	fscanf(fp, "%ld %lf %lf %lf", &dummy, &x, &y, &z);
	if (dummy != i + 1)
	    {
	    sprintf(buf,
		"Coordinate::Read -> Incorrect data for node %ld", i + 1);
	    Error(FATAL_ERROR, 4, buf);
	    }
	this->GetP(i)->c.x = x;
	this->GetP(i)->c.y = y;
	this->GetP(i)->c.z = z;
	this->GetP(i)->l = -1;     // Mark: used
	}

    FirstFree = NNodes;
    while (i < NeededVectors * VSC3D_ELEMS_PER_VECTOR)
	{
	this->GetP(i)->l = i+1;  // Next Free Node
	i++;
	}

    TraceOff(nome);
    }

void acVSCoord3D::Print(FILE *fp)
    {
    static char *nome = "acVSCoord3D::Print";
    TraceOn(nome);
    fprintf(fp, "\n*COORDINATES\n %ld\n",NNodes);
    for (long i = 0; i < NNodes; i++)
	fprintf(fp, "%ld %.16lg %.16lg %.16lg\n",
		i+1, GetP(i)->c.x, GetP(i)->c.y, GetP(i)->c.z);
    TraceOff(nome);
    }

void acVSCoord3D::Save(char *nametab, int version, char *nomefile)
    {
    static char *nome = "acVSCoord3D::Save";
    FILE *fp;

    TraceOn(nome);

    fp = OpenSave(nametab, version, nomefile);
    gFWrite((char  *) &NNodes, sizeof(long), 1, fp);
    long NNeededVectors = 0;
    for (; NNeededVectors < NVectors; NNeededVectors++)
	if (VCoords[NNeededVectors] == NULL) break;
    gFWrite((char  *) &NNeededVectors, sizeof(long), 1, fp);
    gFWrite((char  *) &FirstFree, sizeof(long), 1, fp);
    gFWrite((char  *) &SizePerm, sizeof(long), 1, fp);

    for (long i = 0; i<NNeededVectors; i++)
	gFWrite((char  *) VCoords[i], sizeof(acPoint3l),
	    VSC3D_ELEMS_PER_VECTOR, fp);

    if (SizePerm > 0)
	gFWrite((char  *) &Perm, sizeof(long), SizePerm, fp);
    CloseSave(fp, nomefile);

    TraceOff(nome);
    }

void acVSCoord3D::Restore(char *nametab, int version, char *nomefile)
    {
    static char *nome = "acVSCoord3D::Restore";
    char buf[100];

    TraceOn(nome);

    if (NNodes > 0)
	{ //  22, "An Object Of Class |%s| Exists And Will Be Eliminated."
	GetError(22, buf);
	Error(WARNING, 2, buf, "acVSCoord3D");
	Free();
	}

    FILE *fp;

    if ((fp = OpenRestore(nametab, version, nomefile)) == NULL)
	{ //   13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
	GetError(13, buf);
	Error(FATAL_ERROR, 3, buf, nametab, version, nomefile);
	}

    gFRead((char  *) &NNodes, sizeof(long), 1, fp);
    long NNeededVectors;
    gFRead((char  *) &NNeededVectors, sizeof(long), 1, fp);
    NVectors = NNeededVectors + VSC3D_GROWTH;
    gFRead((char  *) &FirstFree, sizeof(long), 1, fp);
    gFRead((char  *) &SizePerm, sizeof(long), 1, fp);

    VCoords = (acPoint3l **) mMalloc(sizeof(acPoint3l *) * NVectors);
    if (!VCoords)
	ErrMem();

    for (long i = 0; i<NNeededVectors; i++)
	{
	VCoords[i] = (acPoint3l *)
	    mMalloc(sizeof(acPoint3l) * VSC3D_ELEMS_PER_VECTOR);
	if (!VCoords[i])
	    ErrMem();
	gFRead((char  *) VCoords[i], sizeof(acPoint3l),
	    VSC3D_ELEMS_PER_VECTOR, fp);
	}
    if (SizePerm)
	{
	Perm = (long *) mMalloc(sizeof(long) * SizePerm);
	if (!Perm)
	    ErrMem();
	gFRead((char  *) Perm, sizeof(long), SizePerm, fp);
	}

    CloseRestore(fp);

    TraceOff(nome);
    }

void acVSCoord3D::DXFOut(FILE *fp)
    {
    char *nome  = "acVSCoord3D::DXFOut";
    TraceOn(nome);
    for (long i=0; i<NNodes; i++)
	{
	fprintf(fp,"VERTEX\n  8\n0\n");
	fprintf(fp," 10\n%lf\n 20\n%lf\n 30\n%lf\n 70\n 192\n  0\n",
	       GetP(i)->c.x, GetP(i)->c.y, GetP(i)->c.z);
	}
    TraceOff(nome);
    }

//DXFIn

// **********
// Operations
// **********
acLimit3 acVSCoord3D::Box()
    {
    acLimit3 limits(0.0,0.0,0.0,0.0,0.0,0.0);
    if (NNodes <= 0) return limits;
    long i, n, maxIndex, usedVectors;
    for (usedVectors = 0; usedVectors < NVectors && VCoords[usedVectors];
	 usedVectors++);
    maxIndex = usedVectors * VSC3D_ELEMS_PER_VECTOR;
    n = 0; int first = 1;
    for (i = 0; i < maxIndex && n < NNodes; i++)
	{
	if (GetP(i)->l == -1)
	    {
	    n++;
	    if (first)
		{
		limits.xmin = limits.xmax = GetP(i)->c.x;
		limits.ymin = limits.ymax = GetP(i)->c.y;
		limits.zmin = limits.zmax = GetP(i)->c.z;
		first = 0;
		}
	    else
		{
		if (GetP(i)->c.x < limits.xmin)
		    limits.xmin = GetP(i)->c.x;
		else if (GetP(i)->c.x > limits.xmax)
		    limits.xmax = GetP(i)->c.x;
		if (GetP(i)->c.y < limits.ymin)
		    limits.ymin = GetP(i)->c.y;
		else if (GetP(i)->c.y > limits.ymax)
		    limits.ymax = GetP(i)->c.y;
		if (GetP(i)->c.z < limits.zmin)
		    limits.zmin = GetP(i)->c.z;
		else if (GetP(i)->c.z > limits.zmax)
		    limits.zmax = GetP(i)->c.z;
		}
	    }
	}

    return limits;
    }

void acVSCoord3D::Rotate(acVSCoord3D &coor, acRotation *t)
    {
    long n;
    static char *nome="acVSCoord3D::Rotate";

    TraceOn(nome);

    if(NNodes != coor.NNodes)
	Error(FATAL_ERROR, 1, "Objects of different size");

    for (n = 0; n < NNodes; n++)
       {
       GetP(n)->c = t->GetXYZ((acPoint3 &)coor[n]);
       }

    TraceOff(nome);
    }

// ************************
// Variable size operations
// ************************

void acVSCoord3D::FreePerm()
    {
    if (Perm) mFree(Perm);
    Perm = NULL;
    SizePerm = 0;
    }

long acVSCoord3D::AddCoord (acPoint3 &point)
    {
    long vector, offset;
    long Position = FirstFree;
    vector = Position / VSC3D_ELEMS_PER_VECTOR;
    offset = Position % VSC3D_ELEMS_PER_VECTOR;
    if (offset == 0)	// May have changed vector (reserved?)
	{
	if (vector == NVectors)	// Must realloc VCoords
	    {
	    long newNVectors = NVectors + VSC3D_GROWTH;
	    acPoint3l **newVCoords = (acPoint3l **)
		mMalloc (newNVectors * sizeof (acPoint3l *));
	    if (!newVCoords)
		ErrMem();
        long j;
	    for (j = 0; j<NVectors; j++)
            newVCoords[j] = VCoords[j];
	    for (;j<newNVectors; j++)
            newVCoords[j] = NULL;
	    mFree(VCoords);
	    NVectors = newNVectors;
	    VCoords = newVCoords;
	    }
	if (VCoords[vector] == NULL)	// Must reserve memory
	    {
	    VCoords[vector] = (acPoint3l *)
		mMalloc(VSC3D_ELEMS_PER_VECTOR * sizeof (acPoint3l));
	    if (!VCoords[vector])
		ErrMem();
	    for (long j=Position; j<(vector+1)*VSC3D_ELEMS_PER_VECTOR; j++)
		this->GetP(j)->l = j+1;
	    }
	}

    FirstFree = VCoords[vector][offset].l;
    VCoords[vector][offset].l = -1;
    VCoords[vector][offset].c = point;
//    VCoords[vector][offset].c.x = point.x;
//    VCoords[vector][offset].c.y = point.y;
//    VCoords[vector][offset].c.z = point.z;
    this->GetP(Position)->l = -1;
    NNodes++;
    return Position;
    }

void acVSCoord3D::DelCoord (long index)
    {
    long vector, offset;
    vector = index / VSC3D_ELEMS_PER_VECTOR;
    offset = index % VSC3D_ELEMS_PER_VECTOR;
    if (index < 0 || vector > NVectors)
	Error(FATAL_ERROR, 1, "DelCoord: index out of range");
    if (VCoords[vector] == NULL)
	Error(FATAL_ERROR, 2, "DelCoord: index out of range");
    if (VCoords[vector][offset].l >= 0)
	{
	Error(WARNING, 2, "DelCoord: point %ld already deleted", index);
	return;
	}
    VCoords[vector][offset].l = FirstFree;
    FirstFree = index;
    NNodes--;
    return;
    }

int acVSCoord3D::CompactCoord (void)
    {
    int mustpack = 0;
    long i;
    for (i = 0; i < NNodes; i++)
	    if (this->GetP(i)->l >= 0)
	        {
	        mustpack = 1;
	        break;
	        }
    if (mustpack)
	    {
	    long NVecs, NNodesMax;
	    for (NVecs = 0; NVecs < NVectors && VCoords[NVecs] != NULL; NVecs++);
	        NNodesMax = NVecs * VSC3D_ELEMS_PER_VECTOR;
	    if (Perm) FreePerm();
	    SizePerm = NNodesMax;
	    Perm = (long  *) mMalloc(SizePerm*sizeof(long));
	    if (!Perm)
	        ErrMem();

    	long k=0;
	    for (i=0; i<NNodesMax; i++)
	        {
	        if (this->GetP(i)->l < 0)
		        {
    		    Perm[i] = k;
	    	    this->GetP(k)->c.x = this->GetP(i)->c.x;
	        	this->GetP(k)->c.y = this->GetP(i)->c.y;
    		    this->GetP(k)->c.z = this->GetP(i)->c.z;
                this->GetP(k)->l =-1;
		        k++;
	    	    }
	        else
    		    Perm[i] = -1;
	        }
/*
	long pf, pv;
	pf = 0;
	pv = NNodes;
	while (1)
	    {
	    // Busca un lugar libre en la zona a ocupar
	    while (pf < NNodes && this->GetP(pf)->l < 0)
	    	{
		    Perm[pf] = pf;
		    pf++;
		    }
	    // Busca un lugar ocupado en la zona a liberar
	    while (pv < NNodesMax && this->GetP(pv)->l >= 0)
		    {
		    Perm[pv] = -1;
		    pv++;
		    }
	    if (pf == NNodes)
		break;
	    *(this->GetP(pf)) = *(this->GetP(pv));
	    Perm[pf] = -1;
	    Perm[pv] = pf;
	    }
	long newNNeededVectors = NNodes / VSC3D_ELEMS_PER_VECTOR + 1;
	for (i = newNNeededVectors; i < NVectors; i++)
	    {
	    if (VCoords[i] != NULL)
		    {
		    mFree(VCoords[i]);
		    VCoords[i] = NULL;
		    }
	    }
	NNodesMax = newNNeededVectors * VSC3D_ELEMS_PER_VECTOR;

	if (newNNeededVectors < NVectors - VSC3D_SHRINK)
	    { // realloc VCoords
	    long newNVectors = newNNeededVectors + VSC3D_GROWTH;
	    acPoint3l  **newVCoords;
	    newVCoords = (acPoint3l **)
		mMalloc(newNVectors * sizeof(acPoint3l *));
	    if (!newVCoords)
		ErrMem();

	    for(i = 0; i<newNNeededVectors; i++)
		newVCoords[i] = VCoords[i];
	    for(; i<newNVectors; i++)
		newVCoords[i] = NULL;
	    mFree(VCoords);

	    VCoords = newVCoords;
	    }
*/
	FirstFree = NNodes;
	for (i = NNodes; i < NNodesMax; i++)
	    this->GetP(i)->l = i+1;
	}
    return mustpack;
    }

long acVSCoord3D::NewIndex (long oldindex)
    {
    long NI;
    if (!Perm)
	    Error(FATAL_ERROR, 1, "NewIndex: Perm vector not present");
    if (oldindex < 0 || oldindex >= SizePerm)
	    Error(FATAL_ERROR, 1, "NewIndex: oldindex out of range");
    NI = Perm[oldindex];
    if (NI < 0)
	    Error(FATAL_ERROR, 1, "NewIndex: invalid oldindex");
    return NI;
    }

