#include "openf.h"
#include "tquality.h"

int main()
    {
    //Inicializacion de ACDP
    StartACDP(0, "seeqlty.sai", "trace.txt");
    InitDB();
    TraceOn("seeqlty");

    char name[256];

    //Apertura del archivo con la malla de volumen y lectura
    Mesh3d *vmesh;
    vmesh = new Mesh3d;

    printf("                      SeeQlty program\n");
    printf("Find the worst angles in a 3D-mesh with tetraedral elements\n");
    printf("\n(Press enter to begin)");
    gets(name);
//    getch();
    char *filedef="*.*";
    if (!acOpenDialog(0,name,filedef)) exit(0);

    FILE *fp= fopen(name, "r");
    if (!fp) Error (FATAL_ERROR, 1, "Can't open mesh file");

    printf("Reading mesh...\n");
    vmesh->Read(fp);
    fclose(fp);

    printf("Number of nodes   : %ld\n"
           "Number of elements: %ld\n"
           "Number of groups  : %d\n\n",
           vmesh->GetNumNodes(),vmesh->GetNumEls(),vmesh->GetNumGroups());

    printf("Calculating angles...\n");
    TetQuality *qlty = new TetQuality(vmesh);

    double diemax,diemin,diemaxav,dieminav;
    long eldiemax,eldiemin;
    qlty->GetDieMax(eldiemax,diemax);
    qlty->GetDieMin(eldiemin,diemin);
    qlty->GetDieAve(diemaxav,dieminav);
    printf("Maximum diedral angle: %lg (element %ld)\n"
           "Minimum diedral angle: %lg (element %ld)\n\n",
           diemax,eldiemax,diemin,eldiemin);

    double facemax,facemin,facemaxav,faceminav;
    long elfacemax,elfacemin;
    qlty->GetFaceMax(elfacemax,facemax);
    qlty->GetFaceMin(elfacemin,facemin);
    qlty->GetFaceAve(facemaxav,faceminav);
    printf("Maximum face angle: %lg (element %ld)\n"
           "Minimum face angle: %lg (element %ld)\n\n",
           facemax,elfacemax,facemin,elfacemin);

    long elvolmin,elvolmax;
    double volmax,volmin;
    qlty->GetVolMax(elvolmax,volmax);
    qlty->GetVolMin(elvolmin,volmin);
    printf("Maximum element volume: %lg (element %ld)\n"
           "Minimum element volume: %lg (element %ld)\n",
           volmax,elvolmax,volmin,elvolmin);

    printf("\n(Press enter to quit)");
    gets(name);
    return (1);
	}

