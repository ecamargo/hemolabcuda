#include <math.h>
#include "tquality.h"

void CalcAng(acPoint3 *coor, double *d, double *c, double &vol);

TetQuality::TetQuality(Mesh3d *mesh)
    {
    acPoint3 coor[4];
    long iel;
    int i;
    char nome[]="TetQuality::TetQuality";
    TraceOn(nome);

    NumElems = mesh->GetNumEls();

    Qlty = (qlty *) mMalloc(sizeof(qlty) * NumElems);
    if (!Qlty)
        Error(2, 1, "TetQuality -> can't allocate memory");

    // Calculo de calidades
    double DieMax,DieMin, FaceMax,FaceMin, VolMax,VolMin;
    double d[6], c[12], vol;
    DieMax = FaceMax = VolMax = (double)-1.0e30;
    DieMin = FaceMin = VolMin = (double)1.0e30;
    DieAveMax  = 0;
    DieAveMin  = 0;
    FaceAveMax = 0;
    FaceAveMin = 0;
    Element *el;

    acCoordinate3D &Coord = *mesh->GetCoords();
    long n0,n1,n2,n3;
    iel = 0;
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem(), iel++)
        {
        Qlty[iel].Flag = 1;
        n0=el->GetNode(0); coor[0]=Coord[n0];
        n1=el->GetNode(1); coor[1]=Coord[n1];
        n2=el->GetNode(2); coor[2]=Coord[n2];
        n3=el->GetNode(3); coor[3]=Coord[n3];

        CalcAng(coor, d, c, vol); // Calculo de los angulos y volumen
        Qlty[iel].Vol     = vol;
        Qlty[iel].DieMax  = d[0];
        Qlty[iel].DieMin  = d[0];
        Qlty[iel].FaceMax = c[0];
        Qlty[iel].FaceMin = c[0];
        for (i=1; i<6; i++)
            {
            if      (Qlty[iel].DieMax < d[i]) Qlty[iel].DieMax = d[i];
            else if (Qlty[iel].DieMin > d[i]) Qlty[iel].DieMin = d[i];
            }
        for (i=1; i<12; i++)
            {
            if      (Qlty[iel].FaceMax < c[i]) Qlty[iel].FaceMax = c[i];
            else if (Qlty[iel].FaceMin > c[i]) Qlty[iel].FaceMin = c[i];
            }

// Calcula promedios globales
        DieAveMax  += Qlty[iel].DieMax;
        DieAveMin  += Qlty[iel].DieMin;
        FaceAveMax += Qlty[iel].FaceMax;
        FaceAveMin += Qlty[iel].FaceMin;

// Calcula maximos y minimos globales
        if (Qlty[iel].DieMax > DieMax)
            {
            DieMax = Qlty[iel].DieMax;
            ElDieMax = iel;
            }
        if (Qlty[iel].DieMin < DieMin)
            {
            DieMin = Qlty[iel].DieMin;
            ElDieMin = iel;
            }
        if (Qlty[iel].FaceMax > FaceMax)
            {
            FaceMax = Qlty[iel].FaceMax;
            ElFaceMax = iel;
            }
        if (Qlty[iel].FaceMin < FaceMin)
            {
            FaceMin = Qlty[iel].FaceMin;
            ElFaceMin = iel;
            }
        if (Qlty[iel].Vol > VolMax)
            {
            VolMax = Qlty[iel].Vol;
            ElVolMax = iel;
            }
        if (Qlty[iel].Vol < VolMin)
            {
            VolMin = Qlty[iel].Vol;
            ElVolMin = iel;
            }
        }

        DieAveMax  /= NumElems;
        DieAveMin  /= NumElems;
        FaceAveMax /= NumElems;
        FaceAveMin /= NumElems;
    TraceOff(nome);
    }

TetQuality::~TetQuality()
    {
    mFree((char *) Qlty);
    }

void TetQuality::PrintDiedralAngles(FILE *fp, long NumDiv)
    {
    }
void TetQuality::PrintFaceAngles(FILE *fp, long NumDiv)
    {
    }
void TetQuality::PrintMaxDiedralAngles(FILE *fp, long NumDiv)
    {
    }
void TetQuality::PrintMinDiedralAngles(FILE *fp, long NumDiv)
    {
    }
void TetQuality::PrintMaxFaceAngles(FILE *fp, long NumDiv)
    {
    }
void TetQuality::PrintMinFaceAngles(FILE *fp, long NumDiv)
    {
    }

void CalcAng(acPoint3 *coor, double *d, double *c, double &vol)
    {
    double pi = 3.14159265359;
    double x21,y21,z21, x31,y31,z31, x41,y41,z41, x32,y32,z32, x42,y42,z42, x43,y43,z43;
    double x123,y123,z123,o123, x124,y124,z124,o124, x134,y134,z134,o134, x234,y234,z234,o234;
    double o21,o31,o41,o32,o42,o43;

    x21  = coor[1].x - coor[0].x;
    y21  = coor[1].y - coor[0].y;
    z21  = coor[1].z - coor[0].z;

    x31  = coor[2].x - coor[0].x;
    y31  = coor[2].y - coor[0].y;
    z31  = coor[2].z - coor[0].z;

    x41  = coor[3].x - coor[0].x;
    y41  = coor[3].y - coor[0].y;
    z41  = coor[3].z - coor[0].z;

    x32  = coor[2].x - coor[1].x;
    y32  = coor[2].y - coor[1].y;
    z32  = coor[2].z - coor[1].z;

    x42  = coor[3].x - coor[1].x;
    y42  = coor[3].y - coor[1].y;
    z42  = coor[3].z - coor[1].z;

    x43  = coor[3].x - coor[2].x;
    y43  = coor[3].y - coor[2].y;
    z43  = coor[3].z - coor[2].z;

    x123 = y21*z31 - z21*y31;
    y123 = z21*x31 - x21*z31;
    z123 = x21*y31 - y21*x31;
    o123 = sqrt(x123*x123 + y123*y123 + z123*z123);

    x124 = y41*z21 - z41*y21;
    y124 = z41*x21 - x41*z21;
    z124 = x41*y21 - y41*x21;
    o124 = sqrt(x124*x124 + y124*y124 + z124*z124);

    x134 = y31*z41 - z31*y41;
    y134 = z31*x41 - x31*z41;
    z134 = x31*y41 - y31*x41;
    o134 = sqrt(x134*x134 + y134*y134 + z134*z134);

    x234 = y42*z32 - z42*y32;
    y234 = z42*x32 - x42*z32;
    z234 = x42*y32 - y42*x32;
    o234 = sqrt(x234*x234 + y234*y234 + z234*z234);

    d[0] = (x123*x124 + y123*y124 + z123*z124)/(o123*o124);
    d[1] = (x123*x134 + y123*y134 + z123*z134)/(o123*o134);
    d[2] = (x134*x124 + y134*y124 + z134*z124)/(o134*o124);
    d[3] = (x123*x234 + y123*y234 + z123*z234)/(o123*o234);
    d[4] = (x124*x234 + y124*y234 + z124*z234)/(o124*o234);
    d[5] = (x134*x234 + y134*y234 + z134*z234)/(o134*o234);
    if (d[0] < -1.0) d[0] = -1.0;
    if (d[0] >  1.0) d[0] =  1.0;
    if (d[1] < -1.0) d[1] = -1.0;
    if (d[1] >  1.0) d[1] =  1.0;
    if (d[2] < -1.0) d[2] = -1.0;
    if (d[2] >  1.0) d[2] =  1.0;
    if (d[3] < -1.0) d[3] = -1.0;
    if (d[3] >  1.0) d[3] =  1.0;
    if (d[4] < -1.0) d[4] = -1.0;
    if (d[4] >  1.0) d[4] =  1.0;
    if (d[5] < -1.0) d[5] = -1.0;
    if (d[5] >  1.0) d[5] =  1.0;

    d[0] = (pi-acos(d[0])) * 180 / pi;
    d[1] = (pi-acos(d[1])) * 180 / pi;
    d[2] = (pi-acos(d[2])) * 180 / pi;
    d[3] = (pi-acos(d[3])) * 180 / pi;
    d[4] = (pi-acos(d[4])) * 180 / pi;
    d[5] = (pi-acos(d[5])) * 180 / pi;

    o21 = sqrt(x21*x21 + y21*y21 + z21*z21);
    o31 = sqrt(x31*x31 + y31*y31 + z31*z31);
    o41 = sqrt(x41*x41 + y41*y41 + z41*z41);
    o32 = sqrt(x32*x32 + y32*y32 + z32*z32);
    o42 = sqrt(x42*x42 + y42*y42 + z42*z42);
    o43 = sqrt(x43*x43 + y43*y43 + z43*z43);
    c[0] = (x21*x31+y21*y31+z21*z31) / (o21*o31);
    c[1] =-(x21*x32+y21*y32+z21*z32) / (o21*o32);
    c[3] = (x21*x41+y21*y41+z21*z41) / (o21*o41);
    c[4] =-(x21*x42+y21*y42+z21*z42) / (o21*o42);
    c[6] = (x32*x42+y32*y42+z32*z42) / (o32*o42);
    c[7] =-(x32*x43+y32*y43+z32*z43) / (o32*o43);
    c[9] = (x31*x41+y31*y41+z31*z41) / (o31*o41);
    c[10]=-(x31*x43+y31*y43+z31*z43) / (o31*o43);
    if (c[0] < -1.0) c[0] = -1.0;
    if (c[0] >  1.0) c[0] =  1.0;
    if (c[1] < -1.0) c[1] = -1.0;
    if (c[1] >  1.0) c[1] =  1.0;
    if (c[3] < -1.0) c[3] = -1.0;
    if (c[3] >  1.0) c[3] =  1.0;
    if (c[4] < -1.0) c[4] = -1.0;
    if (c[4] >  1.0) c[4] =  1.0;
    if (c[6] < -1.0) c[6] = -1.0;
    if (c[6] >  1.0) c[6] =  1.0;
    if (c[7] < -1.0) c[7] = -1.0;
    if (c[7] >  1.0) c[7] =  1.0;
    if (c[9] < -1.0) c[9] = -1.0;
    if (c[9] >  1.0) c[9] =  1.0;
    if (c[10]< -1.0) c[10]= -1.0;
    if (c[10]>  1.0) c[10]=  1.0;

    c[0] = acos(c[0]);
    c[1] = acos(c[1]);
    c[2] = pi - c[0] - c[1];
    c[3] = acos(c[3]);
    c[4] = acos(c[4]);
    c[5] = pi - c[3] - c[4];
    c[6] = acos(c[6]);
    c[7] = acos(c[7]);
    c[8] = pi - c[6] - c[7];
    c[9] = acos(c[9]);
    c[10]= acos(c[10]);
    c[11]= pi - c[9] - c[10];

    c[0]  = c[0] * 180 / pi;
    c[1]  = c[1] * 180 / pi;
    c[2]  = c[2] * 180 / pi;
    c[3]  = c[3] * 180 / pi;
    c[4]  = c[4] * 180 / pi;
    c[5]  = c[5] * 180 / pi;
    c[6]  = c[6] * 180 / pi;
    c[7]  = c[7] * 180 / pi;
    c[8]  = c[8] * 180 / pi;
    c[9]  = c[9] * 180 / pi;
    c[10] = c[10]* 180 / pi;
    c[11] = c[11]* 180 / pi;

    vol = (x123*x41 + y123*y41 + z123*z41) / 6;
    }
