/*
** Contiene las calidades de los elementos
*/

#ifndef TETQUALITY_H
#define TETQUALITY_H

#include "mesh3d.h"

struct qlty
    {
    long  Flag;
    double DieMax;
    double DieMin;
    double FaceMax;
    double FaceMin;
    double Vol;
    };

class TetQuality
    {
    protected:
        long NumElems;
        long ElDieMax, ElDieMin, ElFaceMax, ElFaceMin, ElVolMax, ElVolMin;
        double DieAveMax,DieAveMin, FaceAveMax,FaceAveMin;
        qlty *Qlty;

    public:
        TetQuality()
            {
            NumElems = 0;
            Qlty     = NULL;
            }

        TetQuality(Mesh3d *mesh);

        void PrintDiedralAngles(FILE *fp, long NumDiv);
        void PrintFaceAngles(FILE *fp, long NumDiv);
        void PrintMaxDiedralAngles(FILE *fp, long NumDiv);
        void PrintMinDiedralAngles(FILE *fp, long NumDiv);
        void PrintMaxFaceAngles(FILE *fp, long NumDiv);
        void PrintMinFaceAngles(FILE *fp, long NumDiv);

        long GetNumElems() { return(NumElems); }
        void GetDieMax(long &el, double &q)
            {
            el = ElDieMax;
            q  = Qlty[el].DieMax;
            }

        void GetDieMin(long &el, double &q)
            {
            el = ElDieMin;
            q  = Qlty[el].DieMin;
            }

        void GetDieAve(double &qmin, double &qmax)
            {
            qmin = DieAveMax;
            qmax = DieAveMin;
            }

        void GetFaceMax(long &el, double &q)
            {
            el = ElFaceMax;
            q  = Qlty[el].FaceMax;
            }

        void GetFaceMin(long &el, double &q)
            {
            el = ElFaceMin;
            q  = Qlty[el].FaceMin;
            }

        void GetFaceAve(double &qmin, double &qmax)
            {
            qmin = FaceAveMax;
            qmax = FaceAveMin;
            }

        void GetVolMax(long &el, double &q)
            {
            el = ElVolMax;
            q  = Qlty[el].Vol;
            }

        void GetVolMin(long &el, double &q)
            {
            el = ElVolMin;
            q  = Qlty[el].Vol;
            }

       ~TetQuality();
    };

#endif // TETQUALITY_H
