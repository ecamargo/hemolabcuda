// ElementInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "ElementInfoDlg.h"
//#include "acdp.h"

#include "ViewMeshDoc.h"
#include "ViewMeshView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementInfoDlg dialog

BOOL ElementInfoDlg::Create(GraphMesh *g)
    {
    graph = g;
	m_quality = 0.0;
	m_centroid = _T("");
	m_nodes = _T("");

    return CDialog::Create(ElementInfoDlg::IDD);
    }

ElementInfoDlg::ElementInfoDlg(CView *p)
    {
    m_pView = p;
    }


ElementInfoDlg::ElementInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ElementInfoDlg::IDD, pParent)
{
    m_pView = 0;
	//{{AFX_DATA_INIT(ElementInfoDlg)
	m_quality = 0.0;
	m_centroid = _T("");
	m_nodes = _T("");
	m_number = 0;
	//}}AFX_DATA_INIT
}


void ElementInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ElementInfoDlg)
	DDX_Text(pDX, IDC_EDIT_QUALITY, m_quality);
	DDX_Text(pDX, IDC_EDIT_CENTROID, m_centroid);
	DDX_Text(pDX, IDC_EDIT_NODES, m_nodes);
	DDX_Text(pDX, IDC_EDIT_NUMBER, m_number);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ElementInfoDlg, CDialog)
	//{{AFX_MSG_MAP(ElementInfoDlg)
	ON_BN_CLICKED(IDC_BUTTON_WHERE_IS, OnButtonWhereIs)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE, OnButtonUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ElementInfoDlg message handlers

void ElementInfoDlg::OnOK() 
    {
	// TODO: Add extra validation here
	CDialog::OnOK();
    if (m_pView != NULL)
        m_pView->SendMessage(ID_GOODBYE_ELEMENT, IDOK);
    }

void ElementInfoDlg::OnButtonWhereIs() 
    {
	// TODO: Add your control notification handler code here
    UpdateData(TRUE);
    graph->SetelEl(m_number-1);
    CViewMeshView *pviewm = (CViewMeshView *) m_pView;
    pviewm->WhereIs();
    }


void ElementInfoDlg::OnCancel() 
    {
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
    if (m_pView != NULL)
        m_pView->SendMessage(ID_GOODBYE_ELEMENT, IDCANCEL);
    }

void ElementInfoDlg::OnButtonUpdate() 
    {
	// TODO: Add your control notification handler code here
    UpdateData(TRUE);
    char buf[80];

    long numelems = graph->GetNumElems();

    long elem = m_number;
    elem--;

    if (elem>=0 && elem<numelems)
		{
		int ibuf, nnoel;
		long node;
		acPoint3 gc;
		Element *el = graph->SetAnElem(elem);
		nnoel = el->NNoel();
		ibuf = 0;
		for (int i=0; i<nnoel; i++)
		    {
			node = el->GetNode(i)+1;
			ibuf += sprintf(buf+ibuf,"%ld  ",node);
			}

        m_nodes = buf;
        m_quality = el->Quality();
        gc = el->GravCenter();
        sprintf(buf, "%g  %g  %g", gc.x, gc.y, gc.z);
        m_centroid = buf;    
	    }
    else
        {
        m_nodes = "";
        m_centroid = "";
        m_quality = 0.0;
        }
    UpdateData(FALSE);	
    }
