#if !defined(AFX_ELEMENTINFODLG_H__966189E2_90DA_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_ELEMENTINFODLG_H__966189E2_90DA_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ElementInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ElementInfoDlg dialog

class GraphMesh;

class ElementInfoDlg : public CDialog
{
public:
    CView *m_pView;
    GraphMesh *graph;

// Construction
public:
	ElementInfoDlg(CWnd* pParent = NULL);   // standard constructor
    ElementInfoDlg(CView *);
    BOOL Create(GraphMesh *);

// Dialog Data
	//{{AFX_DATA(ElementInfoDlg)
	enum { IDD = IDD_DIALOG_ELEMENT_INFO };
	double	m_quality;
	CString	m_centroid;
	CString	m_nodes;
	long	m_number;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ElementInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ElementInfoDlg)
	virtual void OnOK();
	afx_msg void OnButtonWhereIs();
	virtual void OnCancel();
	afx_msg void OnButtonUpdate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ELEMENTINFODLG_H__966189E2_90DA_11D1_AA5E_0060979BBB38__INCLUDED_)
