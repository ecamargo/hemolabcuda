// GenInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "GenInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GenInfoDlg dialog


GenInfoDlg::GenInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GenInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GenInfoDlg)
	m_tot_elems = 0;
	m_tot_nodes = 0;
	//}}AFX_DATA_INIT
}


void GenInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GenInfoDlg)
	DDX_Text(pDX, IDC_EDIT_TOTAL_ELEMENTS, m_tot_elems);
	DDX_Text(pDX, IDC_EDIT_TOTAL_NODES, m_tot_nodes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GenInfoDlg, CDialog)
	//{{AFX_MSG_MAP(GenInfoDlg)
	ON_BN_CLICKED(IDC_BUTTON_MORE, OnButtonMore)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GenInfoDlg message handlers

void GenInfoDlg::OnButtonMore() 
    {
	// TODO: Add your control notification handler code here
    EndDialog(IDC_BUTTON_MORE);	
    }
