#if !defined(AFX_GENINFODLG_H__941C2323_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_GENINFODLG_H__941C2323_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GenInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GenInfoDlg dialog

class GenInfoDlg : public CDialog
{
// Construction
public:
	GenInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GenInfoDlg)
	enum { IDD = IDD_DIALOG_GENERAL_INFO };
	long	m_tot_elems;
	long	m_tot_nodes;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GenInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GenInfoDlg)
	afx_msg void OnButtonMore();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENINFODLG_H__941C2323_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_)
