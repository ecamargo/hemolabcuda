// GenInformMoreDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "GenInformMoreDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GenInformMoreDlg dialog


GenInformMoreDlg::GenInformMoreDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GenInformMoreDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GenInformMoreDlg)
	m_avg_quality = 0.0;
	m_min_quality = 0.0;
	m_tot_elems = 0;
	m_tot_nodes = 0;
	m_worst_elem = 0;
	//}}AFX_DATA_INIT
}


void GenInformMoreDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GenInformMoreDlg)
	DDX_Text(pDX, IDC_EDIT_AVG_QUALITY, m_avg_quality);
	DDX_Text(pDX, IDC_EDIT_MIN_QUALITY, m_min_quality);
	DDX_Text(pDX, IDC_EDIT_TOT_ELEMS, m_tot_elems);
	DDX_Text(pDX, IDC_EDIT_TOT_NODES, m_tot_nodes);
	DDX_Text(pDX, IDC_EDIT_WORST_ELEM, m_worst_elem);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GenInformMoreDlg, CDialog)
	//{{AFX_MSG_MAP(GenInformMoreDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GenInformMoreDlg message handlers
