#if !defined(AFX_GENINFORMMOREDLG_H__941C2324_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_GENINFORMMOREDLG_H__941C2324_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GenInformMoreDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GenInformMoreDlg dialog

class GenInformMoreDlg : public CDialog
{
// Construction
public:
	GenInformMoreDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GenInformMoreDlg)
	enum { IDD = IDD_DIALOG_GENERAL_INFORMATION_MORE };
	double	m_avg_quality;
	double	m_min_quality;
	long	m_tot_elems;
	long	m_tot_nodes;
	long	m_worst_elem;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GenInformMoreDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GenInformMoreDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENINFORMMOREDLG_H__941C2324_8CBB_11D1_AA5E_0060979BBB38__INCLUDED_)
