// LogicalEsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "LogicalEsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LogicalEsDlg dialog

LogicalEsDlg::LogicalEsDlg(int *es, GraphMesh *g, CWnd* pParent /*=NULL*/)
	: CDialog(LogicalEsDlg::IDD, pParent)
{
    graph = g;
    ElSw  = es;
	m_from = 0;
	m_num_vis_elems = 0;
	m_step = 0;
	m_to = 0;
	m_tot_num_elems = 0;
	m_all = -1;
}

LogicalEsDlg::LogicalEsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(LogicalEsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(LogicalEsDlg)
	m_from = 0;
	m_num_vis_elems = 0;
	m_step = 0;
	m_to = 0;
	m_tot_num_elems = 0;
	m_all = -1;
	//}}AFX_DATA_INIT
}


void LogicalEsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LogicalEsDlg)
	DDX_Text(pDX, IDC_EDIT_FROM, m_from);
	DDX_Text(pDX, IDC_EDIT_NUM_VIS_ELEMS, m_num_vis_elems);
	DDX_Text(pDX, IDC_EDIT_STEP, m_step);
	DDX_Text(pDX, IDC_EDIT_TO, m_to);
	DDX_Text(pDX, IDC_EDIT_TOT_NUM_ELEMS, m_tot_num_elems);
	DDX_Radio(pDX, IDC_RADIO_ALL, m_all);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LogicalEsDlg, CDialog)
	//{{AFX_MSG_MAP(LogicalEsDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LogicalEsDlg message handlers

void LogicalEsDlg::OnButtonAdd() 
    {
	// TODO: Add your control notification handler code here
    UpdateData(true);

	switch(m_all)
		    {
		case 0:
		    {
		    for (long i=0; i<graph->GetNumElems(); i++)
			    ElSw[i] = 1;
		    m_num_vis_elems = graph->GetNumElems();
		    }
		    break;

		case 1:
		    {
		    if ((m_to-m_from) > 0)
			    m_num_vis_elems = graph->SelEl(ElSw, m_from, m_to, m_step, 1);
		    }
		    break;

		case 2:
		    {
		    if ((m_to-m_from) > 0)
			    m_num_vis_elems = graph->SelNod(ElSw, m_from, m_to, m_step, 1);
		    }
            break;

        default:
            break;
		}
	
    UpdateData(false);
    }

void LogicalEsDlg::OnButtonRemove() 
    {
	// TODO: Add your control notification handler code here
    UpdateData(true);

	switch(m_all)
		    {
		case 0:
		    {
		    for (long i=0; i<graph->GetNumElems(); i++)
			    ElSw[i] = 0;
		    m_num_vis_elems = 0;
		    }
		    break;

		case 1:
		    {
		    if ((m_to-m_from) > 0)
			    m_num_vis_elems = graph->SelEl(ElSw, m_from, m_to, m_step, 0);
		    }
		    break;

		case 2:
		    {
		    if ((m_to-m_from) > 0)
			    m_num_vis_elems = graph->SelNod(ElSw, m_from, m_to, m_step, 0);
		    }
            break;

        default:
            break;
		}
	
    UpdateData(false);
    }

void LogicalEsDlg::OnOK() 
    {
	// TODO: Add extra validation here
    graph->SetElStatus(ElSw);
	
	CDialog::OnOK();
    }
