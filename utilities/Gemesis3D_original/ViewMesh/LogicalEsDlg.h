#if !defined(AFX_LOGICALESDLG_H__55A6DFC2_8CF9_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_LOGICALESDLG_H__55A6DFC2_8CF9_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LogicalEsDlg.h : header file
//

#include "grafmesh.h"

/////////////////////////////////////////////////////////////////////////////
// LogicalEsDlg dialog

class LogicalEsDlg : public CDialog
{
// Construction
public:
	LogicalEsDlg(CWnd* pParent = NULL);   // standard constructor
	LogicalEsDlg(int *, GraphMesh *g, CWnd* pParent = NULL);   //constructor

public:
    int *ElSw;
    GraphMesh *graph;
// Dialog Data
	//{{AFX_DATA(LogicalEsDlg)
	enum { IDD = IDD_DIALOG_LOGICAL_ES };
	long	m_from;
	long	m_num_vis_elems;
	long	m_step;
	long	m_to;
	long	m_tot_num_elems;
	int		m_all;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LogicalEsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LogicalEsDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGICALESDLG_H__55A6DFC2_8CF9_11D1_AA5E_0060979BBB38__INCLUDED_)
