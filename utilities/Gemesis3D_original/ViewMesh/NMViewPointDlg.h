#if !defined(AFX_NMVIEWPOINTDLG_H__BC4AA063_917A_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_NMVIEWPOINTDLG_H__BC4AA063_917A_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// NMViewPointDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// NMViewPointDlg dialog

class GraphMesh;

class NMViewPointDlg : public CDialog
{
public:
    CView *m_pView;
    GraphMesh *graph;
    int m_s1, m_s2;
    int m_pviewflag;

// Construction
public:
	NMViewPointDlg(CWnd* pParent = NULL);   // standard constructor
	NMViewPointDlg(CView *);   // standard constructor
    BOOL Create(GraphMesh *);
    BOOL OnInitDialog();

// Dialog Data
	//{{AFX_DATA(NMViewPointDlg)
	enum { IDD = IDD_DIALOG_NM_VIEW_POINT };
	BOOL	m_animate;
	BOOL	m_wireframe;
	BOOL	m_zoom;
	int		m_phi;
	int		m_theta;
	double	m_X;
	double	m_Y;
	double	m_Z;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NMViewPointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NMViewPointDlg)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NMVIEWPOINTDLG_H__BC4AA063_917A_11D1_AA5E_0060979BBB38__INCLUDED_)
