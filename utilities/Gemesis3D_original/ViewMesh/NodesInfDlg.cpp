// NodesInfDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "NodesInfDlg.h"

#include "ViewMeshDoc.h"
#include "ViewMeshView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NodesInfDlg dialog

BOOL NodesInfDlg::Create(GraphMesh *g)
    {
    graph = g;
	m_node = 0;
	m_X = 0.0;
	m_Y = 0.0;
	m_Z = 0.0;

    return CDialog::Create(NodesInfDlg::IDD);
    }


NodesInfDlg::NodesInfDlg(CWnd* pParent /*=NULL*/)
	: CDialog(NodesInfDlg::IDD, pParent)
    {
    m_pView = 0;
	//{{AFX_DATA_INIT(NodesInfDlg)
	m_node = 0;
	m_X = 0.0;
	m_Y = 0.0;
	m_Z = 0.0;
	//}}AFX_DATA_INIT
    }

NodesInfDlg::NodesInfDlg(CView *cv)
	: CDialog()
    {
    m_pView = cv;
    }

void NodesInfDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NodesInfDlg)
	DDX_Text(pDX, IDC_EDIT_NODE, m_node);
	DDX_Text(pDX, IDC_EDIT_X, m_X);
	DDX_Text(pDX, IDC_EDIT_Y, m_Y);
	DDX_Text(pDX, IDC_EDIT_Z, m_Z);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(NodesInfDlg, CDialog)
	//{{AFX_MSG_MAP(NodesInfDlg)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE, OnButtonUpdate)
	ON_BN_CLICKED(IDC_BUTTON_WHERE_IS, OnButtonWhereIs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// NodesInfDlg message handlers

void NodesInfDlg::OnOK() 
{
	// TODO: Add extra validation here
	CDialog::OnOK();
    if (m_pView != NULL)
        m_pView->SendMessage(ID_GOODBYE_NODE, IDOK);
}

void NodesInfDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
    if (m_pView != NULL)
        m_pView->SendMessage(ID_GOODBYE_NODE, IDCANCEL);
}

void NodesInfDlg::OnButtonUpdate() 
    {
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
    long node = m_node-1;
    long numnodes = graph->GetNumNodes();

    if (node < numnodes && node >=0)
        {
        acPoint3 pp = graph->GetCoord(node);
        m_X = pp.x; m_Y = pp.y; m_Z = pp.z;
        }
    else
        {
        m_X = m_Y = m_Z = 0.0;
        MessageBeep(0);
        }
	UpdateData(FALSE);	
    }

void NodesInfDlg::OnButtonWhereIs() 
    {
	// TODO: Add your control notification handler code here
    UpdateData(TRUE);
    graph->SetnoNo(m_node-1);
    CViewMeshView *pviewm = (CViewMeshView *) m_pView;
    pviewm->NoWhereIs();
    }
	