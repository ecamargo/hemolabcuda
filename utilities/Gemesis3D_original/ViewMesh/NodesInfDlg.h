#if !defined(AFX_NODESINFDLG_H__BC4AA062_917A_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_NODESINFDLG_H__BC4AA062_917A_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// NodesInfDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// NodesInfDlg dialog

class GraphMesh;

class NodesInfDlg : public CDialog
{
// Construction
public:
	NodesInfDlg(CWnd* pParent = NULL);   // standard constructor
	NodesInfDlg(CView *);   // standard constructor
    BOOL Create(GraphMesh *);

public:
    CView *m_pView;
    GraphMesh *graph;

// Dialog Data
	//{{AFX_DATA(NodesInfDlg)
	enum { IDD = IDD_DIALOG_NODE_INFO };
	long	m_node;
	double	m_X;
	double	m_Y;
	double	m_Z;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NodesInfDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NodesInfDlg)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonUpdate();
	afx_msg void OnButtonWhereIs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NODESINFDLG_H__BC4AA062_917A_11D1_AA5E_0060979BBB38__INCLUDED_)
