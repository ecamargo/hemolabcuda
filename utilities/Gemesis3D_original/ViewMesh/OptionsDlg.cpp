// OptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "OptionsDlg.h"
#include "GeomElemSelDlg.h"
#include "LogicalEsDlg.h"
#include "ViewPointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OptionsDlg dialog


OptionsDlg::OptionsDlg(GraphMesh *g, CWnd* pParent /*=NULL*/)
	: CDialog(OptionsDlg::IDD, pParent)
    {
    graph = g;
	m_closed = FALSE;
	m_elements = FALSE;
	m_exploded = FALSE;
	m_nodes = FALSE;
	m_shading = FALSE;
	m_hidden = -1;
	m_none = -1;
    m_selflag = 0;
    m_pviewflag = 0;
    }

OptionsDlg::OptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(OptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(OptionsDlg)
	m_closed = FALSE;
	m_elements = FALSE;
	m_exploded = FALSE;
	m_nodes = FALSE;
	m_shading = FALSE;
	m_hidden = -1;
	m_none = -1;
	//}}AFX_DATA_INIT
    m_selflag = 0;
    m_pviewflag = 0;
}


void OptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OptionsDlg)
	DDX_Check(pDX, IDC_CHECK_CLOSED, m_closed);
	DDX_Check(pDX, IDC_CHECK_ELEMENTS, m_elements);
	DDX_Check(pDX, IDC_CHECK_EXPLODED, m_exploded);
	DDX_Check(pDX, IDC_CHECK_NODES, m_nodes);
	DDX_Check(pDX, IDC_CHECK_SHADING, m_shading);
	DDX_Radio(pDX, IDC_RADIO_HIDDEN_SURF, m_hidden);
	DDX_Radio(pDX, IDC_RADIO_NONE, m_none);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OptionsDlg, CDialog)
	//{{AFX_MSG_MAP(OptionsDlg)
	ON_BN_CLICKED(IDC_BUTTON_GEOMETRIC, OnButtonGeometric)
	ON_BN_CLICKED(IDC_BUTTON_LOGICAL, OnButtonLogical)
	ON_BN_CLICKED(IDC_BUTTON_QUALITY, OnButtonQuality)
	ON_BN_CLICKED(IDC_BUTTON_VIEW_POINT, OnButtonViewPoint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OptionsDlg message handlers

void OptionsDlg::OnButtonGeometric() 
{
	// TODO: Add your control notification handler code here
	int *ElSw = (int *)mMalloc(sizeof(int)*graph->GetNumElems());
	if (!ElSw)
	    { //    1, "Can't Allocate Memory."
	    char buf[100];
	    GetError(1, buf);
	    Error(FATAL_ERROR, 2, buf);
	    }
	graph->CopyElStatus(ElSw);
	long nel = 0;
	for (long i=0; i<graph->GetNumElems(); i++)
	    if (ElSw[i]) nel++;

	GeomElemSelDlg dlg(ElSw, graph);

    dlg.m_tot_num_elems = graph->GetNumElems();
    dlg.m_num_vis_elems = nel;

    if (dlg.DoModal()==IDOK)
        m_selflag = 1;
    
    mFree(ElSw);
}

void OptionsDlg::OnButtonLogical() 
{
	// TODO: Add your control notification handler code here
	int *ElSw = (int *)mMalloc(sizeof(int)*graph->GetNumElems());
	if (!ElSw)
	    { //    1, "Can't Allocate Memory."
	    char buf[100];
	    GetError(1, buf);
	    Error(FATAL_ERROR, 2, buf);
	    }
	graph->CopyElStatus(ElSw);
	long nel = 0;
	for (long i=0; i<graph->GetNumElems(); i++)
	    if (ElSw[i]) nel++;

    LogicalEsDlg dlg(ElSw, graph);
	
    dlg.m_from = 1;
	dlg.m_step = 1;
	dlg.m_to   = 1;
    dlg.m_num_vis_elems = nel;
	dlg.m_tot_num_elems = graph->GetNumElems();
	dlg.m_all = 0;

	if (dlg.DoModal()==IDOK)
        m_selflag = 1;

    mFree(ElSw);
}

void OptionsDlg::OnButtonQuality() 
{
	// TODO: Add your control notification handler code here
	
}

void OptionsDlg::OnButtonViewPoint() 
    {
	// TODO: Add your control notification handler code here
    ViewPointDlg dlg;
    dlg.m_phi    = (int)m_pview_phi;
    dlg.m_theta  = (int)m_pview_theta;
//    dlg.m_sphi   = (int)m_pview_phi;
//    dlg.m_stheta = (int)m_pview_theta;

/*
    dlg.sb1.SetScrollRange(0,360);
    dlg.sb2.SetScrollRange(-90,90);
    dlg.sb1.SetScrollPos(dlg.m_theta);
    dlg.sb2.SetScrollPos(dlg.m_phi);
*/
    if (dlg.DoModal()==IDOK)
        {
        m_pview_phi   = dlg.m_s2;
        m_pview_theta = dlg.m_s1;
        m_pviewflag   = 1;
        }
    }
