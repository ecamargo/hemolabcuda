#if !defined(AFX_OPTIONSDLG_H__94160AC2_8D8E_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_OPTIONSDLG_H__94160AC2_8D8E_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// OptionsDlg.h : header file
//

#include "grafmesh.h"

/////////////////////////////////////////////////////////////////////////////
// OptionsDlg dialog

class OptionsDlg : public CDialog
{
// Construction
public:
	OptionsDlg(CWnd* pParent = NULL);   // standard constructor
	OptionsDlg(GraphMesh *, CWnd* pParent = NULL);   // constructor

public:
    GraphMesh *graph;
    int m_selflag;
    int m_pviewflag;
    double m_pview_theta;
    double m_pview_phi;
// Dialog Data
	//{{AFX_DATA(OptionsDlg)
	enum { IDD = IDD_DIALOG_OPTIONS };
	BOOL	m_closed;
	BOOL	m_elements;
	BOOL	m_exploded;
	BOOL	m_nodes;
	BOOL	m_shading;
	int		m_hidden;
	int		m_none;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(OptionsDlg)
	afx_msg void OnButtonGeometric();
	afx_msg void OnButtonLogical();
	afx_msg void OnButtonQuality();
	afx_msg void OnButtonViewPoint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDLG_H__94160AC2_8D8E_11D1_AA5E_0060979BBB38__INCLUDED_)
