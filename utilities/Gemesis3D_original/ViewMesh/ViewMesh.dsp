# Microsoft Developer Studio Project File - Name="ViewMesh" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ViewMesh - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ViewMesh.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ViewMesh.mak" CFG="ViewMesh - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ViewMesh - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "ViewMesh - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ViewMesh - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "d:\vis-soft\lib\baseoop\header" /I "d:\vis-soft\lib\acdp97\acdpoop\header" /I "d:\vis-soft\lib\acdpgrph\header" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D __MSDOS__=1 /D "__32BITS__" /D "__MFC__" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /nodefaultlib

!ELSEIF  "$(CFG)" == "ViewMesh - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "c:\mesh-gen\v-gemesis3d\bibliot\baseoop\header" /I "c:\mesh-gen\v-gemesis3d\bibliot\acdp97\acdpoop\header" /I "c:\mesh-gen\v-gemesis3d\bibliot\acdp97\acdpgrph\header" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "NDEBUG" /D __MSDOS__=1 /D "__32BITS__" /D "__MFC__" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ViewMesh - Win32 Release"
# Name "ViewMesh - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ElementInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GenInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GenInformMoreDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GeomElemSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LogicalEsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\NMViewPointDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NodesInfDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OptionsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\ViewMesh.cpp
# End Source File
# Begin Source File

SOURCE=.\ViewMesh.rc
# End Source File
# Begin Source File

SOURCE=.\ViewMeshDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ViewMeshView.cpp
# End Source File
# Begin Source File

SOURCE=.\ViewPointDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ElementInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\GenInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\GenInformMoreDlg.h
# End Source File
# Begin Source File

SOURCE=.\GeomElemSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\LogicalEsDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\NMViewPointDlg.h
# End Source File
# Begin Source File

SOURCE=.\NodesInfDlg.h
# End Source File
# Begin Source File

SOURCE=.\OptionsDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\ViewMesh.h
# End Source File
# Begin Source File

SOURCE=.\ViewMeshDoc.h
# End Source File
# Begin Source File

SOURCE=.\ViewMeshView.h
# End Source File
# Begin Source File

SOURCE=.\ViewPointDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\cur00001.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00002.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00003.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00004.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor_p.cur
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\viewm.ico
# End Source File
# Begin Source File

SOURCE=.\res\ViewMesh.ico
# End Source File
# Begin Source File

SOURCE=.\res\ViewMesh.rc2
# End Source File
# Begin Source File

SOURCE=.\res\ViewMeshDoc.ico
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\lib\acdp97\Acdpgrph\Release\acdpgrph.lib
# End Source File
# Begin Source File

SOURCE=..\..\lib\acdp97\Acdpoop\Release\acdpoop.lib
# End Source File
# Begin Source File

SOURCE=..\..\lib\Baseoop\Release\baseoop.lib
# End Source File
# End Target
# End Project
