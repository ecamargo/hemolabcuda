// ViewMesh.h : main header file for the VIEWMESH application
//

#if !defined(AFX_VIEWMESH_H__AC2C032A_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_VIEWMESH_H__AC2C032A_8803_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CViewMeshApp:
// See ViewMesh.cpp for the implementation of this class
//

class CViewMeshApp : public CWinApp
{
public:
	CViewMeshApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewMeshApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CViewMeshApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEWMESH_H__AC2C032A_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
