// ViewMeshDoc.cpp : implementation of the CViewMeshDoc class
//

#include "stdafx.h"
#include "ViewMesh.h"

#include "ViewMeshDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CViewMeshDoc

IMPLEMENT_DYNCREATE(CViewMeshDoc, CDocument)

BEGIN_MESSAGE_MAP(CViewMeshDoc, CDocument)
	//{{AFX_MSG_MAP(CViewMeshDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CViewMeshDoc construction/destruction

CViewMeshDoc::CViewMeshDoc()
{
	// TODO: add one-time construction code here

}

CViewMeshDoc::~CViewMeshDoc()
{
}

BOOL CViewMeshDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CViewMeshDoc serialization

void CViewMeshDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CViewMeshDoc diagnostics

#ifdef _DEBUG
void CViewMeshDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CViewMeshDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CViewMeshDoc commands
