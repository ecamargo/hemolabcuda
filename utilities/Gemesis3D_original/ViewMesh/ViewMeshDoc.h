// ViewMeshDoc.h : interface of the CViewMeshDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIEWMESHDOC_H__AC2C0330_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_VIEWMESHDOC_H__AC2C0330_8803_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CViewMeshDoc : public CDocument
{
protected: // create from serialization only
	CViewMeshDoc();
	DECLARE_DYNCREATE(CViewMeshDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewMeshDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CViewMeshDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CViewMeshDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEWMESHDOC_H__AC2C0330_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
