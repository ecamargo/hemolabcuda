// ViewMeshView.cpp : implementation of the CViewMeshView class
//

#include "stdafx.h"
#include "ViewMesh.h"

#include "ViewMeshDoc.h"
#include "ViewMeshView.h"

// dialogos
#include "GenInfoDlg.h"
#include "GenInformMoreDlg.h"
#include "GeomElemSelDlg.h"
#include "LogicalEsDlg.h"
#include "OptionsDlg.h"
#include "ElementInfoDlg.h"
#include "NodesInfDlg.h"
#include "NMViewPointDlg.h"

#include "acdpgrph.h"
#include "transfor.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern char FileNameArg[255];

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView

IMPLEMENT_DYNCREATE(CViewMeshView, CView)

BEGIN_MESSAGE_MAP(CViewMeshView, CView)
    ON_MESSAGE(ID_GOODBYE_ELEMENT, OnGoodByeElement)
    ON_MESSAGE(ID_GOODBYE_NODE, OnGoodByeNode)
    ON_MESSAGE(ID_GOODBYE_NMVIEWPOINT, OnGoodByeNMViewPoint)
	//{{AFX_MSG_MAP(CViewMeshView)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_INFORMATION_ELEMENT, OnInformationElement)
	ON_UPDATE_COMMAND_UI(ID_INFORMATION_ELEMENT, OnUpdateInformationElement)
	ON_COMMAND(ID_INFORMATION_GENERAL, OnInformationGeneral)
	ON_UPDATE_COMMAND_UI(ID_INFORMATION_GENERAL, OnUpdateInformationGeneral)
	ON_COMMAND(ID_INFORMATION_NODE, OnInformationNode)
	ON_UPDATE_COMMAND_UI(ID_INFORMATION_NODE, OnUpdateInformationNode)
	ON_COMMAND(ID_SELECT_GEOMETRICALLY, OnSelectGeometrically)
	ON_UPDATE_COMMAND_UI(ID_SELECT_GEOMETRICALLY, OnUpdateSelectGeometrically)
	ON_COMMAND(ID_SELECT_LOGICALLY, OnSelectLogically)
	ON_UPDATE_COMMAND_UI(ID_SELECT_LOGICALLY, OnUpdateSelectLogically)
	ON_COMMAND(ID_SELECT_QUALITY, OnSelectQuality)
	ON_UPDATE_COMMAND_UI(ID_SELECT_QUALITY, OnUpdateSelectQuality)
	ON_COMMAND(ID_SELECT_REMOVESELECTED, OnSelectRemoveselected)
	ON_UPDATE_COMMAND_UI(ID_SELECT_REMOVESELECTED, OnUpdateSelectRemoveselected)
	ON_COMMAND(ID_SELECT_RESTOREALL, OnSelectRestoreall)
	ON_UPDATE_COMMAND_UI(ID_SELECT_RESTOREALL, OnUpdateSelectRestoreall)
	ON_COMMAND(ID_TOOLS_AXES, OnToolsAxes)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_AXES, OnUpdateToolsAxes)
	ON_COMMAND(ID_TOOLS_ELEMENT_DETECTION, OnToolsElementDetection)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_ELEMENT_DETECTION, OnUpdateToolsElementDetection)
	ON_COMMAND(ID_TOOLS_ELEMENT_SELECTION, OnToolsElementSelection)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_ELEMENT_SELECTION, OnUpdateToolsElementSelection)
	ON_COMMAND(ID_TOOLS_NODE_DETECTION, OnToolsNodeDetection)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_NODE_DETECTION, OnUpdateToolsNodeDetection)
	ON_COMMAND(ID_TOOLS_PAN, OnToolsPan)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_PAN, OnUpdateToolsPan)
	ON_COMMAND(ID_TOOLS_ZOOM_IN, OnToolsZoomIn)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_ZOOM_IN, OnUpdateToolsZoomIn)
	ON_COMMAND(ID_VIEW_OPTIONS, OnViewOptions)
	ON_UPDATE_COMMAND_UI(ID_VIEW_OPTIONS, OnUpdateViewOptions)
	ON_COMMAND(ID_VIEW_ORIGINAL, OnViewOriginal)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ORIGINAL, OnUpdateViewOriginal)
	ON_COMMAND(ID_VIEW_PAN, OnViewPan)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PAN, OnUpdateViewPan)
	ON_COMMAND(ID_VIEW_PREVIOUS, OnViewPrevious)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PREVIOUS, OnUpdateViewPrevious)
	ON_COMMAND(ID_VIEW_VIEW_POINT, OnViewViewPoint)
	ON_UPDATE_COMMAND_UI(ID_VIEW_VIEW_POINT, OnUpdateViewViewPoint)
	ON_COMMAND(ID_VIEW_ZOOM_IN, OnViewZoomIn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_IN, OnUpdateViewZoomIn)
	ON_COMMAND(ID_VIEW_ZOOM_OUT, OnViewZoomOut)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_OUT, OnUpdateViewZoomOut)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView construction/destruction

CViewMeshView::CViewMeshView()
    {
	// TODO: add construction code here
    graph = 0;

/*
** Variaveis auxiliares
*/
    pview_theta    = 290;
    pview_phi      = 30;

    hiddenflag       = 0;
    shadingflag      = 1;
    explodedflag     = 0;
    edgesflag        = 0;
    closedflag       = 1;
    nodesflag        = 0;
    elementsflag     = 0;
    axesflag         = 0;
    pviewflag        = 0;
    whereflag        = 0;
    selflag          = 0;

    ZoomCenter.x = ZoomCenter.y = ZoomCenter.z = 0;

    BIsDown = 0;
    ZoomExact = 0, swaxes = 0;
    limit.xmin = 0.0;
    limit.xmax = 1.0;
    limit.ymin = 0.0;
    limit.ymax = 1.0;

    ActiveTool = PrevTool = NONE;


    m_pElInfoDlg   = new ElementInfoDlg(this);    // dialogo nao modal
    m_pNodeInfoDlg = new NodesInfDlg(this);       // dialogo nao modal
    m_pNMViewPointDlg = new NMViewPointDlg(this); // dialogo nao modal

    // printer
    Printer = false;

    // cursores
    panCur  = AfxGetApp()->LoadCursor(IDC_CURSOR_PAN);
    zoomCur = AfxGetApp()->LoadCursor(IDC_CURSOR_ZOOM);
    if (!zoomCur)
        Error(0, 0, "nao pegou");
    axesCur = AfxGetApp()->LoadCursor(IDC_CURSOR_AXES);
    nodeCur = AfxGetApp()->LoadCursor(IDC_CURSOR_NODE);
    elemCur = AfxGetApp()->LoadCursor(IDC_CURSOR_ELEM);
    selCur  = AfxGetApp()->LoadCursor(IDC_CURSOR_SEL);
   }

CViewMeshView::~CViewMeshView()
    {
    delete m_pElInfoDlg;
    delete m_pNodeInfoDlg;
    delete m_pNMViewPointDlg;
    }

BOOL CViewMeshView::PreCreateWindow(CREATESTRUCT& cs)
    {
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
    mClassName = AfxRegisterWndClass
        (CS_HREDRAW | CS_VREDRAW,
         0,                                        // sem cursor mvc++ pg 222
         (HBRUSH)::GetStockObject (WHITE_BRUSH),
         0);
    cs.lpszClass = mClassName;
	return CView::PreCreateWindow(cs);
    }   

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView drawing

void CViewMeshView::OnDraw(CDC* pDC)
    {       
    if (FileNameArg[0])
        {
        DataRead(FileNameArg);
        FileNameArg[0] = 0;
        Invalidate();
        return;
        }


	 if (graph)
	     {
         acTransformation trans(limit);  // ndc = 0,1
         trans.SetViewport(4000, 4000);
         trans.SetIsotropic();
         acDeviceContext dco(pDC, trans);
         dco.SetMapMode(acMapIsotropic);
         dco.SetWindowOrg(0, 0);
         dco.SetWindowExt(4000, 4000);

         RECT  r;
        if (Printer)
            {
            r.left = 0;
            r.top  = 0;
            r.right  = pDC->GetDeviceCaps(HORZRES);
            r.bottom = pDC->GetDeviceCaps(VERTRES);
            }
        else 
            GetClientRect(&r);

         dco.SetViewportOrg(0, 0);
         dco.SetViewportExt(r.right, r.bottom);

	     graph->SetDCS(&dco);
	     graph->Graph();
	     if (axesflag)
	    	{
		    axesflag = 0;
		    graph->SetAxesFlag(axesflag);
		    }
	     }


/*
    RECT  r;

    if (Printer)
        {
        r.left = 0;
        r.top  = 0;
        r.right  = pDC->GetDeviceCaps(HORZRES);
        r.bottom = pDC->GetDeviceCaps(VERTRES);
        }
    else 
        GetClientRect(&r);
    float aux = 0.7F * r.right;
    dco.SetViewportOrg(0.3 * r.right, 0);
    dco.SetViewportExt((int) aux, r.bottom);

    pDoc->DoPaint(dco);

*/
}

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView printing

BOOL CViewMeshView::OnPreparePrinting(CPrintInfo* pInfo)
    {
	// default preparation
    pInfo->SetMaxPage(1);      
	return DoPreparePrinting(pInfo);
    }

void CViewMeshView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CViewMeshView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView diagnostics

#ifdef _DEBUG
void CViewMeshView::AssertValid() const
{
	CView::AssertValid();
}

void CViewMeshView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CViewMeshDoc* CViewMeshView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CViewMeshDoc)));
	return (CViewMeshDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CViewMeshView message handlers

void CViewMeshView::OnFileNew() 
{
	// TODO: Add your command handler code here
    if (graph)
		{
		delete mesh;
		delete graph;
		graph = NULL;
		}
	
}

void CViewMeshView::OnUpdateFileNew(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnFileOpen() 
    {
    char nomefile[255];
//	Clear memory


    if (graph)
		{
		delete mesh;
		delete graph;
		graph = NULL;
		}

    CFileDialog dlg(TRUE, "dir", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "Volume File (*.vwm)|*.vwm|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        strcpy(nomefile, dlg.GetPathName());
        DataRead(nomefile);
        Invalidate();
        }
    }

void CViewMeshView::OnInformationElement() 
    {
	// TODO: Add your command handler code here
	if (m_pElInfoDlg->GetSafeHwnd() == 0)
        m_pElInfoDlg->Create(graph);
    }

void CViewMeshView::OnUpdateInformationElement(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    	
    }

void CViewMeshView::OnInformationGeneral() 
    {
	// TODO: Add your command handler code here
    GenInfoDlg dlg;

    dlg.m_tot_elems = graph->GetNumElems();
    dlg.m_tot_nodes = graph->GetNumNodes();
    int res = dlg.DoModal();
    if (res == IDC_BUTTON_MORE)
        {
        long  wstel;
        double Mcal, Acal;

        GenInformMoreDlg dlg1;
        dlg1.m_tot_elems = dlg.m_tot_elems;
        dlg1.m_tot_nodes = dlg.m_tot_nodes;
        graph->WoMiAv(wstel, Mcal, Acal);
        dlg1.m_worst_elem = wstel;
        dlg1.m_min_quality = Mcal;
        dlg1.m_avg_quality = Acal;
        dlg1.DoModal();
        }
    }

void CViewMeshView::OnUpdateInformationGeneral(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    	
    }

void CViewMeshView::OnInformationNode() 
    {
	// TODO: Add your command handler code here
	if (m_pNodeInfoDlg->GetSafeHwnd() == 0)
        m_pNodeInfoDlg->Create(graph);
    }

void CViewMeshView::OnUpdateInformationNode(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnSelectGeometrically() 
    {
	// TODO: Add your command handler code here
    static acPoint3 point1(0.0,0.0,0.0);
    static acPoint3 point2(0.0,0.0,0.0);
    static double radius=1.0;
    static int flgsel = 0;
    static int flgint = 1;
    static int *ElSw;
    static long nel;

	ElSw = (int *)mMalloc(sizeof(int)*graph->GetNumElems());
	if (!ElSw)
	    { //    1, "Can't Allocate Memory."
	    char buf[100];
	    GetError(1, buf);
	    Error(FATAL_ERROR, 2, buf);
	    }
	graph->CopyElStatus(ElSw);
	nel = 0;
	for (long i=0; i<graph->GetNumElems(); i++)
	    if (ElSw[i]) nel++;

	GeomElemSelDlg dlg(ElSw, graph);

    dlg.m_x1 = point1.x;
    dlg.m_x2 = point2.x;
    dlg.m_y1 = point1.y;
    dlg.m_y2 = point2.y;
    dlg.m_z1 = point1.z;
    dlg.m_z2 = point2.z;
    dlg.m_radius = radius;
    dlg.m_all = flgsel;
    dlg.m_inside = flgint;
    dlg.m_tot_num_elems = graph->GetNumElems();
    dlg.m_num_vis_elems = nel;

    dlg.DoModal();

    point1.x = dlg.m_x1;
    point2.x = dlg.m_x2;
    point1.y = dlg.m_y1;
    point2.y = dlg.m_y2;
    point1.z = dlg.m_z1;
    point2.z = dlg.m_z2;
    radius   = dlg.m_radius;
    flgsel   = dlg.m_all;
    flgint   = dlg.m_inside;

    mFree(ElSw);

	graph->RemoveEls();
    Invalidate();
    }

void CViewMeshView::OnUpdateSelectGeometrically(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(graph!=0);    	
    }

void CViewMeshView::OnSelectLogically() 
    {
	// TODO: Add your command handler code here
	int *ElSw = (int *)mMalloc(sizeof(int)*graph->GetNumElems());
	if (!ElSw)
	    { //    1, "Can't Allocate Memory."
	    char buf[100];
	    GetError(1, buf);
	    Error(FATAL_ERROR, 2, buf);
	    }
	graph->CopyElStatus(ElSw);
	long nel = 0;
	for (long i=0; i<graph->GetNumElems(); i++)
	    if (ElSw[i]) nel++;

    LogicalEsDlg dlg(ElSw, graph);
	
    dlg.m_from = 1;
	dlg.m_step = 1;
	dlg.m_to   = 1;
    dlg.m_num_vis_elems = nel;
	dlg.m_tot_num_elems = graph->GetNumElems();
	dlg.m_all = 0;

	if (dlg.DoModal()==IDOK)
        {
        graph->RemoveEls();
        Invalidate();
        }
    mFree(ElSw);
    }

void CViewMeshView::OnUpdateSelectLogically(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(graph!=0);    	
    }

void CViewMeshView::OnSelectQuality() 
{
	// TODO: Add your command handler code here
	
}

void CViewMeshView::OnUpdateSelectQuality(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnSelectRemoveselected() 
    {
	// TODO: Add your command handler code here
    graph->RemoveSelEls();
    Invalidate();
    graph->SetSelectFlag(0);
    graph->SetElemFlag(0);
    }
    
void CViewMeshView::OnUpdateSelectRemoveselected(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnSelectRestoreall() 
    {
	// TODO: Add your command handler code here
    graph->UnRemove();
    Invalidate();
    }

void CViewMeshView::OnUpdateSelectRestoreall(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsAxes() 
{
	// TODO: Add your command handler code here
    ActiveTool = TOOLSAXES;
    PrevTool = ActiveTool;
}

void CViewMeshView::OnUpdateToolsAxes(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsElementDetection() 
    {
	// TODO: Add your command handler code here
    ActiveTool = TOOLSELEMD;
    PrevTool = ActiveTool;
	graph->SetElemFlag(1);
    }

void CViewMeshView::OnUpdateToolsElementDetection(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsElementSelection() 
    {
	// TODO: Add your command handler code here
    ActiveTool = TOOLSELEMS;
    PrevTool = ActiveTool;
	graph->SetSelectFlag(1);
	graph->SetElemFlag(1);
    }

void CViewMeshView::OnUpdateToolsElementSelection(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsNodeDetection() 
    {
	// TODO: Add your command handler code here
    ActiveTool = TOOLSNODED;
    PrevTool = ActiveTool;
	graph->SetNodeFlag(1);
    }

void CViewMeshView::OnUpdateToolsNodeDetection(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsPan() 
    {
	// TODO: Add your command handler code here
    
    ActiveTool = TOOLSPAN;
    PrevTool = ActiveTool;
	
    }

void CViewMeshView::OnUpdateToolsPan(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnToolsZoomIn() 
    {
	// TODO: Add your command handler code here
    ActiveTool = TOOLSZOOMIN;
    PrevTool = ActiveTool;
    ::SetCursor(zoomCur);
    }

void CViewMeshView::OnUpdateToolsZoomIn(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewOptions() 
    {
	// TODO: Add your command handler code here
	OptionsDlg dlg(graph);

    dlg.m_hidden   = hiddenflag;
    dlg.m_shading  = shadingflag;
    dlg.m_exploded = explodedflag;
    dlg.m_none     = edgesflag;
    dlg.m_closed   = closedflag;
    dlg.m_nodes    = nodesflag;
    dlg.m_elements = elementsflag;
    dlg.m_pview_theta = pview_theta;
    dlg.m_pview_phi   = pview_phi;

    if (dlg.DoModal()!=IDOK) return;

    hiddenflag   = dlg.m_hidden;
    shadingflag  = dlg.m_shading;
    explodedflag = dlg.m_exploded;
    edgesflag    = dlg.m_none;
    closedflag   = dlg.m_closed;
    nodesflag    = dlg.m_nodes;
    elementsflag = dlg.m_elements;
    selflag      = dlg.m_selflag;
    pviewflag    = dlg.m_pviewflag;
    pview_theta  = dlg.m_pview_theta;
    pview_phi    = dlg.m_pview_phi;

	if (graph)
	    {
	    graph->SetFlags(hiddenflag, shadingflag, explodedflag,
			   closedflag, edgesflag, nodesflag, elementsflag, axesflag);

        if (selflag)
            {
		    graph->RemoveEls();
            selflag = 0;
            }
        if (pviewflag)
            {
		    graph->Rotate(pview_theta, pview_phi);
            pviewflag = 0;
            }
		}
    Invalidate();
    }

void CViewMeshView::OnUpdateViewOptions(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewOriginal() 
    {
	// TODO: Add your command handler code here
    graph->FullView();
    Invalidate();
    }

void CViewMeshView::OnUpdateViewOriginal(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewPan() 
    {
	// TODO: Add your command handler code here
    PrevTool = ActiveTool;
    ActiveTool = TOOLSPAN;
    }

void CViewMeshView::OnUpdateViewPan(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewPrevious() 
    {
	// TODO: Add your command handler code here
    graph->ZoomPrevious();
    Invalidate();
    }

void CViewMeshView::OnUpdateViewPrevious(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewViewPoint() 
    {
	// TODO: Add your command handler code here
	if (m_pNMViewPointDlg->GetSafeHwnd() == 0)
        {
        m_pNMViewPointDlg->m_phi = (int)pview_phi;
        m_pNMViewPointDlg->m_theta = (int)pview_theta;
        m_pNMViewPointDlg->Create(graph);
        }
    }

void CViewMeshView::OnUpdateViewViewPoint(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewZoomIn() 
    {
	// TODO: Add your command handler code here
    PrevTool = ActiveTool;
    ActiveTool = TOOLSZOOMIN;
    zoom.InitZoom();
    //::SetCursor(ZoomCursor);
    }

void CViewMeshView::OnUpdateViewZoomIn(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);    		
    }

void CViewMeshView::OnViewZoomOut() 
    {
	// TODO: Add your command handler code here
    graph->ZoomOut();
    Invalidate();
    }

void CViewMeshView::OnUpdateViewZoomOut(CCmdUI* pCmdUI) 
    {
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);
    }

void CViewMeshView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CViewMeshView::OnLButtonDown(UINT nFlags, CPoint point) 
    {
	// TODO: Add your message handler code here and/or call default
    if (!graph)
        return;

    CPoint pp;
    pp = point;

    CDC *auxdc = new CClientDC(this);
    acTransformation trans(limit);  // ndc = 0,1
    trans.SetViewport(4000, 4000);
    trans.SetIsotropic();
    acDeviceContext dco(auxdc, trans);
    dco.SetMapMode(acMapIsotropic);
    dco.SetWindowOrg(0, 0);
    dco.SetWindowExt(4000, 4000);

    RECT  r;
    GetClientRect(&r);
    dco.SetViewportOrg(0, 0);
    dco.SetViewportExt(r.right, r.bottom);
    graph->SetDCS(&dco);

    ::DPtoLP(auxdc->m_hDC, &pp, 1);
    delete auxdc;

//	BDownPos.x = point.x;  BDownPos.y = point.y;
	BDownPos.x = pp.x;  BDownPos.y = pp.y;
//    zoom.ButtonDown(point);	
    if (ActiveTool == TOOLSZOOMIN) 
        zoom.InitZoom();
    zoom.ButtonDown(point);
	CView::OnLButtonDown(nFlags, point);
    }

void CViewMeshView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
    if (!graph)
        return;
    int makezoom;
	acLPoint2 BUpPos;
    CPoint pp;
    pp = point;

    CDC *auxdc = new CClientDC(this);
    acTransformation trans(limit);  // ndc = 0,1
    trans.SetViewport(4000, 4000);
    trans.SetIsotropic();
    acDeviceContext dco(auxdc, trans);
    dco.SetMapMode(acMapIsotropic);
    dco.SetWindowOrg(0, 0);
    dco.SetWindowExt(4000, 4000);

    RECT  r;
    GetClientRect(&r);
    dco.SetViewportOrg(0, 0);
    dco.SetViewportExt(r.right, r.bottom);
    graph->SetDCS(&dco);
 
	//auxdc->DPtoLP(&point);
    ::DPtoLP(auxdc->m_hDC, &pp, 1);

    BUpPos.x = pp.x;  BUpPos.y = pp.y;
	switch (ActiveTool)
	    {
	    case TOOLSNODED:
	        if (fabs(BDownPos.x-BUpPos.x)<20 && fabs(BDownPos.y-BUpPos.y)<20)
                graph->NodeNumber(BUpPos);           
    	    break;
	
	    case TOOLSELEMD:
	        if (fabs(BDownPos.x-BUpPos.x)<20 && fabs(BDownPos.y-BUpPos.y)<20)
        		graph->ElementNumber(BUpPos, 1);		
	        break;
	    
	    case TOOLSELEMS:
	        if (fabs(BDownPos.x-BUpPos.x)<20 && fabs(BDownPos.y-BUpPos.y)<20)
        		graph->ElementNumber(BUpPos, 1);		
	        break;
	     
    	case TOOLSAXES:
	        if (fabs(BDownPos.x-BUpPos.x)<20 && fabs(BDownPos.y-BUpPos.y)<20)
	        	{
		        axesflag = 1;
		        graph->SetAxesFlag(axesflag);
		        graph->SetAxes(BDownPos);
		        Invalidate();
		        }
    	    break;
	 
	    case TOOLSZOOMIN:  
	        POINT xse, xnw;
            zoom.ButtonUp(&xnw, &xse, point);
            
	        graph->LCtoWC(BDownPos, Pci);
	        graph->LCtoWC(BUpPos,   Pcf);
	        makezoom = 1;
	        if (ZoomExact)		// Solo cuando viene de Zoom In
		        {
/*
		        DLGPROC lpfnZE = (DLGPROC)MakeProcInstance((FARPROC)ZEDlg, hInst);
		        if (!DialogBox(hInst, "ZOOMEXACT", hwnd, lpfnZE))
		            makezoom = 0;
		        ZoomExact = 0;
		        FreeProcInstance((FARPROC)lpfnZE);
*/
		        }
	        if (makezoom)
		        graph->ZoomIn(Pci, Pcf);
	        Invalidate();

	        if (PrevTool != ActiveTool)
		        {
		        if (PrevTool != NONE)
                    {
                    ActiveTool = PrevTool;
//		            PostMessage (hwnd, WM_COMMAND,
			            //GET_WM_COMMAND_MPS(IDM_TOOLSNODED+PrevTool, hwnd, 0));
                    }
		        else
		            {
		            ActiveTool = NONE;
//		            curr_cursor = LoadCursor((HANDLE) NULL, IDC_ARROW);
		            }
		        }

	        break;
	    
	    case TOOLSPAN:	        

	        graph->Pan(BDownPos, BUpPos);

	        Invalidate();

	        if (PrevTool != ActiveTool)
		        {
		        if (PrevTool != NONE)
                    {
                    ActiveTool = PrevTool;
//		            PostMessage (hwnd, WM_COMMAND,
			            //GET_WM_COMMAND_MPS(IDM_TOOLSNODED+PrevTool, hwnd, 0));
                    }
		        else
		            {
		            ActiveTool = NONE;
//		            curr_cursor = LoadCursor((HANDLE) NULL, IDC_ARROW);
		            }
		        }

	        break;
	    
	    default:
	        break;
	    }
	
    delete auxdc;
	CView::OnLButtonUp(nFlags, point);
    }

void CViewMeshView::OnMouseMove(UINT nFlags, CPoint point) 
    {
	// TODO: Add your message handler code here and/or call default
    zoom.MouseMove(point);

	switch (ActiveTool)
	    {
	    case TOOLSNODED:
            ::SetCursor(nodeCur);
    	    break;
	
	    case TOOLSELEMD:
            ::SetCursor(elemCur);
	        break;
	    
	    case TOOLSELEMS:
            ::SetCursor(selCur);
	        break;
	     
    	case TOOLSAXES:
            ::SetCursor(axesCur);
    	    break;
	 
	    case TOOLSZOOMIN:  
            ::SetCursor(zoomCur);
	        break;
	    
	    case TOOLSPAN:	        
            ::SetCursor(panCur);
	        break;
	        
	    default:
            ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	        break;
	    }
	


	CView::OnMouseMove(nFlags, point);
    }

LONG CViewMeshView::OnGoodByeElement(UINT, LONG)
    {
    m_pElInfoDlg->DestroyWindow();
    return 0L;
    }   

LONG CViewMeshView::OnGoodByeNode(UINT, LONG)
    {
    m_pNodeInfoDlg->DestroyWindow();
    return 0L;
    }   

LONG CViewMeshView::OnGoodByeNMViewPoint(UINT, LONG)
    {
    pviewflag = m_pNMViewPointDlg->m_pviewflag;
    if (pviewflag)
        {
        pview_phi   = m_pNMViewPointDlg->m_phi;
        pview_theta = m_pNMViewPointDlg->m_theta;
        graph->Rotate(pview_theta, pview_phi);
        pviewflag = 0;
        Invalidate();
        }
    m_pNMViewPointDlg->DestroyWindow();
    return 0L;
    }   

void CViewMeshView::Rotate()
    {
    if (m_pNMViewPointDlg)
        {
        int wireframe = m_pNMViewPointDlg->m_wireframe;
        int zoom  = m_pNMViewPointDlg->m_zoom;
        acPoint3 ZoomCenter;
        pview_theta = m_pNMViewPointDlg->m_theta;
        pview_phi   = m_pNMViewPointDlg->m_phi;
        ZoomCenter.x = m_pNMViewPointDlg->m_X;
        ZoomCenter.y = m_pNMViewPointDlg->m_Y;
        ZoomCenter.z = m_pNMViewPointDlg->m_Z;
        graph->Rotate(pview_theta, pview_phi, wireframe, zoom, ZoomCenter);
        Invalidate();
        }
    }

void CViewMeshView::WhereIs()
    {
    CDC *auxdc = new CClientDC(this);
    acTransformation trans(limit);  // ndc = 0,1
    trans.SetViewport(4000, 4000);
    trans.SetIsotropic();
    acDeviceContext dco(auxdc, trans);
    dco.SetMapMode(acMapIsotropic);
    dco.SetWindowOrg(0, 0);
    dco.SetWindowExt(4000, 4000);

    RECT  r;
    GetClientRect(&r);
    dco.SetViewportOrg(0, 0);
    dco.SetViewportExt(r.right, r.bottom);
    graph->SetDCS(&dco);

	graph->WhereIs();

    delete auxdc;
    }

void CViewMeshView::NoWhereIs()
    {
    CDC *auxdc = new CClientDC(this);
    acTransformation trans(limit);  // ndc = 0,1
    trans.SetViewport(4000, 4000);
    trans.SetIsotropic();
    acDeviceContext dco(auxdc, trans);
    dco.SetMapMode(acMapIsotropic);
    dco.SetWindowOrg(0, 0);
    dco.SetWindowExt(4000, 4000);

    RECT  r;
    GetClientRect(&r);
    dco.SetViewportOrg(0, 0);
    dco.SetViewportExt(r.right, r.bottom);
    graph->SetDCS(&dco);

	graph->NoWhereIs();

    delete auxdc;
    }

void CViewMeshView::DataRead(char *name)
    {
	FILE *fp = fopen(name, "r");
	if (!fp) Error(FATAL_ERROR, 1, "can't open input file <%s>", name);

// 	Read Mesh

	mesh = new Mesh3d;
	mesh->Read(fp);         // leitura de mesh
	fclose(fp);

	graph = new GraphMesh(mesh, hiddenflag, explodedflag,
			      closedflag, pview_theta, pview_phi);

	graph->SetFlags(hiddenflag,
			shadingflag,
			explodedflag,
			closedflag,
			edgesflag,
			nodesflag,
			elementsflag,
            axesflag);
    }

void CViewMeshView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
    {
	// TODO: Add your specialized code here and/or call the base class
	
//	CView::OnPrint(pDC, pInfo);
    Printer       = TRUE;
    OnDraw(pDC);   
    Printer       = FALSE; 
    }

void CViewMeshView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);
}

void CViewMeshView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(graph!=0);
}
