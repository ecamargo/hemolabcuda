// ViewMeshView.h : interface of the CViewMeshView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIEWMESHVIEW_H__AC2C0332_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_VIEWMESHVIEW_H__AC2C0332_8803_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "acdp.h"
#include "mesh3d.h"
#include "surface.h"
#include "grafmesh.h"
#include "zoom.h"

#define ID_GOODBYE_ELEMENT      WM_USER + 5
#define ID_GOODBYE_NODE         WM_USER + 6
#define ID_GOODBYE_NMVIEWPOINT  WM_USER + 7


enum ActiveToolType  { NONE, TOOLSNODED, TOOLSELEMD, TOOLSELEMS, TOOLSAXES, TOOLSZOOMIN, TOOLSPAN};


class ElementInfoDlg;    // avisa que existe esta classe
class NodesInfDlg;    // avisa que existe esta classe
class NMViewPointDlg;

class CViewMeshView : public CView
{
public:
        CString mClassName;

public:    // ponteiros para dialogos nao modais  (modeless)
    ElementInfoDlg *m_pElInfoDlg;
    NodesInfDlg *m_pNodeInfoDlg;
    NMViewPointDlg *m_pNMViewPointDlg;


protected: // create from serialization only
	CViewMeshView();
	DECLARE_DYNCREATE(CViewMeshView)

// Attributes
public:
	CViewMeshDoc* GetDocument();

public:
    void DataRead(char *);

public:
    LONG OnGoodByeElement(UINT, LONG);
    LONG OnGoodByeNode(UINT, LONG);
    LONG OnGoodByeNMViewPoint(UINT, LONG);
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewMeshView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CViewMeshView();
    void WhereIs();
    void NoWhereIs();
    void Rotate();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
    Mesh3d     *mesh;
    GraphMesh  *graph;
    acZoom      zoom;

    ActiveToolType ActiveTool, PrevTool;

    /*
    ** Variaveis auxiliares
    */
    double pview_theta;
    double pview_phi;

    int    hiddenflag;
    int    shadingflag;
    int    explodedflag;
    int    edgesflag;
    int    closedflag;
    int    nodesflag;
    int    elementsflag;
    int    axesflag;
    int    pviewflag;
    int    whereflag;
    int    selflag;

    acPoint2 Pci, Pcf;
    acPoint3 ZoomCenter;

    int BIsDown;
    acLPoint2 BDownPos;
    int ZoomExact, swaxes;
    long elEl;
    acLimit2 limit;

    bool Printer;

    // cursores
    HCURSOR    panCur, zoomCur, axesCur, nodeCur, elemCur, selCur;


// Generated message map functions
protected:
	//{{AFX_MSG(CViewMeshView)
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnInformationElement();
	afx_msg void OnUpdateInformationElement(CCmdUI* pCmdUI);
	afx_msg void OnInformationGeneral();
	afx_msg void OnUpdateInformationGeneral(CCmdUI* pCmdUI);
	afx_msg void OnInformationNode();
	afx_msg void OnUpdateInformationNode(CCmdUI* pCmdUI);
	afx_msg void OnSelectGeometrically();
	afx_msg void OnUpdateSelectGeometrically(CCmdUI* pCmdUI);
	afx_msg void OnSelectLogically();
	afx_msg void OnUpdateSelectLogically(CCmdUI* pCmdUI);
	afx_msg void OnSelectQuality();
	afx_msg void OnUpdateSelectQuality(CCmdUI* pCmdUI);
	afx_msg void OnSelectRemoveselected();
	afx_msg void OnUpdateSelectRemoveselected(CCmdUI* pCmdUI);
	afx_msg void OnSelectRestoreall();
	afx_msg void OnUpdateSelectRestoreall(CCmdUI* pCmdUI);
	afx_msg void OnToolsAxes();
	afx_msg void OnUpdateToolsAxes(CCmdUI* pCmdUI);
	afx_msg void OnToolsElementDetection();
	afx_msg void OnUpdateToolsElementDetection(CCmdUI* pCmdUI);
	afx_msg void OnToolsElementSelection();
	afx_msg void OnUpdateToolsElementSelection(CCmdUI* pCmdUI);
	afx_msg void OnToolsNodeDetection();
	afx_msg void OnUpdateToolsNodeDetection(CCmdUI* pCmdUI);
	afx_msg void OnToolsPan();
	afx_msg void OnUpdateToolsPan(CCmdUI* pCmdUI);
	afx_msg void OnToolsZoomIn();
	afx_msg void OnUpdateToolsZoomIn(CCmdUI* pCmdUI);
	afx_msg void OnViewOptions();
	afx_msg void OnUpdateViewOptions(CCmdUI* pCmdUI);
	afx_msg void OnViewOriginal();
	afx_msg void OnUpdateViewOriginal(CCmdUI* pCmdUI);
	afx_msg void OnViewPan();
	afx_msg void OnUpdateViewPan(CCmdUI* pCmdUI);
	afx_msg void OnViewPrevious();
	afx_msg void OnUpdateViewPrevious(CCmdUI* pCmdUI);
	afx_msg void OnViewViewPoint();
	afx_msg void OnUpdateViewViewPoint(CCmdUI* pCmdUI);
	afx_msg void OnViewZoomIn();
	afx_msg void OnUpdateViewZoomIn(CCmdUI* pCmdUI);
	afx_msg void OnViewZoomOut();
	afx_msg void OnUpdateViewZoomOut(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ViewMeshView.cpp
inline CViewMeshDoc* CViewMeshView::GetDocument()
   { return (CViewMeshDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEWMESHVIEW_H__AC2C0332_8803_11D1_AA5E_0060979BBB38__INCLUDED_)
