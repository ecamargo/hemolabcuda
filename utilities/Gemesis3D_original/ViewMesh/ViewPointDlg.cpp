// ViewPointDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ViewMesh.h"
#include "ViewPointDlg.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ViewPointDlg dialog

#include "acdp.h"

ViewPointDlg::ViewPointDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ViewPointDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ViewPointDlg)
	m_phi = 0;
	m_theta = 0;
	//}}AFX_DATA_INIT

//    CScrollBar *sb1 = (CScrollBar *)GetDlgItem(IDC_SCROLLBAR1);
//    CScrollBar *sb2 = (CScrollBar *)GetDlgItem(IDC_SCROLLBAR2);
//    sb1->SetScrollRange(0,360);
//    sb2->SetScrollRange(-90,90);
    
}


void ViewPointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ViewPointDlg)
	DDX_Text(pDX, IDC_EDIT_PHI, m_phi);
	DDX_Text(pDX, IDC_EDIT_THETA, m_theta);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ViewPointDlg, CDialog)
	//{{AFX_MSG_MAP(ViewPointDlg)
	ON_WM_HSCROLL()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ViewPointDlg message handlers


BOOL ViewPointDlg::OnInitDialog()
    {
	CDialog::OnInitDialog();

	// Initially create slider control in horizontal position

    CScrollBar *ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR1);    
    ps->SetScrollRange(0, 360);
    ps->SetScrollPos(290);


    ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR2);    
    ps->SetScrollRange(-90, 90);
    ps->SetScrollPos(30);
    return (1);
    }

void ViewPointDlg::OnOK() 
    {
	// TODO: Add extra validation here
    CScrollBar *ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR1);    	
    m_s1 = ps->GetScrollPos();
    ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR2);    	
    m_s2 = ps->GetScrollPos();
	CDialog::OnOK();
    }

void ViewPointDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
    {
	// TODO: Add your message handler code here and/or call default
	int t1;
    int nmin, nmax;
    t1 = pScrollBar->GetScrollPos();
    switch(nSBCode)
        {
        case SB_THUMBPOSITION:
//            pScrollBar->GetScrollRange(&nmin, &nmax);
            pScrollBar->SetScrollPos(nPos);
            break;

        case SB_PAGEUP:
            pScrollBar->GetScrollRange(&nmin, &nmax);
            if ((t1 - 10) > nmin)
                t1  -= 10;
            else
                t1 = nmin;
            pScrollBar->SetScrollPos(t1);
            break;

        case SB_LINEUP:
            pScrollBar->GetScrollRange(&nmin, &nmax);
            if ((t1 - 1) > nmin)
                t1  -= 1;
            else
                t1 = nmin;
            pScrollBar->SetScrollPos(t1);
            break;

        case SB_PAGEDOWN:
            pScrollBar->GetScrollRange(&nmin, &nmax);
            if ((t1 + 10) < nmax)
                t1  += 10;
            else
                t1 = nmax;
            pScrollBar->SetScrollPos(t1);
            break;

        case SB_LINEDOWN:
            pScrollBar->GetScrollRange(&nmin, &nmax);
            if ((t1 + 1) < nmax)
                t1  += 1;
            else
                t1 = nmax;
            pScrollBar->SetScrollPos(t1);
            break;

        default:
            break;
        }

    CScrollBar *ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR1);    	
	m_theta = ps->GetScrollPos();
    ps = (CScrollBar *) GetDlgItem(IDC_SCROLLBAR2);    	
    m_phi = ps->GetScrollPos();
    UpdateData(FALSE);

    RECT r;
    GetClientRect(&r);
    int extx = r.right - r.left;
    int exty = r.bottom - r.top;
    r.left  = (int)(extx * 71L/100);
    r.right = (int)(extx * 91L/100);
    r.top   = 0;
    r.bottom= exty * 33/100;

    InvalidateRect(&r);
    }

void ViewPointDlg::OnPaint() 
    {
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	int extx, exty;
    RECT r;
    GetClientRect(&r);
    extx = r.right - r.left;
    exty = r.bottom - r.top;

    double sinti, costi, sinfi, cosfi, a1,a2, b1,b2,b3;

    costi = cos((float)m_theta * 0.01745329252);
    sinti = sin((float)m_theta * 0.01745329252);
    cosfi = cos((float)m_phi   * 0.01745329252);
	sinfi = sin((float)m_phi   * 0.01745329252);
	a1 = -sinti; a2 = costi;
	b1 = -costi * sinfi; b2 = -sinti * sinfi; b3 = cosfi;

    dc.SetMapMode(MM_ISOTROPIC);
    dc.SetViewportExt(extx*10/100, exty*15/100);
    dc.SetWindowExt(25, 25);
    dc.SetViewportOrg(extx*81/100,exty*16/100);
    dc.SetWindowOrg(0,0);

	int ix1, iy1, ix2, iy2;
	ix1 = 0;
	iy1 = 0;

//          SetROP2 (hdc, R2_BLACK);
	    // 'x axis'
    ix2 = ix1 + (int)(a1 * 25);
    iy2 = iy1 - (int)(b1 * 25);
    dc.MoveTo(ix1, iy1);
    dc.LineTo(ix2, iy2);

	    // 'y axis'
    ix2 = ix1 + (int)(a2 * 25);
    iy2 = iy1 - (int)(b2 * 25);
    dc.MoveTo(ix1, iy1);
    dc.LineTo(ix2, iy2);

	    // 'z axis'
	ix2 = ix1;
    iy2 = iy1 - (int)(b3 * 25);
    dc.MoveTo(ix1, iy1);
    dc.LineTo(ix2, iy2);

	// Do not call CDialog::OnPaint() for painting messages
    }
