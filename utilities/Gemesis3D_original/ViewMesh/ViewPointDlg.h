#if !defined(AFX_VIEWPOINTDLG_H__05B77E02_8E59_11D1_AA5E_0060979BBB38__INCLUDED_)
#define AFX_VIEWPOINTDLG_H__05B77E02_8E59_11D1_AA5E_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ViewPointDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ViewPointDlg dialog

class ViewPointDlg : public CDialog
{
// Construction
public:
	ViewPointDlg(CWnd* pParent = NULL);   // standard constructor
    BOOL OnInitDialog();
public:
    int m_s1, m_s2;

public:
// Dialog Data
	//{{AFX_DATA(ViewPointDlg)
	enum { IDD = IDD_DIALOG_VIEW_POINT };
	int		m_phi;
	int		m_theta;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ViewPointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ViewPointDlg)
	virtual void OnOK();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEWPOINTDLG_H__05B77E02_8E59_11D1_AA5E_0060979BBB38__INCLUDED_)
