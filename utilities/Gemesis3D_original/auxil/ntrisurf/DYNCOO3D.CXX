/*
** dyncoo3D.cpp - Enzo, Fancello, Raul, Venere & Salgado may 1992
*/

#include "stdafx.h"
#include "dyncoo3d.h"
#include "stdlib.h"

acDynCoord3D::acDynCoord3D(long nod)
    {
    NumNodes= nod;
    NNosMax = NumNodes + COORD_GROWTH;

    vc = (acPoint3 *) malloc(NNosMax * sizeof(acPoint3));
    if (!vc)
        Error(FATAL_ERROR, 4, "Insufficient Memory for coordinates");
    }

void acDynCoord3D::Read(FILE *fp)
    {
    register long i;
    char buf[80];
    long dummy;
    static char *palcha = "COORDINATES";

    if (NumNodes)
        {
        Error(WARNING, 1, "An object of class Coordinate exist and will be eliminated");
        Free();
        }

    if (!FindKeyWord(palcha, fp))
        {//  27, "Keyword |%s| Not Found."
        Error(FATAL_ERROR, 2, "Keyword |%s| Not Found.", palcha);
        }

    fscanf(fp, "%ld", &NumNodes);
    if (NumNodes < 1)
        Error(FATAL_ERROR, 3, "Number of nodes negative or null");
    NNosMax = NumNodes + COORD_GROWTH;

    vc = (acPoint3 *) malloc(NNosMax * sizeof(acPoint3));
    if (!vc)
        Error(FATAL_ERROR, 4, "Insufficient Memory for coordinates");

    for (i = 0; i < NumNodes; i++)
        {
        double x, y, z;
        fscanf(fp, "%ld %lf %lf %lf", &dummy, &x, &y, &z);
        if (dummy != i + 1)
            {
            sprintf(buf,
                "Coordinate::Read -> Incorrect data for node %ld", i + 1);
            Error(FATAL_ERROR, 4, buf);
	    }
        vc[i].x = x;
        vc[i].y = y;
        vc[i].z = z;
        }

    }

void acDynCoord3D::Free()
    {
    NumNodes = NNosMax = 0;
    free(vc);
    vc = NULL;
    }

void acDynCoord3D::AddCoord (acPoint3 &point)
    {
    if (NumNodes >= NNosMax)
	{
	long tam,i;
	NNosMax += COORD_GROWTH;
	tam = NNosMax * sizeof(acPoint3);

	acPoint3 *New_vc = (acPoint3 *) malloc(tam);
	if (!New_vc)
	    {
	    char buf[80];
	    sprintf(buf, "Memory exhausted for DynCoord");
	    Error(FATAL_ERROR, 1, buf);
	    }
	for(i = 0; i<NumNodes; i++)
            New_vc[i] = vc[i];
        free(vc);
        vc = New_vc;
	}
    vc[NumNodes] = point;
    NumNodes++;
    }

void acDynCoord3D::RemoveNodes(long *aux)
    {
    long i,k,ufa;
    ufa = NumNodes;
    for (k=i=0; i<ufa; i++)
	{
	if (aux[i])
	    {
	    vc[k]=vc[i];
	    aux[i] = k++;
	    }
	else
	    {
	    aux[i] = -1;
	    NumNodes--;
	    }
	}
    }

void acDynCoord3D::MergeNodes(double cotap, long *aux)
    {
    long *auxn;
    long exist,nn,i;
    double cota;
    double dx,dy,dz;
    auxn = (long *)malloc(sizeof(long)*NumNodes);
    acLimit3 vol=Box();

    dx = vol.xmax-vol.xmin;
    dy = vol.ymax-vol.ymin;
    dz = vol.zmax-vol.zmin;
    cota = (dx*dx+dy*dy+dz*dz)*cotap;
    auxn[0] = 0;
    nn = 1;
    for (i=1; i<NumNodes; i++)
        {
        exist=-1;
        if (aux[i])
            {
            for (long j=0; j<i; j++)
                {
                if (aux[j])
                    {
                    dx = vc[i].x - vc[j].x;
                    dy = vc[i].y - vc[j].y;
                    dz = vc[i].z - vc[j].z;
                    if (dx*dx+dy*dy+dz*dz < cota)
                        {
                        exist=j;
                        break;
                        }
                    }
                }
            }
        if (exist==-1)
            auxn[i]=nn++;
        else
            auxn[i]=auxn[exist];
        }

    for (i=0; i<NumNodes; i++)
        {
        vc[auxn[i]] = vc[i];
        }

    for (i=0; i<NumNodes; i++)
        aux[i] = auxn[i];

    NumNodes = nn;
    free(auxn);
    }

void acDynCoord3D::Renumber(long *NuevaNum)
    {
    long nod=0;
    long i;
    for (i=0; i<NumNodes; i++)
        {
        if (NuevaNum[i] == i)
            {
            vc[nod] = vc[i];
            NuevaNum[i] = nod++;
            }
        }
    NumNodes = nod;
    }

void acDynCoord3D::DXFOut(FILE *fp)
    {
    for (long i=0; i<NumNodes; i++)
        {
        fprintf(fp,"VERTEX\n  8\n0\n");
		fprintf(fp," 10\n%lf\n 20\n%lf\n 30\n%lf\n 70\n 192\n  0\n",
               vc[i].x, vc[i].y, vc[i].z);
        }
    }

void acDynCoord3D::Print(FILE *fp)
    {
    fprintf(fp, "\n*COORDINATES\n %ld\n",NumNodes);
    for (long i = 0; i<NumNodes; i++)
        fprintf(fp, " %ld %.16lg %.16lg %.16lg\n",
                i+1, vc[i].x, vc[i].y, vc[i].z);
    }

acLimit3 acDynCoord3D::Box()
{
	acLimit3 box;
	box.xmax=box.xmin=vc[0].x;
	box.ymax=box.ymin=vc[0].y;
	box.zmax=box.zmin=vc[0].z;
    for (long i = 1; i<NumNodes; i++)
	{
		if (box.xmin > vc[i].x) box.xmin = vc[i].x;
		if (box.ymin > vc[i].y) box.ymin = vc[i].y;
		if (box.zmin > vc[i].z) box.zmin = vc[i].z;
		if (box.xmax < vc[i].x) box.xmax = vc[i].x;
		if (box.ymax < vc[i].y) box.ymax = vc[i].y;
		if (box.zmax < vc[i].z) box.zmax = vc[i].z;
	}
	return box;
}
