/*
** dyncoord.h - Enzo, Fancello, Salgado, Venere & Raul
*/
#ifndef DYNCOO3D_H
#define DYNCOO3D_H

#define COORD_GROWTH 5000

#include "general.h"

class acDynCoord3D
    {
    protected:
        long NNosMax;
        long NumNodes;  // number of nodes
        acPoint3 *vc;   // coordenadas x,y,z de cada nodo

    public:
// contructores
        acDynCoord3D () { NNosMax =0; NumNodes=0; vc=NULL;}
        acDynCoord3D (long nod);

// metodos
        void    Free();
        void    Read(FILE *fp);
        void	Print(FILE *fp);
        long	GetNumNodes() { return(NumNodes); }
        void    AddCoord (acPoint3 &point);
        void    RemoveNodes(long *aux);
        void    MergeNodes(double cota, long *aux);
        void    Renumber(long *aux);
		void	DXFOut(FILE *fp);
		acLimit3 Box();

// operadores
        acPoint3 &operator[](long n)
           {
           return(vc[n]);
           }
        acDynCoord3D & operator=(const acDynCoord3D &c);

    };

#endif // DYNCOORD_H
