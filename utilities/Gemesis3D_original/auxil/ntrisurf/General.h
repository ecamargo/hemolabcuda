#ifndef GENERAL_H
#define GENERAL_H

#define WARNING      0
#define COMMON_ERROR 1
#define FATAL_ERROR  2

#include <stdlib.h>
#include <string.h>

int FindKeyWord(char *string, FILE *fp);
void Error(int icod, int numerr, char *msg, ...);

struct acPoint3
    {
    double x, y, z;
    };

struct acLimit3
{
	double xmin, xmax;
	double ymin, ymax;
	double zmin, zmax;
};

#endif
