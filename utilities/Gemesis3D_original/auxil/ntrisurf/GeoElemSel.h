#if !defined(AFX_GEOELEMSEL_H__392E9041_A7D2_11D3_A5CB_00104B9E21C1__INCLUDED_)
#define AFX_GEOELEMSEL_H__392E9041_A7D2_11D3_A5CB_00104B9E21C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GeoElemSel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GeoElemSel dialog
#include "surfgen.h"
class GeoElemSel : public CDialog
{
// Construction
public:
	GeoElemSel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GeoElemSel)
	enum { IDD = IDD_GEOELEMSEL };
	double	m_radius;
	double	m_x;
	double	m_nx;
	double	m_y;
	double	m_ny;
	double	m_z;
	double	m_nz;
	int		m_inter;
	long	m_selelem;
	long	m_totnel;
	int		m_sel;
	int		m_gr;
	int		m_g;
	//}}AFX_DATA
    SurfaceGen *surf;
    long   *m_elem;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GeoElemSel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GeoElemSel)
	afx_msg void OnAdd();
	afx_msg void OnAddsel();
	afx_msg void OnRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GEOELEMSEL_H__392E9041_A7D2_11D3_A5CB_00104B9E21C1__INCLUDED_)
