#if !defined(AFX_INPUTTEXT_H__59052AA0_A741_11D3_A5CB_00104B9E21C1__INCLUDED_)
#define AFX_INPUTTEXT_H__59052AA0_A741_11D3_A5CB_00104B9E21C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputText.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// InputText dialog

class InputText : public CDialog
{
// Construction
public:
	InputText(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(InputText)
	enum { IDD = IDD_INPUTTEXT };
	CString	m_text;
	CString	m_fixtext;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InputText)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InputText)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTTEXT_H__59052AA0_A741_11D3_A5CB_00104B9E21C1__INCLUDED_)
