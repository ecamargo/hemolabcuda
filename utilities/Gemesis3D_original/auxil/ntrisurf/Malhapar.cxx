/*
** Raul, Enzo, Marcelo, Fancello & Salgado
*/

#include "stdafx.h"
#include "malhapar.h"

int VolIntersec (acLimit3 &Vola, acLimit3 &Volb);

ParamMesh::ParamMesh()
    {
    H = NULL;
    Octree = NULL;
    }

void ParamMesh::Read(FILE *fp)
    {
    static char *htit = "EDGES_SIZE";
    static char *kcoord = "COORDINATES";
    static char *kelem  = "ELEMENT GROUPS";

    if (!FindKeyWord(kcoord, fp))
        {
        Error(FATAL_ERROR, 1, "Keyword |%s| Not Found.", kcoord);
        }
    Coords = new acDynCoord3D;
    Coords->Read(fp);
    Volume = Coords->Box();

    if (!FindKeyWord(kelem, fp))
        {
        Error(FATAL_ERROR, 1, "Keyword |%s| Not Found.", kelem);
        }

//    ElGrps = new ElGroups;	//LABURAR
//    ElGrps->Read(fp);

    float h;

    if (!FindKeyWord(htit, fp))
        {
        Error(FATAL_ERROR, 1, "Keyword |%s| Not Found.", htit);
        }

    long nnod = Coords->GetNumNodes();
    H = (double *) malloc (nnod * sizeof(double));
    for (long i = 0; i < nnod; i++)
        {
        fscanf (fp,"%f", &h);
        H[i] = h;
        }
    }

void ParamMesh::BuildOctree()
    {
    acPoint3 cg, tc;
    register int i;
    /*
    ** calcula e fixa dimensoes da window da malha de parametros
    */
    tc.x = Volume.xmax - Volume.xmin;
    tc.y = Volume.ymax - Volume.ymin;
    tc.z = Volume.zmax - Volume.zmin;
    cg.x = Volume.xmin + tc.x * 0.5;
    cg.y = Volume.ymin + tc.y * 0.5;
    cg.z = Volume.zmin + tc.z * 0.5;

    /*
    ** cria primeiro no' da arvore com o no' 1
    */
    Octree = CreNodeOct(cg, tc);
    long nol = 0;
    Octree->l.push_front(nol);

    /*
    ** varre os nos restantes
    */
    for (i = 1; i < Coords->GetNumNodes(); i++)
        InsNodeOct(i);

    /*
    ** monta lista no ramos finais da arvore
    */
    LiberaLista(Octree);
    MontaLista();
    }

void ParamMesh::LiberaLista(MP_Octree  *oct)
    {
    register int i;
    if (oct->ptr[0])
        for (i = 0; i < 8; i++)
            LiberaLista(oct->ptr[i]);
    else
        oct->l.clear();
    }

MP_Octree  *ParamMesh::CreNodeOct(acPoint3 &cg, acPoint3 &tc)
    {
    MP_Octree *temp;

    /*
    ** cria o no'
    */
    temp = (MP_Octree  *) malloc(sizeof(struct MP_Octree));

    /*
    ** coloca os elementos e inicializa o resto com NULL
    */
    temp->cg = cg;
    temp->tc = tc;
//    temp->no = -1;

    for (int i = 0; i < 8; i++)
        temp->ptr[i] = NULL;

    return(temp);
    }

void ParamMesh::InsNodeOct(long no)
    {
    acPoint3 point, pointp;
    register int oct;
    MP_Octree  *temp,  *tree;
    acDynCoord3D &Coord = *Coords;

    tree = Octree;
    point = Coord[no];

    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    while (tree->l.size() > NUMNODMAX) // Mientras la hoja tenga mas de NUMNODMAX nodos
        {
        for (int i = 0; i < 8; i++)
            {
            acPoint3 cgf, tcf;
            cgf = tree->cg;
            tcf = tree->tc;
            CalcNewMeio(cgf, tcf, i);
            tree->ptr[i] = CreNodeOct(cgf, tcf);
            }

        long nol;
        while (!tree->l.empty())
            {
			nol = *(long *)tree->l.pop_front();
            pointp = Coord[nol];
            oct      = FindOctante(pointp, tree->cg);
            temp     = tree->ptr[oct];
            temp->l.push_front(nol);
//			it++;
//            temp->no = tree->no;
            }
        oct      = FindOctante(point, tree->cg);
        tree     = tree->ptr[oct];
        }
    tree->l.push_front(no);
    }

void ParamMesh::MontaLista()
    {
    acLimit3 volt;
    acPoint3 point1, point2;
    long elem;
    Element *el;

    /*
    ** loop sobre cada elemento da malha de parametros
    */
    for (el = SetFirstElem(), elem=0; el; el = SetNextElem(),elem++)
        {
        point1 = (*Coords)[el->GetNode(0)];
        point2 = (*Coords)[el->GetNode(1)];
        if (point1.x < point2.x)
            { volt.xmin = point1.x; volt.xmax = point2.x; }
        else
            { volt.xmin = point2.x; volt.xmax = point1.x; }
        if (point1.y < point2.y)
            { volt.ymin = point1.y; volt.ymax = point2.y; }
        else
            { volt.ymin = point2.y; volt.ymax = point1.y; }
        if (point1.z < point2.z)
            { volt.zmin = point1.z; volt.zmax = point2.z; }
        else
            { volt.zmin = point2.z; volt.zmax = point1.z; }
        point1 = (*Coords)[el->GetNode(2)];
        point2 = (*Coords)[el->GetNode(3)];
        if (point1.x < point2.x)
            {
            if (point1.x < volt.xmin) volt.xmin = point1.x;
            if (point2.x > volt.xmax) volt.xmax = point2.x;
            }
        else
            {
            if (point2.x < volt.xmin) volt.xmin = point2.x;
            if (point1.x > volt.xmax) volt.xmax = point1.x;
            }
        if (point1.y < point2.y)
            {
            if (point1.y < volt.ymin) volt.ymin = point1.y;
            if (point2.y > volt.ymax) volt.ymax = point2.y;
            }
        else
            {
            if (point2.y < volt.ymin) volt.ymin = point2.y;
            if (point1.y > volt.ymax) volt.ymax = point1.y;
            }
        if (point1.z < point2.z)
            {
            if (point1.z < volt.zmin) volt.zmin = point1.z;
            if (point2.z > volt.zmax) volt.zmax = point2.z;
            }
        else
            {
            if (point2.z < volt.zmin) volt.zmin = point2.z;
            if (point1.z > volt.zmax) volt.zmax = point1.z;
            }

//        volt = el->Box();
        MListOct(Octree, volt, elem);
        }
    }

void ParamMesh::MListOct(MP_Octree  *oct, acLimit3 &volt, long elem)
    {
    acLimit3 voloct;
	long nol;
    register int i;

    CalcWinOctante (voloct, oct->cg, oct->tc);

    if (VolIntersec(voloct, volt))
        {
        if (oct->ptr[0] == NULL)
            {
            nol.numelem = elem;
            oct->l.push_front(nol);
            }
        else
            for (i = 0; i < 8; i++)
                MListOct(oct->ptr[i], volt, elem);
        }
    }

int VolIntersec (acLimit3 &Vola, acLimit3 &Volb)
    {
    if ((Vola.xmin <= Volb.xmax && Vola.xmax >= Volb.xmin) &&
        (Vola.ymin <= Volb.ymax && Vola.ymax >= Volb.ymin) &&
        (Vola.zmin <= Volb.zmax && Vola.zmax >= Volb.zmin))
            return (1);
    return (0);
    }

void ParamMesh::PrintOctree(FILE *fp)
    {
    PrintBranch(fp, Octree);
    }

void ParamMesh::PrintBranch(FILE *fp, MP_Octree  *oct)
    {
    register int i;

    if (oct->ptr[0])
        for (i = 0; i < 8; i++)
            PrintBranch(fp, oct->ptr[i]);
    else
        {
		list<long>::iterator it=oct->l.begin();
        while (it)
           {
           nol = *it;
           fprintf (fp,"%ld ",nol);
           it++;
           }
        fprintf (fp,"\n");
        }
    }

void ParamMesh::CalcWinOctante (acLimit3 &pl, acPoint3 &cg, acPoint3 &tc)
    {
    pl.xmin = cg.x - tc.x * 0.5;
    pl.xmax = cg.x + tc.x * 0.5;
    pl.ymin = cg.y - tc.y * 0.5;
    pl.ymax = cg.y + tc.y * 0.5;
    pl.zmin = cg.z - tc.z * 0.5;
    pl.zmax = cg.z + tc.z * 0.5;
    }

/*
float ParamMesh::GetH(acPoint3 &point)
    {
    static char buf[81];
    acCoordinate3D &Coord = *Coords;
    int oct;
    MP_Octree  *tree;
    tree = Octree;

    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    acNodeList  *pl;
    float ct1, ct2, ct3, ct4;
    long n1,n2,n3,n4;
    float x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    acPoint3 v;
    Element *el = ElGrps->GetElement(0);
    struct st_nodeclist  *nol;
    pl = tree->l.GetHead();

    while (pl)
       {
       nol = (struct st_nodeclist *) pl->GetDados();
       ElGrps->SetElData(0, nol->numelem);
       n1 = el->GetNode(0);
       n2 = el->GetNode(1);
       n3 = el->GetNode(2);
       n4 = el->GetNode(3);
       v = Coord[n1];
       x1 = v.x - point.x; y1 = v.y - point.y; z1 = v.z - point.z;
       v = Coord[n2];
       x2 = v.x - point.x; y2 = v.y - point.y; z2 = v.z - point.z;
       v = Coord[n3];
       x3 = v.x - point.x; y3 = v.y - point.y; z3 = v.z - point.z;
       ct4 = - x1*(y2*z3-y3*z2) + y1*(x2*z3-x3*z2) - z1*(x2*y3-x3*y2);
       if (ct4 >= 0.0)
         {
         v = Coord[n4];
         x4 = v.x - point.x; y4 = v.y - point.y; z4 = v.z - point.z;
         ct1 = - x2*(y4*z3-y3*z4) + y2*(x4*z3-x3*z4) - z2*(x4*y3-x3*y4);
         if (ct1 >= 0.0)
           {
           ct2 = - x4*(y1*z3-y3*z1) + y4*(x1*z3-x3*z1) - z4*(x1*y3-x3*y1);
           if (ct2 >= 0.0)
             {
             ct3 = - x4*(y2*z1-y1*z2) + y4*(x2*z1-x1*z2) - z4*(x2*y1-x1*y2);
             if (ct3 >= 0.0)
                 {
                 return ( (ct1*H[n1]+ct2*H[n2]+ct3*H[n3]+ct4*H[n4]) /
                          (ct1+ct2+ct3+ct4) );
                 }
             }
           }
         }
       pl = pl->next;
       }

    sprintf(buf, "Point (%lf, %lf, %lf) outside mesh",
	point.x, point.y, point.z);
    Error(FATAL_ERROR, 1, buf);
    return(0.0);
    }
*/

double ParamMesh::GetH(acPoint3 &point)
    {
    static char buf[81];
    acCoordinate3D &Coord = *Coords;
    int oct;
    MP_Octree  *tree;
    tree = Octree;

    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    double ct1, ct2, ct3, ct4;
    long n1,n2,n3,n4;
    double x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    long nn1, nn2, nn3, nn4, naux;
    int signo;
    acPoint3 v;
    Element *el = ElGrps->GetElement(0);
    long nol;
    list <long>::iterator it = tree->l.begin();

    while (it)
       {
       nol = *it;
       ElGrps->SetElData(0, nol);
       n1 = el->GetNode(0);
       n2 = el->GetNode(1);
       n3 = el->GetNode(2);
       n4 = el->GetNode(3);

	signo = 1; nn1 = n1; nn2 = n2; nn3 = n3, nn4 = n4;
// Ordena nn1, nn2, nn3, nn4 tal que nn1 < nn2 < nn3 < nn4
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }
	if (nn2 > nn3) { naux = nn2; nn2 = nn3; nn3 = naux; signo *= -1; }
	if (nn3 > nn4) { naux = nn3; nn3 = nn4; nn4 = naux; signo *= -1; }
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }
	if (nn2 > nn3) { naux = nn2; nn2 = nn3; nn3 = naux; signo *= -1; }
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }

// Cara nn1, nn2, nn3
       v = Coord[nn1];
       x1 = v.x - point.x; y1 = v.y - point.y; z1 = v.z - point.z;
       v = Coord[nn2];
       x2 = v.x - point.x; y2 = v.y - point.y; z2 = v.z - point.z;
       v = Coord[nn3];
       x3 = v.x - point.x; y3 = v.y - point.y; z3 = v.z - point.z;
       ct4 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
	   ct4 *= -1; ct4 *= signo;
       if (ct4 >= 0.0)
         {
// Cara nn1, nn2, nn4
         v = Coord[nn4];
         x4 = v.x - point.x; y4 = v.y - point.y; z4 = v.z - point.z;
         ct3 = x1*(y2*z4-z2*y4) + y1*(z2*x4-x2*z4) + z1*(x2*y4-y2*x4);
	  ct3 *= signo;
         if (ct3 >= 0.0)
           {
// Cara nn1, nn3, nn4
           ct2 = x1*(y3*z4-z3*y4) + y1*(z3*x4-x3*z4) + z1*(x3*y4-y3*x4);
	    ct2 *= -1; ct2 *=signo;
           if (ct2 >= 0.0)
             {
// Cara nn2, nn3, nn4
             ct1 = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
	      ct1 *= signo;
             if (ct1 >= 0.0)
                 {
                 return ( (ct1*H[nn1]+ct2*H[nn2]+ct3*H[nn3]+ct4*H[nn4]) /
                          (ct1+ct2+ct3+ct4) );
                 }
             }
           }
         }
       it++;
       }
    v = Coord[0];
    long noadop = 0;
    double dist;
    double dmin=  (v.x-point.x)*(v.x-point.x)
                + (v.y-point.y)*(v.y-point.y)
                + (v.z-point.z)*(v.z-point.z);
    for (long no = 1; no<Coord.GetNumNodes();no++)
        {
        v = Coord[no];
        dist =    (v.x-point.x)*(v.x-point.x)
                + (v.y-point.y)*(v.y-point.y)
                + (v.z-point.z)*(v.z-point.z);
        if(dist < dmin)
           {
           noadop = no;
           dmin = dist;
           }
        }
    sprintf(buf, "Point (%lf, %lf, %lf) outside mesh -> Adopted %ld",
	        point.x, point.y, point.z, noadop+1);
//    Error(WARNING, 1, buf);
    return(H[noadop]);
    }

void ParamMesh::SetH (void)
    {
    long nnod = Coords->GetNumNodes();
    long i;
    acPoint3 point;
    acDynCoord3D &Coord = *Coords;

    for (i = 0; i < nnod; i++)
        {
        point = Coord[i];
        H[i] = point.x + 2.0 * point.y + 0.5 * point.z;
        }
    }
