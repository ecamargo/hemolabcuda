#if !defined(AFX_MERGEDLG_H__C2C23B60_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_)
#define AFX_MERGEDLG_H__C2C23B60_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Mergedlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Mergedlg dialog

class Mergedlg : public CDialog
{
// Construction
public:
	Mergedlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Mergedlg)
	enum { IDD = IDD_MERGE };
	long	m_gr1;
	double	m_tol;
	long	m_gr2;
	int		m_all;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Mergedlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Mergedlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MERGEDLG_H__C2C23B60_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_)
