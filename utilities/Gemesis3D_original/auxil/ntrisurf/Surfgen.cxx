#include "stdafx.h"
#include "surfgen.h"
#include <math.h>


void SurfaceGen::Read(FILE *fp) //<< SurfaceGen >>
    {
    SurfTriang **vd;
    long i;

    TypeCrit = SC_DELA;
    CotaColap = 0.1;
    CotaCopla = 0.99;
    Factor=1.0;

    Coord = new acDynCoord3D;
    Coord->Read(fp);

//                                  Coloca los nodos
    if (!FindKeyWord("ELEMENT GROUPS", fp))
        {
        Error(FATAL_ERROR, 1, "Keyword ELEMENT GROUPS Not Found.");
        }

    int numels   = 0;
    int numnodes = 0;

    fscanf(fp, "%d", &NumGroups);    // le num. de grupos
    if (NumGroups < 1)
    	Error(FATAL_ERROR, 1, "Invalid number of Groups");

    int *numel = (int *) malloc(sizeof(int) * NumGroups);
    if (!numel)
	    { 
	    Error(FATAL_ERROR, 2, "Can't Allocate Memory.");
    	}

	long idum,n;
	char eltype[20];
    for (i = 0; i < NumGroups; i++)
		{
		fscanf(fp, "%d %ld %s", &idum, &n, eltype);
		if (idum != i+1 || n < 1)
			{
			char buf[80];
			sprintf(buf, "Incorrect Group data %d %ld", idum, n);
			Error(FATAL_ERROR, 3, buf);
			}
		numels += n;
		numel[i] = numels;
		}

    vd = (SurfTriang **)malloc(sizeof(SurfTriang *)*numels);

    if (!FindKeyWord("INCIDENCE", fp))
        {
        Error(FATAL_ERROR, 1, "Keyword INCIDENCE Not Found.");
        }

	long n1,n2,n3;
	int gr = 0;
    for (i=0; i<numels; i++)
        {
		if (i== numel[gr]) gr++;
        curr = new SurfTriang;
        vd[i]  = curr;
		fscanf(fp, "%d %ld %ld", &n1, &n2, &n3);
        curr->n1 = n1;
        curr->n2 = n2;
        curr->n3 = n3;
		curr->gr = gr;
        }

	//FALTA CALCULAR LOS VECINOS Y ENGANCHARLOS!!!!!!!!
}
		/*
SurfaceGen::SurfaceGen(Mesh3d *mesh, ParamMesh *mpar) //<< SurfaceGen >>
    {
    SurfTriang **vd;
    SparseElNo  *El  = mesh->NewElNo();
    SparseFaFaV *ElV = mesh->NewFaFaV();
    long i;
    int g;

    TypeCrit = SC_DELA;
    CotaColap = 0.1;
    CotaCopla = 0.99;
    Factor=1.0;
    MP = mpar;

    vd = (SurfTriang **)malloc(sizeof(SurfTriang *)*El->GetTIndex());

//                                  Coloca los nodos
    for (i=0; i<El->GetTIndex(); i++)
        {
        curr = new SurfTriang;
        vd[i]  = curr;
        curr->n1 = El->GetElem(i, 0);
        curr->n2 = El->GetElem(i, 1);
        curr->n3 = El->GetElem(i, 2);
        }

//                                  Coloca los vecinos
    for (i=0; i<ElV->GetTIndex(); i++)
        {
        curr = vd[i];
        if (ElV->GetElem(i, 0) >= 0)
            curr->v1 = vd[ElV->GetElem(i, 0)];
        else
            curr->v1 = NULL;
        if (ElV->GetElem(i, 1) >= 0)
            curr->v2 = vd[ElV->GetElem(i, 1)];
        else
            curr->v2 = NULL;
        if (ElV->GetElem(i, 2) >= 0)
            curr->v3 = vd[ElV->GetElem(i, 2)];
        else
            curr->v3 = NULL;
        }

//                                  Engancha la lista
    first = vd[0];
    first->prev = NULL;
    first->next = vd[1];
    for (i=1; i<ElV->GetTIndex()-1; i++)
        {
        curr = vd[i];
        curr->prev = vd[i-1];
        curr->next = vd[i+1];
        }
    SurfTriang *last;
    last = vd[ElV->GetTIndex()-1];
    last->prev = vd[ElV->GetTIndex()-2];
    last->next = NULL;

//                                  Coloca los numeros de grupo
    curr = first;
    NumGroups = mesh->GetNumGroups();
    for (g=0; g<NumGroups; g++)
        {
        for (i=0; i<mesh->GetNumElsGrp(g); i++)
            {
            curr->gr = g;
            curr = curr->next;
            }
        }
//                                  Borra estructuras auxiliares
    free(vd);
    delete El;
    delete ElV;

//                                  Copia las coordenadas
	 acDynCoord3D &c = *mesh->GetCoords();
	 acPoint3 p;
	 Coord = new acDynCoord3D;
	 for (i=0; i< mesh->GetNumNodes(); i++)
		 {
		 p = c[i];
		 Coord->AddCoord(p);
		 }
	 }
/*
void SurfaceGen::ImportSurface(Mesh3d *mesh)
    {
    SurfTriang **vd;
    SparseElNo  *El  = mesh->NewElNo();
    SparseFaFaV *ElV = mesh->NewFaFaV();
    long i,nod=Coord->GetNumNodes();
    int g;

    vd = (SurfTriang **)malloc(sizeof(SurfTriang *)*El->GetTIndex());

//                                  Coloca los nodos
    for (i=0; i<El->GetTIndex(); i++)
        {
        curr = new SurfTriang;
        vd[i]  = curr;
        curr->n1 = El->GetElem(i, 0)+nod;
        curr->n2 = El->GetElem(i, 1)+nod;
        curr->n3 = El->GetElem(i, 2)+nod;
        }

//                                  Coloca los vecinos
    for (i=0; i<ElV->GetTIndex(); i++)
        {
        curr = vd[i];
        if (ElV->GetElem(i, 0) >= 0)
            curr->v1 = vd[ElV->GetElem(i, 0)];
        else
            curr->v1 = NULL;
        if (ElV->GetElem(i, 1) >= 0)
            curr->v2 = vd[ElV->GetElem(i, 1)];
        else
            curr->v2 = NULL;
        if (ElV->GetElem(i, 2) >= 0)
            curr->v3 = vd[ElV->GetElem(i, 2)];
        else
            curr->v3 = NULL;
        }

//                                  Engancha la lista
    curr=first;
    while(curr->next)
        curr=curr->next;
    curr->next = vd[0];
    curr->next->prev = curr;
    curr->next->next = vd[1];
    for (i=1; i<ElV->GetTIndex()-1; i++)
        {
        curr = vd[i];
        curr->prev = vd[i-1];
        curr->next = vd[i+1];
        }
    SurfTriang *last;
    last = vd[ElV->GetTIndex()-1];
    last->prev = vd[ElV->GetTIndex()-2];
    last->next = NULL;

//                                  Coloca los numeros de grupo
    curr = vd[0];
    int ng = mesh->GetNumGroups();
    for (g=0; g<ng; g++)
        {
        for (i=0; i<mesh->GetNumElsGrp(g); i++)
            {
            curr->gr = g+NumGroups;
            curr = curr->next;
            }
        }
    NumGroups+=ng;
//                                  Borra estructuras auxiliares
    free(vd);
    delete El;
    delete ElV;

//                                  Copia las coordenadas
	 acDynCoord3D &c = *mesh->GetCoords();
	 acPoint3 p;
	 for (i=0; i< mesh->GetNumNodes(); i++)
		 {
		 p = c[i];
		 Coord->AddCoord(p);
		 }
    }
*/
  void SurfaceGen::TestMesh(long &nod, long &nel)
    {
    char buf[80];

    nel = 0;
    nod = Coord->GetNumNodes();
    curr = first;
    while (curr)
	{
	nel++;
	if (curr->v1 == NULL)
	    {
	    sprintf(buf,"Elemento %ld sin vecino",nel);
    	Error(WARNING, 1, buf);
	    }
	else if (!(curr->v1->v1 == curr || curr->v1->v2 == curr || curr->v1->v3 == curr))
	    {
	    sprintf(buf,"Elemento %ld con vecino equivocado",nel);
    	Error(WARNING, 1, buf);
	    }
	curr = curr->next;
	}

    nodlist **nodo;
    nodo=(nodlist **)malloc(sizeof(nodlist *)*nod);
    for (int i=0;i<nod;i++)
        nodo[i]=new nodlist;
    curr = first;
    nel=0;
    while (curr)
        {
    	nel++;
        if (nodo[curr->n1]->add(curr->n2) >1 ||
            nodo[curr->n1]->add(curr->n3) >1 ||
            nodo[curr->n2]->add(curr->n1) >1 ||
            nodo[curr->n2]->add(curr->n3) >1 ||
            nodo[curr->n3]->add(curr->n1) >1 ||
            nodo[curr->n3]->add(curr->n2) >1)
    	    {
	        sprintf(buf,"Elemento %ld con mas de tres vecinos",nel);
    	    Error(WARNING, 1, buf);
	        }
        curr=curr->next;
        }
    for (i=0;i<nod;i++)
        delete nodo[i];
    free(nodo);
    }

long SurfaceGen::RemoveFreeNodes()
    {

    long nodp,nodn;
    nodp = Coord->GetNumNodes();
    NuevaNum = (long *)malloc(sizeof(long)*nodp);
    if (!NuevaNum)
        Error(FATAL_ERROR,0,"Cannot allocate memory");
    for (long i=0; i<Coord->GetNumNodes(); i++)
        NuevaNum[i]=0;

    curr = first;
    while (curr)
        {
        NuevaNum[curr->n1]=1;
        NuevaNum[curr->n2]=1;
        NuevaNum[curr->n3]=1;
        curr = curr->next;
        }
    Coord->RemoveNodes(NuevaNum);

    curr = first;
    while (curr)
        {
        curr->n1 = NuevaNum[curr->n1];
        curr->n2 = NuevaNum[curr->n2];
        curr->n3 = NuevaNum[curr->n3];
        curr = curr->next;
        }

    free(NuevaNum);
    nodn = Coord->GetNumNodes();
    return(nodp-nodn);
    }

long SurfaceGen::ColapsaTodos()                       //<< ColapsaTodos >>
    {
//    NuevaNum = (long *)malloc(sizeof(long)*Coord->GetNumNodes());
//    for (long i=0; i<Coord->GetNumNodes(); i++)
//		  NuevaNum[i]=i;

	 curr = first;
	 while (curr)
        curr = Colapsa(curr);

//	 Renumera();
//	 free(NuevaNum);
     RemoveTresTri();
     return RemoveFreeNodes();
	 }

void SurfaceGen::RemoveTresTri()
    {
    int vd,ind;
    curr = first;
    while (curr)
	    {
	    if (curr->v1)
	        {
            if      (curr->v1->v1 == curr) ind = 1;
	        else if (curr->v1->v2 == curr) ind = 2;
	        else                           ind = 0;
            vd = FronteraDer(curr->v1,ind);
            if (vd==2)
                {
                RemoveDos(curr,0);
                curr=curr->next;
                continue;
                }
            }
        if (curr->v2)
	        {
            if      (curr->v2->v1 == curr) ind = 1;
	        else if (curr->v2->v2 == curr) ind = 2;
	        else                           ind = 0;
            vd = FronteraDer(curr->v2,ind);
            if (vd==2)
                {
                RemoveDos(curr,1);
                curr=curr->next;
                continue;
                }
            }
        if (curr->v3)
	        {
            if      (curr->v3->v1 == curr) ind = 1;
	        else if (curr->v3->v2 == curr) ind = 2;
	        else                           ind = 0;
            vd = FronteraDer(curr->v3,ind);
            if (vd==2)
                {
                RemoveDos(curr,2);
                curr=curr->next;
                continue;
                }
            }
        curr=curr->next;
        }
    }

void SurfaceGen::RemoveDos(SurfTriang *el, int ind)
    {
    SurfTriang *elv;
    if (ind==0)
        {
        elv=el->v1;
        if (elv->v1==el)
            {
            el->n1= elv->n3;
            el->v1= elv->v3;
            }
        else if (elv->v2==el)
            {
            el->n1= elv->n1;
            el->v1= elv->v1;
            }
        else
            {
            el->n1= elv->n2;
            el->v1= elv->v2;
            }
        if (el->v1)
            {
            if     (el->v1->v1==elv) el->v1->v1=el;
            else if(el->v1->v2==elv) el->v1->v2=el;
            else                     el->v1->v3=el;
            }
        Delete(elv);
        elv=el->v3;
        if      (elv->v1==el)  el->v3= elv->v2;
        else if (elv->v2==el)  el->v3= elv->v3;
        else                   el->v3= elv->v1;
        if (el->v3)
            {
            if     (el->v3->v1==elv) el->v3->v1=el;
            else if(el->v3->v2==elv) el->v3->v2=el;
            else                     el->v3->v3=el;
            }
        Delete(elv);
        }
    else if (ind==1)
        {
        elv=el->v2;
        if (elv->v1==el)
            {
            el->n2= elv->n3;
            el->v2= elv->v3;
            }
        else if (elv->v2==el)
            {
            el->n2= elv->n1;
            el->v2= elv->v1;
            }
        else
            {
            el->n2= elv->n2;
            el->v2= elv->v2;
            }
        if (el->v2)
            {
            if     (el->v2->v1==elv) el->v2->v1=el;
            else if(el->v2->v2==elv) el->v2->v2=el;
            else                     el->v2->v3=el;
            }
        Delete(elv);
        elv=el->v1;
        if      (elv->v1==el)  el->v1= elv->v2;
        else if (elv->v2==el)  el->v1= elv->v3;
        else                   el->v1= elv->v1;
        if (el->v1)
            {
            if     (el->v1->v1==elv) el->v1->v1=el;
            else if(el->v1->v2==elv) el->v1->v2=el;
            else                     el->v1->v3=el;
            }
        Delete(elv);
        }
    else if (ind==1)
        {
        elv=el->v3;
        if (elv->v1==el)
            {
            el->n3= elv->n3;
            el->v3= elv->v3;
            }
        else if (elv->v2==el)
            {
            el->n3= elv->n1;
            el->v3= elv->v1;
            }
        else
            {
            el->n3= elv->n2;
            el->v3= elv->v2;
            }
        if (el->v3)
            {
            if     (el->v3->v1==elv) el->v3->v1=el;
            else if(el->v3->v2==elv) el->v3->v2=el;
            else                     el->v3->v3=el;
            }
        Delete(elv);
        elv=el->v2;
        if      (elv->v1==el)  el->v2= elv->v2;
        else if (elv->v2==el)  el->v2= elv->v3;
        else                   el->v2= elv->v1;
        if (el->v2)
            {
            if     (el->v2->v1==elv) el->v2->v1=el;
            else if(el->v2->v2==elv) el->v2->v2=el;
            else                     el->v2->v3=el;
            }
        Delete(elv);
        }
    }

long SurfaceGen::SwapAll()
    {
	long sws = 1;
	long swst= 0;
	long numswap = 0;

	while (sws)
	    {
	    sws = 0;
	    curr = first;
	    while (curr)
		    {
		    if (curr->v1)
		        {
		        RotaElem(curr,0);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		    if (curr->v2)
		        {
		        RotaElem(curr,1);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		    if (curr->v2)   //Esta bien!! es por la rotacion
		        {
		        RotaElem(curr,1);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		    curr = curr->next;
		    }
		numswap++;
		if (numswap > 6) break;
		swst = swst + sws;
	    }
	return (swst);
	}

void SurfaceGen::Renumera()                            //<< Renumera >>
	 {
	 Coord->Renumber(NuevaNum);
	 curr = first;
	 while (curr)
		{
		curr->n1 = NuevaNum[curr->n1];
		curr->n2 = NuevaNum[curr->n2];
		curr->n3 = NuevaNum[curr->n3];
		if (curr->n1>=Coord->GetNumNodes() ||
		    curr->n2>=Coord->GetNumNodes() ||
		    curr->n3>=Coord->GetNumNodes())
           	Error(FATAL_ERROR, 1, "Invalid surface");
    	curr = curr->next;
	    }
	}

SurfTriang * SurfaceGen::Colapsa(SurfTriang *el)    //<< Colapsa >>
	 {
	 SurfTriang *va;
	 double x21,y21,z21,x31,y31,z31,da,db,d;
	 acPoint3 p1,p2,p3;
	 int ind,indd,indi,vi,vd;
	 acDynCoord3D &coord = *Coord;

	 p1 = coord[el->n1];
	 p2 = coord[el->n2];
	 p3 = coord[el->n3];

	 d  = MP->GetH(p1);

	 // Se fija si algun lado es menor que lo debido
	 x21 = p2.x-p1.x; y21 = p2.y-p1.y; z21 = p2.z-p1.z;
	 da  = x21*x21+y21*y21+z21*z21; ind = 0;

	 x31 = p3.x-p2.x; y31 = p3.y-p2.y; z31 = p3.z-p2.z;
	 db  = x31*x31+y31*y31+z31*z31;
	 if (da > db) {ind = 1; da = db;}

	 x31 = p3.x-p1.x; y31 = p3.y-p1.y; z31 = p3.z-p1.z;
	 db  = x31*x31+y31*y31+z31*z31;
	 if (da > db) {ind = 2; da = db;}

	 if (da > CotaColap*d*d) return(el->next);

    RotaElem(el,ind);
    SurfTriang *elv=el->v1;
    if (!elv) return (el->next); //MODIFICAR PARA SUP ABIERTAS
    if (elv->v2)
        {
        if      (elv->v2->v1 == elv) indi = 2;
	    else if (elv->v2->v2 == elv) indi = 0;
	    else                         indi = 1;
        curr=elv;
        vi = FronteraIzq(elv->v2,indi);
        if (vi==2) return (el->next);
        curr=el;
        }
	 
    if (!el->v2)
        vd = 0;
    else
        {
    	if      (el->v2->v1 == el) { indd = 1; indi = 2; }
	    else if (el->v2->v2 == el) { indd = 2; indi = 0; }
	    else                       { indd = 0; indi = 1; }
        vd = FronteraDer(el->v2,indd);
        vi = FronteraIzq(el->v2,indi); 
        if (vi==2) return (el->next);
        }

    if (!el->v3)
        vi = 0;
    else
        {
        if      (el->v3->v1 == el) indi = 2;
	    else if (el->v3->v2 == el) indi = 0;
	    else                       indi = 1;
        vi = FronteraIzq(el->v3,indi);
        }

	 // Calcula la nueva posicion de n1 (p1)
	 p1 = coord[el->n1];
	 p2 = coord[el->n2];
//     if (vi==2 || vd==2) return (el->next);
	 if ((!vi && !vd) || (vi && vd))
        {
        p1.x = (p2.x+p1.x)*0.5;
        p1.y = (p2.y+p1.y)*0.5;
        p1.z = (p2.z+p1.z)*0.5;
        }
	 else if (!vd) p1 = p2;

	 // Test para verificar que los vecinos no se dan vuelta
	if (CotaColap<1.0 && el->v2 && !TestVecDer(el->n2,p1,el->v2,indd)) 
        return(el->next);
	if (CotaColap<1.0 && el->v3 && !TestVecIzq(el->n1,p1,el->v3,indi)) 
        return(el->next);

	 // Test para verificar que los vecinos sean coplanares
	if (el->v2 && !Coplanar(el,1)) 
        return(el->next);
	if (el->v3 && !Coplanar(el,2)) 
        return(el->next);

	 // Renombra los nodos
//	NuevaNum[el->n2] = el->n1;
	if (!el->v2)
        vd = 0;
    else
        vd = CambiaNodoDer(el->n2,el->n1,el->v2,indd);
    if (!vd && el->v3)
        vi = CambiaNodoIzq(el->n2,el->n1,el->v1,2);

	(*Coord)[el->n1] = p1;

	 // Actualiza los vecinos
    if (el->v2)
        {
        if      (el->v2->v1 == el) el->v2->v1 = el->v3;
	    else if (el->v2->v2 == el) el->v2->v2 = el->v3;
	    else                       el->v2->v3 = el->v3;
        }

    if (el->v3)
        {
        if      (el->v3->v1 == el) el->v3->v1 = el->v2;
	    else if (el->v3->v2 == el) el->v3->v2 = el->v2;
	    else                       el->v3->v3 = el->v2;
        }

    if (elv->v2)
        {
	    if      (elv->v2->v1 == elv) elv->v2->v1 = elv->v3;
	    else if (elv->v2->v2 == elv) elv->v2->v2 = elv->v3;
	    else                         elv->v2->v3 = elv->v3;
        }

    if (elv->v3)
        {
	    if      (elv->v3->v1 == elv) elv->v3->v1 = elv->v2;
	    else if (elv->v3->v2 == elv) elv->v3->v2 = elv->v2;
	    else                         elv->v3->v3 = elv->v2;
        }

	 // Borra los elementos de la lista
     Delete(el->v1);
	 va = el->next;
	 Delete(el);
	 return(va);
	 }

long SurfaceGen::DivideTodos()
	 {
	 double tam, d;
	 int ind;
	 long n;
	 acPoint3 pos;
     double factor = 1.4*Factor;

	 curr = first;
	 while (curr)
          {
		  CalNewNode(curr, pos, tam, ind);
		  d = MP->GetH(pos);
		  while (tam > factor*factor*d*d)
              factor*=2;
		  curr = curr->next;
          }

	 double factorant=factor;
	 long sw = 1;
	 long swt= 0;
	 SwapAll();
	 for (FlagCop = 0; FlagCop <= 1; FlagCop++)
		{
		while (factor>=1.4*Factor)
			 {
			 sw = 0;
			 curr = first;
			 while (curr)
				  {
				  CalNewNode(curr, pos, tam, ind);
				  d = factor*MP->GetH(pos);
				  if (tam > d*d)
					  {
					  n  = Coord->GetNumNodes();
					  Coord->AddCoord(pos);
					  DivideElement(curr,ind,n);
					  sw ++;
					  }
				  curr = curr->next;
				  }
			swt = swt + sw;
			SwapAll();
			if (!sw) factor/=2;
			}
        factor = factorant;
		}
	 return(swt);
	 }

long SurfaceGen::DivideObtusos(double cota)
	 {
	 long n,num;
	 double tam;
	 int ind;
	 acPoint3 pos;

	 FlagCop = 1;
	 num = 0;

	curr = first;
	while (curr)
		 {
		  Obtuso(curr, pos, tam, ind, cota);
		  if (tam > 0)
				{
				n   = Coord->GetNumNodes();
				Coord->AddCoord(pos);
				DivideElement(curr,ind,n);
				num++;
				}
		 curr = curr->next;
		 }

	 return(num);
	 }

void SurfaceGen::DivideElement(SurfTriang *el,int ind, long n)
	 {
	SurfTriang *el1, *el2, *elv;
	RotaElem(el, ind);
	elv = el->v1;

	el1 = new SurfTriang;
	if (elv)
		 el2 = new SurfTriang;
	else
		 el2 = NULL;

	el1->SetData(el->n2, el->n3, n, el->gr, el->v2, el, el2);
	el->SetData (el->n3, el->n1, n, el->gr, el->v3, elv, el1);
	InsertFirst(el1);
	if (elv)
		 {
		 el2->SetData(elv->n3, elv->n1, n, elv->gr, elv->v3, el1, elv);
		 elv->SetData(elv->n2, elv->n3, n, elv->gr, elv->v2, el2, el);
		 InsertFirst(el2);
		 }

	if (el1->v1)
		{
		if (el1->v1->v1 == el)
			 el1->v1->v1 = el1;
		else if (el1->v1->v2 == el)
			 el1->v1->v2 = el1;
		else if (el1->v1->v3 == el)
			 el1->v1->v3 = el1;
		SwapDiag(el1,el1->v1);
		}

	if (el->v1)
		{
		SwapDiag(el,el->v1);
		}

	if (elv)
		{
		if (el2->v1)
			{
			if (el2->v1->v1 == elv)
		  el2->v1->v1 = el2;
			else if (el2->v1->v2 == elv)
		  el2->v1->v2 = el2;
			else if (el2->v1->v3 == elv)
		  el2->v1->v3 = el2;
			SwapDiag(el2,el2->v1);
			}
		if (elv->v1)
			{
			SwapDiag(elv,elv->v1);
			}
		}
	 }

int SurfaceGen::SwapDiag(SurfTriang *el, SurfTriang *elv)
	 {
    long naux;
    SurfTriang *vaux;

    if (el->gr != elv->gr)
	return(0);
// Primero ordena elv

    if (elv->v1 != el)
       {
       if (elv->v2 != el)
			 {
          naux=elv->n1; elv->n1=elv->n3; elv->n3=elv->n2; elv->n2=naux;
          vaux=elv->v1; elv->v1=elv->v3; elv->v3=elv->v2; elv->v2=vaux;
          }
       else
	  {
          naux=elv->n1; elv->n1=elv->n2; elv->n2=elv->n3; elv->n3=naux;
          vaux=elv->v1; elv->v1=elv->v2; elv->v2=elv->v3; elv->v3=vaux;
          }
       }

// Aca viene el swaping

    if (!Conviene(el, elv))
	return(0);

    el->n2 = elv->n3; elv->n2 = el->n3;
    el->v1 = elv->v2; elv->v1 = el->v2; el->v2 = elv; elv->v2 = el;

    if (elv->v1 != NULL)
	{
	if      (elv->v1->v1 == el) elv->v1->v1 = elv;
	else if (elv->v1->v2 == el) elv->v1->v2 = elv;
	else                        elv->v1->v3 = elv;
	}

    if (el->v1 != NULL)
	{
	if      (el->v1->v1 == elv) el->v1->v1 = el;
	else if (el->v1->v2 == elv) el->v1->v2 = el;
	else                        el->v1->v3 = el;
	SwapDiag(el, el->v1);
	}
    if (elv->v3!= NULL)
	{
	naux    = elv->n3;
	elv->n3 = elv->n2;
	elv->n2 = elv->n1;
	elv->n1 = naux;
	vaux    = elv->v3;
	elv->v3 = elv->v2;
	elv->v2 = elv->v1;
	elv->v1 = vaux;
	SwapDiag(elv, elv->v1);
	}
    return(1);
    }

void SurfaceGen::RotaElem(SurfTriang *el, int ind)
    {
    long na;
    SurfTriang *va;
//                      Rota el elemento
    if (ind == 1)
	{
	na = el->n1;
	el->n1 = el->n2;
	el->n2 = el->n3;
	el->n3 = na;
	va = el->v1;
	el->v1 = el->v2;
	el->v2 = el->v3;
	el->v3 = va;
	}
    else if (ind == 2)
	{
	na = el->n1;
	el->n1 = el->n3;
	el->n3 = el->n2;
	el->n2 = na;
	va = el->v1;
	el->v1 = el->v3;
	el->v3 = el->v2;
	el->v2 = va;
	}
//                      Rota el vecino
    if (!el->v1) return;
    if (el->v1->v2 == el)
	{
	na = el->v1->n1;
	el->v1->n1 = el->v1->n2;
	el->v1->n2 = el->v1->n3;
	el->v1->n3 = na;
	va = el->v1->v1;
	el->v1->v1 = el->v1->v2;
	el->v1->v2 = el->v1->v3;
	el->v1->v3 = va;
	}
    else if (el->v1->v3 == el)
	{
	na = el->v1->n1;
	el->v1->n1 = el->v1->n3;
	el->v1->n3 = el->v1->n2;
	el->v1->n2 = na;
	va = el->v1->v1;
	el->v1->v1 = el->v1->v3;
	el->v1->v3 = el->v1->v2;
	el->v1->v2 = va;
	}
}

int SurfaceGen::Coplanar(SurfTriang *el, int ind)
	 {
	 acDynCoord3D &coord = *Coord;
	 acPoint3 p1, p2, p3, p4;
	 double x21,y21,z21, x31,y31,z31, x41,y41,z41, x43,y43,z43;
	 double nx,ny,nz, nvx,nvy,nvz, mod,modv,m21,m43,aretip;

	 if (ind == 0)
	     {
	     if (!el->v1) return(0);
	     p1 = coord[el->n1];
	     p2 = coord[el->n2];
	     p3 = coord[el->n3];
	     if      (el->v1->v1==el) p4 = coord[el->v1->n3];
	     else if (el->v1->v2==el) p4 = coord[el->v1->n1];
	     else if (el->v1->v3==el) p4 = coord[el->v1->n2];
	     }

	 else if (ind == 1)
	{
	if (!el->v2) return(0);
	p1 = coord[el->n2];
	p2 = coord[el->n3];
	p3 = coord[el->n1];
	if      (el->v2->v1==el) p4 = coord[el->v2->n3];
	else if (el->v2->v2==el) p4 = coord[el->v2->n1];
	else if (el->v2->v3==el) p4 = coord[el->v2->n2];
	}

    else if (ind == 2)
	{
	if (!el->v3) return(0);
	p1 = coord[el->n3];
	p2 = coord[el->n1];
	p3 = coord[el->n2];
	if      (el->v3->v1==el) p4 = coord[el->v3->n3];
	else if (el->v3->v2==el) p4 = coord[el->v3->n1];
	else if (el->v3->v3==el) p4 = coord[el->v3->n2];
	}

    x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
    x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
    x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
    x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;

    nx  = y21*z31-z21*y31; ny  = z21*x31-x21*z31; nz  = x21*y31-y21*x31;
    nvx = y41*z21-z41*y21; nvy = z41*x21-x41*z21; nvz = x41*y21-y41*x21;
    mod = sqrt(nx*nx+ny*ny+nz*nz); modv = sqrt(nvx*nvx+nvy*nvy+nvz*nvz);

    m21 = x21*x21+y21*y21+z21*z21;
    m43 = x43*x43+y43*y43+z43*z43;
    aretip = m21 > m43 ? m21 : m43; // Area tipica

    if (mod  <= 1.0e-12*aretip || modv <= 1.0e-12*aretip) 
	    {  // Elemento demasiado pequenho
       	Error(WARNING, 1, "element with null area");
    	return(0);
	    }

    nx /= mod; ny /=mod; nz/=mod;
    if ((nx*nvx + ny*nvy + nz*nvz) < CotaCopla*modv)  return(0);
    return(1);
    }

int SurfaceGen::Conviene(SurfTriang *el, SurfTriang *elv)
    {
    acDynCoord3D &coord = *Coord;
    acPoint3 p1, p2, p3, p4;
    double x21,y21,z21, x31,y31,z31, x41,y41,z41;
    double x32,y32,z32, x42,y42,z42, x43,y43,z43;
    double nx,ny,nz, nvx,nvy,nvz, mod,modv;
    double m31,m41,m32,m42, m21,m43;
    double fo,fa,f1,f2,f3,f4;
    double aretip;

    p1 = coord[el->n1];
    p2 = coord[el->n2];
    p3 = coord[el->n3];
    p4 = coord[elv->n3];

    x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
    x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
    x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
    x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;
    x32 = p3.x-p2.x;   y32 = p3.y-p2.y;   z32 = p3.z-p2.z;
    x42 = p4.x-p2.x;   y42 = p4.y-p2.y;   z42 = p4.z-p2.z;

    nx  = y21*z31-z21*y31; ny  = z21*x31-x21*z31; nz  = x21*y31-y21*x31;
    nvx = y41*z21-z41*y21; nvy = z41*x21-x41*z21; nvz = x41*y21-y41*x21;
    mod = sqrt(nx*nx+ny*ny+nz*nz); modv = sqrt(nvx*nvx+nvy*nvy+nvz*nvz);

				  // si no son seudo-coplanares no conviene
    m21 = x21*x21+y21*y21+z21*z21;
    m43 = x43*x43+y43*y43+z43*z43;
    aretip = m21 > m43 ? m21 : m43; // Area tipica

    if (mod  <= 1.0e-12*aretip
     || modv <= 1.0e-12*aretip)   // Elemento demasiado pequenho
	{
        // Verifico si cambiando diagonal soluciono el problema
    double v1x,v1y,v1z,v2x,v2y,v2z;
    v1x = y41*z31-y31*z41; v1y = z41*x31-x41*z31; v1z = x41*y31-x31*y41;
    v2x = y32*z42-y42*z32; v2y = z32*x42-x32*z42; v2z = x32*y42-x42*y32;
    if (v1x*v2x+v1y*v2y+v1z*v2z > 0) return(1);
   	Error(WARNING, 1, "element with null area");
	return(0);
	}

// Compara: n . nv = |n| |nv| cos (ang) .vs. |n| |nv| cota

    if ((nx*nvx + ny*nvy + nz*nvz) < CotaCopla*mod*modv)  return(0);
    nx  /= mod;  ny  /= mod;  nz  /= mod;

			 // 41-31 si forman angulo > 180, no se puede
    nvx = y41*z31-z41*y31; nvy = z41*x31-x41*z31; nvz = x41*y31-y41*x31;
    fa  = nx*nvx + ny*nvy + nz*nvz; // Proyecta sobre normal
    if (fa <= 0.0)  return(0);
    f1 = fa; // 2 * Area (1 4 3)

			 // 32-42 si forman angulo > 180, no se puede
    nvx = y32*z42-z32*y42; nvy = z32*x42-x32*z42; nvz = x32*y42-y32*x42;
    fa  = nx*nvx + ny*nvy + nz*nvz;
    if (fa <= 0.0)  return(0);
    f2 = fa; // 2 * Area (2 3 4)

			 // 31-32 si forman angulo > 180, es obligatorio
    nvx = y31*z32-z31*y32; nvy = z31*x32-x31*z32; nvz = x31*y32-y31*x32;
    fa  = nx*nvx + ny*nvy + nz*nvz;
    if (fa <= 0.0)  return(1);
    f3 = fa; // 2 * Area (3 1 2)

			 // 42-41 si forman angulo > 180, es obligatorio
    nvx = y42*z41-z42*y41; nvy = z42*x41-x42*z41; nvz = x42*y41-y42*x41;
    fa  = nx*nvx + ny*nvy + nz*nvz;
    if (fa <= 0.0)  return(1);
    f4 = fa; // 2 * Area (4 2 1)

    switch (TypeCrit)
	{
	case SC_DELA:
	  {
	    m21 = sqrt(x21*x21+y21*y21+z21*z21);
	    m31 = sqrt(x31*x31+y31*y31+z31*z31);
	    m32 = sqrt(x32*x32+y32*y32+z32*z32);
	    m41 = sqrt(x41*x41+y41*y41+z41*z41);
	    m42 = sqrt(x42*x42+y42*y42+z42*z42);
	    m43 = sqrt(x43*x43+y43*y43+z43*z43);

	    fo = 2.0;
				// 41 x 21
	    if (x41*x21+y41*y21+z41*z21 > 0)
	       {
	       fo = f4/(m41*m21);
	       }
				// 21 x 31
	    if (x21*x31+y21*y31+z21*z31 > 0)
	       {
	       fa = f3/(m21*m31);
	       if (fa < fo) fo = fa;
	       }
				// -12 x 42
	    if (x21*x42+y21*y42+z21*z42 < 0)
	       {
	       fa = f4/(m21*m42);
	       if (fa < fo) fo = fa;
	       }
				// 32 x -12
	    if (x32*x21+y32*y21+z32*z21 < 0)
	       {
	       fa = f3/(m32*m21);
	       if (fa < fo) fo = fa;
	       }

				// -13 x 43
	    if (x31*x43+y31*y43+z31*z43 < 0)
	       {
	       fa = f1/(m31*m43);
	       if (0.9999 * fa < fo) return(0);
	       }
				// 43 x -23
	    if (x43*x32+y43*y32+z43*z32 < 0)
	       {
	       fa = f2/(m43*m32);
	       if (0.9999 * fa < fo) return(0);
	       }
				// -24 x -34
	    if (x42*x43+y42*y43+z42*z43 > 0)
	       {
	       fa = f2/(m42*m43);
	       if (0.9999 * fa < fo) return(0);
	       }
				// -34 x -14
	    if (x43*x41+y43*y41+z43*z41 > 0)
	       {
	       fa = f1/(m43*m41);
	       if (0.9999 * fa < fo) return(0);
	       }
	    return(1);
	  }
	case SC_MINMAX:
	  {
	    m31 = sqrt(x31*x31+y31*y31+z31*z31);
	    m32 = sqrt(x32*x32+y32*y32+z32*z32);
	    m41 = sqrt(x41*x41+y41*y41+z41*z41);
	    m42 = sqrt(x42*x42+y42*y42+z42*z42);

	    f1 = f1/(m41*m31);          // f1: sin (41-31)
	    if (x41*x31+y41*y31+z41*z31 < 0.0) f1 = 2.0-f1;
	    f2 = f2/(m32*m42);
	    if (x32*x42+y32*y42+z32*z42 < 0.0) f2 = 2.0-f2;
	    if (f1 > f2)
	       fo = f1;
	    else
	       fo = f2;
	    f3 = f3/(m31*m32);
	    if (x31*x32+y31*y32+z31*z32 < 0.0) f3 = 2.0-f3;
	    if (fo < 0.9999 * f3) return(1);
	    f4 = f4/(m42*m41);
	    if (x42*x41+y42*y41+z42*z41 < 0.0) f4 = 2.0-f4;
	    if (fo < 0.9999 * f4) return(1);
	    return(0);
	  }
	default:
	    return(0);
	}
    }

void SurfaceGen::Obtuso(SurfTriang *El, acPoint3 &pos,
		double &tam, int &ind, double cota)
    {
	 acDynCoord3D &coord = *Coord;
	 double a, b, c, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

    dx = p2.x-p1.x;
    dy = p2.y-p1.y;
    dz = p2.z-p1.z;
    a = dx*dx+dy*dy+dz*dz;
    dx = p3.x-p2.x;
    dy = p3.y-p2.y;
    dz = p3.z-p2.z;
    b = dx*dx+dy*dy+dz*dz;
    if (a>b)
       {
       ind = 0;
       tam = a;
       }
    else
       {
       ind = 1;
       tam = b;
       }
    dx = p3.x-p1.x;
    dy = p3.y-p1.y;
    dz = p3.z-p1.z;
    c = dx*dx+dy*dy+dz*dz;
    if (tam < c)
       {
       if (c < a + b - 2*cota*sqrt(a*b)) tam = 0.0;
       else
          {
          ind = 2;
          pos.x = (p3.x+p1.x)*0.5;
          pos.y = (p3.y+p1.y)*0.5;
          pos.z = (p3.z+p1.z)*0.5;
          }
       }
    else if (ind == 0)
       {
       if (a < c + b - 2*cota*sqrt(c*b)) tam = 0.0;
       else
          {
          pos.x = (p2.x+p1.x)*0.5;
          pos.y = (p2.y+p1.y)*0.5;
          pos.z = (p2.z+p1.z)*0.5;
          }
       }
    else
       {
       if (b < a + c - 2*cota*sqrt(a*c)) tam = 0.0;
       else
          {
          pos.x = (p2.x+p3.x)*0.5;
          pos.y = (p2.y+p3.y)*0.5;
          pos.z = (p2.z+p3.z)*0.5;
          }
       }
    }

void SurfaceGen::CalNewNode(SurfTriang *El, acPoint3 &pos,
			    double &tam, int &ind)
    {
	 acDynCoord3D &coord = *Coord;
	 double d, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

	 tam = 0.0; ind = -1;
	 if ( FlagCop || !Coplanar(El,0) )
       {
       dx = p2.x-p1.x;
       dy = p2.y-p1.y;
       dz = p2.z-p1.z;
       tam = dx*dx+dy*dy+dz*dz;
       ind = 0;
       }
    if ( FlagCop || !Coplanar(El,1) )
       {
       dx = p3.x-p2.x;
       dy = p3.y-p2.y;
       dz = p3.z-p2.z;
       d  = dx*dx+dy*dy+dz*dz;
       if (d > tam)
	   {
	   ind = 1;
	   tam = d;
	   }
       }
    if ( FlagCop || !Coplanar(El,2) )
       {
       dx = p1.x-p3.x;
       dy = p1.y-p3.y;
       dz = p1.z-p3.z;
       d  = dx*dx+dy*dy+dz*dz;
       if (d > tam)
	   {
	   ind = 2;
	   tam = d;
	   }
       }

    if (ind == 0)
	{
	pos.x = (p2.x + p1.x)*0.5;
	pos.y = (p2.y + p1.y)*0.5;
	pos.z = (p2.z + p1.z)*0.5;
	}
    else if (ind == 1)
	{
	pos.x = (p3.x + p2.x)*0.5;
	pos.y = (p3.y + p2.y)*0.5;
	pos.z = (p3.z + p2.z)*0.5;
	}
    else
	{
	pos.x = (p1.x + p3.x)*0.5;
	pos.y = (p1.y + p3.y)*0.5;
	pos.z = (p1.z + p3.z)*0.5;
	}
    }

double SurfaceGen::CalDiamEl(SurfTriang *El)
    {
	 acDynCoord3D &coord = *Coord;
	 double diam, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

    dx    = p2.x-p1.x; dy = p2.y-p1.y; dz = p2.z-p1.z;
    diam  = sqrt(dx*dx+dy*dy+dz*dz);
    dx    = p3.x-p2.x; dy = p3.y-p2.y; dz = p3.z-p2.z;
    diam += sqrt(dx*dx+dy*dy+dz*dz);
    dx    = p1.x-p3.x; dy = p1.y-p3.y; dz = p1.z-p3.z;
    diam += sqrt(dx*dx+dy*dy+dz*dz);
    diam /= 3.0;

    return (diam);
    }

acPoint3 SurfaceGen::CalCentElem(SurfTriang *El)
    {
	 acDynCoord3D &coord = *Coord;
	 acPoint3 cent, p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

	 cent.x = ( p1.x + p2.x + p3.x )/3.0;
    cent.y = ( p1.y + p2.y + p3.y )/3.0;
    cent.z = ( p1.z + p2.z + p3.z )/3.0;

    return(cent);
    }

void SurfaceGen::InsertFirst(SurfTriang *el)
    {
    el->next    = first;
    el->prev    = NULL;
    first->prev = el;
    first       = el;
    }

void SurfaceGen::Delete(SurfTriang *el)
    {
    if (el == first)
        {
        el->next->prev = NULL;
        first = el->next;
        }
    else if (!el->next)
        el->prev->next = NULL;
    else
        {
        el->prev->next = el->next;
        el->next->prev = el->prev;
        }
    delete el;
    }

void SurfaceGen::Print(FILE *fp)
    {
    long *nelg;
    int i;

//                                      Imprime informacion de grupos
    nelg = (long *) malloc (NumGroups * sizeof(long));
    for (i=0; i<NumGroups; i++)
        nelg[i]=0;

    curr=first;
    while (curr)
       {
       nelg[curr->gr]++;
       curr = curr->next;
       }

    fprintf(fp, "*ELEMENT GROUPS\n %d\n",NumGroups);
    for (i=0; i<NumGroups; i++)
	    fprintf(fp, "%d %ld Tri3\n", i+1, nelg[i]);
    free(nelg);

//                                      Imprime incidencia
    fprintf(fp,"\n*INCIDENCE\n");
    for (i=0; i<NumGroups; i++)
        {
        fprintf(fp,"\n");
        curr = first;
        while (curr)
            {
            if (curr->gr == i)
            fprintf(fp,"%ld %ld %ld\n",curr->n1+1, curr->n2+1, curr->n3+1);
            curr = curr->next;
            }
        }

//                                      Imprime coordenadas
    Coord->Print(fp);

//                                      Imprime volumen
    acLimit3 vol = Coord->Box();
    fprintf(fp, "*FRONT_VOLUME\n %lf %lf %lf %lf %lf %lf\n",
	    vol.xmin,vol.xmax,vol.ymin,vol.ymax,vol.zmin,vol.zmax);
    }

void SurfaceGen::PrintEdges(FILE *fp)
    {
    long fn, fna, n;
    long nnodes = Coord->GetNumNodes();
    long * nlist = (long *)malloc(sizeof(long)*nnodes);

    for (n=0; n<nnodes; n++)
        nlist[n]=0;

    for (int g=0; g<NumGroups; g++)
        {
        for (int gv=g+1; gv<NumGroups; gv++)
            {
            curr = first;
            int flag = 0;
            while (curr)
                {
                if (curr->gr == g)
                    {
                    if (curr->v1 && curr->v1->gr == gv)
                        { nlist[curr->n1] = curr->n2+1; flag=1; }
                    if (curr->v2 && curr->v2->gr == gv)
                        { nlist[curr->n2] = curr->n3+1; flag=1; }
                    if (curr->v3 && curr->v3->gr == gv)
                        { nlist[curr->n3] = curr->n1+1; flag=1; }
                    }
                curr = curr->next;
                }
            if (!flag) continue;

            //  Hay frontera entre g y gv. Busco el primer nodo
            for (n=0; n<nnodes; n++)
                {
                if (nlist[n])
                    nlist[labs(nlist[n])-1]=-nlist[labs(nlist[n])-1];
                }
            for (n=0; n<nnodes; n++)
                {
                if (nlist[n])
                    {
                    fn = n;
                    if (nlist[n]>0) break;
                    }
                }
            fprintf(fp,"Frontera entre grupos %d-%d\n",g+1,gv+1);
            fprintf(fp," %ld\n",fn+1);
            long nnf=0;
            while (nlist[fn])
                {
                fna = fn;
                fn = labs(nlist[fn]);
                nlist[fna]=0;
                fprintf(fp," %ld\n",fn);
                fn--; nnf++;
                }
            fprintf(fp,"Numero de nodos: %ld\n\n",nnf);
            }
        }
    }

void SurfaceGen::PrintBoundary(FILE *fpb)
    {
    long nnodes = Coord->GetNumNodes();
    long j, nnodgr;
    int i,k;
    int * nodeflag = (int *)malloc(sizeof(int)*nnodes);
    if (!nodeflag)
	    {
	    Error(WARNING, 1, "Can't allocate memory");
	    return;
	    }
    for (i=0; i<NumGroups; i++)
	    {
	    fprintf(fpb, "*GROUP%d\n",i+1);
	    for (j=0; j<nnodes; j++)
	        nodeflag[j] = 0;

	    curr = first;
	    while (curr)
	        {
	        if (curr->gr == i)
		        {
		        nodeflag[curr->n1] = 1;
		        nodeflag[curr->n2] = 1;
		        nodeflag[curr->n3] = 1;
		        }
	        curr = curr->next;
	        }
    	nnodgr = 0;
	    for (j=0; j<nnodes; j++)
	        if (nodeflag[j]) nnodgr++;

        fprintf(fpb, "*NUMBER OF NODES: %ld\n", nnodgr);
	    k = 1;
	    for (j=0; j<nnodes; j++)
	        if (nodeflag[j])
    		    {
	    	    fprintf(fpb, " %ld", j+1);
		        k++;
		        if (k>10) { fprintf (fpb, "\n"); k=1; }
		        nodeflag[j] = 0;
		        }
	    if (k<=10) fprintf (fpb, "\n");
	    }
    free(nodeflag);
    }

void SurfaceGen::PegaNodos(double cotap)
    {
    long nod=Coord->GetNumNodes();
    long *nuevo;
    int skip = 0;

    nuevo = (long *)malloc(nod * sizeof(long));
    for (long i=0; i<nod; i++) nuevo[i] = 0;

    // Marca nodos del borde
    curr = first;
    while (curr)
       {
       if (!curr->v1) { nuevo[curr->n1]=1; nuevo[curr->n2]=1;}
       if (!curr->v2) { nuevo[curr->n2]=1; nuevo[curr->n3]=1;}
       if (!curr->v3) { nuevo[curr->n3]=1; nuevo[curr->n1]=1;}
       curr = curr->next;
       }
    Coord->MergeNodes(cotap, nuevo);

    // Renumera la incidencia
    curr = first;
    while (curr)
       {
       curr->n1 = nuevo[curr->n1];
       curr->n2 = nuevo[curr->n2];
       curr->n3 = nuevo[curr->n3];
       curr->flg=0;
       curr = curr->next;
       }

    // Une la incidencia
    SurfTriang *aux;
    curr = first;
    while (curr)
       {
       if (!curr->v1)                   // no existe vecino 1
           {
           aux = curr->next;
           while (aux)                  // Lo busca primero bien orientado
               {
               if (aux->n1 == curr->n1 && aux->n3 == curr->n2)
                   { aux->v3 = curr; curr->v1 = aux; break;}
               if (aux->n2 == curr->n1 && aux->n1 == curr->n2)
                   { aux->v1 = curr; curr->v1 = aux; break;}
               if (aux->n3 == curr->n1 && aux->n2 == curr->n2)
                   { aux->v2 = curr; curr->v1 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v1)               // Ahora lo busca orientado al reves
               {
               aux = curr->next;
               while (aux)
                   {
                   if (aux->n1 == curr->n2 && aux->n3 == curr->n1)
                       {
                       chori(aux);
                       aux->v3 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n2 && aux->n1 == curr->n1)
                       {
                       chori(aux);
                       aux->v2 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n2 && aux->n2 == curr->n1)
                       {
                       chori(aux);
                       aux->v1 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }
       if (!curr->v2)
           {
           aux = curr->next;
           while (aux)
               {
               if (aux->n1 == curr->n2 && aux->n3 == curr->n3)
                   { aux->v3 = curr; curr->v2 = aux; break;}
               if (aux->n2 == curr->n2 && aux->n1 == curr->n3)
                   { aux->v1 = curr; curr->v2 = aux; break;}
               if (aux->n3 == curr->n2 && aux->n2 == curr->n3)
                   { aux->v2 = curr; curr->v2 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v2)
               {
               aux = curr->next;
               while (aux)
                   {
                   if (aux->n1 == curr->n3 && aux->n3 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v3 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n3 && aux->n1 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v2 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n3 && aux->n2 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v1 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }
       if (!curr->v3)
           {
           aux = curr->next;
           while (aux)
               {
	       if (aux->n1 == curr->n3 && aux->n3 == curr->n1)
		   { aux->v3 = curr; curr->v3 = aux; break;}
	       if (aux->n2 == curr->n3 && aux->n1 == curr->n1)
                   { aux->v1 = curr; curr->v3 = aux; break;}
               if (aux->n3 == curr->n3 && aux->n2 == curr->n1)
                   { aux->v2 = curr; curr->v3 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v3)
               {
               aux = curr->next;
               while (aux)
                   {
	           if (aux->n1 == curr->n3 && aux->n3 == curr->n1)
		       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v3 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n3 && aux->n1 == curr->n1)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v2 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n3 && aux->n2 == curr->n1)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v1 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }

       if (!skip && (!curr->v1 || !curr->v2 || !curr->v3))
           {
           char buf[280];
           acDynCoord3D &coord = *Coord;
           acPoint3 p1, p2, p3;

           p1 = coord[curr->n1];
           p2 = coord[curr->n2];
           p3 = coord[curr->n3];
           sprintf(buf,
           "Quedo una frontera!\n%lg %lg %lg\n%lg %lg %lg\n%lg %lg %lg\nSkip others?",
            p1.x,p1.y,p1.z,p2.x,p2.y,p2.z,p3.x,p3.y,p3.z);
            Error(WARNING, 1, buf);
            skip = 1;
           }

       curr = curr->next;
       }
    free(nuevo);
    }

void SurfaceGen::chori(SurfTriang *el)
    {
    SurfTriang *va;
    long na;
    acQueue Cola(sizeof(SurfTriang *));
    acQueue Colab(sizeof(SurfTriang *));
    Cola.Push(&el);
    Colab.Push(&el);
    el->flg = 1;
    while(Cola.Pop(&el))
        {
        na = el->n1; el->n1 = el->n3; el->n3 = na;
        va = el->v1; el->v1 = el->v2; el->v2 = va;
        va = el->v1;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        va = el->v2;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        va = el->v3;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        }

    while(Colab.Pop(&el))
        {
        el->flg=0;
        }
    }

int SurfaceGen::TestVecDer(long no,acPoint3 p, SurfTriang *el, int ind)
	{
	if (ind==0 && el->v1==curr) return(1);
	if (ind==1 && el->v2==curr) return(1);
	if (ind==2 && el->v3==curr) return(1);

	acPoint3 p1,p2,p3,normv,normn;
	if      (el->n1 == no)
	    {
	    p1 = (*Coord)[el->n1];
	    p2 = (*Coord)[el->n2];
	    p3 = (*Coord)[el->n3];
	    }
	else if (el->n2 == no)
	    {
	    p1 = (*Coord)[el->n2];
	    p2 = (*Coord)[el->n3];
	    p3 = (*Coord)[el->n1];
	    }
	else if (el->n3 == no)
	    {
	    p1 = (*Coord)[el->n3];
	    p2 = (*Coord)[el->n1];
	    p3 = (*Coord)[el->n2];
	    }
	else
	    {
       	Error(WARNING, 1, "Invalid surface");
	    return(0);
	    }

	normv.x = (p2.y-p1.y)*(p3.z-p1.z)-(p2.z-p1.z)*(p3.y-p1.y);
	normv.y = (p2.z-p1.z)*(p3.x-p1.x)-(p2.x-p1.x)*(p3.z-p1.z);
	normv.z = (p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
	normn.x = (p2.y-p.y)*(p3.z-p.z)-(p2.z-p.z)*(p3.y-p.y);
	normn.y = (p2.z-p.z)*(p3.x-p.x)-(p2.x-p.x)*(p3.z-p.z);
	normn.z = (p2.x-p.x)*(p3.y-p.y)-(p2.y-p.y)*(p3.x-p.x);
	if (normv.x*normn.x + normv.y*normn.y + normv.z*normn.z <= 0.0) return(0);

	 // Averigua por donde debe salir y sale
	if (ind ==0)
	    {
        if (!el->v1) return 1;
	    if      (el->v1->v1 == el) ind = 1;
	    else if (el->v1->v2 == el) ind = 2;
	    else if (el->v1->v3 == el) ind = 0;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    return(TestVecDer(no, p, el->v1, ind));
	    }
    else if (ind == 1)
        {
        if (!el->v2) return 1;
	    if      (el->v2->v1 == el) ind = 1;
	    else if (el->v2->v2 == el) ind = 2;
	    else if (el->v2->v3 == el) ind = 0;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    return(TestVecDer(no, p, el->v2, ind));
	    }
    else   //ind == 2
        {
        if (!el->v3) return 1;
	    if      (el->v3->v1 == el) ind = 1;
	    else if (el->v3->v2 == el) ind = 2;
	    else if (el->v3->v3 == el) ind = 0;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    return(TestVecDer(no, p, el->v3, ind));
	    }
	}

int SurfaceGen::TestVecIzq(long no,acPoint3 p, SurfTriang *el, int ind)
	{
	if (ind==0 && el->v1 == curr) return(1);
	if (ind==1 && el->v2 == curr) return(1);
	if (ind==2 && el->v3 == curr) return(1);

	acPoint3 p1,p2,p3,normv,normn;
	if      (el->n1 == no)
	    {
	    p1 = (*Coord)[el->n1];
	    p2 = (*Coord)[el->n2];
	    p3 = (*Coord)[el->n3];
	    }
	else if (el->n2 == no)
	    {
	    p1 = (*Coord)[el->n2];
	    p2 = (*Coord)[el->n3];
	    p3 = (*Coord)[el->n1];
	    }
	else if (el->n3 == no)
	    {
	    p1 = (*Coord)[el->n3];
	    p2 = (*Coord)[el->n1];
	    p3 = (*Coord)[el->n2];
	    }
	else
	    {
       	Error(WARNING, 1, "Invalid surface");
	    return(0);
	    }

	normv.x = (p2.y-p1.y)*(p3.z-p1.z)-(p2.z-p1.z)*(p3.y-p1.y);
	normv.y = (p2.z-p1.z)*(p3.x-p1.x)-(p2.x-p1.x)*(p3.z-p1.z);
	normv.z = (p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
	normn.x = (p2.y-p.y)*(p3.z-p.z)-(p2.z-p.z)*(p3.y-p.y);
	normn.y = (p2.z-p.z)*(p3.x-p.x)-(p2.x-p.x)*(p3.z-p.z);
	normn.z = (p2.x-p.x)*(p3.y-p.y)-(p2.y-p.y)*(p3.x-p.x);
	if (normv.x*normn.x + normv.y*normn.y + normv.z*normn.z <= 0.0) return(0);

	 // Averigua por donde debe salir y sale
	if (ind ==0)
	    {
        if (!el->v1) return 1;
	    if      (el->v1->v1 == el) ind = 2;
	    else if (el->v1->v2 == el) ind = 0;
	    else if (el->v1->v3 == el) ind = 1;
	    else
		    {
           	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v1, ind));
	    }
	else if (ind == 1)
	    {
        if (!el->v2) return 1;
        if      (el->v2->v1 == el) ind = 2;
	    else if (el->v2->v2 == el) ind = 0;
	    else if (el->v2->v3 == el) ind = 1;
	    else
		    {
           	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v2, ind));
	    }
	else   //ind == 2
	    {
        if (!el->v3) return 1;
	    if      (el->v3->v1 == el) ind = 2;
	    else if (el->v3->v2 == el) ind = 0;
	    else if (el->v3->v3 == el) ind = 1;
	    else
		    {
           	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v3, ind));
	    }
	}

int SurfaceGen::FronteraDer(SurfTriang *el, int ind)
    {
	 // Averigua por donde debe salir y sale
	int r;
    if (ind ==0)
	    {
        if (!el->v1)              return 0;
	    if (el->v1 == curr)       return 1;
	    if (el->gr != el->v1->gr) return 0;
	    if      (el->v1->v1 == el) ind = 1;
	    else if (el->v1->v2 == el) ind = 2;
	    else if (el->v1->v3 == el) ind = 0;
	    else
		    {
           	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    r = FronteraDer(el->v1,ind);
	    }
    else if (ind == 1)
        {
        if (!el->v2)              return 0;
	    if (el->v2 == curr)       return 1;
        if (el->gr != el->v2->gr) return(0);
	    if      (el->v2->v1 == el) ind = 1;
        else if (el->v2->v2 == el) ind = 2;
        else if (el->v2->v3 == el) ind = 0;
        else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraDer(el->v2,ind);
        }
    else   //ind == 2
        {
        if (!el->v3)              return 0;
	    if (el->v3 == curr)       return 1;
        if (el->gr != el->v3->gr) return(0);
	    if      (el->v3->v1 == el) ind = 1;
	    else if (el->v3->v2 == el) ind = 2;
	    else if (el->v3->v3 == el) ind = 0;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraDer(el->v3,ind);
	    }
    if (r)  return (r+1);
    else    return 0;
    }

int SurfaceGen::FronteraIzq(SurfTriang *el, int ind)
    {
    // Averigua por donde debe salir y sale
	int r;
    if (ind ==0)
        {
        if (!el->v1)              return 0;
	    if (el->v1 == curr)       return 1;
	    if (el->gr != el->v1->gr) return 0;
        if      (el->v1->v1 == el) ind = 2;
        else if (el->v1->v2 == el) ind = 0;
        else if (el->v1->v3 == el) ind = 1;
        else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraIzq(el->v1,ind);
        }
    else if (ind == 1)
	    {
        if (!el->v2)              return 0;
	    if (el->v2 == curr)       return 1;
	    if (el->gr != el->v2->gr) return 0;
	    if      (el->v2->v1 == el) ind = 2;
	    else if (el->v2->v2 == el) ind = 0;
	    else if (el->v2->v3 == el) ind = 1;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraIzq(el->v2,ind);
	    }
    else   //ind == 2
        {
        if (!el->v3)              return 0;
	    if (el->v3 == curr)       return 1;
	    if (el->gr != el->v3->gr) return 0;
	    if      (el->v3->v1 == el) ind = 2;
	    else if (el->v3->v2 == el) ind = 0;
	    else if (el->v3->v3 == el) ind = 1;
	    else
	        {
           	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraIzq(el->v3,ind);
	    }
    if (r)  return (r+1);
    else    return 0;
    }

int SurfaceGen::CambiaNodoIzq (long no, long nn, SurfTriang *el, int ind)
    {
    // Renombra el nodo en cuestion
    if      (el->n1 == no) el->n1 = nn;
    else if (el->n2 == no) el->n2 = nn;
    else if (el->n3 == no) el->n3 = nn;
    else
   	    Error(FATAL_ERROR, 0, "Invalid surface");
    // Averigua por donde debe salir y sale
    if (ind ==0)
	    {
        if (!el->v1) return 0;
        if (el->v1 == curr) return 1;
        if      (el->v1->v1 == el) ind = 2;
        else if (el->v1->v2 == el) ind = 0;
        else if (el->v1->v3 == el) ind = 1;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoIzq(no,nn,el->v1,ind);
    	}
    else if (ind == 1)
        {
        if (!el->v2) return 0;
        if (el->v2 == curr) return 1;
        if      (el->v2->v1 == el) ind = 2;
        else if (el->v2->v2 == el) ind = 0;
        else if (el->v2->v3 == el) ind = 1;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoIzq(no,nn,el->v2,ind);
        }
    else   //ind == 2
	    {
        if (!el->v3) return 0;
        if (el->v3 == curr) return 1;
        if      (el->v3->v1 == el) ind = 2;
        else if (el->v3->v2 == el) ind = 0;
        else if (el->v3->v3 == el) ind = 1;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoIzq(no,nn,el->v3,ind);
        }
    }

int SurfaceGen::CambiaNodoDer (long no, long nn, SurfTriang *el, int ind)
    {
    // Renombra el nodo en cuestion
    if      (el->n1 == no) el->n1 = nn;
    else if (el->n2 == no) el->n2 = nn;
    else if (el->n3 == no) el->n3 = nn;
    else
   	    Error(FATAL_ERROR, 0, "Invalid surface");
    // Averigua por donde debe salir y sale
    if (ind ==0)
	    {
        if (!el->v1)        return 0;
        if (el->v1 == curr) return 1;
        if      (el->v1->v1 == el) ind = 1;
        else if (el->v1->v2 == el) ind = 2;
        else if (el->v1->v3 == el) ind = 0;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoDer(no,nn,el->v1,ind);
    	}
    else if (ind == 1)
        {
        if (!el->v2) return 0;
        if (el->v2 == curr) return 1;
        if      (el->v2->v1 == el) ind = 1;
        else if (el->v2->v2 == el) ind = 2;
        else if (el->v2->v3 == el) ind = 0;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoDer(no,nn,el->v2,ind);
        }
    else   //ind == 2
	    {
        if (!el->v3) return 0;
        if (el->v3 == curr) return 1;
        if      (el->v3->v1 == el) ind = 1;
        else if (el->v3->v2 == el) ind = 2;
        else if (el->v3->v3 == el) ind = 0;
	    else
    	    Error(FATAL_ERROR, 0, "Invalid surface");
    	return CambiaNodoDer(no,nn,el->v3,ind);
        }
    }

void SurfaceGen::SwapGroup(int num)
    {
    long na;
    SurfTriang *el;
    curr = first;
    num--;
    while (curr)
	{
        if (curr->gr == num)
	    {
	    el = curr->v3;
	    curr->v3 = curr->v2;
	    curr->v2 = el;
            na = curr->n1;
            curr->n1 = curr->n2;
            curr->n2 = na;
            }
        curr = curr->next;
        }
    }

void  SurfaceGen::Information(long *nod, long *nel, long *grp)
    {
    *nod = Coord->GetNumNodes();
    *nel = 0; *grp = 0;
    curr=first;
    while(curr)
        {
        *nel = *nel+1;
        if (curr->gr > *grp) *grp = curr->gr;
        curr=curr->next;
        }
    *grp = *grp+1;
    }

void SurfaceGen::Smooth(int iter, double fac, int shr, int Gr)
    {
    long n;
    long nod     = Coord->GetNumNodes();
    long *num    = (long*)malloc(sizeof(long)*nod);
    acPoint3 *np = (acPoint3*)malloc(sizeof(acPoint3)*nod);
    
    for (n=0; n<nod; n++) 
        num[n]=0;
    curr=first;
    while (curr)        //Marco los que no se pueden mover
        {
        if (Gr && (Gr-1)!=curr->gr) 
			{
			num[curr->n1]=num[curr->n2]=num[curr->n3]=-1;
			curr=curr->next; 
			continue;
			}
        if (!curr->v1 || curr->v1->gr != curr->gr)
            num[curr->n1]=num[curr->n2]=-1;
        if (!curr->v2 || curr->v2->gr != curr->gr)
            num[curr->n2]=num[curr->n3]=-1;
        if (!curr->v3 || curr->v3->gr != curr->gr)
            num[curr->n3]=num[curr->n1]=-1;
        curr=curr->next;
        }

    acPoint3 p1,p2,p3;
    if (shr) iter*=2;
    double fac2,fac1=fac;
    if (shr)
        fac2=1.0/(0.1-1.0/fac);
    else
        fac2=fac;
    for (int it=0; it<iter; it++)
        {
        for (n=0; n<nod; n++) 
            {
            if (num[n]==-1) continue;
			num[n]=0;
			np[n].x=np[n].y=np[n].z=0.0;
            }
        curr=first;
        while (curr)        //Calculo nuevas coordenadas
            {
            p1=(*Coord)[curr->n1];
            p2=(*Coord)[curr->n2];
            p3=(*Coord)[curr->n3];
            n = curr->n1;
            if (num[n]!=-1)
                {
                np[n].x = np[n].x + p2.x + p3.x;
                np[n].y = np[n].y + p2.y + p3.y;
                np[n].z = np[n].z + p2.z + p3.z;
                num[n]+=2;
                }
            n = curr->n2;
            if (num[n]!=-1)
                {
                np[n].x = np[n].x + p3.x + p1.x;
                np[n].y = np[n].y + p3.y + p1.y;
                np[n].z = np[n].z + p3.z + p1.z;
                num[n]+=2;
                }
            n = curr->n3;
            if (num[n]!=-1)
                {
                np[n].x = np[n].x + p1.x + p2.x;
                np[n].y = np[n].y + p1.y + p2.y;
                np[n].z = np[n].z + p1.z + p2.z;
                num[n]+=2;
                }
            curr=curr->next;
            }

        if ((it/2)*2==it) fac=fac1;
        else              fac=fac2;
        for (n=0; n<nod; n++)
            {
            if (num[n]!=-1 && num[n])
                {
                p1   = (*Coord)[n];
                p1.x = p1.x + fac*(np[n].x/num[n]-p1.x);
                p1.y = p1.y + fac*(np[n].y/num[n]-p1.y);
                p1.z = p1.z + fac*(np[n].z/num[n]-p1.z);
                (*Coord)[n]=p1;
                }
            }
        }
    free(np);
    free(num);
    }
void SurfaceGen::SelectElement(long *elem, int seltype, int inttype, int add, int gr,
              double x,double y,double z,double nx,double ny,double nz, double r)
    {
    acPoint3 p1,p2,p3;
    double d,dis;
    curr=first;
    long el=-1;
    switch (seltype)
        {
        case 0:
            while(curr)
                {
                elem[++el]=add;
                curr=curr->next;
                }
            break;
        case 1:
            d = -(nx*x + ny*y + nz*z);
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        dis= nx*p1.x + ny*p1.y + nz*p1.z + d;
                if (dis>0 && inttype) { elem[el]=add; continue; }
                else if (dis<0 && !inttype) continue;
		        dis= nx*p2.x + ny*p2.y + nz*p2.z + d;
                if (dis>0 && inttype) { elem[el]=add; continue; }
                else if (dis<0 && !inttype) continue;
		        dis= nx*p3.x + ny*p3.y + nz*p3.z + d;
                if (dis>0 )             elem[el]=add;
                }
            break;
        case 2:
            while (curr)
                {
                el++;
                if (curr->gr==gr) elem[el]=add;
                curr=curr->next;
                }
            break;
        case 3:
            r=r*r;
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        dis= (p1.x-x)*(p1.x-x)+(p1.y-y)*(p1.y-y)+(p1.z-z)*(p1.z-z);
                if (dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        dis= (p2.x-x)*(p2.x-x)+(p2.y-y)*(p2.y-y)+(p2.z-z)*(p2.z-z);
                if (dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        dis= (p3.x-x)*(p3.x-x)+(p3.y-y)*(p3.y-y)+(p3.z-z)*(p3.z-z);
                if (dis<r)              elem[el]=add;
                }
            break;
        case 4:
            dis = sqrt(nx*nx + ny*ny + nz*nz);
            if (dis==0.0) break;
            nx /= dis; ny /= dis; nz /= dis;
            r=r*r;
            double px,py,pz;
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        px = (p1.y-y)*nz - (p1.z-z)*ny;
		        py = (p1.z-z)*nx - (p1.x-x)*nz;
		        pz = (p1.x-x)*ny - (p1.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        px = (p2.y-y)*nz - (p2.z-z)*ny;
		        py = (p2.z-z)*nx - (p2.x-x)*nz;
		        pz = (p2.x-x)*ny - (p2.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        px = (p3.y-y)*nz - (p3.z-z)*ny;
		        py = (p3.z-z)*nx - (p3.x-x)*nz;
		        pz = (p3.x-x)*ny - (p3.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r)              elem[el]=add;
                }
            break;

        default:
            break;
        }
    }

void SurfaceGen::SetGroup(long *elem, int gr)
    {
    if (NumGroups<gr+1) NumGroups=gr+1;
    long *msw= (long *)malloc(sizeof(long)*NumGroups);
    for (int i=0; i<NumGroups; i++)
        msw[i]=0;

    long el=0;
    curr=first;
    while(curr)
        {
        if (elem[el++]) 
            curr->gr=gr;
        msw[curr->gr]=1;
        curr=curr->next;
        }

    for (i=1; i<NumGroups; i++)
        msw[i]+=msw[i-1];
    NumGroups=msw[NumGroups-1];

    curr=first;
    while(curr)
        {
        curr->gr=msw[curr->gr]-1;
        curr=curr->next;
        }
    }

long SurfaceGen::RemoveGroup(long gr)
    {
    gr--;
    curr=first;
    while(curr)
        {
        if (curr->gr==gr)
            {
            PutNullVec(curr,gr);
            if (curr->next)
                {
                curr=curr->next;
                Delete(curr->prev);
                }
            else
                {
                Delete(curr);
                curr=0;
                }
            continue;
            }
        else if (curr->gr>gr) 
            curr->gr=curr->gr-1;
        curr=curr->next;
        }
    NumGroups--;
    return RemoveFreeNodes();
    }

void SurfaceGen::PutNullVec(SurfTriang *el, long gr)
    {
    if (el->v1)
        {
        if      (el->v1->v1==el) el->v1->v1=0;
        else if (el->v1->v2==el) el->v1->v2=0;
        else if (el->v1->v3==el) el->v1->v3=0;
        }
    if (el->v2)
        {
        if      (el->v2->v1==el) el->v2->v1=0;
        else if (el->v2->v2==el) el->v2->v2=0;
        else if (el->v2->v3==el) el->v2->v3=0;
        }
    if (el->v3)
        {
        if      (el->v3->v1==el) el->v3->v1=0;
        else if (el->v3->v2==el) el->v3->v2=0;
        else if (el->v3->v3==el) el->v3->v3=0;
        }
    }

void SurfaceGen::AddCapa(long gr1,long gr2)
    {
    }

void SurfaceGen::Transform(double tx, double ty, double tz,
                           double sx, double sy, double sz,
                           double rx, double ry, double rz)
    {
    acPoint3 p;
    acDynCoord3D &c=*Coord;
    for (long n=0; n<Coord->GetNumNodes(); n++)
        {
        p=c[n];
        p.x+=tx; p.y+=ty; p.z+=tz;
        p.x*=sx; p.y*=sy; p.z*=sz;
        c[n]=p;
        }
    }

nodlist::~nodlist()
    {
    struct nod *pr;
    while (first)
        {
        pr=first;
        first=first->next;
        delete pr;
        }
    }
int nodlist::add(long n)
    {
    int num=0;
    struct nod *pr=0,*el=first;
    while (el)
        {
        pr = el;
        if (el->n==n) num++;
        el=el->next;
        }
    if (pr)
        {
        pr->next=new struct nod;
        pr->next->n=n;
        pr->next->next=0;
        }
    else
        {
        first=new struct nod;
        first->n=n;
        first->next=0;
        }
    return num;
    }
