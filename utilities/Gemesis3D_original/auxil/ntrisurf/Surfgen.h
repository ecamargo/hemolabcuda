#ifndef SURFGEN
#define SURFGEN

#include "General.h"
#include "dyncoo3d.h"
#include "malhapar.h"
//#include "mesh3d.h"

class SurfTriang
    {
    public:
        long n1, n2, n3;
        SurfTriang *v1, *v2, *v3;
        SurfTriang *prev, *next;
        int gr,flg;

    public:
        SurfTriang() { prev = next = NULL; }
        void SetData(long N1, long N2, long N3, int g,
                     SurfTriang *V1, SurfTriang *V2, SurfTriang *V3)
            {
            gr = g;
            n1 = N1; n2 = N2; n3 = N3;
            v1 = V1; v2 = V2; v3 = V3;
            flg = 0;
            }
    };

#define SC_DELA   1
#define SC_MINMAX 2

class SurfaceGen
    {
    private:
        SurfTriang *first, *curr;
        acDynCoord3D *Coord;
        ParamMesh *MP;

        long *NuevaNum;
        int FlagCop;
        double CotaColap, CotaCopla, Factor;
        int TypeCrit;
        int NumGroups;

    public:
        SurfaceGen()
            {
            first = NULL;
            curr  = NULL;
            Coord = NULL;
            TypeCrit = SC_DELA;
            Factor=1.0;
            }

        void  Read(FILE *fp);
		void  SetPar(ParamMesh *mpar) { MP=mpar;}
//        SurfaceGen(Mesh3d *mesh, ParamMesh *mpar);
//        void  ImportSurface(Mesh3d *mesh);
        void  Print(FILE *fp);
        void  PrintBoundary(FILE *fp);
        void  PrintEdges(FILE *fp);

        long  DivideTodos();
        long  DivideObtusos(double cota);

        long  ColapsaTodos();

        void  Smooth(int iter, double fac, int shr, int Gr);
        long  RemoveFreeNodes();
        long  RemoveGroup(long gr);
        void  SwapGroup(int num);

        acLimit3 Box()                  { return Coord->Box(); }
        void  TestMesh(long &nod, long &nel);
        void  PegaNodos(double tol);
        void  AddCapa(long gr1, long gr2);
        long  SwapAll();
        void  SetFactorH  (double fact) { Factor    = fact; }
        void  SetCotaColap(double cota) { CotaColap = cota; }
        void  SetCotaCopla(double cota) { CotaCopla = cota; }
        void  chori(SurfTriang *el);
        void  Information(long *nod, long *nel, long *grp);
        double GetFactorH  () { return Factor; }
        void  SelectElement(long *elem, int seltype, int inttype, int add, int g,
              double x,double y,double z,double nx,double ny,double nz, double r);
        void  SetGroup(long *elem, int gr);
        void  Transform(double tx, double ty, double tz,
                        double sx, double sy, double sz,
                        double rx, double ry, double rz);

    protected:
        SurfTriang *Colapsa(SurfTriang *el);
        void  RemoveTresTri();
        void  RemoveDos(SurfTriang *el, int ind);
        void  DivideElement(SurfTriang *el,int ind, long n);
        void  Obtuso(SurfTriang *El, acPoint3 &pos, double &tam, int &ind, double cota);
        void  CalNewNode(SurfTriang *El, acPoint3 &pos, double &tam, int &ind);
        void  RotaElem(SurfTriang *el, int ind);
        double CalDiamEl(SurfTriang *el);
        acPoint3 CalCentElem(SurfTriang *el);
        int   Coplanar(SurfTriang *el, int ind);
        int   Conviene(SurfTriang *el, SurfTriang *elv);
        int   SwapDiag(SurfTriang *el, SurfTriang *elv);
        int   FronteraIzq(SurfTriang *el, int ind);
        int   FronteraDer(SurfTriang *el, int ind);
        int   TestVecIzq(long no,acPoint3 p, SurfTriang *el, int ind);
        int   TestVecDer(long no,acPoint3 p, SurfTriang *el, int ind);
        int   CambiaNodoIzq(long nn, long no, SurfTriang *el, int ind);
        int   CambiaNodoDer(long nn, long no, SurfTriang *el, int ind);
        void  Renumera();
        void  InsertFirst(SurfTriang *el);
        void  Delete(SurfTriang *el);
        void  PutNullVec(SurfTriang *el, long gr);
    };

class nodlist
    {
    struct nod 
        {
        long n;
        struct nod *next;
        }*first;
    public:
        nodlist() {first=0;}
        ~nodlist();
        int add(long n);
    };
#endif