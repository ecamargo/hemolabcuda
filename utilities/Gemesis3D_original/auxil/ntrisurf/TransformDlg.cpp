// TransformDlg.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "TransformDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TransformDlg dialog


TransformDlg::TransformDlg(CWnd* pParent /*=NULL*/)
	: CDialog(TransformDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(TransformDlg)
	m_tx = 0.0;
	m_ty = 0.0;
	m_tz = 0.0;
	m_sx = 0.0;
	m_sy = 0.0;
	m_sz = 0.0;
	m_rx = 0.0;
	m_ry = 0.0;
	m_rz = 0.0;
	//}}AFX_DATA_INIT
}


void TransformDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TransformDlg)
	DDX_Text(pDX, IDC_EDIT1, m_tx);
	DDX_Text(pDX, IDC_EDIT2, m_ty);
	DDX_Text(pDX, IDC_EDIT4, m_tz);
	DDX_Text(pDX, IDC_EDIT5, m_sx);
	DDX_Text(pDX, IDC_EDIT6, m_sy);
	DDX_Text(pDX, IDC_EDIT7, m_sz);
	DDX_Text(pDX, IDC_EDIT8, m_rx);
	DDV_MinMaxDouble(pDX, m_rx, -180., 180.);
	DDX_Text(pDX, IDC_EDIT9, m_ry);
	DDV_MinMaxDouble(pDX, m_ry, -180., 180.);
	DDX_Text(pDX, IDC_EDIT10, m_rz);
	DDV_MinMaxDouble(pDX, m_rz, -180., 180.);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TransformDlg, CDialog)
	//{{AFX_MSG_MAP(TransformDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TransformDlg message handlers
