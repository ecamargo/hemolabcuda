#include "stdafx.h"
#include "general.h"
#include <string.h>

int FindKeyWord(char *string, FILE *fp)
    {
    char buffer[101], *pt;
    int vartudo, achou, cont;
    int tam;

    vartudo = achou = 0;
    cont = 1;
    tam = strlen(string);

    while (cont)
        {
        pt = fgets(buffer, 101, fp);   /* le uma linha */

        /*
        ** Verifica se chegou ao final do arquivo.
        ** Se for a primeira vez, reposiciona o arquivo no inicio.
        ** Senao, erro: Nao achou a string.
        */
        if (pt == NULL)
            if (vartudo)
                {
                rewind(fp);
                return(0);
                }
            else
                {
                vartudo = 1;
                rewind(fp);
                }

            /*
            ** Pesquisa para ver se e' candidato.
            */
        else if (*pt++ == '*')    /* e' candidato */
            {
            if (strncmp(pt, "END", 3) == 0)
                if (vartudo)
                    {
                    rewind(fp);
                    return(0);
                    }
                else
                    {
                    vartudo = 1;
                    rewind(fp);
                    }
                    
            else if (strncmp(pt, string, tam) == 0)
                {
                cont = 0;
                achou = 1;
                }
            }
        }

    return(achou);
    }

void Error(int icod, int numerr, char *msg, ...)
    {
    static char buf[201];
    va_list argptr;
    int res;

    va_start(argptr, msg);
    vsprintf(buf, msg, argptr);
    va_end(argptr);

    switch (icod)
        {
        case WARNING :      /* warning */
            res = MessageBox (0, buf, "WARNING", MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                exit(0);
            break;

        case COMMON_ERROR :      /* erro, porem o programa continua */
            res = MessageBox (0, buf, "ERROR", MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                exit(0);
            break;

        case FATAL_ERROR :      /* parada do programa */
            MessageBox(0, buf, "FATAL ERROR", MB_OK | MB_TASKMODAL);

        default:
            break;
    }
    if (icod == FATAL_ERROR)
    exit(0);
    }

