//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by trisurf.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TRISURTYPE                  129
#define IDD_INPUTDOUBLE                 131
#define IDD_INPUTTEXT                   132
#define IDD_GEOELEMSEL                  134
#define IDD_SMOOTHDLG                   135
#define IDD_MERGE                       136
#define IDD_TRANSFORM                   137
#define IDC_RADIO_COMPLETELY_INSIDE     1000
#define IDC_EDIT1                       1001
#define IDC_RADIO_INTERSECTING          1001
#define IDC_EDIT2                       1002
#define IDC_RADIO_ALL                   1002
#define IDC_RADIO_HALF_SPACE            1003
#define IDC_EDIT4                       1003
#define IDC_RADIO_BOX                   1004
#define IDC_EDIT5                       1004
#define IDC_RADIO_GROUP                 1004
#define IDC_RADIO_SPHERE                1005
#define IDC_EDIT6                       1005
#define IDC_RADIO_CYLINDER              1006
#define IDC_EDIT7                       1006
#define IDC_REMOVE                      1007
#define IDC_EDIT8                       1007
#define IDC_ADD                         1008
#define IDC_EDIT9                       1008
#define IDC_EDIT_X1                     1009
#define IDC_EDIT10                      1009
#define IDC_EDIT_Y1                     1010
#define IDC_EDIT_Z1                     1011
#define IDC_ADDSEL                      1012
#define IDC_EDIT3                       1013
#define IDC_TOTNEL                      1014
#define IDC_EDIT_X2                     1015
#define IDC_RADIO1                      1015
#define IDC_EDIT_Y2                     1016
#define IDC_RADIO2                      1016
#define IDC_EDIT_Z2                     1017
#define IDC_EDIT_RADIUS                 1018
#define IDC_SELNEL                      1019
#define IDC_G                           1026
#define IDC_CHECK1                      1027
#define ID_FILE_OPEN_SUR                32771
#define ID_FILE_OPEN_PAR                32772
#define ID_FILE_EXPORT                  32773
#define ID_MODIFY_CREATE                32774
#define ID_MODIFY_MERGE                 32775
#define ID_MODIFY_SWAPP                 32776
#define ID_MODIFY_INSERT                32777
#define ID_MODIFY_COLLAPSE              32778
#define ID_MODIFY_SMOOTH                32779
#define ID_MODIFY_GROUPOR               32780
#define ID_MODIFY_DIVIDEOB              32781
#define ID_MODIFY_FREENODES             32782
#define ID_MODIFY_COPYGROUP             32783
#define ID_INFORM_TEST                  32784
#define ID_INFORM_INFO                  32785
#define ID_VIEW_PLOT                    32786
#define ID_BUTTON32792                  32792
#define ID_MODIFY_ELSEL                 32797
#define ID_FILE_IMPORT                  32798
#define ID_MODIFY_TRANSFORM             32799
#define ID_MODIFY_REMOVE_GROUP          32800
#define ID_FILE_EDGES                   32801

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32802
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
