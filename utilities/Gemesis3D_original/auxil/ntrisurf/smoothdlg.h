#if !defined(AFX_SMOOTHDLG_H__66A9F7E0_A8AD_11D3_9741_004F49040760__INCLUDED_)
#define AFX_SMOOTHDLG_H__66A9F7E0_A8AD_11D3_9741_004F49040760__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// smoothdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// smoothdlg dialog

class smoothdlg : public CDialog
{
// Construction
public:
	smoothdlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(smoothdlg)
	enum { IDD = IDD_SMOOTHDLG };
	int		m_iter;
	double	m_fac;
	int		m_gr;
    int     maxgr;
	BOOL	m_shrink;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(smoothdlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(smoothdlg)
	afx_msg void Onshrink();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMOOTHDLG_H__66A9F7E0_A8AD_11D3_9741_004F49040760__INCLUDED_)
