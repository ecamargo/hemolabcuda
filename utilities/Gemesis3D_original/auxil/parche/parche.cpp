#include "acdp.h"
#include "mesh3d.h"

void Parche(acCoordinate3D *);
void Print(FILE *fp1, Mesh3d *mesh);

int main()
    {

    StartACDP(0, "parche.sai", "trace.txt");
    InitDB();
    TraceOn("Parche");

    Mesh3d *mesh;

    //Apertura del archivo con la malla
    char name[256];
    printf("File_name with mesh: ");
    scanf ("%s", name);
    FILE *fp1= fopen(name, "r");
    if (!fp1)
        {
        printf("ERROR: Cannot open file");
        return(0);
        }

	mesh = new Mesh3d;
	mesh->Read(fp1);         // leitura de mesh
    fclose(fp1);

    printf("%ld elementos\n%ld nodos ",mesh->GetNumEls(),mesh->GetNumNodes());
    //Aca viene el parche
//    acCoordinate3D *Coord = mesh->GetCoords();
//    Parche(Coord);

    //Se salva la nueva malla
    printf("File_name to store new mesh: ");
    scanf ("%s", name);
    fp1= fopen(name, "w");
    Print(fp1,mesh);
    fclose(fp1);

	return (0);
	}

void Parche(acCoordinate3D *Coord)
    {
    acCoordinate3D &coord=*Coord;
    acPoint3 p;

    for (long n=0; n< Coord->GetNumNodes(); n++)
        {
        p = coord[n];
        p.z = p.z/4;
        coord[n] = p;
        }
    }
// Imprime la malla de volumen en el formato de sven
/*
void Print(FILE *fp, Mesh3d *mesh)
    {
    acCoordinate3D &Coord = *mesh->GetCoords();
    acPoint3 p;
    Element *el;
    long n1,n2,n3,n4,nel=1;
    fprintf(fp,"Elements : %ld\n",mesh->GetNumEls());
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        n1=el->GetNode(0)+1;
        n2=el->GetNode(1)+1;
        n3=el->GetNode(2)+1;
        n4=el->GetNode(3)+1;
        fprintf(fp,"%ld %ld %ld %ld %ld\n",nel++,n1,n2,n3,n4);
        }
    fprintf(fp,"\nNodes : %ld\n",mesh->GetNumNodes());
    for (long n=0; n<mesh->GetNumNodes(); n++)
        {
        p=Coord[n];
        fprintf(fp,"%ld %lg %lg %lg\n",n+1,p.x,p.y,p.z);
        }
    }
*/
/*
void Print(FILE *fp, Mesh3d *mesh)
    {
    acCoordinate3D &Coord = *mesh->GetCoords();
    long *node=(long *)mMalloc(sizeof(long)*mesh->GetNumNodes());
    Element *el;
    long n1,n2,n3;
    el = mesh->SetFirstElem();
    for (int g=0; g<mesh->GetNumGroups(); g++)
        {
        for (long e=0; e<mesh->GetNumElsGrp(g); e++)
	        {
            n1=el->GetNode(0);
            n2=el->GetNode(1);
            n3=el->GetNode(2);
            node[n1]=node[n2]=node[n3]=g;
            el = mesh->SetNextElem();
            }
        }
    long n,nodi=0,nodo=0,nodw=0, nodto=0;
    for (n=0; n<mesh->GetNumNodes(); n++)
        if      (node[n]==0) nodi++;
        else if (node[n]==1) 
            {
            acPoint3 p=Coord[n];
            if (p.z > 0.88) nodto++;
            else            nodo++;
            }
        else                 nodw++;


    fprintf(fp,"\nInlet nodes : %ld\n",nodi);
    for (n=0; n<mesh->GetNumNodes(); n++)
        if (node[n]==0)
            fprintf(fp,"%ld\n",n+1);
    fprintf(fp,"\nTop Outlet nodes : %ld\n",nodto);
    for (n=0; n<mesh->GetNumNodes(); n++)
        if (node[n]==1)
            {
            acPoint3 p=Coord[n];
            if (p.z > 0.88) fprintf(fp,"%ld\n",n+1);
            }
    fprintf(fp,"\nOther Outlet nodes : %ld\n",nodo);
    for (n=0; n<mesh->GetNumNodes(); n++)
        if (node[n]==1)
            {
            acPoint3 p=Coord[n];
            if (!(p.z > 0.88)) fprintf(fp,"%ld\n",n+1);
            }
    fprintf(fp,"\nWall nodes : %ld\n",nodw);
    for (n=0; n<mesh->GetNumNodes(); n++)
        if (node[n]==2)
            fprintf(fp,"%ld\n",n+1);

    }
*/
// Separa elementos en una superficie segun consideraciones geometricas
void Print(FILE *fp, Mesh3d *mesh)
    {
    acCoordinate3D &Coord = *mesh->GetCoords();
    acPoint3 p;
    long np=0;

    for (long n=0; n<Coord.GetNumNodes(); n++)
        {
        p=Coord[n];
        double r=p.y*p.y+p.z*p.z;
        if (p.x>0.19995 && r<0.00491*0.00491)
            { fprintf(fp," %ld\n",n+1); np++; }
        }
    fprintf(fp,"\nNum: %ld",np);
/*
    acPoint3 p1,p2,p3,p4;
    Element *el;
    long n1,n2,n3,n4,nel1=0,nel2=0,nel3=0;
//    fprintf(fp,"\n*INCIDENCE\n");
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        n1=el->GetNode(0);p1=Coord[n1];
        n2=el->GetNode(1);p2=Coord[n2];
        n3=el->GetNode(2);p3=Coord[n3];
        n4=el->GetNode(3);p4=Coord[n4];
        
        if (!(p1.z<0.001 && p2.z<0.001 && p3.z<0.001))
            if (!(p1.z>=0.9065 && p2.z>=0.9065 && p3.z>=0.9065))
            {
            nel1++;
            fprintf(fp,"%ld %ld %ld\n",n1+1,n2+1,n3+1);
            }
        }
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        n1=el->GetNode(0);p1=Coord[n1];
        n2=el->GetNode(1);p2=Coord[n2];
        n3=el->GetNode(2);p3=Coord[n3];
        
        if (p1.z<0.001 && p2.z<0.001 && p3.z<0.001)
            {
            nel2++;
            fprintf(fp," %ld %ld %ld\n",n1+1,n2+1,n3+1);
            }
        }
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        n1=el->GetNode(0);p1=Coord[n1];
        n2=el->GetNode(1);p2=Coord[n2];
        n3=el->GetNode(2);p3=Coord[n3];
        
        if ((p1.z>=0.9065 && p2.z>=0.9065 && p3.z>=0.9065))
            {
            nel3++;
            fprintf(fp," %ld %ld %ld\n",n1+1,n2+1,n3+1);
            }
        }
    fprintf(fp,"\n*ELEMENT GROUPS\n 3\n 1 %ld Tri3\n 2 %ld Tri3\n 3 %ld Tri3\n",nel1,nel2,nel3);

//    fprintf(fp,"\n*COORDINATES\n %ld\n",nod);
    Coord.Print(fp);
    fprintf(fp,"*END");
*/
    }

/*
void Print(FILE *fp, Mesh3d *mesh)
    {
    Element *el;
    fprintf(fp,"\n*ELEMENT GROUPS\n %ld\n",mesh->GetNumGroups());

    for (int n=0; n<mesh->GetNumGroups(); n++)
        {
        el = mesh->GetElement(n);
        if (el->NNoel() == 4)
            fprintf(fp," %ld %ld Tetra4\n",n+1,mesh->GetNumElsGrp(n));
        else if (el->NNoel() == 3)
            fprintf(fp," %ld %ld Tri3\n",n+1,mesh->GetNumElsGrp(n));
        }


    fprintf(fp,"\n*INCIDENCE\n");

    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        for (int j=0; j<el->NNoel(); j++)
            fprintf(fp," %ld",el->GetNode(j)+1);
        fprintf(fp,"\n");
        }
    acCoordinate3D *Coord = mesh->GetCoords();
    Coord->Print(fp);
    }
*/