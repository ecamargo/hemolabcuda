// GeomElemSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "select.h"
#include "GeomElemSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GeomElemSelDlg dialog


GeomElemSelDlg::GeomElemSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GeomElemSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GeomElemSelDlg)
	m_x1 = 0.0;
	m_x2 = 0.0;
	m_y1 = 0.0;
	m_y2 = 0.0;
	m_z1 = 0.0;
	m_z2 = 0.0;
	m_num_vis_elems = 0;
	m_radius = 0.0;
	m_tot_num_elems = 0;
	m_all = -1;
	m_inside = -1;
	//}}AFX_DATA_INIT
}

void GeomElemSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GeomElemSelDlg)
	DDX_Text(pDX, IDC_EDIT_X1, m_x1);
	DDX_Text(pDX, IDC_EDIT_X2, m_x2);
	DDX_Text(pDX, IDC_EDIT_Y1, m_y1);
	DDX_Text(pDX, IDC_EDIT_Y2, m_y2);
	DDX_Text(pDX, IDC_EDIT_Z1, m_z1);
	DDX_Text(pDX, IDC_EDIT_Z2, m_z2);
	DDX_Text(pDX, IDC_EDIT_NUM_VIS_ELEMS, m_num_vis_elems);
	DDX_Text(pDX, IDC_EDIT_RADIUS, m_radius);
	DDX_Text(pDX, IDC_EDIT_TOT_NUM_ELEMS, m_tot_num_elems);
	DDX_Radio(pDX, IDC_RADIO_ALL, m_all);
	DDX_Radio(pDX, IDC_RADIO_COMPLETELY_INSIDE, m_inside);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GeomElemSelDlg, CDialog)
	//{{AFX_MSG_MAP(GeomElemSelDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GeomElemSelDlg message handlers

void GeomElemSelDlg::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
    UpdateData(true);

    switch(m_all)
		{
	    case 0:     //ALL
		    {
		    for (long i=0; i<m_tot_num_elems; i++)
		        Sel[i] = 1;
		    m_num_vis_elems = m_tot_num_elems;
		    break;
		    }
	    case 1:     //HALF_SPACE
		    {
		    //m_num_vis_elems = graph->SelHalfSpace(ElSw, point1, point2, 1, m_inside);
		    break;
		    }
	    case 2:     //BOX
	    	{
		    //m_num_vis_elems = graph->SelBox(ElSw, point1, point2, 1, m_inside);
		    break;
		    }
	    case 3:     //SPHERE
		    {
		    //m_num_vis_elems = graph->SelSphere(ElSw, point1, m_radius, 1, m_inside);
		    break;
		    }
	    case 4:     //CYLINDER:
		    {
		    //m_num_vis_elems = graph->SelCylinder(ElSw, point1, point2, m_radius, 1, m_inside);
	    	break;
		    }
        default:
            break;
		}
    UpdateData(false);
	
}

void GeomElemSelDlg::OnButtonRemove() 
{
	// TODO: Add your control notification handler code here
    UpdateData(true);

    switch(m_all)
		{
	    case 0:     //ALL
		    {
		    for (long i=0; i<m_tot_num_elems; i++)
		        Sel[i] = 0;
		    m_num_vis_elems = 0;
		    break;
		    }
	    case 1:     //HALF_SPACE
		    {
		    //m_num_vis_elems = graph->SelHalfSpace(ElSw, point1, point2, 0, m_inside);
		    break;
		    }
	    case 2:     //BOX
	    	{
		    //m_num_vis_elems = graph->SelBox(ElSw, point1, point2, 0, m_inside);
		    break;
		    }
	    case 3:     //SPHERE
		    {
		    //m_num_vis_elems = graph->SelSphere(ElSw, point1, m_radius, 0, m_inside);
		    break;
		    }
	    case 4:     //CYLINDER:
		    {
		    //m_num_vis_elems = graph->SelCylinder(ElSw, point1, point2, m_radius, 0, m_inside);
	    	break;
		    }
        default:
            break;
		}
    UpdateData(false);
	
}
