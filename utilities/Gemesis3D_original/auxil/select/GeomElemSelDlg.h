#if !defined(AFX_GEOMELEMSELDLG_H__BD1E8098_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
#define AFX_GEOMELEMSELDLG_H__BD1E8098_5BD9_11D3_AA65_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GeomElemSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GeomElemSelDlg dialog

class GeomElemSelDlg : public CDialog
{
// Construction
public:
	GeomElemSelDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GeomElemSelDlg)
	enum { IDD = IDD_DLG_GEOM_ELEM_SEL };
	double	m_x1;
	double	m_x2;
	double	m_y1;
	double	m_y2;
	double	m_z1;
	double	m_z2;
	long	m_num_vis_elems;
	double	m_radius;
	long	m_tot_num_elems;
	int		m_all;
	int		m_inside;
	//}}AFX_DATA
    int *Sel;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GeomElemSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GeomElemSelDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GEOMELEMSELDLG_H__BD1E8098_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
