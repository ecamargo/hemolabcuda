//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by select.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SELECTTYPE                  129
#define IDD_DLG_GEOM_ELEM_SEL           130
#define IDC_RADIO_COMPLETELY_INSIDE     1000
#define IDC_RADIO_INTERSECTING          1001
#define IDC_RADIO_ALL                   1002
#define IDC_RADIO_HALF_SPACE            1003
#define IDC_RADIO_BOX                   1004
#define IDC_RADIO_SPHERE                1005
#define IDC_RADIO_CYLINDER              1006
#define IDC_BUTTON_ADD                  1007
#define IDC_BUTTON_REMOVE               1008
#define IDC_EDIT_X1                     1009
#define IDC_EDIT_Y1                     1010
#define IDC_EDIT_Z1                     1011
#define IDC_EDIT_X2                     1015
#define IDC_EDIT_Y2                     1016
#define IDC_EDIT_Z2                     1017
#define IDC_EDIT_RADIUS                 1018
#define IDC_EDIT_TOT_NUM_ELEMS          1019
#define IDC_EDIT_NUM_VIS_ELEMS          1020
#define ID_SELECT_GEOM                  32771
#define ID_SELECT_LOGIC                 32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
