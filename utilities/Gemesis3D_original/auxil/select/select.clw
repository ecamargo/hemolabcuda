; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=GeomElemSelDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "select.h"
LastPage=0

ClassCount=6
Class1=CSelectApp
Class2=CSelectDoc
Class3=CSelectView
Class4=CMainFrame

ResourceCount=3
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Class5=CAboutDlg
Resource3=IDD_DLG_GEOM_ELEM_SEL
Class6=GeomElemSelDlg

[CLS:CSelectApp]
Type=0
HeaderFile=select.h
ImplementationFile=select.cpp
Filter=N

[CLS:CSelectDoc]
Type=0
HeaderFile=selectDoc.h
ImplementationFile=selectDoc.cpp
Filter=N

[CLS:CSelectView]
Type=0
HeaderFile=selectView.h
ImplementationFile=selectView.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=CSelectView

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T



[CLS:CAboutDlg]
Type=0
HeaderFile=select.cpp
ImplementationFile=select.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
Command17=ID_SELECT_GEOM
Command18=ID_SELECT_LOGIC
CommandCount=18

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[DLG:IDD_DLG_GEOM_ELEM_SEL]
Type=1
Class=GeomElemSelDlg
ControlCount=31
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_RADIO_COMPLETELY_INSIDE,button,1342373897
Control4=IDC_RADIO_INTERSECTING,button,1342242825
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,button,1342177287
Control7=IDC_RADIO_ALL,button,1342373897
Control8=IDC_RADIO_HALF_SPACE,button,1342242825
Control9=IDC_RADIO_BOX,button,1342242825
Control10=IDC_RADIO_SPHERE,button,1342242825
Control11=IDC_RADIO_CYLINDER,button,1342242825
Control12=IDC_BUTTON_ADD,button,1342242816
Control13=IDC_BUTTON_REMOVE,button,1342242816
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_X1,edit,1350631552
Control18=IDC_EDIT_Y1,edit,1350631552
Control19=IDC_EDIT_Z1,edit,1350631552
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_EDIT_X2,edit,1350631552
Control24=IDC_EDIT_Y2,edit,1350631552
Control25=IDC_EDIT_Z2,edit,1350631552
Control26=IDC_STATIC,static,1342308352
Control27=IDC_EDIT_RADIUS,edit,1350631552
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_EDIT_TOT_NUM_ELEMS,edit,1350633600
Control31=IDC_EDIT_NUM_VIS_ELEMS,edit,1350633600

[CLS:GeomElemSelDlg]
Type=0
HeaderFile=GeomElemSelDlg.h
ImplementationFile=GeomElemSelDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_EDIT_X1
VirtualFilter=dWC

