// select.h : main header file for the SELECT application
//

#if !defined(AFX_SELECT_H__BD1E8088_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
#define AFX_SELECT_H__BD1E8088_5BD9_11D3_AA65_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSelectApp:
// See select.cpp for the implementation of this class
//

class CSelectApp : public CWinApp
{
public:
	CSelectApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSelectApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECT_H__BD1E8088_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
