// selectDoc.cpp : implementation of the CSelectDoc class
//

#include "stdafx.h"
#include "select.h"

#include "selectDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDoc

IMPLEMENT_DYNCREATE(CSelectDoc, CDocument)

BEGIN_MESSAGE_MAP(CSelectDoc, CDocument)
	//{{AFX_MSG_MAP(CSelectDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDoc construction/destruction

CSelectDoc::CSelectDoc()
{
	// TODO: add one-time construction code here

}

CSelectDoc::~CSelectDoc()
{
}

BOOL CSelectDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSelectDoc serialization

void CSelectDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSelectDoc diagnostics

#ifdef _DEBUG
void CSelectDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSelectDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSelectDoc commands
