// selectDoc.h : interface of the CSelectDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SELECTDOC_H__BD1E808E_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
#define AFX_SELECTDOC_H__BD1E808E_5BD9_11D3_AA65_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CSelectDoc : public CDocument
{
protected: // create from serialization only
	CSelectDoc();
	DECLARE_DYNCREATE(CSelectDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSelectDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSelectDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTDOC_H__BD1E808E_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
