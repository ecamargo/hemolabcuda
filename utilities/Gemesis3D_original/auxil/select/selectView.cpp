// selectView.cpp : implementation of the CSelectView class
//

#include "stdafx.h"
#include "select.h"

#include "selectDoc.h"
#include "selectView.h"
#include "GeomElemSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectView

IMPLEMENT_DYNCREATE(CSelectView, CView)

BEGIN_MESSAGE_MAP(CSelectView, CView)
	//{{AFX_MSG_MAP(CSelectView)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_SELECT_GEOM, OnSelectGeom)
	ON_UPDATE_COMMAND_UI(ID_SELECT_GEOM, OnUpdateSelectGeom)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectView construction/destruction

CSelectView::CSelectView()
{
	// TODO: add construction code here

}

CSelectView::~CSelectView()
{
}

BOOL CSelectView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSelectView drawing

void CSelectView::OnDraw(CDC* pDC)
{
	CSelectDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSelectView printing

BOOL CSelectView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSelectView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSelectView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSelectView diagnostics

#ifdef _DEBUG
void CSelectView::AssertValid() const
{
	CView::AssertValid();
}

void CSelectView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSelectDoc* CSelectView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSelectDoc)));
	return (CSelectDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSelectView message handlers

void CSelectView::OnFileOpen() 
{
	// TODO: Add your command handler code here
    char name[255];

    CFileDialog dlg(TRUE, "sur", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        strcpy(name, dlg.GetPathName());
        FILE *fp= fopen(name,"r");
        if (fp)
            {
            mesh = new Mesh3d;
            mesh->Read(fp);
            sel = (int *)malloc(sizeof(int)*mesh->GetNumEls());
            for (long i=0; i<mesh->GetNumEls(); i++)
                sel[i]=1;
            fclose(fp);
            }
//        Invalidate();
        }
}

void CSelectView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here

}

void CSelectView::OnFileSave() 
{
	// TODO: Add your command handler code here
    char name[255];

    CFileDialog dlg(FALSE, "sur", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        strcpy(name, dlg.GetPathName());
        FILE *fp= fopen(name,"w");
        //mesh.Read(fp);
        fclose(fp);
        }
}

void CSelectView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(mesh!=0);    	
	
}

void CSelectView::OnSelectGeom() 
{
	// TODO: Add your command handler code here
    static acPoint3 point1(0.0,0.0,0.0);
    static acPoint3 point2(0.0,0.0,0.0);
    static double radius=1.0;
    static int flgsel = 0;
    static int flgint = 1;
    static long nel;

	nel = 0;
	for (long i=0; i<mesh->GetNumEls(); i++)
	    if (sel[i]) nel++;

	GeomElemSelDlg dlg;

    dlg.Sel  = sel;
    dlg.m_x1 = point1.x;
    dlg.m_x2 = point2.x;
    dlg.m_y1 = point1.y;
    dlg.m_y2 = point2.y;
    dlg.m_z1 = point1.z;
    dlg.m_z2 = point2.z;
    dlg.m_radius = radius;
    dlg.m_all = flgsel;
    dlg.m_inside = flgint;
    dlg.m_tot_num_elems = mesh->GetNumEls();
    dlg.m_num_vis_elems = nel;

    dlg.DoModal();

    point1.x = dlg.m_x1;
    point2.x = dlg.m_x2;
    point1.y = dlg.m_y1;
    point2.y = dlg.m_y2;
    point1.z = dlg.m_z1;
    point2.z = dlg.m_z2;
    radius   = dlg.m_radius;
    flgsel   = dlg.m_all;
    flgint   = dlg.m_inside;

}

void CSelectView::OnUpdateSelectGeom(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(mesh!=0);    	
	
}
