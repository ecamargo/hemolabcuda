// selectView.h : interface of the CSelectView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SELECTVIEW_H__BD1E8090_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
#define AFX_SELECTVIEW_H__BD1E8090_5BD9_11D3_AA65_0060979BBB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "mesh3d.h"

class CSelectView : public CView
{
protected: // create from serialization only
	CSelectView();
	DECLARE_DYNCREATE(CSelectView)

// Attributes
public:
	CSelectDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSelectView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
    Mesh3d *mesh;
    int *sel;

// Generated message map functions
protected:
	//{{AFX_MSG(CSelectView)
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnSelectGeom();
	afx_msg void OnUpdateSelectGeom(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in selectView.cpp
inline CSelectDoc* CSelectView::GetDocument()
   { return (CSelectDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTVIEW_H__BD1E8090_5BD9_11D3_AA65_0060979BBB38__INCLUDED_)
