#include "acdp.h"
#include "mesh3d.h"
#include <math.h>

void Print(FILE *fp1, Mesh3d *mesh);
void SmoothSurf(Mesh3d *mesh);
//int resuelve(double K[][4], double *FX, double *FY, double *FZ);

int main()
    {

    StartACDP(0, "acdp.sai", "trace.txt");
    InitDB();
    TraceOn("Main");

    Mesh3d *mesh;

    //Apertura del archivo con la malla
    char name[256];
    printf("\nFile_name with surface mesh: ");
    scanf ("%s", name);
    FILE *fp1= fopen(name, "r");
    if (!fp1)
        {
        printf("ERROR: Cannot open file");
        return(0);
        }

	mesh = new Mesh3d;
	mesh->Read(fp1);         // leitura de mesh
    fclose(fp1);

    printf("%ld elements\n%ld nodes \n\n",mesh->GetNumEls(),mesh->GetNumNodes());
    //Aca viene el smooth
    SmoothSurf(mesh);

    //Se salva la nueva malla
    printf("File_name to store new mesh: ");
    scanf ("%s", name);
    fp1= fopen(name, "w");
    Print(fp1,mesh);
    fclose(fp1);

    TraceOff("Main");
	return (0);
	}

void Print(FILE *fp, Mesh3d *mesh)
    {
    Element *el;
    long n1,n2,n3;
    fprintf(fp,"\n*ELEMENT GROUPS\n %d\n",mesh->GetNumGroups());
    for (int g=0; g<mesh->GetNumGroups() ; g++)
        fprintf(fp,"%d %ld Tri3\n",g+1,mesh->GetNumElsGrp(g));
    
    fprintf(fp,"\n*INCIDENCE\n");
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem())
	    {
        n1=el->GetNode(0);
        n2=el->GetNode(1);
        n3=el->GetNode(2);
        fprintf(fp,"%ld %ld %ld\n",n1+1,n2+1,n3+1);
        }
    acCoordinate3D &Coord = *mesh->GetCoords();
    Coord.Print(fp);
    fprintf(fp,"*END");
    }

void SmoothSurf(Mesh3d *mesh)
    {
    long n;
    long *node=(long *)mMalloc(sizeof(long)*mesh->GetNumNodes());
    for (n=0; n<mesh->GetNumNodes(); n++)
        node[n]=-1;
    Element *el;
    el = mesh->SetFirstElem();
    for (int g=0; g<mesh->GetNumGroups(); g++)
        {
        for (long e=0; e<mesh->GetNumElsGrp(g); e++)
	        {
            for (int i=0; i<3; i++)
                {
                n=el->GetNode(i);
                if      (node[n]==-1)  node[n]=g+1;
                else if (node[n]!=g+1) node[n]=0;
                }
            el = mesh->SetNextElem();
            }
        }

    acCoordinate3D &Coord = *mesh->GetCoords();
	SparseNoEl  *NoEl = mesh->NewNoEl();
    acPoint3 p1,p2,p3,p;
    for (n=0; n<mesh->GetNumNodes(); n++)
        {
        if (!node[n]) continue;
        p.x=p.y=p.z=0.0;
        for (long ie=0; ie<NoEl->GetNelems(n); ie++)
            {
            long e = NoEl->GetElem(n,ie);
	        el= mesh->SetAnElem(e);
            p1=Coord[el->GetNode(0)];
            p2=Coord[el->GetNode(1)];
            p3=Coord[el->GetNode(2)];
            p.x=p.x+(p1.x+p2.x+p3.x);
            p.y=p.y+(p1.y+p2.y+p3.y);
            p.z=p.z+(p1.z+p2.z+p3.z);
            }
        p.x=p.x/(NoEl->GetNelems(n)*3);
        p.y=p.y/(NoEl->GetNelems(n)*3);
        p.z=p.z/(NoEl->GetNelems(n)*3);
        Coord[n]=p;
        }
    }
/*
void SmoothSurf(Mesh3d *mesh)
    {
    long n,n1,n2,n3;
    long *node=(long *)mMalloc(sizeof(long)*mesh->GetNumNodes());
    for (n=0; n<mesh->GetNumNodes(); n++)
        node[n]=-1;
    Element *el;
    el = mesh->SetFirstElem();
    for (int g=0; g<mesh->GetNumGroups(); g++)
        {
        for (long e=0; e<mesh->GetNumElsGrp(g); e++)
	        {
            for (int i=0; i<3; i++)
                {
                n=el->GetNode(i);
                if      (node[n]==-1)  node[n]=g+1;
                else if (node[n]!=g+1) node[n]=0;
                }
            el = mesh->SetNextElem();
            }
        }

    acCoordinate3D &Coord = *mesh->GetCoords();
	SparseNoEl  *NoEl = mesh->NewNoEl();
    acPoint3 p1,p2,p3,p,pn;
    double K[4][4],FX[4],FY[4],FZ[4];
    static double ac[3][3]={ {0.5,0.5,0.0}, {0.0,0.5,0.5}, {0.5,0.0,0.5} };
    double peso=1.0/3.0;
    double area,x,y,z;
    int i,j;
    for (n=0; n<mesh->GetNumNodes(); n++)
        {
        if (!node[n]) continue;

        for (i=0; i<4; i++)
            {
            FX[i]=FY[i]=FZ[i]=0.0;
            for (j=0; j<4; j++)
                K[i][j]=0.0;
            }

        for (long ie=0; ie<NoEl->GetNelems(n); ie++)
            {
            long e = NoEl->GetElem(n,ie);
	        el= mesh->SetAnElem(e);
            p1=Coord[el->GetNode(0)];
            p2=Coord[el->GetNode(1)];
            p3=Coord[el->GetNode(2)];

            area=peso*el->Area();
            for (i=0; i<3; i++)
                {
                x=ac[i][0]*p1.x+ac[i][1]*p2.x+ac[i][2]*p3.x;
                y=ac[i][0]*p1.y+ac[i][1]*p2.y+ac[i][2]*p3.y;
                z=ac[i][0]*p1.z+ac[i][1]*p2.z+ac[i][2]*p3.z;
                K[0][0]+=1*area;
                K[0][1]+=x*area;
                K[0][2]+=y*area;
                K[0][3]+=z*area;

                K[1][1]+=x*x*area;
                K[1][2]+=x*y*area;
                K[1][3]+=x*z*area;

                K[2][2]+=y*y*area;
                K[2][3]+=y*z*area;

                K[3][3]+=z*z*area;

                FX[0]+=  x*area;
                FX[1]+=x*x*area;
                FX[2]+=y*x*area;
                FX[3]+=z*x*area;

                FY[0]+=  y*area;
                FY[1]+=x*y*area;
                FY[2]+=y*y*area;
                FY[3]+=z*y*area;

                FZ[0]+=  z*area;
                FZ[1]+=x*z*area;
                FZ[2]+=y*z*area;
                FZ[3]+=z*z*area;
                }
            }
        K[1][0]=K[0][1];
        K[2][0]=K[0][2];
        K[2][1]=K[1][2];
        K[3][0]=K[0][3];
        K[3][1]=K[1][3];
        K[3][2]=K[2][3];
        if (!resuelve(K,FX,FY,FZ))
            {
            printf("Error: Sistema singular\n");
            exit(1);
            }
        p = Coord[n];
        pn.x = FX[0]+FX[1]*p.x+FX[2]*p.y+FX[3]*p.z;
        pn.y = FY[0]+FY[1]*p.x+FY[2]*p.y+FY[3]*p.z;
        pn.z = FZ[0]+FZ[1]*p.x+FZ[2]*p.y+FZ[3]*p.z;
        Coord[n]=pn;
        }
    }
int resuelve(double K[][4], double *FX, double *FY, double *FZ)
    {
    //Triangulariza
    int i,j,k;
    double fa;
    for (k=0; k<3; k++)
        for (i=k; i<3; i++)
            {
            if (fabs(K[i][i])<1.0e-36) return 0;
            fa = -K[i+1][i]/K[i][i];
            for (j=k; j<4; j++)
                K[i+1][j]+=K[i][j]*fa;
            FX[i+1]+=FX[i]*fa;
            FY[i+1]+=FY[i]*fa;
            FZ[i+1]+=FZ[i]*fa;
            }
    //Retrosustituye
    for (i=3; i>=0; i--)
        {
        fa=0.0;
        for (j=i+1; j<4; j++)
            fa+=K[i][j]*FX[j];
        FX[i]=(FX[i]-fa)/K[i][i];
        fa=0.0;
        for (j=i+1; j<4; j++)
            fa+=K[i][j]*FY[j];
        FY[i]=(FY[i]-fa)/K[i][i];
        fa=0.0;
        for (j=i+1; j<4; j++)
            fa+=K[i][j]*FZ[j];
        FZ[i]=(FZ[i]-fa)/K[i][i];
        }
    return 1;
    }
*/
