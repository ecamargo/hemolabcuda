#include <stdlib.h>
#include <stdio.h>

int read_header_bmp_image(FILE *fp,long &npx, long &npy)
    {
    char BM[2];
    fread(BM,sizeof(char),2,fp);
    if(!(BM[0]==66 && BM[1]==77))
        {
        printf("\nERROR: Data in file not in bmp format\n");
        return(0);
        }
    char dummy[24];
    fread(dummy,sizeof(char),16,fp);
    fread(&npx,sizeof(long),1,fp);
    fread(&npy,sizeof(long),1,fp);
    fread(dummy,sizeof(char),4,fp);
    if (dummy[2] != 24)
        {
        printf("\nERROR: This program can only handle 24 bits depth color image");
        return(0);
        }
    fread(dummy,sizeof(char),24,fp);
    return 1;
    }

int read_bmp_image(FILE *fp,long npx, long npy,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    long tamx = (npx*3/4)*4;
    if (tamx != 3*npx) tamx+=4;
    char *buf = (char *)malloc(sizeof(char)*tamx);

    unsigned char r,g,b;
    long ip=0;

    for (long iv=0; iv<npy; iv++)
        {
        for (long ih=0; ih<npx; ih++, ip++)
            {
            fread(&r,sizeof(char),1,fp);
            fread(&g,sizeof(char),1,fp);
            fread(&b,sizeof(char),1,fp);
            imr[ip]=r;
            img[ip]=g;
            imb[ip]=b;
            }
        long resto = tamx-3*npx;
        if (resto) fread(buf,sizeof(char),resto,fp);
        }
    return(1);
    }

int read_raw_image(FILE *fp,long np, long tam, long numcan, long inter,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    unsigned char dum;
    for (long i=0; i<tam; i++)
       if (!fread(&dum,sizeof(char),1,fp)) return(0);

	if (inter)
		{
	    for (i=0; i<np; i++)
		    {
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			imr[i]=dum;
			if (numcan == 1) continue;
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			img[i]=dum;
			if (numcan == 2) continue;
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			imb[i]=dum;
			}
		}
	else
		{
	    for (i=0; i<np; i++)
		    {
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			imr[i]=dum;
			}
		if (numcan == 1) return(1);
	    for (i=0; i<np; i++)
		    {
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			img[i]=dum;
			}
		if (numcan == 2) return(1);
	    for (i=0; i<np; i++)
		    {
			if (!fread(&dum,sizeof(char),1,fp)) return(0);
			imb[i]=dum;
			}
		}
	return(1);
	}

int read_txt_image(FILE *fp,long npx, long npy, long npz,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    char name[40];
    int dum;
    long np=0;
    for (long k=0; k<npz; k++)
        {
        fscanf(fp,"%s",name);
        for (long j=0; j<npy; j++)
            for (long i=0; i<npx; i++)
                {
                fscanf(fp, "%d",&dum);
                imr[np]=(unsigned char)dum;
                fscanf(fp, "%d",&dum);
                img[np]=(unsigned char)dum;
                fscanf(fp, "%d",&dum);
                imb[np]=(unsigned char)dum;
                np++;
                }
        }
    return 1;
    }

int write_bmp_image(FILE *fp,long npx, long npy, long numcan,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    unsigned char rgb[3];
    long tamx = (npx*3/4)*4;
    unsigned long aux;
    if (tamx != 3*npx) tamx+=4;

//  Escribe el header (54 bytes)
    fwrite("BM",sizeof(char),2,fp);
    aux = tamx*npy+54;
    fwrite(&aux,sizeof(long),1,fp); // tamanio del archivo
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);
    aux = 54; fwrite(&aux,sizeof(long),1,fp);
    aux = 40; fwrite(&aux,sizeof(long),1,fp);
    fwrite(&npx,sizeof(long),1,fp);   // Numero de pixels en X
    fwrite(&npy,sizeof(long),1,fp);   // Numero de pixels en Y
    rgb[0]=1;rgb[1]=0;rgb[2]=24;
    fwrite(rgb,sizeof(char),3,fp);    //Numero de bits por color
    fwrite(rgb+1,sizeof(char),1,fp);
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);
    aux = tamx*npy;
    fwrite(&aux,sizeof(long),1,fp); // tamanio de los datos
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);
    aux = 0;  fwrite(&aux,sizeof(long),1,fp);

    unsigned char *buf = (unsigned char *)malloc(tamx*sizeof(char));
    buf[tamx-4]=buf[tamx-3]=buf[tamx-2]=buf[tamx-1]=0;
    long ip=0;
    for (long iv=0; iv<npy; iv++)
        {
        for (long ih=0; ih<npx; ih++, ip++)
            {
            if (numcan ==1)
                {
                buf[3*ih]=buf[3*ih+1]=buf[3*ih+2]=imr[ip];
                }
            else if (numcan == 2)			//Asumimos que son 12 bits de informacion
                {
                int col=imr[ip]*256+img[ip];
                buf[3*ih]  =(col)/16;
                buf[3*ih+1]=(col+8)/16;
                buf[3*ih+2]=0;
                }
            else
                {
                buf[3*ih]=imb[ip];
                buf[3*ih+1]=img[ip];
                buf[3*ih+2]=imr[ip];
                }
            }
        fwrite(buf,sizeof(char),tamx,fp);
        }
    return 1;
    }

int write_txt_image(FILE *fp,long npx, long npy, long npz, long numcan,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    char name[40];
    long np=0;
    for (long k=0; k<npz; k++)
        {
        fscanf(fp,"%s",name);
        for (long j=0; j<npy; j++)
            for (long i=0; i<npx; i++)
                {
                if (numcan == 1)
                    {
                    int col = imr[np];
                    fprintf (fp,"%d\n",col);
                    }
                if (numcan == 2)
                    {
                    int col = imr[np]*256+img[np];
                    fprintf (fp,"%d\n",col);
                    }
                if (numcan == 3)
                    {
                    long col = imr[np]*256*256+img[np]*256+imb[np];
                    fprintf (fp,"%ld\n",col);
                    }
                np++;
                }
        }
    return 1;
    }

int write_raw_image(FILE *fp,long np, long numcan, long inter,
                   unsigned char *imr,unsigned char *img, unsigned char *imb)
    {
    unsigned char dum;
	long i;
	if (inter)
		{
	    for (i=0; i<np; i++)
		    {
			if (numcan == 1)
				{
				dum=imr[i];
				fwrite(&dum,sizeof(char),1,fp);
				}
			else if (numcan==2)	// Asumimos 12 bits de informacion
				{
				dum=(imr[i]*256+img[i])/16;
				fwrite(&dum,sizeof(char),1,fp);
				}
			else
				{
				dum=imr[i];
				fwrite(&dum,sizeof(char),1,fp);
				dum=img[i];
				fwrite(&dum,sizeof(char),1,fp);
				dum=imb[i];
				fwrite(&dum,sizeof(char),1,fp);
				}
			}
		}
	else
		{
		if (numcan == 1)
			fwrite(imr,sizeof(char),np,fp);
		else if (numcan == 2)	// Asumimos 12 bits de informacion
			{
			for (i=0; i<np; i++)
				{
				dum=(imr[i]*256+img[i])/16;
				fwrite(&dum,sizeof(char),1,fp);
				}
			}
		else
			{
			fwrite(imr,sizeof(char),np,fp);
			fwrite(img,sizeof(char),np,fp);
			fwrite(imb,sizeof(char),np,fp);
			}
		}
	return(1);
	}

