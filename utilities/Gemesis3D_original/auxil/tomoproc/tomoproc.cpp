#include "tomoproc.h"

int main()
    {
    StartACDP(0, "acdp.sai", "trace.txt");
    InitDB();
    TraceOn("Tomoproc");

    //Apertura del archivo de configuracion
    char name[256];
    printf("Configuration File_name: ");
    scanf ("%s", name);
    FILE *fcfg= fopen(name, "r");
    if (!fcfg)
        {
        printf("ERROR: Cannot open file");
        return(0);
        }

    //Dimensionamiento
    long npx,npy,npz;
    char *kwd0 = "NPX_NPY_NPZ";
    if (FindKeyWord(kwd0, fcfg))
        {
        fscanf(fcfg,"%ld %ld %ld",&npx,&npy,&npz);
        }
    else
        Error(FATAL_ERROR, 1, "Keyword NPX_NPY_NPZ not found");

    char *kwd01= "SIZE_HEADER";
    long tamheader;
    if (FindKeyWord(kwd01, fcfg))
        fscanf(fcfg,"%ld",&tamheader);
    else
        tamheader=0;

    char *kwd02= "NUM_CHANEL";
    long numcan;
    if (FindKeyWord(kwd02, fcfg))
        fscanf(fcfg,"%ld",&numcan);
    else
        numcan=1;

	long inter=0;
	if (numcan>1)
	{
		char *kwd03= "INTERLACED";
		if (FindKeyWord(kwd03, fcfg))
			fscanf(fcfg,"%ld",&inter);
	}

    unsigned char *imr, *img, *imb;
	imr = (unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);
	if (numcan > 1)
	    img=(unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);
	if (numcan > 2)
	    imb=(unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);

    //Lectura del archivo con la tomografia
    char *kwd1 = "INPUT_BMP";
    char *kwd2 = "INPUT_TXT";
    char *kwd3 = "INPUT_RAW";
    char *kwda = "INPUT_STACK";
    char filename[255];
    FILE *fp;
    if (FindKeyWord(kwd1, fcfg))
        {
        long npxa,npya;
        fscanf(fcfg,"%s",filename);
        printf("\n\nInput data from file: %s", filename);
        fp = fopen(filename, "rb");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open input file");
        read_header_bmp_image(fp, npxa, npya);
        if (npx!=npxa && npy*npz!=npya)
            Error(FATAL_ERROR, 3, "Incompatible size");
        read_bmp_image(fp, npxa, npya, imr, img, imb);
        }
    else if (FindKeyWord(kwd2, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nInput mesh from file: %s", filename);
        fp = fopen(filename, "r");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open input file");
        read_txt_image(fp, npx, npy, npz, imr, img, imb);
        }
    else if (FindKeyWord(kwd3, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nInput mesh from file: %s", filename);
        fp = fopen(filename, "rb");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open input file");
        read_raw_image(fp, npx*npy*npz, tamheader, numcan, inter, imr, img, imb);
        }
    else if (FindKeyWord(kwda, fcfg))
        {
        printf("\nMounting the stack...\n");
        long npxa,npya,ip=0;
        for (int i=0; i<npz; i++,ip+=npx*npy)
            {
            fscanf(fcfg,"%s",filename);
            printf("\n\nInput data from file: %s", filename);
            fp = fopen(filename, "rb");
            if (!fp) Error(FATAL_ERROR, 2, "Can't open input file");
            read_header_bmp_image(fp, npxa, npya);
            if (npx!=npxa && npy*npz!=npya)
                Error(FATAL_ERROR, 3, "Incompatible size");
            read_bmp_image(fp, npxa, npya, imr+ip, img+ip, imb+ip);
            fclose(fp);
            }
        }
    else
        Error(FATAL_ERROR, 2, "Can't find some INPUT_ keyword");
    fclose(fp);

    char *kwd4 = "SELECT_BOX";
    if (FindKeyWord(kwd4, fcfg))
        {
        printf("\nSelectin box...\n");
        long npx_min, npx_max;
        long npy_min, npy_max;
        long npz_min, npz_max;
        fscanf (fcfg,"%ld %ld %ld %ld %ld %ld",
                &npx_min,&npx_max,&npy_min,&npy_max,&npz_min,&npz_max); 
        long anpy=npy-npy_min;
        npy_min  =npy-npy_max;
        npy_max  =anpy;
        if (npx<npx_max || npy<npy_max || npz<npz_max)
                Error(FATAL_ERROR, 3, "Incompatible size");
        npx_min--; npy_min--; npz_min--;
        long tam=(npx_max-npx_min)*(npy_max-npy_min)*(npz_max-npz_min);
        unsigned char *imrn, *imgn, *imbn;
		imrn = (unsigned char *)mMalloc(sizeof(char)*tam);
		if (numcan!=1)
			imgn = (unsigned char *)mMalloc(sizeof(char)*tam);
		if (numcan==3)
			imbn = (unsigned char *)mMalloc(sizeof(char)*tam);
        select(numcan,npx,npy,npz,imr,img,imb, imrn,imgn,imbn,
               npx_min,npx_max,npy_min,npy_max,npz_min,npz_max);
        free(imr);
        imr = imrn;
		if (numcan!=1) free(img);
        img = imgn;
		if (numcan==3) free(imb);
        imb = imbn;
        npx=npx_max-npx_min;
        npy=npy_max-npy_min;
        npz=npz_max-npz_min;
        }

    char *kwd5 = "CONDENSE";
    if (FindKeyWord(kwd5, fcfg))
        {
        int cx,cy,cz;
        fscanf(fcfg,"%d %d %d",&cx,&cy,&cz);
        printf("\nCondensing image...\n");
/*
        unsigned char *cimr, *cimg, *cimb;
	    cimr = (unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);
	    if (numcan > 1)
	        cimg=(unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);
	    if (numcan > 2)
	        cimb=(unsigned char *)mMalloc(sizeof(char)*npx*npy*npz);
*/
        Condensa(imr,img,imb,numcan,npx,npy,npz,cx,cy,cz);
/*
        free(imr);
	    if (numcan > 1) free(img);
	    if (numcan > 2) free(imb);
        imr = cimr;
        img = cimg;
        imb = cimb;
        npx/=2; npy/=2; npz/=2;
*/
        }

    //Output
    char *kwd6 = "OUTPUT_BMP";
    char *kwd7 = "OUTPUT_TXT";
    char *kwd8 = "OUTPUT_RAW";
    char *kwd9 = "OUTPUT_VWM";
    char *kwd10= "OUTPUT_SEL_VWM";
    if (FindKeyWord(kwd6, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput to file: %s", filename);
        fp = fopen(filename, "wb");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open output file");
        write_bmp_image(fp, npx, npy*npz, numcan, imr,img,imb);
        }
    else if (FindKeyWord(kwd7, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput to file: %s", filename);
        fp = fopen(filename, "w");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open output file");
        write_txt_image(fp, npx, npy, npz, numcan, imr,img,imb);
        }
    else if (FindKeyWord(kwd8, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput to file: %s", filename);
        fp = fopen(filename, "wb");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open output file");
        write_raw_image(fp, npx*npy*npz, numcan, inter, imr,img,imb);
        }
    else if (FindKeyWord(kwd9, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput to file: %s", filename);
        double dx,dy,dz;
        fscanf(fcfg,"%lf %lf %lf",&dx,&dy,&dz);
        fp = fopen(filename, "w");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open output file");
        PrintMesh(fp, npx, npy, npz,numcan,imr,img,imb,dx,dy,dz);
        }
    else if (FindKeyWord(kwd10, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nOutput to file: %s", filename);
        double dx,dy,dz;
        fscanf(fcfg,"%lf %lf %lf",&dx,&dy,&dz);
        fp = fopen(filename, "w");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open output file");
        SelPrintMesh(fp, npx, npy, npz,numcan,imr,img,imb,dx,dy,dz);
        }
    else
        Error(FATAL_ERROR, 2, "Can't find some OUTPUT_ keyword");
    fclose(fp);

	return (0);
	}

void select(long numcan,long npx,long npy,long npz,
			unsigned char *imr, unsigned char *img, unsigned char *imb, 
			unsigned char *imrn, unsigned char *imgn, unsigned char *imbn, 
            long npx_min,long npx_max,
			long npy_min,long npy_max,
            long npz_min,long npz_max)
    {
    long np=0;
    for (long k=npz_min; k<npz_max; k++)
        {
        for (long j=npy_min; j<npy_max; j++)
            {
            for (long i=npx_min; i<npx_max; i++)
                {
				long ind=k*npx*npy+j*npx+i;
                imrn[np]=imr[ind];
                if (numcan!=1)
					imgn[np]=img[ind];
                if (numcan==3)
					imbn[np]=imb[ind];
                np++;
                }
            }
        }
    }

void PrintMesh(FILE *fo, long npx, long npy, long npz, long numcan,
			unsigned char *imr, unsigned char *img, unsigned char *imb,
			double dx, double dy, double dz)
   {
   long nelem=0;
   long nx=npx+1;
   long ny=npy+1;
   long nz=npz+1;

   long n1,n2,n3,n4,n5,n6,n7,n8;
   long i,j,k,e;
   fprintf(fo,"*INCIDENCE\n");
   for (k=0; k<npz; k++)
      for (j=0; j<npy; j++)
         for (i=0; i<npx; i++)
             {
             n1 = k*nx*ny + j*nx + i + 1;
             n2 = k*nx*ny + j*nx + i + 2;
             n3 = k*nx*ny + (j+1)*nx + i + 1;
             n4 = k*nx*ny + (j+1)*nx + i + 2;
             n5 = (k+1)*nx*ny + j*nx + i + 1;
             n6 = (k+1)*nx*ny + j*nx + i + 2;
             n7 = (k+1)*nx*ny + (j+1)*nx + i + 1;
             n8 = (k+1)*nx*ny + (j+1)*nx + i + 2;
             fprintf(fo,"%ld %ld %ld %ld\n",n2,n4,n3,n8);
             fprintf(fo,"%ld %ld %ld %ld\n",n1,n2,n3,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n3,n2,n8,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n7,n5,n1,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n7,n8,n6,n3);
             fprintf(fo,"%ld %ld %ld %ld\n",n3,n6,n7,n1);
             nelem+=6;
             }

   fprintf(fo,"*ELEMENT GROUPS\n 1\n 1 %ld Tetra4\n",nelem);

   fprintf(fo,"*COORDINATES\n %ld\n",nx*ny*nz);
   for (k=0; k<nz; k++)
      for (j=0; j<ny; j++)
         for (i=0; i<nx; i++)
             {
             n1 = k*nx*ny + j*nx + i + 1;
             fprintf(fo,"%ld %lg %lg %lg\n",n1,dx*i,dy*j,dz*k);
             }

    fprintf(fo,"*SOLUTION\n sol\n");
    for (k=0; k<nz; k++)
        for (j=0; j<ny; j++)
            for (i=0; i<nx; i++)
		        {
                double col=0;
                long acum=0;
                if (k<npz)
                    {
                    if (j<npy)
                        {
                        if (i<npx)
                            {
                            e =     k*npx*npy +     j*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e =     k*npx*npy +     j*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    if (j>0)
                        {
                        if (i<npx)
                            {
                            e =     k*npx*npy + (j-1)*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e =     k*npx*npy + (j-1)*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    }
                if (k>0)
                    {
                    if (j<npy)
                        {
                        if (i<npx)
                            {
                            e = (k-1)*npx*npy +     j*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e = (k-1)*npx*npy +     j*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    if (j>0)
                        {
                        if (i<npx)
                            {
                            e = (k-1)*npx*npy + (j-1)*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e = (k-1)*npx*npy + (j-1)*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    }
                fprintf(fo,"%lg\n",col/acum);
                }
   fprintf(fo,"*END");
   }

void SelPrintMesh(FILE *fo, long npx, long npy, long npz, long numcan,
			unsigned char *imr, unsigned char *img, unsigned char *imb,
			double dx, double dy, double dz)
   {
   long n1,n2,n3,n4,n5,n6,n7,n8;
   long i,j,k,e;
   long nelem=0,ind=0;
   long nx=npx+1;
   long ny=npy+1;
   long nz=npz+1;

   float *sol=(float *)malloc(sizeof(float)*nx*ny*nz);
   for (k=0; k<nz; k++)
       for (j=0; j<ny; j++)
           for (i=0; i<nx; i++,ind++)
               {
               float col=0;
               long acum=0;
               if (k<npz)
                    {
                    if (j<npy)
                        {
                        if (i<npx)
                            {
                            e =     k*npx*npy +     j*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e =     k*npx*npy +     j*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    if (j>0)
                        {
                        if (i<npx)
                            {
                            e =     k*npx*npy + (j-1)*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e =     k*npx*npy + (j-1)*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    }
                if (k>0)
                    {
                    if (j<npy)
                        {
                        if (i<npx)
                            {
                            e = (k-1)*npx*npy +     j*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e = (k-1)*npx*npy +     j*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    if (j>0)
                        {
                        if (i<npx)
                            {
                            e = (k-1)*npx*npy + (j-1)*npx + i;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        if (i>0)
                            {
                            e = (k-1)*npx*npy + (j-1)*npx + i - 1;
                            if (numcan==1) col+= imr[e];
                            else if (numcan==2) col+= imr[e]*256+img[e];
                            else if (numcan==3) col+= imr[e];//imr[e]*256*256+img[e]*256+imb[e];
                            acum++;
                            }
                        }
                    }
                sol[ind]=col/acum;
                }

   fprintf(fo,"*INCIDENCE\n");
   for (k=0; k<npz; k++)
      for (j=0; j<npy; j++)
         for (i=0; i<npx; i++)
             {
             n1 = k*nx*ny + j*nx + i + 1;
             n2 = k*nx*ny + j*nx + i + 2;
             n3 = k*nx*ny + (j+1)*nx + i + 1;
             n4 = k*nx*ny + (j+1)*nx + i + 2;
             n5 = (k+1)*nx*ny + j*nx + i + 1;
             n6 = (k+1)*nx*ny + j*nx + i + 2;
             n7 = (k+1)*nx*ny + (j+1)*nx + i + 1;
             n8 = (k+1)*nx*ny + (j+1)*nx + i + 2;
             if(sol[n1-1]==sol[n2-1] && sol[n1-1]==sol[n3-1] &&
                sol[n1-1]==sol[n4-1] && sol[n1-1]==sol[n5-1] &&
                sol[n1-1]==sol[n6-1] && sol[n1-1]==sol[n7-1] &&
                sol[n1-1]==sol[n8-1] && sol[n1-1]<1) continue;
             fprintf(fo,"%ld %ld %ld %ld\n",n2,n4,n3,n8);
             fprintf(fo,"%ld %ld %ld %ld\n",n1,n2,n3,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n3,n2,n8,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n7,n5,n1,n6);
             fprintf(fo,"%ld %ld %ld %ld\n",n7,n8,n6,n3);
             fprintf(fo,"%ld %ld %ld %ld\n",n3,n6,n7,n1);
             nelem+=6;
             }

   fprintf(fo,"*ELEMENT GROUPS\n 1\n 1 %ld Tetra4\n",nelem);

   fprintf(fo,"*COORDINATES\n %ld\n",nx*ny*nz);
   for (k=0; k<nz; k++)
      for (j=0; j<ny; j++)
         for (i=0; i<nx; i++)
             {
             n1 = k*nx*ny + j*nx + i + 1;
             fprintf(fo,"%ld %lg %lg %lg\n",n1,dx*i,dy*j,dz*k);
             }

   fprintf(fo,"*SOLUTION\n sol\n");
   for (i=0; i<ind; i++)
       fprintf(fo,"%f\n",sol[i]);
   fprintf(fo,"*END");
   }

void Condensa(unsigned char *imr, unsigned char *img, unsigned char *imb,
              long numcan, long &npx, long &npy, long &npz, int cx, int cy, int cz)
//              unsigned char *cimr,unsigned char *cimg,unsigned char *cimb,
    {

    long sumr,sumg,sumb;
    long i,j,k,jj;
    
    // Condensa X
    long ip=0;
    for (k=0; k<npz; k++)
        for (j=0; j<npy; j++)
            {
            long shift=k*npx*npy+j*npx;
            for (i=0; i<npx; i+=cx)
                {
                sumr=imr[shift+i];
                if(numcan>1) sumg=img[shift+i];
                if(numcan>2) sumb=imb[shift+i];
                for (jj=1; jj<cx && i+jj<npx; jj++)
                    {
                    sumr+=imr[shift+i+jj];
                    if(numcan>1) sumg+=img[shift+i+jj];
                    if(numcan>2) sumb+=imb[shift+i+jj];
                    }
                imr[ip]=sumr/cx;
                if(numcan>1) img[ip]=sumg/cx;
                if(numcan>2) imb[ip]=sumb/cx;
                ip++;
                }
            }
    npx/=cx;

    // Condensa Y
    ip=0;
    if (cy>1)
    for (k=0; k<npz; k++)
        for (j=0; j<npy; j+=cy)
            {
            long shift=k*npx*npy;
            for (i=0; i<npx; i++)
                {
                sumr=imr[shift+j*npx+i];
                if(numcan>1) sumg=img[shift+j*npx+i];
                if(numcan>2) sumb=imb[shift+j*npx+i];
                for (jj=1; jj<cy && j+jj<npy; jj++)
                    {
                    sumr+=imr[shift+(j+jj)*npx+i];
                    if(numcan>1) sumg+=img[shift+(j+jj)*npx+i];
                    if(numcan>2) sumb+=imb[shift+(j+jj)*npx+i];
                    }
                imr[ip]=sumr/cy;
                if(numcan>1) img[ip]=sumg/cy;
                if(numcan>2) imb[ip]=sumb/cy;
                ip++;
                }
            }
    npy/=cy;

    // Condensa Z
    ip=0;
    if (cz>1)
    for (k=0; k<npz; k+=cz)
        for (j=0; j<npy; j++)
            {
            long shift=j*npx;
            for (i=0; i<npx; i++)
                {
                sumr=imr[shift+k*npx*npy+i];
                if(numcan>1) sumg=img[shift+k*npx*npy+i];
                if(numcan>2) sumb=imb[shift+k*npx*npy+i];
                for (jj=1; jj<cz && k+jj<npz; jj++)
                    {
                    sumr+=imr[shift+(k+jj)*npx*npy+i];
                    if(numcan>1) sumg+=img[shift+(k+jj)*npx*npy+i];
                    if(numcan>2) sumb+=imb[shift+(k+jj)*npx*npy+i];
                    }
                imr[ip]=sumr/cz;
                if(numcan>1) img[ip]=sumg/cz;
                if(numcan>2) imb[ip]=sumb/cz;
                ip++;
                }
            }
    npz/=cz;
/*
   long ipc = 0;
   long ip  = 0;
   long caux;
   for (long izc=0; izc<npz/2; izc++)
       for (long iyc=0; iyc<npy/2; iyc++)
           for (long ixc=0; ixc<npx/2; ixc++, ipc++)
               {
               caux = (long)imr[2*izc*npx*npy+2*iyc*npx+2*ixc] +
                      imr[2*izc*npx*npy+2*iyc*npx+2*ixc+1] +
                      imr[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc] +
                      imr[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc+1] +
                      imr[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc] +
                      imr[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc+1] +
                      imr[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc] +
                      imr[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc+1];
               cimr[ipc]=(unsigned char)(caux/8);
    	       if (numcan > 1)
                  {
                  caux = (long)img[2*izc*npx*npy+2*iyc*npx+2*ixc] +
                         img[2*izc*npx*npy+2*iyc*npx+2*ixc+1] +
                         img[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc] +
                         img[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc+1] +
                         img[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc] +
                         img[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc+1] +
                         img[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc] +
                         img[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc+1];
                  cimg[ipc]=(unsigned char)(caux/8);
                  }
    	       if (numcan > 2)
                  {
                  caux = (long)imb[2*izc*npx*npy+2*iyc*npx+2*ixc] +
                         imb[2*izc*npx*npy+2*iyc*npx+2*ixc+1] +
                         imb[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc] +
                         imb[2*izc*npx*npy+(2*iyc+1)*npx+2*ixc+1] +
                         imb[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc] +
                         imb[(2*izc+1)*npx*npy+2*iyc*npx+2*ixc+1] +
                         imb[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc] +
                         imb[(2*izc+1)*npx*npy+(2*iyc+1)*npx+2*ixc+1];
                  cimb[ipc]=(unsigned char)(caux/8);
                  }
               }
*/
    }
