#include "acdp.h"

int read_header_bmp_image(FILE *fp,long &npx, long &npy);
int read_bmp_image(FILE *fp,long npx, long npy,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
int read_raw_image(FILE *fp,long np, long tam, long numcan, long inter,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
int read_txt_image(FILE *fp,long npx,long npy,long npz,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
int write_bmp_image(FILE *fp,long npx, long npy, long numcan,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
int write_txt_image(FILE *fp,long npx,long npy,long npz, long numcan,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
int write_raw_image(FILE *fp,long np, long numcan, long inter,
                   unsigned char *imr,unsigned char *img, unsigned char *imb);
void select(long numcan,long npx,long npy,long npz,
			unsigned char *imr, unsigned char *img, unsigned char *imb, 
			unsigned char *imrn, unsigned char *imgn, unsigned char *imbn, 
            long npx_min,long npx_max,
			long npy_min,long npy_max,
            long npz_min,long npz_max);
void Condensa(unsigned char *imr, unsigned char *img, unsigned char *imb,
              long numcan, long &npx, long &npy, long &npz, int cx, int cy, int cz);
void PrintMesh(FILE *fo, long npx, long npy, long npz, long numcan,
			unsigned char *imr, unsigned char *img, unsigned char *imb,
			double dx, double dy, double dz);
void SelPrintMesh(FILE *fo, long npx, long npy, long npz, long numcan,
			unsigned char *imr, unsigned char *img, unsigned char *imb,
			double dx, double dy, double dz);
