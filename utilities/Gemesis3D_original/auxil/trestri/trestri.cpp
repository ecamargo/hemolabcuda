#include "acdp.h"
#include "mesh3d.h"

struct elem
    {
    long n1,n2,n3;
    long v1,v2,v3;
    int fl;
    };

struct elem *MontaEstruct(Mesh3d *mesh);
long *MarcaNodos(Mesh3d *mesh);
void EliminaTres(long nel, struct elem *el, long *nodo);
void Print(FILE *fp, Mesh3d *mesh, struct elem *el, long *nodo);

int main()
    {
    StartACDP(0, "acdp.sai", "trace.txt");
    InitDB();
    TraceOn("Main");

    Mesh3d *mesh;

    //Apertura del archivo con la malla
    char name[256];
    printf("File_name with mesh: ");
    scanf ("%s", name);
    FILE *fp1= fopen(name, "r");
    if (!fp1)
        {
        printf("ERROR: Cannot open file");
        return(0);
        }

	mesh = new Mesh3d;
	mesh->Read(fp1);         // leitura de mesh
    fclose(fp1);

    printf("\n%ld elementos\n%ld nodos\n",mesh->GetNumEls(),mesh->GetNumNodes());

    long *nodo=MarcaNodos(mesh);
    struct elem *el = MontaEstruct(mesh);
    EliminaTres(mesh->GetNumEls(),el,nodo);

    //Se salva la nueva malla
    printf("File_name to store new mesh: ");
    scanf ("%s", name);
    fp1= fopen(name, "w");
    Print(fp1,mesh,el,nodo);
    fclose(fp1);

	return (0);
	}

long *MarcaNodos(Mesh3d *mesh)
    {
    long nn=0;
	SparseNoEl  *NoEl = mesh->NewNoEl();
    long *nodo = (long *)mMalloc(sizeof(long)*mesh->GetNumNodes());
    if (!nodo)
        printf("\nError: Cannot allocate memory\n");
    for (long n=0; n<mesh->GetNumNodes(); n++)
        if (NoEl->GetNelems(n) < 3)
            {
            printf("Error: Nodo %ld pertenece a %ld elementos\n",n+1,NoEl->GetNelems(n));
            nodo[n]=nn++;
            }
        else if (NoEl->GetNelems(n)==3)
            {
            printf("Nodo %ld pertenece a tres elementos\n",n+1);
            nodo[n]=-1;
            }
        else
            nodo[n]=nn++;
    delete NoEl;
    printf("\nNumber of cases found: %ld\n",mesh->GetNumNodes()-nn);
    return nodo;
    }


struct elem *MontaEstruct(Mesh3d *mesh)
    {
    Element *ele;
    SparseFaFaV *ElEl= mesh->NewFaFaV();
    long e=0;
    struct elem *el=(struct elem *)mMalloc(sizeof(struct elem)*mesh->GetNumEls());
    for (ele = mesh->SetFirstElem(); ele; ele = mesh->SetNextElem(), e++)
	    {
        el[e].n1=ele->GetNode(0);
        el[e].n2=ele->GetNode(1);
        el[e].n3=ele->GetNode(2);
//        el[e].v1=ElEl->GetElem(e,0);
//        el[e].v2=ElEl->GetElem(e,1);
//        el[e].v3=ElEl->GetElem(e,2);
        el[e].v1=ElEl->GetElem(e,1);
        el[e].v2=ElEl->GetElem(e,2);
        el[e].v3=ElEl->GetElem(e,0);
        el[e].fl=0;
        }
    delete ElEl;
    return el;
    }

void EliminaTres(long nel, struct elem *el, long *nodo)
    {
    long v;
    for (long e=0; e<nel; e++)
        {
        if (el[e].fl) continue;
        if      (nodo[el[e].n1]<0)
            {
            if (el[el[e].v2].fl || el[el[e].v3].fl)
                printf("\nHuy...\n");
            el[el[e].v2].fl=1;
            el[el[e].v3].fl=1;
            v=el[e].v3;
            if      (el[v].n1!=el[e].n1 && 
                     el[v].n1!=el[e].n2 &&
                     el[v].n1!=el[e].n3)
                el[e].n1=el[v].n1;
            else if (el[v].n2!=el[e].n1 && 
                     el[v].n2!=el[e].n2 &&
                     el[v].n2!=el[e].n3)
                el[e].n1=el[v].n2;
            else if (el[v].n3!=el[e].n1 && 
                     el[v].n3!=el[e].n2 &&
                     el[v].n3!=el[e].n3)
                el[e].n1=el[v].n3;
            else
                printf("La cosa esta jodida...\n");
            }
        else if (nodo[el[e].n2]<0)
            {
            if (el[el[e].v1].fl || el[el[e].v3].fl)
                printf("\nHuy...\n");
            el[el[e].v1].fl=1;
            el[el[e].v3].fl=1;
            v=el[e].v3;
            if      (el[v].n1!=el[e].n1 && 
                     el[v].n1!=el[e].n2 &&
                     el[v].n1!=el[e].n3)
                el[e].n2=el[v].n1;
            else if (el[v].n2!=el[e].n1 && 
                     el[v].n2!=el[e].n2 &&
                     el[v].n2!=el[e].n3)
                el[e].n2=el[v].n2;
            else if (el[v].n3!=el[e].n1 && 
                     el[v].n3!=el[e].n2 &&
                     el[v].n3!=el[e].n3)
                el[e].n2=el[v].n3;
            else
                printf("La cosa esta jodida...\n");
            }
        else if (nodo[el[e].n3]<0)
            {
            if (el[el[e].v2].fl || el[el[e].v1].fl)
                printf("\nHuy...\n");
            el[el[e].v2].fl=1;
            el[el[e].v1].fl=1;
            v=el[e].v1;
            if      (el[v].n1!=el[e].n1 && 
                     el[v].n1!=el[e].n2 &&
                     el[v].n1!=el[e].n3)
                el[e].n3=el[v].n1;
            else if (el[v].n2!=el[e].n1 && 
                     el[v].n2!=el[e].n2 &&
                     el[v].n2!=el[e].n3)
                el[e].n3=el[v].n2;
            else if (el[v].n3!=el[e].n1 && 
                     el[v].n3!=el[e].n2 &&
                     el[v].n3!=el[e].n3)
                el[e].n3=el[v].n3;
            else
                printf("La cosa esta jodida...\n");
            }
        }
    }

void Print(FILE *fp, Mesh3d *mesh, struct elem *el, long *nodo)
    {
    long n1,n2,n3,nel=0;
    long e;
    fprintf(fp,"\n*INCIDENCE\n");
    for (e = 0; e<mesh->GetNumEls(); e++)
	    {
        if (el[e].fl) continue;
        n1=nodo[el[e].n1]+1;
        n2=nodo[el[e].n2]+1;
        n3=nodo[el[e].n3]+1;
        nel++;
        fprintf(fp,"%ld %ld %ld\n",n1,n2,n3);
        }
    fprintf(fp,"\n*ELEMENT GROUPS\n 1\n 1 %ld Tri3\n",nel);

    acCoordinate3D &Coord = *mesh->GetCoords();
    long n,nod=0;
    for (n=0; n<mesh->GetNumNodes(); n++)
        if (nodo[n]>=0) nod++;
    n1=1;
    acPoint3 p;
    fprintf(fp,"\n*COORDINATES\n %ld\n",nod);
    for (n=0; n<mesh->GetNumNodes(); n++)
        {
        if (nodo[n]>=0)
            {
            p = Coord[n];
            fprintf(fp,"%ld %lg %lg %lg\n",n1++,p.x,p.y,p.z);
            }
        }
    }
