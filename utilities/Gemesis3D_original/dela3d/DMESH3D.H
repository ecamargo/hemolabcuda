#ifndef DMESH3D_H
#define DMESH3D_H
#include "tsurface.h"
#include "ord_ot.h"

#ifndef REAL
#define REAL double
#endif

class Dmesh3D;
class cluster;

//----------------------------------------------------------------------
class elem                              // Elemento tetraedrico
{
  private:
    long  n1,n2,n3,n4; // Orden: n1,n2,n3: antihorario vistos desde n4
    long  v1,v2,v3,v4; // Vi: vecino opuesto a ni
    REAL radio, xc, yc, zc;

  public:
//  Metodo set: modifica atributos de un elemento, calcula circumscircle
    void set (REAL huge *x, REAL huge *y, REAL huge *z,
	      long no1, long no2, long no3, long no4,
	      long ve1=-1L, long ve2=-1L, long ve3=-1L, long ve4=-1L);

    void setw (REAL huge *x, REAL huge *y, REAL huge *z,
	      long no1, long no2, long no3, long no4,
	      long ve1=-1L, long ve2=-1L, long ve3=-1L, long ve4=-1L);

//  Metodo set: modifica atributos logicos de un elemento
    void set (long no1, long no2, long no3, long no4,
	      long ve1=-1L, long ve2=-1L, long ve3=-1L, long ve4=-1L)
	{ n1 = no1; n2 = no2; n3 = no3; n4 = no4; 
	  v1 = ve1; v2 = ve2; v3 = ve3; v4 = ve4; }

// Rota descripcion del elemento de manera que n1 sea no1, y que
//	no2, no3 sean n2, n3 o n3, n2.
    void rot_log (long no1, long no2, long no3);

//  Metodo neighbour: indica hacia donde ir para encontrar xp, yp, zp
    inline long neighbour(REAL huge *x, REAL huge *y, REAL huge *z,
			  REAL xp, REAL yp, REAL zp);

//  Metodo fail_Delaunay_test: devuelve 1 en tal caso
    inline int fail_Delaunay_test (REAL xp, REAL yp, REAL zp);

//  Metodo volume:
    REAL volume (REAL huge *x, REAL huge *y, REAL huge *z)
	{ REAL x1,x2,x3,y1,y2,y3,z1,z2,z3;
	x1 = x[n2]-x[n1]; y1 = y[n2]-y[n1]; z1 = z[n2]-z[n1];
	x2 = x[n3]-x[n1]; y2 = y[n3]-y[n1]; z2 = z[n3]-z[n1];
	x3 = x[n4]-x[n1]; y3 = y[n4]-y[n1]; z3 = z[n4]-z[n1];
	return (x1*(y2*z3-z2*y3)+y1*(z2*x3-x2*z3)+z1*(x2*y3-y2*x3)); }

friend Dmesh3D;
friend cluster;

}; 					// End class elem

// Funcion fail_Delaunay_test
// devuelve 1 si el elemento falla el test Delaunay con el punto (xp,yp)
inline int elem::fail_Delaunay_test (REAL xp, REAL yp, REAL zp)
    {
    REAL dx, dy, dz;
    dx = xp-xc; dy = yp-yc; dz = zp-zc;
    if (dx*dx + dy*dy + dz*dz < radio*0.999999999) return 1;
    else return 0;
    }

//----------------------------------------------------------------------
#define ELE_PER_VEC 10240
class cluster;

class Dmesh3D                              // Red Delaunay
  {
  private:
    long nod;	     // Nodos en la red
    long nel;	     // Elementos en la red
    int n_vecs;      // Numero de vectores de elementos
    long firstfree;  // 1er elemento de la lista de elementos libres
    elem **v_elem;   // Vector de pointers de los vectores de elementos
    REAL huge *x, huge *y, huge *z;      // Vectores de coordenadas
    long huge *an_element;   // Vector de elemento cercano a un nodo
    ord_ot *tree;    // Arbol conteniendo nodos previamente insertados
// Variables para estadistica:
    long nodes_added;
    long min_cluster_size, max_cluster_size, avg_cluster_size;
    long min_nel_incr, max_nel_incr;

  public:
//  Constructor: crea los cinco tetraedros iniciales
    Dmesh3D (long nodes, REAL huge *x0, REAL huge *y0, REAL huge *z0);
//  Constructor: Lee malla de un archivo
    Dmesh3D (FILE *fp);

//  Destructor: Libera la memoria ocupada por los elementos
    ~Dmesh3D ();

//  Metodo para obtener las estadisticas de la malla
    void GetStats (long &n_a, long &mi_c_s, long &ma_c_s,
	double &a_c_s, long &mi_n_i, long &ma_n_i)
	{
	n_a    = nodes_added;
	mi_c_s = min_cluster_size;
	ma_c_s = max_cluster_size;
	a_c_s  = (n_a == 0) ? 0 : (double)avg_cluster_size/n_a;
	mi_n_i = min_nel_incr;
	ma_n_i = max_nel_incr;
	}

// Metodo p_elem (devuelve el puntero para un elemento, dado su indice)
    elem *p_elem (long index)
	{
	int i_vec, offset;
	i_vec  = (int)(index / ELE_PER_VEC);
	offset = (int)(index % ELE_PER_VEC);
	return (v_elem[i_vec]+offset);
	}

//  Metodo add: agrega un nodo en la red
    void add (long node);

//  Metodo add_notree: agrega un nodo en la red sin aprovechar arbol
    void add_notree (long node);

//  Metodo which_elem: busqueda dirigida
    long which_elem (long node);

//  Metodo destroy_non_Delaunay:
    void destroy_non_Delaunay (cluster &cl, long node);

//  Metodo regenerate: regenera la red en un cluster con nodo interior
    void regenerate (cluster &cl, long node);

//  Metodo renumber: renumera la red segun vector de permutacion
    void renumber (long huge *vper);

//  Metodo print (imprime la red con formato para VIEWMESH)
    void print (char *name, REAL huge *fun=NULL);

//  Metodo is_ok(): test de red valida
    int is_ok(int chk_neighbours, int chk_an_element, int chk_geometry);

//  Metodo get_size: devuelve tamanho reservado
    long get_size (void);

//  Metodo get_info: devuelve en el string buf, los datos del elemento ele
    void get_info (long elem, char *buf);

//  Metodo rebuild_boundary: recupera la frontera.
    long B_Recovery (T_Surface *s, long &CwI, long &C_EF, long &C_EE);
    void BuildList (long node, long ele, int huge *msw, int nmsw,
	acList &ListElems);
    int Edge_Recover(long n1, long n2, acList &ListElems, int &caso);
    void DelPair(long ele1, long ele2);
    long AddElement(void);
    void Reg_Half_Cluster(int nt, long *tets, long *v1, long*v2);
    int Face_Recover(long n1, long n2, long n3, acList &ListElems,
	int huge *msw, int &nmsw);
    int Face_Rec_Edge(long n1, long n2, long n3, acList &ListElems);

    long Count_Lost_Nodes(void);
    long Reinsert_Nodes(void);


//  Metodo delext(): borra cascara, deja la red definitiva
    void delext(void);
    long Delext (T_Surface *s);

//  Metodo DelFreeNodes: elimina "nodos sueltos"
    void DelFreeNodes(void);
};					// end class mesh

#endif
