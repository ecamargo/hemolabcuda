#include "dmesh3d.h"
#include "clus3d.h"

long Dmesh3D::Delext (T_Surface *surf)
    {
    char *nome = "Dmesh3D::Delext";
    long inciface[3];
    long i, face, n1, n2, n3;
    long huge *swi;
    int huge *msw;
    int nmsw;

    TraceOn(nome);

    long nnel = n_vecs * ELE_PER_VEC;
    msw = (int huge *) mMalloc(nnel * sizeof(int));
    swi = (long huge *) mMalloc(nnel * sizeof(long));
    if (!msw || !swi)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
    for (i = 0; i < nnel; i++) swi[i] = msw[i] = 0;
    nmsw = 0;

    for (face = 0; face < surf->GetNumElems(); face++)
        {
        acList ListElems(sizeof(long)); // Lista de elementos cercanos
        acNodeList *nl;
        int exists;
        long *pele, ele;
        elem *el;
        surf->GetInciFace(face, inciface);

	if (!surf->GetFaceSW(face))
	    continue;

// Search for elements sharing this face
        n1 = inciface[0];
        n2 = inciface[1];
	n3 = inciface[2];
        nmsw++;
        ele = an_element[n1];
	msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
	BuildList (n1, ele, msw, nmsw, ListElems);
        exists = FALSE;
        for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
            {
            pele = (long *) nl->GetDados();
            ele = *pele;
            el = p_elem(ele);
            if ((el->n1 == n2 ||
                 el->n2 == n2 ||
                 el->n3 == n2 ||
                 el->n4 == n2)  &&
                (el->n1 == n3 ||
                 el->n2 == n3 ||
                 el->n3 == n3 ||
                 el->n4 == n3))
                exists = TRUE;
            }
        if (!exists)
            Error (FATAL_ERROR, 1,
                "\nFace: %ld - %ld - %ld: can't find elem", n1,n2,n3);
        else
            {
            el->rot_log(n1, n2, n3); // face: clockwise from exterior
            if (el->n2 == n2)        // el: interior
                {
		swi[ele]    = -1;
                swi[el->v4] =  1;
                }
            else
                {
                swi[ele]    =  1;
                swi[el->v4] = -1;
                }
            }
        ListElems.Clear();

        }  // Next face

// Sweeps elements, until no change is made (fill-in algorithm)
    int changed = 1;
    long elev;
    elem *el;
    while (changed)
        {
        changed = 0;
        for (i = 0; i<nnel; i++)
            {
            el = p_elem(i);
            if (el->n2 >=0)  // Element exists
                {
		if (swi[i] == 1)  // Element is exterior
                    {
                    elev = el->v1;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v2;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v3;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v4;
                    if (swi[elev] == 0) swi[elev] = 1;
                    swi[i] = 2;
                    el->n2 = -1;
                    changed = 1;
                    }
                }
            }
        }

// Assign new element numbers to the remaining elements
    long new_nel = 0;
    for (i = 0; i<nnel; i++)
        {
        el = p_elem(i);
        if (el->n2 >= 0)        // El lugar esta ocupado x 1 elemento
            {
	    swi[i] = new_nel;
            new_nel++;
            }
        else
            swi[i] = -1;
        }

    new_nel = 0;
    long v1,v2,v3,v4;
    for (i = 0; i<nnel; i++)
        {
        el = p_elem(i);
        if (el->n2 >= 0)        // El lugar esta ocupado x 1 elemento
            {
            v1 = swi[el->v1];
            v2 = swi[el->v2];
            v3 = swi[el->v3];
            v4 = swi[el->v4];
            p_elem(new_nel)->set(el->n1, el->n2, el->n3, el->n4,
                v1, v2, v3, v4);
            new_nel++;
            }
        }
    nel = new_nel;
    nod -= 8;
    mFree(msw);
    mFree(swi);
    TraceOff(nome);
    return nel;
    }

long Dmesh3D::Count_Lost_Nodes(void)
  {
  long i, lost_nodes;
  long iel;
  elem *el;
  lost_nodes = 0;
  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      lost_nodes++;
    }
  return lost_nodes;
  }

long Dmesh3D::Reinsert_Nodes(void)
  {
  long i, lost_nodes;
  long iel;
  elem *el;

  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      add_notree(i);
    }

  lost_nodes = 0;
  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      lost_nodes++;
    }
  return lost_nodes;
  }

void Dmesh3D::DelFreeNodes(void)
  {
  long i, iel, k, nodori;
  long huge * NuevaNum;
  NuevaNum = (long huge *) mMalloc(sizeof(long)*nod);
  if (!NuevaNum)
    Error(FATAL_ERROR, 1, "Insufficient memory");
  for (i = 0; i < nod; i++)
    NuevaNum[i]=0;

  for (i = iel = 0; iel<nel; i++)
    {
    elem *el = p_elem(i);
    if (el->n2 >= 0)
      {
      NuevaNum[el->n1] = 1;
      NuevaNum[el->n2] = 1;
      NuevaNum[el->n3] = 1;
      NuevaNum[el->n4] = 1;
      iel++;
      }
    }

  nodori = nod;
  for (k=i=0; i<nodori; i++)
    {
    if (NuevaNum[i])
      {
      x[k]=x[i];
      y[k]=y[i];
      z[k]=z[i];
      NuevaNum[i] = k++;
      }
    else
      {
      NuevaNum[i] = -1;
      nod--;
      }
    }

  if (nod != nodori)
    Error(COMMON_ERROR, 1, "%ld free nodes removed", nodori-nod);

  for (i = iel = 0; iel<nel; i++)
    {
    elem *el = p_elem(i);
    if (el->n2 >= 0)
      {
      el->n1 = NuevaNum[el->n1];
      el->n2 = NuevaNum[el->n2];
      el->n3 = NuevaNum[el->n3];
      el->n4 = NuevaNum[el->n4];
      iel++;
      }
    }
  }
