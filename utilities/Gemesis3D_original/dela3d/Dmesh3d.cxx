#include "dmesh3d.h"
REAL r_max;
//#include <stdio.h>
#include "clus3d.h"
#include <math.h>
//#include "mesh3d.h"

// Implementacion de clase elem:
// Metodo set de la clase elem
void elem::set (REAL huge *x, REAL huge *y, REAL huge *z,
                long no1, long no2, long no3, long no4,
                long ve1, long ve2, long ve3, long ve4)
  {
  n1 = no1; n2 = no2; n3 = no3; n4 = no4;
  v1 = ve1; v2 = ve2; v3 = ve3; v4 = ve4;

  REAL x1,y1,z1,x2,y2,z2,a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3;
  REAL de1, de2, de3, det;

  if (n2 < n1 && n2 < n3)
    { no1 = n2; no2 = n3; no3 = n1; }
  else if (n3 < n1 && n3 < n2)
    { no1 = n3; no2 = n1; no3 = n2; }

  x1 = x[no1];  y1 = y[no1];  z1 = z[no1];
  x2 = x[no2];  y2 = y[no2];  z2 = z[no2];
  a1 = x2 - x1;  b1 = y2 - y1;  c1 = z2 - z1;
  d1 = a1 * (x2+x1)*0.5 + b1 * (y2+y1)*0.5 + c1 * (z2+z1)*0.5;

  x2 = x[no3];  y2 = y[no3];  z2 = z[no3];
  a2 = x2 - x1;  b2 = y2 - y1;  c2 = z2 - z1;
  d2 = a2 * (x2+x1)*0.5 + b2 * (y2+y1)*0.5 + c2 * (z2+z1)*0.5;

  x2 = x[no4];  y2 = y[no4];  z2 = z[no4];
  a3 = x2 - x1;  b3 = y2 - y1;  c3 = z2 - z1;
  d3 = a3 * (x2+x1)*0.5 + b3 * (y2+y1)*0.5 + c3 * (z2+z1)*0.5;

  de1 = b1*c2 - c1*b2;
  de2 = c1*a2 - a1*c2;
  de3 = a1*b2 - b1*a2;

  det = (a3 * de1 + b3 * de2 + c3 * de3);
  if (det == 0.0)
    Error (FATAL_ERROR, 1, "Volumen nulo, simplex: %ld, %ld, %ld, %ld",
        n1,n2,n3,n4);
  else if (det < 0.0)
    printf("\nNegative volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);

  de1 = b2*c3 - c2*b3;
  de2 = b3*c1 - c3*b1;
  de3 = b1*c2 - c1*b2;
  det = 1.0 / det;

  xc = (d1*de1 + d2*de2 + d3*de3) * det;
  yc = (d1*(a3*c2-a2*c3) + d2*(a1*c3-a3*c1) + d3*(a2*c1-a1*c2)) * det;
  zc = (d1*(a2*b3-a3*b2) + d2*(a3*b1-a1*b3) + d3*(a1*b2-a2*b1)) * det;
  x1 = x1-xc; y1 = y1-yc; z1 = z1-zc;
  radio = x1*x1 + y1*y1 + z1*z1;
  }

void elem::setw (REAL huge *x, REAL huge *y, REAL huge *z,
                long no1, long no2, long no3, long no4,
                long ve1, long ve2, long ve3, long ve4)
  {
  n1 = no1; n2 = no2; n3 = no3; n4 = no4;
  v1 = ve1; v2 = ve2; v3 = ve3; v4 = ve4;

  REAL x1,y1,z1,x2,y2,z2,a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3;
  REAL de1, de2, de3, det;

  x1 = x[n1];  y1 = y[n1];  z1 = z[n1];
  x2 = x[n2];  y2 = y[n2];  z2 = z[n2];
  a1 = x2 - x1;  b1 = y2 - y1;  c1 = z2 - z1;
  d1 = a1 * (x2+x1)*0.5 + b1 * (y2+y1)*0.5 + c1 * (z2+z1)*0.5;

  x2 = x[n3];  y2 = y[n3];  z2 = z[n3];
  a2 = x2 - x1;  b2 = y2 - y1;  c2 = z2 - z1;
  d2 = a2 * (x2+x1)*0.5 + b2 * (y2+y1)*0.5 + c2 * (z2+z1)*0.5;

  x2 = x[n4];  y2 = y[n4];  z2 = z[n4];
  a3 = x2 - x1;  b3 = y2 - y1;  c3 = z2 - z1;
  d3 = a3 * (x2+x1)*0.5 + b3 * (y2+y1)*0.5 + c3 * (z2+z1)*0.5;

  de1 = b2*c3 - c2*b3;
  de2 = b3*c1 - c3*b1;
  de3 = b1*c2 - c1*b2;

  det = (a1 * de1 + a2 * de2 + a3 * de3);
  if (det == 0.0)
    printf("\nNull volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);
  else if (det < 0.0)
    printf("\nNegative volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);

  det = 1.0 / det;

  xc = (d1*de1 + d2*de2 + d3*de3) * det;
  yc = (d1*(a3*c2-a2*c3) + d2*(a1*c3-a3*c1) + d3*(a2*c1-a1*c2)) * det;
  zc = (d1*(a2*b3-a3*b2) + d2*(a3*b1-a1*b3) + d3*(a1*b2-a2*b1)) * det;
  x1 = x1-xc; y1 = y1-yc; z1 = z1-zc;
  radio = x1*x1 + y1*y1 + z1*z1;
  }


// Funcion neighbour (busqueda dirigida):
// Devuelve -1 si el punto cayo en el elemento,
// o el indice del elemento a donde dirigirse
long elem::neighbour (REAL huge *x, REAL huge *y, REAL huge *z,
        REAL xp, REAL yp, REAL zp)
    {
    REAL x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    REAL vol1,vol2,vol3,vol4,volm;
    long retval, signo, nn1, nn2, nn3, nn4, naux;
    long vv1, vv2, vv3, vv4;

    signo = 1; nn1 = n1; nn2 = n2; nn3 = n3, nn4 = n4;
    vv1 = v1; vv2 = v2; vv3 = v3; vv4 = v4;

    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}
    if (nn2 > nn3)
	{
	naux = nn2; nn2 = nn3; nn3 = naux;
	naux = vv2; vv2 = vv3; vv3 = naux;
	signo *= -1;
	}
    if (nn3 > nn4)
	{
	naux = nn3; nn3 = nn4; nn4 = naux;
	naux = vv3; vv3 = vv4; vv4 = naux;
	signo *= -1;
	}
    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}
    if (nn2 > nn3)
	{
	naux = nn2; nn2 = nn3; nn3 = naux;
	naux = vv2; vv2 = vv3; vv3 = naux;
	signo *= -1;
	}
    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}

  x1 = x[nn1]-xp; y1 = y[nn1]-yp; z1 = z[nn1]-zp;
  x2 = x[nn2]-xp; y2 = y[nn2]-yp; z2 = z[nn2]-zp;
  x3 = x[nn3]-xp; y3 = y[nn3]-yp; z3 = z[nn3]-zp;

  vol4 = x1*(y2*z3-y3*z2) + y1*(x3*z2-x2*z3) + z1*(x2*y3-x3*y2);
  vol4 *= signo * (-1);

  x4 = x[nn4]-xp; y4 = y[nn4]-yp; z4 = z[nn4]-zp;
  vol1 = x2*(y3*z4-y4*z3) + y2*(x4*z3-x3*z4) + z2*(x3*y4-x4*y3);
  vol1 *= signo;

  vol2 = x1*(y3*z4-y4*z3) + y1*(x4*z3-x3*z4) + z1*(x3*y4-x4*y3);
  vol2 *= signo * (-1);

  vol3 = x1*(y2*z4-y4*z2) + y1*(x4*z2-x2*z4) + z1*(x2*y4-x4*y2);
  vol3 *= signo;

  volm = vol1; retval = vv1;
  if (vol2<volm) { volm = vol2; retval = vv2; }
  if (vol3<volm) { volm = vol3; retval = vv3; }
  if (vol4<volm) { volm = vol4; retval = vv4; }

  if (volm >= 0) retval = -1;
  return retval;
  }

/*
long elem::neighbour (REAL huge *x, REAL huge *y, REAL huge *z,
        REAL xp, REAL yp, REAL zp)
  {
  REAL x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4, v;
  long retval;

  x1 = x[n1]-xp; y1 = y[n1]-yp; z1 = z[n1]-zp;
  x2 = x[n2]-xp; y2 = y[n2]-yp; z2 = z[n2]-zp;
  x3 = x[n3]-xp; y3 = y[n3]-yp; z3 = z[n3]-zp;
  v = - x1*(y2*z3-y3*z2) + y1*(x2*z3-x3*z2) - z1*(x2*y3-x3*y2);
  if (v<0) { retval = v4; goto cycle; }

  x4 = x[n4]-xp; y4 = y[n4]-yp; z4 = z[n4]-zp;
  v = - x2*(y4*z3-y3*z4) + y2*(x4*z3-x3*z4) - z2*(x4*y3-x3*y4);
  if (v<0) { retval = v1; goto cycle; }

  v = - x4*(y1*z3-y3*z1) + y4*(x1*z3-x3*z1) - z4*(x1*y3-x3*y1);
  if (v<0) { retval = v2; goto cycle; }

  v = - x4*(y2*z1-y1*z2) + y4*(x2*z1-x1*z2) - z4*(x2*y1-x1*y2);
  if (v<0) return v3;   // Si es la unica cara no es necesario rotar.

  return -1L;

  cycle:
  long aux = v1;
        v1 = v2;
        v2 = v3;
        v3 = aux;
       aux = n1;
        n1 = n2;
        n2 = n3;
        n3 = aux;
  return retval;
  }
*/

//----------------------------------------------------------------------
// Metodo rot_log de la clase elem: rota la descripcion logica de un
// elemento de manera que n1 = nodo1, n4 distinto de nodo1,nodo2,nodo3
void elem::rot_log (long nodo1, long nodo2, long nodo3)
    {
    long aux, vaux;

    if (n2 == nodo1)
        {
        aux  = n1; n1 = n2; n2 = n3; n3 = aux;
        vaux = v1; v1 = v2; v2 = v3; v3 = vaux;
        }
    else if (n3 == nodo1)
        {
        aux  = n3; n3 = n2; n2 = n1; n1 = aux;
        vaux = v3; v3 = v2; v2 = v1; v1 = vaux;
        }
    else if (n4 == nodo1)
        {
        aux  = n4; n4 = n2; n2 = n1; n1 = aux;
        vaux = v4; v4 = v2; v2 = v1; v1 = vaux;
        }
    else if (n1 != nodo1)
        Error (FATAL_ERROR, 1,
            "elem::rot_log: ele: %ld %ld %ld %ld, args: %ld %ld %ld",
            n1, n2, n3, n4, nodo1, nodo2, nodo3);

    if (n2 != nodo2 && n2 != nodo3)
        {
        aux  = n2; n2 = n3; n3 = n4; n4 = aux;
        vaux = v2; v2 = v3; v3 = v4; v4 = vaux;
        }
    else if (n3 != nodo2 && n3 != nodo3)
        {
        aux  = n4; n4 = n3; n3 = n2; n2 = aux;
        vaux = v4; v4 = v3; v3 = v2; v2 = vaux;
        }

    if (!(((n2 == nodo2) && (n3 == nodo3)) ||
          ((n2 == nodo3) && (n3 == nodo2))) )
        Error (FATAL_ERROR, 1,
            "elem::rot_log: ele: %ld %ld %ld %ld, args: %ld %ld %ld",
            n1, n2, n3, n4, nodo1, nodo2, nodo3);
    }

// Implementacion de clase Dmesh3D:

//  Constructor: crea los dos triangulos iniciales
Dmesh3D::Dmesh3D (long nodes,
        REAL huge *x0, REAL huge *y0, REAL huge *z0)
  {
  char *nome = "Dmesh3D::Dmesh3D(l,R*,R*,R*)";
  TraceOn(nome);
  nod = nodes;
  nel = 5;
  n_vecs = 1;
  firstfree = 5;
  v_elem = (elem **) mMalloc (n_vecs*sizeof(elem *));
  an_element = (long *) mMalloc (nod*sizeof(long));
  if (!v_elem || !an_element)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  v_elem[0] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
  if (!v_elem[0])
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  x = x0;
  y = y0;
  z = z0;

//      Crea arbol que contendra nodos insertados
  REAL xmmi, xmma, ymmi, ymma, zmmi, zmma, xd, yd, zd;
  xmmi = x[nod-8]; xmma = x[nod-1];
  ymmi = y[nod-8]; ymma = y[nod-1];
  zmmi = z[nod-8]; zmma = z[nod-1];
  r_max = (zmmi-zmma)* 1.0e+6;
  xd = xmma - xmmi; yd = ymma - ymmi; zd = zmma - zmmi;
  xd *= 0.3; yd *= 0.3; zd *= 0.3;
  xmmi += xd; xmma -= xd;
  ymmi += yd; ymma -= yd;
  zmmi += zd; zmma -= zd;
  tree = new ord_ot(xmmi,xmma,ymmi,ymma,zmmi,zmma);
  tree->add(nod-8, x, y, z, 0L);
  tree->add(nod-7, x, y, z, 0L);
  tree->add(nod-6, x, y, z, 0L);
  tree->add(nod-5, x, y, z, 0L);
  tree->add(nod-4, x, y, z, 0L);
  tree->add(nod-3, x, y, z, 0L);
  tree->add(nod-2, x, y, z, 0L);
  tree->add(nod-1, x, y, z, 0L);

//      Setea lista de elementos libres
  elem *v_aux = v_elem[0];
  for (int i = 5; i<ELE_PER_VEC; i++) {
      v_aux[i].n1 = i+1;
      v_aux[i].n2 = -1;
  }

  v_aux[0].set (x, y, z, nod-8, nod-7, nod-6, nod-4, 4L, -1L, -1L, -1L);
  v_aux[1].set (x, y, z, nod-5, nod-6, nod-7, nod-1, 4L, -1L, -1L, -1L);
  v_aux[2].set (x, y, z, nod-3, nod-4, nod-1, nod-7, 4L, -1L, -1L, -1L);
  v_aux[3].set (x, y, z, nod-2, nod-1, nod-4, nod-6, 4L, -1L, -1L, -1L);
  v_aux[4].set (x, y, z, nod-1, nod-4, nod-6, nod-7, 0L,  1L,  2L,  3L);

  an_element[nod-8] = 0L;
  an_element[nod-7] = 0L;
  an_element[nod-6] = 1L;
  an_element[nod-5] = 1L;
  an_element[nod-4] = 2L;
  an_element[nod-3] = 2L;
  an_element[nod-2] = 3L;
  an_element[nod-1] = 3L;

  nodes_added = 0;
  min_cluster_size = max_cluster_size = 5;
  avg_cluster_size = 0;
  min_nel_incr = max_nel_incr = 7;
  TraceOff(nome);
  }

//  Constructor: Lee de un archivo, calcula vecinos
Dmesh3D::Dmesh3D (FILE *fp)
  {
  char *nome = "Dmesh3D::Dmesh3D(FILE *)";
  TraceOn(nome);
// Lee Mesh3D, para aprovechar el calculo de elelv
  Mesh3d *m3d;
  SparseElElV *elelv;
  m3d = new Mesh3d;
  m3d->Read(fp);
  elelv = m3d->NewElElV();
  nod = m3d->GetNumNodes();

  x = (REAL *) mMalloc (nod * sizeof(REAL));
  y = (REAL *) mMalloc (nod * sizeof(REAL));
  z = (REAL *) mMalloc (nod * sizeof(REAL));
  if (!x || !y || !z)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

  n_vecs = 1;
  firstfree = 0;
  v_elem = (elem **) mMalloc (n_vecs*sizeof(elem *));
  an_element = (long *) mMalloc (nod*sizeof(long));
  if (!v_elem || !an_element)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  v_elem[0] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
  if (!v_elem[0])
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  for (int i = 0; i<ELE_PER_VEC; i++)
    {
    v_elem[0][i].n1 = i+1;
    v_elem[0][i].n2 = -1;
    }

  Element *el;
  long n1,n2,n3,n4,v1,v2,v3,v4;
  nel = 0;
  for (el = m3d->SetFirstElem(); el; el = m3d->SetNextElem())
    {
    n1 = el->GetNode(0); n2 = el->GetNode(1);
    n3 = el->GetNode(2); n4 = el->GetNode(3);
    v4 = elelv->GetElem (nel, 0); v3 = elelv->GetElem (nel, 1);
    v1 = elelv->GetElem (nel, 2); v2 = elelv->GetElem (nel, 3);
    an_element[n1] = an_element[n2] =
    an_element[n3] = an_element[n4] = nel;
    AddElement();
    p_elem(nel-1)->set(n1,n2,n3,n4, v1,v2,v3,v4);
    }
  delete elelv;

  acPoint3 pp;
//  acCoordinate3D *cc;
//  cc = m3d->GetCoords();
  for (i = 0; i < nod; i++)
    {
    pp = (*(m3d->GetCoords()))[i];
    x[i] = pp.x; y[i] = pp.y; z[i] = pp.z;
    }

  delete m3d;

  tree = NULL;

  TraceOff(nome);
  }

//  Destructor de la clase Dmesh3D:
Dmesh3D::~Dmesh3D ()
  {
  for (int i=0; i<n_vecs; i++)
    mFree (v_elem[i]);
  mFree(v_elem);
  mFree(an_element);
  if (tree) delete tree;
  }

// Metodo add de la clase mesh:
// Agrega un nodo en la red. Utiliza otros metodos de la clase
void Dmesh3D::add (long node)
  {
  long nelprev, cl_size, nel_incr;

  long ele_ini;

  ele_ini = which_elem (node);
  elem *p_el = p_elem(ele_ini);

  cluster cl(p_el->n1, p_el->n2, p_el->n3, p_el->n4,
             p_el->v1, p_el->v2, p_el->v3, p_el->v4);
  //printf("Creo cluster\n");

  nelprev = nel;

  // Libera elemento ele_ini
  p_el->n2 = -1;
  p_el->n1 = firstfree;
  firstfree = ele_ini;
  nel--;

  destroy_non_Delaunay (cl, node);
  //printf("Expandio cluster\n");

  cl_size = nelprev - nel;
  avg_cluster_size += cl_size;
  if      (cl_size < min_cluster_size) min_cluster_size = cl_size;
  else if (cl_size > max_cluster_size) max_cluster_size = cl_size;

  regenerate (cl, node);
  //printf("Regenero cluster\n");

  nel_incr = nel - nelprev;
  if      (nel_incr < min_nel_incr) min_nel_incr = nel_incr;
  else if (nel_incr > max_nel_incr) max_nel_incr = nel_incr;

  nodes_added++;
  }

// Metodo add_notree de la clase mesh:
// Agrega un nodo en la red. No aprovecha estructura de datos eficiente
void Dmesh3D::add_notree (long node)
  {
  long nelprev, cl_size, nel_incr;

  long ele_ini;

  long curr, next;
  REAL xp, yp, zp;
  xp = x[node];
  yp = y[node];
  zp = z[node];

  curr = 0;  // Comienza por el elemento 0

  while ((next = p_elem(curr)->neighbour(x,y,z,xp,yp,zp)) >= 0)
     curr = next;
  ele_ini = curr;

  elem *p_el = p_elem(ele_ini);

  cluster cl(p_el->n1, p_el->n2, p_el->n3, p_el->n4,
             p_el->v1, p_el->v2, p_el->v3, p_el->v4);

  nelprev = nel;

  // Libera elemento ele_ini
  p_el->n2 = -1;
  p_el->n1 = firstfree;
  firstfree = ele_ini;
  nel--;

  destroy_non_Delaunay (cl, node);

  cl_size = nelprev - nel;
  avg_cluster_size += cl_size;
  if      (cl_size < min_cluster_size) min_cluster_size = cl_size;
  else if (cl_size > max_cluster_size) max_cluster_size = cl_size;

  regenerate (cl, node);

  nel_incr = nel - nelprev;
  if      (nel_incr < min_nel_incr) min_nel_incr = nel_incr;
  else if (nel_incr > max_nel_incr) max_nel_incr = nel_incr;

  nodes_added++;
  }

//  Metodo which_elem: busqueda dirigida
long Dmesh3D::which_elem (long node)
  {
  long curr, next;
  long nrec;
  REAL xp, yp, zp;
  xp = x[node];
  yp = y[node];
  zp = z[node];

                // Para determinar con que elemento comenzar
  long hoja_ini = 0L;   // se fija en el arbol que va armando
//printf("Entro a which_elem\n");
  curr = tree->nodo_cercano (xp, yp, zp, hoja_ini);
//printf("Nodo cercano: %ld\n", curr);
  tree->add(node, x, y, z, hoja_ini);

  curr = an_element[curr];
//printf("Elemento asociado: %ld\n", curr);

  nrec = 0;
  while ((next = p_elem(curr)->neighbour(x,y,z,xp,yp,zp)) >= 0 &&
	 nrec < nel+20)
	{
//printf("Proximo elemento: %ld\n", next);
     nrec++;
     curr = next;
	}
  if (nrec > nel)
   printf("\nInfinite loop at which_elem, used: %ld", curr);
  return curr;
  }

// Metodo destroy_non_Delaunay de la clase mesh
// borra los tetraedros q' fallan el test delaunay con el nodo node,
// actualizando el cluster cl
void Dmesh3D::destroy_non_Delaunay (cluster &cl, long node)
{
  int edge_collapse, all_analized;
  node_cluster *curr;
  elem *p_ele;
  REAL xp, yp, zp;
  xp = x[node]; yp = y[node]; zp = z[node];

  int iface, currcv1, currcv2, currcv3;
  long ele, currn1, currn2, currn3;

  all_analized = 0;
  while (!all_analized) {
  all_analized = 1;

  int nfaces = cl.getnfaces();
  for (iface = 0; iface < nfaces ; iface++) {   // Recorre vector de caras
    curr    = cl.get_p (iface);
    currn1  = cl.getn1 (iface);
    currn2  = cl.getn2 (iface);
    currn3  = cl.getn3 (iface);
    currcv1 = cl.getcv1(iface);
    currcv2 = cl.getcv2(iface);
    currcv3 = cl.getcv3(iface);
    ele     = cl.getv(iface);                   // Toma elemento vecino
    p_ele   = p_elem(ele);

    if  ( ele >= 0 && !curr->is_analized() )
      {
      if (p_ele->n2 < 0) {      // Algo anda mal. Esta cara no deberia estar
        cl.collapse_faces(iface);
        nfaces = 0;
        all_analized = 0;
      }
      else if ( p_ele->fail_Delaunay_test (xp, yp, zp) ||
           !curr->is_visible_from (node,x,y,z,r_max) ) {
                                          // Si falla el test Delaunay
                                          // o la cara no es visible

// Actualizar cluster:
      p_ele->rot_log (currn1, currn2, currn3);

// Test p/ver si dos caras del cluster se anulan:
      edge_collapse = 0;
      if (cl.getv(currcv1) == ele) {    // Colapsa arista curr-cv1
        edge_collapse = 1;
      }
      else {
        if (cl.getv(currcv2) == ele) {  // Colapsa arista curr-cv2
          edge_collapse = 1;
          p_ele->rot_log (currn2, currn3, currn1);
          curr->rot_log (currn2);
          currcv1 = currcv2;
        }
        else {
          if (cl.getv(currcv3) == ele) {// Colapsa arista curr-cv3
            edge_collapse = 1;
            p_ele->rot_log (currn3, currn1, currn2);
            curr->rot_log (currn3);
            currcv1 = currcv3;
          }
        }
      }

      if (edge_collapse) {   // Desaparece un elemento, 2 caras se modifican
//
//                  2 3                        2                 3
//                   ^                         ^         ele:   ^
//                  /|\                       / \              /|\
//                 / | \                     / a \            / | \
//                /a |  \                 1 /     \ 3        /  |  \
//              1<- -|- ->1      --->      <------->       4<- -|- ->1
//                \  | b/                 3 \     / 1        \  |  /
//                 \ | /                     \ b /            \ | /
//                  \|/                       \ /              \|/
//                   v                         v                v
//                  3 2                        2                 2
//

        long elvt, elvb;
        elvt = p_ele->v2; elvb = p_ele->v3;

// Borra elemento
        p_ele->n1 = firstfree;
        p_ele->n2 = -1;
        firstfree = ele;
        nel--;

// Actualiza cluster
        cl.collapse_edge(currcv1, iface, elvt, elvb);

// Analiza caras recien creadas: currcv1, iface
        int collapse_1, collapse_2;
        collapse_1 = collapse_2 = FALSE;
        ele = cl.getv(currcv1);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_1 = TRUE;
        }
        ele = cl.getv(iface);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_2 = TRUE;
        }

        if (collapse_1 || collapse_2) {
          if (collapse_1 && collapse_2)
            cl.collapse_faces (currcv1, iface);
          else if (collapse_1)
            cl.collapse_faces (currcv1);
          else if (collapse_2)
            cl.collapse_faces (iface);
          nfaces = 0;
        }

      }         //end if (edge_colapse) then

      else {    // Desaparece un elemento, nacen 3 nuevas caras (-1)
//
//          2                 3                 2                 3
//           <--------------->                   <--------------->
//            \-_         _-/                     \-_         _-/
//             \ -_     _- /                       \ -_     _- /
//              \  -_ _-  /                         \  -_4_-  /
//               \   |   /                           \   |   /
//                \  |  /                             \  |  /
//                 \ | /                        ele:   \ | /
//                  \|/                                 \|/
//                   v                                   v
//                   1                                   1
//

        long elvt, elvl, elvr;
        p_ele->rot_log (currn1, currn2, currn3);
        elvt = p_ele->v1;
        elvl = p_ele->v3;
        elvr = p_ele->v2;

        long nodop = p_ele->n4;

// Borra elemento
        p_ele->n1 = firstfree;
        p_ele->n2 = -1;
        firstfree = ele;
        nel--;

// Actualiza cluster
        cl.destroy_face(iface, nodop, elvt, elvr, elvl);

// Analiza caras recien creadas: iface, n_sides-2, n_sides-1
        int iface2, iface3;
        iface2 = cl.getnfaces()-1;
        iface3 = iface2-1;
        int collapse_1, collapse_2, collapse_3;
        collapse_1 = collapse_2 = collapse_3 = FALSE;
        ele = cl.getv(iface);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_1 = TRUE;
        }
        ele = cl.getv(iface2);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_2 = TRUE;
        }
        ele = cl.getv(iface3);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_3 = TRUE;
        }

        if (collapse_1 || collapse_2 || collapse_3) {
          if (collapse_1 && collapse_2 && collapse_3)
            cl.collapse_faces (iface, iface2, iface3);
          else if (collapse_1 && collapse_2)
            cl.collapse_faces (iface, iface2);
          else if (collapse_2 && collapse_3)
            cl.collapse_faces (iface2, iface3);
          else if (collapse_1 && collapse_3)
            cl.collapse_faces (iface, iface3);
          else if (collapse_1)
            cl.collapse_faces (iface);
          else if (collapse_2)
            cl.collapse_faces (iface2);
          else if (collapse_3)
            cl.collapse_faces (iface3);
          nfaces = 0;
        }
      }

      all_analized = 0;

    }   //end if (fail_Delaunay_test)
    else
      curr->set_analized();
    }   //end if (ele>=0 && ~analized
    else {
      curr->set_analized();
    }
  }
  }
}

// Metodo regenerate de la clase mesh:
// Regenera red en el cluster cl, que contiene al nodo node.

void Dmesh3D::regenerate (cluster &cl, long node)
{
  int curr_face, nfaces;
  long curr_el, vec;
  long n1, n2, n3;

// PRIMER LOOP: crea nuevos elementos, estableciendo conectividad solo
//      con el exterior del cluster.
  nfaces = cl.getnfaces();
  for (curr_face = 0; curr_face<nfaces; curr_face++) {

    n1 = cl.getn1(curr_face);
    n2 = cl.getn2(curr_face);
    n3 = cl.getn3(curr_face);
    vec = cl.getv(curr_face);           // Elemento externo
    curr_el = AddElement();
    p_elem(curr_el)->set(x, y, z, n1, n3, n2, node, -1L, -1L, -1L, vec);
    cl.setv (curr_face, curr_el);

    if (vec>=0) {
      elem *pt_vec = p_elem(vec);
      if (pt_vec->n1 != n1 && pt_vec->n1 != n2 && pt_vec->n1 != n3)
        pt_vec->v1 = curr_el;
      else
      if (pt_vec->n2 != n1 && pt_vec->n2 != n2 && pt_vec->n2 != n3)
        pt_vec->v2 = curr_el;
      else
      if (pt_vec->n3 != n1 && pt_vec->n3 != n2 && pt_vec->n3 != n3)
        pt_vec->v3 = curr_el;
      else
        pt_vec->v4 = curr_el;
    }

    an_element[n1] = an_element[n2] = an_element[n3] = curr_el;
  }

  an_element[node] = curr_el;

// SEGUNDO LOOP: establece conectividad entre los elementos recien generados

  for (curr_face = 0; curr_face<nfaces; curr_face++) {
    curr_el = cl.getv(curr_face);
    p_elem(curr_el)->v1 = cl.getv(cl.getcv1(curr_face));
    p_elem(curr_el)->v2 = cl.getv(cl.getcv3(curr_face));
    p_elem(curr_el)->v3 = cl.getv(cl.getcv2(curr_face));
  }

}

// Metodo renumber de la clase mesh
void Dmesh3D::renumber (long huge *vper)
{
  long i;
  REAL huge *vaux;
  long huge *perinv;
  char *nome = "Dmesh3d::renumber";
  TraceOn(nome);

  vaux   = (REAL huge *) mMalloc (nod*sizeof(REAL));
  perinv = (long huge *) mMalloc (nod*sizeof(long));
  if (!vaux || !perinv)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

  long iel;

  for (i=0; i<nod; i++)
      perinv[vper[i]] = i;

  for (i=0; i<nod; i++)
      vaux[i] = x[perinv[i]];
  for (i=0; i<nod; i++)
      x[i] = vaux[i];

  for (i=0; i<nod; i++)
      vaux[i] = y[perinv[i]];
  for (i=0; i<nod; i++)
      y[i] = vaux[i];

  for (i=0; i<nod; i++)
      vaux[i] = z[perinv[i]];
  for (i=0; i<nod; i++)
      z[i] = vaux[i];

  for (i = iel = 0; iel<nel; i++) {
      elem *el = p_elem(i);
      if (el->n2 >= 0) {
          el->n1 = vper[el->n1];
          el->n2 = vper[el->n2];
          el->n3 = vper[el->n3];
          el->n4 = vper[el->n4];
          an_element[el->n1] =
          an_element[el->n2] =
          an_element[el->n3] =
          an_element[el->n4] = i;
          iel++;
      }
  }

  mFree (vaux);
  mFree (perinv);
  TraceOff(nome);
}

// Metodo print de la clase mesh
void Dmesh3D::print (char *name, REAL huge *fun)
    {
    long i;
    FILE *fp;
    char *nome = "Dmesh3D::print";
    TraceOn(nome);

    fp = fopen (name,"w");

    long iel;

    fprintf (fp,"*COORDINATES\n");
    fprintf (fp,"%ld\n", nod);
    for (i = 0; i<nod; i++)
        fprintf (fp,"%ld %.16lg %.16lg %.16lg\n", i+1, x[i], y[i], z[i]);
    fprintf (fp,"*ELEMENT GROUPS\n");
    fprintf (fp," 1\n");
    fprintf (fp," 1 %ld LINEAR_TETRAHEDRUM\n", nel);
    fprintf (fp,"*INCIDENCE\n");
    for (i = iel = 0; iel<nel; i++)
        {
        elem *el = p_elem(i);
        if (el->n2 >= 0)
            {
            fprintf (fp, "%ld %ld %ld %ld\n",
            (el->n1)+1, (el->n2)+1, (el->n3)+1, (el->n4)+1);
            iel++;
            }
        }

    if (fun)
        {
        fprintf (fp,"*EDGES_SIZE\n");
        for (i = 0; i<nod; i++)
            {
            fprintf (fp," %g", fun[i]);
            if (!((i+1)%4)) fprintf (fp,"\n");
            }
        fprintf (fp,"\n");
        }

    fprintf (fp,"*END\n");

    fclose (fp);
    TraceOff(nome);
    }

//----------------------------------------------------------------------
// Metodo is_ok de la clase mesh: test logico y geometrico de la red
int Dmesh3D::is_ok (int check_neighbours, int check_an_element,
    int check_geometry)
  {
  elem *el, *neighbour;
  char *nome="Dmesh3D::is_ok";

  TraceOn(nome);

  long caras_externas = 0;
  long i, iel, ivec;
    long nn1, nn2, nn3;
  int conect_ns = 0;
  int carext_n12 = 0;

    if (check_neighbours)
        {
  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) {
      el = p_elem(i);
      if (el->n2 >= 0) {        // El lugar esta ocupado x 1 elemento
          iel++;
          ivec = el->v1;                        // VECINO 1
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                neighbour->rot_log(el->n2, el->n3, el->n4);
                if (neighbour->v4 != i) conect_ns = 1;
                neighbour->rot_log(nn1, nn2, nn3);
          }
          else
              caras_externas++;
          ivec = el->v2;                        // VECINO 2
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                neighbour->rot_log(el->n1, el->n4, el->n3);
                if (neighbour->v4 != i) conect_ns = 1;
                neighbour->rot_log(nn1, nn2, nn3);
          }
          else
              caras_externas++;
          ivec = el->v3;                        // VECINO 3
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                neighbour->rot_log(el->n1, el->n2, el->n4);
                if (neighbour->v4 != i) conect_ns = 1;
                neighbour->rot_log(nn1, nn2, nn3);
          }
          else
              caras_externas++;
          ivec = el->v4;                        // VECINO 4
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                neighbour->rot_log(el->n1, el->n3, el->n2);
                if (neighbour->v4 != i) conect_ns = 1;
                neighbour->rot_log(nn1, nn2, nn3);
          }
          else
              caras_externas++;
      }
  }

  if (caras_externas != 12) carext_n12 = 1;
        } // endif check_neighbours

//------------------- test an_element vector
    if (check_an_element)
        {
  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      Error(WARNING, 0,
      "Bad vector an_element. Node: %ld. Element: %ld %ld %ld %ld",
      i, el->n1, el->n2, el->n3, el->n4);
    }
        }

//------------------- test geometrico
  REAL qmin = 1.0, vmin = 1.0e+30;
  elem *el_peor, *el_min;
    if (check_geometry)
        {
  long n1, n2, n3, n4;


  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) {
   el = p_elem(i);
   if (el->n2 >= 0) {           // El lugar esta ocupado x 1 elemento
    iel++;
    n1 = el->n1; n2 = el->n2; n3 = el->n3; n4 = el->n4;

    if (n1<nod-8 && n2<nod-8 && n3<nod-8 && n4<nod-8) { // solo elementos
                                        // de la red sin la caja
    REAL x2,y2,z2, x3,y3,z3, x4,y4,z4;
    x2 = x3 = x4 = x[n1];
    y2 = y3 = y4 = y[n1];
    z2 = z3 = z4 = z[n1];
    x2 = x[n2] - x2;    y2 = y[n2] - y2;    z2 = z[n2] - z2;
    x3 = x[n3] - x3;    y3 = y[n3] - y3;    z3 = z[n3] - z3;
    x4 = x[n4] - x4;    y4 = y[n4] - y4;    z4 = z[n4] - z4;
    REAL vol = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
    if (vol < vmin) {
      vmin = vol;
      el_min = el;
    }

    REAL au, a, a1,a2,a3, b, b1,b2,b3, c1,c2,c3, xa,ya, x32;
    au = sqrt(x2*x2 + y2*y2 + z2*z2);    a = 1.0 / au;
    a1 = x2 * a; a2 = y2 *a; a3 = z2 * a;
    b1 = a2 * z3 - a3 * y3;  b2 = a3 * x3 - a1 * z3;  b3 = a1 * y3 - a2 * x3;
    b = 1.0 / sqrt(b1*b1 + b2*b2 + b3*b3);  b1 *= b; b2 *= b; b3 *= b;
    c1 = a3*b2 - a2*b3;    c2 = a1*b3 - a3*b1;   c3 = a2*b1 - a1*b2;
    x2 = au;
    a  = a1*x3 + a2*y3 + a3*z3;
    y3 = c1*x3 + c2*y3 + c3*z3;
    x3 = a;
    a  = a1*x4 + a2*y4 + a3*z4;
    b  = c1*x4 + c2*y4 + c3*z4;
    z4 = b1*x4 + b2*y4 + b3*z4;
    x4 = a;  y4 = b;
    ya = sqrt (z4*z4 + y4*y4) + y4;
    x32= x3 - x2;
    a1 = y3*z4;
    a2 = x32*z4;
    a3 = (x4-x2)*y3 - x32*y4;
    xa = x32*ya + a3 - sqrt (a1*a1 + a2*a2 + a3*a3);
    b2 = -x3*z4;
    b3 = x3*y4 - x4*y3;
    b = sqrt (a1*a1 + b2*b2 + b3*b3);

    REAL r_inscr = a1*x2 / (b - xa + x3*ya - b3);

    if (el->radio == 0.0)
        if (vol > 0.0)
            el->setw(x, y, z, el->n1, el->n2, el->n3, el->n4,
                el->v1, el->v2, el->v3, el->v4);
        else
            {
            el->radio = 1;
            r_inscr = 0;
            }
    REAL qele = r_inscr / sqrt(el->radio);
    if (qele < qmin) {
      qmin = qele;
      el_peor = el;
    }
    }
   }
  }
        } // endif check_geometry

  int todo_ok = 1;
    if (check_neighbours)
        {
  if (conect_ns) {
    Error (WARNING, 0, "Bad element-element conectivity");
    todo_ok = 0;
  }
  if (carext_n12) {
    printf("\nNumber of external faces (OK:12) = %ld",caras_externas);
    todo_ok = 0;
  }
        }

    if (check_geometry)
        {
  if (vmin <= 0.0)
    todo_ok = 0;

  printf("\nVolumen minimo: %f , elemento: %ld-%ld-%ld-%ld",
        vmin, el_min->n1, el_min->n2, el_min->n3, el_min->n4);
  printf("\nCalidad minima: %f , elemento: %ld-%ld-%ld-%ld",
        qmin, el_peor->n1, el_peor->n2, el_peor->n3, el_peor->n4);
        }

  TraceOff(nome);
  return todo_ok;
}

//----------------------------------------------------------------------
// Metodo delext de la clase mesh3D: borra los tetraedros de la caja,
//                                 compacta informacion de los restantes
//      OJO: Solo guarda informacion de los nodos de cada elemento

void Dmesh3D::delext (void)
{
  elem *el, *new_pos;
  long new_nel = 0;
  long n1, n2, n3, n4;
  char *nome = "Dmesh3D::delext";
  TraceOn(nome);

  long iel, i;
  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) {
      el = p_elem(i);
      if (el->n2 >= 0) {        // El lugar esta ocupado x 1 elemento
          iel++;

          n1 = el->n1; n2 = el->n2; n3 = el->n3; n4 = el->n4;

          if (n1<nod-8 && n2<nod-8 && n3<nod-8 && n4<nod-8) {//sobrevive
              new_pos = p_elem(new_nel);
              new_nel++;
              new_pos->n1 = n1; new_pos->n2 = n2;
              new_pos->n3 = n3; new_pos->n4 = n4;
          }
      }
  }
  nel = new_nel;
  nod = nod - 8;
  TraceOff(nome);
}

//  Metodo get_size: devuelve tamanho reservado
long Dmesh3D::get_size (void) {
  return (n_vecs*ELE_PER_VEC);
}

//Metodo get_info: devuelve en el string buf, los datos del elemento ele
void Dmesh3D::get_info (long ele, char *buf) {
  elem *el = p_elem(ele);
  if (el->n2 >= 0)
      sprintf (buf, "%5ld:%5ld-%5ld-%5ld-%5ld  %5ld-%5ld-%5ld-%5ld",ele,
          el->n1, el->n2, el->n3, el->n4,
          el->v1, el->v2, el->v3, el->v4);
  else
      sprintf (buf, "%5ld: Space Available", ele);
  return;
}


