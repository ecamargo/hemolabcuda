#include "dmesh3d.h"
#include "clus3d.h"

#include <stdarg.h>

// Boundary recovery procedure.
// Returns number of faces not recovered.
long Dmesh3D::B_Recovery (T_Surface *surf,
                long &CwIter, long &C_E_F, long &C_E_E)
    {
    char *nome = "Dmesh3D::B_Recovery";
    long FnotR;
    long inciface[3];
    long i, face, n1, n2;
    int huge *msw;
    int nmsw;

//    int deb;
//	deb = 1;

    TraceOn(nome);
    FnotR = CwIter = C_E_F = C_E_E = 0;

    long nnel = n_vecs * ELE_PER_VEC;
    msw = (int huge *) mMalloc(nnel * sizeof(int));
    if (!msw)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
    for (i = 0; i < nnel; i++) msw[i] = 0;
    nmsw = 0;

    for (face = 0; face < surf->GetNumElems(); face++)
        {
        acList ListElems(sizeof(long)); // Lista de elementos cercanos
        acNodeList *nl;
        int exists;
        long *pele, ele;
        elem *el;
        surf->GetInciFace(face, inciface);

        for (int kEdge=0; kEdge<3; kEdge++)
// Edge kEdge:
        {
        n1 = inciface[kEdge];
        n2 = inciface[(kEdge+1)%3];

        ele = an_element[n1];
        nmsw++;
        msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
        BuildList (n1, ele, msw, nmsw, ListElems);
        exists = FALSE;
        for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
            {
            pele = (long *) nl->GetDados();
            ele = *pele;
            el = p_elem(ele);
            if (el->n1 == n2 ||
                el->n2 == n2 ||
                el->n3 == n2 ||
                el->n4 == n2)
                exists = TRUE;
            }
        if (!exists)
            {

//fprintf(stdout, "\nRecovering %ld %ld\n", n1, n2);

            int retval, method;  // 0: recoverEd, 1: repeat, 2: fail.
            retval = Edge_Recover(n1, n2, ListElems, method);
            if      (method == 1) C_E_F++;
            else if (method == 2) C_E_E++;
            while (retval == 1)
                {
                CwIter++;
                ListElems.Clear();
                nmsw++;
                ele = an_element[n1];
		msw[ele] = nmsw;
                ListElems.InsertFirst(&ele);
                BuildList (n1, ele, msw, nmsw, ListElems);
                retval = Edge_Recover(n1, n2, ListElems, method);
                if      (method == 1) C_E_F++;
                else if (method == 2) C_E_E++;
                }
            if (retval)
                Error(WARNING, 0,
                    "\nEdge %ld-%ld Not Recovered!", n1, n2);
//if (deb) is_ok(1,1,0);
            }
        ListElems.Clear();
        } // End Loop Edges


        } // End Loop Faces

// Test for face existence
    for (face = 0; face < surf->GetNumElems(); face++)
        {
        acList ListElems(sizeof(long)); // Lista de elementos cercanos
        acNodeList *nl;
        int exists;
        long *pele, ele;
        elem *el;
        surf->GetInciFace(face, inciface);
        n1 = inciface[0];
        nmsw++;
        ele = an_element[n1];
	msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
        BuildList (n1, ele, msw, nmsw, ListElems);
        n1 = inciface[1];
        n2 = inciface[2];
        exists = FALSE;
        for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
            {
            pele = (long *) nl->GetDados();
            ele = *pele;
            el = p_elem(ele);
            if ((el->n1 == n1 ||
                 el->n2 == n1 ||
                 el->n3 == n1 ||
                 el->n4 == n1)  &&
                (el->n1 == n2 ||
                 el->n2 == n2 ||
                 el->n3 == n2 ||
                 el->n4 == n2))
                exists = TRUE;
            }
        if (!exists)
            {
	    printf("\nRecovering: %ld %ld %ld", inciface[0], n1, n2);
            FnotR +=
            Face_Recover(inciface[0], n1, n2, ListElems, msw, nmsw);
            }

        ListElems.Clear();

        }  // Next face

    mFree(msw);
    TraceOff(nome);
    return FnotR;
    }

void Dmesh3D::BuildList
    (long node, long ele, int huge *msw, int nmsw, acList &ListElems)
    {
    long v1, v2, v3;
    elem *el = p_elem(ele);

    if (node == el->n1)
        {
        v1 = el->v2;
        v2 = el->v3;
        v3 = el->v4;
        }
    else if (node == el->n2)
        {
        v1 = el->v1;
        v2 = el->v3;
        v3 = el->v4;
        }
    else if (node == el->n3)
        {
        v1 = el->v1;
        v2 = el->v2;
        v3 = el->v4;
        }
    else if (node == el->n4)
        {
        v1 = el->v1;
        v2 = el->v2;
        v3 = el->v3;
        }
    else
        {
        Error (FATAL_ERROR, 1,
            "BuildList, Element: %ld %ld %ld %ld, Node: %ld",
            el->n1, el->n2, el->n3, el->n4, node);
        }
    if (msw[v1] != nmsw)
        {
	msw[v1] = nmsw;
        ListElems.InsertFirst(&v1);
        BuildList(node, v1, msw, nmsw, ListElems);
        }
    if (msw[v2] != nmsw)
        {
	msw[v2] = nmsw;
        ListElems.InsertFirst(&v2);
        BuildList(node, v2, msw, nmsw, ListElems);
        }
    if (msw[v3] != nmsw)
        {
	msw[v3] = nmsw;
        ListElems.InsertFirst(&v3);
        BuildList(node, v3, msw, nmsw, ListElems);
        }
    }

int Dmesh3D::Edge_Recover(long n1, long n2, acList &ListElems,
        int &caso)
    {
    acNodeList *pl;
    long *pele, ele, elev;
    long fn1, fn2, fn3;
    long iaux;
    elem *el, *elv;
    int Case;  // 0: undefined, 1: face, 2: crossed edges.
    REAL xo, yo, zo, x1, x2, x3, y1, y2, y3, z1, z2, z3;
    REAL xn1, yn1, zn1, volt, vol1, vol2, vol3;
    REAL tol;
    FILE *fp;
    fp = GetMensFile();
//    fprintf(fp, " Recuperando arista: %ld %ld\n", n1, n2);

    tol = 0.00001;

    Case = 0;
    for (pl = ListElems.GetHead(); pl && !Case; pl = pl->next)
        {
        pele = (long *) pl->GetDados();
        ele = *pele;
        el = p_elem(ele);
        if (el->n1 == n1)  // Find opposite face
            { fn1 = el->n2; fn2 = el->n3; fn3 = el->n4; elev = el->v1; }
        else if (el->n2 == n1)
            { fn1 = el->n1; fn2 = el->n4; fn3 = el->n3; elev = el->v2; }
        else if (el->n3 == n1)
            { fn1 = el->n1; fn2 = el->n2; fn3 = el->n4; elev = el->v3; }
        else if (el->n4 == n1)
            { fn1 = el->n1; fn2 = el->n3; fn3 = el->n2; elev = el->v4; }
        elv = p_elem(elev);

// Chech edge <-> opposite face intersection
        xn1 = x[n1]; yn1 = y[n1]; zn1 = z[n1];
        x1 = x[fn1] - xn1; y1 = y[fn1] - yn1; z1 = z[fn1] - zn1;
        x2 = x[fn2] - xn1; y2 = y[fn2] - yn1; z2 = z[fn2] - zn1;
        x3 = x[fn3] - xn1; y3 = y[fn3] - yn1; z3 = z[fn3] - zn1;
        xo = x[n2]  - xn1; yo = y[n2]  - yn1; zo = z[n2]  - zn1;

        volt = x3 * (y1*z2 - z1*y2) +
               y3 * (z1*x2 - x1*z2) +
               z3 * (x1*y2 - y1*x2); // Volumen para comparacion

        vol1 = xo * (y1*z2 - z1*y2) +
               yo * (z1*x2 - x1*z2) +
               zo * (x1*y2 - y1*x2);   vol1 = vol1 / volt;
        if (vol1 < -tol) continue;  // Proximo elemento
        vol2 = xo * (y2*z3 - z2*y3) +
               yo * (z2*x3 - x2*z3) +
               zo * (x2*y3 - y2*x3);   vol2 = vol2 / volt;
        if (vol2 < -tol) continue;  // Proximo elemento
        vol3 = xo * (y3*z1 - z3*y1) +
               yo * (z3*x1 - x3*z1) +
               zo * (x3*y1 - y3*x1);   vol3 = vol3 / volt;
        if (vol3 < -tol) continue;  // Proximo elemento

        if (vol1 < tol)  // Caso cruce de aristas
            {
            Case = 2;
            break;
            }
        if (vol2 < tol)  // Caso cruce de aristas
            {
            iaux= fn1;
            fn1 = fn2;
            fn2 = fn3;
            fn3 = iaux;
            Case = 2;
            break;
            }
        if (vol3 < tol)  // Caso cruce de aristas
            {
            iaux= fn3;
            fn3 = fn2;
            fn2 = fn1;
            fn1 = iaux;
            Case = 2;
            break;
            }

        Case = 1;  // Case edge <-> face intersection

        }  // End loop n1's neighbours
    caso = Case;
/*
    if (Case == 1)
    fprintf(fp, " Caso cruce cara - arista\n");
    else
    fprintf(fp, " Caso cruce arista - arista\n");
*/

    if (Case == 1) // Edge <-> Face intersection
        {
        long nop, new_el;
        long v1f1, v1f2, v1f3, vof1, vof2, vof3;
        el->rot_log(n1, fn1, fn2);
        elv->rot_log(fn1, fn2, fn3);
        nop = elv->n4;
        v1f1 = el->v2;  v1f2 = el->v3;  v1f3 = el->v4;
        vof1 = elv->v1; vof2 = elv->v2; vof3 = elv->v3; 

        // Swap Face -> Edge
        new_el = AddElement();

        el->set            (n1,fn1,fn2,nop, vof3, elev,   new_el, v1f3);
        elv->set           (n1,fn2,fn3,nop, vof1, new_el, ele,    v1f1);
        p_elem(new_el)->set(n1,fn3,fn1,nop, vof2, ele,    elev,   v1f2);
        an_element[n1] = an_element[nop] = ele;
        an_element[fn1] = an_element[fn2] = ele;
        an_element[fn3] = elev;
        elem *pvv;
        pvv = p_elem(vof3);
        pvv->rot_log(fn1, fn2, nop);
        pvv->v4 = ele;
        pvv = p_elem(v1f3);
        pvv->rot_log(fn2, fn1, n1);
        pvv->v4 = ele;
        pvv = p_elem(vof1);
        pvv->rot_log(fn2, fn3, nop);
        pvv->v4 = elev;
        pvv = p_elem(v1f1);
        pvv->rot_log(fn3, fn2, n1);
        pvv->v4 = elev;
        pvv = p_elem(vof2);
        pvv->rot_log(fn3, fn1, nop);
        pvv->v4 = new_el;
        pvv = p_elem(v1f2);
        pvv->rot_log(fn1, fn3, n1);
        pvv->v4 = new_el;
// Check for element <-> anti-element annihilation
// and for negative volume elements
        if (el->v1 == el->v4)
            {
            el->rot_log(n1, nop, fn1);
            DelPair (ele, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }
        if (elv->v1 == elv->v4)
            {
            elv->rot_log(n1, nop, fn2);
            DelPair (elev, elv->v1);
            }
        else
            {
            if (elv->volume(x,y,z) <=0)
                {
                //Error(WARNING, 0, "Volume: %lf", elv->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        elv->n1, elv->n2, elv->n3, elv->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }
        el = p_elem(new_el);
        if (el->v1 == el->v4)
            {
            el->rot_log(n1, nop, fn3);
            DelPair (new_el, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <=0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }

//      fprintf (fp, "Se recupero arista: %ld %ld\n", n1, nop);
        if (nop != n2)
            return 1;
        else
            return 0;
        }
    else if (Case == 2)
        {
        long nop, nn;
        el->rot_log(n1, fn1, fn2);
        elv->rot_log(fn1, fn2, fn3);
        nop = elv->n4;
        // Cuenta tetraedros que concurren a la arista fn1, fn2
        long v_ele;
        elem *v_el;
        v_ele = el->v4;
        v_el = p_elem(v_ele);
        v_el->rot_log(n1, fn2, fn1);
        if (v_el->v1 == elv->v3) // 4 tetraedros
            {
//    fprintf(fp, " Caso cuatro tetraedros\n");
            long v_elev;
            elem *v_elv;
            long v11n, v1n2, v123, v131;
            long vo1n, von2, vo23, vo31;
            v_elev = elv->v3;
            v_elv = p_elem(v_elev);
            v_elv->rot_log(nop, fn1, fn2);
            nn = v_el->n4;
            v11n = v_el->v2;
            v1n2 = v_el->v3;
            v123 = el->v2;
            v131 = el->v3;
            vo1n = v_elv->v3;
            von2 = v_elv->v2;
            vo23 = elv->v1;
            vo31 = elv->v2;
            el->set    (n1, nop, fn3, fn1, vo31, v131, elev,   v_elev);
            elv->set   (n1, nop, fn1, nn,  vo1n, v11n, v_ele,  ele);
            v_el->set  (n1, nop, nn,  fn2, von2, v1n2, v_elev, elev);
            v_elv->set (n1, nop, fn2, fn3, vo23, v123, ele,    v_ele);
            an_element[n1] = an_element[nop] = ele;
            an_element[fn1] = an_element[nn] = elev;
            an_element[nn] = an_element[fn2] = v_ele;
            an_element[fn2] = an_element[fn3] = v_elev;
            an_element[fn3] = an_element[fn1] = ele;
            // Actualiza informacion de vecinos
            elem *pvv;
            pvv = p_elem(vo31);
            pvv->rot_log(nop, fn3, fn1);
            pvv->v4 = ele;
            pvv = p_elem(v131);
            pvv->rot_log(n1, fn1, fn3);
            pvv->v4 = ele;

            pvv = p_elem(vo1n);
            pvv->rot_log(nop, fn1, nn);
            pvv->v4 = elev;
            pvv = p_elem(v11n);
            pvv->rot_log(n1, nn, fn1);
            pvv->v4 = elev;

            pvv = p_elem(von2);
            pvv->rot_log(nop, nn, fn2);
            pvv->v4 = v_ele;
            pvv = p_elem(v1n2);
            pvv->rot_log(n1, fn2, nn);
            pvv->v4 = v_ele;

            pvv = p_elem(vo23);
            pvv->rot_log(nop, fn2, fn3);
            pvv->v4 = v_elev;
            pvv = p_elem(v123);
            pvv->rot_log(n1, fn3, fn2);
            pvv->v4 = v_elev;

//  Check element <-> anti-element annihilation
//  and for negative volumes
            if (el->v1 == el->v2)
                DelPair (ele, el->v1);
            else
                {
                if (el->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (elv->v1 == elv->v2)
                DelPair (elev, elv->v1);
            else
                {
                if (elv->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", elv->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        elv->n1, elv->n2, elv->n3, elv->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (v_el->v1 == v_el->v2)
                DelPair (v_ele, v_el->v1);
            else
                {
                if (v_el->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", v_el->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        v_el->n1, v_el->n2, v_el->n3, v_el->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (v_elv->v1 == v_elv->v2)
                DelPair (v_elev, v_elv->v1);
            else
                {
                if (v_elv->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", v_elv->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        v_elv->n1, v_elv->n2, v_elv->n3, v_elv->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }

//          fprintf (fp, "Se recupero arista: %ld %ld\n", n1, nop);
            if (nop != n2)
                return 1;
            else
                return 0;
            }
        else
            {
// Edge that must be removed has more than 4 tetrahedra
// Count # of tetrahedra
            int numt = 1;  // ele
            int posn2 = 0;
            elev = el->v1;
            nn = el->n4;
            do
                {
                numt++;
                elv = p_elem(elev);
                elv->rot_log(nn, fn1, fn2);
                nn = elv->n4;
                if (nn == n2) posn2 = numt;
                elev = elv->v1;
                } while (elev != ele);
            if (posn2 == 0)
                {
                Error(WARNING, 1, "\nNo encontro n2 en cluster");
                return 2;
                }

//    fprintf(fp, " Caso %d tetraedros\n", numt);
            long vfn1, vfn2; vfn1 = vfn2 = -2;
            long *tets = (long *) mMalloc(numt * sizeof(long));
            if (!tets)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

            numt = 1;  // ele
            tets[0] = ele;
            elev = el->v1;
            nn = el->n4;
            do
                {
                tets[numt] = elev;
                numt++;
                elv = p_elem(elev);
                elv->rot_log(nn, fn1, fn2);
                nn = elv->n4;
                elev = elv->v1;
                } while (elev != ele);

            Reg_Half_Cluster(posn2,      tets,       &vfn1, &vfn2);
            Reg_Half_Cluster(numt-posn2, tets+posn2, &vfn1, &vfn2);
//          fprintf (fp, "Se recupero arista: %ld %ld\n", n1, n2);

            mFree(tets);
            return 0;
            }
        }
    // Case ???????
    Error(WARNING, 1, "\nCase ???????");
    return 2;
    }  // End method Edge_Recover

void Dmesh3D::Reg_Half_Cluster (int numt, long *tets, long *v1,long *v2)
    {
    long fn1, fn2;
    elem *el;
    el = p_elem(tets[0]);
    fn1 = el->n2; fn2 = el->n3;
    FILE *fp = GetMensFile();

    while (numt > 3) // Try to reduce size
        {
        long *n, *v1, *v2;
        int i, posopt = 0;
        REAL x1,x2,x3, y1,y2,y3, z1,z2,z3;
        REAL vol1, vol2, vmin, volopt = -1.0e+30;
        n  = (long *) mMalloc ((numt+1) * sizeof(long));
        v1 = (long *) mMalloc ( numt    * sizeof(long));
        v2 = (long *) mMalloc ( numt    * sizeof(long));
        for (i = 0; i < numt; i++)
            {
            el = p_elem(tets[i]);
            n[i] = el->n1; v1[i] = el->v3; v2[i] = el->v2;
            }
        n[i] = el->n4;
        for (i = 1; i < numt; i++)
            {
            x1 = x[n[i-1]] - x[n[i]];
            y1 = y[n[i-1]] - y[n[i]];
            z1 = z[n[i-1]] - z[n[i]];
            x2 = x[n[i+1]] - x[n[i]];
            y2 = y[n[i+1]] - y[n[i]];
            z2 = z[n[i+1]] - z[n[i]];
            x3 = x[fn1] - x[n[i]];
            y3 = y[fn1] - y[n[i]];
            z3 = z[fn1] - z[n[i]];
            vol1 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
            x3 = -(x[fn2] - x[n[i]]);
            y3 = -(y[fn2] - y[n[i]]);
            z3 = -(z[fn2] - z[n[i]]);
            vol2 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
            if (v1[i-1] != v1[i])
                vmin = vol1;
            else
                vmin = 1.0e+20;
            if (v2[i-1] != v2[i])
                if (vol2 < vmin) vmin = vol2;
            if (vmin > volopt)
                {
                volopt = vmin;
                posopt = i;
                }
            }
// Genera par de tetraedros, actualiza vectores tets, n, v1 y v2
        long ele1, ele2;
        i = posopt;
        ele1 = tets[i-1];
        ele2 = AddElement();
        el = p_elem(ele1);
        el->set(n[i],n[i-1],n[i+1],fn1, tets[i],v1[i],v1[i-1],ele2);
        el = p_elem(v1[i]);
        el->rot_log(n[i+1], n[i], fn1);
        el->v4 = ele1;
        el = p_elem(v1[i-1]);
        el->rot_log(n[i], n[i-1], fn1);
        el->v4 = ele1;
        
        el = p_elem(ele2);
        el->set(n[i],n[i+1],n[i-1],fn2, tets[i],v2[i-1],v2[i],ele1);
        el = p_elem(v2[i]);
        el->rot_log(n[i], n[i+1], fn2);
        el->v4 = ele2;
        el = p_elem(v2[i-1]);
        el->rot_log(n[i-1], n[i], fn2);
        el->v4 = ele2;
        
        an_element[n[i]] = ele1;
        el = p_elem(tets[i]);
        el->n1 = n[i-1];
        el->v2 = ele2;
        el->v3 = ele1;
        while (i < numt)
            {
            tets[i-1] = tets[i];
            i++;
            }
        numt--;
        mFree(n);
        mFree(v1);
        mFree(v2);
// Check for element - anti-element annihilation
// and for negative volumes
        el = p_elem(ele1);
        if (el->v2 == el->v3)
            {
            el->rot_log(el->n2, el->n3, el->n1);
            DelPair (ele1, el->v2);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n[0],n[numt+1]);
                }
            }
        el = p_elem(ele2);
        if (el->v2 == el->v3)
            {
            el->rot_log(el->n2, el->n3, el->n1);
            DelPair (ele2, el->v2);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n[0],n[numt+1]);
                }
            }
        }

    if (numt == 3) // Case: Octahedron
        {
//
//                      fn1
//                      //\\
//                    / /  \ \
//                  /  /    \  \
//                n2- /- - - \- n3
//                 | /        \ |
//                 |/__________\|
//                n1\          /n4
//                    \      /
//                      \  /
//                       \/
//                      fn2
//
        long n1, n2, n3, n4;
        long v11, v12, v13, v14;
        long v21, v22, v23, v24;
        el = p_elem(tets[0]);
        n1 = el->n1; v11 = el->v3; v21 = el->v2;
        el = p_elem(tets[1]);
        n2 = el->n1; v12 = el->v3; v22 = el->v2;
        el = p_elem(tets[2]);
        n3 = el->n1; v13 = el->v3; v23 = el->v2;
        n4 = el->n4; v14 = *v1;    v24 = *v2;
        // Check for regeneration on edge n1 - n3:
        // Tetrahedra: n1 n3 n2 fn1, n1 n3 fn1 n4,
        //             n1 n3 n4 fn2, n1 n3 fn2 n2
        REAL vol, vm13, vm24;
        REAL x1,x2,x3, y1,y2,y3, z1,z2,z3;
        vm13 = 1.0e+25;
        x3 = x[n3]-x[n1];  y3 = y[n3]-y[n1];  z3 = z[n3]-z[n1];
        x1 = x[n2]-x[n1];  y1 = y[n2]-y[n1];  z1 = z[n2]-z[n1];
        x2 = x[fn1]-x[n1]; y2 = y[fn1]-y[n1]; z2 = z[fn1]-z[n1];
        if (v11 != v12)
          vm13 = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        else vm13 = 1.0e+25;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n4]-x[n1]; y2 = y[n4]-y[n1]; z2 = z[n4]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm13) vm13 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[fn2]-x[n1]; y2 = y[fn2]-y[n1]; z2 = z[fn2]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm13) vm13 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n2]-x[n1]; y2 = y[n2]-y[n1]; z2 = z[n2]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (v21 != v22 && vol < vm13) vm13 = vol;
        // Check for regeneration on edge n2 - n4:
        // Tetrahedra: n2 n4 n3 fn1, n2 n4 fn1 n1,
        //             n2 n4 n1 fn2, n2 n4 fn2 n3
        x3 = x[n4]-x[n2];  y3 = y[n4]-y[n2];  z3 = z[n4]-z[n2];
        x1 = x[n3]-x[n2];  y1 = y[n3]-y[n2];  z1 = z[n3]-z[n2];
        x2 = x[fn1]-x[n2]; y2 = y[fn1]-y[n2]; z2 = z[fn1]-z[n2];
        if (v12 != v13)
          vm24 = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        else vm24 = 1.0e+25;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n1]-x[n2]; y2 = y[n1]-y[n2]; z2 = z[n1]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm24) vm24 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[fn2]-x[n2]; y2 = y[fn2]-y[n2]; z2 = z[fn2]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm24) vm24 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n3]-x[n2]; y2 = y[n3]-y[n2]; z2 = z[n3]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (v22 != v23 && vol < vm24) vm24 = vol;
        long ele1, ele2, ele3, ele4;
        ele1 = tets[0]; ele2 = tets[1];
        ele3 = tets[2]; ele4 = AddElement();

        if (vm13 < vm24) // Regeneration on edge 2-4
            {
            el = p_elem(ele1);          // 1 elemento
            el->set(n2, n4, n3, fn1, v13, v12, ele2, ele4);
            an_element[n2] = an_element[n4] =
            an_element[n3] = ele1;
            el = p_elem(v13);
            el->rot_log(n3, fn1, n4);
            el->v4 = ele1;
            el = p_elem(v12);
            el->rot_log(n2, fn1, n3);
            el->v4 = ele1;

            el = p_elem(ele2);          // 2 elementos
            el->set(n2, n4, fn1, n1, v14, v11, ele3, ele1);
            an_element[fn1] = ele2;
            if (v14 == -2)
                *v1 = ele2;
            else
                {
                el = p_elem(v14);
                el->rot_log(n1, n4, fn1);
                el->v4 = ele2;
                }
            el = p_elem(v11);
            el->rot_log(n2, n1, fn1);
            el->v4 = ele2;

            el = p_elem(ele3);          // 3 elementos
            el->set(n2, n4, n1, fn2, v24, v21, ele4, ele2);
            an_element[n1] = ele3;
            if (v24 == -2)
                *v2 = ele3;
            else
                {
                el = p_elem(v24);
                el->rot_log(n1, fn2, n4);
                el->v4 = ele3;
                }
            el = p_elem(v21);
            el->rot_log(n1, n2, fn2);
            el->v4 = ele3;

            el = p_elem(ele4);          // 4 elementos
            el->set(n2, n4, fn2, n3, v23, v22, ele1, ele3);
            an_element[fn2] = ele4;
            el = p_elem(v23);
            el->rot_log(n3, n4, fn2);
            el->v4 = ele4;
            el = p_elem(v22);
            el->rot_log(n3, fn2, n2);
            el->v4 = ele4;
//      fprintf(fp, "Se genero arista: %ld %ld\n", n2, n4);
            }
        else // Regeneration on edge 1-3
            {
            el = p_elem(ele1);          // 1 elemento
            el->set(n1, n3, n2, fn1, v12, v11, ele2, ele4);
            an_element[n1] = an_element[n3] =
            an_element[n2] = ele1;
            el = p_elem(v11);
            el->rot_log(n1, fn1, n2);
            el->v4 = ele1;
            el = p_elem(v12);
            el->rot_log(n2, fn1, n3);
            el->v4 = ele1;

            el = p_elem(ele2);          // 2 elementos
            el->set(n1, n3, fn1, n4, v13, v14, ele3, ele1);
            an_element[fn1] = ele2;
            el = p_elem(v13);
            el->rot_log(n4, n3, fn1);
            el->v4 = ele2;
            if (v14 == -2)
                *v1 = ele2;
            else
                {
                el = p_elem(v14);
                el->rot_log(n1, fn1, n4);
                el->v4 = ele2;
                }

            el = p_elem(ele3);          // 3 elementos
            el->set(n1, n3, n4, fn2, v23, v24, ele4, ele2);
            an_element[n4] = ele3;
            el = p_elem(v23);
            el->rot_log(n3, n4, fn2);
            el->v4 = ele3;
            if (v24 == -2)
                *v2 = ele3;
            else
                {
                el = p_elem(v24);
                el->rot_log(n1, fn2, n4);
                el->v4 = ele3;
                }

            el = p_elem(ele4);          // 4 elementos
            el->set(n1, n3, fn2, n2, v22, v21, ele1, ele3);
            an_element[fn2] = ele4;
            el = p_elem(v22);
            el->rot_log(n2, n3, fn2);
            el->v4 = ele4;
            el = p_elem(v21);
            el->rot_log(n2, fn2, n1);
            el->v4 = ele4;
//      fprintf(fp, "Se genero arista: %ld %ld\n", n1, n3);
            }
//      int volneg = 0;
// Check for element - anti-element annihilation
        el = p_elem(ele1);
        if (el->v1 == el->v2)
            DelPair (ele1, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele2);
        if (el->v1 == el->v2)
            DelPair (ele2, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele3);
        if (el->v1 == el->v2)
            DelPair (ele3, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele4);
        if (el->v1 == el->v2)
            DelPair (ele4, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
            
        }
    else if (numt == 2)
//                      fn1
//                      /\\
//                     /  \ \
//                    /    \  \
//                   /      \   \
//                  /        \  /| n1
//              n3 /__________\/ |
//                 \        n2/ /
//                   \       / /
//                     \    / /
//                       \ //
//                      fn2
        {
        long ele1, ele2;
        long n1, n2, n3;
        long v11, v12, v13;
        long v21, v22, v23;
        el = p_elem(tets[0]);
        n1 = el->n1; v11 = el->v3; v21 = el->v2;
        el = p_elem(tets[1]);
        n2 = el->n1; v12 = el->v3; v22 = el->v2;
        n3 = el->n4; v13 = *v1;    v23 = *v2;

        ele1 = tets[0]; ele2 = tets[1];
        el = p_elem(ele1);
        el->set(n1, fn1, n3, n2, v12, ele2, v11, v13);
        an_element[n1] = an_element[fn1] =
        an_element[n3] = an_element[n2] = ele1;
        el = p_elem (v12);
        el->rot_log (n2, fn1, n3);
        el->v4 = ele1;
        el = p_elem (v11);
        el->rot_log (n1, fn1, n2);
        el->v4 = ele1;
        if (v13 == -2)
            *v1 = ele1;
        else
            {
            el = p_elem(v13);
            el->rot_log(n3, fn1, n1);
            el->v4 = ele1;
            }

        el = p_elem(ele2);
        el->set(n3, fn2, n1, n2, v21, ele1, v22, v23);
        an_element[fn2] = ele2;
        el = p_elem (v21);
        el->rot_log (n2, fn2, n1);
        el->v4 = ele2;
        el = p_elem (v22);
        el->rot_log (n3, fn2, n2);
        el->v4 = ele2;
        if (v23 == -2)
            *v2 = ele2;
        else
            {
            el = p_elem(v23);
            el->rot_log(n1, fn2, n3);
            el->v4 = ele2;
            }

        el = p_elem(ele1);
        if (el->v1 == el->v3)
            {
            el->rot_log(el->n1, el->n3, el->n4);
            DelPair (ele1, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n3);
                }
            }
        el = p_elem(ele2);
        if (el->v1 == el->v3)
            {
            el->rot_log(el->n1, el->n3, el->n4);
            DelPair (ele2, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n3);
                }
            }
        }
    return;
    }

void Dmesh3D::DelPair (long ele1, long ele2)
    {
// Elimina elementos ele1 y ele2, debido a que ele1->v1 = ele1->v2 =ele2
// Se elimina arista ele1->n3 -- ele1->n4
// Hay que conectar ele1->v3 con el vecino de ele2 opuesto a n3
// Hay que conectar ele1->v4 con el vecino de ele2 opuesto a n4
// Actualizar an_element
    elem *el1, *el2, *vs1, *vs2, *vi1, *vi2;
    long ves1, ves2, vei1, vei2;
    el1 = p_elem(ele1);
    ves1 = el1->v3; vei1 = el1->v4;
    el2 = p_elem(ele2);
    if (el2->n1 == el1->n3)
        ves2 = el2->v1;
    else if (el2->n1 == el1->n4)
        vei2 = el2->v1;
    if (el2->n2 == el1->n3)
        ves2 = el2->v2;
    else if (el2->n2 == el1->n4)
        vei2 = el2->v2;
    if (el2->n3 == el1->n3)
        ves2 = el2->v3;
    else if (el2->n3 == el1->n4)
        vei2 = el2->v3;
    if (el2->n4 == el1->n3)
        ves2 = el2->v4;
    else if (el2->n4 == el1->n4)
        vei2 = el2->v4;
    vs1 = p_elem(ves1);
    if      (vs1->v1 == ele1) vs1->v1 = ves2;
    else if (vs1->v2 == ele1) vs1->v2 = ves2;
    else if (vs1->v3 == ele1) vs1->v3 = ves2;
    else if (vs1->v4 == ele1) vs1->v4 = ves2;
    vs2 = p_elem(ves2);
    if      (vs2->v1 == ele2) vs2->v1 = ves1;
    else if (vs2->v2 == ele2) vs2->v2 = ves1;
    else if (vs2->v3 == ele2) vs2->v3 = ves1;
    else if (vs2->v4 == ele2) vs2->v4 = ves1;
    vi1 = p_elem(vei1);
    if      (vi1->v1 == ele1) vi1->v1 = vei2;
    else if (vi1->v2 == ele1) vi1->v2 = vei2;
    else if (vi1->v3 == ele1) vi1->v3 = vei2;
    else if (vi1->v4 == ele1) vi1->v4 = vei2;
    vi2 = p_elem(vei2);
    if      (vi2->v1 == ele2) vi2->v1 = vei1;
    else if (vi2->v2 == ele2) vi2->v2 = vei1;
    else if (vi2->v3 == ele2) vi2->v3 = vei1;
    else if (vi2->v4 == ele2) vi2->v4 = vei1;
// Actualiza an_element
    an_element[el1->n1] = 
    an_element[el1->n2] =
    an_element[el1->n3] = vei1;
    an_element[el1->n4] = ves1;
// Borra elementos ele1, ele2
    el2->n1 = firstfree;
    el2->n2 = -1;
    nel--;
    el1->n1 = ele2;
    el1->n2 = -1;
    nel--;
    firstfree = ele1;
    }

int Dmesh3D::Face_Recover(long n1, long n2, long n3, acList &ListElems,
    int huge *msw, int &nmsw)
    {
    int retval, changes;
    long ele;
    changes = 1;
    while (changes)
        {
        changes = 0;
        retval = 1;
        while (retval == 1)
            {
            retval = Face_Rec_Edge (n1, n2, n3, ListElems);
            if (!retval) return 0;
//          if (retval == 1) changes = 1;
            }
        ListElems.Clear();
        nmsw++;
        ele = an_element[n2];
	msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
        BuildList (n2, ele, msw, nmsw, ListElems);
        retval = 1;
        while (retval == 1)
            {
            retval = Face_Rec_Edge (n2, n3, n1, ListElems);
            if (!retval) return 0;
            if (retval == 1) changes = 1;
            }
        ListElems.Clear();
        nmsw++;
        ele = an_element[n3];
	msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
	BuildList (n3, ele, msw, nmsw, ListElems);
        retval = 1;
        while (retval == 1)
            {
            retval = Face_Rec_Edge (n3, n1, n2, ListElems);
            if (!retval) return 0;
            if (retval == 1) changes = 1;
            }
        }
    return 1;
    }

int Dmesh3D::Face_Rec_Edge(long n1, long n2, long n3, acList &ListElems)
    {
    acNodeList *nl;
    long *pel, ele;
    elem *el;
// Search for an element containing Edge n1 -> n2
    ele = -1;
    for (nl = ListElems.GetHead(); nl; nl = nl->next)
        {
        pel = (long *) nl->GetDados();
        ele = *pel;
        el = p_elem(ele);
        if (el->n1 == n2 || el->n2 == n2 ||
            el->n3 == n2 || el->n4 == n2)
            break;
        }
    if (ele == -1)
        Error (FATAL_ERROR, 1, "Face_Rec_Edge: not found edge %ld %ld",
            n1, n2);

// Rota "el" / el->n1 == n1 && el->n2 == n2

    if (el->n1 != n1 && el->n1 != n2)
        {
        el->rot_log(n1, n2, el->n1);
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        }
    else if (el->n2 != n1 && el->n2 != n2)
        {
        el->rot_log(n1, n2, el->n2);
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        }
    else if (el->n3 != n1 && el->n3 != n2)
        {
        el->rot_log(n1, n2, el->n3);
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        }

// Search for Edge intersecting face
    long nn;
    REAL x1,x2, y1,y2, z1,z2;
    REAL ax,ay,az, v3,v4;
    x1 = x[n2] - x[n1]; y1 = y[n2] - y[n1]; z1 = z[n2] - z[n1];
    x2 = x[n3] - x[n1]; y2 = y[n3] - y[n1]; z2 = z[n3] - z[n1];
    ax = y1*z2 - z1*y2; ay = z1*x2 - x1*z2; az = x1*y2 - y1*x2;
    while (1)
        {
        v3 = (x[el->n3]-x[n1]) * ax +
             (y[el->n3]-y[n1]) * ay +
             (z[el->n3]-z[n1]) * az;
        v4 = (x[el->n4]-x[n1]) * ax +
             (y[el->n4]-y[n1]) * ay +
             (z[el->n4]-z[n1]) * az;
        if (v3 < 0 && v4 > 0)
            break;
        nn = el->n4;
        ele = el->v3;
        el = p_elem(ele);
        el->rot_log(n1, n2, nn);
        }
    printf("\n Edge: %ld %ld", el->n3, el->n4);

// Check for 3 elements sharing this Edge (el->n3 - el->n4)
    long neigh1, neigh2;
    neigh1 = el->v1;
    neigh2 = el->v2;
    elem *neig1, *neig2;
    neig1 = p_elem(neigh1);
    neig1->rot_log(n2, el->n3, el->n4);
    if (neig1->v1 == neigh2) // 3 elements! => swap edge->face
        {
        long ve123, ve124, ve2n3, ve2n4, ven13, ven14;
        neig2 = p_elem(neigh2);
        nn = neig1->n4;
        neig2->rot_log(nn, el->n3, el->n4);
        ve123 = el->v4;    ve124 = el->v3;
        ve2n3 = neig1->v3; ve2n4 = neig1->v2;
        ven13 = neig2->v3; ven14 = neig2->v2;
        neig1->set(n1, n2, nn, el->n4, ve2n4, ven14, ve124, neigh2);
        neig2->set(n1, nn, n2, el->n3, ve2n3, ve123, ven13, neigh1);
        el->n1 = firstfree;
        el->n2 = -1;
        firstfree = ele;
        nel--;
        el = p_elem(ven14);
        el->rot_log(nn, n1, neig1->n4);
        el->v4 = neigh1;
        el = p_elem(ve124);
        el->rot_log(n1, n2, neig1->n4);
        el->v4 = neigh1;
        el = p_elem(ve2n4);
        el->rot_log(n2, nn, neig1->n4);
        el->v4 = neigh1;
        el = p_elem(ve123);
        el->rot_log(n2, n1, neig2->n4);
        el->v4 = neigh2;
        el = p_elem(ve2n3);
        el->rot_log(nn, n2, neig2->n4);
        el->v4 = neigh2;
        el = p_elem(ven13);
        el->rot_log(n1, nn, neig2->n4);
        el->v4 = neigh2;
        an_element[neig1->n1] = an_element[neig1->n2] =
        an_element[neig1->n3] = an_element[neig1->n4] = neigh1;
        an_element[neig2->n4] = neigh2;
        if (nn == n3)
            return 0;
        else
            return 1;
        }
    return 2;
    }

long Dmesh3D::AddElement(void)
    {
    long retval;
    if (firstfree >= n_vecs*ELE_PER_VEC) // Expandir vector de elementos
        {
        elem ** new_v;
        int i;
        new_v = (elem **) mMalloc ((n_vecs+1)*sizeof(elem *));
        if (!new_v)
            Error (FATAL_ERROR, 1, "Can't realloc elems");
        for (i=0; i<n_vecs; i++)
            new_v[i] = v_elem[i];
        mFree(v_elem);
        v_elem = new_v;
        v_elem[n_vecs] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
        if (!v_elem[n_vecs])
            Error (FATAL_ERROR, 1, "Ins. Memory (Dmesh3D::AddElem)");

        long j = n_vecs*ELE_PER_VEC;
        for (i = 0; i<ELE_PER_VEC; i++, j++)
            {
            v_elem[n_vecs][i].n1 = j+1;
            v_elem[n_vecs][i].n2 = -1;
            }

        n_vecs++;
        }
    retval = firstfree;
    firstfree = p_elem(firstfree)->n1;
    nel++;
    return (retval);
    }


