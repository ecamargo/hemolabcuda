// GeoElemSel.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "GeoElemSel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GeoElemSel dialog


GeoElemSel::GeoElemSel(CWnd* pParent /*=NULL*/)
	: CDialog(GeoElemSel::IDD, pParent)
{
	//{{AFX_DATA_INIT(GeoElemSel)
	m_gr = 0;
	m_g = 0;
	//}}AFX_DATA_INIT
    surf=0;
    m_elem=0;
}

void GeoElemSel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GeoElemSel)
	DDX_Text(pDX, IDC_EDIT_RADIUS, m_radius);
	DDX_Text(pDX, IDC_EDIT_X1, m_x);
	DDX_Text(pDX, IDC_EDIT_X2, m_nx);
	DDX_Text(pDX, IDC_EDIT_Y1, m_y);
	DDX_Text(pDX, IDC_EDIT_Y2, m_ny);
	DDX_Text(pDX, IDC_EDIT_Z1, m_z);
	DDX_Text(pDX, IDC_EDIT_Z2, m_nz);
	DDX_Radio(pDX, IDC_RADIO_COMPLETELY_INSIDE, m_inter);
	DDX_Text(pDX, IDC_SELNEL, m_selelem);
	DDX_Text(pDX, IDC_TOTNEL, m_totnel);
	DDX_Radio(pDX, IDC_RADIO_ALL, m_sel);
	DDX_Text(pDX, IDC_EDIT3, m_gr);
	DDX_Text(pDX, IDC_G, m_g);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GeoElemSel, CDialog)
	//{{AFX_MSG_MAP(GeoElemSel)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADDSEL, OnAddsel)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GeoElemSel message handlers

void GeoElemSel::OnAdd() 
{
	// TODO: Add your control notification handler code here
    if(!surf || !m_elem) return;
    UpdateData(true);
    surf->SelectElement(m_elem,m_sel,m_inter,1,m_g-1,
          m_x, m_y, m_z, m_nx, m_ny, m_nz, m_radius);	
    long nod,nel,gr;
    surf->Information(&nod,&nel,&gr);
    m_selelem=0;
    for (long el=0; el<nel; el++)
        if (m_elem[el]) m_selelem++;
    UpdateData(false);
}

void GeoElemSel::OnAddsel() 
{
	// TODO: Add your control notification handler code here
    if(!surf || !m_elem) return;
    UpdateData(true);
    surf->SetGroup(m_elem,m_gr-1);
    long nod,nel,gr;
    surf->Information(&nod,&nel,&gr);
    m_gr=gr+1;
    UpdateData(false);
}

void GeoElemSel::OnRemove() 
{
	// TODO: Add your control notification handler code here
    UpdateData(true);
    if(!surf || !m_elem) return;
    surf->SelectElement(m_elem,m_sel,m_inter,0,m_g-1,
          m_x, m_y, m_z, m_nx, m_ny, m_nz, m_radius);	
    long nod,nel,gr;
    surf->Information(&nod,&nel,&gr);
    m_selelem=0;
    for (long el=0; el<nel; el++)
        if (m_elem[el]) m_selelem++;
    UpdateData(false);
}
