// InputDouble.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "InputDouble.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InputDouble dialog


InputDouble::InputDouble(CWnd* pParent /*=NULL*/)
	: CDialog(InputDouble::IDD, pParent)
{
	//{{AFX_DATA_INIT(InputDouble)
	m_number = 0.0;
	m_text = _T("");
	//}}AFX_DATA_INIT
}


void InputDouble::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InputDouble)
	DDX_Text(pDX, IDC_EDIT1, m_number);
	DDX_Text(pDX, IDC_EDIT2, m_text);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InputDouble, CDialog)
	//{{AFX_MSG_MAP(InputDouble)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InputDouble message handlers
