#if !defined(AFX_INPUTDOUBLE_H__5CC1E320_985C_11D3_9D5B_0000E857BD8C__INCLUDED_)
#define AFX_INPUTDOUBLE_H__5CC1E320_985C_11D3_9D5B_0000E857BD8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputDouble.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// InputDouble dialog

class InputDouble : public CDialog
{
// Construction
public:
	InputDouble(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(InputDouble)
	enum { IDD = IDD_INPUTDOUBLE };
	double	m_number;
	CString	m_text;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InputDouble)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InputDouble)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTDOUBLE_H__5CC1E320_985C_11D3_9D5B_0000E857BD8C__INCLUDED_)
