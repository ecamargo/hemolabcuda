// InputText.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "InputText.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InputText dialog


InputText::InputText(CWnd* pParent /*=NULL*/)
	: CDialog(InputText::IDD, pParent)
{
	//{{AFX_DATA_INIT(InputText)
	m_text = _T("");
	m_fixtext = _T("");
	//}}AFX_DATA_INIT
}


void InputText::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InputText)
	DDX_Text(pDX, IDC_EDIT1, m_text);
	DDV_MaxChars(pDX, m_text, 255);
	DDX_Text(pDX, IDC_EDIT2, m_fixtext);
	DDV_MaxChars(pDX, m_fixtext, 255);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InputText, CDialog)
	//{{AFX_MSG_MAP(InputText)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InputText message handlers
