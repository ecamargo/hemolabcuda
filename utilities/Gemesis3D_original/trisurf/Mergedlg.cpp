// Mergedlg.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "Mergedlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Mergedlg dialog


Mergedlg::Mergedlg(CWnd* pParent /*=NULL*/)
	: CDialog(Mergedlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(Mergedlg)
	m_gr1 = 0;
	m_tol = 0.0;
	m_gr2 = 0;
	m_all = -1;
	//}}AFX_DATA_INIT
}


void Mergedlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Mergedlg)
	DDX_Text(pDX, IDC_EDIT2, m_gr1);
	DDX_Text(pDX, IDC_EDIT1, m_tol);
	DDX_Text(pDX, IDC_EDIT4, m_gr2);
	DDX_Radio(pDX, IDC_RADIO1, m_all);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Mergedlg, CDialog)
	//{{AFX_MSG_MAP(Mergedlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Mergedlg message handlers
