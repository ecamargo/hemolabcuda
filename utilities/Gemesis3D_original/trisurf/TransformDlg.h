#if !defined(AFX_TRANSFORMDLG_H__C2C23B61_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_)
#define AFX_TRANSFORMDLG_H__C2C23B61_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TransformDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// TransformDlg dialog

class TransformDlg : public CDialog
{
// Construction
public:
	TransformDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(TransformDlg)
	enum { IDD = IDD_TRANSFORM };
	double	m_tx;
	double	m_ty;
	double	m_tz;
	double	m_sx;
	double	m_sy;
	double	m_sz;
	double	m_rx;
	double	m_ry;
	double	m_rz;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TransformDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TransformDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSFORMDLG_H__C2C23B61_ABD1_11D3_9EAC_00104B9E2079__INCLUDED_)
