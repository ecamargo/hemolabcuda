// smoothdlg.cpp : implementation file
//

#include "stdafx.h"
#include "trisurf.h"
#include "smoothdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// smoothdlg dialog


smoothdlg::smoothdlg(CWnd* pParent /*=NULL*/)
	: CDialog(smoothdlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(smoothdlg)
	m_iter = 0;
	m_fac = 0.0;
	m_gr = 0;
	m_shrink = FALSE;
	//}}AFX_DATA_INIT
}


void smoothdlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(smoothdlg)
	DDX_Text(pDX, IDC_EDIT1, m_iter);
	DDV_MinMaxInt(pDX, m_iter, 1, 100);
	DDX_Text(pDX, IDC_EDIT2, m_fac);
	DDV_MinMaxDouble(pDX, m_fac, -2., 2.);
	DDX_Text(pDX, IDC_EDIT4, m_gr);
	DDV_MinMaxInt(pDX, m_gr, 0, maxgr);
	DDX_Check(pDX, IDC_CHECK1, m_shrink);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(smoothdlg, CDialog)
	//{{AFX_MSG_MAP(smoothdlg)
	ON_BN_CLICKED(IDC_CHECK1, Onshrink)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// smoothdlg message handlers

void smoothdlg::Onshrink() 
{
	// TODO: Add your control notification handler code here
	
}
