; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTrisurfView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "trisurf.h"
LastPage=0

ClassCount=12
Class1=CTrisurfApp
Class2=CTrisurfDoc
Class3=CTrisurfView
Class4=CMainFrame

ResourceCount=8
Resource1=IDR_MAINFRAME
Class5=CAboutDlg
Resource2=IDD_MERGE
Class6=InputDouble
Resource3=IDD_GEOELEMSEL
Class7=InputText
Resource4=IDD_ABOUTBOX
Class8=GeomElemSelection
Class9=GeoElemSel
Resource5=IDD_INPUTDOUBLE
Class10=smoothdlg
Resource6=IDD_INPUTTEXT
Class11=Mergedlg
Resource7=IDD_SMOOTHDLG
Class12=TransformDlg
Resource8=IDD_TRANSFORM

[CLS:CTrisurfApp]
Type=0
HeaderFile=trisurf.h
ImplementationFile=trisurf.cpp
Filter=N

[CLS:CTrisurfDoc]
Type=0
HeaderFile=trisurfDoc.h
ImplementationFile=trisurfDoc.cpp
Filter=N

[CLS:CTrisurfView]
Type=0
HeaderFile=trisurfView.h
ImplementationFile=trisurfView.cpp
Filter=C
BaseClass=CView
VirtualFilter=VWC
LastObject=ID_REMOVE_TRES


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame
BaseClass=CFrameWnd
VirtualFilter=fWC




[CLS:CAboutDlg]
Type=0
HeaderFile=trisurf.cpp
ImplementationFile=trisurf.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN_SUR
Command3=ID_FILE_OPEN_PAR
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_IMPORT
Command7=ID_FILE_EXPORT
Command8=ID_FILE_EDGES
Command9=ID_FILE_PRINT
Command10=ID_FILE_PRINT_PREVIEW
Command11=ID_FILE_PRINT_SETUP
Command12=ID_APP_EXIT
Command13=ID_MODIFY_SWAPP
Command14=ID_MODIFY_INSERT
Command15=ID_MODIFY_COLLAPSE
Command16=ID_MODIFY_SMOOTH
Command17=ID_MODIFY_DIVIDEOB
Command18=ID_REMOVE_TRES
Command19=ID_MODIFY_FREENODES
Command20=ID_MODIFY_TRANSFORM
Command21=ID_MODIFY_MERGE
Command22=ID_MODIFY_GROUPOR
Command23=ID_MODIFY_REMOVE_GROUP
Command24=ID_MODIFY_COPYGROUP
Command25=ID_MODIFY_ELSEL
Command26=ID_INFORM_TEST
Command27=ID_INFORM_INFO
Command28=ID_VIEW_PLOT
Command29=ID_VIEW_TOOLBAR
Command30=ID_VIEW_STATUS_BAR
Command31=ID_APP_ABOUT
CommandCount=31

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[TB:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN_SUR
Command3=ID_FILE_OPEN_PAR
Command4=ID_FILE_SAVE
Command5=ID_MODIFY_SWAPP
Command6=ID_MODIFY_INSERT
Command7=ID_MODIFY_COLLAPSE
Command8=ID_MODIFY_SMOOTH
Command9=ID_MODIFY_DIVIDEOB
Command10=ID_MODIFY_FREENODES
Command11=ID_PONETAPAS
Command12=ID_FILE_PRINT
Command13=ID_APP_ABOUT
CommandCount=13

[DLG:IDD_INPUTDOUBLE]
Type=1
Class=InputDouble
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_EDIT2,edit,1350568068

[CLS:InputDouble]
Type=0
HeaderFile=InputDouble.h
ImplementationFile=InputDouble.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_EDIT1

[DLG:IDD_INPUTTEXT]
Type=1
Class=InputText
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_EDIT2,edit,1484849284

[CLS:InputText]
Type=0
HeaderFile=InputText.h
ImplementationFile=InputText.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=ID_APP_ABOUT

[CLS:GeomElemSelection]
Type=0
HeaderFile=GeomElemSelection.h
ImplementationFile=GeomElemSelection.cpp
BaseClass=CDialog
Filter=D
LastObject=GeomElemSelection

[DLG:IDD_GEOELEMSEL]
Type=1
Class=GeoElemSel
ControlCount=32
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,button,1342177287
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_X1,edit,1350631552
Control7=IDC_EDIT_Y1,edit,1350631552
Control8=IDC_EDIT_Z1,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT_X2,edit,1350631552
Control13=IDC_EDIT_Y2,edit,1350631552
Control14=IDC_EDIT_Z2,edit,1350631552
Control15=IDC_STATIC,static,1342308352
Control16=IDC_EDIT_RADIUS,edit,1350631552
Control17=IDC_RADIO_COMPLETELY_INSIDE,button,1342373897
Control18=IDC_RADIO_INTERSECTING,button,1342242825
Control19=IDC_RADIO_ALL,button,1342373897
Control20=IDC_RADIO_HALF_SPACE,button,1342242825
Control21=IDC_RADIO_GROUP,button,1342242825
Control22=IDC_RADIO_SPHERE,button,1342242825
Control23=IDC_RADIO_CYLINDER,button,1342242825
Control24=IDC_REMOVE,button,1342242816
Control25=IDC_ADD,button,1342242816
Control26=IDC_ADDSEL,button,1342251008
Control27=IDC_EDIT3,edit,1350631552
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_TOTNEL,edit,1484849280
Control31=IDC_SELNEL,edit,1484849280
Control32=IDC_G,edit,1350631552

[CLS:GeoElemSel]
Type=0
HeaderFile=GeoElemSel.h
ImplementationFile=GeoElemSel.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_G

[DLG:IDD_SMOOTHDLG]
Type=1
Class=smoothdlg
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT1,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT2,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT4,edit,1350631552
Control9=IDC_CHECK1,button,1342242819

[CLS:smoothdlg]
Type=0
HeaderFile=smoothdlg.h
ImplementationFile=smoothdlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=ID_APP_ABOUT

[DLG:IDD_MERGE]
Type=1
Class=Mergedlg
ControlCount=12
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_RADIO1,button,1342308361
Control4=IDC_RADIO2,button,1342177289
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_EDIT1,edit,1350631552
Control8=IDC_STATIC,static,1342308352
Control9=IDC_EDIT2,edit,1350631552
Control10=IDC_STATIC,static,1342308352
Control11=IDC_EDIT4,edit,1350631552
Control12=IDC_STATIC,static,1342308352

[CLS:Mergedlg]
Type=0
HeaderFile=Mergedlg.h
ImplementationFile=Mergedlg.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_APP_ABOUT
VirtualFilter=dWC

[DLG:IDD_TRANSFORM]
Type=1
Class=TransformDlg
ControlCount=23
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT2,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT4,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT5,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_EDIT6,edit,1350631552
Control14=IDC_STATIC,static,1342308352
Control15=IDC_EDIT7,edit,1350631552
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT8,edit,1350631552
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308352
Control20=IDC_EDIT9,edit,1350631552
Control21=IDC_STATIC,static,1342308352
Control22=IDC_EDIT10,edit,1350631552
Control23=IDC_STATIC,static,1342308352

[CLS:TransformDlg]
Type=0
HeaderFile=TransformDlg.h
ImplementationFile=TransformDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=ID_APP_ABOUT

