// trisurfDoc.cpp : implementation of the CTrisurfDoc class
//

#include "stdafx.h"
#include "trisurf.h"

#include "trisurfDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrisurfDoc

IMPLEMENT_DYNCREATE(CTrisurfDoc, CDocument)

BEGIN_MESSAGE_MAP(CTrisurfDoc, CDocument)
	//{{AFX_MSG_MAP(CTrisurfDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrisurfDoc construction/destruction

CTrisurfDoc::CTrisurfDoc()
{
	// TODO: add one-time construction code here

}

CTrisurfDoc::~CTrisurfDoc()
{
}

BOOL CTrisurfDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTrisurfDoc serialization

void CTrisurfDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTrisurfDoc diagnostics

#ifdef _DEBUG
void CTrisurfDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTrisurfDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTrisurfDoc commands
