// trisurfDoc.h : interface of the CTrisurfDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRISURFDOC_H__F439F00A_9689_11D3_9D5B_0000E857BD8C__INCLUDED_)
#define AFX_TRISURFDOC_H__F439F00A_9689_11D3_9D5B_0000E857BD8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTrisurfDoc : public CDocument
{
protected: // create from serialization only
	CTrisurfDoc();
	DECLARE_DYNCREATE(CTrisurfDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrisurfDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTrisurfDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTrisurfDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRISURFDOC_H__F439F00A_9689_11D3_9D5B_0000E857BD8C__INCLUDED_)
