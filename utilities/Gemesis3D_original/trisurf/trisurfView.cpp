// trisurfView.cpp : implementation of the CTrisurfView class
//

#include "stdafx.h"
#include "trisurf.h"

#include <math.h>

#include "trisurfDoc.h"
#include "trisurfView.h"

#include "transformdlg.h"
#include "mergedlg.h"
#include "smoothdlg.h"
#include "GeoElemSel.h"
#include "InputDouble.h"
#include "InputText.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView

IMPLEMENT_DYNCREATE(CTrisurfView, CView)

BEGIN_MESSAGE_MAP(CTrisurfView, CView)
	//{{AFX_MSG_MAP(CTrisurfView)
	ON_COMMAND(ID_FILE_OPEN_SUR, OnFileOpenSur)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN_SUR, OnUpdateFileOpenSur)
	ON_COMMAND(ID_FILE_OPEN_PAR, OnFileOpenPar)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN_PAR, OnUpdateFileOpenPar)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_COMMAND(ID_MODIFY_MERGE, OnModifyMerge)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_MERGE, OnUpdateModifyMerge)
	ON_COMMAND(ID_MODIFY_SWAPP, OnModifySwapp)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_SWAPP, OnUpdateModifySwapp)
	ON_COMMAND(ID_MODIFY_INSERT, OnModifyInsert)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_INSERT, OnUpdateModifyInsert)
	ON_COMMAND(ID_MODIFY_COLLAPSE, OnModifyCollapse)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_COLLAPSE, OnUpdateModifyCollapse)
	ON_COMMAND(ID_MODIFY_DIVIDEOB, OnModifyDivideob)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_DIVIDEOB, OnUpdateModifyDivideob)
	ON_COMMAND(ID_MODIFY_FREENODES, OnModifyFreenodes)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_FREENODES, OnUpdateModifyFreenodes)
	ON_COMMAND(ID_MODIFY_SMOOTH, OnModifySmooth)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_SMOOTH, OnUpdateModifySmooth)
	ON_COMMAND(ID_INFORM_INFO, OnInformInfo)
	ON_UPDATE_COMMAND_UI(ID_INFORM_INFO, OnUpdateInformInfo)
	ON_COMMAND(ID_INFORM_TEST, OnInformTest)
	ON_UPDATE_COMMAND_UI(ID_INFORM_TEST, OnUpdateInformTest)
	ON_COMMAND(ID_MODIFY_GROUPOR, OnModifyGroupor)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_GROUPOR, OnUpdateModifyGroupor)
	ON_COMMAND(ID_MODIFY_ELSEL, OnModifyElsel)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_ELSEL, OnUpdateModifyElsel)
	ON_COMMAND(ID_FILE_IMPORT, OnFileImport)
	ON_UPDATE_COMMAND_UI(ID_FILE_IMPORT, OnUpdateFileImport)
	ON_COMMAND(ID_MODIFY_TRANSFORM, OnModifyTransform)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_TRANSFORM, OnUpdateModifyTransform)
	ON_COMMAND(ID_MODIFY_REMOVE_GROUP, OnModifyRemoveGroup)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_REMOVE_GROUP, OnUpdateModifyRemoveGroup)
	ON_COMMAND(ID_FILE_EXPORT, OnFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, OnUpdateFileExport)
	ON_COMMAND(ID_FILE_EDGES, OnFileEdges)
	ON_UPDATE_COMMAND_UI(ID_FILE_EDGES, OnUpdateFileEdges)
	ON_COMMAND(ID_PONETAPAS, OnPonetapas)
	ON_UPDATE_COMMAND_UI(ID_PONETAPAS, OnUpdatePonetapas)
	ON_COMMAND(ID_REMOVE_TRES, OnRemoveTres)
	ON_UPDATE_COMMAND_UI(ID_REMOVE_TRES, OnUpdateRemoveTres)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView construction/destruction

CTrisurfView::CTrisurfView()
{
	// TODO: add construction code here
    surf=NULL;
    mesh=NULL;
    mpar=NULL;
	CoplanarCote=1.0;
	ColapseCote=0.1;
    ObtuseCote=160.0;
}

CTrisurfView::~CTrisurfView()
{
}

BOOL CTrisurfView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView drawing

void CTrisurfView::OnDraw(CDC* pDC)
{
	CTrisurfDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView printing

BOOL CTrisurfView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTrisurfView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTrisurfView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView diagnostics

#ifdef _DEBUG
void CTrisurfView::AssertValid() const
{
	CView::AssertValid();
}

void CTrisurfView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTrisurfDoc* CTrisurfView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTrisurfDoc)));
	return (CTrisurfDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTrisurfView message handlers

void CTrisurfView::OnFileOpenSur() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(TRUE, "dir", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "CAD File (*.dxf)|*.dxf|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        
        strcpy(filename, dlg.GetPathName());

		FILE *fp = fopen(filename, "r");
		if (!fp) Error(FATAL_ERROR, 1, "can't open input file");

		mesh = new Mesh3d;
        int au=strlen(filename)-3;
        if(!strcmp(&filename[au],"dxf"))
    		mesh->DXFIn(fp);
        else
            mesh->Read(fp);
		fclose(fp);
    	surf = new SurfaceGen(mesh, mpar);
	    surf->SetCotaCopla(cos(CoplanarCote*3.141592/180.0));
	    surf->SetCotaColap(ColapseCote);

        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateFileOpenSur(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(mesh==0 && surf==0);    		
}

void CTrisurfView::OnFileOpenPar() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(TRUE, "dir", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Parameters File (*.spi)|*.spi|"
                    "CAD File (*.par)|*.par|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        char name[255];
        strcpy(name, dlg.GetPathName());

		FILE *fp = fopen(name, "r");
		if (!fp) Error(FATAL_ERROR, 1, "can't open input file");

        mpar = new ParamMesh;
        mpar->Read(fp);
        fclose(fp);
        mpar->BuildOctree();
        if (surf) surf->SetPar(mpar);

        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateFileOpenPar(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(mpar==0);    		
}

void CTrisurfView::OnFileNew() 
{
	// TODO: Add your command handler code here
    if (mesh) delete mesh;
    if (mpar) delete mpar;
    if (surf) delete surf;
//    if (graph)delete graph;
//    if (mesha)delete mesha;
    mesh = NULL;
    mpar = NULL;
    surf = NULL;
//    graph= NULL;
//    mesha= NULL;
}


void CTrisurfView::OnFileSave() 
{
	// TODO: Add your command handler code here
    HCURSOR wait_cursor, prev_cursor;
    wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
    prev_cursor = ::SetCursor(wait_cursor);

	FILE *fp = fopen(filename, "w");
	if (!fp) Error(FATAL_ERROR, 2, "can't open output file");

	surf->Print(fp);
	fclose(fp);

    ::SetCursor(prev_cursor);
}

void CTrisurfView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}

void CTrisurfView::OnFileSaveAs() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(FALSE, "sur", filename, OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        strcpy(filename, dlg.GetPathName());
    	FILE *fp = fopen(filename, "w");
	    if (!fp) Error(FATAL_ERROR, 2, "can't open output file");

	    surf->Print(fp);
	    fclose(fp);
        }
}

void CTrisurfView::OnFileImport() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(TRUE, "dir", NULL,
                    OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
                    "Surface File (*.sur)|*.sur|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        
        strcpy(filename, dlg.GetPathName());

		FILE *fp = fopen(filename, "r");
		if (!fp) Error(FATAL_ERROR, 1, "can't open input file");

		Mesh3d *mesha = new Mesh3d;
        mesha->Read(fp);
		fclose(fp);
    	surf->ImportSurface(mesha);
        delete mesha;

        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateFileImport(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}


void CTrisurfView::OnModifyMerge() 
{
	// TODO: Add your command handler code here
    acLimit3 box=surf->Box();
    double dx=box.xmax-box.xmin;
    double dy=box.ymax-box.ymin;
    double dz=box.zmax-box.zmin;
    Mergedlg dlg;
    dlg.m_all=0;
    dlg.m_gr1=1;
    dlg.m_gr2=1;
    dlg.m_tol=1.e-6*sqrt(dx*dx+dy*dy+dz*dz);
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        if(dlg.m_all==0)
        	surf->PegaNodos(dlg.m_tol);
        else
            {
            if (dlg.m_tol>0)
                {
                if (dlg.m_gr1>dlg.m_gr2)
                    {
                    surf->RemoveGroup(dlg.m_gr1);
                    surf->RemoveGroup(dlg.m_gr2);
                    }
                else
                    {
                    surf->RemoveGroup(dlg.m_gr2);
                    surf->RemoveGroup(dlg.m_gr1);
                    }
            	surf->PegaNodos(dlg.m_tol*dlg.m_tol);
                }
            else
                {
                surf->AddCapa(dlg.m_gr1,dlg.m_gr2);
                }
            }
        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateModifyMerge(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}

void CTrisurfView::OnModifySwapp() 
{
	// TODO: Add your command handler code here
    InputDouble dlg;
    dlg.m_number=CoplanarCote;
    dlg.m_text="If the angle between two elements normals\r\nis lower than the given value,\r\nthe diagonal could be swapped";
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        CoplanarCote=dlg.m_number;
        surf->SetCotaCopla(CoplanarCote);
        long k = surf->SwapAll();
        char buf[80];
        sprintf(buf,"%ld diagonals swapped",k);
        ::SetCursor(prev_cursor);
	    Error(WARNING, 1, buf);
        }
}

void CTrisurfView::OnUpdateModifySwapp(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}

void CTrisurfView::OnModifyInsert() 
{
	// TODO: Add your command handler code here
    double factorh=1.0;
    InputDouble dlg;
    dlg.m_number=factorh;
    dlg.m_text="Size multiplier";
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        factorh=dlg.m_number;
        surf->SetFactorH(factorh);
        long k = surf->DivideTodos();
        char buf[80];
        sprintf(buf,"%ld nodes inserted",k);
        ::SetCursor(prev_cursor);
	    Error(WARNING, 1, buf);
        }
}

void CTrisurfView::OnUpdateModifyInsert(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0 && mpar!=0);    		
}

void CTrisurfView::OnModifyCollapse() 
{
	// TODO: Add your command handler code here
    InputDouble dlg;
    dlg.m_number=sqrt(ColapseCote);
    dlg.m_text="Elements with size lower than\r\nthe given value are collapsed";
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        ColapseCote=dlg.m_number*dlg.m_number;
        surf->SetCotaColap(ColapseCote);
        long k=surf->ColapsaTodos();
        char buf[80];
        sprintf(buf,"%ld edges collapsed",k);
        ::SetCursor(prev_cursor);
	    Error(WARNING, 1, buf);
        }
}

void CTrisurfView::OnUpdateModifyCollapse(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0 && mpar!=0);    		
}

void CTrisurfView::OnModifyDivideob() 
{
	// TODO: Add your command handler code here
    InputDouble dlg;
    dlg.m_number=ObtuseCote;
    dlg.m_text="Elements with an angle greater than the given value will be divided";
    if (dlg.DoModal() == IDOK) 
        {
	    ObtuseCote=dlg.m_number;
        double cote  = cos(ObtuseCote*3.141592/180.0);
		long k = surf->DivideObtusos(cote);
        char buf[80];
        sprintf(buf,"%ld Elements divided",k);
	    Error(WARNING, 1, buf);
        }
}

void CTrisurfView::OnUpdateModifyDivideob(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}

void CTrisurfView::OnModifyFreenodes() 
{
	// TODO: Add your command handler code here
    char buf[255];
    long nod = surf->RemoveFreeNodes();
    sprintf(buf,"%ld nodes removed",nod);
    Error(WARNING, 0, buf);
}

void CTrisurfView::OnUpdateModifyFreenodes(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
}

void CTrisurfView::OnModifySmooth() 
{
	// TODO: Add your command handler code here
    smoothdlg dlg;
    long nodes,nelems,groups;
    surf->Information(&nodes,&nelems,&groups);

    dlg.maxgr=groups;
    dlg.m_gr=0;
    dlg.m_iter=1;
    dlg.m_fac=0.63;
    dlg.m_shrink=1;
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);

    	surf->Smooth(dlg.m_iter,dlg.m_fac,dlg.m_shrink,dlg.m_gr);

        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateModifySmooth(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnInformInfo() 
{
	// TODO: Add your command handler code here
    long nodes,nelems,groups;
    surf->Information(&nodes,&nelems,&groups);
    char bufaux[512];
    sprintf(bufaux,"Number of nodes   : %ld\n"
                   "Number of elements: %ld\n"
                   "Number of groups  : %ld\n",nodes,nelems,groups);
    Error(WARNING, 0, bufaux);
}

void CTrisurfView::OnUpdateInformInfo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnInformTest() 
{
	// TODO: Add your command handler code here
    long nod,nel;
    char bufaux[512];
    surf->TestMesh(nod,nel);
    sprintf(bufaux,"Number of nodes               : %ld\n"
                   "Number of elements          : %ld\n"
			       "Numb. of components-num. of holes : %ld\n",
			        nod,nel,(2*nod-nel)/4);
    Error(WARNING, 0, bufaux);
}

void CTrisurfView::OnUpdateInformTest(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnModifyGroupor() 
{
	// TODO: Add your command handler code here
    InputText dlg;
    dlg.m_text=" ";
    dlg.m_fixtext="Change the orientation\r\nof the groups specified\r\nMax:20";
    if (dlg.DoModal() == IDOK) 
        {
        int g[20];
        int num = sscanf(dlg.m_text,
        "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
        &g[0],&g[1],&g[2],&g[3],&g[4],&g[5],&g[6],&g[7],&g[8],&g[9],
        &g[10],&g[11],&g[12],&g[13],&g[14],&g[15],&g[16],&g[17],&g[18],&g[19]);
        for (int kk=0; kk<num; kk++)
            surf->SwapGroup(g[kk]);
        }
}

void CTrisurfView::OnUpdateModifyGroupor(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnModifyElsel() 
{
	// TODO: Add your command handler code here
    GeoElemSel dlg;
    long nod,nel,gr;
    surf->Information(&nod,&nel,&gr);
    long *els = (long *)mMalloc(sizeof(long)*nel);
    for (long i=0; i<nel; i++) els[i]=0;
    dlg.surf   = surf;
    dlg.m_elem = els;
    dlg.m_selelem=0;
    dlg.m_totnel=nel;
    dlg.m_x     = 0.0;
    dlg.m_y     = 0.0;
    dlg.m_z     = 0.0;
    dlg.m_nx    = 0.0;
    dlg.m_ny    = 0.0;
    dlg.m_nz    = 0.0;
    dlg.m_radius= 0.0;
    dlg.m_sel   = 0;
    dlg.m_inter = 1;
    dlg.m_g     = 1;
    dlg.m_gr    = gr+1;
    dlg.DoModal();
}

void CTrisurfView::OnUpdateModifyElsel(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnModifyTransform() 
{
	// TODO: Add your command handler code here
    TransformDlg dlg;
    dlg.m_tx=dlg.m_ty=dlg.m_tz=	0.0;
    dlg.m_sx=dlg.m_sy=dlg.m_sz=	1.0;
    dlg.m_rx=dlg.m_ry=dlg.m_rz=	0.0;
    if (dlg.DoModal() == IDOK)
        {
        surf->Transform(dlg.m_tx,dlg.m_ty,dlg.m_tz,
                        dlg.m_sx,dlg.m_sy,dlg.m_sz,
                        dlg.m_rx,dlg.m_ry,dlg.m_rz);
        }
}

void CTrisurfView::OnUpdateModifyTransform(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnModifyRemoveGroup() 
{
	// TODO: Add your command handler code here
    InputText dlg;
    long nn=0;
    dlg.m_text=" ";
    dlg.m_fixtext="Groups to be removed\r\nfrom the mesh. Max:20";
    if (dlg.DoModal() == IDOK) 
        {
        int g[20];
        int num = sscanf(dlg.m_text,
        "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
        &g[0],&g[1],&g[2],&g[3],&g[4],&g[5],&g[6],&g[7],&g[8],&g[9],
        &g[10],&g[11],&g[12],&g[13],&g[14],&g[15],&g[16],&g[17],&g[18],&g[19]);
        for (int k1=0; k1<num; k1++)
            for (int k2=k1+1; k2<num; k2++)
                if (g[k1]<g[k2])
                    {
                    int a = g[k1];
                    g[k1]=g[k2];
                    g[k2]=a;
                    }
        for (k1=0; k1<num; k1++)
            nn+=surf->RemoveGroup(g[k1]);
        char bufaux[128];
        sprintf(bufaux,"%ld nodes removed\n",nn);
        Error(WARNING, 0, bufaux);
        }
}

void CTrisurfView::OnUpdateModifyRemoveGroup(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnFileExport() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(FALSE, "sur", filename, OFN_HIDEREADONLY,
                    "Surface File (*.dat)|*.dat|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        
    	FILE *fp = fopen(dlg.GetPathName(), "w");
	    if (!fp) Error(FATAL_ERROR, 2, "can't open output file");

    	surf->PrintBoundary(fp);
	    fclose(fp);
        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateFileExport(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnFileEdges() 
{
	// TODO: Add your command handler code here
    CFileDialog dlg(FALSE, "sur", filename, OFN_HIDEREADONLY,
                    "Surface File (*.dat)|*.dat|"
                    "All Files (*.*)|*.*||");
    if (dlg.DoModal() == IDOK) 
        {
        HCURSOR wait_cursor, prev_cursor;
        wait_cursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);
        prev_cursor = ::SetCursor(wait_cursor);
        
    	FILE *fp = fopen(dlg.GetPathName(), "w");
	    if (!fp) Error(FATAL_ERROR, 2, "can't open output file");

    	surf->PrintEdges(fp);
	    fclose(fp);
        ::SetCursor(prev_cursor);
        }
}

void CTrisurfView::OnUpdateFileEdges(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
}

void CTrisurfView::OnPonetapas() 
{
	// TODO: Add your command handler code here
	surf->PoneTapas();	
}

void CTrisurfView::OnUpdatePonetapas(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);    		
	
}

void CTrisurfView::OnRemoveTres() 
{
	// TODO: Add your command handler code here
	surf->RemoveTresTri();	
	
}

void CTrisurfView::OnUpdateRemoveTres(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
    pCmdUI->Enable(surf!=0);
	
}
