// trisurfView.h : interface of the CTrisurfView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRISURFVIEW_H__F439F00C_9689_11D3_9D5B_0000E857BD8C__INCLUDED_)
#define AFX_TRISURFVIEW_H__F439F00C_9689_11D3_9D5B_0000E857BD8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "acdp.h"
#include "surfgen.h"
#include "malhapar.h"
#include "mesh3d.h"

class CTrisurfView : public CView
{
protected: // create from serialization only
	CTrisurfView();
	DECLARE_DYNCREATE(CTrisurfView)

// Attributes
public:
	CTrisurfDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrisurfView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTrisurfView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

SurfaceGen    *surf;
Mesh3d        *mesh;
ParamMesh     *mpar;
double ColapseCote,CoplanarCote,ObtuseCote;
char filename[255];

// Generated message map functions
protected:
	//{{AFX_MSG(CTrisurfView)
	afx_msg void OnFileOpenSur();
	afx_msg void OnUpdateFileOpenSur(CCmdUI* pCmdUI);
	afx_msg void OnFileOpenPar();
	afx_msg void OnUpdateFileOpenPar(CCmdUI* pCmdUI);
	afx_msg void OnFileNew();
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnFileSaveAs();
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnModifyMerge();
	afx_msg void OnUpdateModifyMerge(CCmdUI* pCmdUI);
	afx_msg void OnModifySwapp();
	afx_msg void OnUpdateModifySwapp(CCmdUI* pCmdUI);
	afx_msg void OnModifyInsert();
	afx_msg void OnUpdateModifyInsert(CCmdUI* pCmdUI);
	afx_msg void OnModifyCollapse();
	afx_msg void OnUpdateModifyCollapse(CCmdUI* pCmdUI);
	afx_msg void OnModifyDivideob();
	afx_msg void OnUpdateModifyDivideob(CCmdUI* pCmdUI);
	afx_msg void OnModifyFreenodes();
	afx_msg void OnUpdateModifyFreenodes(CCmdUI* pCmdUI);
	afx_msg void OnModifySmooth();
	afx_msg void OnUpdateModifySmooth(CCmdUI* pCmdUI);
	afx_msg void OnInformInfo();
	afx_msg void OnUpdateInformInfo(CCmdUI* pCmdUI);
	afx_msg void OnInformTest();
	afx_msg void OnUpdateInformTest(CCmdUI* pCmdUI);
	afx_msg void OnModifyGroupor();
	afx_msg void OnUpdateModifyGroupor(CCmdUI* pCmdUI);
	afx_msg void OnModifyElsel();
	afx_msg void OnUpdateModifyElsel(CCmdUI* pCmdUI);
	afx_msg void OnFileImport();
	afx_msg void OnUpdateFileImport(CCmdUI* pCmdUI);
	afx_msg void OnModifyTransform();
	afx_msg void OnUpdateModifyTransform(CCmdUI* pCmdUI);
	afx_msg void OnModifyRemoveGroup();
	afx_msg void OnUpdateModifyRemoveGroup(CCmdUI* pCmdUI);
	afx_msg void OnFileExport();
	afx_msg void OnUpdateFileExport(CCmdUI* pCmdUI);
	afx_msg void OnFileEdges();
	afx_msg void OnUpdateFileEdges(CCmdUI* pCmdUI);
	afx_msg void OnPonetapas();
	afx_msg void OnUpdatePonetapas(CCmdUI* pCmdUI);
	afx_msg void OnRemoveTres();
	afx_msg void OnUpdateRemoveTres(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in trisurfView.cpp
inline CTrisurfDoc* CTrisurfView::GetDocument()
   { return (CTrisurfDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRISURFVIEW_H__F439F00C_9689_11D3_9D5B_0000E857BD8C__INCLUDED_)
