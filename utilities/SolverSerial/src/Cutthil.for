
	Subroutine Cathill(IE,JE,IET,JET,NOlds,Nodos,NelT,NodT,NSteps)
C     Renumera una red por el metodo de Catleen-Mc Nee 
	Parameter (MCOM1=150)
      Integer ERR_ALLOC
	Dimension IE(1),JE(1),IET(1),JET(1),NOlds(1),Nodos(1),
     &          NodosIniciales(NSteps)
      Allocatable IA   (:)  
      Allocatable JA   (:)  
      Allocatable NCON (:)  
      Allocatable NEW  (:)  


      	LengthJE   = IE(NelT+1)-1
      	LengthJET  = LengthJE
	    LengthIA   = NodT + 1
C	    LengthNCON = MCON                         
          MCON = NodT

       PRINT *, "ALLOCATING NCON_Renum()"
      Allocate ( NCON (MCON), STAT = ERR_ALLOC ) 
       IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC
       PRINT *, "ALLOCATING IA_Renum(NodT+1)"
      Allocate ( IA (NodT+1), STAT = ERR_ALLOC ) 
       IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC
       PRINT *, "ALLOCATING New_Renum(NodT)"
      Allocate ( NEW (NodT), STAT = ERR_ALLOC ) 
       IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC

      DO I = 1,NodT
            Nodos(I)=I
      Enddo

      CALL TRASIM ( IE,JE,IET,JET,NodT,NelT )

	Call PreRIGIDEZ (IE,JE,IET,JET,IA,NOlds,NodT,iAnchoBanda,LengthJA)
C      Allocating JA(LengthJA)!!!!!!!!!!!!!!!!!!
          LengthJAMax=LengthJA

       PRINT *, "ALLOCATING JA_Renum(",LengthJA,")"
      Allocate ( JA (LengthJA), STAT = ERR_ALLOC ) 
       IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC


      CALL RIGIDEZ (IE,JE,IET,JET,IA,JA,NOlds,NodT)
      WRITE (*,*)
      WRITE (*,*) 'Initial BandWidth ',iAnchoBanda
      WRITE (*,*) 'Initial BandWidth ',iAnchoBanda
      WRITE (*,*) 'Initial BandWidth ',iAnchoBanda
      MinAncho = iAnchoBanda

      CALL NODOSINI ( IE,JE,IA,NodosIniciales,NSteps,NelT )


      NPmin=1
      DO 200 NP = 1,NSteps + 1
          IF ( NP .EQ. NSteps + 1 ) THEN
             NodoInicial = NodosIniciales(NPmin)
          ELSE   
             NodoInicial = NodosIniciales(NP)
          ENDIF   

          MCON = iAnchoBanda
          CALL RENUMBER  ( NodoInicial,NodT,NSteps,MCON,LengthJE,NEW,
     *                     NOlds,NCON,IA,JA,JE,NodosIniciales ) 
 
          CALL ACTUALIZA_Nodos ( Nodos,NEW,NodT )  

          CALL TRASIM ( IE,JE,IET,JET,NodT,NelT)

	Call PreRIGIDEZ (IE,JE,IET,JET,IA,NOlds,NodT,iAnchoBanda,LengthJA)
          IF(LengthJA.GT.LengthJAMAX) then
C          DeAllocating JA(LengthJA)!!!!!!!!!!!!!!!!!!
                      PRINT *, "DEALLOCATING JA_Renum()"
           Deallocate (JA) 
C          Allocating JA(LengthJA)!!!!!!!!!!!!!!!!!!
          LengthJAMax=LengthJA
                     PRINT *, "ALLOCATING JA_Renum(",LengthJA,")"
           Allocate ( JA (LengthJA), STAT = ERR_ALLOC ) 
                     IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC
          endif

          CALL RIGIDEZ ( IE,JE,IET,JET,IA,JA,NOlds,NodT )
          WRITE (*,*)
          WRITE (*,*) 'BandWidth',NP,iAnchoBanda
      
          IF ( iAnchoBanda .LT. MinAncho ) THEN
             NPmin = NP
             MinAncho = iAnchoBanda            
          ENDIF   

 200  CONTINUE
C
      Do I=1,NodT
          NOlds( Nodos(I) )=I
      Enddo

           Deallocate (  IA  ) 
           Deallocate (  JA  ) 
           Deallocate ( NCON ) 
           Deallocate ( NEW  ) 
	Write(*,*)'Renum End'
      Return
      End

C-----------------------------------------------------------------------
C    Encuentra los nodos de prueba 
C-----------------------------------------------------------------------
      SUBROUTINE NODOSINI( IE,JE,IA,NodosIniciales,NumeroPasos,NelT )
      INTEGER  IE(1),JE(1),IA(1),NodosIniciales(1),Elemento
      logical Segui
	KStep= NelT/NumeroPasos
! como agarro los elementos se supone que estos nodos van a estar conectados
! con algun otro
      DO I = 1,NumeroPasos
          Elemento =  (I-1) * KStep + 1
 123	    NodInic  =   JE (IE(Elemento))
	    NodosConectados = IA(NodInic+1)-IA(NodInic)
          if (NodosConectados < 1 ) then 
	           Elemento = Elemento + 1
                 if (Elemento .GT. NelT) Then
                          call RANDOM_SEED() 
                          call RANDOM_NUMBER(X) 
                          Elemento= MAX( NINT(X*NelT) ,1 )
                 End if 
                 goto 123
          endif
          NodosIniciales(I) = NodInic       
           
      EndDo
 
      WRITE(*,*)'Selection of nodes for RENUMBERING'
      WRITE(*,*)(NODOSINICIALES(I),I=1,NUMEROPASOS)
      RETURN
      END

C
	SUBROUTINE RIGIDEZ ( IE,JE,IET,JET,IA,JA,NAUX,NTOT )

	DIMENSION IE(1),JE(1),IET(1),JET(1),IA(1),JA(1),NAUX(1) 

C       NAUX => IP MultipleKey Vector to mark assembled columns	
      DO I=1,NTOT
         NAUX(I)=0
      Enddo

c	IBAN=0
	JP=1
	IA(1)=1
	DO 30 I=1,NTOT
       NAUX(I) = I
c       JA(JP)=I
c       JP=JP+1

       IETA=IET(I)
       IETB=IET(I+1)-1
          DO 20 IP=IETA,IETB
          J=JET(IP)
          IEA=IE(J)
          IEB=IE(J+1)-1
          DO 10 KP=IEA,IEB
              K=JE(KP)
c              IF(K.EQ.I)GO TO 10
              IF(NAUX(K).EQ.I)GO TO 10
              JA(JP)=K
              JP=JP+1
              NAUX(K)=I
c             IDIF=IABS(K-I)
c             IF(IDIF.GT.IBAN)IBAN=IDIF
 10        CONTINUE
 20   CONTINUE

 30   IA(I+1)=JP
	
      RETURN
      END

	SUBROUTINE PreRIGIDEZ ( IE,JE,IET,JET,IA,NAUX,NTOT,IBAN,Len )

	DIMENSION IE(1),JE(1),IET(1),JET(1),IA(1),NAUX(1) 
C       NAUX => IP MultipleKey Vector to mark assembled columns	
      DO I=1,NTOT
         NAUX(I)=0
      Enddo

      IBAN=0
      JP=1
      IA(1)=1
      DO 30 I=1,NTOT
       NAUX(I) = I
c       JP=JP+1

       IETA=IET(I)
       IETB=IET(I+1)-1
          DO 20 IP=IETA,IETB
          J=JET(IP)
          IEA=IE(J)
          IEB=IE(J+1)-1
          DO 10 KP=IEA,IEB
              K=JE(KP)
c              IF(K.EQ.I)GO TO 10
              IF(NAUX(K).EQ.I)GO TO 10
c              JA(JP)=K
              JP=JP+1
              NAUX(K)=I
              IDIF=IABS(K-I)
              IF(IDIF.GT.IBAN)IBAN=IDIF
 10        CONTINUE
 20   CONTINUE

 30   IA(I+1)=JP

      Len = IA(I)-1
	RETURN
	END

C-----------------------------------------------------------------------
	SUBROUTINE ACTUALIZA_Nodos ( Nodos,NEW,NodT )
C-----------------------------------------------------------------------
	DIMENSION Nodos(1),NEW(1) 
	
	DO Nod = 1,NodT
	    Nodo = Nodos (Nod)
	    Nodos (Nod) = NEW (Nodo)
      Enddo
	 
	RETURN
	END
C-----------------------------------------------------------------------
C     Renumeracion de Cathill-Mc Nee
C----------------------------------------------------------------------- 
	SUBROUTINE RENUMBER ( NodoInicial,NodT,NPasos,MCON,NJE,NEW,
     *                      NOlds,NCON,IA,JA,JE,NodosIniciales )
C     NJE = JE Length
C     NPasos = cantidad de nodos iniciales desde donde probar
C     New (I) = Nodo Viejo I tiene Nodo Nuevo New(I)
C     NOlds (I) = Nodo Nuevo I tiene Nodo Viejo  NOlds(I)

	INTEGER Contador
	DIMENSION  NEW(NodT),NOlds(NodT),NCON(1),IA(1),JA(1),JE(1),
     *           NodosIniciales(1)
	
 	NOlds = 0
	NEW   = 0

	NOlds (1)  =  NodoInicial
	New (NodoInicial) = 1
	Contador = 2

	CALL INIENT ( NCON,MCON,0 )

	Loop_Nodes: DO NodoNew = 1,NodT

	I = NOlds (NodoNew)
!     Cuando el nodo actual no tiene NOld
!     es porque es no-conectado y si encontre un no-conectado es porque
!      ya recorri todos los conectados (ojo que esto no renumera redes inconexas)
!      OBS: todo nodo de una malla "razonable" tiene al menos un conectado
!           como contador arranca en 2 y nodoNew en 1, en la vuelta actual
!           por lo menos llenare Nolds(Contador) y entonces tendre un nodo para la siguiente vuelta,
!           excepto que haya agotado todos los conectados
      IF ( I == 0  ) Exit Loop_Nodes
	IAA  = IA  (I)
	IAB  = IA (I+1) - 1

	MConMax=0
	DO  IP = IAA,IAB
!          agarro los nodos Conectados al I (por la conectividad de A) y los
!          ordeno por la cantidad de nodos que a su vez estan conectados a ellos
!          se ordenan en MCON(). Ejemplo Node=MCON(5) tiene 5 nodos conectados
	       J  =  JA (IP)
	       NodosConectados =  IA (J+1) - IA (J)
             if (NodosConectados .gt. MConMax)MConMax=NodosConectados
             NCON (NodosConectados) = J
      END DO
! Descomentado
!	If (MConMax.eq.0)then
!	    NOlds (Contador) = K
!	    New (K) = Contador
!	    Contador = Contador+1
!	    IF ( Contador .GT. Nodt ) Exit Loop_Nodes
!	endif

	DO 130 J = 1,MConMax
!      con J ordeno los nodos en NCON(J), es decir, Nodo=NCON(J) tiene J nodos conectados 
	    K = NCON(J)
	    IF (    K   == 0 ) CYCLE
	    IF ( New(K) == 0 ) Then
	                       NOlds (Contador) = K
	                       New (K) = Contador
	                       Contador = Contador+1
	                       IF ( Contador .GT. NodT ) Exit Loop_Nodes
          END IF 

          DO  IP = IAA,IAB
!         Este loop encuentra los nodos Tapados de orden igual al K que fueron enmascarados por el K en MCon(J)  
	        KK = JA (IP)
	        NodosConectados = IA (KK+1) - IA(KK)
	        IF ( NodosConectados  .NE. J ) CYCLE
	        IF (         KK       .EQ. K ) CYCLE
	        IF ( New (KK) .NE. 0 ) CYCLE
	        NOlds (Contador) = KK
	        New(KK) = Contador
	        Contador = Contador+1
	        IF ( Contador .GT. NodT ) Exit Loop_Nodes
          END DO

!         ya verifique el orden de conexion J, lo pongo en cero para el siguiente NodoNew
	    NCON(J)= 0
 
 130  CONTINUE

      END DO Loop_Nodes
!          110  CONTINUE


C      Los Nodos No-conectados se ponen aqui
      if ( Contador.LE.NodT )Then
        Do K=1,NodT
          IF ( New(K) .Eq. 0 ) then
              NOlds (Contador) = K
              New (K) = Contador
              Contador = Contador+1
              IF ( Contador .GT. NodT ) Exit
          endif
        Enddo
      Endif
	
      DO K = 1,NPasos
        KK = NodosIniciales (K)
        NodosIniciales (K) = New (KK)
      End do
C      WRITE(*,*)( NODOSINICIALES(I),I=1,NPASOS) 

      DO K     = 1,NJE
         KK    = JE (K)
         JE(K) = New (KK)
      End do

      RETURN
      END
C



