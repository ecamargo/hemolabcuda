
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Subroutine 
     & EscribirVtk(IE,JE,X,Sol,Time,DTm,nStep,nStepOut,Nodos,NOlds,
     &             NodT,iDofT,NelT,NDim,iElementType)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION Sol(1),IE(1),JE(1),X(1),iElementType(1)
	DIMENSION Nodos(1),NOlds(1)
      INTEGER, SAVE :: Istep, Nelint 

	Istep = Istep + 1
	IUnitVtK = 50

	If ( Istep .eq. 1) then
	do Nel=1,NelT
	     if(iElementType(Nel) .EQ. 1) NelInt = NelInt+1
      enddo
	  NelWin=NelT-NelInt
	  NBranch=NelWin-1
	  if(iElementType(1) .EQ. 1) NBranch=NBranch+1
! NData son todos los datos o valores que se escriben para definir cada Line
	  NData = 2*NBranch + NelInt
          OPEN ( IUnitVtK, FILE= 'HemoOut.xyz') 
          rewind IUnitVtK 
          Write (IUnitVtK,111)
 111      Format('# vtk DataFile Version 2.0')
          Write (IUnitVtK,*)"HemoMesh"
          Write (IUnitVtK,*)"ASCII"
          Write (IUnitVtK,*)"DATASET POLYDATA"
          Write (IUnitVtK,*)"POINTS ",NodT," float"
       SelectCase (NDim)
	  Case (1)
          do Nod=1, NodT
            NodN=Nodos(Nod)
            Write (IUnitVtK,113) X( NodN)," 0.0 0.0"	
	    enddo
	  Case(2)
        do Nod=1, NodT
         NodN=Nodos(Nod)
         Write(IUnitVtK,114)X((NodN-1)*NDim+1),X((NodN-1)*NDim+2)," 0.0"
	  enddo
	  Case(3)
          do Nod=1, NodT
           NodN=Nodos(Nod)
           Write (IUnitVtK,112)X((NodN-1)*NDim+1),X((NodN-1)*NDim+2),
     *                       X((NodN-1)*NDim+3)	
	    enddo
	    CASE DEFAULT
          Write (IUnitVtK,*)"ARE YOU A BIG MAN,NDIM >3"
	    STOP
       END SELECT
!#######################################################
      Write (IUnitVtK,*)
	Nel=2
      if(iElementType(1) .EQ. 1)Nel=1

      Write (IUnitVtK,*)"LINES",NBranch,NData
      do Nbr=1, NBranch
      	NelInic=Nel
          Do while (iElementType(Nel).NE.2)
	        Nel=Nel+1
	    enddo
!      Los vectores de VTk empiezan en cero, por eso restamos 1 a Je
        Write (IUnitVtK,117)
     &  Nel-NelInic+1,( NOlds(JE( IE(Neli) ))-1, Neli=NelInic, Nel-1)	
          Write (IUnitVtK,117)NOlds(JE( IE(Nel-1)+1 ))-1
          Nel=Nel+1
      enddo
 117  Format(10I6)
!#######################################################

          close (IUnitVtK)
          OPEN ( IUnitVtK, FILE='HemoOut.Q') 
          rewind IUnitVtK 
          Write (IUnitVtK,111)
          Write (IUnitVtK,*)"Hemo Solution"
          Write (IUnitVtK,*)"ASCII"
          Write (IUnitVtK,*)"POINT_DATA ",NodT

	else
          OPEN ( IUnitVtK, FILE='HemoOut.Q', POSITION= 'APPEND') 
      
	endif

      Write (IUnitVtK,100)nStep
 100  format('SCALARS',I7,'AREA FLOAT 1') 
      Write (IUnitVtK,*)"LOOKUP_TABLE DEFAULT"
      do Nod=1, NodT
        NodN=Nodos(Nod)
	  Soli = Sol( (NodN-1)*iDofT + 2 )
        if ( dabs(Soli) .lt. 1.0d-99 )Soli=0.0d0
        Write (IUnitVtK,112)Soli 	
	Enddo

      Write (IUnitVtK,101)nStep
 101  format('SCALARS',I7,'Q FLOAT 1') 
      Write (IUnitVtK,*)"LOOKUP_TABLE DEFAULT"
      do Nod=1, NodT
        NodN=Nodos(Nod)
	  Soli = Sol( (NodN-1)*iDofT + 1 )
        if ( dabs(Soli) .lt. 1.0d-99 )Soli=0.0d0
        Write (IUnitVtK,112)Soli 	
	Enddo

      Write (IUnitVtK,102)nStep
 102  format('SCALARS',I7,'PRESSURE FLOAT 1') 
      Write (IUnitVtK,*)"LOOKUP_TABLE DEFAULT"
      do Nod=1, NodT
        NodN=Nodos(Nod)
	  Soli = Sol( (NodN-1)*iDofT + 3 )
        if ( dabs(Soli) .lt. 1.0d-99 )Soli=0.0d0
        Write (IUnitVtK,112)Soli 	
	Enddo
 112  Format(3E16.7)
 113  Format(1E16.7,A8)
 114  Format(2E16.7,A4)
      close (IUnitVtK)


	return
	end
!\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
