!-----------------------------------------------------------------------
	REAL*8 FUNCTION AFP                                   &
            ( PT,AR,AC,AL,EL,ELCO,DF0,DFR,P0,             &
                               AREAINI,DT,VP1D,VP2D,iSW )
	IMPLICIT REAL*8 (A-H,O-Z)

	AN     = AR
	ITER   = 0
! NEWTON-RAPHSON PARA ENCONTRAR A(P)
 10   AP     = ( AN - AREAINI ) / DT 
	DPE    = DPEFA   ( AN,AC,AL,EL,ELCO,DF0,DFR,iSW )
	               
	DPV    = DPVFA (AN,AP,AC,AL,VP1D,VP2D,DT)
	X1     = 1.D0 / ( DPE + DPV )
	PELAS  = PEFA  ( AN,AC,AL,EL,ELCO,DF0,DFR,P0,iSW )
	PEVIS  = PVFA  ( AN,AP,AC,AL,VP1D,VP2D )
	PEST   = PELAS + PEVIS
	ANMAS1 = AN + ( PT - PEST ) * X1
	ERR    = DABS ( ANMAS1 - AN ) / AC
	AN     = ANMAS1
	ITER   = ITER + 1
	   IF (ITER.GT.200) THEN
	       WRITE (*,*)'EN AFP1 MAS DE 200 ITERACIONES'
	       STOP
	   ENDIF
	IF (ERR.GT.1.D-8) GOTO 10

	AFP   = ANMAS1

	RETURN
	END
!-----------------------------------------------------------------------
    REAL*8 FUNCTION PEFA (A,AC,RoCo2,EL,ELCO,DF0,DFR,P0,iSwitch)
    IMPLICIT REAL*8 (A-H,O-Z)
    real*8 K

!    RoCo2 = EL*AL/AC/2.0d0 

    Select Case (iSwitch)
	 Case (1)
!        NO lineal
	   A1    = 2.0d0*RoCo2* (AC/A)
	   ST    = ( DSQRT (A/AC) ) - 1.D0
	   A2    = ST
	   U     = (ST - DF0)/DFR
	   U     = DEXP ( U ) + 1.0D0
	   U     = DLOG ( U )
	   A3    = ELCO/EL * DFR * U
       Case (2)
! Lineal
	   A1    = 2.0d0*RoCo2 
	   ST    = ( DSQRT (A/AC) ) - 1.D0
	   A2    = ST
	   U     = (ST - DF0)/DFR
	   U     = DEXP ( U ) + 1.0D0
	   U     = DLOG ( U )
	   A3    = ELCO/EL * DFR * U

	 Case (3)
!        NO lineal P-Po= Ro Co^2 /B (A/Ao)^k((A/Ao)^B - 1)
!       AL=PI Ro Ho
        B = ElCO ;  K = DF0
!         R0    = DSQRT(AC/PI)
!         H0    = AL/PI/R0 
!         H0/R0 = AL/AC


	   ST    =  (A/AC)**B  - 1.D0
	   A1    = RoCo2 / B * (A/AC)**K 
	   A2    = ST

	   A3    = .0d0


      End Select


	   PEFA  = P0  +  A1 * ( A2 + A3 )   
        
	RETURN
	END
!-----------------------------------------------------------------------
REAL*8 FUNCTION DPEFA (A,AC,RoCo2,EL,ELCO,DF0,DFR,iSwitch)
    IMPLICIT REAL*8 (A-H,O-Z)
    real*8 K
    Select Case (iSwitch)
      Case (1)
!        NO lineal
	   A1  = 2.0d0 * RoCo2 * AC  / ( A * A ) 
	   ROOT   =  DSQRT (A/AC)  
	   ROOTS2 =  .5D0*ROOT
	   ST     = ROOT - 1.D0
	   A2     = 1.D0 - ROOTS2

	   U      = (ST - DF0)/DFR
	   EU     = DEXP ( U )
	   EU1    = EU + 1.0D0
	   EUSEU1 = EU/EU1
	   RLOEU1  = DLOG(EU1)
	   A3     = ELCO/EL * ( - DFR * RLOEU1 + EUSEU1 * ROOTS2 )
!------------------------------------------------------------
       Case (2)
! Lineal
	   A1  = 2.0d0*RoCo2 / A 
	   ROOT   =  DSQRT (A/AC)  
	   ROOTS2 =  .5D0*ROOT
	   A2     = ROOTS2

	   ST     = ROOT - 1.D0
	   U      = (ST - DF0)/DFR
	   EU     = DEXP ( U )
	   EU1    = EU + 1.0D0
	   EUSEU1 = EU/EU1
!	   RLOEU1  = DLOG(EU1)
	   A3     = ELCO/EL * EUSEU1 * ROOTS2

	 Case (3)
!        NO lineal P-Po= Ro Co^2 /B (A/Ao)^k((A/Ao)^B - 1)
!        DPA= Ro Co^2 /A (A/Ao)^k  ( (A/Ao)^B + k ((A/Ao)^B -1) /B )
       B = ElCO ; K = DF0
       If(dabs(B) < 1.d-5) then
           write(6,*)"WARNING: B exponent in P(A) law too small"
           stop
       Endif
       If(Dabs (K + 1.0d0) < 1.d-5 ) K=-.9999D0
       RA    = A/AC
       C1    = RA**B
       C2    = RA**K
	   ST    =  C1 - 1.D0
	   A1    = RoCo2 /A * C2 

	   A2    = C1
	   A3    = K*ST / B

      End Select


    DPEFA = A1 * ( A2 + A3 )

	RETURN
	END
!-----------------------------------------------------------------------
 Real*8 Function DPAref ( A,AC,RoCo2,EL,ELCO,DF0,DFR,iSw )
    IMPLICIT REAL*8 (A-H,O-Z)
    Real*8 K

    Select case (iSw)

	 Case (1)
!        NO lineal
	   A1    = 2.0d0*RoCo2 / A
       RooT  = DSQRT (A/AC)
	   ST    = RooT  - 1.D0

	   U     = (ST - DF0)/DFR
	   EU    = DEXP ( U )
	   rLogU = DLOG ( EU + 1.0d0)

	   A2    = ST + ELCO/EL * DFR * rLogU
	   A3    = -.5d0*RooT*(1.0d0 + ELCO/EL * (EU/(EU+1.0d0))  )
       Case (2)
! Lineal
	   A1    = 2.0d0*RoCo2 / AC
       RooT  = DSQRT (A/AC)
	   ST    = RooT  - 1.D0

	   U     = (ST - DF0)/DFR
	   EU    = DEXP ( U )
	   rLogU = DLOG ( EU + 1.0d0)

	   A2    = .0d0
	   A3    = -.5d0*RooT*(1.0d0 + ELCO/EL * (EU/(EU+1.0d0))  )


      case(3)
      B=ELCO
      K=DF0
!        NO lineal P-Po= Ro Co^2 /B (A/Ao)^k((A/Ao)^B - 1)
!         AL=PI Ro Ho
!         R0    = DSQRT(AC/PI)
!         H0    = AL/PI/R0 
!         H0/R0 = AL/Ao

       If(dabs(B) < 1.d-5) then
           write(6,*)"WARNING: B exponent in P(A) law too small"
           stop
       Endif
       RA    = A/AC
       CB    = RA**B
       ST    =  CB - 1.D0
       A1    =  -RoCo2 / B * (RA**K)/AC
       A2    =  K*ST
       A3    =  B*CB
      Case default
       A1    =  .0d0
       A2    =  .0d0
       A3    =  .0d0
    Endselect

   DPAref  = A1*(A2+A3)

 Return
End

!-----------------------------------------------------------------------      
 REAL*8 FUNCTION D2PA (A,AC,AL,EL,ELCO,DF0,DFR)
!        
	IMPLICIT REAL*8 (A-H,O-Z)
!
!ojo ya no sirve AL=roCo2
	   A1     =  AL  /  ( A * A * A ) 
	   ROOT   =  DSQRT (A/AC)  
	   ST     = ROOT - 1.D0
	   A2     = EL * ( - 2.0D0 + .75D0 * ROOT )
!
	   U      = (ST - DF0)/DFR
	   EU     = DEXP ( U )
	   EU1    = EU + 1.0D0
	   EUSEU1 = EU/EU1
	   RLOEU1  = DLOG(EU1)
	   A3     = ELCO * ( 2.D0*DFR*RLOEU1 + .25D0*EUSEU1 *ROOT*  &
                         ( - 5.D0 + ROOT * (1.D0-EUSEU1) / DFR )  )

	   D2PA = A1 * ( A2 + A3 )

	RETURN
	END 
!-----------------------------------------------------------------------
REAL*8 FUNCTION PVFA (  A , AP , AC , roCo2 , VP1 , VP2 )
        
	IMPLICIT REAL*8 (A-H,O-Z)

	   DEN    = 2.0d0*DSQRT ( A*AC )
	   Epunto = AP/DEN
	   VP2M1 = VP2 - 1.0D0
           
          AbsEpunto = DABS(Epunto)
       if (ABS(VP2-1.0E0) .LT. 1.E-4) then 
	  POT=1.0d0
       else  
	   IF (AbsEpunto.LT.1.D-13) THEN
		POT = 0.0D0
	   ELSE
		POT = AbsEpunto** VP2M1
	   ENDIF
       Endif
           
	   PVFA = 2.0d0*roCo2*AC/A*VP1*POT*Epunto 
        
	RETURN
	END
!-----------------------------------------------------------------------
REAL*8 FUNCTION DPVFA (  A , AP , AC , roCo2 , VP1 , VP2 , DT )
        
    IMPLICIT REAL*8 (A-H,O-Z)
	   DEN    = 2.0d0*DSQRT ( A*AC )
	   Epunto = AP/DEN
	   VP2M1 = VP2 - 1.0D0
           
          AbsEpunto = DABS(Epunto)
       if (ABS(VP2-1.0E0) .LT. 1.E-4) then 
	  POT=1.0d0
       else  
	   IF (AbsEpunto.LT.1.D-13) THEN
		POT = 0.0D0
	   ELSE
		POT = AbsEpunto** VP2M1
	   ENDIF
       Endif
	   DPVFA = 2.0d0*roCo2*AC/A*VP1*POT/DEN/DT

	RETURN
	END
!-----------------------------------------------------------------------
REAL*8 FUNCTION Cvis (  A , AP , AC , roCo2 , VP1 , VP2 )
        
	IMPLICIT REAL*8 (A-H,O-Z)

	   DEN    = 2.0d0*DSQRT ( A*AC )
	   Epunto = AP/DEN
	   VP2M1 = VP2 - 1.0D0
           
       AbsEpunto = DABS(Epunto)
       if (ABS(VP2-1.0E0) .LT. 1.E-4) then 
	        POT=1.0d0
       else  
	   IF (AbsEpunto.LT.1.D-13) THEN
		POT = 0.0D0
	   ELSE
		POT = AbsEpunto** VP2M1
	   ENDIF
       Endif
           
	   Cvis = 2.0d0*roCo2*AC/A*VP1*POT/DEN 
        
	RETURN
	END
!-----------------------------------------------------------------------
REAL*8 FUNCTION  FP (A,Q,RO,DPEA,Alfa)
      IMPLICIT REAL*8 (A-H,O-Z)
	   APA  = A * DPEA / RO 
	   QoA  = Q / A
	   RAPA = DSQRT (Alfa * (Alfa-1.0d0) *QoA*QoA + APA )
	   FP   = Alfa * QoA   +  RAPA
RETURN
END
!-----------------------------------------------------------------------
REAL*8 FUNCTION  FM (A,Q,RO,DPEA,Alfa)
      IMPLICIT REAL*8 (A-H,O-Z)
	   APA  = A * DPEA / RO 
	   QoA  = Q / A
	   RAPA = DSQRT (Alfa * (Alfa-1.0d0) *QoA*QoA + APA )
	   FM   = Alfa * QoA   -  RAPA
RETURN
END
!-----------------------------------------------------------------------
REAL*8 FUNCTION DFA ( A,Q,RO,DPA1,D2PA1,F )
	IMPLICIT REAL*8 (A-H,O-Z)
!
	USA   = 1.D0/A
	QSA   = Q * USA
	A1    = - QSA * USA
	B1    = 0.5D0 / (  ( F - QSA )  *   RO )
	C1    = A * D2PA1 + DPA1
!
	DFA   = A1 + B1 * C1
!
RETURN
END 
!-----------------------------------------------------------------------
REAL*8 FUNCTION G (A,Q,AC,AL,EL,CK,PC,RO,PE,PV,F,ELCO,DF0,DFR,P0)
	IMPLICIT REAL*8 (A-H,O-Z)
!	Parameter (PI = 3.141592653589793238D0)
!
!        DELPFA = PE - P0
!
    G1  = - AL / RO 
!	G11   = G1 * (G2+G3+G4+G5+G6)
!	G22   = - CMU *  Q / A 
	G11   = 0.0d0
	G22   = 0.0d0 
	G33   = CK * (PE + PV - PC) * F

	G     = G11 + G22 + G33 

RETURN
END

!-----------------------------------------------------------------------      
 Real*8 Function Friction (V,A,Vis)
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter (PI = 3.141592653589793238D0)

    If ( Vis .NE. 0.0d0 ) then
                                  Rey = V * 2 * Dsqrt (A/PI)/ Vis
      Else
                                  Rey = 0.0d0
    Endif

    If ( Rey .NE. 0.0d0 ) then
                                  Friction = 64.0d0/Rey 
      Else
                                  Friction = 0.0d0 
    Endif

  Return
 End
!-----------------------------------------------------------------------      
      Real*8 Function DPA (A,Aref,Alfa)
      IMPLICIT REAL*8 (A-H,O-Z)

      AlfAsA = Alfa / A
      RootA = Dsqrt (A / Aref)

      DPA = 0.5d0*AlfAsA/RootA/Aref - AlfAsA / A * (RootA-1.0d0)

      Return
      End
!-----------------------------------------------------------------------      
      Real*8 Function PotA (A,Ao,Co2,B,K)
      IMPLICIT REAL*8 (A-H,O-Z)
      Real*8 K,K1
!        Select Case (iSw)
!          Case (2)
            K1   = K + 1.0d0
            BK1  = B + K1
            AR   = A/Ao
            PotA1= (AR**BK1-1.0d0)* (B+K)/BK1
            if (K /= -1.0d0) then
            PotA1 = PotA1 - K/K1*(AR**K1-1.0d0)
            else
            PotA1 = PotA1 - K*Log(AR)
            endif
            PotA = PotA1*Ao*Co2/B
!        endselect
      Return
      End
!-----------------------------------------------------------------------      
      Real*8 Function DPAlfA (A,Aref,Alfa)
      IMPLICIT REAL*8 (A-H,O-Z)
          RootA = Dsqrt (A / Aref)
!          DPAlfa = (RootA - 1.0d0)/A 
          DPAlfa = (RootA - 1.0d0) 
      Return
      End
!-----------------------------------------------------------------------      
      Real*8 Function PTer_F ( T , Nf , Curve )
      IMPLICIT REAL*8 (A-H , O-Z)
!      Parameter (PI = 3.141592653589793238D0)
      DIMENSION Curve( 2 , Nf )
      INTEGER , SAVE :: IT           
      IT = 2
!      if (IT ==0)IT=2
!      Select Case (Nf)
!	Case (1)
!          Tau = 1.0d0
!	if (T .lt. Tau) then
!          PTer_F = ( 1.0d0 + dsin(2.0d0*pi*T/Tau - pi/2.0d0) )*1.0d4
!      else
!          PTer_F = .0d4
!      endif
!       PTer_F = 1.0d4

!      Case DefaulT
!	    PTer_F = 0.0d0

!      End Select

!        TAU  = Curve(1,NF)-Curve(1,1)
      TAU  = Curve(1,NF)
      if (T.LE.TAU)then
            X2  = T
      Else
            X2  = DMOD(T,TAU)
            if (X2.GT.TAU) X2=TAU
      endif

      if (X2 .LT. Curve(1,IT-1) ) IT=2 	
      DO WHILE ( X2.GT. Curve(1,IT) )
              	IT=IT+1
      END DO

      DX2    = X2 - Curve(1,IT-1)
      PTer_F = RECTA (Curve(1,IT-1),Curve(1,IT),Curve(2,IT-1),Curve(2,IT),DX2)

      Return
      End
!-----------------------------------------------------------------------      
    REAL*8 FUNCTION RECTA (X1,X2,Y1,Y2,DL)
    IMPLICIT REAL*8 (A-H,O-Z)
        P = ( Y2 - Y1 ) / ( X2 - X1 )
        RECTA = Y1 + P * DL
    RETURN
    END

!____Pablo y Sebastian
    REAL*8 FUNCTION DResAA (DPA,PFA,D2PA,P,DT)		
      IMPLICIT REAL*8 (A-H,O-Z)
       D1 = DPA / DT * DPA
       D2 = ( P - PFA ) * D2PA
       DResAA = D1 + D2  	
    RETURN
    END
!___________________________________

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 SubRoutine ProductoVectorial ( XA , XB , Vn , Modulo ) 
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 Real*8 XA(3) , XB(3) , Vn(3), Modulo
          Vn(1)  =   XA(2) * XB(3) - XA(3) * XB(2)
          Vn(2)  = - XA(1) * XB(3) + XA(3) * XB(1)
          Vn(3)  =   XA(1) * XB(2) - XA(2) * XB(1)
          Modulo = DSQRT( Vn(1)*Vn(1) + Vn(2)*Vn(2) + Vn(3)*Vn(3) )  
!          Modulo    = DSQRT( DOT_PRODUCT (Vn, Vn) )  
 Return
 End
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
