!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
      SubRoutine ReadLibrary(iElementLib,CommonPar,iNames,rNorm,Tol,	
     &  iXupdate,
     &  Sym,Iterative,MaxIter,iSolverType,SRP,ITMAXSOL,
     &  EPS,Krylov,LFIL,TolPre,
     &  iDofT,MaxElemLib,MaxLCPar,Ndim,Lch,IUnit)
!	Parameter (Lch=16)
	Character(Lch) Names(iDofT)
	dimension iNames(Lch*iDofT),iElementLib(MaxElemLib,1),iXupdate(1)
	Real*8 rNorm(1),Tol(1),CommonPar(MaxLCPar,1)
	Real*8 SRP,EPS,TolPre
	Logical Sym,Iterative

       Read (IUnit,*)Sym,Iterative,MaxIter,SRP,iSolverType
      	Select CASE (iSolverType)
	      case (-1,0)
	      Case (1,2,3)
             READ(IUnit,*) ITMAXSOL,EPS
	      Case (4,5,6)
             Read (IUnit,*)ITMAXSOL,EPS,Krylov
	      Case (100:120)
             Read (IUnit,*)ITMAXSOL,EPS,Krylov,LFIL,TolPre
     	      CASE Default
             Write (6,*)"Invalid Solver Type"
		   Stop
	    end select

!      Read (IUnit,*) ((iElementLib(L,I), I=1 ,   5    ),L=1,MaxElemLib)
!      Read (IUnit,*) ((CommonPar(I,L)  , I=1 ,MaxLCPar),L=1,MaxElemLib)
      Do L=1,MaxElemLib
       Read (IUnit,*) (iElementLib(L,I), I=1 , 5 )
      End do
      Do L=1,MaxElemLib
       Read (IUnit,*) (CommonPar(I,L)  , I=1 ,MaxLCPar)
      End do

      Read (IUnit,*) ( rNorm(N)    , N=1 , iDofT )
      Read (IUnit,*) ( Tol(N)      , N=1 , iDofT )
      Read (IUnit,*) ( Names(N)    , N=1 , iDofT )
      Read (IUnit,*) ( iXupdate(N) , N=1 , Ndim )

! convierto a integer los nombres de las variables
      do i=1,iDofT
	do j=1,Lch
	iNames(j+Lch*(i-1)) =IACHAR (Names(i)(j:j))
	enddo
	enddo

      RETURN
      END   


      Subroutine
     1     writepar(MaxId_DataSet,Ie__Param,Ie_JParam,Param,JParam,iW)    
      Real*8 Param
      dimension Ie__Param(*),Ie_JParam(*),Param(*),JParam(*)    
! escribo Param.txt de nuevo

      if(iW /= 0) then
      If (MaxId_DataSet > 0 ) Then
      ioUnit = 25
      OPEN ( ioUnit, FILE= 'Param.txt' ,STATUS= 'OLD' )
      rewind(ioUnit)
!          Elementwise varying Parameters
      Write (ioUnit,'(1A17)')'*Parameter Groups'
      Write (ioUnit,*) MaxId_DataSet
! Lectura de par�metros reales
       
      Write (ioUnit,'(1A16)')'*Real Parameters'
      Write (ioUnit,'(10I8)')
     1               (Ie__Param(I+1)-Ie__Param(I),I=1,MaxId_DataSet)

      Length_Param = Ie__Param(MaxId_DataSet+1)-1
       if (Length_Param .ne. 0) then
                Write (ioUnit,'(3E14.6)') (Param(k),k=1,Length_Param)
       End if

      Write (ioUnit,'(1A19)')'*Integer Parameters'
      Write (ioUnit,'(10I8)')
     1               (Ie_JParam(I+1)-Ie_JParam(I),I=1,MaxId_DataSet)

      Length_JParam = Ie_JParam(MaxId_DataSet+1)-1
       if (Length_JParam .ne. 0) then
                Write (ioUnit,'(10I8)') (JParam(k),k=1,Length_JParam)
       End if
      
      Close ( ioUnit)

      Endif
      Endif


      RETURN
      END   


!----------------------------------------------------------------------           
!      ASIGNA AL VECTOR X EL VALOR DE Y ( REALES )
!----------------------------------------------------------------------
      SUBROUTINE AsigReal ( X,Y,Long ) 
      REAL*8 X(Long),Y(Long)
      X = Y 
!      DO 10 I = 1,Long
! 10      X(I) = Y(I) 
      RETURN
      END
!----------------------------------------------------------------------           
!      ASIGNA AL VECTOR X EL VALOR DE Y ( ENTEROS )
!----------------------------------------------------------------------
      SUBROUTINE AsigInt ( X,Y,Long ) 
      INTEGER X(Long),Y(Long)
      X = Y 
!      DO 10 I = 1,Long
! 10      X(I) = Y(I) 
      RETURN
      END
!-----------------------------------------------------------------------
!       INICIALIZA UN VECTOR REAL  
!-----------------------------------------------------------------------
      SUBROUTINE IniReal ( Vec,Long,Valor ) 
      REAL*8 Vec(Long),Valor
      Vec = Valor
!      DO 10 I = 1,Long
! 10      Vec(I) = Valor
      RETURN
      END
!-----------------------------------------------------------------------
!       INICIALIZA UN VECTOR ENTERO  
!-----------------------------------------------------------------------
      SUBROUTINE IniEnt ( Vec,Long,Valor ) 
      INTEGER  Vec(Long),Valor
      Vec = Valor     
!      DO 10 I = 1,Long
! 10      Vec(I) = Valor
      
      RETURN
      END
!-----------------------------------------------------------------------
!       INICIALIZA UN VECTOR MENOS LOS ELEMENTOS DE DIRICHLET
!-----------------------------------------------------------------------
      SUBROUTINE IniDir ( Vec,Dir,Long,Valor )
      REAL*8 Vec(1),Valor
      INTEGER Dir(*)
      
      DO 10 I = 1,Long
      
      IF ( Dir(I) .EQ. 0 ) THEN
         Vec (I) = 0.0D0         
      ELSE 
         Vec (I) = Valor
      ENDIF
 
 10   CONTINUE

      RETURN            
      END
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      SubRoutine DirAsig ( B_Load,Dirich,iDofType,IEqT,N)
      SubRoutine DirAsig (D,B_Load,Dirich,iDofType,IEqT,N)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      REAL*8 B_Load(1),Dirich(1),D(1)
      Dimension IEqT(1),iDofType(1)

      DO 10 I = 1,N
      
	    iDof=iDofType(I)
          IF ( iDof .LT. 0 ) THEN
                    B_Load( IEqT(I) )=Dirich(I)
                    D( IEqT(I) )=1.0d0
	    Elseif ( iDof .GT. 0 ) then
                    B_Load( IEqT(I) )=0.0d0
          ENDIF
 
 10   CONTINUE

      RETURN            
      END
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine CompressVect( IEqNode,IEqDof,UnK1,Sol,iRowT,iDofT )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      REAL*8 UnK1(iRowT),Sol(1)
      Dimension IEqNode(1),IEqDof(1)

      DO I = 1,iRowT
              iDof    = IEqDof(I)
              Node    = IEqNode(I)
              UnK1(I) = Sol ( (Node-1)*iDofT + iDof )
      EndDo

      RETURN            
      END

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine SpanVect( IEqNode,IEqDof,UnK1,Sol1b,iRowT,iDofT )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      REAL*8 UnK1(iRowT),Sol1b(1)
      Dimension IEqNode(1),IEqDof(1)

      DO I = 1,iRowT
          iDof = IEqDof(I)
          Node = IEqNode(I)
          Sol1b ( (Node-1)*iDofT + iDof ) = UnK1(I)
      Enddo

      RETURN            
      END
!___________________________________________
	Subroutine ERROR_ALLOC 
          Write(6,*) "ALLOCATION ERROR"
          stop
      return
	end

	Subroutine MEM_ALLOC(PrintLabel,MAdd,iT,Memory,iERR_ALLOC)
      Character*(*) PrintLabel  
      Write(6,*) "Allocating " // PrintLabel,MAdd
      IF(iERR_ALLOC.NE.0)Call ERROR_ALLOC
      Memory = Memory + MAdd*iT
      Write(6,*) "MEMORY USED =",Memory," BYTES"
      Write(6,*)

      return
	end


!---------------------------------------------
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Integer*4 Function iFindStringInFile(Str,ioUnit)
! Busca un String en un archivo (STR), sino lo encuentra rebovina el archivo
! y pone iError < 0 como indicador de no hallazgo
! Str: String to find, ioUnit: Unit assigned to Input File; iError: Error Status variable
      Logical,Parameter  :: Segui=.True.
      Character(*) Str,DummyString*120

      iError=0
      Leng = Len_Trim(Str)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      Search_Loop: do while (segui)
      read(ioUnit,100,iostat=iError)DummyString
!       if (iError==59) backspace(ioUnit)
        if (iError.lt.0) then
			rewind (ioUnit)
			exit Search_Loop
		endif
        if ( DummyString(1:1)    /=    '*'      ) Cycle Search_Loop 
		if ( DummyString(1:Leng) == Str(1:Leng) ) Exit Search_Loop
	end do Search_Loop
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      iFindStringInFile = iError
      Return

 100  Format(1A120)
      End Function

      Integer Function IEarrange(IE,NelT)
      Dimension IE(1) 
      LIE  = 0
      LP   = IE(1)
      IE(1)= 1
      Do Nel=2,NelT+1
              LP1       = IE(Nel)
              IE(Nel)   = IE(Nel-1) + LP
              LIE       = LIE       + LP
              LP = LP1
      End do
      IEarrange = LIE
      return
	End Function

