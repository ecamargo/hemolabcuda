!    ElementLib(MaxElemLib,5) !!!Ojo con este 5 si se cambia la cantidad
!    de parametros que definenen  un elemento en element lib
!    hay que tocar dimen1 y ReadElementLib
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!    Remember : Falta implementar la interrupcion en el tiempo
!             : SOl0 se lee inicialmente en IniFile.txt, Sol0(iDofT*NodT*nOldtimeSteps)
!               iXupdate para actualizar las coordenadas con alg�n Dof
!--------------------------------------------------------------
!    Doftype (DofT*NodT)= 1 esta el Dof (en la entrada, solo importan los negativos)
!                         0 No esta el Dof (en la entrada solo importan los negativos)
!                        -x  esta el Dof pero es Dirichlet
! (x define opciones, tal como funciones asociadas, no implementado)
! se lee en AssembDofType
!--------------------------------------------------------------
!  Las matrices unitarias elementales tienen que ser simetricas
!--------------------------------------------------------------
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!
!    
!
!-----------------------------------------------------------------------
!          PROGRAMA DE PROPOSITO GENERAL PARA LA RESOLUCION DE SISTEMAS 
!                             Reducibles por Elementos
!                               GENERIC(O)
!----------------------------------------------------------------------- 
	Subroutine Generico ( NodT,NelT,NDim,iDofT,nOldTimeSteps,
     &            NonLinearProblem,MaxIterG,SRPG,nkt,NStepsRenum,
     &            Sym,Iterative,MaxIter,SRP,iSolverType,
     &            ITMAXSOL,EPS,Krylov,LFIL,TolPre,
     &            iElementLib,iCoupling,
     &            iDofNamesG,rNormG,TolG,
     &            iXupdateG,
     &            iDofNames,rNorm,Tol,CommonPar,
     &            iXupdate,
     &            DelT,Tini,Tmax,nStepOut,NodOUT,NodesOut,
     &            MaxNodEl,MaxElemLib,MaxLCommonPar,
!     &           MaxId_CouplingSet,MaxId_DataSet,
     &            X,IE,JE,iElementType,iElementMat,
     &            Ie__Param,Ie_JParam,Param,JParam,
     &            iDofType,Dirich,Sol0,Lch,NSubSteps,
     &            MaxDatSet,iWDatSet   )      

!      use InterF
      IMPLICIT REAL*8 (A-H,O-Z)
      Parameter (NPini    = 1 )
      Logical Sym, NonLinearProblem,Iterative
      INTEGER ERR_ALLOC
      CHARACTER (LEN=20):: formato
!      Parameter (Lch=16)

      Allocatable IEqNode (:)  
      Allocatable IEqDof  (:) 
      Allocatable IAdd    (:)  
      Allocatable Unk1    (:) 
      Allocatable B_Load  (:)  
      Allocatable IP      (:)  
      Allocatable IA      (:)  
      Allocatable JA      (:)  
      Allocatable AD      (:)  
      Allocatable AC      (:)  
      Allocatable IAC     (:)  
      Allocatable JAC     (:)  
      ALLOCATABLE AN      (:)  
!      REAL*8, ALLOCATABLE, TARGET :: AN (:)  
!      Real*8, POINTER :: ANT (:)
      Allocatable IU      (:)  
      Allocatable JU      (:)  
      Allocatable JUsk    (:)  
!      REAL*8, ALLOCATABLE, TARGET :: UD (:)  
!      REAL*8, ALLOCATABLE, TARGET :: UN (:)  
      ALLOCATABLE UD (:)  
      ALLOCATABLE UN (:)  
!      Real*8, POINTER :: UNT(:)
!      Real*8, POINTER :: DIT(:)
      Allocatable IUP      (:)  

      Allocatable WorkSpace (:)  
!      Allocatable ZZ       (:)  

      Allocatable NOlds    (:)  
      Allocatable Nodos    (:)  

      Character*(60) StrV
      Character*(120) StrV1
      Character(Lch) DofNamesG(iDofT)
      Character(Lch) DofNames(iDofT,NSubSteps)
      Dimension iDofNamesG (iDofT*Lch)
      Dimension rNormG ( 1 )
      Dimension TolG   ( 1 )
      Dimension iDofNames(iDofT*Lch,NSubSteps)
      Dimension rNorm ( iDofT,1 )
      Dimension Tol   ( iDofT,1 )
      Dimension CommonPar (MaxLCommonPar*MaxElemLib,1)
      Dimension iElementLib  ( 5*MaxElemLib,1 )
      Dimension iCoupling    ( 1 )
!      Dimension iCoupling    ( (iDofT*MaxNodEl)**2  * MaxId_CouplingSet)

      Dimension X   ( NodT*NDim )
      Dimension IE  ( NelT +1 )
      Dimension JE  ( IE(NelT +1) - 1 )
      Dimension IET ( NodT+1)   
      Dimension JET ( IE(NelT +1)     )
      Dimension iElementType ( NelT )
      Dimension iElementMat  ( NelT )
      Dimension Ie__Param ( 1 )
      Dimension Ie_JParam ( 1 )
      Dimension Param     ( 1 )
      Dimension JParam    ( 1 )
      Dimension iDofType  ( NodT * iDofT * NSubSteps )
      Dimension Sol0      ( NodT * iDofT * nOldTimeSteps )
      Dimension Sol1a     ( NodT * iDofT )
      Dimension Sol1b     ( NodT * iDofT )
      Dimension Sol1c     ( NodT * iDofT )
      Dimension Dirich    ( NodT * iDofT * NSubSteps )
      Dimension IEqT      ( NodT * iDofT * NSubSteps  )  
      Dimension NodesOut(1)  

      Dimension iXupdateG(1)  
      Dimension iXupdate(Ndim,1)  
      Dimension Sym(1),Iterative(1),MaxIter(1)
      Dimension iSolverType(1),SRP(1)
      Dimension ITMAXSOL(1),EPS(1),Krylov(1),LFIL(1),TolPre(1)

      Dimension iControl(10)  

      Dimension iRowT(NSubSteps)
      Dimension iRowT1(NSubSteps)
      Dimension ip_iRowT(NSubSteps+1)
      Dimension ip_iRowT1(NSubSteps+1)
      Dimension ip_IA(NSubSteps+1)
      Dimension ip_JA(NSubSteps+1)
      Dimension ip_AN(NSubSteps+1)
	Dimension L_JA(NSubSteps)


      Dimension ip_IU(NSubSteps+1)
      Dimension ip_JU(NSubSteps+1)
      Dimension ip_UD(NSubSteps+1) 
      Dimension ip_UN(NSubSteps+1)
      Dimension ip_IUP(NSubSteps+1) 
      Dimension L_SKyLine(NSubSteps)
      Dimension L_IU(NSubSteps)
      Dimension L_UD(NSubSteps) 
      Dimension L_IUP(NSubSteps) 
	Dimension L_JU(NSubSteps)

      Dimension ip_UnK(NSubSteps+1)
	Dimension L_WorkSpace(NSubSteps)
!      Call writepar(MaxDatSet,Ie__Param,Ie_JParam,Param,JParam,iWDatSet)    

	iControl=0
      iTimer=1
	StrV = 'Initiating Calculus'
!      if (iTimer.ne.0)Call TimerOUT(Strv)
 
      LenUnK   = iDofT * NodT
      MaxLRows = iDofT * MaxNodEl
      NStepsRenum= MIN(NelT/4,NStepsRenum)
!      X, Sol1a,b,c,Dirich, Sol0, 
      Memory= NodT * (Ndim + iDofT* (3+NSubSteps+nOldTimeSteps))*8
!     IEQT, iDofType
      Memory=Memory + 2*NodT*iDofT*NSubSteps * 4
!       IE,JE,IET,JET
      Memory=Memory + ((NodT+1)+(NelT+1)+2*(IE(NelT+1)-1))*4
      
! vuelvo a character los nombres de los grados de libertad
      do i=1,iDofT
      do j=1,Lch
         DofNamesG(i)(j:j) = CHAR( iDofNamesG( j+Lch*(i-1) ) )
      enddo
      enddo
      do nstp=1,NSubSteps
      do i=1,iDofT
      do j=1,Lch
         DofNames(i,nstp)(j:j) = CHAR( iDofNames(j+Lch*(i-1),nstp) )
      enddo
      enddo
      enddo



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     Obtaining traspose of connectivity matrix
      CALL TrasComp(IE,JE,IET,JET,NodT,NelT)
!	StrV = 'Transpose of connectivity matrix Obtained '
!      if (iTimer.ne.0)Call TimerOUT(StrV)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Preparing the Element's coupling matrices. If they weren't read they are calculated here
      Call CouplingMatrix( iElementLib,iCoupling,CommonPar,
     &     iDofT,MaxLRows,MaxElemlib,MaxLCommonPar,NDim,NSubSteps)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Allocate ( Nodos (NodT), STAT = ERR_ALLOC ) 
	Call MEM_ALLOC("Nodos(NodT)",NodT,4,Memory,ERR_ALLOC)
      Allocate ( NOlds (NodT), STAT = ERR_ALLOC ) 
	Call MEM_ALLOC("NOlds(NodT)",NodT,4,Memory,ERR_ALLOC)

      

	Do I=1,NodT
          NOlds(I)=I
          Nodos(I)=I
      EndDo

!      SelectCase (iSolverType)   
!      Case (-1,0)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Ordering the nodes to minimize bandwith
      if ( NStepsRenum .GT. 0 ) then
!	StrV = 'Renumbering'
!      if (iTimer.ne.0)Call TimerOUT(StrV)

       Call Cathill(IE,JE,IET,JET,NOlds,Nodos,NelT,NodT,NStepsRenum)
       Call Permutar(X,iDofType,Dirich,Sol0,Nodos,
     &               NodT,iDofT,NDim,nOldTimeSteps,NSubSteps)
      Endif
!      Case Default
!      write(233,'(3I7)')(NOlds(I),Nodos(I),NOlds(Nodos(I)),I=1,NodT)
!      EndSelect

!	StrV = 'Processing DOFTYPE ID Matrix'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Calculates wich nodes have active DOfs and how much are them.
      Do nstp = 1 , NSubSteps
         ipND = (nstp-1) * LenUnK + 1  
      Call AssembDofType1( IE,JE,iDofType(ipND),
     &       iElementType,iElementLib(1,nstp),iCoupling,
     &       NodT,NelT,iDofT,MaxLRows,MaxElemlib,iRowT(nstp))
!     iRowT=total number of equations to be solved
      Enddo
      iRowT1 = iRowT + 1
      iRowTM = MaxVal(iRowT)
      iRowTL = Sum(iRowT)
      iRowT1L= Sum(iRowT1)
      iRowT1M=iRowTM+1 

      Allocate ( IEqNode (iRowTL), STAT = ERR_ALLOC ) 
	Call MEM_ALLOC("iEQNode(iRowTL)",iRowTL,4,Memory,ERR_ALLOC)

      Allocate ( IEqDof  (iRowTL), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("iEqDof",iRowTL,4,Memory,ERR_ALLOC)

      Allocate ( Unk1 (iRowTM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("Unk1(iRowTM)",iRowTM,8,Memory,ERR_ALLOC)

      Allocate ( B_Load  (iRowTM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("B_Load(iRowTM)",iRowTM,8,Memory,ERR_ALLOC)

      Allocate ( IP (iRowT1M), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("IP(iRowTM+1)",iRowT1M,4,Memory,ERR_ALLOC)

      Allocate ( IAdd (iRowTL), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("IAdd(iRowTL)",iRowTL,4,Memory,ERR_ALLOC)
                      
      Allocate ( IA (iRowT1L), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("IA(iRowTL1)",iRowT1L,4,Memory,ERR_ALLOC)
!      Memory=Memory+ 4*(3*iRowTL+iRowT1L+iRowT1M)+8*(2*iRowTM)


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     Asembling Global matrix symbolic structure
!	StrV = 'Assembling Global matrix symbolic structure, First Pass'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
      ipRow  = 1
      ipRow1 = 1
      ipND  = 1
      Do nstp = 1 , NSubSteps

       Call PreEnSimGen ( IE,JE,IET,JET,IA(ipRow1),
     &                   IP,iDofType(ipND),IAdd(ipRow),
     &                   iElementType,iElementLib(1,nstp),iCoupling,
     &                   NodT,NelT,
     &                   IEqNode(ipRow),IEqDof(ipRow),IEqT(ipND),
     &                   iDofT,MaxElemlib,
     &                   MaxLRows, L_JA(nstp) )

          ipRow  = ipRow  + iRowT (nstp) 
          ipRow1 = ipRow1 + iRowT1(nstp)
          ipND   = ipND   + LenUnK   
      Enddo


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      Total Allocation for GLOBAL matrices
!      L_JA(nstp) have the number of nonzeros of each step global matrix
      L_JAL = 0
      L_ANM = 0
      Do nstp = 1 , NSubSteps
		L_JAL = L_JAL + L_JA(nstp)
		L_AN  = L_JA(nstp)
		If (.not. Sym(nstp))L_AN = L_AN + L_JA(nstp)
		L_ANM = max(L_AN,L_ANM)
      Enddo

	Allocate ( JA (L_JAL), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("JA",L_JAL,4,Memory,ERR_ALLOC)

	Allocate ( AN (L_ANM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("AN",L_ANM,8,Memory,ERR_ALLOC)

	Allocate ( AD (iRowTM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("AD(iRowTM)",iRowTM,8,Memory,ERR_ALLOC)


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      SET THE POINTERS TO EACH MATRIX IN COMMON MEMORY
      ip_IA (1)   = 1
      ip_JA (1)   = 1
!      ip_AN (1)   = 1
      ip_UnK(1)   = 1
      ip_iRowT (1)= 1
      ip_iRowT1(1)= 1

      Do nstp = 1 , NSubSteps - 1
!     iRowT=total number of equations to be solved
          ip_IA (nstp+1) = ip_IA (nstp)  + iRowT1(nstp)
          ip_JA (nstp+1) = ip_JA (nstp)  + L_JA(nstp)
          ip_UnK(nstp+1) = ip_UnK(nstp)  + LenUnK   
          ip_iRowT (nstp+1)= ip_iRowT (nstp)+ iRowT (nstp)
          ip_iRowT1(nstp+1)= ip_iRowT1(nstp)+ iRowT1(nstp)
      Enddo

!///////////////////////////////////////////////////////////
! Se dimensiona la matriz factorizada si iSolverType=0 y Dem�s Vectores usados en la soluci�n
!///////////////////////////////////////////////////////////
          L_IU  = 0
          L_UD  = 0 
          L_IUP = 0
          L_IUM = 0  
          L_UDM = 0

          Do nstp=1, NSubSteps
!          Verifico el caso Directo que se factoriza a Priori
             SelectCase (iSolverType(nstp))   
              Case ( -1 )
	            L_IU(nstp) = iRowT1(nstp)
	            L_UD(nstp) = iRowT1(nstp)
!                 Reservo para DIT  
                  iF(.not. Sym(nstp))L_UD(nstp)=2*L_UD(nstp)
	            L_IUP(nstp) = iRowT1(nstp)
              Case ( 0 )
	            L_IU(nstp) = iRowT1(nstp)
	            L_IUP(nstp)= iRowT1(nstp)
! OJOOOO reservo dos veces la diagonal para usar como auxiliar
                  L_UDM=max(L_UDM,2*iRowT(nstp))   

              Case ( 2,3,4,5,6 )
	            L_IU(nstp) = iRowT1(nstp)
                  L_UDM=max(L_UDM,2*iRowT(nstp))   
! OJOOOO reservo dos veces la diagonal para usar como auxiliar  !!Revisar
              Case ( 100: )
	            L_IU(nstp) = iRowT1(nstp)
!                 No se usa la diagonal
                  L_UDM=max(L_UDM,iRowT(nstp))   

            EndSelect
       enddo

       L_IUPM = MaxVal(L_IUP)
!&&&&&&&&&&
      ip_IU(1) = 1
      Do nstp=1, NSubSteps
      ip_IU(nstp+1)=ip_IU(nstp)+L_IU(nstp) 
      Enddo

      ip_UD = 0
      LUDP  = 1
      Do nstp=1, NSubSteps
        SelectCase (iSolverType(nstp))   
          Case (-1)
 	      ip_UD(nstp) = LUDP  
            LUDP = LUDP + L_UD(nstp)
        End Select 
      enddo

      Do nstp=1, NSubSteps
        SelectCase (iSolverType(nstp))   
          Case (0,2,3,4,5,6,100:)
	      ip_UD(nstp) = LUDP  
        End Select 
      enddo
      L_UDM = LUDP + L_UDM - 1
      L_IUM = ip_IU(NSubSteps+1)-1 
      L_UDM = Max(1,L_UDM)
      L_IUM = Max(1,L_IUM)

       Allocate ( IU (L_IUM ), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("IU",L_IUM,4,Memory,ERR_ALLOC)

       Allocate ( UD (L_UDM ), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("UD",L_UDM,8,Memory,ERR_ALLOC)

       if(L_IUPM>0) then 
          Allocate ( IUP(L_IUPM), STAT = ERR_ALLOC  ) 
          Call MEM_ALLOC("IUP",L_IUPM,4,Memory,ERR_ALLOC)
       endif   

!&&&&&&&&&&

! almaceno vectores para los iterativos
		L_WorkSpace=0
          Do nstp=1, NSubSteps
             SelectCase (iSolverType(nstp))   
              Case (1)
!               al menos 1 caso es iterativo               
                L_WorkSpace(nstp) =  2*iRowT (nstp)
              Case (2,3)
!               al menos 1 caso es iterativo Tipo CGS               
                L_WorkSpace(nstp) =  8*iRowT (nstp)
              Case (4,5,6)
!               al menos 1 caso es iterativo Tipo GMRES               
	          iRRR = iRowT(nstp)
			  Kry  = Krylov(nstp)
!                L_WorkSpace(nstp) = 4*iRRR +5*Kry + (Kry+1)^2 + 
!     &                              (Kry+1)*iRRR  + 2
!              Case (6)
!               al menos 1 caso es iterativo Tipo GMRES IMSL
			  KDMax=max(Kry,20)
	          L_WorkSpace(nstp)=4*iRRR + iRRR*(KDMax+2) + KDMax**2 +
     &                            5*KDMax+5

            !%%%%%%%%%%%%%%%%
                  Case (100,103)
            !%%%%%%%%%%%%%%%%
                  L_WorkSpace(nstp) = 5 * iRowT(nstp) + iRowT(nstp)
!     (1) Work space required by each of the iterative solver
!     routines is as follows:
! 100       CG      == 5 * n
! 103      CGNR    == 5 * n
! 101      BCG     == 7 * n
! 102      DBCG    == 11 * n
! 104      BCGSTAB == 8 * n
! 105      TFQMR   == 11 * n
! 106      FOM     == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
! 107      GMRES   == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
! 108      FGMRES  == 2*n*(m+1) + (m+1)*m/2 + 3*m + 2 (m = ipar(5),
!                  default m=15)
! 109      DQGMRES == n + lb * (2*n+4) (lb=ipar(5)+1, default lb = 16)


            !%%%%%%%%%%%%%%%%
                  Case (101,110)
            !%%%%%%%%%%%%%%%%
                  L_WorkSpace(nstp)= 7 * iRowT(nstp) + iRowT(nstp)

            !%%%%%%%%%%%%%%%%
                  Case (102,105)
            !%%%%%%%%%%%%%%%%
                  L_WorkSpace(nstp) =11 * iRowT(nstp) + iRowT(nstp)

            !%%%%%%%%%%%%%%%%
                  Case (104)
            !%%%%%%%%%%%%%%%%
                  L_WorkSpace(nstp) = 8 * iRowT(nstp) + iRowT(nstp)

            !%%%%%%%%%%%%%%%%
                  Case (106:107)
            !%%%%%%%%%%%%%%%%
	          iRRR = iRowT(nstp)
			  Kry  = Krylov(nstp)
                L_WorkSpace(nstp)=(iRRR+3)*(Kry+2) +(Kry+1)*Kry/2 + iRRR       !(m = ipar(5), default m=15)
            !%%%%%%%%%%%%%%%%
                  Case (108)
            !%%%%%%%%%%%%%%%%
	          iRRR = iRowT(nstp)
			  Kry  = Krylov(nstp)
             L_WorkSpace(nstp)=2*iRRR*(Kry+1)+(Kry+1)*Kry/2+3*Kry+2+iRRR     !(m = ipar(5),
            !%%%%%%%%%%%%%%%%
                  Case (109)
            !%%%%%%%%%%%%%%%%
	          iRRR = iRowT(nstp)
			  Kry  = Krylov(nstp)
                L_WorkSpace(nstp) = iRRR + (Kry + 1) * (2*iRRR+4) + iRRR      !(lb=ipar(5)+1, default lb = 16)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            !%%%%%%%%%%%%%%%%
!                  Case (111)
            !%%%%%%%%%%%%%%%%
!                L_WorkSpace(nstp) = 5*iRowT(nstp) + L_JA(nstp)

             EndSelect
          enddo
		LenWSP=Max( MaxVal(L_WorkSpace),1)	
!     Iterative Solvers
	    Allocate ( WorkSpace(LenWSP), STAT = ERR_ALLOC  ) 
          Call MEM_ALLOC("WorkSpace",LenWSP,8,Memory,ERR_ALLOC)

!-------------------------------------------------
!     Allocation for those solvers that need a Complete Representation
      L_JACM = 0
       Do nstp=1, NSubSteps
             SelectCase (iSolverType(nstp))   
              Case (3,4,5,6,100:)
                  L_JAC1= 2*L_JA(nstp)+iRowT(nstp)
                  L_JACM=max(L_JACM,L_JAC1)   
            End Select 
       enddo

      If(L_JACM > 0) then
          Allocate ( IAC(iRowT1M) , STAT = ERR_ALLOC ) 
          Allocate ( JAC(L_JACM ) , STAT = ERR_ALLOC ) 
          Allocate ( AC (L_JACM ) , STAT = ERR_ALLOC ) 
	Call MEM_ALLOC("ACMat",iRowT1M+L_JACM+2*L_JACM,4,Memory,ERR_ALLOC)

      else
          Allocate ( IAC(1) , STAT = ERR_ALLOC ) 
          Allocate ( JAC(1) , STAT = ERR_ALLOC ) 
          Allocate ( AC(1)  , STAT = ERR_ALLOC ) 
               IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC
            Memory=Memory+ 4*(1+1+2)
      Endif
!-------------------------------------------------





!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      Assembling symbolic Matrix      
!	StrV = 'Assembling Global symbolic System Matrix, 2nd pass'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
      Do nstp=1, NSubSteps
          ipRow  = ip_iRowT(nstp)
          ipRow1 = ip_iRowT1(nstp)
          ipJA   = ip_JA(nstp)
          ipND   = ip_Unk(nstp)

           Call EnSimGen ( IE,JE,IET,JET,IA(ipRow1),JA(ipJA),
     &                     IP,iDofType(ipND),IAdd(ipRow),
     &                     iElementType,iElementLib(1,nstp),iCoupling,
     &             NodT,NelT,iRowT(nstp),IEqNode(ipRow),IEqDof(ipRow),
     &                     IEqT(ipND),iDofT,MaxElemlib,
     &                     MaxLRows, L_JA(nstp), L_SKyLine(nstp))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! OJO Poner el salto en ElementLib

!	StrV = 'Ordering symbolic Matrix'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
!   Double transposition using de real value components as the transpose matrix IP=IAT,AN=JAT
          iRowTx=iRowT(nstp)
          CALL TrasComp(IA(ipRow1),JA(ipJA),IP,AN,iRowTx,iRowTx)
          CALL TrasComp(IP,AN,IA(ipRow1),JA(ipJA),iRowTx,iRowTx)
!      CALL Ordena   ( IA,JA,IP,iRowT )

      Enddo
!
! Se dimensiona la matriz factorizada si iSolverType=0
      L_SKyLineL = 0
      Do nstp=1, NSubSteps
      SelectCase (iSolverType(nstp))   
          Case (-1,0)
      	    L_SKyLineL = L_SKyLineL + L_SKyLine(nstp)
	EndSelect
      Enddo
       L_SKyLineL=Max(L_SKyLineL ,1)
!      iF(L_SKyLineL>0) then 
      Allocate( JUsk(L_SKyLineL), STAT =ERR_ALLOC) 
	Call MEM_ALLOC("JUsk(L_SKyLineL)",L_SKyLineL,4,Memory,ERR_ALLOC)
!      endif

!  %%%%%%%%%%%%% Symbolic Factorization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      StrV='Symbolic Factorization'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
      L_JU = 0
      LJUP = 1
      Do nstp=1, NSubSteps
         ipRow  = ip_iRowT(nstp)
         ipRow1 = ip_iRowT1(nstp)
         ipJA   = ip_JA(nstp)
         ipIU   = ip_IU(nstp)
         ipND   = ip_Unk(nstp)

        SelectCase (iSolverType(nstp))   
          Case (-1,0)
          CALL TrianSim (IA(ipRow1),JA(ipJA),IU(ipIU),JUsk(LJUP),
     &                   IP,iRowT(nstp),L_JU(nstp) )
      LJUP = LJUP + L_JU(nstp)
!-------------------------------
        Case (1,2,5)
!-------------------------------
!       Conjugate Gradient Squared
              L_JU(nstp) = 0
              NZLOW = 0
!-------------------------------
        Case (3,4,6)
!-------------------------------
!       Conjugate Gradient Squared Incomple Cholezki Factorization
!         OJO!!!!!!!!!!!!! implementacion actual para GMRES DE IMSL deberia tener L_JU()=0 como para el caso 5

             L_JU(nstp) = L_JA(nstp)

        Case (100:)
!-------------------------------
!        SAAD Iterative Solvers preconditioning matrix
!        L_JU(nstp)=2*L_JA(nstp)+iRowT1(nstp)+iRowT(nstp)*2*LFIL(nstp)
        L_JU(nstp)=iRowT1(nstp) + iRowT (nstp)*(2*LFIL(nstp))+1

	  Case Default
	  EndSelect
      Enddo

      
      ip_JU(1) = 1
      Do nstp=1, NSubSteps
                          ip_JU(nstp+1)=ip_JU(nstp)+L_JU(nstp) 
      Enddo


      L_UNM = 0
      Do nstp=1, NSubSteps
             SelectCase (iSolverType(nstp))   
              Case (3,4,6)
!!!!!!!!!!!!!!!! OJO al Pomo con el 6 aqui, ademas no es necesario guardar UN porque se usa AN
                  if(Sym(nstp)) then 
	               LUN = L_JA(nstp)
                  else
	               LUN = 2*L_JA(nstp)
                  endif
                  L_UNM = max(L_UNM , LUN)
              Case (100:)
!                 SAAD Iterative Solvers 
                  LUN = L_JU(nstp)
                  L_UNM = max(L_UNM , LUN)

             Case (0)
                  if(Sym(nstp)) then 
	               LUN = L_JU(nstp)
                  else
	               LUN = 2*L_JU(nstp)
                  endif
                  L_UNM = max(L_UNM , LUN)

            End Select 
      enddo

      ip_UN = 0
      LUNP  = 1
      Do nstp=1, NSubSteps
        SelectCase (iSolverType(nstp))   
          Case (-1)
            if(Sym(nstp)) then 
                              LUN = L_JU(nstp)
            else
                              LUN = 2*L_JU(nstp)
            endif
	      ip_UN(nstp) = LUNP  
            LUNP = LUNP + LUN
        End Select 
      enddo

      Do nstp=1, NSubSteps
        SelectCase (iSolverType(nstp))   
          Case (0,2,3,4,5,6,100:)
	      ip_UN(nstp) = LUNP  
        End Select 
      enddo
      L_UNM = LUNP + L_UNM - 1
      L_JUM = ip_JU(NSubSteps+1)-1 
      L_UNM = Max(1,L_UNM)
      L_JUM = Max(1,L_JUM)

      Allocate ( JU(L_JUM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("JU",L_JUM,4,Memory,ERR_ALLOC)

!-------------------------------
      LJUP = 1
      Do nstp=1, NSubSteps
        SelectCase (iSolverType(nstp))   
          Case (-1,0)
	     ipJU = ip_JU(nstp)  
	     ipIU = ip_IU(nstp)  
	     LJU  = L_JU (nstp)
           iRowTx = iRowT(nstp)  
           Call AsigInt ( JU(ipJU),JUsk(LJUP),LJU )
           CALL TrasComp(IU(ipIU),JU(ipJU),IP,JUsk(LJUP),iRowTx,iRowTx)
           CALL TrasComp(IP,JUsk(LJUP),IU(ipIU),JU(ipJU),iRowTx,iRowTx)
           LJUP = LJUP + LJU
        End Select 
      enddo

      LoopNStp: Do nstp=1, NSubSteps
      SelectCase (iSolverType(nstp))   
      Case (-1,0)
                Deallocate (JUsk) 
                    Write(6,*) "DEALLOCATING JUsk",L_SKyLineL
                    Memory=Memory- 4*(L_SKyLineL)
                    Write(6,*) "MEMORY USED",Memory,"   BYTES"

                 Exit LoopNStp 

      End Select 
      enddo LoopNStp

      Allocate ( UN(L_UNM), STAT = ERR_ALLOC  ) 
	Call MEM_ALLOC("UN",L_UNM,8,Memory,ERR_ALLOC)


      Write(6,*) "TOTAL MEMORY ALLOCATED",Memory," BYTES"
      Write(6,*) "TOTAL MEMORY ALLOCATED",Memory/1024," KB"
      Write(6,*) "TOTAL MEMORY ALLOCATED",Memory/(1024*1024)," MB"

      Do nstp=1, NSubSteps
      SelectCase (iSolverType(nstp))   
      Case (2)
              ipIU = ip_IU(nstp)
          	DO iH=ipIU,ipIU+iRowT(nstp)
                 IU (iH)=1
              enddo
      Case (3,4,6)

!	StrV = 'Obtaining General Format Sparse Matrix in GCS &GMRES'
!      if (iTimer.ne.0)Call TimerOUT(StrV)
          ipIA = ip_IA(nstp)
          ipJA = ip_JA(nstp)
          ipANT= 1
	    if(.not.Sym(nstp))ipANT=ipANT+L_JA(nstp)
!      !!! OJO: Revisar esto no es neceario aqui
       Call TRANS_ULtoC( IA(ipIA),JA(ipJA),AN(ipANT),AN,AD,
     &                   IAC,JAC,AC,iRowT(nstp))
          ipIU = ip_IU(nstp)
	    LIU  = L_IU (nstp)
          ipJU = ip_JU(nstp)
	    LJU  = L_JU (nstp)
          Call IniEnt ( IU(ipIU),LIU, 0 )
          Call IniEnt ( JU(ipJU),LJU, 0 )
          Call SYMCMR (IAC,JAC,IU(ipIU),JU(ipJU),iRowT(nstp),NJU)
      Case (5)
          ipIU = ip_IU(nstp)
          	    DO iH=ipIU,ipIU+iRowT(nstp)
                     IU (iH)=1
                  enddo

	Case Default
	EndSelect
	Enddo
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!---------------------------------------
      Time=Tini
      nStep=0
      Dtm=DelT
      write(6,*)'Output Initial Values'
!---------------------------------------
! Writing restart file and time step zero
      CALL Escribir(IE,JE,X,Sol0,Time,DTm,nStep,nStepOut,Nodos,NOlds,	
     & NodOUT,NodesOut,NodT,iDofT,NelT,NDim,iElementType,nOldTimeSteps)
!      CALL EscribirVtk ( IE,JE,X,Sol0,Time,DTm,nStep,nStepOut,	
!     &                    NodT,iDofT,NelT,NDim,iElementType )
!      CALL EscribirVtk(IE,JE,X,Sol0,Time,DTm,nStep,nStepOut,Nodos,NOlds,	
!     &                    NodT,iDofT,NelT,NDim,iElementType )
       
        StrV = ''
!        if (iTimer.ne.0)Call TimerOutStep(StrV)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DO While (Time.LT.Tmax)
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

      if (mod(nStep,nkT).eq.0)then
        StrV=Repeat('_',59) 
        write(6,*)StrV
        StrV= 
     &     '[][][][][][][][][]--> NEW TIME STEP <--[][][][][][][][][][]'
        write(6,*)StrV
        StrV = ''
!        if (iTimer.ne.0)Call TimerOUT(StrV)
        write(6,988)nStep+1,Time,DTm
        write(6,*)
!      StrV1=Repeat('_',59) // Achar(13) //
      endif
 988  Format(' Step= ',I8,', Time=',G12.4E2,', DT=',G12.4E2)

      iConver = 0
	NConverG=0
      CALL AsigReal  ( Sol1a,Sol0,LenUnK)

      Convergence_loop : DO While (iConver .eq.0)
	NConverG=NConverG+1

      CALL AsigReal  ( Sol1c,Sol1a,LenUnK)

!       Ciclo de SubSteps
      SubStepLoop: Do nstp=1,NSubSteps
        iRow  = iRowT(nstp)
        iRow1 = iRowT1(nstp)
        ipRow  = ip_iRowT(nstp)
        ipRow1 = ip_iRowT1(nstp)
        iIA   = ip_IA(nstp)
        iJA   = ip_JA(nstp)
        LJA   = L_JA(nstp)
        iANT  = 1
	  if(.not.Sym(nstp))iANT  = iANT  + LJA 
        iND   = ip_Unk(nstp)

      iConverS = 0
	NConverS=0

      CALL AsigReal  ( Sol1b,Sol1a,LenUnK)
      Convergence_SUBloop : DO While (iConverS .eq.0)
	NConverS=NConverS+1
            
        IP = 0 ; B_Load = 0.D0 
        CALL INIREAL ( AD,iRow,0.0d0 )
        CALL INIREAL ( AN,LJA,0.0d0 )
        if(.not.Sym(nstp)) CALL INIREAL ( AN(iANT),LJA,0.0d0 )

!        CALL DirAsig( B_Load,Dirich(iND),iDofType(iND),IEqT(iND),LenUnK)
      CALL DirAsig(AD,B_Load,Dirich(iND),iDofType(iND),IEqT(iND),LenUnK)

!       write(6,*)'entre a MatrixCoef'
	  CALL MatrixCoef( IE,JE,X,
     &     IA(iIA),JA(iJA),AN,AN(iANT),AD,
     &     Sol0,Sol1a,B_Load,
     &     iDofType(iND),IAdd(ipRow),IP,
     &     Param,JParam,CommonPar(1,nstp),
     &     Ie__Param,Ie_JParam,MaxDatSet,                            
     &     iElementType,iElementMat,iElementLib(1,nstp),
     &     iCoupling,
     &     IEqNode(ipRow),IEqDof(ipRow),IEqT(iND),
     &     NodT,NelT,iDofT,NDim,MaxNodEl,MaxElemlib,MaxLCommonPar,
     &     iRow,MaxLRows,Sym(nstp),
     &     DelT,DTm,Time,nOldTimeSteps,iControl )

! if iControl(1) .ne. zero then Dt has been modified, nonlinear iterations restart
      if (iControl(1).ne.0) then
          CALL AsigReal  ( Sol1a,Sol0,LenUnK)
          iControl(1) = 0
          Cycle Convergence_loop
	endif
      if (mod(nStep,nkT).eq.0)then
          StrV = 'System Matrix is assembled, NOW SOLVING'
!         if (iTimer.ne.0)Call TimerOUT(StrV)
          write(6,*)
      ENDIF
!          Compressing the Solution vector for iterative solvers
      SelectCase (iSolverType(nstp))   
              Case (1,2,3,4,5,6)
!                call AsigReal ( UnK1,B_Load,iRowT ) 
       Call CompressVect( IEqNode(ipRow),IEqDof(ipRow),UnK1,Sol1a,
     &                                                      iRow,iDofT )
              Case Default
      EndSelect



!	  write(6,*)'TrianNS'
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
! *************** LINEAR EQUATIONS SOLVERS ******************** !  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
      SelectCase (iSolverType(nstp))   
!%%%%%%%%%%%%%%%%
	Case (-1)
!%%%%%%%%%%%%%%%%
!       iSolverType=-1 Case=Gaussian Factorization Solver only once


!	Use SetPointers

        iIU   = ip_IU(nstp)
        iJU   = ip_JU(nstp)
        LJU   = L_JU(nstp)
        iUD   = ip_UD(nstp)
        iDIT  = iUD
	  if(.not.Sym(nstp))iDIT  = iDIT  + iRow 
        iUN   = ip_UN(nstp)
        iUNT  = ip_UN(nstp)
	  if(.not.Sym(nstp))iUNT  = iUNT  + LJU 

	if (nStep.eq.0)Call TrianNS(IA(iIA),JA(iJA),
     &                            AN,AN(iANT),AD,
     &                            IU(iIU),JU(iJU),
     &                            UN(iUN),UN(iUNT),UD(iUD),
     &                            UD(iDIT),IP,IUP ,iRow,Sym(nstp) )
!       Obtaining solution vector UnK1
        CALL ForwBack ( IU(iIU),JU(iJU),
     &                  UD(iUD),UN(iUN),UN(iUNT),B_Load,UnK1,iRow )
!%%%%%%%%%%%%%%%%
	Case (0)
!%%%%%%%%%%%%%%%%
!       iSolverType=0 Case=Gaussian Factorization Solver
        iIU   = ip_IU(nstp)
        iJU   = ip_JU(nstp)
        LJU   = L_JU(nstp)
        iUD   = ip_UD(nstp)
        iDIT  = iUD
	  if(.not.Sym(nstp))iDIT  = iDIT  + iRow 
        iUN   = ip_UN(nstp)
        iUNT  = ip_UN(nstp)
	  if(.not.Sym(nstp))iUNT  = iUNT  + LJU 

	  Call TrianNS(IA(iIA),JA(iJA),
     &                            AN,AN(iANT),AD,
     &                            IU(iIU),JU(iJU),
     &                            UN(iUN),UN(iUNT),UD(iUD),
     &                            UD(iDIT),IP,IUP ,iRow,Sym(nstp) )
!        write(6,*)'Solver'

!       Obtaining solution vector UnK1
        CALL ForwBack ( IU(iIU),JU(iJU),
     &                  UD(iUD),UN(iUN),UN(iUNT),B_Load,UnK1,iRow )
!

!%%%%%%%%%%%%%%%%
	Case Default
!%%%%%%%%%%%%%%%%
        iIU   = ip_IU(nstp)
        iJU   = ip_JU(nstp)
        LJU   = L_JU(nstp)
        iUD   = ip_UD(nstp)
        iDIT  = iUD
!        IDIT actually not used for iterative solvers
	  if(.not.Sym(nstp))iDIT  = iDIT  + iRow 
        iUN   = ip_UN(nstp)
        iUNT  = ip_UN(nstp)
	  if(.not.Sym(nstp))iUNT  = iUNT  + LJU 



      Call ITERATIVE_SOLVER(IA(iIA),JA(iJA),AN(iANT),AD,AN,
     &            B_Load,UnK1,
     -            IU(iIU),JU(iJU),UN(iUNT),UD(iUD),UN(IUN),             
     -            IAC,JAC,AC,
     _            EPS(nstp),ITMAXSOL(nstp),
     _            ISTAT,iRow,Krylov(nstp),LFil(nstp),TolPre(nstp),
     _            WorkSpace,
     _            iSolverType(nstp),Sym(nstp),
     &            IE,JE,IET,JET,X,
     &            IEqNode(ipRow),IEqDof(ipRow),IEqT(iND),iElementType,
     &            NodT,NelT,iDofT,NDim,iControl,DelT,DTm             )
!     _            UV,WW,C,R0,G,Q,H,W,Z,

! if iControl(1) .ne. zero then Dt has been modified, nonlinear iterations restart
      if (iControl(1).ne.0) then
          CALL AsigReal  ( Sol1a,Sol0,LenUnK)
          iControl(1) = 0
          Cycle Convergence_loop
	endif



	EndSelect

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!       Spaning solution vector to extended form
       Call SpanVect(IEqNode(ipRow),IEqDof(ipRow),UnK1,Sol1b,iRow,iDofT)
!       Nonlinear convergence
!        write(6,*)'Conver'
        call subrelajar(Sol1b,Sol1a,Srp(nstp),iDofType(iND),iDofT*NodT)
        Call ConverS( Sol1b,Sol1a,iDofT,NodT,rNorm(1,(nstp)),
     &          Tol(1,(nstp)),Iterative(nstp),MaxIter(nstp),
     &            DofNames(1,(nstp)),iConverS,nkT,nStep,nstp,NConverS )

        CALL AsigReal  ( Sol1a, Sol1b, LenUnK)

      End Do Convergence_Subloop

!Updates the geometry, if necesary
      CALL Xupdate(X,Sol1a,NodT,iDofT,NDim,iXupdate(1,nstp))

      End Do SubStepLoop
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!       Nonlinear convergence
!        write(6,*)'Conver'
!C OJOJOJOJOJO Parche
        call subrelajar(Sol1a,Sol1c,SrpG,iDofType(iND),iDofT*NodT)
        Call Conver( Sol1a,Sol1c,iDofT,NodT,rNormG(1),TolG(1),
     &NonLinearProblem,MaxIterG,DofNamesG(1),iConver,nkT,nStep,NConverG)

!        CALL AsigReal  ( Sol1c, Sol1a, LenUnK)


      End Do Convergence_loop

!     Shift back the Solution Vector to accommodate the new solution
      Do kn = LenUnK*nOldTimeSteps, LenUnK+1,-1
            Sol0(kn)=Sol0(kn-LenUnK)
	End do


!      Actualizamos los parametros variables con el paso de tiempo. Veloc. Bubbles
      LengthParam =Ie__Param(MaxDatSet+1)-1
      LengthJParam=Ie_JParam(MaxDatSet+1)-1
      Do kn = 1, LengthParam
            Param(kn)=Param(kn+LengthParam)
	End do
      Do kn = 1, LengthJParam
            JParam(kn)=JParam(kn+LengthJParam)
	End do

!     Store the new solution
      CALL AsigReal(Sol0, Sol1a, LenUnK)
      nStep = nStep + 1
      Time  = Time  + DTm

!Updates the geometry, if necesary
      CALL Xupdate(X,Sol0,NodT,iDofT,NDim,iXupdateG)


      CALL Escribir(IE,JE,X,Sol0,Time,DTm,nStep,nStepOut,Nodos,NOlds,	
     & NodOUT,NodesOut,NodT,iDofT,NelT,NDim,iElementType,nOldTimeSteps)

      IF ( MOD(nStep,nStepOut) .EQ. 0 ) Then 
      Call writepar(MaxDatSet,Ie__Param,Ie_JParam,Param,JParam,iWDatSet)    

!      CALL EscribirVtk(IE,JE,X,Sol0,Time,DTm,nStep,nStepOut,Nodos,NOlds,	
!     &                    NodT,iDofT,NelT,NDim,iElementType )

      Endif 


!      if (mod(nStep,nkT).eq.0) Write(6,*)nStep,Time,DTm
      if (mod(nStep,nkT).eq.0) then
                                StrV = 'Time Step Finished'
!                                Call TimerOutStep(StrV)
                                StrV =  Repeat('_',59) 
                                write(6,'(1x,1A59,///)')StrV
      endif


!%!%!%!%!%!%!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      END DO
!&!&!&!&!&!&!&!&!&!&!$!$!$!$!/!\!/!\!/!\!/!\&&&&&&&&&&&&&&&&&&&&&&&&&&&

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IUnit_Ini=61
       write(6,*)'Writing IniFile.txt for restart '
       OPEN ( IUnit_Ini, FILE= 'IniFile.txt' )
       rewind IUnit_Ini 
       write(IUnit_Ini,'(1A19)')'*Initial Conditions'
       NFR = iDofT
C        write(formato,*)"(SP,",NFR,"E17.8E3)"  
	
       Do KNTi= 1 , nOldTimeSteps 
       iShG   = (KNTi-1) * LenUnK
          Do Nod=1, NodT
            NSh = ( Nodos(Nod)-1 )*iDofT + iShG
!            write(IUnit_Ini,402)( Sol0(NSh + J),J=1,iDofT )
!            write(IUnit_Ini,formato)( Sol0(NSh + J),J=1,iDofT )
            write(IUnit_Ini,'(E17.8E3)')( Sol0(NSh + J),J=1,iDofT )    
	    Enddo
       End do
       Write(IUnit_Ini,*)
       Write(IUnit_Ini,510)Time,DTm
       Close (IUnit_Ini)
 510   Format('*Time   [ T , DT ]',/,1E16.7,1X,1E16.7) 
! 610   Format(1A59,///)
 
! 402   Format(SP,formato)
!402     Format(SP,<NFR>(E17.8E3))

!      CALL Escribir (IE,JE,X,Sol0,Time,DTm,nStep,NodT,iDofT,NelT,NDim)
!      CALL EscribirVtk(IE,JE,X,Sol0,Time,DTm,nStep,NodT,iDofT,NelT,NDim,
!     *                 iElementType )

      Return
      END
!----------------------------------------------------------------------
      SUBROUTINE Xupdate(X,Sol,NodT,iDofT,Ndim,iXupdate)
      IMPLICIT REAL*8 (A-H,O-Z)
      Dimension X(1),Sol(1),iXupdate(1)

	do nsd=1,Ndim

       iXup=iXupdate(nsd)  
       select case (iXup)
        Case (:-1) 
           iXupa=abs(iXup)
           do nod=1,NodT
              X( (nod-1)*Ndim+nsd ) = Sol( (nod-1)*iDofT + iXupa )
           enddo
        Case (1:) 
           do nod=1,NodT
              X( (nod-1)*Ndim+nsd ) =   X( (nod-1)*Ndim  + nsd  ) 
     &                              + Sol( (nod-1)*iDofT + iXup )
           enddo
       End Select

      end do


      Return
      END

      SUBROUTINE subrelajar(Sol1b,Sol1a,Srp,iDofType,Length)
      IMPLICIT REAL*8 (A-H,O-Z)
      Dimension Sol1b(1),Sol1a(1),iDofType(1)
	do i=1,Length
	        IF(iDofType(i).lt.0)Cycle
	        Sol1b(i) = Srp*Sol1b(i) + (1.0d0-Srp)*Sol1a(i)
	enddo
      Return
      END

      
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     SUBROUTINE AssembDofType
! IE ()  : Row pointer by element to Je -Conectivity matrix-
! JE ()  : Nodes in an Elemnt -Conectivity matrix
! Dof                : degree of freedom				  
! DofType(NodT*DofT) : vector ordered by Node&Dof containing
!                      an identifier of the type of Dof for each unkwown
!                      (0 does not exist, <0 dirichlet, >0 Dof ID) 
! ElementType(NelT)  : list of Element property set ID.
!                      contains
!                              IdElemLib: Element ID in the ElemntLib 
! ElementLib()	  :	element library
!                     contains
!                           1  ElementID:
!                           2  iAdd
!                           3  Not Used
!                           4  IdCoup   : Coupling Property set ID
!						  5  
! NodeType_Set(IdElemType,Nod): for each node in an element type 
!  (related to ElementLib) Contains a pointer to the GlobalNodeType
! GlobalNodeType(Dof,NType): Node Type Data Set, vectors of length DofT
!                            containing 1s or 0s,
!                             identifying if a Dof is present(1) or not(0)
!                             in that NodeType   
! MaxElemlib    : Total Number of Elemnts in ElementLib
! RowT : Total number of equations in global system
!        (number of Global matrix Rows)							 
!-------------------------------------------------------------------
      SUBROUTINE AssembDofType1 ( IE,JE,DofType,
     &    ElementType,ElementLib,Coupling,
     &    NodT,NelT,DofT,MaxLRows,MaxElemlib,RowT )
!-------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER RowT,DofT,DofR,GlobalNode
      Integer ElementType(1),ElementLib(MaxElemlib,*),
     &        DofType(1),Coupling(MaxLRows,MaxLRows,1)      	 
      DIMENSION IE(1),JE(1)

	do Id=1, DofT*NodT
                         IF ( DofType( Id ) .GT. 0)DofType( Id )=0  
	enddo
!  Loop 10: asembling DofType	; for each element scanning
!       which kind of Dof is asociated with each node  
!      A loop on all Elements to obtain the vector DofType(NodT*DofT)
!      identifying each Dof for each Node
	Do 10 Nel=1,NelT
				IdElemLib   = ElementType (Nel)
                  IE1         = IE(Nel)
                  IE2         = IE(Nel+1)
                  IE11        = IE1-1
	            NodEl       = IE2-IE1
	            IdCoup      = ElementLib  (IdElemLib,4)
                  If(IdCoup==0)Cycle
! ojo!!!!!!!!!!!!!!!!!!!!!! el IE(NelT+1) tiene que llenarse
              Loop_Nod:Do Nod=1, NodEl
                 GlobalNode = JE ( IE11 + Nod )
                 ipGlobal   = (GlobalNode-1) * DofT
                 ipLocal    = (   Nod - 1  ) * DofT 

                Loop_DofR:Do DofR=1,DofT
! Loop en las filas
                  LocalRow = ipLocal + DofR

!Modificado antes iba Loop en columnas todos los comentarios que empiecen con !1*
!Esta version asume que toda fila que tiene alguna columna ocupada existe, lo cual es correcto
!Pero dificulta la implementacion de ciertos casos. Para eso preguntaremos por la diagonal y en los casos de 
!multiplicadores de Lagrange habra que agregar el termino de la diagonal en los multiplicadores y hacerlo cero
! Loop en las columnas
!1*               LocalCol = 0 
!1*               Do While (LocalCol .LT. MaxLRows)
!1*                LocalCol = LocalCol + 1

!1*                 If(Coupling (LocalRow,LocalCol,IdCoup).NE.0) Then
!2*                   If(Coupling (LocalRow,LocalRow,IdCoup).NE.0) Then
                      
                 iPointer = ipGlobal + DofR
                  IF (DofType( iPointer ).LT.0) Then
                                  DofType( iPointer )=-DofR
                  Else   
                      If(Coupling (LocalRow,LocalRow,IdCoup).NE.0) Then
                                  DofType( iPointer )=DofR
    				    Endif
!1*                      Exit
                  Endif
!1*               End Do
            End Do Loop_DofR
          End Do Loop_Nod

 10      Continue


        RowT=0
        Do 20 Ipointer=1,NodT*DofT
              IF (DofType( Ipointer ).NE.0) RowT=RowT+1
 20   Continue

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      SUBROUTINE EnSimGen 
! Assembles symbolic global matrix.
! Always assembles the diagonal despite it is present or not
! IE ()  : Row pointer by element to Je -Conectivity matrix-
! JE ()  : Nodes in an Elemnt -Conectivity matrix
! IET ()  : traspose of Conectivity matrix-
! JET ()  : traspose of Conectivity matrix-
! IA(),JA(): Global Simbolic matrix
! Dof                : degree of freedom				  
! DofType(NodT*DofT) : vector ordered by Node&Dof containing
!                      an identifier of the type of Dof for each unkwown
!                      (0 does not exist, <0 dirichlet, >0 Dof ID) 
! RowT         : Total number of Rows (equations)(local variable)
! IEq (RowT)   : vector ordered by Global matrix Row,
!                Values (Node-1)*DofT+Dof, Not used !!!!!!!!! 
! IEqNode(RowT): vector ordered by Global matrix Row,
!                Value Node number asociated with that Row(Equation) 
! IEqDof(RowT) : vector ordered by Global matrix Row,
!                Value Dof ID asociated with that Row(Equation)
! IEqT  (DofT*NodT): vector ordered by Node&Dof(like DofType), 
!                    Value: corresponding Row (or Equation)
! ElementType(NelT)  : list of Element property set ID.
!                      contains
! IdElemLib: Element ID in the ElemntLib 
! ElementLib()	  :	element library
!                     contains
!                            1 ElementID:
!                            2 IdAdd    : is an Additive Element
!                            3 
!                            4 IdCoup   : Coupling Property set ID
!						   5 NodEl
! NodeType_Set(IdElemNType,Nod): for each node in that Type of Elemen
!          ntains a pointer to GlobalNodeType
! GlobalNodeType(DofR,NType): a set of vectors (1 or 0)
!                               identifying
!                               if a Dof is present in that node  
! MaxElemlib    : Total Number of Elemnts in ElementLib
!							 
! IP(): aux vector, key vector to mark a col(umn) already assembled
!________________________________________________________________
      SUBROUTINE PreEnSimGen ( IE,JE,IET,JET,IA,
     &                   IP,DofType,IAdd,
     &                   ElementType,ElementLib,Coupling,
     &                   NodT,NelT,IEqNode,IEqDof,IEqT,DofT,MaxElemlib,
     &                   MaxLRows, L_JA )
!--------------------------------------------------------------------
!
!
      INTEGER PunJA,RowT,Row,Col,DofT,DofR,DofC,GlobalNode
      Integer ElementType(1),ElementLib(MaxElemlib,*),
     & Coupling(MaxLRows,MaxLRows,1),DofType(1)      	 
      DIMENSION IE(1),JE(1),IET(1),JET(1),   IA(1),IP(1),
     &  IEqNode(1),IEqDof(1),IEqT(1),IAdd(1)
 


      CALL INIENT ( IEqT,NodT*DofT,0 )

! A loop to identify each equation in the global system
! with its corresponding Node & Dof
      RowT=0
!------------------------------
          DO 40 Node = 1,NodT
!------------------------------
           IpoinDofType=(Node-1)*DofT
          
           Do DofR=1,DofT
             NType = DofType ( IpoinDofType + DofR )
    
             IF ( NType.NE.0) Then	
              RowT = RowT+1
              IEqNode (RowT) = Node
              IEqDof  (RowT) = DofR
              IEqT(IpoinDofType + DofR)=RowT
             End If
           End do
!------------------------------
 40       CONTINUE
!------------------------------

      CALL INIENT ( IAdd,RowT  ,0 )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     Asssembling iAdd vector, defining if a Row is aditive or not
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Do 10 Nel=1,NelT
                  IdElemLib   = ElementType (Nel)
                  IdElemAdd	= ElementLib(IdElemLib,2)
      if (IdElemAdd .NE. 0 ) then
                  IdCoup      = ElementLib(IdElemLib,4)
                  If(IdCoup==0)Cycle
!                  NodEl       = ElementLib(IdElemLib,5)
                  IE1         = IE(Nel)
                  IE2         = IE(Nel+1)
                  IE11        = IE1-1
                  NodEl       = IE2-IE1
            Do 5 Nod=1, NodEl

                 GlobalNode = JE ( IE11 + Nod )
                 ipGlobal   = (GlobalNode-1) * DofT
                 ipLocal    = (   Nod - 1  ) * DofT 

             Loop_Dof:Do DofR=1,DofT

                  LocalRow = ipLocal + DofR
!                  LocalCol = LocalRow 
!                  LocalCol = 0 
!                  Do While (LocalCol .LT. MaxLRows)
!                  LocalCol = LocalCol + 1
                  If(Coupling (LocalRow,LocalRow,IdCoup).LT.0) Then
!                 Filas que tienen un negativo en la diagonal son no aditivas
                       iPointer = ipGlobal + DofR
                       iRowNotAdd = IEqT( Ipointer )
                       iAdd(iRowNotAdd)=1
                  Endif
            End Do Loop_Dof

 5        Continue
      Endif

 10      Continue
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PRE_ASSEMBLING SYMBOLIC GLOBAL MATRIX
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CALL INIENT ( IP  ,RowT ,0 )
      PunJA  = 1
      NodeRow    = 0
!------------------------------
      DO WHILE (NodeRow .LT. NodT)
!------------------------------
          NodeRow    = NodeRow + 1
!	 this Row come from a Dof present in the correspondig node,
!      which in turn may be coupled with other Dof of another Node'
!	 and then must be assembled
!	    NodeRow    = IEqNode(Row)

       Do 60 DofR = 1, DofT
!	    DofR       = IEqDof(Row)
          IpNTypeR   = (NodeRow-1)*DofT + DofR
          Row = IEqT ( IpNTypeR )

!     Asembling diagonal Element
!	    JA (PunJA) = Row
!	    PunJA      = PunJA + 1
!	    IP (Row)   = Row

! si este grado de libertad en el nodorow no tiene fila asociada entonces
! venga un nuevo grado de libertad
       IF_Row: If (Row.NE.0) Then
          NTypeDofR  = DofType ( IpNTypeR )
          IniRowIA   = PunJA

! Si es de dirichlet me voy
        IF_Dirich:If (NTypeDofR.GT.0) Then

!             Agarro los elementos que estan conectados con ese nodo  
              IETA = IET (NodeRow)
              IETB = IET (NodeRow+1) - 1
              DO 50 IPun = IETA,IETB
!             recorro cada triangulo para sacar los nodos que estan conectados
              Nel         = JET(IPun)
              IdElemLib   = ElementType ( Nel )
              IdCoup      = ElementLib  ( IdElemLib , 4 )
              If(IdCoup==0)Cycle

              IEA         = IE( Nel   )
              IEB         = IE( Nel+1 ) - 1

             LocalNodeRow = 0
             IEA1 = IEA - 1
             Krow = IEA1
             DO While (Krow .LT. IEB)
!      en LocalNodeRow ubico en que posicion local esta Node Row
                 LocalNodeRow = LocalNodeRow + 1
                 Krow = Krow + 1
                 if (JE(Krow) .EQ. NodeRow) Exit
             End do
              LocalRow = (LocalNodeRow-1)*DofT + DofR

              ElemConected: DO KP = IEA,IEB
               NodeCol  = JE(KP)
               LocalNodeCol = KP - IEA1
               ipLocalCol = (LocalNodeCol-1)*DofT
               IpNTypeC   = ( NodeCol - 1  )*DofT

               Loop_DofC:Do DofC=1,DofT
                    IpCol     = IpNTypeC + DofC
                    NTypeDofC = DofType ( IPCol )
                    Col       = IEqT    ( IPCol )
                    LocalCol  = ipLocalCol + DofC
!	              Salto si no hay acople, o si el GrL es dirichlet
!                   o no esta presente
	              IF( Coupling(LocalRow,LocalCol,IdCoup).NE.0 
     &                                   .and. NTypeDofC.GT.0) Then 
!                      Ensamblo el triangulo superior 
                       IF ( Col.GT.Row ) Then  
!                            Salto si ya lo ensamble
                             IF ( IP(Col) .NE. Row ) Then 
!			                  JA (PunJA) = Col
                                PunJA      = PunJA + 1
                                IP (Col)   = Row
                             Endif
                       Endif
                    Endif 
               End do Loop_DofC
             End do ElemConected   
   
 50		  CONTINUE

          Endif IF_Dirich
!------------------------------
          LastRow  = Row
          IA (Row) = IniRowIA
!------------------------------
       Endif IF_Row
 60    CONTINUE

      End Do

      IA ( LastRow + 1 ) = PunJA
      L_JA        = PunJA - 1

      RETURN
      END

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SUBROUTINE EnSimGen ( IE,JE,IET,JET,IA,JA,IP,DofType,IAdd,
     & ElementType,ElementLib,Coupling,NodT,NelT,RowT,
     & IEqNode,IEqDof,IEqT,DofT,MaxElemlib,MaxLRows,
     & Long_JA,L_SKyLine)
!--------------------------------------------------------------------
      INTEGER PunJA,RowT,Row,Col,DofT,DofR,DofC,GlobalNode
      Integer ElementType(1),ElementLib(MaxElemlib,*),
     & Coupling(MaxLRows,MaxLRows,1),DofType(1)      	 
      DIMENSION IE(1),JE(1),IET(1),JET(1),IA(1),JA(1),IP(1),
     &  IEqNode(1),IEqDof(1),IEqT(1),IAdd(1)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! ASSEMBLING SYMBOLIC GLOBAL MATRIX
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CALL INIENT ( IP  ,RowT ,0 )
      PunJA  = 1
	NodeRow    = 0
!------------------------------
      DO WHILE (NodeRow .LT. NodT)
!------------------------------
          NodeRow    = NodeRow + 1
!	 this Row come from a Dof present in the correspondig node,
!      which in turn may be coupled with other Dof of another Node'
!	 and then must be assembled
!	    NodeRow    = IEqNode(Row)

       Do 60 DofR = 1, DofT
!	    DofR       = IEqDof(Row)
          IpNTypeR   = (NodeRow-1)*DofT + DofR
          Row = IEqT ( IpNTypeR )

!     Asembling diagonal Element
!	    JA (PunJA) = Row
!	    PunJA      = PunJA + 1
!	    IP (Row)   = Row

! si este grado de libertad en el nodorow no tiene fila asociada entonces
! venga un nuevo grado de libertad
       IF_Row:If (Row.NE.0) Then
          NTypeDofR  = DofType ( IpNTypeR )
          IniRowIA   = PunJA

! Si es de dirichlet me voy
          IF_Dirich:If (NTypeDofR.GT.0) Then

!             Agarro los elementos que estan conectados con ese nodo  
          IETA = IET (NodeRow)
          IETB = IET (NodeRow+1) - 1
          DO 50 IPun = IETA,IETB
!             recorro cada triangulo para sacar los nodos que estan conectados
              Nel         = JET(IPun)
              IdElemLib   = ElementType ( Nel )
              IdCoup      = ElementLib  ( IdElemLib , 4 )
              If(IdCoup==0)Cycle

              IEA         = IE( Nel   )
              IEB         = IE( Nel+1 ) - 1

              LocalNodeRow = 0
              IEA1 = IEA - 1
              Krow = IEA1
              DO While (Krow .LT. IEB)
!      en LocalNodeRow ubico en que posicion local esta Node Row
                 LocalNodeRow = LocalNodeRow + 1
                 Krow = Krow + 1
                 if (JE(Krow) .EQ. NodeRow) Exit
              End do
              LocalRow = (LocalNodeRow-1)*DofT + DofR

              ElemConected:DO KP = IEA,IEB
                NodeCol  = JE(KP)
                LocalNodeCol = KP - IEA1
                ipLocalCol = (LocalNodeCol-1)*DofT
                IpNTypeC   = ( NodeCol - 1  )*DofT

              Loop_DofC:DO DofC=1,DofT
                    IpCol     = IpNTypeC + DofC
                    NTypeDofC = DofType ( IPCol )
                    Col       = IEqT    ( IPCol )
                    LocalCol  = ipLocalCol + DofC
!	              Salto si no hay acople, o si el GrL es dirichlet
!                   o no esta presente
                  IF( Coupling(LocalRow,LocalCol,IdCoup).NE.0 
     &                                   .and. NTypeDofC.GT.0) Then 
!                      Ensamblo el triangulo superior 
                       IF ( Col.GT.Row ) Then  
!                            Salto si ya lo ensamble
                             IF ( IP(Col) .NE. Row ) Then 
                                JA (PunJA) = Col
                                PunJA      = PunJA + 1
                                IP (Col)   = Row
                             Endif
                       Endif
                    Endif 
               End do Loop_DofC
             End do ElemConected   
   
 50		  CONTINUE

          Endif IF_Dirich
!------------------------------
          LastRow  = Row
          IA (Row) = IniRowIA
!------------------------------
       Endif IF_Row
 60   CONTINUE

      End Do

      IA ( LastRow + 1 ) = PunJA
      Long_JA        = PunJA - 1


!------------------------------
      CALL INIENT ( IP  ,RowT ,0 )

      nRow    = 0
      DO WHILE (nRow .LT. RowT)
          nRow    = nRow + 1

          iCA = IA (nRow)
          iCB = IA (nRow+1)-1
          DO iCol = iCA,iCB
                    nCol = JA (iCol)
                    if ( IP(nCol) .Eq. 0 ) IP (nCol)= nCol-nRow
          End Do

      End Do
!------------------------------
      L_SKyLine = 0
      DO iRow = 1,RowT
          L_SKyLine = L_SKyLine + IP (iRow)
      End Do



      RETURN
      END

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Conver ( Sol1b,Sol1a,iDofT,NodT,rNorm,Tol,
     &                    NonLinearProblem,MaxIter,Names,iConver,nkT,
     &                    nStep,Iter )
      IMPLICIT REAL*8 (A-H,O-Z)
!      INTEGER, STATIC :: Iter
	Parameter (nst=14)
      Character(16) Names(iDofT)
      DIMENSION Sol1b(1),Sol1a(1),rNorm(1),Tol(1),NodeM(iDofT)
      Real*8 Maxi(iDofT)
	Logical NonLinearProblem
	Character(16)Ckey
!      Iter    = Iter + 1
      iConver = 1
      If ( NonLinearProblem ) then

          Maxi = 0.0d0   
          NodeM=0
	    Do Node = 1, NodT

           ip = ( Node - 1 ) * iDofT
           Do iDof = 1, iDofT
            ipp = ip + iDof   
            Error = Dabs( ( Sol1b(ipp)-Sol1a(ipp) ) / rNorm ( iDof ) ) 
            if (Error .GT. Maxi(iDof) ) then 
                      Maxi(iDof) = Error 
	                NodeM(iDof)=Node
	      endif
           End do

          End do

          Do iDof = 1, iDofT
                   if (Maxi(iDof) .GT. Tol(iDof) ) iConver = 0

          End do

	  if (mod(nStep,nkT).eq.0. .or.Iter.gt.nst) then
          write(6,*)
          write(6,*)
     &     '%%%%%%%%% OUTER NONLINEAR CONVERGENCE INFORMATION %%%%%%%%%'
          write(6,100)'DOF Name       ',
     *                'ERROR Value     ','TOL Value  ','Node' 
          Do iDof = 1, iDofT
                write(6,101)Names(iDof),Maxi(iDof),Tol(iDof),NodeM(iDof) 
          End do
          write(6,*)
	  endif
          If ( iConver. eq. 1) then 
                if (mod(nStep,nkT).eq.0) then  
                  write(6,*)'Nonlinear ITER =',Iter
                  write(6,*)'---->CONVERGENCE ACHIEVED AT ITER =',Iter 
                  write(6,*) 
                  write(6,*)
     &     '%%%%%%%%% OUTER NONLINEAR CONVERGENCE INFORMATION %%%%%%%%%'
                  write(6,*) 
                endif
                  Iter=0
          Else

                  if (mod(nStep,nkT).eq.0.or.Iter.gt.nst)then
                    write(6,*)'Nonlinear ITER =',Iter
                    write(6,*) 
                    write(6,*)
     &     '%%%%%%%%% OUTER NONLINEAR CONVERGENCE INFORMATION %%%%%%%%%'
                    write(6,*) 
                  endif

                  if (iTer. gt. MaxIter) then 
                    write(6,*)'NOT CONVERGED IN',MaxIter,'ITERATIONS' 
                    write(6,*)'NOT CONVERGED IN',MaxIter,'ITERATIONS' 
                    write(6,*)'PROGRAM STOPED, EXECUTION ABORTED' 
                    write(6,*)'!!!!!!!! PRESS <RETURN>' 
                    READ(5,*) 
                    Stop
                  endif

          Endif

      Endif
 100  format(1x,2A17,1A12,2x,1A4)
 101  format(1x,A16,1x,E10.3,7x,E10.3,2x,1I6)
 102  format(1x,A16,I4)

      RETURN
      END   
      SubRoutine ConverS ( Sol1b,Sol1a,iDofT,NodT,rNorm,Tol,
     &                    NonLinearProblem,MaxIter,Names,iConver,nkT,
     &                    nStep,nSubStep,Iter )
      IMPLICIT REAL*8 (A-H,O-Z)
!      INTEGER, STATIC :: Iter
	Parameter (nst=14)
      Character(16) Names(iDofT)
      DIMENSION Sol1b(1),Sol1a(1),rNorm(1),Tol(1)
      Real*8 Maxi(iDofT)
	Logical NonLinearProblem
	Character(16)Ckey
!      Iter    = Iter + 1
      iConver = 1
      If ( NonLinearProblem ) then

          Maxi = 0.0d0   

	    Do Node = 1, NodT

           ip = ( Node - 1 ) * iDofT
           Do iDof = 1, iDofT
            ipp = ip + iDof   
            Error = Dabs( ( Sol1b(ipp)-Sol1a(ipp) ) / rNorm ( iDof ) ) 
            if (Maxi(iDof) .LT. Error ) Maxi(iDof) = Error 
           End do

          End do

          Do iDof = 1, iDofT
                   if (Maxi(iDof) .GT. Tol(iDof) ) iConver = 0

          End do

	  if (mod(nStep,nkT).eq.0. .or.Iter.gt.nst) then
          write(6,*)
          write(6,*)
     &     '%%%%%%%%%%%% CONVERGENCE SUBSTEP INFORMATION %%%%%%%%%%%'
          write(6,104)'<< SubStep =',nSubStep,' || ITER =',Iter,' >>'
          write(6,*)
          write(6,100)'DOF Name       ',
     *                'ERROR Value     ','TOL Value       ' 
          Do iDof = 1, iDofT
                write(6,101)Names(iDof),Maxi(iDof),Tol(iDof) 
          End do
          write(6,*)
	  endif
          If ( iConver. eq. 1) then 
                if (mod(nStep,nkT).eq.0) then  
                  write(6,*)'---->CONVERGENCE ACHIEVED AT ITER =',Iter 
                  write(6,*)
     &     '%%%%%%%%%%%% CONVERGENCE SUBSTEP INFORMATION %%%%%%%%%%%'
                  write(6,*) 
                endif
                  Iter=0
          Else

                  if (mod(nStep,nkT).eq.0.or.Iter.gt.nst) then
                    write(6,*)
     &     '%%%%%%%%%%%% CONVERGENCE SUBSTEP INFORMATION %%%%%%%%%%%'
                    write(6,*) 
                  endif
                  if (iTer. gt. MaxIter) then 
                    write(6,*)'NOT CONVERGED IN',MaxIter,'ITERATIONS' 
                    write(6,*)'NOT CONVERGED IN',MaxIter,'ITERATIONS' 
                    write(6,*)'PROGRAM STOPED, EXECUTION ABORTED' 
                    write(6,*)'!!!!!!!! PRESS <RETURN>' 
                    READ(5,*) 
                    Stop
                  endif

          Endif
      
      
      else

         if (mod(nStep,nkT).eq.0) then  
	      write(6,*)
     &     '%%%%%%%%%%%%%%% LINEAR SUBSTEP INFORMATION %%%%%%%%%%%%%'
            write(6,102)'<< SubStep ',nSubStep,'  finished >>'
!	      write(6,*)
	      write(6,*)
     &     '%%%%%%%%%%%%%%% LINEAR SUBSTEP INFORMATION %%%%%%%%%%%%%'
	      write(6,*)

         Endif
      Endif
 100  format(1x,3A17)
 101  format(1x,A16,1x,E10.3,7x,E10.3)
 102  format(17x,1A11,I2,1A13)
 103  format(1x,1A9,I4)
 104  format(14x,1A12,I2,1A10,I4,1A3)

      RETURN
      END   




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Subroutine 
     &   Escribir (IE,JE,X,Sol,Time,DTm,nStep,nStepOut,Nodos,NOlds,
     & NodOUT,NodesOut,NodT,iDofT,NelT,NDim,iElementType,nOldTimeSteps)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION Sol(1),IE(1),JE(1),X(1),NodesOut(1),iElementType(1),
     &          Nodos(1),NOlds(1) 
      INTEGER, SAVE :: Istep
      character (LEN=20)::formato1,formato2
      integer ierr    

	Istep = Istep + 1
	IUnit = 50
      IUnit_Ini= 61
      IF_nStep: IF ( MOD(nStep,nStepOut) .EQ. 0 ) Then 
	Write(6,*)'Writing Output'
       OPEN ( IUnit_Ini, FILE= 'IniFile.txt' )
       Rewind IUnit_Ini
       write(IUnit_Ini,'(1A19)')'*Initial Conditions'
       NFR = iDofT
       LenUnK_G = NodT *iDofT 
       Do KNT = 1 , nOldTimeSteps 
       iShG  = (KNT-1) * LenUnK_G
!	  write(formato1,*)"(SP,",NFR," E17.8E3)"  	
!	  write(formato1,*) (SP,  NFR   E17.8E3)   	
          Do Nod=1, NodT
            NSh = ( Nodos(Nod)-1 )*iDofT + iShG
!ESTAS LINEAS TIENEN QUE SER DECOMENTADAS Y LAS SIGUIENTES DEBEN SER COMETADAS EN Sun OS!!
!           write(IUnit_Ini,formato1)( Sol(NSh + J),J=1,iDofT )
            write(IUnit_Ini,402)( Sol(NSh + J),J=1,iDofT )
	    Enddo
       End do
	    Write(IUnit_Ini,*)
          Write(IUnit_Ini,510)Time,DTm
 510   Format('*Time   [ T , DT ]',/,1E16.7,1X,1E16.7) 
       Close (IUnit_Ini)


		IF_Istep: If ( Istep .eq. 1) then
 555          OPEN ( IUnit, FILE= 'DataOut.txt',iostat=ierr) 
              if( ierr /=0) then
                  Write(6,*)'Error opening Dataout.txt, retrying'
                  goto 555
              endif
			Rewind IUnit 
!            IF_NodOut: if (NodOUT .LT. 1) then
!              NFI=4
!			Write (IUnit,403)NodT,NelT,iDofT,NDim
!              NFR= nDim
!              Do Nod=1, NodT
!                 NSh = ( Nodos(Nod)-1 )*NDim
!                 Write (IUnit,402) (X( NSh + I ),I=1,NDim)	
!              Enddo
!              NFI=5
!			Write (IUnit,403) ( IE(I),I=1,NelT+1)
!			do Nel=1, NelT
!			 Nodes= IE(Nel+1)-IE(Nel)
!               NFI = Nodes
!			 Write(IUnit,403)(NOlds(JE( IE(Nel)+Nod )),Nod=0,Nodes-1)	
!              enddo

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!            endif IF_NodOut

          else
 556         OPEN ( IUnit, FILE= 'DataOut.txt',position ='append')
              ierr = 0
              if( ierr /=0) then
                  Write(6,*)'Error opening Dataout.txt, retrying'
                  goto 556
              endif
          endif IF_Istep
	  
!ESTA LINEA TIENE QUE SER DECOMENTADAS Y LAS SIGUIENTES DEBEN SER COMETADAS EN Sun OS!!
!        write(formato1,*)"(SP,",NFR,"E17.8E3)"  	
          if (NodOUT .LT. 1) then
	      write (IUnit,401)nStep,Time,DTm
              NFR = iDofT
              Do Nod=1, NodT
                NSh = ( Nodos(Nod)-1 )*iDofT
               write(IUnit,402)( Sol(NSh + J),J=1,iDofT )
!                write(IUnit,formato1)( Sol(NSh + J),J=1,iDofT )
	        Enddo
          Else
!%%%%%%%%%%%% Escribe en los nodos Seleccionados %%%%%%%%
		    Write (IUnit,401)nStep,Time,DTm
	        NFR = iDofT
!ESTAS LINEAS TIENEN QUE SER DECOMENTADAS Y LAS SIGUIENTES DEBEN SER COMETADAS EN Sun OS!!
!		    Write (IUnit,formato1) ( (Sol( (Nodos(NodesOut(Nod))-1)*iDofT
!     &                            +iDoF),iDoF=1,iDofT),Nod=1,NodOUT )
		    Write (IUnit,402) ( (Sol( (Nodos(NodesOut(Nod))-1)*iDofT
     &                            +iDoF),iDoF=1,iDofT),Nod=1,NodOUT )

          Endif
		close (IUnit)

	endif IF_nStep 


 401  Format('*Time',/,I8,2G12.4E2)
 402  Format(SP,<NFR>(E17.8E3))
! 403  Format(<NFI>(I8))

	return
	end
!\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

!\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	Subroutine WriteIVect (ivec,long, IUnit)
	dimension ivec(1)

	    write(IUnit,402)(ivec(i),i=1,long)
!	    write(IUnit,*)
 402		Format(I9)

	return
	end

	Subroutine WriteVect (vec,long, IUnit)
	real*8 vec(1)

	    write(IUnit,402)(vec(i),i=1,long)
!	    write(IUnit,*)
 402		Format(E14.6)

	return
	end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      Subroutine Permutar
     &   (X,iDofType,Dirich,Sol0,Nodos,NodT,iDofT,NDim,nOldTimeSteps,
     &    NSubSteps)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION Sol0(1),X(NodT*NDim),iDofType(NodT*iDofT*NSubSteps),
     &  Dirich(NodT*iDofT*NSubSteps),Nodos(NodT),
     &  IVec(NodT*iDofT*NSubSteps),RVec( NodT*max(iDofT,Ndim)*NSubSteps)  

      LenUnK = NodT * iDofT

      IVec=iDofType
	RVec=Dirich
	Do Nstp=1,NSubSteps
	 iShStp=(Nstp-1)*LenUnK
              Do N=1,NodT
                  NShOlD = (N-1)*iDofT + iShStp
                  NShNew = (Nodos(N)-1)*iDofT + iShStp
                  Do iDof=1,iDofT
                   iDofType( NShNew+iDof )=IVec( NShOld+iDof )
                   Dirich  ( NShNew+iDof )=RVec( NShOld+iDof )
                  Enddo
              Enddo
      Enddo

      Do KNT = 1 , nOldTimeSteps 
        iSh  = (KNT-1) * LenUnK
        Do N = 1 , LenUnK
          RVec(N)= Sol0(N+iSh)
        Enddo
        Do N=1,NodT
          NShOlD = (    N-1   ) * iDofT
          NShNew = (Nodos(N)-1) * iDofT
          Do iDof=1,iDofT
           Sol0( NShNew+iDof + iSh ) = RVec( NShOld+iDof )
          Enddo
        Enddo
      Enddo

      Do N=1,NDim*NodT
       RVEC(N)=X(N)
      Enddo
      Do N=1,NodT
          NShOlD = (N-1)*NDim
          NShNew = (Nodos(N)-1)*NDim
          Do iDof=1,NDim
           X( NShNew+iDof )=RVec( NShOld+iDof )
          Enddo
      Enddo

      return
	end



