!      Module MatCoef
!      contains                                                          
                                                                         
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!                         SubRoutine MatrixCoef                           
!                                                                       
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! ElementLib()	  :	element library
!                     contains
!                            1 Id_Elem_Family : General Element Family
!                            2 The group of elemnts is additive or not
!                            3 
!                            4 Id_Coup   : Coupling Property set ID
!
!
! MaxElemlib    :     Total Number of Elements in ElementLib
!
!_______________________________________________________________________
	  SubRoutine MatrixCoef                                             &  
     &    (IE,JE,X,                                                     &
     &     IA,JA,AN,ANT,AD,                                             &
     &     Sol0,Sol1,B_Load,                                            &
     &     iDofType,iAdd,IP,                                            &
     &     Param,JParam,CommonPar,                                      &
     &     Ie__Param,Ie_JParam,MaxDatSet,                               &
     &     ElementType,ElementMat,ElementLib,                           &
     &     Coupling,                                                    &
     &     IEqNode,IEqDof,IEqT,                                         &
     &     NodT,NelT,iDofT,NDim,MaxNodEl,MaxElemlib,MaxLCommonPar,      &
     &     iRowT,MaxLRows,Sym,                                          &
     &     DelT,DTm,Time,nOldTimeSteps,iControl )

      IMPLICIT REAL*8 (A-H,O-Z)

      COMMON Det,LengthParam,LengthJParam

      DIMENSION IE(*),JE(*),X(*),                                       &
     &     IA(*),JA(*),AN(*),ANT(*),AD(*),                              &
     &     Sol0(*),Sol1(*),B_Load(*),                                   &
     &     iDofType(*),iAdd(*),IP(*),                                   &
     &     Param(*),JParam(*),CommonPar(MaxLCommonPar,*),               &
!    &     Ie__Param(1),Ie_JParam(1),Je__Param(1),Je_JParam(1),         &
     &     Ie__Param(*),Ie_JParam(*),                                   &
     &     IEqNode(*),IEqDof(*),IEqT(*),iControl(*)                     
	Logical Sym
      Integer ElementType(*),ElementMat(*),ElementLib(MaxElemLib,*),    &
     &        Coupling(MaxLRows,MaxLRows,*)   	 

      DIMENSION AE(MaxLRows,MaxLRows),XL(MaxNodEl*Ndim),Nodes(MaxNodEl), &
     &Sol0_Elem(MaxLRows*nOldTimeSteps),Sol1_Elem(MaxLRows),BE(MaxLRows) 

	DTm = DelT
	iElem = 0

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	Do While ( iElem < NelT )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          iElem     = iElem + 1
	do iC=1,MaxLrows
                        BE(iC)    = 0.0d0	
      do iR=1,MaxLrows
                        AE(iR,iC) = 0.0d0
	Enddo
	Enddo

          Id_DataSet = ElementMat  (iElem)
          IdElemLib  = ElementType (iElem)

            Id_Elem_Family            = ElementLib(IdElemLib,1)
            Id_Add                    = ElementLib(IdElemLib,2)
            Id_Coup                   = ElementLib(IdElemLib,4)
            If(Id_Coup==0)Cycle

!            Id_Elem_NodeType          = ElementLib(IdElemLib,4)
!	write(*,*)'Elem=',ielem,Id_Elem_Family

            iE1 = IE(iElem)
            ipJE1 = IE1-1
            NodEl                     = IE(iElem+1)-IE1

            iPar  = Ie__Param(Id_DataSet)
            iJPar = Ie_JParam(Id_DataSet)
			LengthParam  = Ie__Param(MaxDatSet+1) - 1
			LengthJParam = Ie_JParam(MaxDatSet+1) - 1


            Do N=1,NodEl
              Node = JE (ipJE1 + N)
              Nodes(N) = Node
	        NN    = Ndim * (N    - 1)
	        NNode = Ndim * (Node - 1)
              Do iDim=1, NDim
                          XL ( NN + iDim ) = X ( NNode + iDim )
              Enddo  
            Enddo  
!		if(iElem==396)then
!	        write(33,*)NodEL
!	        write(33,*)Nodes
!	        write(33,*)XL
!		endif
            Do N=1,NodEl
              ipLocal  = (N-1) * iDofT
              ipGlobal = ( Nodes(N)-1 )*iDofT
             Do iDof=1,iDofT
              ipLocal1  = ipLocal  + iDof
              ipGlobal1 = ipGlobal + iDof
              Sol1_Elem(ipLocal1) = Sol1(ipGlobal1)
             Enddo  
            Enddo  

          LenUnK_G = NodT *iDofT 
          LenUnK_L = NodEl*iDofT 
          Do KNT = 1 , nOldTimeSteps 
            iShG  = (KNT-1) * LenUnK_G
            iShL  = (KNT-1) * LenUnK_L
            Do N=1,NodEl
              ipLocal  =     (N-1)     * iDofT + iShL
              ipGlobal = ( Nodes(N)-1 )* iDofT + iShG
              Do iDof=1,iDofT
               ipLocal1  = ipLocal  + iDof
               ipGlobal1 = ipGlobal + iDof
               Sol0_Elem(ipLocal1) = Sol0(ipGlobal1)
              Enddo  
            Enddo  
          End do
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          Element_Family: SelectCase (Id_Elem_Family)
!_______________________________________________________________________
          Case (1)
!      One-Dimensional Pipe-Flow LeastSquares Formulation over Characteristics Lines 
          Call PipeFlow1D_QP (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,  &
!          Call PipeFlow1D_QPPe (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,     &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
!    &    Je__Param(iPar),Je_JParam(iJPar),Param,JParam,DelT,DTm,Time)
     &                        Param(iPar),JParam(iJPar),DelT,DTm,Time)

          Case (2)
!      One-Dimensional Pipe-Flow Lax-Wendroff Formulation
          Call PipeFlow1D_LW (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,       &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )
          Case (3)
!      One-Dimensional Pipe-Flow Conection Element
!          Call PipeFlow1D_UP_Conec (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,
          Call PipeFlow1D_QP_Conec (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl, &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                        Param(iPar),JParam(iJPar),DelT,DTm,Time)
          Case (4)
!      One-Dimensional Pipe-Flow Lax-Wendroff Formulation Boundary Element
          Call PipeFlowIN_LW (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,       &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )

          Case (5)
!      One-Dimensional Pipe-Flow Lax-Wendroff Formulation Boundary Element
          Call PipeFlowOUT_LW (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,      &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )

          Case (10)
!      One-Dimensional Pipe-Flow QPeAPt Conection Element
          Call PipeFlow1D_QPPe_Conec(AE,BE,MaxLRows,XL,NDim,iDofT,NodEl, &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   & 
     &                        Param(iPar),JParam(iJPar),DelT,DTm,Time)
          Case (11)
!      One-Dimensional Pipe-Flow QPeAPt Inlet Boundary
          Call PipeFlow1D_QPeAPtIN (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,  &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )

          Case (12)
!      One-Dimensional Pipe-Flow QPeAPt Outlet Boundary
          Call PipeFlow1D_QPeAPtOUT (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl, &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )

          Case (13)
!      One-Dimensional Pipe-Flow QPeAPt
          Call PipeFlow1D_QPeAPt (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,   &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                     Param(iPar),JParam(iJPar),DelT,DTm,Time )


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


          Case (22)
          Call Coup2D1DNV                                               &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (23)
          Call Coup2D1DNVTer                                            &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (230)
          Call Coup2D1DNVTer_Alfa                                       &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (24)
          Call Coup2D1DNVTerP                                           &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (52)
          Call NavierStokesBubALE                                       &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (520)
          Call NavierStokesBubALE                                       &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (521)
          Call NavierStokesBubALE2                                      &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (522)
          Call NavierStokesBubALEDummy                                  &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (523)
          Call NavierStokesBubALEDummy2                                 &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (524)
          Call NavierStokesBubALEDummyTP1                               &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (525)
          Call NavierStokesBubALEDummyTP2                               &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
 
          Case (5245)
          Call NS3DIncPr				                                &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )


          Case (540)
          Call Presure_NV_Linear                                        &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

!          Case (53) !reemplazado por el 3D1D (54)
!          Call Coup_2D1D_NV_Linear 
!     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,
!     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),
!     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (541)   !desacopla el 1D del 3d Iterndo entre ellos en 1 solo substep
          Call Coup_3D1D_NV_LinearD                                     &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (542)   !desacopla el 1D del 3d totalmente no iterando resolviendo en substeps distintos
          Call Coup_3D1D_NV_LinearDD                                     &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (54)
          Call Coup_3D1D_NV_Linear                                      &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (550)
          Call Coup_3D1D_NV_Linear_Alfa                                 &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (55)
          Call Coup_3D1D_NV_Quad                                        &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )


!          Case (56)  ! Reemplazado por el 3D
!          Call Spring2DNV_Linear
!     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,
!     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),
!     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (570)
          Call Spring3DNV_LinearSplit1                                  &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (571)
          Call Spring3DNV_LinearSplit2                                  &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (57)
          Call Spring3DNV_Linear                                        &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (579)
          Call Spring3DNV_LinearDes                                        &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (5705)
          Call Spring3DIncPr	                                        &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (58)
          Call Spring3DNV_Quad                                          &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (100)
	    Call DirichElem                                                 &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )
          Case (101)
	    Call DirichElemOblic1                                           &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (102)
	    Call VarImpFT                                                   &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (104)
	    Call DirichElemOblic2                                           &
     &                  ( AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,           &
     &                    Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),   &
     &                    Param(iPar),JParam(iJPar),DelT,DTm,Time    )

          Case (110)
!      Add concentrated loads to the 2nd member
          Call ConcentratedLoad (AE,BE,MaxLRows,XL,NDim,iDofT,NodEl,    &
     &                     Sol0_Elem,Sol1_Elem,CommonPar(1,IdElemLib),  &
     &                        Param(iPar),JParam(iJPar),DelT,DTm,Time)

	    Case Default
                   Write(*,*) '!!!!!!! WARNING, ELEMENT NOT IDENTIFIED'	
!_______________________________________________________________________


          End Select Element_Family  
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


!     Asembles LocalElement Matrix in Global Matrix

if (Det .lt. 0.0d0) then
       Write(6,*)'WARNING, jacobian < 0 at element',iElem,Det

            Write(6,*)'Id_Elem_Family',Id_Elem_Family
            X11= 0.0
            X22= 0.0
            X33= 0.0

            Do N=1,NodEl
	        NN    = Ndim * (N    - 1)
            X11= X11 +  XL ( NN + 1 )/NodEl
            X22= X22 +  XL ( NN + 2 )/NodEl
            X33= X33 +  XL ( NN + 3 )/NodEl
            Enddo  
            write(6,*)'Coordinates',X11,X22,X33

End if


      Call EnsNumGen ( AE,Nodes,IA,JA,AN,ANT,AD,                        &
     &      B_Load,BE,IP,IEqT,Coupling(1,1,Id_Coup),                    &
     &      iDofType,MaxLRows,iDofT,NodEl,Id_Add,iAdd,Sym)



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	End Do
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Return

End Subroutine
!      end module MatCoef
!-----------------------------------------------------------------------
      SUBROUTINE EnsNumGen ( AE,Nodes,IA,JA,AGlobal_U,AGlobal_L,AD,             &
     &      BG,BE,IP,IEqT,iCoupling,DofType,MaxLRows,DofT,NodEl,Id_Add,iAdd,Sym)

!     ojo con MaxLRows=DofT*MaxNodEl
!     NetRows = IpIELocal(IdelemLib + 1 ) - IpIELocal( IdelemLib )
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER   DofT,DofR,DofC,PunJA,DofType(*)
      DIMENSION AE(MaxLRows,*),Nodes(*),IA(*),JA(*),                    &
     &          AGlobal_U(*),AGlobal_L(*),AD(*),BG(*),BE(*),IP(*),      &
     &          IEqT(*),iAdd(*),iCoupling(MaxLRows,MaxLRows)
	Logical Sym,NOTSym
	NOTSym = .not. Sym	     
!--------------------------          
      DO NodR=1, NodEl
          
        ipLRow       = ( NodR -1)*DofT
        NodRGlobal   = Nodes (NodR)
        iPGlobalR    = (NodRGlobal-1)*DofT
      DO DofR=1, DofT
!--------------------------
         LRow          = ipLRow  + DofR
!         NodR         = LocalNodeR (L)        
!         DofR         = LocalDofR  (L)        
         IpIEqTR       = iPGlobalR + DofR
         iRow          = IEqT   ( IpIEqTR )
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	   if ( iRow.Ne. 0 ) then 
            NodeTypeR     = DofType( IpIEqTR )
!      if(iAdd(iRow).Eq.0 .or. Id_Add .Eq. 1) then
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!         IP (iRow) = LRow
!	  Dirichlet Row
            if (NodeTypeR.GT.0) Then
!               Not a Dirichlet Row

!               if(iAdd(iRow).Eq.0 .or. Id_Add .Eq. 1) then
                if(iAdd(iRow).Eq.0 ) then
!               assembling the diagonal
                AD(iRow)  = AD(iRow) + AE(LRow,LRow)
                BG(iRow)  = BG(iRow) + BE(LRow)
            elseif (iCoupling(LRow,LRow).LT.0 )then 

                AD(iRow)  = AD(iRow) + AE(LRow,LRow)
                BG(iRow)  = BG(iRow) + BE(LRow)
            Endif

          Do NodC=1,NodEl
	        ipLCol       = ( NodC -1)*DofT
            NodCGlobal   = Nodes (NodC)
	        iPGlobalC    = (NodCGlobal-1)*DofT

            Do DofC=1,DofT
			   LCol        = ipLCol + DofC        
              if ( iCoupling(LRow,LCol) .NE. 0 ) then 
                  IpIEqTC     = iPGlobalC + DofC
                  iCol        = IEqT   ( IpIEqTC )
			      NodeTypeC   = DofType( IpIEqTC )

!			    Dirichlet	Col
				    IF (NodeTypeC.GT.0) Then	
				        IF (iCol .GT. iRow) then
                                              IP (iCol) = LCol
!                        ElseIf (iCol .Eq. iRow) then
!                                   AD(iRow)  = AD(iRow) + AE(LRow,LCol)
                        Endif
				    Else
!                      Dirichlet Column 
                     if(iAdd(iRow).Eq.0 ) then
				        BG(iRow)   = BG(iRow) - AE(LRow,LCol)*BG(iCol)
                     elseif (iCoupling(LRow,LRow).LT.0 )then 
				        BG(iRow)   = BG(iRow) - AE(LRow,LCol)*BG(iCol)
                     Endif
                    
                    Endif

               Endif
            End Do
          End Do

           IAA = IA(iRow)
           IAB = IA(iRow+1)-1

           DO PunJA = IAA,IAB
               iColGl= JA(PunJA)      
               LCol = IP ( iColGl )

             IF ( LCol .NE. 0 ) then 
!            IF(iAdd(iRow).Eq.0 .or. Id_Add .Eq. 1) then
               IF(iAdd(iRow).Eq.0 ) then
	                                      AGlobal_U(PunJA) = AGlobal_U(PunJA) + AE(LRow,LCol)
                elseif (iCoupling(LRow,LRow).LT.0 )then 
                                          AGlobal_U(PunJA) = AGlobal_U(PunJA) + AE(LRow,LCol)
               Endif
               if (NOTSym) then	
!               IF(iAdd(iColGl).Eq.0 .or. Id_Add .Eq. 1) then
                 IF(iAdd(iColGl).Eq.0 ) then
                                            AGlobal_L(PunJA) = AGlobal_L(PunJA) + AE(LCol,LRow)
                 elseif (iCoupling(LCol,LCol).LT.0 )then 
                                            AGlobal_L(PunJA) = AGlobal_L(PunJA) + AE(LCol,LRow)
                 Endif
               Endif
                    IP ( iColGl ) = 0
            
             ENDIF

           Enddo


         Else  
!               A Dirichlet Row . If comented, dirichlet are treated in
!                AD(iRow)=1.0d0

         Endif
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!      cierra if  aditividad
!      Endif
! cierra si la fila no esta
      Endif
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!--------------------------          
      End Do
     End Do
!--------------------------          
      RETURN
	END subroutine




