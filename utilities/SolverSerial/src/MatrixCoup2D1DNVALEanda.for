C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup2D1DNV
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
      Parameter (Theta = 1.0D0 )
      Parameter (Theta1= 1.0d0-Theta )

C     NDimE: dimensionality of the simplex element,
C     NGP= Number of gauus points
C     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
      Parameter (NodEL = 3 )
      Parameter (NDimE = 1 )
      Parameter (	NGP = 3 )
c      Real*8 Jac(NDimE,NDimE)
c      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),QC(NodEL),
     &  XLL(*),Sol0((Nodel+1)*idofT),Sol1((Nodel+1)*iDofT),
     &  Pm(NodEL),XL(NDim*NodEl),
     &  Param(*), JParam(*),CommonPar(*),
     &  Psi(NDimE,NGP),W(NGP),Vel0(NDim),Vel1(NDim),Pres(NodEL),
     &  dFi1_L(NDimE,NodEl),dFi2_L(NDimE,NodEl),dFi2_G(NDimE,NodEL),
     &  Fi1(NodEl),Fi2(NodEl),dFi2i(NDimE),Vt(NDim),Vn(NDim)


C      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
c      OnlyVertex = .True. 
           
	Penalty = CommonPar(1)
C      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
C      si es grande se impone aditivamente.  
      Last = 2
	iSimetry= nint(CommonPar(2))
	Last= Last+1
	iSimetryAxis= nint(CommonPar(3))
	Last= Last+1
	DPnor= CommonPar(4)
      Do nci=1, NDim
C          Vn= Vector Normal a la Interfase, eg, (-1,0) interfase entrante
           Vn(nci)= CommonPar( Last+nci )
      end do
      Last = Last+NDim+1

      Do Nod=1, NodEl
C       ojo que el primer nodo es el terminal, las coordenadas
C       en XLL estan desplazadas un nodo
       Do nsd=1, NDim
        if (nsd .ne. iSimetryAxis+1) then
         XL( (Nod-1)*NDim + nsd ) = XLL( (Nod  )*NDim + nsd)
        else
         XL( (Nod-1)*NDim + nsd ) = Sol1 ( Nod*iDofT + NDim + 2 )
        endif
       end do
      end do

      DX = 0.0d0
      Do nsd=1, NDim
                      Vt(nsd) = XL( NDim + nsd ) - XL( nsd )
                      DX       = DX + ( Vt(nsd) )**2
      end do
      DX = DSqrt( DX )
      Vt=Vt/DX

C      iSimplex = 0 -> The element is a Simplex
C      iSimplex = 1 -> The element is a Hipercube
      iSimplex = 0     
C     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
C     iBU: use bubble function
      iBU=0
C     NodG = number of geometric nodes
C     NodP = number of pressure nodes
      NDimE1  = NDimE+1
      NodP    = 2
      NodG    = NodP

      QC=0.0d0
      Pm=0.0d0
      Det = DX
C--------------------------
      LoopGaussPoints: Do nG = 1, NGP
C--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DetW=Det*W(nG)

       if (iSimetry .eq. 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodG 
                 Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
                Enddo
               DetW=DetW*2.0d0*PI*Radio
       Endif

C%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  Fi2i = Fi2(i)
                  QC(i)= QC(i) + Fi2i*DetW
C                  Pm(i) = Pm(i) + Fi2i*DetW
       End do LoopRow
C%%%%%%%%%%%%%%%%%%%%%%%
C--------------------------
      EndDo LoopGaussPoints
C--------------------------
      Pm=QC*DPnor

C------------------------------------
      ipRowT    =  0 
      ipRowQT   =  ipRowT  + 1
      ipColPT   =  ipRowQT + NDim

      do NodR=1, NodEL
       ipRow   = NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty
!       if (iSimetry .eq. 0) VnP=VnP*PI
C        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP
C        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta
!Si va theta falta agregar en Be
         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)

       Enddo
      Enddo

      Return
      End


C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup2D1DNVTer
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      Parameter (PI = 3.141592653589793238D0)
	IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION AE(MaxLRows,MaxLRows),BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(iDofT),
     &  Param(*), JParam(*),CommonPar(*)

	Penalty   = CommonPar(1)
	PresTer   = CommonPar(2)
	CTer      = CommonPar(3)
	Direction = CommonPar(4)
!Direction, terminal Sign.
	Direction   = dsign(1.0d0, Direction) 

      ipRowQT  = 1
      ipRowPT  = ipRowQT + NDim

!      AE(ipRowQT,ipRowQT) = dsign( -1.0d0 , Direction )*Penalty
      AE(ipRowQT,ipRowQT) = Penalty*Direction
      AE(ipRowQT,ipRowPT) = -CTer*Penalty
      BE(ipRowQT)         = -CTer*PresTer*Penalty
      Return
      End

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup2D1DNVTer_Alfa
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!      Parameter (PI = 3.141592653589793238D0)
	IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION AE(MaxLRows,MaxLRows),BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(iDofT),
     &  Param(*), JParam(*),CommonPar(*)

	Penalty   = CommonPar(1)
	PresTer   = CommonPar(2)
	CTer      = CommonPar(3)
	Direction = CommonPar(4)
	QNorm     = CommonPar(5)

!Direction, terminal Sign.
	Direction   = dsign(1.0d0, Direction) 

      ipRowQT    = 1
      ipRowPT    = ipRowQT + NDim
	ipRowAlfaT = ipRowPT + 1

!      AE(ipRowQT,ipRowQT) = dsign( -1.0d0 , Direction )*Penalty
      AE(ipRowQT,ipRowQT)       = Penalty*Direction
      AE(ipRowQT,ipRowPT)       = -CTer*Penalty
      BE(ipRowQT)               = -CTer*PresTer*Penalty

	AE(ipRowAlfaT,ipRowAlfaT) = 1.0d0

      Return
      End

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup2D1DNVTerP
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION AE(MaxLRows,MaxLRows),BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),
     &  Param(*), JParam(*),CommonPar(*)

	Penalty   = CommonPar(1)
	PresTer   = CommonPar(2)
	CTer      = CommonPar(3)

      ipRowQT  = 1
      ipRowPT  = ipRowQT + NDim

      AE(ipRowQT,ipRowQT) = Penalty
c      AE(ipRowQT,ipRowPT) = -CTer*Penalty
c      BE(ipRowQT)         = -CTer*PresTer*Penalty

      AE(ipRowPT,ipRowPT) = CTer*Penalty
      AE(ipRowPT,ipRowQT) = Penalty
      BE(ipRowPT)         = CTer*PresTer*Penalty

      Return
      End
