!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_3D1D_NV_Linear                      &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,  &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (NodEL = 3 )
!      Parameter (NdimE = 2 )
!      Parameter (NdimG = 3 )
!      Parameter (NdimE1 = 3, NdimG1 = 4)      
      Parameter (	NGP = 4 )
!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         & 
     &  QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodELT-1)),  &
     &  Param(*), JParam(*),CommonPar(*),               &
     &  Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &  
     &  Vn(3),XA(3),XB(3),Fi1(3),PSI(2,NGP),W(NGP)
           
    NodEL = NodELT-1 ; NdimE = Ndim -1 ; NdimG = Ndim ; NdimE1 = Ndim ; NdimG1 = Ndim + 1      

	Penalty     =       CommonPar(1)
	CN          =       CommonPar(2)
    Incremental = nint( CommonPar(3) ) 
    Theta       = CommonPar(4)
    Theta1      = 1.0d0-Theta
	iSimetry    = nint( CommonPar(5) )
	iSimetryAxis= nint( CommonPar(6) )

!   CN Normal sign (outward positive)
	CN          = dsign(1.0d0, CN) 
    OpTi=1.0d0 !*Dtm

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do

! Side Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
      Acoef = 6.0d0
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0
Endif       
!Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN



 if (iSimetry == 1 .and. NdimE == 1) then

      Det=Dabs(DA)
      iSimplex = 0     
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
!     iBU: use bubble function
      iBU=0

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
!       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DetW=Det*W(nG)

       Radio = 0.0d0
       nci = 1 + iSimetryAxis
       Do Nodrad=1, NDimE1 
            Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
       Enddo
       DetW=DetW*2.0d0*PI*Radio

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  Fi1i = Fi1(i)
                  QC(i)= QC(i) + Fi1i*DetW
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------


 else 

      QC = DA / Acoef

 Endif

  Pm = QC

!------------------------------------
!      ipRowT    =  0 
!      ipRowQT   =  ipRowT  + 1
      ipRowQT   =  1
      ipColPT   =  ipRowQT + NDimG
      do NodR  =1, NodEL
        ipRow  =   NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty

!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP

!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo, va positivo porque esta del lado de las incognitas, no del segundo miembro 
         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta*OpTi
         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)*OpTi
!               AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_3D1D_NV_LinearD                     &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,  &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (NodEL = 3 )
!      Parameter (NdimE = 2 )
!      Parameter (NdimG = 3 )
!      Parameter (NdimE1 = 3, NdimG1 = 4)      
      Parameter (	NGP = 4 )
!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         & 
     &  QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodELT-1)),  &
     &  Param(*), JParam(*),CommonPar(*),               &
     &  Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &  
     &  Vn(3),XA(3),XB(3),Fi1(3),PSI(2,NGP),W(NGP)
           
    NodEL = NodELT-1 ; NdimE = Ndim -1 ; NdimG = Ndim ; NdimE1 = Ndim ; NdimG1 = Ndim + 1      

	Penalty     =       CommonPar(1)
	CN          =       CommonPar(2)
    Incremental = nint( CommonPar(3) ) 
    Theta       = CommonPar(4)
    Theta1      = 1.0d0-Theta
	iSimetry    = nint( CommonPar(5) )
	iSimetryAxis= nint( CommonPar(6) )

!   CN Normal sign (outward positive)
	CN          = dsign(1.0d0, CN) 
    OpTi=1.0d0 !*Dtm

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do

! Side Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
      Acoef = 6.0d0
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0
Endif       
!Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN



 if (iSimetry == 1 .and. NdimE == 1) then

      Det=Dabs(DA)
      iSimplex = 0     
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
!     iBU: use bubble function
      iBU=0

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
!       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DetW=Det*W(nG)

       Radio = 0.0d0
       nci = 1 + iSimetryAxis
       Do Nodrad=1, NDimE1 
            Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
       Enddo
       DetW=DetW*2.0d0*PI*Radio

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  Fi1i = Fi1(i)
                  QC(i)= QC(i) + Fi1i*DetW
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------


 else 

      QC = DA / Acoef

 Endif

  Pm = QC

!------------------------------------
!      ipRowT    =  0 
!      ipRowQT   =  ipRowT  + 1
      ipRowQT   =  1
      ipColPT   =  ipRowQT + NDimG
      do NodR  =1, NodEL
        ipRow  =   NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty

!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
!         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP
         BE ( ipRowQT ) = BE ( ipRowQT ) - QC(NodR)*VnP*Sol1(ipRowV)

!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo, va positivo porque esta del lado de las incognitas, no del segundo miembro 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta*OpTi
!         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)*OpTi
         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta*Sol1(ipColPT)*OpTi- Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)*OpTi
!               AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_3D1D_NV_LinearDD                    &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,  &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (NodEL = 3 )
!      Parameter (NdimE = 2 )
!      Parameter (NdimG = 3 )
!      Parameter (NdimE1 = 3, NdimG1 = 4)      
      Parameter (	NGP = 4 )
!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         & 
     &  QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodELT-1)),  &
     &  Param(*), JParam(*),CommonPar(*),               &
     &  Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &  
     &  Vn(3),XA(3),XB(3),Fi1(3),PSI(2,NGP),W(NGP)
           
    NodEL = NodELT-1 ; NdimE = Ndim -1 ; NdimG = Ndim ; NdimE1 = Ndim ; NdimG1 = Ndim + 1      

	Penalty     =       CommonPar(1)
	CN          =       CommonPar(2)
    Incremental = nint( CommonPar(3) ) 
    Theta       = CommonPar(4)
    Theta1      = 1.0d0-Theta
	iSimetry    = nint( CommonPar(5) )
	iSimetryAxis= nint( CommonPar(6) )

!   CN Normal sign (outward positive)
	CN          = dsign(1.0d0, CN) 
    OpTi=1.0d0 !*Dtm

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do

! Side Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
      Acoef = 6.0d0
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0
Endif       
!Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN



 if (iSimetry == 1 .and. NdimE == 1) then

      Det=Dabs(DA)
      iSimplex = 0     
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
!     iBU: use bubble function
      iBU=0

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
!       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DetW=Det*W(nG)

       Radio = 0.0d0
       nci = 1 + iSimetryAxis
       Do Nodrad=1, NDimE1 
            Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
       Enddo
       DetW=DetW*2.0d0*PI*Radio

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  Fi1i = Fi1(i)
                  QC(i)= QC(i) + Fi1i*DetW
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------


 else 

      QC = DA / Acoef

 Endif

  Pm = QC

!------------------------------------
!      ipRowT    =  0 
!      ipRowQT   =  ipRowT  + 1
      ipRowQT   =  1
      ipColPT   =  ipRowQT + NDimG
      do NodR  =1, NodEL
        ipRow  =   NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty

!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
!         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP
         BE ( ipRowQT ) = BE ( ipRowQT ) - QC(NodR)*VnP*Sol1(ipRowV)

!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo, va positivo porque esta del lado de las incognitas, no del segundo miembro 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta*OpTi
!         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)*OpTi
!         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta*Sol1(ipColPT)*OpTi- Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)*OpTi
!               AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_3D1D_NV_Linear_Alfa                 &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,  &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (NodEL = 3 )
!      Parameter (NdimE = 2 )
!      Parameter (NdimG = 3 )
!      Parameter (NdimE1 = 3, NdimG1 = 4)      
      Parameter (	NGP = 4 )
!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         & 
     &  QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodELT-1)),  &
     &  Param(*), JParam(*),CommonPar(*),               &
     &  Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &  
     &  Vn(3),XA(3),XB(3),Fi1(3),PSI(2,NGP),W(NGP),		&
	 &  Vit(3,3),VescN(3),Vmod(3),VitGP(3)
           
    NodEL = NodELT-1 ; NdimE = Ndim -1 ; NdimG = Ndim ; NdimE1 = Ndim ; NdimG1 = Ndim + 1      

	Penalty     =       CommonPar(1)
	CN          =       CommonPar(2)
    Incremental = nint( CommonPar(3) ) 
    Theta       = CommonPar(4)
    Theta1      = 1.0d0-Theta
	iSimetry    = nint( CommonPar(5) )
	iSimetryAxis= nint( CommonPar(6) )
	QNorm       = CommonPar(7)

!   CN Normal sign (outward positive)
	CN          = dsign(1.0d0, CN) 

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do

! Side Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
      Acoef = 6.0d0
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0
Endif       
!Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN


 if (iSimetry == 1 .and. NdimE == 1) then
      QC = 0.0d0
      Det=Dabs(DA)
      iSimplex = 0     
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
!     iBU: use bubble function
      iBU=0

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
!       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DetW=Det*W(nG)

       Radio = 0.0d0
       nci = 1 + iSimetryAxis
       Do Nodrad=1, NDimE1 
            Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
       Enddo
       DetW=DetW*2.0d0*PI*Radio

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  QC(i)= QC(i) + Fi1(i)*DetW
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------

 else 

      QC = DA / Acoef

 Endif




  Pm = QC

  ipRowQT    = 1
  ipColPT    = ipRowQT + NDimG
  ipRowAlfaT = ipColPT + 1
  ipRowVIni	 = iDofT



  Vit   = 0.0d0
  VescN = 0.0d0
  Vmod  = 0.0d0

  Ait   = Sol1(ipColPT-1)
  Qit   = Sol1(ipRowQT)

  If (Dabs(Qit) .gt. QNorm*1e-3) Then

      AsQit = Ait / ( Qit * Qit )

      Do k=1, NodEl
        Do nci=1, NDim
          Vit(k,nci) = Sol1(k*iDofT+nci)
	    EndDo
      EndDo
  
      Do k=1, NodEl
        Do nci=1, NDim
	      VescN(k) = VescN(k) + Vit(k,nci) * Vn(nci)
          Vmod(k)  = Vmod(k)  + Vit(k,nci) * Vit(k,nci)
	    EndDo
      EndDo

      Det=Dabs(DA)

! iBU: use bubble function
      iSimplex = 0     
      iBU=0

! Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    

!--------------------------
    LoopGaussPointsAlfa: Do nG = 1, NGP
!--------------------------
        Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)

        DetW=Det*W(nG)

	    VitGP     = 0.0d0
	    VitGPescN = 0.0d0
	    VitGPmod  = 0.0d0

        Do nci=1, NDim
	      Do nod=1, NodEl
            VitGP(nci) = VitGP(nci) + Vit(nod,nci) * Fi1(nod)
          EndDo
	    EndDo
        Do nci=1, NDim
	      VitGPescN = VitGPescN + VitGP(nci) * Vn(nci)
	      VitGPmod  = VitGPmod  + VitGP(nci) * VitGP(nci)
	    EndDo

	    CosTheta2 = VitGPescN*VitGPescN / VitGPmod

!%%%%%%%%%%%%%%%%%%%%%%%
        LoopCol: Do i=1,NodEL
	      ipRow0 = ipRowVIni + (i-1)*iDofT
          Fi1i = Fi1(i)

          Do nci=1, NDim
	        ipRowV = ipRow0 + nci
            AE(ipRowAlfaT , ipRowV) = AE(ipRowAlfaT , ipRowV) -			&
								      AsQit*VitGPescN*Vn(nci)*Fi1i*DetW
!								    1.0d0*AsQit*VitGP(nci)*CosTheta2*Fi1i*DetW
!								    2.0d0*AsQit*VitGP(nci)*CosTheta2*Fi1i*DetW
	      EndDo

        End do LoopCol
!%%%%%%%%%%%%%%%%%%%%%%%

        BE(ipRowAlfaT) = BE(ipRowAlfaT) ! - AsQit*VitGPescN*VitGPescN*DetW

!--------------------------
    EndDo LoopGaussPointsAlfa
!--------------------------

  else

    Do i=1,NodEL
	  ipRow0 = ipRowVIni + (i-1)*iDofT
	  Do nci=1, NDim
	    ipRowV = ipRow0 + nci
      !  AE(ipRowAlfaT , ipRowV) = 0.0d0
	  EndDo
    EndDo
	  !  BE(ipRowAlfaT) = 0.0d0

  EndIf



      Do NodR  =1, NodEL
        ipRow  =   NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty

!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP

!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo, va positivo porque esta del lado de las incognitas, no del segundo miembro 
         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta
         BE(ipRowV)        = BE(ipRowV) - Pm(NodR)*Vn(ncv)*Theta1*Sol0(ipColPT)
!               AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta

       Enddo
      Enddo


      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Presure_NV_Linear                        &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,  &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
      Parameter (Theta = 1.0D0 )
      Parameter (Theta1= 1.0d0-Theta )
      Parameter ( NGP = 4 )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (	NGP = 3 )
!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex
      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         & 
     &  QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodELT-1)),  &
     &  Param(*), JParam(*),CommonPar(*),               &
     &  Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &  
     &  Vn(3),XA(3),XB(3),								&
	 &  Psi(NDim-1,NGP),W(NGP),Fi1(NodElT-1)

      NodEL = NodELT-1 ; NdimE = Ndim -1 ; NdimG = Ndim ; NdimE1 = Ndim ; NdimG1 = Ndim + 1      
           
	Pressure  = CommonPar(1)
	NodShift  = nint( CommonPar(2) )
	CN        = CommonPar(3)
!Direction, terminal Sign. CN Normal sign
!	Direction =dsign(1.0d0, Direction) 
	CN        =dsign(1.0d0, CN) 
    Nf_Pter     = nint( CommonPar(4) )
    Incremental = nint( CommonPar(5) ) 
	iSimetry     = nint( CommonPar(6) )
	iSimetryAxis = nint( CommonPar(7) )

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do


     If (Nf_Pter .GT. 0 ) Then
     NPoints = JParam(1)
!           ipFunc  = 5               
!        goto 897
!           NPoints=JParam(NodEl)
!           NPoints = Nf_Pter
!           Pressure_0 = PTer_F  ( Time        , NPoints, Param )
           Pressure_1 = PTer_F  ( Time  + Dtm , NPoints, Param )
!           Pressure  = 0.5D0 * ( Pressure_1 + Pressure_0 )
           Pressure  = Pressure_1
     Endif


if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
      Acoef = 6.0d0
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0
Endif       
 !Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN

 if (iSimetry .ne. 1 .and. NdimE == 1) then

      QC = DA / Acoef

 else 

      Det = Dabs(DA)
      iSimplex = 0     
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    
!     iBU: use bubble function
      iBU=0

      QC=0.0d0
      Pm=0.0d0
!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)

       DS=Det*W(nG)

       if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NDimE1 
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
                Enddo
               DS=DS*2.0d0*PI*Radio
       Endif

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  QC(i)= QC(i) + Fi1(i)*DS
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------

  Endif

      Pm=QC

!      Pm = DA / 6.0d0
!------------------------------------
!      ipRowT    =  0 
!      ipRowQT   =  ipRowT  + 1
!      ipColPT   =  ipRowQT + NDimG

      do NodR=1, NodEL
       ipRow   = ( NodR - 1 + NodShift ) * iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
!         VnP = Vn(ncv)*Penalty
         BE(ipRowV)= - Pm(NodR)*Vn(ncv)*Pressure

!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
!         AE ( ipRowQT , ipRowV ) = Direction*QC(NodR)*VnP
!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_2D1D_NV_Linear                       &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,   &
     &             Sol0,Sol1,CommonPar,                    &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
      Parameter (Theta = 1.0D0 )
      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NdimG = 2 )
      Parameter (NdimE1 = 2, NdimG1 = 3)      
      Parameter (NGP    = 3 )
!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
      Parameter ( iSimplex = 0 ,iBU=0 )     
!     iBU: use bubble function

!      Real*8 Jac(NDimE,NDimE)
!      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),    &
     &  XLL(*),Sol0(*),Sol1(*),                         &
     &  QC(NodEL),Pm(NodEL),XL(NDimG*NodEl),            &
     &  Param(*), JParam(*),CommonPar(*),               & 
     &  Vel0(NdimG),Vel1(NdimG),Pres(NodEL),            &
     &  Vn(3),XA(3),XB(3),Fi1(NodEl),Psi(NdimE,NGP),W(NGP)


!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
           
	Penalty   = CommonPar(1)
!	Direction = CommonPar(2)
	CN        = CommonPar(2)
!Direction, terminal Sign. CN Normal sign
	Direction =dsign(1.0d0, Direction) 
	CN        =dsign(1.0d0, CN) 
    Incremental  = nint( CommonPar(3) ) 
	iSimetry     = nint( CommonPar(4) )
	iSimetryAxis = nint( CommonPar(5) )

!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
           if(Incremental > 0 ) then   
              XL(iPNoda + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd)
             else
              XL(iPNoda + nsd) =XLL(iPNod + nsd)  + Sol1(iPNDof + nsd)
            endif
       end do
      end do

! Sides Vectors
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0  
      XB(3) = 1.0d0  
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN
!Normal Vector Vn

!------------------------------------
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    

      QC=0.0d0
      Pm=0.0d0
      Det = Dabs(DA)
!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
!       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)
       DS=Det*W(nG)
       if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl 
                 Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
                Enddo
               DS=DS*2.0d0*PI*Radio
       Endif

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  QC(i)= QC(i) + Fi1(i)*DS
!                  Pm(i) = Pm(i)+ Fi2(i)*DS
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------


      !QC = 0.5d0*Dabs(DA)
      Pm = QC

!------------------------------------
      ipRowT    =  0 
      ipRowQT   =  ipRowT  + 1
      ipColPT   =  ipRowQT + NDimG

      do NodR=1, NodEL
       ipRow   = NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty
!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
         AE ( ipRowQT , ipRowV ) = QC(NodR)*VnP
!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta
         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine Coup_3D1D_NV_Quad                       &
                 ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT, &
                   Sol0,Sol1,CommonPar,                  & 
                   Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
      Parameter (Theta = 1.0D0 )
      Parameter (Theta1= 1.0d0-Theta )

!     NDimE: dimensionality of the simplex element,
!     NodEl: Nodos del Triangulo
!     OJO!!!!!!! NodELT=4, N1=nodo conexion 1D
!      Parameter (NodEL = 6 )
!      Parameter (NdimE = 2 )
!      Parameter (NdimG = 3 )
!      Parameter (NdimE1 = 3, NdimG1 = 4)      
      Parameter (	NGP = 3 )
      Logical OnlyVertex
!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
      Parameter ( iSimplex = 0 ,iBU=0 )     
!     iBU: use bubble function
      
      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),   &
       XLL(*),Sol0(*),Sol1(*),                         & 
       QC(NodELT-1),Pm(NodELT-1),XL(NDim*(NodElT-1)),  &
       Param(*), JParam(*),CommonPar(*),               &
       Vel0(Ndim),Vel1(Ndim),Pres(NodELT-1),           &
       Vn(3),XA(3),XB(3),                              & 
       Psi(NDim-1,NGP),W(NGP),                         &
       dFi1_L(NDim,NodElT-1),dFi2_L(NDim,NodElT-1),dFi2_G(NDim,NodElT-1), &
       Fi1(NodElT-1),Fi2(NodElT-1),dFi2i(NDim),Vt(3)
      Real*8 Jac(NDim,NDim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
           
	Penalty   = CommonPar(1)
	Direction = CommonPar(2)
	CN        = CommonPar(3)
!Direction, terminal Sign. CN Normal sign
	Direction =dsign(1.0d0, Direction) 
	CN        =dsign(1.0d0, CN) 
      Last = 4
	iSimetry= nint(CommonPar(4))
	Last= Last+1
	iSimetryAxis= nint(CommonPar(Last))
	Last= Last+1
!	DPnor= CommonPar(Last)
!      coeficiente de penalizacion, si vale 1.0 la ecuacion se impone noaditivamente,
!      si es grande se impone aditivamente.  
!       ojo que el primer nodo es el terminal, las coordenadas
!       en XLL estan desplazadas un nodo
      NodEL = NodELT - 1
      NdimE = Ndim - 1 ; NdimE1 = NdimE + 1        
      NdimG = Ndim ; NdimG1 = NdimG + 1
!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
      NodP    = NdimE1
      NodG    = NodP

      Do Nod=1, NodEl
       iPNod = Nod*NdimG 
       iPNoda= iPNod-NdimG 
       iPNDof= Nod*iDofT + NdimG1
       Do nsd=1, NDimG
         XL(iPNoda + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
       end do
      end do

! Side Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)
Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0
        
      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
Endif        
 !Normal Vector Vn      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn = Vn/DA*CN

!------------------------------------
!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NDimE,NGP,iSimplex)    

      QC=0.0d0
      Pm=0.0d0
      Det = Dabs(DA)
!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
       Call ShapeF (NodEL,NDimE,Fi1,PSI(1,nG),1,iBu)
       Call ShapeF (NodEL,NDimE,Fi2,PSI(1,nG),2,iBu)

       DS=Det*W(nG)

       if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodG 
                 Radio =Radio + XL( (Nodrad-1)*NDim + nci)*Fi1(Nodrad)
                Enddo
               DS=DS*2.0d0*PI*Radio
       Endif

!%%%%%%%%%%%%%%%%%%%%%%%
       LoopRow: Do i=1,NodEL
                  QC(i)= QC(i) + Fi2(i)*DS
!                  Pm(i) = Pm(i)+ Fi2(i)*DS
       End do LoopRow
!%%%%%%%%%%%%%%%%%%%%%%%
!--------------------------
      EndDo LoopGaussPoints
!--------------------------
      Pm=QC
!------------------------------------
      ipRowT    =  0 
      ipRowQT   =  ipRowT  + 1
      ipColPT   =  ipRowQT + NDimG

      do NodR=1, NodEL
       ipRow   = NodR*iDofT 
       Do ncv=1, NDim
         ipRowV = ipRow + ncv
         VnP = Vn(ncv)*Penalty
!        aqui se suma Int( (V,n)*dA ) en la ecuacion de caudal del nodo terminal 
         AE ( ipRowQT , ipRowV ) = Direction*QC(NodR)*VnP
!        aqui se suma Int( -p(n,V)*dA ) en la ecuacion de momento de los nodos del Triangulo 
!         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)*Theta
         AE(ipRowV,ipColPT)= Pm(NodR)*Vn(ncv)

       Enddo
      Enddo

      Return
      End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

