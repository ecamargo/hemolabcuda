C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine DirichElem
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

c	Parameter (PI = 3.141592653589793238D0)
c      Parameter (Theta = 0.50D0 )
      Parameter (NodEL = 1 )

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),Param(*), JParam(*),CommonPar(*),
     &  XL (NodEl*NDim),Vn(NDim),Vt(NDim)
     
	Penalty = CommonPar(1)
      ISHIFT  = nint(CommonPar(2))

C------------------------------------
       Do nci  = 1, iDofT
          nci1 = nci + ISHIFT
          if ( nint(CommonPar(nci1)) .NE. 0 ) then
                   ipRow  = nci
                   ipCol  = ipRow
                   AE( ipRow , ipCol ) = Penalty
                   BE( ipRow )         = Penalty*CommonPar(iDofT+nci1)
          end if
       end do

      Return
      End

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine ConcentratedLoad
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

c	Parameter (PI = 3.141592653589793238D0)
c      Parameter (Theta = 0.50D0 )
      Parameter (NodEL = 1 )

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),Param(*), JParam(*),CommonPar(*)
!     &  ,XL (NodEl*NDim),Vn(NDim),Vt(NDim)
     
!	Penalty = CommonPar(1)
C------------------------------------
      Do nci  = 1, iDofT
                   BE( nci ) = Param(nci)
      end do

      Return
      End

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine DirichElemOblic1
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)
!Symmetric least squares form
!      Ui Penalty= DX .Vn(i) . Penalty
c	Parameter (PI = 3.141592653589793238D0)
c      Parameter (Theta = 0.50D0 )
!	Penalty = CommonPar(1) Par�metro de penalizacion
!	iX      = CommonPar(2) posicion del grado de libertad que quiero tratar oblicuamente
!	DX      = CommonPar(3) Valor del desplazamiento impuesto
!	Vn(i)= CommonPar(3+i) Vector normal
      Parameter (NodEL = 1 )

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),Param(*), JParam(*),CommonPar(*),
     &  XL (3),Vn(3),Vt(3)
     
	Penalty = CommonPar(1)
	iX      = NInT(CommonPar(2))
	iX      = iX-1
	DX      = CommonPar(3)
C------------------------------------
       Do nci  = 1, Ndim
	   Vn(nci)= CommonPar(3+nci)
       End do

       Do nci  = 1, Ndim
        ipRow  = iX + nci
        Vni    = Penalty*Vn(nci)
        BE( ipRow )  = DX*Vni

             Do ncj  = 1, Ndim
               ipCol = iX + ncj
               AE( ipRow , ipCol ) = Vni*Vn(ncj)
             end do
       end do

      Return
      End

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine DirichElemOblic2
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

c	Parameter (PI = 3.141592653589793238D0)
c      Parameter (Theta = 0.50D0 )
      Parameter (NodEL = 1 )

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),Param(*), JParam(*),CommonPar(*),
     &  XL (3),Vn(3),Vt(3)
     
	Penalty = CommonPar(1)
	iShift  = nint(CommonPar(2))-1
	DX      = CommonPar(3)
       Do nci  = 1, Ndim
	   Vn(nci)= CommonPar(3+nci)
       End do
	iRow  = nint(CommonPar(7))
C------------------------------------

      BE( iRow )  = DX*Penalty
       Do nci  = 1, Ndim
        iCol = iShift + nci
        AE( iRow , iCol ) = Vn(nci)*Penalty
       end do

      Return
      End


C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine VarImpFT
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,
     &             Sol0,Sol1,CommonPar,
     &             Param,JParam,DelT,DTm,Time)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
c      Parameter (Theta = 0.50D0 )
      Parameter (NodEL = 1 )

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),
     &  XLL(*),Sol0(*),Sol1(*),Param(*), JParam(*),CommonPar(*),
     &  XL (NodEl*NDim),Vn(NDim),Vt(NDim)
     
	Penalty = CommonPar(1)
      ISHIFT  = nint(CommonPar(2))
      IDOFVAR = nint(CommonPar(3))
	TAU     = CommonPar(4)
	TAUS    = CommonPar(5)
	AMP     = CommonPar(6)
	AMP=Amp*(1.-(XL(2)/.37)**2)

	T1 = DMOD(Time+Dtm,TAU)
	if(T1.gt.TAUS) then
      Var = 0.0d0
	else
	Var=Amp*dsin(2*PI/TAUS*T1)
	endif


C------------------------------------
       Do nci  = 1, iDofT
          nci1 = nci + iShift
          if ( nint(CommonPar(nci1)) .NE. 0 ) then
                   ipRow  = nci
                   ipCol  = ipRow
                   AE( ipRow , ipCol ) = Penalty
                   BE( ipRow )         = Penalty*CommonPar(iDofT+nci1)
          end if
       end do

      ipRow  = IDOFVAR
      ipCol  = ipRow
      AE( ipRow , ipCol ) = Penalty
      BE( ipRow )         = Penalty*VAR

      Return
      End





