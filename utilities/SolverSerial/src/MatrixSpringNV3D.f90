!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DNV_Linear                                  &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!!!!!!!!!!!!!!!! Ojo hay que revisarlo !!!!!!!!!!!!!!!!!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
 Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0) !, NdimE1 = 3, NdimG1 = 4)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),               &
           XLL(*),Sol0(*),Sol1(*),                            &
           Param(*), JParam(*),CommonPar(*),                  &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),      &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                     &
           Pres(NodELT),XL(Ndim*NodELT),Pm(NodELT,NodELT)
 Real*8   Kv
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

NodEL = NodElT   ; NdimE = Ndim-1 ; NdimG = Ndim
NdimE1 = NdimE+1 ; NdimG1 = NdimG +1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
!      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      if(nint(CommonPar(11)) == 0 ) then  
              ElasH         = CommonPar(7)
!              h            = CommonPar(8)
              RC           = CommonPar(8)
              PC           = CommonPar(9)
              Kv           = CommonPar(10)
      else
              ElasH        = Param(1)  ! ElasH=E*h/Rc
!              h            = CommonPar(8)
              RC           = Param(2)
              PC           = Param(3)
              Kv           = Param(4)  ! Kv = K*h/Rc
      endif
      Incremental = nint( CommonPar(12) ) 
        
!       Enduro = (10001.0d0 - 100.0d0/.1*Time) 
       if(Enduro.lt.1.0d0) Enduro = 1.0d0

!      RC           = Param(.1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
       rElas        = ElasH/RC*Enduro
       rKv          = Kv/RC

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
   if(Incremental > 0 ) then   
      XL(iPNod + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd) !Comentado para la version incremental
     else
      XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
    endif
  end do
 end do

! Sides Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0

Endif       
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                Enddo
                 DA3=DA3*2.0d0*PI*Radio/Dfloat(NodEL)
   Endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  P-Pc = (E h /Rc) (R-Rc)/Rc = ( E h / Rc^2 )  (R - RC)= RElas (R-RC)
!  PC: External Pressure ; RC: undeformed radius ; E: Young Modulus; h: wall thickness
!   Uni = (R-Rc).ni  (n:normal vector; Un: Total Displacement Vector)
!   Uni = (P-Pc)/RElas . ni
!  Incremental Version
!    k+1       k+1    k             k+1 k
!   Vni    =( Uni  - Uni ) /DT = ( P - P ) / RElas . ni / DT

!      k+1    k         k+1 k
!   ( Uni  - Uni ) = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Dni           = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Vni           = ( P - P ) / RElas . ni / DT ==> Vn = dP/dt . RElas . ni
!     k+1               k+1                     k
!    Dni           -   P  /RElas  . ni     = - P  /RElas . ni 
!     k+1               k+1                     k
!    Vni           -   P /RElas  . ni /DT  = - P /RElas . ni / DT
!     k+1               k+1               k
!    Dni  . RElas  -   P    . ni     = - P  . ni 
!     k+1               k+1               k
!    Vni  . RElas  -   P    . ni /DT = - P  . ni / DT


 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 
!        AE( ipRowV , ipRowV ) =   Penalty
!        AE( ipRowV , ipRowX ) = - Penalty / Dtm
!        BE( ipRowV )          = - Penalty*Sol0(ipRowX)  / Dtm

!        AE(ipRowX,ipRowX)  =   rElas * DA3
!        AE(ipRowX,ipRowP)  = - VecNi * DA3
!        BE( ipRowX )       = - VecNi * DA3 * PC  
        AE( ipRowV , ipRowV ) =   ( rElas + rKv/Dtm ) * DA3 
        AE( ipRowV , ipRowP ) = - VecNi * DA3 / Dtm
        if(Incremental /= 0 ) then
          BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRowV)
        else
         ! BE( ipRowV )          = - VecNi * DA3*PC /Dtm + rElas * DA3 *(-Sol0(ipRowX))/Dtm 
          BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRowV)
        endif

        if(Incremental /= 0 ) then   
        !!!!!!! OJO al POMO
            AE(ipRowX,ipRowX)     =  DA3
            AE(ipRowX,ipRowV)     = -Dtm * DA3
            BE( ipRowX )          = 0.0d0
        else
            AE(ipRowX,ipRowX)     =   rElas * DA3
            AE(ipRowX,ipRowV)     =   rKv   * DA3
            AE(ipRowX,ipRowP)     = - VecNi * DA3
            BE( ipRowX )          = - VecNi * DA3 * PC  !Total Displacement Version
        endif




       Enddo
 Enddo
!--------------------------

 Return
 End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DNV_LinearDes                               &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!!!!!!!!!!!!!!!! Ojo hay que revisarlo !!!!!!!!!!!!!!!!!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
 Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0) !, NdimE1 = 3, NdimG1 = 4)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),               &
           XLL(*),Sol0(*),Sol1(*),                            &
           Param(*), JParam(*),CommonPar(*),                  &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),      &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                     &
           Pres(NodELT),XL(Ndim*NodELT),Pm(NodELT,NodELT)
 Real*8   Kv
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

NodEL = NodElT   ; NdimE = Ndim-1 ; NdimG = Ndim
NdimE1 = NdimE+1 ; NdimG1 = NdimG +1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
!      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      if(nint(CommonPar(11)) == 0 ) then  
              ElasH         = CommonPar(7)
!              h            = CommonPar(8)
              RC           = CommonPar(8)
              PC           = CommonPar(9)
              Kv           = CommonPar(10)
      else
              ElasH        = Param(1)  ! ElasH=E*h/Rc
!              h            = CommonPar(8)
              RC           = Param(2)
              PC           = Param(3)
              Kv           = Param(4)  ! Kv = K*h/Rc
      endif
      Incremental = nint( CommonPar(12) ) 
        
!       Enduro = (10001.0d0 - 100.0d0/.1*Time) 
       if(Enduro.lt.1.0d0) Enduro = 1.0d0

!      RC           = Param(.1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
       rElas        = ElasH/RC*Enduro
       rKv          = Kv/RC

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
   if(Incremental > 0 ) then   
      XL(iPNod + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd) !Comentado para la version incremental
     else
      XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
    endif
  end do
 end do

! Sides Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0

Endif       
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                Enddo
                 DA3=DA3*2.0d0*PI*Radio/Dfloat(NodEL)
   Endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  P-Pc = (E h /Rc) (R-Rc)/Rc = ( E h / Rc^2 )  (R - RC)= RElas (R-RC)
!  PC: External Pressure ; RC: undeformed radius ; E: Young Modulus; h: wall thickness
!   Uni = (R-Rc).ni  (n:normal vector; Un: Total Displacement Vector)
!   Uni = (P-Pc)/RElas . ni
!  Incremental Version
!    k+1       k+1    k             k+1 k
!   Vni    =( Uni  - Uni ) /DT = ( P - P ) / RElas . ni / DT

!      k+1    k         k+1 k
!   ( Uni  - Uni ) = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Dni           = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Vni           = ( P - P ) / RElas . ni / DT ==> Vn = dP/dt . RElas . ni
!     k+1               k+1                     k
!    Dni           -   P  /RElas  . ni     = - P  /RElas . ni 
!     k+1               k+1                     k
!    Vni           -   P /RElas  . ni /DT  = - P /RElas . ni / DT
!     k+1               k+1               k
!    Dni  . RElas  -   P    . ni     = - P  . ni 
!     k+1               k+1               k
!    Vni  . RElas  -   P    . ni /DT = - P  . ni / DT


 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
	   rAE = ( rElas + rKv/Dtm ) * DA3 

        AE( ipRow+1 , ipRow+1 ) = rAE
        AE( ipRow+2 , ipRow+2 ) = rAE

        AE( ipRow+1 , ipRowP ) = - Vn(1) * DA3 / Dtm
        AE( ipRow+2 , ipRowP ) = - Vn(2) * DA3 / Dtm

        if(Incremental /= 0 ) then
          BE( ipRow+1 )          = - Vn(1) * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRow+1)
          BE( ipRow+2 )          = - Vn(2) * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRow+2)
        else
          BE( ipRow+1 )          = - Vn(1) * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRow+1)
		  BE( ipRow+2 )          = - Vn(2) * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRow+2)
        endif

        if(Incremental /= 0 ) then   
        !!!!!!! OJO al POMO
            AE(ipRowP+1,ipRowP+1)     =  DA3
            AE(ipRowP+2,ipRowP+2)     =  DA3

            AE(ipRowP+1,ipRow+1)     = -Dtm * DA3
            AE(ipRowP+2,ipRow+2)     = -Dtm * DA3

            BE( ipRowP+1 )          = 0.0d0
            BE( ipRowP+2 )          = 0.0d0
        else
            AE(ipRowP+1,ipRowP+1)     =   rElas * DA3
            AE(ipRowP+2,ipRowP+2)     =   rElas * DA3

            AE(ipRowP+1,ipRow+1)     =   rKv   * DA3
            AE(ipRowP+2,ipRow+2)     =   rKv   * DA3

            AE(ipRowP+1,ipRowP)     = - Vn(1) * DA3
            AE(ipRowP+2,ipRowP)     = - Vn(2) * DA3

            BE( ipRowP+1 )          = - Vn(1) * DA3 * PC  !Total Displacement Version
            BE( ipRowP+2 )          = - Vn(2) * DA3 * PC  !Total Displacement Version
        endif

!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd) 

!       AE( ipRowV , ipRowV ) =   ( rElas + rKv/Dtm ) * DA3 
!       AE( ipRowV , ipRowP ) = - VecNi * DA3 / Dtm

!       if(Incremental /= 0 ) then
!         BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRowV)
!       else
!         BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRowV)
!       endif

!        if(Incremental /= 0 ) then   
        !!!!!!! OJO al POMO
!            AE(ipRowX,ipRowX)     =  DA3
!            AE(ipRowX,ipRowV)     = -Dtm * DA3
!            BE( ipRowX )          = 0.0d0
!        else
!            AE(ipRowX,ipRowX)     =   rElas * DA3
!            AE(ipRowX,ipRowV)     =   rKv   * DA3
!            AE(ipRowX,ipRowP)     = - VecNi * DA3
!            BE( ipRowX )          = - VecNi * DA3 * PC  !Total Displacement Version
!        endif

!       Enddo
 Enddo
!--------------------------

 Return
 End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DNV_LinearSplit1                            &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
 Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0) !, NdimE1 = 3, NdimG1 = 4)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),               &
           XLL(*),Sol0(*),Sol1(*),                            &
           Param(*), JParam(*),CommonPar(*),                  &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),      &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                     &
           Pres(NodELT),XL(Ndim*NodELT),Pm(NodELT,NodELT)
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

NodEL = NodElT   ; NdimE = Ndim-1 ; NdimG = Ndim
NdimE1 = NdimE+1 ; NdimG1 = NdimG +1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      if(nint(CommonPar(10)) == 0 ) then  
              ElasH         = CommonPar(7)
!              h            = CommonPar(8)
              RC           = CommonPar(8)
              PC           = CommonPar(9)
      else
              ElasH        = Param(1)  ! ElasH=E*h/RC
!              h            = CommonPar(8)
              RC           = Param(2)
              PC           = Param(3)
      endif
      Incremental = nint( CommonPar(11) ) 
        
       Enduro = (101.0d0 - 100.0d0/.05*Time) 
       if(Enduro.lt.1.0d0)Enduro = 1.0d0

!      RC           = Param(1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
!       rElas        = ElasH/(RC**2)*Enduro
        rElas        = ElasH/RC*Enduro

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
   if(Incremental > 0 ) then   
      XL(iPNod + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd) !Comentado para la version incremental
     else
      XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd) !Comentado para la version incremental
    endif
  end do
 end do

! Sides Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0

Endif       
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                Enddo
                 DA3=DA3*2.0d0*PI*Radio/Dfloat(NodEL)
   Endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  P-Pc = (E h /Rc) (R-Rc)/Rc = ( E h / Rc^2 )  (R - RC)= RElas (R-RC)
!  PC: External Pressure ; RC: undeformed radius ; E: Young Modulus; h: wall thickness
!   Uni = (R-Rc).ni  (n:normal vector; Un: Total Displacement Vector)
!   Uni = (P-Pc)/RElas . ni
!  Incremental Version
!    k+1       k+1    k             k+1 k
!   Vni    =( Uni  - Uni ) /DT = ( P - P ) / RElas . ni / DT

!      k+1    k         k+1 k
!   ( Uni  - Uni ) = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Dni           = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Vni           = ( P - P ) / RElas . ni / DT ==> Vn = dP/dt . RElas . ni
!     k+1               k+1                     k
!    Dni           -   P  /RElas  . ni     = - P  /RElas . ni 
!     k+1               k+1                     k
!    Vni           -   P /RElas  . ni /DT  = - P /RElas . ni / DT
!     k+1               k+1               k
!    Dni  . RElas  -   P    . ni     = - P  . ni 
!     k+1               k+1               k
!    Vni  . RElas  -   P    . ni /DT = - P  . ni / DT


 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 
!        AE( ipRowV , ipRowV ) =   Penalty
!        AE( ipRowV , ipRowX ) = - Penalty / Dtm
!        BE( ipRowV )          = - Penalty*Sol0(ipRowX)  / Dtm

!        AE(ipRowX,ipRowX)  =   rElas * DA3
!        AE(ipRowX,ipRowP)  = - VecNi * DA3
!        BE( ipRowX )       = - VecNi * DA3 * PC  
        AE( ipRowV , ipRowV ) =   rElas * DA3 
        AE( ipRowV , ipRowP ) = - VecNi * DA3 / Dtm 

        if(Incremental /= 0 ) then
          BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm 
        else
          BE( ipRowV )          = - VecNi * DA3*PC /Dtm + rElas * DA3*(-Sol0(ipRowX))/Dtm 
        endif

       Enddo
 Enddo
!--------------------------

 Return
 End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DNV_LinearSplit2                            &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
 Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0) !, NdimE1 = 3, NdimG1 = 4)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),               &
           XLL(*),Sol0(*),Sol1(*),                            &
           Param(*), JParam(*),CommonPar(*),                  &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),      &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                     &
           Pres(NodELT),XL(Ndim*NodELT),Pm(NodELT,NodELT)
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

NodEL = NodElT   ; NdimE = Ndim-1 ; NdimG = Ndim
NdimE1 = NdimE+1 ; NdimG1 = NdimG +1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      if(nint(CommonPar(10)) == 0 ) then  
              ElasH         = CommonPar(7)
!              h            = CommonPar(8)
              RC           = CommonPar(8)
              PC           = CommonPar(9)
      else
              ElasH        = Param(1)  ! ElasH=E*h/RC
!              h            = CommonPar(8)
              RC           = Param(2)
              PC           = Param(3)
              if(ElasH <=0.0d0 .or. RC<=0.0d0) write(*,*)"null parameters in Spring"
      endif
      Incremental = nint( CommonPar(11) ) 
        
       Enduro = (101.0d0 - 100.0d0/.05*Time) 
       if(Enduro.lt.1.0d0) Enduro = 1.0d0
!      RC           = Param(1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
       rElas        = ElasH/RC*Enduro

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
   if(Incremental > 0 ) then   
      XL(iPNod + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd) !Comentado para la version incremental
     else
      XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
    endif
  end do
 end do

! Sides Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0

Endif       
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                Enddo
                 DA3=DA3*2.0d0*PI*Radio/Dfloat(NodEL)
   Endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  P-Pc = (E h /Rc) (R-Rc)/Rc = ( E h / Rc^2 )  (R - RC)= RElas (R-RC)
!  PC: External Pressure ; RC: undeformed radius ; E: Young Modulus; h: wall thickness
!   Uni = (R-Rc).ni  (n:normal vector; Un: Total Displacement Vector)
!   Uni = (P-Pc)/RElas . ni
!  Incremental Version
!    k+1       k+1    k             k+1 k
!   Vni    =( Uni  - Uni ) /DT = ( P - P ) / RElas . ni / DT

!      k+1    k         k+1 k
!   ( Uni  - Uni ) = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Dni           = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Vni           = ( P - P ) / RElas . ni / DT ==> Vn = dP/dt . RElas . ni
!     k+1               k+1                     k
!    Dni           -   P  /RElas  . ni     = - P  /RElas . ni 
!     k+1               k+1                     k
!    Vni           -   P /RElas  . ni /DT  = - P /RElas . ni / DT
!     k+1               k+1               k
!    Dni  . RElas  -   P    . ni     = - P  . ni 
!     k+1               k+1               k
!    Vni  . RElas  -   P    . ni /DT = - P  . ni / DT


 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 
        if(Incremental /= 0 ) then   
            AE(ipRowX,ipRowX)     = DA3
            BE( ipRowX )          = Sol1(ipRowV)*Dtm*DA3
        else
            AE(ipRowX,ipRowX)     =   rElas * DA3
!            AE(ipRowX,ipRowP)     = - VecNi * DA3
            BE( ipRowX )          = - VecNi * DA3 * (PC-Sol1(ipRowP))  !Total Displacement Version
        endif

       Enddo
 Enddo
!--------------------------

 Return
 End



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DNV_Quad                                    &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
! Parameter (NdimE1 = 3, NdimG1 = 4)
  Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),            &
           XLL(*),Sol0(*),Sol1(*),                         &
           Param(*), JParam(*),CommonPar(*),               &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),   &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                  &
           Pres(NodELT),XL(Ndim*NodElT)                    &
           ,Pm(NodELT,NodELT),NodeEdge(3)
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1
      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      Elas         = CommonPar(7)
      h            = CommonPar(8)
      RC           = CommonPar(9)
      PC           = CommonPar(10)

! ATENCION CON dPNOR
!      RC           = Param(1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
      rElas        = Elas*h/(RC**2)*Enduro

      NodEL  = NodELT ; NdimE = Ndim -1  ; NdimG = Ndim 
      Ndim1 = Ndim+1   ; NdimE1 = Ndim  ; NdimG1 = NdimG + 1
      NodG   = NdimE1 ; NodP  = NodG ; NodP1  = NodG-1   

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
   XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
  end do
 end do

! Sides Vectors

if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

      NodeEdge(1)=4
      NodeEdge(2)=6
      NodeEdge(3)=5

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0

      Acoef = 2.0d0

      NodeEdge(1)=3
Endif        
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty


!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                !Do Nodrad=1, NodG
                 Nodrad=3  
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                !Enddo
                 DA3=DA3*2.0d0*PI*Radio
   Endif


!%%%%%&&&&&&&&&&&&&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
goto 10
!alternativa U,P
 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd

!        AE( ipRowV , ipRowV ) =   DA3 
!        AE( ipRowV , ipRowX ) = - DA3/Dtm
!        BE( ipRowV )          = - DA3 * Sol0(ipRowX) /Dtm

        AE( ipRowV , ipRowV ) =   Penalty 
        AE( ipRowV , ipRowX ) = - Penalty/Dtm
        BE( ipRowV )          = - Penalty*Sol0(ipRowX) /Dtm

       Enddo
 Enddo
!--------------------------
 do NodR = 1, NodG
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 

        AE(ipRowX,ipRowX)  =   rElas * DA3
        AE(ipRowX,ipRowP)  = - VecNi * DA3
        BE( ipRowX )       = - VecNi * DA3 * PC  
       Enddo
 Enddo
!--------------------------
Return


10 continue

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! alternativa V,P
do NodR = 1, NodG
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 

        AE( ipRowV , ipRowV ) =   rElas * DA3 
        AE( ipRowV , ipRowP ) = - VecNi * DA3 / Dtm
        BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm

      Enddo
Enddo
!--------------------------
do NodR = 1, NodG
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 

        AE(ipRowX,ipRowX)  =   rElas * DA3
        AE(ipRowX,ipRowP)  = - VecNi * DA3
        BE( ipRowX )       = - VecNi * DA3 * PC  
       Enddo
Enddo

! do NodR = NodG+1, NodEL
!       ipRow    = (NodR-1) * iDofT 
!       ipRowP   =  ipRow   + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd

!        AE( ipRowV , ipRowV ) =   Penalty 
!        AE( ipRowV , ipRowX ) = - Penalty/Dtm
!        BE( ipRowV )          = - Penalty*Sol0(ipRowX) /Dtm
!       Enddo
! Enddo

! Midside nodes equations
    nEdge=0
    Do nod  = 1, NodP1
      ipNod = (nod-1) * iDofT 
      Do nodr   =  nod  + 1,   NodP
         ipNodr = (nodr-1) * iDofT 
         nEdge  = nEdge +  1
         ipMidsideNode = (NodeEdge(nEdge)-1) * iDofT
          Do nci = 1, Ndim				
                !V
             AE( ipMidsideNode + nci , ipMidsideNode + nci ) = 1.0d0     
             AE( ipMidsideNode + nci , ipNod + nci ) = -0.5d0     
             AE( ipMidsideNode + nci , ipNodr+ nci ) = -0.5d0     
                !X
             AE( ipMidsideNode + nci + Ndim1 , ipMidsideNode + nci + Ndim1 ) = 1.0d0     
             AE( ipMidsideNode + nci + Ndim1 , ipNod + nci + Ndim1 ) = -0.5d0     
             AE( ipMidsideNode + nci + Ndim1 , ipNodr+ nci + Ndim1 ) = -0.5d0     
          enddo
      enddo
    enddo
!--------------------------
Return
End



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring2DNV_Linear                           &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT, &
                  Sol0,Sol1,CommonPar,                  &
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
 Parameter ( NodEL = 2, NdimE = 1 , NdimG = 2 )
! Parameter ( NGP  = 2 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0, NdimE1 = 2, NdimG1 = 3)      

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),           &
           XLL(*),Sol0(*),Sol1(*),                        &
           Param(*), JParam(*),CommonPar(*),              &
           XL(NdimG*NodEl),Vel0(NdimG),Vel1(NdimG),       &
           Vn(3),XA(3),XB(3) !,                   
            !Fi1(NodEl),Pm(NodEL,NodEL),Psi(NdimE,NGP),W(NGP),
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      Elas         = CommonPar(7)
      h            = CommonPar(8)
      RC           = CommonPar(9)
      PC           = CommonPar(10)

! ATENCION CON dPNOR
!      RC           = Param(1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
      rElas        = Elas*h/(RC**2)*Enduro


 Do Nod=1, NodEl
  iPNod = (Nod-1)*NDimG 
  iPNDof= (Nod-1)*iDofT + NdimG1
  Do nsd=1, NDimG
   XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
  end do
 end do

! Sides Vectors
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0  
      XB(3) = 1.0d0  
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn / DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA2 = Dabs(DA) / 2.0d0*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 
        AE( ipRowV , ipRowV ) =   Penalty 
        AE( ipRowV , ipRowX ) = - Penalty / Dtm
        BE( ipRowV )          = - Penalty*Sol0(ipRowX) / Dtm 

        AE(ipRowX,ipRowX)  =   rElas * DA2
        AE(ipRowX,ipRowP)  = - VecNi * DA2
        BE( ipRowX )       = - VecNi * DA2 * PC  
       Enddo
 Enddo
!--------------------------

 Return
 End

