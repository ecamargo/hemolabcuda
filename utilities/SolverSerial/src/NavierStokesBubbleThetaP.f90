!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Elemento de Navier Stokes ALE con movimiento de red (primer substep)
! P1Bubble/P1
! Dofs ordering:  (U , V , W)    P     (DX , DY , DZ)
!                  Velocity , Pressure , Mesh displacement
! It is splitted into two substeps
! In the first substep the bubbles velocities are eliminated
! In the second substep Bubbles are calculated and stored in the commonpar 1:3
! requires Param Write Swicht =1
! Incremental and nonincremental version depending on commonpar(7)  
! Commonpar(7) : == 0 Nonincremental displacement version
!                 > 0 Incremental version with fixed coordinates at the previous time step, coordinates are poscalculated with ixupdated swicht
!                 < 0 Incremental version nonlinear on incremental displacement, the mesh is updated iteratively at the same time with the solution
      SubRoutine NavierStokesBubALEDummyTP1                   &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,       & 
                  Sol0,Sol1,CommonPar,                        &  
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

    COMMON Det,LengthParam,LengthJParam

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
      Parameter (Ls1 = 1 )
      Parameter (Ls2 = 0 )
!     NdimE: dimensionality of the triangle element,
!     it could be hapen that we use a 2d element in a 3d problem. 
!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
! los dos tendrian que ir en ElemLib, porque se necesita
! la dimension del elemento, No esta implicito en el tipo de elemento
!  usado, aunque esto hace que no se pueda usar el mismo codigo
! para 2D y 3D
! OnlyVertex indicates a geometric interpolation with only vertex nodes 
      Logical OnlyVertex
! iSimplex = 0 -> The element is a Simplex
! iSimplex = 1 -> The element is a Hipercube
! iBU: use bubble function
      Parameter ( iSimplex = 0, iBU = 1 , OnlyVertex = .True. , UseVred=1.0d0)     
      Real*8 Jac(Ndim,Ndim)
      DIMENSION AE(MaxLRows,MaxLRows),BE(MaxLRows),AEd((NodELT+1)*iDofT,(NodELT+1)*iDofT), &
       XLL(*),Sol0(*),Sol1(*),BEd(5*iDofT),                                                &
       Param(*), JParam(*),CommonPar(*),                                                   &
       Gravity(Ndim),Psi(Ndim,11),W(11),Vel0(Ndim),Vel1(Ndim),                             &
       XL(Ndim*(NodELT+1)),VelUp(Ndim),VelRed(Ndim),VelC(Ndim),                            &
       Fi1(NodElT+1),Fi2(NodElT+1),dFi2i(Ndim),dFi2j(Ndim),                                &
       dFi1i(Ndim),dFi1j(Ndim),dFi1_G(Ndim,NodElT+1),dFi1_L(Ndim,NodElT+1),                &
       dFi2_L(Ndim,NodElT+1),dFi2_G(Ndim,NodELT+1),UX1(Ndim*NodELT),                       &
       UX0(Ndim*NodELT),V0(Ndim*(NodELT+1)),V1(Ndim*(NodELT+1)) !,VR(Ndim*(NodELT+1))    !,NodeEdge(6)
       
!      DIMENSION AEb(MaxLRows,MaxLRows),BEb(MaxLRows)

!  Real*8 , Static :: Mass(4,4),MassL(4)
!  Static iFirst
    AEd=0.0d0 ; BEd=0.0d0
    NodEl  = NodElT + 1
    NDimE=Ndim

    OpTi=1.0d0 !*Dtm
! nci do variable,  number of coordinate index
      Ro       = CommonPar(1)
      Vis      = CommonPar(2)
      Epsilona = CommonPar(3)
!     Epsilon  = CommonPar(3)*Ro

      Theta         =       CommonPar(4)
	  iSimetry      = nint( CommonPar(5) )
	  iSimetryAxis  = nint( CommonPar(6) )
      iSimetryAxis1 = iSimetryAxis + 1
!     iSimetryAxis = 1 -> RS about X axis 
!     iSimetryAxis = 0 -> RS about Y axis 

      Incremental = nint( CommonPar(7) ) 
!	  Last = Last+1
      Proyec      =       CommonPar(8)
	  Last = 8
      Do nci=1, NdimE
             Gravity(nci)= CommonPar( Last+nci )
             V0(Ndim*NodElT+nci) = Param(nci)
      end do

	Theta1  = 1.0d0-Theta
!	ThetaP  = Theta
	ThetaP  = 1.0d0
	Theta1P = 1.0d0-ThetaP
    Rot     = Ro/Dtm

!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
     NdimE1  = Ndim + 1
     NodP    = NdimE1
!     IF (OnlyVertex) Then
     NodG    = NodP
!     Else
!          NodG = NodEL -iBu
!     EndIF

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     c�lculo de "h" Hred para determinar Psi1i 16/9/01
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
!   ojo esta formula solo vale para NDim = 2 o 3
    nAristas = NdimE1 + 2*(Ndim-2)
! Se proyectan los lados en la direccion de la velocidad
    VelUp=0.0d0
!    ipNodSb = (NodEl-1)*Ndim
!    VR ( ipNodSb )=0.0d0
    Do nod=1, NodG
       ipNodV = (nod-1)*iDofT
       ipNodS = (nod-1)*Ndim
       ipNodX =  ipNodV + NdimE1  
       Do nci = 1, Ndim				
          ipNodXi      = ipNodX + nci 
          ipNodSi      = ipNodS + nci 
          ipNodVi      = ipNodV + nci
          UX1( ipNodSi )= Sol1(ipNodXi)  !Displacement or incremental Displacement
          UX0( ipNodSi )= Sol0(ipNodXi)
          if(Incremental> 0 ) then   
           XL ( ipNodSi )= XLL( ipNodSi)  !Incremental : the coordinates are post-calculated 
            else
           XL ( ipNodSi )= XLL( ipNodSi) + Sol1(ipNodXi) ! Non incremental
          endif
          V0 ( ipNodSi )= Sol0(ipNodVi)
          V1 ( ipNodSi )= Sol1(ipNodVi)
!          VR ( ipNodSi )= UX1( ipNodSi )/DTm ! Incremental
!          VR ( ipNodSb )= VR ( ipNodSb ) + VR ( ipNodSi ) ! VR bubble
            if (Incremental /= 0 ) then
              VelUp( nci  ) = VelUp(nci)    + V1 ( ipNodSi ) - UX1( ipNodSi )/DTm *UseVred ! Incremental 
            else
              VelUp( nci  ) = VelUp(nci)    + V1 ( ipNodSi ) - ( UX1( ipNodSi )-UX0( ipNodSi ) )/Dtm *UseVred
            endif
       enddo
    enddo
    VelUp = VelUp / NodG
!    VR ( ipNodSb )= VR ( ipNodSb )/ NodG

    VRmax      = 0.d0
    VArisAcum  = 0.d0
    NodP1  = NodP - 1
    Do nod  = 1, NodP1
      ipNod = (nod-1) * Ndim

      Do nodr   =  nod  + 1,   NodP
         ipNodr = (nodr - 1) * Ndim

          VR     = 0.0d0 
          VAris  = 0.0d0 
          Do nci = 1, Ndim				
             VArisi = XL(ipNodr+nci) - XL(ipNod+nci)      
             VR     = VR + VArisi*VelUp(nci)
             VAris  = VAris + VArisi*VArisi   
          enddo
          VArisAcum = VArisAcum + Dsqrt(VAris)
          VRabs = dabs(VR) ; if (VRmax < VRabs) VRmax = VRabs
      enddo
    enddo

	VArisAcum = VArisAcum / nAristas
    velmod    = Dsqrt( Sum(VelUp*VelUp) )
    if ( velmod .ne. 0.0d0 ) then
      Hred = 0.5 * VRmax / velmod
    else
      Hred = 0.5 * VArisAcum
    endif
    Coura=velmod*Dtm/VArisAcum
 !   if (Coura.gt..85) Write(*,*)"Courant", Coura  


    Pec  = Ro * velmod * Hred /Vis /2.0d0
    If ( Pec .Gt. 1.0d0 ) Then
       TauU  = Hred /( 2.d0*velmod )*(1.0d0-1.0d0/Pec)
!       TauU  = 1.d0/( 2.d0*Ro*velmod/Hred + 4.d0*vis/Ro/(Hred**2) )

!       TauU  = 1.d0/( 2.d0*Ro*velmod/Hred + 4.d0*vis/Ro/(Hred**2) )
    else
          TauU=0.0d0
    endif
!       TauP      = - TauU 

!           Fin Calculo Hred
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  If (Ndim<3) Then
!    NGP = 3
!  Else
!    NGP = 4
!  EndIf
  If (Ndim < 3) Then
      NGP =7
    Else
      NGP =11
  EndIf

!   Psi: Gauss Points Matrix, W: Weights
    Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
	
!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
	Vel0  =0.0d0
	Vel1  =0.0d0
	VelC  =0.0d0
    VelRed=0.0d0

!     dFi1_L = first derivatives with respect to intrinsic Local coordinates
!              refers to linear shape functions
!     dFi1_G = first derivatives with respect to Global coordinates
!     dFi2_L = first derivatives with respect to intrinsic Local coordinates
!              refers to Cuadratic shape functions

!      Call LocalShapeDer(NodG+iBu,NdimE,dFi1_L,PSI(1,nG),1,iBu)
!      Call LocalShapeDer(NodEL,NdimE,dFi2_L,PSI(1,nG),2,iBu)

       Call LocalShapeDer(NodEL,NdimE,dFi1_L,PSI(1,nG),1,iBu)
       if (nG .eq. 1) Call Jacobian(Jac,Det,NdimE,NodG,XL,dFi1_L)
       dFi1_G=0.0d0  
       Call GlobalShapeDer(NodEL,NdimE,dFi1_L,dFi1_G,Jac)
       Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)

      Do NodV= 1, NodG
       ipNodS = (NodV-1)*Ndim   
        Do nci=1, Ndim
        ipNodSi = ipNodS + nci

        Vel1i = Theta*V1(ipNodSi) + Theta1*V0(ipNodSi)
        if (Incremental /= 0 ) then
          VelRi = UX1(ipNodSi)/Dtm
         else
          VelRi = (UX1(ipNodSi) - UX0(ipNodSi))/Dtm ! Non incremental
        endif


        Vel1  (nci) = Vel1(nci)   + Vel1i*Fi1(NodV)
        VelRed(nci) = VelRed(nci) + VelRi*Fi1(NodV) *UseVred
       Enddo
      Enddo

      VelC = Vel1 - VelRed 
      DV=Det*W(nG)*OpTi
	if (iSimetry .eq. 1) then
            Radio = 0.0d0
	        nci = iSimetryAxis1
                Do Nodrad=1, NodG
                 FiNodRad = Fi1(Nodrad)
                 Radio =Radio + XL( (Nodrad-1)*NdimE + nci)*FiNodRad
                Enddo
      DV=DV*Radio*2.0d0*PI
	Endif

      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRow1 = ipRow + 1
!        Fi2i = Fi2(i)
        Fi1i = Fi1(i)

        ConvecUpi=0.0d0
        Do nci=1, NdimE
           dFi1i(nci) = dFi1_G(nci,i)
           ConvecUpi= ConvecUpi + VelC(nci)*dFi1i(nci)
        Enddo
!               ConvecUpi = < V , grad FiI > 

        WUpi      = (Fi1i + TauU * ConvecUpi)* DV
!        WLSi      = (Fi2i + Theta*DTm * ConvecUpi)* DV
        Cvis1     = vis * DV 
        Cvis0     = Theta1 * Cvis1 
        Cvis1     = Theta  * Cvis1 

        Do nci=1, NdimE
           BEd( ipRow + nci ) =BEd( ipRow + nci ) + Ro*Gravity(nci)*WUpi 
        Enddo


        LoopCol: Do j=1,NodEL
         ipCol  = (j-1)*iDofT 
         ipCol1 = ipCol + 1
         Fi1j   = Fi1(j)

         ipCVj  = (j-1)*Ndim 
         ipCVj1 = (j-1)*Ndim + 1 

         GradFi_GradFj= 0.0d0
         Ro_V_GradFj  = 0.0d0   
         Do nci=1, NdimE
           GradFi_GradFj = GradFi_GradFj + dFi1i(nci) * dFi1_G(nci,j) 
	       Ro_V_GradFj   = Ro_V_GradFj   + Ro * VelC(nci) * dFi1_G(nci,j)
         Enddo                                            

	       CoefK0 = Cvis0 * GradFi_GradFj
	       CoefK1 = Cvis1 * GradFi_GradFj
! Para Upwinding en M masas y LS
!	       C0ij = WUpi  * (-Rot*Fi2j + Ro_V_GradFj * Theta1 )
!	       C1ij = WUpi  * ( Rot*Fi2j + Ro_V_GradFj * Theta  )
!	       CLsij= WLsi  * ( Rot*Fi2j + Ro_V_GradFj * Theta  )

! para el tradicional
	       C0ij = (-Rot*Fi1j*Fi1i* DV + WUpi  * Ro_V_GradFj * Theta1 )
	       C1ij = ( Rot*Fi1j*Fi1i* DV + WUpi  * Ro_V_GradFj * Theta  )
! Mass Lumped  La matriz de masas se pone afuera
!	       C0ij = ( WUpi  * Ro_V_GradFj * Theta1 )
!	       C1ij = ( WUpi  * Ro_V_GradFj * Theta  )



! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
! 1-LS
!             AE(ipRow1 , ipCol1)= AE(ipRow1 , ipCol1) + CLsij + CoefK1 
! 2-Picard comun
           AEd(ipRow1 , ipCol1)= AEd(ipRow1 , ipCol1) + C1ij + CoefK1 

         Do nci=1, NdimE
          BEd( ipRow + nci )= BEd( ipRow + nci ) - (C0ij+CoefK0)*V0(ipCVj+ nci) 
! 1-LS
!               BE( ipRow + nci )= BE( ipRow + nci ) - (C1ij-CLsij )*Sol1(ipCol+ nci) 
         Enddo                                            
!      Hasta aqui ya ensamble para iDof=1, ahora para 2 y/o 3
         Do iDof=1,NdimE-1
           AEd( ipRow1 + iDof , ipCol1 + iDof ) =  AEd( ipRow1 , ipCol1 )
         Enddo

           if (iSimetry .eq. 1) then
             ipRowV = ipRow1 + iSimetryAxis
             ipColV = ipCol1 + iSimetryAxis
             ipCVjr = ipCVj1 + iSimetryAxis
             CoefK0 = Cvis0*Fi1i/Radio * Fi1j/Radio
             CoefK1 = Cvis1*Fi1i/Radio * Fi1j/Radio
             AEd( ipRowV , ipColV )= AEd( ipRowV , ipColV ) + CoefK1
             BEd( ipRowV )= BEd( ipRowV ) - CoefK0*V0(ipCVjr) 
           endif

        End do LoopCol

!-----------------------------------------------
!     ensamble de P
       ipRowP = ipRow + NdimE1 
       CMPres = - Epsilona*Fi1(i)*DV !/DTm

!!!!!!!!!!!!!!!!!!!!!!!!! lumped pressure Mass Matrix
            If (i <= NodP) then
             AMPresij  =  - Epsilona*DV/Dfloat(NdimE1)
             AKPresij  = 0.0d0
             AEd( ipRowP, ipRowP ) = AEd( ipRowP, ipRowP ) + AMPresij !+ AKPresij
             BEd( ipRowP ) = BEd( ipRowP ) + AMPresij *Sol1(ipRowP) !+ AKPresij*Sol1(ipColP)

            Endif

!!!!!!!!!!!!!!!!!!!!!!!


        LoopColP: Do j=1,NodP
            ipColP = (j-1)*iDofT + NdimE1 
	        Fi1jDV=Fi1(j)*DV

            Do nci=1,NdimE
               ipRowV = ipRow + nci
	           AEd( ipRowV , ipColP )= AEd( ipRowV , ipColP ) -  dFi1i(nci) * Fi1jDV
!                                                           -  div(Fi2i)  *(Fi1j Pj)       
            Enddo                     

            if (iSimetry .eq. 1) then
                nci = iSimetryAxis1
                ipRowV = ipRow + nci
	            AEd( ipRowV , ipColP )= AEd( ipRowV , ipColP )- Fi1i/Radio * Fi1jDV
            endif

!          Pressure Mass Matrix
            If (i <= NodP) then
             AMPresij  = CMPres *Fi1(j) *0.0d0  !!!!OJO al Pomo
             AKPresij  = 0.0d0
              Do nci=1,NdimE
                  AKPresij  = AKPresij - dFi1i(nci)*dFi1_G(nci,j)*DV/Rot*DTm
              Enddo                     
             AEd( ipRowP, ipColP ) = AEd( ipRowP, ipColP ) + AMPresij !+ AKPresij
             BEd( ipRowP ) = BEd( ipRowP ) + AMPresij *Sol1(ipColP) !+ AKPresij*Sol1(ipColP)

!             BE( ipRowP ) = BE( ipRowP ) + AMPresij *Sol0(ipColP)

            !	Ensamblamos la parte del movimiento de la Red.
            !####################################################
            !                 calculo de la red                 ! 
            !####################################################
                     Do ndRow   = 1, Ndim
                            ipRowR  = ipRowP + ndRow
                            ipColR  = ipColP + ndRow
                            Do is = 1, NdimE
                             AEd(ipRowR, ipColR) = AEd(ipRowR, ipColR) + dFi1i(is)*dFi1_G(is,j)*DV
                            End Do
                     Enddo
            Endif
         Enddo LoopColP
!-----------------------------------------------
     End do LoopRow
!--------------------------
   EndDo LoopGaussPoints
!--------------------------

!   simetrizing presure divergence matrixs
      Do i=1,NodEL
       ipRow  = (i-1)*iDofT
       Do nci=1,NdimE
          ipRowV = ipRow + nci
          Do j=1,NodP
                      ipColP = (j-1)*iDofT + NdimE1 
                      AEd( ipColP ,ipRowV ) = AEd( ipRowV , ipColP ) !*DTm
                      BE( ipRowV ) = BE( ipRowV ) - AE( ipRowV , ipColP )*Theta1P * Sol0(ipColP)
                      AEd( ipRowV , ipColP ) = AEd( ipRowV , ipColP )*ThetaP !*DTm
!                      BE( ipColP ) = BE( ipColP ) + AE( ipRowV , ipColP ) * Sol1(ipRowV)
!                      BE( ipColP ) = BE( ipColP ) - AE( ipRowV , ipColP ) * Sol1(ipRowV)
          Enddo
       Enddo
      Enddo

     ipRowB = NodP*iDofT
        
     Do i=1,NodP
       ipRow  = (i-1)*iDofT
       ipRowP = ipRow + NdimE1 
       Do nci=1,NdimE
          ipRowV = ipRow  + nci
          ipRowVB= ipRowB + nci
          BB  = BEd(ipRowVB)
          rMbb= AEd(ipRowVB,ipRowVB)
          rMib= AEd(ipRowV,ipRowVB)
          BEd( ipRowV ) = BEd( ipRowV ) - rMib*BB/rMbb
          BEd( ipRowP ) = BEd( ipRowP ) - AEd( ipRowP,ipRowVB )*BB/rMbb
          Do j=1,NodP
                      ipCol  = (j-1)*iDofT  
                      ipColV = ipCol + nci  
                      ipColP = ipCol + NdimE1 
                      AEd( ipRowV,ipColV ) = AEd( ipRowV,ipColV ) - AEd(ipRowVB,ipColV )*rMib/rMbb
                      AEd( ipRowV,ipColP ) = AEd( ipRowV,ipColP ) - AEd(ipRowVB,ipColP )*rMib/rMbb
                      AEd( ipRowP,ipColV ) = AEd( ipRowP,ipColV ) - AEd(ipRowP,ipRowVB )*AEd(ipRowVB,ipColV )/rMbb
          Enddo
       Enddo
      Enddo


      Do i=1,NodP
       ipRow  = (i-1)*iDofT
       ipRowP = ipRow + NdimE1 
       Do nci=1,NdimE
          ipRowVB= ipRowB + nci
          rMbb= AEd(ipRowVB,ipRowVB)
          Do j=1,NodP
                      ipCol  = (j-1)*iDofT  
                      ipColP = ipCol + NdimE1 
                      AEd( ipRowP,ipColP ) = AEd( ipRowP,ipColP ) - AEd(ipRowP,ipRowVB )*AEd(ipRowVB,ipColP )/rMbb
          Enddo
       Enddo
      Enddo


      Do i=1,NodP
       ipRow  = (i-1)*iDofT
       Do nci=1,iDofT
         ipRowi = ipRow + nci
         BE( ipRowi) = BEd ( ipRowi )
         Do j=1,NodP
          ipCol  = (j-1)*iDofT
          Do ncj=1,iDofT
          ipColj = ipCol + ncj
                      AE( ipRowi ,ipColj ) = AEd( ipRowi ,ipColj )
        Enddo
        Enddo
       Enddo
      Enddo





  Return
 End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Elemento de Navier Stokes ALE con movimiento de red (2d substep)
! P1Bubble/P1
! Dofs ordering:  (U , V , W)    P     (DX , DY , DZ)
!                  Velocity   Pressure Mesh displacement
! It is splitted into two substeps
! In the first substep the bubbles velocities are eliminated
! In the second substep Bubbles are calculated and stored in the commonpar
! requires Param Write Swicht =1
! Incremental and nonincremental version depending on commonpar(7)  
! Commonpar(7) : == 0 Nonincremental displacement version
!                 > 0 Incremental version with fixed coordinates at the previous time step, coordinates are poscalculated with ixupdated swicht
!                 < 0 Incremental version nonlinear on incremental displacement, the mesh is updated iteratively at the same time with the solution
      SubRoutine NavierStokesBubALEDummyTP2                   &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,       & 
                  Sol0,Sol1,CommonPar,                        &  
                  Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)
    COMMON Det,LengthParam,LengthJParam

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 1.0D0 )
      Parameter (Ls1 = 1 )
      Parameter (Ls2 = 0 )
!     NdimE: dimensionality of the triangle element,
!     it could be hapen that we use a 2d element in a 3d problem. 
!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
! los dos tendrian que ir en ElemLib, porque se necesita
! la dimension del elemento, No esta implicito en el tipo de elemento
!  usado, aunque esto hace que no se pueda usar el mismo codigo
! para 2D y 3D
! OnlyVertex indicates a geometric interpolation with only vertex nodes 
      Logical OnlyVertex,Casson
! iSimplex = 0 -> The element is a Simplex
! iSimplex = 1 -> The element is a Hipercube
! iBU: use bubble function
      Parameter ( iSimplex = 0, iBU = 1 , OnlyVertex = .True. , UseVred=1.0d0 )     
      Real*8 Jac(Ndim,Ndim)
      DIMENSION AE(MaxLRows,MaxLRows),BE(MaxLRows),AEd((NodELT+1)*iDofT,(NodELT+1)*iDofT), &
       XLL(*),Sol0(*),Sol1(*),BEd(5*iDofT),                                                &
       Param(*), JParam(*),CommonPar(*),                                                   &
       Gravity(Ndim),Psi(Ndim,11),W(11),Vel0(Ndim),Vel1(Ndim),                             &
       XL(Ndim*(NodELT+1)),VelUp(Ndim),VelRed(Ndim),VelC(Ndim),                            &
       Fi1(NodElT+1),Fi2(NodElT+1),dFi2i(Ndim),dFi2j(Ndim),GradSimV(Ndim,Ndim),                                &
       dFi1i(Ndim),dFi1j(Ndim),dFi1_G(Ndim,NodElT+1),dFi1_L(Ndim,NodElT+1),                &
       dFi2_L(Ndim,NodElT+1),dFi2_G(Ndim,NodELT+1),UX1(Ndim*NodELT),                       &
       UX0(Ndim*NodELT),V0(Ndim*(NodELT+1)),V1(Ndim*(NodELT+1)),dFi1_GP(Ndim,NodElT+1) !,VR(Ndim*(NodELT+1))    !,NodeEdge(6)
       
!      DIMENSION AEb(MaxLRows,MaxLRows),BEb(MaxLRows)

!  Real*8 , Static :: Mass(4,4),MassL(4)
!  Static iFirst
    AEd=0.0d0 ; BEd=0.0d0
    NodEl  = NodElT + 1
    NDimE=Ndim

    OpTi=1.0d0 !*Dtm
! nci do variable,  number of coordinate index
      Ro       = CommonPar(1)
      Vis      = CommonPar(2)
	  Vis0     = Vis
      Epsilona = CommonPar(3)
!     Epsilon  = CommonPar(3)*Ro

      Theta         =       CommonPar(4)
	  iSimetry      = nint( CommonPar(5) )
	  iSimetryAxis  = nint( CommonPar(6) )
      iSimetryAxis1 = iSimetryAxis + 1
!     iSimetryAxis = 1 -> RS about X axis 
!     iSimetryAxis = 0 -> RS about Y axis 

      Incremental = nint( CommonPar(7) ) 
!	  Last = Last+1
      iCasson=      nint( CommonPar(8))
      Yield  =       CommonPar(9)
	  Casson=.false.
	  if (iCasson /= 0)Casson=.true.
      ShearRateLimit = CommonPar(10)
	  Last = 10
      Do nci=1, NdimE
             Gravity(nci)= CommonPar( Last+nci )
             V0(Ndim*NodElT+nci) = Param(nci)
      end do

	Theta1  = 1.0d0-Theta
!	ThetaP  = Theta
	ThetaP  = 1.0d0
	Theta1P = 1.0d0-ThetaP
    Rot   = Ro/Dtm

!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
     NdimE1  = Ndim + 1
     NodP    = NdimE1
!     IF (OnlyVertex) Then
     NodG    = NodP
!     Else
!          NodG = NodEL -iBu
!     EndIF

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     c�lculo de "h" Hred para determinar Psi1i 16/9/01
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
!   ojo esta formula solo vale para NDim = 2 o 3
    nAristas = NdimE1 + 2*(Ndim-2)
! Se proyectan los lados en la direccion de la velocidad
    VelUp=0.0d0
!    ipNodSb = (NodEl-1)*Ndim
!    VR ( ipNodSb )=0.0d0
    Do nod=1, NodG
       ipNodV = (nod-1)*iDofT
       ipNodS = (nod-1)*Ndim
       ipNodX =  ipNodV + NdimE1  
       Do nci = 1, Ndim				
          ipNodXi      = ipNodX + nci 
          ipNodSi      = ipNodS + nci 
          ipNodVi      = ipNodV + nci
          UX1( ipNodSi )= Sol1(ipNodXi)  !Displacement or incremental Displacement
          UX0( ipNodSi )= Sol0(ipNodXi)
          if(Incremental> 0 ) then   
           XL ( ipNodSi )= XLL( ipNodSi)  !Incremental : the coordinates are post-calculated 
            else
           XL ( ipNodSi )= XLL( ipNodSi) + Sol1(ipNodXi) ! Non incremental
          endif
          V0 ( ipNodSi )= Sol0(ipNodVi)
          V1 ( ipNodSi )= Sol1(ipNodVi)
!          VR ( ipNodSi )= UX1( ipNodSi )/DTm ! Incremental
!          VR ( ipNodSb )= VR ( ipNodSb ) + VR ( ipNodSi ) ! VR bubble
            if (Incremental /= 0 ) then
              VelUp( nci  ) = VelUp(nci)    + V1 ( ipNodSi ) - UX1( ipNodSi )/DTm  *UseVred ! Incremental 
            else
              VelUp( nci  ) = VelUp(nci)    + V1 ( ipNodSi ) - ( UX1( ipNodSi )-UX0( ipNodSi ) )/Dtm  *UseVred
            endif
       enddo
    enddo
    VelUp = VelUp / NodG

	if (Casson) then
	!Gauss point coordinates
       PSI(1,1) = 0.25D0
       PSI(2,1) = 0.25D0
       PSI(3,1) = 0.25D0
	   !Local Shape funtions Derivatives
	   Call LocalShapeDer(NodEL,NdimE,dFi1_L,PSI(1,1),1,0)
       Call Jacobian(Jac,Det,NdimE,NodG,XL,dFi1_L)
       dFi1_G=0.0d0  
	   !Global Shape funtions Derivatives
       Call GlobalShapeDer(NodEL,NdimE,dFi1_L,dFi1_G,Jac)
	   !EpSNorm= Norm of Symmetric grad of Vel
	   Call STRAIN1(Ndim,NodG,V1,dFi1_G,GradSimV,EpSNorm)
	    ShearRate = DSQRT(2.0d0)*EpSNorm
	    if (ShearRate>ShearRateLimit) then 			
		Vis = (DSQRT(Vis0)+DSQRT( Yield / ShearRate))**2	
		else
		Vis=(DSQRT(Vis0)+DSQRT( Yield / ShearRateLimit))**2

		endif
	endif




    VRmax      = 0.d0
    VArisAcum  = 0.d0
    NodP1  = NodP - 1
    Do nod  = 1, NodP1
      ipNod = (nod-1) * Ndim

      Do nodr   =  nod  + 1,   NodP
         ipNodr = (nodr - 1) * Ndim

          VR     = 0.0d0 
          VAris  = 0.0d0 
          Do nci = 1, Ndim				
             VArisi = XL(ipNodr+nci) - XL(ipNod+nci)      
             VR     = VR + VArisi*VelUp(nci)
             VAris  = VAris + VArisi*VArisi   
          enddo
          VArisAcum = VArisAcum + Dsqrt(VAris)
          VRabs = dabs(VR) ; if (VRmax < VRabs) VRmax = VRabs
      enddo
    enddo

	VArisAcum = VArisAcum / nAristas
    velmod    = Dsqrt( Sum(VelUp*VelUp) )
    if ( velmod .ne. 0.0d0 ) then
      Hred = 0.5 * VRmax / velmod
    else
      Hred = 0.5 * VArisAcum
    endif
    Coura=velmod*Dtm/VArisAcum
 !   if (Coura.gt..85) Write(*,*)"Courant", Coura  


    Pec  = Ro * velmod * Hred /Vis /2.0d0
    If ( Pec .Gt. 1.0d0 ) Then
       TauU  = Hred /( 2.d0*velmod )*(1.0d0-1.0d0/Pec)
!       TauU  = 1.d0/( 2.d0*Ro*velmod/Hred + 4.d0*vis/Ro/(Hred**2) )

!       TauU  = 1.d0/( 2.d0*Ro*velmod/Hred + 4.d0*vis/Ro/(Hred**2) )
    else
          TauU=0.0d0
    endif
!       TauP      = - TauU 

!           Fin Calculo Hred
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  If (Ndim<3) Then
!    NGP = 3
!  Else
!    NGP = 4
!  EndIf
  If (Ndim < 3) Then
      NGP =7
    Else
      NGP =11
  EndIf

!   Psi: Gauss Points Matrix, W: Weights
    Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
	
!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
	Vel0  =0.0d0
	Vel1  =0.0d0
	VelC  =0.0d0
    VelRed=0.0d0

!     dFi1_L = first derivatives with respect to intrinsic Local coordinates
!              refers to linear shape functions
!     dFi1_G = first derivatives with respect to Global coordinates
!     dFi2_L = first derivatives with respect to intrinsic Local coordinates
!              refers to Cuadratic shape functions

!      Call LocalShapeDer(NodG+iBu,NdimE,dFi1_L,PSI(1,nG),1,iBu)
!      Call LocalShapeDer(NodEL,NdimE,dFi2_L,PSI(1,nG),2,iBu)

       Call LocalShapeDer(NodEL,NdimE,dFi1_L,PSI(1,nG),1,iBu)
       if (nG .eq. 1) Call Jacobian(Jac,Det,NdimE,NodG,XL,dFi1_L)
       dFi1_G=0.0d0  
       Call GlobalShapeDer(NodEL,NdimE,dFi1_L,dFi1_G,Jac)
       Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!       if (nG .eq. 1) dFi1_GP = dFi1_G
      Do NodV= 1, NodG
       ipNodS = (NodV-1)*Ndim   
        Do nci=1, Ndim
        ipNodSi = ipNodS + nci

        Vel1i = Theta*V1(ipNodSi) + Theta1*V0(ipNodSi)
        if (Incremental /= 0 ) then
          VelRi = UX1(ipNodSi)/Dtm
         else
          VelRi = (UX1(ipNodSi) - UX0(ipNodSi))/Dtm ! Non incremental
        endif

        Vel1  (nci) = Vel1(nci)   + Vel1i*Fi1(NodV)
        VelRed(nci) = VelRed(nci) + VelRi*Fi1(NodV) *UseVred
       Enddo
      Enddo

      VelC = Vel1 - VelRed 
      DV=Det*W(nG)*OpTi
	if (iSimetry .eq. 1) then
            Radio = 0.0d0
	        nci = iSimetryAxis1
                Do Nodrad=1, NodG
                 FiNodRad = Fi1(Nodrad)
                 Radio =Radio + XL( (Nodrad-1)*NdimE + nci)*FiNodRad
                Enddo
      DV=DV*Radio*2.0d0*PI
	Endif

      LoopRow: Do i=NodEL,NodEL
        ipRow  = (i-1)*iDofT
        ipRow1 = ipRow + 1
!        Fi2i = Fi2(i)
        Fi1i = Fi1(i)

        ConvecUpi=0.0d0
        Do nci=1, NdimE
           dFi1i(nci) = dFi1_G(nci,i)
           ConvecUpi= ConvecUpi + VelC(nci)*dFi1i(nci)
        Enddo
!               ConvecUpi = < V , grad FiI > 

        WUpi      = (Fi1i + TauU * ConvecUpi)* DV
!        WLSi      = (Fi2i + Theta*DTm * ConvecUpi)* DV
        Cvis1     = vis * DV 
        Cvis0     = Theta1 * Cvis1 
        Cvis1     = Theta  * Cvis1 

        Do nci=1, NdimE
           BEd( ipRow + nci ) =BEd( ipRow + nci ) + Ro*Gravity(nci)*WUpi 
        Enddo


        LoopCol: Do j=1,NodEL
         ipCol  = (j-1)*iDofT 
         ipCol1 = ipCol + 1
         Fi1j   = Fi1(j)

         ipCVj  = (j-1)*Ndim 
         ipCVj1 = (j-1)*Ndim + 1 

         GradFi_GradFj= 0.0d0
         Ro_V_GradFj  = 0.0d0   
         Do nci=1, NdimE
           dFi1j(nci) = dFi1_G(nci,j)
           GradFi_GradFj = GradFi_GradFj + dFi1i(nci) * dFi1j(nci) 
	       Ro_V_GradFj   = Ro_V_GradFj   + Ro * VelC(nci) * dFi1j(nci)
         Enddo                                            

	       CoefK0 = Cvis0 * GradFi_GradFj
	       CoefK1 = Cvis1 * GradFi_GradFj
! Para Upwinding en M masas y LS
!	       C0ij = WUpi  * (-Rot*Fi2j + Ro_V_GradFj * Theta1 )
!	       C1ij = WUpi  * ( Rot*Fi2j + Ro_V_GradFj * Theta  )
!	       CLsij= WLsi  * ( Rot*Fi2j + Ro_V_GradFj * Theta  )

! para el tradicional
	       C0ij = (-Rot*Fi1j*Fi1i* DV + WUpi  * Ro_V_GradFj * Theta1 )
	       C1ij = ( Rot*Fi1j*Fi1i* DV + WUpi  * Ro_V_GradFj * Theta  )
! Mass Lumped  La matriz de masas se pone afuera
!	       C0ij = ( WUpi  * Ro_V_GradFj * Theta1 )
!	       C1ij = ( WUpi  * Ro_V_GradFj * Theta  )



! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
! 1-LS
!             AE(ipRow1 , ipCol1)= AE(ipRow1 , ipCol1) + CLsij + CoefK1 
! 2-Picard comun
           AEd(ipRow1 , ipCol1)= AEd(ipRow1 , ipCol1) + C1ij + CoefK1 

         Do nci=1, NdimE
          BEd( ipRow + nci )= BEd( ipRow + nci ) - (C0ij+CoefK0)*V0(ipCVj+ nci) 

            if (Casson) then
		    Do ncj=1, NdimE
            BEd( ipRow + nci )= BEd( ipRow + nci ) - CVis1*dFi1j(nci)*dFi1i(ncj)*V1(ipCVj + ncj)&
	    &- CVis0*dFi1j(nci)*dFi1i(ncj)*V0(ipCVj + ncj) 
            Enddo
			EndIf                                            
! 1-LS
!               BE( ipRow + nci )= BE( ipRow + nci ) - (C1ij-CLsij )*Sol1(ipCol+ nci) 
         Enddo                                            
!      Hasta aqui ya ensamble para iDof=1, ahora para 2 y/o 3
         Do iDof=1,NdimE-1
           AEd( ipRow1 + iDof , ipCol1 + iDof ) =  AEd( ipRow1 , ipCol1 )
         Enddo

           if (iSimetry .eq. 1) then
             ipRowV = ipRow1 + iSimetryAxis
             ipColV = ipCol1 + iSimetryAxis
             ipCVjr = ipCVj1 + iSimetryAxis
             CoefK0 = Cvis0*Fi1i/Radio * Fi1j/Radio
             CoefK1 = Cvis1*Fi1i/Radio * Fi1j/Radio
             AEd( ipRowV , ipColV )= AEd( ipRowV , ipColV ) + CoefK1
             BEd( ipRowV )= BEd( ipRowV ) - CoefK0*V0(ipCVjr) 
           endif

        End do LoopCol

!-----------------------------------------------
!     ensamble de P
        LoopColP: Do j=1,NodP
            ipColP = (j-1)*iDofT + NdimE1 
	        Fi1jDV=Fi1(j)*DV

            Do nci=1,NdimE
               ipRowV = ipRow + nci
	           AEd( ipRowV , ipColP )= AEd( ipRowV , ipColP ) -  dFi1i(nci) * Fi1jDV
!                                                           -  div(Fi2i)  *(Fi1j Pj)       
            Enddo                     

            if (iSimetry .eq. 1) then
                nci = iSimetryAxis1
                ipRowV = ipRow + nci
	            AEd( ipRowV , ipColP )= AEd( ipRowV , ipColP )- Fi1i/Radio * Fi1jDV
            endif

         Enddo LoopColP
!-----------------------------------------------
     End do LoopRow
!--------------------------
   EndDo LoopGaussPoints
!--------------------------


      ipRowB = (NodP)*iDofT
       Do nci=1,NdimE
          ipRowVB= ipRowB + nci
          Do j=1,NodP
                      ipCol  = (j-1)*iDofT  
                      ipColP = ipCol + NdimE1 
                      BEd( ipRowVB ) = BEd( ipRowVB ) - AEd( ipRowVB,ipColP )*Sol0(ipColP)*Theta1P
                      AEd( ipRowVB , ipColP )= AEd( ipRowVB , ipColP )*ThetaP
          Enddo
       Enddo

       Do nci=1,NdimE
          ipRowVB= ipRowB + nci
          rMbb= AEd(ipRowVB,ipRowVB)
!          AEd(ipRowVB,ipRowVB) = 1.0d0
          BEd( ipRowVB ) = BEd( ipRowVB )/rMbb
          Do j=1,NodP
                      ipCol  = (j-1)*iDofT  
                      ipColV = ipCol + nci  
                      ipColP = ipCol + NdimE1 
                      BEd( ipRowVB ) = BEd( ipRowVB ) - AEd( ipRowVB,ipColV )*Sol1(ipColV)/rMbb
                      BEd( ipRowVB ) = BEd( ipRowVB ) - AEd( ipRowVB,ipColP )*Sol1(ipColP)/rMbb
          Enddo
          Param(nci+LengthParam) = BEd( ipRowVB )
       Enddo

! se impone una ecuacion falsa para la presion del primer nodo
       ipRowP  = Ndim + 1
       BE ( ipRowP )          = Sol1(ipRowP)
       AE ( ipRowP , ipRowP ) = 1.0d0


!####################################################
!                 calculo de la red                 ! 
!	Ensamblamos la parte del movimiento de la Red.
!####################################################
! Usamos las derivadas del 1� pto de gauss del Loop para el nodo bubble
!     DV = Det/6.0d0
!      DV = Det
!      LoopRowPR: Do i=1,NodP

!        ipRowP  = (i-1)*iDofT + NdimE1
!        Do nci=1, NdimE
!           dFi1i(nci) = dFi1_GP(nci,i)
!        Enddo

!          LoopColPR: Do j=1,NodP
!            ipColP = (j-1)*iDofT + NdimE1 
!                     Do ndRow   = 1, NdimE
!                            ipRowR  = ipRowP + ndRow
!                            ipColR  = ipColP + ndRow
!                            Do is = 1, NdimE
!                             AE(ipRowR, ipColR) = AE(ipRowR, ipColR) + dFi1i(is)*dFi1_GP(is,j)*DV
!                            End Do
!                     Enddo
!          Enddo LoopColPR
!-----------------------------------------------
!       End do LoopRowPR

  Return
 End


