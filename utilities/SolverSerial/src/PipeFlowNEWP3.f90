!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubRoutine PipeFlow1D_QP_Conec( AE,BE,MaxLRows,XL,NDim,iDofT,  &
     &   NodEl,Sol0,Sol1,CommonPar,Param,JParam,DelT,DTm,Time )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION AE(MaxLRows,*), BE(*), XL(*),Sol0(*),Sol1(*), &
     & Param(*), JParam(*),CommonPar(*),iFlowDir(NodEl)

      Parameter (PI = 3.141592653589793238D0)
      Parameter (C3 = 0.333333333333333333D0)
      Parameter (C6 = 0.166666666666666667D0)
!      Parameter (Theta = 0.5D0 )
      Parameter (Ls1 = 1 )
      Parameter (Ls2 = 0 )


      Penalty= CommonPar(1)  
      iShiftP= nint(CommonPar(2))
      Theta= CommonPar(3)  
      Theta1= 1.0d0-Theta  
!      DPnor= CommonPar(4)  

      R1      = Param( 1 )
      R2      = Param( 2 )
      Cap     = Param( 3 )
      Pter    = Param( 4 )
      dPter   = 0.0d0
      Nf_Pter = JParam(NodEl)

      Nod=1
      do while (Nod .lt. NodEl)
                 iFlowDir(Nod) = JParam(Nod) 
                 Nod=Nod + 1
      end do

     If (Nf_Pter .GT. 0 ) Then
           ipFunc  = 5               
!        goto 897
!           NPoints=JParam(NodEl)
           NPoints = Nf_Pter
           R1_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           R1_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           R1  = 0.5D0 * ( R1_1 + R1_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+1)
           R2_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           R2_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           R2  = 0.5D0 * ( R2_1 + R2_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+2)
           Cap_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           Cap_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           Cap  = 0.5D0 * ( Cap_1 + Cap_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+3)
897        Pter0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           Pter1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           Pter  = Pter1
!          Pter  = 0.5D0 * ( Pter1 + Pter0 )
           dPter = ( Pter1 - Pter0 ) / Dtm



!	           Pter0 = PTer_F  ( Time        , Nf_Pter, Param( 5 )  )
!	           Pter1 = PTer_F  ( Time  + Dtm , Nf_Pter, Param( 5 )  )
!                 Pter  = Pter1
!                 Pter  = 0.5D0 * ( Pter1 + Pter0 )
!                 dPter = ( Pter1 - Pter0 ) / Dtm





      Endif

!	If (Nf_Pter .LT. 0 ) Then
!	           Pter0 = 1.333d5* (1.d0 -  Time/.8d0)
!	           Pter1 = 1.333d5* (1.d0 - (Time  + Dtm) / .8d0 )
!                dPter = ( Pter1 - Pter0 ) / Dtm
!                 Pter  = Pter1

!                If (Pter1 .LT. 0.0d0 ) Then
!                  Pter=0.0d0c
!	            dPter=0.0d0
!                endif
!      Endif



!	ipQ   = (NodEl-1) * iDofT + 1
	ipQ   = 1
	ipP   = ipQ + iShiftP
      Q0N   = Sol0 ( ipQ )
      P0N   = Sol0 ( ipP )
      Q1N   = Sol1 ( ipQ )
      P1N   = Sol1 ( ipP )

!	iRow_Qter = 1
	iRow_Qter = ipQ
	iRow_Pter = iRow_Qter + iShiftP

!        AE(iRow_Pter,iRow_Pter) = (1.0d0 + R2*Cap/Dtm )*Penalty*DPnor
        AE(iRow_Pter,iRow_Pter) = (1.0d0 + R2*Cap/Dtm )*Penalty
        AE(iRow_Pter,iRow_Qter) =-((R1+R2)+ R1*R2*Cap/Dtm)*Penalty
        AE(iRow_Qter,iRow_Qter) = 1.0d0*Penalty

	    Do Nod = 1, NodEl - 1
! los nodos estan desplazados en 1
	        iRow_QNod = iDofT*Nod + 1 
	        iRow_PNod = iRow_QNod + iShiftP
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              AE (iRow_PNod , iRow_PNod ) =  1.0d0*Penalty
              AE (iRow_PNod , iRow_Pter ) = -1.0d0*Penalty
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              AE (iRow_Qter,iRow_QNod) = -1.0d0*iFlowDir(Nod)*Penalty

	    End Do


!     BE(iRow_Pter)= (  (   R2*Cap/Dtm   )*DPnor  *P0N +     &
     BE(iRow_Pter)= (  (   R2*Cap/Dtm   )  *P0N +     &
                       ( - R1*R2*Cap/Dtm)  *Q0N +     &
                           Pter + R2*Cap*dPter ) *Penalty


Return
End 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine PipeFlow1D_QP                            &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!     Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 )
      Parameter (CLap = .0d0 )
      Parameter (iSwQ = 0  )
      Parameter (iSwP = 0  )
!      Parameter (iSwPF = 2 ) ! Controla la forma de P(A)
! iSwi controla escaleo de las ecuaciones ver al final

!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NGP   = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP = nint(CommonPar(4))
      Theta   = CommonPar(5)
      iSwPF   = nint(CommonPar(6))
      Theta1  = 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do

      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )  !Alfa= pi . Ro . Ho
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  7 )
      Ep0          = Param (  8 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref= (ArefD - ArefI) / DX
      DxAlfa= (AlfaD - AlfaI) / DX
      roCo2I = Elas*AlfaI/ArefI/2.0d0 
      roCo2D = Elas*AlfaD/ArefD/2.0d0 
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
	iBU=0

!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTP  
      ipAI  = ipPI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTP
      ipAD  = ipPD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )

	APunI = (A1I-A0I) / Dtm
	APunD = (A1D-A0D) / Dtm

	PvI   = PVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
	PvD   = PVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      PRE1I = PRE1I + PvI
!      PRE1D = PRE1D + PvD
!      Pv0I   = P0I*DPnor - PRE0I 
!      Pv0D   = P0D*DPnor - PRE0D 
!      if (Dabs(APunI/A1I)+Dabs(APunI/A1I) .GT. 1.d-8/Dtm )then
!      DtPv   = .5d0*(PvI+PvD-Pv0I-Pv0D)/Dtm
!	DxPv   = .5d0*((Pv0D-Pv0I)+(PvD-PvI))/Dx
!      else
!      DtPv   = .0d0
!	DxPv   = .0d0
!	endif
      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      RoCsAI = DsqrT(A0IsRo*DPA0I) /A0IsRo
      RoCsAD = DsqrT(A0DsRo*DPA0D) /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs( (Fmas1I+Fmen1I)/(Fmas1I-Fmen1I) )
      rMachD= Dabs( (Fmas1D+Fmen1D)/(Fmas1D-Fmen1D) )

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)


      DPA1 = 0.5d0* (DPA1I+DPA1D)
      DPA0 = 0.5d0* (DPA0I+DPA0D)
!	DPvis_men = DtPv + Fmen1*DxPv 
!	DPvis_mas = DtPv + Fmas1*DxPv 
!	GGn =  - Fmas1/DPA1*DPvis_men
!	GGs =  - Fmen1/DPA1*DPvis_mas

!   OOOOJJJJOOO normalizacion de la presion !!!!!!!!!!!!!
!      DPA1  = 0.5d0* ( DPA1 + DPA0 )/DPnor
      DPA1  = 0.5d0* ( DPA1 + DPA0 )
!      Fmas1 = 0.5d0* (Fmas1 + Fmas0)
!      Fmen1 = 0.5d0* (Fmen1 + Fmen0)
	Pv = 0.5d0*(PvI+PvD)
	CpPvPi= Cp*(Pv-Pinf)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
    if (Ls2 > 0) then
!            Upmen= UpLs2/(CourantFmen*Theta)
!            Upmas= UpLs2/(CourantFmas*Theta)
!            UpmenTheta = UpLs2 / CourantFmen * DTm
!            UpmasTheta = UpLs2 / CourantFmas * DTm
            UpmenTheta = UpLs2 * DX / dabs(Fmen1)
            UpmasTheta = UpLs2 * DX / dabs(Fmas1)
    else
            UpmenTheta = Theta * DTm  
            UpmasTheta = Theta * DTm
    endif 

      Fr  = Friction (Vel1,A1,Vis)
      Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)*Vel1
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)

      FWallvis= - .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX
      GI  =  Fvis + Gravi*A1I + FWallvis
      GD  =  Fvis + Gravi*A1D + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

      GG  = 0.5d0 * ( GI + GD )
      GGmen = GG 
      GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

      Fmas1_DPA1 = Fmas1/DPA1
      Fmen1_DPA1 = Fmen1/DPA1
      Fmas1xCpPvPi= Fmas1*CpPvPi
      Fmen1xCpPvPi= Fmen1*CpPvPi

! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------

	Det = Dx
	dFi1_G (1,1) = - 1.0d0/DX
	dFi1_G (1,2) =   1.0d0/DX


      Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!      Call ShapeF (NodEL,NdimE,Fi2,PSI(1,nG),2,iBu)

      DetW=Det*W(nG)

      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowQ = ipRow + 1
        ipRowP = ipRowQ + iShifTP
        Fi1i  = Fi1(i)
        dFi1i = dFi1_G ( 1 , i )

        dResMenQ_i = Fi1i  + UpmenTheta * Fmen1 * dFi1i
        dResMasQ_i = Fi1i  + UpmasTheta * Fmas1 * dFi1i
!	    dResMenP_i = -(Fmas1_DPA1*dResMenQ_i + Fmas1* Cp1* theta *Fi1i)
!	    dResMasP_i = -(Fmen1_DPA1*dResMasQ_i + Fmen1* Cp1* theta *Fi1i)
	    dResMenP_i = -(Fmas1_DPA1*dResMenQ_i )
	    dResMasP_i = -(Fmen1_DPA1*dResMasQ_i )

        dResMenQ_i = dResMenQ_i * DetW
        dResMasQ_i = dResMasQ_i * DetW
        dResMenP_i = dResMenP_i * DetW
        dResMasP_i = dResMasP_i * DetW


         BE( ipRowQ ) = BE( ipRowQ )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenQ_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasQ_i 
         BE( ipRowP ) = BE( ipRowP )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenP_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasP_i 


        LoopCol: Do j=1,NodEL
         ipCol    = (j-1)*iDofT 
         ipColQ   = ipCol + 1
         ipColP   = ipColQ + iShifTP

         dFi1j    = dFi1_G ( 1 , j )
         Fi1j     = Fi1(j)
         Fi1j_Dtm = Fi1j/Dtm
         Fmen1xdFi1j = Fmen1*dFi1j
         Fmas1xdFi1j = Fmas1*dFi1j

         CoefMen  = Fi1j_Dtm + Fmen1xdFi1j*Theta
         CoefMas  = Fi1j_Dtm + Fmas1xdFi1j*Theta

! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
         AE(ipRowQ , ipColQ)= AE(ipRowQ , ipColQ) + CoefMen * dResMenQ_i + CoefMas * dResMasQ_i 
!     &      - Fvis *(dResMenQ_i + dResMasQ_i) 
          Cp1xThetaxFi1j       = Cp1*Theta*Fi1j
          Fmas1xCp1xThetaxFi1j = Fmas1*Cp1xThetaxFi1j
          Fmen1xCp1xThetaxFi1j = Fmen1*Cp1xThetaxFi1j

         AE(ipRowQ , ipColP)= AE(ipRowQ , ipColP)                    &
          -( Fmas1_DPA1*CoefMen + Fmas1xCp1xThetaxFi1j )*dResMenQ_i  &
          -( Fmen1_DPA1*CoefMas + Fmen1xCp1xThetaxFi1j )*dResMasQ_i   

         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)                    &
          -( Fmas1_DPA1*CoefMen + Fmas1xCp1xThetaxFi1j )*dResMenP_i  &
          -( Fmen1_DPA1*CoefMas + Fmen1xCp1xThetaxFi1j )*dResMasP_i  

         AE(ipRowP , ipColQ)= AE(ipRowP , ipColQ) + CoefMen * dResMenP_i + CoefMas * dResMasP_i
!     &      - Fvis *(dResMenP_i + dResMasP_i) 

	   BEmenj = Fi1j_Dtm - Fmen1xdFi1j*theta1
	   BEmasj = Fi1j_Dtm - Fmas1xdFi1j*theta1
       CpTh1Fj= Cp1*theta1*Fi1j

       BE1 = BEmenj*Sol0(ipColQ) + (-Fmas1_DPA1*BEmenj + Fmas1*CpTh1Fj)*Sol0(ipColP)
       BE2 = BEmasj*Sol0(ipColQ) + (-Fmen1_DPA1*BEmasj + Fmen1*CpTh1Fj)*Sol0(ipColP)

       BE( ipRowQ ) = BE( ipRowQ )+ BE1*dResMenQ_i + BE2*dResMasQ_i
       BE( ipRowP ) = BE( ipRowP )+ BE1*dResMenP_i + BE2*dResMasP_i  
! Lapidus Shock capturing
!         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)+rLap*dFi1i*dFi1j*DetW  


        End do LoopCol

!-----------------------------------------------
      End do LoopRow


!--------------------------
      EndDo LoopGaussPoints
!--------------------------


!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!      LoopArea: Do i=1,NodEL
	i = 1
        ipRowQ  = (i-1)*iDofT + 1
        ipColP = ipRowQ + iShifTP
        ipRowA = ipColP - 1
      AE (ipRowA,ipColP) =    1.0D0
      AE (ipRowA,ipRowA) = -(DPA1I +1.0d0* DPVAI)
      BE (ipRowA)        =  (PRE1I+PvI  - (DPA1I +1.0d0* DPVAI) * A1I)
!      AE (ipRowA,ipRowA) = -(DPA1I +1.0d0* DPVAI)/DPnor
!      BE (ipRowA)        =  (PRE1I+PvI  - (DPA1I +1.0d0* DPVAI) * A1I)/DPnor 

	i = 2
        ipRowQ  = (i-1)*iDofT + 1
        ipColP = ipRowQ + iShifTP
        ipRowA = ipColP - 1
      AE (ipRowA,ipColP) =    1.0D0
      AE (ipRowA,ipRowA) = -(DPA1D +1.0d0* DPVAD)
      BE (ipRowA)        =  (PRE1D+PvD  - (DPA1D +1.0d0* DPVAD) * A1D)
!      AE (ipRowA,ipRowA) = -(DPA1D +1.0d0* DPVAD)/DPnor
!      BE (ipRowA)        =  (PRE1D+PvD  - (DPA1D +1.0d0* DPVAD) * A1D)/DPnor


!      End do LoopArea
      if(iSwP.ne.0) then
          ipColQ1 = 1
          ipRowQ1 = 1
          ipRowP1 = 1 + iShifTP
          ipColQ2 = 1 + iDofT
          ipRowP2 = ipColQ2 + iShifTP
          ipRowQ2 = ipColQ2
          RoCsAI=1.0d0/RoCsAI
          RoCsAD=1.0d0/RoCsAD

          AE (ipRowP1,ipColQ1) = AE (ipRowP1,ipColQ1)/RoCsAI 
          AE (ipRowP1,ipColQ2) = AE (ipRowP1,ipColQ2)/RoCsAI
          AE (ipRowP1,ipRowP1) = AE (ipRowP1,ipRowP1)/RoCsAI
          AE (ipRowP1,ipRowP2) = AE (ipRowP1,ipRowP2)/RoCsAI
          BE (ipRowP1) =  BE (ipRowP1)/RoCsAI

          AE (ipRowP2,ipColQ1) = AE (ipRowP2,ipColQ1)/RoCsAD 
          AE (ipRowP2,ipColQ2) = AE (ipRowP2,ipColQ2)/RoCsAD
          AE (ipRowP2,ipRowP1) = AE (ipRowP2,ipRowP1)/RoCsAD
          AE (ipRowP2,ipRowP2) = AE (ipRowP2,ipRowP2)/RoCsAD
          BE (ipRowP2) =  BE (ipRowP2)/RoCsAD

      endif
      if(iSwQ.ne.0) then
          ipColQ1 = 1
          ipRowQ1 = 1
          ipRowP1 = 1 + iShifTP
          ipColQ2 = 1 + iDofT
          ipRowP2 = ipColQ2 + iShifTP
          ipRowQ2 = ipColQ2
          RoCsAI=1.0d0/RoCsAI
          RoCsAD=1.0d0/RoCsAD

          AE (ipRowQ1,ipColQ1) = AE (ipRowQ1,ipColQ1)/RoCsAI 
          AE (ipRowQ1,ipColQ2) = AE (ipRowQ1,ipColQ2)/RoCsAI
          AE (ipRowQ1,ipRowP1) = AE (ipRowQ1,ipRowP1)/RoCsAI
          AE (ipRowQ1,ipRowP2) = AE (ipRowQ1,ipRowP2)/RoCsAI
          BE (ipRowQ1) =  BE (ipRowQ1)/RoCsAI

          AE (ipRowQ2,ipColQ1) = AE (ipRowQ2,ipColQ1)/RoCsAD 
          AE (ipRowQ2,ipColQ2) = AE (ipRowQ2,ipColQ2)/RoCsAD
          AE (ipRowQ2,ipRowP1) = AE (ipRowQ2,ipRowP1)/RoCsAD
          AE (ipRowQ2,ipRowP2) = AE (ipRowQ2,ipRowP2)/RoCsAD
          BE (ipRowQ2) =  BE (ipRowQ2)/RoCsAD
      endif




!      End do LoopArea
!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubRoutine PipeFlowIN_LW                                  &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NdimE: dimensionality of the triangle element,
!     it could be hapen that we use a 2d element in a 3d problem. 
!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
! los dos tendrian que ir en ElemLib, porque se necesita
! la dimension del elemento, No esta implicito en el tipo de elemento
!  usado, aunque esto hace que no se pueda usar el mismo codigo
! para 2D y 3D
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (	NGP = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP= nint(CommonPar(4))
      Theta  = CommonPar(5)
      iSwPF  = nint(CommonPar(6))
      Theta1= 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do

      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  7 )
      Ep0          = Param (  8 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp 

      DxAref= (ArefD - ArefI) / DX
      DxAlfa= (AlfaD - AlfaI) / DX
      roCo2I = Elas*AlfaI/ArefI/2.0d0 
      roCo2D = Elas*AlfaD/ArefD/2.0d0 
      DxroCo2 = (roCo2D - roCo2I) / DX


!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1
      NodG    = NodP

      ipQI  = 1
      ipPI  = ipQI + iShifTP  
      ipAI  = ipPI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTP
      ipAD  = ipPD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )

      APunI = (A1I-A0I) / Dtm
      APunD = (A1D-A0D) / Dtm

      PvI   = PVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
      PvD   = PVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      PRE1I = PRE1I + PvI
!      PRE1D = PRE1D + PvD
!      Pv0I   = P0I*DPnor - PRE0I 
!      Pv0D   = P0D*DPnor - PRE0D 
!      if (Dabs(APunI/A1I)+Dabs(APunI/A1I) .GT. 1.d-8/Dtm )then
!      DtPv   = .5d0*(PvI+PvD-Pv0I-Pv0D)/Dtm
!	DxPv   = .5d0*((Pv0D-Pv0I)+(PvD-PvI))/Dx
!      else
!      DtPv   = .0d0
!	DxPv   = .0d0
!	endif
      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )


      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)


      rMachI= Dabs((Fmas1I+Fmen1I)/(Fmas1I-Fmen1I))
      rMachD= Dabs((Fmas1D+Fmen1D)/(Fmas1D-Fmen1D))

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)


      DPA1 = 0.5d0* (DPA1I+DPA1D)
      DPA0 = 0.5d0* (DPA0I+DPA0D)
!   OOOOJJJJOOO normalizacion de la presion !!!!!!!!!!!!!
!      DPA1  = 0.5d0* ( DPA1 + DPA0 )/DPnor
      DPA1  = 0.5d0* ( DPA1 + DPA0 )
!      Fmas1 = 0.5d0* (Fmas1 + Fmas0)
!      Fmen1 = 0.5d0* (Fmen1 + Fmen0)
	Pv = 0.5d0*(PvI+PvD)
	CpPvPi= Cp*(Pv-Pinf)


      FrI  = Friction (Vel0I,A0I,Vis)
      FrD  = Friction (Vel0D,A0D,Vis)
      FKvisI= FrI/4.0d0*Dsqrt( A0I*PI)*Dabs(Vel0I)/A0I
      FKvisD= FrD/4.0d0*Dsqrt( A0D*PI)*Dabs(Vel0D)/A0D
      FvisI = - FKvisI*Q0I
      FvisD = - FKvisD*Q0D
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)

      FWallvis= - .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX
      GI  =  FvisI + Gravi*A1I + FWallvis
      GD  =  FvisD + Gravi*A1D + FWallvis

      Ges   = GI  + ( GD-GI )/DX * (-Fmen1)*Dtm 
      Qes   = Q0I + (Q0D-Q0I)/DX * (-Fmen1)*Dtm 
      Aes   = A0I + (A0D-A0I)/DX * (-Fmen1)*Dtm 
      GG    = 0.5d0*(Ges+GI)
    i = 1
    ipRow  = (i-1)*iDofT
    ipRowQ = ipRow + 1
    ipRowP = ipRowQ + iShifTP
    ipRowA = ipRowP - 1

        BE( ipRowQ          ) =  GG*Dtm + Qes - Fmas1*Aes
        AE( ipRowQ , ipRowQ ) =  1.0d0 
        AE( ipRowQ , ipRowA ) = -Fmas1

!        BE( ipRowA          ) =  GG*Dtm + Qes - Fmas1*Aes
!        AE( ipRowA , ipRowQ ) =  1.0d0 
!        AE( ipRowA , ipRowA ) = -Fmas1

      AE (ipRowA,ipRowP) =    1.0D0
!      AE (ipRowA,ipRowA) = -(DPA1I +1.0d0* DPVAI)/DPnor
      AE (ipRowA,ipRowA) = -(DPA1I +1.0d0* DPVAI)
!      BE (ipRowA)        =  (PRE1I - (DPA1I +1.0d0* DPVAI) * A1I)/DPnor 
      BE (ipRowA)        =  (PRE1I - (DPA1I +1.0d0* DPVAI) * A1I)





 Return
End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubRoutine PipeFlowOUT_LW                                  &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )

!     NdimE: dimensionality of the triangle element,
!     it could be hapen that we use a 2d element in a 3d problem. 
!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
! los dos tendrian que ir en ElemLib, porque se necesita
! la dimension del elemento, No esta implicito en el tipo de elemento
!  usado, aunque esto hace que no se pueda usar el mismo codigo
! para 2D y 3D
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (	NGP = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP= nint(CommonPar(4))
      Theta  = CommonPar(5)
      iSwPF  = nint(CommonPar(6))
      Theta1= 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do

      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  7 )
      Ep0          = Param (  8 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref= (ArefD - ArefI) / DX
      DxAlfa= (AlfaD - AlfaI) / DX
      roCo2I = Elas*AlfaI/ArefI/2.0d0 
      roCo2D = Elas*AlfaD/ArefD/2.0d0 
      DxroCo2 = (roCo2D - roCo2I) / DX


!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1
      NodG    = NodP

      ipQI  = 1
      ipPI  = ipQI + iShifTP  
      ipAI  = ipPI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTP
      ipAD  = ipPD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )

      APunI = (A1I-A0I) / Dtm
      APunD = (A1D-A0D) / Dtm

      PvI   = PVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
      PvD   = PVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      PRE1I = PRE1I + PvI
!      PRE1D = PRE1D + PvD
!      Pv0I   = P0I*DPnor - PRE0I 
!      Pv0D   = P0D*DPnor - PRE0D 
!      if (Dabs(APunI/A1I)+Dabs(APunI/A1I) .GT. 1.d-8/Dtm )then
!      DtPv   = .5d0*(PvI+PvD-Pv0I-Pv0D)/Dtm
!	DxPv   = .5d0*((Pv0D-Pv0I)+(PvD-PvI))/Dx
!      else
!      DtPv   = .0d0
!	DxPv   = .0d0
!	endif
      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )


      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)


      rMachI= Dabs((Fmas1I+Fmen1I)/(Fmas1I-Fmen1I))
      rMachD= Dabs((Fmas1D+Fmen1D)/(Fmas1D-Fmen1D))

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)


      DPA1 = 0.5d0* (DPA1I+DPA1D)
      DPA0 = 0.5d0* (DPA0I+DPA0D)
!   OOOOJJJJOOO normalizacion de la presion !!!!!!!!!!!!!
!      DPA1  = 0.5d0* ( DPA1 + DPA0 )/DPnor
      DPA1  = 0.5d0* ( DPA1 + DPA0 )
!      Fmas1 = 0.5d0* (Fmas1 + Fmas0)
!      Fmen1 = 0.5d0* (Fmen1 + Fmen0)
	Pv = 0.5d0*(PvI+PvD)
	CpPvPi= Cp*(Pv-Pinf)


      FrI  = Friction (Vel0I,A0I,Vis)
      FrD  = Friction (Vel0D,A0D,Vis)
      FKvisI= FrI/4.0d0*Dsqrt( A0I*PI)*Dabs(Vel0I)/A0I
      FKvisD= FrD/4.0d0*Dsqrt( A0D*PI)*Dabs(Vel0D)/A0D
      FvisI = - FKvisI*Q0I
      FvisD = - FKvisD*Q0D
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)

      FWallvis= - .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX
      GI  =  FvisI + Gravi*A1I + FWallvis
      GD  =  FvisD + Gravi*A1D + FWallvis

      Ges   = GI  + ( GD-GI )/DX * (DX-Fmas1*Dtm) 
      Qes   = Q0I + (Q0D-Q0I)/DX * (DX-Fmas1*Dtm) 
      Aes   = A0I + (A0D-A0I)/DX * (DX-Fmas1*Dtm)
      GG    = 0.5d0*(Ges+GD)
    i = 2
    ipRow  = (i-1)*iDofT
    ipRowQ = ipRow + 1
    ipRowP = ipRowQ + iShifTP
    ipRowA = ipRowP - 1

         BE( ipRowQ          ) =  GG*Dtm + Qes - Fmen1*Aes
         AE( ipRowQ , ipRowQ ) =  1.0d0 
         AE( ipRowQ , ipRowA ) = -Fmen1

!        BE( ipRowA          ) =  GG*Dtm + Qes - Fmas1*Aes
!        AE( ipRowA , ipRowQ ) =  1.0d0 
!        AE( ipRowA , ipRowA ) = -Fmas1
      AE (ipRowA,ipRowP) =    1.0D0
!      AE (ipRowA,ipRowA) = -(DPA1D +1.0d0* DPVAD)/DPnor
      AE (ipRowA,ipRowA) = -(DPA1D +1.0d0* DPVAD)
!      BE (ipRowA)        =  (PRE1D - (DPA1D +1.0d0* DPVAD) * A1D)/DPnor
      BE (ipRowA)        =  (PRE1D - (DPA1D +1.0d0* DPVAD) * A1D)



 Return
End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine PipeFlow1D_LW                              &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,     &
     &             Sol0,Sol1,CommonPar,                     &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!      Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 , C3s2=1.5d0,C2s3=2.d0/3.d0 )
! con iSwP se elige la forma de la relacion P(A)
      Parameter (CLap = .0d0 )

!     NdimE: dimensionality of the triangle element,
!     it could be hapen that we use a 2d element in a 3d problem. 
!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
! los dos tendrian que ir en ElemLib, porque se necesita
! la dimension del elemento, No esta implicito en el tipo de elemento
!  usado, aunque esto hace que no se pueda usar el mismo codigo
! para 2D y 3D
!     Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (	NGP = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                  &
     &  XLL(*),Sol0(*),Sol1(*),                                       &
     &  Param(*), JParam(*),CommonPar(*),                             &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                          &  
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL),  &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
    Ro      = CommonPar(1)
    Rot     = Ro/Dtm
    Vis     = CommonPar(2)/Ro
    Profile = CommonPar(3)
    iShiftP = nint(CommonPar(4))
    Theta   = CommonPar(5)
	iSwP    = CommonPar(6)
    Theta1  = 1.0d0-Theta 
	Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do

      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  7 )
      Ep0          = Param (  8 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref= (ArefD - ArefI) / DX
      DxAlfa= (AlfaD - AlfaI) / DX
      roCo2I = Elas*AlfaI/ArefI/2.0d0 
      roCo2D = Elas*AlfaD/ArefD/2.0d0 
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
       iBU=0

!     NodG = number of geometric nodes
!     NodP = number of pressure nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTP  
      ipAI  = ipPI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTP
      ipAD  = ipPD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPArI = DPEFA ( ArefI,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwP )
      DPArD = DPEFA ( ArefD,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwP )

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwP )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwP )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwP )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwP )

      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwP )
      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwP )
      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwP )
      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwP )

      APunI = (A1I-A0I) / Dtm
      APunD = (A1D-A0D) / Dtm

      PvI   = PVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
      PvD   = PVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      PRE1I = PRE1I + PvI
!      PRE1D = PRE1D + PvD
!      Pv0I   = P0I*DPnor - PRE0I 
!      Pv0D   = P0D*DPnor - PRE0D 
!      if (Dabs(APunI/A1I)+Dabs(APunI/A1I) .GT. 1.d-8/Dtm )then
!      DtPv   = .5d0*(PvI+PvD-Pv0I-Pv0D)/Dtm
!	DxPv   = .5d0*((Pv0D-Pv0I)+(PvD-PvI))/Dx
!      else
!      DtPv   = .0d0
!	DxPv   = .0d0
!	endif
      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      C0cI = A0IsRo*DPA0I
      C0cD = A0DsRo*DPA0D
      C1cI = A1IsRo*DPA1I
      C1cD = A1DsRo*DPA1D

      C0r2I= ArefI*DPArI/Ro
      C0r2D= ArefD*DPArD/Ro

!      PotAI = C2s3*ArefI* C0r2I*( (A1I/ArefI)**C3s2 - 1 )
!      PotAD = C2s3*ArefD* C0r2D*( (A1D/ArefD)**C3s2 - 1 )
!   OJO EstoFunciona con Relacion P(D) lineal
      PotAI = C2s3*ArefI* C0r2I*( (A0I/ArefI)**C3s2 - 1.d0 )
      PotAD = C2s3*ArefD* C0r2D*( (A0D/ArefD)**C3s2 - 1.d0 )
      PotA  = .5d0*(PotAI+PotAD)

      Q2AI = Profile*Q0I*Q0I/A0I
      Q2AD = Profile*Q0D*Q0D/A0D
      Q2A  = .5d0*(Q2AI+Q2AD)

      DQ0X = (Q0D - Q0I  )/DX
      DA0X = (A0D - A0I  )/DX
      DFAX = DQ0X
      DFQX = (Q2AD - Q2AI  +   PotAD -PotAI  )/DX
      DFAA = 0.0d0   
      DFAQ = 1.0d0
      DFQAi= -Q2AI/A0I + C0cI 
      DFQAd= -Q2AD/A0D + C0cD 
      DFQA = .5d0*(DFQAd+DFQAi)  
      DFQQi= 2.d0*Q0I/A0I 
      DFQQd= 2.d0*Q0D/A0D 
      DFQQ = .5d0*(DFQQd+DFQQi)  

!      RoCsAI = Fmas0I /A0IsRo
!      RoCsAD = Fmas0D /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs((Fmas1I+Fmen1I)/(Fmas1I-Fmen1I))
      rMachD= Dabs((Fmas1D+Fmen1D)/(Fmas1D-Fmen1D))

	  if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)

      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)

      DPA1 = 0.5d0* (DPA1I+DPA1D)
      DPA0 = 0.5d0* (DPA0I+DPA0D)
!	DPvis_men = DtPv + Fmen1*DxPv 
!	DPvis_mas = DtPv + Fmas1*DxPv 
!	GGn =  - Fmas1/DPA1*DPvis_men
!	GGs =  - Fmen1/DPA1*DPvis_mas

!   OOOOJJJJOOO normalizacion de la presion !!!!!!!!!!!!!
!      DPA1  = 0.5d0* ( DPA1 + DPA0 )/DPnor
      DPA1  = 0.5d0* ( DPA1 + DPA0 )
!      Fmas1 = 0.5d0* (Fmas1 + Fmas0)
!      Fmen1 = 0.5d0* (Fmen1 + Fmen0)
!      Pv = 0.5d0*(PvI+PvD)
!      CpPvPi= Cp*(Pv-Pinf)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
	  if ( CourantFmen.ge.1 .or. CourantFmas.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'Courant NUMBER IS REACHING UNITY'
        write(*,*)'Instabilities can occur'
      ENDIF

!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
!    Upmen= ( UpLs2/(CourantFmen*Theta) )**Ls2
!    Upmas= ( UpLs2/(CourantFmas*Theta) )**Ls2


      FrI  = Friction (Vel0I,A0I,Vis)
      FrD  = Friction (Vel0D,A0D,Vis)
      FKvisI= FrI/4.0d0*Dsqrt( A0I*PI)*Dabs(Vel0I)/A0I
      FKvisD= FrD/4.0d0*Dsqrt( A0D*PI)*Dabs(Vel0D)/A0D
      FvisI = - FKvisI*Q0I
      FvisD = - FKvisD*Q0D
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)

      FWallvis= - .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX
      GI  =  FvisI + Gravi*A1I + FWallvis
      GD  =  FvisD + Gravi*A1D + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

    GG  = 0.5d0 * ( GI + GD )
    GGmen = GG 
    GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

!2nd member of LAxWendroff
     DProCo2I= (Pre0I-Pref)/roCo2I*DxroCo2  
     DProCo2D= (Pre0D-Pref)/roCo2D*DxroCo2  
     
     DPArefI= DPAref ( A0I,ArefI,RoCo2,ELas,ELCO,Ep0,Epr,iSwP )*DxAref  
     DPArefD= DPAref ( A0D,ArefD,RoCo2,ELas,ELCO,Ep0,Epr,iSwP )*DxAref  
     BQi= - GI + A0IsRo*(DProCo2I+DPArefI)
     BQd= - GD + A0DsRo*(DProCo2D+DPArefD)
     DBQx= (BQd-BQi)/DX
     DFQQxBQX = (DFQQd*BQd-DFQQi*BQi)/Dx

     DBQAi = -FKvisI/A0I + (DProCo2I+DPArefI)/Ro + .5d0*A0IsRo*(DPArefI/A0I + 1.d0/Dsqrt (A0I * ArefI) )
     DBQAd = -FKvisD/A0D + (DProCo2D+DPArefD)/Ro + .5d0*A0DsRo*(DPArefD/A0D + 1.d0/Dsqrt (A0D * ArefD) )
     DBQA  = .5d0*(DBQAi+DBQAd)

     DBQQi =  FKvisI  
     DBQQd =  FKvisD
     DBQQ  = .5d0*(DBQQi+DBQQd)



!      Fmas1_DPA1 = Fmas1/DPA1
!      Fmen1_DPA1 = Fmen1/DPA1
!      Fmas1xCpPvPi= Fmas1*CpPvPi
!      Fmen1xCpPvPi= Fmen1*CpPvPi
! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          


    Det = Dx
    dFi1_G (1,1) = - 1.0d0/DX
    dFi1_G (1,2) =   1.0d0/DX
 

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------
!	Q0=0.0d0
!	Q1=0.0d0
!	Pre0=0.0d0
!	Pre1=0.0d0

!     dFi1_L = first derivatives with respect to intrinsic Local coordinates
!              refers to linear shape functions
!     dFi1_G = first derivatives with respect to Global coordinates
!     dFi2_L = first derivatives with respect to intrinsic Local coordinates
!              refers to Cuadratic shape functions

!      Call LocalShapeDer(NodG+iBu,NdimE,dFi1_L,PSI(1,nG),1,iBu)
!      Call LocalShapeDer(NodEL,NdimE,dFi1_L,PSI(1,nG),1,iBu)
!      Call LocalShapeDer(NodEL,NdimE,dFi2_L,PSI(1,nG),2,iBu)


!	Det = 0.0d0
!      Do nci=1, Ndim
!        Do Nod=1, NodEL
!                        XL( Nod )= XLL( (Nod-1)*Ndim + nci)      
!        end do
!            Call Jacobian(Jac,Det1,NdimE,NodG,XL,dFi2_L)
!            Det = Det + Det1*Det1
!      end do
!	Det = Dsqrt ( Det )
!	Jac(1,1)=1.0d0/Det
!      Call GlobalShapeDer (NodEL,NdimE,dFi1_L,dFi1_G,Jac)

!	Det = Dx
!	dFi1_G (1,1) = - 1.0d0/DX
!	dFi1_G (1,2) =   1.0d0/DX


      Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!      Call ShapeF (NodEL,NdimE,Fi2,PSI(1,nG),2,iBu)

!      Do NodQ=1, NodEL
!	ipNodQ = (NodQ-1)*iDofT + 1
!        Q0 = Q0 + Sol0( ipNodQ )*Fi1(NodQ)
!        Q1 = Q1 + Sol1( ipNodQ )*Fi1(NodQ)
!	ipNodP = ipNodQ + Ndim
!        Pre0   = Pre0   + Sol0( ipNodP )*Fi1(NodQ)
!        Pre1   = Pre1   + Sol1( ipNodP )*Fi1(NodQ)
!      Enddo

      DetW=Det*W(nG)
      BA = 0.0d0
      BQ = BQi*Fi1(1)+BQd*Fi1(2)  

      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowQ = ipRow + 1
        ipRowP = ipRowQ + iShifTP
        ipRowA = ipRowP - 1
        Fi1i  = Fi1(i)
        dFi1i = dFi1_G ( 1 , i )

!	  dResMenQ_i = Fi1i/Dtm + Upmen * (Fmen1 * theta * dFi1i)
!	  dResMasQ_i = Fi1i/Dtm + Upmas * (Fmas1 * theta * dFi1i)
!	  dResMenP_i = -(Fmas1_DPA1*dResMenQ_i + Fmas1* Cp1* theta *Fi1i)
!	  dResMasP_i = -(Fmen1_DPA1*dResMasQ_i + Fmen1* Cp1* theta *Fi1i)
!	  dResMenP_i = -(Fmas1_DPA1*dResMenQ_i )
!	  dResMasP_i = -(Fmen1_DPA1*dResMasQ_i )

!	  dResMenQ_i = dResMenQ_i * DetW
!	  dResMasQ_i = dResMasQ_i * DetW
!	  dResMenP_i = dResMenP_i * DetW
!	  dResMasP_i = dResMasP_i * DetW


         BE( ipRowA ) = BE( ipRowA ) + DetW * (     & 
           -( DFAx + BA )*Fi1i  *Dtm                &
           +(-DFQx*dFi1i + DBQx*Fi1i)*Dtm*Dtm*.5d0    )
         BE( ipRowQ ) = BE( ipRowQ ) + DetW * (     &
           -( DFQx + BQ )*Fi1i  *Dtm                &
           +(-(DFQA*DFAx+DFQQ*DFQx)*dFi1i + (DFQQxBQX+DBQA*DFAx+DBQQ*(DFQx+BQ) )*Fi1i )*Dtm*Dtm*.5d0   )

        LoopCol: Do j=1,NodEL
         ipCol    = (j-1)*iDofT 
         ipColQ   = ipCol + 1
         ipColP   = ipColQ + iShifTP
         ipColA   = ipColP - 1

         dFi1j    = dFi1_G ( 1 , j )
         Fi1j     = Fi1(j)
!         Fi1j_Dtm = Fi1j/Dtm
!         Fmen1xdFi1j = Fmen1*dFi1j
!         Fmas1xdFi1j = Fmas1*dFi1j

! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
         rMij = Fi1i*Fi1j*DetW   
         AE(ipRowQ , ipColQ)= AE(ipRowQ , ipColQ) + rMij 
         AE(ipRowA , ipColA)= AE(ipRowA , ipColA) + rMij 

         BE( ipRowQ ) = BE( ipRowQ )+ rMij*Sol0(ipColQ)
         BE( ipRowA ) = BE( ipRowA )+ rMij*Sol0(ipColA)

! Lapidus Shock capturing
!         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)+rLap*dFi1i*dFi1j*DetW  


        End do LoopCol

!-----------------------------------------------
      End do LoopRow


!--------------------------
      EndDo LoopGaussPoints
!--------------------------


!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!      LoopArea: Do i=1,NodEL
	i = 1
        ipRowQ  = (i-1)*iDofT + 1
        ipRowP = ipRowQ + iShifTP
        ipColA = ipRowP - 1
      AE (ipRowP,ipRowP) =    1.0D0
!      AE (ipRowP,ipColA) = -(DPA1I +1.0d0* DPVAI)/DPnor
      AE (ipRowP,ipColA) = -(DPA1I +1.0d0* DPVAI)
!      BE (ipRowP)        =  (PRE1I - (DPA1I +1.0d0* DPVAI) * A1I)/DPnor 
      BE (ipRowP)        =  (PRE1I - (DPA1I +1.0d0* DPVAI) * A1I)

	i = 2
        ipRowQ  = (i-1)*iDofT + 1
        ipRowP = ipRowQ + iShifTP
        ipColA = ipRowP - 1
      AE (ipRowP,ipRowP) =    1.0D0
!      AE (ipRowP,ipColA) = -(DPA1D +1.0d0* DPVAD)/DPnor
!      BE (ipRowP)        =  (PRE1D - (DPA1D +1.0d0* DPVAD) * A1D)/DPnor
      AE (ipRowP,ipColA) = -(DPA1D +1.0d0* DPVAD)
      BE (ipRowP)        =  (PRE1D - (DPA1D +1.0d0* DPVAD) * A1D)

!      End do LoopArea
!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End

