!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  Elemento Tipo LS sobre caracteristicas en Q,A  viscoeleastico
!  DOfs = Q,Pe,A,Pt
!  ID = 9
      SubRoutine PipeFlow1D_QPeAPt                        &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!     Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 )
      Parameter (CLap = .0d0 )

!  iSwPF = 2  Controla la forma de P(A)

!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NGP   = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP = nint(CommonPar(4))
      Theta   = CommonPar(5)
      iSwPF   = nint(CommonPar(6))
      Theta1  = 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do
      
      iShiftPe = 1
      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )  !Alfa= pi . Ro . Ho
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  8 )
      Ep0          = Param (  7 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref  = (ArefD - ArefI) / DX
      DxAlfa  = (AlfaD - AlfaI) / DX
      roCo2I  = Elas*AlfaI/ArefI/2.0d0 
      roCo2D  = Elas*AlfaD/ArefD/2.0d0 
      roCo2   = (roCo2I+roCo2D)/2.0d0
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
	   iBU=0

!     NodG = number of geometric nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTPe  
      ipPtI = ipQI + iShifTP
      ipAI  = ipPtI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTPe
      ipPtD = ipQD + iShifTP
      ipAD  = ipPtD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Pt0I  = Sol0 ( ipPtI)
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )
      Pt0D  = Sol0 ( ipPtD)

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Pt1I  = Sol1 ( ipPtI)
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )
      Pt1D  = Sol1 ( ipPtD)


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

!      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      DPAC0I = DPAref ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC0D = DPAref ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1I = DPAref ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1D = DPAref ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )



	  DQx   = (Q0D-Q0I) / DX
	  APunI = (A1I-A0I) / Dtm
	  APunD = (A1D-A0D) / Dtm

	  CPvI   = Cvis( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
	  CPvD   = Cvis( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
!      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      RoCsAI = DsqrT(A0IsRo*DPA0I) /A0IsRo
      RoCsAD = DsqrT(A0DsRo*DPA0D) /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs( (Fmas1I+Fmen1I)/(Fmas1I-Fmen1I) )
      rMachD= Dabs( (Fmas1D+Fmen1D)/(Fmas1D-Fmen1D) )

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)
      A1sRo = A1/Ro


      DPA1 = 0.5d0* (DPA1I + DPA1D)
      DPA0 = 0.5d0* (DPA0I + DPA0D)
      DPA1 = 0.5d0* (DPA1  + DPA0 )

      DPACI= 0.5d0* (DPAC0I+DPAC1I)
      DPACD= 0.5d0* (DPAC0D+DPAC1D)
!      DPAC1= 0.5d0* (DPAC1 + DPAC0)


!	Pv = 0.5d0*(PvI+PvD)
!	CpPvPi= Cp*(Pv-Pinf)
      CpPvPi= Cp*(-Pinf)
      CPv   =.5d0*(CPvI+CpvD)
      Betav= CPv*Cp  
      Betav= Betav / (1.0d0 + Betav)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
    if (Ls2 > 0) then
!            Upmen= UpLs2/(CourantFmen*Theta)
!            Upmas= UpLs2/(CourantFmas*Theta)
!            UpmenTheta = UpLs2 / CourantFmen * DTm
!            UpmasTheta = UpLs2 / CourantFmas * DTm
            UpmenTheta = UpLs2 * DX / dabs(Fmen1)
            UpmasTheta = UpLs2 * DX / dabs(Fmas1)
    else
            UpmenTheta = Theta * DTm  
            UpmasTheta = Theta * DTm
    endif 

      Fr  = Friction (Vel1,A1,Vis)
      Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)*Vel1
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)


!!!!!!!!   OJJJJOOOOOOO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FWallvis= 0.0d0  !- .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX

      DProCo2I = (.5d0*(P0I+P1I) - Pref)/roCo2I  
      DProCo2D = (.5d0*(P0D+P1D) - Pref)/roCo2D  

      GI  =  Fvis + Gravi*A1I - .5d0*(A0IsRo+A1IsRo)*(DPACI*DxAref + DProCo2I*DxroCo2) + FWallvis 
      GD  =  Fvis + Gravi*A1D - .5d0*(A0DsRo+A1DsRo)*(DPACD*DxAref + DProCo2D*DxroCo2) + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

      GG  = 0.5d0 * ( GI + GD )
      GGmen = GG 
      GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

!      Fmas1_DPA1 = Fmas1/DPA1
!      Fmen1_DPA1 = Fmen1/DPA1
      Fmas1xCpPvPi= Fmas1*CpPvPi
      Fmen1xCpPvPi= Fmen1*CpPvPi

! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------

	Det = Dx
	dFi1_G (1,1) = - 1.0d0/DX
	dFi1_G (1,2) =   1.0d0/DX


      Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!      Call ShapeF (NodEL,NdimE,Fi2,PSI(1,nG),2,iBu)

!      Do NodQ=1, NodEL
!	ipNodQ = (NodQ-1)*iDofT + 1
!        Q0 = Q0 + Sol0( ipNodQ )*Fi1(NodQ)
!        Q1 = Q1 + Sol1( ipNodQ )*Fi1(NodQ)
!	ipNodP = ipNodQ + Ndim
!        Pre0   = Pre0   + Sol0( ipNodP )*Fi1(NodQ)
!        Pre1   = Pre1   + Sol1( ipNodP )*Fi1(NodQ)
!      Enddo

      DetW=Det*W(nG)

      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowQ = ipRow + 1
        ipRowP = ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        ipRowA = ipRowPt - 1

        Fi1i  = Fi1(i)
        dFi1i = dFi1_G ( 1 , i )

        dResMenQ_i = Fi1i  + UpmenTheta * Fmen1 * dFi1i
        dResMasQ_i = Fi1i  + UpmasTheta * Fmas1 * dFi1i

	    dResMenA_i = -(Fmas1*dResMenQ_i )
	    dResMasA_i = -(Fmen1*dResMasQ_i )

        dResMenQ_i = dResMenQ_i * DetW
        dResMasQ_i = dResMasQ_i * DetW
        dResMenA_i = dResMenA_i * DetW
        dResMasA_i = dResMasA_i * DetW


         BE( ipRowQ ) = BE( ipRowQ )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenQ_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasQ_i 
         BE( ipRowA ) = BE( ipRowA )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenA_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasA_i 


        LoopCol: Do j=1,NodEL
         ipCol    = (j-1)*iDofT 
         ipColQ   = ipCol + 1
         ipColP   = ipColQ + iShifTPe
         ipColPt  = ipColQ + iShifTP
         ipColA   = ipColPt - 1

         dFi1j    = dFi1_G ( 1 , j )
         Fi1j     = Fi1(j)
         Fi1j_Dtm = Fi1j/Dtm
         Fmen1xdFi1j = Fmen1*dFi1j
         Fmas1xdFi1j = Fmas1*dFi1j

         CoefMen_j  = Fi1j_Dtm + Fmen1xdFi1j*Theta
         CoefMas_j  = Fi1j_Dtm + Fmas1xdFi1j*Theta
         
          Cp1xThetaxFi1j       = Cp1*Theta*Fi1j
          Fmas1xCp1xThetaxFi1j = Fmas1*Cp1xThetaxFi1j
          Fmen1xCp1xThetaxFi1j = Fmen1*Cp1xThetaxFi1j
          VisDPe_j             = A1sRo*Betav*dFi1j
          VisDQ_j              = A1sRo*(1.0d0-Betav)*CPv*dFi1j* 2.0d0* DetW

! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
         AE(ipRowQ , ipColQ)= AE(ipRowQ , ipColQ) + CoefMen_j * dResMenQ_i + CoefMas_j * dResMasQ_i &
                                                  + Theta*VisDQ_j *dFi1i
!     &      - Fvis *(dResMenQ_i + dResMasQ_i) 


         AE(ipRowQ , ipColA)= AE(ipRowQ , ipColA)                 &
                                  - Fmas1*CoefMen_j * dResMenQ_i  &
                                  - Fmen1*CoefMas_j * dResMasQ_i 

         AE(ipRowQ , ipColP)= AE(ipRowQ , ipColP)                     &
                              - VisDPe_j * (dResMenQ_i + dResMasQ_i)*Theta  


         AE(ipRowQ ,ipColPt)= AE(ipRowQ , ipColPt)                   &
                                - Fmas1xCp1xThetaxFi1j * dResMenQ_i  &
                                - Fmen1xCp1xThetaxFi1j * dResMasQ_i   

         AE(ipRowA , ipColA)= AE(ipRowA , ipColA)                 &
                                  - Fmas1*CoefMen_j * dResMenA_i  &
                                  - Fmen1*CoefMas_j * dResMasA_i

         AE(ipRowA , ipColP)= AE(ipRowA , ipColP)                 &
                              - VisDPe_j * (dResMenA_i + dResMasA_i)*Theta  

         AE(ipRowA ,ipColPt)= AE(ipRowA , ipColPt)                   &
                                - Fmas1xCp1xThetaxFi1j * dResMenA_i  &
                                - Fmen1xCp1xThetaxFi1j * dResMasA_i  

         AE(ipRowA , ipColQ)= AE(ipRowA , ipColQ) + CoefMen_j * dResMenA_i + CoefMas_j * dResMasA_i &
                                                  + Theta*VisDQ_j*dFi1i*(-Vel1)
!     &      - Fvis *(dResMenA_i + dResMasA_i) 

	   BEmenj    = Fi1j_Dtm - Fmen1xdFi1j*theta1
	   BEmasj    = Fi1j_Dtm - Fmas1xdFi1j*theta1
       CpTh1Fj   = Cp1*theta1*Fi1j
       VisDPeT1j = VisDPe_j*theta1

       BE1 = BEmenj*Sol0(ipColQ) - (Fmas1*BEmenj)*Sol0(ipColA) +VisDPeT1j*Sol0(ipColP) + Fmas1*CpTh1Fj*Sol0(ipColPt)
       BE2 = BEmasj*Sol0(ipColQ) - (Fmen1*BEmasj)*Sol0(ipColA) +VisDPeT1j*Sol0(ipColP) + Fmen1*CpTh1Fj*Sol0(ipColPt)

       BE( ipRowQ ) = BE( ipRowQ )+ BE1*dResMenQ_i + BE2*dResMasQ_i -        Theta1*VisDQ_j*dFi1i*Sol0(ipColQ)
       BE( ipRowA ) = BE( ipRowA )+ BE1*dResMenA_i + BE2*dResMasA_i -(-Vel1)*Theta1*VisDQ_j*dFi1i*Sol0(ipColQ) 
! Lapidus Shock capturing
!         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)+rLap*dFi1i*dFi1j*DetW  


        End do LoopCol

!-----------------------------------------------
      End do LoopRow


!--------------------------
      EndDo LoopGaussPoints
!--------------------------
!      LoopArea: Do i=1,NodEL
	i = 1
        ipRowQ  = (i-1)*iDofT + 1
        ipColQ  = ipRowQ
        ipRowPe= ipRowQ + iShifTPe
        ipColPe= ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        ipColPt= ipRowPt
        ipRowA = ipRowPt - 1
        ipColA = ipRowA

          AE (ipRowPe,ipColPe) =   1.0D0
          AE (ipRowPe,ipColA)  = - DPA1I
          BE (ipRowPe)         =   P1I - DPA1I * A1I

          AE (ipRowPt,ipColPt) =   1.0D0 + CPvI*Cp
          AE (ipRowPt,ipColPe) =  -1.0D0 

          AE (ipRowPt,ipColQ )       = - CPvI/DX*Theta 
          AE (ipRowPt,ipColQ+iDofT ) =   CPvI/DX*Theta 
          BE (ipRowPt)               = + CPvI*Cp*Pinf - CPvI*Theta1*DQx

	i = 2
        ipRowQ  = (i-1)*iDofT + 1
        ipColQ  = ipRowQ
        ipRowPe= ipRowQ + iShifTPe
        ipColPe= ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        ipColPt= ipRowPt
        ipRowA = ipRowPt - 1
        ipColA = ipRowA

          AE (ipRowPe,ipColPe) =   1.0D0
          AE (ipRowPe,ipColA)  = - DPA1D 
          BE (ipRowPe)         =   P1D - DPA1D * A1D

          AE (ipRowPt,ipColPt)=   1.0D0 + CPvD*Cp
          AE (ipRowPt,ipColPe)=  -1.0D0 

          AE (ipRowPt,ipColQ )       =   CPvD/DX*Theta 
          AE (ipRowPt,ipColQ-iDofT ) = - CPvD/DX*Theta 
          BE (ipRowPt)               = + CPvD*Cp*Pinf- CPvD*Theta1*DQx

!      End do LoopArea






!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine PipeFlow1D_QPeAPtIN                      &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!     Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 )
      Parameter (CLap = .0d0 )

!  iSwPF = 2  Controla la forma de P(A)

!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NGP   = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP = nint(CommonPar(4))
      Theta   = CommonPar(5)
      iSwPF   = nint(CommonPar(6))
      Theta1  = 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do
      
      iShiftPe = 1
      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )  !Alfa= pi . Ro . Ho
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  8 )
      Ep0          = Param (  7 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref  = (ArefD - ArefI) / DX
      DxAlfa  = (AlfaD - AlfaI) / DX
      roCo2I  = Elas*AlfaI/ArefI/2.0d0 
      roCo2D  = Elas*AlfaD/ArefD/2.0d0 
      roCo2   = (roCo2I+roCo2D)/2.0d0
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
	   iBU=0

!     NodG = number of geometric nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTPe  
      ipPtI = ipQI + iShifTP
      ipAI  = ipPtI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTPe
      ipPtD = ipQD + iShifTP
      ipAD  = ipPtD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Pt0I  = Sol0 ( ipPtI)
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )
      Pt0D  = Sol0 ( ipPtD)

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Pt1I  = Sol1 ( ipPtI)
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )
      Pt1D  = Sol1 ( ipPtD)


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

!      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      DPAC0I = DPAref ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC0D = DPAref ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1I = DPAref ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1D = DPAref ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )



	  DQx   = (Q0D-Q0I) / DX
	  APunI = (A1I-A0I) / Dtm
	  APunD = (A1D-A0D) / Dtm

	  CPvI   = Cvis( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
	  CPvD   = Cvis( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
!      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      RoCsAI = DsqrT(A0IsRo*DPA0I) /A0IsRo
      RoCsAD = DsqrT(A0DsRo*DPA0D) /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs( (Fmas1I+Fmen1I)/(Fmas1I-Fmen1I) )
      rMachD= Dabs( (Fmas1D+Fmen1D)/(Fmas1D-Fmen1D) )

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)
      A1sRo = A1/Ro


      DPA1 = 0.5d0* (DPA1I + DPA1D)
      DPA0 = 0.5d0* (DPA0I + DPA0D)
      DPA1 = 0.5d0* (DPA1  + DPA0 )

      DPACI= 0.5d0* (DPAC0I+DPAC1I)
      DPACD= 0.5d0* (DPAC0D+DPAC1D)
!      DPAC1= 0.5d0* (DPAC1 + DPAC0)


!	Pv = 0.5d0*(PvI+PvD)
!	CpPvPi= Cp*(Pv-Pinf)
      CpPvPi= Cp*(-Pinf)
      CPv   =.5d0*(CPvI+CpvD)
      Betav= CPv*Cp  
      Betav= Betav / (1.0d0 + Betav)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
    if (Ls2 > 0) then
!            Upmen= UpLs2/(CourantFmen*Theta)
!            Upmas= UpLs2/(CourantFmas*Theta)
!            UpmenTheta = UpLs2 / CourantFmen * DTm
!            UpmasTheta = UpLs2 / CourantFmas * DTm
            UpmenTheta = UpLs2 * DX / dabs(Fmen1)
            UpmasTheta = UpLs2 * DX / dabs(Fmas1)
    else
            UpmenTheta = Theta * DTm  
            UpmasTheta = Theta * DTm
    endif 

      Fr  = Friction (Vel1,A1,Vis)
      Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)*Vel1
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)


!!!!!!!!   OJJJJOOOOOOO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FWallvis= 0.0d0  !- .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX

      DProCo2I = (.5d0*(P0I+P1I) - Pref)/roCo2I  
      DProCo2D = (.5d0*(P0D+P1D) - Pref)/roCo2D  

      GI  =  Fvis + Gravi*A1I - .5d0*(A0IsRo+A1IsRo)*(DPACI*DxAref + DProCo2I*DxroCo2) + FWallvis 
      GD  =  Fvis + Gravi*A1D - .5d0*(A0DsRo+A1DsRo)*(DPACD*DxAref + DProCo2D*DxroCo2) + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

      GG  = 0.5d0 * ( GI + GD )
      GGmen = GG 
      GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

!      Fmas1_DPA1 = Fmas1/DPA1
!      Fmen1_DPA1 = Fmen1/DPA1
      Fmas1xCpPvPi= Fmas1*CpPvPi
      Fmen1xCpPvPi= Fmen1*CpPvPi

! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          

!      LoopArea: Do i=1,NodEL
	i = 1
        ipRowQ  = (i-1)*iDofT + 1
        ipColQ  = ipRowQ
        ipRowPe = ipRowQ + iShifTPe
        ipColPe = ipRowPe
        ipRowPt = ipRowQ + iShifTP
        ipColPt = ipRowPt
        ipRowA  = ipRowPt - 1

          AE (ipRowA,ipColPe) =   1.0D0
          AE (ipRowA,ipRowA)  = - DPA1I
          BE (ipRowA)         =   P1I - DPA1I * A1I

          AE (ipRowPe,ipColPt) =   1.0D0 + CPvI*Cp
          AE (ipRowPe,ipColPe) =  -1.0D0 
          AE (ipRowPe,ipColQ )       = - CPvI/DX *Theta
          AE (ipRowPe,ipColQ+iDofT ) =   CPvI/DX *Theta
          BE (ipRowPe)               = + CPvI*Cp*Pinf- CPvI*Theta1*DQx

!	i = 2
!        ipRowQ  = (i-1)*iDofT + 1
!        ipColQ  = ipRowQ
!        ipRowPe = ipRowQ + iShifTPe
!        ipColPe = ipRowPe
!        ipRowPt = ipRowQ + iShifTP
!        ipColPt = ipRowPt
!        ipRowA  = ipRowPt - 1

!          AE (ipRowA,ipColPe) =   1.0D0
!          AE (ipRowA,ipRowA)  = - DPA1D 
!          BE (ipRowA)         =   P1D - DPA1D * A1D

!          AE (ipRowPe,ipColPt)=   1.0D0 + CPvD*Cp
!          AE (ipRowPe,ipColPe)=  -1.0D0 

!          AE (ipRowPe,ipColQ )       =   CPvD/DX 
!          AE (ipRowPe,ipColQ-iDofT ) = - CPvD/DX 
!          BE (ipRowPe)               = + CPvD*Cp*Pinf

!      End do LoopArea






!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine PipeFlow1D_QPeAPtOUT                     &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!     Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 )
      Parameter (CLap = .0d0 )

!  iSwPF = 2  Controla la forma de P(A)

!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NGP   = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP = nint(CommonPar(4))
      Theta   = CommonPar(5)
      iSwPF   = nint(CommonPar(6))
      Theta1  = 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do
      
      iShiftPe = 1
      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )  !Alfa= pi . Ro . Ho
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  8 )
      Ep0          = Param (  7 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref  = (ArefD - ArefI) / DX
      DxAlfa  = (AlfaD - AlfaI) / DX
      roCo2I  = Elas*AlfaI/ArefI/2.0d0 
      roCo2D  = Elas*AlfaD/ArefD/2.0d0 
      roCo2   = (roCo2I+roCo2D)/2.0d0
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
	   iBU=0

!     NodG = number of geometric nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTPe  
      ipPtI = ipQI + iShifTP
      ipAI  = ipPtI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTPe
      ipPtD = ipQD + iShifTP
      ipAD  = ipPtD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Pt0I  = Sol0 ( ipPtI)
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )
      Pt0D  = Sol0 ( ipPtD)

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Pt1I  = Sol1 ( ipPtI)
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )
      Pt1D  = Sol1 ( ipPtD)


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

!      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
      DPAC0I = DPAref ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC0D = DPAref ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1I = DPAref ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPAC1D = DPAref ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )



	  DQx   = (Q0D-Q0I) / DX
	  APunI = (A1I-A0I) / Dtm
	  APunD = (A1D-A0D) / Dtm

	  CPvI   = Cvis( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
	  CPvD   = Cvis( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
!      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      RoCsAI = DsqrT(A0IsRo*DPA0I) /A0IsRo
      RoCsAD = DsqrT(A0DsRo*DPA0D) /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs( (Fmas1I+Fmen1I)/(Fmas1I-Fmen1I) )
      rMachD= Dabs( (Fmas1D+Fmen1D)/(Fmas1D-Fmen1D) )

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)
      A1sRo = A1/Ro


      DPA1 = 0.5d0* (DPA1I + DPA1D)
      DPA0 = 0.5d0* (DPA0I + DPA0D)
      DPA1 = 0.5d0* (DPA1  + DPA0 )

      DPACI= 0.5d0* (DPAC0I+DPAC1I)
      DPACD= 0.5d0* (DPAC0D+DPAC1D)
!      DPAC1= 0.5d0* (DPAC1 + DPAC0)


!	Pv = 0.5d0*(PvI+PvD)
!	CpPvPi= Cp*(Pv-Pinf)
      CpPvPi= Cp*(-Pinf)
      CPv   =.5d0*(CPvI+CpvD)
      Betav= CPv*Cp  
      Betav= Betav / (1.0d0 + Betav)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
    if (Ls2 > 0) then
!            Upmen= UpLs2/(CourantFmen*Theta)
!            Upmas= UpLs2/(CourantFmas*Theta)
!            UpmenTheta = UpLs2 / CourantFmen * DTm
!            UpmasTheta = UpLs2 / CourantFmas * DTm
            UpmenTheta = UpLs2 * DX / dabs(Fmen1)
            UpmasTheta = UpLs2 * DX / dabs(Fmas1)
    else
            UpmenTheta = Theta * DTm  
            UpmasTheta = Theta * DTm
    endif 

      Fr  = Friction (Vel1,A1,Vis)
      Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)*Vel1
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)


!!!!!!!!   OJJJJOOOOOOO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FWallvis= 0.0d0  !- .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX

      DProCo2I = (.5d0*(P0I+P1I) - Pref)/roCo2I  
      DProCo2D = (.5d0*(P0D+P1D) - Pref)/roCo2D  

      GI  =  Fvis + Gravi*A1I - .5d0*(A0IsRo+A1IsRo)*(DPACI*DxAref + DProCo2I*DxroCo2) + FWallvis 
      GD  =  Fvis + Gravi*A1D - .5d0*(A0DsRo+A1DsRo)*(DPACD*DxAref + DProCo2D*DxroCo2) + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

      GG  = 0.5d0 * ( GI + GD )
      GGmen = GG 
      GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

!      Fmas1_DPA1 = Fmas1/DPA1
!      Fmen1_DPA1 = Fmen1/DPA1
      Fmas1xCpPvPi= Fmas1*CpPvPi
      Fmen1xCpPvPi= Fmen1*CpPvPi

! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          

!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRowQ  = (i-1)*iDofT + 1
!        ipColQ  = ipRowQ
!        ipRowPe = ipRowQ + iShifTPe
!        ipColPe = ipRowPe
!        ipRowPt = ipRowQ + iShifTP
!        ipColPt = ipRowPt
!        ipRowA  = ipRowPt - 1

!          AE (ipRowA,ipColPe) =   1.0D0
!          AE (ipRowA,ipRowA)  = - DPA1I
!          BE (ipRowA)         =   P1I - DPA1I * A1I

!          AE (ipRowPe,ipColPt) =   1.0D0 + CPvI*Cp
!          AE (ipRowPe,ipColPe) =  -1.0D0 
!          AE (ipRowPe,ipColQ )       = - CPvI/DX 
!          AE (ipRowPe,ipColQ+iDofT ) =   CPvI/DX 
!          BE (ipRowPe)               = + CPvI*Cp*Pinf

	i = 2
        ipRowQ  = (i-1)*iDofT + 1
        ipColQ  = ipRowQ
        ipRowPe = ipRowQ + iShifTPe
        ipColPe = ipRowPe
        ipRowPt = ipRowQ + iShifTP
        ipColPt = ipRowPt
        ipRowA  = ipRowPt - 1

          AE (ipRowA,ipColPe) =   1.0D0
          AE (ipRowA,ipRowA)  = - DPA1D 
          BE (ipRowA)         =   P1D - DPA1D * A1D

          AE (ipRowPe,ipColPt)=   1.0D0 + CPvD*Cp
          AE (ipRowPe,ipColPe)=  -1.0D0 

          AE (ipRowPe,ipColQ )       =   CPvD/DX *Theta
          AE (ipRowPe,ipColQ-iDofT ) = - CPvD/DX *Theta
          BE (ipRowPe)               = + CPvD*Cp*Pinf- CPvD*Theta1*DQx

!      End do LoopArea






!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End






!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubRoutine PipeFlow1D_QPPe_Conec( AE,BE,MaxLRows,XL,NDim,iDofT,  &
     &   NodEl,Sol0,Sol1,CommonPar,Param,JParam,DelT,DTm,Time )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION AE(MaxLRows,*), BE(*), XL(*),Sol0(*),Sol1(*), &
     & Param(*), JParam(*),CommonPar(*),iFlowDir(NodEl)

      Parameter (PI = 3.141592653589793238D0)
      Parameter (C3 = 0.333333333333333333D0)
      Parameter (C6 = 0.166666666666666667D0)
!      Parameter (Theta = 0.5D0 )
      Parameter (Ls1 = 1 )
      Parameter (Ls2 = 0 )
      Parameter (iOP4Dof = 0 ) !WARNING Verify before runing = 1 Sets the Option for Q,Pe,A,P ; =0 Q,A,P


      Penalty = CommonPar(1)  
      iShiftP = nint(CommonPar(2))
      iShiftPe= 1
      Theta   = CommonPar(3)  
      Theta1  = 1.0d0-Theta  
!      DPnor= CommonPar(4)  

      R1      = Param( 1 )
      R2      = Param( 2 )
      Cap     = Param( 3 )
      Pter    = Param( 4 )
      dPter   = 0.0d0
      Nf_Pter = JParam(NodEl)

      Nod=1
      do while (Nod .lt. NodEl)
                 iFlowDir(Nod) = JParam(Nod) 
                 Nod=Nod + 1
      end do

     If (Nf_Pter .GT. 0 ) Then
           ipFunc  = 5               
!        goto 897
!           NPoints=JParam(NodEl)
           NPoints = Nf_Pter
           R1_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           R1_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           R1  = 0.5D0 * ( R1_1 + R1_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+1)
           R2_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           R2_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           R2  = 0.5D0 * ( R2_1 + R2_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+2)
           Cap_0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           Cap_1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           Cap  = 0.5D0 * ( Cap_1 + Cap_0 )

           ipFunc  = ipFunc + 2*NPoints               
           NPoints = JParam(NodEl+3)
897        Pter0 = PTer_F  ( Time        , NPoints, Param( ipFunc ) )
           Pter1 = PTer_F  ( Time  + Dtm , NPoints, Param( ipFunc ) )
           Pter  = Pter1
!          Pter  = 0.5D0 * ( Pter1 + Pter0 )
           dPter = ( Pter1 - Pter0 ) / Dtm



!	           Pter0 = PTer_F  ( Time        , Nf_Pter, Param( 5 )  )
!	           Pter1 = PTer_F  ( Time  + Dtm , Nf_Pter, Param( 5 )  )
!                 Pter  = Pter1
!                 Pter  = 0.5D0 * ( Pter1 + Pter0 )
!                 dPter = ( Pter1 - Pter0 ) / Dtm





      Endif

!	If (Nf_Pter .LT. 0 ) Then
!	           Pter0 = 1.333d5* (1.d0 -  Time/.8d0)
!	           Pter1 = 1.333d5* (1.d0 - (Time  + Dtm) / .8d0 )
!                dPter = ( Pter1 - Pter0 ) / Dtm
!                 Pter  = Pter1

!                If (Pter1 .LT. 0.0d0 ) Then
!                  Pter=0.0d0c
!	            dPter=0.0d0
!                endif
!      Endif



!	ipQ   = (NodEl-1) * iDofT + 1
	ipQ   = 1
	ipP   = ipQ + iShiftP
      Q0N   = Sol0 ( ipQ )
      P0N   = Sol0 ( ipP )
      Q1N   = Sol1 ( ipQ )
      P1N   = Sol1 ( ipP )

!	iRow_Qter = 1
	iRow_Qter = ipQ
	iRow_Pter = iRow_Qter + iShiftP

!        AE(iRow_Pter,iRow_Pter) = (1.0d0 + R2*Cap/Dtm )*Penalty*DPnor
        AE(iRow_Pter,iRow_Pter) = (1.0d0 + R2*Cap/Dtm )*Penalty
        AE(iRow_Pter,iRow_Qter) =-((R1+R2)+ R1*R2*Cap/Dtm)*Penalty
        AE(iRow_Qter,iRow_Qter) = 1.0d0*Penalty

	    Do Nod = 1, NodEl - 1
! los nodos estan desplazados en 1
	        iRow_QNod = iDofT*Nod + 1 
	        iRow_PNod = iRow_QNod + iShiftP
	        iRow_PeNod = iRow_QNod + iShiftPe
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              AE (iRow_PNod , iRow_PNod   ) =  1.0d0*Penalty
              AE (iRow_PNod , iRow_Pter   ) = -1.0d0*Penalty
              If (iOP4Dof .NE. 0) then
              AE (iRow_PeNod , iRow_PeNod ) =  1.0d0*Penalty
              AE (iRow_PeNod , iRow_PNod  ) = -1.0d0*Penalty
              endif

!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              AE (iRow_Qter,iRow_QNod) = -1.0d0*iFlowDir(Nod)*Penalty

	    End Do


!     BE(iRow_Pter)= (  (   R2*Cap/Dtm   )*DPnor  *P0N +     &
     BE(iRow_Pter)= (  (   R2*Cap/Dtm   )  *P0N +     &
                       ( - R1*R2*Cap/Dtm)  *Q0N +     &
                           Pter + R2*Cap*dPter ) *Penalty


Return
End 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SubRoutine PipeFlow1D_QPPe                          &
     &           ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodEL,   &
     &             Sol0,Sol1,CommonPar,                   &
     &             Param,JParam,DelT,DTm,Time)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

	Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = .50D0 )
!      Parameter (Theta1= 1.0d0-Theta )
!      Parameter (Ls1 = 1 )

!     Ls2 = 0 => LS ; 1 => Full Upwinding
!      Parameter (Ls2 = 1 )
!      Parameter (UpLs2 = 2.0d0 )
      Parameter (Ls2 = 1 )
      Parameter (UpLs2 = 0.5d0 )
      Parameter (CLap = .0d0 )

!  iSwPF = 2  Controla la forma de P(A)

!     NGP= Number of gauus points
! dependeria de Ndim y Integration order rule
!      Parameter (NodEL = 2 )
      Parameter (NdimE = 1 )
      Parameter (NGP   = 2 )
      Real*8 Jac(NdimE,NdimE)
      Logical OnlyVertex

      DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),                 &
     &  XLL(*),Sol0(*),Sol1(*),                                      &
     &  Param(*), JParam(*),CommonPar(*),                            &
     &  Gravity(Ndim),Psi(NdimE,NGP),W(NGP),                         &
     &  dFi1_L(NdimE,NodEl),dFi2_L(NdimE,NodEl),dFi1_G(NdimE,NodEL), &
     &  Fi1(NodEl),Fi2(NodEl),           XL(Ndim*NodEL),Vdx(Ndim)

!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
      OnlyVertex = .True. 
           
! nci do variable,  number of coordinate index
      Ro      = CommonPar(1)
      Rot     = Ro/Dtm
      Vis     = CommonPar(2)/Ro
      Profile = CommonPar(3)
      iShiftP = nint(CommonPar(4))
      Theta   = CommonPar(5)
      iSwPF   = nint(CommonPar(6))
      Theta1  = 1.0d0-Theta 
      Last    = 6 
      DX = 0.0d0
      Do nsd=1, Ndim
                      Vdx(nsd) = XLL( Ndim + nsd ) - XLL( nsd )
                      DX       = DX + ( Vdx(nsd) )**2
      end do

      DX = DSqrt( DX )
      Do nsd=1, Ndim
                      Vdx(nsd) = Vdx(nsd)/DX
      end do
      Gravi = 0.0d0
      Do nsd=1, Ndim
                   Gravity(nsd)= CommonPar( Last + nsd )
                   Gravi       = Gravi + Gravity(nsd)*Vdx(nsd)
      end do
      
      iShiftPe = 1
      ArefI        = Param (  1 )
      ArefD        = Param (  2 )
      AlfaI        = Param (  3 )  !Alfa= pi . Ro . Ho
      AlfaD        = Param (  4 )
      Elas         = Param (  5 )
      ElCo         = Param (  6 )
      Epr          = Param (  7 )
      Ep0          = Param (  8 )
      Viswall      = Param (  9 )/Elas
      ExpViswall   = Param ( 10 )
      Pref         = Param ( 11 )
      Pinf         = Param ( 12 )
      Permeability = Param ( 13 )
      Cp           = Permeability 
!%%%%%%%%%%OJOOOOOO%%%%%%%%%%%%%%%%
!      Cp = Cp * ( 1.0d0 - dexp(-Time/.8) )
!      Viswall=Viswall*( 1.0d0 - dexp(-Time/.2) )
!      Cp1          = Cp*DPnor 
      Cp1          = Cp

      DxAref  = (ArefD - ArefI) / DX
      DxAlfa  = (AlfaD - AlfaI) / DX
      roCo2I  = Elas*AlfaI/ArefI/2.0d0 
      roCo2D  = Elas*AlfaD/ArefD/2.0d0 
      roCo2   = (roCo2I+roCo2D)/2.0d0
      DxroCo2 = (roCo2D - roCo2I) / DX

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
        iSimplex = 0     

!     Psi: Gauss Points Matrix, W: Weights
      Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    

!      iBU: use bubble function
	   iBU=0

!     NodG = number of geometric nodes
      NdimE1  = NdimE+1
      NodP    = NdimE1


      IF (OnlyVertex) Then
          NodG    = NodP
      Else
          NodG = NodEL - iBu
      EndIF

      ipQI  = 1
      ipPI  = ipQI + iShifTPe  
      ipPtI = ipQI + iShifTP
      ipAI  = ipPtI - 1
      ipQD  = 1    + iDofT 
      ipPD  = ipQD + iShifTPe
      ipPtD = ipQD + iShifTP
      ipAD  = ipPtD - 1

      Q0I   = Sol0 ( ipQI )
      P0I   = Sol0 ( ipPI )
      A0I   = Sol0 ( ipAI )
      Pt0I  = Sol0 ( ipPtI)
      Q0D   = Sol0 ( ipQD )
      P0D   = Sol0 ( ipPD )
      A0D   = Sol0 ( ipAD )
      Pt0D  = Sol0 ( ipPtD)

      Q1I   = Sol1 ( ipQI )
      P1I   = Sol1 ( ipPI )
      A1I   = Sol1 ( ipAI )
      Pt1I  = Sol1 ( ipPtI)
      Q1D   = Sol1 ( ipQD )
      P1D   = Sol1 ( ipPD )
      A1D   = Sol1 ( ipAD )
      Pt1D  = Sol1 ( ipPtD)


      Vel0I = Q0I / A0I
      Vel0D = Q0D / A0D
      Vel1I = Q1I / A1I
      Vel1D = Q1D / A1D

      A0IsRo= A0I / Ro
      A0DsRo= A0D / Ro
      A1IsRo= A1I / Ro
      A1DsRo= A1D / Ro

      DPA0I = DPEFA ( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA0D = DPEFA ( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1I = DPEFA ( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,iSwPF )
      DPA1D = DPEFA ( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,iSwPF )

!      PRE0I = PEFA( A0I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE0D = PEFA( A0D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1I = PEFA( A1I,ArefI,roCo2I,ELas,ELCO,Ep0,Epr,Pref,iSwPF )
!      PRE1D = PEFA( A1D,ArefD,roCo2D,ELas,ELCO,Ep0,Epr,Pref,iSwPF )

	  DQx   = (Q0D-Q0I) / DX
	  APunI = (A1I-A0I) / Dtm
	  APunD = (A1D-A0D) / Dtm

	  CPvI   = Cvis( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall )
	  CPvD   = Cvis( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall )
!      DPVAI = DPVFA( A1I,APunI,ArefI,roCo2I,Viswall,ExpViswall,Dtm )
!      DPVAD = DPVFA( A1D,APunD,ArefD,roCo2D,Viswall,ExpViswall,Dtm )

      RoCsAI = DsqrT(A0IsRo*DPA0I) /A0IsRo
      RoCsAD = DsqrT(A0DsRo*DPA0D) /A0DsRo

      Fmen1I = FM (A1I,Q1I,Ro,DPA1I,Profile)
      Fmen1D = FM (A1D,Q1D,Ro,DPA1D,Profile)
      Fmen0I = FM (A0I,Q0I,Ro,DPA0I,Profile)
      Fmen0D = FM (A0D,Q0D,Ro,DPA0D,Profile)

      Fmas1I = FP (A1I,Q1I,Ro,DPA1I,Profile)
      Fmas1D = FP (A1D,Q1D,Ro,DPA1D,Profile)
      Fmas0I = FP (A0I,Q0I,Ro,DPA0I,Profile)
      Fmas0D = FP (A0D,Q0D,Ro,DPA0D,Profile)

      rMachI= Dabs( (Fmas1I+Fmen1I)/(Fmas1I-Fmen1I) )
      rMachD= Dabs( (Fmas1D+Fmen1D)/(Fmas1D-Fmen1D) )

      if ( rMachI.ge.1 .or. rMachD.ge.1) then
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'WARNING !!! #### @@@@@@@ !!!'
        write(*,*)'MACH NUMBER IS REACHING UNITY'
        write(*,*)'POSIBLE SUPERSONIC ARTERIAL FLOW'
      ENDIF

      Fmas0 = 0.5d0*(Fmas0I+Fmas0D)
      Fmen0 = 0.5d0*(Fmen0I+Fmen0D)
      Fmas1 = 0.5d0*(Fmas1I+Fmas1D)
      Fmen1 = 0.5d0*(Fmen1I+Fmen1D)


      Vel1 = 0.5d0*(Vel1I+Vel1D)
      A1   = 0.5d0*(A1I+A1D)
      A1sRo = A1/Ro


      DPA1 = 0.5d0* (DPA1I+DPA1D)
      DPA0 = 0.5d0* (DPA0I+DPA0D)

      DPA1  = 0.5d0* ( DPA1 + DPA0 )
!	Pv = 0.5d0*(PvI+PvD)
!	CpPvPi= Cp*(Pv-Pinf)
      CpPvPi= Cp*(-Pinf)
      CPv   =.5d0*(CPvI+CpvD)
      Betav= CPv*Cp  
      Betav= Betav / (1.0d0 + Betav)

      CourantFmen =dabs(Fmen1)*DTm/DX
      CourantFmas =dabs(Fmas1)*DTm/DX
!	Upmen= (1.0d0/(CourantFmen*Theta)/UpLs2)**Ls2
!	Upmas= (1.0d0/(CourantFmas*Theta)/UpLs2)**Ls2

!      if (CourantFmen > 1.0d0) CourantFmen = 1.0d0
!      if (CourantFmas > 1.0d0) CourantFmas = 1.0d0
    if (Ls2 > 0) then
!            Upmen= UpLs2/(CourantFmen*Theta)
!            Upmas= UpLs2/(CourantFmas*Theta)
!            UpmenTheta = UpLs2 / CourantFmen * DTm
!            UpmasTheta = UpLs2 / CourantFmas * DTm
            UpmenTheta = UpLs2 * DX / dabs(Fmen1)
            UpmasTheta = UpLs2 * DX / dabs(Fmas1)
    else
            UpmenTheta = Theta * DTm  
            UpmasTheta = Theta * DTm
    endif 

      Fr  = Friction (Vel1,A1,Vis)
      Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)*Vel1
!       Fvis= - Fr/4.0d0*Dsqrt( A1*PI)*Dabs(Vel1)/(A1I+A1D)


!!!!!!!!   OJJJJOOOOOOO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FWallvis= 0.0d0  !- .5d0*(A1IsRo+A1DsRo)*(PvD-PvI)/DX
      GI  =  Fvis + Gravi*A1I + FWallvis
      GD  =  Fvis + Gravi*A1D + FWallvis
!       GI  =         Gravi*A1I + FWallvis
!       GD  =         Gravi*A1D + FWallvis

      GG  = 0.5d0 * ( GI + GD )
      GGmen = GG 
      GGmas = GG 
!	GGmen = GG + GGn
!	GGmas = GG + GGs

      Fmas1_DPA1 = Fmas1/DPA1
      Fmen1_DPA1 = Fmen1/DPA1
      Fmas1xCpPvPi= Fmas1*CpPvPi
      Fmen1xCpPvPi= Fmen1*CpPvPi

! Lapidus artificial difussivity
!      rLap= CLap*Dabs(Dx*(P1D-P1I)*Fmas1_DPA1)          

!--------------------------
      LoopGaussPoints: Do nG = 1, NGP
!--------------------------

	Det = Dx
	dFi1_G (1,1) = - 1.0d0/DX
	dFi1_G (1,2) =   1.0d0/DX


      Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!      Call ShapeF (NodEL,NdimE,Fi2,PSI(1,nG),2,iBu)

!      Do NodQ=1, NodEL
!	ipNodQ = (NodQ-1)*iDofT + 1
!        Q0 = Q0 + Sol0( ipNodQ )*Fi1(NodQ)
!        Q1 = Q1 + Sol1( ipNodQ )*Fi1(NodQ)
!	ipNodP = ipNodQ + Ndim
!        Pre0   = Pre0   + Sol0( ipNodP )*Fi1(NodQ)
!        Pre1   = Pre1   + Sol1( ipNodP )*Fi1(NodQ)
!      Enddo

      DetW=Det*W(nG)

      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowQ = ipRow + 1
        ipRowP = ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        Fi1i  = Fi1(i)
        dFi1i = dFi1_G ( 1 , i )

        dResMenQ_i = Fi1i  + UpmenTheta * Fmen1 * dFi1i
        dResMasQ_i = Fi1i  + UpmasTheta * Fmas1 * dFi1i
!	    dResMenP_i = -(Fmas1_DPA1*dResMenQ_i + Fmas1* Cp1* theta *Fi1i)
!	    dResMasP_i = -(Fmen1_DPA1*dResMasQ_i + Fmen1* Cp1* theta *Fi1i)
	    dResMenP_i = -(Fmas1_DPA1*dResMenQ_i )
	    dResMasP_i = -(Fmen1_DPA1*dResMasQ_i )

        dResMenQ_i = dResMenQ_i * DetW
        dResMasQ_i = dResMasQ_i * DetW
        dResMenP_i = dResMenP_i * DetW
        dResMasP_i = dResMasP_i * DetW


         BE( ipRowQ ) = BE( ipRowQ )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenQ_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasQ_i 
         BE( ipRowP ) = BE( ipRowP )         + &
           (GGmen + Fmas1xCpPvPi)*dResMenP_i + &
           (GGmas + Fmen1xCpPvPi)*dResMasP_i 


        LoopCol: Do j=1,NodEL
         ipCol    = (j-1)*iDofT 
         ipColQ   = ipCol + 1
         ipColP   = ipColQ + iShifTPe
         ipColPt   = ipColQ + iShifTP

         dFi1j    = dFi1_G ( 1 , j )
         Fi1j     = Fi1(j)
         Fi1j_Dtm = Fi1j/Dtm
         Fmen1xdFi1j = Fmen1*dFi1j
         Fmas1xdFi1j = Fmas1*dFi1j

         CoefMen_j  = Fi1j_Dtm + Fmen1xdFi1j*Theta
         CoefMas_j  = Fi1j_Dtm + Fmas1xdFi1j*Theta
         
          Cp1xThetaxFi1j       = Cp1*Theta*Fi1j
          Fmas1xCp1xThetaxFi1j = Fmas1*Cp1xThetaxFi1j
          Fmen1xCp1xThetaxFi1j = Fmen1*Cp1xThetaxFi1j
          VisDPe_j             = A1sRo*Betav*dFi1j
          VisDQ_j              = A1sRo*(1.0d0-Betav)*CPv*dFi1j* 2.0d0* DetW
          CRPe                 = -A1/RoCo2*Vel1  

! se puede sacar fuera del loop de ptos de gauss !!!!!!!!!
         AE(ipRowQ , ipColQ)= AE(ipRowQ , ipColQ) + CoefMen_j * dResMenQ_i + CoefMas_j * dResMasQ_i &
                                                  + Theta*VisDQ_j *dFi1i
!     &      - Fvis *(dResMenQ_i + dResMasQ_i) 


         AE(ipRowQ , ipColP)= AE(ipRowQ , ipColP)                     &
                                  - Fmas1_DPA1*CoefMen_j * dResMenQ_i &
                                  - Fmen1_DPA1*CoefMas_j * dResMasQ_i &  
                                  - VisDPe_j * (dResMenQ_i + dResMasQ_i)*Theta  
         AE(ipRowQ ,ipColPt)= AE(ipRowQ , ipColPt)                   &
                                - Fmas1xCp1xThetaxFi1j * dResMenQ_i  &
                                - Fmen1xCp1xThetaxFi1j * dResMasQ_i   

         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)                     &
                                  - Fmas1_DPA1*CoefMen_j * dResMenP_i &
                                  - Fmen1_DPA1*CoefMas_j * dResMasP_i & 
                                  - VisDPe_j * (dResMenP_i + dResMasP_i)*Theta  
         AE(ipRowP ,ipColPt)= AE(ipRowP , ipColPt)                   &
                                - Fmas1xCp1xThetaxFi1j * dResMenP_i  &
                                - Fmen1xCp1xThetaxFi1j * dResMasP_i  

         AE(ipRowP , ipColQ)= AE(ipRowP , ipColQ) + CoefMen_j * dResMenP_i + CoefMas_j * dResMasP_i  &
                                                  + Theta*VisDQ_j*dFi1i*CRPe
!     &      - Fvis *(dResMenP_i + dResMasP_i) 

	   BEmenj    = Fi1j_Dtm - Fmen1xdFi1j*theta1
	   BEmasj    = Fi1j_Dtm - Fmas1xdFi1j*theta1
       CpTh1Fj   = Cp1*theta1*Fi1j
       VisDPeT1j = VisDPe_j*theta1

       BE1 = BEmenj*Sol0(ipColQ) - (Fmas1_DPA1*BEmenj-VisDPeT1j)*Sol0(ipColP) + Fmas1*CpTh1Fj*Sol0(ipColPt)
       BE2 = BEmasj*Sol0(ipColQ) - (Fmen1_DPA1*BEmasj-VisDPeT1j)*Sol0(ipColP) + Fmen1*CpTh1Fj*Sol0(ipColPt)

       BE( ipRowQ ) = BE( ipRowQ )+ BE1*dResMenQ_i + BE2*dResMasQ_i -      Theta1*VisDQ_j*dFi1i*Sol0(ipColQ)
       BE( ipRowP ) = BE( ipRowP )+ BE1*dResMenP_i + BE2*dResMasP_i - CRPe*Theta1*VisDQ_j*dFi1i*Sol0(ipColQ) 
! Lapidus Shock capturing
!         AE(ipRowP , ipColP)= AE(ipRowP , ipColP)+rLap*dFi1i*dFi1j*DetW  


        End do LoopCol

!-----------------------------------------------
      End do LoopRow


!--------------------------
      EndDo LoopGaussPoints
!--------------------------


!      LoopArea: Do i=1,NodEL
!	i = 1
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1I
!      BE (ipRowA)        =   PRE1I - DPA1I * A1I 

!	i = 2
!        ipRow  = (i-1)*iDofT + 1
!        ipColP = ipRow + iShifTP
!        ipRowA = ipColP - 1
!      AE (ipRowA,ipColP) =    1.0D0
!      AE (ipRowA,ipRowA) = - DPA1D
!      BE (ipRowA)        =   PRE1D - DPA1D * A1D

!      End do LoopArea

!      LoopArea: Do i=1,NodEL
	i = 1
        ipRowQ  = (i-1)*iDofT + 1
        ipColPe= ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        ipColPt= ipRowPt
        ipRowA = ipRowPt - 1
      AE (ipRowA,ipColPe) =   1.0D0
      AE (ipRowA,ipRowA)  = - DPA1I
      BE (ipRowA)         =   P1I - DPA1I * A1I
      AE (ipRowPt,ipRowPt)=   1.0D0 + CPvI*Cp
      AE (ipRowPt,ipColPe)=  -1.0D0 
      AE (ipRowPt,ipRowQ )       = - CPvI/DX 
      AE (ipRowPt,ipRowQ+iDofT ) =   CPvI/DX 
      BE (ipRowPt)               = + CPvI*Cp*Pinf


!      AE (ipRowA,ipRowA) = -(DPA1I +1.0d0* DPVAI)/DPnor
!      BE (ipRowA)        =  (PRE1I+PvI  - (DPA1I +1.0d0* DPVAI) * A1I)/DPnor 

	i = 2
        ipRowQ  = (i-1)*iDofT + 1
        ipColPe= ipRowQ + iShifTPe
        ipRowPt= ipRowQ + iShifTP
        ipColPt= ipRowPt
        ipRowA = ipRowPt - 1
      AE (ipRowA,ipColPe) =   1.0D0
      AE (ipRowA,ipRowA)  = - DPA1D 
      BE (ipRowA)         =   P1D - DPA1D * A1D
      AE (ipRowPt,ipRowPt)=   1.0D0 + CPvD*Cp
      AE (ipRowPt,ipColPe)=  -1.0D0 
      AE (ipRowPt,ipRowQ )       =   CPvD/DX 
      AE (ipRowPt,ipRowQ-iDofT ) = - CPvD/DX 
      BE (ipRowPt)               = + CPvD*Cp*Pinf

!      AE (ipRowA,ipRowA) = -(DPA1D +1.0d0* DPVAD)/DPnor
!      BE (ipRowA)        =  (PRE1D+PvD  - (DPA1D +1.0d0* DPVAD) * A1D)/DPnor


!      End do LoopArea
!	Write(18,*)MaxLrows*MaxLrows
!	 do i=1,MaxLrows
!	 do j=1,MaxLrows
!	Write(18,*)i,j,AE(i,j)
!      enddo
!      enddo
!
      Return
      End


