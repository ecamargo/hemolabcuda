!
!-----------------------------------------------------------------------
!          PROGRAMA DE PROPOSITO GENERAL PARA RESOLUCION DE SISTEMAS 
!                Reducibles por Elementos
!                                  GENE(RI(CO))
!----------------------------------------------------------------------- 
! La rutina Ppal es MainGeneric
!      USE NUMERICAL_LIBRARIES
!      Use MSIMSL
!      INCLUDE IMSLF90.FI
      Program Premain    
!      Use ModuloLectura
      IMPLICIT REAL*8 (A-H,O-Z)
	Logical Sym, NonLinearProblem,Iterative
      INTEGER ERR_ALLOC
	Parameter (Lch=16)
	Character Str*120


!      INTERFACE ReadMat
!          MODULE PROCEDURE ReadMat
!      END INTERFACE
!      INTERFACE ReadCoupling
!          MODULE PROCEDURE ReadCoupling
!      END INTERFACE
!      INTERFACE Dimen
!          MODULE PROCEDURE Dimen
!      END INTERFACE

      Allocatable X         (:)
      Allocatable Ie__Param (:)
      Allocatable Ie_JParam (:)
      Allocatable Param     (:)
      Allocatable JParam    (:)
      Allocatable iDofType  (:)
      Allocatable Sol0      (:)
      Allocatable Dirich    (:)
      Allocatable iElementType (:)
      Allocatable iElementMat(:)
      Allocatable IE(:)
      Allocatable JE(:)

      Allocatable NodesOut(:)

      Allocatable iElementLib (:,:)
      Allocatable CommonPar (:,:)
      Allocatable iCoupling (:,:,:)
	Allocatable MC (:)
	Character(Lch) DofNamesG
	Allocatable DofNamesG (:)
	Allocatable iDofNamesG (:)
      Allocatable rNormG     (:)
      Allocatable TolG       (:)
      Allocatable iXupdateG  (:)
	Allocatable iDofNames (:,:)
      Allocatable rNorm     (:,:)
      Allocatable Tol       (:,:)
      Allocatable iXupdate  (:,:)

	Character*30 NelName
      Allocatable IDGroup(:),NelTG(:),NelName(:),NodElG(:)
      Allocatable  iElementTypeG(:)
      Allocatable  iElementMatG (:)

      Allocatable Sym(:),Iterative(:),MaxIter(:)
      Allocatable iSolverType(:),SRP(:)
      Allocatable ITMAXSOL(:),EPS(:),Krylov(:),LFIL(:),TolPre(:)

      IUnit_BasicParam = 12
      IUnit_Mesh       = 13
      IUnit_Param      = 14
      IUnit_Ini        = 61
      ERR_ALLOC=0
      OPEN ( IUnit_BasicParam, FILE= 'Basparam.txt',STATUS= 'OLD' )
      OPEN ( IUnit_Mesh, FILE= 'Mesh.txt',STATUS= 'OLD' )
      OPEN ( IUnit_Ini, FILE= 'IniFile.txt',STATUS= 'OLD' )


! LECTURA DE DATOS
      ioUnit=IUnit_BasicParam

!      Str='*LOG FILE'
      Str='*L'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
      if (iError /= 0) then
          Str='*l'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
      Endif
      if (iError == 0) then
!          READ(ioUnit,*,ERR=99)Str
          READ(ioUnit,'(1A120)')Str
	    Str=ADJUSTL(Str)
	    Length=Len_Trim(Str)
	    if(Length/=0) then
              if (.not. (Length==1 .and. Str(1:1)=='*') )then 
                  Open(6,FILE=Str)
	            rewind (6)
              endif
          endif
      End if

      Write(6,*)'Reading Data'
!_______________________________________________________________
      Str='*NODAL DOFs'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
        Write(6,*)Str,"NOT FOUND"
        Stop
      End if
      READ(IUnit_Mesh,*)iDofT
!_______________________________________________________________
      Str='*DIMEN'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
        Write(6,*)Str,"NOT FOUND"
        Stop
      End if
      READ(IUnit_Mesh,*)Ndim
!________________________________________________________________
!%%%%%%Lectura de Malla-Coordenadas Nodales y Conectivity Matrix%%%%%%%%
      Str='*COORDINATES'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
        Write(6,*)Str,"NOT FOUND"
        Stop
      End if
      READ(IUnit_Mesh,*)NodT
      Allocate(X(NodT*Ndim))
      READ(IUnit_Mesh,*)X
!________________________________________________________________
      Str='*ELEMENT GROUPS'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
        Write(6,*)Str,"NOT FOUND"
        Stop
      End if
      READ(IUnit_Mesh,*)NGROUPS
      Allocate (IDGroup(NGROUPS),NelTG(NGROUPS),NelName(NGROUPS))
      Allocate (NodElG(NGROUPS))
      Do ng=1,NGROUPS
           READ(IUnit_Mesh,*)IDGroup(ng),NelTG(ng),NelName(ng)
	Enddo

      NelT=Sum(NelTG)

	ALLOCATE(IE(NelT+1))
      ipNel = 0
      Do ng=1,NGROUPS
              Len= Len_Trim(NelName(ng))
              NodElG(ng)=0
              If ( NelName(ng)(1:Len) == 'Point') NodElG(ng)=1
              If ( NelName(ng)(1:Len) == 'Seg2') NodElG(ng)=2
              If ( NelName(ng)(1:Len) == 'Seg3') NodElG(ng)=3
              If ( NelName(ng)(1:Len) == 'Tri3') NodElG(ng)=3
              If ( NelName(ng)(1:Len) == 'Tri6') NodElG(ng)=6
              If ( NelName(ng)(1:Len) == 'Tetra4') NodElG(ng)=4
              If ( NelName(ng)(1:Len) == 'Quad4') NodElG(ng)=4
              If ( NelName(ng)(1:Len) == 'Quad8') NodElG(ng)=8
              If ( NelName(ng)(1:Len) == 'Cub8') NodElG(ng)=8
              If ( NelName(ng)(1:Len) == 'Cub20') NodElG(ng)=20
              If ( NelName(ng)(1:Len) == 'Tetra10')NodElG(ng)=10
      IF(NelName(ng)(1:7)=='Generic' .or.NelName(ng)(1:7)=='generic'.or.
     &    NelName(ng)(1:7)=='GENERIC' ) then
                READ(IUnit_Mesh,*) ( IE(ipNel+ Nel), Nel=1, NelTG(ng))
      else
              if (NodElG(ng).eq.0) then
                    Write(6,*)"ELEMENT TYPE NOT IMPLEMENTED"
                    Stop
              End if

              Do Nel=1,NelTG(ng)
                                IE(ipNel+Nel)=NodElG(ng)
              enddo
      Endif
                ipNel=ipNel+NelTG(ng)

      Enddo
!________________________________________________________________
       Length_JE = IEarrange(IE,NelT)
       Allocate ( JE( Length_JE )  )
       Str='*INCIDENCE'
       rewind(IUnit_Mesh)
       iError = iFindStringInFile(Str,IUnit_Mesh)
          if (iError.ne.0) then
            Write(6,*)Str,"NOT FOUND"
            Stop
          End if
       READ(IUnit_Mesh,*)JE
!________________________________________________________________
      Allocate (  iElementType (NelT) )
      Allocate (  iElementMat  (NelT), STAT = ERR_ALLOC  )
          IF(ERR_ALLOC.NE.0)Call ERROR_ALLOC
  
      LDOFTs = iDofT*NodT
!________________________________________________________________
!      Allocate (  iElementTypeG(NGROUPS) )
!      Allocate (  iElementMatG (NGROUPS) )
      Str='*ELEMENT TYPE'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
            Write(6,*)Str,"NOT FOUND"
            Stop
      End if
      ipNel = 0
      Do ng=1, NGROUPS
       If(NodElG(ng) /= 0) then
         READ(IUnit_Mesh,*)iElemTypeG
         Do Nel=1,NelTG(ng)
                     iElementType(ipNel+Nel)=iElemTypeG
         enddo
         ipNel=ipNel+NelTG(ng)
       else
          READ(IUnit_Mesh,*)(iElementType(ipNel+Nel),Nel=1,NelTG(ng))
          ipNel=ipNel+NelTG(ng)
       End if
      Enddo
!__________________________________________________________
      Str='*ELEMENT MAT'
      rewind(IUnit_Mesh)
      iError = iFindStringInFile(Str,IUnit_Mesh)
      if (iError.ne.0) then
         Write(6,*)Str,"NOT FOUND"
         Stop
      End if
      
      ipNel = 0
      Do ng=1, NGROUPS
       If(NodElG(ng) /= 0) then
         READ(IUnit_Mesh,*)iElemMatG
         Do Nel=1,NelTG(ng)
                     iElementMat(ipNel+Nel)=iElemMatG
         enddo
         ipNel=ipNel+NelTG(ng)
       else
          READ(IUnit_Mesh,*)(iElementMat(ipNel+Nel),Nel=1,NelTG(ng))
          ipNel=ipNel+NelTG(ng)
       End if
      Enddo

!----------------------------------
      MaxNodEl=0
      do Nel=1,NelT
          NodesE= IE(Nel+1)-IE(Nel)
          if (NodesE .gt. MaxNodEl) MaxNodEl = NodesE
	end do
      MaxLRows = MaxNodEl*iDofT
!----------------------------------


!________________________________________________________________
      iWDataSet=0  
!     iWDataSet > Switch for rewriting param.txt as a restart
!     iWDataSet==0 (Default val)don't rewrite param.txt; <>0 rewrites      
!      Str='*Param Write Swicht'
      Str='*Param W'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError == 0) then
              READ(ioUnit,*)iWDataSet
	    else
              Write(6,*)
     1      '*Param Write Swicht NOT FOUND, proceed with default value'  
         Endif

!      Str='*Initial Time'  0: Tini as in Basparam ; 1: as in Inifile
      Str='*Init'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError == 0) then
              READ(ioUnit,*)initialTime
	    else
              Write(6,*)
     1      '*Initial Time NOT FOUND, proceed with default value'  
              initialTime = 0
         Endif


          Str='*TimeStep'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
	    READ(ioUnit,*)DelT,Tini,Tmax
 
          Str='*OutputControl'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND, Proceed with default values"
            nkt=1 ; nStepOut=1  
	    else
          	    READ(ioUnit,*)nkt,nStepOut
          End if

          Str='*NodeOutputControl'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND, Proceed with default value 0"
	      NodOUT=0
	    else
          	    READ(ioUnit,*)NodOUT
          End if

          if (NodOUT .gt.0) then
            Allocate (  NodesOut(NodOUT) )
            READ(ioUnit,*) (NodesOut(i), i=1,NodOUT)
          endif

          Str='*ElementLibraryControl'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
	    READ(ioUnit,*)MaxElemLib,MaxLCommonPar

          Str='*Renumbering'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND, Proceed with default value 0"
	        NStepsRenum=0
	    else
      	    READ(ioUnit,*)NStepsRenum
          End if
!_____________________________________________________________
! Counting the number of Substeps
      NSubSteps=0
          rewind(ioUnit)
          Str='*SubStep'
	Do while (iError==0)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError==0)NSubSteps = NSubSteps +1
      Enddo
      if (NSubSteps==0) then
	  Write(6,*)'NSubSteps==0,At least ONE Substep must exist' 
        stop
	endif
       rewind(ioUnit)

!%%%%%%%% Lectura de libreria de elementos en Basparam %%%%%%%%%%%%%%%%%%%%
      Allocate( Sym(NSubSteps),Iterative(NSubSteps),MaxIter(NSubSteps))
      Allocate( iSolverType(NSubSteps),SRP(NSubSteps))
      Allocate( ITMAXSOL(NSubSteps),EPS(NSubSteps),Krylov(NSubSteps))
      Allocate( LFIL(NSubSteps),TolPre(NSubSteps))
      LCommonPar = MaxElemLib*MaxLCommonPar
      Allocate (  rNormG        ( iDofT  )  )
      Allocate (  TolG          ( iDofT  )  )
      Allocate (  rNorm        ( iDofT , NSubSteps  )  )
      Allocate (  Tol          ( iDofT , NSubSteps  )  )
      Allocate (  CommonPar    ( LCommonPar   , NSubSteps   )  )
      Allocate (  iElementLib  ( 5*MaxElemLib , NSubSteps )  )
	Allocate (  iDofNamesG    ( iDofT*Lch )  )
	Allocate (   DofNamesG    ( iDofT  )  )
      Allocate (  iXupdateG     ( Ndim   )  )
	Allocate (  iDofNames    ( iDofT*Lch , NSubSteps    )  )
      Allocate (  iXupdate     ( Ndim  , NSubSteps         )  )

!_______________________________________________________________
      iError=0
      Str='*StepContinuationControl'
      rewind(ioUnit)
      iError = iFindStringInFile(Str,ioUnit)
      if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
        Stop
      End if
      read(ioUnit,*)NonLinearProblem,MaxIterG,SRPG,nOldTimeSteps
      Read (ioUnit,*) ( rNormG(N)    , N=1 , iDofT )
      Read (ioUnit,*) ( TolG(N)      , N=1 , iDofT )
      Read (ioUnit,*) ( DOfNamesG(N) , N=1 , iDofT )
      Read (ioUnit,*) ( iXupdateG(N) , N=1 , Ndim )

! convierto a integer los nombres de las variables
      do i=1,iDofT
	do j=1,Lch
	iDofNamesG(j+Lch*(i-1)) =IACHAR (DofNamesG(i)(j:j))
	enddo
	enddo

      MaxCoup= 0
      iNCoup = 0
      Do NSbp=1,NSubSteps
          Str='*SubStep'
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
          CALL ReadLibrary ( iElementLib(1,NSbp),CommonPar(1,NSbp),
     &    iDofNames(1,NSbp),rNorm(1,NSbp),Tol(1,NSbp),iXupdate(1,NSbp),
     &    Sym(NSbp),Iterative(NSbp),MaxIter(NSbp),
     &    iSolverType(NSbp),SRP(NSbp),
     &    ITMAXSOL(NSbp),EPS(NSbp),Krylov(NSbp),LFIL(NSbp),TolPre(NSbp),
     &    iDofT,MaxElemLib,MaxLCommonPar,Ndim,Lch,IUnit_BasicParam )
! Calculo la ultima matriz de acoplamiento para Dimensionar y Cuantas hay que leer
          do L=1,MaxElemLib
	        ipELib= (4-1)*MaxElemLib+L
	        MC1 = iElementLib(ipELib,NSbp)
	        iCoup=abs( MC1 )
              MaxCoup= max(iCoup,MaxCoup)
!              if (   MC >    0   )iNCoup  = iNCoup + 1
          enddo
      Enddo

      Allocate ( MC ( MaxCoup ) )
	MC=0
      Do NSbp=1,NSubSteps
          do L=1,MaxElemLib
	        ipELib= (4-1)*MaxElemLib+L
	        MC1 = iElementLib(ipELib,NSbp)
!	        iCoup=abs( MC1 )
!              if (iCoup > MaxCoup)MaxCoup = iCoup
              if (   MC1 >    0   )MC(MC1)=1
          enddo
      Enddo
      iNCoup = Sum(MC)

!%%%%%%%%Lectura de la matriz de acoplamiento en BasicParam%%%%%%
      allocate( iCoupling(MaxLRows,MaxLRows,MaxCoup) )
	iCoupling=0
      if (   iNCoup >    0   )then
          Str='*Coupling Matrices'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
        Do iNCou=1,iNCoup
         Read (ioUnit,*) L
         Read (ioUnit,*)((iCoupling (iDofR,iDofC,L) ,iDofC=1,MaxLRows ),
     &                 iDofR=1,MaxLRows)

        Enddo
      End if

      Close(ioUnit)
!%%%%%%Lectura de materiales en Param.txt%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!	Call ReadMaT (Ie__Param,Ie_JParam,Param,JParam,iUnit_Param )
!%%%%%%Lectura de materiales%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ioUnit = iUnit_Param
      OPEN ( ioUnit, FILE= 'Param.txt' ,STATUS= 'OLD' )

!          Elementwise varying Parameters
          Str='*Parameter Groups'
          rewind(ioUnit)
          iError = iFindStringInFile(Str,ioUnit)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
      Read (ioUnit,*) MaxId_DataSet
       Allocate (  Ie__Param( MaxId_DataSet+1 )  )
       Allocate (  Ie_JParam( MaxId_DataSet+1 )  )
      If (MaxId_DataSet > 0 ) Then
!      Allocate (  Ie__Param( max(MaxId_DataSet+1,2))  )
!      Allocate (  Ie_JParam( max(MaxId_DataSet+1,2))  )

! Lectura de par�metros reales
       Str='*Real Parameters'
       rewind(ioUnit)
       iError = iFindStringInFile(Str,ioUnit)
       if (iError.ne.0) then
                    Write(6,*)Str
                    Write(6,*)"NOT FOUND"
                 Stop
       End if
       Read (ioUnit,*) (Ie__Param(I) ,I=1, MaxId_DataSet)
       Length_Param  = IEarrange(Ie__Param,MaxId_DataSet)
       Allocate (  Param(  max(1,2*Length_Param) )  )
       if (Length_Param .ne. 0) then
                Read (ioUnit,*) (Param(I),I=1, Length_Param)
			  do i=1,Length_Param
	          Param(i+Length_Param)=Param(i)
			  enddo
       End if

! Lectura de par�metros enteros
              Str='*Integer Parameters'
              rewind(ioUnit)
              iError = iFindStringInFile(Str,ioUnit)
              if (iError.ne.0) then
                Write(6,*)Str
                Write(6,*)"NOT FOUND"
                Stop
              End if
              Read (ioUnit,*) (Ie_JParam(I) ,I=1, MaxId_DataSet)
              Length_JParam  = IEarrange(Ie_JParam,MaxId_DataSet)
              Allocate ( JParam( max(1,2*Length_JParam) )  )
              if (Length_JParam .ne. 0) then
                Read (ioUnit,*) (JParam(I),I=1, Length_JParam)
			  do i=1,Length_JParam
	          JParam(i+Length_JParam)=JParam(i)
			  enddo
              End if
      else

      Ie__Param=1
      Ie_JParam=1
      Allocate ( Param ( 1 )  )
      Allocate ( JParam( 1 )  )
      

      Endif
      Close ( ioUnit)

!Fin Lectura par�metros
!----------------------------------
!________________________________________________________________
      Allocate (  iDofType     ( LDOFTs * NSubSteps       )  )
      Allocate (  Sol0         ( LDOFTs * nOldTimeSteps   )  )
      Allocate (  Dirich       ( LDOFTs * NSubSteps       )  )
          Str='*DIRICHLET CONDITIONS'
          rewind(IUnit_Mesh)
          iError = iFindStringInFile(Str,IUnit_Mesh)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND"
            Stop
          End if
	    READ(IUnit_Mesh,*)iDofType
	    READ(IUnit_Mesh,*)Dirich
      Close (IUnit_Mesh)
!%%%%%%Fin Lectura de Malla-Coordenadas Nodales y Conectivity Matrix%%%%%%%%

!________________________________________________________________
! Initial Conditions on Inifile.txt
!          Str='*Initial Conditions'
          Str='*Ini'
          Rewind(IUnit_Ini)
          iError = iFindStringInFile(Str,IUnit_Ini)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND in Inifile.txt"
            Write(6,*)"Trying to get the values anyway"
          End if

          read(IUnit_Ini,*,iostat=iError) Sol0
            if (iError.lt.0) then
              Write(6,*)
     &           "End of file while trying to get the values of Inifile"
			Stop
             elseif (iError.gt.0) then
              Write(6,*)
     &           "Error while trying to get the values of Inifile"
			Stop
            endif

	if (initialTime /= 0) then
          Str='*Ti'
          Rewind(IUnit_Ini)
          iError = iFindStringInFile(Str,IUnit_Ini)
          if (iError.ne.0) then
            Write(6,*)Str
            Write(6,*)"NOT FOUND in Inifile.txt"
            Write(6,*)"Proceed with Tini as in Basparam.txt"
          else
	    READ(IUnit_Ini,*)Tini
          End if
      
      endif
      Close (IUnit_Ini)
!%%%%%%%% End of initial conditions read %%%%%%%%%%%%%%%



	Call Generico(NodT,NelT,NDim,iDofT,nOldTimeSteps,
     &            NonLinearProblem,MaxIterG,SRPG,nkt,NStepsRenum,
     &            Sym,Iterative,MaxIter,SRP,iSolverType,
     &            ITMAXSOL,EPS,Krylov,LFIL,TolPre,
     &            iElementLib,iCoupling,
     &            iDofNamesG,rNormG,TolG,
     &            iXupdateG,
     &            iDofNames,rNorm,Tol,CommonPar,
     &            iXupdate,
     &            DelT,Tini,Tmax,nStepOut,NodOUT,NodesOut,
     &            MaxNodEl,MaxElemLib,MaxLCommonPar,
!     &           MaxId_CouplingSet,MaxId_DataSet,
     &            X,IE,JE,iElementType,iElementMat,
     &            Ie__Param,Ie_JParam,Param,JParam,
     &            iDofType,Dirich,Sol0,Lch, NSubSteps,
     &            MaxId_DataSet,iWDataSet   )      

      STOP
      END
!-----------------------------------------------------------------------
      
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%