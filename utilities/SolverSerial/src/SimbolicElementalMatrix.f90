!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE CouplingMatrix( ElementLib,Coupling,CommonPar,                &
     &           iDofT,MaxLRows,MaxElemlib,MaxLCommonPar,NDim,NSubSteps )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT REAL*8 (A-H,O-Z)

      Integer ElementLib(MaxElemlib,5,1),Coupling(MaxLRows,MaxLRows,1)      	 
      Dimension CommonPar(MaxLCommonPar,MaxElemlib,1)

!      NodElMax = MaxLRows / iDofT
Do nstp=1,NSubSteps
 
 Do IdElemLib = 1, MaxElemlib
        Id_Elem_Family            = ElementLib(IdElemLib,1,nstp)
        Id_Coup                   = ElementLib(IdElemLib,4,nstp)

        if (Id_Coup == 0 ) Cycle

        if (Id_Coup < 0 ) then
                                               !Coupling Matrix must be assembled
           Id_Coup                      = abs(Id_Coup) 
           ElementLib(IdElemLib,4,nstp) = Id_Coup
           Is_ADD                       = ElementLib(IdElemLib,2,nstp)
          Call AssemblerElementSimbolicMatrix( CommonPar(1,IdElemLib,nstp),Coupling(1,1,Id_Coup),  &
                                               Id_Elem_Family,Is_ADD,MaxLRows,iDofT,NDim          )
        Endif

! simmetrizing elemental matrix
	    Do i = 1,MaxLRows - 1 
	    Do j = i+1,MaxLRows 
              if     ( Coupling(i,j,Id_Coup) .ne. 0) then
		                                                  Coupling(j,i,Id_Coup)=1
              elseif ( Coupling(j,i,Id_Coup) .ne. 0) then 	 
		                                                  Coupling(i,j,Id_Coup)=1
              endif
	    EndDo
	    EndDo


   Enddo
 Enddo
Return
END
!####################

!____________________________________________________________________________
!만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만
 SUBROUTINE AssemblerElementSimbolicMatrix(                             &
     &  CommonPar,Coupling,Id_Elem_Family,Is_ADD,MaxLRows,iDofT,NDim )
!만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만만
      IMPLICIT REAL*8 (A-H,O-Z)
      Integer Coupling(MaxLRows,MaxLRows)      	 
      Dimension CommonPar(*)

      iAdd=1 ; if ( Is_ADD .NE.0 ) iAdd= -1
      Coupling=0


!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
Element_Family: SelectCase (Id_Elem_Family)

!_______________________________________________________________________
!   One-Dimensional Pipe-Flow 
      Case (1,2)
        Call Pipe1D(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!    One-Dimensional Pipe-Flow Conection Element
      Case (3)
        Call Pipe1DCE(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!    One-Dimensional Pipe-Flow Conection Element
      Case (4)
        Call Pipe1D_LWIN(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!    One-Dimensional Pipe-Flow Conection Element
      Case (5)
        Call Pipe1D_LWOUT(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

      Case (13)
        Call Pipe1DL4(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!    One-Dimensional Pipe-Flow Conection Element
      Case (10)
        Call Pipe1DCEL4(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!    One-Dimensional Pipe-Flow Conection Element
      Case (11)
        Call Pipe1D_L4IN(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!    One-Dimensional Pipe-Flow Conection Element
      Case (12)
        Call Pipe1D_L4OUT(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     PipeFlow 1D Least Squares
!       Case (4)
!        Call Pipe1DLS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!      Spring2DNV

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!       Coup2D1DNV          
          Case (22)
           Call Coup2D1DNVS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!       Coup2D1DNVTer Coneccion con 1D
          Case (23)
           Call Coup2D1DNVTerS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

          Case (230)
           Call Coup2D1DNVTer_AlfaS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!       Coup2D1DNVTer
          Case (24)
           Call Coup2D1DNVTerSS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!..............................................................
!      StokesTriangALE Quadratic Elastic
          Case(52)
           Call NavierStokesBubALES(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(520,522,524,5245)
           Call NavierStokesBubALES1(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(521)
           Call NavierStokesBubALES2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(523,525)
           Call NavierStokesBubALESD2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!..............................................................
!Coupling 3D1D Navier
          Case(54)
           Call  Coup_23D1D_NV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(541) !P1D -> P3D
           Call  Coup_23D1D_NV_LinearDS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(542) ! En retraso, Decoupled
           Call  Coup_23D1D_NV_LinearDDS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(540)
           Call Presure_NV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd)
          Case(550)
           Call Coup_23D1D_NV_Linear_AlfaS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd)
!Coupling 3D1D Navier
          Case(55)
           Call  Coup_23D1D_NV_QuadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!..............................................................
!      Elementos de Pared
          Case(57,5705)
           Call Spring23DNV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(579)
           Call Spring23DNV_LinearDesS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(570)
           Call Spring3DNV_LinearSplit1S(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
          Case(571)
           Call Spring3DNV_LinearSplit2S(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!      Elementos de Pared
          Case(58)
           Call Spring23DNV_QuadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!        Dirich Element Los grados restringidos vienen en CommonPar
            Case (100,102)
             Call DirichS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!        Oblic Dirich Element
            Case (101)
             Call DirichO(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!        Oblic Dirich Element
            Case (104)
             Call DirichO2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     Virtual Concentrated Load
       Case (110)
        Call ConcentratedLoadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!..............................................................

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Case Default
           Write(*,*) '!!!!!!! WARNING, ELEMENT NOT IDENTIFIED'	
           Write(*,*) 'while trying to determine the Coupling Matrix'	

!__________________________
 End Select Element_Family  
!&&&&&&&&&&&&&&&&&&&&&&&&&&

! simetrizacion de la matriz simbolica elemental
!	    Do i = 1,MaxLRows - 1 
!	    Do j = i+1,MaxLRows 
!           if ( Coupling(i,j) .ne. 0) then
!		        Coupling(j,i)=1
!               elseif ( Coupling(j,i) .ne. 0) then 	 
!		        Coupling(i,j)=1
!           endif
!	    EndDo
!	    EndDo

      Return
      END
!"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
!_____________________________________________________________________


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Pipe1D(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter ( iVersion=0)  !!! Controla la opcion de 3 o 4 variables   	 
 Integer   Coupling(MaxLRows,MaxLRows)
 Dimension CommonPar(*)
 
 iShiftP= nint(CommonPar(4))

  if ( iVersion == 0 ) then
  !  3 variables Version Q,A,P
     Do NodR = 1, 2
              iRow_Q = iDofT*(NodR-1) + 1 
              iRow_P = iRow_Q + iShiftP
              iRow_A = iRow_P - 1
!              Coupling(iRow_A,iRow_A)=1
!              Coupling(iRow_A,iRow_P)=1

          Do NodC = 1, 2
              iCol_Q = iDofT*(NodC-1) + 1 
              iCol_P = iCol_Q + iShiftP
              iCol_A = iCol_P - 1
              Coupling(iRow_Q,iCol_Q)=1
              Coupling(iRow_Q,iCol_P)=1
              Coupling(iRow_Q,iCol_A)=1
              Coupling(iRow_P,iCol_Q)=1
              Coupling(iRow_P,iCol_P)=1
              Coupling(iRow_P,iCol_A)=1
              Coupling(iRow_A,iCol_A)=1
              Coupling(iRow_A,iCol_Q)=1
              Coupling(iRow_A,iCol_P)=1


           End Do
      End Do

    Else
  !  4 variables Version Q,Pe,A,P (Viscoeleastic)
     iShiftPe = 1
     Do NodR = 1, 2
              iRow_Q  = iDofT*(NodR-1) + 1 
              iRow_Pe = iRow_Q + iShiftPe
              iRow_P  = iRow_Q + iShiftP
              iRow_A  = iRow_P - 1
!              Coupling(iRow_A,iRow_A)=1
!              Coupling(iRow_A,iRow_P)=1

          Do NodC = 1, 2
              iCol_Q  = iDofT*(NodC-1) + 1 
              iCol_Pe = iCol_Q + iShiftPe
              iCol_P  = iCol_Q + iShiftP
              iCol_A  = iCol_P - 1
              Coupling(iRow_Q,iCol_Q )=1
              Coupling(iRow_Q,iCol_P )=1
              Coupling(iRow_Q,iCol_Pe)=1
              Coupling(iRow_Q,iCol_A )=1
              Coupling(iRow_P,iCol_Q )=1
              Coupling(iRow_P,iCol_P )=1
              Coupling(iRow_P,iCol_Pe)=1
              Coupling(iRow_P,iCol_A )=1
              Coupling(iRow_A,iCol_A )=1
              Coupling(iRow_A,iCol_Q )=1
              Coupling(iRow_A,iCol_P )=1
              Coupling(iRow_A,iCol_Pe)=1
              Coupling(iRow_Pe,iCol_Q )=1
              Coupling(iRow_Pe,iCol_P )=1
              Coupling(iRow_Pe,iCol_Pe)=1
              Coupling(iRow_Pe,iCol_A )=1


           End Do
      End Do

    EndIf


Return
End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Pipe1DL4(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter ( iVersion=1)  !!! Controla la opcion de 3 o 4 variables   	 
 Integer   Coupling(MaxLRows,MaxLRows)
 Dimension CommonPar(*)
 
 iShiftP= nint(CommonPar(4))

  if ( iVersion == 0 ) then
  !  3 variables Version Q,A,P
     Do NodR = 1, 2
              iRow_Q = iDofT*(NodR-1) + 1 
              iRow_P = iRow_Q + iShiftP
              iRow_A = iRow_P - 1
!              Coupling(iRow_A,iRow_A)=1
!              Coupling(iRow_A,iRow_P)=1

          Do NodC = 1, 2
              iCol_Q = iDofT*(NodC-1) + 1 
              iCol_P = iCol_Q + iShiftP
              iCol_A = iCol_P - 1
              Coupling(iRow_Q,iCol_Q)=1
              Coupling(iRow_Q,iCol_P)=1
              Coupling(iRow_Q,iCol_A)=1
              Coupling(iRow_P,iCol_Q)=1
              Coupling(iRow_P,iCol_P)=1
              Coupling(iRow_P,iCol_A)=1
              Coupling(iRow_A,iCol_A)=1
              Coupling(iRow_A,iCol_Q)=1
              Coupling(iRow_A,iCol_P)=1


           End Do
      End Do

    Else
  !  4 variables Version Q,Pe,A,P (Viscoeleastic)
     iShiftPe = 1
     Do NodR = 1, 2
              iRow_Q  = iDofT*(NodR-1) + 1 
              iRow_Pe = iRow_Q + iShiftPe
              iRow_P  = iRow_Q + iShiftP
              iRow_A  = iRow_P - 1
!              Coupling(iRow_A,iRow_A)=1
!              Coupling(iRow_A,iRow_P)=1

          Do NodC = 1, 2
              iCol_Q  = iDofT*(NodC-1) + 1 
              iCol_Pe = iCol_Q + iShiftPe
              iCol_P  = iCol_Q + iShiftP
              iCol_A  = iCol_P - 1
              Coupling(iRow_Q,iCol_Q )=1
              Coupling(iRow_Q,iCol_P )=1
              Coupling(iRow_Q,iCol_Pe)=1
              Coupling(iRow_Q,iCol_A )=1
              Coupling(iRow_P,iCol_Q )=1
              Coupling(iRow_P,iCol_P )=1
              Coupling(iRow_P,iCol_Pe)=1
              Coupling(iRow_P,iCol_A )=1
              Coupling(iRow_A,iCol_A )=1
              Coupling(iRow_A,iCol_Q )=1
              Coupling(iRow_A,iCol_P )=1
              Coupling(iRow_A,iCol_Pe)=1
              Coupling(iRow_Pe,iCol_Q )=1
              Coupling(iRow_Pe,iCol_P )=1
              Coupling(iRow_Pe,iCol_Pe)=1
              Coupling(iRow_Pe,iCol_A )=1


           End Do
      End Do

    EndIf


Return
End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1DCE(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter ( iVersion=0) ! Controla la opcion de 3 o 4 variables    	 
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
 NodElMax = MaxLRows / iDofT

        iShiftP = nint(CommonPar(2))

        iRow_Qter = 1
        iRow_Pter = iRow_Qter + iShiftP
          Coupling(iRow_Pter,iRow_Pter) =	1
          Coupling(iRow_Pter,iRow_Qter) =	1
          Coupling(iRow_Qter,iRow_Qter) =	1

  if ( iVersion == 0 ) then
  !  3 variables Version Q,A,P
 
!          Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
!              iRow_QNod = iDofT*Nod + 1 
!              iRow_PNod = iRow_QNod + iShiftP
!              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
!              Coupling (iRow_PNod , iRow_PNod ) =  iADD
!              Coupling (iRow_PNod , iRow_Pter ) =  1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
!              Coupling (iRow_Qter,iRow_QNod) = 1
!              Coupling (iRow_Qter,iRow_ANod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
!              Coupling (iRow_Pter,iRow_QNod) = 1

!          End Do

    Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
              iRow_QNod = iDofT*Nod + 1 
              iRow_PNod = iRow_QNod + iShiftP
              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              Coupling (iRow_PNod , iRow_PNod ) =  iADD
              Coupling (iRow_PNod , iRow_QNod ) =  1
              Coupling (iRow_PNod , iRow_ANod ) =  1
              Coupling (iRow_QNod , iRow_PNod ) =  1
              Coupling (iRow_QNod , iRow_QNod ) =  1
              Coupling (iRow_QNod , iRow_ANod ) =  1
              Coupling (iRow_ANod , iRow_PNod ) =  1
              Coupling (iRow_ANod , iRow_QNod ) =  1
              Coupling (iRow_ANod , iRow_ANod ) =  1

              Coupling (iRow_PNod , iRow_Pter ) =  1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Qter,iRow_QNod) = 1
              Coupling (iRow_Qter,iRow_ANod) = 1
              Coupling (iRow_Qter,iRow_PNod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Pter,iRow_QNod) = 1
              Coupling (iRow_Pter,iRow_ANod) = 1
              Coupling (iRow_Pter,iRow_PNod) = 1

          Do NodC = Nod+1, NodElMax - 1
! los nodos estan desplazados en 1
              iCol_QNod = iDofT*NodC + 1 
              iCol_PNod = iCol_QNod + iShiftP
              iCol_ANod = iCol_PNod - 1
              Coupling (iRow_PNod , iCol_PNod ) =  1
              Coupling (iRow_PNod , iCol_QNod ) =  1
              Coupling (iRow_PNod , iCol_ANod ) =  1
              Coupling (iRow_QNod , iCol_PNod ) =  1
              Coupling (iRow_QNod , iCol_QNod ) =  1
              Coupling (iRow_QNod , iCol_ANod ) =  1
              Coupling (iRow_ANod , iCol_PNod ) =  1
              Coupling (iRow_ANod , iCol_QNod ) =  1
              Coupling (iRow_ANod , iCol_ANod ) =  1
            End do

  End Do


    Else
  !  4 variables Version Q,Pe,A,P (Viscoeleastic)
     iShiftPe = 1

    Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
              iRow_QNod = iDofT*Nod + 1 
              iRow_PNod = iRow_QNod + iShiftP
              iRow_PeNod= iRow_QNod + iShiftPe
              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              Coupling (iRow_PNod , iRow_PNod  ) =  iADD
              Coupling (iRow_PeNod , iRow_PeNod) =  1 ! iADD es no aditivo para imponer Pe=Pter
              Coupling (iRow_PNod , iRow_PeNod ) =  1
              Coupling (iRow_PNod , iRow_QNod  ) =  1
              Coupling (iRow_PNod , iRow_ANod  ) =  1

              Coupling (iRow_QNod , iRow_PNod  ) =  1
              Coupling (iRow_QNod , iRow_PeNod ) =  1
              Coupling (iRow_QNod , iRow_QNod  ) =  1
              Coupling (iRow_QNod , iRow_ANod  ) =  1

              Coupling (iRow_ANod , iRow_PNod  ) =  1
              Coupling (iRow_ANod , iRow_PeNod ) =  1
              Coupling (iRow_ANod , iRow_QNod  ) =  1
              Coupling (iRow_ANod , iRow_ANod  ) =  1

!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Qter,iRow_QNod)  = 1
              Coupling (iRow_Qter,iRow_ANod)  = 1
              Coupling (iRow_Qter,iRow_PNod)  = 1
              Coupling (iRow_Qter,iRow_PeNod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Pter,iRow_QNod)  = 1
              Coupling (iRow_Pter,iRow_ANod)  = 1
              Coupling (iRow_Pter,iRow_PNod)  = 1
              Coupling (iRow_Pter,iRow_PeNod) = 1

          Do NodC = Nod+1, NodElMax - 1
! los nodos estan desplazados en 1
              iCol_QNod = iDofT*NodC + 1 
              iCol_PNod = iCol_QNod + iShiftP
              iCol_PeNod= iCol_QNod + iShiftPe
              iCol_ANod = iCol_PNod - 1
              Coupling (iRow_PNod , iCol_PNod  ) =  1
              Coupling (iRow_PNod , iCol_PeNod ) =  1
              Coupling (iRow_PNod , iCol_QNod  ) =  1
              Coupling (iRow_PNod , iCol_ANod  ) =  1

              Coupling (iRow_PeNod , iCol_PNod  ) =  1
              Coupling (iRow_PeNod , iCol_PeNod ) =  1
              Coupling (iRow_PeNod , iCol_QNod  ) =  1
              Coupling (iRow_PeNod , iCol_ANod  ) =  1

              Coupling (iRow_QNod , iCol_PNod  ) =  1
              Coupling (iRow_QNod , iCol_PeNod ) =  1
              Coupling (iRow_QNod , iCol_QNod  ) =  1
              Coupling (iRow_QNod , iCol_ANod  ) =  1

              Coupling (iRow_ANod , iCol_PNod  ) =  1
              Coupling (iRow_ANod , iCol_PeNod ) =  1
              Coupling (iRow_ANod , iCol_QNod  ) =  1
              Coupling (iRow_ANod , iCol_ANod  ) =  1
            End do

  End Do



    Endif



return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1DCEL4(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter ( iVersion=1) ! Controla la opcion de 3 o 4 variables    	 
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
 NodElMax = MaxLRows / iDofT

        iShiftP = nint(CommonPar(2))

        iRow_Qter = 1
        iRow_Pter = iRow_Qter + iShiftP
          Coupling(iRow_Pter,iRow_Pter) =	1
          Coupling(iRow_Pter,iRow_Qter) =	1
          Coupling(iRow_Qter,iRow_Qter) =	1

  if ( iVersion == 0 ) then
  !  3 variables Version Q,A,P
 
!          Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
!              iRow_QNod = iDofT*Nod + 1 
!              iRow_PNod = iRow_QNod + iShiftP
!              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
!              Coupling (iRow_PNod , iRow_PNod ) =  iADD
!              Coupling (iRow_PNod , iRow_Pter ) =  1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
!              Coupling (iRow_Qter,iRow_QNod) = 1
!              Coupling (iRow_Qter,iRow_ANod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
!              Coupling (iRow_Pter,iRow_QNod) = 1

!          End Do

    Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
              iRow_QNod = iDofT*Nod + 1 
              iRow_PNod = iRow_QNod + iShiftP
              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              Coupling (iRow_PNod , iRow_PNod ) =  iADD
              Coupling (iRow_PNod , iRow_QNod ) =  1
              Coupling (iRow_PNod , iRow_ANod ) =  1
              Coupling (iRow_QNod , iRow_PNod ) =  1
              Coupling (iRow_QNod , iRow_QNod ) =  1
              Coupling (iRow_QNod , iRow_ANod ) =  1
              Coupling (iRow_ANod , iRow_PNod ) =  1
              Coupling (iRow_ANod , iRow_QNod ) =  1
              Coupling (iRow_ANod , iRow_ANod ) =  1

              Coupling (iRow_PNod , iRow_Pter ) =  1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Qter,iRow_QNod) = 1
              Coupling (iRow_Qter,iRow_ANod) = 1
              Coupling (iRow_Qter,iRow_PNod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Pter,iRow_QNod) = 1
              Coupling (iRow_Pter,iRow_ANod) = 1
              Coupling (iRow_Pter,iRow_PNod) = 1

          Do NodC = Nod+1, NodElMax - 1
! los nodos estan desplazados en 1
              iCol_QNod = iDofT*NodC + 1 
              iCol_PNod = iCol_QNod + iShiftP
              iCol_ANod = iCol_PNod - 1
              Coupling (iRow_PNod , iCol_PNod ) =  1
              Coupling (iRow_PNod , iCol_QNod ) =  1
              Coupling (iRow_PNod , iCol_ANod ) =  1
              Coupling (iRow_QNod , iCol_PNod ) =  1
              Coupling (iRow_QNod , iCol_QNod ) =  1
              Coupling (iRow_QNod , iCol_ANod ) =  1
              Coupling (iRow_ANod , iCol_PNod ) =  1
              Coupling (iRow_ANod , iCol_QNod ) =  1
              Coupling (iRow_ANod , iCol_ANod ) =  1
            End do

  End Do


    Else
  !  4 variables Version Q,Pe,A,P (Viscoeleastic)
     iShiftPe = 1

    Do Nod = 1, NodElMax - 1
! los nodos estan desplazados en 1
              iRow_QNod = iDofT*Nod + 1 
              iRow_PNod = iRow_QNod + iShiftP
              iRow_PeNod= iRow_QNod + iShiftPe
              iRow_ANod = iRow_PNod - 1
!-------------Ecuacion de igualdad de presiones Pnod=Pter-----------
              Coupling (iRow_PNod , iRow_PNod  ) =  iADD
              Coupling (iRow_PeNod , iRow_PeNod) =  1 ! iADD es no aditivo para imponer Pe=Pter
              Coupling (iRow_PNod , iRow_PeNod ) =  1
              Coupling (iRow_PNod , iRow_QNod  ) =  1
              Coupling (iRow_PNod , iRow_ANod  ) =  1

              Coupling (iRow_QNod , iRow_PNod  ) =  1
              Coupling (iRow_QNod , iRow_PeNod ) =  1
              Coupling (iRow_QNod , iRow_QNod  ) =  1
              Coupling (iRow_QNod , iRow_ANod  ) =  1

              Coupling (iRow_ANod , iRow_PNod  ) =  1
              Coupling (iRow_ANod , iRow_PeNod ) =  1
              Coupling (iRow_ANod , iRow_QNod  ) =  1
              Coupling (iRow_ANod , iRow_ANod  ) =  1

!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Qter,iRow_QNod)  = 1
              Coupling (iRow_Qter,iRow_ANod)  = 1
              Coupling (iRow_Qter,iRow_PNod)  = 1
              Coupling (iRow_Qter,iRow_PeNod) = 1
!vvvvvvvvvvvvv<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>vvvvvvvvvvvvvvvvvvvv
              Coupling (iRow_Pter,iRow_QNod)  = 1
              Coupling (iRow_Pter,iRow_ANod)  = 1
              Coupling (iRow_Pter,iRow_PNod)  = 1
              Coupling (iRow_Pter,iRow_PeNod) = 1

          Do NodC = Nod+1, NodElMax - 1
! los nodos estan desplazados en 1
              iCol_QNod = iDofT*NodC + 1 
              iCol_PNod = iCol_QNod + iShiftP
              iCol_PeNod= iCol_QNod + iShiftPe
              iCol_ANod = iCol_PNod - 1
              Coupling (iRow_PNod , iCol_PNod  ) =  1
              Coupling (iRow_PNod , iCol_PeNod ) =  1
              Coupling (iRow_PNod , iCol_QNod  ) =  1
              Coupling (iRow_PNod , iCol_ANod  ) =  1

              Coupling (iRow_PeNod , iCol_PNod  ) =  1
              Coupling (iRow_PeNod , iCol_PeNod ) =  1
              Coupling (iRow_PeNod , iCol_QNod  ) =  1
              Coupling (iRow_PeNod , iCol_ANod  ) =  1

              Coupling (iRow_QNod , iCol_PNod  ) =  1
              Coupling (iRow_QNod , iCol_PeNod ) =  1
              Coupling (iRow_QNod , iCol_QNod  ) =  1
              Coupling (iRow_QNod , iCol_ANod  ) =  1

              Coupling (iRow_ANod , iCol_PNod  ) =  1
              Coupling (iRow_ANod , iCol_PeNod ) =  1
              Coupling (iRow_ANod , iCol_QNod  ) =  1
              Coupling (iRow_ANod , iCol_ANod  ) =  1
            End do

  End Do



    Endif



return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1D_LWIN(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 Parameter(iOpcionQAPeP = 0)
 iShiftP= nint(CommonPar(4))
 Nod=1
 iRow_Q = (Nod-1)*iDofT + 1
 iRow_P = iRow_Q + iShiftP
 iRow_A = iRow_P - 1
  if (iOpcionQAPeP /=1) then
  !Lax-Wendroff
        Coupling(iRow_Q,iRow_A) =	1
        Coupling(iRow_Q,iRow_Q) =	iAdd
        Coupling(iRow_A,iRow_A) =	iAdd
        Coupling(iRow_A,iRow_P) =	1
    else
   !LS QAPeP

        iRow_Pe= iRow_Q + 1
        Coupling(iRow_Pe,iRow_Pe) =	iAdd
        Coupling(iRow_Pe,iRow_P) =	1
        Coupling(iRow_Pe,iRow_Q ) =	1
        Coupling(iRow_Pe,iRow_Q+iDofT) =	1
        Coupling(iRow_Pe,iRow_A) =	1

        Coupling(iRow_A,iRow_A ) =	iAdd
        Coupling(iRow_A,iRow_Pe) =	1
        Coupling(iRow_A,iRow_P) =	1

    endif
return
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1D_LWOUT(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter(iOpcionQAPeP = 0)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
 iShiftP= nint(CommonPar(4))
 Nod=2
 iRow_Q = (Nod-1)*iDofT + 1
 iRow_P = iRow_Q + iShiftP
 iRow_A = iRow_P - 1
  if (iOpcionQAPeP /=1) then
  !Lax-Wendroff
        Coupling(iRow_Q,iRow_A) =	1
        Coupling(iRow_Q,iRow_Q) =	iAdd
        Coupling(iRow_A,iRow_A) =	iAdd
        Coupling(iRow_A,iRow_P) =	1
    else
   !LS QAPeP

        iRow_Pe= iRow_Q + 1
        Coupling(iRow_Pe,iRow_Pe) =	iAdd
        Coupling(iRow_Pe,iRow_P) =	1
        Coupling(iRow_Pe,iRow_Q ) =	1
        Coupling(iRow_Pe,iRow_Q-iDofT) =	1
        Coupling(iRow_Pe,iRow_A) =	1

        Coupling(iRow_A,iRow_A ) =	iAdd
        Coupling(iRow_A,iRow_Pe) =	1
        Coupling(iRow_A,iRow_P) =	1

    endif


return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1D_L4IN(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 Parameter(iOpcionQAPeP = 1)
 iShiftP= nint(CommonPar(4))
 Nod=1
 iRow_Q = (Nod-1)*iDofT + 1
 iRow_P = iRow_Q + iShiftP
 iRow_A = iRow_P - 1
  if (iOpcionQAPeP /=1) then
  !Lax-Wendroff
        Coupling(iRow_Q,iRow_A) =	1
        Coupling(iRow_Q,iRow_Q) =	iAdd
        Coupling(iRow_A,iRow_A) =	iAdd
        Coupling(iRow_A,iRow_P) =	1
    else
   !LS QAPeP

        iRow_Pe= iRow_Q + 1
        Coupling(iRow_Pe,iRow_Pe) =	iAdd
        Coupling(iRow_Pe,iRow_P) =	1
        Coupling(iRow_Pe,iRow_Q ) =	1
        Coupling(iRow_Pe,iRow_Q+iDofT) =	1
        Coupling(iRow_Pe,iRow_A) =	1

        Coupling(iRow_A,iRow_A ) =	iAdd
        Coupling(iRow_A,iRow_Pe) =	1
        Coupling(iRow_A,iRow_P) =	1

    endif
return
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Pipe1D_L4OUT(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter(iOpcionQAPeP = 1)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
 iShiftP= nint(CommonPar(4))
 Nod=2
 iRow_Q = (Nod-1)*iDofT + 1
 iRow_P = iRow_Q + iShiftP
 iRow_A = iRow_P - 1
  if (iOpcionQAPeP /=1) then
  !Lax-Wendroff
        Coupling(iRow_Q,iRow_A) =	1
        Coupling(iRow_Q,iRow_Q) =	iAdd
        Coupling(iRow_A,iRow_A) =	iAdd
        Coupling(iRow_A,iRow_P) =	1
    else
   !LS QAPeP

        iRow_Pe= iRow_Q + 1
        Coupling(iRow_Pe,iRow_Pe) =	iAdd
        Coupling(iRow_Pe,iRow_P) =	1
        Coupling(iRow_Pe,iRow_Q ) =	1
        Coupling(iRow_Pe,iRow_Q-iDofT) =	1
        Coupling(iRow_Pe,iRow_A) =	1

        Coupling(iRow_A,iRow_A ) =	iAdd
        Coupling(iRow_A,iRow_Pe) =	1
        Coupling(iRow_A,iRow_P) =	1

    endif


return
end









!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Pipe1DLS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
            iShiftP= nint(CommonPar(4))

            Do i=1,2
                ipRow  = (i-1)*iDofT
                ipRowQ = ipRow + 1
                ipRowP = ipRowQ + iShifTP
	          ipRowA = ipRowP - 1

               Do j=1,2
                 ipCol    = (j-1)*iDofT 
                 ipColQ   = ipCol + 1
                 ipColP   = ipColQ + iShifTP
	           ipColA	= ipColP - 1

!      Aca comienza el llenado de las matrices	
                 Coupling(ipRowQ , ipColQ) = 1
                 Coupling(ipRowQ , ipColA) = 1
                 Coupling(ipRowQ , ipColP) = 1
                 Coupling(ipRowA , ipColQ) = 1  
	           Coupling(ipRowA , ipColA) = 1  
	           Coupling(ipRowA , ipColP) = 1  
	           Coupling(ipRowP , ipColQ) = 1  
	           Coupling(ipRowP , ipColA) = 1  
	           Coupling(ipRowP , ipColP) = 1  

            End do 

!-----------------------------------------------
          End do 
!      LoopArea: Do i=1,NodEL

	    i = 1
            ipRow  = (i-1)*iDofT + 1
            ipRowP = ipRow + iShifTP
            ipRowA = ipRowP - 1
            ipColP = ipRowP
	      ipColA = ipRowA

	    Coupling (ipRowA,ipColP) = 1
          Coupling (ipRowA,ipColA) = 1
	    Coupling (ipRowP,ipColP) = 1
	    Coupling (ipRowP,ipColA) = 1

	    i = 2
            ipRow  = (i-1)*iDofT + 1
            ipRowP = ipRow + iShifTP
            ipRowA = ipRowP - 1
  	      ipColP = ipRowP
	      ipColA = ipRowA

     	    Coupling (ipRowA,ipColP) = 1
	    Coupling (ipRowA,ipColA) = 1
	    Coupling (ipRowP,ipColP) = 1
	    Coupling (ipRowP,ipColA) = 1
return
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup2D1DNVS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
      nVertices = 2
      nAristas = 1
      NodV= nAristas + nVertices
      NodP = nVertices

       ipRowT    =  0 
       ipRowQT   =  ipRowT  + 1
       ipColPT   =  ipRowQT + NDim
       Coupling(ipRowQT,ipRowQT) = iAdd
!       Coupling(ipRowQT,ipColPT) = 1

      Do NodR=1, NodV
       ipRow   = NodR*iDofT 
         Do ncv=1, NDim
           ipRowV = ipRow + ncv
           Coupling(ipRowQT,ipRowV) = 1
           Coupling(ipRowV ,ipRowV) = 1
           Coupling(ipRowV,ipColPT) = 1

         EndDo
	EndDo


return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Coup2D1DNVTerS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
       ipRowQT   = 1
       ipRowPT   = ipRowQT + NDim

       Coupling(ipRowQT,ipRowQT) = iAdd

       Do ipCol=ipRowQT+1,ipRowPT
     !                            Coupling(ipRowQT,ipCol) = 1
       enddo

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Coup2D1DNVTer_AlfaS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 
       ipRowQT    = 1
       ipRowPT    = ipRowQT + NDim
	   ipRowAlfaT = ipRowPT + 1
       
	   Coupling(ipRowQT,ipRowQT)       = iAdd
       Coupling(ipRowQT,ipRowPT)       = 1
       Coupling(ipRowAlfaT,ipRowAlfaT) = iAdd

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Subroutine Coup2D1DNVTerSS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

       ipRowQT   = 1
       ipRowPT   = ipRowQT + NDim
       Coupling(ipRowQT,ipRowQT) = iAdd
       Coupling(ipRowPT,ipRowPT) = 1
       Coupling(ipRowPT,ipRowQT) = 1
       Coupling(ipRowQT,ipRowPT) = 1


return
end



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine NavierStokesBubALES(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 NDim1=NDim+1

  nVertices = NDim1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
! ojo esta formula solo vale para NDim=2 o 3
      nAristas = nVertices + (NDim-2)*2
!      NodEl= nAristas + nVertices
      NodEl= nVertices + 1
!    NodEl= nVertices
!	NodEl1=NodEl+1
	NdimE  = Ndim
	NdimE1 = NdimE+1
    NodP   = nVertices 
      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowP = ipRow + NdimE1 

        LoopCol: Do j=1,NodEL
         ipCol  = (j-1)*iDofT 
         ipColP = ipCol + NdimE1 

         Do ndRow=1,NdimE
              ipRowV = ipRow  + ndRow
              ipColV = ipCol  + ndRow
              ipRowX = ipRowP + ndRow
              ipColX = ipColP + ndRow
 
              Coupling( ipRowV , ipColV ) =  1
              if(j<=NodP)Coupling( ipRowV , ipColP ) =  1
 
!              Do ndCol = ndRow, NdimE
!               ipColX  = ipColP + ndCol
!    OJO acoplo mas de lo necesario
               if(i<=NodP .and. j<=NodP)Coupling( ipRowX , ipColX )=  1 
!              Enddo                     
          Enddo
!       Pressure Mass Matrix
            if(i<=NodP .and. j<=NodP)Coupling( ipRowP, ipColP ) =  1


        End do LoopCol

!-----------------------------------------------
       End do LoopRow

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine NavierStokesBubALES1(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 NDim1=NDim+1

  nVertices = NDim1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
! ojo esta formula solo vale para NDim=2 o 3
      nAristas = nVertices + (NDim-2)*2
!      NodEl= nAristas + nVertices
      NodEl= nVertices 
!    NodEl= nVertices
!	NodEl1=NodEl+1
	NdimE  = Ndim
	NdimE1 = NdimE+1
    NodP   = nVertices 
      LoopRow: Do i=1,NodEL
        ipRow  = (i-1)*iDofT
        ipRowP = ipRow + NdimE1 

        LoopCol: Do j=1,NodEL
         ipCol  = (j-1)*iDofT 
         ipColP = ipCol + NdimE1 

         Do ndRow=1,NdimE
              ipRowV = ipRow  + ndRow
              ipColV = ipCol  + ndRow
              ipRowX = ipRowP + ndRow
              ipColX = ipColP + ndRow
 
              Coupling( ipRowV , ipColV ) =  1
              if(j<=NodP)Coupling( ipRowV , ipColP ) =  1
 
              if(i<=NodP .and. j<=NodP)Coupling( ipRowX , ipColX )=  1 
!              Do ndCol = ndRow, NdimE
!               ipColX  = ipColP + ndCol
!    OJO acoplo mas de lo necesario
!               if(i<=NodP .and. j<=NodP)Coupling( ipRowX , ipColX )=  1 
!              Enddo                     
          Enddo
!       Pressure Mass Matrix
            if(i<=NodP .and. j<=NodP)Coupling( ipRowP, ipColP ) =  1


        End do LoopCol

!-----------------------------------------------
       End do LoopRow

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine NavierStokesBubALES2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 NDim1=NDim+1

  nVertices = NDim1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
! ojo esta formula solo vale para NDim=2 o 3
      nAristas = nVertices + (NDim-2)*2
!      NodEl= nAristas + nVertices
      NodEl= nVertices + 1
!    NodEl= nVertices
!	NodEl1=NodEl+1
	NdimE  = Ndim
	NdimE1 = NdimE+1
    NodP   = nVertices 

!-----------------------------------------------
        ipRow  = (NodEL-1)*iDofT
         Do ndRow=1,NdimE
              ipRowV = ipRow  + ndRow
              ipColV = ipRowV
              Coupling( ipRowV , ipColV ) =  1
         Enddo
!-----------------------------------------------



return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine NavierStokesBubALESD2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 NDim1=NDim+1

  nVertices = NDim1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
! ojo esta formula solo vale para NDim=2 o 3
      nAristas = nVertices + (NDim-2)*2
!      NodEl= nAristas + nVertices
      NodEl= nVertices + 1
!    NodEl= nVertices
!	NodEl1=NodEl+1
	NdimE  = Ndim
	NdimE1 = NdimE+1
    NodP   = nVertices 

    ipRowP = Ndim + 1
    Coupling( ipRowP , ipRowP ) =  1
!-----------------------------------------------

      LoopRow: Do i=1,NodP
        ipRow  = (i-1)*iDofT
        ipRowP = ipRow + NdimE1 

        LoopCol: Do j=1,NodP
         ipCol  = (j-1)*iDofT 
         ipColP = ipCol + NdimE1 

         Do ndRow=1,NdimE
              ipRowV = ipRow  + ndRow
              ipColV = ipCol  + ndRow
              ipRowX = ipRowP + ndRow
              ipColX = ipColP + ndRow
 
!              Coupling( ipRowV , ipColV ) =  1
!              if(j<=NodP)Coupling( ipRowV , ipColP ) =  1
!              Do ndCol = ndRow, NdimE
!               ipColX  = ipColP + ndCol
!    OJO acoplo mas de lo necesario
!!descomentar para el split if(i<=NodP .and. j<=NodP)Coupling( ipRowX , ipColX )=  1 
!              Enddo                     
          Enddo
!       Pressure Mass Matrix
!            if(i<=NodP .and. j<=NodP)Coupling( ipRowP, ipColP ) =  1
        End do LoopCol
!-----------------------------------------------
       End do LoopRow

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Presure_NV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
      NodShift = nint(CommonPar(2))
 NodEL     =  NDim
 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
! Coupling ( ipRowQT , ipRowQT ) = iAdd
! Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
! Coupling ( ipRowQT , ipColPT ) = 1
! Coupling ( ipColPT , ipRowQT ) = 1
 Do NodR=1, NodEL
      ipRow   = (NodR-1+NodShift)*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto de Cholezki
!      Coupling ( ipRowQT , ipRowP  ) = 1
!      Coupling ( ipColPT , ipRowP  ) = 1
!      Coupling ( ipRowP  , ipRowQT ) = 1
!      Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
          Coupling ( ipRowV  , ipRowV ) = 1
!         Coupling ( ipRowQT , ipRowV  ) = 1
!         Coupling ( ipRowV  , ipColPT ) = 1
!         Coupling ( ipRowV  , ipRowQT ) = 1
!         Coupling ( ipColPT , ipRowV  ) = 1
      Enddo
 Enddo

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup_23D1D_NV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

 NodEL     =  NDim
 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
 Coupling ( ipRowQT , ipRowQT ) = iAdd
 Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
 Coupling ( ipRowQT , ipColPT ) = 1
 Coupling ( ipColPT , ipRowQT ) = 1
 Do NodR=1, NodEL
      ipRow   = NodR*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto del precondicionador
      Coupling ( ipRowQT , ipRowP  ) = 1
      Coupling ( ipColPT , ipRowP  ) = 1
      Coupling ( ipRowP  , ipRowQT ) = 1
      Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
         Coupling ( ipRowQT , ipRowV  ) = 1
         Coupling ( ipRowV  , ipColPT ) = 1
         Coupling ( ipRowV  , ipRowQT ) = 1
         Coupling ( ipColPT , ipRowV  ) = 1
      Enddo
      Do NodC=1, NodEL
       ipCol   = NodC*iDofT 
       ipColP  = ipCol + NDim1 
       Coupling ( ipRowP  , ipColP ) = 1
              Do nci=1, NDim
                 ipRowVi = ipRow + nci
                  Do ncj=1, NDim
                     ipColVj = ipCol + ncj
                     Coupling ( ipRowVi  , ipColVj ) = 1
                  Enddo
              Enddo
      Enddo
 Enddo

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup_23D1D_NV_LinearDS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

 NodEL     =  NDim
 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
 Coupling ( ipRowQT , ipRowQT ) = iAdd
! Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
! Coupling ( ipRowQT , ipColPT ) = 1
! Coupling ( ipColPT , ipRowQT ) = 1
 Do NodR=1, NodEL
      ipRow   = NodR*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto del precondicionador
!      Coupling ( ipRowQT , ipRowP  ) = 1
!      Coupling ( ipColPT , ipRowP  ) = 1
!      Coupling ( ipRowP  , ipRowQT ) = 1
!      Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
!         Coupling ( ipRowQT , ipRowV  ) = 1
!         Coupling ( ipRowV  , ipColPT ) = 1
!         Coupling ( ipRowV  , ipRowQT ) = 1
         Coupling ( ipRowV  , ipRowV ) = 1
!         Coupling ( ipColPT , ipRowV  ) = 1

      Enddo

      Do NodC=1, NodEL
       ipCol   = NodC*iDofT 
       ipColP  = ipCol + NDim1 
     !  Coupling ( ipRowP  , ipColP ) = 1
              Do nci=1, NDim
                 ipRowVi = ipRow + nci
                  Do ncj=1, NDim
                     ipColVj = ipCol + ncj
!                     Coupling ( ipRowVi  , ipColVj ) = 1
                  Enddo
              Enddo
      Enddo
 Enddo

return
end




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup_23D1D_NV_LinearDDS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

 NodEL     =  NDim
 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
 Coupling ( ipRowQT , ipRowQT ) = iAdd
! Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
! Coupling ( ipRowQT , ipColPT ) = 1
! Coupling ( ipColPT , ipRowQT ) = 1
 Do NodR=1, NodEL
      ipRow   = NodR*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto del precondicionador
!      Coupling ( ipRowQT , ipRowP  ) = 1
!      Coupling ( ipColPT , ipRowP  ) = 1
!      Coupling ( ipRowP  , ipRowQT ) = 1
!      Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
!         Coupling ( ipRowQT , ipRowV  ) = 1
!         Coupling ( ipRowV  , ipColPT ) = 1
!         Coupling ( ipRowV  , ipRowQT ) = 1
!         Coupling ( ipColPT , ipRowV  ) = 1
      Enddo
      Do NodC=1, NodEL
       ipCol   = NodC*iDofT 
       ipColP  = ipCol + NDim1 
!       Coupling ( ipRowP  , ipColP ) = 1
              Do nci=1, NDim
                 ipRowVi = ipRow + nci
                  Do ncj=1, NDim
                     ipColVj = ipCol + ncj
!                     Coupling ( ipRowVi  , ipColVj ) = 1
                  Enddo
              Enddo
      Enddo
 Enddo

return
end







!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup_23D1D_NV_Linear_AlfaS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

 NodEL     =  NDim
 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
 Coupling ( ipRowQT , ipRowQT ) = iAdd
 Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
 Coupling ( ipRowQT , ipColPT ) = 1
 Coupling ( ipColPT , ipRowQT ) = 1
 Do NodR=1, NodEL
      ipRow   = NodR*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto del precondicionador
      Coupling ( ipRowQT , ipRowP  ) = 1
      Coupling ( ipColPT , ipRowP  ) = 1
      Coupling ( ipRowP  , ipRowQT ) = 1
      Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
         Coupling ( ipRowQT , ipRowV  ) = 1
         Coupling ( ipRowV  , ipColPT ) = 1
         Coupling ( ipRowV  , ipRowQT ) = 1
         Coupling ( ipColPT , ipRowV  ) = 1
      Enddo
      Do NodC=1, NodEL
       ipCol   = NodC*iDofT 
       ipColP  = ipCol + NDim1 
       Coupling ( ipRowP  , ipColP ) = 1
              Do nci=1, NDim
                 ipRowVi = ipRow + nci
                  Do ncj=1, NDim
                     ipColVj = ipCol + ncj
                     Coupling ( ipRowVi  , ipColVj ) = 1
                  Enddo
              Enddo
      Enddo
 Enddo

!Acoplo el el Alfa con los grados de libertad de velocidad
!Pero no acoplo en la diagonal del alfa porque lo pongo en el
!elemento terminal correspondiente (de un solo nodo) ya que sino
!sumar뛞 un 1 por cada vez que entra en este elemento, y tendr뛞
!que dividir por la cantidad de elementos de la entrada.

 ipRowAlfaT = NDim + 1 + 1  !NDim + P + AlfaTerminal
 ipColAlfaT = ipRowAlfaT
! Se acopla en el elemento 23 BIS (el 23 modificado para calcular Alfa)
 Coupling ( ipRowAlfaT  , ipColAlfaT ) = iAdd
 
 NodElTer = iDofT
 ipColIni = NodElTer
 
 Do i=1, NodEL
   ipCol1 = ipColIni + (i-1)*iDofT
   Do nci=1, NDim
     ipColVel = ipCol1 + nci
     Coupling ( ipRowAlfaT , ipColVel ) = 1
   EndDo
 EndDo

return
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Coup_23D1D_NV_QuadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

  NDim1 = NDim + 1
  NdimE  = Ndim-1
  NdimE1 = NdimE+1

  nVertices = NdimE1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
! ojo esta formula solo vale para NDim=2 o 3
  nAristas = 1
  if( NdimE > 1 )nAristas = nVertices
  NodEl= nAristas + nVertices
!    NodEl= nVertices
!	NodEl1=NodEl+1
  NodP   = nVertices 

 ipRowT    =  0 
 ipRowQT   =  ipRowT  + 1
 ipColPT   =  ipRowQT + NDim
 NDim1     = NDim + 1
 Coupling ( ipRowQT , ipRowQT ) = iAdd
 Coupling ( ipColPT , ipColPT ) = 1
! Este acoplamiento no es necesario,se pone para el LU incompleto
 Coupling ( ipRowQT , ipColPT ) = 1
 Coupling ( ipColPT , ipRowQT ) = 1

 Do NodR=1, NodEL
      ipRow   = NodR*iDofT 
      ipRowP  = ipRow + NDim1 
! Estos dos acoplamientos no son necesarios desde el punto de vista de las ecuaciones
! Los pongo para mejorar el Factoreo incompleto de Cholezki
      if(NodR<=NodP) Coupling ( ipRowQT , ipRowP  ) = 1
      if(NodR<=NodP) Coupling ( ipColPT , ipRowP  ) = 1
      if(NodR<=NodP) Coupling ( ipRowP  , ipRowQT ) = 1
      if(NodR<=NodP) Coupling ( ipRowP  , ipColPT ) = 1
      Do ncv=1, NDim
         ipRowV = ipRow + ncv
         Coupling ( ipRowQT , ipRowV  ) = 1
         Coupling ( ipRowV  , ipColPT ) = 1
         Coupling ( ipRowV  , ipRowQT ) = 1
         Coupling ( ipColPT , ipRowV  ) = 1
      Enddo
      Do NodC=1, NodEL
       ipCol   = NodC*iDofT 
!       ipColP  = ipCol + NDim1 
!       Coupling ( ipRowP  , ipColP ) = 1
              Do nci=1, NDim
                 ipRowVi = ipRow + nci
                  Do ncj=1, NDim
                     ipColVj = ipCol + ncj
                     Coupling ( ipRowVi  , ipColVj ) = 1
                  Enddo
              Enddo
      Enddo
 Enddo

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Spring23DNV_LinearS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 Incremental = nint( CommonPar(11) ) 

    NodEl  = Ndim
    NdimG  = Ndim
    NdimG1 = Ndim+1
 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
        Coupling( ipRowV , ipRowV ) = iAdd
        Coupling( ipRowV , ipRowX ) = 1
        Coupling( ipRowV , ipRowP ) = 1

        Coupling( ipRowX , ipRowX ) = iAdd
        Coupling( ipRowX , ipRowV ) = 1
        Coupling( ipRowX , ipRowP ) = 1

       Enddo
 Enddo

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Spring23DNV_LinearDesS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)
 Incremental = nint( CommonPar(11) ) 

    NodEl  = Ndim
    NdimG  = Ndim
    NdimG1 = Ndim+1
 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 

       Coupling( ipRow+1 , ipRow+1 ) = iAdd
	   Coupling( ipRow+2 , ipRow+2 ) = iAdd
	   Coupling( ipRow+3 , ipRow+3 ) = 1

       do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
!       Coupling( ipRowV , ipRowV ) = iAdd
        Coupling( ipRowV , ipRowX ) = 1
        Coupling( ipRowV , ipRowP ) = 1

        Coupling( ipRowX , ipRowX ) = iAdd
        Coupling( ipRowX , ipRowV ) = 1
        Coupling( ipRowX , ipRowP ) = 1

       Enddo
 Enddo

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Spring3DNV_LinearSplit1S(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

    NodEl  = Ndim
    NdimG  = Ndim
    NdimG1 = Ndim+1
 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
        Coupling( ipRowV , ipRowV ) = iAdd
!        Coupling( ipRowV , ipRowX ) = 1
        Coupling( ipRowV , ipRowP ) = 1
!        Coupling( ipRowX , ipRowX ) = iAdd
!        Coupling( ipRowX , ipRowP ) = 1

       Enddo
 Enddo

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Spring3DNV_LinearSplit2S(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

    NodEl  = Ndim
    NdimG  = Ndim
    NdimG1 = Ndim+1
 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
!        Coupling( ipRowV , ipRowV ) = iAdd
!        Coupling( ipRowV , ipRowX ) = 1
!        Coupling( ipRowV , ipRowP ) = 1
        Coupling( ipRowX , ipRowX ) = iAdd
!        Coupling( ipRowX , ipRowP ) = 1

       Enddo
 Enddo

return
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine Spring23DNV_QuadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

  NDim1 = NDim + 1
  NdimE  = Ndim-1
  NdimE1 = NdimE+1

  nVertices = NdimE1
!  nAristas de un simplex = COMBINACIONES (nVertices tomados de a 2) sin repeticiones (n m)= n!/(m!.(n-m)!)
  nAristas = 1
  if( NdimE > 1 )nAristas = nVertices
  NodEl= nAristas + nVertices
!    NodEl= nVertices
!	NodEl1=NodEl+1
  NodP   = nVertices 
  NdimG  = Ndim
  NdimG1 = Ndim+1

 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
!        Coupling( ipRowV , ipRowV ) = 1
!        Coupling( ipRowV , ipRowX ) = 1
        if(NodR<=NodP)Coupling( ipRowX , ipRowP ) = 1

             do NodC     = 1, NodEL
                ipCol    = (NodC-1)* iDofT 
                ipColP   =  ipCol  + NdimG1 
               do nsj   = 1, NdimG
                ipColV   =  ipCol  + nsj
                ipColX   =  ipColP + nsj
                !se ponen mas acoplamientos de los necesarios 
                Coupling( ipRowV , ipColV ) = 1
                Coupling( ipRowX , ipColX ) = 1
                Coupling( ipRowX , ipColV ) = 1
                Coupling( ipRowV , ipColX ) = 1
                if(NodR<=NodP)Coupling( ipColX , ipRowP ) = 1
                if(NodR<=NodP)Coupling( ipColV , ipRowP ) = 1
                if(NodR<=NodP)Coupling( ipRowP , ipColX ) = 1
                if(NodR<=NodP)Coupling( ipRowP , ipColV ) = 1
               Enddo   
             Enddo   
        Enddo
 Enddo


 do NodR   = 1, NodEL
       ipRow    = (NodR-1)* iDofT 
       ipRowP   =  ipRow  + NdimG1 
      do nsd   = 1, NdimG
        ipRowV  = ipRow  + nsd
        ipRowX  = ipRowP + nsd
!        Coupling( ipRowV , ipRowX ) = 1
         Coupling( ipRowV , ipRowV ) = iAdd
         Coupling( ipRowX , ipRowX ) = iAdd
        if(NodR<=NodP) then 
!              Coupling( ipRowX , ipRowP ) = 1
        endif
!             do NodC     = 1, NodEL
!                ipCol    = (NodC-1)* iDofT 
!                ipColP   =  ipCol  + NdimG1 
!                ipColX   =  ipColP + nsd
                !se ponen mas acoplamientos de los necesarios 
                ! falta acoplar las presiones con los nodos vertices               
!                if(NodC/=NodR)Coupling( ipRowX , ipColX ) = 1
!                if(NodR<=NodP)Coupling( ipColX , ipRowP ) = 1
!             Enddo   
     Enddo
 Enddo
return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine DirichS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

!       NodR=1
!       ipRow   = (NodR-1)*iDofT 
   IShift= nint(CommonPar(2))
        ipRow   = 0 
       Do nci=1, iDofT
            if ( nint(CommonPar(IShift+nci)) .NE. 0 ) then
                   ipRowV  = ipRow + nci
                   ipColV  = ipRowV
                   Coupling( ipRowV , ipColV ) = iAdd
            end if
       end do

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine DirichO(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

!       NodR=1
!       ipRow   = (NodR-1)*iDofT 
       ipRow = nint(CommonPar(2))-1 
       Do nci=1, Ndim
                   ipRowX  = ipRow + nci
         Do ncj=1, Ndim
                   ipColX  = ipRow + ncj
                   Coupling( ipRowX , ipColX ) = 1
         end do
       end do

return
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine DirichO2(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

!       NodR=1
!       ipRow   = (NodR-1)*iDofT 
	iShift= nint(CommonPar(2))-1
	iRow  = nint(CommonPar(7))
!------------------------------------

       Do nci  = 1, Ndim
        iCol = iShift + nci
        Coupling( iRow , iCol ) = 1
       end do

       Coupling( iRow , iRow ) = iAdd

return
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine ConcentratedLoadS(Coupling,CommonPar,iDofT,Ndim,MaxLRows,iAdd) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)
 Integer   Coupling(MaxLRows,MaxLRows)      	 
 Dimension CommonPar(*)

       Do nci=1, iDofT
                   Coupling( nci , nci ) = 1
       end do

Return
End

