!      Module Solver
!	contains

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%% Numerical Factorization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%% of a General Matrix with Symetric Structure %%%%%%%%%%%%%%%%%%%%%
      SubRoutine TrianNS(iA,jA,AN,ANT,AD,iU,jU,UN,UNT,DI,DIT,iP,iUP,N,S)
      IMPLICIT REAL*8 (A-H,O-Z)
      Logical*2 S, NotSym
      Dimension iA(*),jA(*),AN(*),ANT(*),AD(*),iU(*),jU(*),UN(*),UNT(*),
     &          iP(*),iUP(*),DI(*),DIT(*)

C	La diagonal de U DU queda en DI 
      NotSym= .not. S
c	write(6,*)ad(1),iA(1),iA(2)
C&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
c      write(18,*)N+2*(IA(N+1)-1),N
c      Do 505 I=1,N
c             iAA=iA(I)
c             iAB=iA(I+1)-1
c             write(18,*)I,I,AD(I)
c             if(iAB.lt.iAA) goto 505
c             Do 503 J=iAA,iAB
C          	write(18,*)I,JA(J),AN(J)
c          	write(18,*)JA(J),I,ANT(J)-AN(J)
c 503         Continue
c 505         Continue

c	Stop
C&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

      Do 10 J=1,N
      iUP(j)=0
 10   iP(j)=0

      Do 170 I=1,N
C      Write(6,*)I,N
             iUA=iU(I)
             iUB=iU(I+1)-1
             if(iUB.lt.iUA) goto 40
             Do 20 J=iUA,iUB
                        DI ( JU(J) )=0.0d0
             if(NotSym) DIT( JU(J) )=0.0d0
 20          Continue
             iAA=iA(I)
             iAB=iA(I+1)-1
             if(iAB.lt.iAA) goto 40
             Do 30 J=iAA,iAB
                               DI ( JA(J) )=AN(J)
                    if(NotSym) DIT( JA(J) )=ANT(J)
 30          Continue

 40          Last=iP(I)
             DI(I) = AD(I) 
C Last is the element in the "list of rows" asociated with the Row-Col I
C IP(Last) has the folowing row of Last, ie, the first Row
C iUP(L) has the position of column I in the L row
             if (Last.eq.0) goto 110
             LN       = iP(Last)
c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 50          L        = LN
c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
             LN       = iP(L)
             iUC      = iUP(L)
             iUD      = iU(L+1)-1
C       Multiplicadores
             UMM       = UN(iUC)/DI(L)
             if (NotSym) then
                             UM       = UNT(iUC)/DI(L)
	       else
                             UM       = UMM
             Endif
C     factoreo de la diagonal
             DI(I)    = DI(I) - UM*UN(iUC)
c     normalizacion del triangulo superior y guardado del L
             UN (iUC) = UMM
             IF (NotSym) UNT(iUC) = UM

             iUP(L) = iUC + 1
             if (iUC.eq.iUD) goto 80
C ojo aca hay una diferencia con la simetrica.
c             iUP(L) = iUC + 1
             Do 60 J= iUP(L),iUD
                    J1     = JU( J )             
                               DI (J1)= DI (J1) - UM * UN (J)
                    if(NotSym) DIT(J1)= DIT(J1) - UMM* UNT(J)
 60          Continue

             J   = JU ( iUP(L) )
             JJ  = iP(J)
C      si JJ=0 la cadena asociada a la columna J siguiente de iuc 
C      esta vacia
C si es asi se inicia la cadena con un solo elemento, sino se engancha
C en la cadena existente
             if ( JJ.eq.0) then
                 iP(J)  = L
                 iP(L)  = L
             else
                 iP (L) = iP(JJ)
                 iP(JJ) = L
             endif

C 80          B(I)  = B(I) - UM*B(L)
c 80           if (NotSym) then
c                   UNT(iUC) = UM
c                   UN (iUC) = UMM
c	         else
c                   UN(iUC) = UM
c              Endif
c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 80         if (L   .ne. Last) goto  50
c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C     Saving Factored coefficients
 110         if (iUB .lt. iUA ) goto 160
             Do 120 J=iUA,iUB
                    JJ     = JU (J )
                    UN (J) = DI (JJ)
                    if(NotSym) UNT(J) = DIT(JJ)
 120         Continue

C     inscribing the current row in the corresponding list for column J      
             J  = JU(iUA)
             JJ = iP(J)
             If ( JJ .eq. 0 ) then
                 iP(J) = I
                 iP(I) = I
              Else
                 iP(I) = iP(JJ)
                 iP(JJ)= I
             Endif
 160         iUP(I)= iUA

 170  Continue
                  
      RETURN
	END subroutine  
C%%%%%%%%%%%Forward & Backward sustitution
	SUBROUTINE ForwBack ( IA, JA, AD, AN, ANT, B, UnK, N )

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IA(*),JA(*),AN(*),ANT(*),AD(*),B(*),UnK(*)

      Call AsigReal ( UnK,B,N ) 
c	write(6,*)
c	write (6,*)AD(1),IA(1),IA(2),B(1),UnK(1)
      Do 50 I=1,N
             iAA = iA(I)
             iAB = iA(I+1)-1
             BI  = UnK(I)
             if(iAB.lt.iAA) goto 49
             Do 30 J=iAA,iAB
	                       JJ = jA(J) 
 30                          UnK(JJ)=UnK(JJ)-ANT(J)*BI
 49          UnK(I) = BI/AD(I)    

 50   Continue
      
      Do 80 I=N-1,1,-1
             iAA=iA(I)
             iAB=iA(I+1)-1
             BI= UnK(I)
             if(iAB.lt.iAA) goto 80
             Do 31 J=iAA,iAB
 31                 BI= BI - AN(J) * UnK( JA(J) )

c	write(15,*)BI
 80   UnK(I) = BI 

c	write(6,*)
c	write (6,*)AD(1),IA(1),IA(2),B(1),UnK(1)

      RETURN
	END subroutine
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE MULTAPNS (IA,JA,AD,AN,ANT,B,C,NodR)

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IA(*),JA(*),AN(*),ANT(*),AD(*),C(*),B(*)
	Integer FilP,ColP,PunJQ
	Call IniReal(C,NodR,0.0d0)

	   DO 10 FilP = 1,NodR

	   Z=B(FilP)
         U=AD(FilP)*Z

	   IQA = IA(FilP)
	   IQB = IA(FilP+1)-1
	   DO 20 PunJQ = IQA,IQB
	      ColP = JA(PunJQ)
	      U = U + AN(PunJQ)*B(ColP)
 20	      C(ColP) = C(ColP) + ANT(PunJQ)*Z
 10        C(FilP) = C(FilP) + U

      RETURN
	END subroutine

C-----------------------------------------------------------------------
C       TRIANGULARIZACION SIMBOLICA 
C-------------------------------------
C      
C     NODT  : orden de la matriz A SOL U            (in)
C     IA,JA : matriz de rigidez (simbolica)         (in)
C     IU,JU : matriz triangularizada (simbolica)    (out)
C     IP    : vector auxiliar ( se inicializa )
C     LONG  : longitud del vector JU                (out)
C
      SUBROUTINE TrianSim ( IA,JA,IU,JU,IP,NODT,LONG )
      
      DIMENSION IA(*),JA(*),IU(*),JU(*),IP(*)
      
      CALL INIENT ( IU,NODT+1,0 )
      CALL INIENT ( IP,NODT,0 ) 

      NM = NODT-1
      NH = NODT+1
      JP = 1
      DO 90 I = 1,NM
      JPI = JP
      JPP = NODT+JP-I
      MIN = NH
      IAA = IA (I)
      IAB = IA (I+1)-1
      IF ( IAB .LT. IAA ) GO TO 30
      DO 20 J = IAA,IAB
      JJ = JA (J)
      JU (JP) = JJ
      JP = JP+1
      IF ( JJ .LT. MIN ) MIN = JJ
 20   IU (JJ) = I
 30   LAST = IP(I)
      IF ( LAST .EQ. 0 ) GO TO 60
      L = LAST
 40   L = IP(L)
      LH = L+1
      IUA = IU (L)
      IUB = IU (LH)-1
      IF ( LH .EQ. I ) IUB = JPI-1
      IU (I) = I
      DO 50 J = IUA,IUB
      JJ = JU (J)
      IF ( IU (JJ) .EQ. I ) GO TO 50
      JU (JP) = JJ
      JP = JP+1
      IU (JJ) = I
      IF ( JJ .LT. MIN ) MIN = JJ
 50   CONTINUE
      IF ( JP .EQ. JPP ) GO TO 70
      IF ( L .NE. LAST ) GO TO 40
 60   IF ( MIN .EQ. NH ) GO TO 90
 70   L = IP (MIN)
      IF ( L .EQ. 0 ) GO TO 80
      IP (I) = IP (L)
      IP (L) = I
      GO TO 90
 80   IP (MIN) = I
      IP (I) = I
 90   IU (I) = JPI
      IU (NODT) = JP
      IU (NH) = JP
      LONG = JP
      RETURN
	END subroutine
C
C     LA MATRIZ U DEBE SER ORDENADA PARA ENTRAR EN "TRIANNUM"
C
C     TRIANGULARIZACION NUMERICA
C-----------------------------------
C
C     IA,JA,AD,AN : matriz de rigidez                     (in)
C     IU,JU       : matriz triangularizada ordenada       (in)
C     DI          : inversa de la diagonal de A           (out)
C     UN          : valores extradiagonales de U          (out)   
C     IP,IUP      : vectores auxiliares  
C                    
      SUBROUTINE TrianNum ( IA,JA,AN,AD,IU,JU,UN,DI,IP,IUP,NODT )
      
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IA(*),JA(*),AN(*),AD(*),IU(*),JU(*),UN(*),DI(*),IP(*),
     *          IUP(*)
	     
      CALL INIENT ( IP,NODT,0 )
      DO 131 I=1,NODT
      IH = I+1
      IUA = IU(I)
      IUB = IU(IH)-1
      IF ( IUB .LT. IUA ) GO TO 41
      DO 21 J = IUA,IUB
 21   DI(JU(J))=0.0D0
      IAA = IA(I)
      IAB = IA(IH)-1
      IF ( IAB .LT. IAA ) GO TO 41
      DO 31 J = IAA,IAB
 31   DI( JA(J) ) = AN(J)
 41   DI(I) = AD(I)
      LAST = IP(I)
      IF ( LAST .EQ. 0 ) GO TO 91
      LN = IP(LAST)
 51   L = LN
      LN = IP(L)
      IUC = IUP(L)
      IUD = IU(L+1)-1
      UM = UN(IUC)*DI(L)
      DO 61 J = IUC,IUD
      JJ = JU(J)
 61   DI(JJ) = DI(JJ) - UN(J)*UM
      UN(IUC) = UM
      IUP(L) = IUC+1
      IF( IUC .EQ. IUD ) GO TO 81
      J = JU (IUC+1)
      JJ = IP(J)
      IF ( JJ .EQ. 0 ) GO TO 71
      IP(L) = IP(JJ)
      IP(JJ) = L
      GO TO 81
 71   IP(J) = L
      IP(L) = L
 81   IF ( L .NE. LAST ) GO TO 51
 91   DI(I) = 1.0D0/DI(I)
      IF ( IUB .LT. IUA ) GO TO 121
      DO 101 J = IUA,IUB
 101  UN(J) = DI( JU(J) )
      J = JU(IUA)
      JJ = IP(J)
      IF ( JJ .EQ. 0 ) GO TO 111
      IP(I) = IP(JJ)
      IP(JJ) = I
      GO TO 121
 111  IP(J) = I
      IP(I) = I
 121  IUP(I) = IUA
 131  CONTINUE 
      RETURN
	END subroutine
C
C     RESOLUCION DEL SISTEMA 
C----------------------------------------
C
C     IU,JU,UN    : matriz triangularizada ordenada       (in)
C     B           : vector segundo miembro                (in)
C     DI          : inversa de la diagonal de A           (in)
C     SOL         : resultado de sistema                  (out)
C
      SUBROUTINE Resolver ( B,IU,JU,UN,DI,SOL,NODT )
      
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION B(*),IU(*),JU(*),UN(*),DI(*),SOL(*)
      
      NM = NODT-1
      DO 12 I=1,NODT
 12   SOL(I)=B(I)

      DO 42 K=1,NM
      IUA=IU(K)
      IUB=IU(K+1)-1
      XX=SOL(K)
      IF(IUB.LT.IUA) GO TO 32
      DO 22 I=IUA,IUB
 22   SOL(JU(I))=SOL(JU(I))-UN(I)*XX
 32   SOL(K)=XX*DI(K)
 42   CONTINUE

      SOL(NODT)=SOL(NODT)*DI(NODT)
      K=NM
 52   IUA=IU(K)
      IUB=IU(K+1)-1
      IF (IUB.LT.IUA) GO TO 72
      XX=SOL(K)
      DO 62 I=IUA,IUB
 62   XX=XX-UN(I)*SOL(JU(I))
      SOL(K)=XX
 72   K=K-1
      IF (K .GT. 0) GO TO 52
      RETURN
	END subroutine


C-----------------------------------------------------------------------
      SUBROUTINE GAUSEILU(IA,JA,AL,AU,AD,B,X,X0,C,NOD,NSWEEP,IStat)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * G A U S E I *
C                                                        ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -   /   /
C-----------------------------------------------------------------------
      REAL*8 AL(20),AU(20),AD(20), B(NOD), X(NOD), X0(NOD), C(NOD)
      INTEGER IA(20), JA(20)
      REAL*8 RNORM2D, ENORM2D,RIGHT
      Real*8,Parameter :: Toler = 1.d-8 , SRLX=.4d0, SRLXm1=1.0d0-SRLX
c      Dimension XNOR(0:500),RNOR(0:500)
      XNOR = 1.
C

  1   Continue
      DO 10 IS = 1, NSWEEP
         CALL COPVECD (X,X0,NOD)
          C=0.0d0
            DO 20 I = 1, NOD
            RIGHT = B(I)
            IAA = IA(I)
            IAB = IA(I+1) - 1
            DO 30 K = IAA, IAB
               J     = JA(K)
               RIGHT = RIGHT - AU(K)*X(J)
   30       CONTINUE

            X(I) = X(I)*SRLXm1 + (RIGHT+C(I))/AD(I) * SRLX

            DO 40 K = IAA, IAB
               J    = JA(K)
               C(J) = C(J) - AL(K)*X(I)
   40       CONTINUE


   20    CONTINUE

c         XNOR(IS) = ENORM2D(X,X0,NOD) / RNORM2D(X,NOD)
c	   WRITE (6,*) IS,XNOR(IS)
         XNOR = ENORM2D(X,X0,NOD) / RNORM2D(X,NOD)
	   WRITE (6,*) NSWEEP,IS,XNOR

	If (Xnor .lt. Toler .and. IS .GT. 5) Exit

   10 CONTINUE
C
	If (Xnor .Gt. Toler ) then
	    ISTAT = -1
!	   WRITE (6,*) 'New NSWEEP'
!         goto 1
      else
	    ISTAT = 0
      endif


      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
C    ------------------------------------------------------------------
C   !                                                                  !
C   !          FUNCIONES  COMUNES  A  CONVDIFE3D  Y  CONVDIFNE3D       !
C   !                                                                  !
C   !                    - 28 de Febrero de 1998 -                     ! 
C-----------------------------------------------------------------------
      SUBROUTINE AXPLBYD (A,X,B,Y,Z,N,KRES)
C-----------------------------------------------------------------------
C                                                      *****************
C A TIMES VECTOR X PLUS B TIMES VECTOR Y               * A X P L B Y D *
C (version doble presicion)                            *****************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -  6/DIC/89
C-----------------------------------------------------------------------
      REAL*8 X(*), Y(*), Z(*)
      REAL*8 A, B
C
      IF (KRES.EQ.1) THEN
         DO 10 I = 1, N
            X(I) = A*X(I) + B*Y(I)
   10    CONTINUE
      ELSE IF (KRES.EQ.2) THEN
         DO 20 I = 1, N
            Y(I) = A*X(I) + B*Y(I)
   20    CONTINUE
      ELSE IF (KRES.EQ.3) THEN
         DO 30 I = 1, N
            Z(I) = A*X(I) + B*Y(I)
   30    CONTINUE
      END IF
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE XPLUSYD (X,Y,Z,N,KOD)
C-----------------------------------------------------------------------
C                                                        ***************
C VECTOR X PLUS VECTOR Y                                 * X P L U S Y *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 18/ENE/90
C-----------------------------------------------------------------------
C ENTREGA EN X,Y O Z LA SUMA O RESTA DE LOS VECTORES X E Y
C
C       SI KOD.EQ.1    X + Y -> X
C       SI KOD.EQ.2    X + Y -> Y
C       SI KOD.EQ.3    X + Y -> Z
C       SI KOD.EQ.-1   X - Y -> X
C       SI KOD.EQ.-2   X - Y -> Y
C       SI KOD.EQ.-3   X - Y -> Z
C
C-----------------------------------------------------------------------
      REAL*8 X(*), Y(*), Z(*)
C
      IF (KOD.EQ.1) THEN
         DO 10 I = 1, N
            X(I) = X(I) + Y(I)
   10    CONTINUE
      ELSE IF (KOD.EQ.2) THEN
         DO 20 I = 1, N
            Y(I) = X(I) + Y(I)
   20    CONTINUE
      ELSE IF (KOD.EQ.3) THEN
         DO 30 I = 1, N
            Z(I) = X(I) + Y(I)
   30    CONTINUE
      ELSE IF (KOD.EQ.-1) THEN
         DO 40 I = 1, N
            X(I) = X(I) - Y(I)
   40    CONTINUE
      ELSE IF (KOD.EQ.-2) THEN
         DO 50 I = 1, N
            Y(I) = X(I) - Y(I)
   50    CONTINUE
      ELSE IF (KOD.EQ.-3) THEN
         DO 60 I = 1, N
            Z(I) = X(I) - Y(I)
   60    CONTINUE
C
      END IF
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SYMCMR (IA,JA,IL,JL,NOD,NJL)
C-----------------------------------------------------------------------
C                                                        ***************
C TRANSFORMACION SIMBOLICA DE REPRESENTACION COMPLETA    * S Y M C M R *
C A MIXTA                                                ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 28/NOV/89
C-----------------------------------------------------------------------
C    Encuentra los IL y JL de una representacion MR(LU)U dada la RR(C)U
C de una matriz simetrica o no, de estructura simetrica, en IA y JA.
C NJL es la longitud de JL.
C
C    La representacion mixta MR(LU)U consiste en dividir la matriz A
C en una suma de 3 matrices:
C
C                           A = AL + AD + AU
C
C donde:   ALij =  Aij  si  j<i        ; triangular inferior   RR(L)U
C          ALij =  0.
C
C          ADij =  Aij  si  j=i        ; diagonal almacenada como vector
C          ADij =  0.   si  j<i o j>i
C
C          AUij =  Aij  si  j>i        ; triangular superior   CR(U)U
C          AUij =  0.   si  j<i o j=i
C
C
C-----------------------------------------------------------------------
      INTEGER IA(*), JA(*), IL(*), JL(*)
C
      KL    = 1
      IL(1) = 1
      DO 20 I = 1, NOD
         IAA = IA(I)
         IAB = IA(I+1) - 1
         IF (IAB.GE.IAA) THEN
            DO 10 K = IAA, IAB
               J = JA(K)
               IF (J.GE.I) GOTO 10
               JL(KL) = J
               KL = KL + 1
   10       CONTINUE
         END IF
         IL(I+1) = KL
   20 CONTINUE
      NJL = KL - 1
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE BWSCOLD (IU,JU,U,IUD,KUD,Y,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C BACKWARD-SUSTITUTION BY COLUMNS                        * B W S C O L *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 30/OCT/89
C-----------------------------------------------------------------------
C REFERENCIAS:
C   "Sparse Matrix Technology", S. Pissanetzky, Academic Press, 1984
C-----------------------------------------------------------------------
C
C  REALIZA LA RETRO-SUSTITUCION POR COLUMNAS PARA RESOLVER U x = y.
C  JUNTO CON LA RUTINA FWSROW RESUELVE EL PROBLEMA L U x = b. DONDE
C  U ESTA ALMACENADA POR COLUMNAS Y L POR FILAS.
C
C     DATOS DE ENTRADA:
C     NOD     NUMERO DE INCOGNITAS
C     IU,JU   ESTRUCTURA DE LA MATRIZ U TRIANGULARIZADA CR(U)U
C     U       ELEMENTOS EXTRADIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     IUD     INVERSAS DE LOS ELEMENTOS DIAGONALES DE U (SI KUD=1)
C     KUD     SWITCH: INDICA SI SE PROVEE EL VECTOR IUD
C                     KUD=1 => SE PROVEE IUD
C                     KUD=0 => NO SE DA IUD. SE SUPONE QUE LA MATRIZ
C                              U TIENE DIAGONAL UNITARIA.
C     Y       VECTOR TERMINO INDEPENDIENTE
C
C     RESULTADOS:
C     X->Y    SOLUCION
C
C-----------------------------------------------------------------------
C
      INTEGER IU(*), JU(*)
      REAL*8  U(*),  IUD(*), Y(*), ZZ
C
      IF (KUD.EQ.1) THEN
         DO 20 J = NOD, 1, -1
            IUA = IU(J)
            IUB = IU(J+1) - 1
            ZZ  = Y(J)*IUD(J)
            IF (IUB.GE.IUA) THEN
               DO 10 K = IUA, IUB
                  Y(JU(K)) = Y(JU(K)) - U(K) * ZZ
   10          CONTINUE
            END IF
            Y(J) = ZZ
   20    CONTINUE
      ELSE
         DO 40 J = NOD, 1, -1
            IUA = IU(J)
            IUB = IU(J+1) - 1
            ZZ  = Y(J)
            IF (IUB.GE.IUA) THEN
               DO 30 K = IUA, IUB
                  Y(JU(K)) = Y(JU(K)) - U(K) * ZZ
   30          CONTINUE
            END IF
            Y(J) = ZZ
   40    CONTINUE
      END IF
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SCTXPYD (S,X,Y,Z,N,KRES)
C-----------------------------------------------------------------------
C                                                        ***************
C SCALAR TIMES X PLUS Y                                  * S C T X P Y *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -  1/NOV/89
C-----------------------------------------------------------------------
C ENTREGA EL RESULTADO DE MULTIPLICAR EL VECTOR X POR EL
C ESCALAR S Y SUMARLO A Y.
C
C S    [I]          VALOR REAL
C X(N) [I,(O)]      VECTOR REAL (SOLUCION SI KRES=1)
C Y(N) [I,(O)]      VECTOR REAL (SOLUCION SI KRES=2)
C Z(N) [O]          VECTOR REAL (SOLUCION SI KRES=3)
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 X(*), Y(*), Z(*)
      IF (KRES.EQ.1) THEN
         DO 10 I = 1, N
            X(I) = S*X(I) + Y(I)
   10    CONTINUE
      ELSE IF (KRES.EQ.2) THEN
         DO 20 I = 1, N
            Y(I) = S*X(I) + Y(I)
   20    CONTINUE
      ELSE IF (KRES.EQ.3) THEN
         DO 30 I = 1, N
            Z(I) = S*X(I) + Y(I)
   30    CONTINUE
      ELSE
         STOP 'SCTXPY/ERROR: KRES DEBE SER 1,2 O 3'
      END IF
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SCTIMXD (ESC,X,N)
C-----------------------------------------------------------------------
C                                                        ***************
C SCALAR TIMES VECTOR                                    * S C T I M X *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 18/ENE/90
C-----------------------------------------------------------------------
      REAL*8 X(*),ESC
      DO 10 I=1,N
         X(I) = ESC*X(I)
 10   CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      REAL*8 FUNCTION SCPRODD (X,Y,N)
C-----------------------------------------------------------------------
C                                                        ***************
C SCALAR PRODUCT                                         * S C P R O D *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -   /   /
C-----------------------------------------------------------------------
C ENTREGA EL PRODUCTO ESCALAR DE LOS VECTORES X E Y.
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8  X(*), Y(*)
      SCPRODD = 0.D0
      DO 10 I = 1, N
         SCPRODD = SCPRODD + X(I)*Y(I)
   10 CONTINUE
      RETURN
      END function

C-----------------------------------------------------------------------
      REAL*8 FUNCTION RNORM2D (Y,N)
C-----------------------------------------------------------------------
C                                                        ***************
C NORMA CUADRATICA L2 DEL VECTOR Y                       * R N O R M 2 *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -   /   /
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 Y(*)
      RNORM2D = 0.D0
      DO 10 I = 1, N
         RNORM2D = RNORM2D + Y(I)*Y(I)
   10 CONTINUE
      RNORM2D = DSQRT(RNORM2D)
      RETURN
      END function
C-----------------------------------------------------------------------
      SUBROUTINE SPMUL2D (IA,JA,AN,AD,U,V,N,KOD)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * S P M U L 2 *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C  MULTIPLICA UNA MATRIZ RALA POR UN VECTOR LLENO.
C
C  DATOS
C  IA,JA,AN  MATRIZ RALA DADA EN R(S)FD O EN R(C)FD.
C  AD  DIAGONAL DE LA MATRIZ DADA,SOLO SI KOD=1.
C  U  VECTOR LLENO DADO.
C  N  CANTIDAD DE FILAS DE LA MATRIZ.
C  KOD=1  PARA MATRIZ SIMETRICA.
C     =2  PARA MATRIZ GENERAL.
C     =3  PRODUCTO DE VECTOR ACOSTADO POR MATRIZ GENERAL. EN ESTE CASO
C         EL RESULTADO ES ACUMULADO EN V Y PUEDE SER NECESARIO INICIA-
C         LIZAR V CON CEROS.
C     =4  PARA MATRIZ TRIANGULAR INFERIOR ALMACENADA RR(L)O *
C * AXEL LARRETEGUY 5/NOV/89
C
C  RESULTADOS.
C  V  VECTOR LLENO V=A*U  O  V=UT*A.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8  AN(*),AD(*),U(*),V(*)
      INTEGER IA(*),JA(*)
      IF(N.LT.1)GOTO 360
      IF(KOD.EQ.2)GOTO 2
      IF(KOD.EQ.3)GOTO 3
      IF(KOD.EQ.4)GOTO 4
      IF(KOD.NE.1)GOTO 360
      DO 10 I=1,N
10    V(I)=AD(I)*U(I)
      DO 20 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
!      IF(IAB.LT.IAA)GOTO 20
      DO 30 JP=IAA,IAB
      J=JA(JP)
      Z=AN(JP)
      V(I)=V(I)+Z*U(J)
30    V(J)=V(J)+Z*U(I)
20    CONTINUE
      RETURN
C
2     DO 40 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
!      IF(IAB.LT.IAA)GOTO 42
      DO 50 JP=IAA,IAB
50    W=W+AN(JP)*U(JA(JP))
42    V(I)=W
40    CONTINUE
      RETURN
C
3     DO 60 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
!      IF(IAB.LT.IAA)GOTO 60
      Z=U(I)
      DO 70 JP=IAA,IAB
      J=JA(JP)
      V(J)=V(J)+AN(JP)*Z
70    CONTINUE
60    CONTINUE
      RETURN
C
4     DO 90 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
!      IF(IAB.LT.IAA)GOTO 82
      DO 80 JP=IAA,IAB
80    W=W+AN(JP)*U(JA(JP))
82    W=W+AD(I)*U(I)
      V(I)=W
90    CONTINUE
      RETURN
C
360   WRITE(6,365)N,KOD
      STOP
365   FORMAT(13H SPMULT.ERROR,18I6)
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SPMUL2DLU (IA,JA,AN,ANT,AD,U,V,N,KOD)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * S P M U L 2 *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C  MULTIPLICA UNA MATRIZ RALA POR UN VECTOR LLENO.
C
C  DATOS
C  IA,JA,AN  MATRIZ RALA DADA EN R(S)FD O EN R(C)FD.
C  AD  DIAGONAL DE LA MATRIZ DADA,SOLO SI KOD=1.
C  U  VECTOR LLENO DADO.
C  N  CANTIDAD DE FILAS DE LA MATRIZ.
C  KOD=1  PARA MATRIZ SIMETRICA.
C     =2  PARA MATRIZ GENERAL.
C     =3  PRODUCTO DE VECTOR ACOSTADO POR MATRIZ GENERAL. EN ESTE CASO
C         EL RESULTADO ES ACUMULADO EN V Y PUEDE SER NECESARIO INICIA-
C         LIZAR V CON CEROS.
C     =4  PARA MATRIZ TRIANGULAR INFERIOR ALMACENADA RR(L)O *
C * AXEL LARRETEGUY 5/NOV/89
C
C  RESULTADOS.
C  V  VECTOR LLENO V=A*U  O  V=UT*A.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8  AN(*),ANT(*),AD(*),U(*),V(*)
      INTEGER IA(*),JA(*)
      IF(N.LT.1)GOTO 360
      IF(KOD.EQ.2)GOTO 2
      IF(KOD.EQ.3)GOTO 3
      IF(KOD.EQ.4)GOTO 4
      IF(KOD.EQ.6)GOTO 6
      IF(KOD.NE.1)GOTO 360
      DO 10 I=1,N
10    V(I)=AD(I)*U(I)
      DO 20 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
!      IF(IAB.LT.IAA)GOTO 20
      DO 30 JP=IAA,IAB
      J=JA(JP)
      Z=AN(JP)
      V(I)=V(I)+Z*U(J)
30    V(J)=V(J)+Z*U(I)
20    CONTINUE
      RETURN
C
2     DO 40 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
!      IF(IAB.LT.IAA)GOTO 42
      DO 50 JP=IAA,IAB
50    W=W+AN(JP)*U(JA(JP))
42    V(I)=W
40    CONTINUE
      RETURN
C
3     DO 60 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
!      IF(IAB.LT.IAA)GOTO 60
      Z=U(I)
      DO 70 JP=IAA,IAB
      J=JA(JP)
      V(J)=V(J)+AN(JP)*Z
70    CONTINUE
60    CONTINUE
      RETURN
C
4     DO 90 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
!      IF(IAB.LT.IAA)GOTO 82
      DO 80 JP=IAA,IAB
80    W=W+AN(JP)*U(JA(JP))
82    W=W+AD(I)*U(I)
      V(I)=W
90    CONTINUE
      RETURN

 6    DO 99 I=1,N
99    V(I)=AD(I)*U(I)
      DO 100 I=1,N
          IAA=IA(I)
          IAB=IA(I+1)-1
!          IF(IAB.LT.IAA)GOTO 100
          DO 101 JP=IAA,IAB
              J=JA(JP)
              V(I)=V(I)+AN(JP)*U(J)
101           V(J)=V(J)+ANT(JP)*U(I)
100   CONTINUE
      RETURN


C
360   WRITE(6,365)N,KOD
      STOP
365   FORMAT(13H SPMULT.ERROR,18I6)
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE CGSAP1D (IA,JA,A,IL,JL,L,IUD,U,X,R,R0,G,Q,H,W,Z,
     -                    NOD,EPS,ITMAX,ISTAT)
C-----------------------------------------------------------------------
C                                                      *****************
C CONJUGATE-GRADIENTS SQUARED SOLVER FOR ASYMMETRIC    * C G S A P 1 D *
C MATRIX EQUATIONS WITH PRECONDITIONING - VERSION 1    *****************
C-----------------------------------------------------------------------
C                       *** DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 18/ENE/90
C-----------------------------------------------------------------------
C METODO ITERATIVO DE GRADIENTES CONJUGADOS CUADRADOS PRECONDICIONADO
C CON FACTORIZACION INCOMPLETA LU (ILU) PARA RESOLVER A x = b, DONDE  LA
C MATRIZ A ESTA ALMACENADA EN FORMATO SPARSE TIPO GUSTAVSON.
C-----------------------------------------------------------------------
C
C ARGUMENTOS:
C [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
C [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
C
C IA,JA,A  [I]      ESTRUCTURA Y VALORES DE LA MATRIZ A EN RR(C)O /1/
C IL,JL    [I]      ESTRUCTURA DE LOS FACTORES TRIANGULARES       /1/
C L        [I]      FACTORES TRIANGULARES INFERIORES EN FORMATO RR(L)O
C U,IUD    [I]      FACTORES TRIANGULARES SUPERIORES EN FORMATO CR(U)O.
C                   EL VECTOR IUD CONTIENE LAS INVERSAS DE Uii /2/,/3/
C R        [I,A]    VECTOR TERMINO INDEPENDIENTE
C R0,G,Q,  [W,X]    VECTORES AUXILIARES
C Z,W,H
C NOD      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
C EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
C                   ( |Xi-Xi+1|/|Xi| < EPS  y |Ri+1|/|B| < EPS ) o
C                   |Ri+1| = 0.   DONDE |X|=NORMA L2 DE X
C ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
C X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
C                   O: VECTOR SOLUCION
C ISTAT    [O]      STATUS:
C                   ISTAT=    0  => OK. EL RESIDUO INICIAL ES MUY CHICO.
C                                    X0 YA ES LA SOLUCION BUSCADA.
C                   ISTAT>    0  => OK. ISTAT CONTIENE EL NUMERO DE
C                                    ITERACIONES QUE SE NECESITARON.
C                   ISTAT=   -1  => NO CONVERGIO EN ITMAX ITERACIONES.
C                   ISTAT=   -2  => |b| ES PRACTICAMENTE NULA (*).
C                   ISTAT=-3xxx  => VARIABLE  ro(n-1) CASI NULA EN
C                                   ITERACION xxx (*).
C                   ISTAT=-4xxx  => VARIABLE  sigma(n-1) CASI NULA EN
C                                   ITERACION xxx (*).
C
C (*) EL ALGORITMO FALLA Y NO PUEDE CONTINUAR. SI NO SE ENCUENTRAN
C     PROBLEMAS EN LOS DATOS, Y ESTAMOS SEGUROS DE QUE LA MATRIZ ES
C     INVERTIBLE, LO MAS PROBABLE ES QUE ESTE METODO NO SEA APLICABLE
C     EN ESE CASO PARTICULAR. ESTE METODO NO GARANTIZA QUE EL PROCESO
C     PUEDA SER SIEMPRE LLEVADO A CABO. EN ESOS CASOS PROBAR CON EL
C     METODO BCG, O CONSULTAR LA REFERENCIA /4/.
C
C STOP:  Si el numero de iteraciones ITMAX es > que las dimensiones
C        utilizadas en el COMMON /NORMAS/ (parameter MAXIMUM)
C
C ALGORITMO: REF /4/
C
C REFERENCIAS:
C /1/ S. PISSANETZKY, "SPARSE MATRIX TECHNOLOGY", ACADEMIC PRESS, 1984.
C /2/ D. S. KERSHAW, J. COMP. PHYSICS 26 (1978) 43-65.
C /3/ D. S. KERSHAW, J. COMP. PHYSICS 38 (1980) 114-123.
C /4/ P. SONNEVELD et al, "MULTIGRID AND CONJUGATE GRADIENT METHODS AS
C     CONVERGENCE ACCELERATION TECHNIQUES", in "Multigrid Methods for
C     Integral and Differential Equations", CLARENDON PRESS, OXFORD, 1985.
C
C RUTINAS UTILIZADAS:
C    ENORM2D, RNORM2D, SPMUL2D, SCTXPYD, COPVECD, FWSROWD, BWSCOLD,
C    SCPRODD, LMULTVD, PUTVALD
C
C RUTINAS RELACIONADAS:
C    ILUFK1D, SYMCMR
C
C-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8 A(*), L(*), IUD(*), U(*), X(*), R0(*), R(*), G(*), H(*),
     -       Q(*), W(*), Z(*)
      REAL*8 SCPRODD, RNORM2D, ENORM2D
      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
C
      ISTAT = 0
!      IF (ITMAX.GT.MAXIMUM)
!     -   STOP 'CGSAP1D/ERROR: Aumentar MAXIMUM en fuente de CGSAP1D.'
C
C |B|: Norma L2 de B
      BNORM = RNORM2D(R,NOD)
      IF (BNORM.LT.TOO_SMALL) THEN
         ISTAT = -2
         RETURN
      END IF
C
C R0 = B - A.X0
      CALL SPMUL2D (IA,JA,A,DUMMY,X,W,NOD,2)
      CALL SCTXPYD (-1.D0,W,R,DUMMY,NOD,2)
      CALL COPVECD (R,R0,NOD)
C
C |R0|/|B| <<<< 1?
      RCONV = RNORM2D(R,NOD) / BNORM
c      IF (RCONV.LE.SMALL) RETURN
C     Modificada por Santiago Urquiza, que no vuelva sin iterar

C
C G[-1] = H[0] = 0.D0
      CALL PUTVALD (G,NOD,0.D0,0,iDUMMY)
      CALL PUTVALD (H,NOD,0.D0,0,iDUMMY)
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C
C           T             -1
C ro(n) = R0  Z ;  Z = (LU)  R ;  beta(n) = ro(n)/ro(n-1)
      RONM1= RON
      CALL COPVECD (R,Z,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      RON  = SCPRODD(R0,Z,NOD)
      IF (I.EQ.0) THEN
         BETA = 0.D0
      ELSE
         IF (DABS(RONM1).LT.TOO_SMALL) THEN
            ISTAT = -(3*1000+I)
            RETURN
         END IF
         BETA = RON/RONM1
      END IF
C
C Q = Z + beta(n) H[n] ;  G[N] = Q + beta(n) ( beta(n) G[n-1] + H[n] )
      CALL SCTXPYD (BETA,H,Z,Q,NOD,3)
      CALL SCTXPYD (BETA,G,H,DUMMY,NOD,1)
      CALL SCTXPYD (BETA,G,Q,DUMMY,NOD,1)
C
C              T              -1
C sigma(n) = R0  Z  ;  Z = (LU) A G[n]
      CALL SPMUL2D (IA,JA,A,DUMMY,G,Z,NOD,2)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      SIGMA = SCPRODD(R0,Z,NOD)
      IF (DABS(SIGMA).LT.TOO_SMALL) THEN
         ISTAT = -(4*1000+I)
!          Write(6,*)"Warning SIGMA Too Small",SIGMA,I
         RETURN
      END IF
C
C H[n+1] = Q - alfa(n) Z  ;  alfa(n) = ro(n) / sigma(n)
      ALFA  = RON / SIGMA
      CALL SCTXPYD (-ALFA,Z,Q,H,NOD,3)
C
C X[n+1] = X[n] + Z  ;   Z = alfa(n) ( Q + H[n+1] )  ; guardar X[n+1] -> W
      CALL XPLUSYD (Q,H,Z,NOD,3)
      CALL SCTIMXD (ALFA,Z,NOD)
      CALL XPLUSYD (X,Z,W,NOD,3)
C
C R[n+1] = R[n] - A Z
      CALL SPMUL2D (IA,JA,A,DUMMY,Z,Q,NOD,2)
      CALL XPLUSYD (R,Q,DUMMY,NOD,-1)
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (RNORM2D(X,NOD)+SHIFT)
      RCONV = RNORM2D(R,NOD) / BNORM
      XNOR(I) = XCONV + 1.E-25
      RNOR(I) = RCONV + 1.E-25
C      WRITE (6,101)I,RCONV,XCONV
 101  Format ( ' -> CGS_iter=',I5,' |R|/|B|=',E13.5,' |DX|/|X|=',E13.5)
C
      CALL COPVECD (W,X,NOD)
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      END IF
C     No convergio en itmax
      IF (I.EQ.ITMAX-1) THEN
         ISTAT = -1
         WRITE (6,101)I,RCONV,XCONV
        RETURN
      END IF
      
C
C Nueva iteracion
      I = I + 1
      GOTO 1
C
      END SubRoutine

C-----------------------------------------------------------------------
C                                                      *****************
C CONJUGATE-GRADIENTS SQUARED SOLVER FOR ASYMMETRIC    * C G S A P 1 D *
C MATRIX EQUATIONS WITH PRECONDITIONING - VERSION 1    *****************
C-----------------------------------------------------------------------
C                       *** DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 18/ENE/90
C-----------------------------------------------------------------------
C METODO ITERATIVO DE GRADIENTES CONJUGADOS CUADRADOS PRECONDICIONADO
C CON FACTORIZACION INCOMPLETA LU (ILU) PARA RESOLVER A x = b, DONDE  LA
C MATRIZ A ESTA ALMACENADA EN FORMATO SPARSE TIPO GUSTAVSON.
C-----------------------------------------------------------------------
C
C ARGUMENTOS:
C [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
C [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
C
C IA,JA,A  [I]      ESTRUCTURA Y VALORES DE LA MATRIZ A EN RR(C)O /1/
C IL,JL    [I]      ESTRUCTURA DE LOS FACTORES TRIANGULARES       /1/
C L        [I]      FACTORES TRIANGULARES INFERIORES EN FORMATO RR(L)O
C U,IUD    [I]      FACTORES TRIANGULARES SUPERIORES EN FORMATO CR(U)O.
C                   EL VECTOR IUD CONTIENE LAS INVERSAS DE Uii /2/,/3/
C R        [I,A]    VECTOR TERMINO INDEPENDIENTE
C R0,G,Q,  [W,X]    VECTORES AUXILIARES
C Z,W,H
C NOD      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
C EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
C                   ( |Xi-Xi+1|/|Xi| < EPS  y |Ri+1|/|B| < EPS ) o
C                   |Ri+1| = 0.   DONDE |X|=NORMA L2 DE X
C ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
C X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
C                   O: VECTOR SOLUCION
C ISTAT    [O]      STATUS:
C                   ISTAT=    0  => OK. EL RESIDUO INICIAL ES MUY CHICO.
C                                    X0 YA ES LA SOLUCION BUSCADA.
C                   ISTAT>    0  => OK. ISTAT CONTIENE EL NUMERO DE
C                                    ITERACIONES QUE SE NECESITARON.
C                   ISTAT=   -1  => NO CONVERGIO EN ITMAX ITERACIONES.
C                   ISTAT=   -2  => |b| ES PRACTICAMENTE NULA (*).
C                   ISTAT=-3xxx  => VARIABLE  ro(n-1) CASI NULA EN
C                                   ITERACION xxx (*).
C                   ISTAT=-4xxx  => VARIABLE  sigma(n-1) CASI NULA EN
C                                   ITERACION xxx (*).
C
C (*) EL ALGORITMO FALLA Y NO PUEDE CONTINUAR. SI NO SE ENCUENTRAN
C     PROBLEMAS EN LOS DATOS, Y ESTAMOS SEGUROS DE QUE LA MATRIZ ES
C     INVERTIBLE, LO MAS PROBABLE ES QUE ESTE METODO NO SEA APLICABLE
C     EN ESE CASO PARTICULAR. ESTE METODO NO GARANTIZA QUE EL PROCESO
C     PUEDA SER SIEMPRE LLEVADO A CABO. EN ESOS CASOS PROBAR CON EL
C     METODO BCG, O CONSULTAR LA REFERENCIA /4/.
C
C STOP:  Si el numero de iteraciones ITMAX es > que las dimensiones
C        utilizadas en el COMMON /NORMAS/ (parameter MAXIMUM)
C
C ALGORITMO: REF /4/
C
C REFERENCIAS:
C /1/ S. PISSANETZKY, "SPARSE MATRIX TECHNOLOGY", ACADEMIC PRESS, 1984.
C /2/ D. S. KERSHAW, J. COMP. PHYSICS 26 (1978) 43-65.
C /3/ D. S. KERSHAW, J. COMP. PHYSICS 38 (1980) 114-123.
C /4/ P. SONNEVELD et al, "MULTIGRID AND CONJUGATE GRADIENT METHODS AS
C     CONVERGENCE ACCELERATION TECHNIQUES", in "Multigrid Methods for
C     Integral and Differential Equations", CLARENDON PRESS, OXFORD, 1985.
C
C RUTINAS UTILIZADAS:
C    ENORM2D, RNORM2D, SPMUL2D, SCTXPYD, COPVECD, FWSROWD, BWSCOLD,
C    SCPRODD, LMULTVD, PUTVALD
C
C RUTINAS RELACIONADAS:
C    ILUFK1D, SYMCMR
C
C-----------------------------------------------------------------------
      SUBROUTINE CGSAP1DLU (IA,JA,AU,AL,AD,IL,JL,L,IUD,U,X,R,
     -                      R0,G,Q,H,W,Z,NOD,EPS,ITMAX,ISTAT)
C-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8 L(*), IUD(*), U(*), X(*), R0(*), R(*), G(*), H(*),
     -       Q(*), W(*), Z(*),AU(*),AL(*),AD(*)
      REAL*8 SCPRODD, RNORM2D, ENORM2D
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
C
      ISTAT = 0
!      IF (ITMAX.GT.MAXIMUM)
!     -   STOP 'CGSAP1D/ERROR: Aumentar MAXIMUM en fuente de CGSAP1D.'
C
C |B|: Norma L2 de B
      BNORM = RNORM2D(R,NOD)
      IF (BNORM.LT.TOO_SMALL) THEN
         ISTAT = -2
         RETURN
      END IF
C
C R0 = B - A.X0
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,X,W,NOD,6)
      CALL SCTXPYD (-1.D0,W,R,DUMMY,NOD,2)
      CALL COPVECD (R,R0,NOD)
C
C |R0|/|B| <<<< 1?
      RCONV = RNORM2D(R,NOD) / BNORM
C      IF (RCONV.LE.SMALL) RETURN
C     Modificada por Santiago Urquiza, que no vuelva sin iterar

C
C G[-1] = H[0] = 0.D0
      CALL PUTVALD (G,NOD,0.D0,0,iDUMMY)
      CALL PUTVALD (H,NOD,0.D0,0,iDUMMY)
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C
C           T             -1
C ro(n) = R0  Z ;  Z = (LU)  R ;  beta(n) = ro(n)/ro(n-1)
      RONM1= RON
      CALL COPVECD (R,Z,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      RON  = SCPRODD(R0,Z,NOD)
      IF (I.EQ.0) THEN
         BETA = 0.D0
      ELSE
         IF (DABS(RONM1).LT.TOO_SMALL) THEN
            ISTAT = -(3*1000+I)
            RETURN
         END IF
         BETA = RON/RONM1
      END IF
C
C Q = Z + beta(n) H[n] ;  G[N] = Q + beta(n) ( beta(n) G[n-1] + H[n] )
      CALL SCTXPYD (BETA,H,Z,Q,NOD,3)
      CALL SCTXPYD (BETA,G,H,DUMMY,NOD,1)
      CALL SCTXPYD (BETA,G,Q,DUMMY,NOD,1)
C
C              T              -1
C sigma(n) = R0  Z  ;  Z = (LU) A G[n]
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,G,Z,NOD,6)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      SIGMA = SCPRODD(R0,Z,NOD)
      IF (DABS(SIGMA).LT.TOO_SMALL) THEN
         ISTAT = -(4*1000+I)
         RETURN
      END IF
C
C H[n+1] = Q - alfa(n) Z  ;  alfa(n) = ro(n) / sigma(n)
      ALFA  = RON / SIGMA
      CALL SCTXPYD (-ALFA,Z,Q,H,NOD,3)
C
C X[n+1] = X[n] + Z  ;   Z = alfa(n) ( Q + H[n+1] )  ; guardar X[n+1] -> W
      CALL XPLUSYD (Q,H,Z,NOD,3)
      CALL SCTIMXD (ALFA,Z,NOD)
      CALL XPLUSYD (X,Z,W,NOD,3)
C
C R[n+1] = R[n] - A Z
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,Z,Q,NOD,6)
      CALL XPLUSYD (R,Q,DUMMY,NOD,-1)
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (RNORM2D(X,NOD)+SHIFT)
      RCONV = RNORM2D(R,NOD) / BNORM
      XNOR(I) = XCONV + 1.E-25
      RNOR(I) = RCONV + 1.E-25
c      if (mod(I,10).eq.0)then
c      WRITE (6,101)I,RCONV,XCONV
 101  Format ( ' ->CGS_iter=',I5,' |R|/|B|=',E13.5,'|DX|/|X|',E13.5)
c      endif
C
      CALL COPVECD (W,X,NOD)
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      endif
C     No convergio en itmax
      IF (I.EQ.ITMAX) THEN
         ISTAT = -1
         WRITE (6,101)I,RCONV,XCONV
         RETURN
      END IF
C
C Nueva iteracion
      I = I + 1
      GOTO 1
C
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE CGSAP1DLU2(IA,JA,AU,AL,AD,IL,JL,L,IUD,U,X,R,
     -                      R0,G,Q,H,W,Z,NOD,EPS,ITMAX,ISTAT)
C-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8 L(*), IUD(*), U(*), X(NOD),R0(NOD),R(NOD),G(NOD),H(NOD),
     -       Q(NOD), W(NOD), Z(NOD),AU(*),AL(*),AD(NOD),BB(NOD)
      REAL*8 SCPRODD, RNORM2D, ENORM2D
      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
C
      ISTAT = 0
!      IF (ITMAX.GT.MAXIMUM)
!     -   STOP 'CGSAP1D/ERROR: Aumentar MAXIMUM en fuente de CGSAP1D.'
C
C |B|: Norma L2 de B
      BNORM = DSqrt(Sum(R*R))
      IF (BNORM.LT.TOO_SMALL) THEN
         ISTAT = -2
         RETURN
      END IF
      BB=R
C      Set W=A.X0
C R0 = B - A.X0
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,X,W,NOD,6)
      R  = R - W
      R0 = R
C
C |R0|/|B| <<<< 1?
      Rent  = DSqrt(Sum(R*R))
      RCONV = Rent / BNORM
	Write(6,*)'First Residual',Rent,BNORM
C      IF (RCONV.LE.SMALL) RETURN
C     Modificada por Santiago Urquiza, que no vuelva sin iterar

C
C G[-1] = H[0] = 0.D0
      G = 0.0d0
	H = 0.0d0
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C
C           T             -1
C ro(n) = R0  Z ;  Z = (LU)  R ;  beta(n) = ro(n)/ro(n-1)
      RONM1= RON
      Z = R
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      RON  = Sum(R0*Z)

      IF (I.EQ.0) THEN
         BETA = 0.D0
      ELSE
         IF (DABS(RONM1).LT.TOO_SMALL) THEN
            ISTAT = -(3*1000+I)
            RETURN
         END IF
         BETA = RON/RONM1
      END IF
C
C Q = Z + beta(n) H[n] ;  G[N] = Q + beta(n) ( beta(n) G[n-1] + H[n] )
!      Q = Z + BETA * H
!      G = BETA*G + H
!      G = BETA*G + Q
	BETA2=BETA*BETA
      Q = BETA * H
	G = Q + BETA2*G 
      Q = Q + Z
      G = G + Q  

C
C              T              -1
C sigma(n) = R0  Z  ;  Z = (LU) A G[n]
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,G,Z,NOD,6)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      SIGMA = Sum(R0*Z)
      IF (DABS(SIGMA).LT.TOO_SMALL) THEN
         ISTAT = -(4*1000+I)
         RETURN
      END IF
C
C H[n+1] = Q - alfa(n) Z  ;  alfa(n) = ro(n) / sigma(n)
      ALFA  = RON / SIGMA
	H = Q - ALFA * Z
C
C X[n+1] = X[n] + Z  ;   Z = alfa(n) ( Q + H[n+1] )  ; guardar X[n+1] -> W
C X[n+1] se almacena en W
      Z = Alfa*(Q + H)
      W = X + Z
C
C R[n+1] = R[n] - A Z
C      Q = A.Z
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,Z,Q,NOD,6)
      R = R - Q
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (DSqrt(Sum(X*X))+SHIFT)
	Rsal = DSqrt(Sum(R*R))
      RCONV = Rsal / BNORM
      XNOR(I) = XCONV + 1.E-25
      RNOR(I) = RCONV + 1.E-25
	RatioR=Rsal/Rent
      if (mod(I,10).eq.0)then
      WRITE (6,101)I,RCONV,XCONV
      WRITE (6,*)RatioR,Rsal
      endif
 101  Format ( ' ->CGS_iter=',I5,' |R|/|B|= ',E8.2,'; |DX|/|X|= ',E8.2)

C      Se actualiza X
      X = W

!      CALL SPMUL2DLU (IA,JA,AU,AL,AD,X,Z,NOD,6)
!      Z = Z - BB
!      RNorma =  DSqrt(Sum(Z*Z)) / BNORM
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      endif
C     No convergio en itmax
!      IF (I.EQ.ITMAX .or. RatioR < 5.e-2 ) THEN
      IF (I.EQ.ITMAX ) THEN
         ISTAT = -1
         WRITE (6,101)I,RCONV,XCONV
!         WRITE (6,*)I,RNorma
         RETURN
      END IF
C
C Nueva iteracion
      I = I + 1
      GOTO 1
C
      END SubRoutine


C-----------------------------------------------------------------------
      SUBROUTINE CGSAP1DLUBand(IA,JA,AU,AL,AD,IP,Band,LDA,NLCA,NUCA,X,R,
     -                      R0,G,Q,H,W,Z,NOD,EPS,ITMAX,ISTAT)
C-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IL(1), JL(1)
      REAL*8 L(1), IUD(1), U(1), X(NOD),R0(NOD),R(NOD),G(NOD),H(NOD),
     -       Q(NOD), W(NOD), Z(NOD),AU(*),AL(*),AD(NOD),BB(NOD),
     _       IP(*),Band(LDA,NOD)
      REAL*8 SCPRODD, RNORM2D, ENORM2D
      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
C
      ISTAT = 0
!      IF (ITMAX.GT.MAXIMUM)
!     -   STOP 'CGSAP1D/ERROR: Aumentar MAXIMUM en fuente de CGSAP1D.'
C
C |B|: Norma L2 de B
      BNORM = DSqrt(Sum(R*R))
      IF (BNORM.LT.TOO_SMALL) THEN
         ISTAT = -2
         RETURN
      END IF
      BB=R
C      Set W=A.X0
C R0 = B - A.X0
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,X,W,NOD,6)
      R  = R - W
      R0 = R
C
C |R0|/|B| <<<< 1?
      RCONV = DSqrt(Sum(R*R)) / BNORM
C      IF (RCONV.LE.SMALL) RETURN
C     Modificada por Santiago Urquiza, que no vuelva sin iterar

C
C G[-1] = H[0] = 0.D0
      G = 0.0d0
	H = 0.0d0
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C
C           T             -1
C ro(n) = R0  Z ;  Z = (LU)  R ;  beta(n) = ro(n)/ro(n-1)
      RONM1= RON
!      Z = R
!      CALL DLFSRB (NOD, BAND, LDA, NLCA, NUCA, IP, R,1,Z)
!      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
!      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      RON  = Sum(R0*Z)

      IF (I.EQ.0) THEN
         BETA = 0.D0
      ELSE
         IF (DABS(RONM1).LT.TOO_SMALL) THEN
            ISTAT = -(3*1000+I)
            RETURN
         END IF
         BETA = RON/RONM1
      END IF
C
C Q = Z + beta(n) H[n] ;  G[N] = Q + beta(n) ( beta(n) G[n-1] + H[n] )
      Q = Z + BETA * H
      G = BETA*G + H
      G = BETA*G + Q
C
C              T              -1
C sigma(n) = R0  Z  ;  Z = (LU) A G[n]
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,G,BB,NOD,6)
!      CALL DLFSRB (NOD, BAND, LDA, NLCA, NUCA, IP, BB,1,Z)
!      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
!      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
      SIGMA = Sum(R0*Z)
      IF (DABS(SIGMA).LT.TOO_SMALL) THEN
         ISTAT = -(4*1000+I)
         RETURN
      END IF
C
C H[n+1] = Q - alfa(n) Z  ;  alfa(n) = ro(n) / sigma(n)
      ALFA  = RON / SIGMA
	H = Q - ALFA * Z
C
C X[n+1] = X[n] + Z  ;   Z = alfa(n) ( Q + H[n+1] )  ; guardar X[n+1] -> W
C X[n+1] se almacena en W
      Z = Alfa*(Q + H)
      W = X + Z
C
C R[n+1] = R[n] - A Z
C      Q = A.Z
      CALL SPMUL2DLU (IA,JA,AU,AL,AD,Z,Q,NOD,6)
      R = R - Q
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (DSqrt(Sum(X*X))+SHIFT)
      RCONV = DSqrt(Sum(R*R)) / BNORM
      XNOR(I) = XCONV + 1.E-25
      RNOR(I) = RCONV + 1.E-25
c      if (mod(I,10).eq.0)then
c      WRITE (6,101)I,RCONV,XCONV
 101  Format ( ' ->CGS_iter=',I5,' |R|/|B|= ',E8.2,'; |DX|/|X|= ',E8.2)
c      endif

C      Se actualiza X
      X = W

!      CALL SPMUL2DLU (IA,JA,AU,AL,AD,X,Z,NOD,6)
!      Z = Z - BB
!      RNorma =  DSqrt(Sum(Z*Z)) / BNORM
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      endif
C     No convergio en itmax
      IF (I.EQ.ITMAX) THEN
         ISTAT = -1
         WRITE (6,101)I,RCONV,XCONV
!         WRITE (6,*)I,RNorma
         RETURN
      END IF
C
C Nueva iteracion
      I = I + 1
      GOTO 1
C
      END SubRoutine





C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C-----------------------------------------------------------------------
      SUBROUTINE BCGAP1D (IA,JA,A,IL,JL,L,IUD,U,X,R,RB,P,PB,W,Z,
     -                    NOD,EPS,ITMAX,ISTAT)
C-----------------------------------------------------------------------
C                                                        ***************
C BICONJUGATE-GRADIENT SOLVER FOR ASYMMETRIC MATRIX      * B C G A P 1 *
C EQUATIONS WITH PRECONDITIONING - VERSION 1             ***************
C-----------------------------------------------------------------------
C                       *** TODO REAL*8 ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 21/NOV/89
C-----------------------------------------------------------------------
C UTILIZA EL METODO ITERATIVO DE GRADIENTES BICONJUGADOS PRECONDICIONADO
C CON FACTORIZACION INCOMPLETA LU (ILU) PARA RESOLVER A x = b, DONDE  LA
C MATRIZ A ESTA ALMACENADA EN FORMATO SPARSE TIPO GUSTAVSON.
C-----------------------------------------------------------------------
C
C ARGUMENTOS:
C [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
C [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
C
C IA,JA,A  [I]      ESTRUCTURA Y VALORES DE LA MATRIZ A EN RR(C)O /1/
C IL,JL    [I]      ESTRUCTURA DE LOS FACTORES TRIANGULARES       /1/
C L        [I]      FACTORES TRIANGULARES INFERIORES EN FORMATO RR(L)O
C U,IUD    [I]      FACTORES TRIANGULARES SUPERIORES EN FORMATO CR(U)O.
C                   EL VECTOR IUD CONTIENE LAS INVERSAS DE Uii /2/,/3/
C R        [I,A]    VECTOR TERMINO INDEPENDIENTE
C RB,P,PB, [W,X]    VECTORES AUXILIARES
C Z,W
C NOD      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
C EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
C                   ( |Xi-Xi+1|/|Xi| < EPS  y |Ri+1|/|B| < EPS ) o
C                   |Ri+1| = 0.   DONDE |X|=NORMA L2 DE X
C ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
C X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
C                   O: VECTOR SOLUCION
C ISTAT    [O]      STATUS DE BCGAP1: ISTAT=0  => EL RESIDUO INICIAL ES
C                                                 MUY CHICO. ES MUY PRO-
C                                                 BABLE QUE X0 YA SEA LA
C                                                 SOLUCION BUSCADA.
C                                     ISTAT>0  => OK. ISTAT CONTIENE EL
C                                                 NUMERO DE ITERACIONES
C                                                 QUE SE NECESITARON.
C                                     ISTAT=-1 => NO CONVERGIO EN ITMAX
C                                                 ITERACIONES.
C
C
C ALGORITMO: REF /4/
C
C REFERENCIAS:
C /1/ S. PISSANETZKY, "SPARSE MATRIX TECHNOLOGY", ACADEMIC PRESS, 1984
C /2/ D. S. KERSHAW, J. COMP. PHYSICS 26 (1978) 43-65
C /3/ D. S. KERSHAW, J. COMP. PHYSICS 38 (1980) 114-123
C /4/ D. V. ANDERSON et al, COMP. PHYS. COMMUNICATIONS 51 (1988) 391-403
C
C RUTINAS UTILIZADAS:
C    ENORM2D, RNORM2D, SPMUL2D, SCTXPYD, COPVECD, FWSROWD, BWSCOLD, 
C    SCPROD, LMULTVD, PUTVALD
C
C RUTINAS RELACIONADAS:
C    ILUFK1D, SYMCMR 
C
C-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8 A(*), L(*), IUD(*), U(*), X(*), R(*), RB(*), P(*), PB(*),
     -       W(*), Z(*)
      REAL*8 ALFA,BETA,BETA0,DUMMY
      REAL*8 RNORM2D,ENORM2D,SCPRODD,EPS
      COMMON /NORMAS/ NORMAS,XNOR(0:MAXIMUM),RNOR(0:MAXIMUM)
      DATA SMALL,SHIFT /1.E-10,1.E-8/
C
      ISTAT = 0
      IF (ITMAX.GT.MAXIMUM) 
     -   STOP 'BCGAP1D/ERROR: aumentar MAXIMUM en fuente de BCGAP1D.'
C
C |B|: Norma L2 de B
      BNORM = RNORM2D(R,NOD)
C
C R0 = B - A.X0
      CALL SPMUL2D (IA,JA,A,DUMMY,X,W,NOD,2)
      CALL SCTXPYD (-1.D0,W,R,DUMMY,NOD,2)
C
C |R0|/|B| <<<< 1?
      RCONV = RNORM2D(R,NOD) / BNORM
      IF (RCONV.LE.SMALL) RETURN
C
C _     T -1
C R0 = U L  R0
      CALL COPVECD (R,W,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,W,NOD)
      CALL LMULTVD (IL,JL,U,IUD,1,W,RB,NOD)
C
C         -1               _    -1
C P0 = (LU)  R0 ; beta0 = (R,(LU) R)
      CALL COPVECD (R,P,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,P,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,P,NOD)
      BETA0 = SCPRODD(RB,P,NOD)
C
C _       T -1
C P0 = (LL  )  R0
      CALL COPVECD (R,PB,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,PB,NOD)
      CALL BWSCOLD (IL,JL,L,DUMMY,0,PB,NOD)
C
C Z0 = P0
      CALL COPVECD (P,Z,NOD)
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C         _         _
C alfa = (Ri,Zi) / (Pi,A Pi)           A Pi -> W
      CALL SPMUL2D (IA,JA,A,DUMMY,P,W,NOD,2)
      ALFA  = BETA0 / SCPRODD(PB,W,NOD)
C
C Ri+1 = Ri - alfa W ;   Xi+1 = Xi + alfa Pi     Xi+1 -> W
      CALL SCTXPYD (-ALFA,W,R,DUMMY,NOD,2)
      CALL SCTXPYD ( ALFA,P,X,W,NOD,3)
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (RNORM2D(X,NOD)+SHIFT)
      RCONV = RNORM2D(R,NOD) / BNORM
C
C     ************************
      XNOR(I) = XCONV + 1.E-10
      RNOR(I) = RCONV + 1.E-10
C     ************************
C
!      CALL COPVECD (W,X,NOD)
!      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. ) RETURN
 101  Format ( ' -> BCG_iter=',I5,' |R|/|B|=',E13.5,' |DX|/|X|=',E13.5)
C
      CALL COPVECD (W,X,NOD)
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      END IF

C
C     No convergio en itmax
      IF (I.EQ.ITMAX-1) THEN
       WRITE (6,101)I,RCONV,XCONV
         ISTAT = -1
         RETURN
      END IF
C
C _      _     T _
C Ri+1 = Ri - A  Pi
      CALL PUTVALD (W,NOD,0.D0,0,DUMMY)
      CALL SPMUL2D (IA,JA,A,DUMMY,PB,W,NOD,3)
      CALL SCTXPYD (-ALFA,W,RB,DUMMY,NOD,2)
C
C           -1
C Zi+1 = (LU)  Ri+1
      CALL COPVECD (R,Z,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,Z,NOD)
C         _             _
C beta = (Ri+1,Zi+1) / (Ri,Zi)
      BETA1 = SCPRODD (RB,Z,NOD)
      BETA  = BETA1 / BETA0
C
C Pi+1 = beta Pi + Zi+1
      CALL SCTXPYD (BETA,P,Z,DUMMY,NOD,1)
C
C _           _       -T _
C Pi+1 = beta Pi + (LU)  Ri+1
      CALL COPVECD (RB,W,NOD)
      CALL FWSROWD (IL,JL,U,IUD,1,W,NOD)
      CALL BWSCOLD (IL,JL,L,DUMMY,0,W,NOD)
      CALL SCTXPYD (BETA,PB,W,DUMMY,NOD,1)
C
C Nueva iteracion
      I = I + 1
      BETA0 = BETA1
      GO TO 1
C
      END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C-----------------------------------------------------------------------
      SUBROUTINE MCGAP1D (IA,JA,A,IL,JL,L,IUD,U,X,R,P,Q,W,Z,NOD,EPS,
     -                    ITMAX,ISTAT)
C-----------------------------------------------------------------------
C                                                      *****************
C MODIFIED CONJUGATE-GRADIENT ALGORITHM FOR ASYMMETRIC * M C G A P 1 D *
C MATRIX EQUATIONS WITH PRECONDITIONING - VERSION 1    *****************
C-----------------------------------------------------------------------
C                      *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 15/NOV/89
C-----------------------------------------------------------------------
C UTILIZA EL METODO ITERATIVO DE GRADIENTES CONJUGADOS PRECONDICIONADO
C CON FACTORIZACION INCOMPLETA LU (ILU) Y SIMETRIZACION PARA RESOLVER
C A x = b, DONDE  LA MATRIZ NO SIMETRICA A ESTA ALMACENADA EN FORMATO
C SPARSE TIPO GUSTAVSON.
C-----------------------------------------------------------------------
C
C ARGUMENTOS:
C [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
C [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
C
C IA,JA,A  [I]      ESTRUCTURA Y VALORES DE LA MATRIZ A EN RR(C)O /1/
C IL,JL    [I]      ESTRUCTURA DE LOS FACTORES TRIANGULARES       /1/
C L        [I]      FACTORES TRIANGULARES INFERIORES EN FORMATO RR(L)O
C U,IUD    [I]      FACTORES TRIANGULARES SUPERIORES EN FORMATO CR(U)O.
C                   EL VECTOR IUD CONTIENE LAS INVERSAS DE Uii /2/,/3/
C R        [I,A]    VECTOR TERMINO INDEPENDIENTE
C P,Q,Z,W  [W,X]    VECTORES AUXILIARES
C NOD      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
C EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
C                   ( |Xi-Xi+1|/|Xi| < EPS y |Ri+1|/|B| < EPS ) o
C                     |Ri+1| = 0.   DONDE |X|=NORMA L2 DE X.
C ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
C X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
C                   O: VECTOR SOLUCION
C ISTAT    [O]      STATUS DE MCGAP1: ISTAT=0  => EL RESIDUO INICIAL ES
C                                                 MUY CHICO. ES MUY PRO-
C                                                 BABLE QUE X0 YA SEA LA
C                                                 SOLUCION.
C                                     ISTAT>0  => OK. ISTAT CONTIENE EL
C                                                 NUMERO DE ITERACIONES
C                                                 QUE FUERON NECESARIAS.
C                                     ISTAT=-1 => NO CONVERGIO EN ITMAX
C                                                 ITERACIONES.
C
C ALGORITMO: REF /4/
C
C REFERENCIAS:
C /1/ "SPARSE MATRIX TECHNOLOGY", S. PISSANETZKY, ACADEMIC PRESS, 1984
C /2/ D. S. KERSHAW, J. COMP. PHYSICS 26 (1978) 43-65
C /3/ D. S. KERSHAW, J. COMP. PHYSICS 38 (1980) 114-123
C /4/ D. V. ANDERSON et al, COMP. PHYS. COMMUNICATIONS 51 (1988) 391-403
C
C RUTINAS UTILIZADAS:
C    ENORM2D, RNORM2D, SPMUL2D, SCTXPYD, COPVECD, FWSROWD, BWSCOLD, 
C    SCPRODD, PUTVALD
C
C RUTINAS RELACIONADAS:
C    ILUFK1D, SYMCMR
C
C-----------------------------------------------------------------------
!      PARAMETER (MAXIMUM=500)
      PARAMETER (MAXIMUM=10000)
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8 A(*), L(*), IUD(*), U(*), X(*), R(*), P(*), Q(*),W(*),Z(*)
      REAL*8 ALFA,BETA,BETA0,DUMMY
      REAL*8 ENORM2D,RNORM2D,SCPRODD,EPS
      COMMON /NORMAS/ NORMAS,XNOR(0:MAXIMUM),RNOR(0:MAXIMUM)
      DATA SMALL,SHIFT /1.E-6,1.E-8/
C
      ISTAT = 0
      IF (ITMAX.GT.MAXIMUM) 
     -   STOP 'MCGAP1D/ERROR: aumentar MAXIMUM en fuente de MCGAP1D.'
C
C |B|: Norma L2 de B
      BNORM = RNORM2D(R,NOD)
C
C R0 = B - A.X0
      CALL SPMUL2D (IA,JA,A,DUMMY,X,W,NOD,2)
      CALL SCTXPYD (-1.D0,W,R,DUMMY,NOD,2)
C
C |R0|/|B| <<<< 1?
      RCONV = RNORM2D(R,NOD) / BNORM
      IF (RCONV.LT.SMALL) RETURN
C
C       T                 T -1
C Q0 = A Z   con   Z = (LL  )  R0
      CALL COPVECD (R,Z,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL PUTVALD (Q,NOD,0.D0,0,DUMMY)
      CALL SPMUL2D (IA,JA,A,DUMMY,Z,Q,NOD,3)
C
C        T -1
C P0 = (U U)  Q0 ;  beta0 = (R0,Z)
      CALL COPVECD (Q,P,NOD)
      CALL FWSROWD (IL,JL,U,IUD,1,P,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,P,NOD)
      BETA0 = SCPRODD(R,Z,NOD)
C
C-----------------------------------------------------------------------
C                           LOOP ITERATIVO
C-----------------------------------------------------------------------
C
      I = 0
    1 CONTINUE
      ISTAT = I + 1
C
C alfa = (Ri,Zi) / (Pi,Qi)
      ALFA  = BETA0 / SCPRODD(P,Q,NOD)
C
C Ri+1 = Ri - alfa A Pi ;   Xi+1 = Xi + alfa Pi     Xi+1 -> W
      CALL SPMUL2D (IA,JA,A,DUMMY,P,W,NOD,2)
      CALL SCTXPYD (-ALFA,W,R,DUMMY,NOD,2)
      CALL SCTXPYD ( ALFA,P,X,W,NOD,3)
C
C Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
C                 |Ri+1| = 0.D0 ?
      XCONV = ENORM2D(X,W,NOD) / (RNORM2D(X,NOD)+SHIFT)
      RCONV = RNORM2D(R,NOD) / BNORM
      XNOR(I) = XCONV + 1.E-10
      RNOR(I) = RCONV + 1.E-10
C
 101  Format ( ' -> MCG_iter=',I5,' |R|/|B|=',E13.5,' |DX|/|X|=',E13.5)
C
      CALL COPVECD (W,X,NOD)
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. )then
       WRITE (6,101)I,RCONV,XCONV
       RETURN
      END IF
!      CALL COPVECD (W,X,NOD)
!      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. RCONV.EQ.0. ) RETURN
C     No convergio en itmax
      IF (I.EQ.ITMAX-1) THEN
         ISTAT = -1
       WRITE (6,101)I,RCONV,XCONV
         RETURN
      END IF
C
C      T            T -1
C W = A Z  ; Z = (LL  )  R
      CALL COPVECD (R,Z,NOD)
      CALL FWSROWD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL BWSCOLD (IL,JL,L,DUMMY,0,Z,NOD)
      CALL PUTVALD (W,NOD,0.D0,0,DUMMY)
      CALL SPMUL2D (IA,JA,A,DUMMY,Z,W,NOD,3)
C
C beta = (Ri+1,Zi+1) / (Ri,Zi)
      BETA1 = SCPRODD(R,Z,NOD)
      BETA  = BETA1 / BETA0
C
C                   T
C Qi+1 = beta Qi + A Zi+1
      CALL SCTXPYD (BETA,Q,W,DUMMY,NOD,1)
C
C          T -1
C Pi+1 = (U U)  Qi+1
      CALL COPVECD (Q,P,NOD)
      CALL FWSROWD (IL,JL,U,IUD,1,P,NOD)
      CALL BWSCOLD (IL,JL,U,IUD,1,P,NOD)
C
C Nueva iteracion
      I = I + 1
      BETA0 = BETA1
      GO TO 1
C
      END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



C-----------------------------------------------------------------------
      SUBROUTINE COPVECD (V1,V2,N)
C-----------------------------------------------------------------------
C                                                        ***************
C COPIA EL VECTOR V1 EN V2                               * C O P V E C *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
      REAL*8 V1(*), V2(*)
      DO 10 I = 1, N
         V2(I) = V1(I)
   10 CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      REAL*8 FUNCTION ENORM2D (X,Y,N)
C-----------------------------------------------------------------------
C                                                        ***************
C NORMA CUADRATICA L2 DEL ERROR X-Y                      * E N O R M 2 *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -  9/ENE/90
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 X(*),Y(*)
      ENORM2D = 0.D0
      DO 10 I = 1, N
         ENORM2D = ENORM2D + (X(I)-Y(I))**2
   10 CONTINUE
      ENORM2D = DSQRT(ENORM2D)
      RETURN
      END Function

C-----------------------------------------------------------------------
      SUBROUTINE ENSIMD (IE,JE,IET,JET,IA,JA,N,MEDA,JP,IX,KDIAG)
C-----------------------------------------------------------------------
C                                                        ***************
C ENSAMBLAJE SIMBOLICO INCLUYENDO CONDICIONES DIRICHLET  * E N S I M D *
C                                                        ***************
C-----------------------------------------------------------------------
C
C  MODIFICACION DE LA RUTINA CONOD (PISSANETZKY-DARY)
C
C  CONOD:  DETERMINA LA ESTRUCTURA IA,JA DE LA MATRIZ DE ENSAMBLAJE
C          NODAL EN EL CASO EN QUE ESTA MATRIZ ES CUADRADA. SE SUPONE
C          QUE LOS ELEMENTOS DE LA DIAGONAL ESTAN SIEMPRE PRESENTES, Y
C          POR LO TANTO NO APARECEN EN IA,JA.
C
C  ENSIMD: REALIZA EL TRABAJO DE CONOD PERO DEJANDO SOLO EL ELEMENTO
C          DIAGONAL DE A SI LA ECUACION CORRESPONDE A UNA CONDICION
C          DE DIRICHLET. ADEMAS, A DIFERENCIA DE CONOD, CONSIDERA LOS
C          ELEMENTOS DIAGONALES.
C
C  ARGUMENTOS:
C
C -> IE,JE : CONECTIVIDAD DE LA RED EN RF(C)D
C -> IET,JET : TRASPUESTA DE IE,JE EN RF(C)D
C -> N : NUMERO DE NODOS (ORDEN DE LA MATRIZ)
C -> MEDA : MEMORIA RESERVADA PARA JA()
C -> IX : VECTOR AUXILIAR DE DIMENSION N. (LLAVE MULTIPLE)
C <- JP : LONGITUD DE JA
C
C  MODIFICACION DE ARGUMENTOS RESPECTO DE CONOD:
C
C <- IA,JA : ESTRUCTURA DE LA MATRIZ ENSAMBLADA, EN RF(C)O O RF(IC)O
C            SEGUN EL VALOR DE KDIAG.
C -> IA : EN ENTRADA IA(I)=1 SI EL NODO I TIENE CONDICION DE DIRICHLET
C                    IA(I)=0 EN OTRO CASO
C -> KDIAG : INDICADOR DIAGONAL;  1 => LA DIAGONAL SE INCLUYE EN IA,JA
C                                 0 => LA DIAGONAL NO SE INCLUYE
C
C  SUBPROGRAMAS : NINGUNO
C
C-----------------------------------------------------------------------
      DIMENSION IA(*),JA(*),IE(*),JE(*),IET(*),JET(*),IX(*)
C
      IF(N.LT.2)GOTO 360
      IF(MEDA.LT.1)GOTO 360
C
      NH = N+1
      DO 10 I = 1,N
         IX(I) = 0
   10 CONTINUE
      JP = 1
      DO 140 I = 1,N
C
C MODIFICACION PARA INCLUIR CONDICIONES DE DIRICHLET (LARRETEGUY)
C
         IF (IA(I).EQ.1) THEN
            IF (KDIAG.EQ.1) THEN
               IA(I) = JP
               JA(JP) = I
               JP = JP + 1
            END IF
            GOTO 140
         END IF
C
C FIN MODIFICACION
C
         IA(I) = JP
         IETA = IET(I)
         IETB = IET(I+1)-1
         DO 130 IP = IETA,IETB
            J = JET(IP)
            IEA = IE(J)
            IEB = IE(J+1)-1
            DO 120 KP = IEA,IEB
               K = JE(KP)
C AGREGADO DE ".AND.KDIAG.EQ.0" PARA ANULAR ELIMINACION DE DIAGONAL
               IF(K.EQ.I.AND.KDIAG.EQ.0) GOTO 120
               IF(IX(K).GE.I)GOTO 120
               IX(K) = I
  120       CONTINUE
  130    CONTINUE
         DO 135 KP = 1, N
            IF (IX(KP).NE.I)GOTO 135
            IF(JP.GT.MEDA)GOTO 360
            JA(JP) = KP
            JP = JP + 1
  135    CONTINUE
  140 CONTINUE
      IA(N+1) = JP
      JP = JP-1
  200 CONTINUE
      RETURN
  360 WRITE(6,365)N,MEDA,I,J,K,IP,JP,KP,IEA,IEB,IETA,IETB
      STOP
  365 FORMAT(13H SPTMC. ERROR,20I5)
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE FWSROWD (IL,JL,L,ILD,KLD,B,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C FORWARD-SUSTITUTION BY ROWS                            * F W S R O W *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 30/OCT/89
C-----------------------------------------------------------------------
C REFERENCIAS:
C   "Sparse Matrix Technology", S. Pissanetzky, Academic Press, 1984
C-----------------------------------------------------------------------
C
C  REALIZA LA REDUCCION POR FILAS PARA RESOLVER  L y = b.
C  JUNTO CON BWSCOL RESUELVE EL SISTEMA L U x = b. DONDE U ESTA
C  ALMACENADA POR COLUMNAS Y L POR FILAS.
C
C     DATOS DE ENTRADA:
C     NOD     NUMERO DE INCOGNITAS
C     IL,JL   ESTRUCTURA DE LA MATRIZ L TRIANGULARIZADA RR(L)U
C     L       ELEMENTOS EXTRADIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     ILD     INVERSAS DE LOS ELEMENTOS DIAGONALES DE L (SI KLD=1)
C     KLD     SWITCH: INDICA SI SE PROVEE EL VECTOR ILD
C                     KLD=1 => SE PROVEE ILD
C                     KLD=0 => NO SE DA ILD. SE SUPONE QUE LA MATRIZ
C                              L TIENE DIAGONAL UNITARIA.
C     B       VECTOR TERMINO INDEPENDIENTE
C
C     RESULTADOS:
C     Y->B    SOLUCION
C
C-----------------------------------------------------------------------
C
      INTEGER IL(*), JL(*)
      REAL*8  L(*),  ILD(*), B(*), ZZ
C
      IF (KLD.EQ.1) THEN
         B(1) = B(1)*ILD(1)
         DO 20 I = 2, NOD
         ILA = IL (I)
         ILB = IL (I+1) - 1
         ZZ  = B(I)
         IF (ILB.GE.ILA) THEN
            DO 10 K = ILA, ILB
               ZZ = ZZ - L(K) * B(JL(K))
   10       CONTINUE
         END IF
         B(I) = ZZ*ILD(I)
   20    CONTINUE
      ELSE
         DO 40 I = 2, NOD
         ILA = IL (I)
         ILB = IL (I+1) - 1
         IF (ILB.GE.ILA) THEN
            ZZ = B(I)
            DO 30 K = ILA, ILB
               ZZ = ZZ - L(K) * B(JL(K))
   30       CONTINUE
            B(I) = ZZ
         END IF
   40    CONTINUE
      END IF
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE ILUFK1D (IA,JA,A,IL,JL,L,ID,U,IP,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C FACTORIZACION INCOMPLETA (L,U) PARA MATRICES NO        * I L U F K 1 *
C SIMETRICAS DE ESTRUCTURA SIMETRICA SPARSE - VERSION 1  ***************
C-----------------------------------------------------------------------
C               *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 25/OCT/89
C-----------------------------------------------------------------------
C
C  OBJETIVO:
C           Encontrar dos matrices L y U tales que
C                           -1
C                       (L*U)  * A = I + E
C                                                                  -1
C  donde I es la matriz identidad y E una matriz error, siendo (L*U)
C  una aproximacion de la inversa de A.
C
C  ALGORITMO:
C            Descomposicion L U incompleta sin pivoting,  manteniendo
C  la estructura sparse de la matriz original  para los factores   LU
C  triangulares (no se permite el llenado). Los pivotes se tratan por
C  el metodo de Kershaw /1/,/2/.
C
C     nbm = numero de bits de la mantisa de un real
C     pat = {(i,j)} conjunto de los pares (i,j)  /  A(i,j) es no nulo
C     vector columna uj / uj(k) = U(k,j) ; k=1,j-1
C     vector fila    lj / lj(k) = L(j,k) ; k=1,j-1
C
C
C  i) Calcular elemento fila i de U y columna i de L
C
C     U(i,i) = A(i,i) - BII   con BII = li(m)*ui(m)
C
C     U(i,j) = A(i,j) - BIJ   con BIJ = li(m)*uj(m)  ; (i,j) -> pat
C     U(i,j) = 0.                                    ; (i,j) +> pat
C
C     L(j,i) = A(j,i) - CJI   con CJI = lj(m)*ui(m)  ; (i,j) -> pat
C     L(j,i) = 0.                                    ; (i,j) +> pat
C
C     donde m = 1,i-1 ;  j = i+1,nod
C           -> : pertenece ; +> : no pertenece
C
C  ii) Evaluar
C
C      MU = max |U(i,j)| ;    SIGMA = max |L(j,i)| ;   j = i+1, nod
C
C  iii) Modificar pivote i
C          2                                                  1/2
C      si U(i,i) < PIVOT2   =>   U(i,i) = signo(U(i,i)) PIVOT2
C                       -nbm
C      donde  PIVOT2 = 2    MU SIGMA
C
C  iv) Normalizar columna i de L
C
C      L(j,i) = L(j,i) / U(i,i)
C
C  v) Incrementar i y volver a i)
C
C  ALMACENAMIENTO SPARSE /3/:
C
C     Matriz A en representacion RR(C)O
C                                            ___
C                               |\           \  |
C     AILDU = L * U ;       L = | \   ;   U = \ |
C                               |__\           \|
C                               RR(L)O      CR(U)O
C
C     L(i,i) = 1 (no se almacenan);      ID(i) = 1/U(i,i)
C
C  REFERENCIAS:
C  /1/ D.S. Kershaw, Journal of Computational Physics, 26 (1978) 43-65
C  /2/ D.S. Kershaw, Journal of Computational Physics, 38 (1980) 114-123
C  /3/ S. Pissanetzky, "Sparse Matrix Technology", Academic Press, 1984
C
C-----------------------------------------------------------------------
      REAL*8   A(*), L(*),  ID(*), U(*)
      INTEGER  IL(*), JL(*), IA(*), JA(*), IP(*)
      REAL*8   MANTIS,UABS,LABS,MU,SIGMA
      REAL*8   PIVOT2, BIJ, BII, CJI
C     **********************
C      COMMON /KPIVOT/ KPIVOT
C     **********************
      DATA     CERO /0.0d0/
C
C     NBM: numero de bits de la mantisa de un REAL en precision doble
C
      NBM    = 53
!      NBM    = 36
!      NBM    = 24
      MANTIS = 1.0d0/2.0d0**NBM
C     **********
      KPIVOT = 0
C     **********
C
C Limpiar vector auxiliar multiple-switch IP
C
      DO 10 I = 1, NOD
         IP(I) = 0
   10 CONTINUE
C
C Computo de ID(i)=1/U(i,i), U(i,j) y L(j,i) ;  i=1,NOD ; j=i+1,nod
C
C     Barrido sobre columnas de U y filas de L

      DO 100 I = 1, NOD
         MU    = CERO
         SIGMA = CERO
         BII = 0.0d0
         ILA = IL(I)
         ILB = IL(I+1) - 1
         NIL = ILB - ILA + 1
         IAA = IA(I)
         IAB = IA(I+1) - 1
         NIA = IAB - IAA + 1
C        Tienen ui y li componentes no nulas?
         IF (ILB.GE.ILA) THEN
C           Calculo de B(i,i) y el multiple-switch IP(i), el cual sera
C           usado en el calculo de todos los B(i,j) y C(j,i)
            DO 40 K = ILA, ILB
               M = JL(K)
               IP(M) = K
               BII = BII + L(K)*U(K)
   40       CONTINUE
         END IF
C        Posicion del Aii
         KII   = IAA + NIL
         ID(I) = A(KII) - BII
         IUA = KII + 1
         IUB = IAB
C        No barrer en j si no existe al menos un j  /  (i,j) -> pat
         IF (IUB.LT.IUA) GOTO 71
C        Barrido por col. sobre fila i de U y por filas sobre col. i de L
         DO 70 KU = IUA, IUB
            J   = JA(KU)
            ILC = IL(J)
            ILD = IL(J+1) - 1
            BIJ = CERO
            CJI = CERO
            IF (ILD.GT.ILC) THEN
C              Productos escalares BIJ y CJI
               DO 60 K = ILC, ILD
                  M  = JL(K)
                  IF (M.EQ.I) GOTO 65
                  KP = IP(M)
                  IF (KP.GT.0) THEN
                     BIJ = BIJ + L(KP)*U(K)
                     CJI = CJI + L(K)*U(KP)
                  END IF
   60          CONTINUE
   65          KLU = K
            ELSE
               KLU = ILD
            END IF
C           Encontrar posiciones de A(i,j) y A(j,i)
            NIU = KLU - ILC
            KJI = IA(J) + NIU
            KIJ = KII
   68       KIJ = KIJ + 1
            IF (JA(KIJ).NE.J) GOTO 68
            U(KLU) = A(KIJ) - BIJ
            L(KLU) = A(KJI) - CJI
            UABS   = DABS(U(KLU))
            LABS   = DABS(L(KLU))
            IF (MU   .LT.UABS) MU    = UABS
            IF (SIGMA.LT.LABS) SIGMA = LABS
   70    CONTINUE
C        Correccion del pivot segun Kershaw /1/
   71    CONTINUE
         ISIGN  = 1
         IF (ID(I).LT.0) ISIGN = -1
         PIVOT2 = MANTIS*SIGMA*MU
!!!!!!!!! WARNING NO SE MODIFICA EL PIVOTE
          IF (ID(I)**2.LT.PIVOT2) THEN
C           *******************
            KPIVOT = KPIVOT + 1
C           *******************
            ID(I) = DFloat(ISIGN)/DSQRT(PIVOT2)
! aqui se puede evitar la modificacion del pivote
         ELSE
            ID(I) = 1.0d0/ID(I)
         END IF
C        Normalizacion de L(j,i)
         IF (IUB.GE.IUA) THEN
            DO 80 K = IUA, IUB
               J  = JA(K)
               KL = IL(J) - 1
C              Loop 75: busca posicion de L(i,j) en fila i de L
   75          KL = KL + 1
               M  = JL(KL)
               IF (M.NE.I) GOTO 75
C              Normaliza con el nuevo pivot
               L(KL) = L(KL)*ID(I)
   80       CONTINUE
         END IF
C        Si ui y li tenian componentes no nulas limpiar switch IP
         IF (ILB.GE.ILA) THEN
            DO 90 K = ILA, ILB
               M = JL(K)
               IP(M) = 0
   90       CONTINUE
         END IF
  100 CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE ILUFK2D (IL,JL,IU,JU,AL,AD,AU,L,ID,U,IP,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C FACTORIZACION INCOMPLETA (L,D,U) PARA MATRICES NO      * I L U F K 2 *
C SIMETRICAS DE ESTRUCTURA SIMETRICA SPARSE - VERSION 2  ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 25/OCT/89
C-----------------------------------------------------------------------
C
C  OBJETIVO:
C           Encontrar tres matrices L, D y U tales que
C                             -1
C                       (L*D*U)  * A = I + E
C                                                                    -1
C  donde I es la matriz identidad y E una matriz error, siendo (L*D*U)
C  una aproximacion de la inversa de A.
C
C  ALGORITMO:
C            Descomposicion LDU incompleta sin pivoting,  manteniendo
C  la estructura sparse de la matriz original  para los factores  LDU
C  triangulares (no se permite el llenado). Los pivotes se tratan por
C  el metodo de Kershaw /1/,/2/.
C
C     nbm = numero de bits de la mantisa de un real
C     pat = {(i,j)} conjunto de los pares (i,j)  /  A(i,j) es no nulo
C     vector columna uj / uj(k) = U(k,j) ; k=1,j-1
C     vector fila    lj / lj(k) = L(j,k) ; k=1,j-1
C
C
C  i) Calcular elemento (i,i) de D, fila i de U y columna i de L
C
C     D(i,i) = A(i,i) - BII   con BII = li(m)*ui(m)
C
C     U(i,j) = A(i,j) - BIJ   con BIJ = li(m)*uj(m)  ; (i,j) -> pat
C     U(i,j) = 0.                                    ; (i,j) +> pat
C
C     L(j,i) = A(j,i) - CJI   con CJI = lj(m)*ui(m)  ; (i,j) -> pat
C     L(j,i) = 0.                                    ; (i,j) +> pat
C
C     donde m = 1,i-1 ;  j = i+1,nod
C           -> : pertenece ; +> : no pertenece
C
C  ii) Evaluar
C
C      MU = max |U(i,j)| ;    SIGMA = max |L(j,i)| ;   j = i+1, nod
C
C  iii) Modificar pivote i
C          2                                                  1/2
C      si D(i,i) < PIVOT2   =>   D(i,i) = signo(D(i,i)) PIVOT2
C                       -nbm
C      donde  PIVOT2 = 2    MU SIGMA
C
C  iv) Normalizar columna i de L
C
C      L(j,i) = L(j,i) / D(i,i)
C
C  v) Incrementar i y volver a i)
C
C  ALMACENAMIENTO SPARSE DE MATRIZ A FACTORIZAR /3/:
C                                                         ___
C                                |\            \          \  |
C     A = AL + AD + AU  ;   AL = | \   ;  AD =  \  ;  AU = \ |
C                                |__\            \          \|
C                               RR(L)O         VECTOR     CR(U)O
C
C     AL(i,i) = AU(i,i) = 0 (no se almacenan)
C
C  ALMACENAMIENTO SPARSE DE FACTORES TRIANGULARES:
C                                                         ___
C                                |\            \          \  |
C     AILDU = L * D * U ;    L = | \   ;   D =  \  ;   U = \ |
C                                |__\            \          \|
C                               RR(L)O         VECTOR     CR(U)O
C
C     L(i,i) = U(i,i) = 1 (no se almacenan)
C
C     Se requiere ademas la descripcion RR(U)O de la estructura de U
C  para implementar en forma eficiente el algoritmo.
C
C
C  REFERENCIAS:
C  /1/ D.S. Kershaw, Journal of Computational Physics, 26 (1978) 43-65
C  /2/ D.S. Kershaw, Journal of Computational Physics, 38 (1980) 114-123
C  /3/ S. Pissanetzky, "Sparse Matrix Technology", Academic Press, 1984
C
C-----------------------------------------------------------------------
	Implicit real*8 (a-h,o-z)
      Real*8 AL(*), AD(*), AU(*), L(*),  ID(*), U(*)
      INTEGER  IL(*), JL(*), IU(*), JU(*), IP(*)
      REAL*8     MANTIS,UABS,LABS,MU,SIGMA
      Parameter (CERO=0.0d0) 
C
C     NBM: numero de bits de la mantisa de un REAL en precision simple
C
!      NBM    = 53
      NBM    = 36
!      NBM    = 24
      MANTIS = 1.0d0/2.0d0**NBM
C
C Limpiar vector auxiliar multiple-switch IP
C
      DO 10 I = 1, NOD
         IP(I) = 0
   10 CONTINUE
C
C Computo de ID(i)=1/D(i), U(i,j) y L(j,i) ;  i=1,NOD ; j=i+1,nod
C
C     Barrido sobre D, columnas de U y filas de L
      DO 100 I = 1, NOD
         MU    = CERO
         SIGMA = CERO
         BII = CERO
         ILA = IL(I)
         ILB = IL(I+1) - 1
C        Tienen ui y li componentes no nulas?
         IF (ILB.GE.ILA) THEN
C           Calculo de B(i,i) y el multiple-switch IP(i), el cual sera
C           usado en el calculo de todos los B(i,j) y C(j,i)
            DO 40 K = ILA, ILB
               M = JL(K)
               IP(M) = K
               BII = BII + L(K)*U(K)
   40       CONTINUE
         END IF
         ID(I) = AD(I) - BII
         IUA = IU(I)
         IUB = IU(I+1) - 1
C        No barrer en j si no existe al menos un j  /  (i,j) -> pat
         IF (IUB.LT.IUA) GOTO 71
C        Barrido por col. sobre fila i de U y por filas sobre col. i de L
         DO 70 KU = IUA, IUB
            J   = JU(KU)
            ILC = IL(J)
            ILD = IL(J+1) - 1
            BIJ = CERO
            CJI = CERO
            IF (ILD.GT.ILC) THEN
C              Productos escalares BIJ y CJI
               DO 60 K = ILC, ILD
                  M  = JL(K)
                  IF (M.EQ.I) GOTO 65
                  KP = IP(M)
                  IF (KP.GT.0) THEN
                     BIJ = BIJ + L(KP)*U(K)
                     CJI = CJI + L(K)*U(KP)
                  END IF
   60          CONTINUE
   65          KLU = K
            ELSE
               KLU = ILD
            END IF
            U(KLU) = AU(KLU) - BIJ
            L(KLU) = AL(KLU) - CJI
            UABS   = DABS(U(KLU))
            LABS   = DABS(L(KLU))
            IF (MU   .LT.UABS) MU    = UABS
            IF (SIGMA.LT.LABS) SIGMA = LABS
   70    CONTINUE
   71    CONTINUE
C        Correccion del pivot segun Kershaw /1/
         ISIGN  = 1
         IF (ID(I).LT.0.0d0) ISIGN = -1
         PIVOT2 = MANTIS*SIGMA*MU
         IF (ID(I)**2.LT.PIVOT2) THEN
            ID(I) = DFloat(ISIGN)/DSQRT(PIVOT2)
         ELSE
            ID(I) = 1.0d0/ID(I)
         END IF

C        Normalizacion de L(j,i)
         IF (IUB.GE.IUA) THEN
            DO 80 K = IUA, IUB
               J  = JU(K)
               KL = IL(J) - 1
C              Loop 75: busca posicion de L(i,j) en fila i de L
   75          KL = KL + 1
               M  = JL(KL)
               IF (M.NE.I) GOTO 75
C              Normaliza con el nuevo pivot
               L(KL) = L(KL)*ID(I)
   80       CONTINUE
         END IF
C        Si ui y li tenian componentes no nulas limpiar switch IP
         IF (ILB.GE.ILA) THEN
            DO 90 K = ILA, ILB
               M = JL(K)
               IP(M) = 0
   90       CONTINUE
         END IF
  100 CONTINUE
      RETURN
      END SubRoutine


C-----------------------------------------------------------------------
      SUBROUTINE PUTVALD (IV,N,IVAL,ISW,INDEX)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * P U T V A L *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C
C     ISW =< 0: COLOCA EN LAS N PRIMERAS POSICIONES DEL VECTOR IV EL
C              VALOR IVAL
C     ISW  > 0: COLOCA EN LAS N POSICIONES (INDICADAS POR INDEX) DEL
C              VECTOR IV EL VALOR IVAL
C     EJEMPLOS:
C                  CALL PUTVAL (A(LG)   ,NEL, 0.0,0,IDUMMY)
C                  CALL PUTVAL (A(LLDIR),NOD, 0,  0,IDUMMY)
C                  CALL PUTVAL (A(LLDIR),NDIR,NOD,1,A(LIDIR))
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8  IV(*), IVAL
      INTEGER INDEX(1)
C
      IF (ISW.LE.0) THEN
         DO 10 I = 1,N
            IV(I)= IVAL
   10    CONTINUE
      ELSE
         DO 20 I = 1,N
            IV(INDEX(I))= IVAL
   20    CONTINUE
      END IF
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      FUNCTION KSP (IA,JA,I,J)
C-----------------------------------------------------------------------
C                                                        ***************
C ENTREGA LA POSICION EN LA MATRIZ A DEL ELEMENTO I,J    *   K  S  P   *
C (A: MATRIZ RALA TIPO PISSANETZKY)                      ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 26/DIC/88
C-----------------------------------------------------------------------
C IA,JA   [I]    Punteros del almacenamiento ralo tipo SP de A.
C I,J     [I]    Indices del elemento buscado.
C KSP     [O]    KSP = posicion del elemento I,J si el mismo existe.
C                    = 0 si no ha sido almacenado.
C
C [I] input,  [O] output
C
C NOTA: la rutina no chequea que el I,J pedido se encuentre dentro del
C       rango de la matriz A.
C-----------------------------------------------------------------------
      DIMENSION IA(*), JA(*)
      IAA = IA(I)
      IAB = IA(I+1) - 1
      IF (IAB.GE.IAA) THEN
         DO 10 KSP = IAA, IAB
   10    IF (JA(KSP).EQ.J) RETURN
      END IF
      KSP = 0
      RETURN
      END function
c-----------------------------------------------------------------------
      logical function searstr(lu, str)

c	  Busca str en la unidad lu y devuelve .true.// Si no lo encuentra
c	devuelve un .false.
c     ------------------------------------------------------------------
      logical rew
      character *(*) str
      character *80 rec

	searstr = .true.

      rew = .false.
      nstrlen = len(str)
   10 read (lu, '(a80)', end=30) rec
      if (rec(1:1) .ne. '*') goto 10
      if (rec(2:nstrlen+1).eq.str) return 
      if (rec(2:4) .eq. 'END') then
         if (rew) then
            REWIND(LU)   ! DEJA EL ARCHIVO REBOBINADO
		  goto 20
         else
            rew =.true.
            rewind (lu)
            goto 10
         endif
      end if
      goto 10
   30 if (rew) then
		REWIND(LU)   ! DEJA EL ARCHIVO REBOBINADO
		goto 20
      else
          rew =.true.
          rewind (lu)
		goto 10
      endif
   20 searstr=.false.
      return
      end function  
C-----------------------------------------------------------------------
      FUNCTION IVAL(IV)
C     ------------------------------------------------------------------
      IVAL=IV
      RETURN
      END Function
C-----------------------------------------------------------------------
      SUBROUTINE MOVECT (IVN,IVO,N)
C     ------------------------------------------------------------------
C
      DIMENSION IVN(*),IVO(*)
C
      IF (N.GT.0) THEN
         DO 10 I = 1,N
            IVN(I) = IVO(I)
   10    CONTINUE
      ELSE
         DO 20 I = ABS(N),1,-1
            IVN(I) = IVO(I)
   20    CONTINUE
      END IF
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE MEMORI (LAST,MAX,*)
C     ------------------------------------------------------------------
C
C     Chequea la memoria calculando el espacio ocupado. Si se pasa
C     da un mensaje de error
C
C     ------------------------------------------------------------------
  100 FORMAT (//' There isn''t enough memory',/,
     *  ' ',I9,' additional bytes are required to complete this step'//
     *  ' For an expanded version contact:'/
     *  25X,'MECOM Group'/25X,'Centro At�mico Bariloche'/
     *  25X,'8400 - S.C. de Bariloche - R.N.'/25X,'Argentina')
C
      IF (LAST.LE.MAX+1) RETURN
      WRITE (6,100) (LAST-MAX-1)*4
      RETURN 1
      END  SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE PUTVAL (IV,N,IVAL,ISW,INDEX)
C     ------------------------------------------------------------------
C
C     ISW =< 0: COLOCA EN LAS N PRIMERAS POSICIONES DEL VECTOR IV EL
C              VALOR IVAL
C     ISW  > 0: COLOCA EN LAS N POSICIONES (INDICADAS POR INDEX) DEL
C              VECTOR IV EL VALOR IVAL
C     EJEMPLOS:
C                  CALL PUTVAL (A(LG)   ,NEL, 0.0,0,IDUMMY)
C                  CALL PUTVAL (A(LLDIR),NOD, 0,  0,IDUMMY)
C                  CALL PUTVAL (A(LLDIR),NDIR,NOD,1,A(LIDIR))
C
C     ------------------------------------------------------------------
      DIMENSION IV(*), INDEX(*)
C
      IF (ISW.LE.0) THEN
         DO 10 I = 1,N
            IV(I)= IVAL
   10    CONTINUE
      ELSE
         DO 20 I = 1,N
            IV(INDEX(I))= IVAL
   20    CONTINUE
      END IF
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE FINDIF (MAXMEM)
C     ------------------------------------------------------------------
C
      WRITE (6,50) MAXMEM/250     
 	WRITE (6,2050) 
C
      RETURN
C             
   50 FORMAT (' Total memory required        :', I6,' Kbytes')
 2050 FORMAT(///' Finalizacion satisfactoria'//,' Gracias por usar este 
     *programa'//'     Consultora COQUENA ')
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE TRASIM(IA,JA,IAT,JAT,M,N)
C     ------------------------------------------------------------------
C
C  THIS SUBROUTINE PERFORMS THE SYMBOLIC TRASPOSITION OF THE STRUCTURE
C  IA,JA OF A SPARSE MATRIX WITH N ROWS AND M COLUMNS.
C
C  DATA.
C  IA,JA  STRUCTURE OF THE SPARSE MATRIX,IN UNORDERED REPRESENTATION.
C        (IF IA(1) IS NOT 1, IS THE NUMBER OF ELEMENTS
C         IN EACH ROW (CONSTANT) THE REST OF IA IS NOT USED)
C  M  QUANTITY OF COLUMNS OF THE MATRIX.
C  N  QUANTITY OF ROWS OF THE MATRIX.
C  EXAMPLE=CALL TRASIM(IE,JE,IET,JET,N,NEL)
C
C  RESULTS.
C  IAT,JAT  STRUCTURE OF THE TRANSPOSE MATRIX,IN ORDERED REPRESENTATION.
C
C  SUBPROGRAMS=NONE.
C  STOP=0.
C     
      LOGICAL SW
      DIMENSION IA(*),JA(*),IAT(*),JAT(*)
C
C1.M AND N MUST BE POSITIVE.
      SW = .FALSE.
      IF (IA(1).EQ.1) GOTO 80
       NEF= IA(1)
       SW = .TRUE.
80    IF(M.LT.1.OR.N.LT.1)GOTO 360
      MH=M+1
      NH=N+1
       DO 20 I=2,MH
20     IAT(I)=0
      IAB=N*NEF
      IF (.NOT.SW) IAB=IA(NH)-1
C2.IF IAB.LT.1 THEN THE MATRIX IS EMPTY.
      IF(IAB.LT.1)GOTO 360
      DO 30 I=1,IAB
        J=JA(I)+2
        IF(J.LE.MH)IAT(J)=IAT(J)+1
30    CONTINUE
C  THE QUANTITY OF NONZEROS IN COLUMN J-2,FOR J IN THE RANGE 3 TO
C  M+1,IS STORED IN IAT(J).
      IAT(1)=1
      IAT(2)=1
      IF(M.EQ.1)GOTO 10
       DO 40 I=3,MH
40     IAT(I)=IAT(I)+IAT(I-1)
C  THE QUANTITY OF NONZEROS +1 UP TO AND INCLUDING COLUMN I-2,FOR I-2
C  IN THE RANGE 1 TO M-1,IS STORED IN IAT(I). THUS,IAT(I) POINTS TO THE
C  POSITION IN JAT WHERE THE DESCRIPTION OF COLUMN I-1 BEGINS,FOR I-1
C  IN THE RANGE 1 TO M(SINCE IAT(2)=1).
C
10    DO 60 I=1,N
        IAB=I*NEF
        IAA=IAB-NEF+1
        IF (SW) GOTO 70
        IAA=IA(I)
        IAB=IA(I+1)-1
        IF(IAB.GE.IAA) GOTO 70
        IF(IAB.EQ.IAA-1) GOTO 60
C3.IF IAB.LT.IAA-1,THEN THE GIVEN STRUCTURE IS WRONG.
        GOTO 360
70      DO 50 JP=IAA,IAB
C  A NONZERO IS FOUND IN COLUMN J-1.
          J=JA(JP)+1
C  K POINTS TO THE POSITION IN JAT WHERE COLUMN J-1 BEGINS.
          K=IAT(J)
C  THE NONZERO JUST FOUND IS RECORDED AND IAT(J) IS INCREASED.
          JAT(K)=I
50      IAT(J)=K+1
60    CONTINUE
      RETURN
C
360   WRITE(6,365)M,N,I,IAA,IAB
      STOP ' '
365   FORMAT(13H TRASIM.ERROR,20I5)
      END SubRoutine
C-----------------------------------------------------------------------  
      SUBROUTINE COLCON (IA,JA,AK,BK,IDIR,DIR, NDIR)
C     ------------------------------------------------------------------  
C
C     Coloca condiciones de Dirichlet, poniendo un 1 en la diagonal 
C     correspondiente al nodo y cero en el resto de la fila, y la
C     condicin de Dirichlet en el t�rmino independiente
C              
C     ------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)                      
      DIMENSION IA(*),JA(*),AK(*),BK(*),IDIR(*),DIR(*)
C
C                                           Condicin de Dirichlet en BK
C
      IF (NDIR.GT.0) THEN
         DO 50 K = 1, NDIR
           I = IDIR(K) 
           BK(I)  = DIR(K)                  
   50    CONTINUE
      END IF
C
C                                           Condicin de Dirichlet en AK
C      
      IF (NDIR.GT.0) THEN
         DO 80 ID = 1, NDIR
              I = IDIR(ID)
              IAA = IA(I)
              IAB = IA(I+1)-1    
              DO 90 K = IAA,IAB
                  AK(K) = 0.D0    
   90         CONTINUE
              K = KSP(IA,JA,I,I)               
              AK(K) = 1.D0
   80     CONTINUE 
      END IF
C                         
      RETURN                                                   
      END SubRoutine                                                              
C-----------------------------------------------------------------------
      SUBROUTINE NOPRECD (L,U,IUD,NL,N)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * N O P R E C *
C                                                        ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -   /   /
C-----------------------------------------------------------------------
      REAL*8 L(*), U(*), IUD(*)
C
      DO 10 K = 1, NL
         L(K) = 0.D0
         U(K) = 0.D0
   10 CONTINUE
      DO 20 I = 1, N
         IUD(I) = 1.D0
   20 CONTINUE
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE GAUSEI (IA,JA,A,B,X,X0,NOD,NSWEEP)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * G A U S E I *
C                                                        ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -   /   /
C-----------------------------------------------------------------------
      REAL*8 A(*), B(*), X(*), X0(*)
      INTEGER IA(*), JA(*)
      REAL*8 RNORM2D, ENORM2D
      COMMON /NORMAS/ NORMAS,XNOR(0:500),RNOR(0:500)
      XNOR(0) = 1.
C

      DO 10 IS = 1, NSWEEP
         CALL COPVECD (X,X0,NOD)
         DO 20 I = 1, NOD
            RIGHT = B(I)
            IAA = IA(I)
            IAB = IA(I+1) - 1
            DO 30 K = IAA, IAB
               J = JA(K)

               IF (J.NE.I) THEN
CCC                  RIGHT = RIGHT - A(K)*X(I)
                  RIGHT = RIGHT - A(K)*X(J)
               ELSE
                  KII = K
               END IF
   30       CONTINUE
CCCC            X(I) = X(I)/A(KII)
            X(I) = RIGHT/A(KII)
   20    CONTINUE

         XNOR(IS) = ENORM2D(X,X0,NOD) / RNORM2D(X,NOD)
	   WRITE (6,*) IS,XNOR(IS)
   10 CONTINUE
C
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE NUMCMR (IA,JA,A,IL,JL,AL,AD,AU,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C TRANSFORMACION NUMERICA  DE REPRESENTACION COMPLETA    * N U M C M R *
C A MIXTA                                                ***************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 1990
C-----------------------------------------------------------------------
C    Dada una matriz no-simetrica de estructura simetrica en RR(C)U
C y la estructura MR(LU)U calculada por SYMCMR, coloca los valores
C numericos en AL,AD y AU.
C
C    La representacion mixta MR(LU)U consiste en dividir la matriz A
C en una suma de 3 matrices:
C
C                           A = AL + AD + AU
C
C donde:   ALij =  Aij  si  j<i        ; triangular inferior   RR(L)U
C          ALij =  0.
C
C          ADij =  Aij  si  j=i        ; diagonal almacenada como vector
C          ADij =  0.   si  j<i o j>i
C
C          AUij =  Aij  si  j>i        ; triangular superior   CR(U)U
C          AUij =  0.   si  j<i o j=i
C
C
C-----------------------------------------------------------------------
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8   A(*), AL(*), AD(*), AU(*)
C     Barrer filas
      DO 40 I = 1, NOD
         IAA = IA(I)
         IAB = IA(I+1) - 1
C        Si la fila esta vacia saltarla
         IF (IAB.LT.IAA) GOTO 40
         DO 30 K = IAA, IAB
C           Encontrar columna
            J = JA(K)
C           Si estamos en el triangulo inferior
            IF (J.LT.I) THEN
               ILA = IL(I)
               ILB = IL(I+1) - 1
               IF (ILB.GE.ILA) THEN
                  DO 10 KL = IAA, IAB
                     IF (JL(KL).EQ.J) GOTO 15
   10             CONTINUE
               END IF
               KL = 0
   15          IF (KL.GT.0) AL(KL) = A(K)
C           Si estamos en el triangulo superior
            ELSE IF (J.GT.I) THEN
               ILA = IL(J)
               ILB = IL(J+1) - 1
               IF (ILB.GE.ILA) THEN
                  DO 20 KL = IAA, IAB
                     IF (JL(KL).EQ.I) GOTO 25
   20             CONTINUE
               END IF
               KL = 0
   25          IF (KL.GT.0) AU(KL) = A(K)
C           Si estamos en la diagonal
            ELSE
               AD(I) = A(K)
            END IF
   30    CONTINUE
   40 CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE NUMCMRMOD (IA,JA,A,IL,JL,AL,AD,AU,NOD)
C-----------------------------------------------------------------------
C                                                        ***************
C TRANSFORMACION NUMERICA  DE REPRESENTACION COMPLETA    * N U M C M R *
C A MIXTA                                                ***************
C-----------------------------------------------------------------------
C Rodrigo Mdena - IB - CAB
C-----------------------------------------------------------------------
C    Dada una matriz no-simetrica de estructura simetrica en RR(C)U
C y la estructura calculada por SYMEN2, coloca los valores numericos en 
C AL,AD y AU.
C
C    La representacion mixta consiste en dividir la matriz A en una suma
C de 3 matrices:
C
C                           A = AL + AD + AU
C
C donde:   ALij =  Aij  si  j<i        ; triangular inferior  
C          ALij =  0.
C
C          ADij =  Aij  si  j=i        ; diagonal almacenada como vector
C          ADij =  0.   si  j<i o j>i
C
C          AUij =  Aij  si  j>i        ; triangular superior  
C          AUij =  0.   si  j<i o j=i
C
C
C-----------------------------------------------------------------------
      INTEGER IA(*), JA(*), IL(*), JL(*)
      REAL*8   A(*), AL(*), AD(*), AU(*)
C     Barrer filas
      DO 40 I = 1, NOD
         IAA = IA(I)
         IAB = IA(I+1) - 1
C        Si la fila esta vacia saltarla
         IF (IAB.LT.IAA) GOTO 40
         DO 30 K = IAA, IAB
C           Encontrar columna
            J = JA(K)
C           Si estamos en el triangulo inferior
            IF (J.LT.I) THEN
               ILA = IL(I)
               ILB = IL(I+1) - 1
               IF (ILB.GE.ILA) THEN
                  DO 10 KL = IAA, IAB
                     IF (JL(KL).EQ.J) GOTO 15
   10             CONTINUE
               END IF
               KL = 0
   15          IF (KL.GT.0) AL(KL) = A(K)
C           Si estamos en el triangulo superior
            ELSE IF (J.GT.I) THEN
               ILA = IL(J)
               ILB = IL(J+1) - 1
               IF (ILB.GE.ILA) THEN
                  DO 20 KL = IAA, IAB
                     IF (JL(KL).EQ.I) GOTO 25
   20             CONTINUE
               END IF
               KL = 0
   25          IF (KL.GT.0) AU(KL) = A(K)
C           Si estamos en la diagonal
            ELSE
               AD(I) = A(K)
            END IF
   30    CONTINUE
   40 CONTINUE
      RETURN
      END SubRoutine
C----------------------------------------------------------------------
      SUBROUTINE SYMFAK(IA,JA,IU,JU,IP,N,MEDU)
C----------------------------------------------------------------------
C  FACTORIZACION TRIANGULAR SIMBOLICA UT*D*U DE GAUSS PARA UNA MATRIZ
C  SIMETRICA IA,JA DE N*N EN R(S)FD. METODO SHERMAN.
C
C  DATOS.
C  IA,JA  ESTRUCTURA DE LA MATRIZ A DADA EN R(S)FD.
C  N  ORDEN DE A.
C  MEDU  MEMORIA RESERVADA PARA JU.
C
C  RESULTADOS.
C  IU,JU  ESTRUCTURA DEL FACTOR TRIANGULAR SUPERIOR EN R(S)FD.
C
C  VECTOR AUXILIAR.
C  IP(N)  LISTAS DE NO-CEROS PRIMEROS DE FILA DE CADA COLUMNA.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C----------------------------------------------------------------------
      DIMENSION IA(*),JA(*),IU(*),JU(*),IP(*)
      NM=N-1
      NH=N+1
      DO 10 I=1,N
      IU(I)=0
10    IP(I)=0
      JP=1
      DO 20 I=1,NM
      JPI=JP
      JPP=N+JP-I
      MIN=NH
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.GE.IAA) GOTO 80
      IF(IAB.EQ.IAA-1) GOTO 30
      GOTO 360
80    JJ=JP+IAB-IAA-1
      IF(JJ.GT.MEDU)GOTO 160
      DO 40 J=IAA,IAB
      JJ=JA(J)
      JU(JP)=JJ
      JP=JP+1
      IF(JJ.LT.MIN)MIN=JJ
40    IU(JJ)=I
30    LAST=IP(I)
      IF(LAST.EQ.0)GOTO 100
      L=LAST
50    L=IP(L)
      LH=L+1
      IUA=IU(L)
      IUB=IU(LH)-1
      IF(LH.EQ.I)IUB=JPI-1
      JJ=JP+IUB-IUA-1
      IF(JJ.GT.MEDU)GOTO 160
      IU(I)=I
      DO 60 J=IUA,IUB
      JJ=JU(J)
      IF(IU(JJ).EQ.I)GOTO 60
      JU(JP)=JJ
      JP=JP+1
      IU(JJ)=I

      IF(JJ.LT.MIN)MIN=JJ
60    CONTINUE
      IF(JP.EQ.JPP)GOTO 170
      IF(L.NE.LAST)GOTO 50
 100   IF(MIN.EQ.NH)GOTO 20

 170   LAST=IP(MIN)

      IF(LAST.EQ.0)GOTO 70

      IP(I)=IP(LAST)

      IP(LAST)=I
      GOTO 20
 70    IP(MIN)=I
      IP(I)=I
 20    IU(I)=JPI
      IU(N)=JP
      IU(NH)=JP
      JP=JP-1
C      WRITE(6,15)N,N,JP
      RETURN
160   WRITE(6,165)I,JJ,IAA,IAB
      STOP
360   WRITE(6,365)N,MEDU,JP,I,IAA,IAB
      STOP
15    FORMAT(1X,'SYMFAK.SE HA GENERADO LA ESTRUCTURA DEL FACTOR U DE UNA
     * MATRIZ DE',I5,'*',I5,'.HAY',I6,' NO-CEROS.')
165   FORMAT(1X,' SYMFAK.AL PROCESAR LA FILA',I5,' YA SE REQUIEREN',I6,
     1' LUGARES PARA JU.',2X,'IAA='I5,2X,'IAB='I5)
365   FORMAT(1X,'SYMFAK.ERROR'20I5)
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE GAUSPRNS (NOD,IU,JU,UNS,UNI,DI,BK)
C-----------------------------------------------------------------------
C
C     REDUCE EL VECTOR DE CARGA Y EFECTUA LA RETROSUSTITUCION
C     (MATRIZ ALMACENADA EN FORMATO RALO, FACTORIZADA POR NUMFAKNS)
C
C     DATOS DE ENTRADA:
C     NOD     NUMERO DE INCOGNITAS
C     IU, JU  ESTRUCTURA DE LA MATRIZ TRIANGULARIZADA
C     UNS,UNI ELEMENTOS EXTRADIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     DI      ELEMENTOS DIAGONALES DE LA MATRIZ TRIANGULARIZADA
C     BK      VECTOR TERMINO INDEPENDIENTE
C
C     RESULTADOS:
C     BK  SOLUCION
C
C     Programador : DARI Enzo Alberto
C
C     Fecha       : Octubre 1991
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IU(*), JU(*), UNS(*), UNI(*), DI(*), BK(*)
C
      NM = NOD - 1
C
C     Reduccion del vector de carga
C
      DO 20 K = 1, NM
         IUA = IU (K)
         IUB = IU (K + 1) - 1
         ZZ = BK (K)
         IF (IUB .LT. IUA) GOTO 10
         DO 30 I = IUA, IUB
            BK (JU (I)) = BK (JU (I)) - UNI (I) * ZZ
   30    CONTINUE
   10    BK (K) = ZZ * DI (K)
   20 CONTINUE
      BK (NOD) = BK (NOD) * DI (NOD)
C
C     Retrosustitucion
C
      K = NM
   40 IUA = IU (K)
      IUB = IU (K + 1) - 1
      IF (IUB .LT. IUA) GOTO 60
      ZZ = BK (K)
      DO 50 I = IUA, IUB
         ZZ = ZZ - UNS (I) * BK (JU (I))
   50 CONTINUE
      BK (K) = ZZ
   60 K = K - 1
      IF (K .GT. 0) GOTO 40
      RETURN
      END SubRoutine
C     ------------------------------------------------------------------
      SUBROUTINE NUMFAKNS 
     *          (IA,JA,ANS,ANI,AD,IU,JU,UNS,UNI,DI,DICOL,IP,IUP,N)
C     ------------------------------------------------------------------
C  REALIZA NUMERICAMENTE LA FACTORIZACION TRIANGULAR L*D*U DE UNA MATRIZ
C  NO SIMETRICA A DE N*N. METODO SHERMAN.
C
C  DATOS.
C  IA,JA,ANS,ANI,AD  MATRIZ A.
C  IU,JU  ESTRUCTURA PARA U.
C  N  ORDEN DE A Y DE U.
C
C  RESULTADOS.
C  UNS,UNI,DI  FACTOR SUPERIOR E INFERIOR DE U. 
C              EN DI QUEDAN LAS INVERSAS DE LOS ELEMENTOS DIAGONALES DE U.
C
C  VECTORES AUXILIARES.
C  IP(N)  LISTAS DE NO-CEROS PRIMEROS DE PEDACITO DE FILA DE CADA COLUMNA
C  IUP(N)  PUNTEROS PARA LOS PEDACITOS DE FILA.
C  DICOL(N) COLUMNA EXPANDIDA
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION IA(*),JA(*),ANS(*),ANI(*),AD(*),
     * IU(*),JU(*),UNS(*),UNI(*),DI(*),DICOL(*),IP(*),IUP(*)
C
      PIVMIN = 1.0E+30
      PIVMAX = 0.0
      NEGDIA = 0
C
      DO 10 J=1,N
   10    IP(J)=0
      DO 150 I=1,N
	 IH=I+1
	 IUA=IU(I)
	 IUB=IU(IH)-1
	 IF(IUB.LT.IUA) GOTO 40 ! No hay elementos extradiagonales en U
	 DO 20 J=IUA,IUB        ! Pone en 0 los vectores expandidos de
	    DI(JU(J))=0.        ! fila y columna del pivot
   20       DICOL(JU(J))=0.
	 IAA=IA(I)
	 IAB=IA(IH)-1
	 IF(IAB.LT.IAA) GOTO 40 ! No hay elementos extradiagonales en A
	 DO 30 J=IAA,IAB
	    DI   (JA(J))=ANS(J)    ! DI(I:N) : Fila siendo procesada
   30       DICOL(JA(J))=ANI(J)    ! Su parte `simetrica'
   40    DI(I)=AD(I)
	 IF(AD(I).LT.1.0D-38) THEN
C            WRITE (6,*)'NUMFAKNS.WARNING ',I,AD(I)
	     NEGDIA = NEGDIA + 1
	 END IF
	 LAST=IP(I)             ! Lista circular de filas c/no-ceros en col.I
	 IF(LAST.EQ.0) GOTO 120
	 LN=IP(LAST)
   50    L=LN                   ! L: `Nodo actual' de la lista circular (fila)
	 LN=IP(L)               ! LN: `Proximo Nodo'
	 IUC=IUP(L)             ! IUC,IUD: Lista en UNS de no ceros de fila L /
	 IUD=IU(L+1)-1          !         con indice de columna >= I
	 UM   =UNI(IUC)*DI(L)      ! Factor a multiplicar * L y restar en I
	 UMCOL=UNS(IUC)*DI(L)      ! Factor p/adelantarse a eliminacion de(JJ,L)
	 IF(ABS(UM).LT.1.0D-38) GOTO 70 ! Saltea los ceros numericos
	 DO 60 J=IUC,IUD
	    JJ=JU(J)
   60       DI   (JJ)=DI   (JJ)-UNS(J)*UM
   70    IF(ABS(UMCOL).LT.1.0D-38) GOTO 90 ! Saltea los ceros numericos
	 DO 80 J=IUC+1,IUD
	    JJ=JU(J)
   80       DICOL(JJ)=DICOL(JJ)-UNI(J)*UMCOL
   90    UNI(IUC)=UM            ! Guarda elemento de matriz `Lower'
	 UNS(IUC)=UMCOL         ! Guarda elemento de matriz `Upper'
	 IUP(L)=IUC+1
	 IF(IUC.EQ.IUD)GOTO 110
	 J=JU(IUC+1)
	 JJ=IP(J)
	 IF(JJ.EQ.0)GOTO 100
	 IP(L)=IP(JJ)
	 IP(JJ)=L
	 GOTO 110
  100    IP(J)=L
	 IP(L)=L
  110    IF(L.NE.LAST)GOTO 50
  120    UM=DI(I)
C
	 IF (ABS(UM).LT.PIVMIN) THEN
	    PIVMIN = ABS(UM)
	 ELSE IF (ABS(UM).GT.PIVMAX) THEN
	    PIVMAX = ABS(UM)
	 ENDIF
C
C         IF(UM.LT.0)WRITE (6,*)'PIVOT NEGATIVO:',I,UM
	 IF(ABS(UM).LT.1.0D-38)GOTO 360
	 DI(I)=1./UM                  ! Guarda elemento de matriz `Diagonal'
	 IF(IUB.LT.IUA)GOTO 150
	 DO 130 J=IUA,IUB            ! Guarda filas y columnas que habian sido
	    UNS(J)=DI(JU(J))         ! expandidas nuevamente en su lugar   
  130       UNI(J)=DICOL(JU(J))
	 J=JU(IUA)
	 JJ=IP(J)
	 IF(JJ.EQ.0)GOTO 140
	 IP(I)=IP(JJ)
	 IP(JJ)=I
	 GOTO 150
  140    IP(J)=I
	 IP(I)=I
  150    IUP(I)=IUA
c      WRITE (6,1000) PIVMIN, PIVMAX
      IF (NEGDIA.GT.0) WRITE (6,*) ' NUMFAKNS.WARNING: ',NEGDIA,
     *                 ' NEG. DIAG.'
      RETURN
C
  360 WRITE(6,365)I,UM,AD(I)
      STOP
  365 FORMAT(1X,'NUMFAKNS.ERROR',I5,2E16.8)
 1000 FORMAT(1X,'NUMFAKNS.PIVOTS: MINIMO:',E16.8,' MAXIMO:',E16.8)
      END SubRoutine
C---------------------------------------------------------------------+
C                                                                     |
C                       +-------------------+                         |
C                       |                   |                         |
C                       |   G  M  R  E  S   |                         |
C                       |                   |                         |
C                       +-------------------+                         |
C                                                                     |
C                   Generalized Minimal RESidual:                     |
C                                                                     |
C                     Non linear systems solver                       |
C                        ( Double precision )                         |
C                                                                     |
C  Pablo Carrica y Axel Larreteguy, 1989.                             |
C  Input data:                                                        |
C  IA, JA, AN : Matriz a resolver en almacenamiento Gustaffson        |
C  IL, JL     : Estructura de la matriz de precondicionamiento        |
C               (triangular inferior sin diagonal)                    |
C  L, U, IUD  : Matrices triangular inferior, superior y diagonal de U|
C  X          : Solucion semilla                                      |
C  B          : Vector termino independiente.                         |
C  UV         : Vector auxiliar de dimension NOD*(K+1)                    |
C  W          : Vector auxiliar de dimension NOD                      |
C  EPSTOL     : Residuo maximo aceptable                              |
C  LMAX       : Numero maximo de iteraciones GMRES                    |
C  ISTAT      : Indicador de numero de iteraciones o de error         |
C               (ISTAT = -1)                                          |
C  NOD        : Orden de la matriz a resolver                         |
C  K          : Numero de vectores del espacio de Krylov              |
C  BETA       : Vector auxiliar                                       |
C  H          : Vector auxiliar                                       |
C  E          : Vector auxiliar                                       |
C  C          : Vector auxiliar                                       |
C  S          : Vector auxiliar                                       |
C  Y          : Vector auxiliar                                       |
C---------------------------------------------------------------------+
      SUBROUTINE GMRESD1 (IA,JA,AN,IL,JL,L,IUD,U,X,B,UV,W,EPSTOL,LMAX,
     -                   ISTAT,NOD,K,BETA,H,E,C,S,Y)
C----------------------------------------------------------------------
	implicit real*8 (a-h,o-z)
      REAL*8 U(*), X(*), B(*), UV(*), W(*), H(*), E(*), C(*), S(*)
      REAL*8 IUD(*), L(*), Y(*), BETA(*)
      REAL*8 R, AUX1, AUX2, RNORM2D, SCPRODD
      REAL*8 AN(*)		                                               !ORIGINAL EN GMRESD: *4	
      REAL*8 EPSTOL, EPS	                                               !ORIGINAL EN GMRESD: *4
      INTEGER IA(*), JA(*), IL(*), JL(*)
C----------------------------------------------------------------------
C      -1
C Set L  B to B
C     ------------------------------------
      CALL FWSROWD (IL,JL,L,DUMMY,0,B,NOD)
C     ------------------------------------
C Accepted error scaling with |B|
      BNORM = RNORM2D (B,NOD)
      EPS   = EPSTOL * BNORM
      Write(6,*)BNorm,eps

C                          +--------------+
C                          | GMRES Cycles |
C                          +--------------+
      DO 10 LC=1,LMAX
	   Write(6,33) LC
C First Krylov space vector definition
C        ------------------------------------------
         CALL COPVECD (X,W,NOD)
         CALL BWSCOLD (IL,JL,U,IUD,1,W,NOD)
         CALL SPMUL2D (IA,JA,AN,DUMMY,W,UV,NOD,2)
         CALL FWSROWD (IL,JL,L,DUMMY,0,UV,NOD)
         CALL AXPLBYD (1.D0,B,-1.D0,UV,DUMMY,NOD,2)
C        ------------------------------------------
         E(1) = RNORM2D (UV,NOD)
         AUX1 = 1.D0/E(1)
C        --------------------------
         CALL SCTIMXD (AUX1,UV,NOD)
C        --------------------------
C                        +------------------+
C                        | GMRES Iterations |
C                        +------------------+
         DO 20 I=1,K
C  K aditional Krylov space vector definitions
C           ------------------------------------------------
            CALL COPVECD (UV(1+(I-1)*NOD),W,NOD)
            CALL BWSCOLD (IL,JL,U,IUD,1,W,NOD)
            CALL SPMUL2D (IA,JA,AN,DUMMY,W,UV(1+I*NOD),NOD,2)
            CALL FWSROWD (IL,JL,L,DUMMY,0,UV(1+I*NOD),NOD)
C           -------------------------------------------------
C Modified Gram-Schmidt orthogonalization
            DO 30 J=1,I
               BETA(J) = SCPRODD(UV(I*NOD+1),UV((J-1)*NOD+1),NOD)
C              ------------------------------------------
               CALL AXPLBYD (1.D0,UV(I*NOD+1),-BETA(J),
     -                       UV((J-1)*NOD+1),DUMMY,NOD,1)
C              ------------------------------------------
               H(((I*(I+1)) /2) -1+J) = BETA(J)
 30         CONTINUE
C Vector normalization
            UVNORM = RNORM2D (UV(I*NOD+1),NOD)
C           --------------------------------------------
            CALL SCTIMXD ((1.D0/UVNORM),UV(I*NOD+1),NOD)
C           --------------------------------------------
C Last term of I column in H matrix
            H((I*(I+3))/2 ) = UVNORM
C Q-R Algoritm
            IF (I.EQ.1) THEN
               R   = Dsqrt(BETA(1)**2 + UVNORM**2)
               C(1)= BETA(1)/R
               S(1)= UVNORM/R
               H(1)= R
               H(2)= 0.D0
               E(2)=-S(1)*E(1)
               E(1)= C(1)*E(1)
               GOTO 20
            END IF
            DO 40 J=1,I-1
               AUX1 = H(((I*(I+1))/2) -1+J)
               AUX2 = H(((I*(I+1))/ 2)+J)
               H(((I*(I+1))/2)-1+J)= C(J)*AUX1+S(J)*AUX2
               H(((I*(I+1))/ 2)+J)  =-S(J)*AUX1+C(J)*AUX2
 40         CONTINUE
            AUX1 = H(((I*(I+3))/2) -1)
            AUX2 = H((I*(I+3))/2 )
            R    = Dsqrt(AUX1**2 + AUX2**2)
            C(I) = AUX1/R
            S(I) = AUX2/R
            H(((I*(I+3))/2) -1)= R
            H(  (I*(I+3))/ 2 )  = 0.D0
            E(I+1) =-S(I)*E(I)
            E(I)   = C(I)*E(I)
            ERR    = DABS(E(I+1))
c            Write(6,*)err/BNORM      !original: err
            IF (ERR.LE.EPS) GOTO 50	 
 20      CONTINUE
	   I=K
! I= I-1 AGREGADO POR RODRIGO MODENA (ABRIL-1998)

C H arrangement conversion
C        ------------------
 50      CALL CONVERD (H,K)
C        ------------------
C Y resolution
C        ---------------------
         CALL RETROD (H,I,Y,E)
C        ---------------------
C Update solution
C        ---------------------------
         CALL UPDATED (X,NOD,UV,Y,I)
C        ---------------------------
      Write(6,*)'-> <GMRES> Error/BNorm =',err/BNORM  
C Final convergence check
         IF (ERR.LE.EPS) GOTO 60
 10   CONTINUE
C Original solution
C     ----------------------------------
 60   CALL BWSCOLD (IL,JL,U,IUD,1,X,NOD)
C     ----------------------------------
      ISTAT = LC
      IF (LC.GE.LMAX) ISTAT =-1
      RETURN
   33	FORMAT (' GMRES iteration:',I5)
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE CONVERD (H,K)
C
C     Vector-cuasi-triangular matrix conversion
C-----------------------------------------------------------------------
      REAL*8 H(*)
      DO 10 I=1,K
         DO 20 J=1,I
            H(((I*(I-1))/ 2)+J)=H(((I*(I+1))/ 2)-1+J)
 20      CONTINUE
 10   CONTINUE
      RETURN
      END SubRoutine
C-----------------------------------------------------------------------
      SUBROUTINE RETROD (H,I,Y,E)
C
C     Fullfilled matrix retrosustitution
C-----------------------------------------------------------------------
      REAL*8 H(*), Y(*), E(*)
      REAL*8 AUX1, AUX2, HI
      Y(I) = E(I)/H(I*(I+1)/ 2 )
      DO 10 J=1,I-1
         L=I-J
         HI   = 1.0d0/H(L*(L+1)/ 2 )
         AUX1 = E(L)*HI
         AUX2 = 0.D0
         DO 20 K=1,J
            M    = I-K+1
            AUX2 = AUX2 + Y(M)*H(M*(M-1)/ 2 +L)*HI
 20      CONTINUE
         Y(L) = AUX1 - AUX2
 10   CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE UPDATED (X,N,U,Y,I)
C
C     Update of the GMRES solution
C-----------------------------------------------------------------------
      REAL*8 X(*), U(*), Y(*)
      REAL*8 AUX
      DO 10 K=1,N
        AUX = 0.D0
         DO 20 J=1,I
            L = N*(J-1) + K
            AUX = AUX + Y(J)*U(L)
 20      CONTINUE
         X(K) = X(K) + AUX
 10   CONTINUE
      RETURN
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SPMUL3D (IA,JA,AN,AD,U,V,N,KOD)
C-----------------------------------------------------------------------
C                                                        ***************
C                                                        * S P M U L 3 *
C                                                        ***************
C-----------------------------------------------------------------------
C           *** VERSION PARCIALMENTE EN DOBLE PRECISION ***
C-----------------------------------------------------------------------
C  MULTIPLICA UNA MATRIZ RALA POR UN VECTOR LLENO.
C
C  DATOS
C  IA,JA,AN  MATRIZ RALA DADA EN R(S)FD O EN R(C)FD.
C  AD  DIAGONAL DE LA MATRIZ DADA,SOLO SI KOD=1.
C  U  VECTOR LLENO DADO.
C  N  CANTIDAD DE FILAS DE LA MATRIZ.
C  KOD=1  PARA MATRIZ SIMETRICA.
C     =2  PARA MATRIZ GENERAL.
C     =3  PRODUCTO DE VECTOR ACOSTADO POR MATRIZ GENERAL. EN ESTE CASO
C         EL RESULTADO ES ACUMULADO EN V Y PUEDE SER NECESARIO INICIA-
C         LIZAR V CON CEROS.
C     =4  PARA MATRIZ TRIANGULAR INFERIOR ALMACENADA RR(L)O *
C * AXEL LARRETEGUY 5/NOV/89
C
C  RESULTADOS.
C  V  VECTOR LLENO V=A*U  O  V=UT*A.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C------------------------------------------------------------------------
      IMPLICIT REAL*8 (B-H,O-Z)
      REAL*8  U(*),V(*)
      REAL    AN(*),AD(*)
      INTEGER IA(*),JA(*)
      IF(N.LT.1)GOTO 360
      IF(KOD.EQ.2)GOTO 2
      IF(KOD.EQ.3)GOTO 3
      IF(KOD.EQ.4)GOTO 4
      IF(KOD.NE.1)GOTO 360
      DO 10 I=1,N
10    V(I)=AD(I)*U(I)
      DO 20 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.LT.IAA)GOTO 20
      DO 30 JP=IAA,IAB
      J=JA(JP)
      Z=AN(JP)
      V(I)=V(I)+Z*U(J)
30    V(J)=V(J)+Z*U(I)
20    CONTINUE
      RETURN
C
2     DO 40 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
      IF(IAB.LT.IAA)GOTO 42
      DO 50 JP=IAA,IAB
50    W=W+AN(JP)*U(JA(JP))
42    V(I)=W
40    CONTINUE
      RETURN
C
3     DO 60 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.LT.IAA)GOTO 60
      Z=U(I)
      DO 70 JP=IAA,IAB
      J=JA(JP)
      V(J)=V(J)+AN(JP)*Z
70    CONTINUE
60    CONTINUE
      RETURN
C
4     DO 90 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
      IF(IAB.LT.IAA)GOTO 82
      DO 80 JP=IAA,IAB
80    W=W+AN(JP)*U(JA(JP))
82    W=W+AD(I)*U(I)
      V(I)=W
90    CONTINUE
      RETURN
C
360   WRITE(6,365)N,KOD
      STOP
365   FORMAT(13H SPMULT.ERROR,18I6)
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE SYMEN2 (IE,JE,IET,JET,N,IA,JA,JP)
C-----------------------------------------------------------------------
C     ENSAMBLAJE SIMBOLICO PARA UNA MATRIZ
C     SIMETRICA IA,JA DE N*N EN RR(U)U
C
C     DATOS DE ENTRADA:
C     IE,JE     MATRIZ DE CONECTIVIDAD
C     IET, JET  TRASPUESTA DE IE,JE
C     N         NUMERO DE NODOS EN LA RED
C     IA        VECTOR DE DIMENSION N CONTENIENDO EL VALOR N EN LAS
C              POSICIONES CORRESPONDIENTES A NODOS DE DIRICHLET Y 0 EN
C              CUALQUIER OTRO LADO
C
C     RESULTADOS:
C     IA, JA  ESTRUCTURA DE LA MATRIZ DE RIGIDEZ
C     JP      NUMERO DE ELEMENTOS EXTRADIAGONALES ALMACENADOS
C
C     Programador : DARI Enzo Alberto (rutina original SYMENS)
C     Fecha       : Junio 1988
C     Modificacion para matrices no-triangulares: LARRETEGUY Axel E.
C     Fecha       : Noviembre 1989
C
C-----------------------------------------------------------------------
C
      DIMENSION IE(*), JE(*), IET(*), JET(*), IA(*), JA(*)
      JP = 1
      NM = N - 1
      DO 40 I = 1, NM
        JPI = JP
        IF (IA (I) .EQ. N) GOTO 30
        IETA = IET (I)
        IETB = IET (I + 1) - 1
        DO 20 IP = IETA, IETB
          J = JET (IP)
          IEA = IE(J)
          IEB = IE(J+1) - 1
          DO 10 KP = IEA, IEB
            K = JE (KP)
            IF (K .LE. I) GOTO 10
              IF (IA (K) .GE. I) GOTO 10
                JA (JP) = K
                JP = JP + 1
                IA (K) = I
   10     CONTINUE
   20   CONTINUE
   30   IA (I) = JPI
   40 CONTINUE
      IA (N) = JP
      IA (N + 1) = JP
      JP = JP - 1
      RETURN 
      END	SubRoutine	   
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      SUBROUTINE TRANS_ULtoC(IA,JA,AL,AU,AD,IAC,JAC,AC,Rows)
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRAnsforma UNA MATRIZ ESPARCIDA IA,JA,AL,AU,AD DE Rows FILAS.
C  En otra de Representacion Completa IAC,JAC,AC
C  DATOS
C  IA,JA,AL,AU,AD  MATRIZ ESPARCIDA DADA.
C  Rows  CANTIDAD DE FILAS DE LA MATRIZ.
C
C  RESULTADOS.
C  IAC,JAC,AC  MATRIZ RR(C)O (Row Represented Complete Ordered
C  El Triangulo inferior de AC queda siempre ordenado,
C  si JA esta ordenado entonces toda AC estara ordenada.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      implicit real*8 (A-H,O-Z)
      DIMENSION IA(*),JA(*),AL(*),AU(*),AD(*),IAC(*),JAC(*),AC(*)
      Integer Rows, ColU, RowsP1
      IF(Rows.LT.1)GOTO 360
      RowsP1 = Rows + 1
      LenghtJAC= (IA(RowsP1)-1)*2+Rows

      DO 20 I = 1,Rows
 20    IAC(I)  = 1 + IA(I+1)- IA(I)
      DO 21 I = 1,LenghtJAC
 21    JAC(I)  = 0

      DO 60 I=1,Rows
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.LT.IAA)GOTO 55
        DO 50 JP=IAA,IAB
	    ColU = JA(JP)
	    IAC(ColU)= IAC(ColU)+ 1
50      CONTINUE
55    CONTINUE
60    CONTINUE

	LPrior= IAC(1)
	IAC(1)= 1
      DO 70 I = 2,RowsP1
          LPost  = IAC(I)
          IAC(I) = IAC(I-1)+ LPrior
          LPrior = LPost
70    Continue

      LoopRows: DO I=1,Rows

          IAA=IA(I)
          NAB=IA(I+1)-IAA
C      Guardo la Diagoral
          ipJAC1D = IAC(I)
          ipJAC2D = IAC(I+1)
	    KDD     = ipJAC2D - NAB - 1
          JAC(KDD)= I
          AC (KDD)= AD(I)


       IF(NAB.GT.0) Then

        DO 90 JP=0,NAB-1
          ipColU = IAA + JP
          ColU   = JA ( ipColU )
          KC     = KDD + JP + 1 
          JAC(KC)= ColU
          AC (KC)= AU(ipColU)

          ipJAC1 = IAC( ColU )
          ipJAC2 = IAC( ColU + 1 )-1
c          ipJAC2 = IAC( ColU + 1 )
c          NAB2   = IA(ColU + 1) - IA(ColU)
c          KD = ipJAC2 - NAB2 - 1
c             DO iCol=ipJAC1 , KD-1 
             DO iCol=ipJAC1 , ipJAC2 
               IF (JAC(iCol) .EQ. 0 ) Exit
             Enddo
          JAC(iCol)= I
          AC (iCol)= AL(ipColU)

90      CONTINUE

	 Endif


      Enddo LoopRows

      RETURN
C
360   WRITE(6,365)Rows
      STOP
365   FORMAT('TRANUM.ERROR, Rows=',I6)
      END subroutine
C-----------------------------------------------------------------------
      SUBROUTINE Sigchange(AD,IAC,AC,BL,Rows)
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRAnsforma Pone todas las filas con diagonal positiva
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      implicit real*8 (A-H,O-Z)
!      Parameter (Tol=1.d4)
      DIMENSION AD(*),IAC(*),AC(*),BL(*)
      Integer Rows

      DO I=1,Rows
	    IF ( AD(I) .LT. 0.0d0 ) then
              BL(I) = -BL(I)
              IAA   = IAC(I)
              IAB   = IAC(I+1)-1
              DO JP = IAA, IAB
	         AC(JP) = -AC(JP)
              Enddo
          END IF
      Enddo

      RETURN
C
      END subroutine

C-----------------------------------------------------------------------
      SUBROUTINE SigchangeLU(IA,JA,AD,AL,AU,BL,Rows,Sym)
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRAnsforma Pone todas las filas con diagonal positiva
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      implicit real*8 (A-H,O-Z)
!      Parameter (Tol=1.d4)
      DIMENSION AD(*),IA(*),JA(*),AL(*),AU(*),BL(*)
      Integer Rows
      Logical Sym

      DO I=1,Rows
	    IF ( AD(I) < 0.0d0 ) then
              BL(I) = -BL(I)
              IAA   = IA(I)
              IAB   = IA(I+1)-1
              DO JP = IAA, IAB
	         AU(JP) = -AU(JP)
              Enddo
          END IF
          
	    IF ( .Not. Sym ) then
              IAA   = IA(I)
              IAB   = IA(I+1)-1
              DO JP = IAA, IAB
	         IF( AD( JA(JP) ) < 0.d0 ) AL(JP) = -AL(JP)
              Enddo
          END IF
      Enddo

      DO I=1,Rows
	    IF ( AD(I) < 0.0d0 ) then
              AD(I) = -AD(I)
          END IF
      Enddo


      RETURN
C
      END subroutine





C-----------------------------------------------------------------------
      SUBROUTINE LMULTVD (IL,JL,L,ILD,KLD,X,Y,N)
C-----------------------------------------------------------------------
C                                                        ***************
C LOWER TRIANGULAR MATRIZ MULTIPLIED BY VECTOR           * L M U L T V *
C                                                        ***************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA -  7/NOV/89
C-----------------------------------------------------------------------
C PROPOSITO:
C      Multiplica la matriz triangular inferior L por el vector lleno x
C
C                          y = L x
C
C donde  L  esta almacenada en formato ralo  RR(L)U  tipo Gustavson /1/.
C Si los Lii son no unitarios, sus inversas deben proveerse en el vector
C ILD,  colocando ademas el switch KLD = 1.   Si KLD = 0  se  supone que
C Lii=1 para todo i, y no se requiere el vector ILD.
C
C ARGUMENTOS:
C IL,JL    [I]      estructura de la matriz L en RR(L)U
C L        [I]      valores extradiagonales de L
C ILD      [I]      inversas de los valores diagonales de L (si KLD=1)
C KLD      [I]      indicador de diagonal unitaria
C X        [I]      vector a multiplicar
C Y        [O,X]    vector solucion
C N        [I]      dimension de X e Y y orden de la matriz
C
C REFERENCIAS:
C /1/ "Sparse Matrix Technology", S. Pissanetzky, Academic Press, 1984
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IL(*), JL(*)
      REAL*8  L(*), ILD(*), X(*), Y(*)
C
      DO 20 I = 1, N
         Y(I) = 0.D0
         ILA  = IL(I)
         ILB  = IL(I+1) - 1
         IF (ILB.GE.ILA) THEN
            DO 10 K = ILA, ILB
               J = JL(K)
               Y(I) = Y(I) + L(K)*X(J)
   10       CONTINUE
         END IF
   20 CONTINUE
C
      IF (KLD.EQ.1) THEN
         DO 30 I = 1, N
            Y(I) = Y(I) + X(I)/ILD(I)
   30    CONTINUE
      ELSE
         DO 40 I = 1, N
            Y(I) = Y(I) + X(I)
   40    CONTINUE
      END IF
C
      RETURN
      END Subroutine


C-----------------------------------------------------------------------
      SUBROUTINE SPMULDiagDLU (IA,JA,AN,ANT,AD,DU,N,KOD)
C-----------------------------------------------------------------------
C                               ******************
C                               * S P M U L Diag *
C                               ******************
C-----------------------------------------------------------------------
C                  *** VERSION DOBLE PRECISION ***
C-----------------------------------------------------------------------
C  MULTIPLICA UNA MATRIZ RALA POR UNA Matriz Diagonal
C
C  DATOS
C  IA,JA,AN  MATRIZ RALA DADA EN R(S)FD O EN R(C)FD.
C  AD  DIAGONAL DE LA MATRIZ DADA.
C  DU  Diagonal Matriz U
C  N  CANTIDAD DE FILAS DE LA MATRIZ.
C  KOD=6  PARA MATRIZ L(Row compresed) U(Column compressed)
C !!!!!  Los KOD siguientes no estan implementados !!!!!!!!!!!
C  KOD=1  PARA MATRIZ SIMETRICA.
C     =2  PARA MATRIZ GENERAL.
C     =3  PRODUCTO DE VECTOR ACOSTADO POR MATRIZ GENERAL. EN ESTE CASO
C         EL RESULTADO ES ACUMULADO EN V Y PUEDE SER NECESARIO INICIA-
C         LIZAR V CON CEROS.
C     =4  PARA MATRIZ TRIANGULAR INFERIOR ALMACENADA RR(L)O *
C 
C
C  RESULTADOS.
C  A  A=A*DU  O  A=DU*A.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8  AN(*),ANT(*),AD(*),DU(*),V(1)
      INTEGER IA(*),JA(*)
      IF(N.LT.1)GOTO 360
      IF(KOD.EQ.2)GOTO 2
      IF(KOD.EQ.3)GOTO 3
      IF(KOD.EQ.4)GOTO 4
      IF(KOD.EQ.5)GOTO 5
      IF(KOD.EQ.6)GOTO 6
      IF(KOD.NE.1)GOTO 360
      DO 10 I=1,N
10    V(I)=AD(I)*DU(I)
      DO 20 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.LT.IAA)GOTO 20
      DO 30 JP=IAA,IAB
      J=JA(JP)
      Z=AN(JP)
      V(I)=V(I)+Z*DU(J)
30    V(J)=V(J)+Z*DU(I)
20    CONTINUE
      RETURN
C
2     DO 40 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
      IF(IAB.LT.IAA)GOTO 42
      DO 50 JP=IAA,IAB
50    W=W+AN(JP)*DU(JA(JP))
42    V(I)=W
40    CONTINUE
      RETURN
C
3     DO 60 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.LT.IAA)GOTO 60
      Z=DU(I)
      DO 70 JP=IAA,IAB
      J=JA(JP)
      V(J)=V(J)+AN(JP)*Z
70    CONTINUE
60    CONTINUE
      RETURN
C
4     DO 90 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      W=0D0
      IF(IAB.LT.IAA)GOTO 82
      DO 80 JP=IAA,IAB
80    W=W+AN(JP)*DU(JA(JP))
82    W=W+AD(I)*DU(I)
      V(I)=W
90    CONTINUE
      RETURN

 5    DO I=1,N
       AD(I)=AD(I)*DU(I)
      enddo
      DO I=1,N
          IAA=IA(I)
          IAB=IA(I+1)-1
          IF(IAB.LT.IAA)Cycle
          DO JP=IAA,IAB
              J=JA(JP)
              AN (JP) =AN (JP) * DU(J)
              ANT(JP) =ANT(JP) * DU(I)
          enddo
      Enddo
      RETURN


 6    DO 99 I=1,N
99    AD(I)=AD(I)*DU(I)
      DO 100 I=1,N
          IAA=IA(I)
          IAB=IA(I+1)-1
          IF(IAB.LT.IAA)GOTO 100
          DO 101 JP=IAA,IAB
              J=JA(JP)
              AN (JP) =AN (JP) * DU(I)
101           ANT(JP) =ANT(JP) * DU(J)
100   CONTINUE
      RETURN


C
360   WRITE(6,365)N,KOD
      STOP
365   FORMAT(13H SPMULT.ERROR,18I6)
      END SubRoutine

C-----------------------------------------------------------------------
      SUBROUTINE DIAGSCD (IA,JA,AN,W,N)
C-----------------------------------------------------------------------
C                      VERSION DOBLE PRECISION
C-----------------------------------------------------------------------
C                                                      *****************
C DIAGONAL SCALING                                     * D I A G S C D *
C                                                      *****************
C-----------------------------------------------------------------------
C Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 15/DIC/89  
C-----------------------------------------------------------------------
C 18/6/91: version modificada para no terminar la ejecucion al 
C          encontrar diagonales negativas
C-----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*)
      REAL*8  AN(*), W(*)
C
C     Busca diagonal de la matriz A y calcula Wi = 1/sqrt(Aii)
C
      KDIAG = 0
C
      DO 30 I = 1, N
         IAA = IA(I)
         IAB = IA(I+1) - 1
         IF (IAB.LT.IAA) GO TO 20
         DO 10 K = IAA, IAB
            J = JA(K)
            IF (I.EQ.J) GO TO 25
   10    CONTINUE
C
   20    WRITE (6,*) 'DIAGSCD/ERROR: NO EXISTE DIAGONAL EN FILA',I
         STOP
C
   25    IF (AN(K).GT.0.D0) THEN
            W(I) = 1/DSQRT(AN(K))
         ELSE IF (AN(K).LT.0.D0) THEN
            KDIAG = KDIAG + 1
            W(I) = 1/DSQRT(-AN(K))
         ELSE
            KDIAG = KDIAG + 1
            W(I) = 1.D0
         END IF
   30 CONTINUE    
      IF (KDIAG.GT.0) WRITE (6,*) 'DIAGSCD/WARNING: ',
     -   'SE ENCONTRARON ',KDIAG,' DIAGONALES NO POSITIVAS.'
C
C Scaling de la matriz 
C
      DO 50 I = 1, N
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO 40 K = IAA, IAB
            J = JA(K)
            AN(K) = AN(K)*W(I)*W(J)
   40    CONTINUE
   50 CONTINUE
C
      RETURN
      END         


C-----------------------------------------------------------------------
      SUBROUTINE PrecLUD(AD,IA,JA,AN,ANT,UD,iRow,Sym)
C-----------------------------------------------------------------------
      implicit real*8 (A-H,O-Z)
	Logical Sym
!      Parameter ( DiagonalScaling = .True. )
      DIMENSION AD(iRow),IA(*),JA(*),AN(*),ANT(*),UD(iRow)                     !,W(iRow)

! Scaling de la matriz con el maximo de cada fila
!      W=Dabs(AD)
      UD=Dabs(AD)
      DO I=1,iRow
!         CMax = Dabs(AD(I))
         CMax = UD(I)
         IAA  = IA(I)
         IAB  = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            if(Dabs(AN(K))>CMax)CMax=Dabs(AN(K))
	      if (.Not. Sym)then
!                   if( Dabs(ANT(K)) > W(J) ) W(J)=Dabs(ANT(K))
                   if( Dabs(ANT(K)) > UD(J) ) UD(J)=Dabs(ANT(K))
	      Endif
         Enddo
	   if( CMax > UD(I) ) UD(I)= CMax
!	    UD(I) = CMax
      ENDDO

!      if (.Not. Sym)then
!      DO I=1,iRow
!            if( W(I) > UD(I) ) UD(I)=W(I)
!      ENDDO
!      Endif
      RETURN
C
      END subroutine


C-----------------------------------------------------------------------
      SUBROUTINE SetBand(AD,IA,JA,AN,ANT,Band,Rows,iLower,iUpper,Sym,LD)
C-----------------------------------------------------------------------
      implicit real*8 (A-H,O-Z)
	Logical Sym
!      Parameter ( DiagonalScaling = .True. )
      Integer Rows
      DIMENSION AD(*),IA(*),JA(*),AN(*),ANT(*),Band(LD,*)

! Transcribe una matriz en formato ralo L D U a Formato Banda
! (recorta si se cae fuera de la banda)  
      
      iDiag = iLower + 1
      DO I=1,Rows
         Band(iDiag,I)= AD(I)
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
	      iU= J - I
            if( iU <= iUpper ) then 
                   Band ( iDiag+iU , I ) = AN(K)
	      Endif
            if( iU <= iLower ) then 
                   Band ( iDiag - iU , J ) = ANT(K)
	      Endif
         Enddo
      ENDDO


      RETURN
C
      END subroutine



C-----------------------------------------------------------------------
      SUBROUTINE DiagScaleLU(AD,IA,JA,AN,ANT,BL,W,Rows,Sym,UD)
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRAnsforma Pone todas las filas con diagonal positiva
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      implicit real*8 (A-H,O-Z)
	Logical DiagonalScaling,Sym
      Parameter ( DiagonalScaling = .True. )
      Integer Rows
      DIMENSION AD(Rows),UD(Rows),IA(*),JA(*),AN(*),ANT(*),BL(*),W(Rows)


! Diagonal Scaling
!      KDIAG = 0
      DO I=1,Rows
        IF (AD(I).GT.0.D0) THEN
            W(I) = 1.0d0/DSQRT(AD(I))
         ELSE IF (AD(I).LT.0.D0) THEN
!            KDIAG = KDIAG + 1
            W(I) = 1.0d0/DSQRT(-AD(I))
         ELSE
            KDIAG = KDIAG + 1
            W(I) = 1.D0
         END IF
      Enddo
      IF (KDIAG.GT.0) WRITE (6,*) 'DIAGSCD/WARNING: ',
     -   'SE ENCONTRARON ',KDIAG,' DIAGONALES NO POSITIVAS.'


!!!! Scaling con el maximo de cada fila
      Goto 166
      W=AD
      DO I=1,Rows
         CMax = W(I)
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            if(Dabs(AN(K))>CMax)CMax=Dabs(AN(K))
	      if (.Not. Sym)then
                   if( Dabs(ANT(K)) > W(J) ) W(J)=Dabs(ANT(K))
	      Endif
         Enddo
	    W(I) = CMax
      ENDDO
!en W twngo los maximos de cada fila
!      W=1.0d0/Dsqrt(W) 
!      W=Dsqrt(W)
!      W=1.0d0/W 
!  166    W=W/DSqrt(UD) 
  166    Continue 
!      W=Dsqrt(W) 

! Scaling de la matriz 
      DO I=1,Rows
         BL(I) = BL(I)*W(I)
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            AN(K) = AN(K)*W(I)*W(J)
            if (.not.Sym)ANT(K) = ANT(K)*W(J)*W(I)
         Enddo
      ENDDO
	AD=1.0d0
!      AD = AD * W 
!      AD = AD * W 

      Return

      DO I=1,Rows
!         CMax = Dabs(AD(I))
         CD = AD(I)
!         AD(I)=AD(I)*CD 
         BL(I) = BL(I)*CD
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            AN(K)=AN(K)*CD
            if(.Not. Sym)then
                   ANT(K) = ANT(K)*W(J)
            Endif
         Enddo
      ENDDO
!	W=1.0d0






      W=AD
      DO I=1,Rows
         CMax = W(I)
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            if(Dabs(AN(K))>CMax)CMax=Dabs(AN(K))
	      if (.Not. Sym)then
                   if( Dabs(ANT(K)) > W(J) ) W(J)=Dabs(ANT(K))
	      Endif
         Enddo
	    W(I) = CMax
      ENDDO

!      if (.Not. Sym)then
!      DO I=1,iRow
!            if( W(I) > UD(I) ) UD(I)=W(I)
!      ENDDO
!      Endif
      W=1.0d0/W



!   Doble Scaling >>>>>>>>>>>>>>>>>>>>>>>>>
!      return
!    divido por el maximo de cada fila
!      W=Dabs(AD)

!      Se escalea usando AD como vector auxiliar

!      DO I=1,Rows
!        IF (AD(I).GT.0.D0) THEN
!            W(I) = 1.0d0/DSQRT(AD(I))
!         ELSE IF (AD(I).LT.0.D0) THEN
!            KDIAG = KDIAG + 1
!            W(I) = 1.0d0/DSQRT(-AD(I))
!         ELSE
!            KDIAG = KDIAG + 1
!            W(I) = 1.D0
!         END IF
!      Enddo
!      IF (KDIAG.GT.0) WRITE (6,*) 'DIAGSCD/WARNING: ',
!     -   'SE ENCONTRARON ',KDIAG,' DIAGONALES NO POSITIVAS.'

      AD=1.0d0/DSqrt(AD)
! Scaling de la matriz 
      DO I=1,Rows
         BL(I) = BL(I)*AD(I)
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IAA, IAB
            J = JA(K)
            AN(K) = AN(K)*AD(I)*AD(J)
            if (.not.Sym)ANT(K) = ANT(K)*AD(J)*AD(I)
         Enddo
      ENDDO

      DO I=1,Rows
         W(I) = W(I)*AD(I)
      ENDDO
!   Doble Scaling >>>>>>>>>>>>>>>>>>>>>>>>>
      AD=1.0d0
      RETURN
C
      END subroutine

C-----------------------------------------------------------------------
      SUBROUTINE DiagScale(AD,IAC,JAC,AC,BL,W,Rows)
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRAnsforma Pone todas las filas con diagonal positiva
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
      implicit real*8 (A-H,O-Z)
	Logical DiagonalScaling
      Parameter ( DiagonalScaling = .True. )
!      Parameter (Tol=1.d4)
      Integer Rows
      DIMENSION AD(*),IAC(*),JAC(*),AC(*),BL(*),W(*),W1(Rows)

!      DO I=1,Rows
!	    IF ( AD(I) .LT. 0.0d0 ) then
!              BL(I) = -BL(I)
!              IAA   = IAC(I)
!              IAB   = IAC(I+1)-1
!              DO JP = IAA, IAB
!	         AC(JP) = -AC(JP)
!              Enddo
!          END IF
!      Enddo

!      if (DiagonalScaling) then

! Diagonal Scaling
!      KDIAG = 0
      DO I=1,Rows
        IF (AD(I).GT.0.D0) THEN
            W(I) = 1.0d0/DSQRT(AD(I))
         ELSE IF (AD(I).LT.0.D0) THEN
!            KDIAG = KDIAG + 1
            W(I) = 1.0d0/DSQRT(-AD(I))
         ELSE
!            KDIAG = KDIAG + 1
            W(I) = 1.D0
         END IF
      Enddo
!      IF (KDIAG.GT.0) WRITE (6,*) 'DIAGSCD/WARNING: ',
!     -   'SE ENCONTRARON ',KDIAG,' DIAGONALES NO POSITIVAS.'

! Scaling de la matriz 
      DO I=1,Rows
         BL(I) = BL(I)*W(I)
         IAA = IAC(I)
         IAB = IAC(I+1) - 1
         DO K = IAA, IAB
            J = JAC(K)
            AC(K) = AC(K)*W(I)*W(J)
         Enddo
      ENDDO

!      DO I=1,Rows
!	write(25,*)I,AD(I)
!      enddo

!   Doble Scaling >>>>>>>>>>>>>>>>>>>>>>>>>
      return
!    divido por el maximo de cada fila
      DO I=1,Rows
          IAA=IAC(I)
          IAB=IAC(I+1)-1
          !busco el maximo
          AMax=0.0d0
          DO K=IAA,IAB
		  if(dabs(AC(K)) .gt. AMax)AMax=dabs(AC(K))
          Enddo
          DO K=IAA,IAB
		 AC(K)=AC(K) / AMax
           if (JAC(K)==I) then
                 AD(I)=AC(K)
           endif   
          Enddo
         BL(I) = BL(I)/AMax
      Enddo

      DO I=1,Rows
         W1(I) = W(I)
      ENDDO

      DO I=1,Rows
        IF (AD(I).GT.0.D0) THEN
            W(I) = 1.0d0/DSQRT(AD(I))
         ELSE IF (AD(I).LT.0.D0) THEN
!            KDIAG = KDIAG + 1
            W(I) = 1.0d0/DSQRT(-AD(I))
         ELSE
!            KDIAG = KDIAG + 1
            W(I) = 1.D0
         END IF
      Enddo
!      IF (KDIAG.GT.0) WRITE (6,*) 'DIAGSCD/WARNING: ',
!     -   'SE ENCONTRARON ',KDIAG,' DIAGONALES NO POSITIVAS.'

! Scaling de la matriz 
      DO I=1,Rows
         BL(I) = BL(I)*W(I)
         IAA = IAC(I)
         IAB = IAC(I+1) - 1
         DO K = IAA, IAB
            J = JAC(K)
            AC(K) = AC(K)*W(I)*W(J)
         Enddo
      ENDDO

      DO I=1,Rows
         W(I) = W(I)*W1(I)
      ENDDO
!   Doble Scaling >>>>>>>>>>>>>>>>>>>>>>>>>


!      endif
      RETURN
C
      END subroutine


!-----------------------------------------------------------------------
      SUBROUTINE CGS_SaadD ( IA,JA,A,IU,JU,U,X,R,R0,G,Q,H,W,Z,
     &                       NOD,EPS,ITMAX,ISTAT )
!-----------------------------------------------------------------------
!                                                      *****************
! CONJUGATE-GRADIENTS SQUARED SOLVER FOR ASYMMETRIC    *   CGS_SaadD   *
! MATRIX EQUATIONS WITH PRECONDITIONING - VERSION 1    *****************
!-----------------------------------------------------------------------
!                       *** DOUBBLE PRECISION ***
!-----------------------------------------------------------------------
! Santiago Urquiza - UNMDP-MAR DEL PLATA - 30/JUN/2003
! SOBRE LA BASE DE CGS DE Axel Larreteguy - Termohidraulica - C.A. Bariloche - CNEA - 18/ENE/90
!-----------------------------------------------------------------------
! METODO ITERATIVO DE GRADIENTES CONJUGADOS CUADRADOS PRECONDICIONADO
! CON FACTORIZACION INCOMPLETA LU (ILU) PARA RESOLVER A x = b, DONDE  LA
! MATRIZ A ESTA ALMACENADA EN FORMATO SPARSE CRS.
!-----------------------------------------------------------------------
!
! ARGUMENTOS:
! [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
! [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
!
! IA,JA,A  [I]      ESTRUCTURA Y VALORES DE LA MATRIZ A EN RR(C)O /1/
! IU,JU    [I]      ESTRUCTURA DE LOS FACTORES TRIANGULARES EN FORMATO MSR DE LA MATRIZ DE PRECONDICIONAMINETO /Y. SAAD, Sparsekit ..../
! U        [I]      FACTORES TRIANGULARES DE LA MATRIZ DE PRECONDICIONAMINETO.
! R        [I,A]    VECTOR TERMINO INDEPENDIENTE
! R0,G,Q,  [W,X]    VECTORES AUXILIARES
! Z,W,H
! NOD      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
! EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
!                   ( |Xi-Xi+1|/|Xi| < EPS  y |Ri+1|/|B| < EPS ) o
!                   |Ri+1| = < 0.001EPS   DONDE |X|=NORMA L2 DE X
! ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
! X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
!                   O: VECTOR SOLUCION
! ISTAT    [O]      STATUS:
!                   ISTAT=    0  => OK. EL RESIDUO INICIAL ES MUY CHICO.
!                                    X0 YA ES LA SOLUCION BUSCADA.
!                   ISTAT>    0  => OK. ISTAT CONTIENE EL NUMERO DE
!                                    ITERACIONES QUE SE NECESITARON.
!                   ISTAT=   -1  => NO CONVERGIO EN ITMAX ITERACIONES.
!                   ISTAT=   -2  => |b| ES PRACTICAMENTE NULA (*).
!                   ISTAT=-3xxx  => VARIABLE  ro(n-1) CASI NULA EN
!                                   ITERACION xxx (*).
!                   ISTAT=-4xxx  => VARIABLE  sigma(n-1) CASI NULA EN
!                                   ITERACION xxx (*).
!
! (*) EL ALGORITMO FALLA Y NO PUEDE CONTINUAR. SI NO SE ENCUENTRAN
!     PROBLEMAS EN LOS DATOS, Y ESTAMOS SEGUROS DE QUE LA MATRIZ ES
!     INVERTIBLE, LO MAS PROBABLE ES QUE ESTE METODO NO SEA APLICABLE
!     EN ESE CASO PARTICULAR. ESTE METODO NO GARANTIZA QUE EL PROCESO
!     PUEDA SER SIEMPRE LLEVADO A CABO. EN ESOS CASOS PROBAR CON EL
!     METODO BCG, O CONSULTAR LA REFERENCIA /4/.
!
! STOP:  Si el numero de iteraciones ITMAX es > que las dimensiones
!        utilizadas en el COMMON /NORMAS/ (parameter MAXIMUM) 
!                                   ^ NOT ACTRUALLY IN USE
! ALGORITMO: REF /4/
!
! REFERENCIAS:
! /1/ S. PISSANETZKY, "SPARSE MATRIX TECHNOLOGY", ACADEMIC PRESS, 1984.
! /2/ D. S. KERSHAW, J. COMP. PHYSICS 26 (1978) 43-65.
! /3/ D. S. KERSHAW, J. COMP. PHYSICS 38 (1980) 114-123.
! /4/ P. SONNEVELD et al, "MULTIGRID AND CONJUGATE GRADIENT METHODS AS
!     CONVERGENCE ACCELERATION TECHNIQUES", in "Multigrid Methods for
!     Integral and Differential Equations", CLARENDON PRESS, OXFORD, 1985.
!
! RUTINAS UTILIZADAS: lusol FROM SPARSEKIT
!    ENORM2D, SPMUL2D 
! RUTINAS RELACIONADAS:
!    ILUT FROM SPARSEKIT USED TO OBTAIN U
!
!-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
!      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IU(*), JU(*)
      REAL*8 A(*),U(*),X(NOD),R0(NOD),R(NOD),G(NOD),H(NOD),Q(NOD),
     &       W(NOD),Z(NOD), EPS
      REAL*8 SCPRODD,RNORM2D,ENORM2D,ALFA,RON,RONM1,SIGMA,BETA,BETA2

      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
!
      ISTAT = 0
!      IF (ITMAX.GT.MAXIMUM)
!     -   STOP 'CGSAP1D/ERROR: Aumentar MAXIMUM en fuente de CGSAP1D.'
!
! |B|: Norma L2 de B
!      BNORM = RNORM2D(R,NOD)
      BNORM = Sqrt(Sum(R*R))
!  Santiago Urquiza -> se suma SHIFT a BNORM, ASI ESTA DE MAS EL IF SIGUIENTE, SINO COMENTAR LA LINEA DE ABAJO Y DESCOMENTAR LAS DOS LINEAS PORTERIORES AL IF
      BNORM = BNORM + SHIFT
      IF (BNORM.LT.TOO_SMALL) THEN
         ISTAT = -2
         RETURN
      END IF
!  Santiago Urquiza -> se suma SHIFT a BNORM
!      BNORM = BNORM + SHIFT
!
! R0 = B - A.X0
      CALL SPMUL2D (IA,JA,A,DUMMY,X,W,NOD,2)
      R  = R - W
      R0 = R

!
! |R0|/|B| <<<< 1?
      Rent  = Sqrt(Sum(R*R))
      RCONV = Rent / BNORM
!      RCONV = RNORM2D(R,NOD) / BNORM
!      IF (RCONV.LE.SMALL) RETURN
!     Modificada por Santiago Urquiza, que no vuelva sin iterar
!	Write(6,*)'First Residual',Rent,BNORM

!
! G[-1] = H[0] = 0.D0
!      CALL PUTVALD (G,NOD,0.D0,0,iDUMMY)
!      CALL PUTVALD (H,NOD,0.D0,0,iDUMMY)
      G = 0.0d0
	H = 0.0d0

!
!-----------------------------------------------------------------------
!                           LOOP ITERATIVO
!-----------------------------------------------------------------------
!
      I = 0
    1 CONTINUE
      ISTAT = I + 1
!
!           T             -1
! ro(n) = R0  Z ;  Z = (LU)  R ;  beta(n) = ro(n)/ro(n-1)
      RONM1= RON

       call lusol (NOD, R, Z, U, ju, iu)

      RON  = Sum(R0*Z)
      IF (I.EQ.0) THEN
         BETA = 0.D0
      ELSE
         IF (DABS(RONM1).LT.TOO_SMALL) THEN
            ISTAT = -(3*1000+I)
            RETURN
         END IF
         BETA = RON/RONM1
      END IF
!
! Q = Z + beta(n) H[n] ;  G[N] = Q + beta(n) ( beta(n) G[n-1] + H[n] )

	BETA2=BETA*BETA
      Q = BETA * H
	G = Q + BETA2*G 
      Q = Q + Z
      G = G + Q  
!
!              T              -1
! sigma(n) = R0  Z  ;  Z = (LU) A G[n]
      CALL SPMUL2D (IA,JA,A,DUMMY,G,Z,NOD,2)

       call lusol (NOD, Z, Z, U, ju, iu)
      SIGMA = Sum(R0*Z)
!      SIGMA = SCPRODD(R0,Z,NOD)
      IF (DABS(SIGMA).LT.TOO_SMALL) THEN
         ISTAT = -(4*1000+I)
          Write(6,*)"Warning SIGMA Too Small",SIGMA,I
         RETURN
      END IF
!
! H[n+1] = Q - alfa(n) Z  ;  alfa(n) = ro(n) / sigma(n)
      ALFA  = RON / SIGMA
	H = Q - ALFA * Z
!      CALL SCTXPYD (-ALFA,Z,Q,H,NOD,3)
!
! X[n+1] = X[n] + Z  ;   Z = alfa(n) ( Q + H[n+1] )  ; guardar X[n+1] -> W
      Z = ALFA * (Q + H)
      W = X + Z
!      CALL XPLUSYD (Q,H,Z,NOD,3)
!      CALL SCTIMXD (ALFA,Z,NOD)
!      CALL XPLUSYD (X,Z,W,NOD,3)
!
! R[n+1] = R[n] - A Z
      CALL SPMUL2D (IA,JA,A,DUMMY,Z,Q,NOD,2)
      R = R - Q
!      CALL XPLUSYD (R,Q,DUMMY,NOD,-1)
!
! Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
!                 |Ri+1| = ABSTOL
!      XCONV = ENORM2D(X,W,NOD) / (RNORM2D(X,NOD)+SHIFT)

      XCONV = ENORM2D(X,W,NOD) / (Sqrt(Sum(X*X))+SHIFT)
      Resf  = Sqrt(Sum(R*R))
      RCONV = Resf / BNORM
      XNOR(I) = XCONV + SHIFT
      RNOR(I) = RCONV + SHIFT

      WRITE (6,101)I,RCONV,XCONV,Resf

!      Se actualiza X
      X = W
!      CALL COPVECD (W,X,NOD)

      ABSTOL=EPS*.001

      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. Resf < ABSTOL )then
       WRITE (6,101)I,RCONV,XCONV,Resf
       RETURN
      END IF
!     No convergio en itmax
      IF (I.EQ.ITMAX-1) THEN
         ISTAT = -1
         WRITE (6,101)I,RCONV,XCONV,Resf
        RETURN
      END IF
      
!
! Nueva iteracion
      I = I + 1
      GOTO 1
!
 101  Format ( ' -> CGS_iter =',I5,'   |R|/|B| =',E8.2,
     &         '   |DX|/|X| =',E8.2,'   |R| =',E8.2 )

      END SubRoutine
!____________ End CGS




!-----------------------------------------------------------------------
      SUBROUTINE IluGauss (IA,JA,A,IU,JU,U,X,B,R0,W,H,IP,
     -                    NROW,EPS,ITMAX,ISTAT)
!-----------------------------------------------------------------------
!                                                      *****************
! CONJUGATE-GRADIENTS SQUARED SOLVER FOR ASYMMETRIC    * C G S A P 1 D *
! MATRIX EQUATIONS WITH PRECONDITIONING - VERSION 1    *****************
!-----------------------------------------------------------------------
!                       *** DOBLE PRECISION ***
!-----------------------------------------------------------------------
! Santiago Urquiza - Mech. Dep. - Eng. Faculty - UNMDP - 6/7/2001
!-----------------------------------------------------------------------
! METODO ITERATIVO DE CON FACTORIZACION INCOMPLETA LU (ILU)
! PARA RESOLVER A x = b, DONDE  LA
! MATRIZ A ESTA ALMACENADA EN FORMATO RCS (Row compresed sparse)
!-----------------------------------------------------------------------
!
! ARGUMENTOS:
! [I] input,  [O] output, [W] work space (espacio auxiliar de trabajo)
! [A] alterado por la rutina, [X] puede tener cualquier valor en entrada
!
! IA,JA,A  [I]      System Matrix CRS
! IU,JU,U  [I]      MRCS
! R        [I,A]    VECTOR TERMINO INDEPENDIENTE
! R0,G,Q,  [W,X]    VECTORES AUXILIARES
! Z,W,H
! NROW      [I]      ORDEN DE A Y DIMENSION DE TODOS LOS VECTORES
! EPS      [I]      CRITERIO DE CONVERGENCIA. EL PROCESO TERMINA CUANDO
!                   ( |Xi-Xi+1|/|Xi| < EPS  y |Ri+1|/|B| < EPS ) o
!                   |Ri+1| = 0.   DONDE |X|=NORMA L2 DE X
! ITMAX    [I]      NUMERO MAXIMO DE ITERACIONES
! X        [I,O]    I: VECTOR DE PARTIDA DE ITERACIONES
!                   O: VECTOR SOLUCION
! ISTAT    [O]      STATUS:
!                   ISTAT=    0  => OK. EL RESIDUO INICIAL ES MUY CHICO.
!                                    X0 YA ES LA SOLUCION BUSCADA.
!                   ISTAT>    0  => OK. ISTAT CONTIENE EL NUMERO DE
!                                    ITERACIONES QUE SE NECESITARON.
!                   ISTAT=   -1  => NO CONVERGIO EN ITMAX ITERACIONES.
!                   ISTAT=   -2  => |b| ES PRACTICAMENTE NULA (*).
!
! REFERENCIAS:
!
!
!-----------------------------------------------------------------------
      PARAMETER (MAXIMUM=10000)  ! ORIGINAL: 500
!      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER IA(*), JA(*), IU(*), JU(*),IP(*)
      REAL*8 A(*), U(*),X(NROW),R0(NROW),B(NROW),H(NROW),
     -       W(NROW),EPS
      REAL*8 SCPRODD, RNORM2D, ENORM2D,ALFA,RON,RONM1,SIGMA,BETA,BETA2

      REAL*4 XNOR(0:ITMAX), RNOR(0:ITMAX)
!      REAL*4 XNOR(0:MAXIMUM), RNOR(0:MAXIMUM)
!      COMMON /NORMAS/ NORMAS,XNOR,RNOR
      DATA TOO_SMALL,SMALL,SHIFT /1.D-30,1.D-20,1.D-25/
!
      ISTAT = 0




! |B|: Norma L2 de B
!      BNORM = RNORM2D(R,NROW)
      BNORM = Sqrt(Sum(B*B)) + SHIFT
!  Santiago Urquiza -> se suma SHIFT a BNORM
      IF (BNORM.LT.TOO_SMALL+Shift) THEN
         Write(6,*)"WARNING, B to small" 
         Write(6,*)"WARNING, B to small" 
         Write(6,*)"WARNING, B to small" 
         Write(6,*)"WARNING, B to small" 
         ISTAT = -2
!         RETURN
      END IF
!
! R0 = B - A.X0
      CALL SPMUL2D (IA,JA,A,DUMMY,X,W,NROW,2)
      R0  = B - W
! |R0|/|B| <<<< 1?
      Rent  = Sqrt(Sum(R0*R0))
      RCONV = Rent / BNORM
!
      Long = IA(NROW+1)-1    
      Write(6,*) "comenzo obtencion de IP",Long,NROW
      DO K = 1,Long
                          IP(K) = 0
      Enddo
      DO I = 1, NRow
      if(Mod(I,100)==0) Write(6,*) "Fila=",I,nrow    
         IUA = JU(I)
         IUB = JU(I+1) - 1
         IAA = IA(I)
         IAB = IA(I+1) - 1
         DO K = IUA, IUB
                    iColU = JU(K)
                  DO L = IAA , IAB 
                            iColA = JA(L)
                            IF (iColA == iColU) Then
	                           IP(L) = 1 
                                 exit
                            ElseIF(iColA > iColU) then
	                            exit
                            endif
                  enddo
         Enddo
      Enddo    

!-----------------------------------------------------------------------
!                           LOOP ITERATIVO
!-----------------------------------------------------------------------
!
      Write(6,*) "LOOP ITERATIVO"    

      I = 0
    1 CONTINUE
      ISTAT = I + 1
!
      DO ir = 1, NRow
      R0(ir)=B(ir)
         IAA = IA(ir)
         IAB = IA(ir+1) - 1
         DO K = IAA, IAB
                    iColA = JA(K)
	              if(iColA==ir) Cycle
                    IF (IP(K) == 0) Then
      	            R0(ir)=R0(ir)-A(K)*X(iColA)
                    endif
         Enddo
      Enddo    

      call lusol (NROW, R0, W, U, ju, iu)

!
!      CALL SPMUL2D (IA,JA,A,DUMMY,W,H,NROW,2)
!      R0 = B - H
!
! Convergencia: ( |Xi-Xi+1|/|Xi| < epsilon  y |Ri+1|/|B| < epsilon ) o
!                 |Ri+1| = 0.D0 ?
!      XCONV = ENORM2D(X,W,NROW) / (RNORM2D(X,NROW)+SHIFT)
      DX = ENORM2D(X,W,NROW)
	XN = Sqrt(Sum(W*W))
      XCONV = DX / (XN+SHIFT)
!      XNOR(I) = XCONV + SHIFT
!      RNOR(I) = RCONV + SHIFT
      WRITE (6,101)I,XCONV,DX,XN
 101  Format ( 
     &' ->ILUGaus=',I5,' |DX|/|X|=',E8.2,' |DX|=',E8.2,' |X|=',E8.2)
 102  Format ( 
     &' ->ILUGaus=',I5,' |R|/|B|=',E8.2,' |DX|/|X|=',E8.2,' |R|=',E8.2)
!
!      Se actualiza X
      X = W
!      CALL COPVECD (W,X,NROW)
      ABSTOL=EPS*.001
      IF ( (XCONV.LE.EPS.AND.RCONV.LE.EPS) .OR. Resf < ABSTOL )then
 
      CALL SPMUL2D (IA,JA,A,DUMMY,X,H,NROW,2)
      R0 = B - H
      Resf  = Sqrt(Sum(R0*R0))
      RCONV = Resf / BNORM
 
       WRITE (6,102)I,RCONV,XCONV,Resf
       RETURN
      END IF
!     No convergio en itmax
      IF (I.EQ.ITMAX-1) THEN
         ISTAT = -1
         WRITE (6,101)I,XCONV
        RETURN
      END IF
      
!
! Nueva iteracion
      I = I + 1
      GOTO 1
!
      END SubRoutine
!____________ End IluGauss





!      end module Solver
