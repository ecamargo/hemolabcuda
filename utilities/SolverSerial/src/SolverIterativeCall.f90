        Subroutine ITERATIVE_SOLVER								&
			      ( IA,JA,ANT,AD,AN,B,UnK1,						&
					IU,JU,UNT,UD,UN,							&          
					IAC,JAC,AC,									&
					EPS,ITMAXSOL,								&
					ISTAT,iRow,Krylov,LFil,TolPre,				&
					WorkSpace,									&
					iSolverType,Sym,                            &
                    IE,JE,IET,JET,X,                            &
                    IEqNode,IEqDof,IEqT,iELType,                &
                    NodT,NelT,iDofT,NDim,iControl,DelT,DTm    )


!     _            UV,WW,C,R0,G,Q,H,W,Z,

Implicit Real*8(A-H,O-Z)
Dimension   IA(*),JA(*),ANT(*),AD(iRow),AN(*),UnK1(iRow),	  &
                 IU(*),JU(*),UNT(*),UD(iRow),UN(*),           &          
                 IE(*),JE(*),IET(*),JET(*),X(*),              &
                 IEqNode(*),IEqDof(*),IEqT(*),iELType(*),     &
                 ipar(16),fpar(16),                           & 
                 IAC(*),JAC(*),AC(*),WorkSpace(*),B(iRow),IP(2*iRow) !,Diag(iRow)
      external cg,bcg,dbcg,bcgstab,tfqmr,gmres,fgmres,dqgmres
      external cgnr, fom, runrc, ilut
Logical Sym
ALLOCATABLE BAND      (:,:)  


Select Case (iSolverType)

!%%%%%%%%%%%%%%%%
      Case (1)
!%%%%%%%%%%%%%%%%
!     iSolverType=1 Gaus Seidel iterative Solver
!       ITMAXSOL=float(iRowT)
!        Call DiagScaleLU(AD,IA,JA,AN,ANT,B,Work,iRow,Sym)
                  
 19   Call GAUSEILU(IA,JA,ANT,AN,AD,B,UnK1,WorkSpace,      &
     &               WorkSpace(iRow+1),iRow,ITMAXSOL,ISTAT)

!      Do ikl=1,iRow
!             UnK1(ikl)=UnK1(ikl)*Work(ikl)
!	enddo

			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->GAUSSEIDEL.ERROR #', ISTAT
                  goto 19
			ELSE 
				WRITE (6,*)'--->GAUSSEIDEL Converged', ISTAT
			ENDIF             

!%%%%%%%%%%%%%%%%
	Case (2)
!%%%%%%%%%%%%%%%%
!     iSolverType=2 Conjugate Gradient Squared iterative Solver

!        iZZ  = iUD  + iRow 
		ipz= 1 + iRow

        Call Precond(Ie,Je,IeT,JeT,X,UD,iELType,IEqNode,IEqDof,IEqT,NodT,Ndim,iDofT)
!        UD=1.0d0
        Call SigchangeLU(IA,JA,AD,ANT,AN,B,iRow,Sym)
!Goto 1999

       iSwitchDS = 0
       iLoop=0
 1998 Continue

!------------OPT DS----------------------
    if(iSwitchDS>0) Then
!        UD= UD/AD 
        Call DiagScaleLU(AD,IA,JA,AN,ANT,B,WorkSpace(ipz),iRow,Sym,UD)
        Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)/WorkSpace(ipz+ikl-1)
        enddo
    Endif
!------------OPT DS----------------------

        NZLOW=0  
!        CALL NOPRECD (UNT,UN,UD,NZLOW,iRow)
   ! if(iSwitchDS==0)Call PrecLUD(AD,IA,JA,AN,ANT,UD,iRow,Sym)
    if(iSwitchDS==0)Then
!        UD = AD 
!        UD = 1.0d0  /AD 
        UD = UD/AD 
!        UD = 1.0d0
    endif
!------------OPT JACOBI----------------------
!           UD=1.0d0/AD
!------------OPT OPT JACOBI----------------------


!------------OPT DS----------------------
!           UD=DSqrt(AD) 
!            UD=1.0d0
!            UD= AD 
                 if(iSwitchDS>0)Then
!                 UD=1./DSqrt(AD) 
!                   UD=DSqrt(UD) 
!                  UD= AD 
!                  AD = 1.0d0
!                   UD = 1.0d0
!                    UD= 1.0d0/AD 
!                  UD= AD 
                 endif
!------------OPT DS----------------------

!               ITMAXSOL= float(iRowT)/5 
  20    CONTINUE
  iLoop=iLoop+1
  IF (iLoop > 10000 .and. iSwitchDS == 0 ) then
    iSwitchDS=1
    goto 1998
  Endif
      CALL AsigReal  ( WorkSpace,B,iRow)
!      C=B_Load

        Call CGSAP1DLU2( IA,JA,AN,ANT,AD,                             &
                         IU,JU,UNT,UD,UN,UnK1,WorkSpace,              &
                         WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),   &
						 WorkSpace(3*iRow+ipz),WorkSpace(4*iRow+ipz), &
						 WorkSpace(5*iRow+ipz),WorkSpace(6*iRow+ipz), &
						 iRow,EPS/(1.+float(iSwitchDS)*10.),ITMAXSOL,ISTAT )

			MM=-ISTAT/1000
			IF (MM.EQ.3 .OR. MM.EQ.4 ) THEN
				WRITE (6,*) '->CGSAP1D.ERROR #', ISTAT
				WRITE (6,*) '->CGSAP1D New Loop Attempted', iLoop
                iLoop=iLoop-1
!				WRITE (6,*)
				GOTO 20
			END IF
			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->CGSAP1D.ERROR #', ISTAT
				WRITE (6,*) '->CGSAP1D New Loop Attempted', iLoop
                  goto 20
			ELSE 
				WRITE (6,*)'--->CGSAP1D Converged', ISTAT
                if(iLoop==1 ) then
				WRITE (6,*) '->CGSAP1D New Loop Attempted', iLoop
                    goto 20
                endif
			ENDIF             
!------------OPT DS----------------------

      if(iSwitchDS>0)then 
        Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)*WorkSpace(ipz+ikl-1)
        enddo
      Endif
!------------OPT DS----------------------

return
1999  Continue
    IDO = 0
	Xnor=EPS

    CALL AsigReal  ( WorkSpace,B,iRow)
!        DO ikl=1,iRow
!           UD(ikl)=AD(ikl)
!           if(UD(ikl).eq.0.0d0)UD(ikl)=1.0d0
!        enddo

!    CALL PCGRC (IDO, N, X, P, R, Z, RELERR, ITMAX)
!  21 CALL DPCGRC (IDO, iRow, UnK1, WorkSpace(iRow+ipz), WorkSpace, WorkSpace(2*iRow+ipz), Xnor, ITMAXSOL)
!  21 CALL DJCGRC (IDO, iRow, AD, UnK1, WorkSpace(iRow+ipz), WorkSpace, WorkSpace(2*iRow+ipz), Xnor, ITMAXSOL)
!10 CALL JCGRC (IDO, N, DIAG, X, P, R, Z, RELERR, ITMAX)

      IF (IDO .EQ. 1) THEN
!            Set Z = AP
!      CALL MURRV (N, N, A, LDA, N, P, 1, N, Z)
      CALL SPMUL2D (IA,JA,AN,AD,WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),iRow,1)
!      GO TO 21

	   ElseIf (IDO == 2 ) then
        DO iKL=1, iRow
            WorkSpace(2*iRow+ipz+iKL-1) = WorkSpace(iKL)/AD(iKL)
        Enddo
!     	 GO TO 21
      END IF
!	If (IDO .NE. 2 ) then
	If (IDO .NE. 3 ) then
	    ISTAT = -1
!	   WRITE (6,*) 'New NSWEEP'
!         goto 1
      else
	    ISTAT = 0
      endif

      
!%%%%%%%%%%%%%%%%
	Case (3)
!%%%%%%%%%%%%%%%%
!        iSolverType=3 Conjugate Gradient Squared iterative Solver Preconditioned with Incomplete Factorization
         Call TRANS_ULtoC(IA,JA,ANT,AN,AD, &
     &                    IAC,JAC,AC,iRow)
!        La matriz factorizada U la guardo en A

ipz=iRow+1
        Call Sigchange(AD,IAC,AC,B,iRow)

!-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-
        Call DiagScale(AD,IAC,JAC,AC,B,WorkSpace(ipz),iRow)
!          SCALING FOR THE INITIAL CONDITION
      Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)/WorkSpace(ipz+ikl-1)
      enddo

        Call IniREAL ( AD, iRow, 1.0d0 ) 
!-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-

        LJA=IA(iRow+1)-1
        Call IniREAL ( AN, LJA ,  .0d0 ) 
        IF ( .Not. Sym)Call IniREAL ( ANT, LJA ,  .0d0 )

!-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU
!        Call IniREAL ( AD, iRow, .0d0 )  
!        CALL  ILUFK1D (IAC,JAC,AC,IU,JU,ANT,AD,AN,IP,iRow)
!-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU-ILU

!        DO ikl=1,iRow
!           AD(ikl)=1.0d0/AD(ikl)
!        enddo
	  ICount=0
  30   CONTINUE
       ICount=ICount+1
      CALL AsigReal  ( WorkSpace,B,iRow)
!      C=B_Load

!      Call BCGAP1D(IAC,JAC,AC,IU(iIU),JU(iJU),AN(iANT),AD,AN,
!     -             UnK1,C,G,H,Q,W,Z,iRow,EPS(nstp),ITMAXSOL(nstp),ISTAT)
!      Call MCGAP1D (IAC,JAC,AC,IU(iIU),JU(iJU),AN(iANT),AD,AN,
!     -             UnK1,C,G,H,Q,W,Z,iRow,EPS(nstp),ITMAXSOL(nstp),ISTAT)
         Call CGSAP1D (IAC,JAC,AC,IU,JU,ANT,AD,AN,                   &
                       UnK1,WorkSpace,                               &
                        WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),   &
						WorkSpace(3*iRow+ipz),WorkSpace(4*iRow+ipz), &
						WorkSpace(5*iRow+ipz),WorkSpace(6*iRow+ipz), &
					   iRow,EPS,ITMAXSOL,ISTAT)
            MM=-ISTAT/1000
            IF (MM.EQ.3 .OR. MM.EQ.4 ) THEN
                          WRITE (6,*) '->CGSAP1D.ERROR #', ISTAT
                  WRITE (6,*)
				GOTO 30
			END IF
			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->CGSAP1D.ERROR #', ISTAT
                  goto 30
			ELSE 
                  If (ICount .EQ. 1 .and. ISTAT .EQ. 1)goto 30 
				WRITE (6,*)'--->CGSAP1D Converged', ISTAT
			ENDIF             
!-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-
      Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)*WorkSpace(ipz+ikl-1)
	enddo
!-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-DS-

!%%%%%%%%%%%%%%%%
	Case (4)
!%%%%%%%%%%%%%%%%
!     iSolverType=4 GMRES Solver Preconditioned with Incomplete Factorization
!        Do I=1,iRowT
!	      if(AD(I).ne. 0.d0) then    
!               UD(I) = DAbs(1.0d0/AD(I))
!            else
!               UD(I) = 1.0d0
!            endif     
!        enddo  
!        UD=1.0d0/AD
	  ipW   = 1  + iRow 
	  ipB   = ipW + iRow  
	  ipH   = ipB + Krylov 		
	  ipE   = ipH + (Krylov+1)**2 		
	  ipC   = ipE + (Krylov+1) 		
	  ipS   = ipC + Krylov 		
	  ipY   = ipS + Krylov 		
	  ipUV  = ipY + Krylov 		
!  BETA       : Vector auxiliar                                       |
!  H          : Vector auxiliar                                       |
!  E          : Vector auxiliar                                       |
!  C          : Vector auxiliar                                       |
!  S          : Vector auxiliar                                       |
!  Y          : Vector auxiliar                                       |
!  UV         : iRow*(K+1)
!  W          : iRow
         Call TRANS_ULtoC(IA,JA,ANT,AN,AD,IAC,JAC,AC,iRow)
!        La matriz resultado de la factorizacion de AC la guardo en A (No se usa U)
        Call Sigchange(AD,IAC,AC,B,iRow)

        LJA=IA(iRow+1)-1
        Call IniREAL ( AN, LJA ,  .0d0 ) 
        IF ( .Not. Sym )Call IniREAL ( ANT, LJA ,  .0d0 )
        Call IniREAL ( AD, iRow, .0d0 ) 
        CALL ILUFK1D (IAC,JAC,AC,IU,JU,ANT,AD,AN,IP,iRow)
!	  AD = UD 
!        DO ikl=0,iRow-1
!           AD(iAD+ikl)=UD(iUD+ikl)
!        enddo

!        call SPMULDiagDLU (IU,JU,AN,ANT,AD,UD,iRowT,5)
!               ITMAXSOL= float(iRowT)/5 
	  ICount=0
  40    CONTINUE
       ICount=ICount+1
      CALL AsigReal  ( WorkSpace,B,iRow)
!      C=B_Load
        Call GMRESD1 (IAC,JAC,AC,IU,JU,								  &
			            ANT,AD,AN,UnK1,WorkSpace,					  &
					    WorkSpace(ipUV),WorkSpace(ipW),EPS,ITMAXSOL,  &
						ISTAT,iRow,Krylov,							  &
						WorkSpace(ipB),WorkSpace(ipH),WorkSpace(ipE), &
						WorkSpace(ipC),WorkSpace(ipS),WorkSpace(ipY))
			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->GMRESD1.ERROR #', ISTAT
				WRITE (6,*) '->GMRESD1.ERROR #', ISTAT
                  GOTO 40
			ELSE 
                  If (ICount .EQ. 1 .and. ISTAT .EQ. 1)goto 40 
				WRITE (6,*)'--->GMRESD1 Converged', ISTAT
			ENDIF             

!%%%%%%%%%%%%%%%%
	Case (5)
!%%%%%%%%%%%%%%%%
!     iSolverType=5 GMRES Solver Preconditioned with Jacobi diagonal matrix

	  ipz   = 1   + iRow 
	  ipW   = ipz + iRow 
	  ipB   = ipW + iRow  
	  ipH   = ipB + Krylov 		
	  ipE   = ipH + (Krylov+1)**2 		
	  ipC   = ipE + (Krylov+1) 		
	  ipS   = ipC + Krylov 		
	  ipY   = ipS + Krylov 		
	  ipUV  = ipY + Krylov 		
!  BETA       : Vector auxiliar                                       |
!  H          : Vector auxiliar                                       |
!  E          : Vector auxiliar                                       |
!  C          : Vector auxiliar                                       |
!  S          : Vector auxiliar                                       |
!  Y          : Vector auxiliar                                       |
!  UV         : iRow*(K+1)
!  W          : iRow


         Call TRANS_ULtoC(IA,JA,ANT,AN,AD,IAC,JAC,AC,iRow)
!        La matriz factorizada U la guardo en A

        Call Sigchange(AD,IAC,AC,B,iRow)
        Call DiagScale(AD,IAC,JAC,AC,B,Workspace(ipz),iRow)
!        DO ikl=0,iRow-1
!           AD(1+ikl)=UD(iUD+ikl)
!        enddo
        NZLOW = 0
        CALL NOPRECD (UNT,UN,UD,NZLOW,iRow)
!        DO ikl=0,iRow-1
!           UD(iUD+ikl)=1.0d0/AD(1+ikl)
!        enddo


!               ITMAXSOL= float(iRowT)/5 
	  ICount=0
  50    CONTINUE
       ICount=ICount+1
      CALL AsigReal  ( WorkSpace,B,iRow)
!      C=B_Load
        Call GMRESD1 (IAC,JAC,AC,IU,JU,								  &
			            ANT,AD,AN,UnK1,WorkSpace,					  &
					    WorkSpace(ipUV),WorkSpace(ipW),EPS,ITMAXSOL,  &
						ISTAT,iRow,Krylov,							  &
						WorkSpace(ipB),WorkSpace(ipH),WorkSpace(ipE), &
						WorkSpace(ipC),WorkSpace(ipS),WorkSpace(ipY))
			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->GMRESD1.ERROR #', ISTAT
				WRITE (6,*) '->GMRESD1.ERROR #', ISTAT
                  GOTO 50
			ELSE 
                  If (ICount .EQ. 1 .and. ISTAT .EQ. 1)goto 50 
				WRITE (6,*)'--->GMRESD1 Converged', ISTAT
			ENDIF             
      Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)*WorkSpace(ipz-1+ikl)
	enddo


!%%%%%%%%%%%%%%%%
	Case (20)
!%%%%%%%%%%%%%%%%
!     iSolverType=2 Conjugate Gradient Squared iterative Solver Preconditioned
! with Band Matrix Obtained by Incomplete Factorization

!        iZZ  = iUD  + iRow 
		ipz= 1 + iRow
        Call SigchangeLU(IA,JA,AD,ANT,AN,B,iRow,Sym)
!Goto 1999

      iSwitchDS=1
! 1998 Continue

!------------OPT DS----------------------
    if(iSwitchDS>0) Then
! OJO !!!!!!!!!!!Comentado el diagonal Scaling
!        Call DiagScaleLU(AD,IA,JA,AN,ANT,B,WorkSpace(ipz),iRow,Sym)
!        Do ikl=1,iRow
!             UnK1(ikl)=UnK1(ikl)/WorkSpace(ipz+ikl-1)
!        enddo
    Endif
!------------OPT DS----------------------

!        NZLOW=0  
!        CALL NOPRECD (UNT,UN,UD,NZLOW,iRow)
!    if(iSwitchDS==0)Call PrecLUD(AD,IA,JA,AN,ANT,UD,iRow,Sym)
!    if(iSwitchDS==0)UD= 1.0d0/UD 

!------------OPT JACOBI----------------------
!           UD=1.0d0/AD
!------------OPT OPT JACOBI----------------------


!------------OPT DS----------------------
!           UD=DSqrt(AD) 
!            UD=1.0d0
!            UD= AD 
                 if(iSwitchDS>0)Then
!                  UD= AD 
                  AD = 1.0d0
                 endif
!------------OPT DS----------------------
iLower=28;iUpper=28
LDA = 2*iLower+iUpper+1
Allocate (  Band(LDA,iRow), STAT = iERR_ALLOC  )
IF(iERR_ALLOC.NE.0)Call ERROR_ALLOC

Call SetBand(AD,IA,JA,AN,ANT,Band,iRow,iLower,iUpper,Sym,LDA)
NLCA=iLower
NUCA=iUpper
!CALL DLFCRB (iRow,Band,LDA,NLCA,NUCA,Band,LDA,IP,RCOND)


!!!! Parche para que compile sin Warnings!!!!! OJOOOO!!!!! Comentadas las lineas de DLFCRB-DLFSRB

! Comentarios en Solver1.for y en SolverIterativeCall.f90

RCOND=1.0


!!!! Parche para que compile sin Warnings!!!!! OJOOOO!!!!! Comentadas las lineas de DLFCRB-DLFSRB



Write(6,*)'Condition Number of precondition system',RCOND,1.0d0/RCOND 
!               ITMAXSOL= float(iRowT)/5 
   iLoop=0
  2005    CONTINUE
  iLoop=iLoop+1
!  IF (iLoop > 100 .and. iSwitchDS == 0 ) then
!    iSwitchDS=1
!    goto 1998
!  Endif

      CALL AsigReal  ( WorkSpace,B,iRow)
!      C=B_Load

        Call CGSAP1DLUBand (IA,JA,AN,ANT,AD,                         &
                        IP,Band,LDA,NLCA,NUCA,UnK1,WorkSpace,        &
                        WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),   &
						WorkSpace(3*iRow+ipz),WorkSpace(4*iRow+ipz), &
						WorkSpace(5*iRow+ipz),WorkSpace(6*iRow+ipz), &
						iRow,EPS,ITMAXSOL,ISTAT        )

			MM=-ISTAT/1000
			IF (MM.EQ.3 .OR. MM.EQ.4 ) THEN
				WRITE (6,*) '->CGSAP1DLUBand.ERROR #', ISTAT
!				WRITE (6,*)
				GOTO 2005
			END IF
			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->CGSAP1DLUBand.ERROR #', ISTAT
                  goto 2005
			ELSE 
				WRITE (6,*)'--->CGSAP1DLUBand Converged', ISTAT
			ENDIF             
!------------OPT DS----------------------

      if(iSwitchDS>0)then 
        Do ikl=1,iRow
             UnK1(ikl)=UnK1(ikl)*WorkSpace(ipz+ikl-1)
        enddo
      Endif
!------------OPT DS----------------------

DeAllocate (Band)


!%%%%%%%%%%%%%%%%
	Case (100:)
!%%%%%%%%%%%%%%%%
     iSwitchDS=1
!     if(iSwitchDS>0) Then
!        Call Precond(Ie,Je,IeT,JeT,X,UD,iELType,IEqNode,IEqDof,IEqT,NodT,Ndim,iDofT)

!              DO I=1,iRow
!                CD = 1.0d0/UD(I)
!                 AD(I)=AD(I)*CD 
!                 B(I) = B(I)*CD
!
!                 IAA = IA(I)
!                 IAB = IA(I+1) - 1
!                 DO K = IAA, IAB
!                    J = JA(K)
!                    AN(K)=AN(K)*CD
!                    if(.Not. Sym)then
!                           ANT(K) = ANT(K)/UD(J)
!                    Endif
!                 Enddo
!              ENDDO
!     endif
         Call TRANS_ULtoC(IA,JA,ANT,AN,AD,IAC,JAC,AC,iRow)

!     set-up the preconditioner ILUT(15, 1E-4) ! new definition of lfil
!
      nrow = iRow  
!      lfil = 15   ! Fill in of each row in L and U
!      tol = 1.0D-4 ! this is too high for ilut for saylr1
!      tol = 1.0D-7
!      nwk = (iRow*2*LFIL+1)  !+2 * ( IA(iRow+1)-1 ) + iRow+1 ! Longitud de JU Y UN 


!------------OPT DS----------------------
     if(iSwitchDS>0) Then

         job = 0 ! gets ac only (the 1 means use 1-norm, 2: 2-norm; 0: max norm)
         call roscal(nrow,job,1,ac,jac,iac,UD,ac,jac,iac,ierr)
         B=UD*B
         call coscal(nrow,job,1,ac,jac,iac,UD,ac,jac,iac,ierr)
         Unk1=Unk1/UD   
     Endif
        tol = TolPre   !1.0D-6
        nwk = iRow+1+ (iRow*2*LFIL+1)  !+2 * ( IA(iRow+1)-1 ) + iRow+1 ! Longitud de JU Y UN 


        Select Case(iSolvertype)
!
! 100       CG      == 5 * n
! 103      CGNR    == 5 * n
! 101      BCG     == 7 * n
! 102      DBCG    == 11 * n
! 104      BCGSTAB == 8 * n
! 105      TFQMR   == 11 * n
! 106      FOM     == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
! 107      GMRES   == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
! 108      FGMRES  == 2*n*(m+1) + (m+1)*m/2 + 3*m + 2 (m = ipar(5),
!                  default m=15)
! 109      DQGMRES == n + lb * (2*n+4) (lb=ipar(5)+1, default lb = 16)

            !%%%%%%%%%%%%%%%%
                  Case (100,103)
            !%%%%%%%%%%%%%%%%
                  lwk = 5 * iRow + iRow

            !%%%%%%%%%%%%%%%%
                  Case (101)
            !%%%%%%%%%%%%%%%%
                  lwk = 7 * iRow + iRow

            !%%%%%%%%%%%%%%%%
                  Case (102,105)
            !%%%%%%%%%%%%%%%%
                  lwk = 11 * iRow + iRow

            !%%%%%%%%%%%%%%%%
                  Case (104)
            !%%%%%%%%%%%%%%%%
                  lwk = 8 * iRow + iRow

            !%%%%%%%%%%%%%%%%
                  Case (106:107)
            !%%%%%%%%%%%%%%%%
                lwk=(iRow+3)*(Krylov+2) +(Krylov+1)*Krylov/2 + iRow       !(m = ipar(5), default m=15)
            !%%%%%%%%%%%%%%%%
                  Case (108)
            !%%%%%%%%%%%%%%%%
             lwk=2*iRow*(Krylov+1)+(Krylov+1)*Krylov/2+3*Krylov+2+iRow     !(m = ipar(5),
            !%%%%%%%%%%%%%%%%
                  Case (109)
            !%%%%%%%%%%%%%%%%
                lwk = iRow + (Krylov + 1) * (2*iRow+4) + iRow      !(lb=ipar(5)+1, default lb = 16)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

             EndSelect

! Set the preconditioning Matrix
! iw      = integer work array of length 2*n -> WorkSpace(2*nrow+2)
! w       = real work array of length n+1 -> WorkSpace(1+nrow)
!      Call TimerOUT("Goes into Preconditioner")

      call ilut (nrow,ac,jac,iac,lfil,tol,un,ju,iu,nwk,WorkSpace(1+nrow),WorkSpace(2*nrow+2),ierr)
        mbloc=nrow
        permtol=.01
   !   call ilutp(nrow,ac,jac,iac,lfil,tol,permtol,mbloc,un,ju,iu,nwk,WorkSpace(1+nrow),WorkSpace(2*nrow+2),ip,ierr)


!      Call TimerOUT("System is Preconditioned, begining solver loops")
!      JU0=iROW+2  
!      JU(1) = JU0
!      IU(1) = JU0
!      DO I = 1, iRow
!         IAA = IAc(I)
!         IAB = IAc(I+1) - 1
!         IF (IAB.LT.IAA) then 
!          write(6,*) "Null Diag of AC, at row=",I
!         endif
!         DO K = IAA, IAB
!            J = JAc(K)
!            IF (I == J) Then
!                 UN(I)=1.0d0/AC(K)
!                 exit      
!            endif
!         enddo
!         JU(I+1) = JU0
!         IU(I+1) = JU0
!       Enddo

!      call iluk (nrow,ac,jac,iac,lfil,au,jau,ju,levs,nwk,wk,iw,ierr)
       if(ierr == -1) Write(6,*) "incomprehensible error. Matrix must be wrong."
       if(ierr == -2) Write(6,*) "insufficient storage in L."
       if(ierr == -3) Write(6,*) "insufficient storage in U."
       if(ierr == -4) Write(6,*) "illegal lfil entered."
       if(ierr == -5) Write(6,*) "zero row encountered."
       if(ierr < 0 ) Stop
!
!     set the parameters for the iterative solvers
!
!     ipar(3) -- stopping criteria (details of this will be
!     discussed later).
!     ipar(4) -- number of elements in the array 'w'. if this is less
!     than the desired size, it will be over-written with the minimum
!     requirement. In which case the status flag ipar(1) = -2.
!     ipar(5) -- size of the Krylov subspace (used by GMRES and its
!     variants), e.g. GMRES(ipar(5)), FGMRES(ipar(5)),
!     DQGMRES(ipar(5)).
!     ipar(6) -- maximum number of matrix-vector multiplies, if not a
!     positive number the iterative solver will run till convergence
!     test is satisfied.
!     ipar(7) -- current number of matrix-vector multiplies. It is
!     incremented after each matrix-vector multiplication. If there
!     is preconditioning, the counter is incremented after the
!     preconditioning associated with each matrix-vector multiplication.
!     ipar(8) -- pointer to the input vector to the requested matrix-
!     vector multiplication.
!     ipar(9) -- pointer to the output vector of the requested matrix-
!     vector multiplication.
!     To perform v = A * u, it is assumed that u is w(ipar(8):ipar(8)+n-1)
!     and v is stored as w(ipar(9):ipar(9)+n-1).
!     ipar(10) -- the return address (used to determine where to go to
!     inside the iterative solvers after the caller has performed the
!     requested services).
!     ipar(11) -- the result of the external convergence test
!     On final return from the iterative solvers, this value
!     will be reflected by ipar(1) = 0 (details discussed later)
!     ipar(12) -- error code of MGSRO, it is
!                  1 if the input vector to MGSRO is linear combination
!                    of others,
!                  0 if MGSRO was successful,
!                 -1 if the input vector to MGSRO is zero,
!                 -2 if the input vector contains invalid number.
!
!     ipar(13) -- number of initializations. During each initilization
!                 residual norm is computed directly from M_l(b - A x).
!     ipar(14) to ipar(16) are NOT defined, they are NOT USED by
!     any iterative solver at this time.

!     fpar(1) = 1.0E-6	! relative tolerance 1.0E-6
!     fpar(2) = 1.0E-10 ! absolute tolerance 1.0E-10
!     fpar(11) = 0.0	! clearing the FLOPS counter





!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!LDA = 2*iLower+iUpper+1
!Allocate (  Band(LDA,iRow), STAT = iERR_ALLOC  )
!IF(iERR_ALLOC.NE.0)Call ERROR_ALLOC


!        iZZ  = iUD  + iRow 
!		ipz= 1 + iRow
!        Call SigchangeLU(IA,JA,AD,ANT,AN,B,iRow,Sym)
!Goto 1999

!      iSwitchDS=1
! 1998 Continue

!------------OPT DS----------------------
!    if(iSwitchDS>0) Then
! OJO !!!!!!!!!!!Comentado el diagonal Scaling
!        Call DiagScaleLU(AD,IA,JA,AN,ANT,B,WorkSpace(ipz),iRow,Sym)
!        Do ikl=1,iRow
!             UnK1(ikl)=UnK1(ikl)/WorkSpace(ipz+ikl-1)
!        enddo
!    Endif
!------------OPT DS----------------------

!        NZLOW=0  
!        CALL NOPRECD (UNT,UN,UD,NZLOW,iRow)
!    if(iSwitchDS==0)Call PrecLUD(AD,IA,JA,AN,ANT,UD,iRow,Sym)
!    if(iSwitchDS==0)UD= 1.0d0/UD 

!------------OPT JACOBI----------------------
!           UD=1.0d0/AD
!------------OPT OPT JACOBI----------------------


!------------OPT DS----------------------
!           UD=DSqrt(AD) 
!            UD=1.0d0
!            UD= AD 
!                 if(iSwitchDS>0)Then
!                  UD= AD 
!                  AD = 1.0d0
!                 endif
!------------OPT DS----------------------

  iLoop=0
 2006    CONTINUE
  iLoop=iLoop+1

!  IF (iLoop > 100 .and. iSwitchDS == 0 ) then
!    iSwitchDS=1
!    goto 1998
!  Endif

      CALL AsigReal  ( WorkSpace,B,iRow)
!      UNK1=.1
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
      ipar=0
      fpar=0.0d0
      ipar(1) = 0
      ipar(2) = 2  !Right preconditioning
      ipar(3) = 2  ! Use the residual criteria for convergence check scaled with the rhs
!      -2 == || dx(i) || <= rtol * || rhs || + atol
!      -1 == || dx(i) || <= rtol * || dx(1) || + atol
!       0 == solver will choose test 1 (next)
!       1 == || residual || <= rtol * || initial residual || + atol
!       2 == || residual || <= rtol * || rhs || + atol
!     999 == caller will perform the test
      ipar(4) = lwk  ! the 'w' has lwk elements
      ipar(5) = Krylov   ! use *GMRES(Krylov) (e.g. FGMRES(Krylov))
      ipar(6) = ITMAXSOL
      fpar(1) = EPS
      fpar(2) = EPS*0.001

      Select Case (iSolverType)
            !%%%%%%%%%%%%%%%%
                  Case (100)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** CG ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,cg)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (101)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** BCG ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,bcg)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (102)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** DBCG ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,dbcg)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (103)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** CGNR ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,cgnr)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (104)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** BCGSTAB ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,bcgstab)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (105)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** TFQMR ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,tfqmr)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (106)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** FOM ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,fom)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (107)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** GMRES ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,gmres)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (108)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** FGMRES ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,fgmres)
             ISTAT = ipar(1)
            !%%%%%%%%%%%%%%%%
                  Case (109)
            !%%%%%%%%%%%%%%%%
      print *, ' '
      print *, '	*** DQGMRES ***'
      call runrc(nrow,WorkSpace,UnK1,ipar,fpar,WorkSpace(1+nrow),ac,jac,iac,UN,JU,IU,dqgmres)
             ISTAT = ipar(1)




            !%%%%%%%%%%%%%%%%
                  Case (110)
            !%%%%%%%%%%%%%%%%
                ipz=1    !+iRow
                Call CGS_SaadD ( IAC,JAC,AC,IU,JU,UN,                          &
                                 UnK1,WorkSpace,                               &
                                 WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),    &
						         WorkSpace(3*iRow+ipz),WorkSpace(4*iRow+ipz),  &
						         WorkSpace(5*iRow+ipz),WorkSpace(6*iRow+ipz),  &
					             iRow,EPS,ITMAXSOL,ISTAT)
                                    MM=-ISTAT/1000
                                    IF (MM.EQ.3 .OR. MM.EQ.4 ) THEN
                                                  WRITE (6,*) '->CGS.ERROR #', ISTAT
                                          WRITE (6,*)
			                        END IF
			                        IF (ISTAT.LT.0) THEN
				                        WRITE (6,*) '->CGS.ERROR #', ISTAT
			                        ELSE 
                                          If (iLoop .EQ. 1 .and. ISTAT .EQ. 1)goto 2006 
				                        WRITE (6,*)'--->CGS Converged', ISTAT
			                        ENDIF             



            !%%%%%%%%%%%%%%%%
!                  Case (111)
            !%%%%%%%%%%%%%%%%
!                ipz=1    !+iRow

!                Call ILUGauss ( IAC,JAC,AC,IU,JU,UN,                           &
!                                 UnK1,WorkSpace,                               &
!                                 WorkSpace(iRow+ipz),WorkSpace(2*iRow+ipz),    &
!						         WorkSpace(3*iRow+ipz),WorkSpace(4*iRow+ipz),  &
!					             iRow,EPS,ITMAXSOL,ISTAT)
!                                    MM=-ISTAT/1000
!                                    IF (MM.EQ.3 .OR. MM.EQ.4 ) THEN
!                                                  WRITE (6,*) '->ILUGauss.ERROR #', ISTAT
!                                          WRITE (6,*)
!			                        END IF
!			                        IF (ISTAT.LT.0) THEN
!				                        WRITE (6,*) '->ILUGauss.ERROR #', ISTAT
!			                        ELSE 
!                                          If (iLoop .EQ. 1 .and. ISTAT .EQ. 1)goto 2006 
!				                        WRITE (6,*)'--->ILUGauss Converged', ISTAT
!			                        ENDIF             
    End Select
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!      Call TimerOUT("")

			IF (ISTAT.LT.0) THEN
				WRITE (6,*) '->SAAD Iterative Solver has NOT CONVERGED in Itmax =',ITMAXSOL
                  IF (ISTAT == -1 ) then
                     WRITE (6,*) '    ->A new Loop is Attempted', iLoop
                   goto 2006
                   endif
                Stop
			ELSE 
				WRITE (6,*)'--->SAAD Iterative Solver has CONVERGED in Loop', iLoop
			ENDIF             
!------------OPT DS----------------------

      if(iSwitchDS>0)then 
         Unk1=Unk1*UD   
!        Do ikl=1,iRow
!             UnK1(ikl)=UnK1(ikl)*WorkSpace(ipz+ikl-1)
!        enddo
      Endif
!------------OPT DS----------------------
               do k=1, iac(nrow+1)-1 
                !  jac(k) = ip(jac(k)) 
               enddo

!DeAllocate (Band)




Case Default

End Select

Return
end



Subroutine Precond(Ie,Je,IeT,JeT,X,PM,iELType,IEqNode,IEqDof,IEqT,NodT,Ndim,iDofT)

Implicit Real*8(A-H,O-Z)

Dimension Ie(*),Je(*),IeT(*),JeT(*),X(*),PM(*),iELType(*),IEqNode(*),IEqDof(*),IEqT(*)
Dimension XA(3),XB(3),XC(3),Vn(3)

       Loop_Nodos: do Nod=1,NodT

!            if(IC(Nod)==0) Cycle Loop_Nodos
!            if(Idir(Nod) /=0)Cycle Loop_Nodos
                iN1 = IET(Nod)
                iN2 = IET(Nod+1)-1
                
                VNod=0.0d0
                DO jNel=iN1,iN2
                    Nelj=JET(jNel)
!                    TempNod =TempNod+(TempElem(Nelj)-Temp(Nod))     
                    if( iELType(Nelj) /= 1 ) Cycle
                       Nj = Ie(Nelj)
                       N1=Je(Nj) 
                       N2=Je(Nj+1) 
                       N3=Je(Nj+2) 
                       N4=Je(Nj+3)
                       ipx1=3*(N1-1)
                       ipx2=3*(N2-1)
                       ipx3=3*(N3-1)
                       ipx4=3*(N4-1)
                       XA(1) = X(ipx2+1) - X(ipx1+1)  
                       XA(2) = X(ipx2+2) - X(ipx1+2)  
                       XA(3) = X(ipx2+3) - X(ipx1+3)  

                       XB(1) = X(ipx3+1) - X(ipx1+1)  
                       XB(2) = X(ipx3+2) - X(ipx1+2)  
                       XB(3) = X(ipx3+3) - X(ipx1+3)  

                       XC(1) = X(ipx4+1) - X(ipx1+1)  
                       XC(2) = X(ipx4+2) - X(ipx1+2)  
                       XC(3) = X(ipx4+3) - X(ipx1+3)  
      
                    Call ProductoVectorial ( XA , XB , Vn , DA ) 
!                    TempNod =TempNod+(TempElem(Nelj)*DA)
                    DV = Dabs(Sum(Vn*XC))/24.0d0
                    VNod=VNod+DV     
                enddo

                ipN=(Nod-1)*iDofT
                do nci=1,iDofT
                    iRowA = IEqT(ipN+nci)
                    if (iRowA==0)  Cycle
                    if ( VNod==0.0d0) Then
                                            PM(iRowA) = 1.0d0
                    Else
                                   if(nci>4) then
                                            PM(iRowA) = 1.0d0
                                   else
    !                                        PM(iRowA) = Vnod**(0.666666666666666666)
                                            PM(iRowA) = Vnod
                                   endif
!                                             PM(iRowA) = Vnod**(0.6666666666666666)
!                                            PM(iRowA) = Vnod
                    Endif
                enddo

       enddo Loop_Nodos


Return
end

!
!----------------------------------------------------------------------c
!                          S P A R S K I T                             !
!----------------------------------------------------------------------!
!         Basic Iterative Solvers with Reverse Communication           c
!----------------------------------------------------------------------c
!     This file currently has several basic iterative linear system    c
!     solvers. They are:                                               c
!     CG       -- Conjugate Gradient Method                            c
!     CGNR     -- Conjugate Gradient Method (Normal Residual equation) c
!     BCG      -- Bi-Conjugate Gradient Method                         c
!     DBCG     -- BCG with partial pivoting                            c
!     BCGSTAB  -- BCG stabilized                                       c
!     TFQMR    -- Transpose-Free Quasi-Minimum Residual method         c
!     FOM      -- Full Orthogonalization Method                        c
!     GMRES    -- Generalized Minimum RESidual method                  c
!     FGMRES   -- Flexible version of Generalized Minimum              c
!                 RESidual method                                      c
!     DQGMRES  -- Direct versions of Quasi Generalize Minimum          c
!                 Residual method                                      c
!----------------------------------------------------------------------c
!     They all have the following calling sequence:
!      subroutine solver(n, rhs, sol, ipar, fpar, w)
!      integer n, ipar(16)
!      real*8 rhs(n), sol(n), fpar(16), w(*)
!     Where
!     (1) 'n' is the size of the linear system,
!     (2) 'rhs' is the right-hand side of the linear system,
!     (3) 'sol' is the solution to the linear system,
!     (4) 'ipar' is an integer parameter array for the reverse
!     communication protocol,
!     (5) 'fpar' is an floating-point parameter array storing
!     information to and from the iterative solvers.
!     (6) 'w' is the work space (size is specified in ipar)
!
!     They are preconditioned iterative solvers with reverse
!     communication. The preconditioners can be applied from either
!     from left or right or both (specified by ipar(2), see below).
!
!     Author: Kesheng John Wu (kewu@mail.cs.umn.edu) 1993
!
!     NOTES:
!
!     (1) Work space required by each of the iterative solver
!     routines is as follows:
!       CG      == 5 * n
!       CGNR    == 5 * n
!       BCG     == 7 * n
!       DBCG    == 11 * n
!       BCGSTAB == 8 * n
!       TFQMR   == 11 * n
!       FOM     == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
!       GMRES   == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
!       FGMRES  == 2*n*(m+1) + (m+1)*m/2 + 3*m + 2 (m = ipar(5),
!                  default m=15)
!       DQGMRES == n + lb * (2*n+4) (lb=ipar(5)+1, default lb = 16)
!
!     (2) ALL iterative solvers require a user-supplied DOT-product
!     routine named DISTDOT. The prototype of DISTDOT is
!
!     real*8 function distdot(n,x,ix,y,iy)
!     integer n, ix, iy
!     real*8 x(1+(n-1)*ix), y(1+(n-1)*iy)
!
!     This interface of DISTDOT is exactly the same as that of
!     DDOT (or SDOT if real == real*8) from BLAS-1. It should have
!     same functionality as DDOT on a single processor machine. On a
!     parallel/distributed environment, each processor can perform
!     DDOT on the data it has, then perform a summation on all the
!     partial results.
!
!     (3) To use this set of routines under SPMD/MIMD program paradigm,
!     several things are to be noted: (a) 'n' should be the number of
!     vector elements of 'rhs' that is present on the local processor.
!     (b) if RHS(i) is on processor j, it is expected that SOL(i)
!     will be on the same processor, i.e. the vectors are distributed
!     to each processor in the same way. (c) the preconditioning and
!     stopping criteria specifications have to be the same on all
!     processor involved, ipar and fpar have to be the same on each
!     processor. (d) DISTDOT should be replaced by a distributed
!     dot-product function.
!
!     ..................................................................
!     Reverse Communication Protocols
!
!     When a reverse-communication routine returns, it could be either
!     that the routine has terminated or it simply requires the caller
!     to perform one matrix-vector multiplication. The possible matrices
!     that involve in the matrix-vector multiplications are:
!     A       (the matrix of the linear system),
!     A^T     (A transposed),
!     Ml^{-1} (inverse of the left preconditioner),
!     Ml^{-T} (inverse of the left preconditioner transposed),
!     Mr^{-1} (inverse of the right preconditioner),
!     Mr^{-T} (inverse of the right preconditioner transposed).
!     For all the matrix vector multiplication, v = A u. The input and
!     output vectors are supposed to be part of the work space 'w', and
!     the starting positions of them are stored in ipar(8:9), see below.
!
!     The array 'ipar' is used to store the information about the solver.
!     Here is the list of what each element represents:
!
!     ipar(1) -- status of the call/return.
!     A call to the solver with ipar(1) == 0 will initialize the
!     iterative solver. On return from the iterative solver, ipar(1)
!     carries the status flag which indicates the condition of the
!     return. The status information is divided into two categories,
!     (1) a positive value indicates the solver requires a matrix-vector
!     multiplication,
!     (2) a non-positive value indicates termination of the solver.
!     Here is the current definition:
!       1 == request a matvec with A,
!       2 == request a matvec with A^T,
!       3 == request a left preconditioner solve (Ml^{-1}),
!       4 == request a left preconditioner transposed solve (Ml^{-T}),
!       5 == request a right preconditioner solve (Mr^{-1}),
!       6 == request a right preconditioner transposed solve (Mr^{-T}),
!      10 == request the caller to perform stopping test,
!       0 == normal termination of the solver, satisfied the stopping
!            criteria,
!      -1 == termination because iteration number is greater than the
!            preset limit,
!      -2 == return due to insufficient work space,
!      -3 == return due to anticipated break-down / divide by zero,
!            in the case where Arnoldi procedure is used, additional
!            error code can be found in ipar(12), where ipar(12) is
!            the error code of orthogonalization procedure MGSRO:
!               -1: zero input vector
!               -2: input vector contains abnormal numbers
!               -3: input vector is a linear combination of others
!               -4: trianguler system in GMRES/FOM/etc. has nul rank
!      -4 == the values of fpar(1) and fpar(2) are both <= 0, the valid
!            ranges are 0 <= fpar(1) < 1, 0 <= fpar(2), and they can
!            not be zero at the same time
!      -9 == while trying to detect a break-down, an abnormal number is
!            detected.
!     -10 == return due to some non-numerical reasons, e.g. invalid
!            floating-point numbers etc.
!
!     ipar(2) -- status of the preconditioning:
!       0 == no preconditioning
!       1 == left preconditioning only
!       2 == right preconditioning only
!       3 == both left and right preconditioning
!
!     ipar(3) -- stopping criteria (details of this will be
!     discussed later).
!
!     ipar(4) -- number of elements in the array 'w'. if this is less
!     than the desired size, it will be over-written with the minimum
!     requirement. In which case the status flag ipar(1) = -2.
!
!     ipar(5) -- size of the Krylov subspace (used by GMRES and its
!     variants), e.g. GMRES(ipar(5)), FGMRES(ipar(5)),
!     DQGMRES(ipar(5)).
!
!     ipar(6) -- maximum number of matrix-vector multiplies, if not a
!     positive number the iterative solver will run till convergence
!     test is satisfied.
!
!     ipar(7) -- current number of matrix-vector multiplies. It is
!     incremented after each matrix-vector multiplication. If there
!     is preconditioning, the counter is incremented after the
!     preconditioning associated with each matrix-vector multiplication.
!
!     ipar(8) -- pointer to the input vector to the requested matrix-
!     vector multiplication.
!
!     ipar(9) -- pointer to the output vector of the requested matrix-
!     vector multiplication.
!
!     To perform v = A * u, it is assumed that u is w(ipar(8):ipar(8)+n-1)
!     and v is stored as w(ipar(9):ipar(9)+n-1).
!
!     ipar(10) -- the return address (used to determine where to go to
!     inside the iterative solvers after the caller has performed the
!     requested services).
!
!     ipar(11) -- the result of the external convergence test
!     On final return from the iterative solvers, this value
!     will be reflected by ipar(1) = 0 (details discussed later)
!
!     ipar(12) -- error code of MGSRO, it is
!                  1 if the input vector to MGSRO is linear combination
!                    of others,
!                  0 if MGSRO was successful,
!                 -1 if the input vector to MGSRO is zero,
!                 -2 if the input vector contains invalid number.
!
!     ipar(13) -- number of initializations. During each initilization
!                 residual norm is computed directly from M_l(b - A x).
!
!     ipar(14) to ipar(16) are NOT defined, they are NOT USED by
!     any iterative solver at this time.
!
!     Information about the error and tolerance are stored in the array
!     FPAR. So are some internal variables that need to be saved from
!     one iteration to the next one. Since the internal variables are
!     not the same for each routine, we only define the common ones.
!
!     The first two are input parameters:
!     fpar(1) -- the relative tolerance,
!     fpar(2) -- the absolute tolerance (details discussed later),
!
!     When the iterative solver terminates,
!     fpar(3) -- initial residual/error norm,
!     fpar(4) -- target residual/error norm,
!     fpar(5) -- current residual norm (if available),
!     fpar(6) -- current residual/error norm,
!     fpar(7) -- convergence rate,
!
!     fpar(8:10) are used by some of the iterative solvers to save some
!     internal information.
!
!     fpar(11) -- number of floating-point operations. The iterative
!     solvers will add the number of FLOPS they used to this variable,
!     but they do NOT initialize it, nor add the number of FLOPS due to
!     matrix-vector multiplications (since matvec is outside of the
!     iterative solvers). To insure the correct FLOPS count, the
!     caller should set fpar(11) = 0 before invoking the iterative
!     solvers and account for the number of FLOPS from matrix-vector
!     multiplications and preconditioners.
!
!     fpar(12:16) are not used in current implementation.
!
!     Whether the content of fpar(3), fpar(4) and fpar(6) are residual
!     norms or error norms depends on ipar(3). If the requested
!     convergence test is based on the residual norm, they will be
!     residual norms. If the caller want to test convergence based the
!     error norms (estimated by the norm of the modifications applied
!     to the approximate solution), they will be error norms.
!     Convergence rate is defined by (Fortran 77 statement)
!     fpar(7) = log10(fpar(3) / fpar(6)) / (ipar(7)-ipar(13))
!     If fpar(7) = 0.5, it means that approximately every 2 (= 1/0.5)
!     steps the residual/error norm decrease by a factor of 10.
!
!     ..................................................................
!     Stopping criteria,
!
!     An iterative solver may be terminated due to (1) satisfying
!     convergence test; (2) exceeding iteration limit; (3) insufficient
!     work space; (4) break-down. Checking of the work space is
!     only done in the initialization stage, i.e. when it is called with
!     ipar(1) == 0. A complete convergence test is done after each
!     update of the solutions. Other conditions are monitored
!     continuously.
!
!     With regard to the number of iteration, when ipar(6) is positive,
!     the current iteration number will be checked against it. If
!     current iteration number is greater the ipar(6) than the solver
!     will return with status -1. If ipar(6) is not positive, the
!     iteration will continue until convergence test is satisfied.
!
!     Two things may be used in the convergence tests, one is the
!     residual 2-norm, the other one is 2-norm of the change in the
!     approximate solution. The residual and the change in approximate
!     solution are from the preconditioned system (if preconditioning
!     is applied). The DQGMRES and TFQMR use two estimates for the
!     residual norms. The estimates are not accurate, but they are
!     acceptable in most of the cases. Generally speaking, the error
!     of the TFQMR's estimate is less accurate.
!
!     The convergence test type is indicated by ipar(3). There are four
!     type convergence tests: (1) tests based on the residual norm;
!     (2) tests based on change in approximate solution; (3) caller
!     does not care, the solver choose one from above two on its own;
!     (4) caller will perform the test, the solver should simply continue.
!     Here is the complete definition:
!      -2 == || dx(i) || <= rtol * || rhs || + atol
!      -1 == || dx(i) || <= rtol * || dx(1) || + atol
!       0 == solver will choose test 1 (next)
!       1 == || residual || <= rtol * || initial residual || + atol
!       2 == || residual || <= rtol * || rhs || + atol
!     999 == caller will perform the test
!     where dx(i) denote the change in the solution at the ith update.
!     ||.|| denotes 2-norm. rtol = fpar(1) and atol = fpar(2).
!
!     If the caller is to perform the convergence test, the outcome
!     should be stored in ipar(11).
!     ipar(11) = 0 -- failed the convergence test, iterative solver
!     should continue
!     ipar(11) = 1 -- satisfied convergence test, iterative solver
!     should perform the clean up job and stop.
!
!     Upon return with ipar(1) = 10,
!     ipar(8)  points to the starting position of the change in
!              solution Sx, where the actual solution of the step is
!              x_j = x_0 + M_r^{-1} Sx.
!              Exception: ipar(8) < 0, Sx = 0. It is mostly used by
!              GMRES and variants to indicate (1) Sx was not necessary,
!              (2) intermediate result of Sx is not computed.
!     ipar(9)  points to the starting position of a work vector that
!              can be used by the caller.
!
!     NOTE: the caller should allow the iterative solver to perform
!     clean up job after the external convergence test is satisfied,
!     since some of the iterative solvers do not directly
!     update the 'sol' array. A typical clean-up stage includes
!     performing the final update of the approximate solution and
!     computing the convergence information (e.g. values of fpar(3:7)).
!
!     NOTE: fpar(4) and fpar(6) are not set by the accelerators (the
!     routines implemented here) if ipar(3) = 999.
!
!     ..................................................................
!     Usage:
!
!     To start solving a linear system, the user needs to specify
!     first 6 elements of the ipar, and first 2 elements of fpar.
!     The user may optionally set fpar(11) = 0 if one wants to count
!     the number of floating-point operations. (Note: the iterative
!     solvers will only add the floating-point operations inside
!     themselves, the caller will have to add the FLOPS from the
!     matrix-vector multiplication routines and the preconditioning
!     routines in order to account for all the arithmetic operations.)
!
!     Here is an example:
!     ipar(1) = 0	! always 0 to start an iterative solver
!     ipar(2) = 2	! right preconditioning
!     ipar(3) = 1	! use convergence test scheme 1
!     ipar(4) = 10000	! the 'w' has 10,000 elements
!     ipar(5) = 10	! use *GMRES(10) (e.g. FGMRES(10))
!     ipar(6) = 100	! use at most 100 matvec's
!     fpar(1) = 1.0E-6	! relative tolerance 1.0E-6
!     fpar(2) = 1.0E-10 ! absolute tolerance 1.0E-10
!     fpar(11) = 0.0	! clearing the FLOPS counter
!
!     After the above specifications, one can start to call an iterative
!     solver, say BCG. Here is a piece of pseudo-code showing how it can
!     be done,
!
! 10   call bcg(n,rhs,sol,ipar,fpar,w)
!      if (ipar(1).eq.1) then
!         call amux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
!         goto 10
!      else if (ipar(1).eq.2) then
!         call atmux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
!         goto 10
!      else if (ipar(1).eq.3) then
!         left preconditioner solver
!         goto 10
!      else if (ipar(1).eq.4) then
!         left preconditioner transposed solve
!         goto 10
!      else if (ipar(1).eq.5) then
!         right preconditioner solve
!         goto 10
!      else if (ipar(1).eq.6) then
!         right preconditioner transposed solve
!         goto 10
!      else if (ipar(1).eq.10) then
!         call my own stopping test routine
!         goto 10
!      else if (ipar(1).gt.0) then
!         ipar(1) is an unspecified code
!      else
!         the iterative solver terminated with code = ipar(1)
!      endif
!
!     This segment of pseudo-code assumes the matrix is in CSR format,
!     AMUX and ATMUX are two routines from the SPARSKIT MATVEC module.
!     They perform matrix-vector multiplications for CSR matrices,
!     where w(ipar(8)) is the first element of the input vectors to the
!     two routines, and w(ipar(9)) is the first element of the output
!     vectors from them. For simplicity, we did not show the name of
!     the routine that performs the preconditioning operations or the
!     convergence tests.
!-----------------------------------------------------------------------
      subroutine runrc(n,rhs,sol,ipar,fpar,wk,a,ja,ia,au,jau,ju,solver)
      implicit none
      integer n,ipar(16),ia(n+1),ja(*),ju(*),jau(*)
      real*8 fpar(16),rhs(n),sol(n),wk(*),a(*),au(*)
      external solver
!-----------------------------------------------------------------------
!     the actual tester. It starts the iterative linear system solvers
!     with a initial guess suppied by the user.
!
!     The structure {au, jau, ju} is assumed to have the output from
!     the ILU* routines in ilut.f.
!
!-----------------------------------------------------------------------
!     local variables
!
      integer i, iou, its
      real*8 res, dnrm2
!     real dtime, dt(2), time
!     external dtime
      external dnrm2
      save its,res
!
!     ipar(2) can be 0, 1, 2, please don't use 3
!
      if (ipar(2).gt.2) then
         print *, 'I can not do both left and right preconditioning.'
         return
      endif
!
!     normal execution
!
      its = 0
      res = 0.0D0
!
!      do i = 1, n
!         sol(i) = guess(i)
!      enddo
!
      iou = 6
      ipar(1) = 0
!     time = dtime(dt)
 10   call solver(n,rhs,sol,ipar,fpar,wk)
!
!     output the residuals
!
      if (ipar(7).ne.its) then
         write (iou, *) its, real(res)
         its = ipar(7)
      endif
      res = fpar(5)
!
      if (ipar(1).eq.1) then
         call amux(n, wk(ipar(8)), wk(ipar(9)), a, ja, ia)
         goto 10
      else if (ipar(1).eq.2) then
         call atmux(n, wk(ipar(8)), wk(ipar(9)), a, ja, ia)
         goto 10
      else if (ipar(1).eq.3 .or. ipar(1).eq.5) then
         call lusol(n,wk(ipar(8)),wk(ipar(9)),au,jau,ju)
         goto 10
      else if (ipar(1).eq.4 .or. ipar(1).eq.6) then
         call lutsol(n,wk(ipar(8)),wk(ipar(9)),au,jau,ju)
         goto 10
      else if (ipar(1).le.0) then
         if (ipar(1).eq.0) then
            print *, 'Iterative sovler has satisfied convergence test.'
         else if (ipar(1).eq.-1) then
            print *, 'Iterative solver has iterated too many times.'
         else if (ipar(1).eq.-2) then
            print *, 'Iterative solver was not given enough work space.'
            print *, 'The work space should at least have ', ipar(4),' elements.'
         else if (ipar(1).eq.-3) then
            print *, 'Iterative sovler is facing a break-down.'
         else
            print *, 'Iterative solver terminated. code =', ipar(1)
         endif
      endif
!     time = dtime(dt)
      write (iou, *) ipar(7), real(fpar(6))
      write (iou, *) '# retrun code =', ipar(1),' convergence rate =', fpar(7)
!     write (iou, *) '# total execution time (sec)', time
!
!     check the error
!
      call amux(n,sol,wk,a,ja,ia)
      do i = 1, n
!         wk(n+i) = sol(i) -1.0D0
         wk(i) = wk(i) - rhs(i)
      enddo
      write (iou, *) '# the actual residual norm is', dnrm2(n,wk,1)
!      write (iou, *) '# the error norm is', dnrm2(n,wk(1+n),1)
!
      if (iou.ne.6) close(iou)
      return
      end
!-----end-of-runrc


