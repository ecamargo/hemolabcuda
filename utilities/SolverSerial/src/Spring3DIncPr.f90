!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 SubRoutine Spring3DIncPr                                      &
                ( AE,BE,MaxLRows,XLL,NDim,iDofT,NodELT,        &
                  Sol0,Sol1,CommonPar,                         &
                  Param,JParam,DelT,DTm,Time)
!!!!!!!!!!!!!!!! Ojo hay que revisarlo !!!!!!!!!!!!!!!!!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 IMPLICIT REAL*8 (A-H,O-Z)

 Parameter (PI = 3.141592653589793238D0)
!      Parameter (Theta = 0.50D0 )
!     NdimE: dimensionality of the triangle element,
!     we use a 2d element in a 3d problem. 
! Parameter ( NodEL = 3, NdimE = 2 , NdimG = 3 )
 Parameter ( NGP  = 3 )
!      Real*8 Jac(NdimE,NdimE)

!      iSimplex = 0 -> The element is a Simplex
!      iSimplex = 1 -> The element is a Hipercube
!      iBU: use bubble function
 Parameter (iSimplex = 0 , iBU = 0) !, NdimE1 = 3, NdimG1 = 4)

 DIMENSION AE(MaxLRows,MaxLRows), BE(MaxLRows),               &
           XLL(*),Sol0(*),Sol1(*),                            &
           Param(*), JParam(*),CommonPar(*),                  &
           Psi(Ndim-1,NGP),W(NGP),Vel0(Ndim),Vel1(Ndim),      &
           Fi1(NodElT),Vn(3),XA(3),XB(3),                     &
           Pres(NodELT),XL(Ndim*NodELT),Pm(NodELT,NodELT)
 Real*8   Kv
! INTEGER, STATIC :: iCicle
! Real*8, STATIC :: Am(NodEL,NodEL) !See SAVE ATTRIBUTE
!      OnlyVertex indicates a subparametric interpolation with only vertex nodes 
!      OnlyVertex = .True. 
!      iCicle = iCicle + 1

NodEL = NodElT   ; NdimE = Ndim-1 ; NdimG = Ndim
NdimE1 = NdimE+1 ; NdimG1 = NdimG +1

      Penalty      = CommonPar(1)
      CN           = CommonPar(2)
!      FacG         = CommonPar(3)
      Enduro       = CommonPar(4)
      iSimetry     = nint(CommonPar(5))
      iSimetryAxis = nint(CommonPar(6))
      if(nint(CommonPar(11)) == 0 ) then  
              ElasH         = CommonPar(7)
!              h            = CommonPar(8)
              RC           = CommonPar(8)
              PC           = CommonPar(9)
              Kv           = CommonPar(10)
      else
              ElasH        = Param(1)  ! ElasH=E*h/Rc
!              h            = CommonPar(8)
              RC           = Param(2)
              PC           = Param(3)
              Kv           = Param(4)  ! Kv = K*h/Rc
      endif
      Incremental = nint( CommonPar(12) ) 
!!!! Ojo fijado a incremental
      Incremental = 1
        
!       Enduro = (10001.0d0 - 100.0d0/.1*Time) 
       if(Enduro.lt.1.0d0) Enduro = 1.0d0

!      RC           = Param(.1)*FacG
!      h            = .1d0*RC
!      Elas         = Param(2)
       rElas        = ElasH/RC*Enduro
       rKv          = Kv/RC

 Do Nod  = 1, NodEl
  iPNod  = (Nod-1)*NDimG 
  iPNDof = (Nod-1)*iDofT + NdimG1
  Do nsd = 1, NDim
!   if(Incremental > 0 ) then   
!      XL(iPNod + nsd) =XLL(iPNod + nsd) !+ Sol1(iPNDof + nsd) !Comentado para la version incremental
!     else
      XL(iPNod + nsd) =XLL(iPNod + nsd) + Sol1(iPNDof + nsd)
!    endif
  end do
 end do

! Sides Vectors
if ( Ndim > 2 ) then
      XA(1) = XL(4) - XL(1)  
      XA(2) = XL(5) - XL(2)  
      XA(3) = XL(6) - XL(3)  

      XB(1) = XL(7) - XL(1)  
      XB(2) = XL(8) - XL(2)  
      XB(3) = XL(9) - XL(3)

      Acoef = 6.0d0

Else
      XA(1) = XL(3) - XL(1)  
      XA(2) = XL(4) - XL(2)  
      XA(3) = 0.0d0  

      XB(1) = 0.0d0  
      XB(2) = 0.0d0
      XB(3) = 1.0d0
      Acoef = 2.0d0

Endif       
      
      Call ProductoVectorial ( XA , XB , Vn , DA ) 
      Vn  = (Vn/DA)*CN !Normal Vector, CN tiene el signo de la normal
      DA3 = Dabs(DA) / Acoef*Penalty
!--------------------------
! The mass matriz is calculated only the first time this procedure is called
! if (iCicle .eq. 1) Then 
!     Am=0.0d0
! Psi: Gauss Points Matrix, W: Weights
!     Call GaussRule (Psi,W,NdimE,NGP,iSimplex)    
!     LoopGaussPoints: Do nG = 1, NGP
!          Call ShapeF (NodEL,NdimE,Fi1,PSI(1,nG),1,iBu)
!          WnG = W(nG)
!          do NodC = 1, NodEL
!              Fi1C = Fi1(NodC)
!              do NodR = 1, NodEL
!                 Am(NodR,NodC) = Am(NodR,NodC) + Fi1(NodR)*Fi1C*WnG 
!              Enddo
!          Enddo
!     End Do LoopGaussPoints
! End if
!--------------------------
!   Pm = Am * DA 
!------------------------------------
! do NodR=1, NodEL
!       ipRow   = (NodR-1)*iDofT 
!       ipRowP  = ipRow + NdimG1 
!       do nsd   = 1, NdimG
!        ipRowV  = ipRow + nsd
!        ipRowX  = ipRowP+ nsd
!        VecNi   = Vn(nsd)*Penalty 
!        AE( ipRowV , ipRowV ) =   Penalty * Dtm
!        AE( ipRowV , ipRowX ) = - Penalty
!        BE( ipRowV )          = - Penalty * Sol0(ipRowX) 
!        BE( ipRowX )          = - VecNi*PC*DA/6.d0 
!        do NodC = 1, NodEL
!          ipCol   = (NodC-1)* iDofT 
!          ipColP  = ipCol  + NdimG1
!          ipColX  = ipColP + nsd
! for computational efficiency transpose matrix are used 
!          AE( ipRowX , ipColX ) =   rElas_tPen * Pm( NodC,NodR )
!          AE( ipRowX , ipColP ) = -    VecNi   * Pm( NodC,NodR )
!        Enddo
!       Enddo
! Enddo
!--------------------------
   if (iSimetry == 1 .AND. NdimE == 1) then
               Radio = 0.0d0
               nci = 1 + iSimetryAxis
                Do Nodrad=1, NodEl
                 Radio = Radio + XL( (Nodrad-1)*NDim + nci)
                Enddo
                 DA3=DA3*2.0d0*PI*Radio/Dfloat(NodEL)
   Endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  P-Pc = (E h /Rc) (R-Rc)/Rc = ( E h / Rc^2 )  (R - RC)= RElas (R-RC)
!  PC: External Pressure ; RC: undeformed radius ; E: Young Modulus; h: wall thickness
!   Uni = (R-Rc).ni  (n:normal vector; Un: Total Displacement Vector)
!   Uni = (P-Pc)/RElas . ni
!  Incremental Version
!    k+1       k+1    k             k+1 k
!   Vni    =( Uni  - Uni ) /DT = ( P - P ) / RElas . ni / DT

!      k+1    k         k+1 k
!   ( Uni  - Uni ) = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Dni           = ( P - P ) / RElas . ni 
!     k+1               k+1 k
!    Vni           = ( P - P ) / RElas . ni / DT ==> Vn = dP/dt . RElas . ni
!     k+1               k+1                     k
!    Dni           -   P  /RElas  . ni     = - P  /RElas . ni 
!     k+1               k+1                     k
!    Vni           -   P /RElas  . ni /DT  = - P /RElas . ni / DT
!     k+1               k+1               k
!    Dni  . RElas  -   P    . ni     = - P  . ni 
!     k+1               k+1               k
!    Vni  . RElas  -   P    . ni /DT = - P  . ni / DT


 do NodR = 1, NodEL
       ipRow    = (NodR-1) * iDofT 
       ipRowP   =  ipRow   + NdimG1 
       do nsd   = 1, NdimG
        ipRowV  = ipRow + nsd
        ipRowX  = ipRowP+ nsd
        VecNi   = Vn(nsd) 
!        AE( ipRowV , ipRowV ) =   Penalty
!        AE( ipRowV , ipRowX ) = - Penalty / Dtm
!        BE( ipRowV )          = - Penalty*Sol0(ipRowX)  / Dtm

!        AE(ipRowX,ipRowX)  =   rElas * DA3
!        AE(ipRowX,ipRowP)  = - VecNi * DA3
!        BE( ipRowX )       = - VecNi * DA3 * PC  

        AE( ipRowV , ipRowV ) =   ( rElas*Dtm + rKv ) * DA3 
        AE( ipRowV , ipRowP ) = - VecNi * DA3 
!!        AE( ipRowV , ipRowV ) =   DA3 
!        AE( ipRowV , ipRowX ) =   -DA3/Dtm 
!        if(Incremental /= 0 ) then
          BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP)  + DA3*rKv *Sol0(ipRowV)
!!          BE( ipRowV )          =  DA3*Sol1(ipRowX) /Dtm
!        else
!          BE( ipRowV )          = - VecNi * DA3*Sol0(ipRowP) /Dtm + DA3*rKv/Dtm *Sol0(ipRowV)
!        endif

!        if(Incremental /= 0 ) then   
        !!!!!!! OJO al POMO
!            AE(ipRowX,ipRowX)     =   (rElas+rKv/Dtm) * DA3
            AE(ipRowX,ipRowX)     =   rElas * DA3
            AE(ipRowX,ipRowV)     =   rKv   * DA3
            AE(ipRowX,ipRowP)     = - VecNi * DA3
!            BE1        = rKv/Dtm * DA3 * Sol0(ipRowX)
            BE1        = rKv * DA3 * Sol0(ipRowV)
            BE2        = VecNi * DA3 * (-Sol0(ipRowP))
            BE(ipRowX) = BE1 + BE2  !Total Displacement Version

!            AE(ipRowX,ipRowX)     =  DA3
!            AE(ipRowX,ipRowV)     = -Dtm * DA3
!            BE( ipRowX )          = 0.0d0
!        else
!            AE(ipRowX,ipRowX)     =   rElas * DA3
!            AE(ipRowX,ipRowV)     =   rKv   * DA3
!            AE(ipRowX,ipRowP)     = - VecNi * DA3
!            BE( ipRowX )          = - VecNi * DA3 * PC  !Total Displacement Version
!        endif




       Enddo
 Enddo
!--------------------------

 Return
 End

