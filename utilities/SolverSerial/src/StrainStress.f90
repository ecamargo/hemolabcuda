!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine STRAIN1(NdimE,NodEl,UX,DF,GradSimU,EpSNorm)
 IMPLICIT REAL*8 (A-H,O-Z)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 DIMENSION GradSimU(NdimE,NdimE),GradU(NdimE,NdimE),UX(NdimE*NodEl),DF(NdimE,NodEl), &
           GradFi(NdimE)

  GradSimU = 0.0d0
  GradU    = 0.0d0
  EpSNorm  = 0.0d0
  Do nod=1, NodEL
     ipNodX = (nod-1)*NdimE
     Do nci = 1, NdimE				
        GradFi(nci)=DF(nci,nod)
     enddo

     Do nci=1, NdimE				
        ipNodXi = ipNodX + nci 
         Do ncj = 1, NdimE				
            GradU(nci,ncj) = GradU(nci,ncj)+UX(ipNodXi)*GradFi(ncj)
         enddo
      enddo
  enddo
        
  Do nci  = 1, NdimE				
   Do ncj = 1, NdimE			
			GradSij = 0.5d0*(GradU(nci,ncj)+GradU(ncj,nci))	
            GradSimU(nci,ncj) = GradSij 
			EpSNorm  = EpSNorm + GradSij*GradSij
   enddo
  enddo
	EpSNorm  = DSQRT(EpSNorm) 
Return
End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine STRAIN(NdimE,NodEl,UX,DF,GradSimU,divU)
 IMPLICIT REAL*8 (A-H,O-Z)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 DIMENSION GradSimU( NdimE + (NdimE*NdimE-NdimE)/2 ) , GradU(NdimE,NdimE), &
&          UX(NdimE*NodEl),DF(NdimE,NodEl),GradFi(NdimE)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Dado un campo de desplazamientos UX de dimension NdimE y dado en cada nodo, length=NdimE*Nodel
! Calcula en tensor de peque�as deformaciones Gradiente Simetrico de los desplazamientos (GradSimU) y
! divU > output > divergencia de UX
! GradSimU > output > Gradiente Simetrico de UX
! UX   > input  > campo de desplazamientos en cada nodo
! DF   > input  > Derivadas de las funciones de Forma DF(i, Nod) FiNod,i evaluadas en algun punto de gauss
! NdimE> input  > Dimension del espacio=grados de libertad de desplazamientos=# coordenadas espaciales
! NodEl> input  > # de Nodos del Elemento
! 
    GradSimU = 0.0d0
    GradU    = 0.0d0
    divU     = 0.0d0   
!    EpSNorm  = 0.0d0 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Do nod=1, NodEL
     ipNodX = (nod-1)*NdimE
     Do nci=1, NdimE				
        GradFi(nci)=DF(nci,nod)
     enddo

     Do nci=1, NdimE				
        ipNodXi = ipNodX + nci 
         Do ncj=1, NdimE				
            GradU(nci,ncj) = GradU(nci,ncj)+UX(ipNodXi)*GradFi(ncj)
         enddo
      enddo
  enddo
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     Do nci=1, NdimE				

         GradSimUnci   = GradU(nci,nci)
!         EpSNorm       = EpSNorm + GradSimUnci*GradSimUnci   
         GradSimU(nci) = GradSimUnci 
         divU          = divU + GradSimUnci   
         ipnci=(NdimE-nci)*(nci-1)
         Do ncj=nci+1, NdimE
             idif=ncj-nci
             ipncij = NdimE + ipnci*(idif+1) + idif				
             GradSimUipncij = GradU(nci,ncj) + GradU(ncj,nci)
!             EpSNorm = EpSNorm + .5d0 * GradSimUipncij*GradSimUipncij   
             GradSimU(ipncij) = GradSimUipncij
         enddo
     enddo
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Return
End
!%%%%%%
!%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine STRAIN_ENERGY(NdimE,GradSimU,divU,bK,G,STREn)
 IMPLICIT REAL*8 (A-H,O-Z)
 Parameter ( DOSTERCIOS = 2.0d0/3.0d0 )
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 DIMENSION GradSimU ( NdimE + (NdimE*NdimE-NdimE)/2 ), &
           SIGMA    ( NdimE + (NdimE*NdimE-NdimE)/2 ) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Dado un tensor de peque�as deformaciones Gradiente Simetrico de los desplazamientos (GradSimU),
! Calcula el producto contraido o escalar de la tension con la deformacion para un solido elastico isotropico
! divU > input > divergencia de UX (campo de desplazamientos en cada nodo)
! GradSimU > input > Gradiente Simetrico de UX
! bK,G   > input  > Modulo de compresibilidad y Elasticidad al corte, Solido elastico isotropico
! NdimE> input  > Dimension del espacio=grados de libertad de desplazamientos=# coordenadas espaciales
! STREn> ouput  > 2 STRainEnergy

    SIGMA = 0.0d0
    STREn = 0.0d0

    SIGMA= G*GradSimU    
    G23=DOSTERCIOS*G
     Do nci=1, NdimE				
         SIGMA(nci) = 2.0d0*SIGMA(nci) + (bK-G23) * divU
         STREn = STREn + GradSimU(nci)*SIGMA(nci)   
         ipnci=(NdimE-nci)*(nci-1)
         Do ncj=nci+1, NdimE
             idif=ncj-nci
             ipncij = NdimE + ipnci *(idif+1) + idif				
             STREn  = STREn + GradSimU(ipncij) * SIGMA(ipncij)   
         Enddo
     Enddo

Return
End
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Subroutine BSTRAIN ( NdimE, NodEl, DF, Bmat )
 IMPLICIT REAL*8 (A-H,O-Z)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 DIMENSION Bmat( (NdimE + (NdimE*NdimE-NdimE)/2),NdimE*NodEl ), &
&          DF(NdimE,NodEl),GradFi(NdimE)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Ensambla las Matrices de deformacion asociadas a cada Nodo, BiN = E( FiN ei )
! Bmat = secuencia de matrices BiN > tine tantas columnas como la cantidad de nodos por Ndim
! Bmat asociada al nodo N > son las columnas (N-1)*Ndim+1 hasta N*Ndim 
! Bmat > output
! DF   > input  > Derivadas de las funciones de Forma DF(i, Nod) FiNod,i evaluadas en algun punto de gauss
! NdimE> input  > Dimension del espacio=grados de libertad de desplazamientos=# coordenadas espaciales
! NodEl> input  > # de Nodos del Elemento

Bmat   = 0.0d0
Do nod = 1, NodEL

     ipNod = (nod-1)*NdimE

     Do nci=1, NdimE				
        GradFi(nci)=DF(nci,nod)
     enddo

     Do nci=1, NdimE				
        ipColi = ipNod + nci 
        Bmat(nci,ipColi) = GradFi(nci)
        ipnci=(NdimE-nci)*(nci-1)

        Do ncj=nci+1, NdimE
            idif=ncj-nci
            ipncij = NdimE + ipnci*(idif+1) + idif				
            ipColj = ipNod + ncj 

            Bmat ( ipncij,ipColi ) = GradFi ( ncj )
            Bmat ( ipncij,ipColj ) = GradFi ( nci )
        enddo
     enddo
enddo

Return
End
!%%%%%%
!%%%%%%

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SubRoutine ElasMatrix (E,P, C, G, Ndim,TP)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Implicit Real*8 (a-h, o-z)
Dimension C(Ndim, Ndim), G(Ndim, Ndim)
Logical TP
! See R.D Cook, Concepts & App. of F.E. Analysis,3thr ed. 1989, pg.21
! E young modulus, P poisson ratio, Ndim space dimensions, Tp: Plane stress id (boolean)
! C,G elastic matrices for variational formulation, consistent with the folowing assebler process

!             Do ndRow   = 1, Ndim
!                ipRowR  = ipRowP + ndRow  ! ipRowP:points to the position in AE to Prevuis DOf, idem ipColP
!                ipColR  = ipColP + ndRow  ! ipRowP = (NodRow-1)*iDofT +iShift(number of former Dofs) 
!                Do is = 1, NdimE
!                    AE(ipRowR, ipColR) = AE(ipRowR, ipColR) +          &
!                    dFi1i(is)*Gm(ndRow,is)*dFi1j(is)*DV
!                End Do
!                Do ndCol = ndRow + 1, Ndim
!                 ipRowC  = ipRowP + ndCol
!                 ipColC  = ipColP + ndCol
!                 AE(ipRowR,ipColC) = AE(ipRowR,ipColC) +               &
!     	           ( dFi1i(ndRow)*Cm(ndRow,ndCol)*dFi1j(ndCol) +       &
!                     dFi1i(ndCol)*Gm(ndRow,ndCol)*dFi1j(ndRow) ) * DV
!                 AE(ipRowC,ipColR) = AE(ipRowC,ipColR) +               &
!     	           ( dFi1i(ndCol)*Cm(ndCol,ndRow)*dFi1j(ndRow) +       &
!                     dFi1i(ndRow)*Gm(ndCol,ndRow)*dFi1j(ndCol) ) * DV
!                Enddo
!             Enddo


P1   = 1.0d0 + P
P2   = 1.0d0 - 2.d0*P
EP1  = E/P1
GG   = EP1/2.0d0

Select Case (Ndim)

 Case (3) 
  Cd      = EP1/P2
  ediag = P*Cd
  diag  = Cd - ediag	

  C(1,1) = diag 
  C(1,2) =ediag
  C(1,3) =ediag
  C(2,1) =ediag
  C(2,2) = diag
  C(2,3) =ediag
  C(3,1) =ediag
  C(3,2) =ediag
  C(3,3) = diag
	 
  G(1,1) = diag 
  G(1,2) = GG
  G(1,3) = GG
  G(2,1) = GG
  G(2,2) = diag
  G(2,3) = GG
  G(3,1) = GG
  G(3,2) = GG
  G(3,3) = diag
 Case (2)
 if (TP) then
! Tension Plana
  diag  = EP1/(1.d0-P)	
  ediag = P*diag
 else
! Deformacion Plana
  Cd      = EP1/P2
  ediag = P*Cd
  diag  = Cd - ediag	
 endif

  C(1,1) = diag 
  C(1,2) =ediag
  C(2,1) =ediag
  C(2,2) = diag
	 
  G(1,1) = diag 
  G(1,2) = GG
  G(2,1) = GG
  G(2,2) = diag

 End Select

Return
End