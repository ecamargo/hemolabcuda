C               TRASPONE UNA MATRIZ RALA 
C            ALMACENADA EN FORMATO COORDENDO         
C
C	IM	 : indices de las filas con no-ceros	 (in)
C     JM      : indices de las columnas con no-ceros    (in)  
C	NCol    : cantidad de columnas de la matriz       (in)
C	NoCeros : cantidad de no-ceros de la matriz       (in)
C	IMT	 : indices de las filas de la traspuesta   (out)
C	JMT	 : indices de las columnas de la traspuesta(out)
C   
      SUBROUTINE TrasCoor ( IM,JM,IMT,JMT,NCol,NoCeros )                  
      
      INTEGER Cont,Columna,Auxiliar
      DIMENSION IM(*),JM(*),IMT(*),JMT(*)
      
	CALL ASIGINT ( IMT, JM, NoCeros )
	CALL ASIGINT ( JMT, IM, NoCeros )
	Cont = 1

      DO 10 Columna = 1,NCol
	  DO 10 IPun = Cont,NoCeros
	    
		IF ( IMT(IPun) .EQ. Columna ) THEN

                    IMT(IPun) = IMT(Cont)
                    IMT(Cont) = Columna

                    Auxiliar = JMT(IPun)
                    JMT(IPun) = JMT(Cont)
                    JMT(Cont) = Auxiliar

                    Cont = Cont+1
             ENDIF

 10	CONTINUE

      RETURN
      END
C-----------------------------------------------------------------------
C               TRASPONE UNA MATRIZ RALA 
C            ALMACENADA EN FORMATO COMPACTO         
C
C     IM,JM   : matriz a trasponer               (in ) 
C     IMT,JMT : traspuesta de M                  (out)
C     iCols    : cantidad de Columnas            (in )
C     iRows    : cantidad de Filas               (in )
C   
      SUBROUTINE TrasComp ( IM,JM,IMT,JMT,iCols,iRows )
      
      DIMENSION IM(*),JM(*),IMT(*),JMT(*)

      CALL INIENT ( IMT,iCols+1,0 )

      IMB = IM(iRows+1) - 1
      DO 24 I = 1,IMB
	  J = JM(I) + 2
	  IF (J .LE. iCols+1) IMT(J) = IMT(J)+1
 24   CONTINUE

      IMT(1) = 1
      IMT(2) = 1
      DO 34 I = 3,iCols+1
 34     IMT(I) = IMT(I) + IMT(I-1)

 44   DO 64 I = 1,iRows       
        IMA = IM(I)
        IMB = IM(I+1)-1
        IF (IMB .LT. IMA) GO TO 64
        DO 54 JP = IMA,IMB
          J = JM(JP) + 1
          K = IMT(J)
          JMT(K) = I
 54       IMT(J) = K+1
 64   CONTINUE

C      Long = IMT (iCols+1) - 1

      RETURN
      END
C------------------------------------------------------------------------      
C                        ORDENA UNA MATRIZ RALA
C                    ALMACENADA EN FORMATO COMPACTO         
C
C     IM,JM   : matriz a ordenar                     (in-out)  
C     IP      : vector auxiliar. ( Se inicializa )
C     NodT    : cantidad de nodos                    (in)
C   
      SUBROUTINE Ordena ( IM,JM,IP,NodT )                  
      
      INTEGER Columna,PunCol,Fila
      DIMENSION IM(*),JM(*),IP(*)
      
      CALL INIENT ( IP, NodT, 0 )

      JP = 0 
      DO 30 Fila = 1,NodT

             IMA = IM  ( Fila )
             IMB = IM ( Fila+1 ) - 1    
             IF ( IMA .GT. IMB ) GO TO 30
      
             DO 10 PunCol = IMA,IMB
	              Columna = JM ( PunCol )
	              IP ( Columna ) = 1
 10          CONTINUE 
 
             DO 20 PunCol = 1,Nodt
	              IF ( IP (PunCol) .EQ. 0 ) GO TO 20
                    IP (PunCol)=0
	              JP = JP + 1
	              JM ( JP ) = PunCol 
 20          CONTINUE

 30   CONTINUE

      RETURN
      END
C-----------------------------------------------------------------------
C  ESTA SUBRUTINA TRASPONE UNA MATRIZ ESPARCIDA IA,JA,AN DE N FILAS Y M COLUMN.
C
C  DATOS
C  IA,JA,AN  MATRIZ ESPARCIDA DADA.
C  M  CANTIDAD DE COLUMNAS DE LA MATRIZ.
C  N  CANTIDAD DE FILAS DE LA MATRIZ.
C
C  RESULTADOS.
C  IAT,JAT,ANT  MATRIZ TRASPUESTA.
C
C  SUBPROGRAMAS=NINGUNO.
C  STOP=0
C
C-----------------------------------------------------------------------
      SUBROUTINE TRANUM(IA,JA,IAT,JAT,AN,ANT,M,N)
	implicit real*8 (a-h,o-z)
      DIMENSION IA(*),JA(*),IAT(*),JAT(*),AN(*),ANT(*)
      IF(M.LT.1.OR.N.LT.1)GOTO 360
      MH=M+1
      NH=N+1
      DO 20 I=2,MH
20    IAT(I)=0
      IAB=IA(NH)-1
      IF(IAB.LT.1)GOTO 360
      DO 30 I=1,IAB
      J=JA(I)+2
      IF(J.LE.MH)IAT(J)=IAT(J)+1
30    CONTINUE
C  EN IAT(J) QUEDA LA CANTIDAD DE ELEM. EN LA COLUMNA J-2,PARA J-2 DE 1 A M-1.
      IAT(1)=1
      IAT(2)=1
      IF(M.EQ.1)GOTO 10
      DO 40 I=3,MH
40    IAT(I)=IAT(I)+IAT(I-1)
C  EN IAT(I) QUEDA LA CANTIDAD DE ELEM.+1 HASTA LA COLUMNA I-2 INCLUSIVE,
C  PARA I-2 DESDE 1 HASTA M-1. ENTONCES IAT(I) ES EL PUESTO EN JAT DONDE
C  EMPIEZA LA COLUMNA I-1,PARA I-1 DESDE 1 HASTA M (PUES IAT(2) ES 1).
C
10    DO 60 I=1,N
      IAA=IA(I)
      IAB=IA(I+1)-1
      IF(IAB.GE.IAA)GOTO 70
      IF(IAB.EQ.IAA-1)GOTO 60
      GOTO 360
70    DO 50 JP=IAA,IAB
C  SE ENCUENTRA UN ELEMENTO EN LA COLUMNA J-1.
      J=JA(JP)+1
C  K ES EL PUESTO EN JAT DONDE EMPIEZA LA COLUMNA J-1.
      K=IAT(J)
C  SE INSCRIBE EL ELEMENTO ENCONTRADO Y SE CORRE IAT(J).
      JAT(K)=I
      ANT(K)=AN(JP)
50    IAT(J)=K+1
60    CONTINUE
      RETURN
C
360   WRITE(6,365)M,N,I,IAA,IAB
      STOP
365   FORMAT('TRANUM.ERROR',20I5)
      END
