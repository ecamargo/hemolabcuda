#include "aclista.h"

void *acListA::InsertSort(void *d)
    {
    acNodeList *curr = head,
               *prev = 0,
               *temp;

    register int res;

    if (!sizedados)     // lista nao inicializada
        {
        errorinit();
        return(0);
        }

    if (!cmp)           // nao possui funcao de comparacao
        {
        errorcmp();
        return(0);
        }

    if (!curr)    /* se nao tem lista, inicializa */
        {
        temp = (acNodeList *) new acNodeList;
        if (!temp)
            {
            erroralloc();
            return(0);
            }

        temp->dados = new char[sizedados];
        if (!temp->dados)
            {
            erroralloc();
            return(0);
            }

        temp->next  = 0;
        memcpy(temp->dados, d, sizedados); // copia dados
        head = temp;
        sizelist++;
        return(temp->dados);
        }

    /*
    ** Enquanto nao chegou ao final da lista, verifica
    ** se existe ou nao o dado em info
    */
    while (curr)
        {
        res = (*cmp)(d, curr->dados);

        if (res > 0)       /* nao achou posicao */
            {
            prev = curr;
            curr = curr->next;
            }

        else if (res < 0)  /* achou posicao */
            break;

        else               /* sao iguais -> retorna */
            return(0);
        }

    /*
    ** aloca no'
    */
    temp = (acNodeList *) new acNodeList;
    if (!temp)
        {
        erroralloc();
        return(0);
        }

    temp->dados = new char[sizedados];
    if (!temp->dados)
        {
        erroralloc();
        return(0);
        }

    memcpy(temp->dados, d, sizedados); // copia dados
    sizelist++;

    /*
    ** insere na posicao que achou
    */
    temp->next = curr;

    if (!prev)
        head = temp;
    else
        prev->next = temp;

    return(temp->dados);
    }



/*
** se nao achou, insere e retorna 1
** se achou, deleta e retorna 0
** coloca em ptr o ponteiro dos dados
** -1 -> erro
*/
int acListA::InsDelSort(void *d, void **ptr)
    {
    acNodeList *curr = head,
               *prev = 0,
               *temp;

    register int res;

    if (!sizedados)     // lista nao inicializada
        {
        errorinit();
        return(-1);
        }

    if (!cmp)           // nao possui funcao de comparacao
        {
        errorcmp();
        return(-1);
        }

    if (!curr)    /* se nao tem lista, inicializa */
        {
        temp = (acNodeList *) new acNodeList;
        if (!temp)
            {
            erroralloc();
            return(-1);
            }

        temp->dados = new char[sizedados];
        if (!temp->dados)
            {
            erroralloc();
            return(-1);
            }
        temp->next  = 0;
        memcpy(temp->dados, d, sizedados); // copia dados
        head = temp;
        sizelist++;
        *ptr = temp->dados;
        return(1);
        }

    /*
    ** Enquanto nao chegou ao final da lista, verifica
    ** se existe ou nao o dado em info
    */
    while (curr)
        {
        res = (*cmp)(d, curr->dados);

        if (res > 0)       /* nao achou posicao */
            {
            prev = curr;
            curr = curr->next;
            }

        else if (res < 0)  /* achou posicao */
            break;

        else               // sao iguais -> deleta e retorna
            {
            temp = curr;

            if (prev)      /* se nao e' o primeiro */
                prev->next = curr->next;

            else           /* e' o primeiro */
                head = curr->next;


            *ptr = temp->dados;
            delete (char *)temp->dados;
            delete (char *)temp;
            sizelist--;
            if (sizelist == 0)
                head = 0;
            return(0);
            }
        }

    /*
    ** aloca no'
    */
    temp = (acNodeList *) new acNodeList;
    if (!temp)
        {
        erroralloc();
        return(-1);
        }

    temp->dados = new char[sizedados];
    if (!temp->dados)
        {
        erroralloc();
        return(-1);
        }

    memcpy(temp->dados, d, sizedados); // copia dados
    sizelist++;

    /*
    ** insere na posicao que achou
    */
    temp->next = curr;

    if (!prev)
        head = temp;
    else
        prev->next = temp;

    *ptr = temp->dados;
    return(1);
    }
