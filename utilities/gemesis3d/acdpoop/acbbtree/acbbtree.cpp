/* 16:50  2/11/1994 */

/*
** Metodos da classe acBBTree - E.A.Dari - 11/94
*/

#include "acbbtree.h"


void acBBTree::cnivel(long nivel, acNodeBBTree *t)
    {
    nivel++;
    if (nivel > NumNiveis)
	NumNiveis = nivel;

    if (t->l)
	cnivel(nivel, t->l);
    if (t->r)
	cnivel(nivel, t->r);
    }

void acBBTree::setniv(long nivel, acNodeBBTree *t)
    {
    vecniv[nivel]++;
    nivel++;
    if (nivel > NumNiveis)
	NumNiveis = nivel;

    if (t->l)
	setniv(nivel, t->l);
    if (t->r)
	setniv(nivel, t->r);
    }

int acBBTree::PrintNivels(FILE *fp)
    {
    long i;

    NumNiveis = 0;
    cnivel(0, head);
    vecniv = (long *) mCalloc(NumNiveis, sizeof(long));
    if (!vecniv)
	{
	erroralloc();
	return(0);
	}

    setniv(0, head);

    for (i = 0; i < NumNiveis; i++)
	fprintf(fp, "Nivel: %ld -> %ld\n", i + 1, vecniv[i]);

    mFree((char *) vecniv);
    return(1);
    }

void acBBTree::setvec(void **v, long &i, acNodeBBTree *t)
    {
    if (t->l) setvec(v, i, t->l);
       v[i++] = t->dados;
    if (t->r) setvec(v, i, t->r);
    delete t;
    }

acNodeBBTree *acBBTree::Rebuild (void **v, long n)
    {
    acNodeBBTree *t;
    long m;
    t = (acNodeBBTree *) new acNodeBBTree;
    if (!t)
	{
	erroralloc();
	return(NULL);
	}
    m = n/2;
    if (m > 0) t->l = Rebuild(v,m);
    else       t->l = NULL;
    t->dados = v[m];
    m++;
    if (m < n) t->r = Rebuild(v+m, n-m);
    else       t->r = NULL;
    return t;
    }

void acBBTree::Balance()
    {
    void * *v;
    long i, n;
    if (!head) return;
    n = Size();
    v = (void * *) mMalloc(n * sizeof(void *));
    if (!v)
	erroralloc();

    i = 0;
    setvec(v, i, head);
    head = Rebuild(v, n);
    mFree((char *) v);
    }

void acBBTree::Reorder()
    {
    void * *v;
    long i, n;
  	int res;
    if (!head) return;
    n = Size();
    v = (void * *) mMalloc(n * sizeof(void *));
    if (!v)
	erroralloc();

    i = 0;
    setvec(v, i, head);

    head = NULL;
    for (i=0; i<n; i++)
	{
	acNodeBBTree *curr=head, *prev=head, *temp;
	temp = (acNodeBBTree *) new acNodeBBTree;
	if (!temp)
	    erroralloc();

	temp->dados = v[i];
	temp->l = temp->r = NULL;
	if (!head)
	    head = temp;
	else
	    {
	    while (curr)
		{
		prev = curr;
		res = (*cmp)(v[i], curr->dados);
		if (res < 0)
		    curr = curr->l;
		else
		    curr = curr->r;
		}
	    if (res<0)
		prev->l = temp;
	    else
		prev->r = temp;
	    }
	}
    mFree((char *) v);
    }

