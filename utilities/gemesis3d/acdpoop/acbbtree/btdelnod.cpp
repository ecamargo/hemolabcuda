/*
**
*/

#include <iostream>
#include "acbbtree.h"

int acBBTree::DelNode(void *info)
    {
    acNodeBBTree *newhead;
    int res;

    if (!head) return (0);

    res = BDelNode(info, head, &newhead);

    head = newhead;
    if (res == 1) return 0;
    return 1;
    }

int acBBTree::BDelNode(void *info, acNodeBBTree *head,
				   acNodeBBTree **newhead)
    {
    acNodeBBTree *nhead;
    int res;

    if (!head)
	{
	*newhead = head;
	return 1;
	}

    res = (*cmp)(info, head->dados);

    if (res == 0)	// FOUND !
	{
	if (head->l && head->r)		// Both sons exist
	    {
	    acNodeBBTree *xx;
	    void *aux;
	    if (head->balance == 1)
		{
		xx = head->r;
		while (xx->l)
		    xx = xx->l;
		aux = head->dados;
		head->dados = xx->dados;
		xx->dados = aux;
		res =  1;	// ATTENTION, CONIINUES BELOW
		}
	    else
		{
		xx = head->l;
		while (xx->r)
		    xx = xx->r;
		aux = head->dados;
		head->dados = xx->dados;
		xx->dados = aux;
		res = -1;	// ATTENTION, CONIINUES BELOW
		}
	    }
	else	// FOUND and at least one son is NULL
	    {
	    if (head->l) // The right son is NULL
		{
		*newhead = head->l;
		delete (char *)head->dados;
		delete (char *)head;
		return (-1);	// Depth decreases
		}
	    else 	 // The left son is NULL
		{
		*newhead = head->r;
		delete (char *)head->dados;
		delete (char *)head;
		return (-1);
		}
	    }
	}

// Continues search, NOT FOUND or after swapping
    if (res < 0)	// Turns to the left
	{
	res = BDelNode(info, head->l, &nhead);
	if (res == 1)
	    return 1;	// Not Found
	head->l = nhead;
	if (res == 0)	// Deleted, no change in depth
	    {
	    *newhead = head;
	    return 0;
	    }
	// res == -1	(Deleted, depth decreased)
	if (head->balance == 1) // Needs rebalancing
	    {
	    res = RebalSubTree(head, &nhead);
	    *newhead = nhead;
	    return (res);
	    }
	else if (head->balance == -1) // Tree gets more balanced, depth decrease
	    {
	    head->balance = 0;
	    *newhead = head;
	    return -1;
	    }
	else	// Tree gets debalanced, no change in depth
	    {
	    head->balance = 1;
	    *newhead = head;
	    return 0;
	    }
	}
//  else       (Turn to the right)
    res = BDelNode(info, head->r, &nhead);
    if (res == 1)
	return 1;	// Not Found
    head->r = nhead;
    if (res == 0)	// Deleted, no change in depth
	{
	*newhead = head;
	return 0;
	}
    // res == -1	(Deleted, depth decreased)
    if (head->balance == -1) // Needs rebalancing
	{
	res = RebalSubTree(head, &nhead);
	*newhead = nhead;
	return (res);
	}
    else if (head->balance == 1) // Tree gets more balanced, depth decrease
	{
	head->balance = 0;
	*newhead = head;
	return -1;
	}
    else	// Tree gets debalanced, no change in depth
	{
	head->balance = -1;
	*newhead = head;
	return 0;
	}
    }

int acBBTree::RebalSubTree(acNodeBBTree *a, acNodeBBTree **newhead)
    {
    acNodeBBTree *b;
    if (a->balance == 1)
	{
	b = a->r;
	if (b->balance == 0)
	    {
	    a->r = b->l;
	    b->l = a;
	    a->balance = 1;
	    b->balance = -1;
	    *newhead = b;
	    return 0;
	    }
	if (b->balance == 1)
	    {
	    a->r = b->l;
	    b->l = a;
	    a->balance = 0;
	    b->balance = 0;
	    *newhead = b;
	    return -1;
	    }
	else
	    {
	    acNodeBBTree *x;
	    x = b->l;
	    a->r = x->l;
	    b->l = x->r;
	    x->l = a;
	    x->r = b;
	    if (x->balance == -1)
		{
		a->balance = 0;
		b->balance = 1;
		}
	    else if (x->balance == 0)
		{
		a->balance = 0;
		b->balance = 0;
		}
	    else //  x->balance == 1
		{
		a->balance = -1;
		b->balance = 0;
		}
	    x->balance = 0;
	    *newhead = x;
	    return -1;
	    }
	}
    else //  a->balance == -1
	{
	b = a->l;
	if (b->balance == 0)
	    {
	    a->l = b->r;
	    b->r = a;
	    a->balance = -1;
	    b->balance = 1;
	    *newhead = b;
	    return 0;
	    }
	if (b->balance == -1)
	    {
	    a->l = b->r;
	    b->r = a;
	    a->balance = 0;
	    b->balance = 0;
	    *newhead = b;
	    return -1;
	    }
	else // b->balance == 1
	    {
	    acNodeBBTree *x;
	    x = b->r;
	    a->l = x->r;
	    b->r = x->l;
	    x->r = a;
	    x->l = b;
	    if (x->balance == 1)
		{
		a->balance = 0;
		b->balance = -1;
		}
	    else if (x->balance == 0)
		{
		a->balance = 0;
		b->balance = 0;
		}
	    else //  x->balance == -1
		{
		a->balance = 1;
		b->balance = 0;
		}
	    x->balance = 0;
	    *newhead = x;
	    return -1;
	    }
	}
    }

