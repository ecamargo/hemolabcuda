#include "acbbtree.h"


int acBBTree::InsDel(void *val)
    {
    acNodeBBTree *curr, *temp, *pre_rebal, *rebal_p, *next;
    int res, res_rebal;
    BOOL inserted;

    if (!cmp)
	{
	errorcmp();
	return(-1);
	}
    if (!sizedados)
	{
	errorinit();
	return(-1);
	}

    // cria um novo no'
    temp = (acNodeBBTree *) new acNodeBBTree;
    if (!temp)
	{
	erroralloc();
	return(-1);
	}

    temp->dados = new char[sizedados];
    if (!temp->dados)
	{
	erroralloc();
	return(-1);
	}

    temp->l = temp->r  = 0;
    temp->balance = 0;
    memcpy(temp->dados, val, sizedados); // copia dados

    if (!head)
	{
	head = temp;
	return 1;
	}

    pre_rebal = NULL;
    rebal_p = head;
    curr = head;

    inserted = FALSE;
    while (!inserted)
	{
	res = (*cmp)(temp->dados, curr->dados);
	if (res == 0)
	    {
	    delete (char *)temp->dados;
	    delete (char *)temp;

        DelNode(val);

	    return 0;
	    }
	if (res < 0) // Move to the Left
	    {
	    next = curr->l;
	    if (!next)
		{
		curr->l = temp;
		inserted = TRUE;
		}
	    }
	else // Move to the Right
	    {
	    next = curr->r;
	    if (!next)
		{
		curr->r = temp;
		inserted = TRUE;
		}
	    }
	if (!inserted)
	    {
	    if (next->balance != 0)
		{
		pre_rebal = curr;
		rebal_p = next;
		}
	    curr = next;
	    }
	}

//	Adjust balance factors (only from rebal_p)
    res_rebal = ((*cmp)(temp->dados, rebal_p->dados) < 0) ? -1 : 1;
    if (res_rebal < 0) curr = next = rebal_p->l;
    else               curr = next = rebal_p->r;

    while (curr != temp)
	{
	res = (*cmp)(temp->dados, curr->dados);
	if (res < 0)
	    {
	    curr->balance = -1;
	    curr = curr->l;
	    }
	else
	    {
	    curr->balance =  1;
	    curr = curr->r;
	    }
	}
//	Balancing
    if (rebal_p->balance == 0)
	{
	rebal_p->balance = res_rebal;
	return 1;
	}
    if (rebal_p->balance != res_rebal)
	{
	rebal_p->balance = 0;
	return 1;
	}
// Needs rebalance
    if (next->balance == res_rebal)	// Single rotation
	{
	if (res_rebal == -1)
	    {
	    rebal_p->l = next->r;
	    next->r = rebal_p;
	    }
	else
	    {
	    rebal_p->r = next->l;
	    next->l = rebal_p;
	    }
	rebal_p->balance = next->balance = 0;
	curr = next;
	}
    else if (next->balance == -res_rebal) // Double rotation
	{
	if (res_rebal == -1)
	    {
	    curr = next->r;
	    next->r    = curr->l;
	    rebal_p->l = curr->r;
	    curr->l = next;
	    curr->r = rebal_p;
	    if      (curr->balance == -1)
		{
		next->balance    = 0;
		rebal_p->balance = 1;
		}
	    else if (curr->balance ==  1)
		{
		next->balance    =-1;
		rebal_p->balance = 0;
		}
	    else
		{
		next->balance    = 0;
		rebal_p->balance = 0;
		}
	    curr->balance = 0;
	    }
	else
	    {
	    curr = next->l;
	    next->l    = curr->r;
	    rebal_p->r = curr->l;
	    curr->r = next;
	    curr->l = rebal_p;
	    if      (curr->balance == -1)
		{
		rebal_p->balance = 0;
		next->balance    = 1;
		}
	    else if (curr->balance ==  1)
		{
		rebal_p->balance =-1;
		next->balance    = 0;
		}
	    else
		{
		rebal_p->balance = 0;
		next->balance    = 0;
		}
	    curr->balance = 0;
	    }
	}
    if (!pre_rebal)
	head = curr;
    else
	{
	if (pre_rebal->r == rebal_p)
	    pre_rebal->r = curr;
	else
	    pre_rebal->l = curr;
	}
    return 1;
    }

