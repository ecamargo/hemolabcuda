#include "acbbtree.h"

void acBBTree::Save(FILE *fp)
    {
    savetree(fp, head);
    }

void acBBTree::savetree(FILE *fp, acNodeBBTree *t)
    {
    if (t)
        {
        savetree(fp, t->l);
        fwrite(t->dados, sizedados, 1, fp);
        savetree(fp, t->r);
        }
    }
