/*
** procura uma chave e retorna um pointer para os dados
**
** Obs.: diferente do algoritmo original
**
** Adaptado de Programs and Data Structures in C
** Leendert Ammeraal - John Wiley & Sons - pg 115
**
** Salgado - 2/90
*/
#include "acbbtree.h"
void *acBBTree::Search(void *x)
    {
    acNodeBBTree *qq;
    int res;

    qq = head;
    while (qq && (res = (*cmp)(x, qq->dados)) != 0)
        qq = (res < 1) ? qq->l : qq->r;
    if (!qq)
        return 0;
    return(qq->dados);
    }
