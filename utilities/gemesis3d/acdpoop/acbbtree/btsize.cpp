#include "acbbtree.h"
int acBBTree::Size()
    {
    acNodeBBTree *aux = head;

    return(size_tree(aux));
    }

int acBBTree::size_tree(acNodeBBTree *t)
    {
    if (t)
        return(1 + size_tree(t->l) + size_tree(t->r));
    else
        return(0);
    }
