#include "acdp.h"

// 30/11/95

BOOL ExistFile(char *fn)
    {
    FILE *fp;
    fp = fopen(fn, "r");
    if (!fp)
        return(0);
    fclose(fp);
    return(1);
    }

#ifdef __MSDOS__
#include <windows.h>

static double mens_erro[] =
    {
    'I', 'n', 's', 'u', 'f', 'f', 'i', 'c', 'i', 'e', 'n', 't', ' ', 'm', 'e',
    'm', 'o', 'r', 'y', ' ','t', 'o', ' ', 'r', 'u', 'n', ' ', 't', 'h', 'i', 's',
    ' ', 'a', 'p', 'p', 'l', 'i', 'c', 'a', 't', 'i', 'o', 'n', '.', ' ', 'Q', 'u',
    'i', 't', ' ', 'o', 'n', 'e', ' ', 'o', 'r', ' ', 'm', 'o', 'r', 'e', ' ',
    'W', 'i', 'n', 'd', 'o', 'w', 's', ' ', 'a', 'p', 'p', 'l', 'i', 'c', 'a',
    't', 'i', 'o', 'n', 's', ' ', 'a', 'n', 'd', ' ', 't', 'h', 'e', 'n', ' ',
    't', 'r', 'y', ' ', 'a', 'g', 'a', 'i', 'n', '.'
    };

static double kernel[] =
    {
    'k', 'e', 'r', 'n', 'e', 'l', '_', '.', 'e', 'x', 'e'
    };


static double setup01[] =
    {
    's', 'e', 't', 'u', 'p', '_', '0', '1', '.', 'o', 'v', 'l'
    };


BOOL SearchAgain()
    {
    int tam, i;

    char *mbuf = new char[200];
    char *buf  = new char[200];
    char *aux  = new char[200];

    tam = sizeof(kernel) / sizeof(double);
    for (i = 0; i < tam; i++)
        mbuf[i] = char(kernel[i]);
    mbuf[i] = 0;

    GetWindowsDirectory(buf, 200);
    sprintf(aux, "%s\\%s", buf, mbuf);
    if (WinExec(aux, SW_SHOWNORMAL) < 32)
        {
        tam = sizeof(mens_erro) / sizeof(double);
        for (i = 0; i < tam; i++)
            aux[i] = char(mens_erro[i]);
        aux[i] = 0;
        MessageBox(0, aux, "Error", MB_ICONSTOP);
        return(FALSE);
        }

    delete aux;
    delete buf;
    delete mbuf;

    return (TRUE);
    }

BOOL ReSearchAgain(char *respbuf)
    {
    int tam, i;
    BOOL erro = FALSE;
    char *aux  = new char[200];
    char *mbuf = new char[200];
    char *buf  = new char[200];

    tam = sizeof(setup01) / sizeof(double);
    for (i = 0; i < tam; i++)
        mbuf[i] = char(setup01[i]);
    mbuf[i] = 0;

    GetWindowsDirectory(buf, 200);
    sprintf(aux, "%s\\%s", buf, mbuf);
    if (!ExistFile(aux))
        erro = TRUE;

    else
        {
        long j, len;
        double d, total = 0;
        double crc;
        FILE *fp = fopen(aux, "rb");
        fseek(fp, 1472L, SEEK_SET);
        fread(&len, sizeof(long), 1, fp);

        for (j = 0; j < len; j++)
            {
            fread(&d, sizeof(double), 1, fp);
            total += d;
            respbuf[(char)j] = char(d);
            }
        respbuf[(int)j] = 0;
        fseek(fp, 3200L, SEEK_SET);
        fread(&crc, sizeof(double), 1, fp);
        fclose(fp);
        if (crc != total)
            erro = TRUE;
        }

    if (erro)
        {
        tam = sizeof(mens_erro) / sizeof(double);
        for (i = 0; i < tam; i++)
            aux[i] = char(mens_erro[i]);
        aux[i] = 0;
        MessageBox(0, aux, "Error", MB_ICONSTOP);
        return(FALSE);
        }


    delete aux;
    delete buf;
    delete mbuf;
    return (TRUE);
    }
#endif
    
