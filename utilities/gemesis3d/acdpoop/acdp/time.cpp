/* 11-10-2005 */

#include "acdp.h"
#include <string.h>
#include <stdlib.h>
#include "acstack.h"
#include "acqueue.h"
#include "acdptype.h"

#if defined(__LINUX__)
extern "C"
    {
    long tmpcpu_(void);
    };
#endif

static acStack *stack;
static acQueue *queue;
static double  HoraInic;

struct EtapaInfo
    {
    char   etapa[81]; // nome da etapa
    double tempo;     // tempo gasto na etapa
    };

void InitTime()
    {
    int   h, m, s, c, dia, mes, ano;
    FILE  *lw;
    static char *nomefnc = "InitTime";

    TraceOn(nomefnc);

    // pega a hora e a data
    GetTime(&h, &m, &s, &c);
    GetDate(&dia, &mes, &ano);

    lw = GetMensFile();    // ptr para arquivo de mensagens

    // inicializa lista e pilha
    stack = new acStack(sizeof(double));
    queue = new acQueue(sizeof(EtapaInfo));

    // inicializa os tempos
    HoraInic = (double) c + s * 100. + m * 6000. + h * 360000.;

    // imprime mensagem do dia e da hora no arquivo de mensagens
    fprintf(lw, "\n");
    PrtLine(lw, '-', 79);
    fprintf(lw, "|\n");
    fprintf(lw, "| Time: %d:%d:%d\n", h, m, s);
    fprintf(lw, "|\n");
    fprintf(lw, "| Date: %d/%d/%d\n", dia, mes, ano);
    fprintf(lw, "|\n");
    PrtLine(lw, '-', 79);
    fprintf(lw, "\n");
    fflush(lw);

    TraceOff(nomefnc);
    }

void TimeOn()
    {
#if !defined(__UNIX__)
    int h, m, s, c;
#endif

    double novo;
    static char *nomefnc = "TimOn";

    TraceOn(nomefnc);

#if !defined(__UNIX__)
    GetTime(&h, &m, &s, &c);
    novo = (double) c + s * 100. + m * 6000. + h * 360000.;
#else
    novo = (double) tmpcpu_();
#endif

    stack->Push(&novo);
    TraceOff(nomefnc);
    }


void TimeOff(char *etapa, int codigo)
    {
    int       h, m, s, c;
    double    fimetap, totcpu, totetap;
    FILE      *lw;
    EtapaInfo etinfo;
    BOOL   imprime = FALSE, guarda  = FALSE;
    double    stempo;
    static char *nomefnc = "TimeOff";

    TraceOn(nomefnc);

    GetTime(&h, &m, &s, &c);   // pega o tempo
    lw = GetMensFile();        // pointer para arquivo de mensagens
    stack->Pop(&stempo);       // pega tempo no topo da pilha

    // calcula os tempos
    fimetap = (double) c + s * 100. + m * 6000. + h * 360000.;
    totcpu  = (double) fimetap - HoraInic;
    totcpu  = totcpu / 100.0;
#if !defined(__UNIX__)
    totetap = (double) fimetap - stempo;
#else
    totetap = (double) tmpcpu_();
    totetap = totetap - stempo;
#endif
    totetap = totetap / 100.0;

    switch (codigo)
        {
    case 1:      // e' impresso, porem nao e' armazenado
        imprime = TRUE;
        guarda  = FALSE;
        break;
    case 2:      // nao e' impresso, porem e' armazenado
        imprime = FALSE;
        guarda  = TRUE;
        break;
    case 3:      // e' impresso e armazenado
        imprime = TRUE;
        guarda  = TRUE;
        break;
    default:
        break;
        }

    // imprime o nome da etapa e os tempos de CPU e da etapa
    if (imprime)
        {
        fprintf(lw, "\n\n");
        PrtLine(lw, '-', 79);
        fprintf(lw, "|\n");
        fprintf(lw, "| Time-Step Name ....: %s\n", etapa);
        fprintf(lw, "| \n");
        fprintf(lw, "| CPU Time ..........: %-15.3lf seg\n",
                totetap);
        fprintf(lw, "| \n");
        fprintf(lw, "| Total CPU Time ....: %-15.3lf seg\n",
                totcpu);
        fprintf(lw, "|\n");
        PrtLine(lw, '-', 79);
        fprintf(lw, "\n\n");
        fflush(lw);
        }

    // armazena a etapa na lista de etapas
    if (guarda)
        {
        StrCpy(etinfo.etapa, etapa);
        etinfo.tempo = totetap;
        queue->Push(&etinfo);
        }

    TraceOff(nomefnc);
    }

void ListTimes()
    {
    static char *nome = "ListTimes";
    int h, m, s, c;
    FILE *lw;
    EtapaInfo etinfo;
    double totcpu, horaat;
    char *mes1 = "Time-Step Name",
         *mes2 = "CPU Time (secs)";

    TraceOn(nome);

    // calcula o tempo total de CPU
    GetTime(&h, &m, &s, &c);    // pega o tempo
    horaat = (double) c + s * 100. + m * 6000. + h * 360000.;
    totcpu = (double) horaat - HoraInic;
    totcpu = totcpu / 100.0;

    lw = GetMensFile();  // pointer para arquivo de mensagens

    // imprime os tempos armazenados
    fprintf(lw, "\n\n");
    PrtLine(lw, '-', 79);
    fprintf(lw, "|\n| Time-Steps\n| ");
    PrtLine(lw, '-', 41);
    fprintf(lw, "|\n| Total CPU Time (secs) = %-15.3lf\n", totcpu);
    fprintf(lw, "|\n| %-40s     %s\n", mes1, mes2);
    while (queue->Push(&etinfo))
        {
        fprintf(lw, "| %-40s     %-15.3lf\n",  etinfo.etapa, etinfo.tempo);
        }
    fprintf(lw, "|\n");
    PrtLine(lw, '-', 79);
    fprintf(lw, "\n\n");
    fflush(lw);
    TraceOff(nome);
    }

