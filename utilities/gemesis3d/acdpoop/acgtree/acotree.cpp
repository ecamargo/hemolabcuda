#include "acotree.h"

acOTree::acOTree(acLimit3 &bbox, int sizeObjects)
    {
    double dx, dy, dz, size;
    acPoint3 cc;
    SizeObjects = sizeObjects;
    cc.x = 0.5 * (bbox.xmax + bbox.xmin);
    cc.y = 0.5 * (bbox.ymax + bbox.ymin);
    cc.z = 0.5 * (bbox.zmax + bbox.zmin);
    dx = bbox.xmax - bbox.xmin;
    dy = bbox.ymax - bbox.ymin;
    dz = bbox.zmax - bbox.zmin;
    size =        ((dx > dy)   ? dx : dy);
    size = 1.01 * ((dz > size) ? dz : size);
    root = CreateNodeOTree(cc, size);
    }

acNodeOTree *acOTree::CreateNodeOTree (acPoint3 &cc, double size)
    {
    acNodeOTree *temp = (acNodeOTree *) mMalloc(sizeof(acNodeOTree));
    if (!temp)
    Error(FATAL_ERROR, 1, "Not enough memory in CreateNodeOTree");
    temp->center = cc;
    temp->size = size;
    for (int i = 0; i < 8; i++)
    temp->ptr[i] = NULL;
    temp->list.SetListData(SizeObjects);
    return (temp);
    }

void acOTree::FreeOTree (acNodeOTree *ntree)
    {
    if (ntree->ptr[0])
    for (int i=0; i<8; i++)
        FreeOTree(ntree->ptr[i]);
    else
    ntree->list.Clear();
    mFree(ntree);
    }

void acOTree::AdjustSize(acPoint3 &pp, double size)
    {
    int oct;
    acNodeOTree *ntree;

    ntree = root;
    while (ntree->ptr[0])
        {
        oct = ntree->Octant (pp);
        ntree = ntree->ptr[oct];
        }
    while (ntree->size > size)
        {
        for (int i = 0; i < 8; i++)
            {
            acPoint3 aux = ntree->CenterLeaf(i);
            ntree->ptr[i] = CreateNodeOTree (aux, 0.5*ntree->size);
            }
        oct = ntree->Octant (pp);
        ntree = ntree->ptr[oct];
        }
    }

void acOTree::BuildListOct(acNodeOTree *ntree, acLimit3 &bbox, void *data)
    {
    acLimit3 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;
    volntree.zmin = ntree->center.z - 0.5 * ntree->size;
    volntree.zmax = ntree->center.z + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
    volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax &&
    volntree.zmax >= bbox.zmin && volntree.zmin <= bbox.zmax)
        {
        if (!ntree->ptr[0])
            ntree->list.InsertFirst(data);
        else
            for (int i = 0; i < 8; i++)
                BuildListOct(ntree->ptr[i], bbox, data);
        }
    }

acNodeList *acOTree::GetListHead (acPoint3 &pp)
    {
    int oct;
    acNodeOTree *ntree;

    ntree = root;
    while (ntree->ptr[0])
        {
        oct = ntree->Octant (pp);
        ntree = ntree->ptr[oct];
        }

    return ntree->list.GetHead();
    }

void acOTree::LoadList (acLimit3 bbox, acList *List)
    {
    List->Clear();
    List->SetListData(SizeObjects);
    LoadListOct(root, bbox, List);
    }

void acOTree::LoadListOct (acNodeOTree *ntree, acLimit3 bbox, acList *List)
    {
    acLimit3 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;
    volntree.zmin = ntree->center.z - 0.5 * ntree->size;
    volntree.zmax = ntree->center.z + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
    volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax &&
    volntree.zmax >= bbox.zmin && volntree.zmin <= bbox.zmax)
        {
        if (!ntree->ptr[0])
            {
            acNodeList *nl = ntree->list.GetHead();
            while (nl)
                {
                List->InsertFirst(nl->GetDados());
                nl = nl->next;
                }
            }
        else
            for (int i = 0; i < 8; i++)
                LoadListOct(ntree->ptr[i], bbox, List);
        }
    }

void acOTree::LoadListSort (acLimit3 bbox, acList *List)
    {
    LoadListSortOct(root, bbox, List);
    }

void acOTree::LoadListSortOct (acNodeOTree *ntree, acLimit3 bbox, acList *List)
    {
    acLimit3 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;
    volntree.zmin = ntree->center.z - 0.5 * ntree->size;
    volntree.zmax = ntree->center.z + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
    volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax &&
    volntree.zmax >= bbox.zmin && volntree.zmin <= bbox.zmax)
        {
        if (!ntree->ptr[0])
            {
            acNodeList *nl = ntree->list.GetHead();
            while (nl)
                {
                List->InsertSort(nl->GetDados());
                nl = nl->next;
                }
            }
        else
            for (int i = 0; i < 8; i++)
                LoadListSortOct(ntree->ptr[i], bbox, List);
        }
    }

