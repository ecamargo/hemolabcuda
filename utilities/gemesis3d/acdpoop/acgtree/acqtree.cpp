#include "acqtree.h"

acQTree::acQTree(acLimit2 &bbox, int sizeObjects, int (*cmpf) (void *, void *))
    {
    double dx, dy, size;
    acPoint2 cc;
    SizeObjects = sizeObjects;
    cc.x = 0.5 * (bbox.xmax + bbox.xmin);
    cc.y = 0.5 * (bbox.ymax + bbox.ymin);
    dx = bbox.xmax - bbox.xmin;
    dy = bbox.ymax - bbox.ymin;
    size = 1.01 * ((dx > dy) ? dx : dy);
    root = CreateNodeQTree(cc, size);
    CmpFunc = cmpf;
    }

acNodeQTree *acQTree::CreateNodeQTree (acPoint2 &cc, double size)
    {
    acNodeQTree *temp = (acNodeQTree *) mMalloc(sizeof(acNodeQTree));
    if (!temp)
	Error(FATAL_ERROR, 1, "Not enough memory in CreateNodeQTree");
    temp->center = cc;
    temp->size = size;
    for (int i = 0; i < 4; i++)
	temp->ptr[i] = NULL;
    temp->list.SetListData(SizeObjects, CmpFunc);
    return (temp);
    }

void acQTree::FreeQTree (acNodeQTree *ntree)
    {
    if (ntree->ptr[0])
	for (int i=0; i<4; i++)
	    FreeQTree(ntree->ptr[i]);
    else
	ntree->list.Clear();
    mFree(ntree);
    }

void acQTree::AdjustSize(acPoint2 &pp, double size)
    {
    int quad;
    acNodeQTree *ntree;

    ntree = root;
    while (ntree->ptr[0])
	{
	quad = ntree->Quadrant (pp);
	ntree = ntree->ptr[quad];
	}
    while (ntree->size > size)
	{
	for (int i = 0; i < 4; i++)
        {
        acPoint2 aux = ntree->CenterLeaf(i);
        ntree->ptr[i] = CreateNodeQTree (aux, 0.5*ntree->size);
        }
	quad = ntree->Quadrant (pp);
	ntree = ntree->ptr[quad];
	}
    }
/*
int acQTree::Quadrant (acPoint2 &pp, acPoint2 &center)
    {
    int quad = 0;
    if (pp.x > center.x)
	quad += 1;
    if (pp.y > center.y)
	quad += 2;
    return quad;
    }

acPoint2 acQTree::CenterLeaf (acPoint2 &center, double size, int q)
    {
    acPoint2 NewCenter;
    if (q > 1)
	{
	NewCenter.y = center.y + size / 4.0;
	q -= 2;
	}
    else
	NewCenter.y = center.y - size / 4.0;
    if (q > 0)
	NewCenter.x = center.x + size / 4.0;
    else
	NewCenter.x = center.x - size / 4.0;
    return NewCenter;
    }
*/
void acQTree::BuildListQuad(acNodeQTree *ntree, acLimit2 &bbox, void *data)
    {
    acLimit2 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
	volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax)
	{
	if (!ntree->ptr[0])
	    ntree->list.InsertFirst(data);
	else
	    for (int i = 0; i < 4; i++)
		BuildListQuad(ntree->ptr[i], bbox, data);
	}
    }

acNodeList *acQTree::GetListHead (acPoint2 &pp)
    {
    int quad;
    acNodeQTree *ntree;

    ntree = root;
    while (ntree->ptr[0])
	{
	quad = ntree->Quadrant (pp);
	ntree = ntree->ptr[quad];
	}

    return ntree->list.GetHead();
    }

void acQTree::LoadList (acLimit2 bbox, acList *List)
    {
    List->Clear();
    List->SetListData(SizeObjects);
    LoadListQuad(root, bbox, List);
    }

void acQTree::LoadListQuad (acNodeQTree *ntree, acLimit2 bbox, acList *List)
    {
    acLimit2 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
	volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax)
	{
	if (!ntree->ptr[0])
	    {
	    acNodeList *nl = ntree->list.GetHead();
	    while (nl)
		{
		List->InsertFirst(nl->GetDados());
		nl = nl->next;
		}
	    }
	else
	    for (int i = 0; i < 4; i++)
		LoadListQuad(ntree->ptr[i], bbox, List);
	}
    }

void acQTree::LoadListSort (acLimit2 bbox, acList *List)
    {
    LoadListSortQuad(root, bbox, List);
    }

void acQTree::LoadListSortQuad (acNodeQTree *ntree, acLimit2 bbox, acList *List)
    {
    acLimit2 volntree;
    volntree.xmin = ntree->center.x - 0.5 * ntree->size;
    volntree.xmax = ntree->center.x + 0.5 * ntree->size;
    volntree.ymin = ntree->center.y - 0.5 * ntree->size;
    volntree.ymax = ntree->center.y + 0.5 * ntree->size;

    if (volntree.xmax >= bbox.xmin && volntree.xmin <= bbox.xmax &&
	volntree.ymax >= bbox.ymin && volntree.ymin <= bbox.ymax)
	{
	if (!ntree->ptr[0])
	    {
	    acNodeList *nl = ntree->list.GetHead();
	    while (nl)
		{
		List->InsertSort(nl->GetDados());
		nl = nl->next;
		}
	    }
	else
	    for (int i = 0; i < 4; i++)
		LoadListSortQuad(ntree->ptr[i], bbox, List);
	}
    }

