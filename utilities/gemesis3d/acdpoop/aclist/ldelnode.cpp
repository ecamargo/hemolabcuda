#include "aclist.h"

int acList::DelNode(void *d)
    {
    acNodeList *aux   = head,
         *prev  = 0,
         *temp;

    register int achou = 0;

    if (!cmp)
        {
        errorcmp();
        return(-1);
        }

    while (aux && !achou)
        {
        if (!(*cmp)(d, aux->dados))
            achou = 1;
        else
            {
            prev = aux;
            aux = aux->next;
            }
        }

    if (achou)
        {
        sizelist--;
        temp = aux;

        if (prev)      /* se nao e' o primeiro */
            prev->next = aux->next;

        else           /* e' o primeiro */
            head = aux->next;

        delete (char *)temp->dados;
        delete (char *)temp;
        if (sizelist == 0)
            head = 0;
        }

    return(achou);
    }
