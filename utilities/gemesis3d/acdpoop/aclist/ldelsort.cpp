#include "aclist.h"

int acList::DelNodeSort(void *d)
    {
    acNodeList *aux   = head,
         *prev  = NULL,
         *temp;
    register int res;

    while (aux)
        {
        res = (*cmp)(d, aux->dados);

        if (res > 0)      /* ainda nao achou */
            {
            prev = aux;
            aux = aux->next;
            }

        else if (res < 0)  /* nao vai achar */
            return(0);

        else               /* achou */
            {
            temp = aux;

            if (prev)      /* se nao e' o primeiro */
                prev->next = aux->next;

            else           /* e' o primeiro */
                head = aux->next;

            delete (char *)temp->dados;
            delete (char *)temp;
            sizelist--;
            if (sizelist == 0)
                head = 0;
            return(1);
            }
        }
    return(0);
    }
