#include "aclist.h"

int acList::InsertFirst(void *a)
    {
    acNodeList *temp;

    if (!sizedados)         // nao inicializou lista
        {
        errorinit();
        return(0);
        }

    temp = (acNodeList *) new acNodeList;
    if (!temp)
        {
        erroralloc();
        return(0);
        }
    temp->dados = new char[sizedados];
    if (!temp->dados)
        {
        erroralloc();
        return(0);
        }
    memcpy(temp->dados, a, sizedados); // copia dados

    sizelist++;

    if (head)
        temp->next = head;
    else
        temp->next = 0;

    head = temp;
    return(1);
    }

