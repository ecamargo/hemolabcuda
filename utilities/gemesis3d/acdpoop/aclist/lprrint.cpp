#include "aclist.h"
/*
** funcao para imprimir lista em arquivo
*/
int acList::Print(FILE *fp)
    {
    acNodeList *aux = head;

    if (!prt)
        {
        errorprt();
        return(0);
        }

    while (aux)
        {
        (*prt)(fp, aux->dados);
        aux = aux->next;
        }
    return(1);
    }
