#include "aclist.h"

void * acList::SearchSeq(void *d)
    {
    acNodeList *aux = head;

    if (!sizedados)
        {
        errorinit();
        return(NULL);
        }

    if (!cmp)
        {
        errorcmp();
        return(NULL);
        }

    while (aux)
       if ((*cmp)(d, aux->dados) == 0)
           return(aux->dados);
       else
           aux = aux->next;

    return(0);
    }
