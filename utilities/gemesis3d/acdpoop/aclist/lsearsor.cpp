#include "aclist.h"

void * acList::SearchSort(void *d)
    {
    acNodeList *aux = head;
    register int res;

    if (!sizedados)
        {
        errorinit();
        return(NULL);
        }

    if (!cmp)
        {
        errorcmp();
        return(NULL);
        }

    while (aux)
        {
        res = (*cmp)(d, aux->dados);

        if (res > 0)
            aux = aux->next;
        else if (res < 0)
            return(0);
        else
            return(aux->dados);
        }
    return(0);
    }
