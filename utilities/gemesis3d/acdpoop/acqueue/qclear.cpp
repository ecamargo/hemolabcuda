#include "acqueue.h"
#include <string.h>

void acQueue::Clear()
    {
    acNodeQueue *curr = Head,
              *temp;

    for ( ; curr; curr = temp)
        {
        temp = curr->next;
        delete (char *)curr->dados;
        delete (char *)curr;
        }

    Head      = Tail = 0;
    SizeDados = 0;
    }
