#include "acqueue.h"
#include <string.h>

int acQueue::Pop(void *a)
    {
    acNodeQueue *temp = Head;

    if (!temp)
        return(0);

    memcpy(a, temp->dados, SizeDados);

    Head = temp->next;
    delete (char *)temp->dados;
    delete (char *)temp;
    if (!Head)
        Tail = NULL;
    return(1);
    }
