#include "acqueue.h"
#include <string.h>

int acQueue::Push(void *a)
    {
    char buf[100];
    acNodeQueue *temp;
    acNodeQueue *auxf = Tail;

    if (!SizeDados)         // nao inicializou lista
        { //  17, "Can't Allocate Memory - Data Size of Node Equal Zero."
        GetError(17,  buf);
        Error(FATAL_ERROR, 1, buf);
        return(0);
        }

    temp = new acNodeQueue;
    if (!temp)
        { //  1, "Can't Allocate Memory."
        GetError(1,  buf);
        Error(FATAL_ERROR, 2, buf);
        return(0);
        }

    temp->dados = new char[SizeDados];
    if (!temp->dados)
        {  // 1, "Can't Allocate Memory."
        GetError(1,  buf);
        Error(FATAL_ERROR, 2, buf);
        return(0);
        }

    memcpy(temp->dados, a, SizeDados); // copia dados
    temp->next = NULL;

    if (Head)
        {
        auxf->next = temp;
        Tail       = temp;
        }
    else
        Head = Tail = temp;

    return(1);
    }
