#include "acstack.h"
#include <string.h>

void acStack::Clear()
    {
    acNodeStack *curr = Head,
              *temp;

    for ( ; curr; curr = temp)
        {
        temp = curr->next;
        delete (char *)curr->dados;
        delete (char *)curr;
        }

    Head      = 0;
    SizeDados = 0;
    }
