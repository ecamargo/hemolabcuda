#include "acstack.h"
#include <string.h>

int acStack::Pop(void *a)
    {
    acNodeStack *temp = Head;

    if (!temp)
        return(0);

    memcpy(a, temp->dados, SizeDados);

    Head = temp->next;
    delete (char *)temp->dados;
    delete (char *)temp;
    return(1);
    }
