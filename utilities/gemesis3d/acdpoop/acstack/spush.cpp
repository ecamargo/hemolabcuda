#include "acstack.h"
#include <string.h>

int acStack::Push(void *a)
    {
    char buf[100];
    acNodeStack *temp;

    if (!SizeDados)         // nao inicializou lista
        { //  17, "Can't Allocate Memory - Data Size of Node Equal Zero."
        GetError(17, buf);
        Error(FATAL_ERROR, 1, buf);
        return(0);
        }

    temp = new acNodeStack;
    if (!temp)
        { //  1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 2, buf);
        return(0);
        }

    temp->dados = new char[SizeDados];
    if (!temp->dados)
        {  // 1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 2, buf);
        return(0);
        }

    memcpy(temp->dados, a, SizeDados); // copia dados

    if (Head)
        temp->next = Head;
    else
        temp->next = 0;

    Head = temp;
    return(1);
    }
