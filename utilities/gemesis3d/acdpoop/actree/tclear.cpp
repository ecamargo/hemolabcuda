#include "actree.h"

// apaga arvore ou ramo
void acTree::Clear(acNodeTree *n, int first)
    {
    acNodeTree *current;

    if (first)
        {
        current = head;
        first   = 0;
        head    = 0;
        }
    else
        current = n;
    if (current)
        {
        Clear(current->l, first);
        Clear(current->r, first);
        delete (char *)current->dados;
        delete (char *)current;
        }
    }
