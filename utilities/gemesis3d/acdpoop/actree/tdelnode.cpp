/*
** deleta um no' da arvore retornando 1 (true) ou 0 (false)
** a funcao procura o no' desejado
**
** Adaptado de Turbo Pascal Programmer's library pag. - 168
**
** Salgado - 2/90
*/
#include "actree.h"
int acTree::DelNode(void *info)
    {
    acNodeTree *sp, *pp, *tp, *t2p;
    register int res;

    if (head == NULL)
        return(0);

    sp = head;
    while (((res = (*cmp)(info, sp->dados)) != 0) && sp)
       {
       pp = sp;
       if (res < 0)
           sp = sp->l;
       else
           sp = sp->r;
       }

    if (sp == NULL)
        return(0);

    if (sp->l == NULL)
        tp = sp->r;
    else
        {
        tp = sp->l;
        t2p = tp;
        while (t2p->r)
            t2p = t2p->r;
        t2p->r = sp->r;
        }

    if (head == sp)
        head = tp;
    else if (pp->l == sp)
        pp->l = tp;
    else
        pp->r = tp;

    delete (char *)sp->dados;
    delete (char *)sp;
    return(1);
    }
