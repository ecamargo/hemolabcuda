#include "actree.h"
#include "acdp.h"

void acTree::errorcmp()           // erro de comparacao
    {  //   18, "Can't Find Comparation Function."
    char buf[100];
    GetError(18, buf);
    Error(FATAL_ERROR, 1, buf);
    }

void acTree::erroralloc()     // erro de alocacao
    {  //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
    }

void acTree::errorinit()      // erro de inicializacao
    { //   17, "Can't Allocate Memory - Data Size of Node Equal Zero."
    char buf[100];
    GetError(17, buf);
    Error(FATAL_ERROR, 3, buf);
    }

void acTree::errorprt()       // erro de impressao
    { //   19, "Can't Find Printer Function."
    char buf[100];
    GetError(19, buf);
    Error(FATAL_ERROR, 1, buf);
    }

