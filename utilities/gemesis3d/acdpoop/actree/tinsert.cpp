#include "actree.h"

#if defined(__UNIX__)
#include <memory.h>
#else
#include <string.h>
#endif

int acTree::Insert(void *val)
    {
    acNodeTree *curr = head, *prev = head, *temp;
    register int res;

    if (!cmp)
        {
        errorcmp();
        return(-1);
        }

    if (!sizedados)
        {
        errorinit();
        return(-1);
        }


    // cria um novo no' e acha a sua posicao na arvore
    temp = (acNodeTree *) new acNodeTree;
    if (!temp)
        {
        erroralloc();
        return(-1);
        }

    temp->dados = new char[sizedados];
    if (!temp->dados)
        {
        erroralloc();
        return(-1);
        }

    temp->l = temp->r  = 0;
    memcpy(temp->dados, val, sizedados); // copia dados

    if (!head)
        head = temp;
    else              // acha a posicao para inserir
        {
        while (curr)
            {
            prev = curr;

            // verifica o ramo em que pode entrar o valor.
            // se for menor, vai para a esquerda (left)
            // se for maior, vai para a direita (right)
            // se for igual, retorna sem fazer nada
            res = (*cmp)(val, curr->dados);
            if (res < 0)
                curr = curr->l;
            else if (res > 0)
                curr = curr->r;
            else
                {
                delete (char *)temp->dados;
                delete (char *)temp;
                return(0);
                }
            }

        // curr == NULL -> prev aponta para o pai
        // verifica, entao o ramo
        if (res < 0)
            prev->l = temp;    // coloca na esquerda
        else
            prev->r = temp;    // coloca na direita
        }

    return(1);
    }
