#include "actree.h"
#include <string.h>

void *acTree::InsRet(void *val)
    {
    acNodeTree *curr = head, *prev = head, *temp;
    void *aux;
    register int res;

    if (!cmp)
	{
	errorcmp();
	return(NULL);
	}

    if (!sizedados)
	{
	errorinit();
	return(NULL);
	}

    /*
    ** cria um novo no' e acha a sua posicao na arvore
    */
    temp = new acNodeTree;
    if (!temp)
	{
        erroralloc();
	return(NULL);
	}
    aux = temp->dados = new char[sizedados];
    if (!aux)
	{
	erroralloc();
	return(NULL);
	}

    temp->l = temp->r  = 0;
    memcpy(temp->dados, val, sizedados); // copia dados

    if (!head)
        head = temp;
    else              /* acha a posicao para inserir */
        {
        while (curr)
            {
            prev = curr;

            /*
            ** verifica o ramo em que pode entrar o valor.
            ** se for menor, vai para a esquerda (left)
            ** se for maior, vai para a direita (right)
            ** se for igual, retorna sem fazer nada
            */
            res = (*cmp)(val, curr->dados);
            if (res < 0)
                curr = curr->l;
            else if (res > 0)
                curr = curr->r;
            else
                {
                delete (char *)temp->dados;
                delete (char *)temp;
                return(NULL);
                }
            }

        /*
        ** curr == NULL -> prev aponta para o pai
        ** verifica, entao o ramo
        */
        if (res < 0)
            prev->l = temp;    /* coloca na esquerda */
        else
            prev->r = temp;    /* coloca na direita */
        }

    return(aux);
    }
