#include "actree.h"

void acTree::Restore(long tam, FILE *fp)
    {
    head = resttree(tam, fp);
    }

acNodeTree *acTree::resttree(long n, FILE *fp)
    {
    long nleft, nright;
    acNodeTree *t;

    if (n == 0)
        return (NULL);

    // calcula numero de elementos de cada lado
    nleft  = n / 2;     
    nright = n - nleft - 1;

    t = (acNodeTree *) new acNodeTree;
    if (!t)
        {
        erroralloc();
        return(NULL);
        }

    t->dados = new char[sizedados];
    if (!t->dados)
        {
        erroralloc();
        return(NULL);
        }

    t->l = resttree(nleft, fp);
    fread(t->dados, sizedados, 1, fp);
    t->r = resttree(nright, fp);
    return (t);
    }
