#include "actree.h"

void acTree::Save(FILE *fp)
    {
    savetree(fp, head);
    }

void acTree::savetree(FILE *fp, acNodeTree *t)
    {
    if (t)
        {
        savetree(fp, t->l);
        fwrite(t->dados, sizedados, 1, fp);
        savetree(fp, t->r);
        }
    }
