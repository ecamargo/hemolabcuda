/*
** acVSLlong.cxx - Salgado - 08/97
*/

#include "acvslong.h"


void acVSLong::AjustaVetor(long val)
    {
    FirstFree.l++;

    if (FirstFree.l >= TotalVetores)
        {
        PLONG *aux = new PLONG[TotalVetores + StepVet];
        for (long i = 0; i < TotalVetores; i++)
            aux[i] = V[i];
        delete V;
        V = aux;
        TotalVetores += StepVet;
        }
                
    FirstFree.c = 0;
    V[FirstFree.l] = new long[NumColsVet]; 
    if (!FirstFree.l)
        ErrMem();
    plin = V[FirstFree.l];
    *plin++ = val;
    FirstFree.c++;
    NNodes++;
    }


acVSLong::~acVSLong()
    {
    for (long i = 0; i <= FirstFree.l; i++)
        delete V[i];

    delete V;
    }

void acVSLong::Print(FILE *fp, int n, BOOL elnum)
    {
    if (n < 1)
        n = 1;

    long el = 1;
    for (long i = 0; i < NNodes; i += n)
        {
        if (elnum)
            fprintf(fp, "%ld ", el++);

        for (long j = 0; j < n; j++)
            fprintf(fp, "%ld ", Get(i));
        fprintf(fp, "\n");
        }
    }

