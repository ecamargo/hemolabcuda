#include "acdp.h"
#include "acdptype.h"
#include "actree.h"
#include "aclist.h"

#include <time.h>
/*
#if defined(__UNIX__)
#include <sys/types.h>
#else
#include <sys\types.h>
#endif
*/

// Informacoes sobre um banco de dados
// Gravado em arquivos .DIR
struct SDBINFO
    {
    // nome de correspondencia entre .DIR e .BDG -> contem a data
    // de criacao dos arquivos
    char     NomeCorp[81];
    int      NumObjs;       // num. de objetos existentes
    long int NumBytes;      // num. de bytes gravados no .BDG
    int      Permis;        // permissao para R/W
    char     Password[81];  // password
    };

// No' da lista com informacoes sobre os bancos de dados abertos
struct SOPENDB
    {
    SDBINFO DBInfo;     // info para acesso
    char   Nome[81];    // nome logico
    char   NomeDir[81]; // nome fisico do arq. .DIR
    char   NomeBdg[81]; // nome fisico do arq. .BDG
    acTree root;        // pointer p/ inicio da arvore
    int FoiAlt;         // indica se o .BDG foi alterado
    };

// Informacoes sobre um objeto no banco de dados
struct SINFOOBJ
    {
    char Nome[81];      // nome do objeto
    int  Versao;        // versao do objeto
    long Pos;           // posicao no BDG
    };


static acList *listDB;   // lista de banco de dados abertos

static int SOPENDBcmp(void *a, void *b)
    {
    SOPENDB *a1 = (SOPENDB *) a;
    SOPENDB *b1 = (SOPENDB *) b;
    return(StrCmp(a1->Nome, b1->Nome));
    }

static int SINFOOBJcmp(void *a, void *b)
    {
    SINFOOBJ *a1 = (SINFOOBJ *) a;
    SINFOOBJ *b1 = (SINFOOBJ *) b;
    int res;

    res = StrCmp(a1->Nome, b1->Nome);
    if (res != 0)
	return(res);
    return(a1->Versao - b1->Versao);
    }

//********************************************************************
// inicializa gerenciamento de banco de dados
// cria lista de bancos de dados abertos
//********************************************************************
void InitDB()
    {
    static char *nome = "InitDB";
    char buf[100];
    
    TraceOn(nome);

    listDB = new acList(sizeof(SOPENDB), SOPENDBcmp);
    if (!listDB)
        { //   1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(2, 1, buf);
        }
    TraceOff(nome);
    }

//********************************************************************
// Cria um banco de dados pela primeira vez.
// Sao passados o nome fisico do banco de dados e a sua password.
// O nome do banco de dados deve ser fornecido sem extensao.
// Caso ja' exista um banco de dados com o nome fornecido, sera'
// verificada se a password fornecida e' igual a do banco de dados.
// Se for, o banco podera' ser recriado. Caso contrario, erro.
// As informacoes sobre o banco de dados serao gravadas nos arquivos
// da seguinte forma:
// nome.DIR -> estrutura do tipo SDBINFO
// nome.BDG -> nome de correspondencia
//********************************************************************
void CreateDB(char *nofisic, char *pasword)
    {
    static char *nome   = "CreateDB";
    int         CanOpen, nivel;
    time_t      tempo;
    char        *aux;
    SDBINFO     DBInfo;
    char        buf_dir[81], buf_bdg[81], buf[100];
    FILE        *fpdir, *fpbdg;

    TraceOn(nome);
    DebugOn(&nivel);

    aux = nofisic;   // verifica se o nome nao possui '.'
    while (*aux)
	if (*aux == '.')
	    { //   2, "Incorrect Name |%s| for Data Base."
	    GetError(2, buf);
	    Error(FATAL_ERROR, 1, buf, nofisic);
	    }
	else
	    aux++;

    StrCpy(buf_dir, nofisic);   // monta nomes dos arquivos
    StrCat(buf_dir, ".dir");
    StrCpy(buf_bdg, nofisic);
    StrCat(buf_bdg, ".bdg");

    // abre o arquivo para leitura
    CanOpen = TRUE;
    if ((fpdir = fopen(buf_dir, "rb+")) != NULL)
        {  // ja' existe o arquivo -> le e testa a password
        fread((char *) &DBInfo, sizeof(struct SDBINFO), 1, fpdir);
        if (!StrEq(pasword, DBInfo.Password))
            CanOpen = FALSE;
        fclose(fpdir);
        }

    if (!CanOpen)  // se nao puder criar o arquivo, erro
        { //   3, "Incorrect Password for Data Base |%s|."
        GetError(3, buf);
        Error(FATAL_ERROR, 2, buf, nofisic);
        }

    // se chegou aqui e' porque pode criar o arquivo
    tempo = time(NULL);
    sprintf(DBInfo.NomeCorp, "%ld", tempo);
    DBInfo.NumObjs = 0;
    DBInfo.Permis  = 1;     // read/write
    StrCpy(DBInfo.Password, pasword);
    DBInfo.NumBytes = 81;   // tamanho de DBInfo.NomCorp

    // abre os arquivos e grava as informacoes necessarias
    if ((fpdir = fopen(buf_dir, "wb")) == NULL)       // .DIR
        { //    4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(FATAL_ERROR, 3, buf, buf_dir);
        }

    fwrite((char *) &DBInfo, sizeof(SDBINFO), 1, fpdir);
    fclose(fpdir);

    if ((fpbdg = fopen(buf_bdg, "wb")) == NULL)       // .BDG
        { //   4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(FATAL_ERROR, 4, buf, buf_bdg);
        }

    fwrite((char *) DBInfo.NomeCorp, 81, 1, fpbdg);
    fclose(fpbdg);

    if (nivel > 0)
        {
        WDebug("Internal name:     |%s|\n", DBInfo.NomeCorp);
        WDebug("Number of Objects: |%d|\n", DBInfo.NumObjs);
        WDebug("Number of Bytes:   |%ld|\n", DBInfo.NumBytes);
        WDebug("Permission:        |%d|\n", DBInfo.Permis);
        WDebug("Password:          |%s|\n", DBInfo.Password);
        }
    DebugOff();
    TraceOff(nome);
    }

//********************************************************************
// Abre um banco de dados existente.
// E' passado o nome do banco de dados (sem extensao).
// Caso o banco de dados ja' esteja aberto, erro.
// A informacao do banco de dados sera' colocada em listDB, que e'
// uma lista contendo as informacoes sobre todos os bancos de dados
// abertos.
// Os nos de listDB (SOPENDB) contem tambem uma arvore com informacoes
// sobre o nome, versao e posicao de cada objeto gravado neste banco.
//********************************************************************
int OpenDB(char *nome)
    {
    static char *nomefc  = "OpenDB";
    SOPENDB OpenDB;
    SDBINFO DBInfo;
    FILE    *fpdir, *fpbdg;
    char    nomcorp[100], *aux, buf[100];
    int     nivel;

    TraceOn(nomefc);
    DebugOn(&nivel);

    // verifica se o nome esta' correto (sem '.')
    aux = nome;
    while (*aux)
	if (*aux == '.')
	    { //    5, "Incorrect Name for Data Base."
	    GetError(5, buf);
	    Error(COMMON_ERROR, 1, buf);
        return 0;
	    }
	else
	   aux++;

    // verifica se este banco de dados ja' esta' aberto
    StrCpy(OpenDB.Nome, nome);
    if (listDB->SearchSort(&OpenDB))
        { //   6, "Data Base |%s| is Already Open."
        GetError(6, buf);
        Error(COMMON_ERROR, 2, buf, nome);
        return 0;
        }

    // nao esta' aberto, pode abrir
    StrCpy(OpenDB.NomeDir, nome);    // monta NomeDir
    StrCat(OpenDB.NomeDir, ".dir");
    StrCpy(OpenDB.NomeBdg, nome);    // monta NomeBdg
    StrCat(OpenDB.NomeBdg, ".bdg");
    OpenDB.FoiAlt = FALSE;
    OpenDB.root.SetTreeData(sizeof(SINFOOBJ), SINFOOBJcmp);

    // O arquivo nao foi aberto vai colocar um novo no' na lista
    // abre o arquivo .DIR
    fpdir = fopen(OpenDB.NomeDir, "rb");
    if (!fpdir)
        { //    4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(COMMON_ERROR, 3, buf, OpenDB.NomeDir);
        return 0;
        }

    // le as informacoes que estao no arquivo diretorio
    fread((char *) &DBInfo, sizeof(SDBINFO), 1, fpdir);

    // abre o .BDG e le o nome de correspondencia
    fpbdg = fopen(OpenDB.NomeBdg, "rb");
    if (!fpbdg)
        { //    4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(COMMON_ERROR, 4, buf, OpenDB.NomeBdg);
        return 0;
        }
    fread(nomcorp, 81, 1, fpbdg);
    fclose(fpbdg);

    // compara nomes de correspondencia do .DIR e .BDG
    if (!StrEq(nomcorp, DBInfo.NomeCorp))
        { //   7, "Incorrect Internal Names |%s| x |%s| for File |%s|."
        GetError(7, buf);
        Error(COMMON_ERROR, 5, buf, nomcorp, DBInfo.NomeCorp, nome);
        return 0;
        }

    OpenDB.DBInfo = DBInfo;    // coloca as informacoes no novo no'

    // monta a arvore deste arquivo
    OpenDB.root.Restore(DBInfo.NumObjs, fpdir);
    fclose(fpdir);

    // coloca o novo no' na lista de arquivos abertos
    listDB->InsertSort(&OpenDB);

    if (nivel > 0)
       {
       WDebug("Internal name:     |%s|\n", DBInfo.NomeCorp);
       WDebug("Number of Objects: |%d|\n", DBInfo.NumObjs);
       WDebug("Number of Bytes:   |%ld|\n", DBInfo.NumBytes);
       WDebug("Permission:        |%d|\n", DBInfo.Permis);
       WDebug("Password:          |%s|\n", DBInfo.Password);
       }

    DebugOff();
    TraceOff(nomefc);
    return 1;
    }

//********************************************************************
// Fecha um banco de dados.
// Se o banco de dados foi alterado, deverao ser gravadas as
// novas informacoes.
//********************************************************************
void CloseDB(char *nome)
    {
    static char *nomefc = "CloseDB";
    SOPENDB *pOpenDB, OpenDB;
    FILE    *fp;
    char buf[100];

    TraceOn(nomefc);

    // verifica se existe lista
    if (!listDB->Exist())
        { //    8, "Can't Find Open Data Bases."
        GetError(8, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    // verifica se o banco de dados esta' aberto
    StrCpy(OpenDB.Nome, nome);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);
    if (!pOpenDB)
        { //    9, "Data Base |%s| is Not Open."
        GetError(9, buf);
        Error(FATAL_ERROR, 2, buf, nome);
        }

    // Se chegou aqui, e' porque existe o banco de dados
    // Se teve permissao para gravar e salvar novos objetos no .BDG,
    // abre o arquivo .DIR e atualiza as informacoes
    if (pOpenDB->DBInfo.Permis == 1)
        if (pOpenDB->FoiAlt)    // foi alterado?
            {
            fp = fopen(pOpenDB->NomeDir, "wb");
            if (!fp)
                { //    4, "Can't Open Data Base |%s|."
                GetError(4, buf);
                Error(FATAL_ERROR, 3, buf, nome);
                }
            fwrite((char *) &pOpenDB->DBInfo, sizeof(SDBINFO), 1, fp);
            pOpenDB->root.Save(fp);  // grava arvore
            fclose(fp);
            }

    // libera a arvore com os nomes das tabelas
    // libera os nomes de arquivos
    pOpenDB->root.Clear();

    // retira o no' da lista
    listDB->DelNodeSort(&OpenDB);
    TraceOff(nomefc);
    }

//********************************************************************
// Prepara banco de dados para gravacao de um objeto.
// Retorna um ponteiro para gravacao no .BDG
//********************************************************************
FILE *OpenSave(char *nomeobj, int versao, char *bda)
    {
    static char *nome = "OpenSave";
    SOPENDB  OpenDB, *pOpenDB;
    SINFOOBJ InfoObj;
    FILE     *fpbdg;
    char     buf[100];

    TraceOn(nome);

    // procura na lista de nomes de bancos de dados para ver se
    // este banco esta' aberto.
    StrCpy(OpenDB.Nome, bda);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);

    if (!pOpenDB)
        { //   9, "Data Base |%s| is Not Open."
        GetError(9, buf);
        Error(FATAL_ERROR, 1, buf, bda);
        }

    // Procura o objeto "nomeobj" de versao "versao" na arvore do
    // arquivo. Se existir, erro
    StrCpy(InfoObj.Nome, nomeobj);
    InfoObj.Versao = versao;
    InfoObj.Pos    = pOpenDB->DBInfo.NumBytes;
    if (pOpenDB->root.Search(&InfoObj))
        { // 10, "The Object |%s|, Version |%d| Already Exists in the Data Base |%s|."
        GetError(10, buf);
        Error(FATAL_ERROR, 2, buf, nomeobj, versao, bda);
        }

    // verifica se pode gravar no arquivo
    if (!pOpenDB->DBInfo.Permis)
        { //   11, "Can't Write in Data Base |%s|."
        GetError(11, buf);
        Error(FATAL_ERROR, 3, buf, bda);
        }

    pOpenDB->DBInfo.NumObjs++;   // atualiza numero de objetos gravados

    if (!pOpenDB->root.Insert(&InfoObj))    // insere na arvore
        { //   12, "Unespected Error."
        GetError(12, buf);
        Error(FATAL_ERROR, 4, buf);
        }

    // grava novas informacoes no final do .BDG
    fpbdg = fopen(pOpenDB->NomeBdg, "ab");
    if (!fpbdg)
        { //    4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(FATAL_ERROR, 5, buf, bda);
        }
    fwrite((char *) &InfoObj, sizeof(SINFOOBJ), 1, fpbdg);

    TraceOff(nome);
    return(fpbdg);
    }


//********************************************************************
// Fecha um banco de dados
// Atualiza as informacoes
//********************************************************************
void CloseSave(FILE *fp, char *bda)
    {
    static char *nome = "CloseSave";
    SOPENDB  OpenDB, *pOpenDB;
    char buf[100];

    TraceOn(nome);

    // verifica se o banco de dados esta' aberto
    StrCpy(OpenDB.Nome, bda);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);
    if (!pOpenDB)
        { //   9, "Data Base |%s| is Not Open."
        GetError(9, buf);
        Error(FATAL_ERROR, 1, buf, bda);
        }

    // calcula posicao do proximo objeto no .BDG
#if defined(__UNIX__)
    fseek(fp, 0l, 2);
#else
    fseek(fp, 0l, SEEK_END);
#endif
    pOpenDB->DBInfo.NumBytes = ftell(fp);

    pOpenDB->FoiAlt = 1;   // avisa que o arquivo foi alterado
    fclose(fp);
    TraceOff(nome);
    }


//********************************************************************
// se prepara para recuperar um objeto
//********************************************************************
FILE *OpenRestore(char *nomeobj, int versao, char *bda)
    {
    static char *nome = "OpenRestore";
    SOPENDB  OpenDB, *pOpenDB;
    SINFOOBJ InfoObj, *pInfoObj, auxObj;
    FILE     *fbdg;
    char     buf[100];

    TraceOn(nome);

    // verifica se o banco de dados esta' aberto
    StrCpy(OpenDB.Nome, bda);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);
    if (!pOpenDB)
       { //    9, "Data Base |%s| is Not Open."
       GetError(9, buf);
       Error(COMMON_ERROR, 1, buf, bda);
       return NULL;
       }

    // procura objeto "nomeobj" de versao "versao" na arvore
    // se nao estiver, erro
    StrCpy(InfoObj.Nome, nomeobj);
    InfoObj.Versao = versao;
    pInfoObj = (SINFOOBJ *) pOpenDB->root.Search(&InfoObj);
    if (!pInfoObj)
        { //  13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
        GetError(13, buf);
        Error(COMMON_ERROR, 2, buf, nomeobj, versao, bda);
        return NULL;
        }

    // se chegou aqui, pode restaurar o objeto
    fbdg = fopen(pOpenDB->NomeBdg, "rb");
    if (!fbdg)
        { //    4, "Can't Open Data Base |%s|."
        GetError(4, buf);
        Error(COMMON_ERROR, 3, buf, pOpenDB->NomeBdg);
        return NULL;
        }

    fseek(fbdg, pInfoObj->Pos, 0);
    fread((char *) &auxObj, sizeof(SINFOOBJ), 1, fbdg);

    TraceOff(nome);
    return(fbdg);
    }

void CloseRestore(FILE *fp)
    {
    fclose(fp);
    }

//********************************************************************
// se prepara para resalvar um objeto
//********************************************************************
FILE *OpenReSave(char *nomeobj, int versao, char *bda)
    {
    static char *nome = "OpenReSave";
    SOPENDB  OpenDB, *pOpenDB;
    SINFOOBJ InfoObj, *pInfoObj;
    FILE     *fbdg;
    char     buf[100];

    TraceOn(nome);

    // verifica se o banco de dados esta' aberto
    StrCpy(OpenDB.Nome, bda);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);
    if (!pOpenDB)
        { //    9, "Data Base |%s| is Not Open."
        GetError(9, buf);
        Error(FATAL_ERROR, 1, buf, bda);
        }

    // procura objeto "nomeobj" de versao "versao" na arvore
    // se nao estiver, erro
    StrCpy(InfoObj.Nome, nomeobj);
    InfoObj.Versao = versao;
    pInfoObj = (SINFOOBJ *) pOpenDB->root.Search(&InfoObj);

    if (!pInfoObj)  // nao esta' no bda.
        fbdg = OpenSave(nomeobj, versao, bda);

    else
        { // ja' esta' no bda. resalvar o objeto
        fbdg = fopen(pOpenDB->NomeBdg, "rb+");
        if (!fbdg)
            { //    4, "Can't Open Data Base |%s|."
            GetError(4, buf);
            Error(FATAL_ERROR, 3, buf, pOpenDB->NomeBdg);
            }

        fseek(fbdg, pInfoObj->Pos + sizeof(SINFOOBJ), 0);
        }

    TraceOff(nome);
    return(fbdg);
    }

//********************************************************************
// Fecha um banco de dados
// Atualiza as informacoes
//********************************************************************
void CloseReSave(FILE *fp, char *bda)
    {
    static char *nome = "OpenReSave";
    TraceOn(nome);
    CloseSave(fp, bda);
    TraceOff(nome);
    }
    
//********************************************************************
// se prepara para recuperar um objeto
//********************************************************************
FILE *OpenRestoreSE(char *nomeobj, int versao, char *bda, int *err)
    {
    static char *nome = "OpenRestoreSE";
    SOPENDB  OpenDB, *pOpenDB;
    SINFOOBJ InfoObj, *pInfoObj, auxObj;
    FILE     *fbdg;

    TraceOn(nome);
    *err = 0;
    // verifica se o banco de dados esta' aberto
    StrCpy(OpenDB.Nome, bda);
    pOpenDB = (SOPENDB *) listDB->SearchSort(&OpenDB);
    if (!pOpenDB)
       { //    9, "Data Base |%s| is Not Open."
       *err = 9;
       return NULL;
       }

    // procura objeto "nomeobj" de versao "versao" na arvore
    // se nao estiver, erro
    StrCpy(InfoObj.Nome, nomeobj);
    InfoObj.Versao = versao;
    pInfoObj = (SINFOOBJ *) pOpenDB->root.Search(&InfoObj);
    if (!pInfoObj)
        { //  13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
        *err = 13;
        return NULL;
        }

    // se chegou aqui, pode restaurar o objeto
    fbdg = fopen(pOpenDB->NomeBdg, "rb");
    if (!fbdg)
        { //    4, "Can't Open Data Base |%s|."
        *err = 4;
        return NULL;
        }

    fseek(fbdg, pInfoObj->Pos, 0);
    fread((char *) &auxObj, sizeof(SINFOOBJ), 1, fbdg);

    TraceOff(nome);
    return(fbdg);
    }

//********************************************************************
// Abre um banco de dados existente.
// E' passado o nome do banco de dados (sem extensao).
// Caso o banco de dados ja' esteja aberto, erro.
// A informacao do banco de dados sera' colocada em listDB, que e'
// uma lista contendo as informacoes sobre todos os bancos de dados
// abertos.
// Os nos de listDB (SOPENDB) contem tambem uma arvore com informacoes
// sobre o nome, versao e posicao de cada objeto gravado neste banco.
//********************************************************************
int OpenDBSE(char *nome, int *err)
    {
    static char *nomefc  = "OpenDBSE";
    SOPENDB OpenDB;
    SDBINFO DBInfo;
    FILE    *fpdir, *fpbdg;
    char    nomcorp[100], *aux;
    int     nivel;

    TraceOn(nomefc);
    DebugOn(&nivel);

    *err = 0;
    // verifica se o nome esta' correto (sem '.')
    aux = nome;
    while (*aux)
	if (*aux == '.')
	    { //    5, "Incorrect Name for Data Base."
        *err = 5;
        return 0;
	    }
	else
	   aux++;

    // verifica se este banco de dados ja' esta' aberto
    StrCpy(OpenDB.Nome, nome);
    if (listDB->SearchSort(&OpenDB))
        { //   6, "Data Base |%s| is Already Open."
        *err = 6;
        return 0;
        }

    // nao esta' aberto, pode abrir
    StrCpy(OpenDB.NomeDir, nome);    // monta NomeDir
    StrCat(OpenDB.NomeDir, ".dir");
    StrCpy(OpenDB.NomeBdg, nome);    // monta NomeBdg
    StrCat(OpenDB.NomeBdg, ".bdg");
    OpenDB.FoiAlt = FALSE;
    OpenDB.root.SetTreeData(sizeof(SINFOOBJ), SINFOOBJcmp);

    // O arquivo nao foi aberto vai colocar um novo no' na lista
    // abre o arquivo .DIR
    fpdir = fopen(OpenDB.NomeDir, "rb");
    if (!fpdir)
        { //    4, "Can't Open Data Base |%s|."
        *err = 4;
        return 0;
        }

    // le as informacoes que estao no arquivo diretorio
    fread((char *) &DBInfo, sizeof(SDBINFO), 1, fpdir);

    // abre o .BDG e le o nome de correspondencia
    fpbdg = fopen(OpenDB.NomeBdg, "rb");
    if (!fpbdg)
        { //    4, "Can't Open Data Base |%s|."
        *err = 4;
        return 0;
        }
    fread(nomcorp, 81, 1, fpbdg);
    fclose(fpbdg);

    // compara nomes de correspondencia do .DIR e .BDG
    if (!StrEq(nomcorp, DBInfo.NomeCorp))
        { //   7, "Incorrect Internal Names |%s| x |%s| for File |%s|."
        *err = 7;
        return 0;
        }

    OpenDB.DBInfo = DBInfo;    // coloca as informacoes no novo no'

    // monta a arvore deste arquivo
    OpenDB.root.Restore(DBInfo.NumObjs, fpdir);
    fclose(fpdir);

    // coloca o novo no' na lista de arquivos abertos
    listDB->InsertSort(&OpenDB);

    if (nivel > 0)
       {
       WDebug("Internal name:     |%s|\n", DBInfo.NomeCorp);
       WDebug("Number of Objects: |%d|\n", DBInfo.NumObjs);
       WDebug("Number of Bytes:   |%ld|\n", DBInfo.NumBytes);
       WDebug("Permission:        |%d|\n", DBInfo.Permis);
       WDebug("Password:          |%s|\n", DBInfo.Password);
       }

    DebugOff();
    TraceOff(nomefc);
    return 1;
    }
