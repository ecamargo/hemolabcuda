#ifndef VECTOR_H
#define VECTOR_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "matematc.h"

class acMatrix;
class acSymMatrix;

class VTK_EXPORT acVector
    {
    protected:
        double huge *V;  // ponteiro para a area alocada
        long   Dim;      // tamanho do vetor


    public:
        /****************************
         * construtores e destrutor *
         ****************************/

        // Cria vetor
        // Ex.: acVector v;
        acVector() { V = 0; Dim = 0; }

        // Cria vetor com tamanho 
        // Ex.: acVector v(10);
        acVector(long);

        // Cria vetor e inicializa
        // Ex.: acVector v(10, 1.0);
        //      acVector v(10, 1.0, 0.5);
        acVector(long, double, double = 0.0);

        // "copy-initializer"
        // cria vetor temporario
        // permite inicializar um vetor com outro
        // Ex.: acVector vb(v);
        acVector(acVector& v);

        // Destrutor
       ~acVector() { Free(); }
       
       
       // Fornece tamanho para um vetor ja' criado
       void SetDim(long t);


        /**************
         * operadores *
         **************/

        // igualdade
        // Ex.: va = vb;
        //       va.Equal(vb);
        acVector& operator=(acVector& vb);
        acVector& Equal(acVector& vb);

        // soma os elementos de dois vetores
        // Ex.: v1 = v2 + v3;
        //      v1.Add(v2, v3);
        //      v1 += v2;
        //      v1.Add(v2);
        friend acVector operator+(acVector& v2, acVector& v3);
        acVector& Add(acVector& v2, acVector& v3);
        acVector& operator+=(acVector &v2);
        acVector& Add(acVector &v2);

        // subtrai os elementos de dois vetores
        // Ex.: v1 = v2 - v3;
        //      v1.Subt(v2, v3);
        //      v1 -= v2;
        //      v1.Subt(v2);
        friend acVector operator-(acVector& v2, acVector& v3);
        acVector& Subt(acVector& v2, acVector& v3);
        acVector& operator-=(acVector &v2);
        acVector& Subt(acVector &v2);

        // produto vetorial
        // Ex.: v1 = v2 * v3;
        //      v1.Prod(v2, v3);
        friend acVector operator*(acVector& v2, acVector& v3);
        acVector& Prod(acVector& v2, acVector& v3);

        // multiplica um vetor por um double
        // Ex.:  v *= 1.5;
        //      v.Prod(1.5);      
        acVector& operator*=(double);
        acVector& Prod(double);

        // calcula produto escalar entre vetores
        // Ex.: r = v1 ^ v2;
        //      r = v1.Scal(v2);
        double operator^(acVector& v2);
        double Scal(acVector& v2); 

        // indexador
        // Ex.: va[i] = 1.5;
        //      r = va[i];
        double huge & operator[](long i) { return (V[i]); }

        // muda o sinal dos elementos
        // Ex.: v1.Chs();
        acVector& Chs();
        acVector& operator-();

        // transformacao linear
        // Ex.: v1 = ma * v2;
        //      v1.MatrixProd(ma, v2);
        friend acVector operator*(acMatrix &ma, acVector &v2);
        acVector &Prod(acMatrix &ma, acVector &v2);        
        
        friend acVector operator*(acSymMatrix &ma, acVector &v2);
        acVector &Prod(acSymMatrix &ma, acVector &v2);


        
        /**********************
         * funcoes auxiliares *
         **********************/

         // libera area
         void Free();

         // norma infinita
         double InfinityNorm() { return norinfv(V, Dim); }

         // norma euclidiana
         double EuclidianNorm() { return normaev(V, Dim); }

        // retorna maior e menor valores
        double GetMax() { return maxd(V, Dim); }
        double GetMin() { return mind(V, Dim); }

        // vetor unitario
        acVector& UnitVector(acVector& v2);

        // retorna numero de elementos
        long GetDim() { return Dim; }

        // retorna ponteiro 
        double huge *GetVectorPtr() { return V; }

        // grava vetor (binario)
        // Ex.: v1.Save("vector1", 1, "vetbda");
        //      v1.Save(fp);
        void Save(char *objname, int version, char *filename);
        void Save(FILE *);

        // le vetor (binario)
        // Ex.: v1.Restore("vector1", 1, "vetbda");
        //      v1.Restore(fp);
        void Restore(char *objname, int version, char *filename);
        void Restore(FILE *);

        // imprime vetor em arquivo
        // Ex.: v1.Print(fp, titulo, "%7.2lf");
        void Print(FILE *fp, char *tit = 0, char *fmt = 0);

        // fixa valor em vetor
        // Ex.: v1.Set(0.0, 0.5);
        acVector& Set(double v, double inc = 0);

        // zera um vetor
        acVector& SetZero();

        friend void EqualSize(acVector& v1, acVector& v2);

    private:
        void ObjectExists();

    };

double huge *AllocVector(long t);
void   VectorDimTest(long t);

#endif

