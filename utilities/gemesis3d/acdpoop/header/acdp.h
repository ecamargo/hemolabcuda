/* header para o acdpoop */
// 11/10/2005

#ifndef ACDP_H
#define ACDP_H

#ifndef __HUGE__
#define __HUGE__
#define huge
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vtkSystemIncludes.h"
#include "acdpdefs.h"  // definicoes
#include "acdptype.h"  // tipos

/* PROTOTIPOS */

// util
void  VTK_EXPORT gFRead(char *buf, int size, long n, FILE *fp);
void  VTK_EXPORT gFWrite(char *buf, int size, long n, FILE *fp);
void  VTK_EXPORT * mCalloc(long, long);
void  VTK_EXPORT mFree(void *);
void  VTK_EXPORT *mMalloc(long);
void  VTK_EXPORT PrtLine(FILE *, int, int);
void  VTK_EXPORT GetError(int, char *);
int	  VTK_EXPORT FindKeyWord(char *string, FILE *fp);
char  VTK_EXPORT *StrToUpper(char *s);
BOOL  VTK_EXPORT ExistFile(char *s);

// trace
void  VTK_EXPORT StartACDP(int, char *, char *);
void  VTK_EXPORT TraceOn(char *nome);
void  VTK_EXPORT TraceOff(char *nome);
void  VTK_EXPORT Error(int tipo, int num, char *fmt, ...);
void  VTK_EXPORT ExitProg(void);
void  VTK_EXPORT CloseMensFile(void);
FILE  VTK_EXPORT *GetMensFile(void);
int   VTK_EXPORT mStrCmp(void *, void *);

void VTK_EXPORT DebugOn(int *nivel);
void VTK_EXPORT DebugOff(void);
void VTK_EXPORT WDebug(char *fmt, ...);

// tempo
void VTK_EXPORT GetTime(int *, int *, int *, int *);
void VTK_EXPORT GetDate(int *, int *, int *);
void VTK_EXPORT InitTime(void);
void VTK_EXPORT ListTimes(void);
void VTK_EXPORT TimeOn(void);
void VTK_EXPORT TimeOff(char *, int);

//bda
void  VTK_EXPORT CreateDB(char *nofisic, char *pasword);
int   VTK_EXPORT CreateDBName(char *bda, char *file);
void  VTK_EXPORT InitDB(void);
int   VTK_EXPORT OpenDB(char *);
int   VTK_EXPORT OpenDBSE(char *nome, int *err);
void  VTK_EXPORT CloseDB(char *);
FILE  VTK_EXPORT *OpenSave(char *, int, char *);
void  VTK_EXPORT CloseSave(FILE *fp, char *bda);
void  VTK_EXPORT CloseRestore(FILE *fp);
FILE  VTK_EXPORT *OpenRestore(char *nomeobj, int versao, char *bda);
FILE  VTK_EXPORT *OpenRestoreSE(char *nomeobj, int versao, char *bda, int *err);
FILE  VTK_EXPORT *OpenReSave(char *nomeobj, int versao, char *bda);
void  VTK_EXPORT CloseReSave(FILE *fp, char *bda);

// argumentos para o main
char VTK_EXPORT **MakeArgcArgv(int &t, char *nome, char *argv);
void VTK_EXPORT ClearArgs(int t, char *argv[]);

// quick sort
typedef int VTK_EXPORT comparF (const void *, const void *);
void        VTK_EXPORT QSort (void * baseP, long nElem, long width,	comparF *compar);

// Rodrigo - 16/01/2007
// ac3point operations 
float VTK_EXPORT ComputeVectorLength(acPoint3 thys);
float VTK_EXPORT ComputeVectorAngle(acPoint3 first, acPoint3 second);

#endif

