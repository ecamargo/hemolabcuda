#ifndef ACDPDEFS_H
#define ACDPDEFS_H

/* 11-10-2005 */

/* ERROS */
#define WARNING      0
#define COMMON_ERROR 1
#define FATAL_ERROR  2

#define StrEq(x, y)        (strcmp((x), (y)) == 0)
#define StrNCmp(x, y, tam) (strncmp((x), (y), (n)) == 0)
#define StrCat(x, y)       (strcat((x), (y)))
#define StrCmp(x, y)       (strcmp((x), (y)))
#define StrCpy(x, y)       (strcpy((x), (y)))
#define StrLen(x)          (strlen((x)))

#define MAX_TAM_NOME 81

#endif

