/* 11/10/2005 */

#include "vtkSystemIncludes.h"

#ifndef ACDPTYPE_H
#define ACDPTYPE_H

#ifndef BOOLDEF
#define BOOLDEF
typedef int BOOL;
#define TRUE  1
#define FALSE 0
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef ACPOINT2
#define ACPOINT2
struct VTK_EXPORT acPoint2
    {
    double x, y;
    acPoint2(double a = 0, double b = 0) { x = a; y = b; }
    };
#endif

#ifndef ACIPOINT2
#define ACIPOINT2
struct VTK_EXPORT acIPoint2
    {
    int x, y;
    acIPoint2(int a = 0, int b = 0) { x = a; y = b; }
    };
#endif


#ifndef ACLIMIT2
#define ACLIMIT2
struct VTK_EXPORT acLimit2
    {
    double xmin, xmax,
           ymin, ymax;
    acLimit2(double a = 0, double b = 0, double c = 0, double d = 0)
        {
        xmin = a; ymin = b; xmax = c; ymax = d;
        }
    };
#endif

#ifndef ACLIMIT3
#define ACLIMIT3
struct VTK_EXPORT acLimit3
    {
    double xmin, xmax,
           ymin, ymax,
           zmin, zmax;
    acLimit3(double a = 0, double b = 0, double c = 0,
             double d = 0, double e = 0, double f = 0)
        {
        xmin = a; ymin = b; zmin = c;
        xmax = d; ymax = e; zmax = f;
        }
     };
#endif


#ifndef ACPOINT3
#define ACPOINT3
struct VTK_EXPORT acPoint3
    {
    double x, y, z;
    acPoint3(double a = 0, double b = 0, double c = 0) { x = a; y = b; z = c; }
    };
#endif


#ifndef ACLPOINT2
#define ACLPOINT2
struct VTK_EXPORT acLPoint2
    {
    double x, y;
    acLPoint2(double a = 0, double b = 0) { x = a; y = b; }
    };
#endif

#ifndef ACWLIMIT
#define ACWLIMIT
typedef acLimit2 acWlimit;
typedef acLimit2 acVlimit;
typedef acLimit3 acWlimit3;
typedef acLimit3 acVlimit3;
#endif


#ifndef ACCOBUNDL
#define ACCOBUNDL
struct VTK_EXPORT acCobundl
    {
    float x, y, z;
    acCobundl(float a = 0, float b = 0, float c = 0) { x = a; y = b; z = c; }
    };
#endif


#endif  // ACDPTYPE_H

