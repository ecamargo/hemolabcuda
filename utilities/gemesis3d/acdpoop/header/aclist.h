#ifndef ACLIST_H
#define ACLIST_H

#include "vtkSystemIncludes.h"
#include "acdp.h"

/*
** definicao de uma lista generica - Salgado - 08/93
** esta lista pode ser acessada livremente
*/

/*
** Os metodos da classe estao no arquivo acList.CPP
*/

typedef int  (*fcmp) (void *, void *);   // funcao de comparacao
typedef void (*fprt) (FILE *, void *);   // funcao de impressao

/*
** definicao de uma estrutura para representar o no' da lista generica
** Obs.: Tudo e' publico no no' da lista.
**       E' responsabilidade do usuario usar corretamente.
*/

class VTK_EXPORT acNodeList
    {
    public:
        void *GetDados()
            {
            if (this)
                return(dados);
            else
                return(0);
            }

    /*
    ** Este pointer permite andar livremente pela lista.
    ** Por este motivo, deve ser usado com cuidado.
    */
    acNodeList *next;    // pointer para o proximo no'
    void *dados;        // pointer para os dados
    };

class VTK_EXPORT acList
    {
    protected:           // para poder ter herdeiros
        acNodeList *head;         // inicio da lista
        int        sizedados;     // numero de bytes dos dados
        int        sizelist;      // tamanho da lista (num. de nos)
        fcmp       cmp;           // funcao de comparacao
        fprt       prt;           // funcao de impressao
        void       errorcmp();    // erro de comparacao
        void       erroralloc();  // erro de alocacao
        void       errorinit();   // erro de inicializacao
        void       errorprt();    // erro de impressao

    public:

        /********************************************
        * Construtor e destrutor da lista generica *
        ********************************************/

        /*
        ** Construtor
        **
        ** Formas de uso:
        **
        **  acList lista(sizeof(tipo));
        **      Cria lista e armazena tamanho dos dados.
        **      Nao sera' permitida a insercao ordenada ou
        **      pesquisa.
        **
        **  acList lista(sizeof(tipo), func_comp);
        **      Cria lista e armazena tamanho dos dados e
        **      a funcao de comparacao para pesquisa
        **      e insercao ordenada.
        **
        ** acList lista(sizeof(tipo), func_comp, func_prt);
        **      Cria lista e armazena tamanho dos dados e
        **      funcoes de comparacao e impressao.
        **
        ** acList lista(sizeof(tipo), func_prt);
        **      Cria lista e armazena tamanho dos dados e
        **      funcao de impressao.
        */
        acList(int s)
            {
            head = 0;
            sizedados = s;
            cmp = 0;
            prt = 0;
            sizelist = 0;
            }

        acList(int s, fcmp c)
            {
            head = 0;
            sizedados = s;
            cmp = c;
            prt = 0;
            sizelist = 0;
            }

        acList(int s, fcmp c, fprt p)
            {
            head = 0;
            sizedados = s;
            cmp = c;
            prt = p;
            sizelist = 0;
            }

        acList(int s, fprt p)
            {
            head = 0;
            sizedados = s;
            cmp = 0;
            prt = p;
            sizelist = 0;
            }

        /*
        ** construtor sem parametros
        */
        acList()
            {
            head = 0;
            sizedados = 0;
            cmp = 0;
            prt = 0;
            sizelist = 0;
            }

        /*
        ** Destrutor
        **
        ** Deleta os elementos da lista
        */
       ~acList()
            {
            Clear();
            }


        /***************************************
        * Funcoes para inserir dados na lista *
        ***************************************/

        /*
        ** Funcao para inserir um no' sempre no inicio da lista.
        ** Deve-se passar um pointer para os dados a serem inseridos.
        ** 1 -> ok
        ** 0 -> erro
        */
        int InsertFirst(void *a);

        /*
        ** funcao para inserir um no' em uma lista ordenada.
        ** Deve-se passar um pointer para os dados a serem inseridos.
        ** Se inseriu o no' retorna 1.
        ** Se retornar 0 e porque tentou inserir um dado ja' existente.
        ** -1 -> erro
        */
        int InsertSort(void *d);

        /*
        ** Funcao para deletar ou inserir um no'
        ** Se o no' nao existe, insere e retorna 1
        ** Se o no' existe, deleta e retorna 0
        ** -1 -> erro
        */
        int InsDelSort(void *d);


        /*************************************
         * Funcoes para deletar nos da lista *
         *************************************/

        /*
        ** Deleta o primeiro no' de uma lista qualquer
        */
        void DelFirst();

        /*
        ** Deleta um no' de uma lista qualquer.
        ** Se deletou, retorna 1 e em caso contrario retorna 0
        ** -1 -> erro
        */
        int DelNode(void *d);

        /*
        ** Deleta um no' de uma lista ordenada.
        ** Se deletou, retorna 1 e em caso contrario retorna 0
        ** -1 -> erro
        */
        int DelNodeSort(void *d);


        /********************************
        * Funcoes de pesquisa na lista *
        ********************************/

        /*
        ** Pesquisa sequencial em qualquer lista.
        ** Se achou retorna pointer para o no' e em
        ** caso contrario 0.
        */
        void *SearchSeq(void *d);

        /*
        ** Pesquisa sequencial em lista ordenada.
        ** Se achou retorna pointer para o no' e em
        ** caso contrario 0.
        */
        void *SearchSort(void *d);

        /*
        ** Retorna um pointer para o primeiro no' de uma
        ** lista qualquer.
        */
        void *GetFirst()
            {
            if (head)
	            return(head->dados);
            else
            	return(0);
            }


        /**********************
         * Funcoes auxiliares *
         **********************/


        /*
        ** Funcao para limpar uma lista
        */
        void Clear();


        /*
        ** Retorna o numero de nos de uma lista
        */
        int GetSize()
            {
            return sizelist;
            }

        /*
        ** retorna inicio da lista
        */
        acNodeList *GetHead()
            {
            return(head);
            }

        /*
        ** Funcao para inicializar lista sem parametros
        */
        void SetListData(int s)
            {
            head = 0;
            sizedados = s;
            cmp = 0;
            prt = 0;
            sizelist = 0;
            }

        void SetListData(int s, fcmp c)
            {
            head = 0;
            sizedados = s;
            cmp = c;
            prt = 0;
            sizelist = 0;
            }

        void SetListData(int s, fcmp c, fprt p)
            {
            head = 0;
            sizedados = s;
            cmp = c;
            prt = p;
            sizelist = 0;
            }

        void SetListData(int s, fprt p)
            {
            head = 0;
            sizedados = s;
            cmp = 0;
            prt = p;
            sizelist = 0;
            }

        void SetCmpFunc(fcmp c)
            {
            cmp = c;
            }

        /*
        ** funcao para imprimir lista em arquivo
        */
        int Print(FILE *fp);

        /*
        ** funcao para verificar existencia de lista
        */
        int Exist()
            {
            return(sizedados != 0);
            }

    };

#endif // ACLIST_H
