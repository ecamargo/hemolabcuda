#ifndef ACLISTA_H
#define ACLISTA_H

#include "vtkSystemIncludes.h"
#include "aclist.h"

class VTK_EXPORT acListA : public acList
    {
    public:
        acListA(int s) : acList(s) {}
        acListA(int s, fcmp c) : acList(s, c) {}
        acListA(int s, fcmp c, fprt p) : acList(s, c, p) {}
        acListA(int s, fprt p) : acList(s, p) {}
        acListA() : acList() {}

        void *InsertSort(void *d);
        int  InsDelSort(void *d, void **ptr);
        void *DelNodeSort(void *d);
    };

#endif
