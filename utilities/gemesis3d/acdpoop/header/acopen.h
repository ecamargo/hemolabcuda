#ifndef ACOPEN_H
#define ACOPEN_H

#include "vtkSystemIncludes.h"

#include <commdlg.h>
BOOL VTK_EXPORT acOpenDialog(HWND hwnd, char *name, char *defname);

#endif
