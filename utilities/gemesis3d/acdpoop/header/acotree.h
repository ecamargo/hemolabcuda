// 11:40  13/06/1994
// Enzo

/*
** Generic Oc-Tree
*/

#ifndef ACOTREE_H
#define ACOTREE_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "aclist.h"
#include "acdptype.h"

class VTK_EXPORT acNodeOTree
    {
    friend class acOTree;

    private:
        acPoint3 center;
        double size;
        acNodeOTree *ptr[8];
        acList list;
    public:
        int Octant (acPoint3 &pp)
            {
            int oct = 0;
            if (pp.x > center.x)
                oct += 1;
            if (pp.y > center.y)
                oct += 2;
            if (pp.z > center.z)
                oct += 4;
            return oct;
            }
        acPoint3 CenterLeaf (int o)
            {
            acPoint3 NewCenter;
            if (o > 3)
                {
                NewCenter.z = center.z + size / 4.0;
                o -= 4;
                }
            else
                NewCenter.z = center.z - size / 4.0;
            if (o > 1)
                {
                NewCenter.y = center.y + size / 4.0;
                o -= 2;
                }
            else
                NewCenter.y = center.y - size / 4.0;
            if (o > 0)
                NewCenter.x = center.x + size / 4.0;
            else
                NewCenter.x = center.x - size / 4.0;
            return NewCenter;
            }
    };

class VTK_EXPORT acOTree
    {
    protected:
        acNodeOTree *root;
        int SizeObjects;

    public:
        acOTree(acLimit3 &bbox, int sizeObjects);

        ~acOTree()
            {
            FreeOTree(root);
            }

        void FreeOTree (acNodeOTree *ntree);

        void AdjustSize (acPoint3 &pp, double size);

        void InsertData (void *data, acLimit3 &bbox)
            {
            BuildListOct (root, bbox, data);
            }

        acNodeList *GetListHead (acPoint3 &pp);

        void LoadList (acLimit3 bbox, acList *List);

        void LoadListSort (acLimit3 bbox, acList *List);

    private:
        acNodeOTree *CreateNodeOTree (acPoint3 &cc, double size);

        void BuildListOct(acNodeOTree *ntree, acLimit3 &bbox, void *data);

        void LoadListOct (acNodeOTree *ntree, acLimit3 bbox, acList *List);

        void LoadListSortOct (acNodeOTree *ntree, acLimit3 bbox, acList *List);

    };

#endif // ACOTREE_H
