// 11:40  13/06/1994
// Enzo

/*
** Generic Quad Tree
*/

#ifndef ACQTREE_H
#define ACQTREE_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "aclist.h"
#include "acdptype.h"
class acQTreeFrontal;
class VTK_EXPORT acNodeQTree
    {
    friend class acQTree;
    friend class acQTreeFrontal;

    private:
	acPoint2 center;
	double size;
	acNodeQTree *ptr[4];
	acList list;
    public:
	int Quadrant (acPoint2 &pp)
	    {
	    int quad = 0;
	    if (pp.x > center.x)
		quad += 1;
	    if (pp.y > center.y)
		quad += 2;
	    return quad;
	    }
	acPoint2 CenterLeaf (int q)
	    {
	    acPoint2 NewCenter;
	    if (q > 1)
		{
		NewCenter.y = center.y + size / 4.0;
		q -= 2;
		}
	    else
		NewCenter.y = center.y - size / 4.0;
	    if (q > 0)
		NewCenter.x = center.x + size / 4.0;
	    else
		NewCenter.x = center.x - size / 4.0;
	    return NewCenter;
	    }
    };

class VTK_EXPORT acQTree
    {
    protected:
	acNodeQTree *root;
	int SizeObjects;
    int (*CmpFunc) (void *, void *);

    public:
	acQTree(acLimit2 &bbox, int sizeObjects, int (*cmpf) (void *, void *) = 0);

	~acQTree()
	    {
	    FreeQTree(root);
	    }

	void FreeQTree (acNodeQTree *ntree);

	void AdjustSize (acPoint2 &pp, double size);

	void InsertData (void *data, acLimit2 &bbox)
	    {
	    BuildListQuad (root, bbox, data);
	    }

	acNodeList *GetListHead (acPoint2 &pp);

	void LoadList (acLimit2 bbox, acList *List);

	void LoadListSort (acLimit2 bbox, acList *List);

    protected:
	acNodeQTree *CreateNodeQTree (acPoint2 &cc, double size);

//	int Quadrant (acPoint2 &pp, acPoint2 &center);

//	acPoint2 CenterLeaf (acPoint2 &center, double size, int q);

	void BuildListQuad(acNodeQTree *ntree, acLimit2 &bbox, void *data);

	void LoadListQuad (acNodeQTree *ntree, acLimit2 bbox, acList *List);

	void LoadListSortQuad (acNodeQTree *ntree, acLimit2 bbox, acList *List);

    };

#endif // ACQTREE_H
