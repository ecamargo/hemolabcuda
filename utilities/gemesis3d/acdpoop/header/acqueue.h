#ifndef ACQUEUE_H
#define ACQUEUE_H

#include "vtkSystemIncludes.h"
#include "acdp.h"

/*
** definicao de uma fila generica - Salgado - 08/93
*/

class VTK_EXPORT acNodeQueue
    {
    friend class acQueue;

    private:
        acNodeQueue *next;    // pointer para o proximo no'
        void        *dados;   // pointer para os dados
    };


class VTK_EXPORT acQueue
    {
    protected:
        acNodeQueue *Head;             // inicio da fila
        acNodeQueue *Tail;             // final da fila
        int         SizeDados;         // numero de bytes dos dados

    public:

        /*******************************************
         * Construtor e destrutor da fila generica *
         *******************************************/

        /*
        ** Construtor
        **
        ** Formas de uso:
        **
        **  acQueue Stack;
        **      Cria sem nada.
        **
        **  acQueue Stack(sizeof(tipo));
        **      Cria fila e armazena tamanho dos dados.
        **
        */
        acQueue()
            {
            Head = Tail = 0;
            SizeDados = 0;
            }

        acQueue(int s)
            {
            Head = Tail = 0;
            SizeDados = s;
            }

        /*
        ** Destrutor
        **
        ** Deleta os elementos da fila
        */
       ~acQueue()
            {
            Clear();
            }

        /*
        ** Funcao para inserir um no'.
        ** Deve-se passar um pointer para os dados a serem inseridos.
        ** 1 -> inseriu
        */
        int Push(void *a);

        /*
        ** Funcao para retirar um no'.
        ** Deve-se passar um pointer para uma area existente
        ** 1 -> retirou
        */
        int Pop(void *a);

        /*
        ** Faz uma copia do dado no topo da fila
        ** Deve-se passar um pointer para uma area existente
        ** Erros = 0 - fila vazia
        */
        int Copy(void *a);

        /*
        ** Funcao para limpar uma fila
        */
        void Clear();

        /*
        ** Funcao para inicializar fila sem parametros
        */
        void SetStackData(int s)
            {
            Head = Tail = 0;
            SizeDados = s;
            }

        /*
        ** funcao para verificar existencia de fila
        */
        int Exist()
            {
            return(Head != 0);
            }
    };

#endif
