#ifndef ACSTACK_H
#define ACSTACK_H

#include "vtkSystemIncludes.h"
#include "acdp.h"

/*
** definicao de uma pilha generica - Salgado - 08/93
*/

/*
** Os metodos da classe estao no arquivo acStack.CPP
*/

class VTK_EXPORT acNodeStack
    {
    friend class acStack;

    private:
        acNodeStack *next;    // pointer para o proximo no'
        void        *dados;   // pointer para os dados
    };

class VTK_EXPORT acStack
    {
    protected:
        acNodeStack *Head;             // inicio da pilha
        int         SizeDados;         // numero de bytes dos dados

    public:

        /********************************************
         * Construtor e destrutor da pilha generica *
         ********************************************/

        /*
        ** Construtor
        **
        ** Formas de uso:
        **
        **  acStack Stack;
        **      Cria sem nada.
        **
        **  acStack Stack(sizeof(tipo));
        **      Cria pilha e armazena tamanho dos dados.
        **
        */
        acStack()
            {
            Head = 0;
            SizeDados = 0;
            }

        acStack(int s)
            {
            Head = 0;
            SizeDados = s;
            }

        /*
        ** Destrutor
        **
        ** Deleta os elementos da pilha
        */
       ~acStack()
            {
            Clear();
            }

        /*
        ** Funcao para inserir um no'.
        ** Deve-se passar um pointer para os dados a serem inseridos.
        ** Erros =  0 - nao alocou memoria
        **       = -1 - nao inicializada (sem tamanho dos dados)
        */
        int Push(void *a);

        /*
        ** Funcao para retirar um no'.
        ** Deve-se passar um pointer para uma area existente
        ** Erros = 0 - pilha vazia
        */
        int Pop(void *a);

        /*
        ** Faz uma copia do dado no topo da pilha
        ** Deve-se passar um pointer para uma area existente
        ** Erros = 0 - pilha vazia
        */
        int Copy(void *a);

        /*
        ** Funcao para limpar uma pilha
        */
        void Clear();

        /*
        ** Funcao para inicializar pilha sem parametros
        */
        void SetStackData(int s)
            {
            Head = 0;
            SizeDados = s;
            }

        /*
        ** funcao para verificar existencia de pilha
        */
        int Exist()
            {
            return(Head != 0);
            }
    };

#endif
