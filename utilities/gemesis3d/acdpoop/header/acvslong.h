/*
** acVSLong.h - Salgado - 08/97
*/
#ifndef ACVSLONG_H
#define ACVSLONG_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "acdptype.h"

const int NumColsVet = 1000;
const int StepVet   =  500;

struct acVSLongInfo
    {
    long l;
    long c;
    };

typedef long *PLONG;

class VTK_EXPORT acVSLong
    {
    protected:
        long TotalVetores;
	    long NNodes;
	    acVSLongInfo FirstFree;
	    PLONG *V;
        long *plin;


    public:
    	acVSLong () 
            {
            NNodes = 0;
            TotalVetores = 1000;
            V = new PLONG[1000];    // aloca vetor
            if (!V)
                ErrMem();

            V[0] = new long[NumColsVet]; // aloca prim. sub-vetor
            if (!V[0])
                ErrMem();

            plin = V[0];      // auxiliar para Put
            FirstFree.l = 0;  // primeira posicao livre
            FirstFree.c = 0;
            NNodes      = 0;  // nao tem nenhum no
            }

	
	   ~acVSLong();

		long GetNumNodes() { return(NNodes); }

        long Get(long n)
            {
	        long l = n / NumColsVet;
            long c = n % NumColsVet;
            long *val = V[l] + c;
            return (*val);
            }

        void Put(long val)
            {
            if (FirstFree.c < NumColsVet)
	            {
                *plin++ = val;
                FirstFree.c++;
                NNodes++;
                }
            else          // passa para outra linha
                AjustaVetor(val);
            }

        void AjustaVetor(long val);

	    void Print(FILE *fp, int npc, BOOL = TRUE);

	    // Memory Error
	    void ErrMem()
	        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
    };

#endif

