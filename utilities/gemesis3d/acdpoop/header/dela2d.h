#ifndef DELA2D_H
#define DELA2D_H

#include "vtkSystemIncludes.h"
#include "coord2d.h"

class VTK_EXPORT Tri3Dela
    {
    public:
//               n3
//              /\
//            /   \
//        v3/      \ v2
//        /         \
//    n1/------------\n2
//            v1

        long n1,n2,n3;
        Tri3Dela *v1,*v2,*v3;
        Tri3Dela *next,*prev;

    public:
        Tri3Dela() {next=prev=NULL;}
    };

class VTK_EXPORT Dela2D
    {
    private:
        Tri3Dela *curr, *currv, *first;
        acCoord_2D *Coord;
        long NumElems;

    public:
        Dela2D() {curr = first = NULL; NumElems = 0;}
        Dela2D(acCoord_2D *coord);
       ~Dela2D();
        void SetCoordinates(acCoord_2D *coord);
        void Generate();
        void Print(FILE *fp);
        void Save(FILE *fp);
        long GetNumElems() { return NumElems; }
        long GetNumNodes() { return Coord ? Coord->GetNumNodes() : 0; }

    private:
        int  Conviene(Tri3Dela *el, Tri3Dela *elv);
        void RotaElElv(Tri3Dela *el, Tri3Dela *elv);
        void FindEl(acPoint2 &p);
        void DivideEl(long node);
        void Divide3(long node);
        void Divide4(long node);
        void SwapDiag(Tri3Dela *el, Tri3Dela *elv);
        void GenBox();

    };
#endif

