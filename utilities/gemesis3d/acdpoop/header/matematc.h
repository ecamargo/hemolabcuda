/* last modification : 11/24/93 : 15:30 */
/*
** matematc.h - Roberto, Adriana e Salgado - julho / 93
*/

#ifndef MATEMATC_H
#define MATEMATC_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "acdptype.h"

/*
** prototypes
*/
double   VTK_EXPORT AreaTri(double  *x, double  *y);
double   VTK_EXPORT AreaTri(acPoint2  *v);
acPoint2 VTK_EXPORT CenterTri(double  *x, double  *y);
acPoint2 VTK_EXPORT CenterTri(acPoint2  *v);
BOOL     VTK_EXPORT IsInPolygon(acPoint2 &p, int n, acPoint2  *v);
void     VTK_EXPORT minmaxd(double  *v, long tam, double *min, double *max);
void     VTK_EXPORT minmaxf(float  *v, long tam, float *min, float *max);
void     VTK_EXPORT pmatbsa(double  *mc, double  *ma, double  *mbs, long int nlina, long int ncola);
acLimit2 VTK_EXPORT GetLimit(acPoint2  *p, long tam);
acLimit3 VTK_EXPORT GetLimit3(acPoint3  *p, long tam);
double   VTK_EXPORT norinfv(double  *u, long dim);
double   VTK_EXPORT normaev(double  *u, long dim);
double   VTK_EXPORT scalv(double  *v1, double  *v2, long n);
double   VTK_EXPORT mind(double  *v, long tam);
double   VTK_EXPORT maxd(double  *v, long tam);
void     VTK_EXPORT addvet(double  *c, double  *a, double  *b, long tam);
void     VTK_EXPORT subtvet(double  *c, double  *a, double  *b, long n);
void     VTK_EXPORT chsvet(double  *v, long n);
void     VTK_EXPORT dcopyv(double  *d, double  *s, long tam);
void     VTK_EXPORT prodmat(double  *mc, double  *ma, double  *mb,long int nlina, long int ncola, long int ncolb);
void     VTK_EXPORT tranmat(double  *mat, double  *ma, long int nlin, long int ncol);
void     VTK_EXPORT dzerov(double  *v, long tam);
int      VTK_EXPORT mgsstsk(double  *ma, long  *maxa, long neq, long *step);
void     VTK_EXPORT solgssk(double  *vetx, double  *ma, long  *maxa, double  *vetb, long neq);
int      VTK_EXPORT gaussk(double  *vetx, double  *ma, long  *maxa, double  *vetb, long int neq, double delta, long int *step);
void     VTK_EXPORT prodmsv(double  *vetc, double  *ms, double  *vet,long int ncolms);
void     VTK_EXPORT prodmatv(double  *u, double  *m, double  *v, long nfil, long ncol);
int      VTK_EXPORT SolSymSkyLAs(double * ma, long * maxa, long neq,int FlagSol,     long &eldiag,    double * F,double * U,  double eps); 

/*******************************************************************/

void   VTK_EXPORT addmat(double  *mc, double  *ma, double  *mb,  long int nlin, long int ncol);
void   VTK_EXPORT addmats(double  *mcs, double  *mas, double  *mbs, long int ncolms);
int    VTK_EXPORT compdbl(double *r, double *s);
int    VTK_EXPORT compflt(float *r, float *s);
int    VTK_EXPORT compint(int *r, int *s);
void   VTK_EXPORT copmuv(double  *v, double  *u, long n);
void   VTK_EXPORT cosxl(double xyz[], int m, double *cx, double *cy, double *cz, double *xl);
void   VTK_EXPORT cosxl2(double xyz[], int m, double *cx, double *cy, double *xl);
void   VTK_EXPORT dbvetor(char *tit, char *tipo, void  *v, long nelem);
void   VTK_EXPORT diag(double  *mat, double val, long nlin);
void   VTK_EXPORT dlistav(char *titulo, double  *v, long n, FILE *fp);
void   VTK_EXPORT icopyv(int  *d, int  *s, long n);
void   VTK_EXPORT invetsq(long  *v, long inic, long tam);
long   VTK_EXPORT ipos(long i, long j, long nlin);
void   VTK_EXPORT lcopyv(long  *d, long  *s, long n);
void   VTK_EXPORT levtflt(float  *v, long tam, FILE *fp);
void   VTK_EXPORT levtint(int  *v, long tam, FILE *fp);
void   VTK_EXPORT lvetarq(char *tit, int tipo, void  *v, long nelem, FILE *lw);
float  VTK_EXPORT maxf(float  *v, long tam);
float  VTK_EXPORT minf(float  *v, long tam);
double VTK_EXPORT noreucm(double  *ma, long int nlin, long int ncol);
double VTK_EXPORT noreums(double  *mas, long int ncolms);
double VTK_EXPORT norinms(double  *ms, long int ncolms);
double VTK_EXPORT norminm(double  *ma, long int nlin, long int ncol);
void   VTK_EXPORT printmat(double  *matriz, char *titulo, int nlin, int ncol, FILE *fi);
void   VTK_EXPORT prmatba(double  *mc, double  *ma, double  *mb, long int nlina, long int ncola);
void   VTK_EXPORT prodmskv(double  *vetc, double  *sk, long  *maxa,double  *vetb, long ncolmsk);
void   VTK_EXPORT promabs(double  *mc, double  *ma, double  *mbs, long int nlina, long int ncola);
void   VTK_EXPORT promatb(double  *mc, double  *ma, double  *mb, long int nlina, long int ncola, long int ncolb);
void   VTK_EXPORT  promats(double  *mc, double  *mas, double  *mbs, long int ncolms);
double VTK_EXPORT pscalm(double  *ma, double  *mb, long int nlin, long int ncol);
double VTK_EXPORT pscalms(double  *mas, double  *mbs, long int ncolms);
void   VTK_EXPORT ptensv(void  *mat, double  *u, double  *v, long n);
void   VTK_EXPORT realpm(double  *kma, double k, double  *ma,long int nlin, long int ncol);
void   VTK_EXPORT realpms(double  *kms, double k, double  *ms, long int ncolms);
void   VTK_EXPORT rpvetd(double  *sv, double  *v, double s, long n);
void   VTK_EXPORT rpvetf(float  *alfav, double alfa, float  *v, long n);
void   VTK_EXPORT swapi(int *i, int *j);

void   VTK_EXPORT vetaddf(float  *vsoma, float  *v1, float  *v2, long n);
acPoint3 VTK_EXPORT NormalTri(acPoint3  *v);
acPoint3 VTK_EXPORT CenterTri(acPoint3  *v);

/*
** Resolucao de sistemas de equacoes lineares
*/
int    VTK_EXPORT gausspp(double  *vetx, double  *ma, double  *vetb,  long int neq, double delta, long int *step);
int    VTK_EXPORT mgausst(double  *ma, long int neq, long int *step);
int    VTK_EXPORT mitgsl(double  *vetx, double  *ma, double  *vetb, long int neq, long int itmax, double eps, double beta, int codconvg);
int    VTK_EXPORT mitgslas(double  *vetx, double  *mas, double  *vetb,long int neq, long int itmax, double eps, double beta, int codconvg);
int    VTK_EXPORT mitgslsk(double  *vetx, double  *sk, int  *maxa, double  *vetb, long int neq, 
								long int itmax, double eps, double beta, int codconvg);
int    VTK_EXPORT grcjprsk(double  *vetx, double  *ma, long int  *maxa,
                double  *veth, long int neq, long int itmax, double eps,
                double omega, int codpr, int codcvg);
int    VTK_EXPORT gcpdsk(double  *vetx, double  *ma, long int  *maxa,
              double  *veth, long int neq, long int itmax, double eps, int codcvg);
int    VTK_EXPORT gcpds(double  *vetx, double  *ma, double  *veth, long int neq,
             long int itmax, double eps, int codcvg);
int    VTK_EXPORT gradcjn(double  *vetx, double  *ma, double  *veth,
               long int neq, long int itmax, double eps, int codcvg);
int    VTK_EXPORT grcjnsk(double  *vetx, double  *ma, long int  *maxa,
               double  *veth, long int neq, long int itmax, double eps,
               int codcvg);
int    VTK_EXPORT gradcjst(double  *vetx, double  *ma, double  *veth,
                long int neq, long int itmax, double eps, int codcvg);
int    VTK_EXPORT grcjprc(double  *vetx, double  *ma, double  *veth,
               long int neq, long int itmax, double eps, double  omega,
               int codpr, int codcvg);
int    VTK_EXPORT grcjstsk(double  *vetx, double  *ma, long int  *maxa,
                double  *veth, long int neq, long int itmax,
                double eps, int codcvg);
int    VTK_EXPORT gaussim(double  *vetx, double  *mas, double  *vetb,
               double delta, long int neq, long int *step);
int    VTK_EXPORT mgausimt(double  *mas, long int neq, long int *step);
void   VTK_EXPORT solgasim(double  *vetx, double  *mas, double  *vetb, long int neq);
void   VTK_EXPORT solgauss(double  *vetx, double  *ma, double  *vetb,long int neq);


#endif /* MATEMATC_H */

