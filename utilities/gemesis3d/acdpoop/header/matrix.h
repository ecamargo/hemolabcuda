#ifndef MATRIX_H
#define MATRIX_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "matematc.h"

class acSymMatrix;

class VTK_EXPORT acMatrix
    {
    protected:
        double huge *M;  // ponteiro para a area alocada
        long   Rows;     // linhas
        long   Cols;     // colunas


    public:
        /****************************
         * construtores e destrutor *
         ****************************/

        // Cria Matriz
        // Ex.: acMatrix m;
        acMatrix() { M = 0; Rows = Cols = 0; }

        // Cria matrix com tamanho 
        // Ex.: acMatrix m(10, 2);
        acMatrix(long r, long c);

        // "copy-initializer"
        // cria matriz temporaria
        // permite inicializar uma matriz com outra
        // Ex.: acMatrix m1(m2);
        acMatrix(acMatrix& m2);

        // Destrutor
       ~acMatrix() { Free(); }


        // Fornece dimensao para uma matriz ja' existente
        void SetDim(long r, long c);


        /**************
         * operadores *
         **************/

        // indexador
        // Ex.: ma(i, j) = 1.5;
        //      r = ma(i, j);
        double huge & operator()(long i, long j)
            {
            return (M[i * Cols + j]);
            }


        // igualdade
        // Ex.: ma = mb;
        //      ma.Equal(mb);
        acMatrix& operator=(acMatrix& mb);
        acMatrix& Equal(acMatrix& vb);


        // muda o sinal dos elementos
        // Ex.: m1.Chs();
        acMatrix& Chs();
        acMatrix& operator-();


        // multiplica uma matriz por um double
        // Ex.: m *= 1.5;
        //      m.Prod(1.5);      
        acMatrix& operator*=(double);
        acMatrix& Prod(double);


        // multiplica duas matrizes
        // Ex.: m = m1 * m2;
        //      m.Prod(m1, m2);
        //      m = m1 * sm2;
        //      m.Prod(m1, sm2);
        //      m = sm * m2;
        //      m.Prod(sm, m2);
        //      m = sm1 * sm2;
        //      m.Prod(sm1, sm2);
        friend acMatrix operator*(acMatrix& m1, acMatrix& m2);
        acMatrix& Prod(acMatrix& m1, acMatrix& m2);
        
        friend acMatrix operator*(acMatrix& m1, acSymMatrix& m2);
        acMatrix& Prod(acMatrix& m1, acSymMatrix& m2);
        
        friend acMatrix operator*(acSymMatrix& m1, acMatrix& m2);
        acMatrix& Prod(acSymMatrix& m1, acMatrix& m2);
        
        friend acMatrix operator*(acSymMatrix& m1, acSymMatrix& m2);
        acMatrix& Prod(acSymMatrix& m1, acSymMatrix& m2);

        
        // calcula produto escalar entre matrizes
        // Ex.: r = m1 ^ m2;
        //      r = m1.Scal(m2);
        double operator^(acMatrix& m2);
        double Scal(acMatrix& m2);    


        // soma os elementos de duas matrizes
        // Ex.: m1 = m2 + m3;
        //      m1.Add(m2, m3);
        //      m1 += v2;
        //      m1.Add(v2);
        friend acMatrix operator+(acMatrix& v2, acMatrix& v3);
        acMatrix& Add(acMatrix& v2, acMatrix& v3);
        
        acMatrix& operator+=(acMatrix &v2);
        acMatrix& Add(acMatrix &v2);


        // subtrai os elementos de duas matrizes
        // Ex.: m1 = m2 - m3;
        //      m1.Subt(m2, m3);
        //      m1 -= m2;
        //      m1.Subt(m2);
        friend acMatrix operator-(acMatrix& m2, acMatrix& m3);
        acMatrix& Subt(acMatrix& m2, acMatrix& m3);
        
        acMatrix& operator-=(acMatrix &m2);
        acMatrix& Subt(acMatrix &m2);


        
        /**********************
         * funcoes auxiliares *
         **********************/

         // libera area
         void Free();

         // transposta
         acMatrix& Transposed(acMatrix& m);

         // numero de linhas e colunas
         long GetRows() { return Rows; }
         long GetCols() { return Cols; }
         void GetDim(long &r, long &c) { r = Rows; c = Cols; }

        // retorna ponteiro
        double huge *GetMatrixPtr() { return M; }

        // retorna maior e menor valores
        double GetMax() { return maxd(M, Rows * Cols); }
        double GetMin() { return mind(M, Rows * Cols); }

        // grava matriz (binario)
        // Ex.: m1.Save("matrix1", 1, "matbda");
        //      m1.Save(fp);
        void Save(char *objname, int version, char *filename);
        void Save(FILE *);

        // le matrix (binario)
        // Ex.: m1.Restore("matrix1", 1, "matbda");
        //      m1.Restore(fp);
        void Restore(char *objname, int version, char *filename);
        void Restore(FILE *);

        // imprime matriz em arquivo
        // Ex.: m1.Print(fp, titulo, "%7.2lf");
        void Print(FILE *fp, char *tit = 0, char *fmt = 0);

        // fixa valor em uma matriz
        // Ex.: m1.Set(0.0, 0.5);
        acMatrix& Set(double v, double inc = 0);

        // norma infinita
        double InfinityNorm();
        
        // norma "One"
        double OneNorm();

        friend void EqualDim(acMatrix& m1, acMatrix& m2);   

    private:
        void ObjectExists();

    };

double huge *AllocMatrix(long l, long c);
void   MatrixDimTest(long l, long c);

#endif

