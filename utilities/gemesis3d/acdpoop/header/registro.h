#ifndef REGISTRO_H
#define REGISTRO_H

#include <afxwin.h>

void Calcula(char *nome, char *prog, int tipo, char *result);
void FazRegistro(char *Argv[], float *paux, CString &, double *);
int  SetKernel(CString &UserName, CString &UserNumber, CString &ProgramName, CString &Version,
              float *paux, CString &RegisteredTo, double *statusregok);
void GetKernel(CString &rt, float *paux, double *statusregok);

#endif
