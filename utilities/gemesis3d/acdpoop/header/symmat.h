#ifndef SYMMAT_H
#define SYMMAT_H

#include "vtkSystemIncludes.h"
#include "acdp.h"
#include "matematc.h"

class acMatrix;

class VTK_EXPORT acSymMatrix
    {
    protected:
        double huge *SM;  // ponteiro para a area alocada
        long   Rows;      // numero linhas
        long   NElem;     // numero de elementos


    public:
        /****************************
         * construtores e destrutor *
         ****************************/

        // Cria Matriz
        // Ex.: acSymMatrix m;
        acSymMatrix() { SM = 0; NElem = Rows = 0; }

        // Cria matrix com numero de linhas ou colunas 
        // Ex.: acSymMatrix m(10);
        acSymMatrix(long rc);

        // "copy-initializer"
        // cria matriz temporaria
        // permite inicializar uma matriz com outra
        // Ex.: acSymMatrix m1(m2);
        acSymMatrix(acSymMatrix& m2);

        // Destrutor
       ~acSymMatrix() { Free(); }
       
       
        // Fornece dimensao para uma matriz ja' existente
        // Ex.: sm.SetDim(10);
        void SetDim(long rc);


        /**************
         * operadores *
         **************/

        // indexador
        // Ex.: ma(i, j) = 1.5;
        //      r = ma(i, j);
        double huge & operator()(long i, long j)
            {
            return SM[GetPos(i,j)];
            }

        // igualdade
        // Ex.: ma = mb;
        //      ma.Equal(mb);
        acSymMatrix& operator=(acSymMatrix& mb);
        acSymMatrix& Equal(acSymMatrix& vb);

        // muda o sinal dos elementos
        // Ex.: m1.Chs();
        acSymMatrix& Chs();
        acSymMatrix& operator-();

        // multiplica uma matriz por um double
        // Ex.: m *= 1.5;
        //      m.Prod(1.5);      
        acSymMatrix& operator*=(double);
        acSymMatrix& Prod(double);

        // multiplica Mt * SM * M
        // Ex.: sm1.Prod_MtSM(m, sm2);       
        acSymMatrix& Prod_MtSM(acMatrix &m, acSymMatrix& sm2);
        

        // calcula produto escalar entre matrizes
        // Ex.: r = m1 ^ m2;
        //      r = m1.Scal(m2);
        double operator^(acSymMatrix& m2);
        double Scal(acSymMatrix& m2);

        // soma os elementos de duas matrizes
        // Ex.: m1 = m2 + m3;
        //      m1.Add(m2, m3);
        //      m1 += m2;
        //      m1.Add(m2);
        friend acSymMatrix operator+(acSymMatrix& m2, acSymMatrix& m3);
        acSymMatrix& Add(acSymMatrix& m2, acSymMatrix& m3);
                
        acSymMatrix& operator+=(acSymMatrix &m2);
        acSymMatrix& Add(acSymMatrix &m2);


        // subtrai os elementos de duas matrizes
        // Ex.: m1 = m2 - m3;
        //      m1.Subt(m2, m3);
        //      m1 -= m2;
        //      m1.Subt(m2);
        friend acSymMatrix operator-(acSymMatrix& m2, acSymMatrix& m3);
        acSymMatrix& Subt(acSymMatrix& m2, acSymMatrix& m3);
        
        acSymMatrix& operator-=(acSymMatrix &m2);
        acSymMatrix& Subt(acSymMatrix &m2);


        
        /**********************
         * funcoes auxiliares *
         **********************/

         // libera area
         void Free();


         // numero de linhas e colunas
         long GetRows()  { return Rows; }
         long GetCols()  { return Rows; }
         long GetNElem() { return NElem; }


        // retorna ponteiro
        double huge *GetSymMatrixPtr() { return SM; }

        // retorna maior e menor valores
        double GetMax() { return maxd(SM, NElem); }
        double GetMin() { return mind(SM, NElem); }
        

        // grava matriz (binario)
        // Ex.: m1.Save("matrix1", 1, "matbda");
        //      m1.Save(fp);
        void Save(char *objname, int version, char *filename);
        void Save(FILE *);

        // le matrix (binario)
        // Ex.: m1.Restore("matrix1", 1, "matbda");
        //      m1.Restore(fp);
        void Restore(char *objname, int version, char *filename);
        void Restore(FILE *);

        // imprime matriz em arquivo
        // Ex.: m1.Print(fp, "matrix 1", "%7.2lf");
        void Print(FILE *fp, char *tit = 0, char *fmt = 0);

        // fixa valor em uma matriz
        // Ex.: m1.Set(0.0, 0.5);
        acSymMatrix& Set(double v, double inc = 0);

        // norma infinita
        double InfinityNorm();
        
        // norma "One"
        double OneNorm();

        friend void EqualDim(acSymMatrix& m1, acSymMatrix& m2);   

    private:
        void ObjectExists();

        long GetPos(long i, long j)
            {
            return ((i < j) ?
                    (i * (2 * Rows - (i + 1)))/2 + j :
                    (j * (2 * Rows - (j + 1)))/2 + i);
            }

    };

double huge *AllocSymMatrix(long d);
void   SymMatrixDimTest(long d);

#endif

