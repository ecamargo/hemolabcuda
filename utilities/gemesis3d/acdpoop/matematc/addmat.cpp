/*
 **************************************************************************
 *                                                                        *
 * void addmat(mc, ma, mb, nlin, ncol)                                    *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Calcular a soma de duas matrizes do tipo double armazenadas por   *
 *      linhas.                                                           *
 * (02) Forma de utilizacao:                                              *
 *      addmat(mc, ma, mb, nlin, ncol);                                   *
 * (03) Descricao:                                                        *
 *      Calcula a soma dos elementos de duas matrizes do tipo double      *
 *      armazenadas por linhas.                                           *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      soma                                                              *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, setembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumantos:                                                       *
 *      -> *ma  - pointer para a matriz 'ma'              - double huge   *
 *      -> *mb  - pointer para a matriz 'mb'              - double huge   *
 *      <- *mc  - pointer para a matriz resultado         - double huge   *
 *      -> nlin - numero de linhas de cada matriz         - long          *
 *      -> ncol - numero de colunas de cada matriz        - long          *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void addmat(double huge *mc, double huge *ma, double huge *mb,    *
 *                  long int nlin, long int ncol);                        *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula a soma matricial
*/

void addmat(double huge *mc, double huge *ma, double huge *mb,
            long int nlin,   long int ncol)
    {
    register long int nelem;        /* numero de elementos de cada matriz */

    nelem = nlin * ncol;

    while (nelem--)
         *mc++ = (*ma++) + (*mb++);
    }
