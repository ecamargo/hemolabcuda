/*
 **************************************************************************
 *                                                                        *
 * void addmats(mcs, mas, mbs, ncolms)                                    *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Calcular a soma de duas matrizes simetricas do tipo double        *
 *      armazenadas na forma supradiagonal por linhas.                    *
 * (02) Forma de utilizacao:                                              *
 *      addmats(mcs, mas, mbs, ncolms);                                   *
 * (03) Descricao:                                                        *
 *      Calcula a soma dos elementos de duas matrizes simetricas do tipo  *
 *      double armazenadas por linhas resultando uma matriz simetrica     *
 *      do tipo double armazenada na forma supradiagonal por linhas.      *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      soma                                                              *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, setembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumantos:                                                       *
 *      -> *mas   - pointer para a matriz simetrica 'mas'  - double huge  *
 *      -> *mbs   - pointer para a matriz simetrica 'mbs'  - double huge  *
 *      <- *mcs   - pointer para a matriz resultado        - double huge  *
 *      -> ncolms - numero de colunas de cada matriz       - long         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void addmats(double huge *mcs, double huge *mas, double huge *mbs,*
 *                   long int ncolms);                                    *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula a soma matricial
*/

void addmats(double huge *mcs, double huge *mas, double huge *mbs,
             long int ncolms)
    {
    register long int nelem;        /* numero de elementos da matriz */

    nelem = ((ncolms + 1) * ncolms)/2;

    while (nelem--)
         *mcs++ = (*mas++) + (*mbs++);
    }
