/*
 **************************************************************************
 *                                                                        *
 * void addvet                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Adicionar dois vetores a e b, armazenando sua soma em c.          *
 * (02) Forma de utilizacao:                                              *
 *      addvet(c, a, b, tam);                                             *
 * (03) Descricao:                                                        *
 *      Adiciona dois vetores do tipo double huge, de dimensao 'tam',     *
 *      armazenados nas tabelas 'a' e 'b', sendo sua soma guardada em     *
 *      'c' , ou seja:    c = a + b                                       *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      adicao                                                            *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *c   -  pointer para vetor que contem resultado - double huge  *
 *      -> *a   -  pointer para vetor operando - double huge              *
 *      -> *b   -  pointer para vetor operando - double huge              *
 *      -> tam  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void addvet(double huge *c, double huge *a, double huge *b,       *
 *                  long tam);                                            *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void addvet(double huge *c, double huge *a, double huge *b, long tam)
    {
    while (tam--)
        *c++ = (*a++) + (*b++);
    }
