// calcula area de um triangulo

#include "matematc.h"

double AreaTri(double huge *x, double huge *y)
    {
    double x10, x20, y10, y20, nz;
    x10 = x[1] - x[0];
    x20 = x[2] - x[0];
    y10 = y[1] - y[0];
    y20 = y[2] - y[0];
    nz  = x10 * y20 - x20 * y10;
    return (0.5 * nz);
    }

double AreaTri(acPoint2 huge *v)
    {
    double x10, x20, y10, y20, nz;
    x10 = v[1].x - v[0].x;
    x20 = v[2].x - v[0].x;
    y10 = v[1].y - v[0].y;
    y20 = v[2].y - v[0].y;
    nz  = x10 * y20 - x20 * y10;
    return (0.5 * nz);
    }
