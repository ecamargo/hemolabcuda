// calcula centro de um triangulo

#include "matematc.h"

acPoint2 CenterTri(double huge *x, double huge *y)
    {
    acPoint2 center;
    center.x = (x[0] + x[1] + x[2]) / 3.0;
    center.y = (y[0] + y[1] + y[2]) / 3.0;
    return(center);
    }

acPoint2 CenterTri(acPoint2 huge *v)
    {
    acPoint2 center;
    center.x = (v[0].x + v[1].x + v[2].x) / 3.0;
    center.y = (v[0].y + v[1].y + v[2].y) / 3.0;
    return(center);
    }
