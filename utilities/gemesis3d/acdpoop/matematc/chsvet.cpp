/*
 **************************************************************************
 *                                                                        *
 * void chsvet                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Inverter o sinal de um vetor.                                     *
 * (02) Forma de utilizacao:                                              *
 *      chsvet(v, n)                                                      *
 * (03) Descricao:                                                        *
 *      Inverte o sinal do vetor 'v' do tipo double huge, cuja dimensao   *
 *      e' dada por 'n'.                                                  *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 *      troca de sinal                                                    *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 6 de setembro de 1988,                      *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'.             *
 * (07) Argumentos:                                                       *
 *      <> *v - pointer para vetor - double huge                          *
 *      -> n  - dimensao do vetor  - long                                 *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void chsvet(double huge *v, long n);                              *
 * (11) Headers necessarios:                                              *
 *      stdio.h,  matematc.h                                              *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void chsvet(double huge *v, register long n)
    {
    while (n--)
        {
        *v = -*v;
         v++;
         }
     }
