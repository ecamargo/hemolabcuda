/* 
** compara dois doubles
*/

#include "matematc.h"

int compdbl(double *i, double *j)
    {
    if (*i == *j)
        return(0);
    else if (*i < *j)
        return(-1);
    else
        return(1);
    }
