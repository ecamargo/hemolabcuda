/*
** compara dois floats
*/

#include "matematc.h"

int compflt(float *i, float *j)
    {
    if (*i == *j)
        return(0);
    else if (*i < *j)
        return(-1);
    else
        return(1);
    }
