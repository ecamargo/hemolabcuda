/*
 **************************************************************************
 *                                                                        *
 * void copmuv                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Copiar um vetor do tipo double huge em outro, trocando o sinal.   *
 * (02) Forma de utilizacao:                                              *
 *      copmuv(v, u, n)                                                   *
 * (03) Descricao:                                                        *
 *      Copia o vetor 'u' no vetor 'v' ambos do tipo double huge e        *
 *      de dimensao 'n', trocando o sinal                                 *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 *      troca de sinal                                                    *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *v - pointer para vetor de destino - double huge.              *
 *      -> *u - pointer para vetor original - double huge.                *
 *      ->  n  - dimensao do vetor.                                       *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void copmuv(double huge *v, double huge *u, long n);              *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
 */


#include "matematc.h"

void copmuv(double huge *v, double huge *u, register long n)
    {
    while (n--)
        *v++ = -*u++;
    }
