
/*
 **************************************************************************
 *                                                                        *
 * void cosxl2                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Calcular cosenos diretores e comprimento de elementos.            *
 * (02) Forma de utilizacao:                                              *
 *      cosxl2(xyz, m, cx, cy, xl);                                       *
 * (03) Descricao:                                                        *
 *      Calcula cosenos diretores e comprimento de elementos unidimen-    *
 *      sionais de dois nos , definidos  em relacao a sistemas de coor-   *
 *      denadas cartesianas bidimensionais.                               *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      coseno diretor                                                    *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 14 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      -> xyz[] - endereco da tabela do tipo double huge que contem as   *
 *                 coordenadas nodais do elemento 'm' de um grupo de      *
 *                 elementos.                          - double huge      *
 *                 -----------------------------------------              *
 *                 |  coordenadas do   |  coordenadas do   |              *
 *                 |  primeiro no'     |  segundo no'      |              *
 *                 -----------------------------------------              *
 *                 | <---------------- 4 ----------------> |              *
 *                                                                        *
 *      -> m     - numero do elemento - int                               *
 *      <- *cx   - coseno diretor do eixo X local (ang. Xloc, Xglob)      *
 *                                                     - double huge      *
 *      <- *cy   - coseno diretor do eixo Y local (ang. Yloc, Yglob)      *
 *                                                     - double huge      *
 *      <- *xl   - comprimento do elemento - double huge                  *
 * (08) Variaveis externas utilizadas:                                    *
 *      TOL   1.0e-03                                                     *
 * (09) Chamadas:                                                         *
 *      trc_on,trc_off,error,exitprg                                      *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void cosxl2(double huge *xyz,int m, double huge *cx,              *
 *                 double huge *cy, double huge *xl);                     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, math.h, util.h, matematc.h                               *
 * (12) Mensagens de erro:                                                *
 *      . Numero 1                                                        *
 *        Mensagem - Elemento de comprimento nulo                         *
 *                   Elemento 'm'                                         *
 *                   Verificar coordenadas nodais:                        *
 *                   'xyz[]'                                              *
 *        Causa    - As coordenadas nodais estao mal definidas.           *
 *        Correcao - Verificar as coordenadas nodais ou a geracao         *
 *                   automatica de coordenadas.                           *
 *        Efeito   - O programa e' interrompido.                          *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"
#include <math.h>

#define TOL 1.0e-03

void cosxl2(double xyz[], int /*m*/, double *cx, double *cy, double *xl)
    {
    static char *nome = "cosxl2";

    TraceOn(nome);

    *cx = xyz[2] - xyz[0];
    *cy = xyz[3] - xyz[1];
    *xl = (*cx) * (*cx) + (*cy) * (*cy);
    if (*xl <= TOL)
        Error(2, 1, "Elemento de comprimento nulo");
    else
        {
        *xl = pow(*xl,0.5);    /* calculo do comprimento do elemento */
        *cx = (*cx)/(*xl);     /* calculo dos cossenos diretores */
        *cy = (*cy)/(*xl);
        }

    TraceOff(nome);
    }
