/*
 **************************************************************************
 *                                                                        *
 * void dcopyv                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Copiar um vetor do tipo double huge em outro.                     *
 * (02) Forma de utilizacao:                                              *
 *      dcopyv(d, s, tam);                                                *
 * (03) Descricao:                                                        *
 *      Copiar o vetor 's' no vetor 'd' ambos do tipo double huge e       *
 *      dimensao 'tam'.                                                   *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *d   -  pointer para vetor que copia - double huge             *
 *      -> *s   -  pointer para vetor original - double huge              *
 *      -> tam  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void dcopyv(double huge *d, double huge *s, long tam);            *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void dcopyv(double huge *d, double huge *s, register long tam)
    {
    while (tam--)
        *d++ = *s++;
    }

