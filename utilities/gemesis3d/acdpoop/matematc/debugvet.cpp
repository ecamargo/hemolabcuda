/*
 **************************************************************************
 *                                                                        *
 * void dbvetor                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Imprimir vetores em um arquivo.                                   *
 * (02) Forma de utilizacao:                                              *
 *      dbvetor(titulo, tipo, v, nelem);                                  *
 * (03) Descricao:                                                        *
 *      Imprime vetores em um arquivo.                                    *
 * (04) Palavras chave:                                                   *
 *      debug                                                             *
 *      vetor                                                             *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, maio de 1988,                               *
 *      Antonio Carlos Salgado Guimaraes e Raul Feijoo                    *
 * (07) Argumentos:                                                       *
 *      -> *titulo - titulo do vetor a ser impresso - char                *
 *      -> *tipo   - tipo dos elementos do vetor - char                   *
 *      -> *v      - vetor a ser impresso - void huge                     *
 *      -> nelem   - numero de elementos do vetor - long                  *
 * (08) Variaveis externas utilizadas:                                    *
 *      struct SDBINFO DBINFO                                             *
 *      struct STRINFO TRINFO                                             *
 * (09) Chamadas:                                                         *
 *      trc_on, trcoff, error, lvetarq, ttipo                             *
 * (10) Prototype:                                                        *
 *      util.h                                                            *
 *      void dbvetor(char *titulo, char *tipo, void huge *v, long nelem); *
 * (11) Headers necessarios:                                              *
 *      stdio.h, util.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      . Numero 1:                                                       *
 *        Mensagem: Tipo de elemento invalido.                            *
 *        Causa   : Nao existe o tipo de elemento indicado.               *
 *        Correcao: Corrigir o tipo do elemento.                          *
 *        Efeito  : O programa e' interrompido.                           *
 *      . Numero 2:                                                       *
 *        Mensagem: Numero de elementos negativo ou nulo.                 *
 *        Causa   : Numero de elementos negativo ou nulo.                 *
 *        Correcao: Corrigir o numero de elementos.                       *
 *        Efeito  : O programa e' interrompido.                           *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "acdp.h"
#include "matematc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern struct SDBINFO DBINFO;
extern struct STRINFO TRINFO;

void dbvetor(char *titulo, char *tipo, void huge *v, long nelem)
    {
    FILE *lw = TRINFO.mensptr;
    int postipo, tamtipo;

    static char *nome_fc = "dbvetor";

    TraceOn(nome_fc);

    /*
    ** testes de validade dos parametros
    */
    //postipo = ttipo(tipo, &tamtipo);
    postipo = mStrCmp(tipo, &tamtipo);

    if (!postipo)
        Error(2, 1, "Tipo de elemento invalido.");

    if (nelem < 1)
        Error(2, 2, "Numero de elementos negativo ou nulo.");
    else
        lvetarq(titulo, postipo, v, nelem, lw);

    TraceOff(nome_fc);
    }
