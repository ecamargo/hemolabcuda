/*
 **************************************************************************
 *                                                                        *
 * void diag                                                              *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Atribuir o valor 'val' a toda  diagonal principal da matriz.      *
 * (02) Forma de utilizacao:                                              *
 *      diag(mat, val, nlin);                                             *
 * (03) Descricao:                                                        *
 *      A matriz  quadrada 'mat' e manipulada como um vetor, armazenada   *
 *      por linhas.  Genericamente uma matriz com essa forma de armaze-   *
 *      namento tem, por exemplo para uma matriz de (3,3), a seguinte     *
 *      estrutura:                                                        *
 *                   | mat(1)  mat(2) mat(3) |                            *
 *          mat  =   | mat(4)  mat(5) mat(6) |                            *
 *                   | mat(7)  mat(8) mat(9) |                            *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <> *mat -  pointer para matriz - double huge                      *
 *      -> val  -  valor a ser atribuido - double huge                    *
 *      -> nlin -  numero de linhas da matriz 'mat' - long                *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void diag(double huge *mat, double val, long nlin);               *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void diag(double huge *mat, double val, register long nlin)
    {
    long tam = nlin;
    nlin++;
    while (tam--)
        {
        *mat = val;
        mat += nlin;
        }
    }

