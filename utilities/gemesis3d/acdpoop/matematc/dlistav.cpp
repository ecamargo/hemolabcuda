/*
 **************************************************************************
 *                                                                        *
 * void dlistav                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Listar um vetor de 'n' posicoes e nome 'titulo' na unidade 'fp'.  *
 * (02) Forma de utilizacao:                                              *
 *      dlistav(titulo, v, n, fp);                                        *
 * (03) Descricao:                                                        *
 *      Lista o vetor 'v' do tipo double huge e dimensao 'n' colocando    *
 *      o titulo 'titulo' na unidade 'fp'.                                *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 *      listar                                                            *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 26 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      -> *titulo -  pointer para nome do vetor - char                   *
 *      -> *v      -  pointer para vetor  - double huge                   *
 *      -> n       -  dimensao do vetor   - long                          *
 *      -> *fp     -  unidade de saida - FILE                             *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void dlistav(char *titulo, double huge *v, long n, FILE *fp);     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void dlistav(char *titulo, double huge *v, long n, FILE *fp)
    {
    register long i = 0;

    fprintf(fp, "\n     Vetor : %s , dimensao = %ld\n\n", titulo, n);
    while (n--)
        {
        fprintf(fp, "%14.7E",*v++);
        i++;
        if (i > 4)
            {
            i = 0;
            fprintf(fp,"\n");
            }
        }
    }
