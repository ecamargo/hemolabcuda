/*
 **************************************************************************
 *                                                                        *
 * void dzerov                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Zerar um vetor.                                                   *
 * (02) Forma de utilizacao:                                              *
 *      dzerov(v, tam);                                                   *
 * (03) Descricao:                                                        *
 *      Zera o vetor 'v' do tipo double huge, de dimensao dada por 'tam'. *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 *      zerar                                                             *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <> *v   -  pointer para vetor - double huge                       *
 *      -> tam  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void dzerov(double huge *v, long tam);                            *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void dzerov(double huge *v, register long tam)
    {
    while (tam--)
        *v++ = 0.0e0;
    }

