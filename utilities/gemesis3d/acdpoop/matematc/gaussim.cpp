/*
 *****************************************************************************
 *                                                                           *
 * int gaussim(vetx, mas, vetb, neq, delta, step)                            *
 *                                                                           *
 * (01) Objetivo:                                                            *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-     *
 *      cao de Gauss.                                                        *
 * (02) Forma de utilizacao:                                                 *
 *      variavel inteira = gaussim(vetx, mas, vetb, neq, delta, step);       *
 * (03) Descricao:                                                           *
 *      Resolve um sistema de equacoes lineares pelo Metodo de Eliminacao    *
 *      de Gauss. A matriz do sistema e simetrica armazenada na forma        *
 *      supradiagonal por linhas. A rotina retorna um codigo informando se   *
 *      obteve sucesso na resolucao ou nao.                                  *
 * (04) Palavras chave:                                                      *
 *      equacoes                                                             *
 *      simetrica                                                            *
 *      direto                                                               *
 * (05) Referencia:                                                          *
 *      ---                                                                  *
 * (06) Data e autor:                                                        *
 *      Rio de Janeiro, LNCC, marco de 1990,                                 *
 *      Roberto Eduardo Garcia.                                              *
 * (07) Argumentos:                                                          *
 *      <- *vetx -  pointer para vetor resposta             - double huge    *
 *      -> *mas  -  pointer para matriz do sistema          - double huge    *
 *      -> *vetb -  pointer para vetor termos independentes - double huge    *
 *      -> neq   -  numero de equacoes                      - long           *
 *      -> delta -  menor valor possivel para elementos pivot                *
 *                  da matriz do sistema                    - double         *
 *      -> *step -  etapa 'k' dos processos de 'forward elimination' - long  *
 * (08) Variaveis externas utilizadas:                                       *
 *      ---                                                                  *
 * (09) Chamadas:                                                            *
 *      ---                                                                  *
 * (10) Prototype:                                                           *
 *      solvequ.h                                                            *
 *      int gaussim(double huge *vetx, double huge *mas, double huge *vetb,  *
 *                  long int neq, double delta, long int *step);             *
 * (11) Headers necessarios:                                                 *
 *      solvequ.h                                                            *
 *      matematc.h                                                           *
 *      math.h                                                               *
 *      util.h                                                               *
 *      dbmanagm.h                                                           *
 * (12) Mensagens de erro:                                                   *
 *      ---                                                                  *
 * (13) Depuracao:                                                           *
 *      ---                                                                  *
 * (14) Portabilidade:                                                       *
 *      C standard.                                                          *
 * (15) Acesso:                                                              *
 *      Livre ao usuario.                                                    *
 *                                                                           *
 *****************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Metodo de Eliminacao de Gauss
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na resolucao do sistema de equacoes.
** 0: a matriz do sistema e' singular, impedindo a execucao
**             do Metodo de Gauss.
*/

int gaussim(double huge *vetx, double huge *mas, double huge *vetb,
            double delta, long int neq,long int *step)
    {
    double huge *pinmas,   /* pointer que guarda o endereco inicial de 'mas' */
           huge *akk,      /* pointer que varre a diagonal principal de 'mas' */
           huge *temp,     /* pointer auxiliar utilizado nas etapas de 'partial
                              pivoting' e 'forward elimination' */
           huge *ptlb,     /* pointer para elemento do 'vetb' */
           huge *pcfin,    /* pointers auxiliares que varrem o 'vetb' */
           huge *plix;     /* pointer para vetor resposta 'vetx' */
    double mik,            /* Aik / Akk */
           paux;           /* variavel auxiliar utilizada na etapa de 'back
                              substitution' */
    register long int j,
                      i,   /* variaveis utlizadas como contadores */
                      k;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int nelem,        /* numero de elementos da matriz do sistema */
             neqm1,        /* neq - 1 */
             aux,
             cont,         /* contadores */
             pulomas,      /* numero de posicoes que o pointer mas deve avancar */
             pulotemp,     /* numero de posicoes que o pointer temp deve avancar */
             pulodg;       /* numero de posicoes que o pointer akk deve retroceder */

    /*
    ** inicializa pointers e constantes
    */

    akk   = pinmas = mas;
    pcfin = ptlb   = vetb;
    neqm1 = neq - 1;
    nelem = ((neq + 1) * neq)/2;
    cont = neqm1;
    pulomas = neq;

    /*
    ** etapa de 'forward elimination'
    */

    for (k = 1; k < neq; k++)
        {
        mas   = akk + pulomas;
        aux   = cont;
        ptlb  = vetb + k;
        pcfin = vetb + (k - 1);
        if (fabs(*akk) < delta)                /* se o valor do pivot for menor             */
            {                                  /* que delta, a rotina e' interrompida       */
            *step = k;                         /* retornando 0 e fica disponivel ao usuario */
            return (0);                        /* e etapa onde o fato ocorreu               */
            }
        for (i = k, pulotemp = 1; i < neq; i++, pulotemp++)
            {
            temp = akk + pulotemp;
            mik = *temp / *akk;
            j = aux;
            while (j--)
                {
                *mas = *mas - mik * (*temp++);
                mas++;
                }
            *ptlb = *ptlb - mik * (*pcfin);
            ptlb++;
            aux--;
            }
        akk += pulomas;
        pulomas--;
        cont--;
        }

    /*
    ** etapa de 'back substitution'
    */

    akk    = pinmas + (nelem - 1);
    pulodg = 1;
    for (i = neqm1; i >= 0 ; i--)
        {
        plix  = vetx + i + 1;
        mas   = akk + 1;
        paux  = 0.0;
        for (j = i + 1; j < neq; j++)
            paux += (*mas++) * (*plix++);
        *(vetx + i) = ((*(vetb + i)) - paux)/(*akk);
        pulodg++;
        akk -= pulodg;
        }
    return(1);
    }
