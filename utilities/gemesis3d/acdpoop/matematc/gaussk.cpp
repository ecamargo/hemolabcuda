/*
 ***************************************************************************
 *                                                                         *
 * int gaussk(vetx, ma, maxa, vetb, neq, delta, step)                      *
 *                                                                         *
 * (01) Objetivo:                                                          *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-   *
 *      cao de Gauss.                                                      *
 * (02) Forma de utilizacao:                                               *
 *      variavel inteira = gaussk(vetx, ma, maxa, vetb, neq, delta, step); *
 * (03) Descricao:                                                         *
 *      Resolve um sistema de equacoes lineares pelo Metodo de Eliminacao  *
 *      de Gauss. A matriz do sistema e' simetrica escalonada por colunas  *
 *      ascendentes. O armazenamento desta matriz se da por meio dos veto- *
 *      res 'ma' e 'maxa'. A rotina retorna um codigo informando se obteve *
 *      sucesso na resolucao ou nao.                                       *
 * (04) Palavras chave:                                                    *
 *      equacoes                                                           *
 *      simetrica                                                          *
 *      skyline                                                            *
 *      direto                                                             *
 * (05) Referencia:                                                        *
 *      ---                                                                *
 * (06) Data e autor:                                                      *
 *      Rio de Janeiro, LNCC, abril de 1990,                               *
 *      Roberto Eduardo Garcia.                                            *
 * (07) Argumentos:                                                        *
 *      <- *vetx -  pointer para vetor resposta             - double huge  *
 *      -> *ma   -  pointer para o vetor que contem os elementos           *
 *                  da matriz do sistema                    - double huge  *
 *      -> *maxa -  pointer para vetor que contem as posicoes dos          *
 *                  elementos da diagonal principal         - int huge     *
 *      -> *vetb -  pointer para vetor termos independentes - double huge  *
 *      -> neq   -  numero de equacoes                      - long         *
 *      -> delta -  menor valor possivel para elementos pivot              *
 *                  da matriz do sistema                    - double       *
 *      -> *step -  etapa 'k' do processo de 'forward elimination' - long  *
 * (08) Variaveis externas utilizadas:                                     *
 *      ---                                                                *
 * (09) Chamadas:                                                          *
 *      ---                                                                *
 * (10) Prototype:                                                         *
 *      solvequ.h                                                          *
 *      int gaussk(double huge *vetx, double huge *ma, int huge *maxa,     *
 *                 double huge *vetb, long int neq, double delta,          *
 *                 long int *step);                                        *
 * (11) Headers necessarios:                                               *
 *      solvequ.h                                                          *
 *      matematc.h                                                         *
 *      math.h                                                             *
 * (12) Mensagens de erro:                                                 *
 *      ---                                                                *
 * (13) Depuracao:                                                         *
 *      ---                                                                *
 * (14) Portabilidade:                                                     *
 *      C standard.                                                        *
 * (15) Acesso:                                                            *
 *      Livre ao usuario.                                                  *
 *                                                                         *
 ***************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Metodo de Eliminacao de Gauss
** A matriz do sistema  e' do tipo 'skyline'
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na resolucao do sistema de equacoes.
** 0: a matriz do sistema e' singular, impedindo a execucao
**             do Metodo de Gauss.
*/

int gaussk(double huge *vetx, double huge *ma, long huge *maxa,
           double huge *vetb, long int neq, double delta, long int *step)
    {
    double huge *aij,      /* pointer para elementos do vetor 'sk' */
           huge *pdiagi,
           huge *pdiagj,   /* pointers para elementos da diagonal principal */
           huge *ptlb,
           huge *pcfin,    /* pointers auxiliares que varrem o 'vetb' */
           huge *plix;     /* pointer para vetor resposta 'vetx' */
    double mik,            /* Aik / Akk */
           paux,           /* variavel auxiliar utilizada na etapa de 'back
                              substitution' */
           akk,            /* variavel que recebe o conteudo do elemento da
                              diagonal principal */
           akj,            /* elemento Akj da etapa de eliminacao */
           aik;            /* elemento Aik da etapa de eliminacao */
    register long int j,
                      i,   /* variaveis utlizadas como contadores */
                      k;   /* etapa do processo de eliminacao */
    long int neqm1,        /* neq - 1 */
             incptr,       /* variavel auxiliar */
             poseldg,      /* posicao do elemento da diagonal principal */
             hcoli,
             hcolj,        /* altura da coluna */
             diagi,
             diagj;        /* posicao do elemento da diagonal principal
                              variaveis auxiliares */

    /*
    ** inicializa pointers e constantes
    */

    pcfin = ptlb = vetb;
    neqm1 = neq - 1;

    /*
    ** Etapa de 'forward elimination'
    */

    for (k = 0; k < neqm1; k++)
        {
        poseldg = *(maxa + k);
        akk     = *(ma + poseldg);
        if (fabs(akk) <= delta)
            {
            *step = poseldg;
            return (0);
            }
        pcfin = vetb + k;
        ptlb = pcfin + 1;
        for (i = k + 1; i < neq; i++)
            {
            hcoli = *(maxa + i + 1) - *(maxa + i);
            if (i - hcoli < k)
                {
                diagi  = *(maxa + i);
                pdiagi = ma + diagi;
                aik = *(pdiagi + (i - k));
                mik = aik/akk;
                for (j = i; j < neq; j++)
                    {
                    diagj = *(maxa + j);
                    pdiagj = ma + diagj;
                    hcolj = *(maxa + j + 1) - diagj;
                    if (j - hcolj < k)
                        {
                        aij = pdiagj + (j - i);
                        akj = *(pdiagj + (j - k));
                        *aij = *aij - (mik * akj);
                        }
                    }
                *ptlb = *ptlb - mik * (*pcfin);
                ptlb++;
                }
            else ptlb++;
            }
        }

    /*
    ** Etapa de 'back substitution'
    */

    for (i = neqm1; i >= 0; i--)
        {
        poseldg = *(maxa + i);
        akk = *(ma + poseldg);
        plix = (vetx + i) + 1;
        paux = 0.0;
        for (j = i + 1, incptr = 1; j < neq; j++, incptr++)
            {
            diagj = *(maxa + j);
            pdiagj = ma + diagj;
            hcolj = *(maxa + j + 1) - diagj;
            if (j - hcolj < i)
                {
                aij = pdiagj + incptr;
                paux += (*aij) * (*plix);
                plix++;
                }
            else plix++;
            }
        *(vetx + i) = (*(vetb + i) - paux)/akk;
        }
    return(1);
    }


