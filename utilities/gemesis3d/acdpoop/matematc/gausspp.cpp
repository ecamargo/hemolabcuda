/*
 **************************************************************************
 *                                                                        *
 * int gausspp(vetx, ma, vetb, neq, delta, step)                          *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-  *
 *      cao de Gauss.                                                     *
 * (02) Forma de utilizacao:                                              *
 *      variavel inteira = gausspp(vetx, ma, vetb, neq, delta, step);     *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares pelo Metodo de Eliminacao *
 *      de Gauss utilizando tecnica de pivoteamento parcial. A matriz do  *
 *      sistema e cheia armazenada por linhas. A rotina retorna um codigo *
 *      informando se obteve sucesso na resolucao ou nao.                 *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      cheia                                                             *
 *      direto                                                            *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, marco de 1990,                              *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <- *vetx -  pointer para vetor resposta             - double huge *
 *      -> *ma   -  pointer para matriz do sistema          - double huge *
 *      -> *vetb -  pointer para vetor termos independentes - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 *      -> delta -  menor valor possivel para elementos pivot             *
 *                  da matriz do sistema                    - double      *
 *      -> *step -  etapa 'k' dos processos de 'partial pivoting' e       *
 *                  'forward elimination'                   - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      int gausspp(double huge *vetx, double huge *ma, double huge *vetb,*
 *                 long int neq, double delta, long int *step);           *
 * (11) Headers necessarios:                                              *
 *      stdio.h                                                           *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 *      util.h                                                            *
 *      dbmanagm.h                                                        *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Metodo de Eliminacao de Gauss
** utilizando tecnica de pivoteamento parcial.
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na resolucao do sistema de equacoes.
** 0: a matriz do sistema e' singular, impedindo a execucao
**             do Metodo de Gauss.
*/

int gausspp(double huge *vetx, double huge *ma, double huge *vetb,
            long int neq, double delta, long int *step)
    {
    double huge *pinma,    /* pointer que guarda o endereco inicial de 'ma' */
           huge *akk,      /* pointer que varre a diagonal principal de 'ma' */
           huge *temp,     /* pointer auxiliar utilizado nas etapas de 'partial
                              pivoting' e 'forward elimination' */
           huge *ppivot,   /* pointer para elemento pivot */
           huge *ptlb,
           huge *pcfin,    /* pointers auxiliares para elementos do 'vetb' */
           huge *plix,     /* pointer para vetor resposta 'vetx' */
           huge *auxptr;   /* pointer para vetor auxiliar utilizado para troca
                              de linhas da matriz 'ma' na etapa de pivoteamanto */
    double mik,            /* Aik / Akk */
           paux,           /* variavel auxiliar utilizada na etapa de 'back
                              substitution' */
           auxb;           /* variavel auxiliar utilizada na troca de elementos
                              do vetor 'vetb' na etapa de 'partial pivoting' */
    register long int j,
                      i,   /* variaveis utlizadas como contadores */
                      k;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int nelem,        /* numero de elementos da matriz do sistema */
             neqm1,        /* neq - 1 */
             neqp1,        /* neq + 1 */
             aux,          /* contador */
             row;          /* linha na qual foi achado o pivot na etapa 'k' */

    /*
    ** inicializa pointers e constantes
    */

    akk   = pinma = ma;
    pcfin = ptlb  = vetb;
    neqm1 = neq - 1;
    neqp1 = neq + 1;
    nelem = neq * neq;
    aux   = neq;

    /*
    ** cria vetor auxiliar 'auxptr'
    */

    auxptr = (double huge *) mMalloc(sizeof(double) * neq);

    for (k = 1; k < neq; k++)
        {
        ppivot = temp = akk;
        for (i = k; i < neq; i++)              /* procura do pivot */
            {
            temp += neq;
            if (fabs(*temp) > fabs(*ppivot))
                {
                ppivot = temp;                 /* achou o pivot e */
                row    = i;                /* se encontra na linha 'row' */
                }
            }
        if (ppivot != akk)
            {
            temp   = akk - (k - 1);
            ppivot = ppivot - (k - 1);
            dcopyv(auxptr, temp, neq);         /* realiza a troca */
            dcopyv(temp, ppivot, neq);         /* das linhas correspondentes */
            dcopyv(ppivot, auxptr, neq);
            ptlb += row;
            pcfin += (k - 1);
            auxb = *pcfin;
            *pcfin = *ptlb;
            *ptlb = auxb;
            }

        if (fabs(*ppivot) < delta)             /* se o valor do pivot for menor */
            {                                  /* que delta, a rotina e' interrompida */
            *step = k;                         /* retornando 0 e fica disponivel ao usuario */
            mFree(auxptr);                     /* a etapa (linha) do pivot */
            return (0);
            }

        /* inicia etapa de 'forward elimination' */

        ma   = akk + neq;
        temp = akk;
        ptlb = vetb + k;
        pcfin = vetb + (k - 1);
        for (i = k + 1; i <= neq; i++)
            {
            mik = *ma / *akk;
            j = aux;
            while (j--)
                {
                *ma = *ma - mik * (*temp++);
                ma++;
                }
            *ptlb = *ptlb - mik * (*pcfin);
            ptlb++;
            temp = akk;
            ma = ma + (k - 1);
            }
        akk += neqp1;
        aux--;                         /* fim da etapa de eliminacao */
        }

    /*
    ** etapa de 'back substitution'
    */

    pcfin = vetb + neqm1;
    vetx += neqm1;
    akk = pinma + (nelem - 1);
    for (k = neq; k >= 1; k--)
        {
        plix = vetx + 1;
        ma = akk + 1;
        paux = 0.0;
        for (j = k + 1; j <= neq; j++)
            paux += (*ma++) * (*plix++);
        *vetx-- = ((*pcfin--) - paux) / *akk;
        akk -= neqp1;
        }
    mFree(auxptr);
    return(1);
    }


