/*
 ****************************************************************************
 *                                                                          *
 * int gcpds(vetx, ma, veth, neq, itmax, eps, codcvg)                       *
 *                                                                          *
 * (01) Objetivo:                                                           *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo do   *
 *      Gradiente Conjugado com tecnica de precondicionamento.              *
 * (02) Forma de utilizacao:                                                *
 *      variavel inteira = gcpds(vetx, ma, veth, neq, itmax, eps, codcvg);  *
 * (03) Descricao:                                                          *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo do    *
 *      Gradiente Conjugado com matriz de precondicionamento. Esta matriz   *
 *      contem os elementos da diagonal principal da matriz do sistema. A   *
 *      matriz do sistema e simetrica armazenada na forma supradiagonal     *
 *      por linhas. A rotina devolve um codigo informando se o calculo      *
 *      convergiu ou nao.                                                   *
 * (04) Palavras chave:                                                     *
 *      equacoes                                                            *
 *      iterativo                                                           *
 *      simetrica                                                           * 
 *      gradiente                                                           *
 *      precondicionamento                                                  *
 *      diagonal                                                            *
 * (05) Referencia:                                                         *
 *      - Finite Element Solution of Boundary Value Problems                *
 *        Academic Press, Inc. 1984                                         *
 *        pag.49                                                            *
 *        Axelsson & Barker                                                 *
 *      - Revista Internacional de Metodos Numericos para Calculo y Diseno  *
 *        en Ingenieria                                                     *
 *        Vol.2 (1986), pag.27 - 41                                         *
 * (06) Data e autor:                                                       *
 *      Rio de Janeiro, fevereiro de 1991                                   *
 *      Roberto Eduardo Garcia.                                             *
 * (07) Argumentos:                                                         *
 *      <> *vetx  - pointer para o vetor resposta 'vetx'     - double huge  *
 *      -> *ma    - pointer para os elementos da matriz 'ma' - double huge  *
 *      -> *veth  - pointer para o vetor dos elementos                      *
 *                                            independentes  - double huge  *
 *      -> neq    - numero de equacoes do sistema            - long         *
 *      -> itmax  - numero maximo de iteracoes               - long         *
 *      -> eps    - constante para calculo da convergencia   - double       *
 *      -> codcvg - codigo de tipo de convergencia           - int          *
 *                  1 - norma euclideana                                    *
 *                  2 - norma infinita                                      *
 * (08) Variaveis externas utilizadas:                                      *
 *      ---                                                                 *
 * (09) Chamadas:                                                           *
 *      scalv                                                               *
 *      prodmsv                                                             *
 *      normaev                                                             *
 *      norinfv                                                             *
 *      praux                                                               *
 * (10) Prototype:                                                          *
 *      solvequ.h                                                           *
 *      int gcpds(double huge *vetx, double huge *ma, double huge *veth,    *
 *                long int neq, long int itmax, double eps, int codcvg);    *
 * (11) Headers necessarios:                                                *
 *      stdio.h                                                             *
 *      solvequ.h                                                           *
 *      matematc.h                                                          *
 *      util.h                                                              *
 *      dbmanagm.h                                                          *
 * (12) Mensagens de erro:                                                  *
 *      ---                                                                 *
 * (13) Depuracao:                                                          *
 *      ---                                                                 *
 * (14) Portabilidade:                                                      *
 *      C standard.                                                         *
 * (15) Acesso:                                                             *
 *      Livre ao usuario.                                                   *
 *                                                                          *
 ****************************************************************************
*/

#include "matematc.h" 

/*
** Funcao que resolve o sistema
**           -1
** vetaux = C  * vetres
** C -> matriz de precondicionamento
*/
static void praux(double huge *vetx, double huge *mas, double huge *vetb,
                  long int neq)

// double huge *vetx,     pointer para o vetor resposta 'vetaux'            
//        huge *mas,      pointer para os elementos da matriz do sistema    
//        huge *vetb;     pointer para os elementos do vetor 'vetres'       
// long int neq;          numero de equacoes do sistema                     
    {
    long int pulomas;              /* numero de posicoes que o pointer mas deve avancar */
    register long int i;           /* variavel utilizada como contador                  */

    pulomas = neq;
    for (i = 0; i < neq; i++)
        {
        *vetx = (*vetb)/(*mas);
        mas += pulomas;
        vetb++;
        vetx++;
        pulomas--;
        }
    }

/*
** Metodo do Gradiente Conjugado com precondicionamento.
** Matriz de Precondicionamento Diagonal (elementos da diagonal principal da matriz do sistema)
** Matriz do Sistema simetrica supradiagonal por linhas.
** A rotina devolve:
** 0 -> se o calculo nao convergir ou
**      se o numero maximo de iteracoes for superado;
** 1 -> se o calculo convergir;
*/

int gcpds(double huge *vetx, double huge *ma, double huge *veth,
          long int neq, long int itmax, double eps, int codcvg)
    {
    double huge *vetaux,    /* vetor auxiliar */
           huge *pvetaux,   /* pointer para vetor auxiliar */
           huge *vetres,    /* vetor residuo (ou gradiente) */
           huge *pvetres,   /* pointer para vetor residuo */
           huge *vetp,      /* vetor direcao conjugada */
           huge *pvetp,     /* pointer para vetor direcao conjugada */
           huge *pveth,     /* pointer para vetor termos independentes */
           huge *pvetx;     /* pointer para vetor resposta */
    double a, b, d,
           delta0,
           delta1,          /* variaveis auxiliares */
           ctecvg,          /* constante de convergencia */
           norinfres,       /* recebe norma infinita do vetor residuo */
           noreures,        /* recebe norma euclideana do vetor residuo */
           norinfh,         /* recebe norma infinita do vetor termos independentes */
           noreuh;          /* recebe norma euclideana do vetor termos independenetes */
    register long int j,
                      k;    /* variaveis utlizadas como contadores */
    int converge;           /* variavel que retorna o codigo para convergencia
                               0 - se nao convergir
                               1 - se convergir
                               2 - se omega nao for valido  */

    /* aloca espaco de memoria para vetores */

    vetaux = (double huge *) mMalloc(sizeof(double) * neq);
    vetres = (double huge *) mMalloc(sizeof(double) * neq);
    vetp   = (double huge *) mMalloc(sizeof(double) * neq);

    pvetaux = vetaux;
    pvetres = vetres;
    pveth   = veth;
    pvetx   = vetx;

    k = 1;
    converge = 0;
    a = b = d = ctecvg = 0.0;

    /*
    ** testa se ha erro no codigo de tipo de convergencia
    ** se ha assume codcvg = 1
    */

    if ((codcvg != 1) && (codcvg != 2))
       codcvg = 1;

    /*
    ** calculo da constante de convergencia
    */

    switch (codcvg)
        {
        case 1 : noreuh = normaev(veth, neq);
                 ctecvg = noreuh * eps;
                 break;

        case 2 : norinfh = norinfv(veth, neq);
                 ctecvg = norinfh * eps;
                 break;

        default : break;
        }

    /*
    ** inicio do metodo
    */

    prodmsv(vetaux, ma, vetx, neq);
    for (j = 0; j < neq; j++)
        *pvetres++ = (*pvetaux++) - (*pveth++);
    pvetaux = vetaux;
    pvetres = vetres;
    pveth = veth;
    pvetp = vetp;
    praux(vetaux, ma, vetres, neq);                  /* resolve o sistema     */
    for (j = 0; j < neq; j++)                        /*           -1          */
        {                                            /* vetaux = C   * vetres */
        *pvetp = -(*pvetaux);                        /* vetp = - vetaux       */
        pvetp++;
        pvetaux++;
        }
    pvetp = vetp;
    pvetaux = vetaux;
    delta0 = scalv(vetres, vetaux, neq);

    /* teste de convergencia */

    switch (codcvg)
        {
        case 1: noreures = normaev(vetres, neq);
                if (noreures <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        case 2: norinfres = norinfv(vetres, neq);
                if (norinfres <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        default: break;
        }
    while (k <= itmax)
        {
        prodmsv(vetaux, ma, vetp, neq);
        d = scalv(vetp, vetaux, neq);
        a = delta0/d;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* resposta         */
            *pvetx = *pvetx + a * (*pvetp);
            pvetp++;
            pvetx++;
            }
        pvetp = vetp;
        pvetx = vetx;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* residuo          */
            *pvetres = *pvetres + a * (*pvetaux);
            pvetaux++;
            pvetres++;
            }
        pvetres = vetres;
        pvetaux = vetaux;
        praux(vetaux, ma, vetres, neq);
        delta1 = scalv(vetres, vetaux, neq);
        switch (codcvg)
            {
            case 1: noreures = normaev(vetres, neq);
                    if (noreures <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            case 2: norinfres = norinfv(vetres, neq);
                    if (norinfres <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            default: break;
            }
        b = delta1/delta0;
        delta0 = delta1;
        for (j = 0; j < neq; j++)
            {
            *pvetp =  b * (*pvetp) - *pvetaux;       /* calculo do vetor  */
            pvetaux++;                               /* direcao conjugada */
            pvetp++;
            }
        pvetaux = vetaux;
        pvetp = vetp;
        k++;
        }
    mFree(vetaux);                                /* deleta vetores alocados */
    mFree(vetres);
    mFree(vetp);
    return (converge);
    }


