/*
 ******************************************************************************
 *                                                                            *
 * int gcpdsk(vetx, ma, maxa, veth, neq, itmax, eps, codcvg)                  *
 *                                                                            *
 * (01) Objetivo:                                                             *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo do     *
 *      Gradiente Conjugado utilizando tecnica de precondicionamento.         *
 * (02) Forma de utilizacao:                                                  *
 *      variavel inteira = gcpdsk(vetx, ma, maxa, veth, neq, itmax,           *
 *                                eps, codcvg);                               *
 * (03) Descricao:                                                            *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo do      *
 *      Gradiente Conjugado, utilizando matriz de precondicionamento . Esta   *
 *      matriz contem os elementos da diagonal principal da matriz do sistema.*
 *      A matriz do sistema e do tipo sky-line por colunas ascendentes e esta *
 *      armazenada nos vetores 'ma' e 'maxa'. A rotina devolve um codigo      *
 *      informando se o calculo convergiu ou nao.                             *
 * (04) Palavras chave:                                                       *
 *      equacoes                                                              *
 *      iterativo                                                             *
 *      sky-line                                                              *
 *      gradiente                                                             *
 *      precondicionamento                                                    *
 *      diagonal                                                              *
 * (05) Referencia:                                                           *
 *      - Finite Element Solution of Boundary Value Problems                  *
 *        Academic Press, Inc  1984                                           *
 *        pag. 49                                                             *
 *        Axelsson & Barker                                                   *
 *      - Revista Internacional de Metodos Numericos para Calculo y Diseno    *
 *        en Ingenieria                                                       *
 *        Vol.2 (1986), pag.27-41                                             *
 * (06) Data e autor:                                                         *
 *      Rio de Janeiro, fevereiro de 1991                                     *
 *      Roberto Eduardo Garcia.                                               *
 * (07) Argumentos:                                                           *
 *      <> *vetx  - pointer para o vetor resposta 'vetx'    - double huge     *
 *      -> *ma    - pointer para vetor que contem os elementos da matriz      *
 *                  do sistema.                             - double huge     *
 *      -> *maxa  - pointer para o vetor que contem a posicao dos elementos   *
 *                  da diagonal principal de 'ma'           - long huge       *
 *      -> *veth  - pointer para o vetor dos elementos                        *
 *                                            independentes - double huge     *
 *      -> neq    - numero de equacoes do sistema           - long            *
 *      -> itmax  - numero maximo de iteracoes              - long            *
 *      -> eps    - constante para calculo da convergencia  - double          *
 *      -> codcvg - codigo de tipo de convergencia          - int             *
 *                  1 - norma euclideana                                      *
 *                  2 - norma infinita                                        *
 * (08) Variaveis externas utilizadas:                                        *
 *      ---                                                                   *
 * (09) Chamadas:                                                             *
 *      scalv                                                                 *
 *      prodmskv                                                              *
 *      normaev                                                               *
 *      norinfv                                                               *
 *      prauxsk                                                               *
 * (10) Prototype:                                                            *
 *      solvequ.h                                                             *
 *      int gcpdsk(double huge *vetx, double huge *ma, long int huge *maxa,   *
 *                 double huge *veth, long int neq, long int itmax,           *
 *                 double eps, int codcvg);                                   *
 * (11) Headers necessarios:                                                  *
 *      stdio.h                                                               *
 *      matematc.h                                                            *
 *      solvequ.h                                                             *
 *      util.h                                                                *
 *      dbmanagm.h                                                            *
 * (12) Mensagens de erro:                                                    *
 *      ---                                                                   *
 * (13) Depuracao:                                                            *
 *      ---                                                                   *
 * (14) Portabilidade:                                                        *
 *      C standard.                                                           *
 * (15) Acesso:                                                               *
 *      Livre ao usuario.                                                     *
 *                                                                            *
 ******************************************************************************
*/

#include "matematc.h"

/*
**  Funcao que resolve o sistema
**            -1
**  vetaux = C  * vetres
**  C -> matriz de precondicionamento
*/
static void prauxsk(double huge *vetx, double huge *ma, long int huge *maxa,
                    double huge *vetb, long int neq)
// double huge *vetx,      pointer para o vetor resposta 'vetaux'             
//        huge *ma,        pointer para os elementos da matriz do sistema     
//        huge *vetb;      pointer para os elementos do vetor 'vetres'        
// long int huge *maxa;    pointer para os elementos do vetor 'maxa'          
// long int neq;           numero de equacoes do sistema                      
    {
    long int poseldg;      /* recebe a posicao do elemento da diagonal principal
                              de 'ma'                                            */
    double vdiag;          /* recebe o valor do elemento da diagonal principal
                              de 'ma'                                            */
    register long int i;   /* variavel utilizada como contador                   */

    for (i = 0; i < neq; i++)
        {
        poseldg = *(maxa + i);
        vdiag = *(ma + poseldg);
        *vetx = (*vetb)/vdiag;
        vetx++;
        vetb++;
        }
    }

/*
** Metodo do Gradiente Conjugado com Precondicionamento
** Matriz de Precondicionamento Diagonal (elementos da diagonal principal da Matriz do Sistema)
** Matriz do Sistema Simetrica Sky - line (colunas ascendentes)
** A rotina devolve:
** 0 -> se o calculo nao convergir ou
**      se o numero maximo de iteracoes for superado;
** 1 -> se o calculo convergir.
*/

int gcpdsk(double huge *vetx, double huge *ma, long int huge *maxa,
           double huge *veth, long int neq, long int itmax,
           double eps, int codcvg)
    {
    double huge *vetaux,   /* vetor auxiliar */
           huge *pvetaux,  /* pointer para vetor auxiliar */
           huge *vetres,   /* vetor residuo (ou gradiente) */
           huge *pvetres,  /* pointer para vetor residuo */
           huge *vetp,     /* vetor direcao conjugada */
           huge *pvetp,    /* pointer para vetor direcao conjugada */
           huge *pveth,    /* pointer para vetor termos independentes */
           huge *pvetx;    /* pointer para vetor resposta */
    double a, b, d,
           delta0,
           delta1,         /* variaveis auxiliares */
           ctecvg,         /* constante de convergencia */
           norinfres,      /* recebe a norma infinita do vetor residuo */
           noreures,       /* recebe a norma euclideana do vetor residuo */
           norinfh,        /* recebe a norma infinita do vetor termos independentes */
           noreuh;         /* recebe a norma euclideana do vetor termos independentes */
    register long int j,
                      k;   /* variaveis utlizadas como contadores */
    int converge;          /* variavel que retorna o codigo para convergencia
                              0 - se nao convergir
                              1 - se convergir
                              2 - se omega nao for valido  */

    /* aloca espaco de memoria para vetores */

    vetaux = (double huge *) mMalloc(sizeof(double) * neq);
    vetres = (double huge *) mMalloc(sizeof(double) * neq);
    vetp   = (double huge *) mMalloc(sizeof(double) * neq);

    pvetaux = vetaux;
    pvetres = vetres;
    pveth   = veth;
    pvetx   = vetx;

    k = 1;
    converge = 0;
    a = b = d = ctecvg = 0.0;

    /*
    ** testa se ha erro no codigo de tipo de convergencia
    ** se ha assume codcvg = 1
    */

    if ((codcvg != 1) && (codcvg != 2))
       codcvg = 1;

    /*
    ** calculo da constante de convergencia
    */

    switch (codcvg)
        {
        case 1 : noreuh = normaev(veth, neq);
                 ctecvg = noreuh * eps;
                 break;

        case 2 : norinfh = norinfv(veth, neq);
                 ctecvg = norinfh * eps;
                 break;

        default : break;
        }

    /*
    ** inicio do metodo
    */

    prodmskv(vetaux, ma, maxa, vetx, neq);
    for (j = 0; j < neq; j++)
        *pvetres++ = (*pvetaux++) - (*pveth++);
    pvetaux = vetaux;
    pvetres = vetres;
    pveth = veth;
    pvetp = vetp;
    prauxsk(vetaux, ma, maxa, vetres, neq);       /* resolve o sistema     */
    for (j = 0; j < neq; j++)                     /*           -1          */
        {                                         /* vetaux = C   * vetres */
        *pvetp = -(*pvetaux);                     /* vetp = - vetaux       */
        pvetp++;
        pvetaux++;
        }
    pvetp = vetp;
    pvetaux = vetaux;
    delta0 = scalv(vetres, vetaux, neq);

    /* teste de convergencia */

    switch (codcvg)
        {
        case 1: noreures = normaev(vetres, neq);
                if (noreures <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        case 2: norinfres = norinfv(vetres, neq);
                if (norinfres <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        default: break;
        }
    while (k <= itmax)
        {
        prodmskv(vetaux, ma, maxa, vetp, neq);
        d = scalv(vetp, vetaux, neq);
        a = delta0/d;
        for (j = 0; j < neq; j++)                 /* calculo do vetor */
            {                                     /* resposta         */
            *pvetx = *pvetx + a * (*pvetp);
            pvetp++;
            pvetx++;
            }
        pvetp = vetp;
        pvetx = vetx;
        for (j = 0; j < neq; j++)                 /* calculo do vetor */
            {                                     /* resuiduo         */
            *pvetres = *pvetres + a * (*pvetaux);
            pvetaux++;
            pvetres++;
            }
        pvetres = vetres;
        pvetaux = vetaux;
        prauxsk(vetaux, ma, maxa, vetres, neq);
        delta1 = scalv(vetres, vetaux, neq);
        switch (codcvg)
            {
            case 1: noreures = normaev(vetres, neq);
                    if (noreures <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            case 2: norinfres = norinfv(vetres, neq);
                    if (norinfres <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            default: break;
            }
        b = delta1/delta0;
        delta0 = delta1;
        for (j = 0; j < neq; j++)                 /* calculo do vetor  */
            {                                     /* direcao conjugada */
            *pvetp =  b * (*pvetp) - *pvetaux;
            pvetaux++;
            pvetp++;
            }
        pvetaux = vetaux;
        pvetp = vetp;
        k++;
        }
    mFree(vetaux);                                /* deleta vetores alocados */
    mFree(vetres);
    mFree(vetp);
    return (converge);
    }




