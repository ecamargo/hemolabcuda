#include "matematc.h"  

acLimit2 GetLimit(acPoint2 huge *vet, long n)
    {
    acLimit2 limit;
    double xmin, xmax, ymin, ymax;
    long i;

    if (n <= 1)
        {
        xmin = xmax = vet[0].x;
        ymin = ymax = vet[0].y;
        }

    else
        {
        if (vet[0].x > vet[1].x)
            {
            xmax = vet[0].x;
            xmin = vet[1].x;
            }
        else
            {
            xmax = vet[1].x;
            xmin = vet[0].x;
            }

        if (vet[0].y > vet[1].y)
            {
            ymax = vet[0].y;
            ymin = vet[1].y;
            }
        else
            {
            ymax = vet[1].y;
            ymin = vet[0].y;
            }

        for (i = 2; (i + 2) <= n; i += 2)
            {
            if (vet[i].x > vet[i + 1].x)
                {
                if (vet[i].x     > xmax) xmax = vet[i].x;
                if (vet[i + 1].x < xmin) xmin = vet[i + 1].x;
                }
            else
                {
                if (vet[i + 1].x > xmax) xmax = vet[i + 1].x;
                if (vet[i].x     < xmin) xmin = vet[i].x;
                }

            if (vet[i].y > vet[i + 1].y)
                {
                if (vet[i].y     > ymax) ymax = vet[i].y;
                if (vet[i + 1].y < ymin) ymin = vet[i + 1].y;
                }
            else
                {
                if (vet[i + 1].y > ymax) ymax = vet[i + 1].y;
                if (vet[i].y     < ymin) ymin = vet[i].y;
                }
            }

        if (i < n)
            {
            if (xmax < vet[i].x)      xmax = vet[i].x;
            else if (xmin > vet[i].x) xmin = vet[i].x;

            if (ymax < vet[i].y)      ymax = vet[i].y;
            else if (ymin > vet[i].y) ymin = vet[i].y;
            }
        }

    limit.xmin = xmin;
    limit.xmax = xmax;
    limit.ymin = ymin;
    limit.ymax = ymax;
    return(limit);
    }
