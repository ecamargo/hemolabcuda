/*
 ****************************************************************************
 *                                                                          *
 * int gradcjst(vetx, ma, veth, neq, itmax, eps, codcvg)                    *
 *                                                                          *
 * (01) Objetivo:                                                           *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo do   *
 *      Gradiente Conjugado Standard, sem tecnica de precondicionamento.    *
 * (02) Forma de utilizacao:                                                *
 *      variavel inteira = gradcjst(vetx, ma, veth, neq, itmax, eps,        *
 *                                  codcvg);                                *
 * (03) Descricao:                                                          *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo do    *
 *      Gradiente Conjugado, versao Standard, sem precondicionamento. A     *
 *      matriz do sistema e simetrica armazenada na forma supradiagonal     *
 *      por linhas. A rotina devolve um codigo informando se o calculo      *
 *      convergiu ou nao.                                                   *
 * (04) Palavras chave:                                                     *
 *      equacoes                                                            *
 *      iterativo                                                           *
 *      simetrica                                                           *
 *      gradiente                                                           *
 * (05) Referencia:                                                         *
 *      - Conjugate Direction Methods in Optimization                       *
 *        Springer-Verlag New York Inc. , 1980                              *
 *        pag.120                                                           *
 *        Magnus Hestenes                                                   *
 *      - Finite Element Solution of Boundary Value Problems                *
 *        Academic Press, Inc.  1984                                        *
 *        pag.23                                                            *
 *        Axelsson & Barker                                                 *
 * (06) Data e autor:                                                       *
 *      Rio de Janeiro, setembro de 1990                                    *
 *      Roberto Eduardo Garcia.                                             *
 * (07) Argumentos:                                                         *
 *      <> *vetx  - pointer para o vetor resposta 'vetx'    - double huge   *
 *      -> *ma    - pointer para os elementos da matriz 'ma'- double huge   *
 *      -> *veth  - pointer para o vetor dos elementos                      *
 *                                            independentes - double huge   *
 *      -> neq    - numero de equacoes do sistema           - long          *
 *      -> itmax  - numero maximo de iteracoes              - long          *
 *      -> eps    - constante para calculo da convergencia  - double        *
 *      -> codcvg - codigo de tipo de convergencia          - int           *
 *                  1 - norma euclideana                                    *
 *                  2 - norma infinita                                      *
 * (08) Variaveis externas utilizadas:                                      *
 *      ---                                                                 *
 * (09) Chamadas:                                                           *
 *      scalv                                                               *
 *      prodmsv                                                             *
 *      normaev                                                             *
 *      norinfv                                                             *
 * (10) Prototype:                                                          *
 *      solvequ.h                                                           *
 *      int gradcjst(double huge *vetx, double huge *ma, double huge *veth, *
 *                   long int neq, long int itmax, double eps, int codcvg); *
 * (11) Headers necessarios:                                                *
 *      stdio.h                                                             *
 *      solvequ.h                                                           *
 *      matematc.h                                                          *
 *      util.h                                                              *
 *      dbmanagm.h                                                          *
 * (12) Mensagens de erro:                                                  *
 *      ---                                                                 *
 * (13) Depuracao:                                                          *
 *      ---                                                                 *
 * (14) Portabilidade:                                                      *
 *      C standard.                                                         *
 * (15) Acesso:                                                             *
 *      Livre ao usuario.                                                   *
 *                                                                          *
 ****************************************************************************
*/

#include "matematc.h"

/*
** Metodo do Gradiente Conjugado (Standard) sem precondicionamento.
** Matriz do Sistema simetrica supradiagonal por linhas.
** A rotina devolve:
** 0 -> se o calculo nao convergir ou
**      se o numero maximo de iteracoes for superado;
** 1 -> se o calculo convergir.
*/

int gradcjst(double huge *vetx, double huge *ma, double huge *veth,
             long neq, long itmax, double eps, int codcvg)
    {
    double huge *vetaux,    /* vetor auxiliar */
           huge *pvetaux,   /* pointer para vetor auxiliar */
           huge *vetres,    /* vetor residuo (ou gradiente) */
           huge *pvetres,   /* pointer para vetor residuo */
           huge *vetp,      /* vetor direcao conjugada */
           huge *pvetp,     /* pointer para vetor direcao conjugada */
           huge *pveth,     /* pointer para vetor termos independentes */
           huge *pvetx;     /* pointer para vetor resposta */
    double a, b, d,
           delta0,
           delta1,          /* variaveis auxiliares */
           ctecvg,          /* constante de convergencia */
           norinfres,       /* recebe a norma infinita do vetor residuo */
           noreures,        /* recebe a norma euclideana do vetor residuo */
           norinfh,         /* recebe a norma infinita do vetor termos independentes */
           noreuh;          /* recebe a norma euclideana do vetor termos independentes */
    register long int j,
                      k;   /* variaveis utlizadas como contadores */
    int converge;          /* variavel que retorna o codigo para convergencia
                              0 - se nao convergir
                              1 - se convergir     */

    /* aloca espaco de memoria para vetores */

    vetaux = (double huge *) mMalloc(sizeof(double) * neq);
    vetres = (double huge *) mMalloc(sizeof(double) * neq);
    vetp   = (double huge *) mMalloc(sizeof(double) * neq);

    pvetaux = vetaux;
    pvetres = vetres;
    pvetp   = vetp;
    pveth   = veth;
    pvetx   = vetx;

    k = 1;
    converge = 0;
    a = b = d = ctecvg = 0.0;

    /*
    ** testa se ha erro no codigo de tipo de convergencia
    ** se ha assume codcvg = 1
    */

    if ((codcvg != 1) && (codcvg != 2))
       codcvg = 1;

    /*
    ** calculo da constante de convergencia
    */

    switch (codcvg)
        {
        case 1 : noreuh = normaev(veth, neq);
                 ctecvg = noreuh * eps;
                 break;

        case 2 : norinfh = norinfv(veth, neq);
                 ctecvg = norinfh * eps;
                 break;

        default : break;
        }

    /*
    ** inicio do metodo
    */

    prodmsv(vetaux, ma, vetx, neq);
    for (j = 0; j < neq; j++)
        *pvetp++ = *pvetres++ = (*pveth++) - (*pvetaux++);
    pvetaux = vetaux;
    pvetres = vetres;
    pvetp = vetp;
    pveth = veth;
    delta0 = scalv(vetres, vetres, neq);

    /* teste de convergencia */

    switch (codcvg)
        {
        case 1: noreures = normaev(vetres, neq);
                if (noreures <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        case 2: norinfres = norinfv(vetres, neq);
                if (norinfres <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        default: break;
        }
    while (k <= itmax)
        {
        prodmsv(vetaux, ma, vetp, neq);
        d = scalv(vetp, vetaux, neq);
        a = delta0/d;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* resposta */
            *pvetx = *pvetx + a * (*pvetp);
            pvetp++;
            pvetx++;
            }
        pvetp = vetp;
        pvetx = vetx;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* residuo          */
            *pvetres = *pvetres - a * (*pvetaux);
            pvetaux++;
            pvetres++;
            }
        pvetres = vetres;
        pvetaux = vetaux;
        delta1 = scalv(vetres, vetres, neq);
        switch (codcvg)
            {
            case 1: noreures = normaev(vetres, neq);
                    if (noreures <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            case 2: norinfres = norinfv(vetres, neq);
                    if (norinfres <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            default: break;
            }
        b = delta1/delta0;
        delta0 = delta1;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* direcao conjugada */
            *pvetp = *pvetres + b * (*pvetp);
            pvetres++;
            pvetp++;
            }
        pvetres = vetres;
        pvetp = vetp;
        k++;
        }
    mFree(vetaux);                                /* deleta vetores alocados */
    mFree(vetres);
    mFree(vetp);
    return (converge);
    }




