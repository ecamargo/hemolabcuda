/*
 ******************************************************************************
 *                                                                            *
 * int grcjprc(vetx, ma, veth, neq, itmax, eps, omega, codpr, codcvg)         *
 *                                                                            *
 * (01) Objetivo:                                                             *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo do     *
 *      Gradiente Conjugado utilizando tecnica de precondicionamento.         *
 * (02) Forma de utilizacao:                                                  *
 *      variavel inteira = grcjprc(vetx, ma, veth, neq, itmax, eps, omega,    *
 *                                 codpr, codcvg);                            *
 * (03) Descricao:                                                            *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo do      *
 *      Gradiente Conjugado, utilizando matriz de precondicionamento cabendo  *
 *      ao usuario escolher atraves do parametro 'codpr' uma das tres matrizes*
 *      de precondicionamento oferecidas. A matriz do sistema e do tipo       *
 *      simetrica armazenada na forma supradiagonal por linhas. A rotina      *
 *      devolve um codigo informando se o calculo convergiu ou nao.           *
 * (04) Palavras chave:                                                       *
 *      equacoes                                                              *
 *      iterativo                                                             *
 *      simetrica                                                             *
 *      gradiente                                                             *
 *      precondicionamento                                                    *
 * (05) Referencia:                                                           *
 *      - Finite Element Solution of Boundary Value Problems                  *
 *        Academic Press, Inc  1984                                           *
 *        pag. 49                                                             *
 *        Axelsson & Barker                                                   *
 *      - Revista Internacional de Metodos Numericos para Calculo y Diseno    *
 *        en Ingenieria                                                       *
 *        Vol.2 (1986), pag.27-41                                             *
 * (06) Data e autor:                                                         *
 *      Rio de Janeiro, fevereiro de 1991                                     *
 *      Roberto Eduardo Garcia.                                               *
 * (07) Argumentos:                                                           *
 *      <> *vetx  - pointer para o vetor resposta 'vetx'    - double huge     *
 *      -> *ma    - pointer para a matriz do sistema.       - double huge     *
 *      -> *veth  - pointer para o vetor dos elementos                        *
 *                                            independentes - double huge     *
 *      -> neq    - numero de equacoes do sistema           - long            *
 *      -> itmax  - numero maximo de iteracoes              - long            *
 *      -> eps    - constante para calculo da convergencia  - double          *
 *      -> omega  - parametro de sobrerelaxacao             - double          *
 *      -> codpr  - codigo que define o tipo de matriz de                     *
 *                  precondicionamento                      - int             *
 *                  0 - Gauss - Seidel Simetrizado                            *
 *                  1 - SSOR (sem fator "2 - omega")                          *
 *                  2 - SSOR                                                  *
 *      -> codcvg - codigo de tipo de convergencia          - int             *
 *                  1 - norma euclideana                                      *
 *                  2 - norma infinita                                        *
 * (08) Variaveis externas utilizadas:                                        *
 *      ---                                                                   *
 * (09) Chamadas:                                                             *
 *      scalv                                                                 *
 *      prodmsv                                                               *
 *      normaev                                                               *
 *      norinfv                                                               *
 *      prodaux                                                               *
 *      bcksubst                                                              *
 *      fwdsubst                                                              *
 * (10) Prototype:                                                            *
 *      solvequ.h                                                             *
 *      int grcjprc(double huge *vetx, double huge *ma, double huge *veth,    *
 *                  long int neq, long int itmax, double eps, double  omega,  *
 *                  int codpr, int codcvg);                                   *
 * (11) Headers necessarios:                                                  *
 *      stdio.h                                                               *
 *      solvequ.h                                                             *
 *      matematc.h                                                            *
 *      util.h                                                                *
 *      dbmanagm.h                                                            *
 * (12) Mensagens de erro:                                                    *
 *      ---                                                                   *
 * (13) Depuracao:                                                            *
 *      ---                                                                   *
 * (14) Portabilidade:                                                        *
 *      C standard.                                                           *
 * (15) Acesso:                                                               *
 *      Livre ao usuario.                                                     *
 *                                                                            *
 ******************************************************************************
*/

#include "matematc.h"


static void fwdsubst(double huge *vetx, double huge *mas, double huge *vetb,
              long int neq, double omega);
static void prodaux(double huge *vetb, double huge *mas,
             long int neq);
static void bcksubst(double huge *vetx, double huge *mas, double huge *vetb,
              long int neq, double omega);


/*
** Funcoes auxiliares
*/

/*
** Funcao que realiza a etapa de 'forward substitution' do sistema
** 'ma'* 'vaux' =  'vetres'
** acumulando a resposta no vetor 'vaux'
*/
static void fwdsubst(double huge *vetx, double huge *mas, double huge *vetb,
                     long int neq, double omega)
// double huge *vetx,    pointer para vetor resposta vaux 
//        huge *mas,     pointer para matriz do sistema 
//        huge *vetb;    pointer para vetor residuo 
// double omega;         omega 
// long int neq;         numero de equacoes 
    {
    double huge *pdiagm,      /* pointer para elementos da diagonal principal
                                 da matriz do sistema */
           huge *pauxm,       /* pointer auxiliar para elementos subdiagonais
                                 da matriz do sistema */
           huge *pvetx,       /* pointer auxiliar para vetor resposta */
           huge *pvetb;       /* pointer auxiliar para vetor residuo */
    double temp;              /* variavel auxiliar que acumula produtos */
    long int pulopm,          /* numero de posicoes que o pointer pauxm
                                 deve avancar */
             pulodg,          /* numero de posicoes que o pointer pdiagm deve
                                 avancar */
             neqm1;           /* recebe o valor numero de equacoes menos 1 */
    register long int i,
                      j;      /* variaveis utilizadas como contadores */

    pulodg = neq;
    neqm1 = neq - 1;
    pauxm = pdiagm = mas;
    pvetb = vetb;
    for (j = 0; j < neq; j++)
        {
        pulopm = neqm1;
        pauxm = mas + j;
        pvetx = vetx;
        temp = 0.0;
        for (i = 0; i < j; i++)
            {
            temp += (*pauxm) * (*pvetx++);
            pauxm += pulopm;
            pulopm--;
            }
        *pvetx = (*pvetb - temp) * (omega / (*pdiagm));
        pvetb++;
        pdiagm += pulodg;
        pulodg--;
        }
    }

/*
** Funcao que realiza o produto entre o elemento da diagonal principal
** da matriz do sistema e o correspondente elemento do vetor resposta
** 'vaux' fornecido pela funcao fwdsbstsk acumulando o resultado no vetor
** 'vaux'.
*/
static void prodaux(double huge *vetb, double huge *mas,
             long int neq)
// double huge *vetb,            pointer para vetor 'vaux' fornecido por
//                                  fwdsubst e que tambem acumula a resposta 
//        huge *mas;             pointer para os elementos da matriz do
//                                  sistema 
// long int neq;                 numero de equacoes 
    {
    long int pulomas;         /* numero de posicoes que o pointer mas deve avancar */
    register long int i;      /* variavel utilizada como contador */

    pulomas = neq;
    for (i = 0; i < neq; i++)
        {
        *vetb = (*mas) * (*vetb);
        mas += pulomas;
        vetb++;
        pulomas--;
        }
    }

/*
** Funcao que realiza a etapa de 'back substitution' do sistema
** 'ma' * 'vetaux' = 'vaux'
** acumulando a resposta no vetor 'vetaux'
*/
static void bcksubst(double huge *vetx, double huge *mas, double huge *vetb,
              long int neq, double omega)
// double huge *vetx,            pointer para vetor resposta 'vetaux' 
//        huge *mas,             pointer para elementos da matriz do sistema 
//        huge *vetb;            pointer para vetor 'vaux' fornecido por prodaux 
// double omega;                 omega 
// long int neq;                 numero de equacoes 
    {
    double huge *pdiagm,      /* pointer para elementos da diagonal principal
                                 da matriz do sistema */
           huge *pauxm,       /* pointer auxiliar para elementos supradiagonais
                                 da matriz do sistema */
           huge *pvetx;       /* pointer auxiliar para vetor resposta */
    double temp;              /* variavel auxiliar que aculula produtos */
    long int neqm1,           /* recebe o valor numero de equacoes menos 1 */
             nelem,           /* numero de elementos da matriz do sistema */
             pulodg;          /* numero de posicoes que o pointer pdiagm deve
                                 avancar */
    register long int j,
                      i;      /* variaveis utilizadas como contadores */

    pdiagm = pauxm = mas;
    neqm1 = neq - 1;
    nelem = ((neq + 1) * neq)/2;
    pdiagm = mas + (nelem - 1);
    pulodg = 1;
    for (i = neqm1; i >= 0 ; i--)
        {
        pvetx = vetx + i + 1;
        pauxm = pdiagm + 1;
        temp = 0.0;
        for (j = i + 1; j < neq; j++)
            temp += (*pauxm++) * (*pvetx++);
        *(vetx + i) = ((*(vetb + i)) - temp)*(omega/(*pdiagm));
        pulodg++;
        pdiagm -= pulodg;
        }
    }

/*
** Metodo do Gradiente Conjugado com Precondicionamento
** Matriz do Sistema simetrica supradiagonal por linhas.
** O usuario devera escolher uma dentre as tres matrizes de precondicionamento
** de acordo com o parametro de entrada codpr
** se codpr = 0 a matriz de precondicionamanto e chamada Gauss-Seidel Simetrizada
**
**                                -1        t
**                C = 1 (2D + L) D  (2D + L)
**                   ---
**                    2
**
** se codpr = 1 a matriz de precondicionamento e chamada SSOR (sem o fator "2 - w")
**
**                                   -1           t
**                C = 1 (1/w D + L) D  (1/w D + L)
**                   ---
**                    w
**
** se codpr = 2 a matriz de precondicionamento e chamada SSOR
**
**                                          -1           t
**                C =    1    ( 1/w D + L) D  (1/w D + L)
**                    -------
**                    w(2 - w)
**
** A rotina devolve:
** 0 -> se o calculo nao convergir ou
**      se o numero maximo de iteracoes for superado;
** 1 -> se o calculo convergir;
** 2 -> se o valor de omega estiver fora da faixa permitida.
**      omega deve estar entre 0 < w < 2
*/

int grcjprc(double huge *vetx, double huge *ma, double huge *veth,
            long neq, long itmax, double eps, double omega,
            int codpr, int codcvg)
    {
    double huge *vetaux,   
           huge *vaux,     /* vetores auxiliares */
           huge *pvetaux,  /* pointer para vetor auxiliar */
           huge *vetres,   /* vetor residuo (ou gradiente) */
           huge *pvetres,  /* pointer para vetor residuo */
           huge *vetp,     /* vetor direcao conjugada */
           huge *pvetp,    /* pointer para vetor direcao conjugada */
           huge *pveth,    /* pointer para vetor termos independentes */
           huge *pvetx;    /* pointer para vetor resposta */
    double a, b, d,
           delta0,
           delta1,         /* variaveis auxiliares */
           c,
           ctecvg,         /* constante de convergencia */
           norinfres,      /* recebe norma infinita do vetor residuo */
           noreures,       /* recebe norma euclideana do vetor residuo */
           norinfh,        /* recebe norma infinita do vetor termos independentes */
           noreuh;         /* recebe norma euclideana do vetor termos independentes */
    register long int j,
                      k;   /* variaveis utlizadas como contadores */
    int converge;          /* variavel que retorna o codigo para convergencia
                              0 - se nao convergir
                              1 - se convergir
                              2 - se omega nao for valido  */

    k = 1;
    converge = 0;
    a = b = d = ctecvg = 0.0;

    /*
    ** testa se ha erro no codigo de tipo de convergencia
    ** se ha assume codcvg = 1
    */

    if ((codcvg != 1) && (codcvg != 2))
       codcvg = 1;

    /*
    ** teste que verifica o valor de omega (0 < w < 2)
    */

    if ((omega < 0.) || (omega >= 2.))
        return (converge = 2);

    /*
    ** calculo da constante cuja inversa multiplica a matriz
    ** de precondicionamento
    */

    switch (codpr)
        {
        /* Matriz de Precondicionamento Gauss-Seidel Simetrizada */
        case 0: if (omega != .5)
                    omega = .5;
                c = 2.;
                break;

        /* Matriz de Precondicionamento SSOR eliminando "2-w" */
        case 1: c = omega;
                break;

        /* Matriz de Precondicionamento SSOR */
        case 2: c = (2. - omega) * omega;
                break;

        /* se codpr estiver errado assume Matriz SSOR */
        default: c = (2. - omega) * omega;
                 break;
        }

    /* aloca espaco de memoria para vetores */

    vetaux = (double huge *) mMalloc(sizeof(double) * neq);
    vaux   = (double huge *) mMalloc(sizeof(double) * neq);
    vetres = (double huge *) mMalloc(sizeof(double) * neq);
    vetp   = (double huge *) mMalloc(sizeof(double) * neq);

    pvetaux = vetaux;
    pvetres = vetres;
    pveth   = veth;
    pvetx   = vetx;

    /*
    ** calculo da constante de convergencia
    */

    switch (codcvg)
        {
        case 1 : noreuh = normaev(veth, neq);
                 ctecvg = noreuh * eps;
                 break;

        case 2 : norinfh = norinfv(veth, neq);
                 ctecvg = norinfh * eps;
                 break;

        default : break;
        }

    /*
    ** inicio do metodo
    */

    prodmsv(vetaux, ma, vetx, neq);
    for (j = 0; j < neq; j++)
        *pvetres++ = (*pvetaux++) - (*pveth++);
    pvetaux = vetaux;
    pvetres = vetres;
    pveth = veth;
    pvetp = vetp;
    fwdsubst(vaux, ma, vetres, neq, omega);          /* resolve o sistema     */
    prodaux(vaux, ma, neq);                          /*           -1          */
    bcksubst(vetaux, ma, vaux, neq, omega);          /* vetaux = C   * vetres */
    for (j = 0; j < neq; j++)
         {
         *pvetaux = (*pvetaux) * c;
         pvetaux++;
         }
    pvetaux = vetaux;
    for (j = 0; j < neq; j++)                        /* vetp = - vetaux */
        {
        *pvetp = -(*pvetaux);
        pvetp++;
        pvetaux++;
        }
    pvetp = vetp;
    pvetaux = vetaux;
    delta0 = scalv(vetres, vetaux, neq);

    /* teste de convergencia */

    switch (codcvg)
        {
        case 1: noreures = normaev(vetres, neq);
                if (noreures <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        case 2: norinfres = norinfv(vetres, neq);
                if (norinfres <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        default: break;
        }
    while (k <= itmax)
        {
        prodmsv(vetaux, ma, vetp, neq);
        d = scalv(vetp, vetaux, neq);
        a = delta0/d;
        for (j = 0; j < neq; j++)                    /* calculo do vetor */
            {                                        /* resposta         */
            *pvetx = *pvetx + a * (*pvetp);
            pvetp++;
            pvetx++;
            }
        pvetp = vetp;
        pvetx = vetx;
        for (j = 0; j < neq; j++)
            {
            *pvetres = *pvetres + a * (*pvetaux);
            pvetaux++;
            pvetres++;
            }
        pvetres = vetres;
        pvetaux = vetaux;
        fwdsubst(vaux, ma, vetres, neq, omega);
        prodaux(vaux, ma, neq);
        bcksubst(vetaux, ma, vaux, neq, omega);
        for (j = 0; j < neq; j++)
            {
            *pvetaux = (*pvetaux) * c;
            pvetaux++;
            }
        pvetaux = vetaux;
        delta1 = scalv(vetres, vetaux, neq);
        switch (codcvg)
            {
            case 1: noreures = normaev(vetres, neq);
                    if (noreures <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            case 2: norinfres = norinfv(vetres, neq);
                    if (norinfres <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            default: break;
            }
        b = delta1/delta0;
        delta0 = delta1;
        for (j = 0; j < neq; j++)                    /* calculo do vetor  */
            {                                        /* direcao conjugada */
            *pvetp =  b * (*pvetp) - *pvetaux;
            pvetaux++;
            pvetp++;
            }
        pvetaux = vetaux;
        pvetp = vetp;
        k++;
        }
    mFree(vetaux);                                /* deleta vetores alocados */
    mFree(vaux);
    mFree(vetres);
    mFree(vetp);
    return (converge);
    }


