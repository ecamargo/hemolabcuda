/*
 ******************************************************************************
 *                                                                            *
 * int grcjprsk(vetx, ma, maxa, veth, neq, itmax, eps, omega, codpr, codcvg)  *
 *                                                                            *
 * (01) Objetivo:                                                             *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo do     *
 *      Gradiente Conjugado utilizando tecnica de precondicionamento.         *
 * (02) Forma de utilizacao:                                                  *
 *      variavel inteira = grcjprsk(vetx, ma, maxa, veth, neq, itmax, eps,    *
 *                                  omega, codpr, codcvg);                    *
 * (03) Descricao:                                                            *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo do      *
 *      Gradiente Conjugado, utilizando matriz de precondicionamento cabendo  *
 *      ao usuario escolher atraves do parametro 'codpr' uma das tres matrizes*
 *      de precondicionamento oferecidas. A matriz do sistema e do tipo       *
 *      sky-line por colunas ascendentes e esta armazenada nos vetores 'ma' e *
 *      'maxa'. A rotina devolve um codigo informando se o calculo convergiu  *
 *      ou nao.                                                               *
 * (04) Palavras chave:                                                       *
 *      equacoes                                                              *
 *      iterativo                                                             *
 *      sky-line                                                              *
 *      gradiente                                                             *
 *      precondicionamento                                                    *
 * (05) Referencia:                                                           *
 *      - Finite Element Solution of Boundary Value Problems                  *
 *        Academic Press, Inc  1984                                           *
 *        pag. 49                                                             *
 *        Axelsson & Barker                                                   *
 *      - Revista Internacional de Metodos Numericos para Calculo y Diseno    *
 *        en Ingenieria                                                       *
 *        Vol.2 (1986), pag.27-41                                             *
 * (06) Data e autor:                                                         *
 *      Rio de Janeiro, fevereiro de 1991                                     *
 *      Roberto Eduardo Garcia.                                               *
 * (07) Argumentos:                                                           *
 *      <> *vetx  - pointer para o vetor resposta 'vetx'    - double huge     *
 *      -> *ma    - pointer para vetor que contem os elementos da matriz      *
 *                  do sistema.                             - double huge     *
 *      -> *maxa  - pointer para o vetor que contem a posicao dos elementos   *
 *                  da diagonal principal de 'ma'           - long huge       *
 *      -> *veth  - pointer para o vetor dos elementos                        *
 *                                            independentes - double huge     *
 *      -> neq    - numero de equacoes do sistema           - long            *
 *      -> itmax  - numero maximo de iteracoes              - long            *
 *      -> eps    - constante para calculo da convergencia  - double          *
 *      -> omega  - parametro de sobrerelaxacao             - double          *
 *      -> codpr  - codigo que define o tipo de matriz de                     *
 *                  precondicionamento                      - int             *
 *                  0 - Gauss - Seidel Simetrizado                            *
 *                  1 - SSOR (sem fator "2 - omega")                          *
 *                  2 - SSOR                                                  *
 *      -> codcvg - codigo de tipo de convergencia          - int             *
 *                  1 - norma euclideana                                      *
 *                  2 - norma infinita                                        *
 * (08) Variaveis externas utilizadas:                                        *
 *      ---                                                                   *
 * (09) Chamadas:                                                             *
 *      scalv                                                                 *
 *      prodmskv                                                              *
 *      normaev                                                               *
 *      norinfv                                                               *
 *      prodauxsk                                                             *
 *      bcksbstsk                                                             *
 *      fwdsbstsk                                                             *
 * (10) Prototype:                                                            *
 *      solvequ.h                                                             *
 *      int grcjprsk(double huge *vetx, double huge *ma, long int huge *maxa, *
 *                   double huge *veth, long int neq, long int itmax,         *
 *                   double eps, double omega, int codpr, int codcvg);        *
 * (11) Headers necessarios:                                                  *
 *      stdio.h                                                               *
 *      matematc.h                                                            *
 *      solvequ.h                                                             *
 *      util.h                                                                *
 *      dbmanagm.h                                                            *
 * (12) Mensagens de erro:                                                    *
 *      ---                                                                   *
 * (13) Depuracao:                                                            *
 *      ---                                                                   *
 * (14) Portabilidade:                                                        *
 *      C standard.                                                           *
 * (15) Acesso:                                                               *
 *      Livre ao usuario.                                                     *
 *                                                                            *
 ******************************************************************************
*/

#include "matematc.h"


static void prodauxsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      long int neq);
static void bcksbstsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      double huge *vetb, long int neq, double omega);
static void fwdsbstsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      double huge *vetb, long int neq, double omega);

/*
** Funcoes auxiliares
*/

/*
** Funcao que realiza a etapa de 'forward substitution' do sistema
** 'ma'* 'vaux' =  'vetres'
** acumulando a resposta no vetor 'vaux'
*/
static void fwdsbstsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      double huge *vetb, long int neq, double omega)
//double huge *vetx,        pointer para vetor resposta vaux 
//       huge *ma,          pointer para vetor que contem a matriz do sistema 
//       huge *vetb;        pointer para vetor residuo 
//long int huge *maxa;      pointer para vetor que contem a posicao dos
//                           elementos da diagonal da matriz do sistema 
//long int neq;             numero de equacoes 
//double omega;             parametro omega 
    {
    double huge *pvetx;   /* pointer auxiliar para o vetor resposta */
    long int primel,      /* posicao do elemento da diagonal principal na matriz */
             ultel;       /* posicao do elemento acima da diagonal principal da
                             matriz do sistema */
    double temp,          /* acumula os produtos temporarios */
           akk;           /* recebe o valor do elemento da diagonal principal */
    register long int j,
                      i;  /* contadores */

    pvetx = vetx;
    for (i = 0; i < neq; i++)
        {
        primel = *(maxa + i);
        ultel =  *(maxa + i + 1);
        pvetx = vetx + (i - 1);
        temp = 0.0;
        for (j = primel + 1; j < ultel; j++)
            temp += *(ma + j) * (*pvetx--);
        akk = *(ma + primel);
        *(vetx + i) = (*(vetb + i) - temp) * (omega/akk);
        }
    }

/*
** Funcao que realiza o produto entre o elemento da diagonal principal
** da matriz do sistema e o correspondente elemento do vetor resposta
** 'vaux' fornecido pela funcao fwdsbstsk acumulando o resultado no vetor
** 'vaux'.
*/
static void prodauxsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      long int neq)
// double huge *vetx,            pointer para vetor 'vaux' fornecido por
//                               fwdsbstsk, e que tambem acumula a resposta 
//        huge *ma;              pointer para vetor que contem a matriz do sistema 
// long int huge *maxa;          pointer para vetor que contem a posicao dos
//                               elementos da diagonal da matriz do sistema 
// long int neq;                 numero de equacoes 
    {
    long int poseldg;        /* recebe a posicao do elemento da diagonal principal */
    double vdiag;            /* recebe o valor do elemento da diagonal principal */
    register long int i;     /* contador */

    for (i = 0; i < neq; i++)
        {
        poseldg = *(maxa + i);
        vdiag = *(ma + poseldg);
        *vetx = vdiag * (*vetx);
        vetx++;
        }
    }

/*
** Funcao que realiza a etapa de 'back substitution' do sistema
** 'ma' * 'vetaux' = 'vaux'
** acumulando a resposta no vetor 'vetaux'
*/
static void bcksbstsk(double huge *vetx, double huge *ma, long int huge *maxa,
                      double huge *vetb, long int neq, double omega)
// double huge *vetx,            pointer para vetor resposta 'vetaux' 
//        huge *ma,              pointer para vetor que contem a matriz do sistema 
//        huge *vetb;            pointer para vetor 'vaux' fornecido por prodauxsk 
// long int huge *maxa;          pointer para vetor que contem a posicao dos
//                               elementos da diagonal da matriz do sistema 
// long int neq;                 numero de equacoes 
// double omega;                 parametro omega 
    {
    double huge *plix,       /* pointer auxiliar para vetor resposta */
           huge *aij,        /* pointer auxiliar para matriz do sistema */
           huge *pdiagj;     /* pointer auxiliar para elementos da diagonal
                                da matriz do sistema */
    double paux,             /* acumula produtos temporarios */
           akk;              /* recebe o valor do elemento da diagonal principal */
    long int neqm1,          /* recebe o valor 'neq menos 1' */
             poseldg,        /* posicao do elemento da diagonal principal */
             diagj,          /* recebe o valor do elemento da diagonal principal */
             hcolj,          /* valor da altura da coluna */
             incptr;
    register long int j,
                      i;     /* contadores */

    neqm1 = neq - 1;
    for (i = neqm1; i >= 0; i--)
        {
        poseldg = *(maxa + i);
        akk = *(ma + poseldg);
        plix = (vetx + i) + 1;
        paux = 0.0;
        for (j = i + 1, incptr = 1; j < neq; j++, incptr++)
            {
            diagj = *(maxa + j);
            pdiagj = ma + diagj;
            hcolj = *(maxa + j + 1) - diagj;
            if (j - hcolj < i)
                {
                aij = pdiagj + incptr;
                paux += (*aij) * (*plix);
                plix++;
                }
            else plix++;
            }
        *(vetx + i) = (*(vetb + i) - paux) * (omega /akk);
        }
    }

/*
** Metodo do Gradiente Conjugado com Precondicionamento
** Matriz do sistema simetrica do tipo sky-line (colunas ascendentes)
** O usuario devera escolher uma dentre as tres matrizes de precondicionamento
** de acordo com o parametro de entrada codpr
** se codpr = 0 a matriz de precondicionamanto e chamada Gauss-Seidel Simetrizada
**
**                                -1        t
**                C = 1 (2D + L) D  (2D + L)
**                   ---
**                    2
**
** se codpr = 1 a matriz de precondicionamento e chamada SSOR (sem o fator "2 - w")
**
**                                   -1           t
**                C = 1 (1/w D + L) D  (1/w D + L)
**                   ---
**                    w
**
** se codpr = 2 a matriz de precondicionamento e chamada SSOR
**
**                                          -1           t
**                C =    1    ( 1/w D + L) D  (1/w D + L)
**                    -------
**                    w(2 - w)
**
** A rotina devolve:
** 0 -> se o calculo nao convergir ou
**      se o numero maximo de iteracoes for superado;
** 1 -> se o calculo convergir;
** 2 -> se o valor de omega estiver fora da faixa permitida.
**      omega deve estar entre 0 < w < 2
*/

int grcjprsk(double huge *vetx, double huge *ma, long huge *maxa,
             double huge *veth, long neq, long itmax, double eps,
             double omega, int codpr, int codcvg)
    {
    double huge *vetaux,
           huge *vaux,     /* vetores auxiliares */
           huge *pvetaux,  /* pointer para vetor auxiliar 'vetaux' */
           huge *vetres,   /* vetor residuo (ou gradiente) */
           huge *pvetres,  /* pointer para 'vetres' */
           huge *vetp,     /* vetor direcao conjugada */
           huge *pvetp,    /* pointer para 'vetp' */
           huge *pveth,    /* pointer auxiliar para vetor dos termos independentes */
           huge *pvetx;    /* pointer auxiliar para vetor resposta */
    double a, b, d,
           delta0,
           delta1,         /* variaveis auxiliares */
           c,
           ctecvg,         /* constante de convergencia */
           norinfres,      /* recebe a norma infinita do vetor residuo */
           noreures,       /* recebe a norma euclideana do vetor residuo */
           norinfh,        /* recebe a norma infinita do vetor dos termos independentes */
           noreuh;         /* recebe a norma euclideana do vetor dos termos independentes */
    register long int j,
                      k;   /* variaveis utlizadas como contadores */
    int converge;          /* variavel que retorna o codigo para convergencia
                              0 - se nao convergir
                              1 - se convergir
                              2 - se omega nao for valido  */

    k = 1;
    converge = 0;
    a = b = d = ctecvg = 0.0;

    /*
    ** testa se ha erro no codigo de tipo de convergencia
    ** se ha assume codcvg = 1
    */

    if ((codcvg != 1) && (codcvg != 2))
       codcvg = 1;

    /*
    ** teste que verifica o valor de omega (0 < w < 2)
    */

    if ((omega < 0.) || (omega >= 2.))
        return (converge = 2);

    /*
    ** calculo da constante cuja inversa multiplica a matriz
    ** de precondicionamento
    */

    switch (codpr)
        {
        /* Matriz de Precondicionamento Gauss-Seidel Simetrizada */
        case 0: if (omega != .5)
                    omega = .5;
                c = 2.;
                break;

        /* Matriz de Precondicionamento SSOR eliminando "2-w" */
        case 1: c = omega;
                break;

        /* Matriz de Precondicionamento SSOR */
        case 2: c = (2. - omega) * omega;
                break;

        /* se codpr estiver errado assume Matriz SSOR */
        default: c = (2. - omega) * omega;
                 break;
        }

    /* aloca espaco de memoria para vetores */

    vetaux = (double huge *) mMalloc(sizeof(double) * neq);
    vaux   = (double huge *) mMalloc(sizeof(double) * neq);
    vetres = (double huge *) mMalloc(sizeof(double) * neq);
    vetp   = (double huge *) mMalloc(sizeof(double) * neq);

    pvetaux = vetaux;
    pvetres = vetres;
    pveth   = veth;
    pvetx   = vetx;

    /*
    ** calculo da constante de convergencia
    */

    switch (codcvg)
        {
        case 1 : noreuh = normaev(veth, neq);
                 ctecvg = noreuh * eps;
                 break;

        case 2 : norinfh = norinfv(veth, neq);
                 ctecvg = norinfh * eps;
                 break;

        default : break;
        }

    /*
    ** inicio do metodo
    */

    prodmskv(vetaux, ma, maxa, vetx, neq);
    for (j = 0; j < neq; j++)
        *pvetres++ = (*pvetaux++) - (*pveth++);
    pvetaux = vetaux;
    pvetres = vetres;
    pveth = veth;
    pvetp = vetp;
    fwdsbstsk(vaux, ma, maxa, vetres, neq, omega);       /* resolve sistema */
    prodauxsk(vaux, ma, maxa, neq);                      /*           -1    */
    bcksbstsk(vetaux, ma, maxa, vaux, neq, omega);       /* vetaux = C  * vetres */
    for (j = 0; j < neq; j++)
         {
         *pvetaux = (*pvetaux) * c;
         pvetaux++;
         }
    pvetaux = vetaux;
    for (j = 0; j < neq; j++)                             /*  vetp = - vetaux */
        {
        *pvetp = -(*pvetaux);
        pvetp++;
        pvetaux++;
        }
    pvetp = vetp;
    pvetaux = vetaux;
    delta0 = scalv(vetres, vetaux, neq);

    /* teste de convergencia */

    switch (codcvg)
        {
        case 1: noreures = normaev(vetres, neq);
                if (noreures <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        case 2: norinfres = norinfv(vetres, neq);
                if (norinfres <= ctecvg)
                    {
                    mFree(vetaux);
                    mFree(vaux);
                    mFree(vetres);
                    mFree(vetp);
                    return (converge = 1);
                    }
                break;
        default: break;
        }
    while (k <= itmax)
        {
        prodmskv(vetaux, ma, maxa, vetp, neq);
        d = scalv(vetp, vetaux, neq);
        a = delta0/d;
        for (j = 0; j < neq; j++)                         /* calculo do vetor */
            {                                             /* resposta         */
            *pvetx = *pvetx + a * (*pvetp);
            pvetp++;
            pvetx++;
            }
        pvetp = vetp;
        pvetx = vetx;
        for (j = 0; j < neq; j++)                         /* calculo do vetor */
            {                                             /* residuo          */
            *pvetres = *pvetres + a * (*pvetaux);
            pvetaux++;
            pvetres++;
            }
        pvetres = vetres;
        pvetaux = vetaux;
        fwdsbstsk(vaux, ma, maxa, vetres, neq, omega);
        prodauxsk(vaux, ma, maxa, neq);
        bcksbstsk(vetaux, ma, maxa, vaux, neq, omega);
        for (j = 0; j < neq; j++)
            {
            *pvetaux = (*pvetaux) * c;
            pvetaux++;
            }
        pvetaux = vetaux;
        delta1 = scalv(vetres, vetaux, neq);
        switch (codcvg)
            {
            case 1: noreures = normaev(vetres, neq);
                    if (noreures <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            case 2: norinfres = norinfv(vetres, neq);
                    if (norinfres <= ctecvg)
                        {
                        mFree(vetaux);
                        mFree(vaux);
                        mFree(vetres);
                        mFree(vetp);
                        return (converge = 1);
                        }
                    break;
            default: break;
            }
        b = delta1/delta0;
        delta0 = delta1;
        for (j = 0; j < neq; j++)                         /* calculo do vetor  */
            {                                             /* direcao conjugada */
            *pvetp =  b * (*pvetp) - *pvetaux;
            pvetaux++;
            pvetp++;
            }
        pvetaux = vetaux;
        pvetp = vetp;
        k++;
        }
    mFree(vetaux);                                     /* deleta vetores alocados */
    mFree(vaux);
    mFree(vetres);
    mFree(vetp);
    return (converge);
    }


