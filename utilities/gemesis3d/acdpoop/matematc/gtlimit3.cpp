#include "matematc.h"

acLimit3 GetLimit3(acPoint3 huge *vet, long n)
    {
    acLimit3 limit;
    double xmin, xmax, ymin, ymax, zmin, zmax;
    long i;

    if (n <= 1)
        {
        xmin = xmax = vet[0].x;
        ymin = ymax = vet[0].y;
        zmin = zmax = vet[0].z;
        }

    else
        {
        if (vet[0].x > vet[1].x)
            {
            xmax = vet[0].x;
            xmin = vet[1].x;
            }
        else
            {
            xmax = vet[1].x;
            xmin = vet[0].x;
            }

        if (vet[0].y > vet[1].y)
            {
            ymax = vet[0].y;
            ymin = vet[1].y;
            }
        else
            {
            ymax = vet[1].y;
            ymin = vet[0].y;
            }

        if (vet[0].z > vet[1].z)
            {
            zmax = vet[0].z;
            zmin = vet[1].z;
            }
        else
            {
            zmax = vet[1].z;
            zmin = vet[0].z;
            }


        for (i = 2; (i + 2) <= n; i += 2)
            {
            if (vet[i].x > vet[i + 1].x)
                {
                if (vet[i].x     > xmax) xmax = vet[i].x;
                if (vet[i + 1].x < xmin) xmin = vet[i + 1].x;
                }
            else
                {
                if (vet[i + 1].x > xmax) xmax = vet[i + 1].x;
                if (vet[i].x     < xmin) xmin = vet[i].x;
                }

            if (vet[i].y > vet[i + 1].y)
                {
                if (vet[i].y     > ymax) ymax = vet[i].y;
                if (vet[i + 1].y < ymin) ymin = vet[i + 1].y;
                }
            else
                {
                if (vet[i + 1].y > ymax) ymax = vet[i + 1].y;
                if (vet[i].y     < ymin) ymin = vet[i].y;
                }

            if (vet[i].z > vet[i + 1].z)
                {
                if (vet[i].z     > zmax) zmax = vet[i].z;
                if (vet[i + 1].z < zmin) zmin = vet[i + 1].z;
                }
            else
                {
                if (vet[i + 1].z > zmax) zmax = vet[i + 1].z;
                if (vet[i].z     < zmin) zmin = vet[i].z;
                }

            }

        if (i < n)
            {
            if (xmax < vet[i].x)      xmax = vet[i].x;
            else if (xmin > vet[i].x) xmin = vet[i].x;

            if (ymax < vet[i].y)      ymax = vet[i].y;
            else if (ymin > vet[i].y) ymin = vet[i].y;

            if (zmax < vet[i].z)      zmax = vet[i].z;
            else if (zmin > vet[i].z) zmin = vet[i].z;

            }
        }

    limit.xmin = xmin;
    limit.xmax = xmax;
    limit.ymin = ymin;
    limit.ymax = ymax;
    limit.zmin = zmin;
    limit.zmax = zmax;
    return(limit);
    }
