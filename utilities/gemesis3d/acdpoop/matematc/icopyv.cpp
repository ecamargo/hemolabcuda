/*
 **************************************************************************
 *                                                                        *
 * void icopyv                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Copiar um vetor do tipo inteiro em outro.                         *
 * (02) Forma de utilizacao:                                              *
 *      icopyv(d, s, n);                                                  *
 * (03) Descricao:                                                        *
 *      Copia o vetor 's' no vetor 'd' ambos do tipo int huge e de        *
 *      dimensao 'n'.                                                     *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 *      copiar                                                            *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *d   -  pointer para vetor de destino - int huge               *
 *      -> *s   -  pointer para vetor original - int huge                 *
 *      -> n    -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void icopyv(int huge *d, int huge *s, long n);                    *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void icopyv(int huge *d, int huge *s, register long n)
    {
    while (n--)
        *d++ = *s++;
    }

