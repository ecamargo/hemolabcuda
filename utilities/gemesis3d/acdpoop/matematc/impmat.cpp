/* rotina para imprimir um matriz */

#include "matematc.h"

void printmat(double huge *matriz, char *titulo, int nlin, int ncol, FILE *fi)
    {
    register int i, j, n;

    fprintf(fi, "\n%s\n", titulo);

    for (j = 0; j < nlin; j++)
        {
	    fprintf(fi, "\n\nRow %4d:\n", j);
	    n = 0;
	    fprintf(fi, "\n%6d: ", n);
	    for (i = 0; i < ncol; i++)
	        {
	        if (n == 5)
		        {
		        fprintf(fi, "\n%6d: ", i);
		        n = 0;
		        }
	        fprintf(fi, " %15.7E", *matriz++);
	        n++;
	        }
	    }
    }
