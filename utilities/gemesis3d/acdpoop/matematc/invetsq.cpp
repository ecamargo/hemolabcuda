/*
 **************************************************************************
 *                                                                        *
 * void invetsq                                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Inicializar um vetor  com numeros sequenciais.                    *
 * (02) Forma de utilizacao:                                              *
 *      invetsq(v, inic, tam);                                              *
 * (03) Descricao:                                                        *
 *      Inicializa um vetor do tipo double  huge de dimensao 'tam' com    *
 *      numeros sequenciais.
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      iniciacao                                                         *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *v   -  pointer para vetor a ser inicializado - long huge      *
 *      -> inic -  numero com o qual vai ser inicializado - long          *
 *      -> tam  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void invetsq(double huge *v, double huge inic, long tam);           *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void invetsq(long huge *v, long inic, long tam)
    {
    while (tam--)
        *v++ = inic++;
    }
