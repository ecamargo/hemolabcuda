/*
 **************************************************************************
 *                                                                        *
 * long ipos                                                              *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Fornecer as posicao de um elemento de uma matriz em um vetor.     *
 * (02) Forma de utilizacao:                                              *
 *      posicao = ipos( i, j, nlin);                                      *
 * (03) Descricao:                                                        *
 *      Fornece a posicao do elemento (i,j) de uma matriz simetrica de    *
 *      'nlin' linhas, em um vetor que contem a parte supradiagonal       *
 *      desta matriz armazenada por linhas.                               *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      matrizes                                                          *
 *      supradiagonal                                                     *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 13 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      -> i    -  variavel que contem o numero da linha - long           *
 *      -> j    -  variavel que contem o numero da coluna - long          *
 *      -> nlin  -  numero de linhas ou colunas - long                    *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      long ipos(long i, long j, long nlin);                             *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

long ipos(long i, long j, long nlin)
    {
    if (i < j)
        return ((i * (2 * nlin - (i + 1)))/2 + j);
    else
        return ((j * (2 * nlin - (j + 1)))/2 + i);
    }

