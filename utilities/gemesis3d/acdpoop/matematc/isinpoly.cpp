// indica se um ponto pertence ao interior de um poligono
// p - ponto
// n - numero de vertices do poligono
// v - poligono
#include "matematc.h"

BOOL IsInPolygon(acPoint2 &p, int n, acPoint2 huge *v)
    {
    int k, lado;
    double prod;
    acPoint2 *p1, *p2;

    lado = n - 1;

    for (k = 0; k < lado; k++)
        {
        p1 = (acPoint2 *) v++;
        p2 = (acPoint2 *) v;
        prod = (p2->y - p1->y) * (p.x - p1->x) -
               (p2->x - p1->x) * (p.y - p1->y);
        
        if (prod <= 0.0)
            {
            if (k == lado - 1)
                return(TRUE);
            else
                continue;
            }
        else
            break;
        }

    return(FALSE);
    }
