/*
 **************************************************************************
 *                                                                        *
 * void lcopyv                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Copiar um vetor do tipo long huge.                                *
 * (02) Forma de utilizacao:                                              *
 *      lcopyv(d, s, n);                                                  *
 * (03) Descricao:                                                        *
 *      Copiar o vetor 's' do tipo long huge, de dimensao 'n' no vetor    *
 *      vetor 'd' do mesmo tipo e igual dimensao.                         *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      copiar                                                            *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 12 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *d   -  pointer para vetor que copia - long huge               *
 *      -> *s   -  pointer para vetor original - long huge                *
 *      -> n    -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void lcopyv(long huge *d, long huge *s, long n);                  *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void lcopyv(long huge *d, long huge *s, long n)
    {
    while (n--)
        *d++ = *s++;
    }

