/*
 **************************************************************************
 *                                                                        *
 * void levtflt                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Ler um vetor float de tam elementos de um arquivo.                *
 * (02) Forma de utilizacao:                                              *
 *      levtflt(vetor, tam, fp);                                          *
 * (03) Descricao:                                                        *
 *      Ler um vetor float de tam elementos de um arquivo.                *
 * (04) Palavras chave:                                                   *
 *      arquivo                                                           *
 *      leitura                                                           *
 *      vetor                                                             *
 *      float                                                             *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, fevereiro de 1989.                          *
 *      Antonio Carlos Salgado Guimaraes                                  *
 * (07) Argumentos:                                                       *
 *      <- *vetor - vetor a ser lido       - float                        *
 *      -> tam    - tamanho do vetor       - int                          *
 *      -> *fp    - arquivo                - FILE                         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      trc_on, trc_off                                                   *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void levtflt(float *, int, FILE *);                               *
 * (11) Headers necessarios:                                              *
 *      stdio.h, util.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void levtflt(float huge *vet, long tam, FILE *fp)
    {
    register long i;

    for (i = 0; i < tam; i++, vet++)
        fscanf(fp, "%f", vet);
    }
