/*
 **************************************************************************
 *                                                                        *
 * void lvetarq                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Imprimir vetores no arquivo dado por "lw".                        *
 * (02) Forma de utilizacao:                                              *
 *      lvetarq(titulo, tipo, v, (long) nelem, lw);                       *
 * (03) Descricao:                                                        *
 *      Imprime vetores no arquivo dado por "lw".                         *
 *      A descricao dos tipos disponiveis se encontra na funcao           *
 *      testa_tipo.                                                       *
 * (04) Palavras chave:                                                   *
 *      impressao                                                         *
 *      vetor                                                             *
 *      arquivo                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, maio de 1988,                               *
 *      Antonio Carlos Salgado Guimaraes e Raul Feijoo                    *
 * (07) Argumentos:                                                       *
 *      -> *titulo - titulo do vetor a ser impresso - char                *
 *      -> tipo    - tipo dos elementos do vetor - int                    *
 *      -> *v      - vetor a ser impresso - void huge                     *
 *      -> nelem   - numero de elementos do vetor - long                  *
 *      -> *lw     - pointer para o arquivo de saida - FILE               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      trc_on, trc_off, error                                            *
 * (10) Prototype:                                                        *
 *      util.h                                                            *
 *      void lvetarq(char *titulo, int tipo, void huge *v,                *
 *                             long nelem, FILE *lw);                     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, util.h                                                   *
 * (12) Mensagens de erro:                                                *
 *      . Numero 1:                                                       *
 *        Mensagem: Tipo de elemento invalido.                            *
 *        Causa   : Tipo de elemento nao existe.                          *
 *        Correcao: Verificar o tipo do elemento.                         *
 *        Efeito  : O programa e' interrompido.                           *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void lvetarq(char *titulo, int tipo, void huge *v, long nelem, FILE *lw)
    {
    register int contcol;      /* contador de colunas */
    int          nplin;        /* numero de elementos por linha */
    long         contaux = 0;  /* contador auxiliar */

    int    huge *vi;           /* pointers auxiliares */
    long   huge *vl;
    float  huge *vf;
    double huge *vd;

    static char *nome_fc = "lvetarq";

    trc_on(nome_fc);

    fprintf(lw, "%s\n", titulo);      /* imprime o titulo */
    contcol = 1;                      /* inicializa contador de colunas */

    /*
    ** conforme o tipo do elemento a ser impresso, fixa o
    ** numero de elementos a serem impressos em cada linha e
    ** aponta para o inicio do vetor que contem os elementos
    */
    switch (tipo)
        {
    case T_INT:         /* tipo int */
        nplin = 9;
        vi = (int huge *) v;
        while (nelem --)
            {
            fprintf(lw, "%7d ", *(vi + contaux++));
            if (++contcol > nplin)
                {
                contcol = 1;
                fprintf(lw, "\n");
                }
            }
        break;
    case T_LONG:        /* tipo long */
        nplin = 8;
        vl = (long huge *) v;
        while (nelem --)
            {
            fprintf(lw, "%8ld ", *(vl + contaux++));
            if (++contcol > nplin)
                {
                contcol = 1;
                fprintf(lw, "\n");
                }
            }
        break;
    case T_FLOAT:       /* tipo float */
        nplin = 4;
        vf = (float huge *) v;
        while (nelem --)
            {
            fprintf(lw, "%15.7e ", *(vf + contaux++));
            if (++contcol > nplin)
                {
                contcol = 1;
                fprintf(lw, "\n");
                }
            }
        break;
    case T_DOUBLE:      /* tipo double */
        nplin = 4;
        vd = (double huge *) v;
        while (nelem --)
            {
            fprintf(lw, "%15.7le ", *(vd + contaux++));
            if (++contcol > nplin)
                {
                contcol = 1;
                fprintf(lw, "\n");
                }
            }
        break;
    default:
        error(2, 1, "Tipo de elemento invalido.");
        break;
        }

    fprintf(lw, "\n");
    trc_off(nome_fc);
    }
