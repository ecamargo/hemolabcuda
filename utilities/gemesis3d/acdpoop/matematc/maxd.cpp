#include "matematc.h"

double maxd(double huge *vet, long tam)
    {
    register long i;
    double aux;

    aux = *vet++;

    for (i = 1; i < tam; i++, vet++)
        if (*vet > aux)
            aux = *vet;

    return(aux);
    }
