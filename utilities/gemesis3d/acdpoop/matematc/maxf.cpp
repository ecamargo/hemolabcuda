/*
 **************************************************************************
 *                                                                        *
 * float maxf                                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Retornar o maior valor de um vetor do tipo float.                 *
 * (02) Forma de utilizacao:                                              *
 *      fmax = maxf(vetor, tam)                                           *
 * (03) Descricao:                                                        *
 *      Retorna o maior valor de um vetor do tipo float.                  *
 * (04) Palavras chave:                                                   *
 *      maximo                                                            *
 *      float                                                             *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, fevereiro de 1989.                          *
 *      Antonio Carlos Salgado Guimaraes                                  *
 * (07) Argumentos:                                                       *
 *      -> *vetor - vetor a ser pesquisado - float huge                   *
 *      -> tam    - tamanho do vetor       - long                         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      float maxf(float huge *, long);                                   *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

float maxf(float huge *vet, long tam)
    {
    register long i;
    float aux;

    aux = *vet++;

    for (i = 1; i < tam; i++, vet++)
        if (*vet > aux)
            aux = *vet;

    return(aux);
    }
