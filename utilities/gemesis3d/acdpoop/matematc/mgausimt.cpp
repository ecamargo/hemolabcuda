/*
 *****************************************************************************
 *                                                                           *
 * int mgausimt(mas, neq, step)                                              *
 *                                                                           *
 * (01) Objetivo:                                                            *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-     *
 *      cao de Gauss.                                                        *
 * (02) Forma de utilizacao:                                                 *
 *      variavel inteira = mgausimt(mas, neq, step);                         *
 * (03) Descricao:                                                           *
 *      Triangulariza a matriz do sistema segundo o Metodo de Eliminacao     *
 *      de Gauss. A matriz do sistema e simetrica armazenada na forma        *
 *      supradiagonal por linhas. A rotina retorna um codigo informando se   *
 *      obteve sucesso na triangularizacao ou nao, sendo que neste caso      *
 *      houve singularidade da matriz do sistema impedindo a continuacao do  *
 *      Metodo de Gauss.                                                     *
 * (04) Palavras chave:                                                      *
 *      equacoes                                                             *
 *      simetrica                                                            *
 *      direto                                                               *
 *      triangularizacao                                                     *
 * (05) Referencia:                                                          *
 *      ---                                                                  *
 * (06) Data e autor:                                                        *
 *      Rio de Janeiro, LNCC, marco de 1990,                                 *
 *      Roberto Eduardo Garcia.                                              *
 * (07) Argumentos:                                                          *
 *      -> *mas  -  pointer para matriz do sistema          - double huge    *
 *      -> neq   -  numero de equacoes                      - long           *
 *      -> *step -  etapa 'k' dos processos de 'forward elimination' - long  *
 * (08) Variaveis externas utilizadas:                                       *
 *      ---                                                                  *
 * (09) Chamadas:                                                            *
 *      ---                                                                  *
 * (10) Prototype:                                                           *
 *      solvequ.h                                                            *
 *      int mgausimt(double huge *mas,long int neq, long int *step);         *
 * (11) Headers necessarios:                                                 *
 *      solvequ.h                                                            *
 *      matematc.h                                                           *
 *      math.h                                                               *
 *      util.h                                                               *
 *      dbmanagm.h                                                           *
 * (12) Mensagens de erro:                                                   *
 *      ---                                                                  *
 * (13) Depuracao:                                                           *
 *      ---                                                                  *
 * (14) Portabilidade:                                                       *
 *      C standard.                                                          *
 * (15) Acesso:                                                              *
 *      Livre ao usuario.                                                    *
 *                                                                           *
 *****************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Metodo de Eliminacao de Gauss
** Esta rotina realiza a etapa de 'forward elimination' da matriz do sistema
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na triangularizacao da matriz do sistema.
** 0: a matriz do sistema e' singular, impedindo a execucao
**             do Metodo de Gauss.
*/

int mgausimt(double huge *mas, long neq, long *step)
    {
    double huge *akk,      /* pointer que varre a diagonal principal de 'mas' */
           huge *temp;     /* pointer auxiliar utilizado nas etapas de 'partial
                              pivoting' e 'forward elimination' */
    double mik;            /* Aik / Akk */
    register long int j,
                      i,   /* variaveis utlizadas como contadores */
                      k;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int neqm1,        /* neq - 1 */
             aux,
             cont,         /* contadores */
             pulomas,      /* numero de posicoes que o pointer mas deve avancar */
             pulotemp;     /* numero de posicoes que o pointer temp deve avancar */

    /*
    ** inicializa pointers e constantes
    */

    akk   = mas;
    neqm1 = neq - 1;
    cont = neqm1;
    pulomas = neq;

    /*
    ** etapa de 'forward elimination'
    */

    for (k = 1; k < neq; k++)
        {
        mas   = akk + pulomas;
        aux   = cont;
        if (fabs(*akk) < 1E-30)                /* se o valor do pivot for menor               */
            {                                  /* que delta (1E-30), a rotina e' interrompida */
            *step = k;                         /* retornando 0 e fica disponivel ao usuario   */
            return (0);                        /* e etapa onde o fato ocorreu                 */
            }
        for (i = k, pulotemp = 1; i < neq; i++, pulotemp++)
            {
            temp = akk + pulotemp;
            mik = *temp / *akk;
            j = aux;
            while (j--)
                {
                *mas = *mas - mik * (*temp++);
                mas++;
                }
            aux--;
            }
        akk += pulomas;
        pulomas--;
        cont--;
        }
    return (1);
    }
