/*
 **************************************************************************
 *                                                                        *
 * int mgausst(ma, neq, step)                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-  *
 *      cao de Gauss.                                                     *
 * (02) Forma de utilizacao:                                              *
 *      variavel inteira = mgausst(ma, neq, step);                        *
 * (03) Descricao:                                                        *
 *      Triangulariza a matriz do sistema pelo Metodo de Eliminacao de    *
 *      Gauss utilizando tecnica de pivoteamento parcial. A matriz do     *
 *      sistema e cheia armazenada por linhas. A rotina retorna um codigo *
 *      informando se obteve sucesso nesta etapa ou nao.                  *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      cheia                                                             *
 *      direto                                                            *
 *      triangularizar                                                    *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, abril de 1990,                              *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ma   -  pointer para matriz do sistema          - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 *      -> *step -  etapa 'k' dos processos de 'partial pivoting' e       *
 *                  'forward elimination'                   - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      int mgausst(double huge *ma, long int neq, long int *step);       *
 * (11) Headers necessarios:                                              *
 *      stdio.h                                                           *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 *      util.h                                                            *
 *      dbmanagm.h                                                        *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include <math.h>
#include "matematc.h"

static long int huge *vinfopiv;

/*
** Metodo de Eliminacao de Gauss
** Rotina responsavel pela triangularizacao da matriz do sistema
** utilizando tecnica de pivoteamento parcial.
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na triangularizacao da matriz do sistema
** 0: a matriz do sistema e' singular, impedindo a execucao
**             do Metodo de Gauss.
*/

int mgausst(double huge *ma, long neq, long *step)
    {
    double huge *aij,      /* pointer que para os elementos de ma a serem modificados */
           huge *aik,      /* pointer para elementos Aik da etapa de eliminacao */
           huge *akk,      /* pointer que varre a diagonal principal de 'ma' */
           huge *ptemp,    /* pointer auxiliar utilizado nas etapas de 'partial
                              pivoting' e 'forward elimination' */
           huge *ppivot,   /* pointer para elemento pivot */
           huge *auxptr;   /* pointer para vetor auxiliar utilizado para troca
                              de linhas da matriz 'ma' na etapa de pivoteamanto */
    double mik;            /* Aik / Akk */
    register long int j,
                      i,   /* variaveis utlizadas como contadores */
                      k;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int neqp1,        /* neq + 1 */
             neqm1,
             row;          /* linha na qual foi achado o pivot na etapa 'k' */

    /*
    ** inicializa pointers e constantes
    */

    akk   = ma;
    neqp1 = neq + 1;
    neqm1 = neq - 1;
    row = 0;

    /*
    ** cria vetores auxiliares 'auxptr' e 'vinfopiv'
    */

    auxptr   = (double huge *) mMalloc(sizeof(double) * neq);
    vinfopiv = (long huge *) mMalloc(sizeof(long) * neqm1);

    /*
    ** Pesquisa do pivot
    */

    for (k = 1; k < neq; k++)
        {
        ppivot = ptemp = akk;
        for (i = k; i < neq; i++)              /* procura do pivot */
            {
            ptemp += neq;
            if (fabs(*ptemp) > fabs(*ppivot))
                {
                ppivot = ptemp;                /* achou o pivot e */
                row    = i;                    /* se encontra na linha 'row' */
                }
            }
        if (ppivot != akk)
            {
            ptemp   = akk - (k - 1);
            ppivot = ppivot - (k - 1);
            dcopyv(auxptr, ptemp, neq);         /* realiza a troca */
            dcopyv(ptemp, ppivot, neq);         /* das linhas correspondentes */
            dcopyv(ppivot, auxptr, neq);
            *(vinfopiv + k - 1) = row;      /* guarda no vetor 'vinfopiv'a linha
                                               na qual ocorreu o pivoteamento */
            }
        else
            *(vinfopiv + k - 1) = 0;       /* informa que nao houve pivoteamento */

        if (fabs(*akk) < 1E-30)            /* se o valor do pivot for menor               */
            {                              /* que delta (1E-30), a rotina e' interrompida */
            *step = k;                     /* retornando 0 e fica disponivel ao usuario   */
            mFree(auxptr);                 /* a etapa (linha) do pivot                    */
            mFree(vinfopiv);
            return (0);
            }

        /* Etapa de 'forward elimination' */

        aik = akk + neq;
        for (i = k; i < neq; i++)
            {
            mik = *aik / *akk;
            aij = aik + 1;
            ptemp = akk + 1;
            for (j = k; j < neq; j++)             /* os elementos Aik nao sao modificados             */
                {                                 /* sao guardados para utilizacao da rotina seguinte */
                *aij = *aij - mik * (*ptemp++);
                aij++;
                }
            aik += neq;
            }
        akk += neqp1;
        }
    mFree(auxptr);
    return(1);
    }

/*
 **************************************************************************
 *                                                                        *
 * void solgauss(vetx, ma, vetb, neq)                                     *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-  *
 *      cao de Gauss.                                                     *
 * (02) Forma de utilizacao:                                              *
 *      solgauss(vetx, ma, vetb, neq);                                    *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares com multiplos vetores de  *
 *      termos independentes pelo Metodo de Eliminacao                    *
 *      de Gauss utilizando tecnica de pivoteamento parcial e realizando  *
 *      modificacao do termo independente de acordo com o resultado da    *
 *      rotina mgausst e executa a etapa de 'back substitution'.          *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      cheia                                                             *
 *      direto                                                            *
 *      solvedor                                                          *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, abril de 1990,                              *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <- *vetx -  pointer para vetor resposta             - double huge *
 *      -> *ma   -  pointer para matriz do sistema modificada double huge *
 *      -> *vetb -  pointer para vetor termos independentes - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      void solgauss(double huge *vetx, double huge *ma, double huge     *
 *                    *vetb, long int neq);                               *
 * (11) Headers necessarios:                                              *
 *      stdio.h                                                           *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

/*
** Metodo de Eliminacao de Gauss
** Rotina responsavel pela modificacao dos termos independentes
** e pela etapa de retro substituicao
*/

void solgauss(double huge *vetx, double huge *ma, double huge *vetb, long neq)
    {
    double huge *pcfin,
           huge *plinb,     /* pointers para os termos independentes */
           huge *plix,      /* pointer para elementos do vetor resposta */
           huge *akk,       /* pointer para elementos da diagonal principal
                               da matriz do sistema modificada */
           huge *aik;       /* pointer para elementos Aik da matriz do sistema */
    double paux,            /* variavel auxiliar que armazena os produtos temporarios
                               na etapa de 'back substitution ' */
           auxb,            /* variavel auxiliar utilizada no pivoteamento dos termos
                               independentes */
           mik;             /* razao dos elementos Aik / Akk */
    long int neqm1,         /* neq - 1 */
             neqp1,         /* neq + 1 */
             nelem,         /* numero de elementos da matriz do sistema */
             row;           /* linha na qual houve pivoteamento */
    register long int j,
                      i,    /* variaveis utilizadas como contadores */
                      k;    /* etapa dos processo de 'forward elimination' */

    /*
    ** inicializa pointer e constantes
    */

    nelem = neq * neq;
    neqm1 = neq - 1;
    neqp1 = neq + 1;
    akk = ma;

    /*
    ** Etapa de pivoteamento
    ** dos termos independentes
    */

    for (k = 1; k < neq; k++)
        {
        pcfin = vetb + (k - 1);
        plinb = vetb;
        row = *(vinfopiv + k - 1);   /* pesquisa se houve ou nao pivoteamento */
        if (row != 0)
            {
            plinb += row;            /* se houve efetua troca de linhas */
            auxb = *pcfin;           /* correspondentes */
            *pcfin = *plinb;
            *plinb = auxb;
            }
        plinb = vetb + k;             /* Etapa de 'forward elimination' */
        aik = akk + neq;
        for (i = k; i < neq; i++)
            {
            mik = *aik / *akk;
            *plinb = *plinb - (mik * (*pcfin));
            plinb++;
            aik += neq;
            }
        akk += neqp1;
        }

    /*
    ** Etapa de 'back substitution
    */

    akk = ma + (nelem - 1);
    for (i = neqm1; i >= 0; i--)
        {
        plix = vetx + i + 1;
        ma = akk + 1;
        paux = 0.0;
        for (j = i + 1; j < neq; j++)
            paux += (*ma++) * (*plix++);
        *(vetx + i) = (*(vetb + i) - paux) / *akk;
        akk -= neqp1;
        }
    mFree(vinfopiv);
    }






