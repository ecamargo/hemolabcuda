/*
 **************************************************************************
 *                                                                        *
 * int mgsstsk(ma, maxa, neq, step)                                       *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-  *
 *      cao de Gauss.                                                     *
 * (02) Forma de utilizacao:                                              *
 *      variavel inteira = mgsstsk(ma, maxa, neq, step);                  *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares pelo Metodo de Eliminacao *
 *      de Gauss. A matriz do sistema e' simetrica escalonada por colunas *
 *      ascendentes. O armazenamento desta matriz se da por meio dos veto-*
 *      res 'ma' e 'maxa'. A rotina executa a etapa de eliminacao direta  *
 *      da matriz do sistema e retorna um codigo informando se obteve     *
 *      sucesso na etapa ou nao.                                          *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      simetrica                                                         *
 *      skyline                                                           *
 *      direto                                                            *
 *      triangularizar                                                    *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, maio de 1990,                               *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ma   -  pointer para o vetor que contem os elementos          *
 *                  da matriz do sistema                    - double huge *
 *      -> *maxa -  pointer para vetor que contem as posicoes dos         *
 *                  elementos da diagonal principal         - long huge   *
 *      -> neq   -  numero de equacoes                      - long        *
 *      <- *step -  etapa 'k' do processo de 'forward elimination' - long *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      int mgsstsk(double huge *ma, long huge *maxa,long int neq,        *
 *                  long int *step);                                      *
 * (11) Headers necessarios:                                              *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include <math.h>
#include "matematc.h"

/*
** Metodo de Eliminacao de Gauss
** A matriz do sistema  e' do tipo 'skyline'
** A rotina devolve ao usuario um dos dois codigos a seguir :
** 1: a rotina obteve sucesso na triangularizacao da matriz do sistema
** 0: a matriz do sistema e' singular, impedindo a execucao
**    do Metodo de Gauss.
*/

int mgsstsk(double huge *ma, long huge *maxa, long neq, long *step)
    {
    double huge *aij,       /* pointer para elementos da matriz do sistema a
                               serem modificados */
           huge *pdiagi,
           huge *pdiagj;    /* pointers para elementos da diagonal principal */
    double mik,             /* variavel que recebe a razao Aik / Akk */
           akk,             /* recebe o conteudo do elemento da diagonal principal */
           akj,             /* recebe o conteudo do elemento Akj na etapa de eliminacao */
           aik;             /* recebe o conteudo do elemento Aik na etapa de eliminacao */
    register long int k,    /* etapa do processo de 'forward elimination' */
                      i,   /* variaveis utlizadas como contadores */
                      j;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int neqm1,        /* neq - 1 */
             poseldg,      /* posicao do elemento da diagonal principal */
             hcoli,
             hcolj,        /* altura da coluna */
             diagi,
             diagj;        /* guardam a posicao do elemento da diagonal principal */

    /*
    ** inicializa constante
    */

    neqm1 = neq - 1;

    /*
    ** Etapa de 'forward elimination'
    */

    for (k = 0; k < neqm1; k++)
        {
        poseldg = *(maxa + k);
        akk     = *(ma + poseldg);
        if (fabs(akk) < 1E-30)
            {
            *step = poseldg;
            return (0);
            }
        for (i = k + 1; i < neq; i++)
            {
            hcoli = *(maxa + i + 1) - *(maxa + i);
            if (i - hcoli < k)
                {
                diagi  = *(maxa + i);
                pdiagi = ma + diagi;
                aik = *(pdiagi + (i - k));
                mik = aik/akk;
                for (j = i; j < neq; j++)
                    {
                    diagj = *(maxa + j);
                    pdiagj = ma + diagj;
                    hcolj = *(maxa + j + 1) - diagj;
                    if (j - hcolj < k)
                        {
                        aij = pdiagj + (j - i);
                        akj = *(pdiagj + (j - k));
                        *aij = *aij - (mik * akj);
                        }
                    }
                }
            }
        }
    return(1);
    }




