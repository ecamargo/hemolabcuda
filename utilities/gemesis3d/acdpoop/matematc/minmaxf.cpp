/*
 **************************************************************************
 *                                                                        *
 * void minmaxf                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Retornar o menor e o maior valor de um vetor do tipo float.       *
 * (02) Forma de utilizacao:                                              *
 *      minmaxf(vetor, tam, &min, &max);                                  *
 * (03) Descricao:                                                        *
 *      Retorna o menor e o maior valor de um vetor do tipo float.        *
 * (04) Palavras chave:                                                   *
 *      maximo                                                            *
 *      minimo                                                            *
 *      float                                                             *
 * (05) Referencia:                                                       *
 *      Dr. Dobbs's Journal, October 1990, pg 58                          *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, junho de 1991.                              *
 *      Antonio Carlos Salgado Guimaraes                                  *
 * (07) Argumentos:                                                       *
 *      -> *vetor - vetor a ser pesquisado - float huge                   *
 *      -> tam    - tamanho do vetor       - long                         *
 *      <- *min   - menor valor            - float                        *
 *      <- *max   - maior valor            - float                        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void minmaxf(float huge *, long, float *, float *);               *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void minmaxf(float huge *vet, long n, float *min, float *max)
    {
    register long i;
    float mini, maxi;

    if (n <= 1)
        mini = maxi = vet[0];
    else
        {
        if (vet[0] > vet[1])
            {
            maxi = vet[0];
            mini = vet[1];
            }
        else
            {
            maxi = vet[1];
            mini = vet[0];
            }

        for (i = 2; (i + 2) <= n; i += 2)
            if (vet[i] > vet[i + 1])
                {
                if (vet[i]     > maxi)  maxi = vet[i];
                if (vet[i + 1] < mini)  mini = vet[i + 1];
                }
            else
                {
                if (vet[i + 1] > maxi)  maxi = vet[i + 1];
                if (vet[i]     < mini)  mini = vet[i];
                }

        if (i < n)
            if (maxi < vet[i]) maxi = vet[i];
            else if (mini > vet[i]) mini = vet[i];
        }
    *min = mini;
    *max = maxi;
    }
