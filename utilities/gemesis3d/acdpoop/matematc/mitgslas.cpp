/*
 **************************************************************************
 *                                                                        *
 * int mitgslas(vetx, mas, vetb, neq, itmax, eps, beta, codconvg)         *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo    *
 *      de Gauss - Seidel.                                                *
 * (02) Forma de utilizacao:                                              *
 *      variavel inteira = mitgslas(vetx, mas, vetb, neq, itmax, eps,     *
 *                                  beta, codconvg);                      *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo de  *
 *      Gauss - Seidel utilizando tecnica de sobre relaxacao. A matriz do *
 *      sistema e simetrica armazenada na forma supradiagonal por linhas. *
 *      A rotina retorna um codigo informando se o resultado convergiu ou *
 *      nao.                                                              *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      simetrica                                                         *
 *      iterativo                                                         *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, fevereiro de 1990,                          *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <> *vetx -  pointer para vetor resposta             - double huge *
 *      -> *mas  -  pointer para matriz do sistema          - double huge *
 *      -> *vetb -  pointer para vetor termos independentes - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 *      -> itmax -  numero maximo de iteracoes              - long        *
 *      -> eps   -  constante para teste de convergencia    - double      *
 *      -> beta  -  parametro de sobre relaxacao (0 - 2)    - double      *
 *      -> codconvg - codigo de tipo de convergencia        - int         *
 *                            1 - norma euclideana                        *
 *                            2 - norma infinita                          *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      int mitgslas(double huge *vetx, double huge *mas, double huge     *
 *                   *vetb,long int neq, long int itmax, double eps,      *
 *                   double beta, int codconvg);                          *
 * (11) Headers necessarios:                                              *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include <math.h>
#include "matematc.h"

/*
** Metodo Iterativo Gauss - Seidel utilizando sobre relaxacao
** A Matriz do Sistema e' simetrica supradiagonal por linhas
*/

int mitgslas(double huge *vetx, double huge *mas, double huge *vetb,
             long neq, long itmax, double eps, double beta, int codconvg)
    {
    double huge *pcjmas,  /* pointer para coluna 'j' de 'mas' */
           huge *plix,    /* pointer para linha 'i' de 'vetx' */
           huge *pcfin,   /* pointer para 'vetb' */
           huge *xi;      /* pointer para vetor resposta 'vetx' */
    double dif,           /* diferenca entre o valor 'x' da iteracao atual
                             e da anterior */
           difaux,        /* valor da norma infinita, ou seja, e' o maior
                             valor de 'dif' */
           dif2,          /* valor de 'dif' elevado ao quadrado */
           xi2,           /* valor de 'x' calculado elevado ao quadrado */
           temp,          /* variavel temporaria que armazena o valor de 'x' */
           norvtx,        /* variavel que recebe a norma euclideana do vetor 'x' */
           nordif,        /* variavel que recebe a norma euclideana do vetor
                             que contem as diferencas */
           betaux,        /* constante utilizada no calculo de 'x' */
           betaii;        /* recebe o valor de beta dividido pelo elemento
                             correspondente da diagonal da matriz do sistema */
    register long int aux,
                        j,
                        i;  /* variaveis utlizadas como contadores */
    long int iter,          /* numero de iteracoes */
             neqm1;         /* constante igual a neq menos 1 */
    int  converge;        /* variavel que retorna o codigo para convergencia
                             0 - se nao convergir
                             1 - se convergir     */

    /* inicializa a constant betaux, neqm1, */
    /* a contagem das iteracoes */
    /* e o codigo para convergencia */

    betaux = (1. - beta);
    neqm1 = neq - 1;
    iter = 0;
    converge = 0;

    /* testa se ha erro no codigo de tipo de convergencia */
    /* se ha assume codconvg = 1 */

    if ((codconvg != 1) && (codconvg != 2))
       codconvg = 1;

    while ((!converge) && (iter < itmax))
        {
        xi2 = difaux = dif2 = 0.0;
        xi = vetx;
        pcfin = vetb;
        for (i = 0; i < neq; i++)
            {
            temp = 0.0;
            dif = 0.0;
            aux = neqm1;
            pcjmas = mas + i;
            plix = vetx;
            for (j = 0; j < i; j++)              /* elementos subdiagonais */
                {
                temp += ((*pcjmas) * (*plix++));
                pcjmas += aux;
                aux--;
                }
            betaii = beta/(*pcjmas++);
            plix++;
            for (j = i + 1; j < neq; j++)        /* elementos supradiagonais */
                temp += ((*pcjmas++) * (*plix++));
            temp = (((*pcfin++ - temp) * betaii) + ((*xi) * betaux));
            dif   = fabs(temp - (*xi));
            dif2 += dif * dif;
            if (dif > difaux) difaux = dif;
            xi2 += temp * temp;
            *xi++ = temp;
            }
        iter++;
        norvtx = sqrt(xi2);
        nordif = sqrt(dif2);

        switch (codconvg)
            {
            case 1: if ((nordif/norvtx) <= eps)
                       converge = 1;
                    break;

            case 2: if (difaux <= eps)
                       converge = 1;
                    break;
            default:
                break;
            }
        }
    return(converge);
    }


