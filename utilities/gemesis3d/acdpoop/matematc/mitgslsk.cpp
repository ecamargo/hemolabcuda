/*
 **************************************************************************
 *                                                                        *
 * int mitgslsk(vetx, sk, maxa, vetb, neq, itmax, eps, beta, codconvg)    *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo Iterativo    *
 *      de Gauss - Seidel.                                                *
 * (02) Forma de utilizacao:                                              *
 *      variavel inteira = mitgslsk(vetx, sk, maxa, vetb, neq, itmax, eps,*
 *                                  beta, codconvg);                      *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares pelo Metodo Iterativo de  *
 *      Gauss - Seidel utilizando tecnica de sobre relaxacao. A matriz do *
 *      sistema e simetrica escalonada por colunas ascendentes. O armaze- *
 *      namento se da atraves dos vetores 'sk' e 'maxa'.                  *
 *      A rotina retorna um codigo informando se o resultado convergiu ou *
 *      nao.                                                              *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      simetrica                                                         *
 *      skyline                                                           *
 *      iterativo                                                         *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, marco de 1990,                              *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <> *vetx -  pointer para vetor resposta             - double huge *
 *      -> *sk   -  pointer para vetor que contem os elementos da matriz  *
 *                  do sistema                              - double huge *
 *      -> *maxa -  pointer para vetor posicao dos elementos da diagonal  *
 *                  principal                               - int huge    *
 *      -> *vetb -  pointer para vetor termos independentes - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 *      -> itmax -  numero maximo de iteracoes              - long        *
 *      -> eps   -  constante para teste de convergencia    - double      *
 *      -> beta  -  parametro de sobre relaxacao (0 - 2)    - double      *
 *      -> codconvg - codigo de tipo de convergencia        - int         *
 *                            1 - norma euclideana                        *
 *                            2 - norma infinita                          *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      int mitgslsk(double huge *vetx, double huge *sk, int huge *maxa,  *
 *                   double huge *vetb,long int neq, long int itmax,      *
 *                   double eps, double beta, int codconvg);              *
 * (11) Headers necessarios:                                              *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 *      math.h                                                            *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Metodo Iterativo Gauss - Seidel utilizando sobre relaxacao
** A Matriz do Sistema e' simetrica armazenada na forma escalonada
** por colunas ascendentes - Skyline
*/

int mitgslsk(double huge *vetx, double huge *sk, int huge *maxa,
             double huge *vetb, long neq, long itmax, double eps,
             double beta, int codconvg)
    {
    double huge *pcfin,   /* pointer para 'vetb' */
           huge *xi;      /* pointer para vetor resposta 'vetx' */
    double dif,           /* diferenca entre o valor 'x' da iteracao atual
                             e da anterior */
           difaux,        /* valor da norma infinita, ou seja, e' o maior
                             valor de 'dif' */
           dif2,          /* valor de 'dif' elevado ao quadrado */
           xi2,           /* valor de 'x' calculado elevado ao quadrado */
           temp,          /* variavel temporaria que armazena o valor de 'x' */
           norvtx,        /* variavel que recebe a norma euclideana do vetor 'x' */
           nordif,        /* variavel que recebe a norma euclideana do vetor
                             que contem as diferencas */
           betaux,        /* constante utilizada no calculo de 'x' */
           betaii;        /* recebe o valor de beta dividido pelo elemento
                             correspondente da diagonal da matriz do sistema */
    register long int j,
                      i;  /* variaveis utlizadas como contadores */
    long int iter,        /* numero de iteracoes */
             primel,      /* posicao no vetor 'sk' do primeiro elemento da coluna
                             a ser multiplicado */
             ultel,       /* posicao no vetor 'sk' do ultimo elemento da coluna
                             a ser multiplicado */
             ii,          /* variavel auxiliar que percorre o vetor 'sk' */
             maxai,
             maxaj,       /* posicao dos elementos da diagonal */
             aux,         /* posicao do elemento supradiagonal no vetor 'sk'
                             a ser multiplicado */
             hcolj;       /* altura da coluna */
    int      converge;    /* variavel que retorna o codigo para convergencia
                             0 - se nao convergir
                             1 - se convergir     */

    /* inicializa a constant betaux, */
    /* a contagem das iteracoes */
    /* e o codigo para convergencia */

    betaux = (1. - beta);
    iter = 0;
    converge = 0;

    /* testa se ha erro no codigo de tipo de convergencia */
    /* se ha assume codconvg = 1 */

    if ((codconvg != 1) && (codconvg != 2))
       codconvg = 1;

    while ((!converge) && (iter < itmax))
        {
        xi2 = difaux = dif2 = 0.0;
        xi = vetx;
        pcfin = vetb;
        for (i = 0; i < neq; i++)
            {
            temp = 0.0;
            dif = 0.0;
            maxai = *(maxa + i);
            if (i > 0)              /* elementos subdiagonais */
                {
                primel = maxai + 1;
                ultel  = *(maxa + i + 1) - 1;
                ii = i;
                for (j = primel; j <= ultel; j++)
                    {
                    ii--;
                    temp += *(sk + j) * *(vetx + ii);
                    }
                }
            betaii = beta/(*(sk + maxai));
            for (j = i + 1; j < neq; j++)        /* elementos supradiagonais */
                {
                maxaj = *(maxa + j);
                hcolj = *(maxa + j + 1) - maxaj;
                aux = maxaj + (j - i);
                if ((j - hcolj) < i)
                    {
                    temp += *(sk + aux) * *(vetx + j);
                    }
                }
            temp = (((*pcfin++ - temp) * betaii) + ((*xi) * betaux));
            dif   = fabs(temp - (*xi));
            dif2 += dif * dif;
            if (dif > difaux) difaux = dif;
            xi2 += temp * temp;
            *xi++ = temp;
            }
        iter++;
        norvtx = sqrt(xi2);
        nordif = sqrt(dif2);

        switch (codconvg)
            {
            case 1: if ((nordif/norvtx) <= eps)
                       converge = 1;
                    break;

            case 2: if (difaux <= eps)
                       converge = 1;
                    break;
            default:
                break;
            }
        }
    return(converge);
    }


