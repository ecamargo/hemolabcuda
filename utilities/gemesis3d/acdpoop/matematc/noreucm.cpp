/*
 *************************************************************************
 *                                                                       *
 * double noreucm(ma, nlin, ncol)                                        *
 *                                                                       *
 * (01) Objetivo:                                                        *
 *      Calcular a norma euclideana de uma matriz do tipo double         *
 *      armazenadas por linhas.                                          *
 * (02) Forma de utilizacao:                                             *
 *      variavel real = noreucm(ma, nlin, ncol);                         *
 * (03) Descricao:                                                       *
 *      Calcula e retorna a norma euclideana de uma matriz do tipo       *
 *      double armazenada por linhas resultando um numero real.          *
 * (04) Palavras chave:                                                  *
 *      matriz                                                           *
 *      produto                                                          *
 *      escalar                                                          *
 * (05) Referencia:                                                      *
 *      ---                                                              *
 * (06) Data e autor:                                                    *
 *      Rio de Janeiro, LNCC, agosto de 1989,                            *
 *      Roberto Eduardo Garcia.                                          *
 * (07) Argumentos:                                                      *
 *      -> *ma  - pointer para a matriz 'ma'         - double huge       *
 *      -> nlin - numero de linhas da matriz 'ma'    - long              *
 *      -> ncol - numero de colunas da matriz 'ma'   - long              *
 * (08) Variaveis externas utilizadas:                                   *
 *      ---                                                              *
 * (09) Chamadas:                                                        *
 *      pscalm                                                           *
 * (10) Prototype:                                                       *
 *      matematc.h                                                       *
 *      double noreucm(double huge *ma, long int nlin, long int ncol);   *
 * (11) Headers necessarios:                                             *
 *      stdio.h, math.h, matematc.h                                      *
 * (12) Mensagens de erro:                                               *
 *      ---                                                              *
 * (13) Depuracao:                                                       *
 *      ---                                                              *
 * (14) Portabilidade:                                                   *
 *      C standard.                                                      *
 * (15) Acesso:                                                          *
 *      Livre ao usuario.                                                *
 *                                                                       *
 *************************************************************************
*/

#include "matematc.h"
#include <math.h>

/*
** Rotina que calcula a norma euclideana
*/

double noreucm(double huge *ma, long int nlin, long int ncol)
    {
    return (sqrt(pscalm(ma, ma, nlin, ncol)));
    }
