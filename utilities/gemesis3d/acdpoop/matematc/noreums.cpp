/*
 *************************************************************************
 *                                                                       *
 * double noreums(mas, ncolms)                                           *
 *                                                                       *
 * (01) Objetivo:                                                        *
 *      Calcular a norma euclideana de uma matriz simetrica do tipo      *
 *      double armazenada na forma supradiagonal por linhas.             *
 * (02) Forma de utilizacao:                                             *
 *      variavel real = noreums(mas, ncolms);                            *
 * (03) Descricao:                                                       *
 *      Calcula e retorna a norma euclideana de uma matrize simetrica    *
 *      do tipo double armazenada na forma supradiagonal por linhas      *
 *      resultando um numero real.                                       *
 * (04) Palavras chave:                                                  *
 *      matriz                                                           *
 *      simetrica                                                        *
 *      escalar                                                          *
 *      produto                                                          *
 * (05) Referencia:                                                      *
 *      ---                                                              *
 * (06) Data e autor:                                                    *
 *      Rio de Janeiro, LNCC, janeiro de 1989,                           *
 *      Roberto Eduardo Garcia.                                          *
 * (07) Argumentos:                                                      *
 *      -> *mas   - pointer para a matriz simetrica 'mas'  - double huge *
 *      -> ncolms - numero de colunas da matriz simetrica  - long        *
 * (08) Variaveis externas utilizadas:                                   *
 *      ---                                                              *
 * (09) Chamadas:                                                        *
 *      pscalms                                                          *
 * (10) Prototype:                                                       *
 *      matematc.h                                                       *
 *      double noreums(double huge *mas, long int ncolms);               *
 * (11) Headers necessarios:                                             *
 *      stdio.h, math.h, matematc.h                                      *
 * (12) Mensagens de erro:                                               *
 *      ---                                                              *
 * (13) Depuracao:                                                       *
 *      ---                                                              *
 * (14) Portabilidade:                                                   *
 *      C standard.                                                      *
 * (15) Acesso:                                                          *
 *      Livre ao usuario.                                                *
 *                                                                       *
 *************************************************************************
*/

#include "matematc.h"
#include <math.h>

double noreums(double huge *mas, long int ncolms)
    {
    return(sqrt(pscalms(mas, mas, ncolms)));
    }
