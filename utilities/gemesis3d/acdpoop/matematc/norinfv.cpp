/*
 **************************************************************************
 *                                                                        *
 * void norinfv                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Aplicar norma infinita.                                           *
 * (02) Forma de utilizacao:                                              *
 *      resultado = norinfv(u, dim)                                       *
 * (03) Descricao:                                                        *
 *      Aplicacao da Norma Infinita.                                      *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 25 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <> *u   -  pointer para vetor - double huge                       *
 *      -> dim  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void norinfv(double huge *u, long dim);                           *
 * (11) Headers necessarios:                                              *
 *      stdio.h, math.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"
#include <math.h>

double norinfv(double huge *u, long dim)
    {
    register long i;
    double max = fabs(*u++);
    double aux;
    for (i = 1; i < dim; i++)
        {
        aux = fabs(*u++);
        if (max < aux)
            max = aux;
        }
    return (max);
    }
