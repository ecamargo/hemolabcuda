/*
 **************************************************************************
 *                                                                        *
 * double norinms(ms, ncolms)                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Fornecer a norma infinita de uma matriz simetrica do tipo         *
 *      double armazenada na forma  supradiagonal por linhas.             *
 * (02) Forma de utilizacao:                                              *
 *      norma infinita = norinms(ma, ncolms);                             *
 * (03) Descricao:                                                        *
 *      Procura e fornece o valor do maior elemento da matriz simetrica,  *
 *      em modulo; ou seja, a norma infinita da matriz simetrica.         *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      simetrica                                                         *
 *      norma                                                             *
 *      infinita                                                          *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, agosto de 1989,                                   *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ms    - pointer para a matriz simetrica 'mas'   - double huge *
 *      -> ncolms - numero de colunas da matriz simetrica   - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      double norinms(double huge *ms, long int ncolms);                 *
 * (11) Headers necessarios:                                              *
 *      stdio.h, math.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"
#include <math.h>

/*
** Rotina que fornece a norma infinita da matriz simetrica
*/

double norinms(double huge *ms, long int ncolms)
    {
    double huge *ptr_rsp,     /* pointer que devolve o maior valor em modulo */
           huge *ptr_aux;     /* pointer que varre a matriz procurando a norma infinita */
    register long int nelem;  /* numero de elementos da matriz simetrica */

    ptr_rsp = ptr_aux = ms;

    nelem = ((ncolms + 1) * ncolms)/2;

    while (nelem--)
         {
         ptr_aux++;
         if (fabs(*ptr_rsp) < fabs(*ptr_aux))
              {
              ptr_rsp = ptr_aux;
              }
         }
    return (fabs(*ptr_rsp));
    }
