/*
 **************************************************************************
 *                                                                        *
 * void normaev                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Calcula a Norma Euclidiana do vetor.                              *
 * (02) Forma de utilizacao:                                              *
 *      resultado = normaev(u, dim)                                       *
 * (03) Descricao:                                                        *
 *      Calcula a Norma Euclidiana do vetor 'u' do tipo double huge e     *
 *      dimensao 'dim'.                                                   *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 25 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <> *u   -  pointer para vetor - double huge                       *
 *      -> dim  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      scalv                                                              *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void normaev(double huge *u, long dim);                           *
 * (11) Headers necessarios:                                              *
 *      stdio.h, math.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"
#include <math.h>


double normaev(double huge *u, long dim)
    {
    return (sqrt(scalv(u,u,dim)));
    }
