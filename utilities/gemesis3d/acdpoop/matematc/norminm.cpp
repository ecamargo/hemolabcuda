/*
 **************************************************************************
 *                                                                        *
 * double norminm(ma, nlin, ncol)                                         *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Fornecer a norma infinita de uma matriz do tipo double armazenada *
 *      por linhas.                                                       *
 * (02) Forma de utilizacao:                                              *
 *      norma infinita = norminm(ma, nlin, ncol);                         *
 * (03) Descricao:                                                        *
 *      Procura e fornece o valor do maior elemento da matriz, em modulo; *
 *      ou seja, a norma infinita da matriz.                              *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      norma                                                             *
 *      infinita                                                          *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, agosto de 1989                                    *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ma   - pointer para os elementos da matriz 'ma'- double huge  *
 *      -> nlin  - numero de linhas da matriz.             - long         *
 *      -> ncol  - numero de colunas da matriz             - long         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      double norminm(double huge *ma, long int nlin, long int ncol);    *
 * (11) Headers necessarios:                                              *
 *      stdio.h, math.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"
#include <math.h>

/*
** Rotina que fornece a norma infinita da matriz
*/

double norminm(double huge *ma, long int nlin, long int ncol)
    {
    double huge *ptr_rsp,      /* pointer que devolve o maior valor em modulo */
           huge *ptr_aux;      /* pointer que varre a matriz procurando a norma infinita */
    register long int nelem;   /* numero de elementos da matriz */

    ptr_rsp = ptr_aux = ma;

    nelem = nlin * ncol;

    while (nelem--)
         {
         ptr_aux++;
         if (fabs(*ptr_rsp) < fabs(*ptr_aux))
              {
              ptr_rsp = ptr_aux;
              }
         }
    return (fabs(*ptr_rsp));
    }
