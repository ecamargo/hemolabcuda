/*
 **************************************************************************
 *                                                                        *
 * void pmatbsa(mcs, ma, mbs, nlina, ncola);                              *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar duas matrizes do tipo double armazenadas por linhas,  *
 *      sendo a segunda uma matriz simetrica armazenada na forma          *
 *      supradiagonal, e pre-multiplicar pela transposta da primeira.     *
 * (02) Forma de utilizacao:                                              *
 *      pmatbsa(mcs, ma, mbs, nlina, ncola);                              *
 * (03) Descricao:                                                        *
 *      Calcula o produto matricial entre duas matrizes do tipo double,   *
 *      sendo a segunda uma matriz simetrica, e pre-mutiplica pela        *
 *      transposta da primeira matriz resultando uma matriz simetrica     *
 *      armazenada na forma supradiagonal por linhas , ou seja, realiza   *
 *      o produto CS = AT * BS * A.                                       *
 * (04) Palavras chave:                                                   *
 *      produto                                                           *
 *      matriz                                                            *
 *      transposta                                                        *
 *      simetrica                                                         *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, dezembro de 1988,                           *
 *      Roberto Eduardo Garcia                                            *
 * (07) Argumentos:                                                       *
 *      -> *ma   - pointer para a matriz 'ma'          - double huge      *
 *      -> *mbs  - pointer para a matriz simetrica     - double huge      *
 *      <- *mcs  - pointer para a matriz resultado     - double huge      *
 *      -> nlina - numero de linhas da matriz  'ma'     - long            *
 *      -> ncola - numero de colunas da matriz 'ma'     - long            *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void pmatbsa(double huge *mcs, double huge *ma, double huge *mbs, *
 *                   long int nlina, long int ncola);                     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"

/*
** Rotina que calcula o produto matricial
** O produto e' realizado segundo a formula matematica a seguir
**                  Ci,j = At i,k * Bk,r * Ar,j
*/

void pmatbsa(double huge *mcs, double huge *ma, double huge *mbs,
             long nlina,       long ncola)
    {
    double huge *pckat,    /* pointer para a coluna 'k' da matriz 'ma' transposta */
           huge *pcja,     /* pointer para a coluna 'j' da matriz 'ma' */
           huge *pcjc,     /* pointer para a coluna 'j' da matriz 'mcs' */
           huge *pcji,     /* pointer para os elementos subdiagonais da matriz 'mcs' */
           huge *pckb,     /* pointer para a coluna 'k' da matriz 'mb' */
           huge *pbrk,     /* pointer para os elementos subdiagonais da matriz 'mbs' */
           huge *pbkr,     /* pointer para os elementos supradiagonais da matriz 'mbs' */
           huge *parj,     /* pointer para a matriz 'ma' */
           huge *patik;    /* pointer para a matriz 'ma' transposta */
    register long r, k, j; /* variaveis utilizadas como contadores */
    long int nlina1,       /* armazena numero de linhas da matriz 'ma' - 1 */
             pulok;        /* indica quantas posicoes deve saltar o pointer 'pbrk' */
    double bakj;           /* variavel que armazena o produto entre os elementos das
                              matrizes 'mbs' e 'ma' */

    nlina1 = nlina - 1;
    pckat  = pcja = ma;   /* ptr col 'k' de at e ptr col 'j' de a */
    pcjc   = mcs;         /* ptr col 'j' de c */

    /*
    ** zera matriz resultado
    */
    r = (ncola * (ncola + 1))/2;
    while (r--)
        *pcjc++ = 0.0;

    pcjc = mcs;
    for (j = 0; j < ncola; j++)
        {
        pckb = mbs;
        patik = pckat;

        for (k = 0; k < nlina; k++)
            {
            bakj  = 0.0;
            pbrk  = pckb;    /* ptr p/ elem. brk */
            pulok = nlina1;
            parj  = pcja;    /* ptr p/ elem. arj */
            r = k;
            while (r--)
                {
                bakj += (*pbrk) * (*parj);
                pbrk += pulok--;
                parj += ncola;
                }
            pbkr = pbrk;
            r = nlina - k;
            while (r--)
                {
                bakj += (*pbkr++) * (*parj);
                parj += ncola;
                }

            pcji  = pcjc;   /* ptr p/ elem. cji */

            r = ncola - j;
            while (r--)
                {
                *pcji += bakj * (*patik++);
                pcji++;
                }

            patik += j;
            pckb++;
            }
        pcjc = pcji;
        pcja++;
        pckat++;
        }
    }
