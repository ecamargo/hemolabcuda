/*
 **************************************************************************
 *                                                                        *
 * void prmatba(mc, ma, mb, nlina, ncola)                                 *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar duas matrizes do tipo double armazenadas por linhas   *
 *      e pre-multiplicar pela transposta da primeira matriz.             *
 * (02) Forma de utlizacao:                                               *
 *      prmatba(ma, mb, mc, nlina, ncola);                                *
 * (03) Descricao:                                                        *
 *      Calcula o produto matricial de duas matrizes do tipo double       *
 *      armazenadas por linhas e pre-multiplica pela transposta da        *
 *      primeira matriz, ou seja, realiza o produto C = AT * B * A.       *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, outubro de 1988,                            *
 *      Roberto Eduardo Garcia                                            *
 * (07) Argumantos:                                                       *
 *      -> *ma   - pointer para a matriz 'ma'            - double huge    *
 *      -> *mb   - pointer para a matriz 'mb'            - double huge    *
 *      <- *mc   - pointer para a matriz resultado       - double huge    *
 *      -> nlina - numero de linhas da matriz 'ma'       - long           *
 *      -> ncola - numero de colunas da matriz 'ma'      - long           *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void prmatba(double huge *mc, double huge *ma, double huge *mb,   *
 *                          long int nlina, long int ncola);              *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto matricial
** O produto e' realizado de acordo com a formula matematica a seguir
**                     Ci,j = At i,r * Br,k * Ak,j
*/

void prmatba(double huge *mc, double huge *ma, double huge *mb,
             long nlina,      long ncola)
    {
    double huge *pcjc,    /* pointer para a coluna 'j' da matriz 'mc' */
           huge *pckb,    /* pointer para a coluna 'k' da matriz 'mb' */
           huge *patir,   /* pointer para a matriz 'ma' transposta */
           huge *pcja,    /* pointer para a coluna 'j' da matriz 'ma' */
           huge *pbrk,    /* pointer para a matriz 'mb' */
           huge *pakj;    /* pointer para a coluna 'j' da matriz 'ma' */
    double barj;          /* variavel que armazena os produtos entre os
                             elementos das matrizes 'mb' e 'ma' */
    register long int r,
                      k,
                      j;  /* variaveis utilizadas como contadores */

    pcja = ma;
    pcjc = mc;

    r = ncola * ncola;

    while (r--)
        *pcjc++ = 0.0;      /* zera a matriz resultado */
    pcjc = mc;

    for (j = 0; j < ncola; j++)
        {
        pckb = mb;
        patir = ma;
        for (k = 0; k < nlina; k++)
            {
            barj = 0.0;
            pbrk = pckb;
            pakj = pcja;
            r = nlina;
            while (r--)
                {
                barj += (*pbrk) * (*pakj);
                pbrk += nlina;
                pakj += ncola;
                }
            r = ncola;
            while (r--)
                {
                *pcjc += barj * (*patir++);
                pcjc++;
                }
            pcjc = mc;
            pckb++;
            }
        mc += ncola;
        pcjc = mc;
        pcja++;
        }
    }
