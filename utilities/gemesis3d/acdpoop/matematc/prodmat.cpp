/*
 **************************************************************************
 *                                                                        *
 * void prodmat(mc, ma, mb, nlina, ncola, ncolb)                          *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar duas matrizes do tipo double armazenadas por linhas.  *
 * (02) Forma de utilizacao:                                              *
 *      prodmat(mc, ma, mb, nlina, ncola, ncolb);                         *
 * (03) Descricao:                                                        *
 *      Calcula o produto de duas matrizes do tipo double armazenadas por *
 *      linhas.                                                           *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, outubro de 1988,                            *
 *      Roberto Eduardo Garcia                                            *
 *      Salgado - dezembro de 1994                                        *
 * (07) Argumentos:                                                       *
 *      -> *ma   - pointer para a matriz 'ma'              - double huge  *
 *      -> *mb   - pointer para a matriz 'mb'              - double huge  *
 *      <- *mc   - pointer para a matriz resultado         - double huge  *
 *      -> nlina - numero de linhas da matriz 'ma'         - long         *
 *      -> ncola - numero de colunas da matriz 'ma'        - long         *
 *      -> ncolb - numero de colunas da matriz 'mb'        - long         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void prodmat(double huge *mc, double huge *ma, double huge *mb,   *
 *                   long int nlina, long int ncola, long int ncolb);     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto matricial
*/


static double myscal(double huge *a, double huge *b, long nca, long ncb)
    {
    double s = 0;
    register long i;

    for (i = 0; i < nca; i++, b += ncb)
        s += *a++ * *b;
    return(s);
    }

void prodmat(double huge *c, double huge *a, double huge *b,
             long nla, long nca, long ncb)
    {
    register long i, j;
    double huge *auxb = b;

    for (i = 0; i < nla; i++, a+= nca)
        {
        b = auxb;
        for (j = 0; j < ncb; j++)
            *c++ = myscal(a, b++, nca, ncb);
        }
    }


