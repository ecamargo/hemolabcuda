//
// Produto de matriz retangular m por vetor v
// A matriz m esta organizada por filas.
//
#include "matematc.h"

void prodmatv(double huge *u, double huge *m, double huge *v, long nfil,
              long ncol)
    {
    double huge *pu = u;
    double huge *pm = m;
    for (long i = 0; i < nfil; i++, pu++, pm+=ncol)
        *pu = scalv( pm, v, ncol);
    }
