/*
 **************************************************************************
 *                                                                        *
 * void prodmskv(vetc, sk, maxa, vetb, ncolmsk)                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar uma matriz simetrica do tipo sky-line (colunas        *
 *      ascendentes) por um vetor do tipo double armazenado por linha.    *
 * (02) Forma de utilizacao:                                              *
 *      prodmskv(vetc, sk, maxa, vetb, ncolmsk);                          *
 * (03) Descricao:                                                        *
 *      Calcula o produto de uma matriz simetrica do tipo sky-line        *
 *      (colunas ascendentes) armazenada nos vetores 'sk' e 'maxa'        *
 *      por um vetor armazenado por linha. Tanto os elementos do vetor    *
 *      quanto os da matriz sao do tipo double.                           *
 * (04) Palavras chave:                                                   *
 *      vetor                                                             *
 *      matriz                                                            *
 *      sky-line                                                          *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, novembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <- *vetc   - pointer para vetor resposta 'vetc'     - double huge *
 *      -> *sk     - pointer para matriz do sistema 'sk'    - double huge *
 *      -> *vetb   - pointer para vetor  'vetb'             - double huge *
 *      -> *maxa   - pointer para o vetor que contem a posicao dos        *
 *                   elementos da diagonal principal da 'sk'  - long huge *
 *      -> ncolmsk - numero de colunas da  matriz 'sk'      - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas                                                          *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void prodmskv(double huge *vetc, double huge *sk, long huge *maxa,*
 *                    double huge *vetb, long int ncolmsk);               *
 * (11) Headers necessarios:                                              *
 *      stdio.h                                                           *
 *      matematc.h                                                        *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void prodmskv(double huge *vetc, double huge *sk, long huge *maxa,
              double huge *vetb, long ncolmsk)
//double huge *vetc,            pointer para vetor resposta 
//       huge *sk,              pointer para matriz do sistema 'sk' 
//       huge *vetb;            pointer para vetor 
//long int huge *maxa;          posicao dos elementos da diagonal principal 
//long int ncolmsk;             numero de colunas da  matriz
    {
    double huge *vcaux;      /* pointer auxiliar para zerar as posicoes do vetor 'vetc'*/
    register long int j, i;  /* variaveis utlizadas como contadores */
    long int primel,         /* posicao do elemento da diagonal principal */
             ultel,          /* posicao do proximo elemento da diagonal principal
                                (em relacao a primel) */
             maxai,
             maxaj,          /* guardam a posicao do elemento da diagonal principal */
             hcolj,          /* altura da coluna j */
             aux,
             ii;             /* variaveis utilizadas como indices para elementos
                                de vetores */

    vcaux = vetc;
    for (j = 0; j < ncolmsk; j++)
        *vcaux++ = 0.0;
    vcaux = vetc;

    for (i = 0; i < ncolmsk; i++)
        {
        maxai = *(maxa + i);
        primel = maxai;
        ultel  = *(maxa + i + 1);
        ii = i;
        for (j = primel; j < ultel; j++)
            {
            *vetc += *(sk + j) * *(vetb + ii);
            ii--;
            }
        for (j = i + 1; j < ncolmsk; j++)
            {
            maxaj = *(maxa + j);
            hcolj = *(maxa + j + 1) - maxaj;
            aux = maxaj + (j - i);
            if ((j - hcolj) < i)
                *vetc += *(sk + aux) * *(vetb + j);
            }
        vetc++;
        }
    }







