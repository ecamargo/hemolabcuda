/*
 **************************************************************************
 *                                                                        *
 * void prodmsv(vetc, ms, vet, ncolms)                                    *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar uma matriz simetrica do tipo double armazenada na     *
 *      forma supradiagonal por linhas por um vetor do tipo double        *
 *      armazenado por linha.                                             *
 * (02) Forma de utilizacao:                                              *
 *      prodmsv(vetc, ms, vet, ncolms);                                   *
 * (03) Descricao:                                                        *
 *      Calcula o produto de uma matriz simetrica do tipo double          *
 *      armazenada na forma supradiagonal por linhas por um vetor         *
 *      do tipo double armazenado por linha.                              *
 * (04) Palavras chave:                                                   *
 *      vetor                                                             *
 *      matriz                                                            *
 *      simetrica                                                         *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, novembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ms    - pointer para a matriz simetrica        - double huge  *
 *      -> *vet   - pointer para o vetor                   - double huge  *
 *      <- *vetc  - pointer para o vetor resultado         - double huge  *
 *      -> ncolms - numero de colunas da matriz simetrica  - long         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas                                                          *
 *      scalv                                                             *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void prodmsv(double huge *vetc, double huge *ms, double huge *vet,*
 *                   long int ncolms);                                    *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto matriz simetrica * vetor
*/

void prodmsv(double huge *vetc, double huge *ms, double huge *vet,
             long ncolms)
    {
    double huge *pamtms,    /* pointer auxiliar de multiplicacao para a matriz simetrica */
           huge *pamtvet,   /* pointer auxiliar de multiplicacao para o vetor */
           huge *pvetrsp;   /* pointer para o vetor resposta */
    register long int ks,
                      aux;  /* variaveis utlizadas como contadores */

    pamtvet = vet;
    pamtms = ms;
    pvetrsp = vetc;

   /* Calculo dos produtos escalares vetor * vetor */

    for (aux = ncolms; aux > 0; aux--)
         {
         *vetc++ = scalv(pamtvet++, pamtms, aux);
         pamtms += aux;
         }

   /* Contribuicao dos elementos supradiagonais da matriz simetrica */

    ks = aux = ncolms - 1;
    pamtms = ms + 1;
    pamtvet = vet;
    vetc = ++pvetrsp;
    while (ks)
         {
         while(aux--)
              {
              *vetc += ((*pamtvet) * (*pamtms++));
              vetc++;
              }
         pamtvet++;
         pamtms++;
         vetc = ++pvetrsp;
         aux = --ks;
         }
    }
