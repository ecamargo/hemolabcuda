/*
 **************************************************************************
 *                                                                        *
 * void promabs(mc, ma, mbs, nlina, ncola)                                *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar uma matriz do tipo double armazenada por linhas por   *
 *      uma matriz simetrica tambem do tipo double armazenada na forma    *
 *      supradiagonal por linhas.                                         *
 * (02) Forma de utilizacao:                                              *
 *      promabs(mc, ma, mbs, nlina, ncola);                               *
 * (03) Descricao:                                                        *
 *      Calcula o produto de duas matrizes do tipo double armazenadas     *
 *      por linhas sendo a segunda uma matriz simetrica.                  *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      simetrica                                                         *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, novembro de 1988,                           *
 *      Roberto Eduardo Garcia                                            *
 * (07) Argumentos:                                                       *
 *      -> *ma    - pointer para a matriz 'ma'            - double huge   *
 *      -> *mbs   - pointer para a matriz simetrica       - double huge   *
 *      <- *mc    - pointer para a matriz resultado       - double huge   *
 *      -> nlina  - numero de linhas da matriz  'ma'      - long          *
 *      -> ncola  - numero de colunas da matriz 'ma'      - long          *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      scalv                                                             *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void promabs(double huge *mc, double huge *ma, double huge *mbs,  *
 *                   long int nlina, long int ncola);                     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto matriz * matriz simetrica
*/

void promabs(double huge *mc, double huge *ma, double huge *mbs,
             long nlina,      long ncola)
    {
    double huge *pamtma,          /* pointer auxiliar de multiplicacao da matriz 'ma' */
           huge *pamtms,          /* pointer auxiliar de multiplicacao da matriz 'mbs'*/
           huge *pauxmc;          /* pointer auxiliar para a matriz 'mc' */
    register long int aux, ks;    /* contadores */

    pamtma = ma;
    pamtms = mbs;
    pauxmc = mc;

    while (nlina--)
        {
        for (aux = ncola; aux > 0; aux--)
            {
            *mc++ = scalv(pamtma++, pamtms, aux);
            pamtms += aux;
            }

    /* Contribuicao dos elementos supradiagonais da matriz simetrica */

        aux = ks = ncola - 1;
        pamtms = mbs + 1;
        pamtma = ma;
        mc = ++pauxmc;

        while (ks)
            {
            while (aux--)
                {
                *mc += ((*pamtma) * (*pamtms++));
                mc++;
                }
            pamtma++;
            pamtms++;
            mc = ++pauxmc;
            aux = --ks;
            }
        ma += ncola;
        pamtma++;
        pamtms = mbs;
        }
    }
