/*
 **************************************************************************
 *                                                                        *
 * void promatb(mc, ma, mb, nlina, ncola, ncolb)                          *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar duas matrizes do tipo double armazenadas por linhas,  *
 *      sendo a primeira uma matriz transposta.                           *
 * (02) Forma de utilizacao:                                              *
 *      promatb(mc, ma, mb, nlina, ncola, ncolb);                         *
 * (03) Descricao:                                                        *
 *      Calcula o produto de uma matriz transposta do tipo double         *
 *      armazenada por linhas (nao e' entrada na forma transposta) por    *
 *      uma matriz tambem do tipo double armazenada por linhas, ou seja,  *
 *      realiza o produto C = AT * B.                                     *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, outubro de 1988,                            *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ma   - pointer para a matriz 'ma'            - double huge    *
 *      -> *mb   - pointer para a matriz 'mb'            - double huge    *
 *      <- *mc   - pointer para a matriz resultado       - double huge    *
 *      -> nlina - numero de linhas da matriz 'ma'       - long           *
 *      -> ncola - numero de colunas da matriz 'ma'      - long           *
 *      -> ncolb - numero de colunas da matriz 'mb'      - long           *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void promatb(double huge *mc, double huge *ma, double huge *mb,   *
 *                   long int nlina, long int ncola, long int ncolb);     *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc,h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto matricial
*/

void promatb(double huge *mc, double huge *ma, double huge *mb,
             long nlina,      long ncola,      long ncolb)
    {
    double huge *colaptr,         /* pointer para as colunas da matriz 'ma' */
           huge *colbptr,         /* pointer para as colunas da matriz 'mb' */
           huge *einmb;           /* aponta para o endereco inicial da matriz 'mb' */
    double peaux;                 /* variavel que armazena os produtos escalares */
    register long int cont,
                      aux, aux1;  /* variaveis utilizadas como contadores */

    colaptr = ma;
    colbptr = einmb = mb;
    aux     = ncola;
    cont    = aux1  = nlina;

    while (aux--)
         {
         while (aux1--)
              {
              peaux = 0.0;
              for (; cont--; ma += ncola, mb += ncolb)
                  peaux += ((*ma) * (*mb));
              *mc++ = peaux;
              cont = nlina;
              ma = colaptr;
              mb = ++colbptr;
              }
         aux1 = nlina;
         ma = ++colaptr;
         mb = colbptr = einmb;
         }
    }
