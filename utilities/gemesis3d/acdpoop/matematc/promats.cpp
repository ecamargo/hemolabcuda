/*
 **************************************************************************
 *                                                                        *
 * void promats(mc, mas, mbs, ncolms)                                     *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar duas matrizes simetricas do tipo double armazenadas   *
 *      na forma supradiagonal por linhas.                                *
 * (02) Forma de utilizacao:                                              *
 *      promats(mc, mas, mbs, ncolms);                                    *
 * (03) Descricao:                                                        *
 *      Calcula o produto de duas matrizes simetricas do tipo double      *
 *      armazenadas na forma supradiagonal por linhas resultando uma      *
 *      matriz quadrada nao simetrica armazenada por linhas.              *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, outubro de 1988,                            *
 *      Roberto Eduardo Garcia                                            *
 * (07) Argumentos:                                                       *
 *      -> *mas   - pointer para a matriz 'mas'       - double huge       *
 *      -> *mbs   - pointer para a matriz 'mbs'       - double huge       *
 *      <- *mc    - pointer para a matriz resultado   - double huge       *
 *      -> ncolms - numero de colunas das matrizes    - long              *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      prodmsv                                                           *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void promats(double huge *mc, double huge *mas, double huge *mbs, *
 *                   long int ncolms);                                    *
 * (11) Headers necessarios:                                              *
 *      stdio.h, stdlib.h, matematc.h                                     *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"

/*
** Rotina que calcula o produto matricial
*/

void promats(double huge *mc, double huge *mas, double huge *mbs,
             long ncolms)
    {
    double huge *vas,        /* pointer para um vetor temporario -> armazena
                                linha por linha da matriz 'mas' */
           huge *einvas,     /* aponta para o endereco inicial de 'vas' */
           huge *ptrcp1,
           huge *ptrcp2;     /* pointers auxiliares para a copia dos elementos
                                de 'mas' para 'vas' */
    register long int cont,
                         j,
                         i,  /* contadores */
                       aux,  /* armazena ncolms */
                      aux1,  /* armazena ncols1 */
                    ncols1,  /* numero de colunas - 1 */
                    ncolcp;  /* numero de colunas copiadas */

    /* aloca espaco de memoria para 'vas' */
    vas = (double huge *) mMalloc((size_t) ncolms * sizeof(double));
    if (!vas)
         Error(2, 1, "Alocacao de memoria do vetor auxiliar VAS");
    einvas = vas;
    ptrcp1 = ptrcp2 = mas;
    aux = ncolms;
    ncols1 = ncolms - 1;
    ncolcp = 0;
    i = ncolms;
    while (i--)
         {
         aux1 = ncols1;
         for (cont = 0; cont < ncolcp; cont++)
              {                           /* copia elementos subdiagonais */
              *vas++ = *ptrcp1;
              ptrcp1 += aux1--;
              }
         ptrcp1 = ++ptrcp2;

         j = aux--;
         while (j--)                      /* copia elementos supradiagonais */
              *vas++ = *mas++;

         ncolcp++;
         vas = einvas;
         prodmsv(mbs, vas, mc, ncolms);   /* chama a rotina prodmsv */
         mc += ncolms;
         }
    mFree((char *) vas);                  /* libera o pointer 'vas' */
    }
