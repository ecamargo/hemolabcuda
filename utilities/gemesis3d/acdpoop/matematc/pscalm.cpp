/*
 *************************************************************************
 *                                                                       *
 * double pscalm(ma, mb, nlin, ncol)                                     *
 *                                                                       *
 * (01) Objetivo:                                                        *
 *      Calcular o produto escalar de duas matrizes do tipo double       *
 *      armazenadas por linhas.                                          *
 * (02) Forma de utilizacao:                                             *
 *      variavel real = pscalm(ma, mb, nlin, ncol);                      *
 * (03) Descricao:                                                       *
 *      Calcula e retorna o produto escalar de duas matrizes do tipo     *
 *      double armazenadas por linhas resultando um numero real.         *
 * (04) Palavras chave:                                                  *
 *      matriz                                                           *
 *      produto                                                          *
 *      escalar                                                          *
 * (05) Referencia:                                                      *
 *      ---                                                              *
 * (06) Data e autor:                                                    *
 *      Rio de Janeiro, LNCC, janeiro de 1989,                           *
 *      Roberto Eduardo Garcia.                                          *
 * (07) Argumentos:                                                      *
 *      -> *ma  - pointer para a matriz 'ma'             - double huge   *
 *      -> *mb  - pointer para a matriz 'mb'             - double huge   *
 *      -> nlin - numero de linhas de cada matriz        - long          *
 *      -> ncol - numero de colunas de cada matriz       - long          *
 * (08) Variaveis externas utilizadas:                                   *
 *      ---                                                              *
 * (09) Chamadas:                                                        *
 *      ---                                                              *
 * (10) Prototype:                                                       *
 *      matematc.h                                                       *
 *      double pscalm(double huge *ma, double huge *mb, long int nlin,   *
 *                             long int ncol);                           *
 * (11) Headers necessarios:                                             *
 *      stdio.h, matematc.h                                              *
 * (12) Mensagens de erro:                                               *
 *      ---                                                              *
 * (13) Depuracao:                                                       *
 *      ---                                                              *
 * (14) Portabilidade:                                                   *
 *      C standard.                                                      *
 * (15) Acesso:                                                          *
 *      Livre ao usuario.                                                *
 *                                                                       *
 *************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto escalar entre as matrizes
*/

double pscalm(double huge *ma, double huge *mb,
              long nlin,       long ncol)
    {
    register long int dim;    /* numero de elementos de cada matriz */
    double resp = 0.0;        /* variavel que armazena o produto escalar */

    dim = nlin * ncol;

    while (dim--)
         resp += ((*ma++) * (*mb++));
    return (resp);
    }
