/*
 *************************************************************************
 *                                                                       *
 * double pscalms(mas, mbs, ncolms)                                      *
 *                                                                       *
 * (01) Objetivo:                                                        *
 *      Calcular o produto escalar de duas matrizes simetricas do tipo   *
 *      double armazenadas na forma supradiagonal por linhas.            *
 * (02) Forma de utilizacao:                                             *
 *      variavel real = pscalms(mas, mbs, ncolms);                       *
 * (03) Descricao:                                                       *
 *      Calcula e retorna o produto escalar de duas matrizes simetricas  *
 *      do tipo double armazenadas na forma supradiagonal por linhas     *
 *      resultando um numero real.                                       *
 * (04) Palavras chave:                                                  *
 *      matriz                                                           *
 *      simetrica                                                        *
 *      escalar                                                          *
 *      produto                                                          *
 * (05) Referencia:                                                      *
 *      ---                                                              *
 * (06) Data e autor:                                                    *
 *      Rio de Janeiro, LNCC, janeiro de 1989,                           *
 *      Roberto Eduardo Garcia.                                          *
 * (07) Argumentos:                                                      *
 *      -> *mas   - pointer para a matriz simetrica 'mas'  - double huge *
 *      -> *mbs   - pointer para a matriz simetrica 'mbs'  - double huge *
 *      -> ncolms - numero de colunas da matriz simetrica  - long        *
 * (08) Variaveis externas utilizadas:                                   *
 *      ---                                                              *
 * (09) Chamadas:                                                        *
 *      ---                                                              *
 * (10) Prototype:                                                       *
 *      matematc.h                                                       *
 *      double pscalms(double huge *mas, double huge *mbs,               *
 *                     long int ncolms);                                 *
 * (11) Headers necessarios:                                             *
 *      stdio.h, matematc.h                                              *
 * (12) Mensagens de erro:                                               *
 *      ---                                                              *
 * (13) Depuracao:                                                       *
 *      ---                                                              *
 * (14) Portabilidade:                                                   *
 *      C standard.                                                      *
 * (15) Acesso:                                                          *
 *      Livre ao usuario.                                                *
 *                                                                       *
 *************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula o produto escalar das matrizes simetricas
*/

double pscalms(double huge *mas, double huge *mbs, long ncolms)
    {
    double prsc = 0.0;              /* variavel que armazena o produto escalar resposta */
    double huge *einmas,            /* aponta para o endereco inicial da matriz 'mas' */
           huge *einmbs;            /* aponta para o endereco inicial da matriz 'mbs' */
    long int dim;                   /* dimensao da matriz simetrica */
    register long int cont, aux;    /* variaveis utilizadas como contadores */

    dim = ((ncolms + 1) * ncolms)/2;
    aux = cont = ncolms;

    einmas = mas;
    einmbs = mbs;

    while (dim--)
        prsc += ((*mas++) * (*mbs++));

    mas = ++einmas;
    mbs = ++einmbs;

    while (aux--)
        {
        cont = aux;
        while (cont--)
              prsc += (*mas++) * (*mbs++);
        mas++;
        mbs++;
        }
    return (prsc);
    }
