/*
 **************************************************************************
 *                                                                        *
 * void ptensv                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Realiza o produto vetorial dos vetores u e v.                     *
 * (02) Forma de utilizacao:                                              *
 *      ptensv(mat, u, v, n)                                              *
 * (03) Descricao:                                                        *
 *      Realiza o produto vetorial dos vetores 'u' e 'v', ambos do tipo   *
 *      double huge de dimensao 'n' e armazena na matriz 'mat' cujo       *
 *      numero de linhas e' dado por 'n'.                                 *
 *      Isto e': mat(i,j) = u(i) * v(j)
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 25 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *mat -  pointer para matriz - double huge                      *
 *      -> *u   -  pointer para vetor  - double huge                      *
 *      -> *v   -  pointer para vetor  - double huge                      *
 *      -> n    -  dimensao do vetor e numero de linhas da matriz - long  *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void ptensv(double huge *mat, double huge *u, double huge *v,     *
 *                  long n);                                              *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void ptensv(double huge *mat, double huge *u, double huge *v, long n)
    {
    double huge *pv = v;
    double huge *a  = (double huge *) mat;
    register long iu,iv;

    for (iu = 0; iu < n; iu++, u++)
        {
        v = pv;
        for (iv = 0; iv < n; iv++)
            *a++ = (*u) * (*v++);
        }
    }
