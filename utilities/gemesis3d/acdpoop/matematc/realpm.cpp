/*
 **************************************************************************
 *                                                                        *
 * void realpm(kma, k, ma, nlin, ncol)                                    *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar uma constante do tipo double por uma matriz do tipo   *
 *      double armazenada por linhas.                                     *
 * (02) Forma de utilizacao:                                              *
 *      realpm(kma, k, ma, nlin, ncol);                                   *
 * (03) Descricao:                                                        *
 *      Calcula o produto de uma constante do tipo double por uma matriz  *
 *      tambem do tipo double armazenada por linhas.                      *
 * (04) Palavras chave:                                                   *
 *      constante                                                         *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, setembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      k    -> constante que multiplica os elementos da matriz - double  *
 *      *ma  -> pointer para a matriz 'ma'                  - double huge *
 *      *kma -> pointer para a matriz resultado             - double huge *
 *      nlin -> numero de linhas da matrizes 'ma' e 'kma'   - long        *
 *      ncol -> numero de colunas das matrizes 'ma' e 'kma' - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void realpm(double huge *kma, double k, double huge *ma,          *
 *                           long int nlin, long int ncol);               *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"

/*
** Rotina que calcula o produto constante * matriz
*/

void realpm(double huge *kma, double k, double huge *ma,
            long nlin,        long ncol)
    {
    register long int dim;

    dim = nlin * ncol;

    while (dim--)
         *kma++ = (k * (*ma++));
    }
