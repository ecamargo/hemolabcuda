/*
 **************************************************************************
 *                                                                        *
 * void realpms(kms, k, ms, ncolms)                                       *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar uma constante do tipo double por uma matriz simetrica *
 *      do tipo double armazenada na forma supradiagonal por linhas.      *
 * (02) Forma de utilizacao:                                              *
 *      realpms(kms, k, ms, ncolms);                                      *
 * (03) Descricao:                                                        *
 *      Calcula o produto de uma constante do tipo double por uma matriz  *
 *      simetrica do tipo double armazenada na forma supradiagonal por    *
 *      linhas.                                                           *
 * (04) Palavras chave:                                                   *
 *      constante                                                         *
 *      matriz                                                            *
 *      produto                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, setembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      k      -> constante que multiplica os elementos da matriz - double*
 *      *ms    -> pointer para a matriz simetrica         - double huge   *
 *      *kms   -> pointer para a matriz resultado         - double huge   *
 *      ncolms -> numero de colunas da matriz simetrica   - long          *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void realpms(double huge *kms, double k, double huge *ms,         *
 *                           long int ncolms);                            *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/
#include "matematc.h"

/*
** Rotina que calcula o produto constante * matriz
*/

void realpms(double huge *kms, double k, double huge *ms, long ncolms)
    {
    register long int dim;          /* numero de elementos da matriz simetrica */

    dim = ((ncolms + 1) * ncolms)/2;

    while (dim--)
         *kms++ = (k * (*ms++));
    }
