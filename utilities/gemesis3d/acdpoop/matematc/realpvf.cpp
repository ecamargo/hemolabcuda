/*
 **************************************************************************
 *                                                                        *
 * void rpvetf                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar um vetor por um float.                                *
 * (02) Forma de utilizacao:                                              *
 *      rpvetf(alfav, alfa, v, n);                                        *
 * (03) Descricao:                                                        *
 *      Multiplica um vetor por um float, retornando um novo vetor.       *
 * (04) Palavras chave:                                                   *
 *      vetor                                                             *
 *      float                                                             *
 *      multiplicacao                                                     *
 *      constante                                                         *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, abril de 1989.                              *
 *      Antonio Carlos Salgado Guimaraes & Raul A. Feijoo.                *
 * (07) Argumentos:                                                       *
 *      <- *alfav - vetor resultante           - flaot huge               *
 *      -> alfa   - valor que ira' multiplicar - double                   *
 *      -> *v     - vetor original             - float huge               *
 *      -> n      - tamanho dos vetores        - long                     *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      trc_on, trc_off, error                                            *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void rpvetf(float huge *alfav, double alfa, float huge *v,        *
 *                  long n);                                              *
 * (11) Headers necessarios:                                              *
 *      stdio.h, util.h, matematc.h                                       *
 * (12) Mensagens de erro:                                                *
 *      .Numero 1:                                                        *
 *       Mensagem: Dimensao negativa ou nula.                             *
 *       Causa   : Dimensao negativa ou nula.                             *
 *       Correcao: Corrigir a dimensao.                                   *
 *       Efeito  : O programa e' interrompido.                            *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void rpvetf(float huge *alfav, double alfa, float huge *v, long n)
    {
    static char *nome = "rpvetf";
    float alfaaux = (float) alfa;

    TraceOn(nome);

    if (n <= 0)
        Error(2, 1, "Dimensao negativa ou nula.");

    while (n--)
        *alfav++ = *v++ * alfaaux;

    TraceOff(nome);
    }
