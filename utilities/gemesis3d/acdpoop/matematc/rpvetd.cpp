/*
 **************************************************************************
 *                                                                        *
 * void rpvetd                                                            *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Multiplicar um vetor por um escalar.                              *
 * (02) Forma de utilizacao:                                              *
 *      rpvetd(sv, v, s, n);                                              *
 * (03) Descricao:                                                        *
 *      Multiplica um vetor  'v' do tipo double huge de dimensao 'n'      *
 *      por um escalar 's' e coloca o resultado no vetor 'sv'.            *
 *      Ou seja:  sv(i) = s * v(i)                                        *
 * (04) Palavras chave:                                                   *
 *      produto                                                           *
 *      escalar                                                           *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 13 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *sv  -  pointer para vetor que contem resultado do produto     *
 *                                                     -  double huge     *
 *      -> *v   -  pointer para vetor que sera multiplicado               *
 *                                                     -  double huge     *
 *      -> s    -  escalar que ira multiplicar vetor - double             *
 *      -> n    -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void rpvetd(double huge *sv, double huge *v, double s, long n);   *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void rpvetd(double huge *sv, double huge *v, double s, long n)
    {
    while (n--)
        *sv++ = s * (*v++);
    }

