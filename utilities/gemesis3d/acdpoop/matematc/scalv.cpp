/*
 **************************************************************************
 *                                                                        *
 * double scalv                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Calcular o produto escalar entre dois vetores .                   *
 * (02) Forma de utilizacao:                                              *
 *      produto = scalv(x, y, tam);                                       *
 * (03) Descricao:                                                        *
 *      Calcula o produto escalar entre dois vetores do tipo double huge, *
 *      de dimensao 'tam'.                                                *
 * (04) Palavras chave:                                                   *
 *      produto                                                           *
 *      escalar                                                           *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 13 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      -> *x   -  pointer para vetor operando - double huge              *
 *      -> *y   -  pointer para vetor operando - double huge              *
 *      -> tam  -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      double scalv(double huge *x, double huge *y, long tam);           *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

double scalv(double huge *x, double huge *y, long tam)
    {
    double aux = 0.0;
    while (tam--)
        aux += ((*x++) * (*y++));
    return (aux);
    }

