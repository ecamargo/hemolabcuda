/*
 *****************************************************************************
 *                                                                           *
 * void solgasim(vetx, mas, vetb, neq)                                       *
 *                                                                           *
 * (01) Objetivo:                                                            *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-     *
 *      cao de Gauss.                                                        *
 * (02) Forma de utilizacao:                                                 *
 *      solgasim(vetx, mas, vetb, neq);                                      *
 * (03) Descricao:                                                           *
 *      Resolve um sistema de equacoes lineares pelo Metodo de Eliminacao    *
 *      de Gauss realizando a modificacao dos termos independentes de acordo *
 *      com o resultado obtido na rotina mgausimt e a etapa de 'back         *
 *      substitution'. Esta rotina permite ao usuario resolver um sistema    *
 *      com multiplos vetores de carga. A matriz do sistema e' do tipo       *
 *      simetrica armazenada na forma supradiagonal por linhas e e' fornecida*
 *      triangularizada pela rotina mgausimt.                                *
 * (04) Palavras chave:                                                      *
 *      equacoes                                                             *
 *      simetrica                                                            *
 *      direto                                                               *
 *      solvedor                                                             *
 * (05) Referencia:                                                          *
 *      ---                                                                  *
 * (06) Data e autor:                                                        *
 *      Rio de Janeiro, LNCC, marco de 1990,                                 *
 *      Roberto Eduardo Garcia.                                              *
 * (07) Argumentos:                                                          *
 *      <- *vetx -  pointer para vetor resposta             - double huge    *
 *      -> *mas  -  pointer para matriz do sistema          - double huge    *
 *      -> *vetb -  pointer para vetor termos independentes - double huge    *
 *      -> neq   -  numero de equacoes                      - long           *
 * (08) Variaveis externas utilizadas:                                       *
 *      ---                                                                  *
 * (09) Chamadas:                                                            *
 *      ---                                                                  *
 * (10) Prototype:                                                           *
 *      solvequ.h                                                            *
 *      void solgasim(double huge *vetx, double huge *mas, double huge *vetb,*
 *                    long int neq);                                         *
 * (11) Headers necessarios:                                                 *
 *      solvequ.h                                                            *
 *      matematc.h                                                           *
 * (12) Mensagens de erro:                                                   *
 *      ---                                                                  *
 * (13) Depuracao:                                                           *
 *      ---                                                                  *
 * (14) Portabilidade:                                                       *
 *      C standard.                                                          *
 * (15) Acesso:                                                              *
 *      Livre ao usuario.                                                    *
 *                                                                           *
 *****************************************************************************
*/

#include "matematc.h"

/*
** Metodo de Eliminacao de Gauss
** Esta rotina realiza a modificacao dos termos independentes e a etapa
** de 'back substitution'.
*/

void solgasim(double huge *vetx, double huge *mas, double huge *vetb,
              long int neq)
    {
    double huge *pinmas,   /* pointer que guarda o endereco inicial de 'mas' */
           huge *akk,      /* pointer que varre a diagonal principal de 'mas' */
           huge *temp,     /* pointer auxiliar utilizado nas etapas de 'partial
                              pivoting' e 'forward elimination' */
           huge *ptlb,
           huge *pcfin,    /* pointers para elementos do vetor 'vetb' */
           huge *plix;     /* pointer para vetor resposta 'vetx' */
    double mik,            /* Aik / Akk */
           paux;           /* variavel auxiliar utilizada na etapa de 'back
                              substitution' */
    register long int i,
                      j,   /* variaveis utlizadas como contadores */
                      k;   /* etapa dos processos de pivoteamanto e eliminacao */
    long int nelem,        /* numero de elementos da matriz do sistema */
             neqm1,        /* neq - 1 */
             pulomas,      /* numero de posicoes que o pointer mas deve avancar */
             pulotemp,     /* numero de posicoes que o pointer temp deve avancar */
             pulodg;       /* numero de posicoes que o pointer akk deve retroceder */

    /*
    ** inicializa pointers e constantes
    */

    akk   = pinmas = mas;
    neqm1 = neq - 1;
    nelem = ((neq + 1) * neq)/2;
    pulomas = neq;

    /*
    ** etapa de 'forward elimination' do vetor
    ** termos independentes
    */

    for (k = 1; k < neq; k++)
        {
        ptlb  = vetb + k;
        pcfin = vetb + (k - 1);
        for (i = k, pulotemp = 1; i < neq; i++, pulotemp++)
            {
            temp = akk + pulotemp;
            mik = *temp / *akk;
            *ptlb = *ptlb - mik * (*pcfin);
            ptlb++;
            }
        akk += pulomas;
        pulomas--;
        }

    /*
    ** etapa de 'back substitution'
    */

    akk    = pinmas + (nelem - 1);
    pulodg = 1;
    for (i = neqm1; i >= 0 ; i--)
        {
        plix  = vetx + i + 1;
        mas   = akk + 1;
        paux  = 0.0;
        for (j = i + 1; j < neq; j++)
            paux += (*mas++) * (*plix++);
        *(vetx + i) = ((*(vetb + i)) - paux)/(*akk);
        pulodg++;
        akk -= pulodg;
        }
    }
