/*
 **************************************************************************
 *                                                                        *
 * void solgssk(vetx, ma, maxa, vetb, neq)                                *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Resolver um sistema de equacoes lineares pelo Metodo de Elimina-  *
 *      cao de Gauss.                                                     *
 * (02) Forma de utilizacao:                                              *
 *      solgssk(vetx, ma, maxa, vetb, neq);                               *
 * (03) Descricao:                                                        *
 *      Resolve um sistema de equacoes lineares com multiplos vetores de  *
 *      termos independentes pelo Metodo de Eliminacao                    *
 *      de Gauss. A matriz do sistema e' simetrica escalonada por colunas *
 *      ascendentes. O armazenamento desta matriz se da por meio dos veto-*
 *      res 'ma' e 'maxa'. Esta rotina realiza as modificacoes nos termos *
 *      independentes e executa a etapa de 'back substitution'.           *
 * (04) Palavras chave:                                                   *
 *      equacoes                                                          *
 *      simetrica                                                         *
 *      skyline                                                           *
 *      direto                                                            *
 *      solvedor                                                          *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, maio de 1990,                               *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      <- *vetx -  pointer para vetor resposta             - double huge *
 *      -> *ma   -  pointer para o vetor que contem os elementos          *
 *                  da matriz do sistema                    - double huge *
 *      -> *maxa -  pointer para vetor que contem as posicoes dos         *
 *                  elementos da diagonal principal         - long huge   *
 *      -> *vetb -  pointer para vetor termos independentes - double huge *
 *      -> neq   -  numero de equacoes                      - long        *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      solvequ.h                                                         *
 *      void solgssk(double huge *vetx, double huge *ma, long huge *maxa, *
 *                   double huge *vetb, long int neq);                    *
 * (11) Headers necessarios:                                              *
 *      solvequ.h                                                         *
 *      matematc.h                                                        *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

/*
** Metodo de Eliminacao de Gauss
** Rotina que modifica os termos independentes
** e realiza a etapa de 'back substitution'
*/

void solgssk(double huge *vetx, double huge *ma, long huge *maxa,
             double huge *vetb, long neq)
    {
    double huge *pdiagi,
           huge *pdiagj,   /* pointer para elementos da diagonal principal */
           huge *pcfin,
           huge *plinb,    /* pointers auxiliares para elementos do 'vetb' */
           huge *plix,     /* pointer para elementos do vetor resposta */
           huge *aij;      /* pointer para elementos da matriz do sistema */
    double mik,            /* variavel que recebe a razao Aik / Akk */
           akk,            /* variavel que recebe elemento da diagonal principal */
           aik,            /* variavel que recebe elemento Aik */
           paux;           /* variavel auxiliar que recebe produtos temporarios
                              na etapa de 'back substitution' */
    register long int k,   /* etapa do processo de 'forward elimination' */
                      i,
                      j;   /* variaveis utlizadas como contadores */
    long int neqm1,        /* neq - 1 */
             poseldg,      /* posicao do elemento da diagonal principal */
             hcoli,
             hcolj,        /* altura da coluna */
             diagi,
             diagj,        /* guardam a posicao do elemento da diagonal principal */
             incptr;       /* variavel auxiliar utilizada na etapa de
                              'back substitution' */

    /*
    ** inicializa constante
    */

    neqm1 = neq - 1;

    /*
    ** Etapa de 'forward elimination'
    */

    for (k = 0; k < neqm1; k++)
        {
        poseldg = *(maxa + k);
        akk     = *(ma + poseldg);
        pcfin = vetb + k;
        plinb = pcfin + 1;
        for (i = k + 1; i < neq; i++)
            {
            hcoli = *(maxa + i + 1) - *(maxa + i);
            if (i - hcoli < k)
                {
                diagi  = *(maxa + i);
                pdiagi = ma + diagi;
                aik = *(pdiagi + (i - k));
                mik = aik/akk;
                *plinb = *plinb - mik * (*pcfin);
                plinb++;
                }
            else plinb++;
            }
        }

    /*
    ** Etapa de 'back substitution'
    */

    for (i = neqm1; i >= 0; i--)
        {
        poseldg = *(maxa + i);
        akk = *(ma + poseldg);
        plix = (vetx + i) + 1;
        paux = 0.0;
        for (j = i + 1, incptr = 1; j < neq; j++, incptr++)
            {
            diagj = *(maxa + j);
            pdiagj = ma + diagj;
            hcolj = *(maxa + j + 1) - diagj;
            if (j - hcolj < i)
                {
                aij = pdiagj + incptr;
                paux += (*aij) * (*plix);
                plix++;
                }
            else plix++;
            }
        *(vetx + i) = (*(vetb + i) - paux)/akk;
        }
    }


