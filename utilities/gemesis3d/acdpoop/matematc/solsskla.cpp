#include "matematc.h"

/*
* SolSymSkyLAs  - February 1994.
** SOLves SYMmetric Linear Sistems stored in SKY-LINE ascending form.
**
** Performs solution of a linear system with coefficients
** stored in ascending sky-line form.
** This function decomposes the matrix K=ma of the system in LDLt and
** find the solution associated to any vector F.
** The solution is given in vector U and the vector F is modified
** during de decomposition process.
** Return 0 if there were no problems. In this case det contains
**          the value of the matrix determinant.
** Return 1 if any diagonal term is less than epsilon. In this case
**          eldiag contains the number of the corresponding equation.
*/

  
int SolSymSkyLAs(double huge* ma, long huge* maxa, long neq,
                 int FlagSol,     long &eldiag,    double huge* F,
                 double huge* U,  double eps)
    {
    long s, ios, i, ioi, firsteq, len, j, m;
    double huge* vectI;
    double huge* vectS;
    double sum;
    long huge* vmaxa;
    long huge* pmaxas;
    long huge* pmaxasp1;
    long huge* pmaxai;
    long huge* pmaxaip1;
    long smi;

    if (FlagSol == 0)
        {
        /*
        ** Matrix descomposition
        */
        pmaxas  = &maxa[1];
        pmaxasp1 = &maxa[2];
        for(s = 1; s < neq; s++, pmaxas++, pmaxasp1++)
            {
            // equation associated to the top of colum s
            ios = s - (*pmaxasp1 - *pmaxas - 1);
            pmaxai   = &maxa[ios + 1];
            pmaxaip1 = &maxa[ios + 2];
            for(i = ios + 1; i < s; i++, pmaxai++, pmaxaip1++)
                {
                // equation associated to the top of colum i
                ioi = i -  (*pmaxaip1 - *pmaxai - 1);
                // first common equation for colums i and s
                // with non-zero coefficients 
                firsteq = (ioi > ios) ? ioi : ios;
                len   = i - firsteq;
                //if (len > 0)
                    {
                    // ptr for ith-column
                    vectI = &ma[*pmaxai + 1];
                    // ptr for the element in sth-column corressponding to
                    // the (i-1)th- equation
                    smi = s - i;
                    vectS = &ma[*pmaxas + smi + 1];
                    sum   = scalv(vectI, vectS, len);
                    ma[*pmaxas + smi] -=sum;
                    }
                }
            sum = 0.0;
            vectS = &ma[*pmaxas + (s - ios)];
            vmaxa = &maxa[ios];
            for(m = ios; m < s; m++, vectS--, vmaxa++)
                {
                sum    += (*vectS) * (*vectS)/ (ma[*vmaxa]);
                *vectS /= (ma[*vmaxa]);
                }

            ma[*pmaxas] -= sum;

            // is the matrix positive definite?
            if (ma[*pmaxas] <= eps)
                {
                eldiag = s;
                return(0);
                }
            }
        return(1);

        }
    else if (FlagSol == 1)
        {
        /*
        ** Performs the solution of the lower system
        */
        for(i = 1; i < neq; i++)
            {
            // hight of ith-column
            len   = maxa[i + 1] - maxa[i] - 1;
            vectI = &ma[maxa[i] + 1];
            vectS = &F[i - 1];
            sum = 0.0;
            for(m = 0; m< len; m++, vectI++, vectS--)
                sum += *vectI * *vectS;  
            F[i] -= sum;
            }
        /*
        ** Performs the solution of the upper  system
        */
        U[neq - 1] = F[neq - 1] / ma[maxa[neq - 1]];
        for(i = neq - 2; i >= 0; i--)         
            {
            s = i + 1;
            ios   = s - (maxa[s + 1] - maxa[s] - 1);
            for(j = ios; j < s; j++)
                F[j] -= U[s] * ma[maxa[s] + (s - j)] * ma[maxa[j]];
            U[i] = F[i] / ma[maxa[i]];
            }
        return(1);
        }
    return(0);
    }
