/*
 **************************************************************************
 *                                                                        *
 * void subtvet                                                           *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Subtrair dois vetores a e b, armazenando a diferenca em c.        *
 * (02) Forma de utilizacao:                                              *
 *      subtvet(c, a, b, n);                                              *
 * (03) Descricao:                                                        *
 *      Subtrai dois vetores do tipo double huge, de dimensao 'n',        *
 *      armazenados nas tabelas 'a' e 'b', sendo a diferenca guardada     *
 *      em 'c' , ou seja:    c = a - b                                    *
 * (04) Palavras chave:                                                   *
 *      auxiliares                                                        *
 *      subtracao                                                         *
 *      vetores                                                           *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, 13 de julho de 1989,                        *
 *      Antonio Carlos Salgado Guimaraes e Adriana Balleste'              *
 * (07) Argumentos:                                                       *
 *      <- *c   -  pointer para vetor que contem resultado - double huge  *
 *      -> *a   -  pointer para vetor operando - double huge              *
 *      -> *b   -  pointer para vetor operando - double huge              *
 *      -> n    -  dimensao do vetor - long                               *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void subtvet(double huge *c, double huge *a, double huge *b,      *
 *                long n);                                                *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Rotina livre ao usuario.                                          *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void subtvet(double huge *c, double huge *a, double huge *b, long n)
    {
    while (n--)
        *c++ = (*a++) - (*b++);
    }
