/*
 **************************************************************************
 *                                                                        *
 * void swapi                                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Trocar inteiros.                                                  *
 * (02) Forma de utilizacao:                                              *
 *      swapi(&i, &j);                                                    *
 * (03) Descricao:                                                        *
 *      Troca os valores de duas variaveis inteiras.                      *
 * (04) Palavras chave:                                                   *
 *      trocar                                                            *
 *      inteiros                                                          *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, abril de 1989.                              *
 *      Antonio Carlos Salgado Guimaraes                                  *
 * (07) Argumentos:                                                       *
 *      <> *i - variavel a ser trocada por 'j' - int                      *
 *      <> *j - variavel a ser trocada por 'i' - int                      *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 *                                                                        *
 **************************************************************************
*/


#include "matematc.h"

void swapi(int *i, int *j)
    {
    register int temp;

    temp = *i;
    *i = *j;
    *j = temp;
    }
