/*
 **************************************************************************
 *                                                                        *
 * void tranmat(mat, ma, nlin, ncol)                                      *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Fornecer a transposta de uma matriz do tipo double armazenada     *
 *      por linhas.                                                       *
 * (02) Forma de utilizacao:                                              *
 *      tranmat(mat, ma, nlin, ncol);                                     *
 * (03) Descricao:                                                        *
 *      Recebe uma matriz (ma) do tipo double armazenada por linhas       *
 *      e fornece a sua matriz transposta (mat).                          *
 * (04) Palavras chave:                                                   *
 *      matriz                                                            *
 *      transposta                                                        *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de Janeiro, LNCC, setembro de 1988,                           *
 *      Roberto Eduardo Garcia.                                           *
 * (07) Argumentos:                                                       *
 *      -> *ma   - pointer para a matriz 'ma'              - double huge  *
 *      <- *mat  - pointer para a matriz transposta 'mat'  - double huge  *
 *      -> nlin  - numero de linhas da matriz 'ma'         - long         *
 *      -> ncol  - numero de colunas da matriz 'ma'        - long         *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      ---                                                               *
 * (10) Prototype:                                                        *
 *      matematc.h                                                        *
 *      void tranmat(double huge *mat, double huge *ma, long int nlin,    *
 *                        long int ncol);                                 *
 * (11) Headers necessarios:                                              *
 *      stdio.h, matematc.h                                               *
 * (12) Mensagens de erro:                                                *
 *      ---                                                               *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario.                                                 *
 **************************************************************************
*/

#include "matematc.h"

/*
** Rotina que calcula a matriz transposta
*/

void tranmat(double huge *mat, double huge *ma, long nlin, long ncol)
    {
    double huge *aux;         /* pointer que varre a matriz de entrada */
    register long int i;      /* variavel utilizada como contador */
    long int j = ncol;        /* variavel utilizada para armazenar ncol */

    while (ncol--)
         {
         aux = ma++;
         i = nlin;
         while (i--)
              {
              *mat++ = *aux;
              aux += j;
              }
         }
    }
