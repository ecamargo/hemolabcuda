/**************************************************************************
 *                                                                        *
 * void vetaddf(v, v1, v2, n)                                             *
 *                                                                        *
 * (01) Objetivo:                                                         *
 *      Adicionar dois vetores n-dimensionais 'v1' e 'v2', armazenando    *
 *      sua soma em 'v'. Os vetores sao do tipo float.                    *
 * (02) Forma de utilizacao:                                              *
 *      void vetaddf(v, v1, v2, n);                                       *
 * (03) Descricao:                                                        *
 *      Esta rotina adiciona dois vetores reais float, de dimensao 'n',   *
 *      armazenando e retornando sua soma em v.                           *
 *      v = v1 + v2                                                       *
 * (04) Palavras chave:                                                   *
 *      vetor                                                             *
 *      soma                                                              *
 *      float                                                             *
 * (05) Referencia:                                                       *
 *      ---                                                               *
 * (06) Data e autor:                                                     *
 *      Rio de janeiro, lncc, julho de 1988,                              *
 *      Eduardo De Carolis                                                *
 * (07) Argumentos:                                                       *
 *      <- *v    - vetor resultado                 - float                *
 *      -> *v1   - pointer para o primeiro vetor   - float                *
 *      -> *v2   - pointer para o segundo vetor    - float                *
 *      -> n     - dimensao dos vetores            - long int             *
 * (08) Variaveis externas utilizadas:                                    *
 *      ---                                                               *
 * (09) Chamadas:                                                         *
 *      error, trc_on, trc_off                                            *
 * (10) Prototype:                                                        *
 *      matematec.h                                                       *
 *      void vetaddf(float* v, float *v1, float *v2, long n);             *
 * (11) Headers necessarios:                                              *
 *      stdio.h, util.h, matematec.h                                      *
 * (12) Mensagens de erro:                                                *
 *      . Numero 1:                                                       *
 *        Mensagem: Dimensao dos vetores negativa ou nula.                *
 *        Causa   : n <= 0                                                *
 *        Correcao: Verifique o parametro n                               *
 *        efeito  : O programa e interrompido                             *
 * (13) Depuracao:                                                        *
 *      ---                                                               *
 * (14) Portabilidade:                                                    *
 *      C standard.                                                       *
 * (15) Acesso:                                                           *
 *      Livre ao usuario                                                  *
 *                                                                        *
 **************************************************************************
*/

#include "matematc.h"

void vetaddf(float huge *v, float huge *v1, float huge *v2, long n)
    {
    static char *nome = "vetaddf";

    TraceOn(nome);

    if (n <= 0)
        Error(2, 1, "Dimensao dos vetores negativa ou nula.");

    while (n--)
        *v++ = *v1++ + *v2++;

    TraceOff(nome);
    }
