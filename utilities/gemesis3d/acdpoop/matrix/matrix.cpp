#include "matrix.h"

// aloca memoria para a matriz
double huge *AllocMatrix(long l, long c)
    {
    double huge *aux;
    char buf[30];
    long t = l * c;
    aux = (double huge *) mMalloc(sizeof(double) * t);
    if (!aux)
        {
        // 1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }
    return(aux);
    }

// testa dimensao da matriz
void MatrixDimTest(long l, long c)
    {
    char buf[20];
    if (l < 0 || c < 0)
       {
       // 33, "Negative Size."
       GetError(33, buf);
       Error(FATAL_ERROR, 2, buf);
       }
    }

// verifica se um objeto existe
void acMatrix::ObjectExists()
    {
    char buf[81];
    if (Rows)
        { //  22, "An Object Of Class |%s| Exists And Will Be Eliminated."
        GetError(22, buf);
        Error(WARNING, 3, buf, "acMatrix");
        Free();
        }
    }

// verifica se as matrizes tem o mesmo tamanho
void EqualDim(acMatrix &m1, acMatrix &m2)
    {
    char buf[50];

    if ((m1.Rows != m2.Rows) || m1.Cols != m2.Cols)
        {
        // 35, "Matrices With Different Sizes."
        GetError(35, buf);
        Error(FATAL_ERROR, 4, buf);
        }
    }


// construtores
acMatrix::acMatrix(long l, long c)
    {
    char *nome = "acMatrix::acMatrix";

    TraceOn(nome);

    MatrixDimTest(l, c);
    Rows = l;
    Cols = c;
    M    = AllocMatrix(l, c);
     
    TraceOff(nome);
    }

acMatrix::acMatrix(acMatrix& x)
    {
    char *nome = "acMatrix::acMatrix";
    TraceOn(nome);

    Rows = x.Rows;
    Cols = x.Cols;
    M    = AllocMatrix(Rows, Cols);

    dcopyv(M, x.M, Rows * Cols);

    TraceOff(nome);
    }


// Libera area
void acMatrix::Free()
    {
    if (Rows || Cols)
        {
        mFree(M);
        Rows = Cols = 0;
        }
    }

