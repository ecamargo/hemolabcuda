#include "matrix.h"

// -m;
acMatrix& acMatrix::operator-()
    {
    chsvet(M, Rows * Cols);
    return *this;
    }

// m.Chs;
acMatrix& acMatrix::Chs()
    {
    chsvet(M, Rows * Cols);
    return *this;
    }

