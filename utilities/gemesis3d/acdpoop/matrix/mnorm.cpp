#include "matrix.h"
#include <math.h>

double acMatrix::InfinityNorm()
    {
    long i, j;
    double soma;
    double maior = 0.0;
    double huge *v = M;
    
    for (i = 0; i < Rows; i++)
        {
        soma = 0.0;
        for (j = 0; j <  Cols; j++)
            soma += fabs(*v++);
        if (soma > maior)
            maior = soma;
        }
        
    return(maior);
    }


double acMatrix::OneNorm()
    {
    long i, j;
    double soma;
    double maior = 0.0;
    double huge *v;
    double huge *aux = M;
    
    for (i = 0; i < Cols; i++, aux++)
        {
        v = aux;
        soma = 0.0;
        for (j = 0; j < Rows; j++, v += Cols)
            soma += fabs(*v++);
        if (soma > maior)
            maior = soma;
        }
        
    return(maior);
    }


