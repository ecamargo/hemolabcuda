#include "matrix.h"

acMatrix& acMatrix::Equal(acMatrix& x)
    {
    Free();
    Rows = x.Rows;
    Cols = x.Cols;
    M    = AllocMatrix(Rows, Cols);

    dcopyv(M, x.M, Rows * Cols);

    return(*this);
    }


acMatrix& acMatrix::operator=(acMatrix& x)
    {
    Free();
    Rows = x.Rows;
    Cols = x.Cols;
    M    = AllocMatrix(Rows, Cols);

    dcopyv(M, x.M, Rows * Cols);

    return(*this);
    }
