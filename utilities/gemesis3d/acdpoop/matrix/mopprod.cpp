#include "matrix.h"
#include "symmat.h"

// m *= 1.5;
acMatrix& acMatrix::operator*=(double r)
    {
    long i;
    long t = Rows * Cols;
    double huge *p = M;

    for (i = 0; i < t; i++, p++)
        *p *= r;

    return *this;
    }

// m.Prod(1.5);
acMatrix& acMatrix::Prod(double r)
    {
    long i;
    long t = Rows * Cols;
    double huge *p = M;

    for (i = 0; i < t; i++, p++)
        *p *= r;

    return *this;
    }

// m = m1 * m2;
acMatrix operator*(acMatrix& m1, acMatrix& m2)
    {
    static char *nome = "operator*";
    char buf[30];
    TraceOn(nome);

    if (m1.Cols != m2.Rows)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    acMatrix m(m1.Rows, m2.Cols);

    prodmat(m.M, m1.M, m2.M, m1.Rows, m1.Cols, m2.Cols);

    TraceOff(nome);
    return(m);        
    }

// m.Prod(m1, m2);
acMatrix& acMatrix::Prod(acMatrix& m1, acMatrix& m2)
    {
    static char *nome = "acMatrix::Prod";
    char buf[30];
    TraceOn(nome);

    if (m1.Cols != m2.Rows)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    Free();
    Rows = m1.Rows;
    Cols = m2.Cols;
    M    = AllocMatrix(Rows, Cols);

    prodmat(M, m1.M, m2.M, m1.Rows, m1.Cols, m2.Cols);

    TraceOff(nome);
    return(*this);
    }


// m = m1 * m2 (m2 e' uma matriz simetrica)
acMatrix operator*(acMatrix& m1, acSymMatrix& m2)
    {
    double huge *pm;
    double huge *pm1, huge *pm1a;
    double huge *pm2, huge *pm2a;
    long i, j, k, inc, inca;
    long RowsSym, ColsSym;
     
    static char *nome = "operator*";
    char buf[30];
    TraceOn(nome);

    RowsSym = m2.GetRows();
    ColsSym = RowsSym;
    if (m1.Cols != RowsSym)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    acMatrix m(m1.Rows, ColsSym);

    pm  = m.M;                          // ptr para o elemento m(i,j)
    pm1 = m1.M;
    inc = ColsSym - 1;

    for (i = 0; i < m1.Rows; i++)       // loop s/filas de m1
        {
        pm2 = m2.GetSymMatrixPtr();
        inca = inc;
        for (j = 0; j < ColsSym; j++)   // loop s/colunas de m2
            {
            pm1a = pm1;                 // ptr para fila   i de m1
            pm2a = pm2++;               // ptr para coluna j da mat. sym. m2

            // contribuicao parte supradiagonal da coluna j de m2
            for (k = 0; k < j; k++)
                {
                *pm += *pm1a * *pm2a;
                pm1a++;
                pm2a += inca--;
                }
            // Contribuicao parte sub diagonal da coluna j de m2
            *pm++ = scalv(pm1a, pm2a, ColsSym - j);
            }
        pm1 = pm1a;
        }

    TraceOff(nome);
    return(m);        
    }

// m.Prod(m1, m2) m2 e'uma matriz simetrica
acMatrix& acMatrix::Prod(acMatrix& m1, acSymMatrix& m2)
    {
    double huge *pm;
    double huge *pm1, huge *pm1a;
    double huge *pm2, huge *pm2a;
    long i, j, k, inc, inca;
    long RowsSym, ColsSym;

    static char *nome = "acMatrix::Prod";
    char buf[30];
    TraceOn(nome);

    RowsSym = m2.GetRows();
    ColsSym = RowsSym;
    if (m1.Cols != RowsSym)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    Free();
    M    = AllocMatrix(Rows, ColsSym);

    pm  = M;                            // ptr para o elemento m(i,j)
    pm1 = m1.M;
    inc = ColsSym - 1;

    for (i = 0; i < m1.Rows; i++)          // loop s/filas de m1
        {
        pm2 = m2.GetSymMatrixPtr();
        inca = inc;
        for (j = 0; j < ColsSym; j++)   // loop s/colunas de m2
            {
            pm1a = pm1;                 // ptr para fila i de m1
            pm2a = pm2++;               // ptr para coluna j da mat. sym. m2

            // contribuicao parte supradiagonal da coluna j de m2
            for (k = 0; k < j; k++)
                {
                *pm += *pm1a * *pm2a;
                pm1a++;
                pm2a += inca--;
                }
            // Contribuicao parte sub diagonal da coluna j de m2
            *pm++ = scalv(pm1a, pm2a, ColsSym - j);
            }
        pm1 = pm1a;
        }
    TraceOff(nome);
    return(*this);
    }
        
// m = m1 * m2 (m1 e' uma matriz simetrica)
acMatrix operator*(acSymMatrix& m1, acMatrix& m2)
    {
    double huge *pm;
    double huge *pm1, huge *pm1a;
    double huge *pm2, huge *pm2a;
    long i, j, k, inc, inca;
    long ColsSym, RowsSym, cols2;
     
    static char *nome = "operator*";
    char buf[30];
    TraceOn(nome);

    RowsSym = m1.GetRows();
    ColsSym = RowsSym;
    inc     = ColsSym - 1;

    cols2   = m2.Cols;

    if (ColsSym != m2.Rows)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    acMatrix m(RowsSym, cols2);

    pm  = m.M;                       // ptr para o elemento m(i,j)
    pm1 = m1.GetSymMatrixPtr();      // pter para fila 0 da mat. sym. m1

    for (i = 0; i < RowsSym; i++)    // loop s/filas de m1
        {
        pm2 = m2.M;                  // pointer para a col. 0 de m2

        for (j = 0; j < cols2; j++)  // loop s/ colunas de m2
            {
            pm1a = pm1;              // ptr para fila i da matriz sym. m1
            pm2a = pm2++;            // pter para coluna j da mat.  m2
            inca = inc;

            // contribuicao parte sub diagonal da fila i de m1
            for (k = 0; k < i; k++)
                {
                *pm += *pm1a * *pm2a;
                pm2a += cols2;
                pm1a += inca--;
                }
            // Contribuicao parte supra diagonal da fila i de m1
            for (k = i; k < ColsSym; k++)
                {
                *pm = *pm1a * *pm2a;
                pm2a += cols2;
                pm1a++;
                }
            pm++;
            }
        pm1++;
        }

    TraceOff(nome);
    return(m);        
    }

// m.Prod(m1, m2) m1 e'uma matriz simetrica
acMatrix& acMatrix::Prod(acSymMatrix& m1, acMatrix& m2)
    {
    double huge *pm;
    double huge *pm1, huge *pm1a;
    double huge *pm2, huge *pm2a;
    long i, j, k, inc, inca;
    long ColsSym, RowsSym, cols2;
     
    static char *nome = "acMatrix::Prod";
    char buf[30];
    TraceOn(nome);

    RowsSym = m1.GetRows();
    ColsSym = RowsSym;
    inc     = ColsSym - 1;

    cols2   = m2.Cols;

    if (ColsSym != m2.Rows)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    Free();
    M    = AllocMatrix(RowsSym, cols2);


    pm  = M;                        // ptr para o elemento m(i,j)
    pm1 = m1.GetSymMatrixPtr();     // ptr para fila 0 da mat. sym. m1

    for (i = 0; i < RowsSym; i++)   // loop s/filas de m1
        {
        pm2 = m2.M;                 // ptr para a col. 0 de m2

        for (j = 0; j < cols2; j++) // loop s/colunas de m2
            {
            pm1a = pm1;             // ptr para fila i da matriz sym. m1
            pm2a = pm2++;           // ptr para coluna j da mat.  m2
            inca = inc;

            // contribuicao parte sub diagonal da fila i de m1
            for (k = 0; k < i; k++)
                {
                *pm += *pm1a * *pm2a;
                pm2a += cols2;
                pm1a += inca--;
                }
            // Contribuicao parte supra diagonal da fila i de m1
            for (k = i; k < ColsSym; k++)
                {
                *pm = *pm1a * *pm2a;
                pm2a += cols2;
                pm1a++;
                }
            pm++;
            }
        pm1++;
        }

    TraceOff(nome);
    return(*this);
    }

        
// m = m1 * m2 (m1 e me sao simetricas)
acMatrix operator*(acSymMatrix& m1, acSymMatrix& m2)
    {
    double huge *pm;
    long i, j, k;
    long Cols1, Rows1, Cols2, Rows2;
     
    static char *nome = "operator*";
    char buf[30];
    TraceOn(nome);

    Rows1 = m1.GetRows();
    Cols1 = Rows1;

    Rows2 = m2.GetRows();
    Cols2 = Rows2;

    if (Cols1 != Rows2)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    acMatrix m(Rows1, Cols2);
    pm  = m.M;                             // ptr para o elemento m(i,j)

    for (i = 0; i < Rows1; i++)
        for (j = 0; j < Cols2; j++, pm++)
            for (k = 0; k < Cols1; k++)
                *pm = m1(i,k) * m2(k,j);   // m(i,j) = m1(i,k) * m2(k,j)
            
    TraceOff(nome);
    return(m);        
    }



// m.Prod(m1, m2) m1 e m2 sao simetricas
acMatrix& acMatrix::Prod(acSymMatrix& m1, acSymMatrix& m2)
    {
    double huge *pm;
    long i, j, k;
    long Cols1, Rows1, Cols2, Rows2;
     
    static char *nome = "acMatrix::Prod";
    char buf[30];
    TraceOn(nome);

    Rows1 = m1.GetRows();
    Cols1 = Rows1;

    Rows2 = m2.GetRows();
    Cols2 = Rows2;


    if (Cols1 != Rows2)
        {
        // 36, "Can't Multiply Matrices.
        GetError(36, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    Free();
    M    = AllocMatrix(Rows1, Cols2);
    pm  = M;                                  // ptr para o elemento m(i,j)

    for (i = 0; i < Rows1; i++)              
        for (j = 0; j < Cols2; j++, pm++)     
            for (k = 0; k < Cols1; k++)       
                *pm = m1(i,k) * m2(k,j);      // m(i,j) = m1(i,k) * m2(k,j)

    TraceOff(nome);
    return(*this);
    }
