#include "matrix.h"

// c = a - b;
acMatrix operator-(acMatrix& m2, acMatrix& m3)
    {
    char *nome = "operator-";
    long t;
    TraceOn(nome);  

    EqualDim(m2, m3);

    acMatrix v1(m2.Rows, m2.Cols);
    t = v1.Rows * v1.Cols;
    subtvet(v1.M, m2.M, m3.M, t);

    TraceOff(nome);
    return(v1);
    }

// c.Subt(a, b);
acMatrix& acMatrix::Subt(acMatrix& m2, acMatrix& m3)
    {
    char *nome = "acMatrix::Subt";
    TraceOn(nome);

    EqualDim(m2, m3);

    Free();
    Rows = m2.Rows;
    Cols = m2.Cols;
    M    = AllocMatrix(Rows, Cols);

    subtvet(M, m2.M, m3.M, Rows * Cols);

    TraceOff(nome);
    return(*this);
    }

// a -= b;
acMatrix& acMatrix::operator-=(acMatrix& m2)
    {
    char *nome = "acMatrix::operator-=";
    register long i, t;
    TraceOn(nome);

    EqualDim(*this, m2);
    double huge *p1 = M,
           huge *p2 = m2.M;

    t = Rows * Cols;
    for (i = 0; i < t; i++, p1++, p2++)
        *p1 -= *p2;

    TraceOff(nome);
    return *this;
    }

// a.Subt(b);
acMatrix& acMatrix::Subt(acMatrix& m2)
    {
    char *nome = "acMatrix::Subt";
    register long i, t;
    TraceOn(nome);

    EqualDim(*this, m2);
    double huge *p1 = M,
           huge *p2 = m2.M;

    t = Rows * Cols;
    for (i = 0; i < t; i++, p1++, p2++)
        *p1 -= *p2;

    TraceOff(nome);
    return *this;
    }
    
