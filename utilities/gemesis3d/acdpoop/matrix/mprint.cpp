#include "matrix.h"

void acMatrix::Print(FILE *fp, char *tit, char *fmt)
    {
    char *format;

    format = (fmt) ? fmt : (char *) "%f ";

    if (tit)
        fprintf(fp, "%s\n", tit);

    register long i, j;
    double huge *aux = M;

    for (i = 0; i < Rows; i++)
        {
        for (j = 0; j < Cols; j++)
            fprintf(fp, format, *aux++);
        fprintf(fp, "\n");
        }
    fprintf(fp, "\n");
    }
