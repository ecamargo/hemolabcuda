#include "matrix.h"

void acMatrix::Restore(FILE *fp)
    {
    char *nome = "acMatrix::Restore";
    TraceOn(nome);
    gFRead((char huge *) &Rows, sizeof(long), 1, fp);
    gFRead((char huge *) &Cols, sizeof(long), 1, fp);
    M = AllocMatrix(Rows, Cols);
    long t = Rows * Cols;
    gFRead((char huge *) M, sizeof(double), t, fp);
    TraceOff(nome);
    }
    

void acMatrix::Restore(char *nametab, int version, char *nomefile)
    {
    char *nome = "acMatrix::Restore";
    char buf[100];
    FILE *fp;

    TraceOn(nome);

    ObjectExists();  // verifica se o objeto existe

    if ((fp = OpenRestore(nametab, version, nomefile)) == NULL)
        { //   13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
        GetError(13, buf);
        Error(FATAL_ERROR, 3, buf, nametab, version, nomefile);
        }

    Restore(fp);
    CloseRestore(fp);

    TraceOff(nome);
    }
