#include "matrix.h"

// grava matrix em arquivo

void acMatrix::Save(FILE *fp)
    {
    long t = Rows * Cols;
    gFWrite((char *) &Rows, sizeof(long),   1, fp);
    gFWrite((char *) &Cols, sizeof(long),   1, fp);
    gFWrite((char *) M,     sizeof(double), t, fp);
    }

void acMatrix::Save(char *objname, int version, char *filename)
    {
    char *nome = "acMatrix::Save";
    FILE *fp;

    TraceOn(nome);

    fp = OpenSave(objname, version, filename);
    Save(fp);
    CloseSave(fp, filename);

    TraceOff(nome);
    }
