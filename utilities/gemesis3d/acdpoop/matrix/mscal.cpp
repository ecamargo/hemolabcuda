#include "matrix.h"

double acMatrix::operator^(acMatrix& v2)
    {
    char *nome = "acMatrix::operator^";
    double d;
    TraceOn(nome);

    EqualDim(*this, v2);
    d = scalv(M, v2.M, Rows * Cols);

    TraceOff(nome);
    return(d);
    }

double acMatrix::Scal(acMatrix& v2)
    {
    char *nome = "acMatrix::Scal";
    double d;

    TraceOn(nome);
    EqualDim(*this, v2);  
    d = scalv(M, v2.M, Rows * Cols);

    TraceOff(nome);
    return(d);
    }
