#include "matrix.h"

acMatrix& acMatrix::Set(double v, double inc)
    {
    double huge *aux = M;
    long i;
    long t = Rows * Cols;

    for (i = 0; i < t; i++, v += inc)
        *aux++ = v;

    return *this;
    }

