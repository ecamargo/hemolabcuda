#include "matrix.h"

// fixa a dimensao de uma matriz ja' existente
void acMatrix::SetDim(long r, long c)
    {
    char *nome = "acMatrix::SetDim";

    TraceOn(nome);
    
    ObjectExists();

    MatrixDimTest(r, c);    
    
    Rows = r;
    Cols = c;
    M = AllocMatrix(r, c);
    
    TraceOff(nome);
    }
    

