#include "matrix.h"

// m.Transpose(m1);
acMatrix& acMatrix::Transposed(acMatrix& m2)
    {
    char *nome = "acMatrix::Transposed";
    TraceOn(nome);

    Free();
    Rows = m2.Cols;
    Cols = m2.Rows;
    M    = AllocMatrix(Rows, Cols);

    tranmat(M, m2.M, m2.Rows, m2.Cols);

    TraceOff(nome);
    return(*this);
    }

