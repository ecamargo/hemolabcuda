#include "registro.h"

void FazRegistro(char *Argv[], float *paux, CString &RegisteredTo, double *StatusRegOk)
    {
    CString Name, Prog, Number, Version;
    static char buf[30];

    strcpy(buf, Argv[2]);
    for (int i = 0; i < (int) strlen(buf); i++)
        if (buf[i] == '_')
            buf[i] = ' ';
    Prog = buf;
   
    strcpy(buf, Argv[3]);
    for (i = 0; i < (int) strlen(buf); i++)
        if (buf[i] == '_')
            buf[i] = ' ';
    Name    = buf;
    Number  = Argv[4];
    Version = Argv[5];
    
    CString UserInfo = "User Info";
    if (SetKernel(Name, Number, Prog, Version, paux, RegisteredTo, StatusRegOk))
        {
        AfxGetApp()->WriteProfileString(UserInfo, "ProgramName", Prog);
        AfxGetApp()->WriteProfileString(UserInfo, "UserNumber",  Number);
        AfxGetApp()->WriteProfileString(UserInfo, "UserName",    Name);
        AfxGetApp()->WriteProfileString(UserInfo, "Version",     Version);
        }        
    }                                                     

