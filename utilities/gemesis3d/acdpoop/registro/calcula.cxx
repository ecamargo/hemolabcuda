#include "registro.h" 

void Calcula(char *nome, char *prog, int tipo, char *result)
    {
    const MaxSize = 21;
    static float tp1 [] = { 't', 'p', 's', 't', 'a', 'n', 'd', 'a', 'r', 'd', 0};
    static float tp2 [] = { 'p', 'r', 'o', 'f', 'e', 's', 's', 'i', 'o', 'n', 0};
    static int tp[10];
    char buf[MaxSize];
    int i, j;
    int vet10[10];
    int vet5[6];

    if (tipo == 0)
        for (i = 0; i < 10; i++)
            tp[i] = (int) tp1[i];
    else if (tipo == 1)
        for (i = 0; i < 10; i++)
            tp[i] = (int) tp2[i];

    int size = strlen(nome);
    memset(buf, 0x00, MaxSize);

    if (size >= MaxSize)
        strncpy(buf, nome, MaxSize - 1);
    
    if (size < MaxSize - 1)
        {
        strcpy(buf, nome);
        i = MaxSize - 1 - size;
        strncpy(&buf[size], nome, i);
        }

    j = 10;
    for (i = 0; i < 10; i++, j++)
        {
        vet10[i] = (int) buf[i] + buf[j] - prog[i] + tp[i];
        vet10[i] = abs(vet10[i]);
        }
    j = 5;
    for (i = 0; i < 5; i++, j++)
       {         
       vet5[i] = vet10[i] + vet10[j];
       while (vet5[i] > 255)
           vet5[i] = vet5[i] - 255;
       if (vet5[i] < 15)
           vet5[i] += 0xf;
       }
        
    sprintf(result, "%x%x%x%x%x", vet5[0], vet5[1],vet5[2],
            vet5[3],vet5[4]);
    }


