#include "registro.h"

void GetKernel(CString &RegisteredTo, float *paux, double *StatusRegOk)
    {
    CString UserInfo = "User Info";
    CString UserName;
    CString UserNumber;
    CString ProgramName;
    CString UserVersion;
    UserName    = AfxGetApp()->GetProfileString(UserInfo, "UserName");
    UserNumber  = AfxGetApp()->GetProfileString(UserInfo, "UserNumber");
    ProgramName = AfxGetApp()->GetProfileString(UserInfo, "ProgramName");
    UserVersion = AfxGetApp()->GetProfileString(UserInfo, "Version");
    if (UserName.IsEmpty() || UserNumber.IsEmpty())
        RegisteredTo = "Demo Version";
    else      // seta informacao de que esta registrado
        SetKernel(UserName, UserNumber, ProgramName, UserVersion, paux, RegisteredTo, StatusRegOk);
    }

