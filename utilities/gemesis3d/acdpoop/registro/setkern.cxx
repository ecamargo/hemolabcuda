#include "registro.h"

// paux: nome do programa .
// ex.: float paux[] = {'F', 'R', 'O', 'N', 'T', 'A', 'L', 0}; 

int SetKernel(CString &UserName, CString &UserNumber, CString &ProgramName, CString &Version,
              float *paux, CString &RegisteredTo, double *StatusRegOk)
    {
    static char nome[81];
    static char number[81];
    static char progname[31];
    static char versao[20];
    strcpy(nome,     UserName);      // nome do usuario
    strcpy(number,   UserNumber);    // numero do usuario
    strcpy(progname, ProgramName);   // nome do programa
    strcpy(versao,   Version);       // versao
    char result[20];
    int tipo;

    for (int i = 0; versao[i]; i++)
        versao[i] = toupper(versao[i]);

    if (strcmp(versao, "STANDARD") == 0)
        tipo = 0;
    else if (strcmp(versao, "PROFESSIONAL") == 0)
        tipo = 1;
    else
        {
        RegisteredTo = "Demo Version";
        return (0);
        }

    Calcula(nome, progname, tipo, result); // calcula num. de registro

    // verifica se nome do programa esta ok
    static char  aux1[10];   // nome do programa lido em maiusculas
    static char  aux2[10];   // nome do programa interno
        
    for (i = 0; progname[i]; i++)
        aux1[i] = (char) toupper(progname[i]);
    for (i = 0; paux[i] != 0.0; i++)
        aux2[i] = (char) paux[i];
    aux2[i] = 0;

    if ((strcmp(result, number) == 0) && (strncmp(aux1, aux2, 7) == 0))
        {
        if (tipo == 0)
            {
            *StatusRegOk = 2.001;
            RegisteredTo = UserName + " - Standard Version";
            return (1);
            }
        else if (tipo == 1)
            {
            *StatusRegOk = 2.010;
            RegisteredTo = UserName + " - Professional Version";
            return (1);
            }
        }

    RegisteredTo = "Demo Version";
    return (0);
    }           

