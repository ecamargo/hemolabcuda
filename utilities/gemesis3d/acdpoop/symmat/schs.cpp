#include "symmat.h"

// -m;
acSymMatrix& acSymMatrix::operator-()
    {
    chsvet(SM, NElem);
    return *this;
    }

// m.Chs;
acSymMatrix& acSymMatrix::Chs()
    {
    chsvet(SM, NElem);
    return *this;
    }

