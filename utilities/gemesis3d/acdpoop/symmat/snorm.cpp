#include "symmat.h"
#include <math.h>

double acSymMatrix::InfinityNorm()
    {
    long i, j, inc;
    double soma;
    double maior = 0.0;
    double huge *v = SM;
    double huge *aux = SM;
    
    for (i = 0; i < Rows; i++)
        {
        soma = 0.0;
        
        v = aux++;
        
        // contribuicao da parte sub-diagonal
        inc = Rows - 1;
        for (j = 0; j < i; j++)
            {
            soma += fabs(*v);
            v += inc--;
            }
                        
        // contribuicao da parte supra-diagonal
        for (j = i; j < Rows; j++)
            soma += fabs(*v++);
        if (soma > maior)
            maior = soma;
        }
        
    return(maior);
    }


double acSymMatrix::OneNorm()
    {
    return (InfinityNorm());
    }


