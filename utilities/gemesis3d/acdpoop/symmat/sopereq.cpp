#include "symmat.h"

acSymMatrix& acSymMatrix::Equal(acSymMatrix& x)
    {
    Free();
    Rows  = x.Rows;
    NElem = x.NElem;
    SM    = AllocSymMatrix(NElem);

    dcopyv(SM, x.SM, NElem);

    return(*this);
    }


acSymMatrix& acSymMatrix::operator=(acSymMatrix& x)
    {
    Free();
    Rows  = x.Rows;
    NElem = x.NElem;
    SM    = AllocSymMatrix(NElem);
        
    dcopyv(SM, x.SM, NElem);

    return(*this);
    }
    
