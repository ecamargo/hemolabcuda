#include "symmat.h"
#include "matrix.h"

// m *= 1.5;
acSymMatrix& acSymMatrix::operator*=(double r)
    {
    long i;
    long t = NElem;
    double huge *p = SM;

    for (i = 0; i < t; i++, p++)
        *p *= r;

    return *this;
    }

// m.Prod(1.5);
acSymMatrix& acSymMatrix::Prod(double r)
    {
    long i;
    long t = NElem;
    double huge *p = SM;

    for (i = 0; i < t; i++, p++)
        *p *= r;

    return *this;
    }


