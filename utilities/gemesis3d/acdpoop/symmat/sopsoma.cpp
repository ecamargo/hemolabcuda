#include "symmat.h"

// c = a + b;
acSymMatrix operator+(acSymMatrix& m2, acSymMatrix& m3)
    {
    char *nome = "operator+";
    TraceOn(nome);  

    EqualDim(m2, m3);

    acSymMatrix m1(m2.Rows);
   
    addvet(m1.SM, m2.SM, m3.SM, m1.NElem);

    TraceOff(nome);
    return(m1);
    }

// c.Add(a, b);
acSymMatrix& acSymMatrix::Add(acSymMatrix& m2, acSymMatrix& m3)
    {
    char *nome = "acSymMatrix::Add";
    TraceOn(nome);

    EqualDim(m2, m3);

    Free();
    Rows  = m2.Rows;
    NElem = m2.NElem;
    SM    = AllocSymMatrix(NElem);

    addvet(SM, m2.SM, m3.SM, NElem);

    TraceOff(nome);
    return(*this);
    }

// a += b;
acSymMatrix& acSymMatrix::operator+=(acSymMatrix& m2)
    {
    char *nome = "acSymMatrix::operator+=";
    register long i;
    TraceOn(nome);

    EqualDim(*this, m2);
    double huge *p1 = SM,
           huge *p2 = m2.SM;

    for (i = 0; i < NElem; i++, p1++, p2++)
        *p1 += *p2;

    TraceOff(nome);
    return *this;
    }

// a.Add(b);
acSymMatrix& acSymMatrix::Add(acSymMatrix& m2)
    {
    char *nome = "acSymMatrix::Add";
    register long i;
    TraceOn(nome);

    EqualDim(*this, m2);
    double huge *p1 = SM,
           huge *p2 = m2.SM;

    for (i = 0; i < NElem; i++, p1++, p2++)
        *p1 += *p2;

    TraceOff(nome);
    return *this;
    }
    
