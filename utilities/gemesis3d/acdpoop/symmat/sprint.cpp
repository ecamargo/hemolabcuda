#include "symmat.h"

void acSymMatrix::Print(FILE *fp, char *tit, char *fmt)
    {
    char *format;

    format = (fmt) ? fmt : (char *) "%f ";

    if (tit)
        fprintf(fp, "%s\n", tit);

    register long i, j;
    double huge *aux = SM;

    for (i = 0; i < Rows; i++)
        {
        for (j = i; j < Rows; j++)
            fprintf(fp, format, *aux++);
        fprintf(fp, "\n");
        }
    fprintf(fp, "\n");
    }
    
