#include "symmat.h"

void acSymMatrix::Restore(FILE *fp)
    {
    char *nome = "acSymMatrix::Restore";
    TraceOn(nome);
    
    gFRead((char huge *) &Rows,  sizeof(long), 1, fp);
    gFRead((char huge *) &NElem, sizeof(long), 1, fp);
    SM = AllocSymMatrix(NElem);
    gFRead((char huge *) SM, sizeof(double), NElem, fp);
    
    TraceOff(nome);
    }
    

void acSymMatrix::Restore(char *nametab, int version, char *nomefile)
    {
    char *nome = "acSymMatrix::Restore";
    char buf[100];
    FILE *fp;

    TraceOn(nome);

    ObjectExists();  // verifica se o objeto existe

    if ((fp = OpenRestore(nametab, version, nomefile)) == NULL)
        { //   13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
        GetError(13, buf);
        Error(FATAL_ERROR, 3, buf, nametab, version, nomefile);
        }

    Restore(fp);
    CloseRestore(fp);

    TraceOff(nome);
    }
    
