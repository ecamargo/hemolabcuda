#include "symmat.h"

// grava matrix em arquivo

void acSymMatrix::Save(FILE *fp)
    {
    gFWrite((char *) &Rows,  sizeof(long),   1,     fp);    
    gFWrite((char *) &NElem, sizeof(long),   1,     fp);
    gFWrite((char *) SM,     sizeof(double), NElem, fp);
    }

void acSymMatrix::Save(char *objname, int version, char *filename)
    {
    char *nome = "acSymMatrix::Save";
    FILE *fp;

    TraceOn(nome);

    fp = OpenSave(objname, version, filename);
    Save(fp);
    CloseSave(fp, filename);

    TraceOff(nome);
    }
