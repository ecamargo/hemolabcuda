#include "symmat.h"

double acSymMatrix::operator^(acSymMatrix& v2)
    {
    char *nome = "acSymMatrix::operator^";
    double d, dd;

    TraceOn(nome);
    EqualDim(*this, v2);  
    d = scalv(SM, v2.SM, NElem);
    
    // pega a diagonal
    long i;
    long inc = Rows;
    dd = 0.0;
    double huge *v = SM;
    for (i = 0; i < Rows; i++)
        {
        dd += *v;
        v += inc--;
        }
        
    d = 2.0 * d  - dd;
        
    TraceOff(nome);
    return(d);
    }

double acSymMatrix::Scal(acSymMatrix& v2)
    {
    char *nome = "acSymMatrix::Scal";
    double d, dd;

    TraceOn(nome);
    EqualDim(*this, v2);  
    d = scalv(SM, v2.SM, NElem);
    
    // pega a diagonal
    long i;
    long inc = Rows;
    dd = 0.0;
    double huge *v = SM;
    for (i = 0; i < Rows; i++)
        {
        dd += *v;
        v += inc--;
        }
        
    d = 2.0 * d  - dd;
        
    TraceOff(nome);
    return(d);
    }
    
