#include "symmat.h"

acSymMatrix& acSymMatrix::Set(double v, double inc)
    {
    double huge *aux = SM;
    long i;

    for (i = 0; i < NElem; i++, v += inc)
        *aux++ = v;

    return *this;
    }

