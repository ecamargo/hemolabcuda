#include "symmat.h"

// fixa a dimensao de uma matriz ja' existente
void acSymMatrix::SetDim(long r)
    {
    char *nome = "acSymMatrix::SetDim";

    TraceOn(nome);
    
    ObjectExists();

    SymMatrixDimTest(r);
    
    Rows  = r;
    NElem = Rows * (Rows + 1) / 2;
    SM    = AllocSymMatrix(NElem);
    
    TraceOff(nome);
    }
    

