#include "symmat.h"

// aloca memoria para a matriz
double huge *AllocSymMatrix(long nelem)
    {
    double huge *aux;
    char buf[30];

    aux = (double huge *) mMalloc(sizeof(double) * nelem);
    if (!aux)
        {
        // 1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }
    return(aux);
    }

// testa dimensao da matriz
void SymMatrixDimTest(long l)
    {
    char buf[20];
    if (l < 0)
       {
       // 33, "Negative Size."
       GetError(33, buf);
       Error(FATAL_ERROR, 2, buf);
       }
    }

// verifica se um objeto existe
void acSymMatrix::ObjectExists()
    {
    char buf[81];
    if (Rows)
        { //  22, "An Object Of Class |%s| Exists And Will Be Eliminated."
        GetError(22, buf);
        Error(WARNING, 3, buf, "acSymMatrix");
        Free();
        }
    }

// verifica se as matrizes tem o mesmo tamanho
void EqualDim(acSymMatrix &m1, acSymMatrix &m2)
    {
    char buf[50];

    if ((m1.Rows != m2.Rows))
        {
        // 35, "Matrices With Different Sizes."
        GetError(35, buf);
        Error(FATAL_ERROR, 4, buf);
        }
    }


// construtores
acSymMatrix::acSymMatrix(long r)
    {
    char *nome = "acSymMatrix::acSymMatrix";

    TraceOn(nome);

    SymMatrixDimTest(r);
    Rows  = r;
    NElem = (r * (r - 1)) / 2;
    SM    = AllocSymMatrix(NElem);
     
    TraceOff(nome);
    }

acSymMatrix::acSymMatrix(acSymMatrix& x)
    {
    char *nome = "acSymMatrix::acSymMatrix";
    TraceOn(nome);

    Rows  = x.Rows;
    NElem = x.NElem;
    SM    = AllocSymMatrix(NElem);

    dcopyv(SM, x.SM, NElem);

    TraceOff(nome);
    }


// Libera area
void acSymMatrix::Free()
    {
    if (Rows)
        {
        mFree(SM);
        Rows = NElem = 0;
        }
    }
