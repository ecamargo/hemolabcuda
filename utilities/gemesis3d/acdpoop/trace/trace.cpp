/*
** Funcoes para Trace - Salgado - 30/11/95
*/
#include "acdp.h"
#include "acdptype.h"
#include <stdarg.h>
#include <ctype.h>
#include "acstack.h"
#include "aclist.h"

                

// definicoes para TRACE
static int nerr1;           // warning - tipos de erros
static int nerr2;           // common error
static int nerr3;           // fatal error
static int keyimp;          // pediu impressao de rastreamento
static int flagfunc;        // ativa impressao se flag estiver ligado
static acStack *stack;      // pilha de rastreamento
static char NomeFunc[81];   // nome da funcao que ativa o rastreamento
static FILE *fmens;         // arquivo de mensagens
static void auxprt(FILE *, int);
static void InitTrace(int cod, FILE *arq_trc);

int mStrCmp(void *a1, void *a2)
    {
    char *s1 = (char *) a1;
    char *s2 = (char *) a2;
    while (*s1 == *s2)
    if (*s1++)
        s2++;
    else
        return(0);
    return(*s1 - *s2);
    }

// definicoes para DEBUG
static void auxdebug(char *nome, int nivel, char *onoff);
static void InitDebug(int cod, FILE *arq_trc);
static int ndepur;          // 0 = nenhuma; -1 = todas; 1 = algumas

struct DebInfo
    {
    char nome[81];
    int  nivel;
    };

static acList  *list;       // lista de rotinas para depuracao


void StartACDP(int cod, char *mens, char *trc)
    {
    FILE *fptrc = NULL;

    fmens = fopen(mens, "w");

    if (cod > 0)
        fptrc = fopen(trc, "r");

    InitTrace(cod, fptrc);
    TraceOn("main");
    InitDebug(cod, fptrc);
    InitTime();
    if (fptrc)
        fclose(fptrc);
    }

void TraceOn(char *nome)
    {
    stack->Push(nome);          // coloca nome na pilha

    if (keyimp)                 // foi pedida impressao de rastreamento?
        {
        if (StrEq(nome, NomeFunc))  // e' a funcao de inicio de impressao?
            flagfunc = TRUE;

        if (flagfunc)              // flag ligado?
            {
            fprintf(fmens, ">Trace On  %s\n", nome);
            fflush(fmens);
            }
        }
    }

void TraceOff(char *nome)
    {
    char aux[81];
    char buf[100];

    stack->Pop(aux);           // tira nome da pilha

    // verifica se o nome entrado e' igual ao que vai ser retirado
    if (!StrEq(nome, aux))
        {
        //  14, "Trace On and Trace Off Mismatch |%s| x |%s|."
        GetError(14, buf);
        Error(0, 1, buf, nome, aux);
        }

    if (keyimp)       // tem impressao?
        {
        if (flagfunc) // esta' ativa?
            {
            fprintf(fmens, ">Trace Off %s\n", aux);
            fflush(fmens);
            }

        if (StrEq(nome, NomeFunc))
            flagfunc = FALSE;
        }
    }

void Error(int icod, int numerr, char *msg, ...)
    {
    static char nome[81];

    static char buf[201];
    va_list argptr;
#if defined(__MSWIN__) || defined(__ZAPP__) || defined (__MFC__)
    int res;
#endif

    va_start(argptr, msg);
    vsprintf(buf, msg, argptr);
    va_end(argptr);

    stack->Copy(nome);

    fprintf(fmens, "\n\n");
    PrtLine(fmens, '-', 79);
    fprintf(fmens, "|\n");
    fprintf(fmens, "| Error on: %s\n", nome);
    fprintf(fmens, "|\n");
    fflush(fmens);

    switch (icod)
        {
        case WARNING :      /* warning */
            nerr1++;
            fprintf(fmens, "| The program can continue.\n");

#if defined(__MSWIN__) || defined (__MFC__)
            res = MessageBox (0, buf, "WARNING", MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                ExitProg();
#endif

#if defined(__ZAPP__)
            res = zMessage(app->rootWindow(), buf, "WARNING",
                MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                ExitProg();
#endif
            break;

        case COMMON_ERROR :      /* erro, porem o programa continua */
            nerr2++;
            fprintf(fmens, "| The procedure was not executed.\n");
#if defined(__MSWIN__) || defined (__MFC__)
            res = MessageBox (0, buf, "ERROR", MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                ExitProg();
#endif

#if defined(__ZAPP__)
            res = zMessage(app->rootWindow(), buf, "ERROR",
                MB_OKCANCEL | MB_TASKMODAL);
            if (res == IDCANCEL)
                ExitProg();
#endif
            break;

        case FATAL_ERROR :      /* parada do programa */
            nerr3++;
#if defined(__MSWIN__) || defined (__MFC__)
            MessageBox(0, buf, "FATAL ERROR", MB_OK | MB_TASKMODAL);
#endif
#if defined(__ZAPP__)
            zMessage(app->rootWindow(), buf, "FATAL ERROR", MB_OK | MB_TASKMODAL);
#endif

            stack->Pop(nome);
            if (!stack->Exist())
                ExitProg();

            if (stack->Pop(nome))
                fprintf(fmens, "| When called by: %s\n", nome);

            while (stack->Pop(nome))        /* imprime a pilha */
                fprintf(fmens, "|%21s%s\n", " ", nome);
            break;

        default:
            break;
    }

    fprintf(fmens, "|\n");
    fprintf(fmens, "| Error number: %d\n", numerr);
    fprintf(fmens, "|\n");
    fprintf(fmens, "| %s\n", buf);
    fprintf(fmens, "|\n");
    PrtLine(fmens, '-', 79);

    fflush(fmens);

    if (icod == FATAL_ERROR)
    ExitProg();     /* programa deve ser interrompido */
    }


void CloseMensFile()
    {
    int i;

    i = nerr1 + nerr2 + nerr3;
    auxprt(fmens, i);
#if defined(__TEXT__)
    auxprt(stdout, i);
#endif
    fclose(fmens);
    stack->Clear();
    delete stack;
    }

void ExitProg()
    {
    int i;

    i = nerr1 + nerr2 + nerr3;
    auxprt(fmens, i);
#if defined(__TEXT__)
    auxprt(stdout, i);
#endif
    fclose(fmens);
    stack->Clear();
    delete stack;
    exit(i);
    }


static void auxprt(FILE *faux, int i)
    {
    fprintf(faux, "\n\n");
    PrtLine(faux, '-', 79);
    fprintf(faux, "|\n");
    fprintf(faux, "| Number Of Detected Errors:\n");
    fprintf(faux, "|\n");
    fprintf(faux, "|   Warnings     : %2d\n", nerr1);
    fprintf(faux, "|   Common Errors: %2d\n", nerr2);
    fprintf(faux, "|   Fatal Error  : %2d\n", nerr3);
    fprintf(faux, "|                  -----\n");
    fprintf(faux, "|   Total        : %2d\n", i);
    fprintf(faux, "|\n");
    fprintf(faux, "|\n|           Execution Terminated\n");
    fprintf(faux, "|\n");
    PrtLine(faux, '-', 79);
    }


static void InitTrace(int cod, FILE *arq_trc)
    {
    static char buf[81];

    // inicializa variaveis globais
    stack = new acStack(81);

    nerr1  = nerr2    = nerr3 = 0;
    keyimp = flagfunc = FALSE;

    if (cod > 0)   // quer rastreamento e/ou depuracao
        {
        if (!arq_trc)
            Error(2, 1, "Can't find file |%s|", arq_trc);

        fgets(buf, 10, arq_trc);
        if (buf[0] == 's' || buf[0] == 'S')
            {
            keyimp = TRUE;
            fgets(NomeFunc, 81, arq_trc);
            NomeFunc[StrLen(NomeFunc) - 1] = '\0';
            }
        }
    }

// -1 -> todas
//  0 -> nenhuma
//  1 -> algumas

void InitDebug(int codigo, FILE *lr)
    {
    static char *nomefnc = "InitDebug";
    int tipo, numrot, i, j;
    static char buf[81];
    DebInfo aux;

    TraceOn(nomefnc);

    list = new acList(sizeof(DebInfo), mStrCmp);

    if (codigo == 0)      // nao quer nada
        {
        ndepur = 0;
        TraceOff(nomefnc);
        return;
        }

    tipo = atoi(fgets(buf, 10, lr));
    if (tipo < 1 || tipo > 3)
    Error(2, 1, "Error in Debug information.");

    switch(tipo)
        {
        case 1 : /* nenhuma */
            ndepur = 0;  break;

        case 2 : /* todas */
            ndepur = -1; break;

        case 3 : /* algumas */
            ndepur = 1;  break;
        }

    // le o numero de rotinas a serem depuradas
    if (tipo == 3)     /* vai depurar algumas rotinas */
        {
        numrot = atoi(fgets(buf, 10, lr));

        // le o nome da rotina e o nivel de depuracao
        for (i = 0; i < numrot; i++)
            {
            fgets(buf, 81, lr);      // le e limpa brancos do final
            j = 0;
            while (!isspace(buf[j]))
            j++;
            buf[j] = '\0';

            StrCpy(aux.nome, buf);

            // le o nivel de depuracao
            aux.nivel = atoi(fgets(buf, 10, lr));

            list->InsertSort(&aux);
            }
        }

    TraceOff(nomefnc);
    }


void DebugOn(int *nivel)
    {
    static char nome[81];

    // verifica se foi pedida depuracao. Se nao foi, retorna
    if (!ndepur)        // nunhuma!
        {
        *nivel = 0;
        return;
        }

    if (ndepur == -1)   // todas!
        {
        *nivel = 1;
        stack->Copy(nome);    // nome da rotina no topo da pilha
        auxdebug(nome, *nivel, " On:");
        return;
        }

    DebInfo *aux;
    stack->Copy(nome);           // nome da rotina no topo da pilha

    aux = (DebInfo *) list->SearchSort(nome); // verifica se esta' na lista

    if (aux)
        {
        *nivel = aux->nivel;
        auxdebug(nome, *nivel, " On:");
        }
    else
        *nivel = 0;          // nao achou na lista
    }


void DebugOff()
    {
    DebInfo *aux;
    int nivel;

    if (!ndepur)    // nao foi pedida depuracao
        return;

    static char nome[81];
    stack->Copy(nome);

    // verifica se depurou esta rotina
    if (ndepur == 1)   // pode ter depurado
        {
        aux = (DebInfo *) list->SearchSort(nome);
        if (!aux)      // se nao achou a rotina, retorna
            return;
        }

    if (ndepur == 1)
        nivel = aux->nivel;
    else
        nivel = 1;

    // avisa que saiu da rotina
    auxdebug(nome, nivel, "Off:");
    }

/*
** rotina auxiliar interna
*/
static void auxdebug(char *nome, int nivel, char *onoff)
    {
    fprintf(fmens, "\n\n");
    PrtLine(fmens, '-', 79);
    fprintf(fmens, "|\n");
    fprintf(fmens, "| Debug %s %s\n", onoff, nome);
    fprintf(fmens, "|\n");
    fprintf(fmens, "| Nivel: %d\n", nivel);
    fprintf(fmens, "|\n");
    PrtLine(fmens, '-', 79);
    fflush(fmens);
    }

void WDebug(char *msg, ...)
    {
    static char buf[201];
    va_list argptr;
    va_start(argptr, msg);
    vsprintf(buf, msg, argptr);
    va_end(argptr);
    fprintf(fmens, buf);
    fflush(fmens);
    }

FILE *GetMensFile()
    {
    return(fmens);
    }


static void LimpaArg(char *arg)
    {
    while (*arg)
    if (*arg == ' ')
        {
        *arg = 0;
        return;
        }
    else
        arg++;
    }


// -> argv - linha de argumentos
// -> nome - nome do programa
// <- t    - numero de argumentos
// retorna ptr para vetor de argumentos
// somente sao reconhecidos argumentos que comecam por '-'
char **MakeArgcArgv(int &t, char *nome, char *argv)
    {
    char bufao[256];
    char **parg;
    char *pc;
    acList lista(256);
    char *pno;
    acNodeList *head;

    StrCpy(bufao, argv);
    t = 1;

    // calcula numero de argumentos que iniciam por -


    pc = strtok(bufao, " ");
    if (pc)
        {
        LimpaArg(pc);
        lista.InsertFirst(pc);
        t++;
        }


    while ((pc = strtok(NULL, " ")) != NULL)

    if (pc)
        {
        LimpaArg(pc);
        lista.InsertFirst(pc);
        t++;
        }

    // monta vetor de argumentos
    parg = new char*[t];
    parg[0] = new char[StrLen(nome) + 1];
    StrCpy(&parg[0][0], nome);

    if (t == 1)
        return(parg);

    head = lista.GetHead();
    int i = t - 1;
    while(head)
        {
        pno = (char *) head->GetDados();
        parg[i] = new char[StrLen(pno) + 1];
        StrCpy(&parg[i][0], pno);
        head = head->next;
        i--;
        }
    return(parg);
    }

void ClearArgs(int argc, char *argv[])
    {
    int i;
    for (i = 0; i < argc; i++)
        delete argv[i];
    delete argv;
    }
