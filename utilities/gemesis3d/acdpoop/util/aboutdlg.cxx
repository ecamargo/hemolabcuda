#if !defined(__MSC16__) && !defined (__MSC32__)
#include <owl\owlpch.h>

#define BRANCO_BIT  170

#include "aboutdlg.h"

DEFINE_RESPONSE_TABLE1(AboutDialog, TDialog)
      EV_COMMAND(BRANCO_BIT, CmBut),
END_RESPONSE_TABLE;

AboutDialog::AboutDialog(TWindow *w, LPSTR rid, float *cap, int tcap,
                                                float *v, int tv, TModule *tm)
    : TDialog(w, rid, tm)
    {
    but = new TButton(this, BRANCO_BIT);

    str1 = new char[tcap];
    for (int i = 0; i < tcap; i++)
        str1[i] = int(cap[i]);

    str = new char[tv];

    for (i = 0; i < tv; i++)
        str[i] = int(v[i]);
    }

void AboutDialog::CmBut()
    {
    MessageBox(str, str1, MB_OK);
    }
#endif
