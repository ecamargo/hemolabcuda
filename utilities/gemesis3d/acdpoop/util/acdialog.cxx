// ObjectWindows - (C) Copyright 1992 by Borland International

#ifdef __MSWIN__
#include "acdpdefs.h"
#include "acdialog.h"
#include <dir.h>                          


DEFINE_RESPONSE_TABLE1(acListDialog, TDialog)
    EV_COMMAND(IDOK, CmOk),
    EV_LBN_DBLCLK(ID_LISTBOX, LBDblClk),
END_RESPONSE_TABLE;

acListDialog::acListDialog(TWindow* AParent, LPSTR AName,
                     LPSTR title, int n, char *vop[], DWORD *op)
        : TDialog(AParent, AName)
    {
    SelItem = op;
    *SelItem = -1;
    Title   = title;
    N       = n;
    Vop     = vop;
    }

void acListDialog::SetupWindow()
    {
    TDialog::SetupWindow();

    if (Title)
        SetCaption(Title);

    for (int i = 0; i < N; i++)
        SendDlgItemMsg(ID_LISTBOX, LB_ADDSTRING, 0, (LONG)Vop[i]);
    }


void acListDialog::CmOk()
    {
    *SelItem = SendDlgItemMsg(ID_LISTBOX, LB_GETCURSEL, 0, 0L);
    CloseWindow();
    }


BOOL acOpenDialog(HWND hwnd, char *name, char *defname)
    {
    static OPENFILENAME ofn;
    static char szDirName[256], szFile[256], szFileTitle[256], szDefName[256];
    UINT i, cbString;
    char chReplace;

    StrCpy(szDefName, defname);

    getcurdir(0, szDirName);
    szFile[0] = 0;

    cbString = StrLen(szDefName);
    chReplace = szDefName[cbString - 1];

    for (i = 0; i < cbString; i++)
        if (szDefName[i] == chReplace)
            szDefName[i] = 0;

    memset(&ofn, 0, sizeof(OPENFILENAME));

    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner   = hwnd;
    ofn.lpstrFilter = szDefName;
    ofn.nFilterIndex = 1;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFileTitle = szFileTitle;
    ofn.nMaxFileTitle = sizeof(szFileTitle);
    ofn.lpstrInitialDir = szDirName;
    ofn.Flags = OFN_SHOWHELP | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (GetOpenFileName(&ofn))
        {
        StrCpy(name, ofn.lpstrFile);
        return(TRUE);
        }
    return(FALSE);
    }
#endif
