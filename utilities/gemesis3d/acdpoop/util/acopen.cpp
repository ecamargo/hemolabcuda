#if defined(__MSWIN__)
#include <owl\owlpch.h>
#include "acdp.h"
#include "acopen.h"
#include <dir.h>    

BOOL acOpenDialog(HWND hwnd, char *name, char *defname)
    {
    static OPENFILENAME ofn;
    static char szDirName[256], szFile[256], szFileTitle[256], szDefName[256];
    UINT i, cbString;
    char chReplace;

    StrCpy(szDefName, defname);

    getcurdir(0, szDirName);
    szFile[0] = 0;

    cbString = StrLen(szDefName);
    chReplace = szDefName[cbString - 1];

    for (i = 0; i < cbString; i++)
        if (szDefName[i] == chReplace)
            szDefName[i] = 0;

    memset(&ofn, 0, sizeof(OPENFILENAME));

    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner   = hwnd;
    ofn.lpstrFilter = szDefName;
    ofn.nFilterIndex = 1;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFileTitle = szFileTitle;
    ofn.nMaxFileTitle = sizeof(szFileTitle);
    ofn.lpstrInitialDir = szDirName;
    ofn.Flags = OFN_SHOWHELP | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (GetOpenFileName(&ofn))
        {
        StrCpy(name, ofn.lpstrFile);
        return(TRUE);
        }
    return(FALSE);
    }
#endif
