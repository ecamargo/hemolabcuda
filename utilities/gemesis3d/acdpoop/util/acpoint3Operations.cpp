#include "acdp.h"
#include "acdptype.h"
#include <stdlib.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////////////////
// Calcula comprimento do vetor
float ComputeVectorLength(acPoint3 thys)
{
    float squarelen;
    float length;
    /* compute vector length */
    squarelen = thys.x * thys.x;
    squarelen += thys.y * thys.y;
    squarelen += thys.z * thys.z;
    length = (float)sqrt((double)squarelen);
    return length;
}

//////////////////////////////////////////////////////////////////////////////////
// Compute Angle between two vectors (3D)
float ComputeVectorAngle(acPoint3 first, acPoint3 second)
{
	float lenfirst;
	float lensecond;
	float lenproduct;
	
	float dotproduct;
	float radians;
	float degrees;
	
	acPoint3 crossproduct;
	
	lenfirst 	= ComputeVectorLength(first);
	lensecond	= ComputeVectorLength(second);
	
	if((lenfirst < 0.0001) || (lensecond < 0.0001))
	  {
		degrees = 180;
	  }
  else
    {
		//crossproduct  = new acPoint3();
		/* compute vector angle */
    float X, Y,Z;

    /* compute cross product */
    //aj3dVectorCrossProduct(first, second, crossproduct);    
    X  = first.y * second.z;
    X -= first.z * second.y;
    crossproduct.x = X;
    Y  = first.z * second.x;
    Y -= first.x * second.z;
    crossproduct.y = Y;
    Z  = first.x * second.y;
    Z -= first.y * second.x;
    crossproduct.z = Z;

		// dotproduct 	= aj3dVectorDotProduct(first, second);
    /* compute dot product */
    dotproduct  = first.x * second.x;
    dotproduct += first.y * second.y;
    dotproduct += first.z * second.z;

		lenproduct   = ComputeVectorLength(crossproduct);
		/* return the arctangent in the range -pi to +pi */
		radians   = (float)atan2((double)lenproduct, (double)dotproduct);
//		degrees   = ajRadToDeg(radians);
		degrees = radians * 180/M_PI;		
    }     
	return( degrees );
}
