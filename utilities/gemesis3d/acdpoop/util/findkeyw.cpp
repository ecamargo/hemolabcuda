/* 16:03:24  4/25/1992 */

#include "acdp.h"

int FindKeyWord(char *string, FILE *fp)
    {
    char buffer[101], *pt;
    int vartudo, achou, cont;
    int tam;

    vartudo = achou = 0;
    cont = 1;
    tam = StrLen(string);

    while (cont)
        {
        pt = fgets(buffer, 101, fp);   /* le uma linha */

        /*
        ** Verifica se chegou ao final do arquivo.
        ** Se for a primeira vez, reposiciona o arquivo no inicio.
        ** Senao, erro: Nao achou a string.
        */
        if (pt == NULL)
            if (vartudo)
                {
                rewind(fp);
                return(0);
                }
            else
                {
                vartudo = 1;
                rewind(fp);
                }

            /*
            ** Pesquisa para ver se e' candidato.
            */
        else if (*pt++ == '*')    /* e' candidato */
            {
            if (strncmp(pt, "END", 3) == 0)
                if (vartudo)
                    {
                    rewind(fp);
                    return(0);
                    }
                else
                    {
                    vartudo = 1;
                    rewind(fp);
                    }
                    
            else if (strncmp(pt, string, tam) == 0)
                {
                cont = 0;
                achou = 1;
                }
            }
        }

    return(achou);
    }
