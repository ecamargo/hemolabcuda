#include "acdp.h"
#include <time.h>

#if !defined(__LINUX__)
#include <windows.h>
void GetDate(int *dia, int *mes, int *ano)
    {
    SYSTEMTIME lpst;

    GetSystemTime(&lpst);
    *dia = lpst.wDay;
    *mes = lpst.wMonth;
    *ano = lpst.wYear;
    }
#else
extern "C"
    {
    int pegdat_(int *, int *, int *);
    };

void GetDate(int *dia, int *mes, int *ano)
    {
    pegdat_(dia, mes, ano);
    }

#endif
