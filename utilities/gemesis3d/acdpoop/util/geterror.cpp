#include "acdp.h"

struct sErro
    {
    int id;
    char *nerr;
    };

static sErro verros[] = 
   {
   {  0, "Unexpected Error."},
   {  1, "Can't Allocate Memory." },
   {  2, "Incorrect Name |%s| for Data Base." },
   {  3, "Incorrect Password for Data Base |%s|." },
   {  4, "Can't Open Data Base |%s|." },
   {  5, "Incorrect Name for Data Base." },
   {  6, "Data Base |%s| is Already Open." },
   {  7, "Incorrect Internal Names |%s| x |%s| for File |%s|." },
   {  8, "Can't Find Open Data Bases." },
   {  9, "Data Base |%s| is Not Open." },
   { 10, "The Object |%s|, Version |%d| Already Exists in the Data Base |%s|." },
   { 11, "Can't Write in Data Base |%s|." },
   { 12, "Unespected Error." },
   { 13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|." },
   { 14, "Trace On and Trace Off Mismatch |%s| x |%s|." },
   { 15, "Can't Find File |%s|." },
   { 16, "Error in Debug Information." },
   { 17, "Can't Allocate Memory - Data Size Of Node Equal Zero." },
   { 18, "Can't Find Comparation Function." },
   { 19, "Can't Find Printer Function." },
   { 20, "Number Of Nodes <= 0." },
   { 21, "Coordinates With Different Sizes." },
   { 22, "An Object Of Class |%s| Exists And Will Be Eliminated." },
   { 23, "Incorrect Data For Node |%ld|." },
   { 24, "Invalid Number of Groups." },
   { 25, "Unknown Element |%s|." },
   { 26, "Incorrect Group Data |%d %ld|." },
   { 27, "Keyword |%s| Not Found." },
   { 28, "Can't Create Palette." },
   { 29, "Incorrect Number Off Colors For New Palette." },
   { 30, "Incorrect Incidence For Element |%ld| In Group |%d|." },
   { 31, "UserElem 2D Must Have a Maximum of 25 Edges." },
   { 32, "UserElem 2D Must Have a Maximum of 18 Triangles." },
   { 33, "Negative Size." },
   { 34, "Vectors With Different Sizes." },
   { 35, "Matrices With Different Sizes." },
   { 36, "Can't Multiply Matrices." }
   };


void GetError(int id, char *buf)
    {
    int t = sizeof(verros) / sizeof(sErro);
    if (id > t)
	Error(2, 0, "Incorrect Error Id |%d|", id);

    StrCpy(buf, verros[id].nerr);
    }


