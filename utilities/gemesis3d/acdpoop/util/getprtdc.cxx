#include "acdp.h"
#ifdef __MSWIN__
#include "acdpprt.h"

HDC GetPrinterDC()
    {
    char  szDevice[64];
    char *pDevice, *pDriver, *pOutput;

    GetProfileString("windows", "device", "", szDevice, 64);

    if ( ((pDevice = strtok(szDevice, "," )) != NULL) &&
	 ((pDriver = strtok(NULL,     ", ")) != NULL) &&
         ((pOutput = strtok(NULL,     ", ")) != NULL) )
        return (CreateDC(pDriver, pDevice, pOutput, NULL));
    else
        return (NULL);
	}
#endif
