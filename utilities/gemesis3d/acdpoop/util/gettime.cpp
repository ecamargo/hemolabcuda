#include "acdp.h" 
#include <time.h>

#if !defined(__LINUX__)
#include <windows.h>
void GetTime(int *h, int *m, int *s, int *c)
    {
    SYSTEMTIME lpst;

    GetSystemTime(&lpst);
    *h = lpst.wHour;
    *m = lpst.wMinute;
    *s = lpst.wSecond;
    *c = lpst.wMilliseconds;
    }
#else
extern "C"
    {
    int peghor_(int *, int *, int *);
    }

void GetTime(int *h, int *m, int *s, int *c)
    {
    peghor_(h, m, s);
    *c = 0;
    }

#endif
