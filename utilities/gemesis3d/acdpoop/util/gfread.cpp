/* 15:44:13  4/25/1992 */

#include "acdp.h"               

void gFRead(char huge *buf, int size, long n, FILE *fp)
    {                                       /* fread */
    long int i;
    int  c;

    for (i = n * size;  i && (c = getc(fp)) >= 0;  --i)
            *buf++ = (char) c;
    }                                       /* fread */
