#include "acdp.h"

#include <stdlib.h>

void *mCalloc(long nelem, long size)
    {
    void *p;

    p = (void *) calloc((unsigned int) nelem, (unsigned int) size);

    return(p);
    }
