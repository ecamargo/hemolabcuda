#include "acdp.h"

#include <stdlib.h>

void mFree(void *p)
    {
    if (!p)
        return;

    free(p);
    }
