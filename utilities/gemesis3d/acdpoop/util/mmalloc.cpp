#include "acdp.h"

#include <stdlib.h>

void *mMalloc(long size)
    {
    void *p;

    p = (void *) malloc((unsigned int) size); 

    return(p);
    }
