#include <string.h>
#include <stdlib.h>

#include <stdio.h>
#include <time.h>


int pegdat_(int *dia, int *mes, int *ano)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *dia = t->tm_mday;
    *mes = t->tm_mon;
    *ano = t->tm_year;
    return(1);
    }

int peghor_(int *hora, int *minuto, int *segundo)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *hora = t->tm_hour;
    *minuto = t->tm_min;
    *segundo = t->tm_sec;
    return(1);
    }
