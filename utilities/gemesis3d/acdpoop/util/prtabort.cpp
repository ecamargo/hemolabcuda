#include "acdp.h"
#ifdef __MSWIN__
#include "acdpprt.h"


BOOL FAR PASCAL AbortPrinterProc(HDC, short)
    {
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        DispatchMessage(&msg);
    return TRUE;
    }

#endif
