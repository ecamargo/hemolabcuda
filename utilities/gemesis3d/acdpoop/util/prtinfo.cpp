#include "acdp.h"
#ifdef __MSWIN__
#include "acdpprt.h"

BOOL GetPrinterInfo(char *driver, char *device, char *port)
	{
	char  szDevice[64];
	char *pDevice, *pDriver, *pOutput;

	GetProfileString("windows", "device", "", szDevice, 64);

	if (((pDevice = strtok(szDevice, "," )) != NULL) &&
	((pDriver = strtok(NULL,     ", ")) != NULL) &&
		((pOutput = strtok(NULL,     ", ")) != NULL))
		{
		StrCpy(driver, pDriver);
		StrCpy(device, pDevice);
		StrCpy(port,   pOutput);
		return(TRUE);
		}
	else
		return (FALSE);
	}
#endif
