#include "acdp.h"

void PrtLine(FILE *lw, int carac, int n)
    {
    while (n--)
        fprintf(lw, "%c", carac);     /* imprime linha */
    fprintf(lw, "\n");                /* pula uma linha */
    }
