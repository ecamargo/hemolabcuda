/* somente para UNIX */
#ifdef __UNIX__
#include "acdp.h"

extern "C"
    {
    long tmpcpu_(void);
    int pegdat_(int *dia, int *mes, int *ano);
    int peghor_(int *hora, int *minuto, int *segundo);
    };

void GetData(int *dia, int *mes, int *ano)
    {
    pegdat_(dia, mes, ano);
    }

void GetTime(int *h, int *m, int *s, int *c)
    {
    peghor_(h, m, s);
    *c = 0;
    }
#endif

