#include <string.h>
#include <stdlib.h>

/*
** somente para UNIX 
*/
#include<stdio.h>
#include <time.h>
#ifndef _WIN32
#include <sys/time.h>
#include <sys/resource.h>
#endif

long tmpcpu_()
    {
    int r;
    long tempo;
    struct rusage t;
    double dtempo;

    r = getrusage(RUSAGE_SELF, &t);
    if (r == -1)
        {
        printf("\nErro em tmpcpu\n");
        exit(1);
        }
    dtempo = t.ru_utime.tv_usec + t.ru_stime.tv_usec;
    dtempo += 1.0e+06*(t.ru_utime.tv_sec + t.ru_stime.tv_sec);

    tempo = dtempo * 1.0E-04;
    return(tempo);
    }


int pegdat_(int *dia, int *mes, int *ano)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *dia = t->tm_mday;
    *mes = t->tm_mon;
    *ano = t->tm_year;
    return(1);
    }

int peghor_(int *hora, int *minuto, int *segundo)
    {
    long tclock;
    struct tm *t;

    tzset();
    tclock = time(0);
    t = localtime(&tclock);
    *hora = t->tm_hour;
    *minuto = t->tm_min;
    *segundo = t->tm_sec;
    return(1);
    }
