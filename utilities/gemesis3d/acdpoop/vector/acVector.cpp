#include "acVector.h"

// aloca memoria para o vetor
double huge *AllocVector(long t)
    {
    double huge *aux;
    char buf[30];
    aux = (double huge *) mMalloc(sizeof(double) * t);
    if (!aux)
        {
        // 1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }
    return(aux);
    }

// testa dimensao do vetor
void VectorDimTest(long t)
    {
    char buf[20];
    if (t < 0)
       {
       // 33, "Negative Size."
       GetError(33, buf);
       Error(FATAL_ERROR, 2, buf);
       }
    }

// verifica se um objeto existe
void acVector::ObjectExists()
    {
    char buf[81];
    if (Dim)
        { //  22, "An Object Of Class |%s| Exists And Will Be Eliminated."
        GetError(22, buf);
        Error(WARNING, 3, buf, "acVector");
        Free();
        }
    }

// verifica se vetores tem o mesmo tamanho
void EqualSize(acVector &v1, acVector &v2)
    {
    char buf[50];

    if (v1.Dim != v2.Dim)
        {
        // 34, "Vectors With Different Sizes."
        GetError(34, buf);
        Error(FATAL_ERROR, 4, buf);
        }
    }



// construtores
acVector::acVector(long t)
    {
    char *nome = "acVector::acVector";

    TraceOn(nome);

    VectorDimTest(t);
    Dim = t;
    V   = AllocVector(Dim);
     
    TraceOff(nome);
    }

acVector::acVector(long t, double inic, double inc)
    {
    char *nome = "acVector::acVector";

    TraceOn(nome);

    VectorDimTest(t);
    Dim = t;
    V   = AllocVector(Dim);

    double huge *aux = V;
    for (long i = 0; i < Dim; i++, inic += inc)
        *aux++ = inic;

    TraceOff(nome);
    }

acVector::acVector(acVector& x)
    {
    char *nome = "acVector::acVector";

    TraceOn(nome);

    Dim = x.Dim;
    V   = AllocVector(Dim);

    dcopyv(V, x.V, Dim);

    TraceOff(nome);
    }


// Libera area
void acVector::Free()
    {
    if (Dim)
        {
        mFree(V);
        Dim = 0;
        }
    }
