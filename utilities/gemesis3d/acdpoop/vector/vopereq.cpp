#include "acVector.h"

acVector& acVector::Equal(acVector& x)
    {
    Free();
    Dim = x.Dim;
    V   = AllocVector(Dim);

    dcopyv(V, x.V, Dim);

    return(*this);
    }


acVector& acVector::operator=(acVector& x)
    {
    Free();
    Dim = x.Dim;
    V   = AllocVector(Dim);

    dcopyv(V, x.V, Dim);

    return(*this);
    }
