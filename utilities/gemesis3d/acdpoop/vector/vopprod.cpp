#include "acVector.h"
#include "matrix.h"
#include "symmat.h"

static void Testa3(long d1, long d2)
    {
    if (d1 != 3 || d2 != 3)
        Error(FATAL_ERROR, 5,
            "For Vectorial Product, Dimension Must Be 3.");
    }

// v1 = v2 * v3;    Dim <- 3
acVector operator*(acVector& a, acVector& b)
    {
    char *nome = "acVector::operator*";
    TraceOn(nome);

    Testa3(a.Dim, b.Dim);

    acVector c(3);

    c.V[0] = a.V[1] * b.V[2] - a.V[2] * b.V[1];
    c.V[1] = a.V[2] * b.V[0] - a.V[0] * b.V[2];
    c.V[2] = a.V[0] * b.V[1] - a.V[1] * b.V[0];

    TraceOff(nome);
    return(c);
    }

// c = a * b;   Dim <- 3
acVector& acVector::Prod(acVector& a, acVector& b)
    {
    char *nome = "acVector::Prod";
    TraceOn(nome);

    Testa3(a.Dim, b.Dim);

    if (Dim != 3)
        {
        Free();
        Dim = 3;
        V   = AllocVector(3);
        }

    V[0] = a.V[1] * b.V[2] - a.V[2] * b.V[1];
    V[1] = a.V[2] * b.V[0] - a.V[0] * b.V[2];
    V[2] = a.V[0] * b.V[1] - a.V[1] * b.V[0];

    TraceOff(nome);
    return(*this);
    }

// v1 *= 1.5;
acVector& acVector::operator*=(double r)
    {
    long i;
    double huge *p = V;

    for (i = 0; i < Dim; i++, p++)
        *p *= r;

    return *this;
    }

acVector& acVector::Prod(double r)
    {
    long i;
    double huge *p = V;

    for (i = 0; i < Dim; i++, p++)
        *p *= r;

    return *this;
    }
    

// v = ma * v1;
acVector operator*(acMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::operator*";
    long i;
    double huge *pv, huge *pma, huge *pv1;
    long ncola = ma.GetCols();    
    long nrowa = ma.GetRows();
    
    TraceOn(nome);
    
    if (ncola != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");
        

    acVector v(nrowa);
    pv  = v.V;
    pv1 = v1.V;
    pma = ma.GetMatrixPtr();
    
    for (i = 0; i < nrowa; i++, pma += ncola)
        *pv++ = scalv(pma, pv1, v1.Dim);
     
    TraceOff(nome);   
    return(v);
    }

    
acVector& acVector::Prod(acMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::Prod";
    long i;
    double huge *pv, huge *pma, huge *pv1;
    long ncola = ma.GetCols();    
    long nrowa = ma.GetRows();
    
    TraceOn(nome);
    
    if (ncola != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");
      
    Free();
    Dim = nrowa;
    V   = AllocVector(Dim);

    pv  = V;
    pv1 = v1.V;
    pma = ma.GetMatrixPtr();

    for (i = 0; i < nrowa; i++, pma += ncola)
        *pv++ = scalv(pma, pv1, v1.Dim);

    TraceOff(nome);
    return(*this);
    }

// v = ma * v1  (matriz simetrica)

/*******************************************************************************
* Esta versao funciona corretamente, mas a que esta' a seguir e' melhor

acVector operator*(acSymMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::operator*";
    long i, j, inc, inca;
    double huge *pv, huge *pma, huge *pmac, huge *pv1, huge *pv1a;
    long nrowa = ma.GetRows();

    TraceOn(nome);

    if (nrowa != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");


    acVector v(nrowa);
    pv  = v.V;
    pv1 = v1.V;
    pma = ma.GetSymMatrixPtr();
    inc = nrowa - 1;

    for (i = 0; i < nrowa; i++, pma++)
        {
        // contribuicao da parte sub-diagonal
        inca = inc;
        *pv  = 0.0;
        pv1a = pv1;
        pmac = pma;
        for (j = 0; j < i; j++)
            {
            *pv +=  *pmac * *pv1a;
            pmac += inca--;
            pv1a++;
            }

        // contribuicao da parte supra-diagonal
        *pv += scalv(pmac, pv1a, nrowa - i);
        pv++;
        }

    TraceOff(nome);
    return(v);
    }
*******************************************************************************
*/

acVector operator*(acSymMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::operator*";
    long i, j;
    double huge *pv, huge *pvc, huge *pv1, huge *pv1c, huge *pma;
    long nrowa = ma.GetRows();

    TraceOn(nome);

    if (nrowa != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");

    acVector v(nrowa);
    pv  = v.V;
    pv1 = v1.V;
    pma = ma.GetSymMatrixPtr();

    pvc = pv;
    for (i = 0; i < nrowa; i++)    // zera vetor resultado
        *pvc++ = 0;

    for (i = 0; i < nrowa; i++, pv++, pv1++)
        {
        pv1c = pv1;
        pvc  = pv;
        *pvc += *pma * *pv1c;
        pv1c++;
        pvc++;
        pma++;

        for (j = i + 1; j < nrowa; j++, pma++, pvc++, pv1c++)
            {
            *pv  += *pma * *pv1c;
            *pvc += *pma * *pv1;
            }
        }

    TraceOff(nome);
    return(v);
    }

/*******************************************************************************
* Esta versao funciona corretamente, mas a que esta' a seguir e' melhor

acVector& acVector::Prod(acSymMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::Prod";
    long i, j, inc, inca;
    double huge *pv, huge *pma, huge *pmac, huge *pv1, huge *pv1a;
    long nrowa = ma.GetRows();

    TraceOn(nome);

    if (nrowa != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");

    Free();
    Dim = nrowa;
    V   = AllocVector(Dim);

    pv  = V;
    pv1 = v1.V;
    pma = ma.GetSymMatrixPtr();
    inc = nrowa - 1;

    for (i = 0; i < nrowa; i++, pma++)
        {
        // contribuicao da parte sub-diagonal
        inca = inc;
        *pv  = 0.0;
        pv1a = pv1;
        pmac = pma;
        for (j = 0; j < i; j++)
            {
            *pv +=  *pmac * *pv1a;
            pmac += inca--;
            pv1a++;
            }

        // contribuicao da parte supra-diagonal
        *pv += scalv(pmac, pv1a, nrowa - i);
        pv++;
        }

    TraceOff(nome);
    return(*this);
    }
*******************************************************************************
*/

acVector& acVector::Prod(acSymMatrix &ma, acVector &v1)
    {
    char *nome = "acVector::Prod";
    long i, j;
    double huge *pv, huge *pvc, huge *pv1, huge *pv1c, huge *pma;
    long nrowa = ma.GetRows();

    TraceOn(nome);

    if (nrowa != v1.Dim)
        Error(2, 1, "Incorrect Dimension for Matrix or Vector.");

    Free();
    Dim = nrowa;
    V   = AllocVector(Dim);

    pv  = V;
    pv1 = v1.V;
    pma = ma.GetSymMatrixPtr();


    pvc = pv;
    for (i = 0; i < nrowa; i++)    // zera vetor resultado
        *pvc++ = 0;

    for (i = 0; i < nrowa; i++, pv++, pv1++)
        {
        pv1c = pv1;
        pvc  = pv;
        *pvc += *pma * *pv1c;
        pv1c++;
        pvc++;
        pma++;

        for (j = i + 1; j < nrowa; j++, pma++, pvc++, pv1c++)
            {
            *pv  += *pma * *pv1c;
            *pvc += *pma * *pv1;
            }
        }

    TraceOff(nome);
    return(*this);
    }


