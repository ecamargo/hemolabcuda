#include "acVector.h"

// c = a - b;
acVector operator-(acVector& v2, acVector& v3)
    {
    char *nome = "operator-";
    TraceOn(nome);

    EqualSize(v2, v3);

    acVector v1(v2.Dim);
    subtvet(v1.V, v2.V, v3.V, v1.Dim);

    TraceOff(nome);
    return(v1);
    }

// c.Subt(a, b);
acVector& acVector::Subt(acVector& v2, acVector& v3)
    {
    char *nome = "acVector::Subt";
    TraceOn(nome);

    EqualSize(v2, v3);

    if (Dim != v2.Dim)
        {
        Free();
        Dim = v2.Dim;
        V   = AllocVector(Dim);
        }

    subtvet(V, v2.V, v3.V, Dim);

    TraceOff(nome);
    return(*this);
    }

// a -= b;
acVector& acVector::operator-=(acVector& v2)
    {
    register long i;
    char *nome = "acVector::operator-=";
    TraceOn(nome);

    EqualSize(*this, v2);
    double huge *p1 = V,
           huge *p2 = v2.V;

    for (i = 0; i < Dim; i++, p1++, p2++)
        *p1 -= *p2;

    TraceOff(nome);
    return *this;
    }

// a.Subt(b);
acVector& acVector::Subt(acVector& v2)
    {
    register long i;
    char *nome = "acVector::Subt";
    TraceOn(nome);

    EqualSize(*this, v2);
    double huge *p1 = V,
           huge *p2 = v2.V;

    for (i = 0; i < Dim; i++, p1++, p2++)
        *p1 -= *p2;

    TraceOff(nome);
    return *this;
    }
