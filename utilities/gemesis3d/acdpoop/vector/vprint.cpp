#include "acVector.h"

// imprime vetor em um arquivo
// Ex.: v.Print(fp, "%7.2f\n");
//      v.Print(fp);
void acVector::Print(FILE *fp, char *tit, char *fmt)
    {
    char *format;

    format = (fmt) ? fmt : (char *) "%f\n";

    if (tit)
        fprintf(fp, "%s\n", tit);

    register long i;
    double huge *aux = V;

    for (i = 0; i < Dim; i++)
        fprintf(fp, format, *aux++);
    fprintf(fp, "\n");
    }
