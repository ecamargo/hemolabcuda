// 29/10/96
#include "acVector.h"

void acVector::Restore(FILE *fp)
    {
    char *nome = "acVector::Restore";

    TraceOn(nome);
    ObjectExists();  // verifica se o objeto existe
    gFRead((char huge *) &Dim, sizeof(long), 1, fp);
    V = AllocVector(Dim);
    gFRead((char huge *) V, sizeof(double), Dim, fp);
    TraceOff(nome);
    }
    

void acVector::Restore(char *nametab, int version, char *nomefile)
    {
    char *nome = "acVector::Restore";
    char buf[100];
    FILE *fp;

    TraceOn(nome);

    ObjectExists();  // verifica se o objeto existe

    if ((fp = OpenRestore(nametab, version, nomefile)) == NULL)
        { //   13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
        GetError(13, buf);
        Error(FATAL_ERROR, 3, buf, nametab, version, nomefile);
        }

    Restore(fp);
    CloseRestore(fp);

    TraceOff(nome);
    }
