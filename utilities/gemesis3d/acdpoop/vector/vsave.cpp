#include "acVector.h"

// grava vetor em arquivo

void acVector::Save(FILE *fp)
    {
    gFWrite((char *) &Dim, sizeof(long),   1,   fp);
    gFWrite((char *) V,    sizeof(double), Dim, fp);
    }

void acVector::Save(char *objname, int version, char *filename)
    {
    char *nome = "acVector::Save";
    FILE *fp;

    TraceOn(nome);

    fp = OpenSave(objname, version, filename);
    Save(fp);
    CloseSave(fp, filename);

    TraceOff(nome);
    }
