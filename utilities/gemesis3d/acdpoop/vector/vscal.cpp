#include "acVector.h"

double acVector::operator^(acVector& v2)
    {
    char *nome = "acVector::operator^";
    double d;
    TraceOn(nome);

    EqualSize(*this, v2);
    d = scalv(V, v2.V, Dim);

    TraceOff(nome);
    return(d);
    }

double acVector::Scal(acVector& v2)
    {
    char *nome = "acVector::Scal";
    double d;

    TraceOn(nome);
    EqualSize(*this, v2);
    d = scalv(V, v2.V, Dim);

    TraceOff(nome);
    return(d);
    }
