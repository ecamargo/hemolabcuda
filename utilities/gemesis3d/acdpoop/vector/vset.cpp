#include "acVector.h"

acVector& acVector::Set(double v, double inc)
    {
    double huge *aux = V;
    long i;

    for (i = 0; i < Dim; i++, v += inc)
        *aux++ = v;

    return *this;
    }

