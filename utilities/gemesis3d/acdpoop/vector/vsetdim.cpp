#include "acVector.h"

// Fixa dimensao para vetor
void acVector::SetDim(long t)
    {
    char *nome = "acVector::SetDim";
    
    TraceOn(nome);
    
    ObjectExists();  // verifica se o objeto existe
    VectorDimTest(t);
    
    Dim = t;
    V = AllocVector(Dim);
    
    TraceOff(nome);
    }
