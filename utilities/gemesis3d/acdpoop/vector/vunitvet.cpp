#include "acVector.h"

acVector& acVector::UnitVector(acVector& v2)
    {
    if (Dim != v2.Dim)
        {
        Free();
        Dim = v2.Dim;
        V   = AllocVector(Dim);
        }

    double norma = v2.EuclidianNorm();

    long i;
    double huge *p1 = V;
    double huge *p2 = v2.V;

    for (i = 0; i < Dim; i++)
        *p1++ = *p2++ / norma;

    return *this;
    }
