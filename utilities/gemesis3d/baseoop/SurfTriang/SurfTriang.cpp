#include "SurfTriang.h"

SurfTriang::SurfTriang()
{ 
	prev = next = NULL;
  this->isNew = 0;	
}

void SurfTriang::SetData(long N1, long N2, long N3, int g,
                   SurfTriang *V1, SurfTriang *V2, SurfTriang *V3)
{
	gr = g;
	n1 = N1; n2 = N2; n3 = N3;
	v1 = V1; v2 = V2; v3 = V3;
	flg = 0;
}
