#include <stdio.h>
#include "vtkPoints.h"
#include "vtkUnstructuredGrid.h"
#include "coord3d.h"
#include "matematc.h"
#include "acdp.h"
#include "rotation.h"

///////////////////////////////////////////////////////////////////////
// Constructors and Destructors
acCoordinate3D::acCoordinate3D(long n)
{
	//printf("Entrou em acCoordinate3D::acCoordinate3D(long n)\n");
  static char *nome = "acCoordinate3D::acCoordinate3D";
  char buf[100];

  TraceOn(nome);

  if (n <= 0)
      { // 20, "Number Of Nodes <= 0."
      GetError(20, buf);
      Error(FATAL_ERROR, 1, buf);
      }

  NumNodes = n;

  vc = (acPoint3 *) mMalloc(sizeof(acPoint3) * NumNodes);

  if (!vc)
      ErrMem();

  TraceOff(nome);
}

acCoordinate3D::~acCoordinate3D()
{
	if (NumNodes) mFree((char *) vc);
}



acLimit3 acCoordinate3D::Box()
    {
    return(GetLimit3(vc, NumNodes));
    }

///////////////////////////////////////////////////////////////////////
// Save Method   
void acCoordinate3D::Save(char *nametab, int version, char *nomefile)
    {
    static char *nome = "acCoordinate3D::Save";
    FILE *fp;

    TraceOn(nome);

    fp = OpenSave(nametab, version, nomefile);
    gFWrite((char  *) &NumNodes, sizeof(long),     1,        fp);
    gFWrite((char  *) vc,        sizeof(acPoint3), NumNodes, fp);
    CloseSave(fp, nomefile);

    TraceOff(nome);
    }    

void acCoordinate3D::DXFOut(FILE *fp)
    {
    char *nome  = "acCoordinate3D::DXFOut";
    TraceOn(nome);
    for (long i=0; i<NumNodes; i++)
        {
        fprintf(fp,"VERTEX\n  8\n0\n");
				fprintf(fp," 10\n%lf\n 20\n%lf\n 30\n%lf\n 70\n 192\n  0\n",
               vc[i].x, vc[i].y, vc[i].z);
        }
    TraceOff(nome);
    }


///////////////////////////////////////////////////////////////////////
// Print Methods   
void acCoordinate3D::Print(FILE *fp)
    {
    fprintf(fp, "\n*COORDINATES\n %ld\n",NumNodes);
    for (long i = 0; i<NumNodes; i++)
        fprintf(fp, " %ld %lg %lg %lg\n",
                i+1, vc[i].x, vc[i].y, vc[i].z);
    }    
    
///////////////////////////////////////////////////////////////////////
// Copy coordinates do vtkPoints
void acCoordinate3D::CopyCoordinates(vtkPoints *points)
{
  for (long i = 0; i<NumNodes; i++)
		points->InsertNextPoint(vc[i].x, vc[i].y, vc[i].z);    
}    
        
    
///////////////////////////////////////////////////////////////////////
// Operators   
acCoordinate3D & acCoordinate3D::operator=(const acCoordinate3D &c)
{
	cout << "acCoordinate3D & acCoordinate3D::operator=(const acCoordinate3D &c)" << endl;
  static char *nome = "acCoordinate3D::operator=";
  char buf[100];

  TraceOn(nome);

  acPoint3   *src,   *dst;

  if (NumNodes != c.NumNodes)
      { //   21, "Coordinates With Different Sizes."
      GetError(21, buf);
      Error(WARNING, 1, buf);
      }

  mFree(vc);

  NumNodes = c.NumNodes;

  vc = (acPoint3   *) mMalloc(sizeof(acPoint3) * NumNodes);
  if (!vc)
      ErrMem();

  src = c.vc;
  dst = vc;

  for (long i = 0; i < NumNodes; i++)
      *dst++ = *src++;

  TraceOff(nome);
  return(*this);
}

// Operators   
void acCoordinate3D::Copy(acCoordinate3D *c)
{
  this->NumNodes = c->NumNodes;
  
  this->vc = (acPoint3   *) mMalloc(sizeof(acPoint3) * this->NumNodes);
  if (!this->vc) ErrMem();

  for (int i = 0; i < NumNodes; i++)
  	{
  	this->vc[i].x = c->vc[i].x;
  	this->vc[i].y = c->vc[i].y;
  	this->vc[i].z = c->vc[i].z;  	  	
  	}
}


///////////////////////////////////////////////////////////////////////
// Error Treatment      
void acCoordinate3D::ErrMem()
    { //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
    }


///////////////////////////////////////////////////////////////////////
// Rotations      
void acCoordinate3D::Rotate(acCoordinate3D &coor, acRotation *t)
{  
	long n;
	static char *nome="acCoordinate3D::Rotate";
	
	TraceOn(nome);
	
	if(NumNodes != coor.NumNodes)
	    Error(FATAL_ERROR, 1, "Objects of different size");
	    
	for (n = 0; n < NumNodes; n++)
	   {
	   vc[n] = t->GetXYZ((acPoint3 &)coor[n]);
	   }
	
	TraceOff(nome);
}    

///////////////////////////////////////////////////////////////////////
// Restore Method      
void acCoordinate3D::Restore(char *nametab, int version, char *nomefile)
{
  static char *nome = "acCoordinate3D::Restore";
  char buf[100];

  TraceOn(nome);

  if (NumNodes)
      { //  22, "An Object Of Class |%s| Exists And Will Be Eliminated."
      GetError(22, buf);
      Error(WARNING, 2, buf, "acCoordinate3D");
      Free();
      }

  FILE *fp;

  if ((fp = OpenRestore(nametab, version, nomefile)) == NULL)
      { //   13, "Can't Find Object |%s|, Version |%d| in Data Base |%s|."
      GetError(13, buf);
      Error(FATAL_ERROR, 3, buf, nametab, version, nomefile);
      }

  gFRead((char   *) &NumNodes, sizeof(long), 1, fp);

  vc = (acPoint3   *) mMalloc(sizeof(acPoint3) * NumNodes);
  if (!vc)
      ErrMem();

  gFRead((char   *) vc, sizeof(acPoint3), NumNodes, fp);

  CloseRestore(fp);

  TraceOff(nome);
}    

    

///////////////////////////////////////////////////////////////////////
// Read Methods
int acCoordinate3D::Read(FILE *fp)
    {
    static char *kcoord = "COORDINATES";
    static char *nome = "acCoordinate3D::Read";
    char buf[100];

    TraceOn(nome);

    if (!FindKeyWord(kcoord, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, kcoord);
        }

    if (NumNodes)
        { //   22, "An Object Of Class |%s| Exists And Will Be Eliminated."
        GetError(22, buf);
        Error(WARNING, 2, buf, "acCoordinate3D");
        Free();
        }

    fscanf(fp, "%ld", &NumNodes);
    if (NumNodes < 1)
        { //   20, "Number Of Nodes <= 0."
        vc = NULL;
				return 0;        	
        //GetError(20, buf);
        //Error(FATAL_ERROR, 3, buf);
        }

    vc = (acPoint3 *) mMalloc(sizeof(acPoint3) * NumNodes);
    if (!vc)
        ErrMem();

    ReadNodes(fp);

    TraceOff(nome);
    return 1;
    }

void acCoordinate3D::ReadNodes(FILE *fp)
{
	long dummy, i;
	double ax, ay, az;
	char buf[100];

	for (i = 0; i < NumNodes; i++)
    {
    fscanf(fp, "%ld %lf %lf %lf", &dummy, &ax, &ay, &az);
    
    if (dummy != i + 1)
        { //  23, "Incorrect Data For Node |%ld|."
        GetError(23, buf);
        Error(FATAL_ERROR, 4, buf, i + 1);
        }

    vc[i].x = ax;
    vc[i].y = ay;
    vc[i].z = az;
    }
}


///////////////////////////////////////////////////////////////////////
// Memory Allocation and deallocation
void acCoordinate3D::Free()
{
	if (NumNodes)
	    mFree((char   *) vc);
	
	NumNodes = 0;
}

void acCoordinate3D::Allocate(int n)
{
  this->NumNodes = n;
  vc = (acPoint3 *) mMalloc(sizeof(acPoint3) * NumNodes);
  if (!vc) ErrMem();     	
}

void acCoordinate3D::Read(vtkUnstructuredGrid *grid)
{
	// Desaloca memória se já existir algo alocado
	if(this->NumNodes) this->Free();
	double vet[3];
	
	int numNodes = grid->GetNumberOfPoints();
	this->Allocate(numNodes);	
	for(int i = 0; i < numNodes; i++)
	{
		grid->GetPoints()->GetPoint(i, (double *) vet);
		this->SetNode(i, vet);	
	}
}

void acCoordinate3D::SetNode(int index, double *vet)
{
	char buf[100];	
	if(index > this->NumNodes)
		{
		printf("Error: acCoordinate3D::SetNode()\n  Index out of range.\n");
    GetError(23, buf);
    Error(FATAL_ERROR, 4, buf, index);	     		
		}
	
	vc[index].x = (double ) vet[0];
	vc[index].y = (double ) vet[1];
	vc[index].z = (double ) vet[2];		
}

void acCoordinate3D::GetNode(int index, double *vet)
{
	char buf[100];	
	if(index >= this->NumNodes)
		{
			printf("Error: acCoordinate3D::GetNode()\n  Index out of range.\n");
	    GetError(23, buf);
      Error(FATAL_ERROR, 4, buf, index);	     		
		}
	
	vet[0] = vc[index].x;
	vet[1] = vc[index].y;
	vet[2] = vc[index].z;		
}
