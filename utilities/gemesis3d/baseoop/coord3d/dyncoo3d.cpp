/*
** dyncoo3D.cpp - Enzo, Fancello, Raul, Venere & Salgado may 1992
*/

#include "vtkUnstructuredGrid.h"
#include "dyncoo3d.h"
#include "acdp.h"

acDynCoord3D::acDynCoord3D(long nod)
    {
    NumNodes= nod;
    NNosMax = NumNodes + COORD_GROWTH;

    vc = (acPoint3   *) mMalloc(NNosMax * sizeof(acPoint3));
    if (!vc)
        Error(FATAL_ERROR, 4, "Insufficient Memory for coordinates");
    }

void acDynCoord3D::Read(vtkUnstructuredGrid *grid)
{
	// Set coordenadas dos pontos já lidos e armazenados em um meshdata

	this->NumNodes = grid->GetNumberOfPoints();
  this->NNosMax = this->NumNodes + COORD_GROWTH;		

  vc = (acPoint3   *) mMalloc(NNosMax * sizeof(acPoint3));
  if (!vc)
		Error(FATAL_ERROR, 4, "Insufficient Memory for coordinates");

	double *vec;
  for (int i = 0; i < this->NumNodes; i++)
		{
		vec = grid->GetPoints()->GetPoint(i);
//		fscanf(fp, "%ld %lf %lf %lf", &dummy, &x, &y, &z);
		vc[i].x = vec[0];
		vc[i].y = vec[1];
		vc[i].z = vec[2];
		}
}

void acDynCoord3D::Read(FILE *fp)
    {
    static char *nome = "acDynCoord3D::Read";
    register long i;
    char buf[80];
    long dummy;
    static char *palcha = "COORDINATES";

    TraceOn(nome);

    if (NumNodes)
        {
        Error(WARNING, 1, "An object of class Coordinate exist and will be eliminated");
        Free();
        }

    if (!FindKeyWord(palcha, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 2, buf, palcha);
        }

    fscanf(fp, "%ld", &NumNodes);
    if (NumNodes < 1)
        Error(FATAL_ERROR, 3, "Number of nodes negative or null");
    NNosMax = NumNodes + COORD_GROWTH;

    vc = (acPoint3   *) mMalloc(NNosMax * sizeof(acPoint3));
    if (!vc)
        Error(FATAL_ERROR, 4, "Insufficient Memory for coordinates");

    for (i = 0; i < NumNodes; i++)
        {
        double x, y, z;
        fscanf(fp, "%ld %lf %lf %lf", &dummy, &x, &y, &z);
        if (dummy != i + 1)
            {
            sprintf(buf,
                "Coordinate::Read -> Incorrect data for node %ld", i + 1);
            Error(FATAL_ERROR, 4, buf);
	    }
        vc[i].x = x;
        vc[i].y = y;
        vc[i].z = z;
        }

    TraceOff(nome);
    }

void acDynCoord3D::Free()
    {
    NumNodes = NNosMax = 0;
    mFree(vc);
    vc = NULL;
    }

/*
void acDynCoord3D::Restore(FILE *fp)
    {
    fread((char *) &NumNodes, sizeof(long), 1, fp);

    NNosMax = NumNodes + COORD_GROWTH;
    vc = (acPoint3   *) mMalloc(NNosMax * sizeof(acPoint3));
    gfread((char   *) vc, sizeof(acPoint3), NumNodes, fp);
    }
*/

void acDynCoord3D::AddCoord (acPoint3 &point)
    {
    if (NumNodes >= NNosMax)
	{
	long tam,i;
	NNosMax += COORD_GROWTH;
	tam = NNosMax * sizeof(acPoint3);

	acPoint3   *New_vc = (acPoint3   *) mMalloc(tam);
	if (!New_vc)
	    {
	    char buf[80];
	    sprintf(buf, "Memory exhausted for DynCoord");
	    Error(FATAL_ERROR, 1, buf);
	    }
	for(i = 0; i<NumNodes; i++)
            New_vc[i] = vc[i];
        mFree(vc);
        vc = New_vc;
	}
    vc[NumNodes] = point;
    NumNodes++;
    }

void acDynCoord3D::RemoveNodes(long   *aux)
    {
    long i,k,ufa;
    ufa = NumNodes;
    for (k=i=0; i<ufa; i++)
	{
	if (aux[i])
	    {
	    vc[k]=vc[i];
	    aux[i] = k++;
	    }
	else
	    {
	    aux[i] = -1;
	    NumNodes--;
	    }
	}
    }

void acDynCoord3D::MergeNodes(double cotap, long   *aux)
    {
    long   *auxn;
    long exist,nn,i;
    double cota;
    double dx,dy,dz;
    auxn = (long   *)mMalloc(sizeof(long)*NumNodes);
    acLimit3 vol=Box();

    dx = vol.xmax-vol.xmin;
    dy = vol.ymax-vol.ymin;
    dz = vol.zmax-vol.zmin;
    cota = (dx*dx+dy*dy+dz*dz)*cotap;
    auxn[0] = 0;
    nn = 1;
    for (i=1; i<NumNodes; i++)
        {
        exist=-1;
        if (aux[i])
            {
            for (long j=0; j<i; j++)
                {
                if (aux[j])
                    {
                    dx = vc[i].x - vc[j].x;
                    dy = vc[i].y - vc[j].y;
                    dz = vc[i].z - vc[j].z;
                    if (dx*dx+dy*dy+dz*dz < cota)
                        {
                        exist=j;
                        break;
                        }
                    }
                }
            }
        if (exist==-1)
            auxn[i]=nn++;
        else
            auxn[i]=auxn[exist];
        }

    for (i=0; i<NumNodes; i++)
        {
        vc[auxn[i]] = vc[i];
        }

    for (i=0; i<NumNodes; i++)
        aux[i] = auxn[i];

    NumNodes = nn;
    mFree(auxn);
    }

void acDynCoord3D::Renumber(long   *NuevaNum)
    {
    long nod=0;
    long i;
    for (i=0; i<NumNodes; i++)
        {
        if (NuevaNum[i] == i)
            {
            vc[nod] = vc[i];
            NuevaNum[i] = nod++;
            }
        }
    NumNodes = nod;
    }
