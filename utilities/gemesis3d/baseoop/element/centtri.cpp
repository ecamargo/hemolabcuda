#include "tri3.h"

acPoint3 CenterTri(acPoint3   *Coords)
    {
    acPoint3 center;
    center.x = (Coords[0].x + Coords[1].x + Coords[2].x) / 3.0;
    center.y = (Coords[0].y + Coords[1].y + Coords[2].y) / 3.0;
    center.z = (Coords[0].z + Coords[1].z + Coords[2].z) / 3.0;
    return(center);
    }
