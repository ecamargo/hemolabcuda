#include "hexah8.h"
#include <math.h>

// Auxiliar matrix for incidence of each triangular sub-facet,
// the first index gives the facet, the 2nd gives the sub-facet,
// and the 3rd. the incidence itself.
// Also used to give the incidence of each facet.

int Hexah8::maux[6][2][3] = {{{1,2,0},{3,0,2}},
                             {{5,4,6},{7,6,4}},
                             {{2,1,6},{5,6,1}},
                             {{2,6,3},{7,3,6}},
                             {{0,3,4},{7,4,3}},
                             {{0,4,1},{5,1,4}} };

Hexah8::Hexah8()
    {
    Incid  = (long *)     mMalloc(8 * sizeof(long));
    Coords = (acPoint3 *) mMalloc(8 * sizeof(acPoint3));
    }

Hexah8::~Hexah8()
    {
    mFree(Incid);
    mFree(Coords);
    }

    
double Hexah8::Volume()
    {
    return (0);
    }


acPoint3 Hexah8::TriFacNormal(int facetnum, int numtri)
    {
    acPoint3 v[3];
    v[0]      = Coords[maux[facetnum][numtri][0]];
    v[1]      = Coords[maux[facetnum][numtri][1]];
    v[2]      = Coords[maux[facetnum][numtri][2]];
    return (NormalTri(v));
    }

void Hexah8::TriFacInf(int facetnum,    int trinum,
                    long *triangincid, int *triangedges)
    {
    triangincid[0] = Incid[maux[facetnum][trinum][0]];
    triangincid[1] = Incid[maux[facetnum][trinum][1]];
    triangincid[2] = Incid[maux[facetnum][trinum][2]];

    triangedges[0]  = 1;
    triangedges[1]  = 0;
    triangedges[2]  = 1;
    }


void Hexah8::SetData(long   *incid, acCoordinate3D &c)
    {
    for (int i = 0 ; i < 8; i++)
        {
        Incid[i]  = incid[i];
        Coords[i] = c[incid[i]];
        }
    }

void Hexah8::SetData(long   *incid)
    {
    for (int i = 0 ; i < 8; i++)
        Incid[i]  = incid[i];
    }


double Hexah8::Quality()
    {
    int   ne;
    double perim2;
    double dist, dista;

    acPoint3 cg = GravCenter();

    for (ne = 0, perim2 = 0.; ne < 12; ne++)  // loop over edges
        {
        perim2 += EdgeLength2(ne);
        }

    dist = (cg.x - Coords[0].x) * (cg.x - Coords[0].x) +
           (cg.y - Coords[0].y) * (cg.y - Coords[0].y) +
           (cg.z - Coords[0].z) * (cg.z - Coords[0].z);
    for (ne = 0; ne < 6; ne++)  // loop over facets
        {
        double a, b, c, d;
        double x21 = Coords[maux[ne][0][2]].x - Coords[maux[ne][0][0]].x;
        double y21 = Coords[maux[ne][0][2]].y - Coords[maux[ne][0][0]].y;
        double z21 = Coords[maux[ne][0][2]].z - Coords[maux[ne][0][0]].z;
        double x31 = Coords[maux[ne][0][1]].x - Coords[maux[ne][0][0]].x;
        double y31 = Coords[maux[ne][0][1]].y - Coords[maux[ne][0][0]].y;
        double z31 = Coords[maux[ne][0][1]].z - Coords[maux[ne][0][0]].z;

        a = y21 * z31 - y31 * z21;
        b = z21 * x31 - z31 * x21;
        c = x21 * y31 - x31 * y21;
        d = -(a * Coords[maux[ne][0][0]].x + b * Coords[maux[ne][0][0]].y +
              c * Coords[maux[ne][0][0]].z);
        dista = a * cg.x + b * cg.y + c * cg.z + d;

        dista = dista * dista / (a * a + b * b + c * c);
        if (dista < dist) dist = dista;
        }
    return(48.0 * (dist / perim2));
    }

double Hexah8::Perimeter()
    {
    int i;
    double perim = 0.0;

    for ( i = 0; i < 12; i++)
        perim += sqrt(EdgeLength2(i));
    return (perim);
    }


char *Hexah8::ElementName(void)
    {
    return ("Trilinear Hexahedron");
    }

char *Hexah8::AdditionalInfo(void)
    {
    return ("Not implemented");
    }


acPoint3 Hexah8::GravCenter()
    {
    acPoint3 g;
    int i;

    g.x = g.y = g.z = 0.0;

    for (i = 0; i < 8; i++)
        {
        g.x += Coords[i].x;
        g.y += Coords[i].y;
        g.z += Coords[i].z;
        }

    g.x = g.x / 8.0;
    g.y = g.y / 8.0;
    g.z = g.z / 8.0;
    return(g);
    }

acPoint3 Hexah8::FacetNormal(int facetnum)
    {
    acPoint3 n1,n2;
    double mod;
    n1 = TriFacNormal(facetnum, 0);
    n2 = TriFacNormal(facetnum, 1);
    n1.x += n2.x;
    n1.y += n2.y;
    n1.z += n2.z;
    mod = sqrt(n1.x*n1.x+n1.y*n1.y+n1.z*n1.z);
    n1.x /= mod; n1.y /= mod; n1.z /= mod;
    return(n1);
    }     
        

void Hexah8::FacInf(int facetnum, long *nodes)
    {
    nodes[0] = Incid[maux[facetnum][0][0]];
    nodes[1] = Incid[maux[facetnum][0][1]];
    nodes[2] = Incid[maux[facetnum][0][2]];
    nodes[3] = Incid[maux[facetnum][1][0]];
    }        
    

acPoint3 Hexah8::FacetCenter(int facetnum)
    {
    acPoint3 center;
    center.x = (Coords[maux[facetnum][0][0]].x +
                Coords[maux[facetnum][0][1]].x +
                Coords[maux[facetnum][0][2]].x +
                Coords[maux[facetnum][1][0]].x) / 4.0;
    center.y = (Coords[maux[facetnum][0][0]].y +
                Coords[maux[facetnum][0][1]].y +
                Coords[maux[facetnum][0][2]].y +
                Coords[maux[facetnum][1][0]].y) / 4.0;
    center.z = (Coords[maux[facetnum][0][0]].z +
                Coords[maux[facetnum][0][1]].z +
                Coords[maux[facetnum][0][2]].z +
                Coords[maux[facetnum][1][0]].z) / 4.0;
    return(center);
    }

double Hexah8::FacetArea(int facetnum)
    {
    double area;
    acPoint3 v[3];

    v[0] = Coords[maux[facetnum][0][0]];
    v[1] = Coords[maux[facetnum][0][1]];
    v[2] = Coords[maux[facetnum][0][2]];
    area = AreaTri(v);
    v[0] = Coords[maux[facetnum][1][0]];
    area += AreaTri(v);

    return(area);
    }    



double Hexah8::EdgeLength2(int edgenum)
    {
    static int edaux[12][2] =
        {
        {0, 1}, {1, 2}, {2, 3}, {3, 0},
        {4, 5}, {5, 6}, {6, 7}, {7, 4},
        {0, 4}, {1, 5}, {2, 6}, {3, 7}
        };

    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;
    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }


double Hexah8::Area()
    {
    int i;
    double s = 0;

    for (i = 0; i < 6; i++)
        s += FacetArea(i);
    return(s);
    }
    
