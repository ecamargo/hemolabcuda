/* 15:27:27  11/23/1992
** libelem.cpp - november 1992 - Raul & Salgado
*/

/*
** O conteudo deste arquivo devera' ser alterado sempre que um novo
** elemento for criado, e o header deste novo elemento devera' ser incluido.
*/

#include "element.h"
#include "tri3.h"
#include "tri6.h"
#include "quad4.h"
#include "tetra4.h"
#include "hexah8.h"
#include "tetra10.h"
//#include <new.h>

static void erroaloc(void);

// cria um novo elemento a partir do nome do elemento ou NULL em caso
// de erro

Element *NewElement(char *ElemName)
    {
    Element *NewElem;
    int error = 1;

    /*
    ** compara com os tipos de elementos existentes
    */
    if (StrCmp(ElemName, "Tri3") == 0)   // tri 3 nos
        NewElem = new Tri3;

    else if (StrCmp(ElemName, "LINEAR_TRIANGLE") == 0)   // tri 3 nos
        NewElem = new Tri3;

    else if (StrCmp(ElemName, "Tri6") == 0)  // tri 6 nos
        NewElem = new Tri6;

    else if (StrCmp(ElemName, "Quad4") == 0)  // cuad 4 nos
        NewElem = new Quad4;

    else if (StrCmp(ElemName, "Hexah8") == 0) // hexahedro de 8 nos
        NewElem = new Hexah8;

    else if (StrCmp(ElemName, "LINEAR_CUBE") == 0) // hexahedro de 8 nos
        NewElem = new Hexah8;

    else if (StrCmp(ElemName, "Tetra4") == 0)   // tetrahedro de 4 nos
        NewElem = new Tetra4;

    else if (StrCmp(ElemName, "LINEAR_TETRAHEDRUM") == 0)   // tetrahedro de 4 nos
        NewElem = new Tetra4;

    else if (StrCmp(ElemName, "Tetra10") == 0)   // tetrahedro de 10 nos
        NewElem = new Tetra10;

    else if (StrCmp(ElemName, "QUADRATIC_TETRAHEDRUM") == 0)   // tetrahedro de 10 nos
        NewElem = new Tetra10;

    else
        {
        error = 0;
        NewElem = NULL;
        }

    if (error && !NewElem)
        Error(2, 1, "Free store exhausted for Element");
    return(NewElem);
    }
