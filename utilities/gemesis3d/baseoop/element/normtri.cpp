#include "element.h"
#include <math.h>

acPoint3 NormalTri(acPoint3   *Coords)
    {
    double x10, x20, y10, y20, z10, z20, nx, ny, nz, n2, normn;
    acPoint3 normal;
    x10 = Coords[1].x - Coords[0].x;
    x20 = Coords[2].x - Coords[0].x;
    y10 = Coords[1].y - Coords[0].y;
    y20 = Coords[2].y - Coords[0].y;
    z10 = Coords[1].z - Coords[0].z;
    z20 = Coords[2].z - Coords[0].z;
    nx  = y10 * z20 - y20 * z10;
    ny  = z10 * x20 - z20 * x10;
    nz  = x10 * y20 - x20 * y10;
    n2 = nx * nx + ny * ny + nz * nz;
    if (n2 <= 0.0)
        {
        Error(1, 2, "Face with null area");
        normal.x = 1.0;
        normal.y = 0.0;
        normal.z = 0.0;
        }
    else
        {
        normn = sqrt(n2);
        normal.x = nx / normn;
        normal.y = ny / normn;
        normal.z = nz / normn;
        }
    return(normal);
    }
