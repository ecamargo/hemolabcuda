#include "quad4.h"
#include <math.h>

// Auxiliar matrix for incidence of each triangular sub-facet,
// the first index gives the facet, the 2nd gives the sub-facet,
// and the 3rd. the incidence itself.
// Also used to give the incidence of each facet.

int Quad4::maux[2][3] = {{2,1,0},{0,3,2}};

Quad4::Quad4()
    {
    Incid  = (long *)     mMalloc(4 * sizeof(long));
    Coords = (acPoint3 *) mMalloc(4 * sizeof(acPoint3));
    }

Quad4::~Quad4()
    {
    mFree(Incid);
    mFree(Coords);
    }


acPoint3 Quad4::TriFacNormal(int /* facetnum */, int numtriang)
    {
    acPoint3 v[3];
    v[0]      = Coords[maux[numtriang][0]];
    v[1]      = Coords[maux[numtriang][1]];
    v[2]      = Coords[maux[numtriang][2]];
    return (NormalTri(v));
    }


void Quad4::TriFacInf(int /* facetnum */, int numtriang,
                      long *triangincid,  int *triangedges)
    {
    triangincid[0] = Incid[maux[numtriang][0]];
    triangincid[1] = Incid[maux[numtriang][1]];
    triangincid[2] = Incid[maux[numtriang][2]];

    triangedges[0]  = 1;
    triangedges[1]  = 1;
    triangedges[2]  = 0;
    }
    

void Quad4::SetData(long   *inci, acCoordinate3D &c)
    {
    for (int i = 0 ; i < 4; i++)
        {
        Incid[i]  = inci[i];
        Coords[i] = c[inci[i]];
        }
    }

void Quad4::SetData(long   *incid)
    {
    for (int i = 0 ; i < 4; i++)
        Incid[i]  = incid[i];
    }
    

double Quad4::Quality()
    {
    int   ne;
    double perim2 = 0;
    double laux, t, x, y, z, dist, dista;
    static int edaux[4][2] =
        {
        {0, 1}, {1, 2}, {2, 3}, {3, 0}
        };

    acPoint3 cg = GravCenter();

    dist = 1.0e+99;
    for (ne = 0; ne < 4; ne++)
        {
        laux = EdgeLength2(ne);
        perim2 += laux;
        x = Coords[edaux[ne][1]].x - Coords[edaux[ne][0]].x;
        y = Coords[edaux[ne][1]].y - Coords[edaux[ne][0]].y;
        z = Coords[edaux[ne][1]].z - Coords[edaux[ne][0]].z;
        t = ((cg.x - Coords[edaux[ne][0]].x) * x +
             (cg.y - Coords[edaux[ne][0]].y) * y +
             (cg.z - Coords[edaux[ne][0]].z) * z) / laux;

        x = Coords[edaux[ne][0]].x + t * x;
        y = Coords[edaux[ne][0]].y + t * y;
        z = Coords[edaux[ne][0]].z + t * z;

        dista = (cg.x - x) * (cg.x - x) +
                (cg.y - y) * (cg.y - y) +
                (cg.z - z) * (cg.z - z);

        if (dista < dist)
            dist = dista;
        }

    return( 16.0 * dist / perim2);
    }


double Quad4::Perimeter()
    {
    int i;
    double perim = 0.0;

    for (i = 0; i < 4; i++)
        perim += sqrt(EdgeLength2(i));
    return (perim);
    }


char *Quad4::ElementName(void)
    {
    return ("Bilinear Quadrilateral");
    }

char *Quad4::AdditionalInfo(void)
    {
    return ("Not implemented");
    }


acPoint3 Quad4::FacetNormal(int facetnum)
    {
    double mod;
    acPoint3 n, aux;
    n   = TriFacNormal(facetnum, 0);
    aux = TriFacNormal(facetnum, 1);
    n.x += aux.x; n.y += aux.y; n.z += aux.z;
    mod = sqrt(n.x*n.x+n.y*n.y+n.z*n.z);
    n.x /= mod; n.y /= mod; n.z /= mod;
    return(n);
    }


void Quad4::FacInf(int /* facetnum */, long *nodes)
    {
    for (int i = 0; i < 4; i++)
        nodes[i] = Incid[i];
    }


acPoint3 Quad4::FacetCenter(int /* facetnum */ )
    {
    acPoint3 center;
    center.x = (Coords[0].x + Coords[1].x + Coords[2].x + Coords[3].x) / 4.0;
    center.y = (Coords[0].y + Coords[1].y + Coords[2].y + Coords[3].y) / 4.0;
    center.z = (Coords[0].z + Coords[1].z + Coords[2].z + Coords[3].z) / 4.0;
    return(center);
    }


double Quad4::FacetArea(int /* facetnum */ )
    {
    double area;
    acPoint3 v[3];

    v[0] = Coords[0];
    v[1] = Coords[1];
    v[2] = Coords[2];
    area = AreaTri(v);
    v[1] = Coords[3];
    area += AreaTri(v);

    return(area);
    }


double Quad4::EdgeLength2(int edgenum)
    {
    static int edaux[4][2] =
        {
        {0, 1}, {1, 2}, {2, 3}, {3, 0}
        };

    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;
    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }

