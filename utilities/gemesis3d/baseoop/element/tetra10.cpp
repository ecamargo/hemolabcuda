#include "tetra10.h"
#include <math.h>

int Tetra10::maux[4][4][3] = {{{0,4,6},{1,5,4},{2,6,5},{4,5,6}},
                              {{0,7,4},{1,4,8},{3,8,7},{4,7,8}},
                              {{1,8,5},{2,5,9},{3,9,8},{5,8,9}},
                              {{0,6,7},{2,9,6},{3,7,9},{6,9,7}}};

Tetra10::Tetra10()
    {
    Incid  = (long   *)      mMalloc(10 * sizeof(long));
    Coords = (acPoint3   *)  mMalloc(10 * sizeof(acPoint3));
    }

Tetra10::~Tetra10()
    {
    mFree(Incid);
    mFree(Coords);
    }


void Tetra10::TriFacInf(int face, int tri, long *tinci, int *tedges)
    {
    tinci[0] = Incid[maux[face][tri][0]];
    tinci[1] = Incid[maux[face][tri][1]];
    tinci[2] = Incid[maux[face][tri][2]];
    tedges[1]= 0;
    if (tri == 3)
        {
        tedges[0]= 0;
        tedges[2]= 0;
        }
    else
        {
        tedges[0]= 1;
        tedges[2]= 1;
        }
    }


acPoint3 Tetra10::TriFacNormal(int facetnum, int trinum)
    {
    acPoint3 v[3];
    v[0]    = Coords[maux[facetnum][trinum][0]];
    v[1]    = Coords[maux[facetnum][trinum][1]];
    v[2]    = Coords[maux[facetnum][trinum][2]];
    return (NormalTri(v));
    }


double Tetra10::Volume()
    {
    double vol,a1,a2,a3,b1,b2,b3,c1,c2,c3,d1,d2,d3;
    a1 = Coords[1].x - Coords[0].x;
    b1 = Coords[1].y - Coords[0].y;
    c1 = Coords[1].z - Coords[0].z;
    a2 = Coords[2].x - Coords[0].x;
    b2 = Coords[2].y - Coords[0].y;
    c2 = Coords[2].z - Coords[0].z;
    a3 = Coords[3].x - Coords[0].x;
    b3 = Coords[3].y - Coords[0].y;
    c3 = Coords[3].z - Coords[0].z;
    d1 = b2*c3 - c2*b3;
    d2 = b3*c1 - c3*b1;
    d3 = b1*c2 - c1*b2;
    vol = (a1 * d1 + a2 * d2 + a3 * d3) / 6.0;
    return(vol);
    }


double Tetra10::Perimeter()
    {
    int i;
    double perim = 0.0;

    for (i = 0; i < 12; i++)
        perim += sqrt(EdgeLength2(i));

    return (perim);
    }


double Tetra10::Quality()
    {
    int   ne;
    double perim2 = 0;
    double dist, dista;

    acPoint3 cg = GravCenter();

    dist = 1.0e+99;
    for (ne = 0; ne < 12; ne++)  // loop over edges
        perim2 += EdgeLength2(ne);

    for (ne = 0; ne < 4; ne++)  // loop over facets
        {
        double a, b, c, d;
        double x21 = Coords[maux[ne][3][2]].x - Coords[maux[ne][3][0]].x;
        double y21 = Coords[maux[ne][3][2]].y - Coords[maux[ne][3][0]].y;
        double z21 = Coords[maux[ne][3][2]].z - Coords[maux[ne][3][0]].z;
        double x31 = Coords[maux[ne][3][1]].x - Coords[maux[ne][3][0]].x;
        double y31 = Coords[maux[ne][3][1]].y - Coords[maux[ne][3][0]].y;
        double z31 = Coords[maux[ne][3][1]].z - Coords[maux[ne][3][0]].z;

        a = y21 * z31 - y31 * z21;
        b = z21 * x31 - z31 * x21;
        c = x21 * y31 - x31 * y21;
        d = -(a * Coords[maux[ne][3][0]].x + b * Coords[maux[ne][3][0]].y +
              c * Coords[maux[ne][3][0]].z);
        dista = (a * cg.x + b * cg.y + c * cg.z + d) /
                sqrt(a * a + b * b + c * c);

        dista = dista * dista;
        if (dist > dista)
            dist = dista;
        }

    return(144.0 * (dist / perim2));
    }


void Tetra10::SetData(long   *incid, acCoordinate3D &c)
    {
    int i;

    for (i = 0 ; i < 10; i++)
        {
        Incid[i]  = incid[i];
        Coords[i] = c[incid[i]];
        }
    }

void Tetra10::SetData(long   *incid)
    {
    int i;
    for (i = 0 ; i < 10; i++)
        Incid[i]  = incid[i];
    }    


char *Tetra10::ElementName(void)
    {
    return ("Triquadratic Tetrahedron");
    }

char *Tetra10::AdditionalInfo(void)
    {
    return ("Not implemented");
    }

acPoint3 Tetra10::GravCenter()
    {
    acPoint3 g;
    int i;

    g.x = g.y = g.z = 0.0;

    for (i = 0; i < 10; i++)
        {
        g.x += Coords[i].x;
        g.y += Coords[i].y;
        g.z += Coords[i].z;
        }

    g.x = g.x * 0.1;
    g.y = g.y * 0.1;
    g.z = g.z * 0.1;
    return(g);
    }


void Tetra10::FacInf(int facetnum, long *nodes)
    {
    nodes[0] = Incid[maux[facetnum][0][0]];
    nodes[1] = Incid[maux[facetnum][1][0]];
    nodes[2] = Incid[maux[facetnum][2][0]];
    nodes[3] = Incid[maux[facetnum][3][0]];
    nodes[4] = Incid[maux[facetnum][3][1]];
    nodes[5] = Incid[maux[facetnum][3][2]];
    }
    

acPoint3 Tetra10::FacetCenter(int facetnum)
    {
    acPoint3 v[3];
    v[0] = Coords[maux[facetnum][3][0]];
    v[1] = Coords[maux[facetnum][3][1]];
    v[2] = Coords[maux[facetnum][3][2]];
    return(CenterTri(v));
    }


double Tetra10::FacetArea(int facetnum)
    {
    double acum;
    acPoint3 v[3];
    acum = 0.0;
    for (int i=0; i<4; i++)
        {
        v[0] = Coords[maux[facetnum][i][0]];
        v[1] = Coords[maux[facetnum][i][1]];
        v[2] = Coords[maux[facetnum][i][2]];
        acum += AreaTri(v);
        }
    return(acum);
    }
    

double Tetra10::EdgeLength2(int edgenum)
    {
    static int edaux[12][2] =
        {
        {0, 4}, {4, 1}, {1, 5}, {5, 2}, {2, 6}, {6, 0},
        {0, 7}, {7, 3}, {1, 8}, {8, 3}, {2, 9}, {9, 3}
        };
    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;
    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }    
    

double Tetra10::Area()
    {
    int i;
    double s = 0;

    for (i = 0; i < 4; i++)
        s += FacetArea(i);
    return(s);
    }
