#include "tetra4.h"
#include <math.h>

int Tetra4::maux[4][3] = {{2,0,1},
                          {1,0,3},
                          {2,1,3},
                          {3,0,2}};

Tetra4::Tetra4()
    {
    Incid  = (long   *)      mMalloc(4 * sizeof(long));
    Coords = (acPoint3   *)  mMalloc(4 * sizeof(acPoint3));
    }

Tetra4::~Tetra4()
    {
    mFree(Incid);
    mFree(Coords);
    }

    
double Tetra4::Volume()
    {
    double vol,a1,a2,a3,b1,b2,b3,c1,c2,c3,d1,d2,d3;
    a1 = Coords[1].x - Coords[0].x;
    b1 = Coords[1].y - Coords[0].y;
    c1 = Coords[1].z - Coords[0].z;
    a2 = Coords[2].x - Coords[0].x;
    b2 = Coords[2].y - Coords[0].y;
    c2 = Coords[2].z - Coords[0].z;
    a3 = Coords[3].x - Coords[0].x;
    b3 = Coords[3].y - Coords[0].y;
    c3 = Coords[3].z - Coords[0].z;
    d1 = b2*c3 - c2*b3;
    d2 = b3*c1 - c3*b1;
    d3 = b1*c2 - c1*b2;
    vol = (a1 * d1 + a2 * d2 + a3 * d3) / 6.0;
    return(vol);
    }
    

acPoint3 Tetra4::TriFacNormal(int facetnum, int /* trinum */ )
    {
    acPoint3 v[3];
    v[0]    = Coords[maux[facetnum][0]];
    v[1]    = Coords[maux[facetnum][1]];
    v[2]    = Coords[maux[facetnum][2]];
    return (NormalTri(v));
    }

void Tetra4::SetData(long   *incid, acCoordinate3D &c)
    {
    int i;

    for (i = 0 ; i < 4; i++)
        {
        Incid[i]  = incid[i];
        Coords[i] = c[incid[i]];
        }
    }

void Tetra4::SetData(long   *incid)
    {
    int i;
    for (i = 0 ; i < 4; i++)
        Incid[i]  = incid[i];
    }


double Tetra4::Quality()
    {
    int   ne;
    double perim2 = 0;
    double dist, dista;

    acPoint3 cg = GravCenter();

    dist = (cg.x - Coords[0].x) * (cg.x - Coords[0].x) +
           (cg.y - Coords[0].y) * (cg.y - Coords[0].y) +
           (cg.z - Coords[0].z) * (cg.z - Coords[0].z);
    for (ne = 0; ne < 6; ne++)  // loop over edges
        perim2 += EdgeLength2(ne);

    for (ne = 0; ne < 4; ne++)  // loop over facets
        {
        double a, b, c, d, denom;
        double x21 = Coords[maux[ne][2]].x - Coords[maux[ne][0]].x;
        double y21 = Coords[maux[ne][2]].y - Coords[maux[ne][0]].y;
        double z21 = Coords[maux[ne][2]].z - Coords[maux[ne][0]].z;
        double x31 = Coords[maux[ne][1]].x - Coords[maux[ne][0]].x;
        double y31 = Coords[maux[ne][1]].y - Coords[maux[ne][0]].y;
        double z31 = Coords[maux[ne][1]].z - Coords[maux[ne][0]].z;

        a = y21 * z31 - y31 * z21;
        b = z21 * x31 - z31 * x21;
        c = x21 * y31 - x31 * y21;
        d = -(a * Coords[maux[ne][0]].x + b * Coords[maux[ne][0]].y +
              c * Coords[maux[ne][0]].z);
        denom = a * a + b * b + c * c;
        if (denom > 0.0)
            dista = (a * cg.x + b * cg.y + c * cg.z + d) / sqrt(denom);
        else
            dista = 0.0;

        dista = dista * dista;
        if (dist > dista)
            dist = dista;
        }

    if (perim2>1.0e-30) return(144.0 * (dist / perim2));
    return(0.0);
    }


double Tetra4::Perimeter()
    {
    int i;
    double perim = 0.0;

    for (i = 0; i < 6; i++)
        perim += sqrt(EdgeLength2(i));

    return (perim);
    } 


char *Tetra4::ElementName(void)
    {
    return ("Linear Tetrahedron");
    }

char *Tetra4::AdditionalInfo(void)
    {
    return ("Not implemented");
    }


acPoint3 Tetra4::GravCenter()
    {
    acPoint3 g;
    int i;

    g.x = g.y = g.z = 0.0;

    for (i = 0; i < 4; i++)
        {
        g.x += Coords[i].x;
        g.y += Coords[i].y;
        g.z += Coords[i].z;
        }

    g.x = g.x / 4.0;
    g.y = g.y / 4.0;
    g.z = g.z / 4.0;
    return(g);
    }


void Tetra4::FacInf(int facetnum, long *nodes)
    {
    nodes[0] = Incid[maux[facetnum][0]];
    nodes[1] = Incid[maux[facetnum][1]];
    nodes[2] = Incid[maux[facetnum][2]];
    }


acPoint3 Tetra4::FacetCenter(int facetnum)
    {
    acPoint3 v[3];
    v[0] = Coords[maux[facetnum][0]];
    v[1] = Coords[maux[facetnum][1]];
    v[2] = Coords[maux[facetnum][2]];
    return(CenterTri(v));
    }


double Tetra4::FacetArea(int facetnum)
    {
    acPoint3 v[3];
    v[0] = Coords[maux[facetnum][0]];
    v[1] = Coords[maux[facetnum][1]];
    v[2] = Coords[maux[facetnum][2]];
    return(AreaTri(v));
    }


double Tetra4::EdgeLength2(int edgenum)
    {
    static int edaux[6][2] =
        {
        {0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}
        };
    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;
    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }


double Tetra4::Area()
    {
    int i;
    double s = 0;

    for (i = 0; i < 4; i++)
        s += FacetArea(i);
    return(s);
    }


