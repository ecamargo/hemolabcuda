#include "tri3.h"
#include <math.h>

Tri3::Tri3()
    {
    Incid  = (long   *)      mMalloc(3 * sizeof(long));
    Coords = (acPoint3   *)  mMalloc(3 * sizeof(acPoint3));
    if (!Incid || !Coords)
	{ //    1, "Can't Allocate Memory."
	char buf[40];
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}
    }

Tri3::~Tri3()
    {
    mFree(Incid);
    mFree(Coords);
    }

void Tri3::SetData(long   *incid, acCoordinate3D &c)
    {
    int i;

    for (i = 0 ; i < 3; i++)
        {
        Incid[i]  = incid[i];
        Coords[i] = c[incid[i]];
        }
    }

void Tri3::SetData(long   *incid)
    {
    int i;
    for (i = 0 ; i < 3; i++)
        Incid[i]  = incid[i];
    }


double Tri3::Quality()
    {
    int   ne;
    double perim2 = 0;
    double laux, t, x, y, z, dist, dista;
    static int edaux[3][2] = {{0, 1}, {1, 2}, {2, 0}};

    acPoint3 cg = GravCenter();

    dist = (cg.x - Coords[0].x) * (cg.x - Coords[0].x) +
           (cg.y - Coords[0].y) * (cg.y - Coords[0].y) +
           (cg.z - Coords[0].z) * (cg.z - Coords[0].z);
    for (ne = 0; ne < 3; ne++)
        {
        laux = EdgeLength2(ne);
        if (laux <= 1.0e-30) return(0.0);
        perim2 += laux;
        x = Coords[edaux[ne][1]].x - Coords[edaux[ne][0]].x;
        y = Coords[edaux[ne][1]].y - Coords[edaux[ne][0]].y;
        z = Coords[edaux[ne][1]].z - Coords[edaux[ne][0]].z;
        t = ((cg.x - Coords[edaux[ne][0]].x) * x +
             (cg.y - Coords[edaux[ne][0]].y) * y +
             (cg.z - Coords[edaux[ne][0]].z) * z) / laux;

        x = Coords[edaux[ne][0]].x + t * x;
        y = Coords[edaux[ne][0]].y + t * y;
        z = Coords[edaux[ne][0]].z + t * z;

        dista = (cg.x - x) * (cg.x - x) +
                (cg.y - y) * (cg.y - y) +
                (cg.z - z) * (cg.z - z);

        if (dista < dist)
            dist = dista;
        }

    return(36 * (dist / perim2));
    }

double Tri3::Perimeter()
    {
    int i;
    double perim = 0.0;

    for ( i = 0; i < 3; i++)
        perim += sqrt(EdgeLength2(i));
    return (perim);
    }



char *Tri3::ElementName(void)
    {
    return ("Linear Triangle");
    }

char *Tri3::AdditionalInfo(void)
    {
    return ("Not implemented");
    }


double Tri3::EdgeLength2(int edgenum)
    {
    static int edaux[3][2] =
        {
        {0, 1}, {1, 2}, {2, 0}
        };
    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;

    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }
