#include "tri6.h"
#include <math.h>


int Tri6::maux[4][3] = {{5,0,3},{3,1,4},{4,2,5},{3,4,5}};

Tri6::Tri6()
    {
    Incid  = (long   *)     mMalloc(6 * sizeof(long));
    Coords = (acPoint3   *) mMalloc(6 * sizeof(acPoint3));
    }

Tri6::~Tri6()
    {
    mFree(Incid);
    mFree(Coords); 
    }


double Tri6::Perimeter()
    {
    int i;
    double perim = 0.0;

    for ( i = 0; i < 6; i++)
        perim += sqrt(EdgeLength2(i));
    return (perim);
    }
    
char *Tri6::ElementName(void)
    {
    return ("Biquadratic Triangle");
    }

char *Tri6::AdditionalInfo(void)
    {
    return ("Not implemented");
    }    

void Tri6::FacInf(int /* facetnum */, long *nodes)
    {
    nodes[0] = Incid[0];
    nodes[1] = Incid[3];
    nodes[2] = Incid[1];
    nodes[3] = Incid[4];
    nodes[4] = Incid[2];
    nodes[5] = Incid[5];
    }

acPoint3 Tri6::FacetNormal(int facetnum)
    {
    double mod;
    acPoint3 n, aux;
    n   = TriFacNormal(facetnum, 0);
    aux = TriFacNormal(facetnum, 1);
    n.x += aux.x; n.y += aux.y; n.z += aux.z;
    aux = TriFacNormal(facetnum, 2);
    n.x += aux.x; n.y += aux.y; n.z += aux.z;
    aux = TriFacNormal(facetnum, 3);
    n.x += aux.x; n.y += aux.y; n.z += aux.z;
    mod = sqrt(n.x * n.x + n.y * n.y + n.z * n.z);
    n.x = n.x / mod; n.y = n.y / mod; n.z = n.z / mod;
    return(n);
    }


void Tri6::TriFacInf(int facetnum, int numtriang,
                     long *triangincid,  int *triangedges)
    {
    triangincid[0] = Incid[this->maux[numtriang][0]];
    triangincid[1] = Incid[this->maux[numtriang][1]];
    triangincid[2] = Incid[this->maux[numtriang][2]];
    if (numtriang == 3)
        triangedges[0] = triangedges[1] = 0;
    else
        triangedges[0] = triangedges[1] = 1;
    triangedges[2] = 0;
    }   
    

double Tri6::EdgeLength2(int edgenum)
    {
    static int edaux[6][2] =
        {
        {0, 3}, {3, 1}, {1, 4}, {4, 2}, {2, 5}, {5, 0}
        };
    double x, y, z, edgelength2;

    x = Coords[edaux[edgenum][1]].x - Coords[edaux[edgenum][0]].x;
    y = Coords[edaux[edgenum][1]].y - Coords[edaux[edgenum][0]].y;
    z = Coords[edaux[edgenum][1]].z - Coords[edaux[edgenum][0]].z;
    edgelength2 = x*x + y*y + z*z;
    return (edgelength2);
    }     
    
double Tri6::FacetArea(int /* facetnum */)
    {
    acPoint3 v[3];
    double area;
    v[0] = Coords[0];
    v[1] = Coords[3];
    v[2] = Coords[5];
    area = AreaTri(v);
    v[0] = Coords[4];
    area += AreaTri(v);
    v[2] = Coords[1];
    area += AreaTri(v);
    v[1] = Coords[2];
    v[2] = Coords[5];
    area += AreaTri(v);
    return(area);
    }


double Tri6::Quality()
    {
    int   ne;
    double perim2 = 0;
    double laux, t, x, y, z, dist, dista;
    static int edaux[6][2] =
        {
        {0, 3}, {3, 1}, {1, 4}, {4, 2}, {2, 5}, {5, 0}
        };

    acPoint3 cg = GravCenter();

    dist = 1.0e+99;
    for (ne = 0; ne < 6; ne++)
        {
        laux = EdgeLength2(ne);
        perim2 += laux;
        x = Coords[edaux[ne][1]].x - Coords[edaux[ne][0]].x;
        y = Coords[edaux[ne][1]].y - Coords[edaux[ne][0]].y;
        z = Coords[edaux[ne][1]].z - Coords[edaux[ne][0]].z;
        t = ((cg.x - Coords[edaux[ne][0]].x) * x +
             (cg.y - Coords[edaux[ne][0]].y) * y +
             (cg.z - Coords[edaux[ne][0]].z) * z) / laux;

        x = Coords[edaux[ne][0]].x + t * x;
        y = Coords[edaux[ne][0]].y + t * y;
        z = Coords[edaux[ne][0]].z + t * z;

        dista = (cg.x - x) * (cg.x - x) +
                (cg.y - y) * (cg.y - y) +
                (cg.z - z) * (cg.z - z);

        if (dista < dist)
            dist = dista;
        }

    return(18 * (dist / perim2));
    }

void Tri6::SetData(long   *incid, acCoordinate3D &c)
    {
    int i;

    for (i = 0 ; i < 6; i++)
        {
        Incid[i]  = incid[i];
        Coords[i] = c[incid[i]];
        }
    }

void Tri6::SetData(long   *incid)
{
    int i;
    for (i = 0 ; i < 6; i++)
       Incid[i]  = incid[i];
}

acPoint3 Tri6::FacetCenter(int /* facetnum */ )
    {
    acPoint3 center;
    center.x = (Coords[3].x + Coords[4].x + Coords[5].x) / 3.0;
    center.y = (Coords[3].y + Coords[4].y + Coords[5].y) / 3.0;
    center.z = (Coords[3].z + Coords[4].z + Coords[5].z) / 3.0;
    return(center);
    }

acPoint3 Tri6::TriFacNormal(int /* facetnum */, int numtriang)
    {
    acPoint3 v[3];
    v[0] = Coords[maux[numtriang][0]];
    v[1] = Coords[maux[numtriang][1]];
    v[2] = Coords[maux[numtriang][2]];
    return (NormalTri(v));
    }
