
#include "elgroups.h"
#include "SurfTriang.h"
#include "vtkCellArray.h"
#include "vtkUnstructuredGrid.h"

///////////////////////////////////////////////////////////////////////
// Constructors and Destructors
ElGroups::ElGroups()
{
	NumGroups = 0;
	NumEls    = 0;
	NumNodes  = 0;
	VInfGrps  = NULL;
	Incid     = NULL;
	pCurrEl   = pPrevEl = NULL;
}


ElGroups::~ElGroups()
{
  for (int i = 0; i < NumGroups; i++)
		  free(VInfGrps[i].WorkEl);
  mFree((char *) VInfGrps); 
  mFree((char *) Incid);
  NumGroups = 0;
}

///////////////////////////////////////////////////////////////////////
// IO Methods
void ElGroups::DXFOut(FILE *fp)
{
  static char *nome = "ElGroups::DXFOut";
  int i, j;
  Element *el;
  long k;

  TraceOn(nome);

  for (i = 0; i < NumGroups; i++)
		{
		long *pinci = VInfGrps[i].PtrIncid;
		el = VInfGrps[i].WorkEl;
		for (k = 0; k < VInfGrps[i].NumElsGrp; k++)
	    {
	    fprintf(fp, "VERTEX\n 8\n0\n 10\n0.0\n 20\n0.0\n 30\n0.0\n");
	    fprintf(fp, " 70\n 128\n");
	
	    for (j = 0; j < el->NNoel() && j<4; j++)
				fprintf(fp, " %d\n %ld\n", 71+j, pinci[j] +1);
	    pinci += el->NNoel();
	    fprintf(fp, " 0\n");
	    }
		}
	TraceOff(nome);
}

// Modified from "void ElGroups::Read(FILE *fp)"
void ElGroups::Copy(ElGroups *group)
{
  int i, j;
  static char buf[80];
  Element *el;
  long tamincid = 0;
  long ii, k, n;

  this->NumEls   = 0;
  this->NumNodes = 0;
  this->NumGroups = group->NumGroups;    // le num. de grupos
  
  this->VInfGrps = (InfoGroup *) mMalloc(sizeof(InfoGroup) * this->NumGroups);
  if (!this->VInfGrps)
    { //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
  	}

  for (i = 0; i < this->NumGroups; i++)
	{
		n = group->VInfGrps[i].NumElsGrp;
		this->VInfGrps[i].InitialEl = this->NumEls;
		this->VInfGrps[i].FinalEl 	= this->NumEls + n - 1;
		this->VInfGrps[i].NumElsGrp = n;
		this->NumEls += n;
		
		strcpy(this->VInfGrps[i].ElType, group->VInfGrps[i].ElType);
	
		el = (Element *) NewElement(this->VInfGrps[i].ElType);
		if (!el)
		    Error(FATAL_ERROR, 4, "Unknown element %s", VInfGrps[i].ElType);
	
		this->VInfGrps[i].WorkEl = el;
		tamincid += el->NNoel() * this->VInfGrps[i].NumElsGrp;
	}

  this->Incid = (long *) mMalloc(tamincid * sizeof(long));
  if (!Incid)
		{ //    1, "Can't Allocate Memory."
		char buf[100];
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}

  ii = 0;
  for (i = 0; i < NumGroups; i++)
		{
		el = this->VInfGrps[i].WorkEl;
		this->VInfGrps[i].PtrIncid = &Incid[ii];
		for (k = 0; k < this->VInfGrps[i].NumElsGrp; k++)
		    for (j = 0; j < el->NNoel(); j++)
			{
			this->Incid[ii] = group->Incid[ii];
			ii++;
			}
		}	
	this->NumNodes = group->NumNodes;	    		
}


void ElGroups::Read(FILE *fp)
{
  static char *nome = "ElGroups::Read";
  int i, j, idum;
  static char buf[80];
  Element *el;
  long tamincid = 0;
  long ii, k, no, n;

  TraceOn(nome);

  this->NumEls   = 0;
  this->NumNodes = 0;

  fscanf(fp, "%d", &this->NumGroups);    // le num. de grupos
  // No groups found or empty surface
  if (this->NumGroups < 1)
	  {
		this->VInfGrps 	= NULL;
		Incid						= NULL;
		return;  
  	//Error(FATAL_ERROR, 1, "Invalid number of Groups");
  	}

  this->VInfGrps = (InfoGroup *) mMalloc(sizeof(InfoGroup) * this->NumGroups);
  if (!this->VInfGrps)
    { //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
  	}

  for (i = 0; i < this->NumGroups; i++)
	{
		fscanf(fp, "%d %ld %s", &idum, &n, this->VInfGrps[i].ElType);
		VInfGrps[i].InitialEl = NumEls;
		VInfGrps[i].FinalEl = NumEls + n - 1;
	
		if (idum != i+1 || n < 1)
		    {
		    sprintf(buf, "Incorrect Group data %d %ld", idum, n);
		    Error(FATAL_ERROR, 3, buf);
		    }
	
		VInfGrps[i].NumElsGrp = n;
		NumEls += n;
	
		el = (Element *) NewElement(VInfGrps[i].ElType);
		if (!el)
		    Error(FATAL_ERROR, 4, "Unknown element %s", VInfGrps[i].ElType);
	
		VInfGrps[i].WorkEl = el;
		tamincid += el->NNoel() * VInfGrps[i].NumElsGrp;
	}

  Incid = (long *) mMalloc(tamincid * sizeof(long));
  if (!Incid)
		{ //    1, "Can't Allocate Memory."
		char buf[100];
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}

  char *kincid = "INCIDENCE";
  if (!FindKeyWord(kincid, fp))
    Error(FATAL_ERROR, 5, "ElGroups -> Keyword %s not found", kincid);

  ii = 0;
  for (i = 0; i < NumGroups; i++)
		{
		el = VInfGrps[i].WorkEl;
		VInfGrps[i].PtrIncid = &Incid[ii];
		for (k = 0; k < VInfGrps[i].NumElsGrp; k++)
		    for (j = 0; j < el->NNoel(); j++)
			{
			fscanf(fp, "%ld", &no);
			if (no > NumNodes)
			    NumNodes = no;	    
			Incid[ii++] = no-1;
			}
		}
	TraceOff(nome);
}

void ElGroups::Read(int NumeroGroups, int *NumElsGroups, long *externIncid, char *type)
{
  int i, j;
  static char buf[80];
  Element *el;
  long tamincid = 0;
  long ii, k, no, n;

  this->NumEls   = 0;
  this->NumNodes = 0;
  this->NumGroups = NumeroGroups;

  if (this->NumGroups < 1)
  	Error(FATAL_ERROR, 1, "Invalid number of Groups");

  this->VInfGrps = (InfoGroup *) mMalloc(sizeof(InfoGroup) * this->NumGroups);

  if (!this->VInfGrps)
    { //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
  	}
  for (i = 0; i < this->NumGroups; i++)
	{
		n = NumElsGroups[i];							// Recebe número de grupos
		strcpy(this->VInfGrps[i].ElType, type); // Tipo dos dados é fixo
		VInfGrps[i].InitialEl = this->NumEls;
		VInfGrps[i].FinalEl 	= this->NumEls + n - 1;
		VInfGrps[i].NumElsGrp = n;
		this->NumEls += n;

		el = (Element *) NewElement(VInfGrps[i].ElType);
		if (!el)
		    Error(FATAL_ERROR, 4, "Unknown element %s", VInfGrps[i].ElType);

		VInfGrps[i].WorkEl = el;
		tamincid += el->NNoel() * VInfGrps[i].NumElsGrp;
	}
  Incid = (long *) mMalloc(tamincid * sizeof(long));
  if (!Incid)
		{ //    1, "Can't Allocate Memory."
		char buf[100];
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}
	// Copia vetor de incidência externo
	for(i = 0; i < tamincid; i++)
		Incid[i] = externIncid[i];

	// Acerta ponteiros de início de cada grupo
  ii = 0;
  for (i = 0; i < this->NumGroups; i++)
		{
		el = this->VInfGrps[i].WorkEl;
		this->VInfGrps[i].PtrIncid = &Incid[ii];
		for (k = 0; k < this->VInfGrps[i].NumElsGrp; k++)
		  for (j = 0; j < el->NNoel(); j++)
				{
				no = Incid[ii++];
				if(no > this->NumNodes) this->NumNodes = no;				
				}
		}
	// Incrementa NumNodes pois pegou o índice que começa de zero
	this->NumNodes++;			
}


void ElGroups::Read(vtkUnstructuredGrid *grid, char *type)
{
  static char *nome = "ElGroups::Read";
  int i, j;
  static char buf[80];
  Element *el;
  long tamincid = 0;
  long ii, no;

  TraceOn(nome);

  this->NumNodes = 0;
	this->NumGroups = 1;
  this->VInfGrps = (InfoGroup *) mMalloc(sizeof(InfoGroup) * this->NumGroups);
  if (!this->VInfGrps)
    { //    1, "Can't Allocate Memory."
    char buf[100];
    GetError(1, buf);
    Error(FATAL_ERROR, 2, buf);
  	}

	strcpy(this->VInfGrps[0].ElType, type);
  this->NumEls = grid->GetNumberOfCells();
   
	this->VInfGrps[0].InitialEl = 0;
	this->VInfGrps[0].FinalEl = NumEls-1;	
	this->VInfGrps[0].NumElsGrp = NumEls;

	el = (Element *) NewElement(this->VInfGrps[0].ElType);
	if (!el) Error(FATAL_ERROR, 4, "Unknown element %s", VInfGrps[0].ElType);

	this->VInfGrps[0].WorkEl = el;
	tamincid = el->NNoel() * this->VInfGrps[0].NumElsGrp;

  Incid = (long *) mMalloc(tamincid * sizeof(long));
  if (!Incid)
		{ //    1, "Can't Allocate Memory."
		char buf[100];
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}

	el = this->VInfGrps[0].WorkEl;
	this->VInfGrps[0].PtrIncid = &Incid[0];

	int coord[4];
	vtkCell *cell;
	
  ii = 0;
	for(i = 0; i < this->NumEls; i++)
		{
		cell = grid->GetCell(i);

//		CÓDIGO DE TESTE PARA INVERTER A DIREÇÃO DA FACE
//		(TESTADO PARA VERIFICAR PROBLEMAS NO DELA3D)
//		11/01/2007		
//	  for (j = 0; j < el->NNoel(); j++)
//			{	
//			coord[j] = cell->GetPointId(j);				
//			if(j==1) coord[j] = cell->GetPointId(2);							
//			if(j==2) coord[j] = cell->GetPointId(1);							
//			
//			no = coord[j];
//			if (no > NumNodes)
//		    NumNodes = no;		    
//			this->Incid[ii++] = no;	// Não é 'no - 1' pois ao ler o 'sur' esse ajuste já é feito
//			}

	  for (j = 0; j < el->NNoel(); j++)
			{	
			coord[j] = cell->GetPointId(j);				
			no = coord[j];
			if (no > NumNodes)
		    NumNodes = no;		    
			this->Incid[ii++] = no;	// Não é 'no - 1' pois ao ler o 'sur' esse ajuste já é feito
			}
		}			
  NumNodes++; // Para corrigir a falta do 'no - 1' anterior
	TraceOff(nome);		
}

void ElGroups::CopyIncidenceVector(ElGroups *group)
{
  int ii, i, j, k;
  Element *el;  
  ii = 0;
  for (i = 0; i < this->NumGroups; i++)
		{
		el = this->VInfGrps[i].WorkEl;			
		this->VInfGrps[i].PtrIncid = &this->Incid[ii];
		for (k = 0; k < this->VInfGrps[i].NumElsGrp; k++)
			{		
		  for (j = 0; j < el->NNoel(); j++)
				{
				this->Incid[ii] = group->Incid[ii];
				ii++;
				}
			}  
		}

}

// Pega incidencia. Este método foi criada para pegar volumes (tetraedros)
void ElGroups::GetVolumeIncidence(long *vet)
{
	for(int i = 0; i < this->NumEls * 4; i++)
		{
		vet[i] = this->Incid[i];		
		}			
}

void ElGroups::GetElement(int i, int *vet)
{
  int ii = i * 3; // Pois a lista de faces é contínua e somente de triangulos
  
  vet[0] = this->Incid[ii++];
  vet[1] = this->Incid[ii++];
  vet[2] = this->Incid[ii++];    
}

void ElGroups::Print(FILE *fp)
{
  static char *nome = "ElGroups::Print";
  Element *el;
  int i;
  long tamincid = 0;
  long ii;

  TraceOn(nome);

  fprintf(fp, "\n*ELEMENT GROUPS\n %d\n", NumGroups);
  for (i = 0; i < NumGroups; i++)
		{
		fprintf(fp, "%d %ld %s\n", i+1, VInfGrps[i].NumElsGrp, VInfGrps[i].ElType);
		el = VInfGrps[i].WorkEl;
		tamincid += VInfGrps[i].NumElsGrp * el->NNoel();
		}
  fprintf(fp, "\n*INCIDENCE\n");
  for (ii = 0; ii<tamincid; ii++)
		{
		fprintf (fp, " %ld", Incid[ii]+1);
		if (!((ii+1)%10)) fprintf (fp, "\n");
		}
  fprintf (fp , "\n");
  TraceOff(nome);
}

