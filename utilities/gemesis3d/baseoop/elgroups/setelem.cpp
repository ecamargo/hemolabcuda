#include "elgroups.h"

Element * ElGroups::SetFirstElem (void)
    {
    pCurrIncid = Incid;             // Incidencia
    CurrGr = 0;                     // Grupo
    CurrElGr = 0;                   // Elem (local)
    pCurrEl = VInfGrps[0].WorkEl;   // Elem (pointer)
    pCurrEl->SetData(pCurrIncid);
    return (pCurrEl);
    }

Element * ElGroups::SetNextElem (void)
    {
    CurrElGr++;
    if (CurrElGr < VInfGrps[CurrGr].NumElsGrp)
        {
        pCurrIncid += pCurrEl->NNoel();
        pCurrEl->SetData(pCurrIncid);
        }
    else
        {
        CurrGr++;
        if (CurrGr < NumGroups)
            {
            CurrElGr = 0;
            pCurrIncid = VInfGrps[CurrGr].PtrIncid;
            pCurrEl = VInfGrps[CurrGr].WorkEl;
            pCurrEl->SetData(pCurrIncid);
            }
        else
            {
            pCurrIncid = NULL;
            CurrElGr = -1;
            CurrGr = -1;
            pCurrEl = NULL;
            }
        }
    return (pCurrEl);
    }

Element * ElGroups::SetAnElem(long iel)
    {
    if ( pPrevEl != NULL )
        if (iel>=VInfGrps[PrevGr].InitialEl &&
            iel<=VInfGrps[PrevGr].FinalEl     )
            {
            PrevElGr = iel - VInfGrps[PrevGr].InitialEl;
            pPrevIncid = VInfGrps[PrevGr].PtrIncid+PrevElGr*pPrevEl->NNoel();
            pPrevEl->SetData(pPrevIncid);
            return pPrevEl;
            }
    int i;
    for (i=0; i<NumGroups; i++)
        {
        if (iel<=VInfGrps[i].FinalEl)
           break;
        }
    PrevGr = i;
    PrevElGr = iel - VInfGrps[PrevGr].InitialEl;
    pPrevEl = VInfGrps[PrevGr].WorkEl;
    pPrevIncid = VInfGrps[PrevGr].PtrIncid+PrevElGr*pPrevEl->NNoel();
    pPrevEl->SetData(pPrevIncid);
    return pPrevEl;
    }

Element * ElGroups::SetFirstElem (acCoordinate3D *coor)
    {
    pCurrIncid = Incid;             // Incidencia
    CurrGr = 0;                     // Grupo
    CurrElGr = 0;                   // Elem (local)
    pCurrEl = VInfGrps[0].WorkEl;   // Elem (pointer)
    pCurrEl->SetData(pCurrIncid, *coor);
    return (pCurrEl);
    }

Element * ElGroups::SetNextElem (acCoordinate3D *coor)
    {
    CurrElGr++;
    if (CurrElGr < VInfGrps[CurrGr].NumElsGrp)
        {
        pCurrIncid += pCurrEl->NNoel();
        pCurrEl->SetData(pCurrIncid, *coor);
        }
    else
        {
        CurrGr++;
        if (CurrGr < NumGroups)
            {
            CurrElGr = 0;
            pCurrIncid = VInfGrps[CurrGr].PtrIncid;
            pCurrEl = VInfGrps[CurrGr].WorkEl;
            pCurrEl->SetData(pCurrIncid, *coor);
            }
        else
            {
            pCurrIncid = NULL;
            CurrElGr = -1;
            CurrGr = -1;
            pCurrEl = NULL;
            }
        }
    return (pCurrEl);
    }

Element * ElGroups::SetAnElem(long iel, acCoordinate3D *coor)
    {
    if ( pPrevEl != NULL )
        if (iel>=VInfGrps[PrevGr].InitialEl &&
            iel<=VInfGrps[PrevGr].FinalEl     )
            {
            PrevElGr = iel - VInfGrps[PrevGr].InitialEl;
            pPrevIncid = VInfGrps[PrevGr].PtrIncid+PrevElGr*pPrevEl->NNoel();
            pPrevEl->SetData(pPrevIncid, *coor);
            return pPrevEl;
            }
    int i;
    for (i=0; i<NumGroups; i++)
        {
        if (iel<=VInfGrps[i].FinalEl)
           break;
        }
    PrevGr = i;
    PrevElGr = iel - VInfGrps[PrevGr].InitialEl;
    pPrevEl = VInfGrps[PrevGr].WorkEl;
    pPrevIncid = VInfGrps[PrevGr].PtrIncid+PrevElGr*pPrevEl->NNoel();
    pPrevEl->SetData(pPrevIncid, *coor);
    return pPrevEl;
    }
