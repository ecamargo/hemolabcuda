#include "elgroups.h"
#include <string.h>

void ElGroups::SetInci2d(long nod, long nel, long las, long   *je)
    {
    static char *nome = "ElGroups::SetInci2d";
    static char buf[80];
    Element *el;
    long ii;

    TraceOn(nome);

    NumEls   = nel;
    NumNodes = nod;
    NumGroups = 1;

    VInfGrps = (InfoGroup   *) mMalloc(sizeof(InfoGroup) * NumGroups);
    VInfGrps[0].InitialEl = 0;
    VInfGrps[0].FinalEl = nel - 1;
    VInfGrps[0].NumElsGrp = nel;

    if (las/nel == 3)
	strcpy(VInfGrps[0].ElType, "Tri3");
    else if (las/nel == 4)
	strcpy(VInfGrps[0].ElType, "Quad4");
    else if (las/nel == 6)
	strcpy(VInfGrps[0].ElType, "Tri6");
    else
	sprintf(VInfGrps[0].ElType,"' %ld nodes'", las/nel);

    el = (Element *) NewElement(VInfGrps[0].ElType);
    if (!el)
	{
	sprintf(buf, "Unknown element %s", VInfGrps[0].ElType);
	Error(FATAL_ERROR, 2, buf);
	}

    VInfGrps[0].WorkEl = el;

    Incid = (long   *) mMalloc(las * sizeof(long));
    VInfGrps[0].PtrIncid = Incid;

    if (las/nel == 6)
	{
	ii = 0;
	for (long j = 0; j < nel; j++)
	    {
	    Incid[ii++] = je[0] -1;
	    Incid[ii++] = je[2] -1;
	    Incid[ii++] = je[4] -1;
	    Incid[ii++] = je[1] -1;
	    Incid[ii++] = je[3] -1;
	    Incid[ii++] = je[5] -1;
	    je += 6;
	    }
	}
    else
	for (ii = 0; ii < las; ii++, je++)
	    Incid[ii] = *je -1;

    TraceOff(nome);
    }

void ElGroups::SetInci2d(long nod, long nel, long las,
    long   *ie, long   *je, int is3D)
    {
    static char *nome = "ElGroups::SetInci2d";
    static char buf[80];
    int numels[11];
    Element *el;
    int i, j;
    long ii, jj;
    int numnodes;
    long neltot;
    long   *incid;

    TraceOn(nome);

    NumEls   = nel;
    NumNodes = nod;

    for (i = 0; i < 11; i++)
	numels[i] = 0;
    for (ii = 0; ii < nel; ii++)
	{
	numnodes = (int) (ie[ii+1] - ie[ii]);
	numels[numnodes]++;
	}

    NumGroups = 0;
    for (i = 0; i < 11; i++)
	{
	if (numels[i] > 0)
	    NumGroups++;
	}
    VInfGrps = (InfoGroup   *) mMalloc(sizeof(InfoGroup) * NumGroups);
    neltot = 0;
    NumGroups = 0;
    ii = 0;
    Incid = (long   *) mMalloc(las * sizeof(long));
    incid = Incid;
    for (i = 0; i < 11; i++)
	{
	if (numels[i] > 0)
	    {
	    VInfGrps[NumGroups].InitialEl = neltot;
	    VInfGrps[NumGroups].FinalEl = neltot + numels[i] - 1;
	    VInfGrps[NumGroups].NumElsGrp = numels[i];
	    switch (i)
		{
	    case 3:
		{
		strcpy(VInfGrps[NumGroups].ElType, "Tri3");
		break;
		}
	    case 4:
		{
		if (is3D)
		    strcpy(VInfGrps[NumGroups].ElType, "Tetra4");
		else
		    strcpy(VInfGrps[NumGroups].ElType, "Quad4");
		break;
		}
	    case 6:
		{
		strcpy(VInfGrps[NumGroups].ElType, "Tri6");
		break;
		}
	    case 8:
		{
		if (is3D)
		    strcpy(VInfGrps[NumGroups].ElType, "Hexah8");
		else
		    strcpy(VInfGrps[NumGroups].ElType, "Quad8");
		break;
		}
	    case 9:
		{
		strcpy(VInfGrps[NumGroups].ElType, "Quad9");
		break;
		}
	    case 10:
		{
		if (is3D)
		    strcpy(VInfGrps[NumGroups].ElType, "Tetra10");
		else
		    strcpy(VInfGrps[NumGroups].ElType, "10 nodes 2D?");
		break;
		}
	    default:
		sprintf(VInfGrps[NumGroups].ElType,"' %d nodes'", i);
		break;
		}
	    el = (Element *) NewElement(VInfGrps[NumGroups].ElType);
	    if (!el)
		{
		sprintf(buf, "Unknown element %s", VInfGrps[NumGroups].ElType);
		Error(FATAL_ERROR, 2, buf);
		}

	    VInfGrps[NumGroups].WorkEl = el;
	    VInfGrps[NumGroups].PtrIncid = incid;
	    for (jj = 0; jj < nel; jj++)
		{
		if (ie[jj+1]-ie[jj] == i)
		    {
		    for (j = 0; j < i; j++)
			Incid[ii++] = je[ie[jj]-1+j] - 1;
		    incid += i;
		    }
		}
	    NumGroups++;
	    }
	}
    TraceOff(nome);
    }

/*
	nodos	3	4	6	8	9	10
	 2 D	Tri3	Quad4   Tri6 	Quad8	Quad9
	 3 D	Tri3	Tetra4	Tri6	Hexah8  Quad9	Tetra10

    if (las/nel == 6)
	{
	ii = 0;
	for (long j = 0; j < nel; j++)
	    {
	    Incid[ii++] = je[0] -1;
	    Incid[ii++] = je[2] -1;
	    Incid[ii++] = je[4] -1;
	    Incid[ii++] = je[1] -1;
	    Incid[ii++] = je[3] -1;
	    Incid[ii++] = je[5] -1;
	    je += 6;
	    }
	}
    else
	for (ii = 0; ii < las; ii++, je++)
	    Incid[ii] = *je -1;

    if (las/nel == 10)
	{
	ii = 0;
	for (long j = 0; j < nel; j++)
	    {
	    Incid[ii++] = je[0] -1;
	    Incid[ii++] = je[2] -1;
	    Incid[ii++] = je[1] -1;
	    Incid[ii++] = je[3] -1;
	    Incid[ii++] = je[6] -1;
	    Incid[ii++] = je[5] -1;
	    Incid[ii++] = je[4] -1;
	    Incid[ii++] = je[7] -1;
	    Incid[ii++] = je[9] -1;
	    Incid[ii++] = je[8] -1;
	    je += 10;
	    }
	}
*/

