#include "sparse.h"
#include "surface.h"

SparseEdges::SparseEdges(Surface *Surf, SparseNoFa *NoFa,
	int allf, int DetectFlag, acRotation *t)
    {
    static char *nome="SparseEdges::SparseEdges";
    TraceOn(nome);
    int e0, e1, e2, eaux;
    long i, j, face, aux, faceaux, k;
    int   *msw;
    SparseNoFa *nofa;
    long inci[3], inciaux[3];
    char buf[40];

    TIndex = Surf->GetNumNodes();
    SI     = (SparseInfo   *) mMalloc(sizeof(SparseInfo) * TIndex);
    if (!SI)
	{ //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }


    for (i = 0; i<TIndex; i++)
        SI[i].n = 0;

    if(!NoFa)
        nofa = new SparseNoFa(Surf);
    else
        nofa = NoFa;

    msw = (int   *) mMalloc(sizeof(int) *TIndex);
    for (i = 0; i < TIndex; i++)
	msw[i] = 0;

    if (!msw)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}

    for (i = 0; i < TIndex; i++)
	{
	for (j = 0; j < nofa->GetNelems(i); j++)
	    {
	    face = nofa->GetElem(i, j);  // Cara donde estoy parada
	    if ((!Surf->FaceOn(face) && !allf))
		 continue;
	    Surf->GetInciFace(face, inci);
	    e0 = 0; e1 = 1; e2 = 2;

	    while (inci[0] != i) //en inci[0] me queda el
				//nodo que estoy considerando
		{
		aux = inci[0];      eaux = e0;
		inci[0] = inci[1];  e0 = e1;
		inci[1] = inci[2];  e1 = e2;
		inci[2] = aux;      e2 = eaux;
		}

	    if (inci[1] > i && !msw[inci[1]]
		&& Surf->EdgeOn(face,e0))
		{
		if (DetectFlag)
		{
		for (k = 0; k < nofa->GetNelems(i); k++)
		    {
		    if (k == j)
			continue;
		    faceaux = nofa->GetElem(i,k);
		    Surf->GetInciFace(faceaux, inciaux);
		    if((inciaux[0]==inci[1]) || (inciaux[1]==inci[1])
			|| (inciaux[2]==inci[1])) // esa es la cara que busco
		    break;
		    }
		if (k < nofa->GetNelems(i)) // Encontro cara
		    {
		    acPoint3 norm1 = Surf->GetNormal(face);
		    acPoint3 norm2 = Surf->GetNormal(faceaux);
		    if (t->GetZ(norm1)*t->GetZ(norm2) <= 0)
			{
			msw[inci[1]] = 1;
			SI[i].n++;
			}
		    else
			{
			//que angulo forman face y faceaux?
			double cos =
			norm1.x*norm2.x + norm1.y*norm2.y + norm1.z*norm2.z;

			if (cos < 0.7)  // Angulo diedro < ???
			    {
			    msw[inci[1]] = 1;
			    SI[i].n++;
			    }
			}
		    }
		else    // No encontro cara vecina
		    {
		    msw[inci[1]] = 1;
		    SI[i].n++;
		    }
		}
		else    // DetectFlag = 0
		{
		    msw[inci[1]] = 1;
		    SI[i].n++;
		}
		}

	    if (inci[2] > i && !msw[inci[2]]
		&& Surf->EdgeOn(face,e2))
		{
		if (DetectFlag)
		{
		for (k = 0; k < nofa->GetNelems(i); k++)
		    {
		    if (k == j)
			continue;
		    faceaux = nofa->GetElem(i,k);
		    Surf->GetInciFace(faceaux, inciaux);
		    if((inciaux[0]==inci[2]) || (inciaux[1]==inci[2])
			|| (inciaux[2]==inci[2])) // esa es la cara que busco
		    break;
		    }
		if (k < nofa->GetNelems(i)) // Encontro cara
		    {
		    acPoint3 norm1 = Surf->GetNormal(face);
		    acPoint3 norm2 = Surf->GetNormal(faceaux);
		    if (t->GetZ(norm1)*t->GetZ(norm2) <= 0)
			{
			msw[inci[2]] = 1;
			SI[i].n++;
			}
		    else
			{
			//que angulo forman face y faceaux?
			double cos =
			norm1.x*norm2.x + norm1.y*norm2.y + norm1.z*norm2.z;

			if (cos < 0.5)  // Angulo diedro < 120
			    {
			    msw[inci[2]] = 1;
			    SI[i].n++;
			    }
			}
		    }
		else    // No encontro cara vecina
		    {
		    msw[inci[2]] = 1;
		    SI[i].n++;
		    }
		}
		else    // DetectFlag = 0
		{
		    msw[inci[2]] = 1;
		    SI[i].n++;
		}
		}

	}

	if (SI[i].n > 0)
	    {
	    SI[i].Elems = (long   *) mMalloc(SI[i].n * sizeof(long));
	    if (!SI[i].Elems)
		{ //    1, "Can't Allocate Memory."
		GetError(1, buf);
		Error(FATAL_ERROR, 1, buf);
		}

	SI[i].n = 0;

        for (j = 0; j < nofa->GetNelems(i); j++)
            {
            face = nofa->GetElem(i, j);
            Surf->GetInciFace(face, inci);
            if (msw[inci[0]])
                {
                msw[inci[0]] = 0;
                SI[i].Elems[SI[i].n] = inci[0];
                SI[i].n++;
                }
            if (msw[inci[1]])
                {
                msw[inci[1]] = 0;
                SI[i].Elems[SI[i].n] = inci[1];
                SI[i].n++;
		}
            if (msw[inci[2]])
                {
                msw[inci[2]] = 0;
                SI[i].Elems[SI[i].n] = inci[2];
                SI[i].n++;
                }
            }
 	    }
       }

    if (!NoFa)
        delete nofa;
    mFree((char *) msw);
    TraceOff(nome);
    }

/*
SparseEdges::SparseEdges(Surface *Surf, SparseNoFa *NoFa, int allf)
    {
    static char *nome="SparseEdges::SparseEdges";
    TraceOn(nome);
    int e0, e1, e2, eaux;
    long i, j, face, aux;
    int   *msw;
    SparseNoFa *nofa;
    long inci[3];
    char buf[40];

    TIndex = Surf->GetNumNodes();
    SI     = (SparseInfo   *) mMalloc(sizeof(SparseInfo) * TIndex);
    if (!SI)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }


    for (i = 0; i<TIndex; i++)
        SI[i].n = 0;

    if(!NoFa)
        nofa = new SparseNoFa(Surf);
    else
        nofa = NoFa;

    // cria area e limpa vetor
    msw = (int   *) mCalloc(sizeof(int), TIndex);
    if (!msw)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    for (i = 0; i < TIndex; i++)
        {
        for (j = 0; j < nofa->GetNelems(i); j++)
            {
            face = nofa->GetElem(i, j);
            if (!Surf->FaceOn(face) && !allf)
                 continue;
            Surf->GetInciFace(face, inci);
            e0 = 0; e1 = 1; e2 = 2;
            while (inci[0] != i)
                {
                aux = inci[0];      eaux = e0;
                inci[0] = inci[1];  e0 = e1;
                inci[1] = inci[2];  e1 = e2;
                inci[2] = aux;      e2 = eaux;
                }

            if (inci[1] > i && !msw[inci[1]]
                && Surf->EdgeOn(face,e0))
                {
                msw[inci[1]] = 1;
                SI[i].n++;
                }
            if (inci[2] > i && !msw[inci[2]]
                && Surf->EdgeOn(face,e2))
                {
                msw[inci[2]] = 1;
                SI[i].n++;
                }
            }

        if (SI[i].n > 0)
            {
            SI[i].Elems = (long   *) mMalloc(SI[i].n * sizeof(long));
            if (!SI[i].Elems)
                { //    1, "Can't Allocate Memory."
                GetError(1, buf);
                Error(FATAL_ERROR, 1, buf);
                }
            }

        SI[i].n = 0;

        for (j = 0; j < nofa->GetNelems(i); j++)
            {
            face = nofa->GetElem(i, j);
            Surf->GetInciFace(face, inci);
            if (msw[inci[0]])
                {
                msw[inci[0]] = 0;
                SI[i].Elems[SI[i].n] = inci[0];
                SI[i].n++;
                }
            if (msw[inci[1]])
                {
                msw[inci[1]] = 0;
                SI[i].Elems[SI[i].n] = inci[1];
                SI[i].n++;
                }
            if (msw[inci[2]])
                {
                msw[inci[2]] = 0;
                SI[i].Elems[SI[i].n] = inci[2];
                SI[i].n++;
                }
            }
        }

    if (!NoFa)
        delete nofa;
    mFree((char *) msw);
    TraceOff(nome);
    }
*/
