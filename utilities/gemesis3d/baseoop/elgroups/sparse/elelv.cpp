#include "sparse.h"

SparseElElV::SparseElElV(ElGroups *elg, SparseNoEl *NoEl)
    {
    int  nfac, nf, nfacv, nfv;
    long iel, ielv, nmsw, l;
    int nelasoc;
    Element *el;
    long   *msw;
    int nodesfiv, nof;
    long firstnode, secondnode, thirdnode;
    SparseNoEl *noel;
    long vn[16];
    static char nome[] = "SparseElElV::SparseElElV";
    char buf[40];
    TraceOn(nome);

    TIndex = elg->GetNumEls();
    SI     = (SparseInfo   *) mCalloc(sizeof(SparseInfo), TIndex);
    if (!SI)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

//    MessageBox(GetFocus(),"Va a calcular NoEl","ElElV5 Testing...",MB_OK);
    if(!NoEl)
        noel = new SparseNoEl(elg);
    else
        noel = NoEl;
//    MessageBox(GetFocus(),"Comienza reserva de memoria","Testing...",MB_OK);

    // Reserva memoria para multiple switch (maximo(nodos, elementos)) (debe estar en 0)!
    if (TIndex > elg->GetNumNodes())
       msw = (long   *) mCalloc(sizeof(long), TIndex);
    else
       msw = (long   *) mCalloc(sizeof(long), elg->GetNumNodes());

    if (!msw)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    // Reserva memoria para conectividad (permite setear vecino y viceversa)

    for (el = elg->SetFirstElem(), iel = 0; el; el = elg->SetNextElem(), iel++)
        {
        nfac = el->NFacet();
        SI[iel].n = nfac;
        SI[iel].Elems = (long   *) mMalloc(nfac * sizeof(long));
        if (!SI[iel].Elems)
            { //    1, "Can't Allocate Memory."
            GetError(1, buf);
            Error(FATAL_ERROR, 1, buf);
            }
        for (nf = 0; nf < nfac; nf++)  // loop sobre as faces do el
            SI[iel].Elems[nf] = -2;    // Marca: no computada vecindad
        }

//    MessageBox(GetFocus(),"Entra al Loop principal","Testing...",MB_OK);
    nmsw = 1;
    for (el = elg->SetFirstElem(), iel = 0; el; el = elg->SetNextElem(), iel++)
        {
        nfac = el->NFacet();

        for (nf = 0; nf < nfac; nf++)     // loop sobre as faces do el
            {
            if (SI[iel].Elems[nf] != -2) continue;  // ya calculado
               
            SI[iel].Elems[nf] = -1;   // En principio limita con el exterior

            if (nf > 0) el = elg->SetAnElem(iel);
            el->FacInf(nf, vn);       // Cambiar por la que da solo 3 nodos ???
	        firstnode  = vn[0];
	        secondnode = vn[1];
	        thirdnode  = vn[2];

            // loop sobre os elems. associados ao 1er no' da face
            nelasoc = (int) noel->GetNelems(firstnode);
            nmsw += 2;
            for (l = nelasoc; l > 0;)
                {
                l--;
                ielv = noel->GetElem(firstnode, l);
                if (ielv <= iel) break;                  // La estructura noel DEBE estar ORDENADA
                msw[ielv] = nmsw;
                }

            // loop sobre os elems. associados ao 2do no' da face
            // da face, exceto o ultimo
            nelasoc = (int) noel->GetNelems(secondnode);
            for (l = nelasoc; l >0;)
                {
                l--;
                ielv = noel->GetElem(secondnode, l);
                if (ielv <= iel) break;                  // La estructura noel DEBE estar ORDENADA
                msw[ielv]++;
                }

            // loop sobre os elems. associados ao 3er no' da face
            nelasoc = (int) noel->GetNelems(thirdnode);
            for (l = nelasoc; l >0;)
                {
                l--;
                ielv = noel->GetElem(thirdnode, l);
                if (ielv <= iel) break;                 // La estructura noel DEBE estar ORDENADA
                if (msw[ielv] == nmsw + 1)
                    {
                    SI[iel].Elems[nf] = ielv;
                    break;
                    }
                }

            if (SI[iel].Elems[nf] != -1)  // Hallo vecino !!!
                {
                el = elg->SetAnElem(ielv);
                nfacv = el->NFacet();

                for (nfv = 0; nfv < nfacv; nfv++)  // Loop sobre caras del vecino
                    {
                    if (SI[ielv].Elems[nfv] != -2) continue; // La cara ya tiene otro vecino
                    nmsw += 2;
                    nodesfiv = el->NodFac(nfv);   // Numero de nodos en esta cara
                    el->FacInf(nfv, vn);          // Todos los nodos de esta cara

                    for (nof = 0; nof < nodesfiv; nof++)
                        msw[vn[nof]] = nmsw;

                    if (msw[firstnode ] == nmsw &&
                        msw[secondnode] == nmsw &&
                        msw[thirdnode ] == nmsw)   // Es esta cara !!!
                        {
                        SI[ielv].Elems[nfv] = iel;
                        break;
                        }
                    }  // Next cara del vecino

                }  // Endif Hallo vecino

            }  // Next cara del elemento
        } // Next elemento

//    MessageBox(GetFocus(),"Sale de ElElV","Testing...",MB_OK);

    if (!NoEl)
       delete noel;
    mFree((char *) msw);
    TraceOff(nome);
    }

