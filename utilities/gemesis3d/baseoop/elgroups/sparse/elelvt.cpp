#include "sparse.h"

SparseElElVT::SparseElElVT(ElGroups *elg, SparseNoEl *NoEl)
    {
    int  nfac, nf;
    long iel, ielv, nmsw, l;
    int nelasoc;
    Element *el;
    long   *msw;
    long firstnode, secondnode, thirdnode, fourthnode;
    SparseNoEl *noel;
    static char nome[] = "SparseElElVT::SparseElElVT";
    char buf[40];
    TraceOn(nome);

    TIndex = elg->GetNumEls();
    SI     = (SparseInfo   *) mCalloc(sizeof(SparseInfo), TIndex);
    if (!SI)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}

//    MessageBox(GetFocus(),"Va a calcular NoEl","ElElV5 Testing...",MB_OK);
    if(!NoEl)
	noel = new SparseNoEl(elg);
    else
	noel = NoEl;
//    MessageBox(GetFocus(),"Comienza reserva de memoria","Testing...",MB_OK);

    // Reserva memoria para multiple switch (maximo(nodos, elementos)) (debe estar en 0)!
    if (TIndex > elg->GetNumNodes())
       msw = (long   *) mCalloc(sizeof(long), TIndex);
    else
       msw = (long   *) mCalloc(sizeof(long), elg->GetNumNodes());

    if (!msw)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}

    // Verifica que sean tetraedros de 4 nodos
    for (int i = 0; i < elg->GetNumGroups(); i++)
	{
	char *etype = elg->GetElType(i);
	if (StrCmp(etype, "LINEAR_TETRAHEDRUM") != 0 &&
	    StrCmp(etype, "Tetra4") != 0)
	    Error(FATAL_ERROR, 1, "Linear tetrahedron elements required");
	}

    // Reserva memoria para conectividad (permite setear vecino y viceversa)

    for (el = elg->SetFirstElem(), iel = 0; el; el = elg->SetNextElem(), iel++)
	{
	nfac = 4;
	SI[iel].n = nfac;
	SI[iel].Elems = (long   *) mMalloc(nfac * sizeof(long));
	if (!SI[iel].Elems)
	    { //    1, "Can't Allocate Memory."
	    GetError(1, buf);
	    Error(FATAL_ERROR, 1, buf);
	    }
	for (nf = 0; nf < nfac; nf++)  // loop sobre as faces do el
	    SI[iel].Elems[nf] = -2;    // Marca: no computada vecindad
	}

//    MessageBox(GetFocus(),"Entra al Loop principal","Testing...",MB_OK);
    nmsw = 1;
    for (el = elg->SetFirstElem(), iel = 0; el; el = elg->SetNextElem(), iel++)
	{
	for (nf = 0; nf < 4; nf++)     // loop sobre as faces do el
	    {
	    if (SI[iel].Elems[nf] != -2) continue;  // ya calculado

	    SI[iel].Elems[nf] = -1;   // En principio limita con el exterior

	    if (nf > 0) el = elg->SetAnElem(iel);

	    if (nf == 0)
		{
		firstnode  = el->GetNode(0);
		secondnode = el->GetNode(1);
		thirdnode  = el->GetNode(2);
		fourthnode = el->GetNode(3);
		}
	    else if (nf == 1)
		{
		firstnode  = el->GetNode(1);
		secondnode = el->GetNode(0);
		thirdnode  = el->GetNode(3);
		fourthnode = el->GetNode(2);
		}
	    else if (nf == 0)
		{
		firstnode  = el->GetNode(1);
		secondnode = el->GetNode(3);
		thirdnode  = el->GetNode(2);
		fourthnode = el->GetNode(0);
		}
	    else
		{
		firstnode  = el->GetNode(0);
		secondnode = el->GetNode(2);
		thirdnode  = el->GetNode(3);
		fourthnode = el->GetNode(1);
		}

	    // loop sobre os elems. associados ao 1er no' da face
	    nelasoc = (int) noel->GetNelems(firstnode);
	    nmsw += 2;
	    for (l = nelasoc; l > 0;)
		{
		l--;
		ielv = noel->GetElem(firstnode, l);
		if (ielv <= iel) break;                  // La estructura noel DEBE estar ORDENADA
		msw[ielv] = nmsw;
		}

	    // loop sobre os elems. associados ao 2do no' da face
	    nelasoc = (int) noel->GetNelems(secondnode);
	    for (l = nelasoc; l >0;)
		{
		l--;
		ielv = noel->GetElem(secondnode, l);
		if (ielv <= iel) break;                  // La estructura noel DEBE estar ORDENADA
		msw[ielv]++;
		}

	    // loop sobre os elems. associados ao 3er no' da face
	    nelasoc = (int) noel->GetNelems(thirdnode);
	    for (l = nelasoc; l >0;)
		{
		l--;
		ielv = noel->GetElem(thirdnode, l);
		if (ielv <= iel) break;                 // La estructura noel DEBE estar ORDENADA
		if (msw[ielv] == nmsw + 1)	// Neighbour found !
		    {
		    // Check if neighbour has correct orientation and has no neighbour
		    el = elg->SetAnElem(ielv);
		    if (el->GetNode(3) != firstnode && el->GetNode(3) != secondnode && el->GetNode(3) != thirdnode)
			{ // 1ra cara (n1,n2,n3)
			if (el->GetNode(0) == firstnode)
			    if (el->GetNode(1) == secondnode) // incorrect orientation
				{
				Error(WARNING, 2, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else if (el->GetNode(0) == secondnode)
			    if (el->GetNode(1) == thirdnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else
			    if (el->GetNode(1) == firstnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			if (SI[ielv].Elems[0] != -2)
			    {
			    Error(WARNING, 13,
			    "Elem: %ld %ld %ld - %ld, not neighbour of: %ld %ld %ld %ld",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
			    continue; // has a neighbour
			    }
			SI[ielv].Elems[0] = iel;
			}
		    else if (el->GetNode(2) != firstnode && el->GetNode(2) != secondnode && el->GetNode(2) != thirdnode)
			{ // 2da cara (n2,n1,n4)
			if (el->GetNode(1) == firstnode)
			    if (el->GetNode(0) == secondnode) // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else if (el->GetNode(1) == secondnode)
			    if (el->GetNode(0) == thirdnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else
			    if (el->GetNode(0) == firstnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			if (SI[ielv].Elems[1] != -2)
			    {
			    Error(WARNING, 13,
			    "Elem: %ld %ld %ld - %ld, not neighbour of: %ld %ld %ld %ld",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
			    continue; // has a neighbour
			    }
			SI[ielv].Elems[1] = iel;
			}
		    else if (el->GetNode(0) != firstnode && el->GetNode(0) != secondnode && el->GetNode(0) != thirdnode)
			{ // 3ra cara (n2,n4,n3)
			if (el->GetNode(1) == firstnode)
			    if (el->GetNode(3) == secondnode) // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else if (el->GetNode(1) == secondnode)
			    if (el->GetNode(3) == thirdnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else
			    if (el->GetNode(3) == firstnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			if (SI[ielv].Elems[2] != -2)
			    {
			    Error(WARNING, 13,
			    "Elem: %ld %ld %ld - %ld, not neighbour of: %ld %ld %ld %ld",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
			    continue; // has a neighbour
			    }
			SI[ielv].Elems[2] = iel;
			}
		    else if (el->GetNode(1) != firstnode && el->GetNode(1) != secondnode && el->GetNode(1) != thirdnode)
			{ // 4ta cara (n1,n3,n4)
			if (el->GetNode(0) == firstnode)
			    if (el->GetNode(2) == secondnode) // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else if (el->GetNode(0) == secondnode)
			    if (el->GetNode(2) == thirdnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			else
			    if (el->GetNode(2) == firstnode)  // incorrect orientation
				{
				Error(WARNING, 1, "Elem: %ld %ld %ld - %ld, and %ld %ld %ld %ld folded",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
				continue;
				}
			if (SI[ielv].Elems[3] != -2)
			    {
			    Error(WARNING, 13,
			    "Elem: %ld %ld %ld - %ld, not neighbour of: %ld %ld %ld %ld",
				firstnode, secondnode, thirdnode, fourthnode,
				el->GetNode(0), el->GetNode(1), el->GetNode(2), el->GetNode(3));
			    continue; // has a neighbour
			    }
			SI[ielv].Elems[3] = iel;
			}

		    SI[iel].Elems[nf] = ielv;
		    break;
		    }
		}	// End loop elems joining thirdnode

	    }  // Next cara del elemento
	} // Next elemento

//    MessageBox(GetFocus(),"Sale de ElElV","Testing...",MB_OK);

    if (!NoEl)
       delete noel;
    mFree((char *) msw);
    TraceOff(nome);
    }

