#include "sparse.h"

SparseElNo::SparseElNo(ElGroups *elg)
    {
    int nnoel,k;
    long iel;
    Element *el;
    TIndex   = elg->GetNumEls();
    char buf[40];
    char *nome = "SparseElNo::SparseElNo";
    TraceOn(nome);

    SI = (SparseInfo   *) mMalloc(sizeof(SparseInfo) * TIndex);
    if (!SI)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    iel = 0;
    for (el = elg->SetFirstElem(); el; el = elg->SetNextElem(), iel++)
        {
        nnoel = el->NNoel();
        SI[iel].n     = nnoel;
        SI[iel].Elems = (long   *) mMalloc(sizeof(long) * nnoel);
        if (!SI[iel].Elems)
            { //    1, "Can't Allocate Memory."
            GetError(1, buf);
            Error(FATAL_ERROR, 1, buf);
            }
        for (k = 0; k < nnoel; k++)
            SI[iel].Elems[k] = el->GetNode(k);
        }

    TraceOff(nome);
    }
