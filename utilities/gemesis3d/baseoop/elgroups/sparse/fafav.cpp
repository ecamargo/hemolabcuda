#include "sparse.h"

SparseFaFaV::SparseFaFaV(ElGroups *elg, SparseNoEl *NoEl)
{
  static char *nome = "SparseFaFaV::SparseFaFaV";
  char buf[60];
  TraceOn(nome);
  int nfac, nf;
  long iel, nmsw, l;
  Element *el;
  long   *msw;
  SparseNoEl *noel;
  long vn[16];

  TIndex = elg->GetNumEls();
  SI     = (SparseInfo   *) mCalloc(sizeof(SparseInfo), TIndex);
  if (!SI)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}

  if(!NoEl)
		noel = new SparseNoEl(elg);
  else
		noel = NoEl;
		
	// cria area e limpa vetor
  msw = (long   *) mCalloc(sizeof(long), TIndex);
  if (!msw)
		{ //    1, "Can't Allocate Memory."
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}

  iel = 0; nmsw = 1;

  for (el = elg->SetFirstElem(); el; el = elg->SetNextElem(), iel++)
  	{
    nfac = el->NEdges();
    SI[iel].n = nfac;
		// PUAJ!!!       el->ElemInci(vn);
		vn[0] = el->GetNode(0);
		vn[1] = el->GetNode(1);
		vn[2] = el->GetNode(2);

		if(nfac!=0)
	    {
	    SI[iel].Elems = (long   *) mMalloc(nfac * sizeof(long));
	    if (!SI[iel].Elems)
				{ //    1, "Can't Allocate Memory."
				GetError(1, buf);
				Error(FATAL_ERROR, 3, buf);
				}
	    }

		for (nf = 0; nf < nfac; nf++)  // loop sobre as aristas do el
      {
      SI[iel].Elems[nf] = -1;

      // loop sobre os elems. associados ao 1er no' da arista
      for (l = 0; l < noel->GetNelems(vn[nf]); l++)
          msw[noel->GetElem(vn[nf], l)] = nmsw;

      // loop sobre os elems. associados ao ultimo no' da arista
      int nfa = nf+1;
      if(nfa==nfac) nfa=0;
      for (l = 0; l < noel->GetNelems(vn[nfa]); l++)
        if ((msw[noel->GetElem(vn[nfa], l)] == nmsw) &&
            (iel != noel->GetElem(vn[nfa], l)))
            {
            SI[iel].Elems[nf] = noel->GetElem(vn[nfa], l);
            break;
            }

      nmsw++;
      }  // loop aristas
    }  // loop elementos

  if (!NoEl)
     delete noel;
  mFree((char *) msw);
  TraceOff(nome);
}
