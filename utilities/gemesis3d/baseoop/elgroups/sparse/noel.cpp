#include "sparse.h"
#include "iostream"

SparseNoEl::SparseNoEl(ElGroups *elg)
{
    int k;
    long j, iel, n;
    Element *el;
    TIndex   = elg->GetNumNodes();
    SparseInfo   *aux;
    char buf[40];
    char *nome = "SparseNoEl::SparseNoEl";
    TraceOn(nome);

    SI = (SparseInfo   *) mMalloc(sizeof(SparseInfo) * TIndex);
    if (!SI)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    aux = SI;
    for (j = 0; j < TIndex; j++, aux++)
        aux->n = 0;

    // calculo dos SI[j].n
    for (el = elg->SetFirstElem(); el; el = elg->SetNextElem())
       {
       for (k = 0; k < el->NNoel(); k++)
           {
           n = el->GetNode(k);
           SI[n].n++;
           }
       }
       
    aux = SI;
    for (j = 0; j < TIndex; j++, aux++)
       {
       if (aux->n)
           {
           aux->Elems = (long   *) mMalloc(aux->n * sizeof(long));
           if (!aux->Elems)
              { //    1, "Can't Allocate Memory."
              GetError(1, buf);
              Error(FATAL_ERROR, 1, buf);
              }
           }
       else
           aux->Elems = NULL;
       aux->n     = 0;
       }

    // calculo dos SI[j].n e  SI[j].Elems
    iel = 0;
    for (el = elg->SetFirstElem(); el; el = elg->SetNextElem(), iel++)
       {
       for (k = 0; k < el->NNoel(); k++)
           {
           n = el->GetNode(k);
           SI[n].Elems[SI[n].n] = iel;
           SI[n].n++;
           }
       }
    TraceOff(nome);
}
