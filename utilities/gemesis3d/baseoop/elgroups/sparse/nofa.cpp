#include "sparse.h"
#include "surface.h"

SparseNoFa::SparseNoFa(Surface *Surf)
    {
    static char *nome= "SparseNoFa::SparseNoFa";
    TraceOn(nome);
    long j, inci[3];
    TIndex = Surf->GetNumNodes();
    SparseInfo   *aux;
    char buf[40];
    long num, node;

    SI = (SparseInfo   *) mMalloc(sizeof(SparseInfo) * TIndex);
    if (!SI)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 18, buf);
	}

    aux = SI;
    for (j = 0; j < TIndex; j++, aux++)
	aux->n = 0;

    // calculo dos SI[j].n
    for (j = 0; j < Surf->GetNFacets(); j++)
	{
	Surf->GetInciFace(j, inci);
	SI[inci[0]].n++;
	SI[inci[1]].n++;
	SI[inci[2]].n++;
	}

    aux = SI;
    for (j = 0; j < TIndex; j++, aux++)
	{
	if (aux->n > 0)
	    {
	    aux->Elems = (long   *) mMalloc(aux->n * sizeof(long));
	    if (!aux->Elems)
		{ //    1, "Can't Allocate Memory."
		GetError(1, buf);
		Error(FATAL_ERROR, 43, buf);
		}
	    }
	aux->n     = 0;
	}

    // calculo dos SI[j].n e  SI[j].Elems
    for (j = 0; j < Surf->GetNFacets(); j++)
	{
	Surf->GetInciFace(j, inci);
	node = inci[0]; num = SI[node].n;
	SI[node].Elems[num] = j;
	SI[node].n++;
	node = inci[1]; num = SI[node].n;
	SI[node].Elems[num] = j;
	SI[node].n++;
	node = inci[2]; num = SI[node].n;
	SI[node].Elems[num] = j;
	SI[node].n++;
	}
    TraceOff(nome);
    }

