#include "sparse.h"

StructSparse::~StructSparse()
    {
    long i;

    for (i = 0; i < TIndex; i++)
        if (SI[i].n)
            mFree((char *) SI[i].Elems);
    mFree((char *) SI);
    }
