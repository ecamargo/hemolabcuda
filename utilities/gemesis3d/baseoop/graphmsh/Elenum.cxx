#include "grafmesh.h"
#include <math.h>

long GraphMesh::ElementNumber(acLPoint2 &p, int flgrp)
    {
    if (!ElemFlag) return(-1);

    long face, elem;
    acPoint2 pw, center;
    numlist nl;
    pw = Scr->LogToWorld(p);
    face = Surf->FindFace(pw);
    if (face >= 0)
	{
	center = Surf->GetXYCenter(face, Rot);
	elem   = Surf->GetElem(face) + 1;

	Scr->TextBkMode(acTransparent);
	Scr->TextColor(acBLACK);
//	Scr->TextAlign(ACTX_CENTER, ACTY_BOTTOM);
	if (flgrp)
	    Scr->Text(center, "%ld (%d)", elem, Surf->GetGrp(face)+1);
	else
	    Scr->Text(center, "%ld", elem);

       nl.p   = center;
       nl.nro = elem;
       nl.flg = SelectFlag;
       Lnum.InsertFirst(&nl);
       return(nl.nro);
       }
    else
       return(-1);
    }
