/*
** graphmesh.cpp
** Raul, Salgado & Venere - may 1992
** Grafica Abstracao geometrica de superficies (sem smooth) de malhas 3d
*/
#include "grafmesh.h"
#include <math.h>
#include <stdio.h>

GraphMesh::GraphMesh(Mesh3d *mesh, int hiddenflag, int explodedmesh,
		     int closedsurf, double pview_tita, double pview_phi,
		     SparseElElV *elelv)
    {
    char nome[]="GraphMesh::GraphMesh";
    TraceOn(nome);

    HiddenFlag = hiddenflag;

    Mesh       = mesh;
    if(!elelv)
    	{
	    SpElElV = Mesh->NewElElV();
	    MustDelElElV = 1;
	    }
    else
	    {
	    SpElElV = elelv;
	    MustDelElElV = 0;
	    }

    Qlty     = NULL;
    Rot      = new acRotation(pview_tita, pview_phi);
    ElStatus = (int  *) mMalloc(sizeof(int)*Mesh->GetNumEls());
    if (!ElStatus)
    	{ //    1, "Can't Allocate Memory."
	    char buf[40];
	    GetError(1, buf);
	    Error(FATAL_ERROR, 1, buf);
	    }

    for (long i=0; i<Mesh->GetNumEls(); i++)
	    ElStatus[i] = 1;

    WireFlag = 0;
    ClosedFlag = closedsurf;
    ExpFlag    = explodedmesh;
    Surf       = new Surface(Rot, Mesh, ElStatus, ClosedFlag, ExpFlag, SpElElV);

    if (HiddenFlag)
       Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
    else
       Segm = NULL;

    Act = Surf->GetXYLimit();
    Ori = Act;

    ActStack = new acStack(sizeof(acLimit2));

    Scr   = NULL;

    // Define la paleta de colores
    acCobundl cb[6];
    cb[0]=acCobundl(1.0F, 0.0F, 1.0F);   //Magenta
    cb[1]=acCobundl(0.5F, 1.0F, 0.5F);   //Verdoso
    cb[2]=acCobundl(0.0F, 1.0F, 1.0F);   //Cyan         Para los grupos
    cb[3]=acCobundl(1.0F, 1.0F, 0.0F);   //Amarillo
    cb[4]=acCobundl(1.0F, 0.5F, 0.5F);   //Ladrillo
    cb[5]=acCobundl(0.5F, 0.5F, 1.0F);   //Azuloso

    int ng,CpG;
//    ng = Mesh->GetNumGroups();
//    if (ng > 6) ng = 6;
    ng = 6;             // Colormap independiente del numero de grupos !!
    CpG = NumMaxColors / ng;
    int j = 1;
    Colors[0].x = Colors[0].y = Colors[0].z = 0.5;
    for (int gr = 0; gr<ng; gr++)
    	{
	    for (int i = 0; i<CpG; i++)
	        {
	        Colors[j].x = cb[gr].x * (i+1) / CpG;
	        Colors[j].y = cb[gr].y * (i+1) / CpG;
	        Colors[j].z = cb[gr].z * (i+1) / CpG;
	        j++;
	        }
	    }

    FacWst = 1.0;
    SelectFlag= 0;
    EdgesFlag = 1;
    LightFlag = 1;
    ElemFlag  = NodeFlag = 0;
    AElemFlag = ANodeFlag = 0;
    Lnum.SetListData(sizeof(numlist));

    SpNoEl = Mesh->NewNoEl();
    Edges = NULL;
    WireFlag = 0;

    TraceOff(nome);
    }

GraphMesh::~GraphMesh()
    {
    if (Qlty) delete Qlty;
    delete Rot;
    mFree(ElStatus);
    delete Surf;
    delete SpNoEl;
    if (MustDelElElV) delete SpElElV;
    if (Segm) delete Segm;
    Lnum.Clear();
    }

void GraphMesh::SetAxesFlag(int flag)
    {AxesFlag = flag;}

void GraphMesh::Rotate(double theta, double phi)
    {
    while (ActStack->Pop(&Act)) ;
    Rot->SetMat(theta, phi);
    Surf->Rotate(Rot, 1);
    Ori = Act = Surf->GetXYLimit();
    if(HiddenFlag)
       {
       if (Segm) delete Segm;
       Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
       }
    Lnum.Clear();
    }

void GraphMesh::Rotate(double theta, double phi,
        int wireframe, int keepzoom, acPoint3 ZCenter)
    {
    while (ActStack->Pop(&Ori));
    Lnum.Clear();
    if (keepzoom)
        {
        acLimit2 ActRel;
        acPoint2 zcenter;
        zcenter = Rot->GetXY(ZCenter);
        ActRel.xmin = Act.xmin - zcenter.x;
        ActRel.xmax = Act.xmax - zcenter.x;
        ActRel.ymin = Act.ymin - zcenter.y;
        ActRel.ymax = Act.ymax - zcenter.y;
        Rot->SetMat(theta, phi);
        Surf->Rotate(Rot, !wireframe);
        Ori = Surf->GetXYLimit();
        zcenter = Rot->GetXY(ZCenter);
        Act.xmin = ActRel.xmin + zcenter.x;
        Act.xmax = ActRel.xmax + zcenter.x;
        Act.ymin = ActRel.ymin + zcenter.y;
        Act.ymax = ActRel.ymax + zcenter.y;
        }
    else
        {
        Rot->SetMat(theta, phi);
        Surf->Rotate(Rot, !wireframe);
        Ori = Act = Surf->GetXYLimit();
        }

    WireFlag = wireframe;
    if (!WireFlag && HiddenFlag)
        {
        if (Segm) delete Segm;
        Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
        }
    }

void GraphMesh::SetFlags(int hiddenflag,
			             int shadingflag,
			             int explodedflag,
			             int closedflag,
			             int edgesflag,
			             int nodesflag,
			             int elementsflag,
                         int axesflag)
    {
    EdgesFlag = edgesflag;
    LightFlag = shadingflag;
    ANodeFlag = nodesflag;
    AElemFlag = elementsflag;
    AxesFlag  = axesflag;

    if (ClosedFlag != closedflag)
	{
	ClosedFlag = closedflag;
	Surf->SetElemIs3D(closedflag, Rot);
	}

    if (ExpFlag != explodedflag)
	{
	ExpFlag    = explodedflag;
	delete Surf;
	Surf  = new Surface(Rot, Mesh, ElStatus, ClosedFlag, ExpFlag);
	Lnum.Clear();
	}
    HiddenFlag = hiddenflag;
    if (Segm) delete Segm;
    Segm = NULL;
    if(HiddenFlag)
	{
	Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
	}
    }

void GraphMesh::CopyElStatus (int   *Elsw)
    {
    for (long i = 0; i<Mesh->GetNumEls(); i++)
	Elsw[i] = ElStatus[i];
    }

void GraphMesh::SetElStatus (int   *Elsw)
    {
    for (long i = 0; i<Mesh->GetNumEls(); i++)
	ElStatus[i] = Elsw[i];
    }

void GraphMesh::Graph()
    {
    char *nome="GraphMesh::Graph";
    TraceOn(nome);

    long inci[3];
    acPoint2 vc[4], segment[2];
    acPoint2 *seg;
    double coli;
    int col;

    int ng,CpG;
    ng = 6;                   // Colormap independiente de NumGroups !!
    CpG = NumMaxColors / ng;

    Scr->SetPalette(NumMaxColors+1, Colors);
    Scr->SetClip(0);
    Scr->TextBkMode(acTransparent);
//    Scr->TextAlign(ACTX_CENTER, ACTY_BOTTOM);

    long na, nb;

    if (WireFlag)
        {
        Scr->SetPen(acBLACK);
        acCoordinate3D *c = Surf->GetCoordRot();
        acCoordinate3D &coor = *c;
        for (na=0; na<Edges->GetTIndex(); na++)
            {
            segment[0].x = coor[na].x;
            segment[0].y = coor[na].y;

            for (long i=0; i<Edges->GetNelems(na); i++)
                {
                nb = Edges->GetElem(na,i);
                segment[1].x = coor[nb].x;
                segment[1].y = coor[nb].y;
                Scr->PolyLine(2, segment);
                }
            }
       }
    else
       {
       if (HiddenFlag)
        {
        Scr->SetPen(acBLACK);
        for (seg=Segm->FirstSegment(); seg; seg=Segm->NextSegment())
	      Scr->PolyLine(2, seg);
        }
    else
        {
        Scr->SetPen(acGRAY, 1, acNullPen);
        for (long fac = 0; fac < Surf->GetNFacets(); fac++)
            {
            if (Surf->FaceOn(fac))
                {
                if (LightFlag ==1)
                   {
                   coli = Surf->GetColInt(fac, Rot);
                   ng = Surf->GetGrp(fac) % 6;
                   col = (int)(coli * CpG);
                   if (col == CpG)
                       col--;
                   col = (int)(ng*CpG + col)+1;
                   }
                else if (LightFlag == 0)
                   {
                   coli = 1.0;

                   ng = Surf->GetGrp(fac) % 6;
                   col = (int)(coli * CpG);
                   if (col == CpG)
                       col--;
                   col = (int)(ng*CpG + col)+1;
                   }
                else
                   col = acWHITE;

                Scr->SetBrush(col);

                Surf->GetInfoFace(fac, inci, vc);

                Scr->Polygon(3, vc);

                if (EdgesFlag)
                    {
        		    Scr->SetPen(acGRAY);
	            	int edg1, edg2, edg3;
            		edg1 = Surf->EdgeOn(fac,0);    // 1: Arista interior 2: Borde
            		edg2 = Surf->EdgeOn(fac,1);
            		edg3 = Surf->EdgeOn(fac,2);
//		    if (Surf->EdgeOn(fac,0))
	            	if (edg1 && ((EdgesFlag-1) || ((edg1-1))))
            			Scr->PolyLine(2, &vc[0]);
//			    if (Surf->EdgeOn(fac,1))
	            	if (edg2 && ((EdgesFlag-1) || ((edg2-1))))
	        			Scr->PolyLine(2, &vc[1]);
//        		    if (Surf->EdgeOn(fac,2))
	            	if (edg3 && ((EdgesFlag-1) || ((edg3-1))))
	            		{
			            vc[3] = vc[0];
			            Scr->PolyLine(2, &vc[2]);
			            }
		            }

		if (AElemFlag || ANodeFlag)
		    Scr->TextColor(acBLACK);

                if (AElemFlag)
                    Scr->Text(Surf->GetXYCenter(fac, Rot),
                              "%ld", Surf->GetElem(fac)+1);

                if (ANodeFlag)
                    {
                    Scr->Text(vc[0], "%ld", inci[0]+1);
                    Scr->Text(vc[1], "%ld", inci[1]+1);
                    Scr->Text(vc[2], "%ld", inci[2]+1);
                    }
                }
            }
        }
       }

    acNodeList *pcno;
    pcno  = Lnum.GetHead();
    numlist *pno;

    Scr->TextColor(acBLACK);
//    Scr->TextAlign(ACTX_CENTER, ACTY_BOTTOM);
    while (pcno)
       {
       pno = (numlist *) pcno->GetDados();
       Scr->Text (pno->p, "%ld", pno->nro);
       pcno = pcno->next;
       }

    if (AxesFlag)
	GraphAxes();

    TraceOff(nome);
    }

void GraphMesh::WhereIs()
    {
    acPoint2 segment[3];

    Element *el = SetAnElem(elEl);
    acPoint3 gc = el->GravCenter();
    Scr->SetPen(acGRAY);

//    acLimit2 Wc  = Scr->DCtoWC();
//    acLimit2 Wc  = Scr->GetWindow();
    acLimit2 Wc = Act;
    segment[1]   = Rot->GetXY(gc);
    segment[0].x = Wc.xmin;
    segment[0].y = Wc.ymin;
    segment[2].x = Wc.xmax;
    segment[2].y = Wc.ymax;
    Scr->PolyLine(3, segment);
    segment[0].x = Wc.xmin;
    segment[0].y = Wc.ymax;
    segment[2].x = Wc.xmax;
    segment[2].y = Wc.ymin;
    Scr->PolyLine(3, segment);
    }

void GraphMesh::RemoveSelEls()
    {
    acNodeList *pcno;
    pcno  = Lnum.GetHead();
    numlist *pno;
    while (pcno)
	{
	pno = (numlist *) pcno->GetDados();
	switch (pno->flg)
	    {
	    case 1:
		ElStatus[pno->nro-1] = 0;
		break;
	    case 2:
		break;
	    default:
		break;
	    }
	pcno = pcno->next;
	}
    RemoveEls();
    }

void GraphMesh::RemoveEls()
    {
    delete Surf;
    Surf  = new Surface(Rot, Mesh, ElStatus, ClosedFlag, ExpFlag);
    if(HiddenFlag)
	{
	if (Segm) delete Segm;
	Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
	}
    SelectFlag = 0;
    Lnum.Clear();
    }

long GraphMesh::SelHalfSpace(int   *Elsw, acPoint3 poin, acPoint3 norm, int add, int inters)
    {
    acCoordinate3D *cr = Mesh->GetCoords();
    acCoordinate3D &rcoord = *cr; 
    acPoint3 pp;
    Element *el;
    long i, nn, nsel;
    double d, dis;
    int swincluded;

    d = -(norm.x*poin.x + norm.y*poin.y + norm.z*poin.z);

    nsel = 0;
    for (el = Mesh->SetFirstElem(), i = 0; el; el = Mesh->SetNextElem(), i++)
	{
	if (add != Elsw[i])
	    {
	    if (inters)
		swincluded = 0;
	    else
		swincluded = 1;
	    for (int j=0; j<el->NNoel(); j++)
		{
		nn = el->GetNode(j);
		pp = rcoord[nn];
		dis= norm.x*pp.x + norm.y*pp.y + norm.z*pp.z + d;
		if (inters)
		    {
		    if (dis > 0.0)
			{swincluded = 1; break; }
		    }
		else
		    {
		    if (dis < 0.0)
			{swincluded = 0; break; }
		    }
		}
	    if (swincluded) Elsw[i] = add;
	    }
	if (Elsw[i]) nsel++;
	}
    return nsel;
    }

long GraphMesh::SelBox(int   *Elsw, acPoint3 c1, acPoint3 c2, int add, int inters)
    {
    acCoordinate3D *cr = Mesh->GetCoords(); 
    acCoordinate3D &rcoord = *cr;
    acPoint3 pp;
    Element *el;
    long i, nn, nsel;
    double dx, dy, dz;
    int swincluded;

    nsel = 0;
    for (el = Mesh->SetFirstElem(), i = 0; el; el = Mesh->SetNextElem(), i++)
	{
	if (add != Elsw[i])
	    {
	    if (inters)
		swincluded = 0;
	    else
		swincluded = 1;
	    for (int j=0; j<el->NNoel(); j++)
		{
		nn = el->GetNode(j);
		pp = rcoord[nn];
		dx = (pp.x - c1.x)*(pp.x - c2.x);
		dy = (pp.y - c1.y)*(pp.y - c2.y);
		dz = (pp.z - c1.z)*(pp.z - c2.z);

		if (inters)
		    {
		    if (dx < 0.0 && dy < 0.0 && dz < 0.0)
			{ swincluded = 1; break; }
		    }
		else
		    {
		    if (dx > 0.0 || dy > 0.0 || dz > 0.0)
			{ swincluded = 0; break; }
		    }
		}
	    if (swincluded) Elsw[i] = add;
	    }
	if (Elsw[i]) nsel++;
	}
    return nsel;
    }

long GraphMesh::SelSphere(int   *Elsw, acPoint3 center, double radius, int add, int inters)
    {
    acCoordinate3D *cr = Mesh->GetCoords(); 
    acCoordinate3D &rcoord = *cr;
    acPoint3 pp;
    Element *el;
    long i, nn, nsel;
    double dis;
    int swincluded;

    radius *= radius;

    nsel = 0;
    for (el = Mesh->SetFirstElem(), i = 0; el; el = Mesh->SetNextElem(), i++)
	{
	if (add != Elsw[i])
	    {
	    if (inters)
		swincluded = 0;
	    else
		swincluded = 1;
	    for (int j=0; j<el->NNoel(); j++)
		{
		nn = el->GetNode(j);
		pp = rcoord[nn];
		dis= radius -
		    ((pp.x-center.x)*(pp.x-center.x) +
		     (pp.y-center.y)*(pp.y-center.y) +
		     (pp.z-center.z)*(pp.z-center.z));
		if (inters)
		    {
		    if (dis > 0.0)
			{swincluded = 1; break; }
		    }
		else
		    {
		    if (dis < 0.0)
			{swincluded = 0; break; }
		    }
		}
	    if (swincluded) Elsw[i] = add;
	    }
	if (Elsw[i]) nsel++;
	}
    return nsel;
    }

long GraphMesh::SelCylinder(int   *Elsw, acPoint3 center, acPoint3 axis,
			   double radius, int add, int inters)
    {
    acCoordinate3D *cr = Mesh->GetCoords(); 
    acCoordinate3D &rcoord = *cr;
    acPoint3 pp;
    Element *el;
    long i, nn, nsel;
    double dis, mod, pvx, pvy, pvz;
    int swincluded;

    mod = sqrt(axis.x*axis.x + axis.y*axis.y + axis.z*axis.z);
    axis.x /= mod; axis.y /= mod; axis.z /= mod;
    radius *= radius;

    nsel = 0;
    for (el = Mesh->SetFirstElem(), i = 0; el; el = Mesh->SetNextElem(), i++)
	{
	if (add != Elsw[i])
	    {
	    if (inters)
		swincluded = 0;
	    else
		swincluded = 1;
	    for (int j=0; j<el->NNoel(); j++)
		{
		nn = el->GetNode(j);
		pp = rcoord[nn];
		pvx = (pp.y - center.y)*axis.z - (pp.z - center.z)*axis.y;
		pvy = (pp.z - center.z)*axis.x - (pp.x - center.x)*axis.z;
		pvz = (pp.x - center.x)*axis.y - (pp.y - center.y)*axis.x;
		dis= radius - (pvx*pvx + pvy*pvy + pvz*pvz);

		if (inters)
		    {
		    if (dis > 0.0)
			{swincluded = 1; break; }
		    }
		else
		    {
		    if (dis < 0.0)
			{swincluded = 0; break; }
		    }
		}
	    if (swincluded) Elsw[i] = add;
	    }
	if (Elsw[i]) nsel++;
	}
    return nsel;
    }

long GraphMesh::SelEl(int   *Elsw, long fromel, long toel, long step, int add)
    {
    long el, nsel;

    nsel = 0;
    for (el = fromel-1; el < toel; el+=step)
	{
	if (add != Elsw[el])
	    {
	    Elsw[el] = add;
	    }
	}
    for (long i=0; i<Mesh->GetNumEls(); i++)
	if (Elsw[i]) nsel++;
    return nsel;
    }

void GraphMesh::WoMiAv(long  &wstel, double &Mcal, double &Acal)
	    {
	    double maxq;
	    if (!Qlty)
	    Qlty = new Quality(Mesh);
	    Qlty->GetQualities(Mcal, maxq, Acal);
	    wstel = Qlty->GetWorstest()+1;
	    }

long GraphMesh::SelNod(int   *Elsw, long fromnod, long tonod, long step, int add)
    {
    long i, el, j, nsel, nelems;

    nsel = 0;
    for (i = fromnod-1; i < tonod; i += step)
	{
	nelems = SpNoEl->GetNelems(i);
	for (j=0; j<nelems; j++)
	    {
	    el = SpNoEl->GetElem(i,j);
	    if (add != Elsw[el])
		Elsw[el] = add;
	    }
	}

    for (i=0; i<Mesh->GetNumEls(); i++)
	if (Elsw[i]) nsel++;
    return nsel;
    }

void GraphMesh::UnRemove()
    {
    delete Surf;
    for (long i=0; i<Mesh->GetNumEls(); i++)
	ElStatus[i] = 1;
    Surf  = new Surface(Rot, Mesh, ElStatus, ClosedFlag, ExpFlag);
    if(HiddenFlag)
	{
	if (Segm) delete Segm;
	Segm = new EdgesSegments(Surf, EdgesFlag, Rot);
	}
    FacWst     = 1.0;
    SelectFlag = 0;
    Lnum.Clear();
    }

long GraphMesh::NodeNumber(acLPoint2 &p)
    {
    if (!NodeFlag || ExpFlag) return(-1);

    long face;
    int no;
    long inci[3];
    acPoint2 pw, vc[3];
    double dm, d;
    numlist nl;
    pw   = Scr->LogToWorld(p);
    face = Surf->FindFace(pw);
    if (face >= 0)
       {
       Scr->TextBkMode(acTransparent);
       Scr->TextColor(acBLACK);
//       Scr->TextAlign(ACTX_CENTER, ACTY_BOTTOM);
       Surf->GetInfoFace(face, inci, vc);
       no = 0;
       dm = (vc[0].x-pw.x)*(vc[0].x-pw.x) +
            (vc[0].y-pw.y)*(vc[0].y-pw.y);
       d  = (vc[1].x-pw.x)*(vc[1].x-pw.x) +
            (vc[1].y-pw.y)*(vc[1].y-pw.y);
       if (d < dm) { dm = d; no = 1; }
       d  = (vc[2].x-pw.x)*(vc[2].x-pw.x) +
            (vc[2].y-pw.y)*(vc[2].y-pw.y);
       if (d < dm) no = 2;

       Scr->Text(vc[no], "%ld", inci[no]+1);

       nl.p   = vc[no];
       nl.nro = inci[no] + 1;
       nl.flg = 0;
       Lnum.InsertFirst(&nl);
       return(nl.nro);
       }
    else
       return(-1);
    }

char *GraphMesh::Information()
    {
    static char buf[250];
    long  nnos    = Mesh->GetNumNodes();
    long  neles   = Mesh->GetNumEls();
    long  ngroups = Mesh->GetNumGroups();
    long  wstel;

    double Mcal, Acal, maxq;
    if (!Qlty)
	Qlty = new Quality(Mesh);
    Qlty->GetQualities(Mcal, maxq, Acal);
    wstel = Qlty->GetWorstest()+1;

    sprintf(buf, "Total number of nodes   : %8ld\n\r"
		 "Total number of groups  : %8ld\n\r"
		 "Total Number of elements: %8ld\n\r"
		 "Worst element           : %8ld\n\r"
		 "Minimum Quality         : %lg\n\r"
		 "Average Quality         : %lg\n\r",
		 nnos, ngroups, neles, wstel, Mcal, Acal);

    return buf;
	 }

void GraphMesh::SetAxes(acLPoint2 &plci)
    {
    double dx, dy;
    int i;
    acPoint2 pci;
    
    pci = Scr->LogToWorld(plci);

    vertax[0].x = 0.0; vertax[0].y = 0.0; vertax[0].z = 0.0;
    vertax[1].x = 1.0; vertax[1].y = 0.0; vertax[1].z = 0.0;
    vertax[2].x = 0.0; vertax[2].y = 1.0; vertax[2].z = 0.0;
    vertax[3].x = 0.0; vertax[3].y = 0.0; vertax[3].z = 1.0;

    for (i=0; i<4; i++)
	vertax[i] = Rot->GetXYZ(vertax[i]);

    dx = (Act.xmax - Act.xmin)/6.0;
    dy = (Act.ymax - Act.ymin)/6.0;

    vertax[0].x = pci.x;
    vertax[0].y = pci.y;
    for (i=1; i<4; i++)
	{
	vertax[i].x = vertax[i].x*dx + pci.x;
	vertax[i].y = vertax[i].y*dy + pci.y;
	}
    }

void GraphMesh::GraphAxes()
    {
    acPoint2 face[3];

    Scr->TextBkMode(acTransparent);
    Scr->TextColor(acBLACK);

    Scr->SetPen(acBLACK);
    face[0].x = vertax[1].x;
    face[1].x = vertax[0].x;
    face[2].x = vertax[2].x;
    face[0].y = vertax[1].y;
    face[1].y = vertax[0].y;
    face[2].y = vertax[2].y;
    Scr->PolyLine(3, face);
    Scr->Text(face[0], "X");
    Scr->Text(face[2], "Y");

    face[0].x = vertax[0].x;
    face[1].x = vertax[3].x;
    face[0].y = vertax[0].y;
    face[1].y = vertax[3].y;
    Scr->PolyLine(2, face);
    Scr->Text(face[1], "Z");
    }
