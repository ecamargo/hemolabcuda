#include "grafmesh.h"
#include "acdp.h"
//#include "printer.h"

extern BOOL      bUserAbort;
extern HWND      hDlgPrint;
extern HINSTANCE hInst;
//HDC GetPrinterDC(void);
BOOL PASCAL _export PrintAbortMsg(HDC hdc, int nCode);
LRESULT CALLBACK PrintAbortDlg(HWND hDlg, UINT wMsg, WPARAM wParam,
				      LPARAM lParam);
LPSTR GetErrorText(int nErrorCode);

void GraphMesh::PrinterPaint(acDCInfo &dci)
    {

    static char szMessage[] = "ViewMesh Printer Output";
    FARPROC     fpAbortProc;
    FARPROC     fpDlgPrint;
    int         nEscError;
    HDC         hPrnDC = dci.ws;
    HWND        hwnd = dci.wnd;

    // Install dialog box and Abort function
    EnableWindow(hwnd, FALSE);
    bUserAbort  = FALSE;

    fpDlgPrint = MakeProcInstance((FARPROC) PrintAbortDlg, hInst);
    hDlgPrint  = CreateDialog(hInst, "PrintDlgBox", hwnd, (DLGPROC)fpDlgPrint);

    fpAbortProc = MakeProcInstance((FARPROC) PrintAbortMsg, hInst);
    Escape(hPrnDC, SETABORTPROC, 0, (LPSTR)fpAbortProc, NULL);

    // Display printing in progress
    nEscError = Escape(hPrnDC, STARTDOC, sizeof(szMessage)-1, szMessage, NULL);
    if (nEscError > 0)
	{
	static acWlimit limit={0.0,1.0,0.0,1.0};
//               acDCInfo dci;
//               dci.ws  = hdcPrinter;
//               dci.wnd = hwnd;
	acDCPrtObject dco(dci, limit);
	SetDCS(&dco);
//               Colors[0].x = Colors[0].y = Colors[0].z = 0.0;
	Graph();
//               Colors[0].x = Colors[0].y = Colors[0].z = 1.0;
	nEscError = Escape(hPrnDC, NEWFRAME, 0, NULL, NULL);
	}

    if (nEscError > 0)
	Escape(hPrnDC, ENDDOC, 0, NULL, NULL);

    if (!bUserAbort)
	{ // Dialog box must be removed if this occurs
	EnableWindow(hwnd, TRUE);
	DestroyWindow(hDlgPrint);
	}
    else if (nEscError > 0)
	nEscError = SP_APPABORT;   // For GetErrorText()

    FreeProcInstance(fpDlgPrint);
    FreeProcInstance(fpAbortProc);

    if (nEscError <= 0)
	{
	if (nEscError & SP_NOTREPORTED)
	    {
	    char szMsg[80];
	    wsprintf(szMsg, "Printer Error: %0.60s!", GetErrorText(nEscError));
	    MessageBox(hwnd, szMsg, "View Mesh", MB_OK | MB_ICONEXCLAMATION);
	    }
	else
	    {
	    nEscError |= SP_NOTREPORTED;

	    // The error was displayed, but other dimensions may be needed.
	    }
	}
    }

/*
 *	GetPrinterDC: EN BIBLIOTECA UTIL DEL ACDPOOP
 *
HDC GetPrinterDC(void)
    {
    char  szDevice[64];
    char *pDevice, *pDriver, *pOutput;

    GetProfileString("windows", "device", "", szDevice, 64);

    if ( ((pDevice = strtok(szDevice, "," )) != NULL) &&
	 ((pDriver = strtok(NULL,     ", ")) != NULL) &&
	 ((pOutput = strtok(NULL,     ", ")) != NULL) )
	return (CreateDC(pDriver, pDevice, pOutput, NULL));
    else
	return (NULL);
    }
*/


////////////////////////////////////////////////////////////////
#ifdef BACA
BOOL PASCAL _export PrintAbortMsg(HDC /*hdc*/, int nCode)
    {
    MSG msg;

    while (!bUserAbort && PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
	if (!hDlgPrint || !IsDialogMessage(hDlgPrint, &msg))
	    {
	    TranslateMessage(&msg);
	    DispatchMessage(&msg);
	    }
	}
    return (!(bUserAbort || nCode == SP_OUTOFDISK));
    }

LRESULT CALLBACK PrintAbortDlg(HWND hDlg, UINT wMsg,
			       WPARAM /*wParam*/, LPARAM /*lParam*/)
    {
    switch (wMsg)
	{
    case WM_INITDIALOG:
	SetWindowText(hDlg, "Printer Output");
	EnableMenuItem(GetSystemMenu(hDlg, FALSE), SC_CLOSE, MF_GRAYED);
	break;

    case WM_COMMAND:
	bUserAbort = TRUE;
	EnableWindow(GetParent(hDlg), TRUE);
	DestroyWindow(hDlg);
	hDlgPrint = 0;
	break;

    default:
	return (FALSE);
	}

    return (TRUE);
    }

LPSTR GetErrorText(int nErrorCode)
    {
    static char *szErrText[] =
       {
       "General error",
       "Aborted by user",
       "Aborted by SPOOLER",
       "TMP directory is full",
       "Not enough memory"
       };

    return (szErrText[~nErrorCode]);
    }

#endif