#ifndef ACPOINT3
#define ACPOINT3

#include "vtkSystemIncludes.h"

struct VTK_EXPORT acPoint3
    {
    double x, y, z;
    };
#endif
