
#include "vtkSystemIncludes.h"
#include <stdio.h>

#ifndef SURFTRI
#define SURFTRI

class VTK_EXPORT SurfTriang
{
  public:
      long n1, n2, n3;
      SurfTriang *v1, *v2, *v3;
      SurfTriang *prev, *next;
      int gr,flg;
      int isNew;

  public:
      SurfTriang();
      void SetData(long N1, long N2, long N3, int g,
                   SurfTriang *V1, SurfTriang *V2, SurfTriang *V3);
};

#endif

