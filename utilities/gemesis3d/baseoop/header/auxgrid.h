#include "surface.h"
#include "aclist.h"

#include "vtkSystemIncludes.h"

class VTK_EXPORT AuxGrid
    {
    private:
        acLimit2 Box;   // Bounding box
        long nx, ny;    // Divisions in each axe
        double dx, dy;  // Cell size

        long icurr, imin, imax;
        long jcurr, jmin, jmax;
        acNodeList *currlist;

        acList   *List;  // Vector of lists

    public:
        AuxGrid(Surface *Surf);
        ~AuxGrid ();

        T3Facet *FirstFace(acLimit2 rect);

        T3Facet *NextFace();
    };
