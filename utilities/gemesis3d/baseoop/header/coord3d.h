// 14:20:38  11/23/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um vetor de coordenadas
*/

#ifndef COORD3D_H
#define COORD3D_H

#include "acdp.h"
#include "acdptype.h"

#include "vtkSystemIncludes.h"

class acRotation;
class vtkPoints;
class vtkUnstructuredGrid;

class VTK_EXPORT acCoordinate3D
{
protected:
	long NumNodes;       // number of nodes
  acPoint3 *vc;   // coordenadas x,y,z de cada nodo

public:
  // Constructores
  acCoordinate3D() { NumNodes = 0; vc = NULL;}
  acCoordinate3D(long n);

  // Destructor
 ~acCoordinate3D();

  // operadores
  acPoint3 &operator[](long n)
     {
     return(vc[n]);
     } 
  acCoordinate3D& operator=(const acCoordinate3D &c);
	
	// Alternative Copy Function
	void Copy(acCoordinate3D *c);  

	// I/O
	int  Read(FILE *fp);
  void Print(FILE *fp);
  void Save(char *TabName, int Version, char *FileName);
  void Restore(char *TabName, int Version, char *FileName);
	void DXFOut(FILE *fp);
	void DXFIn (FILE *fp);
	void SaveSPI(FILE *fp);

	// Memory Treatment
  void Free();
	void Allocate(int n);
	
	// Data Access and Info
  long GetNumNodes() { return(NumNodes); }
  void ReadNodes(FILE *fp);  
  void Read(vtkUnstructuredGrid *);  
  void SetNode(int, double*);
  void SetNode(int i, double x, double y, double z) {  	
  	double vec[3] = {x, y, z};
  	this->SetNode(i,vec);  
  }

  void GetNode(int, double*);
  acPoint3* GetVc(){return vc;}
  void CopyCoordinates(vtkPoints *);        	
  
	// Misc Methods
  void Rotate(acCoordinate3D &coor, acRotation *t);
	acLimit3 Box();

protected:
  void ErrMem();
};

#endif // COORD3D_H

