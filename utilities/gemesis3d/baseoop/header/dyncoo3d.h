/*
** dyncoord.h - Enzo, Fancello, Salgado, Venere & Raul
*/
#ifndef DYNCOO3D_H
#define DYNCOO3D_H

#define COORD_GROWTH 500

#include "vtkSystemIncludes.h"
#include "vtkUnstructuredGrid.h"
#include "coord3d.h"

class VTK_EXPORT acDynCoord3D : public acCoordinate3D
    {
    protected:
        long NNosMax;

    public:
        acDynCoord3D () { NNosMax =0; }
        acDynCoord3D (long nod);

        void    Read(FILE *fp);
				void 		Read(vtkUnstructuredGrid *);        
//        void    Restore(FILE *fp);
        void    Free();
        void    AddCoord (acPoint3 &point);
        void    RemoveNodes(long   *aux);
        void    MergeNodes(double cota, long   *aux);
        void    Renumber(long   *aux);
    };

#endif // DYNCOORD_H
