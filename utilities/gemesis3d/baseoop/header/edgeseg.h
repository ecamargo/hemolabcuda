#ifndef EDGESEG_H
#define EDGESEG_H
#include "auxgrid.h"
#include "sparse.h"
#include "vtkSystemIncludes.h"

struct VTK_EXPORT Segment { acPoint2 p[2]; };

class VTK_EXPORT EdgesSegments
    {
    protected:
        long NSeg;
        acList Seg;
        acNodeList *currseg;

    public:
        EdgesSegments(Surface *Surf, int detectflag, acRotation *t);
        ~EdgesSegments() { Seg.Clear(); }

        acPoint2 *FirstSegment()
            {
            currseg = Seg.GetHead();
            return ((acPoint2 *) ((Segment *) currseg->GetDados())->p);
            }

        acPoint2 *NextSegment()
            {
            currseg = currseg->next;
            if (!currseg) return (NULL);
            else
            return ((acPoint2 *) ((Segment *) currseg->GetDados())->p);
            }

    };

struct VTK_EXPORT SegVisNode
    {
    double left, right;
    SegVisNode *next;
    };

class VTK_EXPORT SegVisList
    {
    protected:
        SegVisNode *head;

    public:
        SegVisList()
            {
            head = (SegVisNode *) mMalloc(sizeof(SegVisNode));
            head->left = 0;
            head->right = 1;
            head->next = NULL;
            }
        ~SegVisList();
        int Substract(double xmin, double xmax);
        SegVisNode *GetHead() { return (head); }
    };

#endif
