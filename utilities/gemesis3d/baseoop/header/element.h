// 14:32:49  11/6/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um elemento
*/

#ifndef ELEMENT_H
#define ELEMENT_H

#include "acdp.h"
#include "acdptype.h"
#include "coord3d.h"
#include "vtkSystemIncludes.h"

class VTK_EXPORT Element
    {
    protected:
        long        *Incid;  // incidence
        acPoint3    *Coords; // coordinates

    public:

        // Constructors

        Element() { Incid = NULL; Coords = NULL; }
        virtual ~Element() {};

        long GetNode(int k) { return(Incid[k]); }

        // virtual methods
	virtual char *ElementName(void) = 0;
	virtual char *AdditionalInfo(void) = 0;

	virtual void SetData(long   *inci, acCoordinate3D &c) = 0;
        virtual void SetData(long   *incid) = 0;
        virtual int NNoel()  = 0;   // Number of nodes in the element
        virtual int NEdges() = 0;   // Number of edges in the element
        virtual int NFacet() = 0;   // Number of facets in the element
        virtual int ElemIs3D() = 0; // Used for face visibility

        virtual int NodFac(int facetnum) = 0; // Number of nodes in facet
        virtual void FacInf(int facetnum, long *vn) = 0;

        virtual int  NumTriFac(int facetnum) = 0;
        virtual void TriFacInf(int facetnum,   int numtri,
                              long *tincid, int *tedges) = 0;
        virtual acPoint3 TriFacNormal(int facetnum, int numtri) = 0;

        virtual double   Perimeter() = 0; // length of all edges
        virtual double   Area() = 0;      // area of all facets
        virtual double   Volume() = 0;
        virtual acPoint3 GravCenter() = 0;
        virtual double   Quality() = 0;
        virtual double   EdgeLength2(int edgenum) = 0;
        virtual double   FacetArea(int facetnum) = 0;
        virtual acPoint3 FacetNormal(int facetnum) = 0;
        virtual acPoint3 FacetCenter(int facetnum) = 0;
    };



// funcoes auxiliares
Element *NewElement(char *ElType);     // Return new element
double   AreaTri(acPoint3   *v);    // Evaluate triangle area
acPoint3 NormalTri(acPoint3   *v);  // Evaluate triangle normal
acPoint3 CenterTri(acPoint3   *v);  // Evaluate triangle center

#endif // ELEMENT_H

