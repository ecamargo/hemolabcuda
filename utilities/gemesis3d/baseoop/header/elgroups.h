// 15:33:40  11/26/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define grupos de elementos
*/


#ifndef ELGROUPS_H
#define ELGROUPS_H

#include "coord3d.h"
#include "element.h"
#include "vtkSystemIncludes.h"

class vtkCellArray;
class vtkUnstructuredGrid;

struct VTK_EXPORT InfoGroup
    {
    char ElType[MAX_TAM_NOME];    // element type
    long NumElsGrp;               // number of elements in the group
    long InitialEl;               // number of initial element
    long FinalEl;                 // number of final element
    long *PtrIncid;          			// aux. pointer for group's incidence
    Element *WorkEl;              // pointer for group's element
    };

class VTK_EXPORT ElGroups
    {
    protected:
        int             NumGroups;     // number of groups
        long            NumEls;        // number of elements (total)
        long            NumNodes;      // number of nodes (total)
        InfoGroup 			*VInfGrps;      // vector of InfoGroups
        long      			*Incid;         // incidencia global (corrida)

        // auxiliares para varrer elementos sequencialmente
        int        CurrGr;
        long       CurrElGr;
        Element   *pCurrEl;
        long 			*pCurrIncid;

        // auxiliares para varrer elementos aleatoriamente
        int        PrevGr;
        long       PrevElGr;
        Element   *pPrevEl;
        long 			*pPrevIncid;

public:

  // Constructor
  ElGroups();

  // Destructor
 	~ElGroups();

	// Copy Elgroup
	void Copy(ElGroups *group);

  // Input - Output
  void Read(FILE *fp);
  void Print(FILE *fp);
	void DXFIn (FILE *fp);
	void DXFOut(FILE *fp);
	int  Restore(char *name, int version, char *dba);
	void Save(char *name, int version, char *dba);

	// Parche para lectura de archivos fortran unformatted
	void SetInci2d(long nod, long nel, long las, long *je);
	void SetInci2d(long nod, long nel, long las,
	long *ie, long *je, int is3D);

  // Entrega de informacion local
  int  GetNumGroups() { return(NumGroups); }
  long GetNumEls()    { return(NumEls); }
  long GetNumNodes()  { return(NumNodes); }

  Element *GetElement(int grp)     { return(VInfGrps[grp].WorkEl);}
  char *GetElType(int grp)         { return(VInfGrps[grp].ElType); }
  long GetNumElsGrp(int grp)       { return(VInfGrps[grp].NumElsGrp); }
  long GetInitialEl(int grp)       { return(VInfGrps[grp].InitialEl); }
  long GetFinalEl(int grp)         { return(VInfGrps[grp].FinalEl); }

  // Overload do método Read para ler a partir de uma superfície
  // Passo apenas os parâmetros da superfície para evitar dependência cíclica. 
	void Read(int NumeroGroups, int *NumElsGroups, long *externIncid, char *type);

  // Seta incidência a partir de um grid não estruturado
  void Read(vtkUnstructuredGrid *, char *);     
  
	// Copy Incidence vector from another group
	void CopyIncidenceVector(ElGroups *);
  
  // Pega incidencia de volumes (tetraedros) 	
  void GetVolumeIncidence(long *);  	  

  // Pega um elemento pelo seu índice (somente para malhar triangulares)
	void GetElement(int i, int *vet);
  
  // Seteo de datos del elemento
  //      Acseso secuencial, numeracion global
  Element *SetFirstElem ();
  Element *SetNextElem ();
  Element *SetFirstElem (acCoordinate3D *coor);
  Element *SetNextElem (acCoordinate3D *coor);

  //      Acseso no secuencial, numeracion global
  Element *SetAnElem (long iel);
  Element *SetAnElem (long iel, acCoordinate3D *coor);

  //      Acseso no secuencial, numeracion por grupos
  void SetElData(int grp, long elnum)
    {
    long *pos = VInfGrps[grp].WorkEl->NNoel() * elnum +
                     VInfGrps[grp].PtrIncid;
    VInfGrps[grp].WorkEl->SetData(pos);
    }
    
  void SetElData(int grp, long elnum, acCoordinate3D &coor)
    {
    long *pos = VInfGrps[grp].WorkEl->NNoel() * elnum +
                     VInfGrps[grp].PtrIncid;
    VInfGrps[grp].WorkEl->SetData(pos, coor);
    }
};


#endif // ELGROUPS_H
