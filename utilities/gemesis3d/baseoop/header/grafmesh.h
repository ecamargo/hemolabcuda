/*
** graphmesh.h
** Raul, Salgado, Daniela & Venere & Enzo - Jan 1993
** Grafica Abstracao geometrica de superficies de malhas 3d
*/

#ifndef GRAPHMESH_H
#define GRAPHMESH_H

#include "mesh3d.h"
#include "surface.h"
#include "rotation.h"
//#include "acdpgrph.h"
#include "quality.h"
#include "sparse.h"
#include "edgeseg.h"
#include "aclist.h"
#include "acstack.h"
#include "vtkSystemIncludes.h"

struct VTK_EXPORT numlist
    {
    acPoint2 p;
    long nro;
    int flg;
    };

#define NumMaxColors 128
#define acGRAY 0

class VTK_EXPORT GraphMesh
    {
    protected:
        Mesh3d         *Mesh;
        Quality        *Qlty;
        Surface        *Surf;
        EdgesSegments  *Segm;
        acRotation     *Rot;
		acDeviceContext *Scr;
        SparseNoEl     *SpNoEl;
    	SparseElElV    *SpElElV;
	    int             MustDelElElV;
        SparseEdges    *Edges;

	    acLimit2 Act, Ori;

	    acCobundl Colors[NumMaxColors+1];
	    acStack *ActStack;
	    int   *ElStatus;
        double FacWst;       // Porcentaje de elementos para Worst Elements
        acList Lnum;         // Lista de numeros en pantalla
        acPoint3 vertax[4];
        long elEl;


// Flags
        int SelectFlag, HiddenFlag;
    	int EdgesFlag,  LightFlag;
        int AElemFlag,  ANodeFlag;
        int NodeFlag,   ElemFlag;
        int ClosedFlag, ExpFlag;
        int AxesFlag, WireFlag;
        int WhereFlag;

    public:
        GraphMesh(Mesh3d *mesh, int hiddenflag, int explodedmesh,
		  int closedsurf, double pview_tita, double pview_phi,
		  SparseElElV *elelv = NULL);
        ~GraphMesh();

        long GetNumElems()           { return Mesh->GetNumEls();    }
        long GetNumNodes()           { return Mesh->GetNumNodes();  }
        Element *SetAnElem(long iel) { return(Mesh->SetAnElem(iel));}

	    void SetAxes(acLPoint2 &plci);
	    void SetelEl(long el){ elEl = el;}
	    void WhereIs();

        acPoint3   &GetCoord(long n)
           {
           acCoordinate3D *c = Mesh->GetCoords();
           acCoordinate3D &cr = *c;
           return (cr[n]);
           }

 	void ZoomIn(acLPoint2 &lnw, acLPoint2 &lse)
	    {
	    acPoint2 wse, wnw;

	    if (lse.x == lnw.x || lse.y == lnw.y)
		return;
		wse = Scr->LogToWorld(lse);
		wnw = Scr->LogToWorld(lnw);
	    ZoomIn(wnw, wse);
	    }

	void ZoomIn(acPoint2 &wnw, acPoint2 &wse)
	    {
	    double aux;

	    if (wse.x == wnw.x || wse.y == wnw.y)
		    return;

	    if (wse.x < wnw.x)
		{
		aux   = wse.x;
		wse.x = wnw.x;
		wnw.x = aux;
		}
	    if (wse.y > wnw.y)
		{
		aux   = wse.y;
		wse.y = wnw.y;
		wnw.y = aux;
		}

	    ActStack->Push(&Act);

	    Act.xmin = wnw.x;
	    Act.xmax = wse.x;
	    Act.ymin = wse.y;
	    Act.ymax = wnw.y;
	    }

	void Pan(acLPoint2 &pi, acLPoint2 &pf)
	    {
	    acPoint2 wi, wf;

	    if (pi.x == pf.x && pi.y == pf.y)
		return;
		wi = Scr->LogToWorld(pi);
		wf = Scr->LogToWorld(pf);

	    ActStack->Push(&Act);

	    Act.xmin += wi.x - wf.x;
	    Act.xmax += wi.x - wf.x;
	    Act.ymin += wi.y - wf.y;
	    Act.ymax += wi.y - wf.y;
	    }

       void LCtoWC (acLPoint2 &lp, acPoint2 &p)
	        { 
    		p = Scr->LogToWorld(lp); 
	        }

       void SetDCS(acDeviceContext *scr)
            {
            double dx, dy, d;
            acWlimit aux;

            Scr = scr;
            dx = Act.xmax - Act.xmin;
            dy = Act.ymax - Act.ymin;
            d = (dx > dy) ? dx : dy;
            aux.xmin = Act.xmin - d*0.05;
            aux.ymin = Act.ymin - d*0.05;
            aux.xmax = Act.xmax + d*0.05;
            aux.ymax = Act.ymax + d*0.05;
//            Scr->SetWindowIsotropic(aux);
            Scr->SetWindow(aux);
            }

        void NewEdges()
            {
            Edges = new SparseEdges(Surf, NULL, 1, EdgesFlag, Rot);
            }

        void DelEdges()
            {
            if(Edges)delete Edges;
            Edges = NULL;
            }

        void Graph();

        long SelHalfSpace(int   *Elsw, acPoint3 poin, acPoint3 norm,
                          int fladd, int flint);

        long SelBox      (int   *Elsw, acPoint3 c1, acPoint3 c2,
                          int fladd, int flint);

        long SelSphere   (int   *Elsw, acPoint3 c1, double radius,
                          int fladd, int flint);

        long SelCylinder (int   *Elsw,
                          acPoint3 center, acPoint3 axis, double radius,
                          int fladd, int flint);

        long SelEl(int   *Elsw, long fromel, long toel, long step, int add);

        long SelNod(int   *Elsw, long fromnod, long tonod, long step, int add);

        void CopyElStatus (int   *Elsw);

        void SetElStatus (int   *Elsw);

        void RemoveSelEls();

    	void RemoveEls();
   
    	void UnRemove();

	void FullView()
	    {
	    ActStack->Push(&Act);
	    Act = Ori;
//	    Lnum.Clear();
	    }
	void ZoomPrevious()
	    {
	    if (!ActStack->Pop(&Act))
		Act = Ori;
	    }
	void ZoomOut()
	    {
	    double d;
	    ActStack->Push(&Act);

	    d = (Act.xmax - Act.xmin) * 0.2;
	    Act.xmin -= d;
	    Act.xmax += d;
	    d = (Act.ymax - Act.ymin) * 0.2;
	    Act.ymin -= d;
	    Act.ymax += d;
	    }

	void Zoom(acLPoint2 &lnw, acLPoint2 &lse)
	    {
	    acPoint2 wse, wnw;
	    double aux;

	    if (lse.x == lnw.x || lse.y == lnw.y)
		return;
		wse = Scr->LogToWorld(lse);
		wnw = Scr->LogToWorld(lnw);
	    if (wse.x < wnw.x)
		{
		aux   = wse.x;
		wse.x = wnw.x;
		wnw.x = aux;
		}
	    if (wse.y > wnw.y)
		{
		aux   = wse.y;
		wse.y = wnw.y;
		wnw.y = aux;
		}

	    ActStack->Push(&Act);
	    Act.xmin = wnw.x;
	    Act.xmax = wse.x;
	    Act.ymin = wse.y;
	    Act.ymax = wnw.y;
	    }

	void Rotate(double theta, double phi);

	void Rotate(double theta, double phi, int wireframe, int keepzoom,
		    acPoint3 ZCenter);

	long GWorstElements(int   *ElSw, double fac)
	    {
	    if (!Qlty)
	       Qlty = new Quality(Mesh);
            return(Qlty->GetWorstEl(ElSw, fac));
            }

	void WoMiAv(long  &wstel, double &Mcal, double &Acal);

       void SetFlags(int hiddenflag,
                      int shadingflag,
                      int explodedflag,
                      int closedflag,
                      int edgesflag,
                      int nodesflag,
                      int elementsflag,
                      int axesflag);

        char *Information();
        long NodeNumber(acLPoint2 &p);
//        long ElementNumber(acLPoint2 &p);
        long ElementNumber(acLPoint2 &p, int flgrp = 0);
//	long ElementNumber(acLPoint2 &p, int flgrp);

        void SetAxesFlag(int newv);
        void SetWireFlag(int flag)   { WireFlag = flag; };
        void SetWhereFlag(int flag)   { WhereFlag = flag; };
        void SetSelectFlag(int newv) { SelectFlag = newv; }
        int  GetSelectFlag()         { return(SelectFlag); }
        void SetElemFlag  (int newv) { ElemFlag   = newv; }
        int  GetElemFlag  ()         { return(ElemFlag); }
        void SetNodeFlag  (int newv) { NodeFlag   = newv; }
        int  GetNodeFlag  ()         { return(NodeFlag); }
        acLimit2 GetWindow()         { return (Act); }
     	void GraphAxes();

    };
#endif // GRAPHMESH_H
