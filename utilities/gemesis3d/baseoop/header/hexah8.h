// 14:32:49  11/6/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um elemento Hexah8
*/

#ifndef HEXAH8_H
#define HEXAH8_H

#include "vtkSystemIncludes.h"
#include "element.h"

class VTK_EXPORT Hexah8 : public Element
    {
    protected:
        static int maux[6][2][3];

    public:
        // Constructors
        Hexah8();
       ~Hexah8();

        // virtual methods
	char *ElementName(void);
	char *AdditionalInfo(void);

        void    SetData(long   *incid, acCoordinate3D &coords);
        void    SetData(long   *incid);

        int     NNoel()  { return(8);}   // Number of nodes in the element
        int     NEdges() { return(12);}  // Number of edges in the element
        int     NFacet() { return(6);}   // Number of facets in the element
        int     ElemIs3D(){ return(1);}  // Used for face visibility

        int     NodFac(int facetnum) { return(4); }
        void    FacInf(int facetnum, long *vn);

        int      NumTriFac(int facetnum) { return(2); }
        void     TriFacInf(int facetnum, int numtri, long *tincid, int *tedges);
        acPoint3 TriFacNormal(int facetnum, int numtri);

        double   Perimeter();
        double   Area();
        double   Volume();
        double   Quality();
        double   EdgeLength2(int edgenum);
        double   FacetArea(int facetnum);
        acPoint3 GravCenter();
        acPoint3 FacetNormal(int facetnum);
        acPoint3 FacetCenter(int facetnum);

    };

#endif // HEXAH8_H
