// 15:33:40  11/26/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define grupos de elementos
*/

#ifndef MESH3D_H
#define MESH3D_H

#include "acdp.h"
#include "coord3d.h"
#include "elgroups.h"
#include "sparse.h"
#include "vtkSystemIncludes.h"

class VTK_EXPORT Mesh3d
    {
    protected:
	acCoordinate3D *Coords;
	ElGroups       *ElGrps;

    public:
	// Constructor
	Mesh3d()
	    {
	    Coords = NULL;
	    ElGrps = NULL;
	    }

	// Destructor
       ~Mesh3d()
	   {
	   if (Coords) delete Coords;
	   if (ElGrps) delete ElGrps;
	   };

	// Interface
	void Read(FILE *fp);
	void Print(FILE *fp);
	void DXFIn (FILE *fp);
	void DXFOut(FILE *fp);
	void SaveSPI(FILE *fp);
	void Restore(char *name, int version, char *dba);
	void Save(char *name, int version, char *dba);

	acCoordinate3D *GetCoords() { return(Coords);}
	int  GetNumGroups()         { return(ElGrps->GetNumGroups()); }
	long GetNumEls()            { return(ElGrps->GetNumEls()); }
	long GetNumNodes()          { return(Coords->GetNumNodes()); }

	long GetNumElsGrp(int grp)  { return(ElGrps->GetNumElsGrp(grp));}
	char *GetElType(int grp)    { return(ElGrps->GetElType(grp));}

	// Funcoes para loop sobre elementos da malha
	Element *SetFirstElem()      {return(ElGrps->SetFirstElem(Coords));}
	Element *SetNextElem()       {return(ElGrps->SetNextElem(Coords));}
	Element *SetAnElem(long iel) {return(ElGrps->SetAnElem(iel,Coords));}

	Element *GetElement(int grp){return(ElGrps->GetElement(grp));}
	void SetElData(int grp, long elnum)
	    {
	    ElGrps->SetElData(grp, elnum, *Coords);
	    }

	SparseElElV *NewElElV() { return(new SparseElElV(ElGrps)); }
	SparseFaFaV *NewFaFaV() { return(new SparseFaFaV(ElGrps)); }
	SparseElNo  *NewElNo()  { return(new SparseElNo(ElGrps)); }
	SparseNoEl  *NewNoEl()  { return(new SparseNoEl(ElGrps)); }
	acLimit3  Limit() {return(Coords->Box());}
	double    Volume();
	double    Volume(int   *ElStatus);
	acPoint3  GravityCenter();
	acPoint3  GravityCenter(int   *ElStatus);
    };

#endif // MESH3D_H
