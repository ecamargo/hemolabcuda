#ifndef NODLIST
#define NODLIST

#include "aclist.h"
#include "vtkSystemIncludes.h"

class VTK_EXPORT nodlist
{
  struct nod 
      {
      long n;
      struct nod *next;
      }*first;
  public:
      nodlist() {first=0;}
      ~nodlist();
      int add(long n);
};

class VTK_EXPORT st_nodeclist
{
	public:
	long numelem;
};

class VTK_EXPORT MP_Octree
{
	public:
  acPoint3 cg;                // coordenadas do centro do no' octree
  acPoint3 tc;                // tamanho do no' do octree
  MP_Octree   *ptr[8];    // ptrs. para os filhos
  acList l;
};

#endif

