// 14:32:49  11/6/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um elemento Quad4
*/

#ifndef QUAD4_H
#define QUAD4_H

#include "vtkSystemIncludes.h"
#include "element.h"

class VTK_EXPORT Quad4  : public Element
    {
    protected:
        static int maux[2][3];

    public:
        // Constructors
        Quad4();
       ~Quad4();


        // virtual methods
	char *ElementName(void);
	char *AdditionalInfo(void);

        void    SetData(long   *incid, acCoordinate3D &c);

        void    SetData(long   *incid);

        int     NNoel()  { return(4);}  // Number of nodes in the element
        int     NEdges() { return(4);}  // Number of edges in the element
        int     NFacet() { return(1);}  // Number of facets in the element
        int     ElemIs3D(){ return(0);}  // Used for face visibility

        int     NodFac(int facetnum) { return (4); }
        void    FacInf(int facetnum, long *vn);
        
        int      NumTriFac(int facetnum) { return(2); }
        void     TriFacInf(int facetnum, int numtri, long *tincid, int *tedges);
        acPoint3 TriFacNormal(int facetnum, int numtri);
        
        double   Perimeter();
        double   Area()   { return(FacetArea(0)); }
        double   Volume() { return (0.0); }
        double   Quality();
        double   EdgeLength2(int edgenum);
        double   FacetArea(int facetnum);
        acPoint3 GravCenter() { return(FacetCenter(0)); }
        acPoint3 FacetNormal(int facetnum);
        acPoint3 FacetCenter(int facetnum);

    };

#endif // QUAD4_H

