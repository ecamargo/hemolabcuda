// 15:33:40  11/26/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Contiene las calidades de los elementos
*/

#ifndef QUALITY_H
#define QUALITY_H

#include "vtkSystemIncludes.h"
#include "mesh3d.h"

struct VTK_EXPORT sqlty
    {
    long  ElNum;
    double Qlty;
    };

typedef VTK_EXPORT struct sqlty *psqlty;

class VTK_EXPORT Quality
    {
    protected:
        long NumElems;
        double MinQlty, MaxQlty, AverageQlty;
        psqlty   *Qlty;

    public:
        Quality()
            {
            NumElems = 0;
            MinQlty  = MaxQlty = AverageQlty = 0;
            Qlty     = NULL;
            }

        Quality(Mesh3d *mesh);
        long GetNumElems() { return(NumElems); }
        void GetQualities(double &minq, double &maxq, double &aveq)
            {
            minq = MinQlty;
            maxq = MaxQlty;
            aveq = AverageQlty;
            }

        long GetWorstEl(int   *elstatus, double percent);
        long GetWorstest() {return(Qlty[0]->ElNum);}
        void Print(FILE *);
       ~Quality();

    private:
        void Qsort();
    };

#endif // QUALITY_H
