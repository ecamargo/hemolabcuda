// 18:10:40  12/4/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define un objeto rotacion
*/

#ifndef ROTATION_H
#define ROTATION_H

#include "vtkSystemIncludes.h"
#include "acdptype.h"

class VTK_EXPORT acRotation
    {
    protected:
        double Mat[3][3];

    public:

        acRotation()
            {
            SetDiag();
            }

        acRotation(double a00, double a01, double a02,
                 double a10, double a11, double a12,
                 double a20, double a21, double a22)
            {
            Mat[0][0] = a00;
            Mat[0][1] = a01;
            Mat[0][2] = a02;
            Mat[1][0] = a10;
            Mat[1][1] = a11;
            Mat[1][2] = a12;
            Mat[2][0] = a20;
            Mat[2][1] = a21;
            Mat[2][2] = a22;
            }

        acRotation(double theta, double phi) { SetMat(theta, phi); }

        acRotation(double rx, double ry, double rz)
            {
            SetDiag();
            SetMat(rx, ry, rz);
            }

        void SetDiag()
            {
            Mat[0][0] = Mat[1][1] = Mat[2][2] = 1;
            Mat[0][1] = Mat[0][2] = Mat[1][0] = 0;
            Mat[1][2] = Mat[2][0] = Mat[2][1] = 0;
            }

        void PreMult(acRotation *r, acRotation *a);

        acPoint3 GetXYZ(acPoint3 &p)
            {
            acPoint3 r;
            r.x = GetTPoint(0, p);
            r.y = GetTPoint(1, p);
            r.z = GetTPoint(2, p);
            return(r);
            }

        void GetXY(double &xr, double &yr, acPoint3 &p)
            {
            xr = GetTPoint(0, p);
            yr = GetTPoint(1, p);
            }

        acPoint2 GetXY(acPoint3 &p)
            {
            acPoint2 p2;
            p2.x = GetTPoint(0, p);
            p2.y = GetTPoint(1, p);
            return(p2);
            }

        double GetZ(acPoint3 &p)
            {
            return(GetTPoint(2, p));
            }

        void SetMat(double *v);
        void SetMat(double theta, double phi);
        void SetMat(double rx, double ry, double rz);
        void SetMat(double a00, double a01, double a02,
                    double a10, double a11, double a12,
                    double a20, double a21, double a22)
            {
            Mat[0][0] = a00;
            Mat[0][1] = a01;
            Mat[0][2] = a02;
            Mat[1][0] = a10;
            Mat[1][1] = a11;
            Mat[1][2] = a12;
            Mat[2][0] = a20;
            Mat[2][1] = a21;
            Mat[2][2] = a22;
            }

    private:
        double GetTPoint(int i, acPoint3 &p)
            {
            return(Mat[i][0] * p.x + Mat[i][1] * p.y + Mat[i][2] * p.z);
            }

        void MultM3(double a[3][3], double b[3][3]);
    };

#endif // ROTATION_H

