// 15:33:40  11/26/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define grupos de elementos
*/

#ifndef SPARSE_H
#define SPARSE_H

#include "vtkSystemIncludes.h"
#include "elgroups.h"
class Surface;

struct VTK_EXPORT SparseInfo
    {
    long n;
    long   *Elems;
    };

class VTK_EXPORT StructSparse
    {
    protected:
        SparseInfo   *SI;
        long TIndex;

    public:
        StructSparse()
            {
            SI     = NULL;
            TIndex = 0;
            }
       ~StructSparse();
        long GetTIndex()               { return(TIndex); }
        long GetNelems(long index)     { return(SI[index].n); }
        long GetElem(long ii, long ij) { return(SI[ii].Elems[ij]); }
	};

class VTK_EXPORT SparseElNo : public StructSparse
    {
    public:
        SparseElNo() {}
        SparseElNo(ElGroups *);
    };

class VTK_EXPORT SparseNoEl : public StructSparse
    {
    public:
        SparseNoEl() {}
        SparseNoEl(ElGroups *);
    };

class VTK_EXPORT SparseElElV : public StructSparse
    {
    public:
        SparseElElV() {}
        SparseElElV(ElGroups *, SparseNoEl * e=NULL);
    };

class VTK_EXPORT SparseElElVT : public StructSparse
    {
    public:
	SparseElElVT() {}
        SparseElElVT(ElGroups *, SparseNoEl * e=NULL);
    };

class VTK_EXPORT SparseNoFa : public StructSparse
    {
    public:
        SparseNoFa() {}
        SparseNoFa(Surface *Surf);
    };

class VTK_EXPORT SparseFaFaV : public StructSparse
    {
    public:
	SparseFaFaV() {}
	SparseFaFaV(ElGroups *, SparseNoEl * e=NULL);
    };

class VTK_EXPORT SparseEdges : public StructSparse
    {
    public:
        SparseEdges() {}
        SparseEdges(Surface *Surf, SparseNoFa * e=NULL, int allf=0,
                    int DetectFlag = 0, acRotation *t=NULL);
    };

#endif // SPARSE_H
