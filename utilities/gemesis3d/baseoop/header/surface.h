// 18:10:40  11/01/1993
// Raul & Salgado & Marcelo & Enzo

/*
** Define superficies
*/

#ifndef SURFACE_H
#define SURFACE_H

#include "vtkSystemIncludes.h"
#include "mesh3d.h"
#include "rotation.h"
#include "acqtree.h"

class VTK_EXPORT T3Facet
    {
    protected:
	double   Height;               // key to sort
	int     Visible;               // 0 if normal.visual<0
	int     Grp;                   // group number
	long    Elem;                  // elem. number
	long    Incid[3];              // incid
	int     Edges[3];              // edges flag
	acPoint3  Center;              // center of the element facet
	acPoint3  Normal;              // facet normal

    public:
	T3Facet(){};

	T3Facet(acRotation *t, int grp, long elem, long *incid,
		int *edges, acPoint3 center, acPoint3 normal,
		int elemis3d)
	    {
	    Height   = t->GetZ(center);
	    Grp      = grp;
	    Elem     = elem;
	    Incid[0] = incid[0]; Incid[1] = incid[1]; Incid[2] = incid[2];
	    Edges[0] = edges[0]; Edges[1] = edges[1]; Edges[2] = edges[2];
	    Center   = center;
	    Normal   = normal;
	    Visible  = (t->GetZ(Normal) < 0 || !elemis3d);
	    }

	// for sort
	void   SetKey(acRotation *t)     { Height = t->GetZ(Center);}
	double GetKey()                  { return(Height); }
	void   SetVisible(acRotation *t) { Visible = (t->GetZ(Normal) < 0); }
	void   SetVisible()              { Visible = 1; }
	int    GetVisible()              { return(Visible); }

	// interface
	int  GetGrp()  { return(Grp); }
	long GetElem() { return(Elem); }

	void GetIncid(long *incid)
	    {
	    incid[0] = Incid[0];
	    incid[1] = Incid[1];
	    incid[2] = Incid[2];
	    }

	double GetColInt(acPoint3 ldir)
	    {
	    double col;
	    col = Normal.x*ldir.x + Normal.y*ldir.y + Normal.z*ldir.z;
	    if (col < 0.0)
		col = -col;
	    return (col);
	    }

	int EdgeOn(int edge) { return(Edges[edge]); }
	void SetEdge(int edge, int newval) { Edges[edge] = newval; }

	acPoint2  GetXYCenter(acRotation *t) { return(t->GetXY(Center)); }

	acPoint3  GetCenter()   { return(Center); }  //para PHIGS
	acPoint3  GetNormal()   { return(Normal); }
    };


typedef T3Facet *PT3Facet;

/*	S	U	R	F	A	C	E	*/
class VTK_EXPORT Surface
    {
    protected:
	long           NFacets;        // number of T3Facet
	PT3Facet         *T3F;      // vector of pointers to T3Facet
	acCoordinate3D *CoordOri;      // original coordinates
	acCoordinate3D *CoordRot;      // rotated coordinates
	double         Fac;            // Factor de expansion (0 = no expanded)
	int            ElemIs3D;       // Indica si la sup es cerrada

	acQTree *QTree;

	acPoint3 ExpCoor(acPoint3 &c1, acPoint3 &c2)
	   {
	   acPoint3 c;
	   c.x = c2.x + Fac*(c1.x - c2.x);
	   c.y = c2.y + Fac*(c1.y - c2.y);
	   c.z = c2.z + Fac*(c1.z - c2.z);
	   return(c);
	   }

    public:

	// Construtor sem parametros
	Surface()
	    {
	    NFacets  = 0;
	    T3F      = NULL;
	    CoordOri = NULL;
	    CoordRot = NULL;
	    ElemIs3D = 0;
	    QTree = NULL;
	    }

	// Construtor com parametros
	// if expanded = 1 -> expanded mesh
	Surface(acRotation *t, Mesh3d *mesh, int   *elstatus,
	    int closed, int exploded, SparseElElV *elelv = NULL);

	// destructor
       ~Surface();

	// Pedidos de informacion
	long GetNumNodes() { return CoordOri->GetNumNodes(); }
	acCoordinate3D * GetCoordRot() { return CoordRot; }
	long GetNFacets()  { return(NFacets); }
	T3Facet * PT3F(long fac) { return (T3F[fac]); }
	void GetInciFace(long fac, long *incid)
	    { T3F[fac]->GetIncid(incid); }

	void GetInfoFace(long fac, long *incid, acPoint2 *coor)
	    {
	    acCoordinate3D &c = *CoordRot;
	    T3F[fac]->GetIncid(incid);
	    coor[0].x = c[incid[0]].x;
	    coor[0].y = c[incid[0]].y;
	    coor[1].x = c[incid[1]].x;
	    coor[1].y = c[incid[1]].y;
	    coor[2].x = c[incid[2]].x;
	    coor[2].y = c[incid[2]].y;
	    }

	int EdgeOn(long fac, int edge) { return(T3F[fac]->EdgeOn(edge)); }
	void SetEdge(long fac, int edge, int newval)
		{ T3F[fac]->SetEdge(edge, newval); }
	int FaceOn(long fac)           { return(T3F[fac]->GetVisible());}

	int GetGrp(long fac)           { return(T3F[fac]->GetGrp()); }

	acLimit2 GetXYLimit()
	    {
	    acLimit2 area;
	    acLimit3 vol;
	    vol = CoordRot->Box();
	    area.xmin = vol.xmin;
	    area.xmax = vol.xmax;
	    area.ymin = vol.ymin;
	    area.ymax = vol.ymax;
	    return(area);
	    }

	acPoint3 GetNormal(long fac)
                { return(T3F[fac]->GetNormal()); }
                
	double GetColInt(long fac, acRotation *t);

	acPoint2 GetXYCenter(long fac, acRotation *t)
	    { 
		return(T3F[fac]->GetXYCenter(t)); 
	    }

	long GetElem(long fac)         { return(T3F[fac]->GetElem()); }

	void Rotate(acRotation *t, int sort = 1)
	    {
	    CoordRot->Rotate(*CoordOri, t);
	    if (sort)Qsort(t);
	    if (QTree)
		delete QTree;
	    QTree = NULL;
	    }

	// Otros
	void BuildQuadtree();	// Arma estructura de datos p/busqueda

	long FindFace(acPoint2 &p);

	void SetElemIs3D(int flag, acRotation *t);

    private:
	void Qsort(acRotation *t);
    };

#endif // SURFACE_H
