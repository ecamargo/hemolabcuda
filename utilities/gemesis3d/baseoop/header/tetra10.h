// 14:32:49  11/6/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um elemento Tetra10
*/

#ifndef TETRA10_H
#define TETRA10_H

#include "vtkSystemIncludes.h"
#include "element.h"

class VTK_EXPORT Tetra10 : public Element
    {
    protected:
        static int maux[4][4][3];

    public:
        // Constructors
        Tetra10();
       ~Tetra10();

        // virtual methods
	char *ElementName(void);
	char *AdditionalInfo(void);

        void    SetData(long   *incid, acCoordinate3D &coords);
        void    SetData(long   *incid);

        int     NNoel()  { return(10);}  // Number of nodes in the element
        int     NEdges() { return(12);}  // Number of edges in the element
        int     NFacet() { return(4);}   // Number of facets in the element
        int     ElemIs3D(){ return(1);}  // Used for face visibility

        int     NodFac(int facetnum) { return(6); }
        void    FacInf(int facetnum, long *vn);
        
        int      NumTriFac(int facetnum) { return(4); }
        void     TriFacInf(int facetnum, int numtri, long *tincid, int *tedges);
        acPoint3 TriFacNormal(int facetnum, int numtri);
        
        double   Perimeter();
        double   Area();
        double   Volume();
        double   Quality();
        double   EdgeLength2(int edgenum);
        double   FacetArea(int facetnum);
        acPoint3 GravCenter();
        acPoint3 FacetNormal(int facetnum) { return(TriFacNormal(facetnum,0)); }
        acPoint3 FacetCenter(int facetnum);

    };

#endif // TETRA4_H
