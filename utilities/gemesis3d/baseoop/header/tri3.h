// 14:32:49  11/6/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define um elemento Tri3
*/

#ifndef TRI3_H
#define TRI3_H

#include "vtkSystemIncludes.h"
#include "element.h"

class VTK_EXPORT Tri3 : public Element
    {
    public:
        // Constructors
        Tri3();
       ~Tri3();

        // virtual methods
	char *ElementName(void);
	char *AdditionalInfo(void);

        void    SetData(long   *incid, acCoordinate3D &coords);
        void    SetData(long   *incid);

        int     NNoel() { return(3);}   // Number of nodes in the element
        int     NEdges() { return(3);}  // Number of edges in the element
        int     NFacet() { return(1);}  // Number of facets in the element
        int     ElemIs3D(){ return(0);}  // Used for face visibility

        int     NodFac(int facetnum) { return(3); }
        void    FacInf(int facetnum, long *vn)
                {
                vn[0] = Incid[0];
                vn[1] = Incid[1];
                vn[2] = Incid[2];
                }
        
        int     NumTriFac(int facetnum) { return(1); }
        void    TriFacInf(int facetnum, int numtri, long *tincid, int *tedges)
                {
                FacInf(facetnum, tincid);
                tedges[0] = tedges[1] = tedges[2] = 1;
                }
        acPoint3 TriFacNormal(int facetnum, int numtri) { return(NormalTri(Coords)); }

        double   Perimeter();
        double   Area()   { return(FacetArea(0)); }
        double   Volume() { return (0.0); }
        double   Quality();
        double   EdgeLength2(int edgenum);
        double   FacetArea(int facetnum) {return(AreaTri(Coords));}
        acPoint3 GravCenter() { return(FacetCenter(0)); }
        acPoint3 FacetNormal(int facetnum) { return(NormalTri(Coords)); }
        acPoint3 FacetCenter(int facetnum) { return(CenterTri(Coords)); }

    };

#endif // TRI3_H

