/*
** vscoo3d.h - Enzo et al., 8/95
*/
#ifndef VSCOO3D_H
#define VSCOO3D_H

#include <stdio.h>
//#include "acdpdefs.h"  // definicoes
//#include "grphtype.h"
#include "acdp.h"

#include "vtkSystemIncludes.h"

#define VSC3D_ELEMS_PER_VECTOR 1024
#define VSC3D_GROWTH 500
#define VSC3D_SHRINK 1000

struct VTK_EXPORT acPoint3l
    {
    acPoint3 c;
    long l;
    };

class VTK_EXPORT acRotation;

class VTK_EXPORT acVSCoord3D
{
  protected:
	long NNodes;
	long NVectors;
	long FirstFree;
	acPoint3l **VCoords;

	acPoint3l *GetP(long i)
	    { return (VCoords[i/VSC3D_ELEMS_PER_VECTOR]+
			      i%VSC3D_ELEMS_PER_VECTOR); }

	long SizePerm;
	long   *Perm;

    public:
	// Constructores
	acVSCoord3D () { NNodes = NVectors = 0; VCoords = NULL; }
	acVSCoord3D (long nod);
	// Destructor
	~acVSCoord3D () { Free(); }
	void Free();

	// Information
	long GetNumNodes() { return(NNodes); }
	acPoint3 &operator[](long n)
	   { return(GetP(n)->c); }
	void GetCoord(int index, double *coord);

	// I/O
	void Read(FILE *fp);
	void Print(FILE *fp);
	
	void Save(char *TabName, int Version, char *FileName);
	void Restore(char *TabName, int Version, char *FileName);

	void DXFOut(FILE *fp);
	void DXFIn (FILE *fp);

	// Operations
	acLimit3 Box();
	void Rotate(acVSCoord3D &coor, acRotation *t);

	// Variable size options
	void FreePerm();
	long AddCoord (acPoint3 &point);
	void DelCoord (long index);
	int  CompactCoord(void);
	long NewIndex (long oldindex);

	// Memory Error
	void ErrMem()
	    { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
    };

#endif // VSCOO3D_H

