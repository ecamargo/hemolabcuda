#include "auxgrid.h"
#include <math.h>

AuxGrid::AuxGrid (Surface *Surf)
    {
    double f;
    long i,j,i0,j0,i1,j1,i2,j2,face;
    long inci[3];
    acPoint2 coor[3];

    static char *nome="AuxGrid::AuxGrid";
    TraceOn(nome);

    Box = Surf->GetXYLimit();
    dx = Box.xmax - Box.xmin;
    Box.xmin -= dx / 50;
    Box.xmax += dx / 50;
    dy = Box.ymax - Box.ymin;
    Box.ymin -= dy / 50;
    Box.ymax += dy / 50;

    f = (Box.ymax - Box.ymin)/(Box.xmax - Box.xmin);
    nx = (long int) sqrt(Surf->GetNFacets() / (3*f));
    nx = nx < 1 ? 1 : nx;
    ny = (long int) f * nx;
    ny = ny < 1 ? 1 : ny;
    dx = (Box.xmax - Box.xmin) / nx;
    dy = (Box.ymax - Box.ymin) / ny;

    List = (acList   *) mMalloc (nx*ny * sizeof (acList));
    if (!List)
        { //    1, "Can't Allocate Memory."
        char buf[30];
        GetError(1, buf);
        Error(FATAL_ERROR, 2, buf);
        }

    for (i = 0; i<nx*ny; i++)
        List[i].SetListData(sizeof(T3Facet *));

    for (face=0; face<Surf->GetNFacets(); face++)
        {
        if (!Surf->FaceOn(face)) continue;
        T3Facet *pface = Surf->PT3F(face);
        Surf->GetInfoFace(face, inci, coor);
        i0 = long((coor[0].x-Box.xmin) / dx);
        j0 = long((coor[0].y-Box.ymin) / dy);
        i1 = long((coor[1].x-Box.xmin) / dx);
        j1 = long((coor[1].y-Box.ymin) / dy);
        i2 = long((coor[2].x-Box.xmin) / dx);
        j2 = long((coor[2].y-Box.ymin) / dy);
        if (i0 < i1)
            { imin = i0; imax = i1; }
        else
            { imin = i1; imax = i0; }
        if      (i2 < imin) imin = i2;
        else if (i2 > imax) imax = i2;
        if (j0 < j1)
            { jmin = j0; jmax = j1; }
        else
            { jmin = j1; jmax = j0; }
        if      (j2 < jmin) jmin = j2;
        else if (j2 > jmax) jmax = j2;

        for (i = imin; i<=imax; i++)
            for (j = jmin; j<=jmax; j++)
            {
        	List[j*nx+i].InsertFirst(&pface);
            }
        }

    imin = imax = 0; icurr = 1;
    jmin = jmax = 0; jcurr = 1;
    currlist = NULL;

    TraceOff(nome);
    }

AuxGrid::~AuxGrid()
    {
    for (long i = 0; i<nx*ny; i++)
        List[i].Clear();
    mFree((char *)List);
    }

T3Facet *AuxGrid::FirstFace(acLimit2 rect)
    {
    PT3Facet *nolist;
    imin = long((rect.xmin-Box.xmin) / dx);
    jmin = long((rect.ymin-Box.ymin) / dy);
    imax = long((rect.xmax-Box.xmin) / dx);
    jmax = long((rect.ymax-Box.ymin) / dy);
    for (icurr = imin; icurr<=imax; icurr++)
        for (jcurr = jmin; jcurr<=jmax; jcurr++)
	        {
	        currlist = List[jcurr*nx+icurr].GetHead();
	        if (currlist)
                {
                nolist = (PT3Facet *)currlist->GetDados();
                return (*nolist);
                }
	        }
    return (NULL);
    }

T3Facet *AuxGrid::NextFace()
    {
    PT3Facet *nolist;
    if (!currlist) return (NULL);
    currlist = currlist->next;
    if (currlist)
        {
        nolist = (PT3Facet *)currlist->GetDados();
        return (*nolist);
        }
    jcurr++;

    for (; icurr<=imax; icurr++, jcurr=jmin)
        for (; jcurr<=jmax; jcurr++)
            {
            currlist = List[jcurr*nx+icurr].GetHead();
            if (currlist)
                {
                nolist = (PT3Facet *)currlist->GetDados();
                return (*nolist);
                }
            }
    return (NULL);
    }
