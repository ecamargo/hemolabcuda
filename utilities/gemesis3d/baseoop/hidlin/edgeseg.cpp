#include "edgeseg.h"
#include <math.h>

EdgesSegments::EdgesSegments(Surface *Surf, int detectflag, acRotation *t)
    {
    static char *nome = "EdgesSegments::EdgesSegments";
    TraceOn(nome);

    acCoordinate3D *c = Surf->GetCoordRot();
    acCoordinate3D &coor = *c;
    AuxGrid grid (Surf);
    SparseEdges *edges;
    if (detectflag == 1) // edges = detect
       edges = new SparseEdges(Surf, NULL, 0, detectflag, t);
    else                 // edges = all
       edges = new SparseEdges(Surf);

    Seg.SetListData(sizeof(Segment));

    Segment Saux;
    acPoint3 pa, pb, p1, p2, p3;
    acLimit2 rect;
    long na, nb, i;
    T3Facet *pf;
    long inci[3];
    int sw1, sw2, sw3;
    double aux, a1,a2,a3, r1,r2,r3, d, xmu;
    double xla,xla1,xla2,xla3, dz,dz1,dz2;

    for (na = 0; na < edges->GetTIndex(); na++)
        {
        for (i = 0; i < edges->GetNelems(na); i++)
            {
            nb = edges->GetElem(na, i);
            pa = coor[na]; pb = coor[nb];

            if (pa.x < pb.x) { rect.xmin = pa.x; rect.xmax = pb.x; }
            else             { rect.xmin = pb.x; rect.xmax = pa.x; }
            if (pa.y < pb.y) { rect.ymin = pa.y; rect.ymax = pb.y; }
            else             { rect.ymin = pb.y; rect.ymax = pa.y; }

            a1 = pa.y - pb.y;
            a2 = pb.x - pa.x;
            a3 = pa.x * pb.y - pb.x * pa.y;

            int edgevis = 1;
            SegVisList lvis;

            for (pf = grid.FirstFace(rect); pf && edgevis; pf = grid.NextFace())
                {
                pf->GetIncid(inci);
                p1 = coor[inci[0]];
                p2 = coor[inci[1]];
                p3 = coor[inci[2]];

                r1 = a1*p1.x + a2*p1.y + a3;
                sw1 = r1 < 0;
                r2 = a1*p2.x + a2*p2.y + a3;
                sw2 = r2 < 0;
                r3 = a1*p3.x + a2*p3.y + a3;
                sw3 = r3 < 0;

                int hubocorte = 0;
                int corteimpar = 0;
                if (sw1 != sw2) // Corte edge<->lado p1-p2
                    {
                    d = r1 - r2;
                    if (fabs(d) > 0.0000000001)
                        {
                        xla = ((pa.x-p1.x)*(p2.y-p1.y) -
                               (pa.y-p1.y)*(p2.x-p1.x)   ) / d;
                        xmu = r1 / d;
                        dz = ((1.0 - xla) * pa.z + xla * pb.z) -
                             ((1.0 - xmu) * p1.z + xmu * p2.z);
                        xla1 = xla;
                        dz1 = dz;
                        corteimpar = 1;
                        }
                    }

                if (sw2 != sw3) // Corte edge<->lado p2-p3
                    {
                    d = r2 - r3;
                    if (fabs(d) > 0.0000000001)
                        {
                        xla = ((pa.x-p2.x)*(p3.y-p2.y) -
                               (pa.y-p2.y)*(p3.x-p2.x)   ) / d;
                        xmu = r2 / d;
                        dz = ((1.0 - xla) * pa.z + xla * pb.z) -
                             ((1.0 - xmu) * p2.z + xmu * p3.z);
                        if (!corteimpar)
                            {
                            xla1 = xla;
                            dz1 = dz;
                            corteimpar = 1;
                            }
                        else
                            {
                            xla2 = xla;
                            dz2  = dz;
                            corteimpar = 0;
                            hubocorte = 1;
                            }
                        }
                    }

                if (sw3 != sw1 && corteimpar) // Corte edge<->lado p3-p1
                    {
                    d = r3 - r1;
                    if (fabs(d) > 0.0000000001)
                        {
                        xla = ((pa.x-p3.x)*(p1.y-p3.y) -
                               (pa.y-p3.y)*(p1.x-p3.x)   ) / d;
                        xmu = r3 / d;
                        dz = ((1.0 - xla) * pa.z + xla * pb.z) -
                             ((1.0 - xmu) * p3.z + xmu * p1.z);
                        xla2 = xla;
                        dz2  = dz;
                        hubocorte = 1;
                        }
                    }

                if (hubocorte)
                    {
                    if (xla1 > xla2)
                        {
                        aux = xla1; xla1 = xla2; xla2 = aux;
                        aux = dz1;  dz1 = dz2;   dz2 = aux;
                        }
                    if (dz1 < -0.000001 || dz2 < -0.000001)
                        {
                        if (dz1 > 0 || dz2 > 0)
                            {
                            xla3 = (xla2 * dz1 - xla1 * dz2) / (dz1-dz2);
                            if (dz1 < 0) xla2 = xla3;
                            else         xla1 = xla3;
                            }

                        edgevis = lvis.Substract(xla1, xla2);
                        }
                    }
                } // Next face
            SegVisNode *node = lvis.GetHead();
            while (node)
                {
                Saux.p[0].x = pa.x*(1-node->left)  + pb.x*node->left;
                Saux.p[0].y = pa.y*(1-node->left)  + pb.y*node->left;
                Saux.p[1].x = pa.x*(1-node->right) + pb.x*node->right;
                Saux.p[1].y = pa.y*(1-node->right) + pb.y*node->right;
                Seg.InsertFirst(&Saux);
                node = node->next;
                }
            }
        }
    delete edges;
    TraceOff(nome);
    }
