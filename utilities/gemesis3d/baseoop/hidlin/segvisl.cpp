#include "edgeseg.h"

SegVisList::~SegVisList()
    {
    SegVisNode *curr;
    while (head)
        {
        curr = head;
        head = head->next;
        mFree (curr);
        }
    }

int SegVisList::Substract(double xmin, double xmax)
    {
    SegVisNode *curr, *prev, *aux;
    curr = head;
    prev = NULL;
    char *nome = "SegVisList::Substract";

    TraceOn(nome);

    xmin -= 0.0001;
    xmax += 0.0001;

    while (curr)
	{
	if (xmax < curr->left)
	    curr = NULL;
	else if (xmin < curr->right) // algo que ver
	    {
	    if (xmin < curr->left) // extremo izquierdo se modifica
		{
		if (xmax < curr->right) // segmento disminuye tamanho
				   //       left          right
				   //  xmin         xmax
		    {
		    curr->left = xmax;
		    curr = NULL;
		    }
		else	// segmento desaparece
				   //      left    right
				   //  xmin              xmax
		    {
		    if (!prev)
			{
			head = curr->next;
			mFree (curr);
			curr = head;
			}
		    else
			{
			prev->next = curr->next;
			mFree (curr);
			curr = prev->next;
			}
		    }
		}
	    else
		{
		if (xmax > curr->right) // segmento disminuye tamanho
				   //  left         right
				   //         xmin            xmax
		    {
		    curr->right = xmin;
		    prev = curr;
		    curr = curr->next;
		    }
		else			// el segmento se divide
				   //  left                 right
				   //         xmin     xmax
		    {
		    aux = (SegVisNode *) mMalloc (sizeof(SegVisNode));
            if (!aux)
                { //    1, "Can't Allocate Memory."
                char buf[100];
                GetError(1, buf);
                Error(FATAL_ERROR, 2, buf);
                }

		    aux->right = curr->right;
		    aux->left = xmax;
		    aux->next = curr->next;

		    curr->next = aux;
		    curr->right = xmin;
		    curr = NULL;
		    }
		}
	    }
	else
	    {
	    prev = curr;
	    curr = curr->next;
	    }
	}

    TraceOff(nome);
    if (!head) return 0;
    else return 1;
    }
