#include "mesh3d.h"
#include <math.h>
#include <string.h>

void Mesh3d::DXFOut(FILE *fp)
    {
    fprintf(fp, "  0\nSECTION\n  2\nENTITIES\n  0\nPOLYLINE\n  8\n0\n");
    fprintf(fp, " 66\n 1\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n 64\n");
    fprintf(fp, " 71\n%ld\n 72\n%ld\n  0\n",
            Coords->GetNumNodes(), ElGrps->GetNumEls());
    Coords->DXFOut(fp);
    ElGrps->DXFOut(fp);
    fprintf(fp, "SEQEND\n 8\n0\n 0\nENDSEC\n 0\nEOF\n");
    }

void Mesh3d::DXFIn(FILE *fp)
		{
    static char *nome = "Mesh3d::DXFIn";
    char buf[256];
    long nodtot = 0;
    long nodp   = 0;
    int ngroups = 0;
    long N,M;
    long nel[500];
    nel[0] = 0;
    int flag,flagb;
    double x,y,z;
    long n1,n2,n3;
    FILE *fw;

    TraceOn(nome);

    fw = fopen("dxf.vwm","w");

//              Barre el archivo para contar numero de nodos y caras
//              y aprovecha para imprimir la incidencia
    fprintf(fw,"*INCIDENCE\n");
    while(fscanf(fp,"%s",buf) != EOF)
	{
	if (strcmp(buf,"POLYLINE")) continue;
	flag = 1;
	while (flag)
	    {
	    fscanf(fp,"%d",&flag);
	    switch (flag)
		{
	    case 70:
		fscanf(fp,"%d",&flagb);
		break;
						case 71:
		fscanf(fp,"%ld",&M);
		break;
						case 72:
		fscanf(fp,"%ld",&N);
		break;
						case 0:
		break;
	    default:
		fscanf(fp,"%s",buf);
		}
	    }

	if ((flagb & 64) == 64)    // Lo que viene es un POLY-FACE-MESH
		    {
			while (1)
		{
		if(fscanf(fp,"%s",buf) == EOF) break;
		if (!strcmp(buf,"SEQEND")) break;
		    if (strcmp(buf,"VERTEX")) break;
		    flag = 1;
		while(flag)
		    {
			fscanf(fp,"%d",&flag);
			switch (flag)
			{
		    case 70:
			fscanf(fp,"%d",&flagb);
			    break;
		    case 71:
			fscanf(fp,"%ld",&n1);
			    break;
			case 72:
			fscanf(fp,"%ld",&n2);
			    break;
			case 73:
			fscanf(fp,"%ld",&n3);
			break;
		    case 0:
			    break;
		    default:
			    fscanf(fp,"%s",buf);
			    }
		    }
		if (flagb == 192) nodtot++;
		else if (flagb == 128)
		    {
		    nel[ngroups]++;
		    fprintf(fw,"%ld %ld %ld\n",
			    labs(n2)+nodp,labs(n1)+nodp,labs(n3)+nodp);
		    }
		}
	    }

	if ((flagb & 16) == 16)   // Lo que viene es un 3D-POLYGON-MESH
	    {
	    long i,j,ip;
	    for (i=0; i<M-1; i++)
		{
		ip = i*N + nodp + 1;
		for (j=0; j<N-1; j++)
		    {
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n", ip+j, ip+j+1, ip+N+j+1);
		    fprintf(fw,"%ld %ld %ld\n", ip+j, ip+N+j+1, ip+N+j);
		    }
		}
	    if ((flagb & 1) == 1)  // Cerrada en M
		{
		ip = N*(M-1)+nodp + 1;
		for (j=0; j<N-1; j++)
		    {
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n",ip+j, ip+j+1, nodp+1+j+1);
		    fprintf(fw,"%ld %ld %ld\n",ip+j, nodp+1+j+1, nodp+1+j);
		    }
		}
	    if ((flagb & 32) == 32)  // Cerrada en N
		{
		for (i=0; i<M-1; i++)
		    {
		    ip = i*N + nodp + 1;
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n", ip+N-1, ip, ip+N);
		    fprintf(fw,"%ld %ld %ld\n", ip+N-1, ip+N, ip+N+N-1);
		    }
		}
	    if ((flagb & 33) == 33)  // Cerrada en N y en M
		{
		nel[ngroups]+=2;
                fprintf(fw,"%ld %ld %ld\n", nodp+N*M, nodp+N*(M-1)+1, nodp+1);
                fprintf(fw,"%ld %ld %ld\n", nodp+N*M, nodp+1, nodp+N);
                }

            while (1)   // Cuenta los nodos (deben ser M*N)
                {
                if(fscanf(fp,"%s",buf) == EOF) break;
                if (!strcmp(buf,"SEQEND")) break;
	            if (strcmp(buf,"VERTEX")) break;
                flag = 1;
                while(flag)
                    {
                    fscanf(fp,"%d",&flag);
                    switch (flag)
                        {
                    case 70:
                        fscanf(fp,"%d",&flagb);
                        break;
                    case 0:
                        break;
                    default:
                        fscanf(fp,"%s",buf);
  	                    }
                    }
                if ((flagb & 64) == 64) nodtot++;
                }
			if (nodtot != nodp+N*M)
		 Error(FATAL_ERROR, 1, "DXF_file is corrupted");
            }
	ngroups++;
        nel[ngroups] = 0;
        nodp = nodtot;
        if (ngroups >=500)
	     Error(FATAL_ERROR, 2, "DXFIn->number of groups exceeded");
        }

//              Imprime informacion de grupos

    fprintf(fw,"*ELEMENT GROUPS\n");
    fprintf(fw,"%d\n", ngroups);
    for (int i=0; i<ngroups; i++)
	fprintf(fw,"%d %ld Tri3\n", i+1, nel[i]);

//               Imprime coordenadas

    nodp = 1;
    rewind(fp);
    fprintf(fw,"*COORDINATES\n %ld\n", nodtot);
    while(fscanf(fp,"%s",buf) != EOF)
	{
	if (strcmp(buf,"POLYLINE")) continue;
	flag = 1;
	while (flag)
	    {
	    fscanf(fp,"%d",&flag);
	    switch (flag)
		{
	    case 70:
		fscanf(fp,"%d",&flagb);
		break;
	    case 0:
		break;
	    default:
		fscanf(fp,"%s",buf);
		}
	    }
//        if (flagb != 64) continue;

	while (1)
	    {
	    if(fscanf(fp,"%s",buf) == EOF) break;
	    if (!strcmp(buf,"SEQEND")) break;
	    if (strcmp(buf,"VERTEX")) break;
	    flag = 1;
	    while(flag)
		{
		fscanf(fp,"%d",&flag);
		switch (flag)
		   {
		case 70:
		   fscanf(fp,"%d",&flagb);
		   break;
		case 10:
		   fscanf(fp,"%lf",&x);
		   break;
		case 20:
		   fscanf(fp,"%lf",&y);
		   break;
		case 30:
		   fscanf(fp,"%lf",&z);
		   break;
		case 0:
		   break;
		default:
		   fscanf(fp,"%s",buf);
		   }
		}
	    if ((flagb & 64) == 64)
			    fprintf(fw,"%ld %lg %lg %lg\n",nodp++,x,y,z);
	    }
	if (strcmp(buf,"SEQEND"))
	    Error(FATAL_ERROR, 3, "DXF_file is corrupted");
	}

    fprintf(fw,"*END\n");
    fclose(fw);
    fw = fopen("dxf.vwm","r");
    Read(fw);
    fclose(fw);

    TraceOff(nome);
    }
