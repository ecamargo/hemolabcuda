#include "mesh3d.h"
#include "dyncoo3d.h"
#include "acqueue.h"
#include <string.h>
#include <math.h>
#include <stdio.h>


double dist2(acPoint3 &p1, acPoint3 &p2)
     {
     return
     ((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z));
     }

int Conviene_123(acPoint3 &p1, acPoint3 &p2, acPoint3 &p3, acPoint3 &p4)
    {
    double x21,x31,x41,x43,x32,x42;	//  2---3
    double y21,y31,y41,y43,y32,y42;	//  |   |
    double z21,z31,z41,z43,z32,z42;	//  1---4
    double m21,m31,m32,m41,m42,m43;
    double cos312,cos231,cos314,cos431,maxcosa;
    double cos241,cos124,cos243,cos324,maxcosb;

    x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
    x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
    x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
    x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;
    x32 = p3.x-p2.x;   y32 = p3.y-p2.y;   z32 = p3.z-p2.z;
    x42 = p4.x-p2.x;   y42 = p4.y-p2.y;   z42 = p4.z-p2.z;

    m21 = sqrt(x21*x21+y21*y21+z21*z21);
    m31 = sqrt(x31*x31+y31*y31+z31*z31);
    m32 = sqrt(x32*x32+y32*y32+z32*z32);
    m41 = sqrt(x41*x41+y41*y41+z41*z41);
    m42 = sqrt(x42*x42+y42*y42+z42*z42);
    m43 = sqrt(x43*x43+y43*y43+z43*z43);

    // Angulo minimo configuracion 123  -  341
    cos312 = (x21*x31 + y21*y31 + z21*z31) / (m31*m21);
    cos231 = (x32*x31 + y32*y31 + z32*z31) / (m31*m32);
    cos314 = (x41*x31 + y41*y31 + z41*z31) / (m31*m41);
    cos431 = (x43*x31 + y43*y31 + z43*z31) / (m31*m43);
    maxcosa = cos312 > cos231 ? cos312 : cos231;
    maxcosa = cos314 > maxcosa ? cos314 : maxcosa;
    maxcosa = cos431 > maxcosa ? cos431 : maxcosa;

    // Angulo minimo configuracion 412  -  234
    cos241 = (x41*x42 + y41*y42 + z41*z42) / (m42*m41);
    cos124 = (x21*x42 + y21*y42 + z21*z42) / (m42*m21);
    cos243 = (x43*x42 + y43*y42 + z43*z42) / (m42*m43);
    cos324 = (x32*x42 + y32*y42 + z32*z42) / (m42*m32);
    maxcosb = cos241 > cos124 ? cos241 : cos124;
    maxcosb = cos243 > maxcosb ? cos243 : maxcosb;
    maxcosb = cos324 > maxcosb ? cos324 : maxcosb;

    if (maxcosa > maxcosb) // angulo a < angulo b
	return 0;
    else
	return 1;
    }

void Mesh3d::DXFOut(FILE *fp)
    {
    fprintf(fp, "  0\nSECTION\n  2\nENTITIES\n  0\nPOLYLINE\n  8\n0\n");
    fprintf(fp, " 66\n 1\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n 64\n");
    fprintf(fp, " 71\n%ld\n 72\n%ld\n  0\n",
            Coords->GetNumNodes(), ElGrps->GetNumEls());
    Coords->DXFOut(fp);
    ElGrps->DXFOut(fp);
    fprintf(fp, "SEQEND\n 8\n0\n 0\nENDSEC\n 0\nEOF\n");
    }

void Mesh3d::DXFIn(FILE *fp)
    {
    static char *nome = "Mesh3d::DXFIn";
    char buf[256];
    long nodp;
    int ngroups;
    long N, M;
    acQueue NelGr(sizeof(long));
    long nelgr;
    int flag, polytype, vertextype;
    acPoint3 pp;
    long n1, n2, n3, n4;
    acDynCoord3D *LDCoord;
    FILE *fw;

    TraceOn(nome);

    fw = fopen("dxf.vwm","w");

    acDynCoord3D DCoord;

    fprintf(fw,"*INCIDENCE\n");
    nodp = ngroups = 0;
    while(fscanf(fp,"%s",buf) != EOF)
	{
	if (strcmp(buf,"POLYLINE")) continue;
	nelgr = 0;
	LDCoord = new acDynCoord3D;

	flag = 1;
	while (flag)
	    {
	    fscanf(fp,"%d",&flag);
	    switch (flag)
		{
	    case 70:
		fscanf(fp,"%d",&polytype);
		break;
	    case 71:
		fscanf(fp,"%ld",&M);
		break;
	    case 72:
		fscanf(fp,"%ld",&N);
		break;
	    case 0:
		break;
	    default:
		fscanf(fp,"%s",buf);
		}
	    }

	while (1)
	    {
	    if(fscanf(fp,"%s",buf) == EOF) break;
	    if (!strcmp(buf,"SEQEND")) break;
	    if (strcmp(buf,"VERTEX")) break;
	    flag = 1;
	    while(flag)
		{
		fscanf(fp,"%d",&flag);
		switch (flag)
		    {
		case 10:
		    fscanf(fp,"%lf",&pp.x);
		    break;
		case 20:
		    fscanf(fp,"%lf",&pp.y);
		    break;
		case 30:
		    fscanf(fp,"%lf",&pp.z);
		    break;
		case 70:
		    fscanf(fp,"%d",&vertextype);
		    break;
		case 71:
		    fscanf(fp,"%ld",&n1);
		    break;
		case 72:
		    fscanf(fp,"%ld",&n2);
		    break;
		case 73:
		    fscanf(fp,"%ld",&n3);
		    break;
		case 0:
		    break;
		default:
		    fscanf(fp,"%s",buf);
		    }
		}
	    if ((vertextype  &  64 && polytype & 16) // 3D-POLYGON-MESH: 64
	    ||  (vertextype == 192 && polytype & 64))// POLY-FACE-MESH: 192
		LDCoord->AddCoord(pp);               // Coordenada
	    else if (vertextype == 128 && polytype & 64)// Cara de POLY-FACE-MESH
		{
		nelgr++;
		fprintf(fw,"%ld %ld %ld\n",
		    labs(n2)+nodp,labs(n1)+nodp,labs(n3)+nodp);
		}
	    }

	if (polytype & 64)    // Lo que vino es un POLY-FACE-MESH
	    {
	    long nodloc = LDCoord->GetNumNodes();
	    for (long i = 0; i < nodloc; i++)
		{
		pp = (*LDCoord)[i];
		DCoord.AddCoord(pp);
		}
	    }

	else if (polytype & 16)   // Lo que vino es un 3D-POLYGON-MESH
	    {
	    long i,j;

	    if (LDCoord->GetNumNodes() != N*M)
		 Error(FATAL_ERROR, 1, "DXF_file is corrup1ed");

	    long huge *vper = (long huge *) mMalloc (M*N * sizeof(long));
	    if (!vper)
		Error(FATAL_ERROR, 1, "Insufficient memory");
	    for (i = 0; i < M*N; i++)
		vper[i] = i;

	    acLimit3 Vol = LDCoord->Box();
	    acPoint3 p1, p2, p3, p4;
	    double dx, dy, dz, dtip2;
	    dx = Vol.xmax - Vol.xmin;
	    dy = Vol.ymax - Vol.ymin;
	    dz = Vol.zmax - Vol.zmin;
	    dtip2 = dx*dx + dy*dy + dz*dz;

	    // Pega lados Sur y Norte
	    for (i = 1; i < M; i++)
		{
		p1 = (*LDCoord)[(i-1)*N];
		p2 = (*LDCoord)[ i   *N];
		if (dist2(p1, p2) < 1.0e-12 * dtip2)
		    {
		    vper[i*N] = vper[(i-1)*N];
		    }
		p1 = (*LDCoord)[ i   *N-1];
		p2 = (*LDCoord)[(i+1)*N-1];
		if (dist2(p1, p2) < 1.0e-12 * dtip2)
		    {
		    vper[(i+1)*N-1] = vper[i*N-1];
		    }
		}

	    // Pega lados Este y Oeste
	    for (j = 1; j < N; j++)
		{
		p1 = (*LDCoord)[j-1];
		p2 = (*LDCoord)[j];
		if (dist2(p1, p2) < 1.0e-12 * dtip2)
		    {
		    vper[j] = vper[j-1];
		    }
		p1 = (*LDCoord)[(M-1)*N+j-1];
		p2 = (*LDCoord)[(M-1)*N+j];
		if (dist2(p1, p2) < 1.0e-12 * dtip2)
		    {
		    vper[(M-1)*N+j] = vper[(M-1)*N+j-1];
		    }
		}

	    long nodloc = 0;
	    for (i = 0; i < N*M; i++)
		{
		if (vper[i] != i)
		    vper[i] = vper[vper[i]];
		else
		    {
		    p1 = (*LDCoord)[i];
		    DCoord.AddCoord(p1);
		    vper[i] = nodloc++;
		    }
		}


	    for (i=0; i<M-1; i++)
		{
		for (j=0; j<N-1; j++)
		    {
		    n1 = vper[i*N+j]    +nodp;
		    n2 = vper[i*N+j+1]  +nodp;
		    n3 = vper[i*N+N+j+1]+nodp;
		    n4 = vper[i*N+N+j]  +nodp;

		    if (n1 == n2)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n3+1, n4+1);
		    else if (n2 == n3)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n4+1);
		    else if (n3 == n4)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else if (n4 == n1)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else
			{
			p1 = DCoord[n1];
			p2 = DCoord[n2];
			p3 = DCoord[n3];
			p4 = DCoord[n4];
			if (Conviene_123(p1, p2, p3, p4))
			    {
			    fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
			    fprintf(fw,"%ld %ld %ld\n", n3+1, n4+1, n1+1);
			    }
			else
			    {
			    fprintf(fw,"%ld %ld %ld\n", n4+1, n1+1, n2+1);
			    fprintf(fw,"%ld %ld %ld\n", n2+1, n3+1, n4+1);
			    }
			nelgr++;
			}
		    nelgr++;
		    }
		}
	    if (polytype & 1)  // Cerrada en M
		{
		for (j=0; j<N-1; j++)
		    {
		    n1 = vper[(M-1)*N+j]  +nodp;
		    n2 = vper[(M-1)*N+j+1]+nodp;
		    n3 = vper[j+1]        +nodp;
		    n4 = vper[j]          +nodp;

		    if (n1 == n2)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n3+1, n4+1);
		    else if (n2 == n3)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n4+1);
		    else if (n3 == n4)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else if (n4 == n1)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else
			{
			p1 = DCoord[n1];
			p2 = DCoord[n2];
			p3 = DCoord[n3];
			p4 = DCoord[n4];
			if (Conviene_123(p1,p2,p3,p4))
			    {
			    fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
			    fprintf(fw,"%ld %ld %ld\n", n3+1, n4+1, n1+1);
			    }
			else
			    {
			    fprintf(fw,"%ld %ld %ld\n", n4+1, n1+1, n2+1);
			    fprintf(fw,"%ld %ld %ld\n", n2+1, n3+1, n4+1);
			    }
			nelgr++;
			}
		    nelgr++;
		    }
		}
	    if (polytype & 32)  // Cerrada en N
		{
		for (i=0; i<M-1; i++)
		    {
		    n1 = vper[i*N+N-1]  +nodp;
		    n2 = vper[i*N]      +nodp;
		    n3 = vper[i*N+N]    +nodp;
		    n4 = vper[i*N+N+N-1]+nodp;

		    if (n1 == n2)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n3+1, n4+1);
		    else if (n2 == n3)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n4+1);
		    else if (n3 == n4)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else if (n4 == n1)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else
			{
			p1 = DCoord[n1];
			p2 = DCoord[n2];
			p3 = DCoord[n3];
			p4 = DCoord[n4];
			if (Conviene_123(p1,p2,p3,p4))
			    {
			    fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
			    fprintf(fw,"%ld %ld %ld\n", n3+1, n4+1, n1+1);
			    }
			else
			    {
			    fprintf(fw,"%ld %ld %ld\n", n4+1, n1+1, n2+1);
			    fprintf(fw,"%ld %ld %ld\n", n2+1, n3+1, n4+1);
			    }
			nelgr++;
			}
		    nelgr++;
		    }
		}
	    if ((polytype & 33) == 33)  // Cerrada en N y en M
		{
		    n1 = vper[N*M-1]+nodp;
		    n2 = vper[N*M-N]+nodp;
		    n3 = vper[0]    +nodp;
		    n4 = vper[N-1]  +nodp;

		    if (n1 == n2)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n3+1, n4+1);
		    else if (n2 == n3)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n4+1);
		    else if (n3 == n4)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else if (n4 == n1)
			fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
		    else
			{
			p1 = DCoord[n1];
			p2 = DCoord[n2];
			p3 = DCoord[n3];
			p4 = DCoord[n4];
			if (Conviene_123(p1,p2,p3,p4))
			    {
			    fprintf(fw,"%ld %ld %ld\n", n1+1, n2+1, n3+1);
			    fprintf(fw,"%ld %ld %ld\n", n3+1, n4+1, n1+1);
			    }
			else
			    {
			    fprintf(fw,"%ld %ld %ld\n", n4+1, n1+1, n2+1);
			    fprintf(fw,"%ld %ld %ld\n", n2+1, n3+1, n4+1);
			    }
			nelgr++;
			}
		    nelgr++;
		}

	    mFree (vper);
	    }

	if (nelgr)
	    {
	    NelGr.Push(&nelgr);
	    ngroups++;
	    }
	nodp = DCoord.GetNumNodes();
	delete LDCoord;
	}

//              Imprime informacion de grupos

    fprintf(fw,"*ELEMENT GROUPS\n");
    fprintf(fw,"%d\n", ngroups);
    for (int i=0; i<ngroups; i++)
	{
	NelGr.Pop(&nelgr);
	fprintf(fw,"%d %ld Tri3\n", i+1, nelgr);
	}

//               Imprime coordenadas

//    fprintf(fw,"*COORDINATES\n %ld\n", nodp);
    DCoord.Print(fw);
    fprintf(fw,"*END\n");
    fclose(fw);


    fw = fopen("dxf.vwm","r");
    Read(fw);
    fclose(fw);

    TraceOff(nome);
    }


double Mesh3d::Volume()
    {
    int i;
    long j;
    Element *el;
    double vol = 0;

    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++)
            {
            SetElData(i, j);
            vol += el->Volume();
            }
        }

    return(vol);
    }

double Mesh3d::Volume(int huge *elstatus)
    {
    int i;
    long j, nel;
    Element *el;
    double vol = 0;

    nel = 0;
    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++, nel++)
            if (elstatus[nel])
                {
                SetElData(i, j);
                vol += el->Volume();
                }
        }

    return(vol);
    }

//SparseFaFaV *Mesh3d::NewFaFaV()
//    {
//    return(new SparseFaFaV(ElGrps));
//    }

void Mesh3d::Read(FILE *fp)
    {
    static char *nome = "Mesh3d::Read";
    static char *kelem  = "ELEMENT GROUPS";
    static char *kelema = "ELEMENT_GROUPS";

    TraceOn(nome);

    Coords = new acCoordinate3D;
    Coords->Read(fp);

    if (!FindKeyWord(kelem, fp))
      if (!FindKeyWord(kelema, fp))
        {
        Error(FATAL_ERROR, 1, "Keyword %s or %s not found", kelem, kelema);
        }

    ElGrps = new ElGroups;
    ElGrps->Read(fp);

    TraceOff(nome);
    }

void Mesh3d::Print(FILE *fp)
    {
    static char *nome = "Mesh3d::Print";

    TraceOn(nome);

    Coords->Print(fp);

    ElGrps->Print(fp);

    TraceOn(nome);
    }

void Mesh3d::SaveSPI(FILE *fp)
    {
    static char *nome = "Mesh3d::SaveSPI";
    long n,ne,j,iel;
    Element *el;
    double h;
    acPoint3 p;

    TraceOn(nome);

    SparseNoEl *noel = new SparseNoEl(ElGrps);

    fprintf(fp, "%ld\n",Coords->GetNumNodes());
    for (n=0; n< Coords->GetNumNodes(); n++)
	{
	h  = 0.0;
	p  = (*Coords)[n];
	ne = noel->GetNelems(n);
	for (j=0; j<ne; j++)
	    {
	    iel = noel->GetElem(n,j);
	    el  = SetAnElem(iel);
	    h   = h + el->Perimeter();
	    }
	if (ne > 0)
	    {
	    h /=(ne*3);
	    fprintf(fp, "%lg %lg %lg %lg\n", p.x,p.y,p.z,h);
	    }
	}
    delete noel;
    TraceOff(nome);
    }
