#include "quality.h"

void Quality::Print(FILE *fp)
    {
    long i;

    fprintf(fp, "NumElems = %ld\n", NumElems);
    fprintf(fp, "MinQlty = %lg  MaxQlty = %lg  AverageQlty = %lg\n",
                 MinQlty, MaxQlty, AverageQlty);

    fprintf(fp, "vector\n");
    for (i = 0; i < NumElems; i++)
        fprintf(fp, "%ld -> %ld  %lg\n", i + 1, Qlty[i]->ElNum,
                                               Qlty[i]->Qlty); 
    }    

