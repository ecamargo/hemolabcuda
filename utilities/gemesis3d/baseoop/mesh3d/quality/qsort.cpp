#include "quality.h"

static int cmp(const void   *a, const void   *b)
    {
    psqlty *a1 = (psqlty *) a;
    psqlty *b1 = (psqlty *) b;
    psqlty a2 = *a1;
    psqlty b2 = *b1;

    if (a2->Qlty > b2->Qlty)
        return(1);
    else if (a2->Qlty < b2->Qlty)
        return(-1);
    else return(0);
    }

void Quality::Qsort()
    {
    QSort(Qlty, NumElems, sizeof(psqlty), cmp);
    }

