#include "quality.h"

Quality::Quality(Mesh3d *mesh)
    {
    long iel, i;
    char buf[80];
    Element *el;
    char nome[]="Quality::Quality";
    TraceOn(nome);

    NumElems = mesh->GetNumEls();

    Qlty = (psqlty   *) mMalloc(sizeof( psqlty   *) * NumElems);
    if (!Qlty)
        { //    1, "Can't Allocate Memory."
        GetError(1, buf);
        Error(FATAL_ERROR, 1, buf);
        }

    for (i = 0; i < NumElems; i++)
        {
        Qlty[i] = (sqlty *) mMalloc(sizeof(sqlty));
        if (!Qlty[i])
            { //    1, "Can't Allocate Memory."
            GetError(1, buf);
            Error(FATAL_ERROR, 1, buf);
            }
        }

    iel  = 0;

    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem(), iel++)
        {
        Qlty[iel]->ElNum = iel;
        Qlty[iel]->Qlty  = el->Quality();
        }

    Qsort();

    MinQlty     = Qlty[0]->Qlty;
    MaxQlty     = Qlty[NumElems - 1]->Qlty;
    AverageQlty = 0;
    for (i = 0; i < NumElems; i++)
        AverageQlty += Qlty[i]->Qlty;

    AverageQlty = AverageQlty / NumElems;
    TraceOff(nome);
    }

Quality::~Quality()
    {
    for (long i = 0; i < NumElems; i++)
        mFree((char *) Qlty[i]);

    mFree((char *) Qlty);
    }

