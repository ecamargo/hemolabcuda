#include "quality.h"

long Quality::GetWorstEl(int   *elstatus, double fac)
    {
    long i;
    int    *pe;
    double aux = NumElems * fac;
    long  nwel = (long) aux + 1;

    if (nwel > NumElems)
        nwel = NumElems;
    if (nwel <= 0) nwel = 1;

    pe = elstatus;
    for (i = 0; i < NumElems; i++)
        *pe++ = 0;

    for (i = 0; i < nwel; i++)
        elstatus[Qlty[i]->ElNum] = 1;
    return(nwel);
    }
