#include "mesh3d.h"

void Mesh3d::Read(FILE *fp)
    {
    static char *nome = "Mesh3d::Read";
    char buf[80];
    static char *kelem  = "ELEMENT GROUPS";

    TraceOn(nome);

    Coords = new acCoordinate3D;
    Coords->Read(fp);

    if (!FindKeyWord(kelem, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, kelem);
        }

    ElGrps = new ElGroups;
    ElGrps->Read(fp);

    TraceOff(nome);
    }

void Mesh3d::Print(FILE *fp)
    {
    static char *nome = "Mesh3d::Print";

    TraceOn(nome);

    Coords->Print(fp);

    ElGrps->Print(fp);

    TraceOn(nome);
    }
