#include <stdio.h>
#include "mesh3d.h"

void Mesh3d::SaveSPI(FILE *fp)
    {
    static char *nome = "Mesh3d::SaveSPI";
    long n,ne,j,iel;
    Element *el;
    double h;
    acPoint3 p;

    TraceOn(nome);

    SparseNoEl *noel = new SparseNoEl(ElGrps);

    fprintf(fp, "%ld\n",Coords->GetNumNodes());
    for (n=0; n< Coords->GetNumNodes(); n++)
	{
	h  = 0.0;
	p  = (*Coords)[n];
	ne = noel->GetNelems(n);
	for (j=0; j<ne; j++)
	    {
	    iel = noel->GetElem(n,j);
	    el  = SetAnElem(iel);
	    h   = h + el->Perimeter();
	    }
	if (ne > 0)
	    {
	    h /=(ne*3);
	    fprintf(fp, "%lg %lg %lg %lg\n", p.x,p.y,p.z,h);
	    }
	}
    delete noel;
    TraceOff(nome);
    }
