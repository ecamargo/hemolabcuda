#include "mesh3d.h"

double Mesh3d::Volume()
    {
    int i;
    long j;
    Element *el;
    double vol = 0;

    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++)
            {
            SetElData(i, j);
            vol += el->Volume();
            }
        }

    return(vol);
    }

double Mesh3d::Volume(int   *elstatus)
    {
    int i;
    long j, nel;
    Element *el;
    double vol = 0;

    nel = 0;
    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++, nel++)
            if (elstatus[nel])
                {
                SetElData(i, j);
                vol += el->Volume();
                }
        }

    return(vol);
    }
