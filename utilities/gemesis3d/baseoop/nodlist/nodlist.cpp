#include "nodlist.h"

nodlist::~nodlist()
    {
    struct nod *pr;
    while (first)
        {
        pr=first;
        first=first->next;
        delete pr;
        }
    }
    
int nodlist::add(long n)
    {
    int num=0;
    struct nod *pr=0,*el=first;
    while (el)
        {
        pr = el;
        if (el->n==n) num++;
        el=el->next;
        }
    if (pr)
        {
        pr->next=new struct nod;
        pr->next->n=n;
        pr->next->next=0;
        }
    else
        {
        first=new struct nod;
        first->n=n;
        first->next=0;
        }
    return num;
    }
