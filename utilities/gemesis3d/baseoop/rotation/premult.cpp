#include "acdp.h"
#include "rotation.h"

void acRotation::PreMult(acRotation *r, acRotation *a)
    {
    int i, j, k;
    double aux;

    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            {
            aux = 0;
            for (k = 0; k < 3; k++)
                aux += a->Mat[i][k]  * Mat[k][j];
            r->Mat[i][j] = aux;
            }
    }
            
void acRotation::MultM3(double a[3][3], double b[3][3])            
   {
    int i, j, k;
    double aux;
    double maux[3][3];
    
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            {
            aux = 0;
            for (k = 0; k < 3; k++)
                aux += a[i][k]  * b[k][j];
            maux[i][j] = aux;
            }
            
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            a[i][j] = maux[i][j];            
    }
