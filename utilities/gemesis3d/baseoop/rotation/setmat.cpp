#include "acdp.h"
#include "rotation.h"
#include <math.h>

void acRotation::SetMat(double theta, double phi)
    {
    /*
    **
    **    Conversion de coordenadas segun punto de vista
    **
    **      / u \   / -sin ti          cos ti          0.0    \ / x \
    **      | v | = | -cos ti sin fi  -sin ti sin fi   cos fi | | y |
    **      \ w /   \  cos ti cos fi   sin ti cos fi   sin fi / \ z /
    **
    */

    double sinti, costi, sinfi, cosfi;

    costi = cos(theta * 0.01745329252);
    sinti = sin(theta * 0.01745329252);
    cosfi = cos(phi   * 0.01745329252);
    sinfi = sin(phi   * 0.01745329252);

    Mat[0][0] = -sinti;       Mat[0][1] =  costi;       Mat[0][2] = 0.0;
    Mat[1][0] = -costi*sinfi; Mat[1][1] = -sinti*sinfi; Mat[1][2] = cosfi;
    Mat[2][0] =  cosfi*costi; Mat[2][1] =  cosfi*sinti; Mat[2][2] = sinfi;

    }

void acRotation::SetMat(double rx, double ry, double rz)
    {
    double mat[3][3];
    double s, c;

    if(rx != 0.0)
        {
        s = sin(rx); c = cos(rx);
        mat[0][0] =  1; mat[0][1] =  0; mat[0][2] =  0;
        mat[1][0] =  0; mat[1][1] =  c; mat[1][2] =  s;
        mat[2][0] =  0; mat[2][1] = -s; mat[2][2] =  c;
        MultM3(Mat, mat);
        }
    if(ry != 0.0)
        {
        s = sin(ry); c = cos(ry);
        mat[0][0] =  c; mat[0][1] =  0; mat[0][2] = -s;
        mat[1][0] =  0; mat[1][1] =  1; mat[1][2] =  0;
        mat[2][0] =  s; mat[2][1] =  0; mat[2][2] =  c;
        MultM3(Mat, mat);
        }
    if(rz != 0.0)
        {
        s = sin(rz); c = cos(rz);
        mat[0][0] =  c; mat[0][1] =  s; mat[0][2] =  0;
        mat[1][0] = -s; mat[1][1] =  c; mat[1][2] =  0;
        mat[2][0] =  0; mat[2][1] =  0; mat[2][2] =  1;
        MultM3(Mat, mat);
        }
    }

void acRotation::SetMat(double *v)
    {
    int i, j, k;

    k = 0;
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++, k++)
            Mat[i][j] = v[k];
    }

