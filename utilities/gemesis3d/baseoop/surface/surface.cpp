#include "surface.h"
#include "sparse.h"
#include <math.h>

Surface::Surface(acRotation *t, Mesh3d *mesh, int   *elstatus,
		 int closed, int swExp, SparseElElV *eev)
    {
    static char *nome = "Surface::Surface";
    TraceOn(nome);
    char buf[100];
    int grps = mesh->GetNumGroups();
    int gr, nfac, nf, edge[3];
    long iel, i, elv, inci[3];
    Element *el;
    long post3f;
    acPoint3 cent, norm;
    acCoordinate3D *coor = mesh->GetCoords();
    SparseElElV *elelv;

    if (eev)
	elelv = eev;
    else
	elelv = mesh->NewElElV();

    QTree = NULL;

    // Calcula el numero de triangulos que formaran la superficie

    Fac = 0.7;

    iel      = 0;
    NFacets  = 0;
    ElemIs3D = closed;
    for (el = mesh->SetFirstElem(); el; el = mesh->SetNextElem(), iel++)
	{
	nfac = el->NFacet();
	if (elstatus[iel])    // 1 = visivel
	    {
	    // varre os elementos vizinhos do elemento iel
	    for (nf = 0; nf < nfac; nf++)
		{
		int faz = 0;

		if (swExp)
		    faz = 1;
		else
		    {
		    elv = elelv->GetElem(iel, nf);
		    if (elv == -1)           faz = 1;
		    else if (!elstatus[elv]) faz = 1;
		    }

		if (faz)
		    NFacets += el->NumTriFac(nf);
		}
	    }
	}

    // Crea los Objetos CoordOri y CoordRot

    acPoint3 pn;
    long numnodes;
    if (swExp)
	numnodes = NFacets*3;
    else
	numnodes = mesh->GetNumNodes();

    CoordOri = new acCoordinate3D(numnodes);
    CoordRot = new acCoordinate3D(numnodes);
    acCoordinate3D &rcoor    = *coor;
    acCoordinate3D &coordori = *CoordOri;
    if (!CoordOri || !CoordRot)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 1, buf);
	}

    if (!swExp)
	coordori = rcoor;

    // constroi vetores
    if (NFacets > 0)
	{
	T3F = (PT3Facet   *) mMalloc(sizeof(PT3Facet) * NFacets);
	if (!T3F)
	    { //    1, "Can't Allocate Memory."
	    GetError(1, buf);
	    Error(FATAL_ERROR, 1, buf);
	    }
	}
    else
       {
       T3F = NULL;
       Error(0, 2, "All elements were removed");
       }

    iel    = 0;
    post3f = 0;
    for (gr = 0; gr < grps; gr++)   // varre grupos
	{
	el   = mesh->GetElement(gr);
	nfac = el->NFacet();

	// varre elementos
	for (i = 0; i < mesh->GetNumElsGrp(gr); i++, iel++)
	    {
	    if (elstatus[iel])    // 1 = visivel
		{
		acPoint3 grc;
		mesh->SetElData(gr, i);
		if (swExp) grc = el->GravCenter();

		// varre os elementos vizinhos do elemento iel
		for (nf = 0; nf < nfac; nf++)
		    {
		    int faz = 0;

		    if (swExp)
			faz = 1;
		    else
			{
			elv = elelv->GetElem(iel, nf);
			if (elv == -1)           faz = 1;
			else if (!elstatus[elv]) faz = 1;
			}

		    if (faz)
			{
			int nt, ntri;
			ntri = el->NumTriFac(nf);
			cent = el->FacetCenter(nf);
			for (nt = 0; nt < ntri; nt++, post3f++)
			    {
			    el->TriFacInf(nf, nt, inci, edge);
			    if (swExp)
				{
				long pos = post3f*3;
				pn = ExpCoor((acPoint3 &)rcoor[inci[0]],grc);
				inci[0] = pos++;
				coordori[inci[0]]= pn;
				pn = ExpCoor((acPoint3 &)rcoor[inci[1]],grc);
				inci[1] = pos++;
				coordori[inci[1]]= pn;
				pn = ExpCoor((acPoint3 &)rcoor[inci[2]],grc);
				inci[2] = pos;
				coordori[inci[2]]= pn;
				}

			    norm = el->TriFacNormal(nf, nt);

			    T3F[post3f] = new T3Facet(t, gr, iel, inci,
					    edge, cent, norm, ElemIs3D);
			    if (!T3F[post3f])
				{ //    1, "Can't Allocate Memory."
				GetError(1, buf);
				Error(FATAL_ERROR, 2, buf);
				}
			    }
			}
		    }
		}
	    }
	}

    Rotate(t);

    if (!eev)
	delete elelv;

// Modificacion de edge para opcion detect
    SparseEdges *edges;
    int k, l, existn2;
    long n1, n2, naux;
    edges = new SparseEdges(this, NULL, 1, 1, t);

    for (i = 0; i < NFacets; i++)
	{
	T3F[i]->GetIncid(inci);

	for (k = 0; k < 3; k++)
	    {
	    if (!T3F[i]->EdgeOn(k)) continue;
	    n1 = inci[k];
	    n2 = inci[(k+1)%3];
	    if (n2 < n1) { naux = n1; n1 = n2; n2 = naux; }
	    existn2 = 0;
	    for (l = 0; l < edges->GetNelems(n1); l++)
		{
		if (edges->GetElem(n1,l) == n2)
		    {
		    existn2 = 1;
		    break;
		    }
		}
	    if (existn2)
		T3F[i]->SetEdge(k, 2);
	    }
	}
    delete edges;

    TraceOff(nome);
    }

Surface::~Surface()
    {
    long i;

    delete CoordOri;
    delete CoordRot;

    for (i = 0; i < NFacets; i++)
	delete T3F[i];

    if(T3F)
	mFree((char *) T3F);

    if (QTree)
        delete QTree;
    }

void Surface::SetElemIs3D(int flag, acRotation *t)
    {
    long fac;
    ElemIs3D = flag;
    if (ElemIs3D)
	for (fac = 0; fac < NFacets; fac++)
	    T3F[fac]->SetVisible(t);
    else
	for (fac = 0; fac < NFacets; fac++)
	    T3F[fac]->SetVisible();
    }

void Surface::BuildQuadtree()
    {
    acLimit2 Volume;
    acPoint2 pp;
    acLimit2 volt;
    acPoint3 pp3[3];
    double size, dx, dy, dz, tari;
    long i;
    long inci[3];
    static char *nome = "Surface::BuildQuadtree";

    if (QTree) return;

    TraceOn(nome);

    Volume = GetXYLimit();

    QTree = new acQTree (Volume, sizeof(long));

    for (i = 0; i < NFacets; i++)
	{
	T3F[i]->GetIncid(inci);
	pp3[0] = (*CoordRot)[inci[0]];
	pp3[1] = (*CoordRot)[inci[1]];
	pp3[2] = (*CoordRot)[inci[2]];
	pp.x = 0.33333333333 * (pp3[0].x+pp3[1].x+pp3[2].x);
	pp.y = 0.33333333333 * (pp3[0].y+pp3[1].y+pp3[2].y);
	dx = pp3[0].x - pp3[1].x;
	dy = pp3[0].y - pp3[1].y;
	dz = pp3[0].z - pp3[1].z;
	size = dx*dx + dy*dy + dz*dz;
	dx = pp3[0].x - pp3[2].x;
	dy = pp3[0].y - pp3[2].y;
	dz = pp3[0].z - pp3[2].z;
	tari = dx*dx + dy*dy + dz*dz;
	size = tari > size ? tari : size;
	dx = pp3[1].x - pp3[2].x;
	dy = pp3[1].y - pp3[2].y;
	dz = pp3[1].z - pp3[2].z;
	tari = dx*dx + dy*dy + dz*dz;
	size = tari > size ? tari : size;
	QTree->AdjustSize (pp, sqrt(size));
	}

    for (i = 0; i < NFacets; i++)
	{
	T3F[i]->GetIncid(inci);
	pp3[0] = (*CoordRot)[inci[0]];
	pp3[1] = (*CoordRot)[inci[1]];
	pp3[2] = (*CoordRot)[inci[2]];
	volt.xmin = volt.xmax = pp3[0].x;
	volt.ymin = volt.ymax = pp3[0].y;
	if      (pp3[1].x < volt.xmin) volt.xmin = pp3[1].x;
	else if (pp3[1].x > volt.xmax) volt.xmax = pp3[1].x;
	if      (pp3[1].y < volt.ymin) volt.ymin = pp3[1].y;
	else if (pp3[1].y > volt.ymax) volt.ymax = pp3[1].y;
	if      (pp3[2].x < volt.xmin) volt.xmin = pp3[2].x;
	else if (pp3[2].x > volt.xmax) volt.xmax = pp3[2].x;
	if      (pp3[2].y < volt.ymin) volt.ymin = pp3[2].y;
	else if (pp3[2].y > volt.ymax) volt.ymax = pp3[2].y;
	QTree->InsertData (&i, volt);
	}

    TraceOff(nome);
    }

long Surface::FindFace(acPoint2 &p)
    {
    double pvec;
    long j;
    int signo;
    long incid[3];
    acPoint2 coor[3];

    if (QTree)
	{
	acNodeList *nl = QTree->GetListHead(p);
	while (nl)
	    {
	    j = *((long *) nl->GetDados());
	    nl = nl->next;

	    GetInfoFace(j, incid, coor);
	    pvec = (coor[1].x-coor[0].x)*(p.y-coor[0].y) -
		   (coor[1].y-coor[0].y)*(p.x-coor[0].x);
	    if (pvec < 0)
		signo = -1;
	    else
		signo = 1;
	    pvec = (coor[2].x-coor[1].x)*(p.y-coor[1].y) -
		   (coor[2].y-coor[1].y)*(p.x-coor[1].x);
	    if (pvec*signo < 0)
		continue;
	    pvec = (coor[0].x-coor[2].x)*(p.y-coor[2].y) -
		   (coor[0].y-coor[2].y)*(p.x-coor[2].x);
	    if (pvec*signo < 0)
		continue;
	    else
		return (j);
	    }
	}
    else
	{
	for (j = NFacets-1; j >= 0; j--)
	    {
	    GetInfoFace(j, incid, coor);
	    pvec = (coor[1].x-coor[0].x)*(p.y-coor[0].y) -
		   (coor[1].y-coor[0].y)*(p.x-coor[0].x);
	    if (pvec < 0)
		signo = -1;
	    else
		signo = 1;
	    pvec = (coor[2].x-coor[1].x)*(p.y-coor[1].y) -
		   (coor[2].y-coor[1].y)*(p.x-coor[1].x);
	    if (pvec*signo < 0)
		continue;
	    pvec = (coor[0].x-coor[2].x)*(p.y-coor[2].y) -
		   (coor[0].y-coor[2].y)*(p.x-coor[2].x);
	    if (pvec*signo < 0)
		continue;
	    else
		return (j);
	    }
	}
    return(-1);
    }

double Surface::GetColInt(long face, acRotation *t)
    {
    acPoint3 normori;
    double intens;
    normori = T3F[face]->GetNormal();
    intens = t->GetZ(normori);
    return (intens >= 0.0 ? intens : -intens);
    }

static int cmp(const void   *a, const void   *b)
    {
    PT3Facet   *a1 = (PT3Facet   *) a;
    PT3Facet   *b1 = (PT3Facet   *) b;
    PT3Facet a2 = *a1;
    PT3Facet b2 = *b1;
    double h1, h2;
    h1 = a2->GetKey();
    h2 = b2->GetKey();

    if (h1 > h2)
	return(1);
    else if (h2 > h1)
	return(-1);
    else return(0);
    }

void Surface::Qsort(acRotation *t)
    {
    long fac;
    // Setar as alturas das facetas
    for (fac = 0; fac < NFacets; fac++)
	{
	T3F[fac]->SetKey(t);
	if (ElemIs3D)
	   T3F[fac]->SetVisible(t);
	}
    QSort(T3F, NFacets, sizeof(PT3Facet), cmp);
    }
