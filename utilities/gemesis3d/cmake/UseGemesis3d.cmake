# Inclue os diretorios contendo os .h do Gemesis 3D
INCLUDE_DIRECTORIES( 
  ${gemesis3d_SOURCE_DIR}/acdpoop/header
  ${gemesis3d_SOURCE_DIR}/baseoop/header
  ${gemesis3d_SOURCE_DIR}/common/
  ${gemesis3d_SOURCE_DIR}/dela3d/
  ${gemesis3d_SOURCE_DIR}/qopt/
  ${gemesis3d_SOURCE_DIR}/trisurf/
)
