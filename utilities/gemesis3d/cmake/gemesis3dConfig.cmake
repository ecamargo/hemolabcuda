# $Id: UseHeMoLab.cmake 122 2006-03-29 16:54:54Z nacho $
#Neste arquivo devemos configurar todas as variáveis globais do projeto.

IF (NOT WIN32)
    ADD_DEFINITIONS(-D__LINUX__)
ENDIF(NOT WIN32)

INCLUDE (${CMAKE_ROOT}/Modules/FindVTK.cmake)
INCLUDE(${USE_VTK_FILE})

INCLUDE_DIRECTORIES(
	${gemesis3d_SOURCE_DIR}/acdpoop/header
    ${gemesis3d_SOURCE_DIR}/baseoop/header
    ${gemesis3d_SOURCE_DIR}/common/
    ${gemesis3d_SOURCE_DIR}/dela3d/
    ${gemesis3d_SOURCE_DIR}/qopt/
    ${gemesis3d_SOURCE_DIR}/trisurf/
)
