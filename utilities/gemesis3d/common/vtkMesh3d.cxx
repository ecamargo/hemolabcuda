
#include "vtkMesh3d.h"
#include "vtkSurfaceGen.h"
#include "vtkMeshData.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "dyncoo3d.h"
#include "acqueue.h"
#include <math.h>
#include <string.h>
#if defined(WIN32)
#include <io.h>
#endif

vtkCxxRevisionMacro(vtkMesh3d, "$Rev: 635 $");
vtkStandardNewMacro(vtkMesh3d);


double vtkMesh3d::dist2(acPoint3 &p1, acPoint3 &p2)
{
     return
     ((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z));
}

int vtkMesh3d::Conviene_123(acPoint3 &p1, acPoint3 &p2, acPoint3 &p3, acPoint3 &p4)
{
    double x21,x31,x41,x43,x32,x42;	//  2---3
    double y21,y31,y41,y43,y32,y42;	//  |   |
    double z21,z31,z41,z43,z32,z42;	//  1---4
    double m21,m31,m32,m41,m42,m43;
    double cos312,cos231,cos314,cos431,maxcosa;
    double cos241,cos124,cos243,cos324,maxcosb;

    x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
    x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
    x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
    x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;
    x32 = p3.x-p2.x;   y32 = p3.y-p2.y;   z32 = p3.z-p2.z;
    x42 = p4.x-p2.x;   y42 = p4.y-p2.y;   z42 = p4.z-p2.z;

    m21 = sqrt(x21*x21+y21*y21+z21*z21);
    m31 = sqrt(x31*x31+y31*y31+z31*z31);
    m32 = sqrt(x32*x32+y32*y32+z32*z32);
    m41 = sqrt(x41*x41+y41*y41+z41*z41);
    m42 = sqrt(x42*x42+y42*y42+z42*z42);
    m43 = sqrt(x43*x43+y43*y43+z43*z43);

    // Angulo minimo configuracion 123  -  341
    cos312 = (x21*x31 + y21*y31 + z21*z31) / (m31*m21);
    cos231 = (x32*x31 + y32*y31 + z32*z31) / (m31*m32);
    cos314 = (x41*x31 + y41*y31 + z41*z31) / (m31*m41);
    cos431 = (x43*x31 + y43*y31 + z43*z31) / (m31*m43);
    maxcosa = cos312 > cos231 ? cos312 : cos231;
    maxcosa = cos314 > maxcosa ? cos314 : maxcosa;
    maxcosa = cos431 > maxcosa ? cos431 : maxcosa;

    // Angulo minimo configuracion 412  -  234
    cos241 = (x41*x42 + y41*y42 + z41*z42) / (m42*m41);
    cos124 = (x21*x42 + y21*y42 + z21*z42) / (m42*m21);
    cos243 = (x43*x42 + y43*y42 + z43*z42) / (m42*m43);
    cos324 = (x32*x42 + y32*y42 + z32*z42) / (m42*m32);
    maxcosb = cos241 > cos124 ? cos241 : cos124;
    maxcosb = cos243 > maxcosb ? cos243 : maxcosb;
    maxcosb = cos324 > maxcosb ? cos324 : maxcosb;

    if (maxcosa > maxcosb) // angulo a < angulo b
	return 0;
    else
	return 1;
}

// Constructor
vtkMesh3d::vtkMesh3d()
{
  Coords = NULL;
  ElGrps = NULL;
}

// Destructor
vtkMesh3d::~vtkMesh3d()
{
	if (Coords) delete Coords;
	if (ElGrps) delete ElGrps;
}

void vtkMesh3d::DXFOut(FILE *fp)
    {
    fprintf(fp, "  0\nSECTION\n  2\nENTITIES\n  0\nPOLYLINE\n  8\n0\n");
    fprintf(fp, " 66\n 1\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n 64\n");
    fprintf(fp, " 71\n%ld\n 72\n%ld\n  0\n",
            Coords->GetNumNodes(), ElGrps->GetNumEls());
    Coords->DXFOut(fp);
    ElGrps->DXFOut(fp);
    fprintf(fp, "SEQEND\n 8\n0\n 0\nENDSEC\n 0\nEOF\n");
    }

void vtkMesh3d::DXFIn(FILE *fp)
		{
    static char *nome = "vtkMesh3d::DXFIn";
    char buf[256];
    long nodtot = 0;
    long nodp   = 0;
    int ngroups = 0;
    long N,M;
    long nel[500];
    nel[0] = 0;
    int flag,flagb;
    double x,y,z;
    long n1,n2,n3;
    FILE *fw;

    TraceOn(nome);

    fw = fopen("dxf.vwm","w");

//              Barre el archivo para contar numero de nodos y caras
//              y aprovecha para imprimir la incidencia
    fprintf(fw,"*INCIDENCE\n");
    while(fscanf(fp,"%s",buf) != EOF)
	{
	if (strcmp(buf,"POLYLINE")) continue;
	flag = 1;
	while (flag)
	    {
	    fscanf(fp,"%d",&flag);
	    switch (flag)
		{
	    case 70:
		fscanf(fp,"%d",&flagb);
		break;
						case 71:
		fscanf(fp,"%ld",&M);
		break;
						case 72:
		fscanf(fp,"%ld",&N);
		break;
						case 0:
		break;
	    default:
		fscanf(fp,"%s",buf);
		}
	    }

	if ((flagb & 64) == 64)    // Lo que viene es un POLY-FACE-MESH
		    {
			while (1)
		{
		if(fscanf(fp,"%s",buf) == EOF) break;
		if (!strcmp(buf,"SEQEND")) break;
		    if (strcmp(buf,"VERTEX")) break;
		    flag = 1;
		while(flag)
		    {
			fscanf(fp,"%d",&flag);
			switch (flag)
			{
		    case 70:
			fscanf(fp,"%d",&flagb);
			    break;
		    case 71:
			fscanf(fp,"%ld",&n1);
			    break;
			case 72:
			fscanf(fp,"%ld",&n2);
			    break;
			case 73:
			fscanf(fp,"%ld",&n3);
			break;
		    case 0:
			    break;
		    default:
			    fscanf(fp,"%s",buf);
			    }
		    }
		if (flagb == 192) nodtot++;
		else if (flagb == 128)
		    {
		    nel[ngroups]++;
		    fprintf(fw,"%ld %ld %ld\n",
			    labs(n2)+nodp,labs(n1)+nodp,labs(n3)+nodp);
		    }
		}
	    }

	if ((flagb & 16) == 16)   // Lo que viene es un 3D-POLYGON-MESH
	    {
	    long i,j,ip;
	    for (i=0; i<M-1; i++)
		{
		ip = i*N + nodp + 1;
		for (j=0; j<N-1; j++)
		    {
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n", ip+j, ip+j+1, ip+N+j+1);
		    fprintf(fw,"%ld %ld %ld\n", ip+j, ip+N+j+1, ip+N+j);
		    }
		}
	    if ((flagb & 1) == 1)  // Cerrada en M
		{
		ip = N*(M-1)+nodp + 1;
		for (j=0; j<N-1; j++)
		    {
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n",ip+j, ip+j+1, nodp+1+j+1);
		    fprintf(fw,"%ld %ld %ld\n",ip+j, nodp+1+j+1, nodp+1+j);
		    }
		}
	    if ((flagb & 32) == 32)  // Cerrada en N
		{
		for (i=0; i<M-1; i++)
		    {
		    ip = i*N + nodp + 1;
		    nel[ngroups]+=2;
		    fprintf(fw,"%ld %ld %ld\n", ip+N-1, ip, ip+N);
		    fprintf(fw,"%ld %ld %ld\n", ip+N-1, ip+N, ip+N+N-1);
		    }
		}
	    if ((flagb & 33) == 33)  // Cerrada en N y en M
		{
		nel[ngroups]+=2;
                fprintf(fw,"%ld %ld %ld\n", nodp+N*M, nodp+N*(M-1)+1, nodp+1);
                fprintf(fw,"%ld %ld %ld\n", nodp+N*M, nodp+1, nodp+N);
                }

            while (1)   // Cuenta los nodos (deben ser M*N)
                {
                if(fscanf(fp,"%s",buf) == EOF) break;
                if (!strcmp(buf,"SEQEND")) break;
	            if (strcmp(buf,"VERTEX")) break;
                flag = 1;
                while(flag)
                    {
                    fscanf(fp,"%d",&flag);
                    switch (flag)
                        {
                    case 70:
                        fscanf(fp,"%d",&flagb);
                        break;
                    case 0:
                        break;
                    default:
                        fscanf(fp,"%s",buf);
  	                    }
                    }
                if ((flagb & 64) == 64) nodtot++;
                }
			if (nodtot != nodp+N*M)
		 Error(FATAL_ERROR, 1, "DXF_file is corrupted");
            }
	ngroups++;
        nel[ngroups] = 0;
        nodp = nodtot;
        if (ngroups >=500)
	     Error(FATAL_ERROR, 2, "DXFIn->number of groups exceeded");
        }

//              Imprime informacion de grupos

    fprintf(fw,"*ELEMENT GROUPS\n");
    fprintf(fw,"%d\n", ngroups);
    for (int i=0; i<ngroups; i++)
	fprintf(fw,"%d %ld Tri3\n", i+1, nel[i]);

//               Imprime coordenadas

    nodp = 1;
    rewind(fp);
    fprintf(fw,"*COORDINATES\n %ld\n", nodtot);
    while(fscanf(fp,"%s",buf) != EOF)
	{
	if (strcmp(buf,"POLYLINE")) continue;
	flag = 1;
	while (flag)
	    {
	    fscanf(fp,"%d",&flag);
	    switch (flag)
		{
	    case 70:
		fscanf(fp,"%d",&flagb);
		break;
	    case 0:
		break;
	    default:
		fscanf(fp,"%s",buf);
		}
	    }
//        if (flagb != 64) continue;

	while (1)
	    {
	    if(fscanf(fp,"%s",buf) == EOF) break;
	    if (!strcmp(buf,"SEQEND")) break;
	    if (strcmp(buf,"VERTEX")) break;
	    flag = 1;
	    while(flag)
		{
		fscanf(fp,"%d",&flag);
		switch (flag)
		   {
		case 70:
		   fscanf(fp,"%d",&flagb);
		   break;
		case 10:
		   fscanf(fp,"%lf",&x);
		   break;
		case 20:
		   fscanf(fp,"%lf",&y);
		   break;
		case 30:
		   fscanf(fp,"%lf",&z);
		   break;
		case 0:
		   break;
		default:
		   fscanf(fp,"%s",buf);
		   }
		}
	    if ((flagb & 64) == 64)
			    fprintf(fw,"%ld %lg %lg %lg\n",nodp++,x,y,z);
	    }
	if (strcmp(buf,"SEQEND"))
	    Error(FATAL_ERROR, 3, "DXF_file is corrupted");
	}

  fprintf(fw,"*END\n");
  fclose(fw);
  fw = fopen("dxf.vwm","r");
  Read(fw);
  fclose(fw);

  TraceOff(nome);
}

void vtkMesh3d::Copy(vtkMesh3d *malha)
{
	// Copia coordenadas
	this->Coords = new acCoordinate3D;  
	this->Coords->Copy(malha->Coords);
	
	// Copia Grupos
	this->ElGrps = new ElGroups();
	this->ElGrps->Copy(malha->ElGrps);
}

// Used when a surface is already opened by Paraview
// In this case, there is only one group
void vtkMesh3d::Update(vtkUnstructuredGrid *grid)
{
	// Seta coordenadas dos pontos do MeshData a partir de um UnstructuredGrid
	if(Coords) delete Coords;
	Coords = new acCoordinate3D();	
	Coords->Read(grid);
	
	// Seta conectividade das faces do MeshData
	if (ElGrps) delete ElGrps;	
  ElGrps = new ElGroups();	
	ElGrps->Read(grid, "Tri3");		
}

// Used in Qopt to update vtkMesh3d with volumetric information
void vtkMesh3d::UpdateVolume(  vtkUnstructuredGrid *grid)
{
	if(Coords) delete Coords;
	Coords = new acCoordinate3D();	
	Coords->Read(grid);
	
	if (ElGrps) delete ElGrps;	
  ElGrps = new ElGroups();	
	ElGrps->Read(grid, "LINEAR_TETRAHEDRUM");		
}

// Update mesh information from a vtkHMSurface with group information
void vtkMesh3d::Update(vtkMeshData *surface)
{
	// Seta coordenadas dos pontos já lidos e armazenados em um MeshData
	if(Coords) delete Coords;	
	Coords = new acCoordinate3D();	
//	Coords->Read(surface);
	Coords->Copy(surface->GetSurface()->GetCoords());

	// Seta conectividade das faces (OK)
	if (ElGrps) delete ElGrps;	  
  ElGrps = new ElGroups();	

	// Prepara os parâmetros que serão passados para o ElGrps		
	vtkSurfaceGen *surf = surface->GetSurface();
	
	// Atualiza número de elementos do surfacegen
	surf->UpdateNumEls();

	// Obtem número de grupos da superfície de entrada
	int numeroGrupos = surf->GetNumGroups();
	
	// Aloca vetor com o número de elementos de cada grupo e o preenche a seguir
	int *elementosPorGrupo = (int *) malloc(numeroGrupos * sizeof(int));
	for(int i = 0; i < numeroGrupos; i++)
		elementosPorGrupo[i] = surf->GetNumElsGroups(i);
	
	// Aloca vetor de incidência e o preenche a seguir
	int tam = surf->GetIncidenceVectorSize();
	long *externIncid = (long *) malloc(sizeof(long) * tam); //new long(tam);	
	surf->CopyIncidenceVector(externIncid);

	ElGrps->Read(numeroGrupos, elementosPorGrupo, externIncid, "Tri3");

	// Desaloca estruturas auxiliares
	free(elementosPorGrupo);
	free(externIncid);
}


// Update mesh information from a vtkSurfaceGen object with group information
void vtkMesh3d::Update(vtkSurfaceGen *surf)
{
	// Seta coordenadas dos pontos já lidos e armazenados em um MeshData
	if(Coords) delete Coords;	
	Coords = new acCoordinate3D();	
//	Coords->Read(surface);
	Coords->Copy(surf->GetCoords());

	// Seta conectividade das faces (OK)
	if (ElGrps) delete ElGrps;	  
  ElGrps = new ElGroups();	

	// Atualiza número de elementos do surfacegen
	surf->UpdateNumEls();

	// Obtem número de grupos da superfície de entrada
	int numeroGrupos = surf->GetNumGroups();
	
	// Aloca vetor com o número de elementos de cada grupo e o preenche a seguir
	int *elementosPorGrupo = (int *) malloc(numeroGrupos * sizeof(int));
	for(int i = 0; i < numeroGrupos; i++)
		elementosPorGrupo[i] = surf->GetNumElsGroups(i);
	
	// Aloca vetor de incidência e o preenche a seguir
	int tam = surf->GetIncidenceVectorSize();
	long *externIncid = (long *) malloc(sizeof(long) * tam); //new long(tam);	
	surf->CopyIncidenceVector(externIncid);

	ElGrps->Read(numeroGrupos, elementosPorGrupo, externIncid, "Tri3");

	// Desaloca estruturas auxiliares
	free(elementosPorGrupo);
	free(externIncid);
}

int vtkMesh3d::Read(FILE *fp)
{
	static char *nome = "vtkMesh3d::Read";
	TraceOn(nome);    
	char buf[80];
	static char *kelem  = "ELEMENT GROUPS";
	
	this->Coords = new acCoordinate3D();  
	
	// Se não tiver nenhum coordenada (arquivo só com volume por exemplo)
	if( !this->Coords->Read(fp) ) return 0;
	
	if (!FindKeyWord(kelem, fp))
	    {//  27, "Keyword |%s| Not Found."
	    GetError(27,buf);
	    Error(FATAL_ERROR, 1, buf, kelem);
	    }

	this->ElGrps = new ElGroups();
	this->ElGrps->Read(fp);
	
	TraceOff(nome);        
	
	return 1;
}

void vtkMesh3d::Print(FILE *fp)
    {
    static char *nome = "vtkMesh3d::Print";

    TraceOn(nome);

    Coords->Print(fp);

    ElGrps->Print(fp);

    TraceOn(nome);
    }
    

void vtkMesh3d::Read2d(FILE *fp)
    {
    static char *nome = "PPvtkMesh3d::Read2d";
    char buf[100];
    long dummy, i;
    double ax, ay;
    static char *kcoord = "COORDINATES";
    static char *kelem  = "ELEMENT GROUPS";

    TraceOn(nome);

//   Coords->Read2d(fp);

    long NumNodes;
    if (!FindKeyWord(kcoord, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, kcoord);
        }

/*    if (NumNodes)
        { //   22, "An Object Of Class |%s| Exists And Will Be Eliminated."
        GetError(22, buf);
        Error(WARNING, 2, buf, "acCoordinate3D");
         Free();
        }
*/
    fscanf(fp, "%ld", &NumNodes);
    if (NumNodes < 1)
        { //   20, "Number Of Nodes <= 0."
        GetError(20, buf);
        Error(FATAL_ERROR, 3, buf);
        }
    Coords = new acCoordinate3D(NumNodes);
    acPoint3 vc;
    for (i=0; i<NumNodes; i++)
	{
	fscanf(fp, "%ld %lf %lf", &dummy, &ax, &ay);
	if (dummy != i + 1)
	    { //  23, "Incorrect Data For Node |%ld|."
	    GetError(23, buf);
	    Error(FATAL_ERROR, 4, buf, i + 1);
	    }

	vc.x = ax;
	vc.y = ay;
	vc.z = 0.;
	(*Coords)[i] = vc;
	}

    if (!FindKeyWord(kelem, fp))
	{//  27, "Keyword |%s| Not Found."
	GetError(27,buf);
	Error(FATAL_ERROR, 1, buf, kelem);
	}

    ElGrps = new ElGroups;
    ElGrps->Read(fp);

    TraceOff(nome);
    }

void ReadFortranRecord (int fh, int format, void huge *buf, long sizerec)
    {
    long dummy[2];
    long sizeaux = sizerec;
    char huge *paux = (char huge *)buf;
    if (format == 1)    // MicroSoft unformatted
	{
	while (sizeaux>128)
	    {
	    read (fh, paux, 128);
	    paux += 128;
	    sizeaux -= 128;
	    read (fh, dummy, 2);
	    }
	if (sizeaux)
            read (fh, paux, (int)sizeaux);
	}
    else if (format == 2)       // MicroWay unformatted (NDP fortran)
	{
	while (sizeaux>32760)
	    {
	    read (fh, paux, 32760);
	    paux += 32760;
	    sizeaux -= 32760;
	    }
	if (sizeaux)
            read (fh, paux, (int)sizeaux);
	read (fh, dummy, 4);
	}
    }

// Lectura de archivos fortran unformatted (estilo pituco)
// fh: file handle, format: 1: MicroSoft unformatted, 2: MicroWay unformatted (NDP)
// archivo abierto y rebobinado.

void vtkMesh3d::Read2d(int fh, int format)
    {
    static char *nome = "vtkMesh3d::Read2d";
    long nod, nel, las, i;
    long dummy[2];
    long huge *je;
    float huge *x;
    float huge *y;
    long huge *aux;

    TraceOn(nome);

    if (format == 1)
	read(fh, dummy, 2);
    else if (format == 2)
	read(fh, dummy, 4);
    else
	Error(FATAL_ERROR, 2, "Unrecognized file format");

    read(fh, &nod, 4);
    read(fh, &nel, 4);
    read(fh, &las, 4);
    read(fh, dummy, 4);

    // Se posiciona al comienzo de los datos del siguiente record
    if (format == 1)
	read(fh, dummy, 2);
    else
	read(fh, dummy, 8);

    aux = (long huge *)mMalloc((nel+1)*4);
    ReadFortranRecord(fh, format, aux, (nel+1)*4);      // Reads IE

    mFree (aux);

    if (format == 1)
	read(fh, dummy, 2);
    else if (format == 2)
	read(fh, dummy, 4);

    je = (long huge *)mMalloc(las*4);
    ReadFortranRecord(fh, format, je, las*4);
    ElGrps = new ElGroups;

    ElGrps->SetInci2d(nod, nel, las, je);

    mFree (je);

    Coords = new acCoordinate3D(nod);

    if (format == 1)
	read(fh, dummy, 2);
    else if (format == 2)
	read(fh, dummy, 4);

    x = (float huge *)mMalloc(nod*4);
    ReadFortranRecord(fh, format, x, nod*4);

    if (format == 1)
	read(fh, dummy, 2);
    else if (format == 2)
	read(fh, dummy, 4);

    y = (float huge *)mMalloc(nod*4);
    ReadFortranRecord(fh, format, y, nod*4);

    acPoint3 vc;
    for (i=0; i<nod; i++)
	{
	vc.x = x[i];
	vc.y = y[i];
	vc.z = 0.;
	(*Coords)[i] = vc;
	}
    mFree (x);
    mFree (y);

    TraceOff(nome);
    }

// Lectura de archivos fortran unformatted (estilo pituco)
// fh: file handle, format: 1: MicroSoft unformatted, 2: MicroWay unformatted (NDP)
// archivo abierto y rebobinado.

void vtkMesh3d::Read2d(int fh, int format, long &InfoFile)
    {
    static char *nome = "vtkMesh3d::Read2d";
    long nod, nel, las, i;
    int is3D, doubleprec;
    long dummy[4];
    long huge *ie;
    long huge *je;
    float huge *sx;
    float huge *sy;
    float huge *sz;
    double huge *dx;
    double huge *dy;
    double huge *dz;
//    long huge *aux;
    int skip2or4;

    TraceOn(nome);

    if (format != 1 && format != 2)
	Error(FATAL_ERROR, 2, "Unrecognized file format");

    skip2or4 = (format == 1) ? 2 : 4;

    read(fh, dummy, skip2or4);

    ReadFortranRecord(fh, format, dummy, 16);      // Reads first record
    nod      = dummy[0];
    nel      = dummy[1];
    las      = dummy[2];
    InfoFile = dummy[3];

    is3D       = (int) (InfoFile & 1);
    doubleprec = (int) (InfoFile & 4);

    // Se posiciona al comienzo de los datos del siguiente record
    read(fh, dummy, skip2or4);

    ie = (long huge *)mMalloc((nel+1)*4);
    ReadFortranRecord(fh, format, ie, (nel+1)*4);      // Reads IE

    read(fh, dummy, skip2or4);

    je = (long huge *)mMalloc(las*4);
    ReadFortranRecord(fh, format, je, las*4);
    ElGrps = new ElGroups;

    ElGrps->SetInci2d(nod, nel, las, ie, je, is3D);

    mFree (ie);
    mFree (je);

    Coords = new acCoordinate3D(nod);

    read(fh, dummy, skip2or4);

    acPoint3 vc;
    if (doubleprec)
	{
	dx = (double huge *)mMalloc(nod*8);
	ReadFortranRecord(fh, format, dx, nod*8);
	read(fh, dummy, skip2or4);
	dy = (double huge *)mMalloc(nod*8);
	ReadFortranRecord(fh, format, dy, nod*8);
	if (is3D)
	    {
	    read(fh, dummy, skip2or4);
	    dz = (double huge *)mMalloc(nod*8);
	    ReadFortranRecord(fh, format, dz, nod*8);
	    }
	for (i=0; i<nod; i++)
	    {
	    vc.x = dx[i];
	    vc.y = dy[i];
	    vc.z = is3D ? dz[i] : 0.;
	    (*Coords)[i] = vc;
	    }
	mFree (dx);
	mFree (dy);
	if (is3D) mFree (dz);
	}
    else
	{
	sx = (float huge *)mMalloc(nod*4);
	ReadFortranRecord(fh, format, sx, nod*4);
	read(fh, dummy, skip2or4);
	sy = (float huge *)mMalloc(nod*4);
	ReadFortranRecord(fh, format, sy, nod*4);
	if (is3D)
	    {
	    read(fh, dummy, skip2or4);
	    sz = (float huge *)mMalloc(nod*4);
	    ReadFortranRecord(fh, format, sz, nod*4);
	    }
	for (i=0; i<nod; i++)
	    {
	    vc.x = sx[i];
	    vc.y = sy[i];
	    vc.z = is3D ? sz[i] : 0.;
	    (*Coords)[i] = vc;
	    }
	mFree (sx);
	mFree (sy);
	if (is3D) mFree (sz);
	}

    TraceOff(nome);
    }


void vtkMesh3d::SaveSPI(FILE *fp)
    {
    static char *nome = "vtkMesh3d::SaveSPI";
    long n,ne,j,iel;
    Element *el;
    double h;
    acPoint3 p;

    TraceOn(nome);

    SparseNoEl *noel = new SparseNoEl(ElGrps);

    fprintf(fp, "%ld\n",Coords->GetNumNodes());
    for (n=0; n< Coords->GetNumNodes(); n++)
	{
	h  = 0.0;
	p  = (*Coords)[n];
	ne = noel->GetNelems(n);
	for (j=0; j<ne; j++)
	    {
	    iel = noel->GetElem(n,j);
	    el  = SetAnElem(iel);
	    h   = h + el->Perimeter();
	    }
	if (ne > 0)
	    {
	    h /=(ne*3);
	    fprintf(fp, "%lg %lg %lg %lg\n", p.x,p.y,p.z,h);
	    }
	}
    delete noel;
    TraceOff(nome);
    }


double vtkMesh3d::Volume()
    {
    int i;
    long j;
    Element *el;
    double vol = 0;

    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++)
            {
            SetElData(i, j);
            vol += el->Volume();
            }
        }

    return(vol);
    }

double vtkMesh3d::Volume(int huge *elstatus)
    {
    int i;
    long j, nel;
    Element *el;
    double vol = 0;

    nel = 0;
    for (i = 0; i < ElGrps->GetNumGroups(); i++)
        {
        el = ElGrps->GetElement(i);
        for (j = 0; j < ElGrps->GetNumElsGrp(i); j++, nel++)
            if (elstatus[nel])
                {
                SetElData(i, j);
                vol += el->Volume();
                }
        }

    return(vol);
    }

    
