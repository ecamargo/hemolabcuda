// 15:33:40  11/26/1992
// Raul & Salgado & Marcelo & Enzo

/*
** Define grupos de elementos
*/

#ifndef __MESH3D_H
#define __MESH3D_H

#include <vtkObjectFactory.h>

#include "acdp.h"
#include "coord3d.h"
#include "elgroups.h"
#include "sparse.h"

class vtkPoints;
class vtkCellArray;
class vtkUnstructuredGrid;
class vtkMeshData;
class vtkSurfaceGen;

class VTK_EXPORT vtkMesh3d: public vtkObject
{	
public:
	acCoordinate3D *Coords;
	ElGroups       *ElGrps;

  vtkTypeRevisionMacro(vtkMesh3d,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent){};
  static vtkMesh3d *New();
	// Constructor
	vtkMesh3d();
	// Destructor
	~vtkMesh3d();

	// Description:
	// Copy mesh structure (coords and elgrps) 
	void Copy(vtkMesh3d *);

	// Description:
	// Update mesh information from an UnstructuredGrid. In this case,
	// there is only one group
	void Update(  vtkUnstructuredGrid *grid);

	// Description:
	// This method is used in the PoneTapas (vtkSurfaceGen) method
	// to re-establish the surface structure after adding new groups.
	void Update(vtkSurfaceGen *surface);

	// Description:
	// Update mesh information from an UnstructuredGrid. In this case,
	// there is only one group
	void UpdateVolume(  vtkUnstructuredGrid *grid);

	// Description:
	// Update mesh information from a vtkMeshData with group information
	void Update(  vtkMeshData *);

	// Interface
	int	 Read(FILE *fp);
	void Read2d(FILE *fp);
	void Read2d(int, int);
	void Read2d(int, int, long int&);
	void Print(FILE *fp);
	void DXFIn (FILE *fp);
	void DXFOut(FILE *fp);
	void SaveSPI(FILE *fp);
//	void Restore(char *name, int version, char *dba){};
//	void Save(char *name, int version, char *dba){};

	acCoordinate3D *GetCoords() { return(this->Coords);}
	ElGroups 			 *GetGroups() { return(this->ElGrps);}	
	int  GetNumGroups()         { return(this->ElGrps->GetNumGroups()); }
	long GetNumEls()            { return(this->ElGrps->GetNumEls()); }
	long GetNumNodes()          { return(this->Coords->GetNumNodes()); }

	long GetNumElsGrp(int grp)  { return(this->ElGrps->GetNumElsGrp(grp));}
	char *GetElType(int grp)    { return(this->ElGrps->GetElType(grp));}

	// Funcoes para loop sobre elementos da malha
	Element *SetFirstElem()      {return(this->ElGrps->SetFirstElem(Coords));}
	Element *SetNextElem()       {return(this->ElGrps->SetNextElem(Coords));}
	Element *SetAnElem(long iel) {return(this->ElGrps->SetAnElem(iel,Coords));}

	Element *GetElement(int grp){return(this->ElGrps->GetElement(grp));}
	void SetElData(int grp, long elnum)
	    {
	    ElGrps->SetElData(grp, elnum, *Coords);
	    }

	SparseElElV *NewElElV() { return(new SparseElElV(this->ElGrps)); }
	SparseFaFaV *NewFaFaV() { return(new SparseFaFaV(this->ElGrps)); }
	SparseElNo  *NewElNo()  { return(new SparseElNo(this->ElGrps)); }
	SparseNoEl  *NewNoEl()  { return(new SparseNoEl(this->ElGrps)); }
	double    Volume();
//	double    Volume(int huge *ElStatus);
	double    Volume(int *ElStatus);
	acPoint3  GravityCenter();
//	acPoint3  GravityCenter(int huge *ElStatus);
	acPoint3  GravityCenter(int *ElStatus);
	acLimit3  Limit() {return(this->Coords->Box());}	
  
  double dist2(acPoint3 &p1, acPoint3 &p2);
  int Conviene_123(acPoint3 &p1,acPoint3 &p2,acPoint3 &p3,acPoint3 &p4);    
};

#endif // MESH3D_H
