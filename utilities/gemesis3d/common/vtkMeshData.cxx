/*
 * $Id:$
 */
/*=========================================================================

  Program:   HeMoLab
  Module:    $RCSfile: vtkMeshData.cxx,v $

  Copyright (c) Rodrigo, Jan Palach
  All rights reserved.
  See Copyright.txt or http://hemo01a.lncc.br

=========================================================================*/

#include "vtkMeshData.h"
#include "vtkMesh3d.h"
#include "vtkSurfaceGen.h"
#include "vtkParamMesh.h"
#include "vtkDoubleArray.h"

#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkIdList.h"

vtkCxxRevisionMacro(vtkMeshData, "$Rev: $");
vtkStandardNewMacro(vtkMeshData);

//----------------------------------------------------------------------------
// Class constructor
vtkMeshData::vtkMeshData()
{
	// É necessário iniciar o ACDP para usar os mGétodos de trace do acdp
  StartACDP(0, "trisurf.sai", "trace.txt");		
	this->surf = NULL;
	this->mesh = NULL;	
	this->mpar = NULL;
	this->CoplanarCote=1.0;
	this->ColapseCote=0.1;
	this->ObtuseCote=160.0;	
	this->ParamMeshLoaded = false;

	this->Volume = vtkUnstructuredGrid::New();	
	
	this->groups = vtkDoubleArray::New();
	this->groups->SetName("Groups");		

	this->selectedGroup = vtkDoubleArray::New();
	this->selectedGroup->SetName("SelectedGroup");		
}

//----------------------------------------------------------------------------
// Class destructor
vtkMeshData::~vtkMeshData()
{ 	
	if(this->surf)   this->surf->Delete();	
	if(this->mesh)   this->mesh->Delete();
	if(this->mpar)   this->mpar->Delete();	

	this->groups->Delete();
	this->selectedGroup->Delete();
	
	if(this->Volume) this->Volume->Delete();
}

//----------------------------------------------------------------------------
void vtkMeshData::CopyStructure(vtkMeshData *ds) 
{
	// Atualiza Internal UnstructuredGrid
	this->Superclass::CopyStructure((vtkDataSet*)ds);	
	this->GetPointData()->PassData(ds->GetPointData());
	this->GetCellData()->PassData(ds->GetCellData());
	
	this->SetVolume( ds->GetVolume() );

	ParamMeshLoaded = ds->ParamMeshLoaded;	

	// Testa se 'ds' possui ou nao uma superficie (pode conter apenas o volume)
	if( !ds->GetMesh3d() ) return;

	// Atualiza mesh com superfície de entrada
	if(this->mesh) this->mesh->Delete();
	this->mesh = vtkMesh3d::New();
	this->mesh->Update(ds);

	// Copia mpar da entrada entrada
	if(this->mpar) this->mpar->Delete();
	this->mpar = vtkParamMesh::New();
	this->mpar->Copy(ds->GetParamMesh());	
	
	// Atualiza surfaceGen com mesh processada
	if(this->surf) this->surf->Delete();
	this->surf = vtkSurfaceGen::New();
	this->surf->Copy(this->mesh, this->mpar);
	this->surf->SetCotaCopla(cos(CoplanarCote*3.141592/180.0));
	this->surf->SetCotaColap(ColapseCote); 
	
	// Atualiza numero de grupos da casca
	this->surf->SetNumberOfShells( ds->GetSurface()->GetNumberOfShells() );
}

//----------------------------------------------------------------------------
void vtkMeshData::CopyUnstructuredGridToMeshDataSurface(vtkUnstructuredGrid *grid)
{
	// Atualiza griddata
	this->Superclass::CopyStructure((vtkDataSet*) grid);
	this->GetPointData()->PassData(grid->GetPointData());
	this->GetCellData()->PassData(grid->GetCellData());	

	this->mesh = vtkMesh3d::New();
	this->mpar = vtkParamMesh::New();

	// Carrega griddata na malha
	this->mesh->Update(grid);
	
	// Carrega malha na superfície
	if(this->surf) this->surf->Delete();
	this->surf = vtkSurfaceGen::New();
	this->surf->Copy(this->mesh, this->mpar);
	this->surf->SetCotaCopla(cos(CoplanarCote*3.141592/180.0));
	this->surf->SetCotaColap(ColapseCote);
}

//----------------------------------------------------------------------------
void vtkMeshData::SetSelectedGroup(int gr)
{
	int maxId = selectedGroup->GetMaxId();
	int value;
	for(int i = 0; i <= maxId; i++)
		{
			value = (gr == groups->GetValue(i)) ? 10 : 0;
			selectedGroup->SetValue(i,value);
		}
}

//----------------------------------------------------------------------------
int vtkMeshData::Read(char *filename)
{
	FILE *fp = fopen(filename, "r");
	if (!fp)
	{ 
		vtkErrorMacro(<<"Can't open " << filename << "input file.");		
		return 0;
	}
	
	if(this->mesh) this->mesh->Delete();
	this->mesh = vtkMesh3d::New();

	int success = this->mesh->Read(fp);

	// Check if something was read
	if(!success)
		{	
		this->mpar = NULL;
		this->surf = NULL;
		if(this->mesh) this->mesh->Delete();		
		this->mesh = NULL;
		return 0;
		}
			
	if(this->mpar) this->mpar->Delete();
	this->mpar = vtkParamMesh::New();

	if(this->surf) this->surf->Delete();
	this->surf = vtkSurfaceGen::New();	
	this->surf->Copy(this->mesh, this->mpar);
	this->surf->SetCotaCopla(cos(CoplanarCote*3.141592/180.0));
	this->surf->SetCotaColap(ColapseCote);
	
	// Check number of shell groups
	char *shellHint = "SHELL GROUPS";
	int shells;
  if (FindKeyWord(shellHint, fp))
  	{
  	fscanf(fp, "%d", &shells);
  	this->surf->SetNumberOfShells(shells);
  	}
  else
  	this->surf->SetNumberOfShells(1); // Default value  	
	
	fclose(fp);	

	return 1;		
}

//----------------------------------------------------------------------------
void vtkMeshData::UpdateGroupProperties(int type)
{
	if(type == 0)	// Update internal UnstructuredGrid properties with surf. information
		this->surf->SetGroupInformation(this->groups, this->selectedGroup);
	else					// Update internal UnstructuredGrid properties with volume information
	{
		this->selectedGroup->Initialize();	
		this->groups->Initialize();	
		
		for(int i = 0; i < this->GetNumberOfCells(); i++)
		{
			this->selectedGroup->InsertNextValue(0);			
			this->groups->InsertNextValue(0);					
		}
	}
	// Atribui e ativa propriedade
	this->GetCellData()->AddArray( this->groups);	
	this->GetCellData()->AddArray( this->selectedGroup );			
}

//----------------------------------------------------------------------------
void vtkMeshData::SetModifiedCells()
{				
	int maxId = selectedGroup->GetMaxId();
	int numCells = this->surf->GetNumElsTotal();

	if(maxId+1 != numCells)
		{
		cout << "void vtkMeshData::SetModifiedCells() : Error: Invalid Number of Cells" << endl;
		return;
		}	
	
	// Aloca vetor e popula com 1 se o flag 'isNew' dos 
	// triângulos estiver ligado ou 0 caso contrário
	int *modifiedVec = new int[numCells];	
	this->surf->GetModifiedCells(modifiedVec);	

	for(int i = 0; i <= maxId; i++)
			selectedGroup->SetValue(i, (modifiedVec[i]) ? 10 : 0);
		
	delete[] modifiedVec;
}

//----------------------------------------------------------------------------
// Update internal UnstructuredGrid with surf. information
void vtkMeshData::SetOutputAsSurface()
{
	if(this->surf)
		this->surf->UpdateUnstructuredGrid((vtkUnstructuredGrid*) this);
}

//----------------------------------------------------------------------------
// Update internal UnstructuredGrid with volume information
void vtkMeshData::SetOutputAsVolume()
{	
	this->Superclass::CopyStructure( this->GetVolume() );		
	this->UpdateGroupProperties(1);	
}

//----------------------------------------------------------------------------
int vtkMeshData::SetSurfaceParFile(char* name)
{	
  FILE *fp = fopen(name, "r");
  if (!fp)
    vtkErrorMacro("Unable to open file " << name);
  
  if(!this->mpar) this->mpar = vtkParamMesh::New();
  this->mpar->Read(fp);
  fclose(fp);
  this->mpar->BuildOctree();
  
  if (this->surf) this->surf->SetPar(this->mpar);
  
  this->ParamMeshLoaded = true;
  
	return 0;
}

//----------------------------------------------------------------------------
void vtkMeshData::SetLoadedParameter(vtkParamMesh* par)
{
  if(this->surf) this->surf->SetPar(par);
  this->ParamMeshLoaded = true;   
}

//----------------------------------------------------------------------------
vtkParamMesh* vtkMeshData::GetLoadedParameter()
{
	return this->mpar;
}


//----------------------------------------------------------------------------
void vtkMeshData::SetVolume(vtkUnstructuredGrid *vol)
{
	this->Volume->CopyStructure(vol);
}

//----------------------------------------------------------------------------
vtkUnstructuredGrid* vtkMeshData::GetVolume()
{
	return this->Volume;
}

//----------------------------------------------------------------------------
int vtkMeshData::CheckOrientation()
{
	if(this->surf && this->mesh)
		return this->surf->CheckOrientation(this->mesh, this->mpar);
	return -1;
}
//----------------------------------------------------------------------------
// PrintSelf Method
void vtkMeshData::PrintSelf(ostream& os, vtkIndent indent)
{
//	this->Superclass::PrintSelf(os,indent);
	long nodes = 0,nelems = 0,groups = 0;
  printf(" Surface Information:");
  vtkSurfaceGen *surf = this->GetSurface();
  if(surf) surf->Information(&nodes,&nelems,&groups);
  printf("\n   Nodes     : %ld"
         "\n   Elements  : %ld"
         "\n   Groups    : %ld",nodes,nelems,groups);	
  printf("\n Volume Information:");  
  printf("\n   Nodes     : %ld"
         "\n   Cells     : %ld\n",this->GetVolume()->GetNumberOfPoints(), 
         													this->GetVolume()->GetNumberOfCells());
}
