/*
 * $Id: vtkMeshData.h 365 2006-05-16 17:19:03Z diego $
 */
/*=========================================================================

  Project:   HeMoLab
  Module:    $RCSfile: vtkMeshData.cpp,v $
  Author: 	 Rodrigo L. S. Silva, Jan Palach

=========================================================================*/ 
// .NAME vtkMeshData
// .SECTION Description
// vtkMeshData é uma representação genêrica para arquivos de Superfície
// e de volume. A classe possui dois objetos vtkSurfaceGen, sendo um
// para superfícies e outro para volumes.

#ifndef _vtkMeshData_
#define _vtkMeshData_

#include "vtkUnstructuredGrid.h"

class vtkMesh3d;
class vtkSurfaceGen;
class vtkParamMesh;
class vtkDoubleArray;
class vtkUnstructuredGrid;

class VTK_EXPORT vtkMeshData : public vtkUnstructuredGrid
{
public:
	static vtkMeshData *New();
	vtkTypeRevisionMacro(vtkMeshData, vtkUnstructuredGrid);
	void PrintSelf(ostream& os, vtkIndent indent);
	
	// Description:
	// Copy de input surface completely (SurfaceGen + UnstructuredGrid)
	void CopyStructure(vtkMeshData *ds);

	// Description:
	// Copy UnstructuredGrid to internal vtkSurfaceGen		
	void CopyUnstructuredGridToMeshDataSurface(vtkUnstructuredGrid *);

	// Description:
	// Fill mesh and surf information from a file
	int Read(char *file);

	// Description:
	// Update the internal vtkUnstructuredGrid with surface information
	void SetOutputAsSurface();

	// Description:
	// Update the internal vtkUnstructuredGrid with volume information
	void SetOutputAsVolume();

	// Description:
	// Load Parameter file by its name
	int SetSurfaceParFile(char *);
	
	// Description:
	// If an parameter file was already loaded, update the surface with it
	void SetLoadedParameter(vtkParamMesh*);
	vtkParamMesh* GetLoadedParameter();	

	// Description:
	// Set/Get ColapseCote atribute
	vtkSetMacro(ColapseCote,double);
	vtkGetMacro(ColapseCote,double);

	// Description:
	// Retorna SurfaceGen, Mesh3d e ParamMesh
	vtkSurfaceGen *GetSurface()		{ return this->surf;};
	void SetSurface(vtkSurfaceGen *surf){this->surf = surf;};
	vtkMesh3d 		*GetMesh3d() 	  { return this->mesh;};
	void SetMesh3d(vtkMesh3d *mesh){this->mesh = mesh;};
	vtkParamMesh  *GetParamMesh() { return this->mpar;};
	void SetParamMesh(vtkParamMesh *mpar){this->mpar = mpar;};
	bool IsParamMeshLoaded() {return this->ParamMeshLoaded;};

	// Description:
	// Check normals orientation
	// Returns 	0: if they are pointed out
	//				1: if they are pointed in
	//				2: if surface is closed and has just one group (can't compute)
	int CheckOrientation();

	// Description:
	// Set/Get ParamMeshLoaded
	vtkSetMacro(ParamMeshLoaded,bool);
	vtkGetMacro(ParamMeshLoaded,bool);

	// Description:
	// Set Group Property Information
	// Type: 	0 -> Surface
	//				1 -> Volume
	void UpdateGroupProperties(int type);

	// Description:
	// Set modified Cells Elements (testing...)
	void SetModifiedCells();

	// Description:
	// Set Selected Group
	void SetSelectedGroup(int );

	// Description:
	// Set/Get the MeshData Volume
	void SetVolume(vtkUnstructuredGrid  *);
	vtkUnstructuredGrid* GetVolume();	
	 
protected:
	vtkMeshData();
	virtual ~vtkMeshData();
  
	// Description
	// MeshData Surface (and auxiliary objects)
  vtkSurfaceGen	*surf;
  vtkMesh3d    	*mesh;
  vtkParamMesh    *mpar; 
  double ColapseCote;
  double CoplanarCote;
  double ObtuseCote;
  char 	filename[512];	

	// Description
	// MeshData Volume
	vtkUnstructuredGrid *Volume;
  
  // Stores surfacegen group information for surfaces
	vtkDoubleArray *groups;  

  // Stores selected Group to be highlighted
	vtkDoubleArray *selectedGroup;  

	// Check if a ParamMesh File was loaded
	bool ParamMeshLoaded;	

private:
	vtkMeshData(const vtkMeshData&); 		// Not Implemented
	void operator=(const vtkMeshData&);	// Not Implemented
};

#endif // _vtkMeshData_
