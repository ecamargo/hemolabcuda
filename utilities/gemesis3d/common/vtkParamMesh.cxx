/*
** Raul, Enzo, Marcelo, Fancello & Salgado
*/

#include "acdp.h"
#include "vtkParamMesh.h"
#include "vtkMesh3d.h"
#include "elgroups.h"

#define NUMNODMAX 2

vtkCxxRevisionMacro(vtkParamMesh, "$Rev: 635 $");
vtkStandardNewMacro(vtkParamMesh);

//int VolIntersec (acLimit3 &Vola, acLimit3 &Volb);

vtkParamMesh::vtkParamMesh()
{
	this->H      = NULL;
	this->Octree = NULL;
	this->Coords = NULL;
	this->ElGrps = NULL;
}

vtkParamMesh::~vtkParamMesh()
{
	if(this->H     ) this->H = NULL;//free(this->H);
	if(this->Octree) this->Octree = NULL;//free(this->Octree);
	if(this->Coords) this->Coords = NULL;//delete this->Coords;
	if(this->ElGrps) this->ElGrps = NULL;//delete this->ElGrps;
}

// O primeiro parâmetro representa a bounding box do objeto de entrada
// tendo a seguinte ordem [xmin, xmax, ymin, ymax, zmin, zmax]
void vtkParamMesh::ComputeParameters(double *b, double edgesSize)
{
	// Aloca inicialmente objeto
  this->Coords = new acCoordinate3D();	
  
 	// A princípio serão sempre 9 coordenadas, como no arquivo SPI
 	this->Coords->Allocate(9);
 	
 	// Cria coordenadas a partir da bounding box
 	this->Coords->SetNode(0, b[0], b[2], b[4]);
 	this->Coords->SetNode(1, b[0], b[3], b[4]);
 	this->Coords->SetNode(2, b[1], b[3], b[4]);
 	this->Coords->SetNode(3, b[1], b[2], b[4]);
 	this->Coords->SetNode(4, b[0], b[2], b[5]);
 	this->Coords->SetNode(5, b[0], b[3], b[5]);
 	this->Coords->SetNode(6, b[1], b[3], b[5]);
 	this->Coords->SetNode(7, b[1], b[2], b[5]);
 	this->Coords->SetNode(8, (b[0]+b[1])/2, (b[2]+b[3])/2, (b[4]+b[5])/2); 
 	// No arquivo SPI as coordenadas deste último ponto eram 0.0
 	 	 	 	 	 	 	 	
  this->Volume = Coords->Box(); 	

	long int externIncid[] =
	{ 1, 3, 2, 9,	 	1, 4, 3, 9,		3, 4, 8, 9,
	  7, 3, 8, 9,		5, 6, 7, 9,		7, 8, 5, 9,
	  2, 5, 1, 9,		2, 6, 5, 9,		5, 8, 4, 9,
	  5, 4, 1, 9,   2, 3, 7, 9,   2, 7, 6, 9};

	// Corrige a lista de incidência original
	for(int i = 0; i < 48; i++)
		externIncid[i]--;		

	// Cria os grupos
  this->ElGrps = new ElGroups();
  int numElemsGroups = 12;
  this->ElGrps->Read(1, &numElemsGroups, externIncid, "Tetra4");

	// Populando variável H
  long nnod = Coords->GetNumNodes();
  H = (double *) mMalloc (nnod * sizeof(double));
  for (long i = 0; i < nnod; i++)
    H[i] = edgesSize;
}

void vtkParamMesh::Read(FILE *fp)
{
    static char *nome = "vtkParamMesh::Read";
    static char *htit = "EDGES_SIZE";
    static char *kcoord = "COORDINATES";
    static char *kelem  = "ELEMENT GROUPS";
    char buf[80];

    TraceOn(nome);

    if (!FindKeyWord(kcoord, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, kcoord);
        }
    Coords = new acCoordinate3D;
    Coords->Read(fp);
    Volume = Coords->Box();

    if (!FindKeyWord(kelem, fp))
        {
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, kelem);
        }

    ElGrps = new ElGroups;
    ElGrps->Read(fp);

    float h;

    if (!FindKeyWord(htit, fp))
        {//  27, "Keyword |%s| Not Found."
        GetError(27,buf);
        Error(FATAL_ERROR, 1, buf, htit);
        }

    long nnod = Coords->GetNumNodes();
    H = (double *) mMalloc (nnod * sizeof(double));
    for (long i = 0; i < nnod; i++)
        {
        fscanf (fp,"%f", &h);
        H[i] = h;
        }

    TraceOff(nome);
}


void vtkParamMesh::Copy(vtkParamMesh *input)
{
  if(input->GetCoords())
	  {
	  Coords = new acCoordinate3D;
	  Coords->Copy(input->GetCoords());
	  Volume = Coords->Box();
	  long nnod = Coords->GetNumNodes();
	  H = (double *) mMalloc (nnod * sizeof(double));
	  for (long i = 0; i < nnod; i++)
	    {
	    H[i] = input->GetH(i);
	    }	  
  	}
  	
  if(input->GetGroups())
  {
	  ElGrps = new ElGroups;
	  ElGrps->Copy(input->GetGroups());
  }
}



void vtkParamMesh::BuildOctree()
{
  acPoint3 cg, tc;
  register int i;
  static char *nome = "vtkParamMesh::BuildOctree";

  TraceOn(nome);

  /*
  ** calcula e fixa dimensoes da window da malha de parametros
  */
  tc.x = Volume.xmax - Volume.xmin;
  tc.y = Volume.ymax - Volume.ymin;
  tc.z = Volume.zmax - Volume.zmin;
  cg.x = Volume.xmin + tc.x * 0.5;
  cg.y = Volume.ymin + tc.y * 0.5;
  cg.z = Volume.zmin + tc.z * 0.5;

  /*
  ** cria primeiro no' da arvore com o no' 1
  */
  Octree = CreNodeOct(cg, tc);
  st_nodeclist nol;
  nol.numelem = 0;
  Octree->l.InsertFirst(&nol);

  /*
  ** varre os nos restantes
  */
  for (i = 1; i < Coords->GetNumNodes(); i++)
      InsNodeOct(i);

  /*
  ** monta lista no ramos finais da arvore
  */
  LiberaLista(Octree);
  MontaLista();

  TraceOff(nome);
}

void vtkParamMesh::LiberaLista(MP_Octree *oct)
    {
    register int i;
    if (oct->ptr[0])
        for (i = 0; i < 8; i++)
            LiberaLista(oct->ptr[i]);
    else
        oct->l.Clear();
    }

MP_Octree *vtkParamMesh::CreNodeOct(acPoint3 &cg, acPoint3 &tc)
    {
    MP_Octree *temp;

    /*
    ** cria o no'
    */
    temp = (MP_Octree *) mMalloc(sizeof(MP_Octree));

    /*
    ** coloca os elementos e inicializa o resto com NULL
    */
    temp->cg = cg;
    temp->tc = tc;

    for (int i = 0; i < 8; i++)
        temp->ptr[i] = NULL;

    temp->l.SetListData(sizeof(st_nodeclist));
    return(temp);
    }

void vtkParamMesh::InsNodeOct(long no)
    {
    acPoint3 point, pointp;
    register int oct;
    MP_Octree *temp, *tree;
    static char *nome = "vtkParamMesh::InsNodeOct";
    acCoordinate3D &Coord = *Coords;

    TraceOn(nome);

    tree = Octree;
    point = Coord[no];

    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    while (tree->l.GetSize() > NUMNODMAX) // Mientras la hoja tenga mas de NUMNODMAX nodos
        {
        for (int i = 0; i < 8; i++)
            {
            acPoint3 cgf, tcf;
            cgf = tree->cg;
            tcf = tree->tc;
            CalcNewMeio(cgf, tcf, i);
            tree->ptr[i] = CreNodeOct(cgf, tcf);
            }

        st_nodeclist *nol;
        while (nol=(st_nodeclist *)tree->l.GetFirst())
            {
            pointp = Coord[nol->numelem];
            oct      = FindOctante(pointp, tree->cg);
            temp     = tree->ptr[oct];
            temp->l.InsertFirst(nol);
            tree->l.DelFirst();
//            temp->no = tree->no;
            }
        oct      = FindOctante(point, tree->cg);
        tree     = tree->ptr[oct];
        }
    st_nodeclist nnol;
    nnol.numelem=no;
    tree->l.InsertFirst(&nnol);
//    tree->no = no;

    TraceOff(nome);
    }

void vtkParamMesh::MontaLista()
    {
    static char *nome = "vtkParamMesh::MontaLista";
    acLimit3 volt;
    acPoint3 point1, point2;
    long elem;
    Element *el;

    TraceOn(nome);

    /*
    ** loop sobre cada elemento da malha de parametros
    */
//    for (el = SetFirstElem(), elem=0; el; el = SetNextElem(),elem++)
/*
 * MUDEI AQUI POIS OS METODOS SETFIRST... E SETNEXT... ESTAVAM SENDO CHAMADOS SEM 
 * NENHUM OBJETO ASSOCIADO COMO ACIMA.
 */
  
			for (el = ElGrps->SetFirstElem(), elem=0; el; el = ElGrps->SetNextElem(),elem++)
        {
        point1 = (*Coords)[el->GetNode(0)];
        point2 = (*Coords)[el->GetNode(1)];
        if (point1.x < point2.x)
            { volt.xmin = point1.x; volt.xmax = point2.x; }
        else
            { volt.xmin = point2.x; volt.xmax = point1.x; }
        if (point1.y < point2.y)
            { volt.ymin = point1.y; volt.ymax = point2.y; }
        else
            { volt.ymin = point2.y; volt.ymax = point1.y; }
        if (point1.z < point2.z)
            { volt.zmin = point1.z; volt.zmax = point2.z; }
        else
            { volt.zmin = point2.z; volt.zmax = point1.z; }
        point1 = (*Coords)[el->GetNode(2)];
        point2 = (*Coords)[el->GetNode(3)];
        if (point1.x < point2.x)
            {
            if (point1.x < volt.xmin) volt.xmin = point1.x;
            if (point2.x > volt.xmax) volt.xmax = point2.x;
            }
        else
            {
            if (point2.x < volt.xmin) volt.xmin = point2.x;
            if (point1.x > volt.xmax) volt.xmax = point1.x;
            }
        if (point1.y < point2.y)
            {
            if (point1.y < volt.ymin) volt.ymin = point1.y;
            if (point2.y > volt.ymax) volt.ymax = point2.y;
            }
        else
            {
            if (point2.y < volt.ymin) volt.ymin = point2.y;
            if (point1.y > volt.ymax) volt.ymax = point1.y;
            }
        if (point1.z < point2.z)
            {
            if (point1.z < volt.zmin) volt.zmin = point1.z;
            if (point2.z > volt.zmax) volt.zmax = point2.z;
            }
        else
            {
            if (point2.z < volt.zmin) volt.zmin = point2.z;
            if (point1.z > volt.zmax) volt.zmax = point1.z;
            }

//        volt = el->Box();
        MListOct(Octree, volt, elem);
        }
    TraceOff(nome);
    }

int VolIntersec (acLimit3 &Vola, acLimit3 &Volb)
    {
    if ((Vola.xmin <= Volb.xmax && Vola.xmax >= Volb.xmin) &&
        (Vola.ymin <= Volb.ymax && Vola.ymax >= Volb.ymin) &&
        (Vola.zmin <= Volb.zmax && Vola.zmax >= Volb.zmin))
            return (1);
    return (0);
    }

void vtkParamMesh::MListOct(MP_Octree *oct, acLimit3 &volt, long elem)
    {
    acLimit3 voloct;
    st_nodeclist nol;
    register int i;

    CalcWinOctante (voloct, oct->cg, oct->tc);

    if (VolIntersec(voloct, volt))
        {
        if (oct->ptr[0] == NULL)
            {
            nol.numelem = elem;
            oct->l.InsertFirst(&nol);
            }
        else
            for (i = 0; i < 8; i++)
                MListOct(oct->ptr[i], volt, elem);
        }
    }

void vtkParamMesh::PrintOctree(FILE *fp)
    {
    static char *nome = "vtkParamMesh::PrintOctree";
    TraceOn(nome);
    PrintBranch(fp, Octree);
    TraceOff(nome);
    }

void vtkParamMesh::PrintBranch(FILE *fp, MP_Octree *oct)
    {
    register int i;

    if (oct->ptr[0])
        for (i = 0; i < 8; i++)
            PrintBranch(fp, oct->ptr[i]);
    else
        {
        acNodeList *pl;
        st_nodeclist *nol;
        pl = oct->l.GetHead();
//        fprintf (fp,"n: %ld\n",oct->no);
        while (pl)
           {
           nol = (st_nodeclist *) pl->GetDados();
           fprintf (fp,"%ld ",nol->numelem);
           pl = pl->next;
           }
        fprintf (fp,"\n");
        }
    }

void vtkParamMesh::CalcWinOctante (acLimit3 &pl, acPoint3 &cg, acPoint3 &tc)
    {
    pl.xmin = cg.x - tc.x * 0.5;
    pl.xmax = cg.x + tc.x * 0.5;
    pl.ymin = cg.y - tc.y * 0.5;
    pl.ymax = cg.y + tc.y * 0.5;
    pl.zmin = cg.z - tc.z * 0.5;
    pl.zmax = cg.z + tc.z * 0.5;
    }

/*
float vtkParamMesh::GetH(acPoint3 &point)
    {
    static char buf[81];
    acCoordinate3D &Coord = *Coords;
    int oct;
    MP_Octree huge *tree;
    tree = Octree;

    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    acNodeList huge *pl;
    float ct1, ct2, ct3, ct4;
    long n1,n2,n3,n4;
    float x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    acPoint3 v;
    Element *el = ElGrps->GetElement(0);
    struct st_nodeclist huge *nol;
    pl = tree->l.GetHead();

    while (pl)
       {
       nol = (struct st_nodeclist *) pl->GetDados();
       ElGrps->SetElData(0, nol->numelem);
       n1 = el->GetNode(0);
       n2 = el->GetNode(1);
       n3 = el->GetNode(2);
       n4 = el->GetNode(3);
       v = Coord[n1];
       x1 = v.x - point.x; y1 = v.y - point.y; z1 = v.z - point.z;
       v = Coord[n2];
       x2 = v.x - point.x; y2 = v.y - point.y; z2 = v.z - point.z;
       v = Coord[n3];
       x3 = v.x - point.x; y3 = v.y - point.y; z3 = v.z - point.z;
       ct4 = - x1*(y2*z3-y3*z2) + y1*(x2*z3-x3*z2) - z1*(x2*y3-x3*y2);
       if (ct4 >= 0.0)
         {
         v = Coord[n4];
         x4 = v.x - point.x; y4 = v.y - point.y; z4 = v.z - point.z;
         ct1 = - x2*(y4*z3-y3*z4) + y2*(x4*z3-x3*z4) - z2*(x4*y3-x3*y4);
         if (ct1 >= 0.0)
           {
           ct2 = - x4*(y1*z3-y3*z1) + y4*(x1*z3-x3*z1) - z4*(x1*y3-x3*y1);
           if (ct2 >= 0.0)
             {
             ct3 = - x4*(y2*z1-y1*z2) + y4*(x2*z1-x1*z2) - z4*(x2*y1-x1*y2);
             if (ct3 >= 0.0)
                 {
                 return ( (ct1*H[n1]+ct2*H[n2]+ct3*H[n3]+ct4*H[n4]) /
                          (ct1+ct2+ct3+ct4) );
                 }
             }
           }
         }
       pl = pl->next;
       }

    sprintf(buf, "Point (%lf, %lf, %lf) outside mesh",
	point.x, point.y, point.z);
    Error(FATAL_ERROR, 1, buf);
    return(0.0);
    }
*/

double vtkParamMesh::GetH(acPoint3 &point)
    {
    static char buf[81];
    acCoordinate3D &Coord = *Coords;
    int oct;
    MP_Octree *tree;
    tree = Octree;
    
    while (tree->ptr[0])      // Mientras tree sea rama
        {
        oct = FindOctante(point, tree->cg);
        tree = tree->ptr[oct];
        }

    acNodeList *pl;
    double ct1, ct2, ct3, ct4;
    long n1,n2,n3,n4;
    double x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    long nn1, nn2, nn3, nn4, naux;
    int signo;
    acPoint3 v;
    Element *el = ElGrps->GetElement(0);
    st_nodeclist *nol;
    pl = tree->l.GetHead();

    while (pl)
       {
       nol = (st_nodeclist *) pl->GetDados();
       ElGrps->SetElData(0, nol->numelem);
       n1 = el->GetNode(0);
       n2 = el->GetNode(1);
       n3 = el->GetNode(2);
       n4 = el->GetNode(3);

	signo = 1; nn1 = n1; nn2 = n2; nn3 = n3, nn4 = n4;
// Ordena nn1, nn2, nn3, nn4 tal que nn1 < nn2 < nn3 < nn4
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }
	if (nn2 > nn3) { naux = nn2; nn2 = nn3; nn3 = naux; signo *= -1; }
	if (nn3 > nn4) { naux = nn3; nn3 = nn4; nn4 = naux; signo *= -1; }
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }
	if (nn2 > nn3) { naux = nn2; nn2 = nn3; nn3 = naux; signo *= -1; }
	if (nn1 > nn2) { naux = nn1; nn1 = nn2; nn2 = naux; signo *= -1; }

// Cara nn1, nn2, nn3
       v = Coord[nn1];
       x1 = v.x - point.x; y1 = v.y - point.y; z1 = v.z - point.z;
       v = Coord[nn2];
       x2 = v.x - point.x; y2 = v.y - point.y; z2 = v.z - point.z;
       v = Coord[nn3];
       x3 = v.x - point.x; y3 = v.y - point.y; z3 = v.z - point.z;
       ct4 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
	ct4 *= -1; ct4 *= signo;
       if (ct4 >= 0.0)
         {
// Cara nn1, nn2, nn4
         v = Coord[nn4];
         x4 = v.x - point.x; y4 = v.y - point.y; z4 = v.z - point.z;
         ct3 = x1*(y2*z4-z2*y4) + y1*(z2*x4-x2*z4) + z1*(x2*y4-y2*x4);
	  ct3 *= signo;
         if (ct3 >= 0.0)
           {
// Cara nn1, nn3, nn4
           ct2 = x1*(y3*z4-z3*y4) + y1*(z3*x4-x3*z4) + z1*(x3*y4-y3*x4);
	    ct2 *= -1; ct2 *=signo;
           if (ct2 >= 0.0)
             {
// Cara nn2, nn3, nn4
             ct1 = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
	      ct1 *= signo;
             if (ct1 >= 0.0)
                 {
                 return ( (ct1*H[nn1]+ct2*H[nn2]+ct3*H[nn3]+ct4*H[nn4]) /
                          (ct1+ct2+ct3+ct4) );
                 }
             }
           }
         }
       pl = pl->next;
       }
    v = Coord[0];
    long noadop = 0;
    double dist;
    double dmin=  (v.x-point.x)*(v.x-point.x)
                + (v.y-point.y)*(v.y-point.y)
                + (v.z-point.z)*(v.z-point.z);

		/*
		 * 
		 * 
		 *  Início do bloco que estava faltando               
		 * 
		 * 
		 */
    pl = tree->l.GetHead();
    while (pl)
       {
       nol = (st_nodeclist *) pl->GetDados();
       ElGrps->SetElData(0, nol->numelem);
       for (int in = 0; in<4; in++)
           {
           n1 = el->GetNode(in);
           v = Coord[n1];
           dist =    (v.x-point.x)*(v.x-point.x)
                   + (v.y-point.y)*(v.y-point.y)
                   + (v.z-point.z)*(v.z-point.z);
           if(dist < dmin)
              {
              noadop = n1;
              dmin = dist;
              }
          }
       pl = pl->next;
       }
    if (tree->l.GetHead()) return(H[noadop]);
		/* 
		 * 
		 * 
		 * Fim de Bloco
		 * 
		 */                
                
    for (long no = 1; no<Coord.GetNumNodes();no++)
        {
        v = Coord[no];
        dist =    (v.x-point.x)*(v.x-point.x)
                + (v.y-point.y)*(v.y-point.y)
                + (v.z-point.z)*(v.z-point.z);
        if(dist < dmin)
           {
           noadop = no;
           dmin = dist;
           }
        }
    sprintf(buf, "Point (%lf, %lf, %lf) outside mesh -> Adopted %ld",
	        point.x, point.y, point.z, noadop+1);
//    Error(WARNING, 1, buf);
    return(H[noadop]);
    }

void vtkParamMesh::SetH (void)
    {
    long nnod = Coords->GetNumNodes();
    long i;
    acPoint3 point;
    acCoordinate3D &Coord = *Coords;

    for (i = 0; i < nnod; i++)
        {
        point = Coord[i];
        H[i] = point.x + 2.0 * point.y + 0.5 * point.z;
        }
    }
