/*
** Raul, Enzo, Marcelo, Fancello & Salgado
*/

/*
** Objeto malha de parametros
*/

#ifndef PARAMMESH_H
#define PARAMMESH_H

#include <vtkObjectFactory.h>

#include "acdp.h"
#include "aclist.h"
#include "vtkMesh3d.h"
#include "elgroups.h"
#include "nodlist.h"

class VTK_EXPORT vtkParamMesh: public vtkObject
{
	protected:
		double  *H;           // vetor com os 'h' de cada no'
		MP_Octree *Octree;  // raiz da octree
		acLimit3 Volume;     // coords. dos maxs. e mins.
		
		acCoordinate3D *Coords;     // ESTAVA FALTANDO
		ElGroups *ElGrps;				    // ESTAVA FALTANDO    

  public:
	  vtkTypeRevisionMacro(vtkParamMesh,vtkObject);
	  void PrintSelf(ostream& os, vtkIndent indent){};
		static vtkParamMesh *New();       
    vtkParamMesh();
		~vtkParamMesh();    
    void Read(FILE *fp);
    void BuildOctree();
    void SetH();
    double GetH(acPoint3 &point);
    double GetH(long node) { return(H[node]); }
    void  PrintOctree(FILE *lw);

		// New Methods		
		// Description:
		// Process SPI coords instead of read it from a file
		void ComputeParameters(double *bounds, double edgesSize);		

		// Description:
		// Copy parameters from an external vtkParamMesh object
		void  Copy(vtkParamMesh *input);

		// Description:
		// Get vtkParamMesh Coords
		acCoordinate3D *GetCoords() {return this->Coords; };

		// Description:
		// Get vtkParamMesh Groups
		ElGroups       *GetGroups() {return this->ElGrps; };		
				
	private:
		void CalcWinTetra(acLimit3 &t, long *pincid);
    MP_Octree *CreNodeOct(acPoint3 &cg, acPoint3 &tc);
    void InsNodeOct(long no);
    void LiberaLista(MP_Octree *oct);
    void MontaLista();
    void MListOct(MP_Octree *oct, acLimit3 &volt, long elem);
    acList *FindList(acPoint3 &p);
    void CalcWinOctante(acLimit3 &voloct, acPoint3 &cg, acPoint3 &tc);
    void PrintBranch(FILE *lw, MP_Octree *oct);

    /*
    ** funcoes inline
    **
    ** retorna o octante do ponto 'p' em relacao ao ponto cg
    */
		int FindOctante(acPoint3 &p, acPoint3 &cg)
	    {
	    int aux = 0;
	    if (p.x > cg.x)
	        aux += 1;
	    if (p.y > cg.y)
	        aux += 2;
	    if (p.z > cg.z)
	        aux += 4;
	    return(aux);
	    }

      /*
      ** calcula novo meio
      ** -> q  - num. do octante (0 - 7)
      ** -> tc - dimensao do pai
      ** <> xg - entra centro do pai e sai centro de 'q'
      */
//        inline void CalcNewMeio(acPoint3 &xg, acPoint3 &tc, int q)
      void CalcNewMeio(acPoint3 &xg, acPoint3 &tc, int q)           
      {
          if (q > 3)
              {
              xg.z += tc.z / 4.0;
              q -= 4;
              }
          else
              xg.z -= tc.z / 4.0;
          if (q > 1)
              {
              xg.y += tc.y / 4.0;
              q -= 2;
              }
          else
              xg.y -= tc.y / 4.0;
          if (q > 0)
              xg.x += tc.x / 4.0;
          else
              xg.x -= tc.x / 4.0;

          tc.x = tc.x * 0.5;
          tc.y = tc.y * 0.5;
          tc.z = tc.z * 0.5;
          }
};

#endif // MALHAPAR_H
