#include "vtkSurfaceGen.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkDoubleArray.h"
#include "vtkCommand.h"
#include "vtkAlgorithm.h"
#include "SurfTriang.h"
#include "nodlist.h"
#include <math.h>

// Recursition max iteraction
#define MAX_ITER 10000

vtkCxxRevisionMacro(vtkSurfaceGen, "$Rev: 635 $");
vtkStandardNewMacro(vtkSurfaceGen);

vtkSurfaceGen::vtkSurfaceGen()
{
	this->first = NULL;
	this->curr = NULL;
	this->Coord = NULL;
	this->NumElsGroups = NULL;
	this->MP = NULL;
	this->TypeCrit = SC_DELA;
	this->Factor=1.0;
	this->colapsaError = false;
	this->recursiveCounter = 0;
	this->Shells = 1;
}    

vtkSurfaceGen::~vtkSurfaceGen()
{
	if(this->NumElsGroups)	delete []this->NumElsGroups; // this->NumElsGroups = NULL;//free(this->NumElsGroups);
	if(this->MP)		   			this->MP = NULL;   	
	if(this->Coord)		   		delete this->Coord; //this->Coord = NULL;//	free(this->Coord);	
}

void vtkSurfaceGen::Delete()
{
	this->Superclass::Delete();
}

void vtkSurfaceGen::GetCoord(int i, double *vec)
{
	if(Coord) Coord->GetNode(i, vec);
	else	 	 cout << "void vtkSurfaceGen::GetCoord() :: Não inicializada!!!!" << endl;
}


void vtkSurfaceGen::SetPar(vtkParamMesh *mpar)
{ 
  this->MP=mpar;
}


vtkParamMesh* vtkSurfaceGen::GetPar()
{
	return this->MP;
}

void vtkSurfaceGen::Copy(vtkMesh3d *mesh, vtkParamMesh *mpar)
{
	static char *nome = "vtkSurfaceGen::Copy(vtkMesh3d *mesh, vtkParamMesh *mpar)";
	TraceOn(nome);

	SparseElNo  *El  = mesh->NewElNo();
	SparseFaFaV *ElV = mesh->NewFaFaV();
  
	long i;
	int g;
	TypeCrit = SC_DELA;
	CotaColap = 0.1;
	CotaCopla = 0.99;
	Factor=1.0;
  
	this->MP = mpar;

	typedef SurfTriang* SurfVector;
	SurfVector *vd = new SurfVector[El->GetTIndex()];

//  SurfTriang **vd;       
//  vd = (SurfTriang **)mMalloc(sizeof(SurfTriang *)*El->GetTIndex());

//                                  Coloca los nodos
  for (i=0; i<El->GetTIndex(); i++)
      {
      curr = new SurfTriang;
      vd[i]  = curr;
      curr->n1 = El->GetElem(i, 0);
      curr->n2 = El->GetElem(i, 1);
      curr->n3 = El->GetElem(i, 2);
      }

//                                  Coloca los vecinos
  for (i=0; i<ElV->GetTIndex(); i++)
      {
      curr = vd[i];
      if (ElV->GetElem(i, 0) >= 0)
          curr->v1 = vd[ElV->GetElem(i, 0)];
      else
          curr->v1 = NULL;
      if (ElV->GetElem(i, 1) >= 0)
          curr->v2 = vd[ElV->GetElem(i, 1)];
      else
          curr->v2 = NULL;
      if (ElV->GetElem(i, 2) >= 0)
          curr->v3 = vd[ElV->GetElem(i, 2)];
      else
          curr->v3 = NULL;
      }

	// Engancha la lista
  first = vd[0];
  first->prev = NULL;
  first->next = vd[1];
  for (i=1; i<ElV->GetTIndex()-1; i++)
      {
      curr = vd[i];
      curr->prev = vd[i-1];
      curr->next = vd[i+1];
      }
  SurfTriang *last;
  last = vd[ElV->GetTIndex()-1];
  last->prev = vd[ElV->GetTIndex()-2];
  last->next = NULL;

	// Coloca los numeros de grupo
  this->curr = this->first;
  this->NumGroups = mesh->GetNumGroups();
	//  this->NumElsGroups = (int *) malloc(NumGroups * sizeof(int)); 
  this->NumElsGroups = new int[NumGroups]; 
  for (g=0; g<NumGroups; g++)
    {
    this->NumElsGroups[g] = mesh->GetNumElsGrp(g);
    for (i=0; i < this->NumElsGroups[g]; i++)       
      {
      curr->gr = g;
      curr = curr->next;
      }
    }

	// Borra estructuras auxiliares
  //mFree(vd);
  delete []vd;
  delete El;
  delete ElV;
	// Copia las coordenadas
	 acCoordinate3D &c = *mesh->GetCoords();
	 acPoint3 p;
	 Coord = new acDynCoord3D;
	 for (i=0; i< mesh->GetNumNodes(); i++)
		 {
		 p = c[i];
		 Coord->AddCoord(p);
		 } 
  TraceOff(nome);
}

void vtkSurfaceGen::ImportSurface(vtkMesh3d *mesh)
{
	SparseElNo *El = mesh->NewElNo();
	SparseFaFaV *ElV = mesh->NewFaFaV();
	long i, nod=Coord->GetNumNodes();
	int g;

	typedef SurfTriang* SurfVector;
	SurfVector *vd = new SurfVector[El->GetTIndex()];

//  SurfTriang **vd;
//  vd = (SurfTriang **)mMalloc(sizeof(SurfTriang *)*El->GetTIndex());

	// Coloca los nodos
  for (i=0; i<El->GetTIndex(); i++)
    {
    curr = new SurfTriang;
    vd[i]  = curr;
    curr->n1 = El->GetElem(i, 0)+nod;
    curr->n2 = El->GetElem(i, 1)+nod;
    curr->n3 = El->GetElem(i, 2)+nod;
    }

	// Coloca los vecinos
  for (i=0; i<ElV->GetTIndex(); i++)
    {
    curr = vd[i];
    if (ElV->GetElem(i, 0) >= 0)
        curr->v1 = vd[ElV->GetElem(i, 0)];
    else
        curr->v1 = NULL;
    if (ElV->GetElem(i, 1) >= 0)
        curr->v2 = vd[ElV->GetElem(i, 1)];
    else
        curr->v2 = NULL;
    if (ElV->GetElem(i, 2) >= 0)
        curr->v3 = vd[ElV->GetElem(i, 2)];
    else
        curr->v3 = NULL;
    }

	//                                  Engancha la lista
  curr=first;
  while(curr->next)
      curr=curr->next;
  curr->next = vd[0];
  curr->next->prev = curr;
  curr->next->next = vd[1];
  for (i=1; i<ElV->GetTIndex()-1; i++)
      {
      curr = vd[i];
      curr->prev = vd[i-1];
      curr->next = vd[i+1];
      }
  SurfTriang *last;
  last = vd[ElV->GetTIndex()-1];
  last->prev = vd[ElV->GetTIndex()-2];
  last->next = NULL;

	//                                  Coloca los numeros de grupo
  curr = vd[0];
  int ng = mesh->GetNumGroups();
  for (g=0; g<ng; g++)
    {
    for (i=0; i<mesh->GetNumElsGrp(g); i++)
      {
      curr->gr = g+NumGroups;
      curr = curr->next;
      }
    }
  NumGroups+=ng;
	//                                  Borra estructuras auxiliares
	//  mFree(vd);
	delete []vd;
	delete El;
	delete ElV;

	//                                  Copia las coordenadas
	acCoordinate3D &c = *mesh->GetCoords();
	acPoint3 p;
	for (i=0; i< mesh->GetNumNodes(); i++) 
		{
		p = c[i];
		Coord->AddCoord(p);
		}
}

void vtkSurfaceGen::TestMesh(long &nod, long &nel, int &null)
{
  static char *nome = "vtkSurfaceGen::TestMesh";
  TraceOn(nome);
  char buf[80];
  
  nel = 0;
  nod = Coord->GetNumNodes();
  curr = first;
  while (curr)
  	{	 
		nel++;
		if (curr->v1 == NULL)
			{
			sprintf(buf,"Elemento %ld sin vecino",nel);
			Error(WARNING, 1, buf);
			}
		else if (!(curr->v1->v1 == curr || curr->v1->v2 == curr || curr->v1->v3 == curr))
		 	{
			sprintf(buf,"Elemento %ld con vecino equivocado",nel);
			Error(WARNING, 1, buf);
		 	}
		curr = curr->next;
		}

	typedef nodlist* nodlistVector;
	nodlistVector *nodo = new nodlistVector[nod];
	
//    nodlist **nodo;
//    nodo=(nodlist **)mMalloc(sizeof(nodlist *)*nod);
    
  int i;
  for (i=0;i<nod;i++)
      nodo[i]=new nodlist;
  curr = first;
  nel=0;
  while (curr)
    {
		nel++;
    if (nodo[curr->n1]->add(curr->n2) >1 ||
        nodo[curr->n1]->add(curr->n3) >1 ||
        nodo[curr->n2]->add(curr->n1) >1 ||
        nodo[curr->n2]->add(curr->n3) >1 ||
        nodo[curr->n3]->add(curr->n1) >1 ||
        nodo[curr->n3]->add(curr->n2) >1)
		  	{
	     	sprintf(buf,"Elemento %ld con mas de tres vecinos",nel);
	     	Error(WARNING, 1, buf);
		  	}
	    	curr=curr->next;
  	}
	for (i=0;i<nod;i++)	delete nodo[i];
//		mFree(nodo);
	delete []nodo;

	// Search for null edges
	null = 0;
  	curr = first;
  	while (curr)
  	{	 
		if (curr->v1 == NULL) null++;
		if (curr->v2 == NULL) null++;
		if (curr->v3 == NULL) null++;
		curr = curr->next;
	}

	TraceOff(nome);
}

long vtkSurfaceGen::RemoveFreeNodes()
{
  static char *nome = "vtkSurfaceGen::RemoveFreeNodes";
  TraceOn(nome);

  long nodp,nodn;
  
  long progressCount;
  
  double parcialProgress=0.30;
  double progressFreeNodes = 0.0;
  
  nodp = Coord->GetNumNodes();
  progressCount = Coord->GetNumNodes();
  
//  NuevaNum = (long *)mMalloc(sizeof(long)*nodp);
  NuevaNum = new long[nodp];
  if (!NuevaNum)
      Error(FATAL_ERROR,0,"Cannot allocate memory");
  long i;
  
  for (i=0; i<Coord->GetNumNodes(); i++)
      NuevaNum[i]=0;

  curr = first;
  
  while (curr)
    {
    NuevaNum[curr->n1]=1;
    NuevaNum[curr->n2]=1;
    NuevaNum[curr->n3]=1;
    curr = curr->next;
    parcialProgress = fabs((((progressCount-1+parcialProgress)/(progressCount*parcialProgress)))-1);
	 if((parcialProgress >= progressFreeNodes) && (parcialProgress < 1))
  	 	{
      progressFreeNodes = parcialProgress;
      this->SetProgress(progressFreeNodes);
      this->UpdateProgress(progressFreeNodes);
    	}
    progressCount = progressCount - 1;
    }
  Coord->RemoveNodes(NuevaNum);
  curr = first;
  while (curr)
    {
    curr->n1 = NuevaNum[curr->n1];
    curr->n2 = NuevaNum[curr->n2];
    curr->n3 = NuevaNum[curr->n3];
    curr = curr->next;
    }

  progressFreeNodes = 1.0;
  this->SetProgress(progressFreeNodes);
  this->UpdateProgress(progressFreeNodes);

	delete []NuevaNum;
//  mFree(NuevaNum);
  nodn = Coord->GetNumNodes();
  TraceOff(nome);
  return(nodp-nodn);
}

long vtkSurfaceGen::ColapsaTodos()
{
	curr = first;
	while (curr)
		{
		curr = Colapsa(curr);
		if(this->colapsaError)	return -1;
		}
   RemoveTresTri();
   return RemoveFreeNodes();
}

void vtkSurfaceGen::ColapsaAlgunos(acQueue *selem)       //<< ColapsaUno >>
{
	static char *nome = "vtkSurfaceGen::ColapsaAlgunos";
	TraceOn(nome);
	long nelem=0;
	long elem;
	int i,flag;

	// Pone los flags en false
	curr = first;
	while (curr)
		{
    nelem++;
		curr->flg = 0;
		curr = curr->next;
		}

//	int *msw = (int *)mMalloc(sizeof(int)*nelem);
	int *msw = new int[nelem];
	for (i=0; i<nelem; i++)
		msw[i]=0;
	while(selem->Pop(&elem))
		msw[elem]=1;

	 // Marca los elementos a colapsar
	 flag = 0;
	 elem = 0;
	 for (i=0; 1; i++)
		  {
		  curr = first;
		  while (curr)
			  {
			  if (curr->gr == i)
				  {
				  if (msw[elem]) curr->flg = 1;
                  elem++;
                  if (elem>=nelem) flag=1;
				  }
			  if (flag) break;
			  curr = curr->next;
			  }
		  if (flag) break;
		  }

  //  mFree(msw);
  delete []msw;
	double cota = CotaColap; CotaColap = 1.0e+09;
	curr = first;
	while (curr)
		{
		if (curr->flg)
			 curr = Colapsa(curr);
		else
			 curr = curr->next;
    }
//	Renumera();
//	mFree(NuevaNum);
	CotaColap = cota;
	RemoveTresTri();
	RemoveFreeNodes();
	TraceOff(nome);
}

void vtkSurfaceGen::RemoveTresTri()
{
	// Evita que entre num loop infinito nesse método recursivo
	this->recursiveCounter = 0;
	
	int vd,ind;
	curr = first; 
	while (curr)
   	{
		if (curr->v1)
			{
			if      (curr->v1->v1 == curr) ind = 1;
			else if (curr->v1->v2 == curr) ind = 2;
			else                           ind = 0;
	    vd = FronteraDer(curr->v1,ind);
	    if (vd==2)
	       {
	       RemoveDos(curr,0);
	       curr=curr->next;
	       continue;
	       }
			}
		if (curr->v2)
			{
			if      (curr->v2->v1 == curr) ind = 1;
			else if (curr->v2->v2 == curr) ind = 2;
			else                           ind = 0;
			vd = FronteraDer(curr->v2,ind);
			if (vd==2)
            {
            RemoveDos(curr,1);
            curr=curr->next;
            continue;
            }
			}
		if (curr->v3)
       	{
			if      (curr->v3->v1 == curr) ind = 1;
			else if (curr->v3->v2 == curr) ind = 2;
			else                           ind = 0;
			vd = FronteraDer(curr->v3,ind);
			if (vd==2)
            {
            RemoveDos(curr,2);
            curr=curr->next;
            continue;
            }
       	}
		curr=curr->next;
   	}
}

void vtkSurfaceGen::RemoveTresTriBoundary()
{
	int vd,ind;
	curr = first; 
	while (curr)
   	{
   	if( this->IsTriangleInBoundary(curr) )
   		{
			if (curr->v1 )
				{
				if      (curr->v1->v1 == curr) ind = 1;
				else if (curr->v1->v2 == curr) ind = 2;
				else                           ind = 0;
		    vd = FronteraDer(curr->v1,ind);
		    if (vd==2)
		       {
		       // Testa se todos os triangulos estão no mesmo grupo. Só remove nesse caso
		       if(curr->v1->gr == curr->gr && curr->v3->gr == curr->gr)
			       RemoveDos(curr,0);
		       curr=curr->next;
		       continue;
		       }
				}
			if (curr->v2)
				{
				if      (curr->v2->v1 == curr) ind = 1;
				else if (curr->v2->v2 == curr) ind = 2;
				else                           ind = 0;
				vd = FronteraDer(curr->v2,ind);
				if (vd==2)
					{
			      // Testa se todos os triangulos estão no mesmo grupo. Só remove nesse caso
			      if(curr->v1->gr == curr->gr && curr->v2->gr == curr->gr)
	            	RemoveDos(curr,1);
	            curr=curr->next;
	            continue;
	            }
				}
			if (curr->v3)
	       	{
				if      (curr->v3->v1 == curr) ind = 1;
				else if (curr->v3->v2 == curr) ind = 2;
				else                           ind = 0;
				vd = FronteraDer(curr->v3,ind);
				if (vd==2)
	            {
			      // Testa se todos os triangulos estão no mesmo grupo. Só remove nesse caso
			      if(curr->v2->gr == curr->gr && curr->v3->gr == curr->gr)       	
	            	RemoveDos(curr,2);
	            curr=curr->next;
	            continue;
	            }
	       	}
   		}
		curr=curr->next;
   	}
}

void vtkSurfaceGen::RemoveDos(SurfTriang *el, int ind)
{
  SurfTriang *elv;
  if (ind==0)
      {
      elv=el->v1;
      if (elv->v1==el)
          {
          el->n1= elv->n3;
          el->v1= elv->v3;
          }
      else if (elv->v2==el)
          {
          el->n1= elv->n1;
          el->v1= elv->v1;
          }
      else
          {
          el->n1= elv->n2;
          el->v1= elv->v2;
          }
      if (el->v1)
          {
          if     (el->v1->v1==elv) el->v1->v1=el;
          else if(el->v1->v2==elv) el->v1->v2=el;
          else                     el->v1->v3=el;
          }
			
      Delete(elv);
      elv=el->v3;
      if      (elv->v1==el)  el->v3= elv->v2;
      else if (elv->v2==el)  el->v3= elv->v3;
      else                   el->v3= elv->v1;
      if (el->v3)
          {
          if     (el->v3->v1==elv) el->v3->v1=el;
          else if(el->v3->v2==elv) el->v3->v2=el;
          else                     el->v3->v3=el;
          }
      Delete(elv);
      }
  else if (ind==1)
      {
      elv=el->v2;
      if (elv->v1==el)
          {
          el->n2= elv->n3;
          el->v2= elv->v3;
          }
      else if (elv->v2==el)
          {
          el->n2= elv->n1;
          el->v2= elv->v1;
          }
      else
          {
          el->n2= elv->n2;
          el->v2= elv->v2;
          }
      if (el->v2)
          {
          if     (el->v2->v1==elv) el->v2->v1=el;
          else if(el->v2->v2==elv) el->v2->v2=el;
          else                     el->v2->v3=el;
          }
      Delete(elv);
      elv=el->v1;
      if      (elv->v1==el)  el->v1= elv->v2;
      else if (elv->v2==el)  el->v1= elv->v3;
      else                   el->v1= elv->v1;
      if (el->v1)
          {
          if     (el->v1->v1==elv) el->v1->v1=el;
          else if(el->v1->v2==elv) el->v1->v2=el;
          else                     el->v1->v3=el;
          }
      Delete(elv);
      }
  else if (ind==1)
      {
      elv=el->v3;
      if (elv->v1==el)
          {
          el->n3= elv->n3;
          el->v3= elv->v3;
          }
      else if (elv->v2==el)
          {
          el->n3= elv->n1;
          el->v3= elv->v1;
          }
      else
          {
          el->n3= elv->n2;
          el->v3= elv->v2;
          }
      if (el->v3)
          {
          if     (el->v3->v1==elv) el->v3->v1=el;
          else if(el->v3->v2==elv) el->v3->v2=el;
          else                     el->v3->v3=el;
          }
      Delete(elv);
      elv=el->v2;
      if      (elv->v1==el)  el->v2= elv->v2;
      else if (elv->v2==el)  el->v2= elv->v3;
      else                   el->v2= elv->v1;
      if (el->v2)
          {
          if     (el->v2->v1==elv) el->v2->v1=el;
          else if(el->v2->v2==elv) el->v2->v2=el;
          else                     el->v2->v3=el;
          }
      Delete(elv);
      }
	// Set Triangle as modified
	el->isNew = 1;            
}

long vtkSurfaceGen::SwapAll()
{
	static char *nome = "vtkSurfaceGen::SwapAll";
	double swapProgress = 0.0;
	TraceOn(nome);
	long sws = 1;
	long swst= 0;
	long numswap = 0;
	int cont = 1;
	
	this->SetProgress(0.10);
	this->UpdateProgress(0.10);

	while (sws)
		{
	    sws = 0;
	    curr = first;
	    while (curr)
		    {
		     swapProgress = fabs(((swapProgress + 0.1*(1.0 - cont))));
		     this->SetProgress(swapProgress);
   	     this->UpdateProgress(swapProgress);
		     if (curr->v1)
		        {
		        RotaElem(curr,0);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		     if (curr->v2)
		        {
		        RotaElem(curr,1);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		     if (curr->v2)   //Esta bien!! es por la rotacion
		        {
		        RotaElem(curr,1);
		        sws = SwapDiag(curr,curr->v1) + sws;
		        }
		    curr = curr->next;
		    }
			numswap++;
			if (numswap > 6) break;
			swst = swst + sws;
			cont = cont +1;
	  }
	    
	swapProgress = 1.0;
	this->SetProgress(swapProgress);
	this->UpdateProgress(swapProgress);  
	  
	TraceOff(nome);
	return (swst);
}

void vtkSurfaceGen::Renumera()                            //<< Renumera >>
{
	 Coord->Renumber(NuevaNum);
	 curr = first;
	 while (curr)
		{
		curr->n1 = NuevaNum[curr->n1];
		curr->n2 = NuevaNum[curr->n2];
		curr->n3 = NuevaNum[curr->n3];
		if (curr->n1>=Coord->GetNumNodes() ||
		    curr->n2>=Coord->GetNumNodes() ||
		    curr->n3>=Coord->GetNumNodes())
           	Error(FATAL_ERROR, 1, "Invalid surface");
    	curr = curr->next;
	   }
}

SurfTriang * vtkSurfaceGen::Colapsa(SurfTriang *el)    //<< Colapsa >>
{
	this->recursiveCounter = 0;
	
	SurfTriang *va;
	double x21,y21,z21,x31,y31,z31,da,db,d;
	acPoint3 p1,p2,p3;
	int ind,indd,indi,vi,vd;
	acCoordinate3D &coord = *Coord;

	p1 = coord[el->n1];
	p2 = coord[el->n2];
	p3 = coord[el->n3];
	
	d  = MP->GetH(p1);
	
	// Se fija si algun lado es menor que lo debido
	x21 = p2.x-p1.x; y21 = p2.y-p1.y; z21 = p2.z-p1.z;
	da  = x21*x21+y21*y21+z21*z21; ind = 0;
	
	x31 = p3.x-p2.x; y31 = p3.y-p2.y; z31 = p3.z-p2.z;
	db  = x31*x31+y31*y31+z31*z31;
	if (da > db) {ind = 1; da = db;}
	
	x31 = p3.x-p1.x; y31 = p3.y-p1.y; z31 = p3.z-p1.z;
	db  = x31*x31+y31*y31+z31*z31;
	if (da > db) {ind = 2; da = db;}
	if (da > CotaColap*d*d) return(el->next);

	RotaElem(el,ind);
	SurfTriang *elv=el->v1;
	if (!elv) return (el->next); //MODIFICAR PARA SUP ABIERTAS
	if (elv->v2)
	  {
	  if      (elv->v2->v1 == elv) indi = 2;
		else if (elv->v2->v2 == elv) indi = 0;
		else                         indi = 1;
	  curr=elv;
	  vi = FronteraIzq(elv->v2,indi);
	  if (vi==2) return (el->next);
	  curr=el;
	  }
	 
    if (!el->v2)
    	vd = 0;
    else
      {
    	if      (el->v2->v1 == el) { indd = 1; indi = 2; }
	    else if (el->v2->v2 == el) { indd = 2; indi = 0; }
	    else                       { indd = 0; indi = 1; }
        vd = FronteraDer(el->v2,indd);
        vi = FronteraIzq(el->v2,indi); 
        if (vi==2) return (el->next);
      }

    if (!el->v3)
    	vi = 0;
    else
      {
      if      (el->v3->v1 == el) indi = 2;
	    else if (el->v3->v2 == el) indi = 0;
	    else                       indi = 1;
      vi = FronteraIzq(el->v3,indi);
      }

	 // Calcula la nueva posicion de n1 (p1)
	p1 = coord[el->n1];
	p2 = coord[el->n2];
	//     if (vi==2 || vd==2) return (el->next);
	if ((!vi && !vd) || (vi && vd))
    {
    p1.x = (p2.x+p1.x)*0.5;
    p1.y = (p2.y+p1.y)*0.5;
    p1.z = (p2.z+p1.z)*0.5;
    }
	else if (!vd) p1 = p2;

	// Test para verificar que los vecinos no se dan vuelta
	if (CotaColap<1.0 && el->v2 && !TestVecDer(el->n2,p1,el->v2,indd)) 
  	return(el->next);
  	
	if (CotaColap<1.0 && el->v3 && !TestVecIzq(el->n1,p1,el->v3,indi)) 
    return(el->next);

	 // Test para verificar que los vecinos sean coplanares
	if (el->v2 && !Coplanar(el,1)) 
    return(el->next);
	if (el->v3 && !Coplanar(el,2)) 
    return(el->next);

// Renombra los nodos
//	NuevaNum[el->n2] = el->n1;
	if (!el->v2)
		vd = 0;
	else
  	{
    vd = CambiaNodoDer(el->n2,el->n1,el->v2,indd);
    if(this->colapsaError) return NULL;      
  	}
	if (!vd && el->v3)
  	{
		vi = CambiaNodoIzq(el->n2,el->n1,el->v1,2);
    if(this->colapsaError) return NULL;
  	}

	(*Coord)[el->n1] = p1;

	// Actualiza los vecinos
	if (el->v2)
	  {
	  if      (el->v2->v1 == el) el->v2->v1 = el->v3;
		else if (el->v2->v2 == el) el->v2->v2 = el->v3;
		else                       el->v2->v3 = el->v3;
	  }

	if (el->v3)
		{
		if      (el->v3->v1 == el) el->v3->v1 = el->v2;
		else if (el->v3->v2 == el) el->v3->v2 = el->v2;
		else                       el->v3->v3 = el->v2;
		}

	if (elv->v2)
  	{
	  if      (elv->v2->v1 == elv) elv->v2->v1 = elv->v3;
	  else if (elv->v2->v2 == elv) elv->v2->v2 = elv->v3;
	  else                         elv->v2->v3 = elv->v3;
    }

	if (elv->v3)
		{
	  if      (elv->v3->v1 == elv) elv->v3->v1 = elv->v2;
	  else if (elv->v3->v2 == elv) elv->v3->v2 = elv->v2;
	  else                         elv->v3->v3 = elv->v2;
    }

	// Borra los elementos de la lista
	Delete(el->v1);
	va = el->next;
	Delete(el);
	return(va);
}

long vtkSurfaceGen::DivideTodos()
{
	 static char *nome = "vtkSurfaceGen::DivideTodos";
	 double tam, d;
	 int ind;
	 long n;
	 acPoint3 pos;
   double factor = 1.4*Factor;
   double divideProgress = 0.1;

	 TraceOn(nome);

	 curr = first;
	 while (curr)
      {
		  CalNewNode(curr, pos, tam, ind);
		  d = MP->GetH(pos);
		  while (tam > factor*factor*d*d)
              factor*=2;
		  curr = curr->next;
      }

	 double factorant=factor;
	 long sw = 1;
	 long swt= 0;
	 long progressCount = 1;
	 
	 SwapAll();
	 for (FlagCop = 0; FlagCop <= 1; FlagCop++)
		{
		while (factor>=1.4*Factor)
			 {
			 sw = 0;
			 curr = first;
			 while (curr)
				  {
				  CalNewNode(curr, pos, tam, ind);

				  d = factor*MP->GetH(pos);
				  if (tam > d*d)
					  {
					  n  = Coord->GetNumNodes();
					  Coord->AddCoord(pos);
					  DivideElement(curr,ind,n);
					  sw ++;
					  }
				  curr = curr->next;
				  
				  divideProgress = this->GetProgress();
				  divideProgress = fabs((((divideProgress)/(progressCount+0.03))*0.07)-1);
				  this->SetProgress(divideProgress);
		
			     this->UpdateProgress(divideProgress);
				  progressCount = progressCount+1;

				  }
			swt = swt + sw;
			SwapAll();
			if (!sw) factor/=2;
			}
        factor = factorant;
		}
	 TraceOff(nome);
	 return(swt);
}

void  vtkSurfaceGen::DivideAlgunos(acQueue *selem)
{
	 static char *nome = "vtkSurfaceGen::DivideAlgunos";
	 long n,elem;
	 double tam;
	 int ind;
	 acPoint3 pos;
	 TraceOn(nome);
	 int i;

	 // Pone los flags en false
    long nelem=0;
	 curr = first;
	 while (curr)
    	 {
         nelem++;
		 curr->flg = 0;
		 curr = curr->next;
		 }

//	 int *msw = (int *)mMalloc(sizeof(int)*nelem);
	 int *msw = new int[nelem];
	 for (i=0; i<nelem; i++)
         msw[i]=0;
     while(selem->Pop(&elem))
         msw[elem]=1;

	 int flag = 0;
	 elem = 0;
	 for (i=0; 1; i++)
		  {
		  curr = first;
		  while (curr)
			  {
			  if (curr->gr == i)
				  {
				  if (msw[elem]) curr->flg = 1;
		  elem++;
		  if (elem>=nelem) flag=1;
				  }
			  if (flag) break;
			  curr = curr->next;
			  }
		  if (flag) break;
		  }

//    mFree(msw);
	delete []msw;
	FlagCop = 1;
	curr = first;
	while (curr)
    {
    if (curr->flg)
         {
         curr->flg = 0;
         CalNewNode(curr, pos, tam, ind);
         n   = Coord->GetNumNodes();
         Coord->AddCoord(pos);
         DivideElement(curr,ind,n);
         }
     curr = curr->next;
     }
  TraceOff(nome);
}

long vtkSurfaceGen::DivideObtusos(double cota)
{
	static char *nome = "vtkSurfaceGen::DivideUno";
	long n,num;
	double tam;
	int ind;
	long iter = 1;
	double progressCount = 0.0;
	double parcialProgress = 0.0;
	acPoint3 pos;
	TraceOn(nome);	 
	
	FlagCop = 1;
	num = 0;

	curr = first;
	while (curr)
		 {
		  	Obtuso(curr, pos, tam, ind, cota);
		   parcialProgress = this->GetProgress();
		  	progressCount = fabs(((((progressCount - iter-num)/10)/(100)))/100);
		  	if(parcialProgress > progressCount)
		  		this->SetProgress(progressCount);
		  	this->UpdateProgress(progressCount);
		  
		  if (tam > 0)
				{
				n   = Coord->GetNumNodes();
				Coord->AddCoord(pos);
				DivideElement(curr,ind,n);
				num++;
				}
		 curr = curr->next;
		 
		 iter = iter+1;
		 }

    this->UpdateProgress(1.0);

	TraceOff(nome);
	return(num);
}

void vtkSurfaceGen::DivideElement(SurfTriang *el,int ind, long n)
{
	SurfTriang *el1, *el2, *elv;

	RotaElem(el, ind);
	elv = el->v1;

	el1 = new SurfTriang;
	if (elv)
		 el2 = new SurfTriang;
	else
		 el2 = NULL;

	el1->SetData(el->n2, el->n3, n, el->gr, el->v2, el, el2);
	el->SetData (el->n3, el->n1, n, el->gr, el->v3, elv, el1);

	InsertFirst(el1);
	
	if (elv)
		 {
		 el2->SetData(elv->n3, elv->n1, n, elv->gr, elv->v3, el1, elv);
		 elv->SetData(elv->n2, elv->n3, n, elv->gr, elv->v2, el2, el);
		 InsertFirst(el2);
		 }

	if (el1->v1)
		{
		if (el1->v1->v1 == el)
			 el1->v1->v1 = el1;
		else if (el1->v1->v2 == el)
			 el1->v1->v2 = el1;
		else if (el1->v1->v3 == el)
			 el1->v1->v3 = el1;
		SwapDiag(el1,el1->v1);
		}

	if (el->v1)
		{
		SwapDiag(el,el->v1);
		}

	if (elv)
		{
		if (el2->v1)
			{
			if (el2->v1->v1 == elv)
		  el2->v1->v1 = el2;
			else if (el2->v1->v2 == elv)
		  el2->v1->v2 = el2;
			else if (el2->v1->v3 == elv)
		  el2->v1->v3 = el2;
			SwapDiag(el2,el2->v1);
			}
		if (elv->v1)
			{
			SwapDiag(elv,elv->v1);
			}
		}
	
	// Marca triângulos modificados como novos
	if(el)	el->isNew = 1;
	if(el1)	el1->isNew = 1;
	if(el2)	el2->isNew = 1;
	if(elv)	elv->isNew = 1;	
}

int vtkSurfaceGen::SwapDiag(SurfTriang *el, SurfTriang *elv)
{
	long naux;
  SurfTriang *vaux;

  if (el->gr != elv->gr)
		return(0);
// Primero ordena elv

    if (elv->v1 != el)
       {
       if (elv->v2 != el)
			{
         naux=elv->n1; elv->n1=elv->n3; elv->n3=elv->n2; elv->n2=naux;
         vaux=elv->v1; elv->v1=elv->v3; elv->v3=elv->v2; elv->v2=vaux;
         }
       else
       	{
         naux=elv->n1; elv->n1=elv->n2; elv->n2=elv->n3; elv->n3=naux;
         vaux=elv->v1; elv->v1=elv->v2; elv->v2=elv->v3; elv->v3=vaux;
         }
       }

// Aca viene el swaping

	if (!Conviene(el, elv))
		return(0);

	el->n2 = elv->n3; elv->n2 = el->n3;
	el->v1 = elv->v2; elv->v1 = el->v2; el->v2 = elv; elv->v2 = el;

	// Set Triangle as modified or new
	el->isNew = 1;
	elv->isNew = 1;	

	if (elv->v1 != NULL)
	{
	if      (elv->v1->v1 == el) elv->v1->v1 = elv;
	else if (elv->v1->v2 == el) elv->v1->v2 = elv;
	else                        elv->v1->v3 = elv;
	}

    if (el->v1 != NULL)
	{
	if      (el->v1->v1 == elv) el->v1->v1 = el;
	else if (el->v1->v2 == elv) el->v1->v2 = el;
	else                        el->v1->v3 = el;
	SwapDiag(el, el->v1);
	}
    if (elv->v3!= NULL)
	{
	naux    = elv->n3;
	elv->n3 = elv->n2;
	elv->n2 = elv->n1;
	elv->n1 = naux;
	vaux    = elv->v3;
	elv->v3 = elv->v2;
	elv->v2 = elv->v1;
	elv->v1 = vaux;
	SwapDiag(elv, elv->v1);
	}
  return(1);
}

void vtkSurfaceGen::RotaElem(SurfTriang *el, int ind)
{
	long na;
  SurfTriang *va;
//                      Rota el elemento

	if (ind == 1)
	{
	na = el->n1;
	el->n1 = el->n2;
	el->n2 = el->n3;
	el->n3 = na;
	va = el->v1;
	el->v1 = el->v2;
	el->v2 = el->v3;
	el->v3 = va;
	}
    else if (ind == 2)
	{
	na = el->n1;
	el->n1 = el->n3;
	el->n3 = el->n2;
	el->n2 = na;
	va = el->v1;
	el->v1 = el->v3;
	el->v3 = el->v2;
	el->v2 = va;
	}
//                      Rota el vecino
	if (!el->v1)
		{
		return;
		}
	if (el->v1->v2 == el)
		{
		na = el->v1->n1;
		el->v1->n1 = el->v1->n2;
		el->v1->n2 = el->v1->n3;
		el->v1->n3 = na;
		va = el->v1->v1;
		el->v1->v1 = el->v1->v2;
		el->v1->v2 = el->v1->v3;
		el->v1->v3 = va;
		}
	else if (el->v1->v3 == el)
		{
		na = el->v1->n1;
		el->v1->n1 = el->v1->n3;
		el->v1->n3 = el->v1->n2;
		el->v1->n2 = na;
		va = el->v1->v1;
		el->v1->v1 = el->v1->v3;
		el->v1->v3 = el->v1->v2;
		el->v1->v2 = va;
		}
}

int vtkSurfaceGen::Coplanar(SurfTriang *el, int ind)
{
	 acCoordinate3D &coord = *Coord;
	 acPoint3 p1, p2, p3, p4;
	 double x21,y21,z21, x31,y31,z31, x41,y41,z41, x43,y43,z43;
	 double nx,ny,nz, nvx,nvy,nvz, mod,modv,m21,m43,aretip;

	 if (ind == 0)
	 	 {
	   if (!el->v1) return(0);
	   p1 = coord[el->n1];
	   p2 = coord[el->n2];
	   p3 = coord[el->n3];
	   if      (el->v1->v1==el) p4 = coord[el->v1->n3];
	   else if (el->v1->v2==el) p4 = coord[el->v1->n1];
	   else if (el->v1->v3==el) p4 = coord[el->v1->n2];
	   }
	else if (ind == 1)
		{
		if (!el->v2) return(0);
		p1 = coord[el->n2];
		p2 = coord[el->n3];
		p3 = coord[el->n1];
		if      (el->v2->v1==el) p4 = coord[el->v2->n3];
		else if (el->v2->v2==el) p4 = coord[el->v2->n1];
		else if (el->v2->v3==el) p4 = coord[el->v2->n2];
		}

  else if (ind == 2)
		{
		if (!el->v3) return(0);
		p1 = coord[el->n3];
		p2 = coord[el->n1];
		p3 = coord[el->n2];
		if      (el->v3->v1==el) p4 = coord[el->v3->n3];
		else if (el->v3->v2==el) p4 = coord[el->v3->n1];
		else if (el->v3->v3==el) p4 = coord[el->v3->n2];
		}

  x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
  x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
  x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
  x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;

  nx  = y21*z31-z21*y31; ny  = z21*x31-x21*z31; nz  = x21*y31-y21*x31;
  nvx = y41*z21-z41*y21; nvy = z41*x21-x41*z21; nvz = x41*y21-y41*x21;
  mod = sqrt(nx*nx+ny*ny+nz*nz); modv = sqrt(nvx*nvx+nvy*nvy+nvz*nvz);

  m21 = x21*x21+y21*y21+z21*z21;
  m43 = x43*x43+y43*y43+z43*z43;
  aretip = m21 > m43 ? m21 : m43; // Area tipica

  if (mod  <= 1.0e-12*aretip || modv <= 1.0e-12*aretip) 
    {  // Elemento demasiado pequenho
    Error(WARNING, 1, "element with null area");
    return(0);
    }

  nx /= mod; ny /=mod; nz/=mod;
  if ((nx*nvx + ny*nvy + nz*nvz) < CotaCopla*modv)  return(0);
  return(1);
}

int vtkSurfaceGen::Conviene(SurfTriang *el, SurfTriang *elv)
{
  acCoordinate3D &coord = *Coord;
  acPoint3 p1, p2, p3, p4;
  double x21,y21,z21, x31,y31,z31, x41,y41,z41;
  double x32,y32,z32, x42,y42,z42, x43,y43,z43;
  double nx,ny,nz, nvx,nvy,nvz, mod,modv;
  double m31,m41,m32,m42, m21,m43;
  double fo,fa,f1,f2,f3,f4;
  double aretip;

  p1 = coord[el->n1];
  p2 = coord[el->n2];
  p3 = coord[el->n3];
  p4 = coord[elv->n3];

  x21 = p2.x-p1.x;   y21 = p2.y-p1.y;   z21 = p2.z-p1.z;
  x31 = p3.x-p1.x;   y31 = p3.y-p1.y;   z31 = p3.z-p1.z;
  x41 = p4.x-p1.x;   y41 = p4.y-p1.y;   z41 = p4.z-p1.z;
  x43 = p4.x-p3.x;   y43 = p4.y-p3.y;   z43 = p4.z-p3.z;
  x32 = p3.x-p2.x;   y32 = p3.y-p2.y;   z32 = p3.z-p2.z;
  x42 = p4.x-p2.x;   y42 = p4.y-p2.y;   z42 = p4.z-p2.z;

  nx  = y21*z31-z21*y31; ny  = z21*x31-x21*z31; nz  = x21*y31-y21*x31;
  nvx = y41*z21-z41*y21; nvy = z41*x21-x41*z21; nvz = x41*y21-y41*x21;
  mod = sqrt(nx*nx+ny*ny+nz*nz); modv = sqrt(nvx*nvx+nvy*nvy+nvz*nvz);

			  // si no son seudo-coplanares no conviene
  m21 = x21*x21+y21*y21+z21*z21;
  m43 = x43*x43+y43*y43+z43*z43;
  aretip = m21 > m43 ? m21 : m43; // Area tipica

  if (mod  <= 1.0e-12*aretip
   || modv <= 1.0e-12*aretip)   // Elemento demasiado pequenho
  		{
	  	// Verifico si cambiando diagonal soluciono el problema
	  	double v1x,v1y,v1z,v2x,v2y,v2z;
	  	v1x = y41*z31-y31*z41; v1y = z41*x31-x41*z31; v1z = x41*y31-x31*y41;
	  	v2x = y32*z42-y42*z32; v2y = z32*x42-x32*z42; v2z = x32*y42-x42*y32;
	  	if (v1x*v2x+v1y*v2y+v1z*v2z > 0) return(1);
   	Error(WARNING, 1, "element with null area");
		return(0);
		}

// Compara: n . nv = |n| |nv| cos (ang) .vs. |n| |nv| cota

  if ((nx*nvx + ny*nvy + nz*nvz) < CotaCopla*mod*modv)  return(0);
  nx  /= mod;  ny  /= mod;  nz  /= mod;

		 // 41-31 si forman angulo > 180, no se puede
  nvx = y41*z31-z41*y31; nvy = z41*x31-x41*z31; nvz = x41*y31-y41*x31;
  fa  = nx*nvx + ny*nvy + nz*nvz; // Proyecta sobre normal
  if (fa <= 0.0)  return(0);
  f1 = fa; // 2 * Area (1 4 3)

		 // 32-42 si forman angulo > 180, no se puede
  nvx = y32*z42-z32*y42; nvy = z32*x42-x32*z42; nvz = x32*y42-y32*x42;
  fa  = nx*nvx + ny*nvy + nz*nvz;
  if (fa <= 0.0)  return(0);
  f2 = fa; // 2 * Area (2 3 4)

		 // 31-32 si forman angulo > 180, es obligatorio
  nvx = y31*z32-z31*y32; nvy = z31*x32-x31*z32; nvz = x31*y32-y31*x32;
  fa  = nx*nvx + ny*nvy + nz*nvz;
  if (fa <= 0.0)  return(1);
  f3 = fa; // 2 * Area (3 1 2)

		 // 42-41 si forman angulo > 180, es obligatorio
  nvx = y42*z41-z42*y41; nvy = z42*x41-x42*z41; nvz = x42*y41-y42*x41;
  fa  = nx*nvx + ny*nvy + nz*nvz;
  if (fa <= 0.0)  return(1);
  f4 = fa; // 2 * Area (4 2 1)

  switch (TypeCrit)
	{
	case SC_DELA:
	  {
	    m21 = sqrt(x21*x21+y21*y21+z21*z21);
	    m31 = sqrt(x31*x31+y31*y31+z31*z31);
	    m32 = sqrt(x32*x32+y32*y32+z32*z32);
	    m41 = sqrt(x41*x41+y41*y41+z41*z41);
	    m42 = sqrt(x42*x42+y42*y42+z42*z42);
	    m43 = sqrt(x43*x43+y43*y43+z43*z43);

	    fo = 2.0;
				// 41 x 21
	    if (x41*x21+y41*y21+z41*z21 > 0)
	       {
	       fo = f4/(m41*m21);
	       }
				// 21 x 31
	    if (x21*x31+y21*y31+z21*z31 > 0)
	       {
	       fa = f3/(m21*m31);
	       if (fa < fo) fo = fa;
	       }
				// -12 x 42
	    if (x21*x42+y21*y42+z21*z42 < 0)
	       {
	       fa = f4/(m21*m42);
	       if (fa < fo) fo = fa;
	       }
				// 32 x -12
	    if (x32*x21+y32*y21+z32*z21 < 0)
	       {
	       fa = f3/(m32*m21);
	       if (fa < fo) fo = fa;
	       }

				// -13 x 43
	    if (x31*x43+y31*y43+z31*z43 < 0)
	       {
	       fa = f1/(m31*m43);
	       if (0.9999 * fa < fo) return(0);
	       }
				// 43 x -23
	    if (x43*x32+y43*y32+z43*z32 < 0)
	       {
	       fa = f2/(m43*m32);
	       if (0.9999 * fa < fo) return(0);
	       }
				// -24 x -34
	    if (x42*x43+y42*y43+z42*z43 > 0)
	       {
	       fa = f2/(m42*m43);
	       if (0.9999 * fa < fo) return(0);
	       }
				// -34 x -14
	    if (x43*x41+y43*y41+z43*z41 > 0)
	       {
	       fa = f1/(m43*m41);
	       if (0.9999 * fa < fo) return(0);
	       }
	    return(1);
	  }
	case SC_MINMAX:
	  {
	    m31 = sqrt(x31*x31+y31*y31+z31*z31);
	    m32 = sqrt(x32*x32+y32*y32+z32*z32);
	    m41 = sqrt(x41*x41+y41*y41+z41*z41);
	    m42 = sqrt(x42*x42+y42*y42+z42*z42);

	    f1 = f1/(m41*m31);          // f1: sin (41-31)
	    if (x41*x31+y41*y31+z41*z31 < 0.0) f1 = 2.0-f1;
	    f2 = f2/(m32*m42);
	    if (x32*x42+y32*y42+z32*z42 < 0.0) f2 = 2.0-f2;
	    if (f1 > f2)
	       fo = f1;
	    else
	       fo = f2;
	    f3 = f3/(m31*m32);
	    if (x31*x32+y31*y32+z31*z32 < 0.0) f3 = 2.0-f3;
	    if (fo < 0.9999 * f3) return(1);
	    f4 = f4/(m42*m41);
	    if (x42*x41+y42*y41+z42*z41 < 0.0) f4 = 2.0-f4;
	    if (fo < 0.9999 * f4) return(1);
	    return(0);
	  }
	default:
	    return(0);
	}
}

void vtkSurfaceGen::Obtuso(SurfTriang *El, acPoint3 &pos,double &tam, int &ind, double cota)
{
	 acCoordinate3D &coord = *Coord;
	 double a, b, c, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

    dx = p2.x-p1.x;
    dy = p2.y-p1.y;
    dz = p2.z-p1.z;
    a = dx*dx+dy*dy+dz*dz;
    dx = p3.x-p2.x;
    dy = p3.y-p2.y;
    dz = p3.z-p2.z;
    b = dx*dx+dy*dy+dz*dz;
    if (a>b)
       {
       ind = 0;
       tam = a;
       }
    else
       {
       ind = 1;
       tam = b;
       }
    dx = p3.x-p1.x;
    dy = p3.y-p1.y;
    dz = p3.z-p1.z;
    c = dx*dx+dy*dy+dz*dz;
    if (tam < c)
       {
       if (c < a + b - 2*cota*sqrt(a*b)) tam = 0.0;
       else
          {
          ind = 2;
          pos.x = (p3.x+p1.x)*0.5;
          pos.y = (p3.y+p1.y)*0.5;
          pos.z = (p3.z+p1.z)*0.5;
          }
       }
    else if (ind == 0)
       {
       if (a < c + b - 2*cota*sqrt(c*b)) tam = 0.0;
       else
          {
          pos.x = (p2.x+p1.x)*0.5;
          pos.y = (p2.y+p1.y)*0.5;
          pos.z = (p2.z+p1.z)*0.5;
          }
       }
    else
       {
       if (b < a + c - 2*cota*sqrt(a*c)) tam = 0.0;
       else
          {
          pos.x = (p2.x+p3.x)*0.5;
          pos.y = (p2.y+p3.y)*0.5;
          pos.z = (p2.z+p3.z)*0.5;
          }
       }
}

void vtkSurfaceGen::CalNewNode(SurfTriang *El, acPoint3 &pos, double &tam, int &ind)
{
	 acCoordinate3D &coord = *Coord;
	 double d, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

	 tam = 0.0; ind = -1;
	 if ( FlagCop || !Coplanar(El,0) )
       {
       dx = p2.x-p1.x;
       dy = p2.y-p1.y;
       dz = p2.z-p1.z;
       tam = dx*dx+dy*dy+dz*dz;
       ind = 0;
       }
    if ( FlagCop || !Coplanar(El,1) )
       {
       dx = p3.x-p2.x;
       dy = p3.y-p2.y;
       dz = p3.z-p2.z;
       d  = dx*dx+dy*dy+dz*dz;
       if (d > tam)
	   {
	   ind = 1;
	   tam = d;
	   }
       }
    if ( FlagCop || !Coplanar(El,2) )
       {
       dx = p1.x-p3.x;
       dy = p1.y-p3.y;
       dz = p1.z-p3.z;
       d  = dx*dx+dy*dy+dz*dz;
       if (d > tam)
	   {
	   ind = 2;
	   tam = d;
	   }
   }

  if (ind == 0)
	{
	pos.x = (p2.x + p1.x)*0.5;
	pos.y = (p2.y + p1.y)*0.5;
	pos.z = (p2.z + p1.z)*0.5;
	}
  else if (ind == 1)
	{
	pos.x = (p3.x + p2.x)*0.5;
	pos.y = (p3.y + p2.y)*0.5;
	pos.z = (p3.z + p2.z)*0.5;
	}
  else
	{
	pos.x = (p1.x + p3.x)*0.5;
	pos.y = (p1.y + p3.y)*0.5;
	pos.z = (p1.z + p3.z)*0.5;
	}
}

double vtkSurfaceGen::CalDiamEl(SurfTriang *El)
{
	 acCoordinate3D &coord = *Coord;
	 double diam, dx, dy, dz;
	 acPoint3 p1, p2, p3;

	 p1 = coord[El->n1];
	 p2 = coord[El->n2];
	 p3 = coord[El->n3];

    dx    = p2.x-p1.x; dy = p2.y-p1.y; dz = p2.z-p1.z;
    diam  = sqrt(dx*dx+dy*dy+dz*dz);
    dx    = p3.x-p2.x; dy = p3.y-p2.y; dz = p3.z-p2.z;
    diam += sqrt(dx*dx+dy*dy+dz*dz);
    dx    = p1.x-p3.x; dy = p1.y-p3.y; dz = p1.z-p3.z;
    diam += sqrt(dx*dx+dy*dy+dz*dz);
    diam /= 3.0;

    return (diam);
}

acPoint3 vtkSurfaceGen::CalCentElem(SurfTriang *El)
{
	acCoordinate3D &coord = *Coord;
	acPoint3 cent, p1, p2, p3;

	p1 = coord[El->n1];
	p2 = coord[El->n2];
	p3 = coord[El->n3];

	cent.x = ( p1.x + p2.x + p3.x )/3.0;
	cent.y = ( p1.y + p2.y + p3.y )/3.0;
	cent.z = ( p1.z + p2.z + p3.z )/3.0;

  return(cent);
}

void vtkSurfaceGen::InsertFirst(SurfTriang *el)
{
	if(!el)
		cout << "vtkSurfaceGen::InsertFirst(SurfTriang *el) : ERROR!" << endl;
  el->next    = first;
  el->prev    = NULL;
  first->prev = el;
  first       = el;
}

void vtkSurfaceGen::Delete(SurfTriang *el)
{
  if (el == first)
      {
      el->next->prev = NULL;
      first = el->next;
      }
  else if (!el->next)
      el->prev->next = NULL;
  else
      {
      el->prev->next = el->next;
      el->next->prev = el->prev;
      }
  delete el;
  el = NULL;
}

void vtkSurfaceGen::UpdateUnstructuredGrid(vtkUnstructuredGrid *grid) 
{
	////////////////////////////////////////////////////////////////////
	// "Print" coordinates into vtkPoints
	vtkPoints* points = vtkPoints::New();
	Coord->CopyCoordinates(points);
	// Restore object to initial state. Release memory back to system.
	grid->Initialize();
	grid->SetPoints(points);
	points->Delete();

	////////////////////////////////////////////////////////////////////
	// Copy Cell information
	int faceIndex[3];	
	vtkCellArray* faces = vtkCellArray::New();
  for (int i=0; i<NumGroups; i++)
	  {
    curr = this->first;
    while (curr)
	    {
      if (curr->gr == i)
      	{
      	faceIndex[0] = curr->n1;
      	faceIndex[1] = curr->n2;
      	faceIndex[2] = curr->n3;      	      	      	
				faces->InsertNextCell( 3, faceIndex);
      	}
      curr = curr->next;
      }
    }
	grid->SetCells(VTK_TRIANGLE, faces);
	faces->Delete();
}

//---------------------------------------------------------------------------------------
int vtkSurfaceGen::GetNumElsTotal()
{
	int total = 0;
  for(int i = 0; i < NumGroups; i++)
  	total += this->NumElsGroups[i];
  return total;
}

//---------------------------------------------------------------------------------------
void vtkSurfaceGen::GetModifiedCells(int *vec)
{
	int i,j;
  for (i=0, j = 0; i < this->NumGroups; i++)
	  {
    curr = this->first;
    while (curr)
	    {
      if (curr->gr == i)    
	      {
      	if(curr->isNew)
      		vec[j++] = 1;
      	else
      		vec[j++] = 0;
      	}
      curr = curr->next;
      }
    }	
}

//---------------------------------------------------------------------------------------
void vtkSurfaceGen::SetGroupInformation(vtkDoubleArray *group, vtkDoubleArray *selected)
{
	group->Initialize();
	selected->Initialize();	

  for (int i=0; i < NumGroups; i++)
	  {
    curr = this->first;
    while (curr)
	    {
      if (curr->gr == i)
      	{
				group->InsertNextValue(i);				
				selected->InsertNextValue(i);								
      	}
      curr = curr->next;
      }
    }
}

// Get Incidence Vector number of elements
int vtkSurfaceGen::GetIncidenceVectorSize()
{
  int ii=0;
  for (int i=0; i < this->NumGroups; i++)
    {
    this->curr = this->first;
    while (this->curr)
      {
      if (this->curr->gr == i)
				ii+=3;
			curr = curr->next;
      }
    } 
  return ii;   	
}

void vtkSurfaceGen::CopyIncidenceVector(long *incidence)
{
  int ii=0;
  for (int i=0; i < this->NumGroups; i++)
    {
    this->curr = this->first;
    while (this->curr)
      {
      if (this->curr->gr == i)
      	{
      	incidence[ii++] = this->curr->n1; 
      	incidence[ii++] = this->curr->n2;
      	incidence[ii++] = this->curr->n3;      	      	
      	}
      curr = curr->next;
      }
    }    
}

void vtkSurfaceGen::UpdateNumEls()
{
//	long *nelg;
//  nelg = (long *) mMalloc (this->NumGroups * sizeof(long));
	long *nelg = new long[this->NumGroups];
  for (int i=0; i<NumGroups; i++)
      nelg[i]=0;	
  this->curr=this->first;
  while (this->curr)
     {
     nelg[curr->gr]++;
     this->curr = this->curr->next;
     }
  for (int i=0; i<this->NumGroups; i++)
    this->NumElsGroups[i] = nelg[i];
  //mFree(nelg);	
  delete []nelg;
}


void vtkSurfaceGen::Print(FILE *fp)
{
  int i;
  // Imprime informacion de grupos
  // long *nelg = (long *) mMalloc (NumGroups * sizeof(long));
	long *nelg = new long[this->NumGroups];    
    for (i=0; i<NumGroups; i++)
       nelg[i]=0;

  curr=first;
  while (curr)
		{
		nelg[curr->gr]++;
		curr = curr->next;
		}

  fprintf(fp, "*ELEMENT GROUPS\n %d\n",NumGroups);
  for (i=0; i<NumGroups; i++)
    fprintf(fp, "%d %ld Tri3\n", i+1, nelg[i]);
	// mFree(nelg);
  delete []nelg;
  
  //Gravando informações a respeito do número de grupos da casca
  fprintf(fp, "*SHELL GROUPS\n %d\n",this->GetNumberOfShells() );
    
  // Imprime incidencia
  fprintf(fp,"*INCIDENCE\n");
  for (i=0; i<NumGroups; i++)
    {
    // fprintf(fp,"\n");
    curr = first;
    while (curr)
			{
			if (curr->gr == i)
			fprintf(fp,"%ld %ld %ld\n",curr->n1+1, curr->n2+1, curr->n3+1);
			curr = curr->next;
			}
    }

  // Imprime coordenadas
  Coord->Print(fp);

  // Imprime volumen
  acLimit3 vol = Coord->Box();
  fprintf(fp, "*FRONT_VOLUME\n %lf %lf %lf %lf %lf %lf\n", vol.xmin,vol.xmax,vol.ymin,vol.ymax,vol.zmin,vol.zmax);
}

void vtkSurfaceGen::PrintEdges(FILE *fp)
{
    long fn, fna, n;
    long nnodes = Coord->GetNumNodes();
    //long * nlist = (long *)mMalloc(sizeof(long)*nnodes);
		long *nlist = new long[nnodes];		

    for (n=0; n<nnodes; n++)
        nlist[n]=0;

    for (int g=0; g<NumGroups; g++)
        {
        for (int gv=g+1; gv<NumGroups; gv++)
            {
            curr = first;
            int flag = 0;
            while (curr)
                {
                if (curr->gr == g)
                    {
                    if (curr->v1 && curr->v1->gr == gv)
                        { nlist[curr->n1] = curr->n2+1; flag=1; }
                    if (curr->v2 && curr->v2->gr == gv)
                        { nlist[curr->n2] = curr->n3+1; flag=1; }
                    if (curr->v3 && curr->v3->gr == gv)
                        { nlist[curr->n3] = curr->n1+1; flag=1; }
                    }
                curr = curr->next;
                }
            if (!flag) continue;

            //  Hay frontera entre g y gv. Busco el primer nodo
            for (n=0; n<nnodes; n++)
                {
                if (nlist[n])
                    nlist[labs(nlist[n])-1]=-nlist[labs(nlist[n])-1];
                }
            for (n=0; n<nnodes; n++)
                {
                if (nlist[n])
                    {
                    fn = n;
                    if (nlist[n]>0) break;
                    }
                }
            fprintf(fp,"Frontera entre grupos %d-%d\n",g+1,gv+1);
            fprintf(fp," %ld\n",fn+1);
            long nnf=0;
            while (nlist[fn])
                {
                fna = fn;
                fn = labs(nlist[fn]);
                nlist[fna]=0;
                fprintf(fp," %ld\n",fn);
                fn--; nnf++;
                }
            fprintf(fp,"Numero de nodos: %ld\n",nnf);
            }
        }
  
  // !!! Esse deleção não tinha no código original
	delete []nlist;
}

void vtkSurfaceGen::PrintBoundary(FILE *fpb)
{
    long nnodes = Coord->GetNumNodes();
    long j, nnodgr;
    int i,k;
//    int * nodeflag = (int *)mMalloc(sizeof(int)*nnodes);
    int * nodeflag = new int[nnodes];    
    if (!nodeflag)
	    {
	    Error(WARNING, 1, "Can't allocate memory");
	    return;
	    }
    for (i=0; i<NumGroups; i++)
	    {
	    fprintf(fpb, "*GROUP%d\n",i+1);
	    for (j=0; j<nnodes; j++)
	        nodeflag[j] = 0;

	    curr = first;
	    while (curr)
	        {
	        if (curr->gr == i)
		        {
		        nodeflag[curr->n1] = 1;
		        nodeflag[curr->n2] = 1;
		        nodeflag[curr->n3] = 1;
		        }
	        curr = curr->next;
	        }
    	nnodgr = 0;
	    for (j=0; j<nnodes; j++)
	        if (nodeflag[j]) nnodgr++;

        fprintf(fpb, "*NUMBER OF NODES: %ld\n", nnodgr);
	    k = 1;
	    for (j=0; j<nnodes; j++)
	        if (nodeflag[j])
    		    {
	    	    fprintf(fpb, " %ld", j+1);
		        k++;
		        if (k>10) { fprintf (fpb, "\n"); k=1; }
		        nodeflag[j] = 0;
		        }
	    if (k<=10) fprintf (fpb, "\n");
	    }
//    mFree(nodeflag);
	delete []nodeflag;
}

void vtkSurfaceGen::PegaNodos(double cotap)
{
    long nod=Coord->GetNumNodes();
    int skip = 0;

//    long *nuevo = (long *)mMalloc(nod * sizeof(long));
    long *nuevo = new long[nod];    
    
    for (long i=0; i<nod; i++) nuevo[i] = 0;

    // Marca nodos del borde
    curr = first;
    while (curr)
       {
       if (!curr->v1) { nuevo[curr->n1]=1; nuevo[curr->n2]=1;}
       if (!curr->v2) { nuevo[curr->n2]=1; nuevo[curr->n3]=1;}
       if (!curr->v3) { nuevo[curr->n3]=1; nuevo[curr->n1]=1;}
       curr = curr->next;
       }
    Coord->MergeNodes(cotap, nuevo);

    // Renumera la incidencia
    curr = first;
    while (curr)
       {
       curr->n1 = nuevo[curr->n1];
       curr->n2 = nuevo[curr->n2];
       curr->n3 = nuevo[curr->n3];
       curr->flg=0;
       curr = curr->next;
       }

    // Une la incidencia
    SurfTriang *aux;
    curr = first;
    while (curr)
       {
       if (!curr->v1)                   // no existe vecino 1
           {
           aux = curr->next;
           while (aux)                  // Lo busca primero bien orientado
               {
               if (aux->n1 == curr->n1 && aux->n3 == curr->n2)
                   { aux->v3 = curr; curr->v1 = aux; break;}
               if (aux->n2 == curr->n1 && aux->n1 == curr->n2)
                   { aux->v1 = curr; curr->v1 = aux; break;}
               if (aux->n3 == curr->n1 && aux->n2 == curr->n2)
                   { aux->v2 = curr; curr->v1 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v1)               // Ahora lo busca orientado al reves
               {
               aux = curr->next;
               while (aux)
                   {
                   if (aux->n1 == curr->n2 && aux->n3 == curr->n1)
                       {
                       chori(aux);
                       aux->v3 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n2 && aux->n1 == curr->n1)
                       {
                       chori(aux);
                       aux->v2 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n2 && aux->n2 == curr->n1)
                       {
                       chori(aux);
                       aux->v1 = curr;  // Se asume que chori cambia v1 con v2
                       curr->v1 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }
       if (!curr->v2)
           {
           aux = curr->next;
           while (aux)
               {
               if (aux->n1 == curr->n2 && aux->n3 == curr->n3)
                   { aux->v3 = curr; curr->v2 = aux; break;}
               if (aux->n2 == curr->n2 && aux->n1 == curr->n3)
                   { aux->v1 = curr; curr->v2 = aux; break;}
               if (aux->n3 == curr->n2 && aux->n2 == curr->n3)
                   { aux->v2 = curr; curr->v2 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v2)
               {
               aux = curr->next;
               while (aux)
                   {
                   if (aux->n1 == curr->n3 && aux->n3 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v3 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n3 && aux->n1 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v2 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n3 && aux->n2 == curr->n2)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v1 = curr;
                       curr->v2 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }
       if (!curr->v3)
           {
           aux = curr->next;
           while (aux)
               {
	       if (aux->n1 == curr->n3 && aux->n3 == curr->n1)
		   { aux->v3 = curr; curr->v3 = aux; break;}
	       if (aux->n2 == curr->n3 && aux->n1 == curr->n1)
                   { aux->v1 = curr; curr->v3 = aux; break;}
               if (aux->n3 == curr->n3 && aux->n2 == curr->n1)
                   { aux->v2 = curr; curr->v3 = aux; break;}
               aux = aux->next;
               }
           if (!curr->v3)
               {
               aux = curr->next;
               while (aux)
                   {
	           if (aux->n1 == curr->n3 && aux->n3 == curr->n1)
		       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v3 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   if (aux->n2 == curr->n3 && aux->n1 == curr->n1)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v2 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   if (aux->n3 == curr->n3 && aux->n2 == curr->n1)
                       {
                       chori(aux);      // Se asume que chori cambia v1 con v2
                       aux->v1 = curr;
                       curr->v3 = aux;
                       break;
                       }
                   aux = aux->next;
                   }
               }
           }

       if (!skip && (!curr->v1 || !curr->v2 || !curr->v3))
           {
           char buf[280];
           acCoordinate3D &coord = *Coord;
           acPoint3 p1, p2, p3;

           p1 = coord[curr->n1];
           p2 = coord[curr->n2];
           p3 = coord[curr->n3];
           sprintf(buf,
           "Quedo una frontera!\n%lg %lg %lg\n%lg %lg %lg\n%lg %lg %lg\nSkip others?",
            p1.x,p1.y,p1.z,p2.x,p2.y,p2.z,p3.x,p3.y,p3.z);
            Error(WARNING, 1, buf);
            skip = 1;
           }

       curr = curr->next;
       }
//    mFree(nuevo);
	delete []nuevo;
}

void vtkSurfaceGen::chori(SurfTriang *el)
{
    SurfTriang *va;
    long na;
    acQueue Cola(sizeof(SurfTriang *));
    acQueue Colab(sizeof(SurfTriang *));
    Cola.Push(&el);
    Colab.Push(&el);
    el->flg = 1;
    while(Cola.Pop(&el))
        {
        na = el->n1; el->n1 = el->n3; el->n3 = na;
        va = el->v1; el->v1 = el->v2; el->v2 = va;
        va = el->v1;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        va = el->v2;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        va = el->v3;
        if (va && !va->flg)
            {
            Cola.Push(&va);
            Colab.Push(&va);
            va->flg = 1;
            }
        }

    while(Colab.Pop(&el))
        {
        el->flg=0;
        }
}

int vtkSurfaceGen::TestVecDer(long no,acPoint3 p, SurfTriang *el, int ind)
{	
	// Evita que entre num loop infinito nesse método recursivo
	this->recursiveCounter++;
	
	if(recursiveCounter > MAX_ITER)
		{
		this->colapsaError = true;
		recursiveCounter = 0;		
		return(0);
		}
		
	if (ind==0 && el->v1==curr)
		{
		recursiveCounter = 0;
		return(1);
		}		
	if (ind==1 && el->v2==curr)
		{
		recursiveCounter = 0;
		return(1);
		}		

	if (ind==2 && el->v3==curr)		
		{
		recursiveCounter = 0;
		return(1);
		}		

//	if (ind==0 && el->v1 == curr) return(1);
//	if (ind==1 && el->v2 == curr) return(1);
//	if (ind==2 && el->v3 == curr) return(1);

	acPoint3 p1,p2,p3,normv,normn;
	if      (el->n1 == no)
	    {
	    p1 = (*Coord)[el->n1];
	    p2 = (*Coord)[el->n2];
	    p3 = (*Coord)[el->n3];
	    }
	else if (el->n2 == no)
	    {
	    p1 = (*Coord)[el->n2];
	    p2 = (*Coord)[el->n3];
	    p3 = (*Coord)[el->n1];
	    }
	else if (el->n3 == no)
	    {
	    p1 = (*Coord)[el->n3];
	    p2 = (*Coord)[el->n1];
	    p3 = (*Coord)[el->n2];
	    }
	else
	    {
       	Error(WARNING, 1, "Invalid surface");
	    return(0);
	    }

	normv.x = (p2.y-p1.y)*(p3.z-p1.z)-(p2.z-p1.z)*(p3.y-p1.y);
	normv.y = (p2.z-p1.z)*(p3.x-p1.x)-(p2.x-p1.x)*(p3.z-p1.z);
	normv.z = (p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
	normn.x = (p2.y-p.y)*(p3.z-p.z)-(p2.z-p.z)*(p3.y-p.y);
	normn.y = (p2.z-p.z)*(p3.x-p.x)-(p2.x-p.x)*(p3.z-p.z);
	normn.z = (p2.x-p.x)*(p3.y-p.y)-(p2.y-p.y)*(p3.x-p.x);
	if (normv.x*normn.x + normv.y*normn.y + normv.z*normn.z <= 0.0) return(0);

	 // Averigua por donde debe salir y sale
	if (ind ==0)
	    {   	
      if (!el->v1) return 1;
	    if      (el->v1->v1 == el) ind = 1;
	    else if (el->v1->v2 == el) ind = 2;
	    else if (el->v1->v3 == el) ind = 0;
	    else
        {
       	Error(WARNING, 1, "Invalid surface");
        return(0);
        }
	    return(TestVecDer(no, p, el->v1, ind));
	    }
    else if (ind == 1)
      {      	
      if (!el->v2) return 1;			            
	    if      (el->v2->v1 == el) ind = 1;
	    else if (el->v2->v2 == el) ind = 2;
	    else if (el->v2->v3 == el) ind = 0;
	    else
        {
       	Error(WARNING, 1, "Invalid surface");         	
        return(0);
        }
	    return(TestVecDer(no, p, el->v2, ind));
	    }
    else   //ind == 2
      {
      if (!el->v3) return 1;				      
	    if      (el->v3->v1 == el) ind = 1;
	    else if (el->v3->v2 == el) ind = 2;
	    else if (el->v3->v3 == el) ind = 0;
	    else
        {
       	Error(WARNING, 1, "Invalid surface");        	
        return(0);
        }
	    return(TestVecDer(no, p, el->v3, ind));
	    }
}

int vtkSurfaceGen::TestVecIzq(long no,acPoint3 p, SurfTriang *el, int ind)
{
	// Evita que entre num loop infinito nesse método recursivo
	this->recursiveCounter++;
	
	if(recursiveCounter > MAX_ITER)
		{
		this->colapsaError = true;
		recursiveCounter = 0;		
		return(0);
		}
		
	if (ind==0 && el->v1==curr)
		{
		recursiveCounter = 0;
		return(1);
		}		
	if (ind==1 && el->v2==curr)
		{
		recursiveCounter = 0;
		return(1);
		}		

	if (ind==2 && el->v3==curr)		
		{
		recursiveCounter = 0;
		return(1);
		}		
//	if (ind==0 && el->v1 == curr) return(1);
//	if (ind==1 && el->v2 == curr) return(1);
//	if (ind==2 && el->v3 == curr) return(1);

	acPoint3 p1,p2,p3,normv,normn;
	if      (el->n1 == no)
	    {
	    p1 = (*Coord)[el->n1];
	    p2 = (*Coord)[el->n2];
	    p3 = (*Coord)[el->n3];
	    }
	else if (el->n2 == no)
	    {
	    p1 = (*Coord)[el->n2];
	    p2 = (*Coord)[el->n3];
	    p3 = (*Coord)[el->n1];
	    }
	else if (el->n3 == no)
	    {
	    p1 = (*Coord)[el->n3];
	    p2 = (*Coord)[el->n1];
	    p3 = (*Coord)[el->n2];
	    }
	else
	    {
       	Error(WARNING, 1, "Invalid surface");
	    return(0);
	    }

	normv.x = (p2.y-p1.y)*(p3.z-p1.z)-(p2.z-p1.z)*(p3.y-p1.y);
	normv.y = (p2.z-p1.z)*(p3.x-p1.x)-(p2.x-p1.x)*(p3.z-p1.z);
	normv.z = (p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
	normn.x = (p2.y-p.y)*(p3.z-p.z)-(p2.z-p.z)*(p3.y-p.y);
	normn.y = (p2.z-p.z)*(p3.x-p.x)-(p2.x-p.x)*(p3.z-p.z);
	normn.z = (p2.x-p.x)*(p3.y-p.y)-(p2.y-p.y)*(p3.x-p.x);
	if (normv.x*normn.x + normv.y*normn.y + normv.z*normn.z <= 0.0) return(0);

	 // Averigua por donde debe salir y sale
	if (ind ==0)
	    {
        if (!el->v1) return 1;
	    if      (el->v1->v1 == el) ind = 2;
	    else if (el->v1->v2 == el) ind = 0;
	    else if (el->v1->v3 == el) ind = 1;
	    else
		    {
        Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v1, ind));
	    }
	else if (ind == 1)
	    {
        if (!el->v2) return 1;
        if      (el->v2->v1 == el) ind = 2;
	    else if (el->v2->v2 == el) ind = 0;
	    else if (el->v2->v3 == el) ind = 1;
	    else
		    {
           	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v2, ind));
	    }
	else   //ind == 2
	    {
      if (!el->v3) return 1;
	    if      (el->v3->v1 == el) ind = 2;
	    else if (el->v3->v2 == el) ind = 0;
	    else if (el->v3->v3 == el) ind = 1;
	    else
		    {
       	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    return(TestVecIzq(no, p, el->v3, ind));
	    }
}

bool vtkSurfaceGen::IsTriangleOk(SurfTriang *tri)
{
	// Correção adicionada por conta de um erro que ocorria apenas no windows.
	if(!tri) return false;  // elemento é nulo
	if(tri->n1 < 0 || tri->n2 < 0 || tri->n3 < 0 ) return false; // elemento possui vizinho negativos
	return true;
}

int vtkSurfaceGen::FronteraDer(SurfTriang *el, int ind)
{
	if( !this->IsTriangleOk(el) ) return 0;
	
	// Evita que entre num loop infinito nesse método recursivo
	this->recursiveCounter++;
	
	if(recursiveCounter > MAX_ITER)
		{
		this->colapsaError = true;
		recursiveCounter = 0;		
		return(0);
		}	

	 // Averigua por donde debe salir y sale
	int r;
    if (ind ==0)
	    {
      if (!el->v1)              return 0;
	    if (el->v1 == curr)       return 1;
	    if (el->gr != el->v1->gr) return 0;
	    if      (el->v1->v1 == el) ind = 1;
	    else if (el->v1->v2 == el) ind = 2;
	    else if (el->v1->v3 == el) ind = 0;
	    else
		    {
       	Error(WARNING, 1, "Invalid surface");
		    return(0);
		    }
	    r = FronteraDer(el->v1,ind);
	    }
    else if (ind == 1)
        {
        if (!el->v2)              return 0;
        if (el->v2 == curr)       return 1;
        if (el->gr != el->v2->gr) return(0);
        if      (el->v2->v1 == el) ind = 1;
        else if (el->v2->v2 == el) ind = 2;
        else if (el->v2->v3 == el) ind = 0;
        else
	        {
         	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
        r = FronteraDer(el->v2,ind);
        }
    else   //ind == 2
        {
        if (!el->v3)              return 0;
        if (el->v3 == curr)       return 1;
        if (el->gr != el->v3->gr) return(0);
        if      (el->v3->v1 == el) ind = 1;
        else if (el->v3->v2 == el) ind = 2;
        else if (el->v3->v3 == el) ind = 0;
        else
	        {
         	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
        r = FronteraDer(el->v3,ind);
        }
    if (r)  return (r+1);
    else    return 0;
}

int vtkSurfaceGen::FronteraIzq(SurfTriang *el, int ind)
{
	// Evita que entre num loop infinito nesse método recursivo
	this->recursiveCounter++;
	
	if(recursiveCounter > MAX_ITER)
		{
		this->colapsaError = true;
		recursiveCounter = 0;		
		return(0);
		}	
	
	
   // Averigua por donde debe salir y sale
	int r;
    if (ind ==0)
        {
        if (!el->v1)              return 0;
        if (el->v1 == curr)       return 1;
        if (el->gr != el->v1->gr) return 0;
        if      (el->v1->v1 == el) ind = 2;
        else if (el->v1->v2 == el) ind = 0;
        else if (el->v1->v3 == el) ind = 1;
        else
	        {
         	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
        r = FronteraIzq(el->v1,ind);
        }
    else if (ind == 1)
	    {
      if (!el->v2)              return 0;
	    if (el->v2 == curr)       return 1;
	    if (el->gr != el->v2->gr) return 0;
	    if      (el->v2->v1 == el) ind = 2;
	    else if (el->v2->v2 == el) ind = 0;
	    else if (el->v2->v3 == el) ind = 1;
	    else
	        {
         	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraIzq(el->v2,ind);
	    }
    else   //ind == 2
        {
        if (!el->v3)              return 0;
        if (el->v3 == curr)       return 1;
        if (el->gr != el->v3->gr) return 0;
        if      (el->v3->v1 == el) ind = 2;
        else if (el->v3->v2 == el) ind = 0;
        else if (el->v3->v3 == el) ind = 1;
        else
	        {
         	Error(WARNING, 1, "Invalid surface");
	        return(0);
	        }
	    r = FronteraIzq(el->v3,ind);
	    }
    if (r)  return (r+1);
    else    return 0;
}

int vtkSurfaceGen::CambiaNodoIzq (long no, long nn, SurfTriang *el, int ind)
{
    // Renombra el nodo en cuestion
    if      (el->n1 == no) el->n1 = nn;
    else if (el->n2 == no) el->n2 = nn;
    else if (el->n3 == no) el->n3 = nn;
    else
	    {
	    this->colapsaError = true;
	    return -1;
   	  //Error(FATAL_ERROR, 0, "Invalid surface");
	    }
    // Averigua por donde debe salir y sale
    if (ind ==0)
	    {
        if (!el->v1) return 0;
        if (el->v1 == curr) return 1;
        if      (el->v1->v1 == el) ind = 2;
        else if (el->v1->v2 == el) ind = 0;
        else if (el->v1->v3 == el) ind = 1;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoIzq(no,nn,el->v1,ind);
    	}
    else if (ind == 1)
        {
        if (!el->v2) return 0;
        if (el->v2 == curr) return 1;
        if      (el->v2->v1 == el) ind = 2;
        else if (el->v2->v2 == el) ind = 0;
        else if (el->v2->v3 == el) ind = 1;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoIzq(no,nn,el->v2,ind);
        }
    else   //ind == 2
	    {
        if (!el->v3) return 0;
        if (el->v3 == curr) return 1;
        if      (el->v3->v1 == el) ind = 2;
        else if (el->v3->v2 == el) ind = 0;
        else if (el->v3->v3 == el) ind = 1;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoIzq(no,nn,el->v3,ind);
      }
}


int vtkSurfaceGen::CambiaNodoDer (long no, long nn, SurfTriang *el, int ind)
{
    // Renombra el nodo en cuestion
    if      (el->n1 == no) el->n1 = nn;
    else if (el->n2 == no) el->n2 = nn;
    else if (el->n3 == no) el->n3 = nn;
    else
	    {
	    this->colapsaError = true;
	    return -1;
   	  //Error(FATAL_ERROR, 0, "Invalid surface");
	    }
    // Averigua por donde debe salir y sale
    if (ind ==0)
	    {
        if (!el->v1)        return 0;
        if (el->v1 == curr) return 1;
        if      (el->v1->v1 == el) ind = 1;
        else if (el->v1->v2 == el) ind = 2;
        else if (el->v1->v3 == el) ind = 0;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoDer(no,nn,el->v1,ind);
    	}
    else if (ind == 1)
        {
        if (!el->v2) return 0;
        if (el->v2 == curr) return 1;
        if      (el->v2->v1 == el) ind = 1;
        else if (el->v2->v2 == el) ind = 2;
        else if (el->v2->v3 == el) ind = 0;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoDer(no,nn,el->v2,ind);
        }
    else   //ind == 2
	    {
        if (!el->v3) return 0;
        if (el->v3 == curr) return 1;
        if      (el->v3->v1 == el) ind = 1;
        else if (el->v3->v2 == el) ind = 2;
        else if (el->v3->v3 == el) ind = 0;
		    else
			    {
			    this->colapsaError = true;
			    return -1;
		   	  //Error(FATAL_ERROR, 0, "Invalid surface");
			    }
    	return CambiaNodoDer(no,nn,el->v3,ind);
      }
}

void vtkSurfaceGen::SwapGroup(int num)
{
  long na;
  SurfTriang *el;
  curr = first;
	//  num--; Para que o primeiro grupo seja realmente o zero
  while (curr)
		{
      if (curr->gr == num)
		    {
		    el = curr->v3;
		    curr->v3 = curr->v2;
		    curr->v2 = el;
        na = curr->n1;
        curr->n1 = curr->n2;
        curr->n2 = na;
        curr->isNew = 1;
        }
     curr = curr->next;
     }
}

void  vtkSurfaceGen::Information(long *nod, long *nel, long *grp)
{
    double informationProgress = 0.0;
    *nod = Coord->GetNumNodes();
    *nel = 0; *grp = 0;
    curr=first;
    
    while(curr)
        {
        *nel = *nel+1;
        if (curr->gr > *grp) *grp = curr->gr;
        curr=curr->next;
        }
    *grp = *grp+1;
}

void  vtkSurfaceGen::GroupInformation(long *numElemGroup, long *numNodesGroup)
{
	int i,k;
  long numCoords = Coord->GetNumNodes();
  
  // Cria vetor temporário com o número de grupos
  int *nodeVec = new int[numCoords];

	// Inicia busca de elementos e nós por grupo
	for(i = 0; i < this->NumGroups; i++)
		{
		numElemGroup[i] = 0;
		numNodesGroup[i] = 0;
			
		// Zera vetor temporário de nós
		for( k = 0; k < numCoords; k++) nodeVec[k] = 0;
		
		// Acessa número de elementos de cada grupo
		numElemGroup[i] = this->GetNumElsGroups(i);	

		// Calculando número de nós por elemento			
    curr=first;			
    while(curr)
			{
      if (curr->gr == i)
       	{
      	nodeVec[curr->n1] = 1;  	
      	nodeVec[curr->n2] = 1;
      	nodeVec[curr->n3] = 1;	       	    		      	
       	}
        curr=curr->next;       	
      }
      
		// Conta número de nós visitados para esse grupo
		for( k = 0; k < numCoords; k++) 
			if(nodeVec[k]) 
				numNodesGroup[i]++;      
		}
		
	delete [] nodeVec;
}


void vtkSurfaceGen::Smooth(int iter, double fac, int shr, int Gr, int enableRemoveNeedle)
{
	int retrieve[5];
	
	// Removing inicial needles
	if(enableRemoveNeedle)
		this->RemoveNeedles(5, retrieve);
	
	double progressSmooth = 0.12;
	long n;
	long nod     = Coord->GetNumNodes();
	long *num    = new long[nod];
	acPoint3 *np = new acPoint3[nod];
	  
	for (n=0; n<nod; n++) num[n]=0;
	
	this->SetProgress(progressSmooth);
	this->UpdateProgress(progressSmooth);
	
	int numTri = 0;
	
	curr=first;    
	while (curr)        //Marco los que no se pueden mover
	  {
//	  if (Gr && (Gr-1)!=curr->gr) // Estava assim quando o valor para "todos" era 0.
	  if (Gr != -1 && Gr != curr->gr) 
			{
			num[curr->n1]=num[curr->n2]=num[curr->n3]=-1;
			curr=curr->next; 
			continue;
			}
	  if (!curr->v1 || curr->v1->gr != curr->gr)
	      num[curr->n1]=num[curr->n2]=-1;
	  if (!curr->v2 || curr->v2->gr != curr->gr)
	      num[curr->n2]=num[curr->n3]=-1;
	  if (!curr->v3 || curr->v3->gr != curr->gr)
	      num[curr->n3]=num[curr->n1]=-1;
	  curr=curr->next;
	  numTri++;
	  }
	
	acPoint3 p1,p2,p3;
	if (shr) iter*=2;
	double fac2,fac1=fac;
	if (shr) fac2=1.0/(0.1-1.0/fac);
	else     fac2=fac;
			
	numTri = numTri * iter;
	int localNumTri = 0;			
			
	for (int it=0; it<iter; it++)
		{	
	  this->UpdateProgress(progressSmooth);
	     
	  for (n=0; n<nod; n++) 
	  	{
	    if (num[n]==-1) continue;
			num[n]=0;
			np[n].x=np[n].y=np[n].z=0.0;
	    }
	  curr=first;
	  while (curr)        //Calculo nuevas coordenadas
	  	{
	    //progressSmooth = fabs((((progressSmooth + 0.07*(1.0 - progressSmooth)) - it)/iter));
		 progressSmooth = double (localNumTri/ double(numTri) );	    
	    this->SetProgress(progressSmooth);
	    this->UpdateProgress(progressSmooth);
	    localNumTri++;
	     	 
	    p1=(*Coord)[curr->n1];
	    p2=(*Coord)[curr->n2];
	    p3=(*Coord)[curr->n3];
	    n = curr->n1;
	    if (num[n]!=-1)
	    	{
	      np[n].x = np[n].x + p2.x + p3.x;
	      np[n].y = np[n].y + p2.y + p3.y;
	      np[n].z = np[n].z + p2.z + p3.z;
	      num[n]+=2;
	      }
	    n = curr->n2;
	    if (num[n]!=-1)
	    	{
	      np[n].x = np[n].x + p3.x + p1.x;
	      np[n].y = np[n].y + p3.y + p1.y;
	      np[n].z = np[n].z + p3.z + p1.z;
	      num[n]+=2;
	      }
	    n = curr->n3;
	    if (num[n]!=-1)
				{
				np[n].x = np[n].x + p1.x + p2.x;
				np[n].y = np[n].y + p1.y + p2.y;
				np[n].z = np[n].z + p1.z + p2.z;
				num[n]+=2;
				}
	    curr=curr->next;
	    }
	
	    if ((it/2)*2==it) fac=fac1;
	    else              fac=fac2;
	    for (n=0; n<nod; n++)
				{
				if (num[n]!=-1 && num[n])
					{
					p1   = (*Coord)[n];
					p1.x = p1.x + fac*(np[n].x/num[n]-p1.x);
					p1.y = p1.y + fac*(np[n].y/num[n]-p1.y);
					p1.z = p1.z + fac*(np[n].z/num[n]-p1.z);
					(*Coord)[n]=p1;
					}
				}
	  }
				
	progressSmooth = 1.0;
	this->SetProgress(progressSmooth);
	this->UpdateProgress(progressSmooth);
	
	delete []np;
	delete []num;
}

void vtkSurfaceGen::SelectElement(long *elem, int seltype, int inttype, int add, int gr,
              double x,double y,double z,double nx,double ny,double nz, double r)
{
    acPoint3 p1,p2,p3;
    double d,dis;
    curr=first;
    long el=-1;
    switch (seltype)
        {
        case 0:
            while(curr)
                {
                elem[++el]=add;
                curr=curr->next;
                }
            break;
        case 1:
            d = -(nx*x + ny*y + nz*z);
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        dis= nx*p1.x + ny*p1.y + nz*p1.z + d;
                if (dis>0 && inttype) { elem[el]=add; continue; }
                else if (dis<0 && !inttype) continue;
		        dis= nx*p2.x + ny*p2.y + nz*p2.z + d;
                if (dis>0 && inttype) { elem[el]=add; continue; }
                else if (dis<0 && !inttype) continue;
		        dis= nx*p3.x + ny*p3.y + nz*p3.z + d;
                if (dis>0 )             elem[el]=add;
                }
            break;
        case 2:
            while (curr)
                {
                el++;
                if (curr->gr==gr) elem[el]=add;
                curr=curr->next;
                }
            break;
        case 3:
            r=r*r;
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        dis= (p1.x-x)*(p1.x-x)+(p1.y-y)*(p1.y-y)+(p1.z-z)*(p1.z-z);
                if (dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        dis= (p2.x-x)*(p2.x-x)+(p2.y-y)*(p2.y-y)+(p2.z-z)*(p2.z-z);
                if (dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        dis= (p3.x-x)*(p3.x-x)+(p3.y-y)*(p3.y-y)+(p3.z-z)*(p3.z-z);
                if (dis<r)              elem[el]=add;
                }
            break;
        case 4:
            dis = sqrt(nx*nx + ny*ny + nz*nz);
            if (dis==0.0) break;
            nx /= dis; ny /= dis; nz /= dis;
            r=r*r;
            double px,py,pz;
            while (curr)
                {
        		p1 = (*Coord)[curr->n1];
        		p2 = (*Coord)[curr->n2];
        		p3 = (*Coord)[curr->n3];
                curr=curr->next;
                el++;
		        px = (p1.y-y)*nz - (p1.z-z)*ny;
		        py = (p1.z-z)*nx - (p1.x-x)*nz;
		        pz = (p1.x-x)*ny - (p1.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        px = (p2.y-y)*nz - (p2.z-z)*ny;
		        py = (p2.z-z)*nx - (p2.x-x)*nz;
		        pz = (p2.x-x)*ny - (p2.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r && inttype) { elem[el]=add; continue; }
                else if (dis>r && !inttype) continue;
		        px = (p3.y-y)*nz - (p3.z-z)*ny;
		        py = (p3.z-z)*nx - (p3.x-x)*nz;
		        pz = (p3.x-x)*ny - (p3.y-y)*nx;
        		dis= px*px + py*py + pz*pz;
                if(dis<r)              elem[el]=add;
                }
            break;

        default:
            break;
        }
}

void vtkSurfaceGen::SetGroup(long *elem, int gr)
{
    int i;
    if (NumGroups<gr+1) NumGroups=gr+1;
//    long *msw= (long *)mMalloc(sizeof(long)*NumGroups);
    long *msw = new long[NumGroups];    
    for (i=0; i<NumGroups; i++)
        msw[i]=0;

    long el=0;
    curr=first;
    while(curr)
        {
        if (elem[el++]) 
            curr->gr=gr;
        msw[curr->gr]=1;
        curr=curr->next;
        }

    for (i=1; i<NumGroups; i++)
        msw[i]+=msw[i-1];
    NumGroups=msw[NumGroups-1];

    curr=first;
    while(curr)
        {
        curr->gr=msw[curr->gr]-1;
        curr=curr->next;
        }
    // !!! Essa deleção não tinha na versão original
    delete []msw;
}

void vtkSurfaceGen::MergeGroups(int id1, int id2)
{
	curr=first;
	while(curr)
  		{
		// Se encontro algum elemento do segundo grupo, renumera para o primeiro
		if (curr->gr == id2)
			curr->gr = id1;
		curr=curr->next;
  		}    
	this->UpdateNumElsGroupsArray();    
}

long vtkSurfaceGen::RemoveGroup(long gr)
{
	//    gr--;
  curr=first;
  while(curr)
      {
      if (curr->gr==gr)
          {
          PutNullVec(curr,gr);
          if (curr->next)
              {
              curr=curr->next;
              Delete(curr->prev);
              }
          else
              {
              Delete(curr);
              curr=0;
              }
          continue;
          }
      else if (curr->gr>gr) 
          curr->gr=curr->gr-1;
      curr=curr->next;
      }
  this->NumGroups--;  
  this->UpdateNumElsGroupsArray();    
  return RemoveFreeNodes();
}

void vtkSurfaceGen::PutNullVec(SurfTriang *el, long gr)
{
  if (el->v1)
    {
    if      (el->v1->v1==el) el->v1->v1=0;
    else if (el->v1->v2==el) el->v1->v2=0;
    else if (el->v1->v3==el) el->v1->v3=0;
    }
  if (el->v2)
    {
    if      (el->v2->v1==el) el->v2->v1=0;
    else if (el->v2->v2==el) el->v2->v2=0;
    else if (el->v2->v3==el) el->v2->v3=0;
    }
  if (el->v3)
    {
    if      (el->v3->v1==el) el->v3->v1=0;
    else if (el->v3->v2==el) el->v3->v2=0;
    else if (el->v3->v3==el) el->v3->v3=0;
    }
}

void vtkSurfaceGen::AddCapa(long gr1,long gr2)
{
}

void vtkSurfaceGen::Transform(double tx, double ty, double tz,
                           double sx, double sy, double sz,
                           double rx, double ry, double rz)
{
  acPoint3 p;
  acCoordinate3D &c=*Coord;
  
  this->SetProgress(0.40);
  this->UpdateProgress(0.40);
  
  for (long n=0; n<Coord->GetNumNodes(); n++)
      {
      
      this->SetProgress(0.85);
      this->UpdateProgress(0.85);
      
      p=c[n];
      p.x+=tx; p.y+=ty; p.z+=tz;
      p.x*=sx; p.y*=sy; p.z*=sz;
      c[n]=p;
      }
      
  this->SetProgress(1.0);
  this->UpdateProgress(1.0);
}

//------------------------------------------------------------
int vtkSurfaceGen::PoneTapas()
{
	int newGroups = 0;
	int *a1 = new int[Coord->GetNumNodes()];	
	typedef SurfTriang* SurfPointer;
	SurfPointer *ae = new SurfPointer[Coord->GetNumNodes()];

	for (int k=0; k<Coord->GetNumNodes(); k++)
		a1[k]=-1;

	this->SetProgress(0.43);
	this->UpdateProgress(0.43);

	curr = first;	
	while(curr)
	{
		if(!curr->v1)
			{a1[curr->n1]=curr->n2; ae[curr->n1]=curr;}
		if(!curr->v2)
			{a1[curr->n2]=curr->n3; ae[curr->n2]=curr;}
		if(!curr->v3)
			{a1[curr->n3]=curr->n1; ae[curr->n3]=curr;}
		curr = curr->next;
	}

	acCoordinate3D &c=*Coord;
	int j = 0, oldnodes = Coord->GetNumNodes();
	
	this->SetProgress(0.85);
	this->UpdateProgress(0.85);
	
	while (j<oldnodes)
	{
		while(a1[j]==-1 && j<oldnodes) j++;
		if (j >= oldnodes) break;

		SurfTriang *primero;
		SurfTriang *prev   = new SurfTriang;
		SurfTriang *actual = new SurfTriang;
		primero = actual;
		int nn = Coord->GetNumNodes();
		acPoint3 p,pc;
		
		int num=0;
		pc.x=pc.y=pc.z=0.0;
		
		while(a1[j]!=-1)
		{	
			p = c[j];
			num++;
			pc.x+= p.x;
			pc.y+= p.y;
			pc.z+= p.z;
			SurfTriang *prox = new SurfTriang;
			if(ae[j]->n1==j) ae[j]->v1 = actual;
			if(ae[j]->n2==j) ae[j]->v2 = actual;
			if(ae[j]->n3==j) ae[j]->v3 = actual;

			actual->SetData(a1[j], j, nn, NumGroups, ae[j], prev, prox);
			InsertFirst(actual);
			prev   = actual;

			actual = prox;
			int n = a1[j];
			a1[j] = -1;
			j = n;
		}
		prev->v3 = primero;
		primero->v2 = prev;
		NumGroups++;
		newGroups++;
		pc.x/=num;
		pc.y/=num;
		pc.z/=num;
		Coord->AddCoord(pc);
	}
	
	this->SetProgress(1.0);
	this->UpdateProgress(1.0);

	delete []a1;
	delete []ae;

	this->UpdateNumElsGroupsArray();
	
	return newGroups;
	
	/*
	 * MOVI ESTA ATUALIZAÇÃO INTERNA PARA O vtkHM3DTrisurfFilter
	 */
	//	// Cria malha 3d temporária com as informações de grupo adicionadas nesse método
	//	vtkMesh3d *mesh = vtkMesh3d::New();
	//	mesh->Update(this);
	//	// Reconstroi superfície a partir desta malha	
	//	this->Copy(mesh, MP);
	//	// Remove malha temporária.
	//	mesh->Delete();				
}

//------------------------------------------------------------
void vtkSurfaceGen::UpdateNumElsGroupsArray()
{
	// Remove e realoca vetor com o número de elementos por grupo após criação de grupos
	delete[] this->NumElsGroups;	
  this->NumElsGroups = new int[this->NumGroups]; 

	// Zera Vetor recém alocado
	for(int i = 0; i < this->NumGroups; i++) this->NumElsGroups[i] = 0; 

	// Popula vetor realocado com novo número de elementos por grupo
  curr=first;				
	while(curr)
		{
		this->NumElsGroups[ curr->gr ]++; 
    curr=curr->next;       	
    }  
}

//------------------------------------------------------------
// Conta quantos triângulos de borda (superfície aberto) há em uma superfície
int vtkSurfaceGen::CountBorderTriangles(SurfTriang *tri)
{
	int counter = 0;
	while(tri)
		{
		if(!tri->v1 || !tri->v2 || !tri->v3) counter++;
		tri = tri->next;
		}
	return counter;
}
//------------------------------------------------------------
int vtkSurfaceGen::CheckOrientation(vtkMesh3d *mesh, vtkParamMesh *mpar)
{
	// Cria superfície temporária
	vtkSurfaceGen *tempSurf = vtkSurfaceGen::New();
	
	// Copia dados do MeshData em memória
	tempSurf->Copy(mesh, mpar);
	
	// Este método remove o menor grupo da superfície (supondo que ela esteja fechada)
	// e calcula a direção do vetor normal desta borda.
	// Retorna 1 se os vetores estiverem apontando para fora (devem apontar para dentro para o Dela3d).
	int orientationProblem = tempSurf->CheckOrientation();	
	
	// Remove superfície temporária
	tempSurf->Delete();
	
	return orientationProblem;
}

//------------------------------------------------------------
int vtkSurfaceGen::CheckOrientation()
{
	// Verifica se a superfície está ou não aberta
	int i, counter;
	counter = this->CountBorderTriangles(this->first);

	// Superfície é fechada e contem um grupo, não será verificada
	if(this->GetNumGroups() == 1 && counter == 0)
			return 2;			
			
	// Se não há triângulos de borda, remover um dos grupos da superfície temporária
	if(!counter)
		{
		int menorGrupo 				= 0;
		int numElementosGrupo = this->GetNumElsGroups(0);	
		for(i = 1; i < this->GetNumGroups(); i++)
			{
			if(this->GetNumElsGroups(i) < numElementosGrupo)
				{
					numElementosGrupo = this->GetNumElsGroups(i);
					menorGrupo = i;
				}			
			}
		cout << "    * Removing group with fewer elements from the temporary surface (Group " << 
		menorGrupo << " with " << numElementosGrupo << " cells)." <<endl;			
		
		// Remove menor grupo
	  this->RemoveGroup(menorGrupo);
		
		// Conta novamente número de triangulos de borda após remoção
		counter = this->CountBorderTriangles(this->first);		
		}

	// Armazena triângulos de borda
	typedef SurfTriang* SurfPointer;	
	SurfPointer *ae = new SurfPointer[counter];
	i = 0;
	this->curr = this->first;
	while(this->curr)
		{
		if(!this->curr->v1 || !this->curr->v2 || !this->curr->v3) 
			ae[i++] = this->curr;
		// Como já se sabe o número de elementos na borda, parar loop se chegar nesse valor
		if(i==counter) break;
		this->curr = this->curr->next;
		}

 	//Armazena triângulos de borda e calcula vetor normal
	acPoint3 pm;	
	acCoordinate3D &c = *Coord;	
	for(i = 0; i < counter; i++)
		{
		pm.x += c[ ae[i]->n1].x;				
		pm.y += c[ ae[i]->n1].y;
		pm.z += c[ ae[i]->n1].z;						
		}
	pm.x/=counter;
	pm.y/=counter;
	pm.z/=counter;

 	// Calcula o vetor normal dos triangulos de borda
	acPoint3 p[3], vetnormal, vetPoint;	
	double angle = 0;
	for(i = 0; i < counter; i++)
		{			
		// Calcula normal do triangulo		
		p[0] = c[ ae[i]->n1];
		p[1] = c[ ae[i]->n2];				
		p[2] = c[ ae[i]->n3];		
		vetnormal = NormalTri(p);	
		//printf("\nVetNormal[%d] : {%.2f, %.2f, %.2f}", i, vetnormal.x, vetnormal.y, vetnormal.z);
		// Calcula vetor entro ponto interno e a face
		vetPoint.x = pm.x - p[0].x;
		vetPoint.y = pm.y - p[0].y;
		vetPoint.z = pm.z - p[0].z;
		//printf("\nVetPoint[%d] : {%.2f, %.2f, %.2f}", i, vetPoint.x, vetPoint.y, vetPoint.z);		
		angle += ComputeVectorAngle(vetnormal, vetPoint);		
		}

	delete []ae;
		
	// Compute final angle
	angle /= counter;	
	
	// Se angulo final entre a normal das faces e o ponto central
	// for menor que 90, normais estão viradas para dentro		
	return (angle < 90) ? 0 : 1;
}

//------------------------------------------------------------
bool vtkSurfaceGen::IsTriangleInside(SurfTriang* triangle, double *center, double radius)
{
	// Armazenará as coordenas x, y, z dos três vértices do triângulo
	// a ser testado
	double coords[3][3];
	
	// Armazena coordenadas do triângulo
	this->GetCoord(triangle->n1, coords[0]);
	this->GetCoord(triangle->n2, coords[1]);
	this->GetCoord(triangle->n3, coords[2]);	

	// Checa distância entre pontos do triângulo e raio da esfera (em relação ao seu centro)
	if( sqrt( pow(coords[0][0] - center[0], 2) + pow(coords[0][1] - center[1], 2) + pow(coords[0][2] - center[2], 2)) > radius ) return false;
	if( sqrt( pow(coords[1][0] - center[0], 2) + pow(coords[1][1] - center[1], 2) + pow(coords[1][2] - center[2], 2)) > radius ) return false;
	if( sqrt( pow(coords[2][0] - center[0], 2) + pow(coords[2][1] - center[1], 2) + pow(coords[2][2] - center[2], 2)) > radius ) return false;	

	// Se passou pelos 3 testes, retorna verdadeiro
	return true;
}

//------------------------------------------------------------
bool vtkSurfaceGen::IsTriangleInside(SurfTriang* triangle, double *bounds)
{
	// Armazenará as coordenas x, y, z dos três vértices do triângulo
	// a ser testado
	double coords[3][3];
	
	// Utilizado para resolver problema de aproximação numérica
	// com a bounding box
	double offset = 0.01;
	
	// Armazena coordenadas do triângulo
	this->GetCoord(triangle->n1, coords[0]);
	this->GetCoord(triangle->n2, coords[1]);
	this->GetCoord(triangle->n3, coords[2]);		
	
	// Testa coordenadas em X
	if(coords[0][0]+offset < bounds[0] || coords[0][0]-offset > bounds[1] ||
		coords[1][0]+offset < bounds[0] || coords[1][0]-offset > bounds[1] ||
		coords[2][0]+offset < bounds[0] || coords[2][0]-offset > bounds[1])	
		return false;
		
	// Passou do X. Testa Y		
	if(coords[0][1]+offset < bounds[2] || coords[0][1]-offset > bounds[3] ||
		coords[1][1]+offset < bounds[2] || coords[1][1]-offset > bounds[3] ||
		coords[2][1]+offset < bounds[2] || coords[2][1]-offset > bounds[3])	
		return false;

	// Passou de X e Y. Testa Z
	if(coords[0][2]+offset < bounds[4] || coords[0][2]-offset > bounds[5] ||
		coords[1][2]+offset < bounds[4] || coords[1][2]-offset > bounds[5] ||
		coords[2][2]+offset < bounds[4] || coords[2][2]-offset > bounds[5])	
		return false;

	// Se passou pelos 3 testes, retorna verdadeiro
	return true;		
}

//------------------------------------------------------------
int vtkSurfaceGen::CreateGroupFromSelection(int type, double *data)
{
	int num = 0; // Conta número de elementos que comporão o novo grupo
	bool groupCreated = false;	
	bool IsInside;
	
	cout << "vtkSurfaceGen::CreateGroupFromSelection() --> Type: " << type << endl;

	curr=first;
	while(curr)
  	{
    curr->isNew = 0;
    IsInside = false;
    
    // Box Widget. Testa se o triângulo corrente está dentro ou fora do box (bounds)
    if(type == 1)
		  if ( this->IsTriangleInside(curr, data) ) IsInside = true;

    // Sphere Widget. Testa se triângulo está dentro ou fora da esfera (as 3 primeiras coordenas são o centro e a 4ª o raio)
		if(type == 2)
			if ( this->IsTriangleInside(curr, data, data[3]) ) IsInside = true;

  	// Se estiver dentro e for o primeiro elemento,
  	// incrementar número de grupos
  	if(!groupCreated && IsInside)
	  	{
	  	this->NumGroups++;
	  	groupCreated = true;
	  	this->Shells++;
	  	}		
		
  	// Apenas agrega a novos grupos triângulos que se encontram na casca
  	// Compara com "Shells-2" pois este índice foi incrementado anteriormente, mas ainda não usado
  	if(curr->gr <= this->Shells-2 && IsInside)
  		{
  		curr->gr = this->NumGroups-1; // Tem o -1 pois os grupos são contados a partir de 0.
  		num++;
  		}
    curr=curr->next;
    }	

	// So atualiza número de elementos por grupo se um novo grupo foi criado
	if(groupCreated) this->UpdateNumElsGroupsArray();

	// Testa se número de grupos da casca é igual ao número total de grupos.
	// Não sendo, os grupos da casca deverão ser os primeiros, agrupados.
	if(this->Shells != this->NumGroups && groupCreated)
		{
		curr=first;
		while(curr)
	  	{
	  	// Muda numeração dos grupos. 
	  	// Se o grupo é novo, troca valor para próxima casca. Tampas com esse valor serão trocadas para o último grupo
	  	if(curr->gr == this->NumGroups-1)   
	  		{
	  		curr->gr = this->Shells-1;
				curr->isNew = 1; // Para poder ver evidenciado o grupo certo
	  		}
	  	else if(curr->gr == this->Shells-1) curr->gr = this->NumGroups-1;
	    curr=curr->next;
	  	}
		}
	// Armazena índice do novo grupo
	this->LastCreatedGroupIndex = this->Shells-1;
	
	return num;	
}

bool vtkSurfaceGen::isNeedle(double angle, SurfTriang *current, double *retrieveAngle)
{
	acPoint3 vec[2];	
	double p[3][3];
	float calcAngle[3];

	this->GetCoord(current->n1, p[0]);
	this->GetCoord(current->n2, p[1]);	
	this->GetCoord(current->n3, p[2]);	
	
	vec[0].x = p[0][0] - p[2][0];		
	vec[0].y = p[0][1] - p[2][1];
	vec[0].z = p[0][2] - p[2][2];
	vec[1].x = p[1][0] - p[2][0];
	vec[1].y = p[1][1] - p[2][1];
	vec[1].z = p[1][2] - p[2][2];
	calcAngle[0] = ComputeVectorAngle( vec[0], vec[1]);

	vec[0].x = p[0][0] - p[1][0];		
	vec[0].y = p[0][1] - p[1][1];
	vec[0].z = p[0][2] - p[1][2];
	vec[1].x = p[2][0] - p[1][0];
	vec[1].y = p[2][1] - p[1][1];
	vec[1].z = p[2][2] - p[1][2];
	calcAngle[1] = ComputeVectorAngle( vec[0], vec[1]);

	vec[0].x = p[2][0] - p[0][0];		
	vec[0].y = p[2][1] - p[0][1];
	vec[0].z = p[2][2] - p[0][2];
	vec[1].x = p[1][0] - p[0][0];
	vec[1].y = p[1][1] - p[0][1];
	vec[1].z = p[1][2] - p[0][2];
	calcAngle[2] = ComputeVectorAngle( vec[0], vec[1]);

	// Calcula o menor ângulo para retornar a quem chamou
	if( (calcAngle[0] < calcAngle[1]) && (calcAngle[0] < calcAngle[2]) )
		*retrieveAngle = calcAngle[0];
	else if( calcAngle[1] < calcAngle[2] )
		*retrieveAngle = calcAngle[1];
	else		
		*retrieveAngle = calcAngle[2];
		
	if(fabs(calcAngle[0]) <= angle || 
		 fabs(calcAngle[1]) <= angle || 
		 fabs(calcAngle[2]) <= angle ) 
		{
		current->isNew = 1;
		return 1;
		}
	return 0;
		
}

//------------------------------------------------------------
long vtkSurfaceGen::ViewNeedles(double angle,char *ids)
{	
	int num 	= 0; // Conta número de elementos que comporão o novo grupo
	int index = 0;
	int inserted = 0;
	char aux[100];
	double needleAngle;
	curr=first;
	while(curr)
  	{
  	if ( this->isNeedle( angle, curr, &needleAngle ) )
	  	{
	  	num++;		 	  	
	  	// Limita número de Ids que aparecerão na interface em 50
	  	if(num < 50)
	  		{
	  		sprintf(aux, "\nId: %4d, Angle: %.2lf", index, needleAngle);
	  		strcat(ids, aux); 		
	  		}
	  	}
		curr=curr->next;			  		
		index++;
    }	

	// Cria um warning com os outros ids se número de agulhas for maior que 50
	if(num > 50)
		{
		strcpy(ids, "\nToo many indexes. See Warning window.");
		char *warningIds = new char[num * 30]; // Estimando como 20 o tamanho máximo de cada linha
		strcpy(warningIds, "");
		index = 0;	
		curr=first;			
		while(curr)
	  	{
	  	if ( this->isNeedle( angle, curr, &needleAngle ) )
		  	{
	  		sprintf(aux, "\nId: %6d, Angle: %.2lf", index, needleAngle);
	  		strcat(warningIds, aux); 		
		  	}
			curr=curr->next;			  		
			index++;
	    }	
	 	vtkWarningMacro(<< warningIds);    
		}		  
	return num;		
}

//------------------------------------------------------------
void vtkSurfaceGen::ChangeNeighborsCoords(int m1, int m2) 
{
	curr=first;
	while(curr)
	  	{
	  	if      ( curr->n1 == m1 ) curr->n1 = m2;
	  	else if ( curr->n2 == m1 ) curr->n2 = m2;
	  	else if ( curr->n3 == m1 ) curr->n3 = m2; 	  	
	  	curr=curr->next;			  		
	  	}	
}

//------------------------------------------------------------
void vtkSurfaceGen::GetTriangleEdgeGlobalInformation(double *average, double *smaller, double *greater)
{
	double p[3][3];	
	double dist[3], laverage = 0.0, lgreater, lsmaller;
	int numEdges = 0;

	// Inicializa maior e menor com primeira aresta do primeiro triângulo
	this->GetCoord(first->n1, p[0]);
	this->GetCoord(first->n2, p[1]);	
	dist[0] = sqrt( pow(p[0][0] - p[1][0], 2) + pow(p[0][1] - p[1][1], 2) + pow(p[0][2] - p[1][2], 2) );
	lgreater = lsmaller = dist[0];	

	curr=first;
	while(curr)
		{	
		// Pega coordenadas do triângulo corrente
		this->GetCoord(curr->n1, p[0]);
		this->GetCoord(curr->n2, p[1]);	
		this->GetCoord(curr->n3, p[2]);		
	
		// Calcula tamanho de cada aresta
		dist[0] = sqrt( pow(p[0][0] - p[1][0], 2) + pow(p[0][1] - p[1][1], 2) + pow(p[0][2] - p[1][2], 2) );
		dist[1] = sqrt( pow(p[0][0] - p[2][0], 2) + pow(p[0][1] - p[2][1], 2) + pow(p[0][2] - p[2][2], 2) );
		dist[2] = sqrt( pow(p[1][0] - p[2][0], 2) + pow(p[1][1] - p[2][1], 2) + pow(p[1][2] - p[2][2], 2) );	

		// Compara com maior e menor previamente calculados
		for(int i = 0; i < 3; i++)
			{
			if(dist[i] > lgreater) lgreater = dist[i];
			if(dist[i] < lsmaller) lsmaller = dist[i];
			laverage += dist[i];
			}
		numEdges+=3; // Pois cada triângulo possui 3 arestas...
		curr=curr->next;			
		}			
		
	*greater = lgreater;
	*smaller = lsmaller;
	if(numEdges)
	*average = laverage / (double) numEdges;
}

//------------------------------------------------------------
int vtkSurfaceGen::GetEdgesSize(SurfTriang *current, double *dist)
{
	double p[3][3];	
	
	// Pega coordenadas do triângulo corrente
	this->GetCoord(current->n1, p[0]);
	this->GetCoord(current->n2, p[1]);	
	this->GetCoord(current->n3, p[2]);		
	
	// Calcula tamanho de cada aresta
	dist[0] = sqrt( pow(p[0][0] - p[1][0], 2) + pow(p[0][1] - p[1][1], 2) + pow(p[0][2] - p[1][2], 2) );
	dist[1] = sqrt( pow(p[0][0] - p[2][0], 2) + pow(p[0][1] - p[2][1], 2) + pow(p[0][2] - p[2][2], 2) );
	dist[2] = sqrt( pow(p[1][0] - p[2][0], 2) + pow(p[1][1] - p[2][1], 2) + pow(p[1][2] - p[2][2], 2) );	
	
	if(dist[0] <= dist[1] && dist[0] <= dist[2])	// Menor aresta é a 1-2
		return 0;
	else if(dist[1] <= dist[2])						// Menor aresta é a 1-3
		return 1;
	else														// Menor aresta é a 2-3
		return 2;
}

//------------------------------------------------------------
void vtkSurfaceGen::GetNeedleSmallerEdgeCoords(SurfTriang *current, int *menorArestaCoord1, int *menorArestaCoord2)
{
	double dist[3];	
	this->GetEdgesSize(current, dist);

	// Armazena coordenadas da menor aresta
	if(dist[0] <= dist[1] && dist[0] <= dist[2])
		{
		// Aresta 0 e 1 é maior			
		*menorArestaCoord1 = current->n1;
		*menorArestaCoord2 = current->n2;		
		}
	else if(dist[1] <= dist[2])
		{
		// Aresta 0 e 2 é maior			
		*menorArestaCoord1 = current->n1;
		*menorArestaCoord2 = current->n3;				
		}
	else
		{
		// Aresta 1 e 2 é maior			
		*menorArestaCoord1 = current->n2;
		*menorArestaCoord2 = current->n3;						
		}
}

//------------------------------------------------------------
// Procura vizinho que compartilha aresta de coordenadas c1 e c2
SurfTriang* vtkSurfaceGen::GetEdgeNeighbor(SurfTriang *t, int c1, int c2)
{
	// Testa primeiro vizinho
	if(t->v1)
		if( (t->v1->n1 == c1 || t->v1->n2 == c1 || t->v1->n3 == c1) && 
			 (t->v1->n1 == c2 || t->v1->n2 == c2 || t->v1->n3 == c2) )
			return t->v1; 		

	// Testa segundo vizinho
	if(t->v2)
		if( (t->v2->n1 == c1 || t->v2->n2 == c1 || t->v2->n3 == c1) && 
			 (t->v2->n1 == c2 || t->v2->n2 == c2 || t->v2->n3 == c2) )
			return t->v2; 		

	// Testa terceiro vizinho
	if(t->v3)
		if( (t->v3->n1 == c1 || t->v3->n2 == c1 || t->v3->n3 == c1) && 
			 (t->v3->n1 == c2 || t->v3->n2 == c2 || t->v3->n3 == c2) )
			return t->v3; 		

	return NULL;	
}

//------------------------------------------------------------
bool vtkSurfaceGen::IsSmallerEdgeInBoundary(SurfTriang* triangle)
{
	bool inBoundary = this->IsTriangleInBoundary(triangle);

	int c1, c2;
	if(inBoundary)
		{
		this->GetNeedleSmallerEdgeCoords(triangle, &c1, &c2);
		if( !(this->IsCoordInBoundary(triangle, c1) && this->IsCoordInBoundary(triangle, c2)) ) inBoundary = false;
		}
	
	return inBoundary; 
}

//------------------------------------------------------------
// Verifica se coordenada está ou não na borda a partir de um triângulo de borda
// Para isso, a idéia é fazer um busca circular a partir da coordenada "coord"
// Até encontrar um triângulo vizinho pertencente a outro grupo (sendo de borda a coordenada)
// ou até voltar ao triângulo inicial (coordenada não é de borda)
// P.S.: AINDA NÃO ESTÁ FAZENDO DESTA FORMA, SIMPLIFIQUEI O A FUNÇÃO PARA FINS DE TESTE!!!!!
// TODO: Otimizar este método para fazer como a descrição acima
bool vtkSurfaceGen::IsCoordInBoundary(SurfTriang* triangle, int coord)
{
	int group = triangle->gr;
	SurfTriang *current = first;
	while(current)
		{
		if(current->n1 == coord || current->n2 == coord || current->n3 == coord)
			if(current->gr != group)
				return true;
		current = current->next;
		}
	return false;	
}

//------------------------------------------------------------
bool vtkSurfaceGen::HasNode(SurfTriang *tri, int node)
{
	return (tri->n1 == node || tri->n2 == node || tri->n3 == node) ? true : false;
}

//------------------------------------------------------------
bool vtkSurfaceGen::CheckDifferentNeighborsGroups(SurfTriang* tri, int n1, int n2, int n3)
{
	// Testa se vizinhos são válidos. Em caso negativo, não compartilha borda com outro grupo
	if(!tri->v1 || !tri->v2 || !tri->v3) return false;

	if( this->HasNode(tri->v1, n1) || this->HasNode(tri->v1, n2) || this->HasNode(tri->v1, n3) )
		if( tri->v1->gr != tri->gr )
			return true;

	if( this->HasNode(tri->v2, n1) || this->HasNode(tri->v2, n2) || this->HasNode(tri->v2, n3) )
		if( tri->v2->gr != tri->gr )
			return true;

	if( this->HasNode(tri->v3, n1) || this->HasNode(tri->v3, n2) || this->HasNode(tri->v3, n3) )
		if( tri->v3->gr != tri->gr )
			return true;
	
	return false;
}

//------------------------------------------------------------
bool vtkSurfaceGen::CheckDifferentNeighborsGroups(SurfTriang* tri)
{
	// Testa se vizinhos são válidos. Em caso negativo, não compartilha borda com outro grupo
	if(!tri->v1 || !tri->v2 || !tri->v3) return false;

	// Testa grupos
	if( tri->v1->gr != tri->gr || 
			tri->v2->gr != tri->gr || 
			tri->v3->gr != tri->gr) 
		return true;
	
	return false;
}

//------------------------------------------------------------
// Verifica se triângulo está ou não na borda
bool vtkSurfaceGen::IsTriangleInBoundary(SurfTriang* triangle)
{
	bool inBoundary = false;
	int n1 = triangle->n1;
	int n2 = triangle->n2;
	int n3 = triangle->n3;	
	
	// Checagem de primeiro nível (vizinho diretos)
	inBoundary = ( CheckDifferentNeighborsGroups(triangle) ) ? true : inBoundary;
	if(inBoundary) return true;
	
	// Checagem de segundo nível (vizinho dos vizinhos direitos)
	if(!inBoundary && triangle->v1)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v1) ) ? true : inBoundary;
	if(!inBoundary && triangle->v2)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v2) ) ? true : inBoundary;
	if(!inBoundary && triangle->v3)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v3) ) ? true : inBoundary;	
	if( inBoundary) return true;	

	// Checagem de terceiro nível (vizinho dos vizinhos dos vizinhos. Neste caso, só checa triângulos que possuam pelo menos um dos vértices do triângulo base)
	if(triangle->v1)
		{
		if(!inBoundary && triangle->v1->v1)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v1->v1, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v1->v2)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v1->v2, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v1->v3)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v1->v3, n1, n2, n3) ) ? true : inBoundary;	
		if( inBoundary) return true;		
		}

	if(triangle->v2)
		{
		if(!inBoundary && triangle->v2->v1)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v2->v1, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v2->v2)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v2->v2, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v2->v3)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v2->v3, n1, n2, n3) ) ? true : inBoundary;	
		if( inBoundary) return true;		
		}

	if(triangle->v3)
		{
		if(!inBoundary && triangle->v3->v1)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v3->v1, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v3->v2)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v3->v2, n1, n2, n3) ) ? true : inBoundary;
		if(!inBoundary && triangle->v3->v3)	inBoundary = ( CheckDifferentNeighborsGroups(triangle->v3->v3, n1, n2, n3) ) ? true : inBoundary;	
		if( inBoundary) return true;		
		}
	return false;		
}

//------------------------------------------------------------
void vtkSurfaceGen::SwapNeighbors(SurfTriang* center, SurfTriang* pivo)
{
	SurfTriang *swap1, *swap2;
	
	// Se triângulo central é nulo, aborta método	
	if(!center) return;
	
	// Encontra vizinhos diferentes de pivo que será removido
	if(center->v1 == pivo)
		{
		swap1 = center->v2;
		swap2 = center->v3;
		}
	else if(center->v2 == pivo)
		{	
		swap1 = center->v1;
		swap2 = center->v3;
		}
	else
		{
		swap1 = center->v1;
		swap2 = center->v2;		
		}

	// Reposiciona primeiro vizinho
	if(swap1)
		{
		if		 (swap1->v1 == center) swap1->v1 = swap2;
		else if(swap1->v2 == center) swap1->v2 = swap2;
		else	 											 swap1->v3 = swap2;				
		}
	
	// Reposiciona segundo vizinho
	if(swap2)
		{
		if		 (swap2->v1 == center) swap2->v1 = swap1;
		else if(swap2->v2 == center) swap2->v2 = swap1;
		else	 											 swap2->v3 = swap1;				
		}	
}

//------------------------------------------------------------
int vtkSurfaceGen::RemoveThinTri()
{
	int num = 0;
	
	/*  1) Identificar linha (triângulo)
	 *  2) Refazer vizinhança
	 *  3) Remover linha (Triângulo)
	 * Obs1.: Nenhum nó e nenhum vizinho será removido nesse processo
	 */
	
	curr=first;
	SurfTriang *left, *right, *next;	
	while(curr)
		{
		// Check if it is a thin triangle
		if( curr->n1 == curr->n2 || curr->n1 == curr->n3 || curr->n2 == curr->n3 )
			{
			// Encontra os dois possíveis vizinhos da "linha"
			left = curr->v1;
			if(curr->v2!=left) right = curr->v2;
			else					 right = curr->v3;
			
			// Reposiciona vizinhos
			if(left->v1 == curr) 		left->v1 = right;
			else if(left->v2 == curr)  left->v2 = right;
			else								left->v3 = right;

			// Reposiciona vizinhos
			if(right->v1 == curr) 		right->v1 = left;
			else if(right->v2 == curr) right->v2 = left;
			else								right->v3 = left;
			
			// Remove linha
			next = curr->next;
			this->Delete(curr);
			curr = NULL;
			curr = next;	
			num++;			
			}
		else
			curr = curr->next;
		}
	return num;
}

//------------------------------------------------------------
void vtkSurfaceGen::RemoveNeedles(double angle, int *retrieveData)
{
	// Remove triângulos deformados em forma de linha (duas coordenadas são iguais)
	retrieveData[0] = RemoveThinTri();

	int num = 0; 				// Conta número de elementos a serem removidos
	int numNeighbors = 0;	// Conta número de triângulos agulha de borda que não foram removidos
	int agulhas = 0;
	int i = 0;
	par *trocas;
	SurfTriang *neighbor = NULL, *holdedNext = NULL;
	int menorArestaCoord1, menorArestaCoord2;
	double average[3], needleAngle;		
	
	// Primeiro loo:
	// Conta agulhas iniciais (não contando agulhas de borda) e marca flag = 0
	curr=first;
	while(curr)
		{
		if( this->isNeedle( angle, curr, &needleAngle ) && !IsTriangleInBoundary(curr) )
				agulhas++;
		curr->flg = 0;
		curr = curr->next;
		}
	
	// Aloca vetor de troca das coordenadas das agulhas a serem removidas
	trocas = new par[agulhas];

	// Segundo loop:
	// Marca agulhas, vizinhos a serem removidos e coordenadas de troca
	curr=first;
 	num = 0;	
	while(curr)
  	{
  	// Armazena próximo elemento
  	holdedNext = curr->next;		
  	if ( this->isNeedle( angle, curr, &needleAngle ) && !IsTriangleInBoundary(curr) && !curr->flg)
	  	{	  		
			// Procura menor aresta e suas respectivas coordenadas	  
  		this->GetNeedleSmallerEdgeCoords(curr, &menorArestaCoord1, &menorArestaCoord2);
	  	
			// Pega vizinho que compartilha menos aresta
			neighbor = this->GetEdgeNeighbor(curr, menorArestaCoord1, menorArestaCoord2); 
	
			// "Costura" vizinhos para poder remover triângulo central
			// Faz-se duas vezes para costurar os vizinhos da agulha e os vizinhos do "espelho" da agulha
			this->SwapNeighbors(curr, neighbor);
			this->SwapNeighbors(neighbor, curr);			
	  	  
			// Armazena coordenadas que serão unidas	
			trocas[num].a1 = menorArestaCoord1;
			trocas[num].a2 = menorArestaCoord2;			
			
			curr->flg = 1;			
			num++;
	
			if(neighbor)
				if(!neighbor->flg)
					{
					neighbor->flg = 1;
					numNeighbors++;
					}					
		  }
		curr=holdedNext;		  		
  	}	

	// Terceiro Loop
	// Remove agulhas marcadas
	curr=first;
	while(curr)
		{
		// Armazena próximo elemento
		holdedNext = curr->next;
		if(curr->flg)
			this->Delete(curr);
		curr = NULL;
		curr = holdedNext;			  					
  	}		

	// Triangulos removidos
	retrieveData[0] += num;	
	retrieveData[1] = numNeighbors;	

	// Quarto Loop:
	// Loop Parcial que apenas troca as coordenadas dos pontos que serão unidos
	for(i = 0; i < num; i++)
		{
		// Não troca se coordenadas forem iguais
		if(trocas[i].a1 == trocas[i].a2) continue;
		
		// Calcula média entre duas coordenadas
		this->ComputeAverageCoord(trocas[i].a1, trocas[i].a2, average); // ADDED 04/06

		// Modifica coordenada de 'a1' para ser o novo ponto central
		this->GetCoords()->SetNode(trocas[i].a1, average );

		// Vizinhos que apontavam 'a2' passam a apontar para 'a1'
		this->ChangeNeighborsCoords(trocas[i].a2, trocas[i].a1); 

		// Troca referências internas do restante do vetor de trocas
		for(int j = i+1; j < num; j++)
			{
			if( trocas[j].a1 == trocas[i].a2 ) trocas[j].a1 = trocas[i].a1;
			if( trocas[j].a2 == trocas[i].a2 ) trocas[j].a2 = trocas[i].a1;	  	
			}
		}
		
	delete[] trocas;
	
	// Chama método especial para remover triângulos agulhas que estão na
	// fronteira entre dois grupos
	this->RemoveBoundaryNeedles(angle, retrieveData);

	// Remove possíveis triângulos deformados gerados durante o processo 
	// em forma de linha (duas coordenadas são iguais)
	retrieveData[0] += RemoveThinTri();	
	
	// Atualiza número de elementos por grupo
	this->UpdateNumElsGroupsArray();
		
	// Remove coordenadas que não estão mais em uso e armazena valor para mostrar na interface
	retrieveData[2] = this->RemoveFreeNodes();
}

void vtkSurfaceGen::FindThirdNeighbor(SurfTriang *base, SurfTriang *neigh1, SurfTriang *neigh2, SurfTriang **thirdNeighbor)
{
	if     (base->v1 == neigh1)
		{
		if(base->v2 == neigh2) *thirdNeighbor = base->v3;
		if(base->v3 == neigh2) *thirdNeighbor = base->v2;		
		}
	else if(base->v2 == neigh1)
		{
		if(base->v1 == neigh2) *thirdNeighbor = base->v3;
		if(base->v3 == neigh2) *thirdNeighbor = base->v1;		
		}
	else if(base->v3 == neigh1)
		{
		if(base->v1 == neigh2) *thirdNeighbor = base->v2;
		if(base->v2 == neigh2) *thirdNeighbor = base->v1;		
		}	
}

void vtkSurfaceGen::ChangeUncommonCoord(SurfTriang *t1, SurfTriang *t2, SurfTriang *helpTri)
{
	int commonVet[3] = {0,0,0};
	int uncommon;
	
	// Encontra inicialmente (em relação a t2) coordenada que não é comum a ambos triângulos
	commonVet[0] = (t2->n1 == t1->n1 || t2->n1 == t1->n2 || t2->n1 == t1->n3) ? 1 : 0;
	commonVet[1] = (t2->n2 == t1->n1 || t2->n2 == t1->n2 || t2->n2 == t1->n3) ? 1 : 0;
	commonVet[2] = (t2->n3 == t1->n1 || t2->n3 == t1->n2 || t2->n3 == t1->n3) ? 1 : 0;
	
	if     (!commonVet[0]) uncommon = t2->n1;
	else if(!commonVet[1]) uncommon = t2->n2;
	else if(!commonVet[2]) uncommon = t2->n3;	

	// Encontrei um caso excepcional no qual um triângulo agulha pode ter dois vértices DE BORDA compartilhados com outro
	// triângulo DO MESMO GRUPO (difícil até de desenhar esse caso, mas existe!!!). Então, para saber qual o vértice correto, 
	// utiliza-se as coordenadas do triângulo que esta no outro grupo para fazer a escolha.
	
	// Troca coordenada, certificando de trocar a "compartilhada" de borda correta pela não compartilhada.
	if     ( IsCoordInBoundary(t1, t1->n1) && (t1->n1 == t2->n1 || t1->n1 == t2->n2 || t1->n1 == t2->n3) 
					&& (t1->n1 == helpTri->n1 || t1->n1 == helpTri->n2 || t1->n1 == helpTri->n3) )								t1->n1 = uncommon;
	else if( IsCoordInBoundary(t1, t1->n2) && (t1->n2 == t2->n1 || t1->n2 == t2->n2 || t1->n2 == t2->n3) 
					&& (t1->n2 == helpTri->n1 || t1->n2 == helpTri->n2 || t1->n2 == helpTri->n3) )								t1->n2 = uncommon;
	else if( IsCoordInBoundary(t1, t1->n3) && (t1->n3 == t2->n1 || t1->n3 == t2->n2 || t1->n3 == t2->n3) 
					&& (t1->n3 == helpTri->n1 || t1->n3 == helpTri->n2 || t1->n3 == helpTri->n3) )								t1->n3 = uncommon;		
}

void vtkSurfaceGen::ChangeNeighbor(SurfTriang *base, SurfTriang *fromTri, SurfTriang *toTri)
{
	if     (base->v1 == fromTri) base->v1 = toTri;
	else if(base->v2 == fromTri) base->v2 = toTri;
	else if(base->v3 == fromTri) base->v3 = toTri;	
}

//       ^																			      	^																																						
//     / | \																			     / \
//    /  |  \																			    /   \
//   /   |   \ -> thirdNeighborUp Unknown					   /     \	-> thirdNeighborUp 
//  /    |    \																		  /       \
// / t1a | t1b \																	 /   t1a   \
// -------------            ----------------->		 -----------      
// \ t2a | t2b /																	 \   t2a   /
//  \    |    /																		  \       /
//   \   |   / -> thirdNeighborDown Unknown				   \     / -> thirdNeighborDown				
//    \  |  /																			    \   /
//     \ | /																			     \ /
//       V																				      V
void vtkSurfaceGen::MergeTriangles(SurfTriang *t1a, SurfTriang *t1b, SurfTriang *t2a, SurfTriang *t2b )
{
	SurfTriang *thirdNeighborUp = NULL, *thirdNeighborDown = NULL;
	
	// A partir da agulha vizinha (t1b), encontra o vizinho mais a direita (é direita para ter a ver com a ilustração)
	this->FindThirdNeighbor(t1b, t1a, t2b, &thirdNeighborUp);
	// A partir do triângulo vizinho de baixo a direita (t2b) encontra o vizinho mais a direita	
	this->FindThirdNeighbor(t2b, t2a, t1b, &thirdNeighborDown);	
	
	// Verifica se encontrou alguma coisa
	if(thirdNeighborUp && thirdNeighborDown)
		{
		// Troca ponteiro da base "t1a" de "t1b" para "thirdNeighborUp" (mesmo lógica para os outros)
		this->ChangeNeighbor(t1a, t1b, thirdNeighborUp);
		this->ChangeNeighbor(thirdNeighborUp, t1b, t1a);		
		this->ChangeNeighbor(t2a, t2b, thirdNeighborDown);
		this->ChangeNeighbor(thirdNeighborDown, t2b, t2a);		
		
		// Troca coordenada não compartilhada de t1a pela mais a direita de t1b 
		this->ChangeUncommonCoord(t1a, t1b, t2b);
		this->ChangeUncommonCoord(t2a, t2b, t1b);
		
		// Marca triângulos para remoção
		t1b->flg = 1;
		t2b->flg = 1;		
		}
}

void vtkSurfaceGen::MergeBoundaryNeedles(double angle, int removeNonNeedles)
{
	int c1, c2;
	double needleAngle;
	SurfTriang *neighbor, *t1 = NULL, *t2 = NULL, *holdedNext;
	SurfTriang *ntemp1 = NULL, *ntemp2 = NULL, *ntemp3 = NULL;
	
	// Zera flag
	curr=first;
	while(curr)
		{
		curr->flg = 0;
		curr = curr->next;
		}
	
	curr=first;
	while(curr)
		{		
		neighbor = t1 = t2 = NULL;
		if( IsTriangleInBoundary(curr) && this->isNeedle( angle, curr, &needleAngle ) )
			{
			// Se o triangulo já tiver sido visitado, não faz nada...
			if(curr->flg)			
				{
				curr = curr->next;
				continue;
				}
						
			// Testa se menor aresta esta na borda (caso de interesse)
		  this->GetNeedleSmallerEdgeCoords(curr, &c1, &c2);
			if( !(this->IsCoordInBoundary(curr, c1) && this->IsCoordInBoundary(curr, c2)) )
				{
				curr = curr->next;
				continue;			
				}
		  
			// Se "removeNonNeedles" estiver desligado (default), vizinho da agulha tem que ser agulha também,
			// caso contrário, qualquer vizinho que tenha a menor aresta na borda serve
			if(!removeNonNeedles)
				{
				// Testa se tem algum vizinho que esteja na borda e seja do mesmo grupo do triangulo corrente
				if     (this->isNeedle( angle, curr->v1, &needleAngle) && (curr->gr == curr->v1->gr) && IsSmallerEdgeInBoundary(curr->v1) ) neighbor = curr->v1;
				else if(this->isNeedle( angle, curr->v2, &needleAngle) && (curr->gr == curr->v2->gr) && IsSmallerEdgeInBoundary(curr->v2) ) neighbor = curr->v2;
				else if(this->isNeedle( angle, curr->v3, &needleAngle) && (curr->gr == curr->v3->gr) && IsSmallerEdgeInBoundary(curr->v3) ) neighbor = curr->v3;
				}
			else
				{		
				// Testa se tem algum vizinho que esteja na borda e seja do mesmo grupo do triangulo corrente
				if     ( (curr->gr == curr->v1->gr) && IsSmallerEdgeInBoundary(curr->v1) ) neighbor = curr->v1;
				else if( (curr->gr == curr->v2->gr) && IsSmallerEdgeInBoundary(curr->v2) ) neighbor = curr->v2;
				else if( (curr->gr == curr->v3->gr) && IsSmallerEdgeInBoundary(curr->v3) ) neighbor = curr->v3;
				}

			// Testa se vizinho foi encontrado, caso contrário, não faz nada...  
			if(!neighbor)			
				{
				curr = curr->next;
				continue;
				}

			// Testa se vizinho já está marcado. Em caso positivo, não faz nada  ADDED 28/01/2008 
			if(neighbor->flg)			
				{
				curr = curr->next;
				continue;
				}
			
			// Procura triângulos que estão no outro grupo e são vizinhos da agulha
			if     ( curr->v1->gr != curr->gr) t1 = curr->v1;
			else if( curr->v2->gr != curr->gr) t1 = curr->v2;
			else if( curr->v3->gr != curr->gr) t1 = curr->v3;
	
			if     ( neighbor->v1->gr != neighbor->gr) t2 = neighbor->v1;
			else if( neighbor->v2->gr != neighbor->gr) t2 = neighbor->v2;
			else if( neighbor->v3->gr != neighbor->gr) t2 = neighbor->v3;
	
			// Junta triangulos se ambos foram encontrados
			if(t1 && t2)
				{
				// Checa também se triângulos do outro grupo são vizinhos (se não forem, é outro situação, não tratada por esse método)
				if(t1->v1 == t2 || t1->v2 == t2 || t1->v3 == t2)
					{				
					// Junta agulha e vizinhos
					this->MergeTriangles(curr, neighbor, t1, t2);
					}
				}
			} // Teste para saber se é agulha e se é de borda
		curr = curr->next;	
		}

	// Remove agulhas marcadas
	curr=first;
	while(curr)
  	{
		// Armazena próximo elemento
		holdedNext = curr->next;
		if(curr->flg)
			this->Delete(curr);
		curr = holdedNext;			  					
  	}			
	this->RemoveFreeNodes();	
}

//------------------------------------------------------------
void vtkSurfaceGen::RemoveBoundaryNeedles(double angle, int *retrieveData)
{
	int i = 0, j = 0;
	int agulhas = 0;	// Armazena número de agulhas de borda	
	par *trocas;			// Armazena coordenadas de troca
	int num;					// Numero de trocas efetivas	
	int n[4];					// Quais coordenadas estão na borda (1 a 3, 0 não é utilizado)
	int small[2];  		// Coordenadas da menor aresta
	SurfTriang *neighbor = NULL, *holdedNext = NULL;
	double average[3], needleAngle;	
	
	// Remove 3 triângulos coplanares contidos em um 
	this->RemoveTresTriBoundary();
		
	// Junta agulhas vizinhas de borda para colapsar agulhas
	this->MergeBoundaryNeedles(angle);
	
	// Junta agulhas com triângulos vizinhos
	this->MergeBoundaryNeedles(angle, 1);
	
	// Primeiro loop:
	// Conta agulhas de borda
	curr=first;
	while(curr)
		{
		if( this->isNeedle( angle, curr, &needleAngle ) && IsTriangleInBoundary(curr) )
			agulhas++;
		curr->flg = 0;
		curr = curr->next;
		}

	// Aloca vetor de troca das coordenadas das agulhas a serem removidas
	trocas = new par[agulhas];

	// Segunda loop:
	// Costura vizinhos e popula vetor de trocas (!!!)
	curr=first;
	int boundaryCoords;
	num = 0;
	while(curr)
		{
		if( this->isNeedle( angle, curr, &needleAngle ) && IsTriangleInBoundary(curr) )
			{
			boundaryCoords = 0;
			// Armazena vizinhos de 'curr' (
			// Vetor possui 4 elementos para manter coerencia com indice original de 'curr'. Elemento '0' será descartado
			int neighborsIds[4] = { 0 , curr->n1, curr->n2, curr->n3};

			for(i = 1; i <= 3; i++)
				n[i] = ( this->IsCoordInBoundary(curr, neighborsIds[i]) ) ? 1 : 0;
	
			// Verifica se alguma coordenada já está no vetor de trocas em alguns casos específicos (B e C)	
			// Se coordenada for igual a troca[i].a2 (que virará a1), também será de borda
			for(i = 1; i <= 3; i++)
				{
				if(!n[i])
					for(j = 0; j < num; j++)
						if(trocas[j].caso == 'B' || trocas[j].caso == 'C')
							if( trocas[j].a2 == neighborsIds[i] )
								n[i] = 1; 
				}

			// Conta número de coordenadas de borda
			for(i = 1; i <= 3; i++)
				if(n[i]) boundaryCoords++;
			
			// Pega coordenadas da menor aresta
			this->GetNeedleSmallerEdgeCoords(curr, &small[0], &small[1]);

	  		// Pega vizinho que compartilha menos aresta
			neighbor = this->GetEdgeNeighbor(curr, small[0], small[1]); 

			// "Costura" vizinhos para acertar vizinhança
			// Faz-se duas vezes para costurar os vizinhos da agulha e os vizinhos do "espelho" da agulha
			this->SwapNeighbors(curr, neighbor);
			this->SwapNeighbors(neighbor, curr);			
  	  
			curr->flg = 1;			

			if(neighbor)
				if(!neighbor->flg)
					neighbor->flg = 1;

			// Trata os casos das agulhas			
			// Testa caso pelo número de coordenadas de borda
			if(boundaryCoords == 1)
				{ 
				// Se tiver uma
				// Testa única coordenada de borda pertence a menor aresta (se positivo, caso A)
				if( (small[0] == curr->n1 || small[1] == curr->n1) && n[1] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n1)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n1
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}
				else if( (small[0] == curr->n2 || small[1] == curr->n2) && n[2] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n2)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n2
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}				
				else if( (small[0] == curr->n3 || small[1] == curr->n3) && n[3] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n3)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n3
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}				
				else 
					{ // Se nenhum dos casos acima satisfez o problema, menor aresta não esta na borda (caso A)
					trocas[num].caso = 'A';
					trocas[num].a1 = small[0];
					trocas[num].a2 = small[1];							
					}
				}
			else if(boundaryCoords >= 2)
				{ // Se tiver duas
				double dist[3];					
				int menorAresta;  // '0' se menor aresta estiver entre n1 e n2
										// '1' se menor aresta estiver entre n1 e n3
										// '2' se menor aresta estiver entre n2 e n3
												 	
				// Calcula a distância euclidiana das 3 arestas de um triângulo
				menorAresta = this->GetEdgesSize(curr, dist);
				
				// Verifica se as duas coordenadas de borda estão na menor aresta
				if( (menorAresta == 0 && n[1] && n[2]) ||
				    (menorAresta == 1 && n[1] && n[3]) ||
				    (menorAresta == 2 && n[2] && n[3]) )
					{
					trocas[num].caso = 'D';					
					trocas[num].a1 = small[0];				
					trocas[num].a2 = small[1];								
					}
				else
					{ // Apenas uma das coordenadas de borda esta na menor aresta
					trocas[num].caso = 'C';					
					if( this->IsCoordInBoundary(curr, small[0]) )
						{
						trocas[num].a1 = small[0];				
						trocas[num].a2 = small[1];																		
						}
					else
						{
						trocas[num].a1 = small[1];				
						trocas[num].a2 = small[0];																							
						}
					}			
				}
			num++;			
			}			
		curr = curr->next;
		}

	// Terceiro Loop:
	// Remove agulhas marcadas
	curr=first;
	while(curr)
  	{
		// Armazena próximo elemento
		holdedNext = curr->next;
		if(curr->flg)
			this->Delete(curr);
		curr = NULL;
		curr = holdedNext;			  					
  	}		

	// Quarto Loop:
	// Loop Parcial que apenas troca as coordenadas dos pontos que serão unidos
	for(i = 0; i < num; i++)
		{
		// Não troca se coordenadas forem iguais
		if(trocas[i].a1 == trocas[i].a2) continue;
		
		if(trocas[i].caso == 'A' || trocas[i].caso == 'D')
			{
			// Calcula média entre duas coordenadas
			this->ComputeAverageCoord(trocas[i].a1, trocas[i].a2, average); // ADDED 04/06
	
	  		// Modifica coordenada de 'a1' para ser o novo ponto central
			this->GetCoords()->SetNode(trocas[i].a1, average );
			}
			
		// Vizinhos que apontavam 'a2' passam a apontar para 'a1'
	  this->ChangeNeighborsCoords(trocas[i].a2, trocas[i].a1); 

	  // Troca referências internas do restante do vetor de trocas
	  for(j = i+1; j < num; j++)	  
	  	  {
	  	  
	  	  if( trocas[j].a1 == trocas[i].a2 ) trocas[j].a1 = trocas[i].a1;
	  	  if( trocas[j].a2 == trocas[i].a2 ) trocas[j].a2 = trocas[i].a1;	  	
	  	  }
		}
		
	delete[] trocas;

	// Número de triângulos de borda removidos
	retrieveData[3] = num; 
}

//------------------------------------------------------------
void vtkSurfaceGen::GetTrianglesSmallerThanValue(double area)
{
	int numTri = 0;
	
	// Conta número de triângulos menores que 'area'
	curr=first;
	while(curr)
  	{
  	if(this->GetTriangleArea(curr->n1, curr->n2, curr->n3) <= area) 
  		numTri++;
  	curr = curr->next;
  	}
	
	if(numTri)
		{
		char *message;
		// Tamanho inicial do vetor calculado em relação ao número de mensagem.
		// O valor 30 refere-se ao tamanho para cada linha já com folga e espaço para a primeira mensagem			
		message = new char[ numTri * 30 ]; 
		char aux[128];		
		sprintf(message, "\n\n Identified Triangles [%d] \n [ S ]- Small/NULL, [ N ] - Needle", numTri);
		double needleAngle;
		
		int index = 0;
		int trinumber = 0;		
		curr=first;
		while(curr)
	  	{
	  	if(this->GetTriangleArea(curr->n1, curr->n2, curr->n3) <= area) 
	  		{
	  		// Base angle is 10°
	  		if( this->isNeedle( 10.0, curr, &needleAngle ) )
	  			sprintf(aux, "\n  %04d -  %d - [ N ]", index++, trinumber);
	  		else // Se não é agulha, é pequeno/nulo
	  			sprintf(aux, "\n  %04d -  %d - [ S ]", index++, trinumber);	
	  		strcat(message, aux);	
	  		}
	  	trinumber++;
	  	curr = curr->next;
	  	}
		
		vtkErrorMacro(<<message);
		delete []message;
		}	
}

//------------------------------------------------------------
void vtkSurfaceGen::GetTriangleAreaGlobalInformation(double *average, double *smaller, double *greater)
{
	double larea, laverage = 0.0, lgreater, lsmaller;
	
	// Inicializa maior e menor área
 	larea = GetTriangleArea(first->n1, first->n2, first->n3);	
 	lgreater = lsmaller = larea;
 	
	// Calcula a área de todos os triângulos
	curr=first;
	while(curr)
	  	{
	  	larea = GetTriangleArea(curr->n1, curr->n2, curr->n3);
	  	if(larea > lgreater) lgreater = larea;
	  	if(larea < lsmaller) lsmaller = larea;  	  	
	  	laverage += larea;
	  	curr = curr->next;
	  	}
  
	*greater = lgreater;
	*smaller = lsmaller;
	*average = laverage / this->GetNumElsTotal();	
}

//------------------------------------------------------------
double vtkSurfaceGen::GetAverageTriangleArea()
{
	double averageArea = 0;
	
	// Calcula a área de todos os triângulos
	curr=first;
	while(curr)
	  	{
	  	averageArea += GetTriangleArea(curr->n1, curr->n2, curr->n3);
	  	curr = curr->next;
	  	}
  return averageArea / this->GetNumElsTotal();
}

//------------------------------------------------------------
double vtkSurfaceGen::GetTriangleArea(int n1, int n2, int n3)
{
	double p[3][3];	
	
	// Pega coordenadas referente aos índices passados como parâmetro
	this->GetCoord(n1, p[0]);
	this->GetCoord(n2, p[1]);	
	this->GetCoord(n3, p[2]);		

	// Aplica fórmula de Heron para calcular área	
	double a, b, c, s, area;
	a = sqrt( pow(p[1][0] - p[0][0],2) + pow(p[1][1] - p[0][1], 2) + pow(p[1][2] - p[0][2], 2));
	b = sqrt( pow(p[2][0] - p[1][0],2) + pow(p[2][1] - p[1][1], 2) + pow(p[2][2] - p[1][2], 2));
	c = sqrt( pow(p[0][0] - p[2][0],2) + pow(p[0][1] - p[2][1], 2) + pow(p[0][2] - p[2][2], 2));

	s = (a + b + c)/2;
	area = sqrt( s * (s-a) * (s-b) * (s-c) );
	return area;		
}

//------------------------------------------------------------
bool vtkSurfaceGen::IsTriangleSmall(SurfTriang* tri, double area)
{
	return ( this->GetTriangleArea(tri->n1, tri->n2, tri->n3) <= area ) ? true : false;
}

//------------------------------------------------------------
long vtkSurfaceGen::ViewSmallTri(double area)
{
	int num = 0; // Conta número de elementos que comporão o novo grupo

	// Zera flag
	curr=first;
	while(curr)
  	{
	 	curr->isNew = 0;
		curr=curr->next;			  		
  	}	
	
	// Visualiza apenas triângulos com área menor ou igual à global
	curr=first;
	while(curr)
  	{
		if ( this->GetTriangleArea(curr->n1, curr->n2, curr->n3) < area )
	  	{
			num++;		  		
			curr->isNew = 1;
	  	}
		curr=curr->next;			  		
  	}

	if(num)
		{
		char aux[100];		
		char *warningIds = new char[ (num+1) * 40]; // Estimando como 40 o tamanho máximo de cada linha
		sprintf(aux, "\n\nSmall Triangles [%d]\n", num);		
		strcpy(warningIds, aux);
		int index = 0;	
		curr=first;			
		while(curr)
	  	{
	  	double triArea = this->GetTriangleArea(curr->n1, curr->n2, curr->n3);
			if ( triArea < area )
				{
	  		sprintf(aux, "Id: %6d - Area: %.14lf\n", index, triArea);
	  		strcat(warningIds, aux); 			  		
				}
			curr=curr->next;			  		
			index++;
	    }
	 	vtkWarningMacro(<< warningIds); 
	 	delete[] warningIds;	
		}
	
	return num;		
}

//------------------------------------------------------------
void vtkSurfaceGen::ComputeAverageCoord(int n1, int n2, int n3, double average[3])
{
	double coords[3][3];
	
	// Armazena coordenadas do triângulo
	this->GetCoord(n1, coords[0]);
	this->GetCoord(n2, coords[1]);
	this->GetCoord(n3, coords[2]);	
	
	// Computa ponto médio
	for(int i = 0; i < 3; i++)
		average[i] = (coords[0][i] + coords[1][i] + coords[2][i]) / 3.0;
}

//------------------------------------------------------------
void vtkSurfaceGen::ComputeAverageCoord(int n1, int n2, double average[3])
{
	double coords[2][3];
	
	// Armazena coordenadas do triângulo
	this->GetCoord(n1, coords[0]);
	this->GetCoord(n2, coords[1]);
	
	// Computa ponto médio
	for(int i = 0; i < 3; i++)
		average[i] = (coords[0][i] + coords[1][i]) / 2.0;
}

//------------------------------------------------------------
long vtkSurfaceGen::RemoveBoundarySmallTri(double area)
{
	int i = 0, j = 0;
	int smallNum = 0;			// Armazena número de elementos pequenos  de borda	
	par *trocas;				// Armazena coordenadas de troca
	int num;						// Numero de trocas efetivas	
	int n[4];					// Quais coordenadas estão na borda (1 a 3, 0 não é utilizado)
	int small[2];  			// Coordenadas da menor aresta
	SurfTriang *neighbor = NULL, *holdedNext = NULL;
	double average[3];	
	
	// Remove 3 triângulos coplanares contidos em um 
	this->RemoveTresTriBoundary();
	
	// Primeiro loop:
	// Conta elementos pequenos de borda
	curr=first;
	while(curr)
		{
		if( IsTriangleSmall(curr, area) && IsTriangleInBoundary(curr) )
			smallNum++;
		curr->flg = 0;
		curr = curr->next;
		}

	// Aloca vetor de troca das coordenadas das agulhas a serem removidas
	trocas = new par[smallNum];

	// Segunda loop:
	// Costura vizinhos e popula vetor de trocas (!!!)
	curr=first;
	int boundaryCoords;
	num = 0;
	while(curr)
		{
		if( IsTriangleSmall(curr, area) && IsTriangleInBoundary(curr) )
			{
			boundaryCoords = 0;
			// Armazena vizinhos de 'curr' (
			// Vetor possui 4 elementos para manter coerencia com indice original de 'curr'. Elemento '0' será descartado
			int neighborsIds[4] = { 0 , curr->n1, curr->n2, curr->n3};

			for(i = 1; i <= 3; i++)
				n[i] = ( this->IsCoordInBoundary(curr, neighborsIds[i]) ) ? 1 : 0;
	
			// Verifica se alguma coordenada já está no vetor de trocas em alguns casos específicos (B e C)	
			// Se coordenada for igual a troca[i].a2 (que virará a1), também será de borda
			for(i = 1; i <= 3; i++)
				{
				if(!n[i])
					for(j = 0; j < num; j++)
						if(trocas[j].caso == 'B' || trocas[j].caso == 'C')
							if( trocas[j].a2 == neighborsIds[i] )
								n[i] = 1; 
				}

			// Conta número de coordenadas de borda
			for(i = 1; i <= 3; i++)
				if(n[i]) boundaryCoords++;
			
			// Pega coordenadas da menor aresta
			this->GetNeedleSmallerEdgeCoords(curr, &small[0], &small[1]);

	  		// Pega vizinho que compartilha menos aresta
			neighbor = this->GetEdgeNeighbor(curr, small[0], small[1]); 

			// "Costura" vizinhos para acertar vizinhança
			// Faz-se duas vezes para costurar os vizinhos do triangulo e os vizinhos do "espelho" do triangulo
			this->SwapNeighbors(curr, neighbor);
			this->SwapNeighbors(neighbor, curr);			
  	  
			curr->flg = 1;			

			if(neighbor)
				if(!neighbor->flg)
					neighbor->flg = 1;

			// Trata os casos dos elementos pequenos			
			// Testa caso pelo número de coordenadas de borda
			if(boundaryCoords == 1)
				{ 
				// Se tiver uma
				// Testa única coordenada de borda pertence a menor aresta (se positivo, caso A)
				if( (small[0] == curr->n1 || small[1] == curr->n1) && n[1] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n1)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n1
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}
				else if( (small[0] == curr->n2 || small[1] == curr->n2) && n[2] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n2)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n2
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}				
				else if( (small[0] == curr->n3 || small[1] == curr->n3) && n[3] )
					{
					trocas[num].caso = 'B';
					if(small[0] == curr->n3)
						{
						trocas[num].a1 = small[0];
						trocas[num].a2 = small[1];
						}
					else // small[1] == curr->n3
						{
						trocas[num].a1 = small[1];
						trocas[num].a2 = small[0];						
						}
					}				
				else 
					{ // Se nenhum dos casos acima satisfez o problema, menor aresta não esta na borda (caso A)
					trocas[num].caso = 'A';
					trocas[num].a1 = small[0];
					trocas[num].a2 = small[1];							
					}
				}
			else if(boundaryCoords >= 2)
				{ // Se tiver duas
				double dist[3];					
				int menorAresta;  // '0' se menor aresta estiver entre n1 e n2
										// '1' se menor aresta estiver entre n1 e n3
										// '2' se menor aresta estiver entre n2 e n3
												 	
				// Calcula a distância euclidiana das 3 arestas de um triângulo
				menorAresta = this->GetEdgesSize(curr, dist);
				
				// Verifica se as duas coordenadas de borda estão na menor aresta
				if( (menorAresta == 0 && n[1] && n[2]) ||
				    (menorAresta == 1 && n[1] && n[3]) ||
				    (menorAresta == 2 && n[2] && n[3]) )
					{
					trocas[num].caso = 'D';					
					trocas[num].a1 = small[0];				
					trocas[num].a2 = small[1];								
					}
				else
					{ // Apenas uma das coordenadas de borda esta na menor aresta
					trocas[num].caso = 'C';					
					if( this->IsCoordInBoundary(curr, small[0]) )
						{
						trocas[num].a1 = small[0];				
						trocas[num].a2 = small[1];																		
						}
					else
						{
						trocas[num].a1 = small[1];				
						trocas[num].a2 = small[0];																							
						}
					}			
				}
			num++;			
			}			
		curr = curr->next;
		}

	// Terceiro Loop:
	// Remove elementos marcados
	curr=first;
	while(curr)
  	{
		// Armazena próximo elemento
		holdedNext = curr->next;
		if(curr->flg)
			this->Delete(curr);
		curr = NULL;
		curr = holdedNext;			  					
  	}		

	// Quarto Loop:
	// Loop Parcial que apenas troca as coordenadas dos pontos que serão unidos
	for(i = 0; i < num; i++)
		{
		// Não troca se coordenadas forem iguais
		if(trocas[i].a1 == trocas[i].a2) continue;
		
		if(trocas[i].caso == 'A' || trocas[i].caso == 'D')
			{
			// Calcula média entre duas coordenadas
			this->ComputeAverageCoord(trocas[i].a1, trocas[i].a2, average); // ADDED 04/06
	
	  		// Modifica coordenada de 'a1' para ser o novo ponto central
			this->GetCoords()->SetNode(trocas[i].a1, average );
			}
			
		// Vizinhos que apontavam 'a2' passam a apontar para 'a1'
	  this->ChangeNeighborsCoords(trocas[i].a2, trocas[i].a1); 

	  // Troca referências internas do restante do vetor de trocas
	  for(j = i+1; j < num; j++)	  
	  	  {
	  	  
	  	  if( trocas[j].a1 == trocas[i].a2 ) trocas[j].a1 = trocas[i].a1;
	  	  if( trocas[j].a2 == trocas[i].a2 ) trocas[j].a2 = trocas[i].a1;	  	
	  	  }
		}
		
	delete[] trocas;

	// Número de triângulos de borda removidos
	return num; 
}


//------------------------------------------------------------
long  vtkSurfaceGen::RemoveSmallTri(double area)
{
	// Pq o global é alterado por outras funções
	SurfTriang *current; 
	int num = 0; // Conta número de elementos que serão removidos

	// Remove triângulos deformados em forma de linha
	// (Elementos com área zero)
	num = RemoveThinTri();	// Remove triângulos deformados em forma de linha (duas coordenadas são iguais)
	
	// Zera flag dos triângulos
	current=first;
	while(current)
	  	{
	  	current->flg = 0;
	  	current = current->next;
	  	} 

	// Marca triângulos com pequena área e seus respectivos vizinhos
	// Triângulo central é marcado com 2 e vizinhos com 1
	current=first;
	while(current)
		{
		// if( IsTriangleSmall(current, area) )
		if( IsTriangleSmall(current, area) && !this->IsTriangleInBoundary(current) )		
			{
			// Triangulo já marcado (passa para próxima iteração)
			if(current->flg == 1)
		  		{
		  		current = current->next;	  			
		  		continue;	  			  		
		  		} 
	  	
	  	// So marca com 2 se triângulo central estiver ainda com zero
	  	if(!current->flg) current->flg = 2;
  	
	  	// Marca vizinhos com 1
	  	if(current->v1)		current->v1->flg = 1;
	  	if(current->v2) 	current->v2->flg = 1;
	  	if(current->v3) 	current->v3->flg = 1;
	  	}
		current = current->next;
		}

	// Remove triângulos marcados
	current=first;
	SurfTriang *del;
	long n1, n2, n3;		
	double average[3];
	while(current)
		{
		if(current->flg)
			{
			// Marca o corrente para remoção
			del = current;
	  	
			// Pega referência do próximo antes de remover
			current=current->next;	  		

			// Se foi marcado com 2, modifica vizinhos e calcula ponto médio			
			if(del->flg == 2)
			{
				n1 = del->n1;
			  	n2 = del->n2;
			  	n3 = del->n3;
			  
			  	// Se estiver fora da borda, calcula ponto central. 
			  	// Caso não, triângulo será removido com outro método
				if(!this->IsTriangleInBoundary(del) )
				{
			  		this->ComputeAverageCoord(n1, n2, n3, average);  	
			  		// Modifica coordenada de n1 para ser o novo ponto central
					this->GetCoords()->SetNode(n1, average );
			  		// Modifica coordenada dos vizinhos para o novo valor central criado (m)
			  		// 'n1' é alterado pelo método anterior
					this->ChangeNeighborsCoords(n2, n1);
					this->ChangeNeighborsCoords(n3, n1);					
				}	  						  
			}
	  	this->Delete(del);
	  	num++;		  			  		
		}
	  	else
			current=current->next;			  		
	}	
   
   // Remove triângulos pequenos de borda 
   // Este método é uma adaptação do método que remove agulhas de borda.
	num += this->RemoveBoundarySmallTri(area);
   
	// Remove triângulos deformados em forma de linha gerados
	// durante o processamento 	
	RemoveThinTri();	
	
	// So atualiza número de elementos por grupo se um novo grupo foi criado
	this->UpdateNumElsGroupsArray();

	// Remove coordenadas que não estão mais em uso
	this->RemoveFreeNodes();	   
	return num;	
}

//------------------------------------------------------------
int vtkSurfaceGen::GetNumberOfSmallTriNeighbors(SurfTriang* tri, double area)
{
	int num = 0;
	if( this->IsTriangleSmall(tri->v1, area) ) num++;
	if( this->IsTriangleSmall(tri->v2, area) ) num++;
	if( this->IsTriangleSmall(tri->v3, area) ) num++;
	return num;
}
