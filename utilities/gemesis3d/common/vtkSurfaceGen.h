#ifndef SURFGEN
#define SURFGEN

#include <vtkObjectFactory.h>

#include <math.h>
#include "dyncoo3d.h"
#include "acdp.h"
#include "acqueue.h"
#include "nodlist.h"
#include "vtkUnstructuredGrid.h"
#include "vtkParamMesh.h"
#include "vtkMesh3d.h"
#include "SurfTriang.h"
#include "vtkAlgorithm.h"

#define SC_DELA   1
#define SC_MINMAX 2

class vtkPoints;
class vtkCellArray;

class VTK_EXPORT vtkSurfaceGen: public vtkAlgorithm
{
  public:

	vtkTypeRevisionMacro(vtkSurfaceGen,vtkObject);
	void PrintSelf(ostream& os, vtkIndent indent) {};
	static vtkSurfaceGen *New();
	vtkSurfaceGen();
	~vtkSurfaceGen();
  
  // Get a coord of index 'i'
  void GetCoord(int i, double *vec);
  SurfTriang* GetFirstTriang() { return this->first;};
  SurfTriang * GetCurrTriang() { return this->curr;};
  acDynCoord3D* GetCoords() { return this->Coord;};
  	
  // Calcula ponto médio entre as coordenadas n1, n2 e n3
  void ComputeAverageCoord(int n1, int n2, int n3, double *);  	

  	// Calcula ponto médio entre as coordenadas n1, n2
	void ComputeAverageCoord(int n1, int n2, double *);  	
  	  
	// Merge Groups
	// Merge Second Group into the first one
	void MergeGroups(int first, int second);

	// Check normals orientation
	// Returns 	0: if they are pointed out
	//				1: if they are pointed in
	int CheckOrientation(); // Needs that a temporary surface is created
	
	// Pass the necessary information to create the temporary surface inside the method
	int CheckOrientation(vtkMesh3d *mesh, vtkParamMesh *mpar);  	

	// Count the number of border triangules of the current surface
	int CountBorderTriangles(SurfTriang *);		

	// Update the unstructuredGrid object (grid) with the internal geometric information
	void UpdateUnstructuredGrid(vtkUnstructuredGrid *grid);

	// Set vtk groups with surfaceGen (ElGroups) information
	void SetGroupInformation(vtkDoubleArray *groups, vtkDoubleArray *selectedgroups);
	
	// Set element variable with internal information
	// Same code structure as "Print" method with a different output
	void CopyIncidenceVector(long *elements);		
	
	// Get Incidence Vector number of elements
	int GetIncidenceVectorSize();

	// Update Number of element atribute
	void UpdateNumEls();
		    
  	// VTK Delete method (used because there is another Delete method in this class)
  	virtual void Delete();
  	
  	// Build this SurfaceGen through a Mesh3d object
  	void  Copy(vtkMesh3d *mesh, vtkParamMesh *mpar);
		
	int  GetNumGroups() 		   { return this->NumGroups;};
	int  GetNumElsGroups(int i) { return this->NumElsGroups[i];};
	int  GetNumElsTotal();
	
	// Get the triangles which "isNew" flag was set to one
	void GetModifiedCells(int *);		
	
	// Set/Get vtkParamMesh
  void  SetPar(vtkParamMesh *mpar); 
  vtkParamMesh*  GetPar();
       
   // Retorna informações referentes ao número elementos e nós por grupo
	void  GroupInformation(long *numElemGroup, long *numNodesGroup);
    
   // Cria um novo grupo a partir de widget (pode ser esférica ou cúbica)    
	int   CreateGroupFromSelection(int type, double *data);   

	// Identifica triângulos agulha
	bool isNeedle(double angle, SurfTriang* current, double*);

	// Marca triangulos com formato de agulha para serem visualizados
	long ViewNeedles(double angle,char *ids);  

	// Remove triangulos com formato de agulha
	void RemoveNeedles(double angle, int *retrieveData);  

	// Remove triangulos com formato de agulha que estão na fronteira entre dois grupos
	void RemoveBoundaryNeedles(double angle, int *retrieveData);  

	// Junta agulhas de borda que são vizinhas
	void MergeBoundaryNeedles(double angle, int removeNonNeedles = 0);

	// Junta Triangulos (t1a com t2a, t1b com t2b)
	void MergeTriangles(SurfTriang *t1a, SurfTriang *t2a, SurfTriang *t1b, SurfTriang *t2b );
	
	// Dado que o triangulo base e dois dos seus vizinhos (neigh1 e neigh2) sejam conhecidos,
	// encontra o outro triângulo e armazena sua referência em thirdNeighbor 
	void FindThirdNeighbor(SurfTriang *base, SurfTriang *neigh1, SurfTriang *neigh2, SurfTriang **thirdNeighbor);
	
	// Troca para o triângulo base o vizinho "fromTri" pelo "toTri"
	void ChangeNeighbor(SurfTriang *base, SurfTriang *fromTri, SurfTriang *toTri);
	
	// Troca coordenada de borda de t1a pela coordenada que não é comum aos dois triângulos de t1b
	// O triângulo 'helpTri' auxilia a escolher a coordenada de borda de 't1' para alguns casos excepcionais
	void ChangeUncommonCoord(SurfTriang *t1a, SurfTriang *t1b, SurfTriang *helpTri);
		
	// Remove triangles that have two equal coordinates (triangle became a line)
	int  RemoveThinTri();
	
	// Calcula a distância euclidiana das 3 arestas de um triângulo
	// Retorna índice da menor aresta
	// '0' se menor aresta estiver entre n1 e n2
	// '1' se menor aresta estiver entre n1 e n3 
	// '2' se menor aresta estiver entre n2 e n3 			
	int GetEdgesSize(SurfTriang *, double *dist);

	// Procura menor aresta em um triângulo e armazena suas coordenadas
  void GetNeedleSmallerEdgeCoords(SurfTriang *, int *c1, int *c2);

	// Procura vizinho que compartilha areasta de coordenadas c1 e c2
  SurfTriang* GetEdgeNeighbor(SurfTriang *, int c1, int c2);
  	
  // Muda globalmente as coordenadas de triângulos de m1 para m2
  // TODO: Talvez esse método possa ser otimizado se fizermos uma busca local ao invés de global!
	void ChangeNeighborsCoords(int m1, int m2);

	// Inverte vizinhos	ao remover o triangulo "center" e seu vizinho "pivo"
	// Método é necessário para recosturar os ponteiros dos vizinhos
	void SwapNeighbors(SurfTriang* center, SurfTriang* pivo);
  	  
	// Checa se os vizinhos de 'triangle' fazem parte do seu grupo
	// Retorna true se forem diferente e false se forem do mesmo grupo
	bool CheckDifferentNeighborsGroups(SurfTriang* triangle);

	// Faz o mesmo que o método acima, mas só testa os triângulos que tiverem pelo menos 
	// um dos nós passados como parâmetro
	bool CheckDifferentNeighborsGroups(SurfTriang* triangle, int n1, int n2, int n3);
	
	// Checa se um triângulo está dentro ou fora de uma box    		
	bool IsTriangleInside(SurfTriang* triangle, double *bounds);
	
	// Checa se triângulo inválido (nulo ou com vértices de índices negativos)    		
	// Esse teste foi adicinado para corrigir um problema de alocação de memória no windows
	bool IsTriangleOk(SurfTriang *el);

	// Checa se um triângulo está dentro ou fora de uma esfera    		
	bool IsTriangleInside(SurfTriang* triangle, double *center, double radius);
	
	// Checa se um triângulo é ou não um triângulo de borda   		
	bool IsTriangleInBoundary(SurfTriang* triangle);

	// Checa para saber se além do triângulo ser de borda, a menor aresta está nessa borda.
	bool IsSmallerEdgeInBoundary(SurfTriang* triangle);	
	
  // Checa se coordenada pertence a fronteira entre dois grupos (borda)
	bool IsCoordInBoundary(SurfTriang* triangle, int coord);

	// Check if triangle 'tri' has the node 'node'
	bool HasNode(SurfTriang *tri, int node);	
	
	// Pega o valor médio de área de todos os triângulos
	double GetAverageTriangleArea();

	// Pega informações sobre as arestas dos triângulos da malha
	void GetTriangleEdgeGlobalInformation(double *average, double *smaller, double *greater);
	
	// Armazena os ids (triIds) dos triângulos menores do que 'area' e gera 
	// erro na "Warning window" com esses ids
	void GetTrianglesSmallerThanValue(double area);
	
	// Pega informações sobre as arestas dos triângulos da malha
	void GetTriangleAreaGlobalInformation(double *average, double *smaller, double *greater);

	// Calcula a área de um triângulo específico
	double GetTriangleArea(int, int, int);

	// Verifica se o triângulo é pequeno de acordo com a área especificada
	bool IsTriangleSmall(SurfTriang* tri, double area);

	// Returno o número de vizinho do nó corrente que também são pequenos
	int  GetNumberOfSmallTriNeighbors(SurfTriang* , double area);
	
	// Marca triangulos de area menor ou igual que parâmetro "area"
	long ViewSmallTri(double area);

	// Remove triangulos de area menor ou igual que parâmetro "area"		
	long RemoveSmallTri(double area);
	
	// Remove triangulos de area menor ou igual que parâmetro "area" que estão na fronteira entre dois grupos
	// Este método é uma modificação do removedor de agulhas na borda (removeBoundaryNeedles)
	long RemoveBoundarySmallTri(double area);  	

	// Atualiza vetor de Número de elementos por grupo quando necessário
	// Deve ser chamado quando o número de grupos for aumentado ou diminuido
	void  UpdateNumElsGroupsArray();
	
	// Get the number of Shells
	int  GetNumberOfShells() 			{ return this->Shells; };
	void SetNumberOfShells(int n) { this->Shells = n; };	
	
	int GetLastCreatedGroupIndex() {return this->LastCreatedGroupIndex; };
		
	// Methods from the original version
	void ImportSurface(vtkMesh3d *mesh);
	void Print(FILE *fp);
	void PrintBoundary(FILE *fp);
	void PrintEdges(FILE *fp);
	long DivideTodos();
	void DivideAlgunos(acQueue *selem);
	long DivideObtusos(double cota);
	long ColapsaTodos();
	void ColapsaAlgunos(acQueue *selem);
	void Smooth(int iter, double fac, int shr, int Gr, int needle);
	long RemoveFreeNodes();
	long RemoveGroup(long gr);
	void SwapGroup(int num);
	void TestMesh(long &nod, long &nel, int &null);
	void PegaNodos(double tol);
	void AddCapa(long gr1, long gr2);
	long SwapAll();
	void SetFactorH (double fact) {Factor = fact;}
	void SetCotaColap(double cota) {CotaColap = cota;}
	void SetCotaCopla(double cota) {CotaCopla = cos(cota*3.141592/180.0);}
	void chori(SurfTriang *el);
	void Information(long *nod, long *nel, long *grp);
	double GetFactorH () {return Factor;}
	void SelectElement(long *elem, int seltype, int inttype, int add, int g,
				double x,double y,double z,double nx,double ny,double nz, double r);
	void SetGroup(long *elem, int gr);
	acLimit3 Box() {return Coord->Box();}
	void Transform(double tx, double ty, double tz,
				double sx, double sy, double sz,
				double rx, double ry, double rz);
	int PoneTapas();
	void RemoveTresTri();
	
	// Modification of the previuos method to remove only boundary "tres tri"
	void RemoveTresTriBoundary();

	// Protected Methods
  protected:
    SurfTriang *Colapsa(SurfTriang *el);
    void  RemoveDos(SurfTriang *el, int ind);
    void  DivideElement(SurfTriang *el,int ind, long n);
    void  Obtuso(SurfTriang *El, acPoint3 &pos, double &tam, int &ind, double cota);
    void  CalNewNode(SurfTriang *El, acPoint3 &pos, double &tam, int &ind);
    void  RotaElem(SurfTriang *el, int ind);
    double CalDiamEl(SurfTriang *el);
    acPoint3 CalCentElem(SurfTriang *el);
    int   Coplanar(SurfTriang *el, int ind);
    int   Conviene(SurfTriang *el, SurfTriang *elv);
    int   SwapDiag(SurfTriang *el, SurfTriang *elv);
    int   FronteraIzq(SurfTriang *el, int ind);
    int   FronteraDer(SurfTriang *el, int ind);
    int   TestVecIzq(long no,acPoint3 p, SurfTriang *el, int ind);
    int   TestVecDer(long no,acPoint3 p, SurfTriang *el, int ind);
    int   CambiaNodoIzq(long nn, long no, SurfTriang *el, int ind);
    int   CambiaNodoDer(long nn, long no, SurfTriang *el, int ind);
    void  Renumera();
    void  InsertFirst(SurfTriang *el);
    void  Delete(SurfTriang *el);
    void  PutNullVec(SurfTriang *el, long gr);
    
  // Protected Attributes
  protected:
    SurfTriang *first;
    SurfTriang *curr;
    acDynCoord3D *Coord;
    vtkParamMesh *MP;

    long *NuevaNum;
    int FlagCop;
    double CotaColap;
    double CotaCopla;
    double Factor;
    int TypeCrit;
    int NumGroups;
    int *NumElsGroups;    
    
    bool colapsaError;    
    int recursiveCounter;    
    
    // Number of shells (Default 1)
    int Shells;
    
    // Armazena índice do último grupo criado
    int LastCreatedGroupIndex;
    
};

// Estrutura que irá armazenar os inteiros de troca de coordenadas
// Para o método de remoção de agulhas
struct par
{
	int a1, a2;		// Coordenadas do par a ser trocado
	char caso;		// Define se é uma agulha tipo A, B, C ou D	
};


#endif


