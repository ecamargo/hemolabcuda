#include <stdio.h>
#include "clus3d.h"

#ifndef BOOLEAN
#define BOOLEAN
typedef int boolean;
#define TRUE   1
#define FALSE  0
#endif

extern int debugging;
char msg[128];

// Implementacion de la clase node_cluster:
// funcion is_visible_from: indica si la cara es visible desde un punto.
int node_cluster::is_visible_from (long node,
	REAL huge *x, REAL huge *y, REAL huge *z, REAL radio_maximo) {
  if (status & VIS_COMP)
    return (status & VISIBLE);

  REAL x1,y1,z1,x2,y2,z2;
  REAL ax,bx,cx,ay,by,cy,az,bz,cz;
  REAL de1, de2, de3, det;
  long nn1, nn2, nn3;
  if (n1 < n2 && n1 < n3)
     { nn1 = n1; nn2 = n2; nn3 = n3; }
  else if (n2 < n1 && n2 < n3)
     { nn1 = n2; nn2 = n3; nn3 = n1; }
  else
     { nn1 = n3; nn2 = n1; nn3 = n2; }

  x1 = x[nn1];  y1 = y[nn1];  z1 = z[nn1];
  x2 = x[nn3];  y2 = y[nn3];  z2 = z[nn3];
  bx = x2 - x1;  by = y2 - y1;  bz = z2 - z1;

  x2 = x[nn2];  y2 = y[nn2];  z2 = z[nn2];
  cx = x2 - x1; cy = y2 - y1; cz = z2 - z1;

  x2 = x[node];  y2 = y[node];  z2 = z[node];
  ax = x2 - x1;  ay = y2 - y1;  az = z2 - z1;

  de1 = by*cz - bz*cy;
  de2 = bz*cx - bx*cz;
  de3 = bx*cy - by*cx;

  det = (ax * de1 + ay * de2 + az * de3);	// det = cte * volumen

/*
  if (debugging) {
    sprintf (msg, "Cara: %ld-%ld-%ld, desde nodo: %ld, vol= %f",
	     n1,n2,n3, node, -det);
    w_error (WARNING, msg);
  }
*/

  status = status | VIS_COMP;
  if (det <= 0.0) {
    radio = radio_maximo * 1.1e+20;
    status = status & 0x0003;		// Clears visibility bit
    return FALSE;
  }
  else {
    status = status | 0x0004;		// Sets visibility bit
    return TRUE;
  }

}

//  Metodo rot_log
void node_cluster::rot_log (long nn1)
{
  if (nn1 == n2) {
    long laux;
    int iaux;
    laux = n1;
      n1 = n2;
      n2 = n3;
      n3 = laux;
    iaux = cv1;
     cv1 = cv2;
     cv2 = cv3;
     cv3 = iaux;
  }
  else {
    if (nn1 == n3) {
      long laux;
      int iaux;
      laux = n3;
	n3 = n2;
	n2 = n1;
	n1 = laux;
      iaux = cv3;
       cv3 = cv2;
       cv2 = cv1;
       cv1 = iaux;
    }
  }
  return;
}


// Implementacion de la clase cluster:
//  Constructor:
cluster::cluster (long n1, long n2, long n3, long n4,
		  long v1, long v2, long v3, long v4)
{
  n_sides = 4;
  list_size = SIDES_INI;
  v_sides = (node_cluster *) mMalloc (list_size*sizeof(node_cluster));
  if (!v_sides) Error (FATAL_ERROR, 1, "(cluster)");

  node_cluster *c1,*c2,*c3,*c4;
  c1 = v_sides; c2 = v_sides+1; c3 = v_sides+2; c4 = v_sides+3;

  c1->n1  = n2;  c1->n2  = n3;  c1->n3  = n4;
  c1->v   = v1;
  c1->cv1 = 1;   c1->cv2 = 2;   c1->cv3 = 3;

  c2->n1  = n1;  c2->n2  = n4;  c2->n3  = n3;
  c2->v   = v2;
  c2->cv1 = 0;   c2->cv2 = 3;   c2->cv3 = 2;

  c3->n1  = n1;  c3->n2  = n2;  c3->n3  = n4;
  c3->v   = v3;
  c3->cv1 = 0;   c3->cv2 = 1;   c3->cv3 = 3;

  c4->n1  = n1;  c4->n2  = n3;  c4->n3  = n2;
  c4->v   = v4;
  c4->cv1 = 0;   c4->cv2 = 2;   c4->cv3 = 1;

  c1->status = c2->status = c3->status = c4->status = 0;
}

//  Metodo collapse_edge: ("diagonal swap")
//  devuelve 0 si no cambio el tamanho del cluster,
//  devuelve 1 si disminuyo el tamanho del cluster.
int cluster::collapse_edge(int lface, int rface, long tel, long bel)
{
  long nr, nl, nt, nb;
  node_cluster *p_lface, *p_rface;

  p_lface = &(v_sides[lface]);
  p_rface = &(v_sides[rface]);

  nr = p_rface->n1;
  nb = p_rface->n2;
  nt = p_rface->n3;

// Ordena informacion en la cara lface (rface ya estaba ordenada)
  nl = p_lface->n1;
  if (nl == nt) {
    nl = p_lface->n3;
  }
  else if (nl == nb) {
    nl = p_lface->n2;
  }
  p_lface->rot_log(nl);

/*
  if (debugging) {
    sprintf (msg,
    "Caras: %d:%ld-%ld-%ld, %d:%ld-%ld-%ld -> %d:%ld-%ld-%ld, %d:%ld-%ld-%ld",
	  lface, p_lface->n1, p_lface->n2, p_lface->n3,
	  rface, p_rface->n1, p_rface->n2, p_rface->n3,
	  lface, p_lface->n1, p_lface->n2, nr,
	  rface, p_rface->n1, p_rface->n2, nl);
    Error (WARNING, 0, msg);
  }
*/

//  Actualiza lface
  int aux;
  aux = p_lface->cv2;
  p_lface->n3  = nr;
  p_lface->cv1 = p_rface->cv2;
  if (v_sides[p_lface->cv1].cv1 == rface)
    v_sides[p_lface->cv1].cv1 = lface;
  else {
    if (v_sides[p_lface->cv1].cv2 == rface)
      v_sides[p_lface->cv1].cv2 = lface;
    else
      v_sides[p_lface->cv1].cv3 = lface;
  }
  p_lface->cv2 = rface;
  p_lface->v   = tel;

//  Actualiza rface
  p_rface->n3 = nl;
  p_rface->cv1 = aux;
  if (v_sides[aux].cv1 == lface)
    v_sides[aux].cv1 = rface;
  else {
    if (v_sides[aux].cv2 == lface)
      v_sides[aux].cv2 = rface;
    else
      v_sides[aux].cv3 = rface;
  }
  p_rface->cv2 = lface;
  p_rface->v   = bel;

  p_lface->status = 0;
  p_rface->status = 0;

/*  Tests siguientes anulados, en destroy_non_Delaunay se agregaron
    las verificaciones equivalentes

//
//	Test p/verificar caso de colapso de 2 aristas.
//	En tal caso deberian quedar 2 caras identicas (salvo orientacion)
//	Ambas apuntando a un elemento destruido.
//
//	Testea cara lface, vecinos cv1 y cv3 (cv2 es rface)
//
  int face_collapse, i_caraa, i_carab, retval;
  retval = 0;
  node_cluster *caraa, *carab;
  face_collapse = FALSE;
  i_caraa = lface;
  caraa = p_lface;
  i_carab = caraa->cv1;
  carab = &(v_sides[i_carab]);
  carab->rot_log (caraa->n3);
  if (carab->n3 == caraa->n1) {
    carab->rot_log (caraa->n1);
    face_collapse = TRUE;
  }
  else {
    i_carab = caraa->cv3;
    carab = &(v_sides[i_carab]);
    carab->rot_log (caraa->n2);
    if (carab->n3 == caraa->n3) {
      caraa->rot_log (caraa->n3);
      carab->rot_log (caraa->n1);
      face_collapse = TRUE;
    }
  }
  if (face_collapse) {
    retval = 1;

//    if (debugging) {
//      sprintf (msg,
//	  "Atencion, colapsan caras %d:%ld-%ld-%ld y %d:%ld-%ld-%ld",
//	  i_caraa, caraa->n1, caraa->n2, caraa->n3,
//	  i_carab, carab->n1, carab->n2, carab->n3);
//      w_error (WARNING, msg);
//    }

    node_cluster *vno, *vne, *vso, *vse;
    vno = &(v_sides[carab->cv3]);
    vne = &(v_sides[caraa->cv2]);
    vso = &(v_sides[carab->cv2]);
    vse = &(v_sides[caraa->cv3]);

    if (vno->cv1 == i_carab)
      vno->cv1 = caraa->cv2;
    else if (vno->cv2 == i_carab)
      vno->cv2 = caraa->cv2;
    else
      vno->cv3 = caraa->cv2;

    if (vne->cv1 == i_caraa)
      vne->cv1 = carab->cv3;
    else if (vne->cv2 == i_caraa)
      vne->cv2 = carab->cv3;
    else
      vne->cv3 = carab->cv3;

    if (vso->cv1 == i_carab)
      vso->cv1 = caraa->cv3;
    else if (vso->cv2 == i_carab)
      vso->cv2 = caraa->cv3;
    else
      vso->cv3 = caraa->cv3;

    if (vse->cv1 == i_caraa)
      vse->cv1 = carab->cv2;
    else if (vse->cv2 == i_caraa)
      vse->cv2 = carab->cv2;
    else
      vse->cv3 = carab->cv2;

//	Compacta vector de caras: Setear:
//      nueva posicion de penultimo: i_caraa,
//      nueva posicion de ultimo:    i_carab.
    if (i_caraa < n_sides-2) {		// caraa esta en el medio
      if (i_carab < n_sides-2) {      	// carab tambien
	v_sides[i_caraa] = v_sides[n_sides-2];
	v_sides[i_carab] = v_sides[n_sides-1];
      }
      else if (i_carab == n_sides-1) 	// i_carab es el ultimo
	v_sides[i_caraa] = v_sides[n_sides-2];
      else {			     	// i_carab es el penultimo
	v_sides[i_caraa] = v_sides[n_sides-1];
	i_carab = i_caraa;
      }
    }
    else {		// i_caraa es uno de los ultimos
      if (i_carab < n_sides-2) {      	// carab esta en el medio
	if (i_caraa == n_sides-1) {	// caraa es el ultimo
	  v_sides[i_carab] = v_sides[n_sides-2];
	  i_caraa = i_carab;
	}
	else                            // caraa es el penultimo
	  v_sides[i_carab] = v_sides[n_sides-1];
      }
    }
    n_sides -= 2;

    for (int i=0; i<n_sides; i++) {
      node_cluster *paux;
      paux = &(v_sides[i]);
      if (paux->cv1 == n_sides)   paux->cv1 = i_caraa;
      if (paux->cv2 == n_sides)   paux->cv2 = i_caraa;
      if (paux->cv3 == n_sides)   paux->cv3 = i_caraa;
      if (paux->cv1 == n_sides+1) paux->cv1 = i_carab;
      if (paux->cv2 == n_sides+1) paux->cv2 = i_carab;
      if (paux->cv3 == n_sides+1) paux->cv3 = i_carab;
    }
  }

//
//	Testea cara rface, vecinos cv1 y cv3 (cv2 es lface)
//
  face_collapse = FALSE;
  i_caraa = rface;
  caraa = p_rface;
  i_carab = caraa->cv1;
  carab = &(v_sides[i_carab]);
  carab->rot_log (caraa->n3);
  if (carab->n3 == caraa->n1) {
    carab->rot_log (caraa->n1);
    face_collapse = TRUE;
  }
  else {
    i_carab = caraa->cv3;
    carab = &(v_sides[i_carab]);
    carab->rot_log (caraa->n2);
    if (carab->n3 == caraa->n3) {
      caraa->rot_log (caraa->n3);
      carab->rot_log (caraa->n1);
      face_collapse = TRUE;
    }
  }
  if (face_collapse) {
    retval = 1;

//    if (debugging) {
//      sprintf (msg,
//	  "Atencion, colapsan caras %d:%ld-%ld-%ld y %d:%ld-%ld-%ld",
//	  i_caraa, caraa->n1, caraa->n2, caraa->n3,
//	  i_carab, carab->n1, carab->n2, carab->n3);
//      w_error (WARNING, msg);
//    }

    node_cluster *vno, *vne, *vso, *vse;
    vno = &(v_sides[carab->cv3]);
    vne = &(v_sides[caraa->cv2]);
    vso = &(v_sides[carab->cv2]);
    vse = &(v_sides[caraa->cv3]);

    if (vno->cv1 == i_carab)
      vno->cv1 = caraa->cv2;
    else if (vno->cv2 == i_carab)
      vno->cv2 = caraa->cv2;
    else
      vno->cv3 = caraa->cv2;

    if (vne->cv1 == i_caraa)
      vne->cv1 = carab->cv3;
    else if (vne->cv2 == i_caraa)
      vne->cv2 = carab->cv3;
    else
      vne->cv3 = carab->cv3;

    if (vso->cv1 == i_carab)
      vso->cv1 = caraa->cv3;
    else if (vso->cv2 == i_carab)
      vso->cv2 = caraa->cv3;
    else
      vso->cv3 = caraa->cv3;

    if (vse->cv1 == i_caraa)
      vse->cv1 = carab->cv2;
    else if (vse->cv2 == i_caraa)
      vse->cv2 = carab->cv2;
    else
      vse->cv3 = carab->cv2;

//	Compacta vector de caras: Setear:
//      nueva posicion de penultimo: i_caraa,
//      nueva posicion de ultimo:    i_carab.
    if (i_caraa < n_sides-2) {		// caraa esta en el medio
      if (i_carab < n_sides-2) {      	// carab tambien
	v_sides[i_caraa] = v_sides[n_sides-2];
	v_sides[i_carab] = v_sides[n_sides-1];
      }
      else if (i_carab == n_sides-1) 	// i_carab es el ultimo
	v_sides[i_caraa] = v_sides[n_sides-2];
      else {			     	// i_carab es el penultimo
	v_sides[i_caraa] = v_sides[n_sides-1];
	i_carab = i_caraa;
      }
    }
    else {		// i_caraa es uno de los ultimos
      if (i_carab < n_sides-2) {      	// carab esta en el medio
	if (i_caraa == n_sides-1) {	// caraa es el ultimo
	  v_sides[i_carab] = v_sides[n_sides-2];
	  i_caraa = i_carab;
	}
	else                            // caraa es el penultimo
	  v_sides[i_carab] = v_sides[n_sides-1];
      }
    }
    n_sides -= 2;

    for (int i=0; i<n_sides; i++) {
      node_cluster *paux;
      paux = &(v_sides[i]);
      if (paux->cv1 == n_sides)   paux->cv1 = i_caraa;
      if (paux->cv2 == n_sides)   paux->cv2 = i_caraa;
      if (paux->cv3 == n_sides)   paux->cv3 = i_caraa;
      if (paux->cv1 == n_sides+1) paux->cv1 = i_carab;
      if (paux->cv2 == n_sides+1) paux->cv2 = i_carab;
      if (paux->cv3 == n_sides+1) paux->cv3 = i_carab;
    }
  }

*/

  return (1);
}

//  Metodo destroy_face: nacen 3 caras de 1.
void cluster::destroy_face(int iface, long nop,
			   long elv1, long elv2, long elv3)
{
  node_cluster *p_iface, *p_tface, *p_rface;
  int tface, rface;
  tface = n_sides++;
  rface = n_sides++;

  if (n_sides >= list_size) {		// Expandir v_sides
//      if (debugging) Error (WARNING, 0, "Realloc in destroy_face");
      node_cluster *v_new;
      int size_new, i;
      size_new = list_size + SIDES_GROW;
      v_new = (node_cluster *) mMalloc (size_new*sizeof(node_cluster));
      if (!v_new) Error (FATAL_ERROR, 1, "(destroy_face, realloc)");
      for (i=0; i<list_size; i++)
          v_new[i] = v_sides[i];
      mFree(v_sides);
      v_sides = v_new;
      list_size = size_new;
  }

// Caras tentativas (setea solo nodos).

  p_iface = &(v_sides[iface]);
  p_tface = &(v_sides[tface]);
  p_rface = &(v_sides[rface]);

  p_tface->n1 = p_iface->n2; p_tface->n2 = p_iface->n3; p_tface->n3 = nop;
  p_rface->n1 = p_iface->n3; p_rface->n2 = p_iface->n1; p_rface->n3 = nop;
							p_iface->n3 = nop;
  p_iface->status = p_tface->status = p_rface->status = 0;

/*
  if (debugging) {
    sprintf (msg,
	  "Creando caras: %d:%ld-%ld-%ld, %d:%ld-%ld-%ld, %d:%ld-%ld-%ld",
	  iface, p_iface->n1, p_iface->n2, p_iface->n3,
	  tface, p_tface->n1, p_tface->n2, p_tface->n3,
	  rface, p_rface->n1, p_rface->n2, p_rface->n3);
    Error (WARNING, 0, msg);
  }
*/

/*
// Test de visibilidad:	(anulado)

	if ( !curr->is_visible_from(node,x,y,z,r_max)   ||
	     ( temp1->is_visible_from(node,x,y,z,r_max) &&
	       temp2->is_visible_from(node,x,y,z,r_max) &&
	       temp3->is_visible_from(node,x,y,z,r_max)    ) ) {
*/

// Completa la informacion de conectividad
  p_iface->v = elv3;
  p_tface->v = elv1;
  p_rface->v = elv2;

  p_tface->cv1 = rface;  p_tface->cv2 = iface;  p_tface->cv3 = p_iface->cv1;
  p_rface->cv1 = iface;  p_rface->cv2 = tface;  p_rface->cv3 = p_iface->cv2;
  p_iface->cv1 = tface;  p_iface->cv2 = rface;

  if (v_sides[p_tface->cv3].cv1 == iface)
    v_sides[p_tface->cv3].cv1 = tface;
  else {
    if (v_sides[p_tface->cv3].cv2 == iface)
      v_sides[p_tface->cv3].cv2 = tface;
    else
      v_sides[p_tface->cv3].cv3 = tface;
  }

  if (v_sides[p_rface->cv3].cv1 == iface)
    v_sides[p_rface->cv3].cv1 = rface;
  else {
    if (v_sides[p_rface->cv3].cv2 == iface)
      v_sides[p_rface->cv3].cv2 = rface;
    else
      v_sides[p_rface->cv3].cv3 = rface;
  }

  return;
}

//  Metodo collapse_faces(int, int, int): las caras caraa, face2, face3
//  tienen como vecinos a elementos que ya fueron borrados.
void cluster::collapse_faces (int i_caraa, int i_face2, int i_face3)
{
//
//	Test p/verificar caso de colapso de 2 caras,
//      no necesariamente vecinas.
//
  int i_carab, n_faces;
  long na1, na2, na3, nb1, nb2, nb3;
  node_cluster *caraa, *carab;

  caraa = &(v_sides[i_caraa]);
  na1 = caraa->n1;
  na2 = caraa->n2;
  na3 = caraa->n3;
  n_faces = n_sides;

  for (i_carab=0; i_carab<n_faces; i_carab++) {
    if (i_carab != i_caraa) {
      int n1eq = FALSE;
      carab = &(v_sides[i_carab]);
      nb1 = carab->n1;
      nb2 = carab->n2;
      nb3 = carab->n3;
      if (nb1 == na1) n1eq = TRUE;
      else if (nb2 == na1) {
	carab->rot_log (nb2);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }
      else if (nb3 == na1) {
	carab->rot_log (nb3);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }

      if (n1eq && nb2 == na3 && nb3 == na2) {	// colapso de caras

/*
	if (debugging) {
	  sprintf (msg,
	      "Atencion, colapsan caras %d:%ld-%ld-%ld y %d:%ld-%ld-%ld",
	      i_caraa, caraa->n1, caraa->n2, caraa->n3,
	      i_carab, carab->n1, carab->n2, carab->n3);
	  Error (WARNING, 0, msg);
	}
*/

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na2);
	carab->rot_log(na2);	// Ahora lado na1-na3

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na3);
	carab->rot_log(na3);	// Ahora lado na1-na2

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	n_faces = 0;	// Para salir del loop

// Compacta informacion del cluster.
//	Compacta vector de caras: Setear:
//      nueva posicion de penultimo: i_caraa,
//      nueva posicion de ultimo:    i_carab.
	if (i_caraa < n_sides-2) {		// caraa esta en el medio
	  if (i_carab < n_sides-2) {      	// carab tambien
	    v_sides[i_caraa] = v_sides[n_sides-2];
	    v_sides[i_carab] = v_sides[n_sides-1];
	  }
	  else if (i_carab == n_sides-1) 	// i_carab es el ultimo
	    v_sides[i_caraa] = v_sides[n_sides-2];
	  else {			     	// i_carab es el penultimo
	    v_sides[i_caraa] = v_sides[n_sides-1];
	    i_carab = i_caraa;
	  }
	}
	else {		// i_caraa es uno de los ultimos
	  if (i_carab < n_sides-2) {      	// carab esta en el medio
	    if (i_caraa == n_sides-1) {	// caraa es el ultimo
	      v_sides[i_carab] = v_sides[n_sides-2];
	      i_caraa = i_carab;
	    }
	    else                            // caraa es el penultimo
	      v_sides[i_carab] = v_sides[n_sides-1];
	  }
	}
	n_sides -= 2;

	for (int i=0; i<n_sides; i++) {
	  node_cluster *paux;
	  paux = &(v_sides[i]);
	  if (paux->cv1 == n_sides)   paux->cv1 = i_caraa;
	  if (paux->cv2 == n_sides)   paux->cv2 = i_caraa;
	  if (paux->cv3 == n_sides)   paux->cv3 = i_caraa;
	  if (paux->cv1 == n_sides+1) paux->cv1 = i_carab;
	  if (paux->cv2 == n_sides+1) paux->cv2 = i_carab;
	  if (paux->cv3 == n_sides+1) paux->cv3 = i_carab;
	}

      } // end if (colapso de caras

    }    	// end if(carab!=caraa)
  }	// end loop

  if (i_face2 == n_sides)   i_face2 = i_caraa;
  if (i_face2 == n_sides+1) i_face2 = i_carab;
  if (i_face3 == n_sides)   i_face3 = i_caraa;
  if (i_face3 == n_sides+1) i_face3 = i_carab;
  this->collapse_faces (i_face2, i_face3);

  return;
}

//  Metodo collapse_faces(int, int): las caras caraa, face2
//  tienen como vecinos a elementos que ya fueron borrados.
void cluster::collapse_faces (int i_caraa, int i_face2)
{
//
//	Test p/verificar caso de colapso de 2 caras,
//      no necesariamente vecinas.
//
  int i_carab, n_faces;
  long na1, na2, na3, nb1, nb2, nb3;
  node_cluster *caraa, *carab;

  caraa = &(v_sides[i_caraa]);
  na1 = caraa->n1;
  na2 = caraa->n2;
  na3 = caraa->n3;
  n_faces = n_sides;

  for (i_carab=0; i_carab<n_faces; i_carab++) {
    if (i_carab != i_caraa) {
      int n1eq = FALSE;
      carab = &(v_sides[i_carab]);
      nb1 = carab->n1;
      nb2 = carab->n2;
      nb3 = carab->n3;
      if (nb1 == na1) n1eq = TRUE;
      else if (nb2 == na1) {
	carab->rot_log (nb2);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }
      else if (nb3 == na1) {
	carab->rot_log (nb3);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }

      if (n1eq && nb2 == na3 && nb3 == na2) {	// colapso de caras

/*
	if (debugging) {
	  sprintf (msg,
	      "Atencion, colapsan caras %d:%ld-%ld-%ld y %d:%ld-%ld-%ld",
	      i_caraa, caraa->n1, caraa->n2, caraa->n3,
	      i_carab, carab->n1, carab->n2, carab->n3);
	  Error (WARNING, 0, msg);
	}
*/

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na2);
	carab->rot_log(na2);	// Ahora lado na1-na3

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na3);
	carab->rot_log(na3);	// Ahora lado na1-na2

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	n_faces = 0;	// Para salir del loop

// Compacta informacion del cluster.
//	Compacta vector de caras: Setear:
//      nueva posicion de penultimo: i_caraa,
//      nueva posicion de ultimo:    i_carab.
	if (i_caraa < n_sides-2) {		// caraa esta en el medio
	  if (i_carab < n_sides-2) {      	// carab tambien
	    v_sides[i_caraa] = v_sides[n_sides-2];
	    v_sides[i_carab] = v_sides[n_sides-1];
	  }
	  else if (i_carab == n_sides-1) 	// i_carab es el ultimo
	    v_sides[i_caraa] = v_sides[n_sides-2];
	  else {			     	// i_carab es el penultimo
	    v_sides[i_caraa] = v_sides[n_sides-1];
	    i_carab = i_caraa;
	  }
	}
	else {		// i_caraa es uno de los ultimos
	  if (i_carab < n_sides-2) {      	// carab esta en el medio
	    if (i_caraa == n_sides-1) {	// caraa es el ultimo
	      v_sides[i_carab] = v_sides[n_sides-2];
	      i_caraa = i_carab;
	    }
	    else                            // caraa es el penultimo
	      v_sides[i_carab] = v_sides[n_sides-1];
	  }
	}
	n_sides -= 2;

	for (int i=0; i<n_sides; i++) {
	  node_cluster *paux;
	  paux = &(v_sides[i]);
	  if (paux->cv1 == n_sides)   paux->cv1 = i_caraa;
	  if (paux->cv2 == n_sides)   paux->cv2 = i_caraa;
	  if (paux->cv3 == n_sides)   paux->cv3 = i_caraa;
	  if (paux->cv1 == n_sides+1) paux->cv1 = i_carab;
	  if (paux->cv2 == n_sides+1) paux->cv2 = i_carab;
	  if (paux->cv3 == n_sides+1) paux->cv3 = i_carab;
	}

      } // end if (colapso de caras

    }    	// end if(carab!=caraa)
  }	// end loop

  if (i_face2 == n_sides)   i_face2 = i_caraa;
  if (i_face2 == n_sides+1) i_face2 = i_carab;
  this->collapse_faces (i_face2);

  return;
}

//  Metodo collapse_faces: la cara caraa tiene como vecino un elemento
//  que ya fue  borrado.
void cluster::collapse_faces (int i_caraa)
{

//
//	Test p/verificar caso de colapso de 2 caras,
//      no necesariamente vecinas.
//
  int i_carab, n_faces;
  long na1, na2, na3, nb1, nb2, nb3;
  node_cluster *caraa, *carab;

  caraa = &(v_sides[i_caraa]);
  na1 = caraa->n1;
  na2 = caraa->n2;
  na3 = caraa->n3;
  n_faces = n_sides;

  for (i_carab=0; i_carab<n_faces; i_carab++) {
    if (i_carab != i_caraa) {
      int n1eq = FALSE;
      carab = &(v_sides[i_carab]);
      nb1 = carab->n1;
      nb2 = carab->n2;
      nb3 = carab->n3;
      if (nb1 == na1) n1eq = TRUE;
      else if (nb2 == na1) {
	carab->rot_log (nb2);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }
      else if (nb3 == na1) {
	carab->rot_log (nb3);
	n1eq = TRUE;
	nb1 = carab->n1;
	nb2 = carab->n2;
	nb3 = carab->n3;
      }

      if (n1eq && nb2 == na3 && nb3 == na2) {	// colapso de caras

/*
	if (debugging) {
	  sprintf (msg,
	      "Atencion, colapsan caras %d:%ld-%ld-%ld y %d:%ld-%ld-%ld",
	      i_caraa, caraa->n1, caraa->n2, caraa->n3,
	      i_carab, carab->n1, carab->n2, carab->n3);
	  Error (WARNING, 0, msg);
	}
*/

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na2);
	carab->rot_log(na2);	// Ahora lado na1-na3

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	caraa->rot_log(na3);
	carab->rot_log(na3);	// Ahora lado na1-na2

	if (caraa->cv1 != i_carab) { // a no es vecina a b x lado 2-3
	  int i_v1a, i_v1b;
	  node_cluster *v1a, *v1b;
	  i_v1a = caraa->cv1;
	  v1a = &(v_sides[i_v1a]);
	  i_v1b = carab->cv1;
	  v1b = &(v_sides[i_v1b]);

	  if (v1a->cv1 == i_caraa)
	    v1a->cv1 = i_v1b;
	  else if (v1a->cv2 == i_caraa)
	    v1a->cv2 = i_v1b;
	  else
	    v1a->cv3 = i_v1b;

	  if (v1b->cv1 == i_carab)
	    v1b->cv1 = i_v1a;
	  else if (v1b->cv2 == i_carab)
	    v1b->cv2 = i_v1a;
	  else
	    v1b->cv3 = i_v1a;
	}	// end if (vecinos x lado 1: n2-n3)

	n_faces = 0;	// Para salir del loop

// Compacta informacion del cluster.
//	Compacta vector de caras: Setear:
//      nueva posicion de penultimo: i_caraa,
//      nueva posicion de ultimo:    i_carab.
	if (i_caraa < n_sides-2) {		// caraa esta en el medio
	  if (i_carab < n_sides-2) {      	// carab tambien
	    v_sides[i_caraa] = v_sides[n_sides-2];
	    v_sides[i_carab] = v_sides[n_sides-1];
	  }
	  else if (i_carab == n_sides-1) 	// i_carab es el ultimo
	    v_sides[i_caraa] = v_sides[n_sides-2];
	  else {			     	// i_carab es el penultimo
	    v_sides[i_caraa] = v_sides[n_sides-1];
	    i_carab = i_caraa;
	  }
	}
	else {		// i_caraa es uno de los ultimos
	  if (i_carab < n_sides-2) {      	// carab esta en el medio
	    if (i_caraa == n_sides-1) {	// caraa es el ultimo
	      v_sides[i_carab] = v_sides[n_sides-2];
	      i_caraa = i_carab;
	    }
	    else                            // caraa es el penultimo
	      v_sides[i_carab] = v_sides[n_sides-1];
	  }
	}
	n_sides -= 2;

	for (int i=0; i<n_sides; i++) {
	  node_cluster *paux;
	  paux = &(v_sides[i]);
	  if (paux->cv1 == n_sides)   paux->cv1 = i_caraa;
	  if (paux->cv2 == n_sides)   paux->cv2 = i_caraa;
	  if (paux->cv3 == n_sides)   paux->cv3 = i_caraa;
	  if (paux->cv1 == n_sides+1) paux->cv1 = i_carab;
	  if (paux->cv2 == n_sides+1) paux->cv2 = i_carab;
	  if (paux->cv3 == n_sides+1) paux->cv3 = i_carab;
	}

      } // end if (colapso de caras

    }    	// end if(carab<>caraa)
  }	// end loop

}
