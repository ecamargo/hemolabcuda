#ifndef CLUS3D_H
#define CLUS3D_H
#include "acdp.h"

#ifndef REAL
#define REAL double
#endif

#define ANALIZED 1
#define VIS_COMP 2
#define VISIBLE  4

class cluster;

//-------------------------------------------------------------------------
class VTK_EXPORT node_cluster                      // Cara de un cluster
{
  private:
    long n1, n2, n3;
    long v;
    int  cv1, cv2, cv3;
    REAL xc, yc, zc, radio;
    int status;

  public:
    int is_visible_from (long node,
	REAL huge *x, REAL huge *y, REAL huge *z, REAL r_max);
    int is_analized (void) { return (status & 0x0001); }
    void set_analized (void) { status = status | 0x0001; }
    void rot_log (long nn1);

  friend class cluster;

};  					// end class node_cluster

//-------------------------------------------------------------------------
#define SIDES_INI  100
#define SIDES_GROW 50

class VTK_EXPORT cluster                           // Cluster: hueco en una red
{

  private:
    int n_sides;
    int list_size;
    node_cluster *v_sides;

  public:

// Constructor sin argumentos:
    cluster () {};

// Constructor con cuatro caras iniciales.
    cluster (long n1, long n2, long n3, long n4,
	     long v1, long v2, long v3, long v4);

    ~cluster()
    {
      mFree(v_sides);
    }

//  getnfaces:   devuelve numero de caras del cluster
    int getnfaces (void) { return n_sides; }

//  get_p: devuelve puntero p/algun lado.
    node_cluster *get_p (int index) { return (&(v_sides[index])); }
//  getni: devuelve nodo de algun lado.
    long getn1  (int index) { return (v_sides[index].n1); }
    long getn2  (int index) { return (v_sides[index].n2); }
    long getn3  (int index) { return (v_sides[index].n3); }
    long getv   (int index) { return (v_sides[index].v); }
    int  getcv1 (int index) { return (v_sides[index].cv1); }
    int  getcv2 (int index) { return (v_sides[index].cv2); }
    int  getcv3 (int index) { return (v_sides[index].cv3); }

    void setv (int index, long vv) { v_sides[index].v = vv; }

    void destroy_face (int face, long nop, long v1, long v2, long v3);

    int collapse_edge (int facel, int facer, long vt, long vb);

    void collapse_faces (int face1);

    void collapse_faces (int face1, int face2);

    void collapse_faces (int face1, int face2, int face3);

};  					// end class cluster

#endif
