#include "mesh3d.h"
#include "dmesh3d.h"

#include "vtkMeshData.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"  

REAL r_max;

#include "clus3d.h"
#include <math.h>
#include <stdarg.h>

// Implementacion de clase elem:
// Metodo set de la clase elem
void elem::set (REAL huge *x, REAL huge *y, REAL huge *z,
                long no1, long no2, long no3, long no4,
                long ve1, long ve2, long ve3, long ve4)
  {
  n1 = no1; n2 = no2; n3 = no3; n4 = no4;
  v1 = ve1; v2 = ve2; v3 = ve3; v4 = ve4;

  REAL x1,y1,z1,x2,y2,z2,a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3;
  REAL de1, de2, de3, det;

  if (n2 < n1 && n2 < n3)
    { no1 = n2; no2 = n3; no3 = n1; }
  else if (n3 < n1 && n3 < n2)
    { no1 = n3; no2 = n1; no3 = n2; }

  x1 = x[no1];  y1 = y[no1];  z1 = z[no1];
  x2 = x[no2];  y2 = y[no2];  z2 = z[no2];
  a1 = x2 - x1;  b1 = y2 - y1;  c1 = z2 - z1;
  d1 = a1 * (x2+x1)*0.5 + b1 * (y2+y1)*0.5 + c1 * (z2+z1)*0.5;

  x2 = x[no3];  y2 = y[no3];  z2 = z[no3];
  a2 = x2 - x1;  b2 = y2 - y1;  c2 = z2 - z1;
  d2 = a2 * (x2+x1)*0.5 + b2 * (y2+y1)*0.5 + c2 * (z2+z1)*0.5;

  x2 = x[no4];  y2 = y[no4];  z2 = z[no4];
  a3 = x2 - x1;  b3 = y2 - y1;  c3 = z2 - z1;
  d3 = a3 * (x2+x1)*0.5 + b3 * (y2+y1)*0.5 + c3 * (z2+z1)*0.5;

  de1 = b1*c2 - c1*b2;
  de2 = c1*a2 - a1*c2;
  de3 = a1*b2 - b1*a2;

  det = (a3 * de1 + b3 * de2 + c3 * de3);
  if (det == 0.0)
    Error (FATAL_ERROR, 1, "Volumen nulo, simplex: %ld, %ld, %ld, %ld",
        n1,n2,n3,n4);
  else if (det < 0.0)
    printf("\nNegative volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);

  de1 = b2*c3 - c2*b3;
  de2 = b3*c1 - c3*b1;
  de3 = b1*c2 - c1*b2;
  det = 1.0 / det;

  xc = (d1*de1 + d2*de2 + d3*de3) * det;
  yc = (d1*(a3*c2-a2*c3) + d2*(a1*c3-a3*c1) + d3*(a2*c1-a1*c2)) * det;
  zc = (d1*(a2*b3-a3*b2) + d2*(a3*b1-a1*b3) + d3*(a1*b2-a2*b1)) * det;
  x1 = x1-xc; y1 = y1-yc; z1 = z1-zc;
  radio = x1*x1 + y1*y1 + z1*z1;
  }

void elem::setw (REAL huge *x, REAL huge *y, REAL huge *z,
                long no1, long no2, long no3, long no4,
                long ve1, long ve2, long ve3, long ve4)
  {
  n1 = no1; n2 = no2; n3 = no3; n4 = no4;
  v1 = ve1; v2 = ve2; v3 = ve3; v4 = ve4;

  REAL x1,y1,z1,x2,y2,z2,a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3;
  REAL de1, de2, de3, det;

  x1 = x[n1];  y1 = y[n1];  z1 = z[n1];
  x2 = x[n2];  y2 = y[n2];  z2 = z[n2];
  a1 = x2 - x1;  b1 = y2 - y1;  c1 = z2 - z1;
  d1 = a1 * (x2+x1)*0.5 + b1 * (y2+y1)*0.5 + c1 * (z2+z1)*0.5;

  x2 = x[n3];  y2 = y[n3];  z2 = z[n3];
  a2 = x2 - x1;  b2 = y2 - y1;  c2 = z2 - z1;
  d2 = a2 * (x2+x1)*0.5 + b2 * (y2+y1)*0.5 + c2 * (z2+z1)*0.5;

  x2 = x[n4];  y2 = y[n4];  z2 = z[n4];
  a3 = x2 - x1;  b3 = y2 - y1;  c3 = z2 - z1;
  d3 = a3 * (x2+x1)*0.5 + b3 * (y2+y1)*0.5 + c3 * (z2+z1)*0.5;

  de1 = b2*c3 - c2*b3;
  de2 = b3*c1 - c3*b1;
  de3 = b1*c2 - c1*b2;

  det = (a1 * de1 + a2 * de2 + a3 * de3);
  if (det == 0.0)
    printf("\nNull volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);
  else if (det < 0.0)
    printf("\nNegative volume, simplex: %ld, %ld, %ld, %ld",n1,n2,n3,n4);

  det = 1.0 / det;

  xc = (d1*de1 + d2*de2 + d3*de3) * det;
  yc = (d1*(a3*c2-a2*c3) + d2*(a1*c3-a3*c1) + d3*(a2*c1-a1*c2)) * det;
  zc = (d1*(a2*b3-a3*b2) + d2*(a3*b1-a1*b3) + d3*(a1*b2-a2*b1)) * det;
  x1 = x1-xc; y1 = y1-yc; z1 = z1-zc;
  radio = x1*x1 + y1*y1 + z1*z1;
  }


// Funcion neighbour (busqueda dirigida):
// Devuelve -1 si el punto cayo en el elemento,
// o el indice del elemento a donde dirigirse
long elem::neighbour (REAL huge *x, REAL huge *y, REAL huge *z,
        REAL xp, REAL yp, REAL zp)
    {
    REAL x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4;
    REAL vol1,vol2,vol3,vol4,volm;
    long retval, signo, nn1, nn2, nn3, nn4, naux;
    long vv1, vv2, vv3, vv4;

    signo = 1; nn1 = n1; nn2 = n2; nn3 = n3, nn4 = n4;
    vv1 = v1; vv2 = v2; vv3 = v3; vv4 = v4;

    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}
    if (nn2 > nn3)
	{
	naux = nn2; nn2 = nn3; nn3 = naux;
	naux = vv2; vv2 = vv3; vv3 = naux;
	signo *= -1;
	}
    if (nn3 > nn4)
	{
	naux = nn3; nn3 = nn4; nn4 = naux;
	naux = vv3; vv3 = vv4; vv4 = naux;
	signo *= -1;
	}
    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}
    if (nn2 > nn3)
	{
	naux = nn2; nn2 = nn3; nn3 = naux;
	naux = vv2; vv2 = vv3; vv3 = naux;
	signo *= -1;
	}
    if (nn1 > nn2)
	{
	naux = nn1; nn1 = nn2; nn2 = naux;
	naux = vv1; vv1 = vv2; vv2 = naux;
	signo *= -1;
	}

  x1 = x[nn1]-xp; y1 = y[nn1]-yp; z1 = z[nn1]-zp;
  x2 = x[nn2]-xp; y2 = y[nn2]-yp; z2 = z[nn2]-zp;
  x3 = x[nn3]-xp; y3 = y[nn3]-yp; z3 = z[nn3]-zp;

  vol4 = x1*(y2*z3-y3*z2) + y1*(x3*z2-x2*z3) + z1*(x2*y3-x3*y2);
  vol4 *= signo * (-1);

  x4 = x[nn4]-xp; y4 = y[nn4]-yp; z4 = z[nn4]-zp;
  vol1 = x2*(y3*z4-y4*z3) + y2*(x4*z3-x3*z4) + z2*(x3*y4-x4*y3);
  vol1 *= signo;

  vol2 = x1*(y3*z4-y4*z3) + y1*(x4*z3-x3*z4) + z1*(x3*y4-x4*y3);
  vol2 *= signo * (-1);

  vol3 = x1*(y2*z4-y4*z2) + y1*(x4*z2-x2*z4) + z1*(x2*y4-x4*y2);
  vol3 *= signo;

  volm = vol1; retval = vv1;
  if (vol2<volm) { volm = vol2; retval = vv2; }
  if (vol3<volm) { volm = vol3; retval = vv3; }
  if (vol4<volm) { volm = vol4; retval = vv4; }

  if (volm >= 0) retval = -1;
  return retval;
  }

/*
long elem::neighbour (REAL huge *x, REAL huge *y, REAL huge *z,
        REAL xp, REAL yp, REAL zp)
  {
  REAL x1,x2,x3,x4, y1,y2,y3,y4, z1,z2,z3,z4, v;
  long retval;

  x1 = x[n1]-xp; y1 = y[n1]-yp; z1 = z[n1]-zp;
  x2 = x[n2]-xp; y2 = y[n2]-yp; z2 = z[n2]-zp;
  x3 = x[n3]-xp; y3 = y[n3]-yp; z3 = z[n3]-zp;
  v = - x1*(y2*z3-y3*z2) + y1*(x2*z3-x3*z2) - z1*(x2*y3-x3*y2);
  if (v<0) { retval = v4; goto cycle; }

  x4 = x[n4]-xp; y4 = y[n4]-yp; z4 = z[n4]-zp;
  v = - x2*(y4*z3-y3*z4) + y2*(x4*z3-x3*z4) - z2*(x4*y3-x3*y4);
  if (v<0) { retval = v1; goto cycle; }

  v = - x4*(y1*z3-y3*z1) + y4*(x1*z3-x3*z1) - z4*(x1*y3-x3*y1);
  if (v<0) { retval = v2; goto cycle; }

  v = - x4*(y2*z1-y1*z2) + y4*(x2*z1-x1*z2) - z4*(x2*y1-x1*y2);
  if (v<0) return v3;   // Si es la unica cara no es necesario rotar.

  return -1L;

  cycle:
  long aux = v1;
        v1 = v2;
        v2 = v3;
        v3 = aux;
       aux = n1;
        n1 = n2;
        n2 = n3;
        n3 = aux;
  return retval;
  }
*/

//----------------------------------------------------------------------
// Metodo rot_log de la clase elem: rota la descripcion logica de un
// elemento de manera que n1 = nodo1, n4 distinto de nodo1,nodo2,nodo3
int elem::rot_log (long nodo1, long nodo2, long nodo3)
    {
    long aux, vaux;

    if (n2 == nodo1)
        {
        aux  = n1; n1 = n2; n2 = n3; n3 = aux;
        vaux = v1; v1 = v2; v2 = v3; v3 = vaux;
        }
    else if (n3 == nodo1)
        {
        aux  = n3; n3 = n2; n2 = n1; n1 = aux;
        vaux = v3; v3 = v2; v2 = v1; v1 = vaux;
        }
    else if (n4 == nodo1)
        {
        aux  = n4; n4 = n2; n2 = n1; n1 = aux;
        vaux = v4; v4 = v2; v2 = v1; v1 = vaux;
        }
    else if (n1 != nodo1)
			{
      cout << "\nError: Input Meshdata is not ready. (elem::rot_log)\n" << endl;		
			this->SetElemError(true);        
      //Error (WARNING, 1,"elem::rot_log: ele: %ld %ld %ld %ld, args: %ld %ld %ld", n1, n2, n3, n4, nodo1, nodo2, nodo3);
      return 1;		                
			}
    if (n2 != nodo2 && n2 != nodo3)
        {
        aux  = n2; n2 = n3; n3 = n4; n4 = aux;
        vaux = v2; v2 = v3; v3 = v4; v4 = vaux;
        }
    else if (n3 != nodo2 && n3 != nodo3)
        {
        aux  = n4; n4 = n3; n3 = n2; n2 = aux;
        vaux = v4; v4 = v3; v3 = v2; v2 = vaux;
        }

    if (!(((n2 == nodo2) && (n3 == nodo3)) ||
          ((n2 == nodo3) && (n3 == nodo2))) )
			{
      cout << "\nError: Input Meshdata is not ready. (elem::rot_log)\n" << endl;	
      this->SetElemError(true);
			//Error (WARNING, 1,"elem::rot_log: ele: %ld %ld %ld %ld, args: %ld %ld %ld", n1, n2, n3, n4, nodo1, nodo2, nodo3);
      return 1;            
      }
	this->SetElemError(false);          
	return 0;
}

// Implementacion de clase Dmesh3D:

//  Constructor: crea los dos triangulos iniciales
Dmesh3D::Dmesh3D (long nodes, REAL huge *x0, REAL huge *y0, REAL huge *z0)
  {
  char *nome = "Dmesh3D::Dmesh3D(l,R*,R*,R*)";
  TraceOn(nome);
  nod = nodes;
  nel = 5;
  n_vecs = 1;
  firstfree = 5;
  v_elem = (elem **) mMalloc (n_vecs*sizeof(elem *));
  an_element = (long *) mMalloc (nod*sizeof(long));
  if (!v_elem || !an_element)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  v_elem[0] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
  if (!v_elem[0])
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  x = x0;
  y = y0;
  z = z0;

//      Crea arbol que contendra nodos insertados
  REAL xmmi, xmma, ymmi, ymma, zmmi, zmma, xd, yd, zd;
  xmmi = x[nod-8]; xmma = x[nod-1];
  ymmi = y[nod-8]; ymma = y[nod-1];
  zmmi = z[nod-8]; zmma = z[nod-1];
  r_max = (zmmi-zmma)* 1.0e+6;
  xd = xmma - xmmi; yd = ymma - ymmi; zd = zmma - zmmi;
  xd *= 0.3; yd *= 0.3; zd *= 0.3;
  xmmi += xd; xmma -= xd;
  ymmi += yd; ymma -= yd;
  zmmi += zd; zmma -= zd;
  tree = new ord_ot(xmmi,xmma,ymmi,ymma,zmmi,zmma);
  tree->add(nod-8, x, y, z, 0L);
  tree->add(nod-7, x, y, z, 0L);
  tree->add(nod-6, x, y, z, 0L);
  tree->add(nod-5, x, y, z, 0L);
  tree->add(nod-4, x, y, z, 0L);
  tree->add(nod-3, x, y, z, 0L);
  tree->add(nod-2, x, y, z, 0L);
  tree->add(nod-1, x, y, z, 0L);

//      Setea lista de elementos libres
  elem *v_aux = v_elem[0];
  for (int i = 5; i<ELE_PER_VEC; i++) {
      v_aux[i].n1 = i+1;
      v_aux[i].n2 = -1;
  }

  v_aux[0].set (x, y, z, nod-8, nod-7, nod-6, nod-4, 4L, -1L, -1L, -1L);
  v_aux[1].set (x, y, z, nod-5, nod-6, nod-7, nod-1, 4L, -1L, -1L, -1L);
  v_aux[2].set (x, y, z, nod-3, nod-4, nod-1, nod-7, 4L, -1L, -1L, -1L);
  v_aux[3].set (x, y, z, nod-2, nod-1, nod-4, nod-6, 4L, -1L, -1L, -1L);
  v_aux[4].set (x, y, z, nod-1, nod-4, nod-6, nod-7, 0L,  1L,  2L,  3L);

  an_element[nod-8] = 0L;
  an_element[nod-7] = 0L;
  an_element[nod-6] = 1L;
  an_element[nod-5] = 1L;
  an_element[nod-4] = 2L;
  an_element[nod-3] = 2L;
  an_element[nod-2] = 3L;
  an_element[nod-1] = 3L;

  nodes_added = 0;
  min_cluster_size = max_cluster_size = 5;
  avg_cluster_size = 0;
  min_nel_incr = max_nel_incr = 7;
  TraceOff(nome);
  }

//  Constructor: Lee de un archivo, calcula vecinos
Dmesh3D::Dmesh3D (FILE *fp)
  {
  char *nome = "Dmesh3D::Dmesh3D(FILE *)";
  TraceOn(nome);
// Lee Mesh3D, para aprovechar el calculo de elelv

  Mesh3d *m3d;
  m3d = new Mesh3d;
  m3d->Read(fp);

  SparseElElV *elelv;
  elelv = m3d->NewElElV();
  nod = m3d->GetNumNodes();

  x = (REAL *) mMalloc (nod * sizeof(REAL));
  y = (REAL *) mMalloc (nod * sizeof(REAL));
  z = (REAL *) mMalloc (nod * sizeof(REAL));
  if (!x || !y || !z)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

  n_vecs = 1;
  firstfree = 0;
  v_elem = (elem **) mMalloc (n_vecs*sizeof(elem *));
  an_element = (long *) mMalloc (nod*sizeof(long));
  if (!v_elem || !an_element)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
  v_elem[0] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
  if (!v_elem[0])
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

  int i;
  for (i = 0; i<ELE_PER_VEC; i++)
    {
    v_elem[0][i].n1 = i+1;
    v_elem[0][i].n2 = -1;
    }

  Element *el;
  long n1,n2,n3,n4,v1,v2,v3,v4;
  nel = 0;
  for (el = m3d->SetFirstElem(); el; el = m3d->SetNextElem())
    {
    n1 = el->GetNode(0); n2 = el->GetNode(1);
    n3 = el->GetNode(2); n4 = el->GetNode(3);
    v4 = elelv->GetElem (nel, 0); v3 = elelv->GetElem (nel, 1);
    v1 = elelv->GetElem (nel, 2); v2 = elelv->GetElem (nel, 3);
    an_element[n1] = an_element[n2] =
    an_element[n3] = an_element[n4] = nel;
    AddElement();
    p_elem(nel-1)->set(n1,n2,n3,n4, v1,v2,v3,v4);
    }
  delete elelv;

  acPoint3 pp;
//  acCoordinate3D *cc;
//  cc = m3d->GetCoords();
  for (i = 0; i < nod; i++)
    {
    pp = (*(m3d->GetCoords()))[i];
    x[i] = pp.x; y[i] = pp.y; z[i] = pp.z;
    }

  delete m3d;

  tree = NULL;

  TraceOff(nome);
  }

//  Destructor de la clase Dmesh3D:
Dmesh3D::~Dmesh3D ()
  {
  for (int i=0; i<n_vecs; i++)
    mFree (v_elem[i]);
  mFree(v_elem);
  mFree(an_element);
  if (tree) delete tree;
  }

// Metodo add de la clase mesh:
// Agrega un nodo en la red. Utiliza otros metodos de la clase
void Dmesh3D::add (long node)
  {
  long nelprev, cl_size, nel_incr;

  long ele_ini;

  ele_ini = which_elem (node);
  elem *p_el = p_elem(ele_ini);

  cluster cl(p_el->n1, p_el->n2, p_el->n3, p_el->n4,
             p_el->v1, p_el->v2, p_el->v3, p_el->v4);
  //printf("Creo cluster\n");

  nelprev = nel;

  // Libera elemento ele_ini
  p_el->n2 = -1;
  p_el->n1 = firstfree;
  firstfree = ele_ini;
  nel--;

  destroy_non_Delaunay (cl, node);
  //printf("Expandio cluster\n");

  cl_size = nelprev - nel;
  avg_cluster_size += cl_size;
  if      (cl_size < min_cluster_size) min_cluster_size = cl_size;
  else if (cl_size > max_cluster_size) max_cluster_size = cl_size;

  regenerate (cl, node);
  //printf("Regenero cluster\n");

  nel_incr = nel - nelprev;
  if      (nel_incr < min_nel_incr) min_nel_incr = nel_incr;
  else if (nel_incr > max_nel_incr) max_nel_incr = nel_incr;

  nodes_added++;
  }

// Metodo add_notree de la clase mesh:
// Agrega un nodo en la red. No aprovecha estructura de datos eficiente
void Dmesh3D::add_notree (long node)
  {
  long nelprev, cl_size, nel_incr;

  long ele_ini;

  long curr, next;
  REAL xp, yp, zp;
  xp = x[node];
  yp = y[node];
  zp = z[node];

  curr = 0;  // Comienza por el elemento 0

  while ((next = p_elem(curr)->neighbour(x,y,z,xp,yp,zp)) >= 0)
     curr = next;
  ele_ini = curr;

  elem *p_el = p_elem(ele_ini);

  cluster cl(p_el->n1, p_el->n2, p_el->n3, p_el->n4,
             p_el->v1, p_el->v2, p_el->v3, p_el->v4);

  nelprev = nel;

  // Libera elemento ele_ini
  p_el->n2 = -1;
  p_el->n1 = firstfree;
  firstfree = ele_ini;
  nel--;

  destroy_non_Delaunay (cl, node);

  cl_size = nelprev - nel;
  avg_cluster_size += cl_size;
  if      (cl_size < min_cluster_size) min_cluster_size = cl_size;
  else if (cl_size > max_cluster_size) max_cluster_size = cl_size;

  regenerate (cl, node);

  nel_incr = nel - nelprev;
  if      (nel_incr < min_nel_incr) min_nel_incr = nel_incr;
  else if (nel_incr > max_nel_incr) max_nel_incr = nel_incr;

  nodes_added++;
  }

//  Metodo which_elem: busqueda dirigida
long Dmesh3D::which_elem (long node)
  {
  long curr, next;
  long nrec;
  REAL xp, yp, zp;
  xp = x[node];
  yp = y[node];
  zp = z[node];

                // Para determinar con que elemento comenzar
  long hoja_ini = 0L;   // se fija en el arbol que va armando
//printf("Entro a which_elem\n");
  curr = tree->nodo_cercano (xp, yp, zp, hoja_ini);
//printf("Nodo cercano: %ld\n", curr);
  tree->add(node, x, y, z, hoja_ini);

  curr = an_element[curr];
//printf("Elemento asociado: %ld\n", curr);

  nrec = 0;
  while ((next = p_elem(curr)->neighbour(x,y,z,xp,yp,zp)) >= 0 &&
	 nrec < nel+20)
	{
//printf("Proximo elemento: %ld\n", next);
     nrec++;
     curr = next;
	}
  if (nrec > nel)
   printf("\nInfinite loop at which_elem, used: %ld", curr);
  return curr;
  }

// Metodo destroy_non_Delaunay de la clase mesh
// borra los tetraedros q' fallan el test delaunay con el nodo node,
// actualizando el cluster cl
void Dmesh3D::destroy_non_Delaunay (cluster &cl, long node)
{
  int edge_collapse, all_analized;
  node_cluster *curr;
  elem *p_ele;
  REAL xp, yp, zp;
  xp = x[node]; yp = y[node]; zp = z[node];

  int iface, currcv1, currcv2, currcv3;
  long ele, currn1, currn2, currn3;

  all_analized = 0;
  while (!all_analized) {
  all_analized = 1;

  int nfaces = cl.getnfaces();
  for (iface = 0; iface < nfaces ; iface++) {   // Recorre vector de caras
    curr    = cl.get_p (iface);
    currn1  = cl.getn1 (iface);
    currn2  = cl.getn2 (iface);
    currn3  = cl.getn3 (iface);
    currcv1 = cl.getcv1(iface);
    currcv2 = cl.getcv2(iface);
    currcv3 = cl.getcv3(iface);
    ele     = cl.getv(iface);                   // Toma elemento vecino
    p_ele   = p_elem(ele);

    if  ( ele >= 0 && !curr->is_analized() )
      {
      if (p_ele->n2 < 0) {      // Algo anda mal. Esta cara no deberia estar
        cl.collapse_faces(iface);
        nfaces = 0;
        all_analized = 0;
      }
      else if ( p_ele->fail_Delaunay_test (xp, yp, zp) ||
           !curr->is_visible_from (node,x,y,z,r_max) ) {
                                          // Si falla el test Delaunay
                                          // o la cara no es visible

// Actualizar cluster:
      p_ele->rot_log (currn1, currn2, currn3);

// Test p/ver si dos caras del cluster se anulan:
      edge_collapse = 0;
      if (cl.getv(currcv1) == ele) {    // Colapsa arista curr-cv1
        edge_collapse = 1;
      }
      else {
        if (cl.getv(currcv2) == ele) {  // Colapsa arista curr-cv2
          edge_collapse = 1;
          p_ele->rot_log (currn2, currn3, currn1);
          curr->rot_log (currn2);
          currcv1 = currcv2;
        }
        else {
          if (cl.getv(currcv3) == ele) {// Colapsa arista curr-cv3
            edge_collapse = 1;
            p_ele->rot_log (currn3, currn1, currn2);
            curr->rot_log (currn3);
            currcv1 = currcv3;
          }
        }
      }

      if (edge_collapse) {   // Desaparece un elemento, 2 caras se modifican
//
//                  2 3                        2                 3
//                   ^                         ^         ele:   ^
//                  /|\                       / \              /|\
//                 / | \                     / a \            / | \
//                /a |  \                 1 /     \ 3        /  |  \
//              1<- -|- ->1      --->      <------->       4<- -|- ->1
//                \  | b/                 3 \     / 1        \  |  /
//                 \ | /                     \ b /            \ | /
//                  \|/                       \ /              \|/
//                   v                         v                v
//                  3 2                        2                 2
//

        long elvt, elvb;
        elvt = p_ele->v2; elvb = p_ele->v3;

// Borra elemento
        p_ele->n1 = firstfree;
        p_ele->n2 = -1;
        firstfree = ele;
        nel--;

// Actualiza cluster
        cl.collapse_edge(currcv1, iface, elvt, elvb);

// Analiza caras recien creadas: currcv1, iface
        int collapse_1, collapse_2;
        collapse_1 = collapse_2 = FALSE;
        ele = cl.getv(currcv1);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_1 = TRUE;
        }
        ele = cl.getv(iface);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_2 = TRUE;
        }

        if (collapse_1 || collapse_2) {
          if (collapse_1 && collapse_2)
            cl.collapse_faces (currcv1, iface);
          else if (collapse_1)
            cl.collapse_faces (currcv1);
          else if (collapse_2)
            cl.collapse_faces (iface);
          nfaces = 0;
        }

      }         //end if (edge_colapse) then

      else {    // Desaparece un elemento, nacen 3 nuevas caras (-1)
//
//          2                 3                 2                 3
//           <--------------->                   <--------------->
//            \-_         _-/                     \-_         _-/
//             \ -_     _- /                       \ -_     _- /
//              \  -_ _-  /                         \  -_4_-  /
//               \   |   /                           \   |   /
//                \  |  /                             \  |  /
//                 \ | /                        ele:   \ | /
//                  \|/                                 \|/
//                   v                                   v
//                   1                                   1
//

        long elvt, elvl, elvr;
        p_ele->rot_log (currn1, currn2, currn3);
        elvt = p_ele->v1;
        elvl = p_ele->v3;
        elvr = p_ele->v2;

        long nodop = p_ele->n4;

// Borra elemento
        p_ele->n1 = firstfree;
        p_ele->n2 = -1;
        firstfree = ele;
        nel--;

// Actualiza cluster
        cl.destroy_face(iface, nodop, elvt, elvr, elvl);

// Analiza caras recien creadas: iface, n_sides-2, n_sides-1
        int iface2, iface3;
        iface2 = cl.getnfaces()-1;
        iface3 = iface2-1;
        int collapse_1, collapse_2, collapse_3;
        collapse_1 = collapse_2 = collapse_3 = FALSE;
        ele = cl.getv(iface);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_1 = TRUE;
        }
        ele = cl.getv(iface2);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_2 = TRUE;
        }
        ele = cl.getv(iface3);
        if (ele >= 0) {
          p_ele = p_elem(ele);
          if (p_ele->n2 < 0) collapse_3 = TRUE;
        }

        if (collapse_1 || collapse_2 || collapse_3) {
          if (collapse_1 && collapse_2 && collapse_3)
            cl.collapse_faces (iface, iface2, iface3);
          else if (collapse_1 && collapse_2)
            cl.collapse_faces (iface, iface2);
          else if (collapse_2 && collapse_3)
            cl.collapse_faces (iface2, iface3);
          else if (collapse_1 && collapse_3)
            cl.collapse_faces (iface, iface3);
          else if (collapse_1)
            cl.collapse_faces (iface);
          else if (collapse_2)
            cl.collapse_faces (iface2);
          else if (collapse_3)
            cl.collapse_faces (iface3);
          nfaces = 0;
        }
      }

      all_analized = 0;

    }   //end if (fail_Delaunay_test)
    else
      curr->set_analized();
    }   //end if (ele>=0 && ~analized
    else {
      curr->set_analized();
    }
  }
  }
}

// Metodo regenerate de la clase mesh:
// Regenera red en el cluster cl, que contiene al nodo node.

void Dmesh3D::regenerate (cluster &cl, long node)
{
  int curr_face, nfaces;
  long curr_el, vec;
  long n1, n2, n3;

// PRIMER LOOP: crea nuevos elementos, estableciendo conectividad solo
//      con el exterior del cluster.
  nfaces = cl.getnfaces();
  for (curr_face = 0; curr_face<nfaces; curr_face++) {

    n1 = cl.getn1(curr_face);
    n2 = cl.getn2(curr_face);
    n3 = cl.getn3(curr_face);
    vec = cl.getv(curr_face);           // Elemento externo
    curr_el = AddElement();
    p_elem(curr_el)->set(x, y, z, n1, n3, n2, node, -1L, -1L, -1L, vec);
    cl.setv (curr_face, curr_el);

    if (vec>=0) {
      elem *pt_vec = p_elem(vec);
      if (pt_vec->n1 != n1 && pt_vec->n1 != n2 && pt_vec->n1 != n3)
        pt_vec->v1 = curr_el;
      else
      if (pt_vec->n2 != n1 && pt_vec->n2 != n2 && pt_vec->n2 != n3)
        pt_vec->v2 = curr_el;
      else
      if (pt_vec->n3 != n1 && pt_vec->n3 != n2 && pt_vec->n3 != n3)
        pt_vec->v3 = curr_el;
      else
        pt_vec->v4 = curr_el;
    }

    an_element[n1] = an_element[n2] = an_element[n3] = curr_el;
  }

  an_element[node] = curr_el;

// SEGUNDO LOOP: establece conectividad entre los elementos recien generados

  for (curr_face = 0; curr_face<nfaces; curr_face++) {
    curr_el = cl.getv(curr_face);
    p_elem(curr_el)->v1 = cl.getv(cl.getcv1(curr_face));
    p_elem(curr_el)->v2 = cl.getv(cl.getcv3(curr_face));
    p_elem(curr_el)->v3 = cl.getv(cl.getcv2(curr_face));
  }

}

// Metodo renumber de la clase mesh
void Dmesh3D::renumber (long huge *vper)
{
  long i;
  REAL huge *vaux;
  long huge *perinv;
  char *nome = "Dmesh3d::renumber";
  TraceOn(nome);

  vaux   = (REAL huge *) mMalloc (nod*sizeof(REAL));
  perinv = (long huge *) mMalloc (nod*sizeof(long));
  if (!vaux || !perinv)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

  long iel;

  for (i=0; i<nod; i++)
      perinv[vper[i]] = i;

  for (i=0; i<nod; i++)
      vaux[i] = x[perinv[i]];
  for (i=0; i<nod; i++)
      x[i] = vaux[i];

  for (i=0; i<nod; i++)
      vaux[i] = y[perinv[i]];
  for (i=0; i<nod; i++)
      y[i] = vaux[i];

  for (i=0; i<nod; i++)
      vaux[i] = z[perinv[i]];
  for (i=0; i<nod; i++)
      z[i] = vaux[i];

  for (i = iel = 0; iel<nel; i++) {
      elem *el = p_elem(i);
      if (el->n2 >= 0) {
          el->n1 = vper[el->n1];
          el->n2 = vper[el->n2];
          el->n3 = vper[el->n3];
          el->n4 = vper[el->n4];
          an_element[el->n1] =
          an_element[el->n2] =
          an_element[el->n3] =
          an_element[el->n4] = i;
          iel++;
      }
  }

  mFree (vaux);
  mFree (perinv);
  TraceOff(nome);
}

// Metodo print de la clase mesh
void Dmesh3D::print (char *name, REAL huge *fun)
{
  long i;
  FILE *fp;
  char *nome = "Dmesh3D::print";
  TraceOn(nome);

  fp = fopen (name,"w");

  long iel;

  fprintf (fp,"*COORDINATES\n");
  fprintf (fp,"%ld\n", nod);
  for (i = 0; i<nod; i++)
      fprintf (fp,"%ld %.16lg %.16lg %.16lg\n", i+1, x[i], y[i], z[i]);
  fprintf (fp,"*ELEMENT GROUPS\n");
  fprintf (fp," 1\n");
  fprintf (fp," 1 %ld LINEAR_TETRAHEDRUM\n", nel);
  fprintf (fp,"*INCIDENCE\n");
  for (i = iel = 0; iel<nel; i++)
		{
		elem *el = p_elem(i);
		if (el->n2 >= 0)
		    {
		    fprintf (fp, "%ld %ld %ld %ld\n",
		    (el->n1)+1, (el->n2)+1, (el->n3)+1, (el->n4)+1);
		    iel++;
		    }
		}

  if (fun)
      {
      fprintf (fp,"*EDGES_SIZE\n");
      for (i = 0; i<nod; i++)
          {
          fprintf (fp," %g", fun[i]);
          if (!((i+1)%4)) fprintf (fp,"\n");
          }
      fprintf (fp,"\n");
      }

  fprintf (fp,"*END\n");

  fclose (fp);
  TraceOff(nome);
}


// Overload of print method to fill
void Dmesh3D::print (vtkMeshData *output)
{

	int tetraIndex[4];  
  vtkCellArray *cells=NULL;
	vtkPoints *points=NULL;
  int *types=NULL;

	// Insere Informações de pontos
	points = vtkPoints::New();
	for ( int i=0 ; i < this->nod; i++ )
		points->InsertNextPoint(x[i], y[i], z[i]);				
	output->GetVolume()->SetPoints(points);
  if(points) points->Delete();

	// Insere Informações de conectividade
	cells =  vtkCellArray::New();	
	types = new int[ this->nel ];		
	for (int i = 0; i < this->nel; i++)
		{
		elem *el = p_elem(i);			
		tetraIndex[0]= el->n1;
		tetraIndex[1]= el->n2;		
		tetraIndex[2]= el->n3;
		tetraIndex[3]= el->n4;		
		cells->InsertNextCell( 4, tetraIndex );		
		types[i] = 10;				
		}
	output->GetVolume()->SetCells(types, cells );	// 10 é para tetraedros
  if (types) delete [] types;
  if (cells) cells->Delete();
}

//----------------------------------------------------------------------
// Metodo is_ok de la clase mesh: test logico y geometrico de la red
int Dmesh3D::is_ok (int check_neighbours, int check_an_element, int check_geometry)
{
  elem *el, *neighbour;

  long caras_externas = 0;
  long i, iel, ivec;
  long nn1, nn2, nn3;
  int conect_ns = 0;
  int carext_n12 = 0;

  if (check_neighbours)
  {
  	for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) 
  	{
      el = p_elem(i);
      if (el->n2 >= 0) {        // El lugar esta ocupado x 1 elemento
          iel++;
          ivec = el->v1;                        // VECINO 1
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                if(neighbour->rot_log(el->n2, el->n3, el->n4)) return -1;
                if(neighbour->v4 != i) conect_ns = 1;
                if(neighbour->rot_log(nn1, nn2, nn3)) return -1;
                
                if( neighbour->GetElemError() ) return -1;
          }
          else
              caras_externas++;
          ivec = el->v2;                        // VECINO 2
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                if(neighbour->rot_log(el->n1, el->n4, el->n3)) return -1;
                if(neighbour->v4 != i) conect_ns = 1;
                if(neighbour->rot_log(nn1, nn2, nn3)) return -1;                
                
                if( neighbour->GetElemError() ) return -1;                
          }
          else
              caras_externas++;
          ivec = el->v3;                        // VECINO 3
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                if(neighbour->rot_log(el->n1, el->n2, el->n4)) return -1;
                if(neighbour->v4 != i) conect_ns = 1;
                if(neighbour->rot_log(nn1, nn2, nn3)) return -1;
                
                if( neighbour->GetElemError() ) return -1;                
          }
          else
              caras_externas++;
          ivec = el->v4;                        // VECINO 4
          if (ivec >= 0) {
                neighbour = p_elem(ivec);
                nn1 = neighbour->n1;
                nn2 = neighbour->n2;
                nn3 = neighbour->n3;
                if(neighbour->rot_log(el->n1, el->n3, el->n2)) return -1;
                if(neighbour->v4 != i) conect_ns = 1;
                if(neighbour->rot_log(nn1, nn2, nn3)) return -1;
                
                if( neighbour->GetElemError() ) return -1;                
          }
          else
              caras_externas++;
      }
  	}
  	if (caras_externas != 12) carext_n12 = 1;
	} 	// endif check_neighbours

	//------------------- test an_element vector
	if (check_an_element)
  	{
  	for (i = 0; i < nod; i++)
    	{
	    iel = an_element[i];
	    el = p_elem(iel);
	    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
	      Error(WARNING, 0,"Bad vector an_element. Node: %ld. Element: %ld %ld %ld %ld",
      i, el->n1, el->n2, el->n3, el->n4);
    	}
    }

//------------------- test geometrico
  REAL qmin = 1.0, vmin = 1.0e+30;
  elem *el_peor, *el_min;
	if (check_geometry)
  {
  	long n1, n2, n3, n4;


	  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) 
	  	{
			el = p_elem(i);
  	 	if (el->n2 >= 0) 
  	 		{           // El lugar esta ocupado x 1 elemento
		    iel++;
		    n1 = el->n1; n2 = el->n2; n3 = el->n3; n4 = el->n4;

		    if (n1<nod-8 && n2<nod-8 && n3<nod-8 && n4<nod-8) 
			    { // solo elementos
	                                        // de la red sin la caja
			    REAL x2,y2,z2, x3,y3,z3, x4,y4,z4;
			    x2 = x3 = x4 = x[n1];
			    y2 = y3 = y4 = y[n1];
			    z2 = z3 = z4 = z[n1];
			    x2 = x[n2] - x2;    y2 = y[n2] - y2;    z2 = z[n2] - z2;
			    x3 = x[n3] - x3;    y3 = y[n3] - y3;    z3 = z[n3] - z3;
			    x4 = x[n4] - x4;    y4 = y[n4] - y4;    z4 = z[n4] - z4;
			    REAL vol = x2*(y3*z4-z3*y4) + y2*(z3*x4-x3*z4) + z2*(x3*y4-y3*x4);
			    if (vol < vmin) {
      			vmin = vol;
			      el_min = el;
				    }

				    REAL au, a, a1,a2,a3, b, b1,b2,b3, c1,c2,c3, xa,ya, x32;
				    au = sqrt(x2*x2 + y2*y2 + z2*z2);    a = 1.0 / au;
				    a1 = x2 * a; a2 = y2 *a; a3 = z2 * a;
				    b1 = a2 * z3 - a3 * y3;  b2 = a3 * x3 - a1 * z3;  b3 = a1 * y3 - a2 * x3;
				    b = 1.0 / sqrt(b1*b1 + b2*b2 + b3*b3);  b1 *= b; b2 *= b; b3 *= b;
				    c1 = a3*b2 - a2*b3;    c2 = a1*b3 - a3*b1;   c3 = a2*b1 - a1*b2;
				    x2 = au;
				    a  = a1*x3 + a2*y3 + a3*z3;
				    y3 = c1*x3 + c2*y3 + c3*z3;
				    x3 = a;
				    a  = a1*x4 + a2*y4 + a3*z4;
				    b  = c1*x4 + c2*y4 + c3*z4;
				    z4 = b1*x4 + b2*y4 + b3*z4;
				    x4 = a;  y4 = b;
				    ya = sqrt (z4*z4 + y4*y4) + y4;
				    x32= x3 - x2;
				    a1 = y3*z4;
				    a2 = x32*z4;
				    a3 = (x4-x2)*y3 - x32*y4;
				    xa = x32*ya + a3 - sqrt (a1*a1 + a2*a2 + a3*a3);
				    b2 = -x3*z4;
				    b3 = x3*y4 - x4*y3;
				    b = sqrt (a1*a1 + b2*b2 + b3*b3);

				    REAL r_inscr = a1*x2 / (b - xa + x3*ya - b3);
				
				    if (el->radio == 0.0)
				        if (vol > 0.0)
				            el->setw(x, y, z, el->n1, el->n2, el->n3, el->n4,
				                el->v1, el->v2, el->v3, el->v4);
		        else
		            {
		            el->radio = 1;
		            r_inscr = 0;
		            }
				    REAL qele = r_inscr / sqrt(el->radio);
				    if (qele < qmin) {
				      qmin = qele;
				      el_peor = el;
				    }
    			}
		   }
  		}
   } // endif check_geometry

  int todo_ok = 1;
  if (check_neighbours)
    {
  	if (conect_ns) 
	  	{
	    Error (WARNING, 0, "Bad element-element conectivity");
	    todo_ok = 0;
	  	}
	  if (carext_n12) 
		  {
	    printf("    Number of external faces (OK:12) = %ld",caras_externas);
	    todo_ok = 0;
	  	}
    }

    if (check_geometry)
	    {
  		if (vmin <= 0.0)
		    todo_ok = 0;

		  printf("\n    Volumen minimo: %f , elemento: %ld-%ld-%ld-%ld",
		        vmin, el_min->n1, el_min->n2, el_min->n3, el_min->n4);
		  printf("\n    Calidad minima: %f , elemento: %ld-%ld-%ld-%ld",
		        qmin, el_peor->n1, el_peor->n2, el_peor->n3, el_peor->n4);
      }
  return todo_ok;
}

//----------------------------------------------------------------------
// Metodo delext de la clase mesh3D: borra los tetraedros de la caja,
//                                 compacta informacion de los restantes
//      OJO: Solo guarda informacion de los nodos de cada elemento

void Dmesh3D::delext (void)
{
  elem *el, *new_pos;
  long new_nel = 0;
  long n1, n2, n3, n4;
  char *nome = "Dmesh3D::delext";
  TraceOn(nome);

  long iel, i;
  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) {
      el = p_elem(i);
      if (el->n2 >= 0) {        // El lugar esta ocupado x 1 elemento
          iel++;

          n1 = el->n1; n2 = el->n2; n3 = el->n3; n4 = el->n4;

          if (n1<nod-8 && n2<nod-8 && n3<nod-8 && n4<nod-8) {//sobrevive
              new_pos = p_elem(new_nel);
              new_nel++;
              new_pos->n1 = n1; new_pos->n2 = n2;
              new_pos->n3 = n3; new_pos->n4 = n4;
          }
      }
  }
  nel = new_nel;
  nod = nod - 8;
  TraceOff(nome);
}

//  Metodo get_size: devuelve tamanho reservado
long Dmesh3D::get_size (void) {
  return (n_vecs*ELE_PER_VEC);
}

//Metodo get_info: devuelve en el string buf, los datos del elemento ele
void Dmesh3D::get_info (long ele, char *buf) {
  elem *el = p_elem(ele);
  if (el->n2 >= 0)
      sprintf (buf, "%5ld:%5ld-%5ld-%5ld-%5ld  %5ld-%5ld-%5ld-%5ld",ele,
          el->n1, el->n2, el->n3, el->n4,
          el->v1, el->v2, el->v3, el->v4);
  else
      sprintf (buf, "%5ld: Space Available", ele);
  return;
}

// Boundary recovery procedure.
// Returns number of faces not recovered.
long Dmesh3D::B_Recovery (T_Surface *surf, long &CwIter, long &C_E_F, long &C_E_E)
    {
    char *nome = "Dmesh3D::B_Recovery";
    long FnotR;
    long inciface[3];
    long i, face, n1, n2;
    int *msw;
    int nmsw;

    TraceOn(nome);
    FnotR = CwIter = C_E_F = C_E_E = 0;

    long nnel = n_vecs * ELE_PER_VEC;
    msw = (int *) mMalloc(nnel * sizeof(int));
    if (!msw)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }
    for (i = 0; i < nnel; i++) msw[i] = 0;
    nmsw = 0;

    for(face = 0; face < surf->GetNumElems(); face++)
       	{
       	acList ListElems(sizeof(long)); // Lista de elementos cercanos
        acNodeList *nl;
        int exists;
        long *pele, ele;
        elem *el;
        surf->GetInciFace(face, inciface);

        for (int kEdge=0; kEdge<3; kEdge++)
				// Edge kEdge:
        {
        n1 = inciface[kEdge];
        n2 = inciface[(kEdge+1)%3];

        ele = an_element[n1];     
        nmsw++;
        msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
        BuildList (n1, ele, msw, nmsw, ListElems);
        exists = FALSE;
        
        for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
            {
            pele = (long *) nl->GetDados();
            ele = *pele;
            el = p_elem(ele);
            if (el->n1 == n2 ||
                el->n2 == n2 ||
                el->n3 == n2 ||
                el->n4 == n2)
                exists = TRUE;
            }
      	            
        if (!exists)
            {
           	int buildError, retval, method;  // 0: recoverEd, 1: repeat, 2: fail.
            retval = Edge_Recover(n1, n2, ListElems, method);
            if      (method == 1) C_E_F++;
            else if (method == 2) C_E_E++;
            while (retval == 1)
                {
                CwIter++;
                ListElems.Clear();
                nmsw++;
                ele = an_element[n1];      
								msw[ele] = nmsw;
                ListElems.InsertFirst(&ele);
                BuildList (n1, ele, msw, nmsw, ListElems);
                //buildError = BuildList (n1, ele, msw, nmsw, ListElems);
                //if(buildError == -1) return -1;
                retval = Edge_Recover(n1, n2, ListElems, method);
	              if      (method == 1) C_E_F++;
                else if (method == 2) C_E_E++;
                }
            if (retval) Error(WARNING, 0,"\n    Edge %ld-%ld Not Recovered!", n1, n2);
            }
        ListElems.Clear();
        } // End Loop Edges
		} // End Loop Faces

	  for (face = 0; face < surf->GetNumElems(); face++)
        {
        acList ListElems(sizeof(long)); // Lista de elementos cercanos
        acNodeList *nl;
        int exists;
        long *pele, ele;
        elem *el;
        surf->GetInciFace(face, inciface);
        n1 = inciface[0];
        nmsw++;
        ele = an_element[n1];
				msw[ele] = nmsw;
        ListElems.InsertFirst(&ele);
        BuildList (n1, ele, msw, nmsw, ListElems);
        n1 = inciface[1];
        n2 = inciface[2];
        exists = FALSE;
        for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
            {
            pele = (long *) nl->GetDados();
            ele = *pele;
            el = p_elem(ele);
            if ((el->n1 == n1 ||
                 el->n2 == n1 ||
                 el->n3 == n1 ||
                 el->n4 == n1)  &&
                (el->n1 == n2 ||
                 el->n2 == n2 ||
                 el->n3 == n2 ||
                 el->n4 == n2))
                exists = TRUE;
            }
        if (!exists)
            {
            int val = 0;
				    printf("\n    Recovering: %ld %ld %ld\n", inciface[0], n1, n2);
				    FnotR += Face_Recover(inciface[0], n1, n2, ListElems, msw, nmsw);
				    // Test removed in 29/04/2008
				    //val = Face_Recover(inciface[0], n1, n2, ListElems, msw, nmsw);
				    //if(val == -1) return -1;
            //FnotR += val;
            }

        ListElems.Clear();
        }  // Next face

        
    mFree(msw);   
    TraceOff(nome);
    return FnotR;
}

int Dmesh3D::BuildList(long node, long ele, int huge *msw, int nmsw, acList &ListElems)
{
    long v1, v2, v3;
    elem *el = p_elem(ele);
    int val = 0;

    if (node == el->n1)
        {
        v1 = el->v2;
        v2 = el->v3;
        v3 = el->v4;
        }
    else if (node == el->n2)
        {
        v1 = el->v1;
        v2 = el->v3;
        v3 = el->v4;
        }
    else if (node == el->n3)
        {
        v1 = el->v1;
        v2 = el->v2;
        v3 = el->v4;
        }
    else if (node == el->n4)
        {
        v1 = el->v1;
        v2 = el->v2;
        v3 = el->v3;
        }
    else
        {
        cout << "\ndmesh3d: Error while building list." << endl;
        return -1;
        //Error (FATAL_ERROR, 1,"BuildList, Element: %ld %ld %ld %ld, Node: %ld", el->n1, el->n2, el->n3, el->n4, node);
        }
        
    if (msw[v1] != nmsw)
        {
				msw[v1] = nmsw;
        ListElems.InsertFirst(&v1);
        val = BuildList(node, v1, msw, nmsw, ListElems);
        if(val == -1) return -1;
        }
    if (msw[v2] != nmsw)
        {
				msw[v2] = nmsw;
        ListElems.InsertFirst(&v2);
        val = BuildList(node, v2, msw, nmsw, ListElems);
        if(val == -1) return -1;        
        }
    if (msw[v3] != nmsw)
        {
				msw[v3] = nmsw;
        ListElems.InsertFirst(&v3);
        val = BuildList(node, v3, msw, nmsw, ListElems);
        if(val == -1) return -1;        
        }
    return val;
}

int Dmesh3D::Edge_Recover(long n1, long n2, acList &ListElems,
        int &caso)
    {
    acNodeList *pl;
    long *pele, ele, elev;
    long fn1, fn2, fn3;
    long iaux;
    elem *el, *elv;
    int Case;  // 0: undefined, 1: face, 2: crossed edges.
    REAL xo, yo, zo, x1, x2, x3, y1, y2, y3, z1, z2, z3;
    REAL xn1, yn1, zn1, volt, vol1, vol2, vol3;
    REAL tol;
    FILE *fp;
    fp = GetMensFile();
//    fprintf(fp, " Recuperando arista: %ld %ld\n", n1, n2);

    tol = 0.00001;

    Case = 0;
    for (pl = ListElems.GetHead(); pl && !Case; pl = pl->next)
        {
        pele = (long *) pl->GetDados();
        ele = *pele;
        el = p_elem(ele);
        if (el->n1 == n1)  // Find opposite face
            { fn1 = el->n2; fn2 = el->n3; fn3 = el->n4; elev = el->v1; }
        else if (el->n2 == n1)
            { fn1 = el->n1; fn2 = el->n4; fn3 = el->n3; elev = el->v2; }
        else if (el->n3 == n1)
            { fn1 = el->n1; fn2 = el->n2; fn3 = el->n4; elev = el->v3; }
        else if (el->n4 == n1)
            { fn1 = el->n1; fn2 = el->n3; fn3 = el->n2; elev = el->v4; }
        elv = p_elem(elev);

// Chech edge <-> opposite face intersection
        xn1 = x[n1]; yn1 = y[n1]; zn1 = z[n1];
        x1 = x[fn1] - xn1; y1 = y[fn1] - yn1; z1 = z[fn1] - zn1;
        x2 = x[fn2] - xn1; y2 = y[fn2] - yn1; z2 = z[fn2] - zn1;
        x3 = x[fn3] - xn1; y3 = y[fn3] - yn1; z3 = z[fn3] - zn1;
        xo = x[n2]  - xn1; yo = y[n2]  - yn1; zo = z[n2]  - zn1;

        volt = x3 * (y1*z2 - z1*y2) +
               y3 * (z1*x2 - x1*z2) +
               z3 * (x1*y2 - y1*x2); // Volumen para comparacion

        vol1 = xo * (y1*z2 - z1*y2) +
               yo * (z1*x2 - x1*z2) +
               zo * (x1*y2 - y1*x2);   vol1 = vol1 / volt;
        if (vol1 < -tol) continue;  // Proximo elemento
        vol2 = xo * (y2*z3 - z2*y3) +
               yo * (z2*x3 - x2*z3) +
               zo * (x2*y3 - y2*x3);   vol2 = vol2 / volt;
        if (vol2 < -tol) continue;  // Proximo elemento
        vol3 = xo * (y3*z1 - z3*y1) +
               yo * (z3*x1 - x3*z1) +
               zo * (x3*y1 - y3*x1);   vol3 = vol3 / volt;
        if (vol3 < -tol) continue;  // Proximo elemento

        if (vol1 < tol)  // Caso cruce de aristas
            {
            Case = 2;
            break;
            }
        if (vol2 < tol)  // Caso cruce de aristas
            {
            iaux= fn1;
            fn1 = fn2;
            fn2 = fn3;
            fn3 = iaux;
            Case = 2;
            break;
            }
        if (vol3 < tol)  // Caso cruce de aristas
            {
            iaux= fn3;
            fn3 = fn2;
            fn2 = fn1;
            fn1 = iaux;
            Case = 2;
            break;
            }

        Case = 1;  // Case edge <-> face intersection

        }  // End loop n1's neighbours
    caso = Case;
/*
    if (Case == 1)
    fprintf(fp, " Caso cruce cara - arista\n");
    else
    fprintf(fp, " Caso cruce arista - arista\n");
*/

    if (Case == 1) // Edge <-> Face intersection
        {
        long nop, new_el;
        long v1f1, v1f2, v1f3, vof1, vof2, vof3;
        el->rot_log(n1, fn1, fn2);
        elv->rot_log(fn1, fn2, fn3);
        nop = elv->n4;
        v1f1 = el->v2;  v1f2 = el->v3;  v1f3 = el->v4;
        vof1 = elv->v1; vof2 = elv->v2; vof3 = elv->v3; 

        // Swap Face -> Edge
        new_el = AddElement();

        el->set            (n1,fn1,fn2,nop, vof3, elev,   new_el, v1f3);
        elv->set           (n1,fn2,fn3,nop, vof1, new_el, ele,    v1f1);
        p_elem(new_el)->set(n1,fn3,fn1,nop, vof2, ele,    elev,   v1f2);
        an_element[n1] = an_element[nop] = ele;
        an_element[fn1] = an_element[fn2] = ele;
        an_element[fn3] = elev;
        elem *pvv;
        pvv = p_elem(vof3);
        pvv->rot_log(fn1, fn2, nop);
        pvv->v4 = ele;
        pvv = p_elem(v1f3);
        pvv->rot_log(fn2, fn1, n1);
        pvv->v4 = ele;
        pvv = p_elem(vof1);
        pvv->rot_log(fn2, fn3, nop);
        pvv->v4 = elev;
        pvv = p_elem(v1f1);
        pvv->rot_log(fn3, fn2, n1);
        pvv->v4 = elev;
        pvv = p_elem(vof2);
        pvv->rot_log(fn3, fn1, nop);
        pvv->v4 = new_el;
        pvv = p_elem(v1f2);
        pvv->rot_log(fn1, fn3, n1);
        pvv->v4 = new_el;
// Check for element <-> anti-element annihilation
// and for negative volume elements
        if (el->v1 == el->v4)
            {
            el->rot_log(n1, nop, fn1);
            DelPair (ele, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }
        if (elv->v1 == elv->v4)
            {
            elv->rot_log(n1, nop, fn2);
            DelPair (elev, elv->v1);
            }
        else
            {
            if (elv->volume(x,y,z) <=0)
                {
                //Error(WARNING, 0, "Volume: %lf", elv->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        elv->n1, elv->n2, elv->n3, elv->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }
        el = p_elem(new_el);
        if (el->v1 == el->v4)
            {
            el->rot_log(n1, nop, fn3);
            DelPair (new_el, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <=0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                }
            }

//      fprintf (fp, "Se recupero arista: %ld %ld\n", n1, nop);
        if (nop != n2)
            return 1;
        else
            return 0;
        }
    else if (Case == 2)
        {
        long nop, nn;
        el->rot_log(n1, fn1, fn2);
        elv->rot_log(fn1, fn2, fn3);
        nop = elv->n4;
        // Cuenta tetraedros que concurren a la arista fn1, fn2
        long v_ele;
        elem *v_el;
        v_ele = el->v4;
        v_el = p_elem(v_ele);
        v_el->rot_log(n1, fn2, fn1);
        if (v_el->v1 == elv->v3) // 4 tetraedros
            {
//    fprintf(fp, " Caso cuatro tetraedros\n");
            long v_elev;
            elem *v_elv;
            long v11n, v1n2, v123, v131;
            long vo1n, von2, vo23, vo31;
            v_elev = elv->v3;
            v_elv = p_elem(v_elev);
            v_elv->rot_log(nop, fn1, fn2);
            nn = v_el->n4;
            v11n = v_el->v2;
            v1n2 = v_el->v3;
            v123 = el->v2;
            v131 = el->v3;
            vo1n = v_elv->v3;
            von2 = v_elv->v2;
            vo23 = elv->v1;
            vo31 = elv->v2;
            el->set    (n1, nop, fn3, fn1, vo31, v131, elev,   v_elev);
            elv->set   (n1, nop, fn1, nn,  vo1n, v11n, v_ele,  ele);
            v_el->set  (n1, nop, nn,  fn2, von2, v1n2, v_elev, elev);
            v_elv->set (n1, nop, fn2, fn3, vo23, v123, ele,    v_ele);
            an_element[n1] = an_element[nop] = ele;
            an_element[fn1] = an_element[nn] = elev;
            an_element[nn] = an_element[fn2] = v_ele;
            an_element[fn2] = an_element[fn3] = v_elev;
            an_element[fn3] = an_element[fn1] = ele;
            // Actualiza informacion de vecinos
            elem *pvv;
            pvv = p_elem(vo31);
            pvv->rot_log(nop, fn3, fn1);
            pvv->v4 = ele;
            pvv = p_elem(v131);
            pvv->rot_log(n1, fn1, fn3);
            pvv->v4 = ele;

            pvv = p_elem(vo1n);
            pvv->rot_log(nop, fn1, nn);
            pvv->v4 = elev;
            pvv = p_elem(v11n);
            pvv->rot_log(n1, nn, fn1);
            pvv->v4 = elev;

            pvv = p_elem(von2);
            pvv->rot_log(nop, nn, fn2);
            pvv->v4 = v_ele;
            pvv = p_elem(v1n2);
            pvv->rot_log(n1, fn2, nn);
            pvv->v4 = v_ele;

            pvv = p_elem(vo23);
            pvv->rot_log(nop, fn2, fn3);
            pvv->v4 = v_elev;
            pvv = p_elem(v123);
            pvv->rot_log(n1, fn3, fn2);
            pvv->v4 = v_elev;

//  Check element <-> anti-element annihilation
//  and for negative volumes
            if (el->v1 == el->v2)
                DelPair (ele, el->v1);
            else
                {
                if (el->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (elv->v1 == elv->v2)
                DelPair (elev, elv->v1);
            else
                {
                if (elv->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", elv->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        elv->n1, elv->n2, elv->n3, elv->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (v_el->v1 == v_el->v2)
                DelPair (v_ele, v_el->v1);
            else
                {
                if (v_el->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", v_el->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        v_el->n1, v_el->n2, v_el->n3, v_el->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }
            if (v_elv->v1 == v_elv->v2)
                DelPair (v_elev, v_elv->v1);
            else
                {
                if (v_elv->volume(x,y,z) <=0)
                    {
                    //Error(WARNING, 0, "Volume: %lf", v_elv->volume(x,y,z));
                    fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        v_elv->n1, v_elv->n2, v_elv->n3, v_elv->n4); 
                    fprintf (fp, "Regenerating: %ld %ld\n", n1, n2);
                    }
                }

//          fprintf (fp, "Se recupero arista: %ld %ld\n", n1, nop);
            if (nop != n2)
                return 1;
            else
                return 0;
            }
        else
            {
// Edge that must be removed has more than 4 tetrahedra
// Count # of tetrahedra
            int numt = 1;  // ele
            int posn2 = 0;
            elev = el->v1;
            nn = el->n4;
            do
                {
                numt++;
                elv = p_elem(elev);
                elv->rot_log(nn, fn1, fn2);
                nn = elv->n4;
                if (nn == n2) posn2 = numt;
                elev = elv->v1;
                } while (elev != ele);
            if (posn2 == 0)
                {
                Error(WARNING, 1, "\nNo encontro n2 en cluster");
                return 2;
                }

//    fprintf(fp, " Caso %d tetraedros\n", numt);
            long vfn1, vfn2; vfn1 = vfn2 = -2;
            long *tets = (long *) mMalloc(numt * sizeof(long));
            if (!tets)
        { char buf[80]; GetError(1, buf); Error (FATAL_ERROR, 1, buf); }

            numt = 1;  // ele
            tets[0] = ele;
            elev = el->v1;
            nn = el->n4;
            do
                {
                tets[numt] = elev;
                numt++;
                elv = p_elem(elev);
                elv->rot_log(nn, fn1, fn2);
                nn = elv->n4;
                elev = elv->v1;
                } while (elev != ele);

            Reg_Half_Cluster(posn2,      tets,       &vfn1, &vfn2);
            Reg_Half_Cluster(numt-posn2, tets+posn2, &vfn1, &vfn2);
//          fprintf (fp, "Se recupero arista: %ld %ld\n", n1, n2);

            mFree(tets);
            return 0;
            }
        }
    // Case ???????
    Error(WARNING, 1, "\nCase ???????");
    return 2;
    }  // End method Edge_Recover

void Dmesh3D::Reg_Half_Cluster (int numt, long *tets, long *v1,long *v2)
    {
    long fn1, fn2;
    elem *el;
    el = p_elem(tets[0]);
    fn1 = el->n2; fn2 = el->n3;
    FILE *fp = GetMensFile();

    while (numt > 3) // Try to reduce size
        {
        long *n, *v1, *v2;
        int i, posopt = 0;
        REAL x1,x2,x3, y1,y2,y3, z1,z2,z3;
        REAL vol1, vol2, vmin, volopt = -1.0e+30;
        n  = (long *) mMalloc ((numt+1) * sizeof(long));
        v1 = (long *) mMalloc ( numt    * sizeof(long));
        v2 = (long *) mMalloc ( numt    * sizeof(long));
        for (i = 0; i < numt; i++)
            {
            el = p_elem(tets[i]);
            n[i] = el->n1; v1[i] = el->v3; v2[i] = el->v2;
            }
        n[i] = el->n4;
        for (i = 1; i < numt; i++)
            {
            x1 = x[n[i-1]] - x[n[i]];
            y1 = y[n[i-1]] - y[n[i]];
            z1 = z[n[i-1]] - z[n[i]];
            x2 = x[n[i+1]] - x[n[i]];
            y2 = y[n[i+1]] - y[n[i]];
            z2 = z[n[i+1]] - z[n[i]];
            x3 = x[fn1] - x[n[i]];
            y3 = y[fn1] - y[n[i]];
            z3 = z[fn1] - z[n[i]];
            vol1 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
            x3 = -(x[fn2] - x[n[i]]);
            y3 = -(y[fn2] - y[n[i]]);
            z3 = -(z[fn2] - z[n[i]]);
            vol2 = x1*(y2*z3-z2*y3) + y1*(z2*x3-x2*z3) + z1*(x2*y3-y2*x3);
            if (v1[i-1] != v1[i])
                vmin = vol1;
            else
                vmin = 1.0e+20;
            if (v2[i-1] != v2[i])
                if (vol2 < vmin) vmin = vol2;
            if (vmin > volopt)
                {
                volopt = vmin;
                posopt = i;
                }
            }
// Genera par de tetraedros, actualiza vectores tets, n, v1 y v2
        long ele1, ele2;
        i = posopt;
        ele1 = tets[i-1];
        ele2 = AddElement();
        el = p_elem(ele1);
        el->set(n[i],n[i-1],n[i+1],fn1, tets[i],v1[i],v1[i-1],ele2);
        el = p_elem(v1[i]);
        el->rot_log(n[i+1], n[i], fn1);
        el->v4 = ele1;
        el = p_elem(v1[i-1]);
        el->rot_log(n[i], n[i-1], fn1);
        el->v4 = ele1;
        
        el = p_elem(ele2);
        el->set(n[i],n[i+1],n[i-1],fn2, tets[i],v2[i-1],v2[i],ele1);
        el = p_elem(v2[i]);
        el->rot_log(n[i], n[i+1], fn2);
        el->v4 = ele2;
        el = p_elem(v2[i-1]);
        el->rot_log(n[i-1], n[i], fn2);
        el->v4 = ele2;
        
        an_element[n[i]] = ele1;
        el = p_elem(tets[i]);
        el->n1 = n[i-1];
        el->v2 = ele2;
        el->v3 = ele1;
        while (i < numt)
            {
            tets[i-1] = tets[i];
            i++;
            }
        numt--;
        mFree(n);
        mFree(v1);
        mFree(v2);
// Check for element - anti-element annihilation
// and for negative volumes
        el = p_elem(ele1);
        if (el->v2 == el->v3)
            {
            el->rot_log(el->n2, el->n3, el->n1);
            DelPair (ele1, el->v2);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n[0],n[numt+1]);
                }
            }
        el = p_elem(ele2);
        if (el->v2 == el->v3)
            {
            el->rot_log(el->n2, el->n3, el->n1);
            DelPair (ele2, el->v2);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n[0],n[numt+1]);
                }
            }
        }

    if (numt == 3) // Case: Octahedron
        {
//
//                      fn1
//                      //\\
//                    / /  \ \
//                  /  /    \  \
//                n2- /- - - \- n3
//                 | /        \ |
//                 |/__________\|
//                n1\          /n4
//                    \      /
//                      \  /
//                       \/
//                      fn2
//
        long n1, n2, n3, n4;
        long v11, v12, v13, v14;
        long v21, v22, v23, v24;
        el = p_elem(tets[0]);
        n1 = el->n1; v11 = el->v3; v21 = el->v2;
        el = p_elem(tets[1]);
        n2 = el->n1; v12 = el->v3; v22 = el->v2;
        el = p_elem(tets[2]);
        n3 = el->n1; v13 = el->v3; v23 = el->v2;
        n4 = el->n4; v14 = *v1;    v24 = *v2;
        // Check for regeneration on edge n1 - n3:
        // Tetrahedra: n1 n3 n2 fn1, n1 n3 fn1 n4,
        //             n1 n3 n4 fn2, n1 n3 fn2 n2
        REAL vol, vm13, vm24;
        REAL x1,x2,x3, y1,y2,y3, z1,z2,z3;
        vm13 = 1.0e+25;
        x3 = x[n3]-x[n1];  y3 = y[n3]-y[n1];  z3 = z[n3]-z[n1];
        x1 = x[n2]-x[n1];  y1 = y[n2]-y[n1];  z1 = z[n2]-z[n1];
        x2 = x[fn1]-x[n1]; y2 = y[fn1]-y[n1]; z2 = z[fn1]-z[n1];
        if (v11 != v12)
          vm13 = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        else vm13 = 1.0e+25;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n4]-x[n1]; y2 = y[n4]-y[n1]; z2 = z[n4]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm13) vm13 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[fn2]-x[n1]; y2 = y[fn2]-y[n1]; z2 = z[fn2]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm13) vm13 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n2]-x[n1]; y2 = y[n2]-y[n1]; z2 = z[n2]-z[n1];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (v21 != v22 && vol < vm13) vm13 = vol;
        // Check for regeneration on edge n2 - n4:
        // Tetrahedra: n2 n4 n3 fn1, n2 n4 fn1 n1,
        //             n2 n4 n1 fn2, n2 n4 fn2 n3
        x3 = x[n4]-x[n2];  y3 = y[n4]-y[n2];  z3 = z[n4]-z[n2];
        x1 = x[n3]-x[n2];  y1 = y[n3]-y[n2];  z1 = z[n3]-z[n2];
        x2 = x[fn1]-x[n2]; y2 = y[fn1]-y[n2]; z2 = z[fn1]-z[n2];
        if (v12 != v13)
          vm24 = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        else vm24 = 1.0e+25;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n1]-x[n2]; y2 = y[n1]-y[n2]; z2 = z[n1]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm24) vm24 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[fn2]-x[n2]; y2 = y[fn2]-y[n2]; z2 = z[fn2]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (vol < vm24) vm24 = vol;
        x1 = x2; y1 = y2; z1 = z2;
        x2 = x[n3]-x[n2]; y2 = y[n3]-y[n2]; z2 = z[n3]-z[n2];
        vol = x3*(y1*z2-z1*y2) + y3*(z1*x2-x1*z2) + z3*(x1*y2-y1*x2);
        if (v22 != v23 && vol < vm24) vm24 = vol;
        long ele1, ele2, ele3, ele4;
        ele1 = tets[0]; ele2 = tets[1];
        ele3 = tets[2]; ele4 = AddElement();

        if (vm13 < vm24) // Regeneration on edge 2-4
            {
            el = p_elem(ele1);          // 1 elemento
            el->set(n2, n4, n3, fn1, v13, v12, ele2, ele4);
            an_element[n2] = an_element[n4] =
            an_element[n3] = ele1;
            el = p_elem(v13);
            el->rot_log(n3, fn1, n4);
            el->v4 = ele1;
            el = p_elem(v12);
            el->rot_log(n2, fn1, n3);
            el->v4 = ele1;

            el = p_elem(ele2);          // 2 elementos
            el->set(n2, n4, fn1, n1, v14, v11, ele3, ele1);
            an_element[fn1] = ele2;
            if (v14 == -2)
                *v1 = ele2;
            else
                {
                el = p_elem(v14);
                el->rot_log(n1, n4, fn1);
                el->v4 = ele2;
                }
            el = p_elem(v11);
            el->rot_log(n2, n1, fn1);
            el->v4 = ele2;

            el = p_elem(ele3);          // 3 elementos
            el->set(n2, n4, n1, fn2, v24, v21, ele4, ele2);
            an_element[n1] = ele3;
            if (v24 == -2)
                *v2 = ele3;
            else
                {
                el = p_elem(v24);
                el->rot_log(n1, fn2, n4);
                el->v4 = ele3;
                }
            el = p_elem(v21);
            el->rot_log(n1, n2, fn2);
            el->v4 = ele3;

            el = p_elem(ele4);          // 4 elementos
            el->set(n2, n4, fn2, n3, v23, v22, ele1, ele3);
            an_element[fn2] = ele4;
            el = p_elem(v23);
            el->rot_log(n3, n4, fn2);
            el->v4 = ele4;
            el = p_elem(v22);
            el->rot_log(n3, fn2, n2);
            el->v4 = ele4;
//      fprintf(fp, "Se genero arista: %ld %ld\n", n2, n4);
            }
        else // Regeneration on edge 1-3
            {
            el = p_elem(ele1);          // 1 elemento
            el->set(n1, n3, n2, fn1, v12, v11, ele2, ele4);
            an_element[n1] = an_element[n3] =
            an_element[n2] = ele1;
            el = p_elem(v11);
            el->rot_log(n1, fn1, n2);
            el->v4 = ele1;
            el = p_elem(v12);
            el->rot_log(n2, fn1, n3);
            el->v4 = ele1;

            el = p_elem(ele2);          // 2 elementos
            el->set(n1, n3, fn1, n4, v13, v14, ele3, ele1);
            an_element[fn1] = ele2;
            el = p_elem(v13);
            el->rot_log(n4, n3, fn1);
            el->v4 = ele2;
            if (v14 == -2)
                *v1 = ele2;
            else
                {
                el = p_elem(v14);
                el->rot_log(n1, fn1, n4);
                el->v4 = ele2;
                }

            el = p_elem(ele3);          // 3 elementos
            el->set(n1, n3, n4, fn2, v23, v24, ele4, ele2);
            an_element[n4] = ele3;
            el = p_elem(v23);
            el->rot_log(n3, n4, fn2);
            el->v4 = ele3;
            if (v24 == -2)
                *v2 = ele3;
            else
                {
                el = p_elem(v24);
                el->rot_log(n1, fn2, n4);
                el->v4 = ele3;
                }

            el = p_elem(ele4);          // 4 elementos
            el->set(n1, n3, fn2, n2, v22, v21, ele1, ele3);
            an_element[fn2] = ele4;
            el = p_elem(v22);
            el->rot_log(n2, n3, fn2);
            el->v4 = ele4;
            el = p_elem(v21);
            el->rot_log(n2, fn2, n1);
            el->v4 = ele4;
//      fprintf(fp, "Se genero arista: %ld %ld\n", n1, n3);
            }
//      int volneg = 0;
// Check for element - anti-element annihilation
        el = p_elem(ele1);
        if (el->v1 == el->v2)
            DelPair (ele1, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele2);
        if (el->v1 == el->v2)
            DelPair (ele2, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele3);
        if (el->v1 == el->v2)
            DelPair (ele3, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
        el = p_elem(ele4);
        if (el->v1 == el->v2)
            DelPair (ele4, el->v1);
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n4);
                }
            }
            
        }
    else if (numt == 2)
//                      fn1
//                      /\\
//                     /  \ \
//                    /    \  \
//                   /      \   \
//                  /        \  /| n1
//              n3 /__________\/ |
//                 \        n2/ /
//                   \       / /
//                     \    / /
//                       \ //
//                      fn2
        {
        long ele1, ele2;
        long n1, n2, n3;
        long v11, v12, v13;
        long v21, v22, v23;
        el = p_elem(tets[0]);
        n1 = el->n1; v11 = el->v3; v21 = el->v2;
        el = p_elem(tets[1]);
        n2 = el->n1; v12 = el->v3; v22 = el->v2;
        n3 = el->n4; v13 = *v1;    v23 = *v2;

        ele1 = tets[0]; ele2 = tets[1];
        el = p_elem(ele1);
        el->set(n1, fn1, n3, n2, v12, ele2, v11, v13);
        an_element[n1] = an_element[fn1] =
        an_element[n3] = an_element[n2] = ele1;
        el = p_elem (v12);
        el->rot_log (n2, fn1, n3);
        el->v4 = ele1;
        el = p_elem (v11);
        el->rot_log (n1, fn1, n2);
        el->v4 = ele1;
        if (v13 == -2)
            *v1 = ele1;
        else
            {
            el = p_elem(v13);
            el->rot_log(n3, fn1, n1);
            el->v4 = ele1;
            }

        el = p_elem(ele2);
        el->set(n3, fn2, n1, n2, v21, ele1, v22, v23);
        an_element[fn2] = ele2;
        el = p_elem (v21);
        el->rot_log (n2, fn2, n1);
        el->v4 = ele2;
        el = p_elem (v22);
        el->rot_log (n3, fn2, n2);
        el->v4 = ele2;
        if (v23 == -2)
            *v2 = ele2;
        else
            {
            el = p_elem(v23);
            el->rot_log(n1, fn2, n3);
            el->v4 = ele2;
            }

        el = p_elem(ele1);
        if (el->v1 == el->v3)
            {
            el->rot_log(el->n1, el->n3, el->n4);
            DelPair (ele1, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n3);
                }
            }
        el = p_elem(ele2);
        if (el->v1 == el->v3)
            {
            el->rot_log(el->n1, el->n3, el->n4);
            DelPair (ele2, el->v1);
            }
        else
            {
            if (el->volume(x,y,z) <= 0)
                {
                //Error(WARNING, 0, "Volume: %lf", el->volume(x,y,z));
                fprintf (fp, "Element: %ld %ld %ld %ld\n",
                        el->n1, el->n2, el->n3, el->n4); 
                fprintf (fp, "Regenerating: %ld %ld\n", n1, n3);
                }
            }
        }
    return;
    }

void Dmesh3D::DelPair (long ele1, long ele2)
    {
// Elimina elementos ele1 y ele2, debido a que ele1->v1 = ele1->v2 =ele2
// Se elimina arista ele1->n3 -- ele1->n4
// Hay que conectar ele1->v3 con el vecino de ele2 opuesto a n3
// Hay que conectar ele1->v4 con el vecino de ele2 opuesto a n4
// Actualizar an_element
    elem *el1, *el2, *vs1, *vs2, *vi1, *vi2;
    long ves1, ves2, vei1, vei2;
    el1 = p_elem(ele1);
    ves1 = el1->v3; vei1 = el1->v4;
    el2 = p_elem(ele2);
    if (el2->n1 == el1->n3)
        ves2 = el2->v1;
    else if (el2->n1 == el1->n4)
        vei2 = el2->v1;
    if (el2->n2 == el1->n3)
        ves2 = el2->v2;
    else if (el2->n2 == el1->n4)
        vei2 = el2->v2;
    if (el2->n3 == el1->n3)
        ves2 = el2->v3;
    else if (el2->n3 == el1->n4)
        vei2 = el2->v3;
    if (el2->n4 == el1->n3)
        ves2 = el2->v4;
    else if (el2->n4 == el1->n4)
        vei2 = el2->v4;
    vs1 = p_elem(ves1);
    if      (vs1->v1 == ele1) vs1->v1 = ves2;
    else if (vs1->v2 == ele1) vs1->v2 = ves2;
    else if (vs1->v3 == ele1) vs1->v3 = ves2;
    else if (vs1->v4 == ele1) vs1->v4 = ves2;
    vs2 = p_elem(ves2);
    if      (vs2->v1 == ele2) vs2->v1 = ves1;
    else if (vs2->v2 == ele2) vs2->v2 = ves1;
    else if (vs2->v3 == ele2) vs2->v3 = ves1;
    else if (vs2->v4 == ele2) vs2->v4 = ves1;
    vi1 = p_elem(vei1);
    if      (vi1->v1 == ele1) vi1->v1 = vei2;
    else if (vi1->v2 == ele1) vi1->v2 = vei2;
    else if (vi1->v3 == ele1) vi1->v3 = vei2;
    else if (vi1->v4 == ele1) vi1->v4 = vei2;
    vi2 = p_elem(vei2);
    if      (vi2->v1 == ele2) vi2->v1 = vei1;
    else if (vi2->v2 == ele2) vi2->v2 = vei1;
    else if (vi2->v3 == ele2) vi2->v3 = vei1;
    else if (vi2->v4 == ele2) vi2->v4 = vei1;
// Actualiza an_element
    an_element[el1->n1] = 
    an_element[el1->n2] =
    an_element[el1->n3] = vei1;
    an_element[el1->n4] = ves1;
// Borra elementos ele1, ele2
    el2->n1 = firstfree;
    el2->n2 = -1;
    nel--;
    el1->n1 = ele2;
    el1->n2 = -1;
    nel--;
    firstfree = ele1;
    }

int Dmesh3D::Face_Recover(long n1, long n2, long n3, acList &ListElems,int huge *msw, int &nmsw)
{
  int retval, changes;
  long ele;
  changes = 1;
	
  while (changes)
      {
      changes = 0;
      retval = 1;
      while (retval == 1)
          {
          retval = Face_Rec_Edge (n1, n2, n3, ListElems);
          if (!retval) return -1;
//          if (retval == 1) changes = 1;
          }
      ListElems.Clear();
      nmsw++;
      ele = an_element[n2];
			msw[ele] = nmsw;
      ListElems.InsertFirst(&ele);
      BuildList (n2, ele, msw, nmsw, ListElems);
      retval = 1;    
      while (retval == 1)
          {
          retval = Face_Rec_Edge (n2, n3, n1, ListElems);
          if (!retval) return -1;
          if (retval == 1) changes = 1;
          }
      ListElems.Clear();
      nmsw++;
      ele = an_element[n3];
			msw[ele] = nmsw;
      ListElems.InsertFirst(&ele);
			BuildList (n3, ele, msw, nmsw, ListElems);
      retval = 1;
      while (retval == 1)
          {
          retval = Face_Rec_Edge (n3, n1, n2, ListElems);
          if (!retval) return -1;
          if (retval == 1) changes = 1;
          }       
      }
  return 1;
}

int Dmesh3D::Face_Rec_Edge(long n1, long n2, long n3, acList &ListElems)
{
    acNodeList *nl;
    long *pel, ele;
    elem *el;
		// Search for an element containing Edge n1 -> n2
    ele = -1;
    for (nl = ListElems.GetHead(); nl; nl = nl->next)
        {
        pel = (long *) nl->GetDados();
        ele = *pel;
        el = p_elem(ele);
        if (el->n1 == n2 || el->n2 == n2 ||
            el->n3 == n2 || el->n4 == n2)
            break;
        }
    if (ele == -1)
        Error (FATAL_ERROR, 1, "Face_Rec_Edge: not found edge %ld %ld", n1, n2);

		// Rota "el" / el->n1 == n1 && el->n2 == n2
    if (el->n1 != n1 && el->n1 != n2)
        {
        el->rot_log(n1, n2, el->n1);
        if( el->GetElemError() ) return 0;         
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        if( el->GetElemError() ) return 0;        
        }
    else if (el->n2 != n1 && el->n2 != n2)
        {
        el->rot_log(n1, n2, el->n2);
        if( el->GetElemError() ) return 0;         
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        if( el->GetElemError() ) return 0;                
        }
    else if (el->n3 != n1 && el->n3 != n2)
        {
        el->rot_log(n1, n2, el->n3);
        if( el->GetElemError() ) return 0;         
        if (el->n2 != n2) el->rot_log(n1, n2, el->n4);
        if( el->GetElemError() ) return 0;                
        }

// Search for Edge intersecting face
    long nn;
    REAL x1,x2, y1,y2, z1,z2;
    REAL ax,ay,az, v3,v4;
    x1 = x[n2] - x[n1]; y1 = y[n2] - y[n1]; z1 = z[n2] - z[n1];
    x2 = x[n3] - x[n1]; y2 = y[n3] - y[n1]; z2 = z[n3] - z[n1];
    ax = y1*z2 - z1*y2; ay = z1*x2 - x1*z2; az = x1*y2 - y1*x2;
    
    int counter = 0;
    while (1)
        {
        v3 = (x[el->n3]-x[n1]) * ax +
             (y[el->n3]-y[n1]) * ay +
             (z[el->n3]-z[n1]) * az;
        v4 = (x[el->n4]-x[n1]) * ax +
             (y[el->n4]-y[n1]) * ay +
             (z[el->n4]-z[n1]) * az;
        if (v3 < 0 && v4 > 0)
            break;
        nn = el->n4;
        ele = el->v3;
        el = p_elem(ele);
        el->rot_log(n1, n2, nn);
        if( el->GetElemError() ) return 0;                
        
        if(counter++ > nel) 
	        {
					cout << "\nint Dmesh3D::Face_Rec_Edge() : Infinite Loop. Skiping..." << endl;
					return 0;
	        }
        }
    printf("    Edge: %ld %ld", el->n3, el->n4);

		// Check for 3 elements sharing this Edge (el->n3 - el->n4)
    long neigh1, neigh2;
    neigh1 = el->v1;
    neigh2 = el->v2;
    elem *neig1, *neig2;
    neig1 = p_elem(neigh1);
    neig1->rot_log(n2, el->n3, el->n4);
    if( neig1->GetElemError() ) return 0;            
    if (neig1->v1 == neigh2) // 3 elements! => swap edge->face
        {
        long ve123, ve124, ve2n3, ve2n4, ven13, ven14;
        neig2 = p_elem(neigh2);
        nn = neig1->n4;
        neig2->rot_log(nn, el->n3, el->n4);
        if( neig2->GetElemError() ) return 0;                
        ve123 = el->v4;    ve124 = el->v3;
        ve2n3 = neig1->v3; ve2n4 = neig1->v2;
        ven13 = neig2->v3; ven14 = neig2->v2;
        neig1->set(n1, n2, nn, el->n4, ve2n4, ven14, ve124, neigh2);
        neig2->set(n1, nn, n2, el->n3, ve2n3, ve123, ven13, neigh1);
        el->n1 = firstfree;
        el->n2 = -1;
        firstfree = ele;
        nel--;
        el = p_elem(ven14);
        el->rot_log(nn, n1, neig1->n4);
        if( el->GetElemError() ) return 0;                
        el->v4 = neigh1;
        el = p_elem(ve124);
        el->rot_log(n1, n2, neig1->n4);
        if( el->GetElemError() ) return 0;              
        el->v4 = neigh1;
        el = p_elem(ve2n4);
        el->rot_log(n2, nn, neig1->n4);
        if( el->GetElemError() ) return 0;              
        el->v4 = neigh1;
        el = p_elem(ve123);
        el->rot_log(n2, n1, neig2->n4);
        if( el->GetElemError() ) return 0;              
        el->v4 = neigh2;
        el = p_elem(ve2n3);
        el->rot_log(nn, n2, neig2->n4);
        if( el->GetElemError() ) return 0;              
        el->v4 = neigh2;
        el = p_elem(ven13);
        el->rot_log(n1, nn, neig2->n4);
        if( el->GetElemError() ) return 0;              
        el->v4 = neigh2;
        an_element[neig1->n1] = an_element[neig1->n2] =
        an_element[neig1->n3] = an_element[neig1->n4] = neigh1;
        an_element[neig2->n4] = neigh2;
        if (nn == n3)
            return 0;
        else
            return 1;
        }
    return 2;
}

long Dmesh3D::AddElement(void)
    {
    long retval;
    if (firstfree >= n_vecs*ELE_PER_VEC) // Expandir vector de elementos
        {
        elem ** new_v;
        int i;
    
        new_v = (elem **) mMalloc ((n_vecs+1)*sizeof(elem *));
        if (!new_v)
            Error (FATAL_ERROR, 1, "Can't realloc elems");
        for (i=0; i<n_vecs; i++)
            new_v[i] = v_elem[i];
        mFree(v_elem);
        v_elem = new_v;
        v_elem[n_vecs] = (elem *) mMalloc (ELE_PER_VEC*sizeof(elem));
        if (!v_elem[n_vecs])
            Error (FATAL_ERROR, 1, "Ins. Memory (Dmesh3D::AddElem)");

        long j = n_vecs*ELE_PER_VEC;
        for (i = 0; i<ELE_PER_VEC; i++, j++)
            {
            v_elem[n_vecs][i].n1 = j+1;
            v_elem[n_vecs][i].n2 = -1;
            }

        n_vecs++;
        }
    retval = firstfree;
    firstfree = p_elem(firstfree)->n1;
    nel++;
    return (retval);
    }

long Dmesh3D::Delext (T_Surface *surf)
{
	char *nome = "Dmesh3D::Delext";
   long inciface[3];
   long i, face, n1, n2, n3;
   long   *swi;
   int   *msw;
   int nmsw;

   TraceOn(nome);

   long nnel = n_vecs * ELE_PER_VEC;
   msw = (int   *) mMalloc(nnel * sizeof(int));
   swi = (long   *) mMalloc(nnel * sizeof(long));
   if (!msw || !swi)
		{
		char buf[80]; 
		GetError(1, buf); 
		Error (FATAL_ERROR, 1, buf); 
		}

   for (i = 0; i < nnel; i++)
		{
		swi[i] = 0;
		msw[i] = 0;
		}
   nmsw = 0;

   for (face = 0; face < surf->GetNumElems(); face++)
		{
      acList ListElems(sizeof(long)); // Lista de elementos cercanos
      acNodeList *nl;
      int exists;
      long *pele, ele;
      elem *el;
      surf->GetInciFace(face, inciface);

		if (!surf->GetFaceSW(face))
			continue;

		// Search for elements sharing this face
      n1 = inciface[0];
      n2 = inciface[1];
		n3 = inciface[2];
      nmsw++;
      ele = an_element[n1];
		msw[ele] = nmsw;
      ListElems.InsertFirst(&ele);
		BuildList (n1, ele, msw, nmsw, ListElems);
      exists = FALSE;
      for (nl = ListElems.GetHead(); nl && !exists; nl = nl->next)
			{
         pele = (long *) nl->GetDados();
         ele = *pele;
         el = p_elem(ele);
         if ((el->n1 == n2 || el->n2 == n2 || el->n3 == n2 || el->n4 == n2)  &&
             (el->n1 == n3 || el->n2 == n3 || el->n3 == n3 || el->n4 == n3))
                exists = TRUE;
			}
		if (!exists)
			{
			printf("\n    ** Error in Dmesh3D::Delext (T_Surface *surf) ** \n");
			free(msw);
			free(swi);
			return -1;
         //Error (FATAL_ERROR, 1,"\nFace: %ld - %ld - %ld: can't find elem", n1,n2,n3);
			}
      else
			{
         el->rot_log(n1, n2, n3); // face: clockwise from exterior
         if (el->n2 == n2)        // el: interior
				{
				swi[ele]    = -1;
            swi[el->v4] =  1;
            }
			else
				{
				swi[ele]    =  1;
				swi[el->v4] = -1;
            }
			}
		ListElems.Clear();
      }  // Next face

	// Sweeps elements, until no change is made (fill-in algorithm)
   int changed = 1;
   long elev;
   elem *el;
   while (changed)
        {
        changed = 0;
        for (i = 0; i<nnel; i++)
            {
            el = p_elem(i);
            if (el->n2 >=0)  // Element exists
                {
		if (swi[i] == 1)  // Element is exterior
                    {
                    elev = el->v1;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v2;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v3;
                    if (swi[elev] == 0) swi[elev] = 1;
                    elev = el->v4;
                    if (swi[elev] == 0) swi[elev] = 1;
                    swi[i] = 2;
                    el->n2 = -1;
                    changed = 1;
                    }
                }
            }
        }

// Assign new element numbers to the remaining elements
    long new_nel = 0;
    for (i = 0; i<nnel; i++)
        {
        el = p_elem(i);
        if (el->n2 >= 0)        // El lugar esta ocupado x 1 elemento
            {
	    swi[i] = new_nel;
            new_nel++;
            }
        else
            swi[i] = -1;
        }

    new_nel = 0;
    long v1,v2,v3,v4;
    for (i = 0; i<nnel; i++)
        {
        el = p_elem(i);
        if (el->n2 >= 0)        // El lugar esta ocupado x 1 elemento
            {
            v1 = swi[el->v1];
            v2 = swi[el->v2];
            v3 = swi[el->v3];
            v4 = swi[el->v4];
            p_elem(new_nel)->set(el->n1, el->n2, el->n3, el->n4,
                v1, v2, v3, v4);
            new_nel++;
            }
        }
    nel = new_nel;
    nod -= 8;
    mFree(msw);
    mFree(swi);
    TraceOff(nome);
    return nel;
    }

long Dmesh3D::Count_Lost_Nodes(void)
  {
  long i, lost_nodes;
  long iel;
  elem *el;
  lost_nodes = 0;
  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      lost_nodes++;
    }
  return lost_nodes;
  }

long Dmesh3D::Reinsert_Nodes(void)
  {
  long i, lost_nodes;
  long iel;
  elem *el;

  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      add_notree(i);
    }

  lost_nodes = 0;
  for (i = 0; i < nod; i++)
    {
    iel = an_element[i];
    el = p_elem(iel);
    if (el->n1 != i && el->n2 != i && el->n3 != i && el->n4 != i)
      lost_nodes++;
    }
  return lost_nodes;
  }

void Dmesh3D::DelFreeNodes(void)
  {
  long i, iel, k, nodori;
  long huge * NuevaNum;
  NuevaNum = (long huge *) mMalloc(sizeof(long)*nod);
  if (!NuevaNum)
    Error(FATAL_ERROR, 1, "Insufficient memory");
  for (i = 0; i < nod; i++)
    NuevaNum[i]=0;

  for (i = iel = 0; iel<nel; i++)
    {
    elem *el = p_elem(i);
    if (el->n2 >= 0)
      {
      NuevaNum[el->n1] = 1;
      NuevaNum[el->n2] = 1;
      NuevaNum[el->n3] = 1;
      NuevaNum[el->n4] = 1;
      iel++;
      }
    }

  nodori = nod;
  for (k=i=0; i<nodori; i++)
    {
    if (NuevaNum[i])
      {
      x[k]=x[i];
      y[k]=y[i];
      z[k]=z[i];
      NuevaNum[i] = k++;
      }
    else
      {
      NuevaNum[i] = -1;
      nod--;
      }
    }

  if (nod != nodori)
    Error(COMMON_ERROR, 1, "%ld free nodes removed", nodori-nod);

  for (i = iel = 0; iel<nel; i++)
    {
    elem *el = p_elem(i);
    if (el->n2 >= 0)
      {
      el->n1 = NuevaNum[el->n1];
      el->n2 = NuevaNum[el->n2];
      el->n3 = NuevaNum[el->n3];
      el->n4 = NuevaNum[el->n4];
      iel++;
      }
    }
  }

// Check consistency of the volume. If there is more than two null (-1)
// neighbors over more than a half of a sampler, the volume is inconsistent.
int Dmesh3D::CheckVolumeConsistency ()
{
	int numSamplers  = 1000;
	int sampleJumper = this->nel / numSamplers;
	int nulos = 0, totalInvalidos = 0, total = 0; 	
	for (int i = 0; i < this->nel; i+=sampleJumper)
	{
		elem *el = p_elem(i);
		if(el->v1 == -1) nulos++;
		if(el->v2 == -1) nulos++;
		if(el->v3 == -1) nulos++;
		if(el->v4 == -1) nulos++;
		// printf("ID: %d [%d, %d, %d, %d]\n", i, el->v1,el->v2,el->v3,el->v4);
		
		// A superfície é inconsistente quando pelo menos a metade dos elementos
		// da amostra tem ao menos dois vizinhos nulos (igual a -1)
		if(nulos >= 2)
		{
			totalInvalidos++;
		}
		total++;
		nulos = 0;
	}
	// printf("Total: %d\nSampler: %d\n", totalInvalidos, total); 
	
	if(totalInvalidos > total/2)
		return 0; // Inconsistente
	return 1;
}
  