#include "ord_ot.h"

// Implementacion de clase tree_node:

// Implementacion de clase ord_ot:

//  Constructor: crea la raiz del arbol
ord_ot::ord_ot (REAL xminn, REAL xmaxx, REAL yminn, REAL ymaxx,
		REAL zminn, REAL zmaxx)
{
  nel = 1;
  n_vecs = 1;
  firstfree = 1;
  v_elem = (tree_node **) mMalloc (n_vecs*sizeof(tree_node *));
  if (!v_elem) Error (FATAL_ERROR, 1, "Ins. Memory (ord_ot)");
  v_elem[0] = (tree_node *) mMalloc (NODES_PER_VEC*sizeof(tree_node));
  if (!v_elem[0]) Error (FATAL_ERROR, 1, "Ins. Memory (ord_ot)");

//	Setea lista de elementos libres
  tree_node *v_aux = v_elem[0];
  for (int i = 1; i<NODES_PER_VEC; i++) {
      v_aux[i].h1 = i+1;
      v_aux[i].flag = 0;
  }

  v_aux[0].set (1, 0,0,0,0,0,0,0,0,
		xminn, yminn, zminn, xmaxx, ymaxx, zmaxx, -1);
}

//  Destructor de la clase ord_ot:
ord_ot::~ord_ot () {
  for (int i=0; i<n_vecs; i++)
    mFree (v_elem[i]);
  mFree(v_elem);
}

/*
// Metodo lock
void ord_ot::lock (void)
{
  h_elem = (HANDLE *) GlobalLock (h_h_ele);
  v_elem = (tree_node **) GlobalLock (h_v_ele);
  if (!h_elem || !v_elem)
      w_error (FATAL_ERROR, "Can't lock data (ord_ot::lock)");

  for (int i = 0; i<n_vecs; i++)
      {
      v_elem[i] = (tree_node *) GlobalLock (h_elem[i]);
      if (!v_elem[i]) w_error (FATAL_ERROR, "Can't lock (ot::lock)");
      }
}

// Metodo unlock
void ord_ot::unlock (void)
{
  for (int i = 0; i<n_vecs; i++)
      GlobalUnlock (h_elem[i]);
  GlobalUnlock (h_h_ele);
  GlobalUnlock (h_v_ele);
}
*/

// Metodo add de la clase ord_ot:
// Agrega un nodo en el arbol.
void ord_ot::add (long n_node,
	REAL huge *x, REAL huge *y, REAL huge *z, long ini_branch)
{
//  char buf[128];

//  int i;
//	Add node

  long terminal = which_branch
		  (x[n_node], y[n_node], z[n_node], ini_branch);
  tree_node *hoja = p_branch(terminal);
  while (hoja->node >= 0)	// Dividir
      {
      long nh1, nh2, nh3, nh4, nh5, nh6, nh7, nh8;
      int expandir = 1;
      nh1 = firstfree;
      if (nh1 < n_vecs*NODES_PER_VEC)
	{
	nh2 = p_branch(nh1)->h1;
	if (nh2 < n_vecs*NODES_PER_VEC)
	  {
	  nh3 = p_branch(nh2)->h1;
	  if (nh3 < n_vecs*NODES_PER_VEC)
	    {
	    nh4 = p_branch(nh3)->h1;
	    if (nh4 < n_vecs*NODES_PER_VEC)
	      {
	      nh5 = p_branch(nh4)->h1;
	      if (nh5 < n_vecs*NODES_PER_VEC)
		{
		nh6 = p_branch(nh5)->h1;
		if (nh6 < n_vecs*NODES_PER_VEC)
		  {
		  nh7 = p_branch(nh6)->h1;
		  if (nh7 < n_vecs*NODES_PER_VEC)
		    {
		    nh8 = p_branch(nh7)->h1;
		    if (nh8 < n_vecs*NODES_PER_VEC)
		      {
		      firstfree = p_branch(nh8)->h1;
		      expandir = 0;
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      if (expandir)
	  {
          tree_node **new_v;
          new_v = (tree_node **) mMalloc((n_vecs+1)*sizeof(tree_node *));
	  if (!new_v)
	    Error (FATAL_ERROR, 1, "Can't realloc elems (ord_ot::add)");
          int i;
          for (i=0; i<n_vecs; i++)
              new_v[i] = v_elem[i];
          mFree(v_elem);

	  v_elem = new_v;
	  v_elem[n_vecs] = (tree_node *) mMalloc (NODES_PER_VEC*sizeof(tree_node));
	  if (!v_elem[n_vecs])
	    Error (FATAL_ERROR, 1, "Ins. Memory (ord_ot::add)");

	  long j = n_vecs*NODES_PER_VEC;
	  for (i = 0; i<NODES_PER_VEC; i++, j++)
	    {
	    v_elem[n_vecs][i].h1 = j+1;
	    v_elem[n_vecs][i].flag = 0;
	    }
	  n_vecs++;

	  nh1 	    = firstfree;
	  nh2 	    = p_branch(nh1)->h1;
	  nh3 	    = p_branch(nh2)->h1;
	  nh4 	    = p_branch(nh3)->h1;
	  nh5 	    = p_branch(nh4)->h1;
	  nh6 	    = p_branch(nh5)->h1;
	  nh7 	    = p_branch(nh6)->h1;
	  nh8 	    = p_branch(nh7)->h1;
	  firstfree = p_branch(nh8)->h1;
	  }	// End if (expandir)

      long noprev = hoja->node;
      REAL xmed, ymed, zmed;
      REAL sizex = hoja->xmax - hoja->xm;
      xmed = (hoja->xm + hoja->xmax) * 0.5;
      ymed = (hoja->ym + hoja->ymax) * 0.5;
      zmed = (hoja->zm + hoja->zmax) * 0.5;

      p_branch(nh1)->set
	(1, 0,0,0,0,0,0,0,0, hoja->xm,   hoja->ym,   hoja->zm,
			     xmed,       ymed,       zmed,       -1);
      p_branch(nh2)->set
	(1, 0,0,0,0,0,0,0,0, xmed,       hoja->ym,   hoja->zm,
			     hoja->xmax, ymed,       zmed,       -1);
      p_branch(nh3)->set
	(1, 0,0,0,0,0,0,0,0, hoja->xm,   ymed,       hoja->zm,
			     xmed,       hoja->ymax, zmed,       -1);
      p_branch(nh4)->set
	(1, 0,0,0,0,0,0,0,0, xmed,       ymed,       hoja->zm,
			     hoja->xmax, hoja->ymax, zmed,       -1);
      p_branch(nh5)->set
	(1, 0,0,0,0,0,0,0,0, hoja->xm,   hoja->ym,   zmed,
			     xmed,       ymed,       hoja->zmax, -1);
      p_branch(nh6)->set
	(1, 0,0,0,0,0,0,0,0, xmed,       hoja->ym,   zmed,
			     hoja->xmax, ymed,       hoja->zmax, -1);
      p_branch(nh7)->set
	(1, 0,0,0,0,0,0,0,0, hoja->xm,   ymed,       zmed,
			     xmed,       hoja->ymax, hoja->zmax, -1);
      p_branch(nh8)->set
	(1, 0,0,0,0,0,0,0,0, xmed,       ymed,       zmed,
			     hoja->xmax, hoja->ymax, hoja->zmax, -1);

      hoja->set (2, nh1,nh2,nh3,nh4,nh5,nh6,nh7,nh8,
		 xmed,ymed,zmed, xmed,ymed,zmed,     -1);

      long iaux= which_branch (x[noprev],y[noprev],z[noprev], terminal);
      p_branch(iaux)->node = noprev;

      terminal = which_branch (x[n_node],y[n_node],z[n_node], terminal);
      if (iaux == terminal)
	{
	REAL dist2;
	dist2 = (x[n_node] - x[noprev]) * (x[n_node] - x[noprev]) +
		(y[n_node] - y[noprev]) * (y[n_node] - y[noprev]) +
		(z[n_node] - z[noprev]) * (z[n_node] - z[noprev]);
	if (dist2 < 1.0e-10 * sizex * sizex)
	    Error(FATAL_ERROR, 1, "Nodes too close: %ld, %ld",
		n_node, noprev);
	}
      hoja = p_branch(terminal);
    }

  hoja->node = n_node;

}

//  Metodo which_branch: busqueda en el arbol
long ord_ot::which_branch (REAL xx, REAL yy, REAL zz, long ini_branch)
{
  tree_node *branch = p_branch(ini_branch);

  while (branch->flag == 2) {
    if (zz < branch->zm) {
      if (yy < branch->ym) {
	if (xx < branch->xm)
	  ini_branch = branch->h1;
	else
	  ini_branch = branch->h2;
      }
      else {
	if (xx < branch->xm)
	  ini_branch = branch->h3;
	else
	  ini_branch = branch->h4;
      }
    }
    else {
      if (yy < branch->ym) {
	if (xx < branch->xm)
	  ini_branch = branch->h5;
	else
	  ini_branch = branch->h6;
      }
      else {
	if (xx < branch->xm)
	  ini_branch = branch->h7;
	else
	  ini_branch = branch->h8;
      }
    }
    branch = p_branch(ini_branch);
  } 	// End while
  return (ini_branch);
}

//  Metodo order: devuelve HANDLE p/un vector de permutacion
long *ord_ot::order (long ini_branch)
{
  long hoja1, hoja2, hoja3, hoja4, hoja5, hoja6, hoja7, hoja8;
  long   n1,  n2,  n3,  n4,  n5,  n6,  n7,  n8,  nt,
	 *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *pt;
  tree_node *rama_0;

  rama_0 = p_branch(ini_branch);
  if (rama_0->flag == 1)		// Es hoja
      {
      if (rama_0->node >= 0)			// Tiene nodo
	  {
	  pt = (long *) mMalloc (sizeof(long));
	  if (!pt)
	      Error (FATAL_ERROR, 1, "Ins. Memory (ord_ot::order)");
	  *pt = rama_0->node; rama_0->node = 1;
	  }
      else
	  {
	  rama_0->node = 0;
	  pt = NULL;
	  }
      }
  else				// Es rama
      {
      hoja1 = rama_0->h1; hoja2 = rama_0->h2;
      hoja3 = rama_0->h3; hoja4 = rama_0->h4;
      hoja5 = rama_0->h5; hoja6 = rama_0->h6;
      hoja7 = rama_0->h7; hoja8 = rama_0->h8;
      p1 = order (hoja1); p2 = order (hoja2);
      p3 = order (hoja3); p4 = order (hoja4);
      p5 = order (hoja5); p6 = order (hoja6);
      p7 = order (hoja7); p8 = order (hoja8);
      n1 = p_branch(hoja1)->node; n2 = p_branch(hoja2)->node;
      n3 = p_branch(hoja3)->node; n4 = p_branch(hoja4)->node;
      n5 = p_branch(hoja5)->node; n6 = p_branch(hoja6)->node;
      n7 = p_branch(hoja7)->node; n8 = p_branch(hoja8)->node;
      nt = n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
      pt = (long *) mMalloc (nt*sizeof(long));
      if (!pt)
	  Error (FATAL_ERROR, 1, "Ins. Memory (ord_ot::order)");

      long *ptt = pt;
      long *pp1 = p1;
      long *pp2 = p2;
      long *pp3 = p3;
      long *pp4 = p4;
      long *pp5 = p5;
      long *pp6 = p6;
      long *pp7 = p7;
      long *pp8 = p8;
      while (n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8)
	  {
	  if (n1) {*ptt++ = *pp1++; n1--;}
	  if (n8) {*ptt++ = *pp8++; n8--;}
	  if (n2) {*ptt++ = *pp2++; n2--;}
	  if (n7) {*ptt++ = *pp7++; n7--;}
	  if (n4) {*ptt++ = *pp4++; n4--;}
	  if (n5) {*ptt++ = *pp5++; n5--;}
	  if (n3) {*ptt++ = *pp3++; n3--;}
	  if (n6) {*ptt++ = *pp6++; n6--;}
	  }
      if (p1) mFree(p1);
      if (p2) mFree(p2);
      if (p3) mFree(p3);
      if (p4) mFree(p4);
      if (p5) mFree(p5);
      if (p6) mFree(p6);
      if (p7) mFree(p7);
      if (p8) mFree(p8);

      rama_0->node = nt;
      }
  return (pt);
}


//  Metodo nodo_cercano: devuelve el nodo contenido en la hoja
//	que contiene xp, yp, zp, si existe, de lo contrario devuelve
//	el nodo contenido en una hoja vecina
//	ademas devuelve en hoja la hoja que contiene a xp, yp, zp
long ord_ot::nodo_cercano (REAL xp, REAL yp, REAL zp, long &hoja)
{
long rama_0,v1,v2,v3,v4,v5,v6,v7;

tree_node *rama0, *pv1, *pv2, *pv3, *pv4, *pv5, *pv6, *pv7;

rama_0 = hoja;
hoja = 0;
while (1)
    {
    rama0 = p_branch(rama_0);
    while (rama0->flag == 2) {		// Es rama
	if (zp < rama0->zm) {
	  if (yp < rama0->ym) {
	    if (xp < rama0->xm) {
		v1 = rama0->h2; v2 = rama0->h3; v3 = rama0->h5;
		v4 = rama0->h4; v5 = rama0->h6; v6 = rama0->h7; v7 = rama0->h8;
		rama_0 = rama0->h1;
	    }
	    else {
		v1 = rama0->h1; v2 = rama0->h4; v3 = rama0->h6;
		v4 = rama0->h3; v5 = rama0->h5; v6 = rama0->h8; v7 = rama0->h7;
		rama_0 = rama0->h2;
	    }
	  }
	  else {
	    if (xp < rama0->xm) {
		v1 = rama0->h4; v2 = rama0->h1; v3 = rama0->h7;
		v4 = rama0->h2; v5 = rama0->h5; v6 = rama0->h8; v7 = rama0->h6;
		rama_0 = rama0->h3;
	    }
	    else {
		v1 = rama0->h3; v2 = rama0->h2; v3 = rama0->h8;
		v4 = rama0->h1; v5 = rama0->h7; v6 = rama0->h6; v7 = rama0->h5;
		rama_0 = rama0->h4;
	    }
	  }
	}
	else {
	  if (yp < rama0->ym) {
	    if (xp < rama0->xm) {
		v1 = rama0->h6; v2 = rama0->h7; v3 = rama0->h1;
		v4 = rama0->h8; v5 = rama0->h2; v6 = rama0->h3; v7 = rama0->h4;
		rama_0 = rama0->h5;
	    }
	    else {
		v1 = rama0->h5; v2 = rama0->h8; v3 = rama0->h2;
		v4 = rama0->h7; v5 = rama0->h1; v6 = rama0->h4; v7 = rama0->h3;
		rama_0 = rama0->h6;
	    }
	  }
	  else {
	    if (xp < rama0->xm) {
		v1 = rama0->h5; v2 = rama0->h8; v3 = rama0->h3;
		v4 = rama0->h6; v5 = rama0->h1; v6 = rama0->h4; v7 = rama0->h2;
		rama_0 = rama0->h7;
	    }
	    else {
		v1 = rama0->h6; v2 = rama0->h7; v3 = rama0->h4;
		v4 = rama0->h5; v5 = rama0->h2; v6 = rama0->h3; v7 = rama0->h1;
		rama_0 = rama0->h8;
	    }
	  }
	}
	rama0 = p_branch(rama_0);
    }

    if (!hoja) hoja = rama_0;
    if (rama0->node >= 0) return (rama0->node);
    pv1 = p_branch(v1);
    pv2 = p_branch(v2);
    pv3 = p_branch(v3);
    pv4 = p_branch(v4);
    pv5 = p_branch(v5);
    pv6 = p_branch(v6);
    pv7 = p_branch(v7);
    if (pv1->flag == 1)
      if (pv1->node >= 0)
	  return (pv1->node);
    if (pv2->flag == 1)
      if (pv2->node >= 0)
	  return (pv2->node);
    if (pv3->flag == 1)
      if (pv3->node >= 0)
	  return (pv3->node);
    if (pv4->flag == 1)
      if (pv4->node >= 0)
	  return (pv4->node);
    if (pv5->flag == 1)
      if (pv5->node >= 0)
	  return (pv5->node);
    if (pv6->flag == 1)
      if (pv6->node >= 0)
	  return (pv6->node);
    if (pv7->flag == 1)
      if (pv7->node >= 0)
	  return (pv7->node);

	 if (pv1->flag == 2) rama_0 = v1;
    else if (pv2->flag == 2) rama_0 = v2;
    else if (pv3->flag == 2) rama_0 = v3;
    else if (pv4->flag == 2) rama_0 = v4;
    else if (pv5->flag == 2) rama_0 = v5;
    else if (pv6->flag == 2) rama_0 = v6;
    else rama_0 = v7;
    }
}

/*
// Metodo plot de la clase ord_qt (no programado)
void mesh::plot (void)
{
  long i, iel;
  h_elem = (HANDLE *) GlobalLock (h_h_ele);
  v_elem = (elem **) GlobalLock (h_v_ele);
  if (!h_elem || !v_elem)
      w_error (FATAL_ERROR, "Can't lock data (plot)");
  for (i = 0; i<n_vecs; i++) {
    v_elem[i] = (elem *) GlobalLock (h_elem[i]);
    if (!v_elem[i]) w_error (FATAL_ERROR, "Can't lock data (plot)");
  }
  for (i = iel = 0; iel<nel && i<n_vecs*ELE_PER_VEC; i++) {
      elem *el = p_elem(i);
      int l1, l2;
      l1 = l2 = 0;
      if (el->n2 >= 0) {
	  if (el->v1 < i) {
	      MoveTo (hdc, ix[el->n1], iy[el->n1]);
	      LineTo (hdc, ix[el->n2], iy[el->n2]);
	      l1 = 1;
	  }
	  if (el->v2 < i) {
	      if (!l1) MoveTo (hdc, ix[el->n2], iy[el->n2]);
	      LineTo (hdc, ix[el->n3], iy[el->n3]);
	      l2 = 1;
	  }
	  if (el->v3 < i) {
	      if (!l2) MoveTo (hdc, ix[el->n3], iy[el->n3]);
	      LineTo (hdc, ix[el->n1], iy[el->n1]);
	  }
	  iel++;
      }
  }

  for (i = 0; i<n_vecs; i++) {
    GlobalUnlock (h_elem[i]);
  }
  GlobalUnlock (h_h_ele);
  GlobalUnlock (h_v_ele);

}
*/
