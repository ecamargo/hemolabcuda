#ifndef ORD_OT_H
#define ORD_OT_H
#include "acdp.h"
#include "vtkSystemIncludes.h"

//#include "wdelau3D.h"

class ord_ot;

#ifndef REAL
#define REAL double
#endif

//-------------------------------------------------------------------------
class VTK_EXPORT tree_node                              // Node of the tree
{
  private:
    int flag;			// 1: intermediate branch
				// 0: terminal branch
    long  h1, h2, h3, h4,
	  h5, h6, h7, h8;	// sub-branches
    REAL xm, ym, zm;    	// minima, media
    REAL xmax, ymax, zmax;      // maxima
    long node;			// node contained

  public:
//  Metodo set: modifica atributos de un nodo del arbol
    void set (int flagg, long hh1, long hh2, long hh3, long hh4,
			 long hh5, long hh6, long hh7, long hh8,
	      REAL xmie, REAL ymie, REAL zmie,
	      REAL xmaxx, REAL ymaxx,  REAL zmaxx, long nodee)
    {
      flag = flagg;
      h1 = hh1;		h2 = hh2;	h3 = hh3;	h4 = hh4;
      h5 = hh5;		h6 = hh6;	h7 = hh7;	h8 = hh8;
      xm = xmie;      	ym = ymie;	zm = zmie;
      xmax = xmaxx;	ymax = ymaxx;	zmax = zmaxx;
      node = nodee;
    }

friend class ord_ot;
}; 					// class tree_node

//-------------------------------------------------------------------------
#define NODES_PER_VEC 1024

class VTK_EXPORT ord_ot                      	// Red Delaunay
{
  private:
    long nel;		// "Elementos" en el arbol
    int n_vecs;	// Numero de vectores de elementos
    long firstfree;	// 1er elemento de la lista de elementos libres
    tree_node **v_elem;	// Vector de pointers de los vectores de elementos

  public:
//  Constructor: crea la raiz del arbol
    ord_ot (REAL xminn, REAL xmaxx, REAL yminn, REAL ymaxx,
	    REAL zminn, REAL zmaxx);

//  Destructor: Libera la memoria ocupada por los elementos
    ~ord_ot ();

//  Metodo lock:
//    void lock (void);

//  Metodo unlock:
//    void unlock (void);

//  Metodo p_branch
//   (devuelve el puntero para una rama, dado su indice)
    tree_node *p_branch (long index) {
      int i_vec, offset;
      i_vec  = (int)(index / NODES_PER_VEC);
      offset = (int)(index % NODES_PER_VEC);
      return (v_elem[i_vec]+offset);
    }

//  Metodo add: agrega un nodo en el arbol
    void add (long node, REAL huge *x, REAL huge *y, REAL huge *z,
	long ini_branch=0L);

//  Metodo which_branch: busqueda eficiente
    long which_branch (REAL xx, REAL yy, REAL zz, long ini_branch=0L);

//  Metodo order: ordenamiento "optimo"
    long * order (long ini_branch);

//  Metodo nodo_cercano: devuelve un nodo cercano a xp,yp
    long nodo_cercano (REAL xp, REAL yp, REAL zp, long &hoja);

//  Metodo plot (dibuja el arbol, funciona bajo WINDOWS)
//    void plot (void);

};					// end class ord_ot

#endif
