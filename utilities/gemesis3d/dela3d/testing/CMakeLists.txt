ADD_EXECUTABLE( testaDela testDela3d.cxx)

INCLUDE_DIRECTORIES(
	${HeMoLab_SOURCE_DIR}/utilities/3dfilters/dela3d
	${HeMoLab_BINARY_DIR}/utilities/3dfilters/dela3d
	${HeMoLab_SOURCE_DIR}/utilities/3dfilters/trisurf
	${HeMoLab_BINARY_DIR}/utilities/3dfilters/trisurf	
)

TARGET_LINK_LIBRARIES(testaDela
	vtkCommon
	vtkRendering 
	vtkGraphics	
	vtkGraphicsCS
	baseoop
	acdpoop	
	vtkTrisurf	
	vtkDela3d	
)

