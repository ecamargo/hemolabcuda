char *cfgfile = "d3d.cfg";
char *msgfile = "d3d.msg";
char *prgname = "DELA3D";

#include "wx/wx.h"
#include "malhapar.h"
#include "dmesh3d.h"
#include "clus3d.h"
#include "tsurface.h"
#include "acqueue.h"
//#include "openf.h"

#ifndef REAL
#define REAL double
#endif

int gargc;
char **gargv;


// Funciones p/reordenar
void o_order (long , REAL  *, REAL  *, REAL  *, long  *vper);
void o_orderl(long , REAL  *, REAL  *, REAL  *, long  *vper, acQueue *lper);
void r_order (long , REAL  *, REAL  *, REAL  *, long  *vper);


void DoAll(char *);

// ----------------------------------------------------------------------------
// private classes
// ----------------------------------------------------------------------------

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
    virtual bool OnInit();
};

// Define a new frame type: this is going to be our main frame
class MyFrame : public wxFrame
{
public:
    // ctor(s)
    MyFrame(const wxString& title);

    // event handlers (these functions should _not_ be virtual)
    void OnQuit(wxCommandEvent& event);
    void OnFileOpen(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);

private:
    // any class wishing to process wxWidgets events must use this macro
    DECLARE_EVENT_TABLE()
};

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

#define ID_FILE_OPEN 5000

// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(wxID_EXIT,  MyFrame::OnQuit)
    EVT_MENU(ID_FILE_OPEN, MyFrame::OnFileOpen)
    EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
END_EVENT_TABLE()

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp)

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
    // create the main application window
    MyFrame *frame = new MyFrame(_T("Dela 3D App"));

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
    frame->Show(true);

    // pega argumentos da linha de comando
    gargc = argc;
    gargv = argv;

    if (argc == 2)	// entrou nome de arquivo
    {			// faz tudo e cai fora
	char filename[255];
	strcpy(filename, argv[1]);
	DoAll(filename);
	exit(0);
    }
    
    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
MyFrame::MyFrame(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title)
{
    // set the frame icon
//    SetIcon(wxICON(sample));

#if wxUSE_MENUS
    // create a menu bar
    wxMenu *fileMenu = new wxMenu;

    // the "About" item should be in the help menu
    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(wxID_ABOUT, _T("&About...\tF1"), _T("Show about dialog"));

    fileMenu->Append(ID_FILE_OPEN, _T("Open File"));
    fileMenu->Append(wxID_EXIT, _T("E&xit\tAlt-X"), _T("Quit this program"));

    // now append the freshly created menu to the menu bar...
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, _T("&File"));
    menuBar->Append(helpMenu, _T("&Help"));

    // ... and attach this menu bar to the frame
    SetMenuBar(menuBar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar just for fun (by default with 1 pane only)
    CreateStatusBar(2);
    SetStatusText(_T("Dela3D"));
#endif // wxUSE_STATUSBAR
}


// event handlers

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    // true is to force the frame to close
    Close(true);
}


void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxString msg;
    msg.Printf( _T("Dela 3D"));
    wxMessageBox(msg, _T("About Dela 3D"), wxOK | wxICON_INFORMATION, this);
}


void MyFrame::OnFileOpen(wxCommandEvent& WXUNUSED(event))
{

    wxFileDialog dialog
                 (
                    this,
                    _T("Open file"),
                    wxEmptyString,
                    wxEmptyString,
#ifdef __WXMOTIF__
                    _T("cfg files (cfg.*)|*.*")
#else
                    _T("cfg files (cfg.*)|*.*")
#endif
                 );

    dialog.SetDirectory(wxGetHomeDir());
    dialog.CentreOnParent();
    if (dialog.ShowModal() == wxID_OK)
    {
	char filename[255];

	strcpy(filename, dialog.GetFilename().c_str());
	DoAll(filename);
    }

}



void DoAll(char *filecfg)
    {
    long nod;
    REAL  *x,  *y,  *z;
    long  *vper;
    acQueue *lper;
    Dmesh3D *pt_my_mesh;
    ParamMesh *pm;
    T_Surface *surf;
    char filename[256];
    char buf[256];
    FILE *fcfg, *fp;

    StartACDP(0, "Dela3d.txt", "trace.txt");
    TraceOn("main");

//    printf("\nConfiguration file_name:");
//    scanf ("%s",filecfg);
//    char *filedef="dela3d.cfg";
//    if (!acOpenDialog(0,filecfg,filedef)) exit(0);
    fcfg = fopen(filecfg, "r");
    if (!fcfg) Error (FATAL_ERROR, 1, "Can't open configuration file");

/*
 *	Comienza parche para generacion de mallas de parametros
 */
    char *kpp = "PARAMETERS_POINTS";
    if (FindKeyWord(kpp, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nPoints with parameters from file: %s", filename);
        printf ("\n   Reading points...");
        TimeOn();
        fp = fopen(filename, "r");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open points file");
        fscanf(fp, "%ld", &nod);
        nod += 8;
        x = (REAL  *) mMalloc(nod * sizeof(REAL));
        y = (REAL  *) mMalloc(nod * sizeof(REAL));
        z = (REAL  *) mMalloc(nod * sizeof(REAL));
        REAL  *par = (REAL  *) mMalloc(nod * sizeof(REAL));
        REAL paravg = 0;
        vper = (long  *) mMalloc(nod * sizeof(long));
        if (!x || !y || !z || !vper)
            Error (FATAL_ERROR, 7, "Can't alloc memory");
        acLimit3 vv;
        REAL xx, yy, zz;
	long j;
        for (j = 0; j < nod-8; j++)
            {
            fscanf(fp, " %lf %lf %lf %lf", &xx, &yy, &zz, par+j);
            x[j] = xx; y[j] = yy; z[j] = zz;
            if (j == 0)
                {
                vv.xmin = vv.xmax = xx;
                vv.ymin = vv.ymax = yy;
                vv.zmin = vv.zmax = zz;
                }
            else
                {
                if      (xx < vv.xmin) vv.xmin = xx;
                else if (xx > vv.xmax) vv.xmax = xx;
                if      (yy < vv.ymin) vv.ymin = yy;
                else if (yy > vv.ymax) vv.ymax = yy;
                if      (zz < vv.zmin) vv.zmin = zz;
                else if (zz > vv.zmax) vv.zmax = zz;
                }
            paravg += par[j];
            }
        paravg /= nod-8;
        par[j++]=paravg; par[j++]=paravg; par[j++]=paravg; par[j++]=paravg;
        par[j++]=paravg; par[j++]=paravg; par[j++]=paravg; par[j++]=paravg;

    //	Genera nodos extra
        acPoint3 center, size;
        REAL sizecube;
        center.x = (vv.xmin + vv.xmax) * 0.5;
        center.y = (vv.ymin + vv.ymax) * 0.5;
        center.z = (vv.zmin + vv.zmax) * 0.5;
        size.x = (vv.xmax - vv.xmin) * 1.05;
        size.y = (vv.ymax - vv.ymin) * 1.05;
        size.z = (vv.zmax - vv.zmin) * 1.05;
        sizecube = size.x > size.y ? size.x : size.y;
        sizecube = size.z > sizecube ? size.z : sizecube;
        sizecube *= 1.5;
        x[nod-8] = center.x-sizecube;    x[nod-7] = center.x+sizecube;
        y[nod-8] = center.y-sizecube;    y[nod-7] = center.y-sizecube;
        z[nod-8] = center.z-sizecube;    z[nod-7] = center.z-sizecube;
        x[nod-6] = center.x-sizecube;    x[nod-5] = center.x+sizecube;
        y[nod-6] = center.y+sizecube;    y[nod-5] = center.y+sizecube;
        z[nod-6] = center.z-sizecube;    z[nod-5] = center.z-sizecube;
        x[nod-4] = center.x-sizecube;    x[nod-3] = center.x+sizecube;
        y[nod-4] = center.y-sizecube;    y[nod-3] = center.y-sizecube;
        z[nod-4] = center.z+sizecube;    z[nod-3] = center.z+sizecube;
        x[nod-2] = center.x-sizecube;    x[nod-1] = center.x+sizecube;
        y[nod-2] = center.y+sizecube;    y[nod-1] = center.y+sizecube;
        z[nod-2] = center.z+sizecube;    z[nod-1] = center.z+sizecube;

        printf ("\n   ...ready.");
        TimeOff("Parameters points reading", 3);
        fclose(fp);

        printf ("\n\nGenerating triangulation...");
    //	Renumeracion de nodos
        int ren_method;
        if (!FindKeyWord("RENUMBERING_METHOD", fcfg))
            ren_method = 1;
        else
            fscanf(fcfg,"%d",&ren_method);

        TimeOn();
        if (ren_method == 1) // Optimal
            o_order (nod, x, y, z, vper);
        else if (ren_method == 2) // Random
            r_order (nod, x, y, z, vper);
        else
            for (long i=0; i<nod; i++) vper[i] = i;

        TimeOff("Nodes reordering", 3);

    //	Generar malla inicial
        printf ("\n   Creating Delaunay mesh...");
        pt_my_mesh = new Dmesh3D (nod, x, y, z);
        if (!pt_my_mesh)
            Error (FATAL_ERROR, 9, "pt_my_mesh");

    //	Agregar todos los nodos
        printf ("\n   Adding nodes...");
        TimeOn();
        for (long i = 0; i<nod-8; i++)
            pt_my_mesh->add (i);
        TimeOff("Bowier-Watson algorithm", 3);

        TimeOn();
        pt_my_mesh->renumber(vper);
        mFree (vper);
        TimeOff("Nodes reordering", 3);

        printf ("\n   Testing lost nodes...");
        TimeOn();
        long lost_nodes = pt_my_mesh->Count_Lost_Nodes();
        int maxiters = 5;
        while (lost_nodes && maxiters)
            {
            printf ("\n   Reinserting %ld nodes.", lost_nodes);
            lost_nodes = pt_my_mesh->Reinsert_Nodes();
            maxiters--;
            }
        TimeOff("Lost Nodes reinserting", 3);
        if (lost_nodes)
            Error(WARNING, 10, "%ld lost nodes remain");

        if (FindKeyWord("OUTPUT_PARAMETERS_MESH", fcfg))
            fscanf(fcfg,"%s",filename);
        else
            strcpy(filename, "paramesh.spi");

        TimeOn();
        pt_my_mesh->print(filename, par);
        TimeOff("Mesh writing", 3);
        printf ("\n...ready parameters mesh.");
        printf ("\n\nSaved mesh in %s.\n\n", filename);
        fclose(fcfg);

        mFree (x);
        mFree (y);
        mFree (z);
        mFree (par);
        delete pt_my_mesh;
        TraceOff("main");
        ExitProg();
        }

/*
 *	Fin parche para generacion de mallas de parametros
 */

    int mesh_read;
    char *kdm = "DELAUNAY_MESH";
    if (FindKeyWord(kdm, fcfg))
        {
        fscanf(fcfg,"%s",filename);
        printf("\n\nDelaunay mesh from file: %s", filename);
        printf ("\n   Reading Dmesh3D...");
        TimeOn();
        fp = fopen(filename, "r");
        if (!fp) Error(FATAL_ERROR, 2, "Can't open Delaunay mesh file");
        pt_my_mesh = new Dmesh3D(fp);
        fclose(fp);
        TimeOff("Delaunay mesh reading", 3);
        printf ("\n...ready");
        mesh_read = 1;
        }
    else
        {
        pt_my_mesh = NULL;
        mesh_read = 0;
        }

    if (!mesh_read)
        {
        char *kpm = "PARAMETERS_MESH";
        if (!FindKeyWord(kpm, fcfg))
            {
            GetError(27, buf);
            Error(FATAL_ERROR, 2, buf, kpm);
            }
        fscanf(fcfg,"%s",filename);
        printf("\n\nParameters mesh from file: %s", filename);
        fp = fopen(filename, "r");
        if (!fp) Error (FATAL_ERROR, 3, "Can't open parameters mesh file");
        printf ("\n   Creating ParamMesh...");
        pm = new ParamMesh;
        printf ("\n   Reading ParamMesh...");
        TimeOn();
        pm->Read(fp);
        fclose(fp);
        TimeOff("ParamMesh reading", 3);
        printf ("\n   Building Octree...");
        TimeOn();
        pm->BuildOctree();
        TimeOff("ParamMesh Octree building", 3);
        printf ("\n   ...ready ParamMesh.");
        }

    char *ksur = "SURFACE_MESH";
    if (!FindKeyWord(ksur, fcfg))
        {
        GetError(27, buf);
        Error(FATAL_ERROR, 4, buf, ksur);
        }
    fscanf(fcfg,"%s",filename);
    
    printf("\n\nO NOME DO ARQUIVO EH::::::%s ", filename);
	    
    printf("\n\nSurface mesh from file: %s\n", filename);
    fp = fopen(filename, "r");
    if (!fp) Error (FATAL_ERROR, 5, "Can't open surface mesh file");
    printf ("\n   Creating surface...");
    surf = new T_Surface;
    printf ("\n   Reading surface...");
    TimeOn();
    surf->Read(fp);
    TimeOff("Surface reading", 3);
    fclose(fp);
    printf ("\n   %ld nodes, %ld triangles.",
    surf->GetNumNodes(), surf->GetNumElems());

    if (!pt_my_mesh)
        {
//    printf ("\n   Building Trees...");
//    TimeOn();
//    surf->BuildTrees();
//    TimeOff("Surface Trees building", 3);
        printf ("\n   ...ready Surface.");

        char *kint = "INTERIOR_POINTS_FILE";
        int points_generated;
        if (FindKeyWord(kint, fcfg))
            {
            points_generated = 0;
            fscanf(fcfg,"%s",filename);

            printf("\n\nLENDO SUPERFICIE DO ARQUIVO::::::%s", filename);
	    
            printf("\n\nInterior nodes from file: %s", filename);
            fp = fopen(filename, "r");
            if (!fp) Error (FATAL_ERROR, 7, "Can't open nodes file");
            printf ("\n\nReading interior nodes...");
            TimeOn();
            if (!FindKeyWord("COORDINATES", fp))
                {
                GetError(27, buf);
                Error(FATAL_ERROR, 8, buf, "COORDINATES");
                }
            long i, idum, numnodes;
            acPoint3 pp;
            fscanf(fp, "%ld", &numnodes);
            for (i = 0; i<numnodes; i++)
                {
                fscanf(fp, "%ld %lf %lf %lf",&idum, &pp.x,&pp.y,&pp.z);
                surf->SetCoord(i, pp);
                }
            TimeOff("Interior nodes reading", 3);
            fclose(fp);
            }
        else
            {
            points_generated = 1;
            if (FindKeyWord("SIZE_ELEMENTS", fcfg))
                {
                REAL newfactor;
                fscanf(fcfg, "%lf", &newfactor);
                surf->SetFactorSize(newfactor);
                }
            int flagextnod=0;
            if (FindKeyWord("GENERATE_EXTERIOR_NODES", fcfg))
                fscanf(fcfg, "%d", &flagextnod);
            surf->SetFlagextnod(flagextnod);
            if (FindKeyWord("DISTANCE_FROM_INTERIOR_SURFACES", fcfg))
                {
                REAL newdfis;
                fscanf(fcfg, "%lf", &newdfis);
                surf->SetDfis(newdfis);
                }
            else
                surf->SetDfis(0.3);

            printf ("\n\nGenerating interior nodes...");
            long pNodes = surf->GetNumNodes();
            TimeOn();
            lper = surf->GenPoints(pm);
        //	delete lper;  // Renumeracion ignorada!
            TimeOff("Interior nodes generation", 3);
            printf ("\n   %ld Nodes Generated",
                surf->GetNumNodes() - pNodes);
            }
        printf ("\n   ...ready interior nodes.");
        delete pm;

        nod = surf->GetNumNodes() + 8;
        x = (REAL  *) mMalloc(nod * sizeof(REAL));
        y = (REAL  *) mMalloc(nod * sizeof(REAL));
        z = (REAL  *) mMalloc(nod * sizeof(REAL));
        vper = (long  *) mMalloc(nod * sizeof(long));
        if (!x || !y || !z || !vper)
            Error (FATAL_ERROR, 7, "Can't alloc memory");
        surf->CopyCoordVecs(x, y, z);
        surf->FreeCoords();

        if (FindKeyWord("OUTPUT_POINTS", fcfg))
            fscanf(fcfg,"%s",filename);
        else
            strcpy(filename, "points.dat");

        fp = fopen(filename, "w");
        if (!fp) Error (FATAL_ERROR, 8, "Can't open file for points");
        fprintf (fp, "*COORDINATES\n %ld\n", surf->GetNumNodes());
        for (long j = 0; j < surf->GetNumNodes(); j++)
            fprintf (fp, " %ld %lf %lf %lf\n", j+1, x[j], y[j], z[j]);
        fprintf (fp, "*END\n");
        fclose (fp);
        printf ("\n\nSaved points in %s.", filename);

        printf ("\n\nGenerating triangulation...");
        acLimit3 vv = surf->GetVolume();
    // Genera nodos extra
        acPoint3 center, size;
        REAL sizecube;
        center.x = (vv.xmin + vv.xmax) * 0.5;
        center.y = (vv.ymin + vv.ymax) * 0.5;
        center.z = (vv.zmin + vv.zmax) * 0.5;
        size.x = (vv.xmax - vv.xmin) * 1.05;
        size.y = (vv.ymax - vv.ymin) * 1.05;
        size.z = (vv.zmax - vv.zmin) * 1.05;
        sizecube = size.x > size.y ? size.x : size.y;
        sizecube = size.z > sizecube ? size.z : sizecube;
        sizecube *= 1.5;
        x[nod-8] = center.x-sizecube;    x[nod-7] = center.x+sizecube;
        y[nod-8] = center.y-sizecube;    y[nod-7] = center.y-sizecube;
        z[nod-8] = center.z-sizecube;    z[nod-7] = center.z-sizecube;
        x[nod-6] = center.x-sizecube;    x[nod-5] = center.x+sizecube;
        y[nod-6] = center.y+sizecube;    y[nod-5] = center.y+sizecube;
        z[nod-6] = center.z-sizecube;    z[nod-5] = center.z-sizecube;
        x[nod-4] = center.x-sizecube;    x[nod-3] = center.x+sizecube;
        y[nod-4] = center.y-sizecube;    y[nod-3] = center.y-sizecube;
        z[nod-4] = center.z+sizecube;    z[nod-3] = center.z+sizecube;
        x[nod-2] = center.x-sizecube;    x[nod-1] = center.x+sizecube;
        y[nod-2] = center.y+sizecube;    y[nod-1] = center.y+sizecube;
        z[nod-2] = center.z+sizecube;    z[nod-1] = center.z+sizecube;

        // Renumeracion de nodos
        int ren_method;
        if (!FindKeyWord("RENUMBERING_METHOD", fcfg))
            ren_method = 1;
        else
            fscanf(fcfg,"%d",&ren_method);

        TimeOn();
        if (ren_method == 1) // Optimal
            {
            if (points_generated)
                o_orderl (nod, x, y, z, vper, lper);
            else
                o_order (nod, x, y, z, vper);
            }
        else if (ren_method == 2) // Random
            r_order (nod, x, y, z, vper);
        else
            for (long i=0; i<nod; i++) vper[i] = i;

        if (points_generated) delete lper;
        TimeOff("Nodes reordering", 3);

        // Generar malla inicial
        printf ("\n   Creating Delaunay mesh...");
        pt_my_mesh = new Dmesh3D (nod, x, y, z);
        if (!pt_my_mesh)
            Error (FATAL_ERROR, 9, "pt_my_mesh");

        // Agregar todos los nodos
        printf ("\n   Adding nodes...");
        TimeOn();
        for (long i = 0; i<nod-8; i++)
            pt_my_mesh->add (i);
        TimeOff("Bowier-Watson algorithm", 3);

        long dummy, min_cs, max_cs, min_ni, max_ni;
        double avg_cs;
        pt_my_mesh->GetStats(dummy, min_cs, max_cs, avg_cs, min_ni, max_ni);
        printf ("\n   Cluster size:"
             "\n      Min: %ld"
             "\n      Max: %ld"
             "\n      Avg: %lf"
             "\n   nel increment:"
             "\n      Min: %ld"
             "\n      Max: %ld",
         min_cs, max_cs, avg_cs, min_ni, max_ni);

        TimeOn();
        pt_my_mesh->renumber(vper);
        mFree (vper);
        TimeOff("Nodes reordering", 3);

        printf ("\n   Testing lost nodes...");
        TimeOn();
        long lost_nodes = pt_my_mesh->Count_Lost_Nodes();
        int maxiters = 5;
        while (lost_nodes && maxiters)
            {
            printf ("\n   Reinserting %ld nodes.", lost_nodes);
            lost_nodes = pt_my_mesh->Reinsert_Nodes();
            maxiters--;
            }
        TimeOff("Lost Nodes reinserting", 3);
        if (lost_nodes)
            Error(WARNING, 10, "%ld lost nodes remain");

        printf ("\n   Testing mesh...");
        // Test de la malla
        TimeOn();
        pt_my_mesh->is_ok(1, 1, 1);
        TimeOff("Mesh testing", 3);

        if (FindKeyWord("OUTPUT_CUBE_MESH", fcfg))
            fscanf(fcfg,"%s",filename);
        else
            strcpy(filename, "cubemesh.vwm");

        TimeOn();
        pt_my_mesh->print(filename);
        TimeOff("Mesh writing", 3);
        printf ("\n...ready Delaunay mesh.");
        printf ("\n\nSaved mesh in %s.", filename);
        }

    fprintf (stdout, "\n\nRecovering boundary triangles...");
    long FnotR, CwIter, C_E_F, C_E_E;
    printf ("\n   %ld edges, %ld faces to recover.",
	surf->GetNumElems()*3/2, surf->GetNumElems());
    printf ("\n   First sweep...");
    TimeOn();
    FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
    TimeOff("Boundary recovering (first sweep)", 3);
    printf ("\n   Cases with more than 1 iteration: %ld.",
	CwIter);
    printf ("\n   Cases Edge-Face: %ld, Cases Edge-Edge: %ld.",
	C_E_F, C_E_E);
    if (FnotR > 0)
        {
        printf ("   Can't recover %ld faces.", FnotR);
        printf ("\n   Second sweep...");
        TimeOn();
        FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
        TimeOff("Boundary recovering (second sweep)", 3);
        printf ("\n   Cases with more than 1 iteration: %ld.",
            CwIter);
        printf (
            "\n   Cases Edge-Face: %ld, Cases Edge-Edge: %ld.",
            C_E_F, C_E_E);
        }
    if (FnotR > 0)
        {
        printf ("\nCan't recover %ld faces\n", FnotR);
        printf ("\n   Testing mesh...");
        TimeOn();
        pt_my_mesh->is_ok(1,1,1);
        TimeOff("Mesh testing", 3);

        if (FindKeyWord("OUTPUT_CUBE_MESH", fcfg))
            fscanf(fcfg,"%s",filename);
        else
            strcpy(filename, "cubemesh.vwm");
        TimeOn();
        pt_my_mesh->print(filename);
        TimeOff("Mesh writing", 3);
        printf ("\n...ready boundary recovering.");
        printf ("\n\nSaved mesh in %s.", filename);
        Error(FATAL_ERROR, 10, "%ld faces not recovered", FnotR);
        }

    printf ("\n   Testing mesh...");
    TimeOn();
    pt_my_mesh->is_ok(1,1,1);
    TimeOff("Mesh testing", 3);
    printf ("\n...ready boundary recovering.");

    printf ("\n\nDeleting external tetrahedra...");
    TimeOn();
    long rtet = pt_my_mesh->Delext(surf);
    pt_my_mesh->DelFreeNodes();
    TimeOff("Exterior elements deleting", 3);
    printf ("\n...Ready.");
    printf ("\n   %ld remaining tetrahedra.", rtet);
    printf ("\n   Testing mesh...");
    TimeOn();
    pt_my_mesh->is_ok(1,0,1);
    TimeOff("Mesh testing", 3);

    if (FindKeyWord("OUTPUT_MESH", fcfg))
	fscanf(fcfg,"%s",filename);
    else
	strcpy(filename, "d3dmesh.vwm");

    TimeOn();
    pt_my_mesh->print(filename);
    TimeOff("Mesh writing", 3);
    printf ("\n...ready deleting external tetrahedra.");
    printf ("\n\nSaved mesh in %s.\n\n", filename);

    fclose(fcfg);

    delete surf;
    mFree (x);
    mFree (y);
    mFree (z);
    delete pt_my_mesh;
    TraceOff("main");
    ExitProg();
    }

void o_orderl (long nod, REAL  *x, REAL  *y, REAL  *z,
	long  *vper, acQueue *lper)
    {
    long i, node;

    nod -= 8;

    i = 0;
    while (lper->Pop(&node) == 1)
	{
	vper[i++] = node;
	}


    if (i != nod)
	Error(WARNING, 1, "o_orderl: i: %ld, nod: %ld", i, nod);
    int  *msw;
    msw = (int  *) mMalloc(nod*sizeof(int));
    if (!msw) Error (FATAL_ERROR, 1, "Ins. memory (o_order)");
    for (i = 0; i<nod; i++) msw[i] = 0;
    for (i = 0; i<nod; i++)
	{
	if (vper[i] >= nod || vper[i]<0)
	    Error(FATAL_ERROR, 3, "Nodo %ld en la lista", vper[i]);
	if (msw[vper[i]] == 1)
	    Error(WARNING, 2, "Nodo %ld duplicado", vper[i]);
	else
	    msw[vper[i]] = 1;
	}
    mFree(msw);

    REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
    if (!ptaux) Error (FATAL_ERROR, 1, "Ins. memory (o_order)");

    for (i=0; i < nod; i++)
	ptaux[i] = x[vper[i]];
    for (i=0; i < nod; i++)
	x[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = y[vper[i]];
    for (i=0; i < nod; i++)
	y[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = z[vper[i]];
    for (i=0; i < nod; i++)
	z[i] = ptaux[i];

    for (i=0; i < 8; i++)
	vper[nod+i] = nod+i;

    mFree (ptaux);
    }

void r_order
(long nod, REAL  *x, REAL  *y, REAL  *z, long  *vper)
    {
    long pos1, pos2, aux;
    nod -= 8;
    long i;
    for (i = 0; i < nod; i++)
	vper[i] = i;

    for (i = 0; i < nod / 2; i++)
	{
	pos1 = rand() % nod;
	pos2 = rand() % nod;
	aux        = vper[pos1];
	vper[pos1] = vper[pos2];
	vper[pos2] = aux;
	}

    REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
    if (!ptaux) Error (FATAL_ERROR, 1, "Ins. memory (r_order)");

    for (i=0; i < nod; i++)
	ptaux[i] = x[vper[i]];
    for (i=0; i < nod; i++)
	x[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = y[vper[i]];
    for (i=0; i < nod; i++)
	y[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = z[vper[i]];
    for (i=0; i < nod; i++)
	z[i] = ptaux[i];

    for (i=0; i < 8; i++)
	vper[nod+i] = nod+i;

    mFree (ptaux);
    }

void o_order (long nod, REAL  *x, REAL  *y, REAL  *z, long  *vper)
    {
    REAL xmmi, xmma, ymmi, ymma, zmmi, zmma, xd, yd, zd;
    xmmi = x[nod-8]; xmma = x[nod-1];
    ymmi = y[nod-8]; ymma = y[nod-1];
    zmmi = z[nod-8]; zmma = z[nod-1];
    xd = xmma - xmmi; yd = ymma - ymmi, zd = zmma - zmmi;
    if (xd <= 0 || yd <= 0 || zd <= 0)
	Error (FATAL_ERROR, 2,
	"Bad dimensions of mesh (o_order) %f x %f, %f y %f, %f z %f",
	xmmi, xmma, ymmi, ymma, zmmi, zmma);

    xd *= 0.3; yd *= 0.3; zd *= 0.3;

    xmmi += xd; xmma -= xd;
    ymmi += yd; ymma -= yd;
    zmmi += zd; zmma -= zd;
    ord_ot *tree = new ord_ot(xmmi,xmma,ymmi,ymma,zmmi,zmma);
    if (!tree) Error (FATAL_ERROR, 1, "Ins. memory (o_order)");

    nod -= 8;
    long i;
    for (i = 0; i < nod; i++)
	tree->add (i, x, y, z, 0L);

//    if (debugging)
//      Error (WARNING, 0, "Arbol completo");

    long  *ptper = tree->order(0L);

//    if (debugging)
//      Error (WARNING, 0, "Vector permutacion listo");

    delete tree;

    REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
    if (!ptaux) Error (FATAL_ERROR, 1, "Ins. memory (o_order)");

    for (i=0; i < nod; i++)
	ptaux[i] = x[ptper[i]];
    for (i=0; i < nod; i++)
	x[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = y[ptper[i]];
    for (i=0; i < nod; i++)
	y[i] = ptaux[i];

    for (i=0; i < nod; i++)
	ptaux[i] = z[ptper[i]];
    for (i=0; i < nod; i++)
	z[i] = ptaux[i];

    for (i=0; i < nod; i++)
	vper[i] = ptper[i];

    for (i=0; i < 8; i++)
	vper[nod+i] = nod+i;

    mFree (ptaux);
    mFree (ptper);
    }
