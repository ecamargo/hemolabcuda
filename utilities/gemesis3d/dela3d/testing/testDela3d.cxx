#include <iostream.h>
#include "vtkDela3d.h"

int main(int argc, char **argv)
{
  vtkDela3d* dela = vtkDela3d::New();
	dela->DoAll("carotida.cfg");
	cout << "Terminou o processamento" << endl;
	dela->Delete();
	cout << "Deletou objeto da classe vtkDela3d" << endl;	
  return 0;
}
