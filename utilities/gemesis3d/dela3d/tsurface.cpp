/*
** Enzo et al.
*/

#include "tsurface.h"
#include "vtkMeshData.h"
#include "vtkParamMesh.h"
#include "vtkCell.h"
#include "elgroups.h"
#include <string.h>
#include <math.h>

int cmplong (void *a, void *b)
{
	long la, lb;
	la = *((long *) a);
	lb = *((long *) b);
	if (la == lb) return 0;
	return ( (la < lb) ? -1 : 1 );
}

int Triangle_Intersect_Box(acLimit3 Box, acPoint3 p1, acPoint3 p2, acPoint3 p3);

T_Surface::T_Surface()
{
  NumNodes = NumElems = 0;
  Coords = NULL;
  Incid = NULL;
  GP_Octree = NULL;
  GrInfo.NumGroups = 0;
  FactorSize = 1.0;
}

T_Surface::~T_Surface()
{
	if (Coords) delete Coords;
	if (Incid) mFree(Incid);
  if (GP_Octree) FreeOctree(GP_Octree);
  if (GrInfo.NumGroups)
		{
		mFree(GrInfo.sw);
		mFree(GrInfo.lastel);
		}
}

void T_Surface::GetInciFace(long face, long *inci)
{
	inci[0] = Incid[3*face];
	inci[1] = Incid[3*face+1];
	inci[2] = Incid[3*face+2];
}

int T_Surface::GetFaceSW (long face)
{
  int group = 0;
  while (face > GrInfo.lastel[group])
		{
		group++;
		}
	return GrInfo.sw[group];
}

void T_Surface::CopyCoordVecs (double huge *x, double huge *y, double huge *z)
{
  for (long i = 0; i<NumNodes; i++)
		{
		x[i] = (*Coords)[i].x;
		y[i] = (*Coords)[i].y;
		z[i] = (*Coords)[i].z;
		}
}

void T_Surface::FreeCoords (void)
{
	if (Coords)
		{
		delete Coords;
		Coords = NULL;
		}
}

void T_Surface::LoadListSort(TS_GP_NOctree *branch, acList *FaceList, acPoint3 pp)
{
  if (branch->ptr[0])
		{
		int sbranch;
		sbranch  = pp.x > branch->cg.x ? 1 : 0;
		sbranch += pp.y > branch->cg.y ? 2 : 0;
		if (pp.z < branch->cg.z)
		    LoadListSort(branch->ptr[sbranch], FaceList, pp);
		LoadListSort(branch->ptr[sbranch+4], FaceList, pp);
		return;
		}
  if (!branch->FaceList) return;
    acNodeList *nl;
    nl = branch->FaceList->GetHead();
	while (nl)
	{
	long face;
	face = *((long *)nl->GetDados());
	FaceList->InsertSort(&face);
	nl = nl->next;
	}
	return;
}

int T_Surface::Interior(acPoint3 &point)
{
    int N_Cortes;
    acPoint3 pp;
    pp = point;

    acList FaceList(sizeof(long), cmplong);
    acNodeList *pl;
    long elem, n1, n2, n3;
    int idx1, idx2, idx3, idy1, idy2, idy3;
    int ida, idb;
    double alpha, xx, zz;
    acPoint3 pn1, pn2, pn3;
    acPoint3 pa, pb;

    N_Cortes = 0;
// Recorre lista de caras en esta hoja.
    LoadListSort(GP_Octree, &FaceList, pp);
    for (pl = FaceList.GetHead(); pl; pl = pl->next)
	{
	elem = *((long *) pl->GetDados());
	n1 = Incid[3*elem];
	n2 = Incid[3*elem+1];
	n3 = Incid[3*elem+2];
	pn1 = (*Coords)[n1];
	pn2 = (*Coords)[n2];
	pn3 = (*Coords)[n3];
// Normaliza coordenadas x e y de los 3 puntos de la cara
// a un sistema con origen en el punto pp
// Determina posiciones de n1, n2 y n3 con respecto a pp
	pn1.x -= pp.x; idx1 = (pn1.x > 0) ? 1 : -1;
	pn1.y -= pp.y; idy1 = (pn1.y > 0) ? 1 : -1;
	pn2.x -= pp.x; idx2 = (pn2.x > 0) ? 1 : -1;
	pn2.y -= pp.y; idy2 = (pn2.y > 0) ? 1 : -1;
	pn3.x -= pp.x; idx3 = (pn3.x > 0) ? 1 : -1;
	pn3.y -= pp.y; idy3 = (pn3.y > 0) ? 1 : -1;

	if ((idx1 + idx2 + idx3) == -3 ||
	    (idx1 + idx2 + idx3) ==  3 ||
	    (idy1 + idy2 + idy3) == -3 ||
	    (idy1 + idy2 + idy3) ==  3)   continue;

// Determina interseccion del triangulo (n1,n2,n3) con el plano x - z
// (en el espacio)
	ida = idb = 0;
	if ((idy1 + idy2) == 0)   // Arista n1-n2 corta al eje x.
	    {
// Para hacer siempre la misma cuenta: en el numerador el nodo
// del semiplano y > 0
	    if (idy1 == 1)
		{
		alpha = pn1.y / (pn1.y - pn2.y);
		pa.x = (1.0 - alpha) * pn1.x + alpha * pn2.x;
		pa.z = (1.0 - alpha) * pn1.z + alpha * pn2.z;
		}
	    else
		{
		alpha = pn2.y / (pn2.y - pn1.y);
		pa.x = (1.0 - alpha) * pn2.x + alpha * pn1.x;
		pa.z = (1.0 - alpha) * pn2.z + alpha * pn1.z;
		}
	    ida = (pa.x > 0) ? 1 : -1;
	    }

	if ((idy1 + idy3) == 0)   // Arista n1-n3 corta al eje x.
	    {
	// Para hacer siempre la misma cuenta: en el numerador el nodo
	// del semiplano y > 0
	    if (idy1 == 1)
		{
		alpha = pn1.y / (pn1.y - pn3.y);
		xx = (1.0 - alpha) * pn1.x + alpha * pn3.x;
		zz = (1.0 - alpha) * pn1.z + alpha * pn3.z;
		}
	    else
		{
		alpha = pn3.y / (pn3.y - pn1.y);
		xx = (1.0 - alpha) * pn3.x + alpha * pn1.x;
		zz = (1.0 - alpha) * pn3.z + alpha * pn1.z;
		}
	    if (ida == 0)
		{
		ida = (xx > 0) ? 1 : -1;
		pa.x = xx;
		pa.z = zz;
		}
	    else
		{
		idb = (xx > 0) ? 1 : -1;
		pb.x = xx;
		pb.z = zz;
		}
	    }

	if (idb == 0          &&
	    (idy2 + idy3) == 0)   // Arista n2-n3 corta al eje x.
	    {
// Para hacer siempre la misma cuenta: en el numerador el nodo
// del semiplano y > 0
	    if (idy2 == 1)
		{
		alpha = pn2.y / (pn2.y - pn3.y);
		pb.x = (1.0 - alpha) * pn2.x + alpha * pn3.x;
		pb.z = (1.0 - alpha) * pn2.z + alpha * pn3.z;
		}
	    else
		{
		alpha = pn3.y / (pn3.y - pn2.y);
		pb.x = (1.0 - alpha) * pn3.x + alpha * pn2.x;
		pb.z = (1.0 - alpha) * pn3.z + alpha * pn2.z;
		}
	    idb = (pb.x > 0) ? 1 : -1;
	    }

	if ((ida + idb) != 0) // El segmento interseccion no contiene
	    continue;           // al origen

	// Determina coordenada z del corte
	ida = (pa.z > point.z) ? 1 : -1;
	idb = (pb.z > point.z) ? 1 : -1;
	if ((ida + idb) == -2) // El corte esta debajo de point
	    continue;

	if ((ida + idb) == 2) // El corte esta sobre point
	    {
	    N_Cortes++;
	    continue;
	    }

	if (ida == 1)
	    {
	    alpha = pa.x / (pa.x - pb.x);
	    zz = (1.0 - alpha) * pa.z + alpha * pb.z;
	    }
	else
	    {
	    alpha = pb.x / (pb.x - pa.x);
	    zz = (1.0 - alpha) * pb.z + alpha * pa.z;
	    }

	if (zz > point.z) N_Cortes++;
	}

	return (N_Cortes & 1);      // TRUE si N_Cortes es impar
}

acQueue *T_Surface::GenPoints (vtkParamMesh *mp)
{
	acQueue *retval;
	int retError;
	acPoint3 cg;
	double dx, dy, dz, d;
	dx = Volume.xmax-Volume.xmin;
	dy = Volume.ymax-Volume.ymin;
	dz = Volume.zmax-Volume.zmin;
	cg.x = Volume.xmax*0.49+Volume.xmin*0.51;
	cg.y = Volume.ymax*0.49+Volume.ymin*0.51;
	cg.z = Volume.zmax*0.49+Volume.zmin*0.51;
	d = dx > dy ? dx : dy; d = dz > d ? dz : d;
	d *= 0.55;
	
	this->GP_Octree = new TS_GP_NOctree;
	if (!GP_Octree)
		Error (FATAL_ERROR, 1, "Insufficient memory (GenPoints)");
	GP_Octree->cg = cg;
	GP_Octree->tc = d;

	for (int i = 0; i<8; i++)
		GP_Octree->ptr[i] = NULL;
	GP_Octree->no = -1;
	GP_Octree->FaceList = NULL;
	
  for (long j = 0; j<NumNodes; j++)
	  {
		retError = AddNodeGPOct(j);
		if(retError) return NULL;
  	}
	//    TimeOff("Boundary nodes octree building", 3);
	
	for (long face = 0; face < this->NumElems; face++)
		InsertFaceOT(this->GP_Octree, face);

	retval = BranchGenPoints(GP_Octree, mp);
	
	FreeOctree(GP_Octree);
	GP_Octree = NULL;

	return retval;
}

void T_Surface::InsertFaceOT (TS_GP_NOctree *branch, long face)
{
    long pinci = 3*face;
    long n1, n2, n3;
    acLimit3 Lbox;
    acPoint3 p1, p2, p3;
    n1 = Incid[pinci++];
    n2 = Incid[pinci++];
    n3 = Incid[pinci];
    p1 = (*Coords)[n1];
    p2 = (*Coords)[n2];
    p3 = (*Coords)[n3];

    Lbox.xmin = branch->cg.x - branch->tc;
    Lbox.xmax = branch->cg.x + branch->tc;
    Lbox.ymin = branch->cg.y - branch->tc;
    Lbox.ymax = branch->cg.y + branch->tc;
    Lbox.zmin = branch->cg.z - branch->tc;
    Lbox.zmax = branch->cg.z + branch->tc;

    if (!Triangle_Intersect_Box(Lbox, p1, p2, p3))
	return;

  if (branch->ptr[0])
	{
	for (int i = 0; i < 8; i++)
	    InsertFaceOT(branch->ptr[i], face);
	return;
	}

  if (!branch->FaceList)
		branch->FaceList = new acList(sizeof(long));
  branch->FaceList->InsertFirst(&face);
}

void T_Surface::FreeOctree (TS_GP_NOctree *branch)
    {
    if (branch->ptr[0])
	for (int i = 0; i < 8; i++)
	    FreeOctree(branch->ptr[i]);
    if (branch->FaceList)
	delete branch->FaceList;
    delete branch;
    }

int T_Surface::AddNodeGPOct (long newnode)
    {
    TS_GP_NOctree *curr = GP_Octree;
    TS_GP_NOctree *pbranch;
    int branch, branch2;

    while (curr->ptr[0] != NULL)     // Baja hasta una hoja
	{
	branch  = (*Coords)[newnode].x > curr->cg.x ? 1 : 0;
	branch += (*Coords)[newnode].y > curr->cg.y ? 2 : 0;
	branch += (*Coords)[newnode].z > curr->cg.z ? 4 : 0;
	curr = curr->ptr[branch];
	}

    while (curr->no >= 0)    // Ya hay un nodo en esta hoja
	{
	acPoint3 ncg;
	double ntc;
	ntc = curr->tc * 0.5;
	for (int  i=0; i<8; i++)     // Divide hoja
	    {
	    pbranch = new TS_GP_NOctree;
	    if (!pbranch)
		    {
				Error (FATAL_ERROR, 1,"Insufficient Memory (AddNodeGPOct)");
				return 1;
		    }
	    curr->ptr[i] = pbranch;
	    ncg.x = (i % 2) ?
		     curr->cg.x + ntc :
		     curr->cg.x - ntc;
	    ncg.y = ((i/2) % 2) ?
		     curr->cg.y + ntc :
		     curr->cg.y - ntc;
	    ncg.z = (i > 3) ?
		     curr->cg.z + ntc :
		     curr->cg.z - ntc;
	    pbranch->cg = ncg;
	    pbranch->tc = ntc;
	    for (int j=0; j<8; j++) pbranch->ptr[j] = NULL;
	    pbranch->no = -1;
	    pbranch->FaceList = NULL;
	    }

	branch  = (*Coords)[curr->no].x > curr->cg.x ? 1 : 0;
	branch += (*Coords)[curr->no].y > curr->cg.y ? 2 : 0;
	branch += (*Coords)[curr->no].z > curr->cg.z ? 4 : 0;

	branch2  = (*Coords)[newnode].x > curr->cg.x ? 1 : 0;
	branch2 += (*Coords)[newnode].y > curr->cg.y ? 2 : 0;
	branch2 += (*Coords)[newnode].z > curr->cg.z ? 4 : 0;

	if (branch == branch2)
	    {
	    double d2, dx, dy, dz;
	    dx = (*Coords)[curr->no].x - (*Coords)[newnode].x;
	    dy = (*Coords)[curr->no].y - (*Coords)[newnode].y;
	    dz = (*Coords)[curr->no].z - (*Coords)[newnode].z;
	    d2 = dx*dx + dy*dy + dz*dz;
	    dx = curr->tc;
	    if (d2 < 1.0e-12 * dx*dx)
				{
				return 2;
				//Error (FATAL_ERROR, 2,"Nodes: %ld and %ld too close", curr->no+1, newnode+1);
				}
		}

	curr->ptr[branch]->no = curr->no;
	curr->no = -1;

	curr = curr->ptr[branch2];
	}

	curr->no = newnode;
	return 0;
}

acQueue *T_Surface::BranchGenPoints(TS_GP_NOctree *pBranch, vtkParamMesh *mp)
{
  if (pBranch->ptr[0])     // Es rama
		{
		int i, changed;
		acQueue *rlist;
		acQueue *hlist[8];
		long nodo;
		for (i = 0; i < 8; i++)
		    hlist[i] = BranchGenPoints(pBranch->ptr[i], mp);
	// Baraja listas
		rlist = new acQueue(sizeof(long));
	
		changed = 1;
		while (changed)
			{
		  changed = 0;
		  for (i = 0; i < 8; i++)
				{
				if (hlist[i])
			    if (hlist[i]->Pop(&nodo) == 1)
						{
						rlist->Push(&nodo);
						changed = 1;
						}
				}
		  }
	
	// Borra hojas
		for (i = 0; i < 8; i++)
		    {
		    if (hlist[i]) delete hlist[i];
		    delete pBranch->ptr[i];
		    }
	// Convierte en hoja
	
		pBranch->ptr[0] = NULL;
		return rlist;
		}
	else
		{
		acLimit3 Vol;
		Vol.xmin = pBranch->cg.x - pBranch->tc;
		Vol.xmax = pBranch->cg.x + pBranch->tc;
		Vol.ymin = pBranch->cg.y - pBranch->tc;
		Vol.ymax = pBranch->cg.y + pBranch->tc;
		Vol.zmin = pBranch->cg.z - pBranch->tc;
		Vol.zmax = pBranch->cg.z + pBranch->tc;
		if (pBranch->no >= 0 && pBranch->FaceList)
		{
		  return (CubeGenPointsN(Vol, mp, pBranch->no, pBranch->FaceList->GetHead()));
		}
		else if (pBranch->FaceList)	// Has some associated faces => boundary
		{
		  return (CubeGenPoints (Vol, mp, pBranch->FaceList->GetHead()));
		}
		else if (Interior(pBranch->cg))
		{
		  return (CubeGenPoints (Vol, mp, NULL));
		}
		else
		  return NULL;
		}
}

// Genera puntos en un cubo que no contiene ningun nodo
// de la superficie.
acQueue *T_Surface::CubeGenPoints (acLimit3 Vol, vtkParamMesh *mp, acNodeList *nl)
{
	acQueue *rlist;
	acPoint3 centro, pp;
	double sizeoct, size;
	sizeoct = Vol.xmax - Vol.xmin;

  if (!nl)	// Cube is interior
	{
  // Divide segun Malla de Parametros		
	double npoints;
	double siz;
	centro.x = (Vol.xmin+Vol.xmax)*0.5;
	centro.y = (Vol.ymin+Vol.ymax)*0.5;
	centro.z = (Vol.zmin+Vol.zmax)*0.5;
	size = FactorSize * mp->GetH(centro);
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
	pp.z = Vol.zmin * 0.75 + Vol.zmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,min,min
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,min,min
	size = siz < size ? siz : size;
	pp.y = Vol.ymin * 0.25 + Vol.ymax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,max,min
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,max,min
	size = siz < size ? siz : size;
	pp.z = Vol.zmin * 0.25 + Vol.zmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// min,max,max
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,max,max
	size = siz < size ? siz : size;
	pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// max,min,max
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,min,max
	size = siz < size ? siz : size;

	npoints = (sizeoct / size);
	npoints = npoints*npoints*npoints;
	if (npoints < 8.5) // coloca puntos
	    {
	    if (npoints < 0.5)
		return NULL;
	    else if (npoints <= 1.5)    // Exactamente 1 punto
		{
		rlist = new acQueue(sizeof(long));
		pp.x = 0.5*Vol.xmin + 0.5*Vol.xmax;
		pp.y = 0.5*Vol.ymin + 0.5*Vol.ymax;
		pp.z = 0.5*Vol.zmin + 0.5*Vol.zmax;
		//if (!TooNear(pp, size))
		    {
		    Coords->AddCoord(pp); rlist->Push(&NumNodes);
		    NumNodes++;
		    }
		}
	    else                        // Por lo menos 2 puntos
		{
		rlist = new acQueue(sizeof(long));
		pp.x = 0.75*Vol.xmin + 0.25*Vol.xmax;
		pp.y = 0.75*Vol.ymin + 0.25*Vol.ymax;
		pp.z = 0.75*Vol.zmin + 0.25*Vol.zmax;
		//if (!TooNear(pp, size))
		    {
		    Coords->AddCoord(pp); rlist->Push(&NumNodes);
		    NumNodes++;
		    }
		pp.x = 0.25*Vol.xmin + 0.75*Vol.xmax;
		pp.y = 0.25*Vol.ymin + 0.75*Vol.ymax;
		pp.z = 0.25*Vol.zmin + 0.75*Vol.zmax;
		//if (!TooNear(pp, size))
		    {
		    Coords->AddCoord(pp); rlist->Push(&NumNodes);
		    NumNodes++;
		    }
		if (npoints > 2.5) 	// Por lo menos 3 puntos
		    {
		    pp.x = 0.75*Vol.xmin + 0.25*Vol.xmax;
		    pp.y = 0.75*Vol.ymin + 0.25*Vol.ymax;
		    pp.z = 0.25*Vol.zmin + 0.75*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		if (npoints > 3.5) 	// Por lo menos 4 puntos
		    {
		    pp.x = 0.25*Vol.xmin + 0.75*Vol.xmax;
		    pp.y = 0.25*Vol.ymin + 0.75*Vol.ymax;
		    pp.z = 0.75*Vol.zmin + 0.25*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		if (npoints > 4.5) 	// Por lo menos 5 puntos
		    {
		    pp.x = 0.25*Vol.xmin + 0.75*Vol.xmax;
		    pp.y = 0.75*Vol.ymin + 0.25*Vol.ymax;
		    pp.z = 0.75*Vol.zmin + 0.25*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		if (npoints > 5.5) 	// Por lo menos 6 puntos
		    {
		    pp.x = 0.75*Vol.xmin + 0.25*Vol.xmax;
		    pp.y = 0.25*Vol.ymin + 0.75*Vol.ymax;
		    pp.z = 0.25*Vol.zmin + 0.75*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		if (npoints > 6.5) 	// Por lo menos 7 puntos
		    {
		    pp.x = 0.25*Vol.xmin + 0.75*Vol.xmax;
		    pp.y = 0.75*Vol.ymin + 0.25*Vol.ymax;
		    pp.z = 0.25*Vol.zmin + 0.75*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		if (npoints > 7.5) 	// 8 puntos
		    {
		    pp.x = 0.75*Vol.xmin + 0.25*Vol.xmax;
		    pp.y = 0.25*Vol.ymin + 0.75*Vol.ymax;
		    pp.z = 0.75*Vol.zmin + 0.25*Vol.zmax;
		    //if (!TooNear(pp, size))
			{
			Coords->AddCoord(pp); rlist->Push(&NumNodes);
			NumNodes++;
			}
		    }
		}
	    return rlist;
	    }  	// endif colocar puntos, programar la division abajo !!
	}       // endif cubo es interior
    else  // cubo sobre la frontera
	{
	double npoints;
	double siz;
	centro.x = (Vol.xmin+Vol.xmax)*0.5;
	centro.y = (Vol.ymin+Vol.ymax)*0.5;
	centro.z = (Vol.zmin+Vol.zmax)*0.5;
	size = FactorSize * mp->GetH(centro);
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
	pp.z = Vol.zmin * 0.75 + Vol.zmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,min,min
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,min,min
	size = siz < size ? siz : size;
	pp.y = Vol.ymin * 0.25 + Vol.ymax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,max,min
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,max,min
	size = siz < size ? siz : size;
	pp.z = Vol.zmin * 0.25 + Vol.zmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// min,max,max
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
	siz = FactorSize * mp->GetH(pp);	// max,max,max
	size = siz < size ? siz : size;
	pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// max,min,max
	size = siz < size ? siz : size;
	pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
	siz = FactorSize * mp->GetH(pp);	// min,min,max
	size = siz < size ? siz : size;

	npoints = (sizeoct / size);
	npoints = npoints*npoints*npoints;
	if (npoints < 1)
	    return NULL;
	}

//	Cube Subdivision
    int i;
    acPoint3 cc;
    acQueue *hlist[8];
    acLimit3 SubVol;
    for (i = 0; i < 8; i++)
	{
	SubVol.xmin = (i % 2) ? centro.x : Vol.xmin;
	SubVol.xmax = (i % 2) ? Vol.xmax : centro.x;
	SubVol.ymin = ((i/2) % 2) ? centro.y : Vol.ymin;
	SubVol.ymax = ((i/2) % 2) ? Vol.ymax : centro.y;
	SubVol.zmin = (i > 3) ? centro.z : Vol.zmin;
	SubVol.zmax = (i > 3) ? Vol.zmax : centro.z;
	if (Triangles_Intersect_Cube(SubVol, nl))
	    hlist[i] = CubeGenPoints (SubVol, mp, nl);
	else
	    {
	    cc.x = 0.5*(SubVol.xmin + SubVol.xmax);
	    cc.y = 0.5*(SubVol.ymin + SubVol.ymax);
	    cc.z = 0.5*(SubVol.zmin + SubVol.zmax);
// ***********Modificacion Marc
	    if (Flagextnod || Interior(cc))
            hlist[i] = CubeGenPoints (SubVol, mp, NULL);
	    else
            hlist[i] = NULL;
	    }
	}
// Baraja listas
    rlist = new acQueue(sizeof(long));
    long nodo;
    int changed = 1;
    while (changed)
	{
	changed = 0;
	for (i = 0; i < 8; i++)
	    {
	    if (hlist[i])
		if (hlist[i]->Pop(&nodo) == 1)
		    {
		    rlist->Push(&nodo);
		    changed = 1;
		    }
	    }
	}

	// Borra sub-listas
  for (i = 0; i < 8; i++)
	if (hlist[i]) delete hlist[i];
  return rlist;
}

// Genera puntos en un cubo que contiene algun nodo
// de la superficie.
acQueue *T_Surface::CubeGenPointsN (acLimit3 Vol, vtkParamMesh *mp, long N,acNodeList *nl)
{
  acPoint3 centro, pp;
  double sizeoct, size, siz;
  sizeoct = Vol.xmax - Vol.xmin;
	  // Divide segun Malla de Parametros
  double npoints;
	centro.x = (Vol.xmin+Vol.xmax)*0.5;
  centro.y = (Vol.ymin+Vol.ymax)*0.5;
  centro.z = (Vol.zmin+Vol.zmax)*0.5;
  size = FactorSize * mp->GetH(centro);
  pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
  pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
  pp.z = Vol.zmin * 0.75 + Vol.zmax * 0.25;
  siz = FactorSize * mp->GetH(pp);	// min,min,min
  size = siz < size ? siz : size;
  pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
  siz = FactorSize * mp->GetH(pp);	// max,min,min
  size = siz < size ? siz : size;
  pp.y = Vol.ymin * 0.25 + Vol.ymax * 0.75;
  siz = FactorSize * mp->GetH(pp);	// max,max,min
  size = siz < size ? siz : size;
  pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
  siz = FactorSize * mp->GetH(pp);	// min,max,min
  size = siz < size ? siz : size;
  pp.z = Vol.zmin * 0.25 + Vol.zmax * 0.75;
  siz = FactorSize * mp->GetH(pp);	// min,max,max
  size = siz < size ? siz : size;
  pp.x = Vol.xmin * 0.25 + Vol.xmax * 0.75;
  siz = FactorSize * mp->GetH(pp);	// max,max,max
  size = siz < size ? siz : size;
  pp.y = Vol.ymin * 0.75 + Vol.ymax * 0.25;
  siz = FactorSize * mp->GetH(pp);	// max,min,max
  size = siz < size ? siz : size;
  pp.x = Vol.xmin * 0.75 + Vol.xmax * 0.25;
  siz = FactorSize * mp->GetH(pp);	// min,min,max
  size = siz < size ? siz : size;
	
  npoints = (sizeoct / size);
  npoints = npoints*npoints*npoints;
  if (npoints < 1)  // devuelve lista con nodo contenido
		{
		acQueue *rlist = new acQueue(sizeof(long));
		rlist->Push(&N);
		return rlist;
		}

	// divide
  int i, branch;
  branch  = (*Coords)[N].x > centro.x ? 1 : 0;
  branch += (*Coords)[N].y > centro.y ? 2 : 0;
  branch += (*Coords)[N].z > centro.z ? 4 : 0;

  acQueue *rlist;
  acQueue *hlist[8];
  acLimit3 SubVol;
  for (i = 0; i < 8; i++)
		{
		SubVol.xmin = (i % 2) ? centro.x : Vol.xmin;
		SubVol.xmax = (i % 2) ? Vol.xmax : centro.x;
		SubVol.ymin = ((i/2) % 2) ? centro.y : Vol.ymin;
		SubVol.ymax = ((i/2) % 2) ? Vol.ymax : centro.y;
		SubVol.zmin = (i > 3) ? centro.z : Vol.zmin;
		SubVol.zmax = (i > 3) ? Vol.zmax : centro.z;
		if (i == branch)
	    hlist[i] = CubeGenPointsN (SubVol, mp, N, nl);
		else if (Triangles_Intersect_Cube(SubVol, nl))
		    hlist[i] = CubeGenPoints (SubVol, mp, nl);
		else
	    {
	    acPoint3 cc;
	    cc.x = 0.5*(SubVol.xmin + SubVol.xmax);
	    cc.y = 0.5*(SubVol.ymin + SubVol.ymax);
	    cc.z = 0.5*(SubVol.zmin + SubVol.zmax);
	    if (Interior(cc))
				hlist[i] = CubeGenPoints (SubVol, mp, NULL);
	    else
				hlist[i] = NULL;
	    }
		}
		
	// Baraja listas
  rlist = new acQueue(sizeof(long));
  long nodo;
  int changed = 1;
  while (changed)
	{
		changed = 0;
		for (i = 0; i < 8; i++)
    {
    if (hlist[i])
			if (hlist[i]->Pop(&nodo) == 1)
		    {
		    rlist->Push(&nodo);
		    changed = 1;
		    }
	    }
		}

	// Borra sub-listas
  for (i = 0; i < 8; i++)
	if (hlist[i]) delete hlist[i];

  return rlist;
}

int T_Surface::Triangles_Intersect_Cube(acLimit3 SubVol, acNodeList *nl)
{
	acPoint3 p1, p2, p3;
	long face, n1, n2, n3;
	while (nl)
	{
	face = *((long *)nl->GetDados());
	n1 = Incid[3*face];
	n2 = Incid[3*face+1];
	n3 = Incid[3*face+2];
	p1 = (*Coords)[n1];
	p2 = (*Coords)[n2];
	p3 = (*Coords)[n3];
	if (Triangle_Intersect_Box(SubVol, p1, p2, p3))
	    return 1;
	nl = nl->next;
	}
  return 0;
}

void T_Surface::SetCoord(long i, acPoint3 &pp)
{
  if (i < NumNodes)
		(*Coords)[i] = pp;
  else
		{
		Coords->AddCoord(pp);
		NumNodes++;
		}
}


int Triangle_Intersect_Box(acLimit3 Box, acPoint3 p1, acPoint3 p2, acPoint3 p3)
{
    acLimit3 Bbox;
    Bbox.xmin = Bbox.xmax = p1.x;
    Bbox.ymin = Bbox.ymax = p1.y;
    Bbox.zmin = Bbox.zmax = p1.z;
    if      (p2.x < Bbox.xmin) Bbox.xmin = p2.x;
    else if (p2.x > Bbox.xmax) Bbox.xmax = p2.x;
    if      (p2.y < Bbox.ymin) Bbox.ymin = p2.y;
    else if (p2.y > Bbox.ymax) Bbox.ymax = p2.y;
    if      (p2.z < Bbox.zmin) Bbox.zmin = p2.z;
    else if (p2.z > Bbox.zmax) Bbox.zmax = p2.z;
    if      (p3.x < Bbox.xmin) Bbox.xmin = p3.x;
    else if (p3.x > Bbox.xmax) Bbox.xmax = p3.x;
    if      (p3.y < Bbox.ymin) Bbox.ymin = p3.y;
    else if (p3.y > Bbox.ymax) Bbox.ymax = p3.y;
    if      (p3.z < Bbox.zmin) Bbox.zmin = p3.z;
    else if (p3.z > Bbox.zmax) Bbox.zmax = p3.z;

    if (Box.xmin > Bbox.xmax || Box.xmax < Bbox.xmin)
	return 0;
    if (Box.ymin > Bbox.ymax || Box.ymax < Bbox.ymin)
	return 0;
    if (Box.zmin > Bbox.zmax || Box.zmax < Bbox.zmin)
	return 0;

    int sgxmin1, sgxmax1, sgymin1, sgymax1, sgzmin1, sgzmax1,
	sgxmin2, sgxmax2, sgymin2, sgymax2, sgzmin2, sgzmax2,
	sgxmin3, sgxmax3, sgymin3, sgymax3, sgzmin3, sgzmax3;
    sgxmin1 = (p1.x > Box.xmin) ? 1 : -1;
    sgxmax1 = (p1.x > Box.xmax) ? 1 : -1;
    sgymin1 = (p1.y > Box.ymin) ? 1 : -1;
    sgymax1 = (p1.y > Box.ymax) ? 1 : -1;
    sgzmin1 = (p1.z > Box.zmin) ? 1 : -1;
    sgzmax1 = (p1.z > Box.zmax) ? 1 : -1;
    sgxmin2 = (p2.x > Box.xmin) ? 1 : -1;
    sgxmax2 = (p2.x > Box.xmax) ? 1 : -1;
    sgymin2 = (p2.y > Box.ymin) ? 1 : -1;
    sgymax2 = (p2.y > Box.ymax) ? 1 : -1;
    sgzmin2 = (p2.z > Box.zmin) ? 1 : -1;
    sgzmax2 = (p2.z > Box.zmax) ? 1 : -1;
    sgxmin3 = (p3.x > Box.xmin) ? 1 : -1;
    sgxmax3 = (p3.x > Box.xmax) ? 1 : -1;
    sgymin3 = (p3.y > Box.ymin) ? 1 : -1;
    sgymax3 = (p3.y > Box.ymax) ? 1 : -1;
    sgzmin3 = (p3.z > Box.zmin) ? 1 : -1;
    sgzmax3 = (p3.z > Box.zmax) ? 1 : -1;
	{	// Punto 1 interior
	if (sgxmin1 * sgxmax1 < 0 &&
	    sgymin1 * sgymax1 < 0 &&
	    sgzmin1 * sgzmax1 < 0)
	    return 1;
	}
	{	// Punto 2 interior
	if (sgxmin2 * sgxmax2 < 0 &&
	    sgymin2 * sgymax2 < 0 &&
	    sgzmin2 * sgzmax2 < 0)
	    return 1;
	}
	{	// Punto 3 interior
	if (sgxmin3 * sgxmax3 < 0 &&
	    sgymin3 * sgymax3 < 0 &&
	    sgzmin3 * sgzmax3 < 0)
	    return 1;
	}
	{	// intersecci�n arista 1-2 con cara xmin
	if(sgxmin1 * sgxmin2 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmin - p1.x) / (p2.x - p1.x);
	    yy = (1-alpha)*p1.y + alpha*p2.y;
	    zz = (1-alpha)*p1.z + alpha*p2.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 1-2 con cara xmax
	if(sgxmax1 * sgxmax2 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmax - p1.x) / (p2.x - p1.x);
	    yy = (1-alpha)*p1.y + alpha*p2.y;
	    zz = (1-alpha)*p1.z + alpha*p2.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 1-2 con cara ymin
	if(sgymin1 * sgymin2 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymin - p1.y) / (p2.y - p1.y);
	    xx = (1-alpha)*p1.x + alpha*p2.x;
	    zz = (1-alpha)*p1.z + alpha*p2.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 1-2 con cara ymax
	if(sgymax1 * sgymax2 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymax - p1.y) / (p2.y - p1.y);
	    xx = (1-alpha)*p1.x + alpha*p2.x;
	    zz = (1-alpha)*p1.z + alpha*p2.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 1-2 con cara zmin
	if(sgzmin1 * sgzmin2 < 0)
	    {
	    double alpha, xx, yy;
	    alpha = (Box.zmin - p1.z) / (p2.z - p1.z);
	    yy = (1-alpha)*p1.y + alpha*p2.y;
	    xx = (1-alpha)*p1.x + alpha*p2.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 1-2 con cara zmax
	if(sgzmax1 * sgzmax2 < 0)
	    {
	    double alpha, yy, xx;
	    alpha = (Box.zmax - p1.z) / (p2.z - p1.z);
	    yy = (1-alpha)*p1.y + alpha*p2.y;
	    xx = (1-alpha)*p1.x + alpha*p2.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}

	{	// intersecci�n arista 2-3 con cara xmin
	if(sgxmin2 * sgxmin3 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmin - p2.x) / (p3.x - p2.x);
	    yy = (1-alpha)*p2.y + alpha*p3.y;
	    zz = (1-alpha)*p2.z + alpha*p3.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 2-3 con cara xmax
	if(sgxmax2 * sgxmax3 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmax - p2.x) / (p3.x - p2.x);
	    yy = (1-alpha)*p2.y + alpha*p3.y;
	    zz = (1-alpha)*p2.z + alpha*p3.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 2-3 con cara ymin
	if(sgymin2 * sgymin3 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymin - p2.y) / (p3.y - p2.y);
	    xx = (1-alpha)*p2.x + alpha*p3.x;
	    zz = (1-alpha)*p2.z + alpha*p3.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 2-3 con cara ymax
	if(sgymax2 * sgymax3 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymax - p2.y) / (p3.y - p2.y);
	    xx = (1-alpha)*p2.x + alpha*p3.x;
	    zz = (1-alpha)*p2.z + alpha*p3.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 2-3 con cara zmin
	if(sgzmin2 * sgzmin3 < 0)
	    {
	    double alpha, xx, yy;
	    alpha = (Box.zmin - p2.z) / (p3.z - p2.z);
	    yy = (1-alpha)*p2.y + alpha*p3.y;
	    xx = (1-alpha)*p2.x + alpha*p3.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 2-3 con cara zmax
	if(sgzmax2 * sgzmax3 < 0)
	    {
	    double alpha, yy, xx;
	    alpha = (Box.zmax - p2.z) / (p3.z - p2.z);
	    yy = (1-alpha)*p2.y + alpha*p3.y;
	    xx = (1-alpha)*p2.x + alpha*p3.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}

	{	// intersecci�n arista 3-1 con cara xmin
	if(sgxmin3 * sgxmin1 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmin - p3.x) / (p1.x - p3.x);
	    yy = (1-alpha)*p3.y + alpha*p1.y;
	    zz = (1-alpha)*p3.z + alpha*p1.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 3-1 con cara xmax
	if(sgxmax3 * sgxmax1 < 0)
	    {
	    double alpha, yy, zz;
	    alpha = (Box.xmax - p3.x) / (p1.x - p3.x);
	    yy = (1-alpha)*p3.y + alpha*p1.y;
	    zz = (1-alpha)*p3.z + alpha*p1.z;
	    if (yy>Box.ymin && yy<Box.ymax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 3-1 con cara ymin
	if(sgymin3 * sgymin1 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymin - p3.y) / (p1.y - p3.y);
	    xx = (1-alpha)*p3.x + alpha*p1.x;
	    zz = (1-alpha)*p3.z + alpha*p1.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 3-1 con cara ymax
	if(sgymax3 * sgymax1 < 0)
	    {
	    double alpha, xx, zz;
	    alpha = (Box.ymax - p3.y) / (p1.y - p3.y);
	    xx = (1-alpha)*p3.x + alpha*p1.x;
	    zz = (1-alpha)*p3.z + alpha*p1.z;
	    if (xx>Box.xmin && xx<Box.xmax &&
		zz>Box.zmin && zz<Box.zmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 3-1 con cara zmin
	if(sgzmin3 * sgzmin1 < 0)
	    {
	    double alpha, xx, yy;
	    alpha = (Box.zmin - p3.z) / (p1.z - p3.z);
	    yy = (1-alpha)*p3.y + alpha*p1.y;
	    xx = (1-alpha)*p3.x + alpha*p1.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}
	{	// intersecci�n arista 3-1 con cara zmax
	if(sgzmax3 * sgzmax1 < 0)
	    {
	    double alpha, yy, xx;
	    alpha = (Box.zmax - p3.z) / (p1.z - p3.z);
	    yy = (1-alpha)*p3.y + alpha*p1.y;
	    xx = (1-alpha)*p3.x + alpha*p1.x;
	    if (yy>Box.ymin && yy<Box.ymax &&
		xx>Box.xmin && xx<Box.xmax)
		return 1;
	    }
	}

	{	// intersecci�n aristas del cubo con triangulo
	double a, b, c, d; // Plane equation
	a = (p2.y-p1.y)*(p3.z-p1.z)-(p2.z-p1.z)*(p3.y-p1.y);
	b = (p2.z-p1.z)*(p3.x-p1.x)-(p2.x-p1.x)*(p3.z-p1.z);
	c = (p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
	d = -(a*p1.x + b*p1.y + c*p1.z);

	double d000, d001, d010, d011, d100, d101, d110, d111;
	int sg000, sg001, sg010, sg011, sg100, sg101, sg110, sg111;
	d000 = a*Box.xmin+b*Box.ymin+c*Box.zmin+d; sg000 = (d000>0)? 1 : -1;
	d001 = a*Box.xmax+b*Box.ymin+c*Box.zmin+d; sg001 = (d001>0)? 1 : -1;
	d010 = a*Box.xmin+b*Box.ymax+c*Box.zmin+d; sg010 = (d010>0)? 1 : -1;
	d011 = a*Box.xmax+b*Box.ymax+c*Box.zmin+d; sg011 = (d011>0)? 1 : -1;
	d100 = a*Box.xmin+b*Box.ymin+c*Box.zmax+d; sg100 = (d100>0)? 1 : -1;
	d101 = a*Box.xmax+b*Box.ymin+c*Box.zmax+d; sg101 = (d101>0)? 1 : -1;
	d110 = a*Box.xmin+b*Box.ymax+c*Box.zmax+d; sg110 = (d110>0)? 1 : -1;
	d111 = a*Box.xmax+b*Box.ymax+c*Box.zmax+d; sg111 = (d111>0)? 1 : -1;
	if (sg000 * sg001 < 0)	// ymin-zmin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d000 / (d001 - d000);
	    xx = (1-alpha) * Box.xmin + alpha * Box.xmax;
	    yy = Box.ymin;
	    zz = Box.zmin;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg010 * sg011 < 0)	// ymax-zmin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d010 / (d011 - d010);
	    xx = (1-alpha) * Box.xmin + alpha * Box.xmax;
	    yy = Box.ymax;
	    zz = Box.zmin;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg100 * sg101 < 0)	// ymin-zmax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d100 / (d101 - d100);
	    xx = (1-alpha) * Box.xmin + alpha * Box.xmax;
	    yy = Box.ymin;
	    zz = Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg110 * sg111 < 0)	// ymax-zmax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d110 / (d111 - d110);
	    xx = (1-alpha) * Box.xmin + alpha * Box.xmax;
	    yy = Box.ymin;
	    zz = Box.zmin;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }

	if (sg000 * sg010 < 0)	// xmin-zmin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d000 / (d010 - d000);
	    xx = Box.xmin;
	    yy = (1-alpha) * Box.ymin + alpha * Box.ymax;
	    zz = Box.zmin;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg001 * sg011 < 0)	// xmax-zmin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d001 / (d011 - d001);
	    xx = Box.xmax;
	    yy = (1-alpha) * Box.ymin + alpha * Box.ymax;
	    zz = Box.zmin;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg100 * sg110 < 0)	// xmin-zmax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d100 / (d110 - d100);
	    xx = Box.xmin;
	    yy = (1-alpha) * Box.ymin + alpha * Box.ymax;
	    zz = Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg101 * sg111 < 0)	// xmax-zmax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d101 / (d111 - d101);
	    xx = Box.xmax;
	    yy = (1-alpha) * Box.ymin + alpha * Box.ymax;
	    zz = Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }

	if (sg000 * sg100 < 0)	// xmin-ymin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d000 / (d100 - d000);
	    xx = Box.xmin;
	    yy = Box.ymin;
	    zz = (1-alpha) * Box.zmin + alpha * Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg001 * sg101 < 0)	// xmax-ymin
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d001 / (d101 - d001);
	    xx = Box.xmax;
	    yy = Box.ymin;
	    zz = (1-alpha) * Box.zmin + alpha * Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg010 * sg110 < 0)	// xmin-ymax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d010 / (d110 - d010);
	    xx = Box.xmin;
	    yy = Box.ymax;
	    zz = (1-alpha) * Box.zmin + alpha * Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	if (sg011 * sg111 < 0)	// xmax-ymax
	    {
	    double alpha, xx, yy, zz, a1, a2, a3;
	    alpha = - d011 / (d111 - d011);
	    xx = Box.xmax;
	    yy = Box.ymax;
	    zz = (1-alpha) * Box.zmin + alpha * Box.zmax;
	    a1 = ((p2.y-p1.y)*(zz-p1.z)-(p2.z-p1.z)*(yy-p1.y)) * a +
		 ((p2.z-p1.z)*(xx-p1.x)-(p2.x-p1.x)*(zz-p1.z)) * b +
		 ((p2.x-p1.x)*(yy-p1.y)-(p2.y-p1.y)*(xx-p1.x)) * c ;
	    a2 = ((p3.y-p2.y)*(zz-p2.z)-(p3.z-p2.z)*(yy-p2.y)) * a +
		 ((p3.z-p2.z)*(xx-p2.x)-(p3.x-p2.x)*(zz-p2.z)) * b +
		 ((p3.x-p2.x)*(yy-p2.y)-(p3.y-p2.y)*(xx-p2.x)) * c ;
	    a3 = ((p1.y-p3.y)*(zz-p3.z)-(p1.z-p3.z)*(yy-p3.y)) * a +
		 ((p1.z-p3.z)*(xx-p3.x)-(p1.x-p3.x)*(zz-p3.z)) * b +
		 ((p1.x-p3.x)*(yy-p3.y)-(p1.y-p3.y)*(xx-p3.x)) * c ;
	    if (a1 > 0 && a2 > 0 && a3 > 0)
		return 1;
	    }
	}	// end aristas del cubo vs triangulo
	return 0;
}

void T_Surface::Read(FILE *fp)
{
  static char *nome = "T_Surface::Read";
  char buf[100];
  char tipoelem[50];
  static char *kcoord = "COORDINATES";
  static char *kelem  = "ELEMENT GROUPS";

  TraceOn(nome);

  if (!FindKeyWord(kcoord, fp))
		{//  27, "Keyword |%s| Not Found."
		GetError(27,buf);
		Error(FATAL_ERROR, 1, buf, kcoord);
		}
  Coords = new acDynCoord3D;
  Coords->Read(fp);

  NumNodes = Coords->GetNumNodes();

  Volume.xmax = Volume.xmin = (*Coords)[0].x;
  Volume.ymax = Volume.ymin = (*Coords)[0].y;
  Volume.zmax = Volume.zmin = (*Coords)[0].z;
  long i;
  for (i = 1; i<NumNodes; i++)
		{
		if      ((*Coords)[i].x<Volume.xmin) Volume.xmin=(*Coords)[i].x;
		else if ((*Coords)[i].x>Volume.xmax) Volume.xmax=(*Coords)[i].x;
		if      ((*Coords)[i].y<Volume.ymin) Volume.ymin=(*Coords)[i].y;
		else if ((*Coords)[i].y>Volume.ymax) Volume.ymax=(*Coords)[i].y;
		if      ((*Coords)[i].z<Volume.zmin) Volume.zmin=(*Coords)[i].z;
		else if ((*Coords)[i].z>Volume.zmax) Volume.zmax=(*Coords)[i].z;
		}

  if (!FindKeyWord(kelem, fp))
	{//  27, "Keyword |%s| Not Found."
	GetError(27,buf);
	Error(FATAL_ERROR, 1, buf, kelem);
	}

  NumElems = 0;

  int NumGroups, ii, idum;
  long n;
  fscanf(fp, "%d", &NumGroups);    // le num. de grupos
  if (NumGroups < 1)
	Error(FATAL_ERROR, 1, "Invalid number of Groups");

  GrInfo.NumGroups = NumGroups;
  GrInfo.sw = (int *) mMalloc (NumGroups * sizeof(int));
  GrInfo.lastel = (long *) mMalloc (NumGroups * sizeof(long));
  if (!GrInfo.sw || !GrInfo.lastel)
		{
		GetError(1,buf);
		Error(FATAL_ERROR, 1, buf);
		}

  for (ii = 0; ii < NumGroups; ii++)
		{
		fscanf(fp, "%d %ld %s", &idum, &n, tipoelem);
	
		if (idum != ii+1 || n < 1 ||
		   (strcmp(tipoelem,"Tri3")&&strcmp(tipoelem,"LINEAR_TRIANGLE")))
		    Error(FATAL_ERROR, 3, "Incorrect Group data %d %ld %s",idum, n, tipoelem);
	
		NumElems += n;
		GrInfo.lastel[ii] = NumElems-1;
		}

  if (!FindKeyWord("GROUPS INFO", fp))
		{
		printf("\nGROUPS INFO not found (assumed all groups external)");
		for (ii = 0; ii < NumGroups; ii++)
		    GrInfo.sw[ii] = 1;
		}
  else
		{
		int swr;
		for (ii = 0; ii < NumGroups; ii++)
	  	{
	    fscanf(fp, "%d %d %", &idum, &swr);
	    if (idum != ii+1 || swr < 0 || swr > 1)
				Error(FATAL_ERROR, 3, "Incorrect Group Info data %d %d", idum, swr);
	    GrInfo.sw[ii] = swr;
	    }
		}

    Incid = (long huge *) mMalloc(3*NumElems * sizeof(long));
    if (!Incid)
	{ //    1, "Can't Allocate Memory."
	GetError(1, buf);
	Error(FATAL_ERROR, 2, buf);
	}

  char *kincid = "INCIDENCE";
  if (!FindKeyWord(kincid, fp))
	Error(FATAL_ERROR, 5, "Keyword %s not found", kincid);

  long no;
  for (i = 0; i < 3*NumElems; i++)
		{
		fscanf(fp, "%ld", &no);
		Incid[i] = no-1;
		}
  TraceOff(nome);
}

void T_Surface::Read(vtkMeshData *input)
{
  char buf[100];

	// Atualiza output para poder pegar as células (elementos) corretamente
	input->SetOutputAsSurface();

	// Seta coordenadas dos pontos já lidos e armazenados em um MeshData
	Coords = new acDynCoord3D();	
	Coords->Read(input);
	
  NumNodes = Coords->GetNumNodes();

  Volume.xmax = Volume.xmin = (*Coords)[0].x;
  Volume.ymax = Volume.ymin = (*Coords)[0].y;
  Volume.zmax = Volume.zmin = (*Coords)[0].z;
  long i;
  for (i = 1; i<NumNodes; i++)
		{
		if      ((*Coords)[i].x<Volume.xmin) Volume.xmin=(*Coords)[i].x;
		else if ((*Coords)[i].x>Volume.xmax) Volume.xmax=(*Coords)[i].x;
		if      ((*Coords)[i].y<Volume.ymin) Volume.ymin=(*Coords)[i].y;
		else if ((*Coords)[i].y>Volume.ymax) Volume.ymax=(*Coords)[i].y;
		if      ((*Coords)[i].z<Volume.zmin) Volume.zmin=(*Coords)[i].z;
		else if ((*Coords)[i].z>Volume.zmax) Volume.zmax=(*Coords)[i].z;
		}
	
  int NumGroups = 1, ii;

  GrInfo.NumGroups = NumGroups;
  GrInfo.sw = (int *) mMalloc (NumGroups * sizeof(int));
  GrInfo.lastel = (long *) mMalloc (NumGroups * sizeof(long));
  if (!GrInfo.sw || !GrInfo.lastel)
		{
		GetError(1,buf);
		Error(FATAL_ERROR, 1, buf);
		}

	// SE FOR TRABALHAR COM MAIS DE UM GRUPO INTERNAMENTE, ESSE PARÂMETRO DEVERÁ SER ALTERADO
	// Pq tem apenas um grupo
  this->NumElems = input->GetNumberOfCells();
  GrInfo.lastel[0] = NumElems-1;
	GrInfo.sw[0] = 1;  
	
	Incid = (long huge *) mMalloc(3*NumElems * sizeof(long));
	if (!Incid)
		{ //    1, "Can't Allocate Memory."
		GetError(1, buf);
		Error(FATAL_ERROR, 2, buf);
		}

  ii = 0;	
	int coord[3];
	vtkCell *cell;  
	for(int i = 0; i < this->NumElems; i++)
		{
		cell = input->GetCell(i);
		for(int j = 0; j < 3; j++)
			{
			coord[j] = cell->GetPointId(j);				
			this->Incid[ii++] = coord[j];
			}
		}	
}
