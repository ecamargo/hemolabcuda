/*
** Enzo et al.
*/

/*
** Clase TSurface (superficie de triangulos)
*/

#ifndef TSURFACE_H
#define TSURFACE_H

#include "acdp.h"
#include "aclist.h"
#include "acqueue.h"
#include "acdptype.h"
#include "dyncoo3d.h"

class vtkMeshData;
class vtkParamMesh;

struct VTK_EXPORT TS_GP_NOctree
{
		acPoint3 cg;
		double tc;
		TS_GP_NOctree *ptr[8];
		long no;
		acList *FaceList;
};

struct VTK_EXPORT TS_GroupInfo
    {
    int NumGroups;
    int *sw;
    long *lastel;
    };

class VTK_EXPORT T_Surface
    {
    protected:
	long NumNodes, NumElems;
	acDynCoord3D *Coords;
	long huge *Incid;
	TS_GroupInfo GrInfo;

	double FactorSize;
	double Dfis;	   // Minimum distance from internal surfaces
    int    Flagextnod; // Activa generacion de nodos exteriores

//	acQTree *QTree;		// Quadtree for Ray Test
//	acOTree *OTree;		// Octree for internal surfaces
	acLimit3 Volume;     	// coords. dos maxs. e mins.
	TS_GP_NOctree *GP_Octree;	// raiz da octree

    public:
	T_Surface();
	~T_Surface();
	long GetNumNodes() { return NumNodes; }
	long GetNumElems() { return NumElems; }
	long GetNumGroups() { return GrInfo.NumGroups; }
	long GetLastEl(int i) { return GrInfo.lastel[i]; }
	void SetFactorSize(double newfactor) { FactorSize = newfactor; }
	void SetDfis(double newdfis) { Dfis = newdfis; }
	void SetFlagextnod(int flagextnod) { Flagextnod = flagextnod; }
	void GetInciFace(long face, long *inci);
	int GetFaceSW (long face);
	void CopyCoordVecs (double huge *x, double huge *y, double huge *z);
	void FreeCoords();
	acLimit3 GetVolume() { return Volume; }
	void Read(FILE *fp);
	void Read(vtkMeshData *);	// NEW!!!!!
	void InsertFaceOT(TS_GP_NOctree *branch, long face);
	int Interior(acPoint3 &point);
//	void Interiores(int npoints, acPoint3 *points, int *interior);
	acQueue *GenPoints(vtkParamMesh *mp);
	//int TooNear(acPoint3 &pp, double size);
	void SetCoord(long i, acPoint3 &pp);

    private:
	void CalcWinTrian(acLimit3 &t, long huge *pincid);
	void FreeOctree(TS_GP_NOctree *oct);
        void LoadListSort(TS_GP_NOctree *branch, acList *FaceList, acPoint3 pp);
        int Triangles_Intersect_Cube(acLimit3 SubVol, acNodeList *nl);

	int AddNodeGPOct(long node);
	acQueue *BranchGenPoints(TS_GP_NOctree *branch, vtkParamMesh *mp);
	acQueue *CubeGenPoints
	  (acLimit3 Vol, vtkParamMesh *mp, acNodeList *head);
	acQueue *CubeGenPointsN
	  (acLimit3 Vol, vtkParamMesh *mp, long N, acNodeList *head);
    };

#endif // TSURFACE_H
