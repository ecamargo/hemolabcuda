/*
 * $Id$
 */
#include <vtkObjectFactory.h>
#include "vtkDela3d.h"
#include "vtkParamMesh.h"
#include "vtkMeshData.h"
#include "dmesh3d.h"
#include "clus3d.h"
#include "tsurface.h"
#include "acqueue.h"

vtkCxxRevisionMacro(vtkDela3d, "$Revision:$");
vtkStandardNewMacro(vtkDela3d);

vtkDela3d::vtkDela3d()
{
	this->input  = NULL;
	this->output = NULL;
}

vtkDela3d::~vtkDela3d()
{
	this->input  = NULL;
	this->output = NULL;
}

void vtkDela3d::SetInput(vtkMeshData *in)
{ 
	this->input = in;
}

void vtkDela3d::SetOutput(vtkMeshData *ou)
{ 
	this->output = ou; 
}	
	
int vtkDela3d::o_orderl (long nod, REAL  *x, REAL  *y, REAL  *z, long  *vper, acQueue *lper)
{
  long i, node;
  int  *msw;

  nod -= 8;
  i = 0;
  
  while (lper->Pop(&node) == 1)
  {
	vper[i++] = node;
  }
  
  if (i != nod)
    Error(WARNING, 1, "o_orderl: i: %ld, nod: %ld", i, nod);
    
  msw = (int  *) mMalloc(nod*sizeof(int));
  if (!msw)
  	{ 
  	//Error (FATAL_ERROR, 1, "Ins. memory (o_order)");
  	return 2;
  	}
  	
  for (i = 0; i<nod; i++) msw[i] = 0;
  for (i = 0; i<nod; i++)
  {
    if (vper[i] >= nod || vper[i]<0)
      Error(FATAL_ERROR, 3, "Nodo %ld en la lista", vper[i]);
	if (msw[vper[i]] == 1)
	  Error(WARNING, 2, "Nodo %ld duplicado", vper[i]);
	else
	  msw[vper[i]] = 1;
  }
  mFree(msw);
	
  REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
  if (!ptaux)
  	{
  	//Error (FATAL_ERROR, 1, "Ins. memory (o_order)");
  	return 2;
  	}
  		
  for (i=0; i < nod; i++)
	ptaux[i] = x[vper[i]];
  
  for (i=0; i < nod; i++)
	x[i] = ptaux[i];
	
  for (i=0; i < nod; i++)
	ptaux[i] = y[vper[i]];
  
  for (i=0; i < nod; i++)
	y[i] = ptaux[i];

  for (i=0; i < nod; i++)
	ptaux[i] = z[vper[i]];
  
  for (i=0; i < nod; i++)
	z[i] = ptaux[i];

  for (i=0; i < 8; i++)
	vper[nod+i] = nod+i;

  mFree (ptaux);
  return 0;
}
	
	
int vtkDela3d::r_order(long nod, REAL  *x, REAL  *y, REAL  *z, long  *vper)
{
  long pos1, pos2, aux;
  nod -= 8;
  
  long i;
  for (i = 0; i < nod; i++)
    vper[i] = i;
  
  for (i = 0; i < nod / 2; i++)
  {
	pos1 = rand() % nod;
	pos2 = rand() % nod;
	aux        = vper[pos1];
	vper[pos1] = vper[pos2];
	vper[pos2] = aux;
  }
	
  REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
  if (!ptaux) 
  	{	
  	//Error (FATAL_ERROR, 1, "Ins. memory (r_order)");
  	return 2;
  	}

  for (i=0; i < nod; i++)
    ptaux[i] = x[vper[i]];
  
  for (i=0; i < nod; i++)
	x[i] = ptaux[i];

  for (i=0; i < nod; i++)
	ptaux[i] = y[vper[i]];
  
  for (i=0; i < nod; i++)
	y[i] = ptaux[i];

  for (i=0; i < nod; i++)
	ptaux[i] = z[vper[i]];
  
  for (i=0; i < nod; i++)
	z[i] = ptaux[i];

  for (i=0; i < 8; i++)
	vper[nod+i] = nod+i;

  mFree (ptaux);
  return 0;
}
	

int vtkDela3d::o_order (long nod, REAL  *x, REAL  *y, REAL  *z, long  *vper)
{
  REAL xmmi, xmma, ymmi, ymma, zmmi, zmma, xd, yd, zd;
  xmmi = x[nod-8]; xmma = x[nod-1];
  ymmi = y[nod-8]; ymma = y[nod-1];
  zmmi = z[nod-8]; zmma = z[nod-1];
  xd = xmma - xmmi; yd = ymma - ymmi, zd = zmma - zmmi;
  if (xd <= 0 || yd <= 0 || zd <= 0)
	Error (FATAL_ERROR, 2,
		"Bad dimensions of mesh (o_order) %f x %f, %f y %f, %f z %f",
		xmmi, xmma, ymmi, ymma, zmmi, zmma);

  xd *= 0.3; yd *= 0.3; zd *= 0.3;

  xmmi += xd; xmma -= xd;
  ymmi += yd; ymma -= yd;
  zmmi += zd; zmma -= zd;
  ord_ot *tree = new ord_ot(xmmi,xmma,ymmi,ymma,zmmi,zmma);
  if (!tree) 
  	{
  	//Error (FATAL_ERROR, 1, "Ins. memory (o_order)");
  	return 2;
  	}

  nod -= 8;
  long i;
  for (i = 0; i < nod; i++)
		tree->add (i, x, y, z, 0L);

  long  *ptper = tree->order(0L);

  delete tree;

  REAL  *ptaux = (REAL  *) mMalloc (nod*sizeof(REAL));
  
  if (!ptaux)
  	{
  	//Error (FATAL_ERROR, 1, "Ins. memory (o_order)");
  	return 2;
  	}

  for (i=0; i < nod; i++)
		ptaux[i] = x[ptper[i]];
		
  for (i=0; i < nod; i++)
		x[i] = ptaux[i];

  for (i=0; i < nod; i++)
		ptaux[i] = y[ptper[i]];
		
  for (i=0; i < nod; i++)
		y[i] = ptaux[i];

  for (i=0; i < nod; i++)
		ptaux[i] = z[ptper[i]];
		
  for (i=0; i < nod; i++)
		z[i] = ptaux[i];

  for (i=0; i < nod; i++)
		vper[i] = ptper[i];

  for (i=0; i < 8; i++)
		vper[nod+i] = nod+i;

  mFree (ptaux);
  mFree (ptper);
	return 0;
}

void ReleaseMemory(double *x, double *y, double *z, T_Surface *surf, Dmesh3D * pt_my_mesh, FILE *fcfg)
{
  if(surf) 			 delete surf;
  if(pt_my_mesh) delete pt_my_mesh;	
  if(fcfg)    	 fclose(fcfg);  
  if(x) mFree (x);
  if(y) mFree (y);
  if(z) mFree (z);
}

////////////////////////////////////////////////////////////////////
// Process Filter using manual settings
int vtkDela3d::ProcessFilter(double *bounds, double edgesSize, double sizeElements, double renumbering)
{
  printf("==> STARTING DELA3D\n");
  long nod;
  REAL  *x = NULL,  *y = NULL,  *z = NULL;
  long  *vper;
  acQueue *lper;
  Dmesh3D *pt_my_mesh = NULL;
	FILE *fcfg = NULL;
  int points_generated = 1;

	printf("--> Parameters: Edges Size:         %.2lf\n",edgesSize);
	printf("                Size Elements:      %.2lf\n",sizeElements);
	printf("                Renumbering Method: %.2lf\n",renumbering);
	printf("                Bounds: [%.2lf, %.2lf, %.2lf, %.2lf, %.2lf, %.2lf]\n",
					bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]);	

	// Computing Parameters
  StartACDP(0, "Dela3d.txt", "trace.txt");
	vtkParamMesh *pm = vtkParamMesh::New();
  printf("--> Applying Parameters...\n");
	pm->ComputeParameters(bounds, edgesSize);		
  printf("--> Building Octree...\n");
  pm->BuildOctree();

	// Criando superfície
	printf("--> Creating surface...\n");;  
	T_Surface *surf = new T_Surface;		
	printf("--> Reading surface from input MeshData...\n");
	surf->Read(input);	

	printf ("    %ld nodes, %ld triangles.\n",
	surf->GetNumNodes(), surf->GetNumElems());
 	this->UpdateProgress(0.22);
  
  surf->SetFactorSize(sizeElements);
	surf->SetFlagextnod(0);
  surf->SetDfis(0.3);
	this->UpdateProgress(0.35);	  	

  long pNodes = surf->GetNumNodes();	
  printf ("--> Generating interior nodes...\n");      
	this->UpdateProgress(0.40);		      
  lper = surf->GenPoints(pm);

	if(!lper)
		{ 
		printf("\n");
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
		return 1;    
		}   
		
	this->UpdateProgress(0.45);			
 	int generatedNodes = surf->GetNumNodes() - pNodes;
  printf ("    %ld New Nodes Generated.\n",generatedNodes);
  pm->Delete();	

	if(generatedNodes == 0)
		{ 
		printf("\n");
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
		return 1;    
		}      
    
  nod = surf->GetNumNodes() + 8;
  x = (REAL  *) mMalloc(nod * sizeof(REAL));
  y = (REAL  *) mMalloc(nod * sizeof(REAL));
  z = (REAL  *) mMalloc(nod * sizeof(REAL));
  vper = (long  *) mMalloc(nod * sizeof(long));
  if (!x || !y || !z || !vper)
  	{
    //Error (FATAL_ERROR, 7, "Can't alloc memory");
    return 2;
  	}
    
  surf->CopyCoordVecs(x, y, z);
  surf->FreeCoords();

  printf ("--> Generating triangulation...");
  acLimit3 vv = surf->GetVolume();
// Genera nodos extra
  acPoint3 center, size;
  REAL sizecube;
  center.x = (vv.xmin + vv.xmax) * 0.5;
  center.y = (vv.ymin + vv.ymax) * 0.5;
  center.z = (vv.zmin + vv.zmax) * 0.5;
  size.x = (vv.xmax - vv.xmin) * 1.05;
  size.y = (vv.ymax - vv.ymin) * 1.05;
  size.z = (vv.zmax - vv.zmin) * 1.05;
  sizecube = size.x > size.y ? size.x : size.y;
  sizecube = size.z > sizecube ? size.z : sizecube;
  sizecube *= 1.5;
  x[nod-8] = center.x-sizecube;    x[nod-7] = center.x+sizecube;
  y[nod-8] = center.y-sizecube;    y[nod-7] = center.y-sizecube;
  z[nod-8] = center.z-sizecube;    z[nod-7] = center.z-sizecube;
  x[nod-6] = center.x-sizecube;    x[nod-5] = center.x+sizecube;
  y[nod-6] = center.y+sizecube;    y[nod-5] = center.y+sizecube;
  z[nod-6] = center.z-sizecube;    z[nod-5] = center.z-sizecube;
  x[nod-4] = center.x-sizecube;    x[nod-3] = center.x+sizecube;
  y[nod-4] = center.y-sizecube;    y[nod-3] = center.y-sizecube;
  z[nod-4] = center.z+sizecube;    z[nod-3] = center.z+sizecube;
  x[nod-2] = center.x-sizecube;    x[nod-1] = center.x+sizecube;
  y[nod-2] = center.y+sizecube;    y[nod-1] = center.y+sizecube;
  z[nod-2] = center.z+sizecube;    z[nod-1] = center.z+sizecube;

  // Renumeracion de nodos
  int ren_method = (int) renumbering;

	int retValue;
	if (ren_method == 1) // Optimal
		{
    if (points_generated)
    	{
      retValue = o_orderl (nod, x, y, z, vper, lper);
      if(retValue) return retValue;
    	}
    else
    	{
      retValue = o_order (nod, x, y, z, vper);
	    if(retValue) return retValue;
    	}        
    }
    else if (ren_method == 2) // Random
    	{
      retValue = r_order (nod, x, y, z, vper);
	    if(retValue) return retValue;              
    	}
    else
        for (long i=0; i<nod; i++) vper[i] = i;

  if (points_generated) delete lper;
	this->UpdateProgress(0.50);	
  // Generar malla inicial
  printf ("\n    Creating Delaunay mesh...");
  pt_my_mesh = new Dmesh3D (nod, x, y, z);

  if (!pt_my_mesh)
  	{
    //Error (FATAL_ERROR, 9, "pt_my_mesh");
    return 4;
  	}

	this->UpdateProgress(.55);	
  // Agregar todos los nodos
  printf ("\n    Adding nodes...");
	for (long i = 0; i<nod-8; i++)
			pt_my_mesh->add (i);

  long dummy, min_cs, max_cs, min_ni, max_ni;
  double avg_cs;
  pt_my_mesh->GetStats(dummy, min_cs, max_cs, avg_cs, min_ni, max_ni);
  printf ("\n    Cluster size:"
        "\n       Min: %ld"
        "\n       Max: %ld"
        "\n       Avg: %lf"
        "\n    nel increment:"
        "\n       Min: %ld"
        "\n       Max: %ld",
    min_cs, max_cs, avg_cs, min_ni, max_ni);
	this->UpdateProgress(.58);	
  TimeOn();
  pt_my_mesh->renumber(vper);
  mFree (vper);
  TimeOff("Nodes reordering", 3);

  printf ("\n    Testing lost nodes...");
  TimeOn();
  long lost_nodes = pt_my_mesh->Count_Lost_Nodes();
  int maxiters = 5;
  while (lost_nodes && maxiters)
      {
      printf ("\n     Reinserting %ld nodes.", lost_nodes);
      lost_nodes = pt_my_mesh->Reinsert_Nodes();
      maxiters--;
      }
  TimeOff("Lost Nodes reinserting", 3);
  if (lost_nodes)
      Error(WARNING, 10, "%ld lost nodes remain");

  printf ("\n    Testing mesh...");
  // Test de la malla
  TimeOn();

	if(pt_my_mesh->is_ok(1,1,1) == -1)
		{ 
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
	  return 1;    
		}
  TimeOff("Mesh testing", 3);

	this->UpdateProgress(.65);	
  fprintf (stdout, "\n--> Recovering boundary triangles...\n");
  long FnotR, CwIter, C_E_F, C_E_E;
  printf ("    %ld edges, %ld faces to recover.\n",
  surf->GetNumElems()*3/2, surf->GetNumElems());
  printf ("    First sweep...");
  TimeOn();
  FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
//  if(FnotR == -1)
//  	return 5;
  TimeOff("Boundary recovering (first sweep)", 3);
  printf ("\n    Cases with more than 1 iteration: %ld.", CwIter);
  printf ("\n    Cases Edge-Face: %ld, Cases Edge-Edge: %ld.", C_E_F, C_E_E);
	this->UpdateProgress(.70);  
  if (FnotR > 0)
  {
    printf ("    Can't recover %ld faces.", FnotR);
    printf ("\n    Second sweep...");
    TimeOn();
    FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
//	  if(FnotR == -1)
//  		return 5;    
    TimeOff("Boundary recovering (second sweep)", 3);
    printf ("\n    Cases with more than 1 iteration: %ld.",
        CwIter);
    printf (
        "\n    Cases Edge-Face: %ld, Cases Edge-Edge: %ld.",
        C_E_F, C_E_E);
  }
  if (FnotR > 0)
  {
    printf ("\nCan't recover %ld faces\n", FnotR);
    printf ("\n   Testing mesh...");
    TimeOn();
  
	  if(pt_my_mesh->is_ok(1,1,1) == -1)
		  { 
			ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
	  	return 1;    
		  }
    
    printf("\n\n  >> Error: %ld faces not recovered. Can't continue. << \n", FnotR);
    return 1;    
  }
  printf ("\n    Testing mesh...");
  TimeOn();
  
	if(pt_my_mesh->is_ok(1,1,1) == -1)
	{ 
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
  		return 1;    
  	}

	TimeOff("Mesh testing", 3);
	printf ("\n--> Boundary recovering ok!\n");

	printf ("--> Deleting external tetrahedra...\n");
	long rtet = pt_my_mesh->Delext(surf);

	if(rtet == -1)
		{
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  		return 7;    
		}
  
	pt_my_mesh->DelFreeNodes();
	printf ("    %ld remaining tetrahedra.\n", rtet);
	printf ("    Testing mesh...");

	// Esse teste dá erro se a superfície vier com as normais para fora
	// ao invés de para dentro (esperado)
	if(pt_my_mesh->is_ok(1,0,1) == -1)
	{
	ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  	return 1;    
	}

	printf("\n--> Updating \'output\' MeshData...");
	pt_my_mesh->print(output);
	printf("done!\n");		
 	this->UpdateProgress(.90);	
	printf ("--> Checking Volume Consistency...\n");
	if( !pt_my_mesh->CheckVolumeConsistency() )
		{
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  		return 8; // Inconsistent mesh    
		}

	ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
	TraceOff("main");		
  
 	printf("==> ENDING DELA3D\n\n\n");    
  	return 0;
}

// Original Method (uses configuration file)
int vtkDela3d::ProcessFilter(char *filecfg)
{
  printf("            DELA3D - GEMESIS3D - HEMOLAB\n");
  printf("               Create Delaunay 3D Mesh\n\n");	
  long nod;
  REAL  *x = NULL,  *y = NULL,  *z = NULL;
  long  *vper;
  acQueue *lper;
  Dmesh3D *pt_my_mesh = NULL;
  vtkParamMesh *pm = NULL;
  T_Surface *surf = NULL;
  char filename[256];
  char buf[256];
  FILE *fcfg = NULL, *fp;	

  StartACDP(0, "Dela3d.txt", "trace.txt");
  TraceOn("main");
	cout << "File: " << filecfg << endl;
  fcfg = fopen(filecfg, "r");
  
  if (!fcfg) Error (FATAL_ERROR, 1, "Can't open configuration file");

  /*
   *	Comienza parche para generacion de mallas de parametros
   */
  char *kpp = "PARAMETERS_POINTS";
  if (FindKeyWord(kpp, fcfg))
  {
    fscanf(fcfg,"%s",filename);
    std::cout << "\n\nPoints with parameters from file:" << filename <<  endl;
    std::cout << "\n   Reading points..." << endl;
    TimeOn();
    fp = fopen(filename, "r");
    if (!fp) Error(FATAL_ERROR, 2, "Can't open points file");
    fscanf(fp, "%ld", &nod);
    nod += 8;
    x = (REAL  *) mMalloc(nod * sizeof(REAL));
    y = (REAL  *) mMalloc(nod * sizeof(REAL));
    z = (REAL  *) mMalloc(nod * sizeof(REAL));
    REAL  *par = (REAL  *) mMalloc(nod * sizeof(REAL));
    REAL paravg = 0;
    vper = (long  *) mMalloc(nod * sizeof(long));
    if (!x || !y || !z || !vper) Error (FATAL_ERROR, 7, "Can't alloc memory");
    acLimit3 vv;
    REAL xx, yy, zz;
    
    long j;
    for (j = 0; j < nod-8; j++)
    {
      fscanf(fp, " %lf %lf %lf %lf", &xx, &yy, &zz, par+j);
      x[j] = xx; y[j] = yy; z[j] = zz;
      if (j == 0)
      {
        vv.xmin = vv.xmax = xx;
        vv.ymin = vv.ymax = yy;
        vv.zmin = vv.zmax = zz;
      }
      else
      {
        if      (xx < vv.xmin) vv.xmin = xx;
        else if (xx > vv.xmax) vv.xmax = xx;
        if      (yy < vv.ymin) vv.ymin = yy;
        else if (yy > vv.ymax) vv.ymax = yy;
        if      (zz < vv.zmin) vv.zmin = zz;
        else if (zz > vv.zmax) vv.zmax = zz;
      }
     
      paravg += par[j];
    }
     
    paravg /= nod-8;
    par[j++]=paravg; par[j++]=paravg; par[j++]=paravg; par[j++]=paravg;
    par[j++]=paravg; par[j++]=paravg; par[j++]=paravg; par[j++]=paravg;

    //Genera nodos extra
    acPoint3 center, size;
    REAL sizecube;
    center.x = (vv.xmin + vv.xmax) * 0.5;
    center.y = (vv.ymin + vv.ymax) * 0.5;
    center.z = (vv.zmin + vv.zmax) * 0.5;
    size.x = (vv.xmax - vv.xmin) * 1.05;
    size.y = (vv.ymax - vv.ymin) * 1.05;
    size.z = (vv.zmax - vv.zmin) * 1.05;
    sizecube = size.x > size.y ? size.x : size.y;
    sizecube = size.z > sizecube ? size.z : sizecube;
    sizecube *= 1.5;
    x[nod-8] = center.x-sizecube;    x[nod-7] = center.x+sizecube;
    y[nod-8] = center.y-sizecube;    y[nod-7] = center.y-sizecube;
    z[nod-8] = center.z-sizecube;    z[nod-7] = center.z-sizecube;
    x[nod-6] = center.x-sizecube;    x[nod-5] = center.x+sizecube;
    y[nod-6] = center.y+sizecube;    y[nod-5] = center.y+sizecube;
    z[nod-6] = center.z-sizecube;    z[nod-5] = center.z-sizecube;
    x[nod-4] = center.x-sizecube;    x[nod-3] = center.x+sizecube;
    y[nod-4] = center.y-sizecube;    y[nod-3] = center.y-sizecube;
    z[nod-4] = center.z+sizecube;    z[nod-3] = center.z+sizecube;
    x[nod-2] = center.x-sizecube;    x[nod-1] = center.x+sizecube;
    y[nod-2] = center.y+sizecube;    y[nod-1] = center.y+sizecube;
    z[nod-2] = center.z+sizecube;    z[nod-1] = center.z+sizecube;

    printf ("\n   ...ready.");
    TimeOff("Parameters points reading", 3);
    fclose(fp);

    printf ("\n\nGenerating triangulation...");
    //Renumeracion de nodos
    int ren_method;
    if (!FindKeyWord("RENUMBERING_METHOD", fcfg))
        ren_method = 1;
    else
        fscanf(fcfg,"%d",&ren_method);

    TimeOn();
    if (ren_method == 1) // Optimal
        o_order (nod, x, y, z, vper);
    else if (ren_method == 2) // Random
        r_order (nod, x, y, z, vper);
    else
        for (long i=0; i<nod; i++) vper[i] = i;

    TimeOff("Nodes reordering", 3);

    //Generar malla inicial
    printf ("\n   Creating Delaunay mesh...");
    pt_my_mesh = new Dmesh3D (nod, x, y, z);
    if (!pt_my_mesh) Error (FATAL_ERROR, 9, "pt_my_mesh");

    //Agregar todos los nodos
    printf ("\n   Adding nodes...");
    TimeOn();
    for (long i = 0; i<nod-8; i++)
        pt_my_mesh->add (i);
    TimeOff("Bowier-Watson algorithm", 3);

    TimeOn();
    pt_my_mesh->renumber(vper);
    mFree (vper);
    TimeOff("Nodes reordering", 3);

    printf ("\n   Testing lost nodes...");
    TimeOn();
    long lost_nodes = pt_my_mesh->Count_Lost_Nodes();
    int maxiters = 5;
    while (lost_nodes && maxiters)
        {
        printf ("\n   Reinserting %ld nodes.", lost_nodes);
        lost_nodes = pt_my_mesh->Reinsert_Nodes();
        maxiters--;
        }
    TimeOff("Lost Nodes reinserting", 3);
    if (lost_nodes)
        Error(WARNING, 10, "%ld lost nodes remain");

    if (FindKeyWord("OUTPUT_PARAMETERS_MESH", fcfg))
        fscanf(fcfg,"%s",filename);
    else
        strcpy(filename, "paramesh.spi");

    TimeOn();
    pt_my_mesh->print(filename, par);
    TimeOff("Mesh writing", 3);
    printf ("\n...ready parameters mesh.");
    printf ("\n\nSaved mesh in %s.\n\n", filename);
    fclose(fcfg);

    mFree (x);
    mFree (y);
    mFree (z);
    mFree (par);
    delete pt_my_mesh;
    TraceOff("main");
//    ExitProg();
   }

/*
 *	Fin parche para generacion de mallas de parametros
 */
   int mesh_read;
   char *kdm = "DELAUNAY_MESH";
   if (FindKeyWord(kdm, fcfg))
   {
	  fscanf(fcfg,"%s",filename);
	  printf("\n\nDelaunay mesh from file: %s", filename);
	  printf ("\n   Reading Dmesh3D...");
	  TimeOn();
	  fp = fopen(filename, "r");
	  if (!fp) Error(FATAL_ERROR, 2, "Can't open Delaunay mesh file");
	  pt_my_mesh = new Dmesh3D(fp);
	  fclose(fp);
	  TimeOff("Delaunay mesh reading", 3);
	  printf ("\n...ready");
	  mesh_read = 1;
	}

   else
   {
     pt_my_mesh = NULL;
     mesh_read = 0;
   }

   if (!mesh_read)
   {
     char *kpm = "PARAMETERS_MESH";
     if (!FindKeyWord(kpm, fcfg))
     {
       GetError(27, buf);
       Error(FATAL_ERROR, 2, buf, kpm);
     }
  
     fscanf(fcfg,"%s",filename);
		 
     printf("\n\nParameters mesh from file: %s", filename);
     fp = fopen(filename, "r");
//     if (!fp) Error (FATAL_ERROR, 3, "Can't open parameters mesh file");
		 if(!fp)
		 	 {
		   cout << "\n\nError (vtkDela3d.cxx): Can't open parameters mesh file" << endl;		 	
		   cout << "Check file \"" << filecfg << "\" and check .spi file path." << endl;	
		   return 2;		   
		 	 }
     printf ("\n   Creating ParamMesh...");
		 pm = vtkParamMesh::New();
     printf ("\n   Reading ParamMesh...\n");
     TimeOn();
     pm->Read(fp);
     fclose(fp);
     TimeOff("ParamMesh reading", 3);
     printf ("\n   Building Octree...");
     TimeOn();
     pm->BuildOctree();
     TimeOff("ParamMesh Octree building", 3);
     printf ("\n   ...ready ParamMesh.");
   }

  // Verifica se alguma superfície de input foi setado anteriomente
  // Caso não, lê superfície do arquivo CFG.
	printf ("\n   Creating surface...");  
	surf = new T_Surface;		

	if(input)
		{
		printf ("\n   Reading surface from input MeshData...");
		surf->Read(input);	
		}
	else
		{
		char *ksur = "SURFACE_MESH";
		if (!FindKeyWord(ksur, fcfg))
			{
		  GetError(27, buf);
		  Error(FATAL_ERROR, 4, buf, ksur);
			}
		
		fscanf(fcfg,"%s",filename);
		printf("\n\nSurface mesh from file: %s\n", filename);
		fp = fopen(filename, "r");
		if (!fp) Error (FATAL_ERROR, 5, "Can't open surface mesh file");
		printf ("\n   Reading surface...");
		TimeOn();
		surf->Read(fp);
		TimeOff("Surface reading", 3);
		fclose(fp);
		}

	printf ("\n   %ld nodes, %ld triangles.",
	surf->GetNumNodes(), surf->GetNumElems());		
  printf ("\n   ...ready Surface.");		
    
 	this->UpdateProgress(0.22);
  
  if (!pt_my_mesh)
  {
		int points_generated;
    char *kint = "INTERIOR_POINTS_FILE";
    if (FindKeyWord(kint, fcfg))
    {
      points_generated = 0;
      fscanf(fcfg,"%s",filename);
      printf("\n\nInterior nodes from file: %s", filename);
      fp = fopen(filename, "r");
      if (!fp) Error (FATAL_ERROR, 7, "Can't open nodes file");
      printf ("\n\nReading interior nodes...");
      TimeOn();
      if (!FindKeyWord("COORDINATES", fp))
      {
        GetError(27, buf);
        Error(FATAL_ERROR, 8, buf, "COORDINATES");
      }
      long i, idum, numnodes;
      acPoint3 pp;
      fscanf(fp, "%ld", &numnodes);
      for (i = 0; i<numnodes; i++)
      {
        fscanf(fp, "%ld %lf %lf %lf",&idum, &pp.x,&pp.y,&pp.z);
        surf->SetCoord(i, pp);
      }
      TimeOff("Interior nodes reading", 3);
      fclose(fp);
    }
    
    else
    {
		 	this->UpdateProgress(0.30);		  
      points_generated = 1;
      if (FindKeyWord("SIZE_ELEMENTS", fcfg))
      {
        REAL newfactor;
        fscanf(fcfg, "%lf", &newfactor);
        surf->SetFactorSize(newfactor);
      }
      int flagextnod=0;
      if (FindKeyWord("GENERATE_EXTERIOR_NODES", fcfg))
        fscanf(fcfg, "%d", &flagextnod);
      surf->SetFlagextnod(flagextnod);
		 	this->UpdateProgress(0.35);	  	
      if (FindKeyWord("DISTANCE_FROM_INTERIOR_SURFACES", fcfg))
      {
        REAL newdfis;
        fscanf(fcfg, "%lf", &newdfis);
        surf->SetDfis(newdfis);
      }
      else
        surf->SetDfis(0.3);


      printf ("\n\nGenerating interior nodes...\n");      
      long pNodes = surf->GetNumNodes();	
		 	this->UpdateProgress(0.40);		      
      lper = surf->GenPoints(pm);
		 	this->UpdateProgress(0.45);			
		 	int generatedNodes = surf->GetNumNodes() - pNodes;
      printf ("\n   %ld New Nodes Generated",generatedNodes);
      if(generatedNodes == 0)
			  { 
				ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
		  	return 1;    
			  }      
    }
    
	printf ("\n   ...ready interior nodes.");
	pm->Delete();

	nod = surf->GetNumNodes() + 8;
	x = (REAL  *) mMalloc(nod * sizeof(REAL));
	y = (REAL  *) mMalloc(nod * sizeof(REAL));
	z = (REAL  *) mMalloc(nod * sizeof(REAL));
	vper = (long  *) mMalloc(nod * sizeof(long));
	if (!x || !y || !z || !vper)
		{
	  	//Error (FATAL_ERROR, 7, "Can't alloc memory");
	  	return 2;
		}
	surf->CopyCoordVecs(x, y, z);
	surf->FreeCoords();
	if (FindKeyWord("OUTPUT_POINTS", fcfg))
	 fscanf(fcfg,"%s",filename);
	else
	    strcpy(filename, "points.dat");
	fp = fopen(filename, "w");
	if (!fp) Error (FATAL_ERROR, 8, "Can't open file for points");
	fprintf (fp, "*COORDINATES\n %ld\n", surf->GetNumNodes());
	for (long j = 0; j < surf->GetNumNodes(); j++)
	    fprintf (fp, " %ld %lf %lf %lf\n", j+1, x[j], y[j], z[j]);
	fprintf (fp, "*END\n");
	fclose (fp);
	printf ("\n\nSaved points in %s.", filename);

    printf ("\n\nGenerating triangulation...");
    acLimit3 vv = surf->GetVolume();
// Genera nodos extra
    acPoint3 center, size;
    REAL sizecube;
    center.x = (vv.xmin + vv.xmax) * 0.5;
    center.y = (vv.ymin + vv.ymax) * 0.5;
    center.z = (vv.zmin + vv.zmax) * 0.5;
    size.x = (vv.xmax - vv.xmin) * 1.05;
    size.y = (vv.ymax - vv.ymin) * 1.05;
    size.z = (vv.zmax - vv.zmin) * 1.05;
    sizecube = size.x > size.y ? size.x : size.y;
    sizecube = size.z > sizecube ? size.z : sizecube;
    sizecube *= 1.5;
    x[nod-8] = center.x-sizecube;    x[nod-7] = center.x+sizecube;
    y[nod-8] = center.y-sizecube;    y[nod-7] = center.y-sizecube;
    z[nod-8] = center.z-sizecube;    z[nod-7] = center.z-sizecube;
    x[nod-6] = center.x-sizecube;    x[nod-5] = center.x+sizecube;
    y[nod-6] = center.y+sizecube;    y[nod-5] = center.y+sizecube;
    z[nod-6] = center.z-sizecube;    z[nod-5] = center.z-sizecube;
    x[nod-4] = center.x-sizecube;    x[nod-3] = center.x+sizecube;
    y[nod-4] = center.y-sizecube;    y[nod-3] = center.y-sizecube;
    z[nod-4] = center.z+sizecube;    z[nod-3] = center.z+sizecube;
    x[nod-2] = center.x-sizecube;    x[nod-1] = center.x+sizecube;
    y[nod-2] = center.y+sizecube;    y[nod-1] = center.y+sizecube;
    z[nod-2] = center.z+sizecube;    z[nod-1] = center.z+sizecube;

    // Renumeracion de nodos
    int ren_method;
    if (!FindKeyWord("RENUMBERING_METHOD", fcfg))
        ren_method = 1;
    else
        fscanf(fcfg,"%d",&ren_method);

   TimeOn();
  	int retValue;
  	if (ren_method == 1) // Optimal
  		{
      if (points_generated)
      	{
        	retValue = o_orderl (nod, x, y, z, vper, lper);
        	if(retValue) return retValue;
      	}
      else
      	{
        	retValue = o_order (nod, x, y, z, vper);
  	    	if(retValue) return retValue;
      	}        
      }
      else if (ren_method == 2) // Random
      	{
        	retValue = r_order (nod, x, y, z, vper);
  	    	if(retValue) return retValue;              
      	}
      else
          for (long i=0; i<nod; i++) vper[i] = i;

    if (points_generated) delete lper;
    TimeOff("Nodes reordering", 3);

	 	this->UpdateProgress(0.50);	
    // Generar malla inicial
    printf ("\n   Creating Delaunay mesh...");
    pt_my_mesh = new Dmesh3D (nod, x, y, z);

    if (!pt_my_mesh)
    	{
      //Error (FATAL_ERROR, 9, "pt_my_mesh");
      return 4;
    	}

	 	this->UpdateProgress(.55);	
    // Agregar todos los nodos
    printf ("\n   Adding nodes...");
    TimeOn();
    for (long i = 0; i<nod-8; i++)
        pt_my_mesh->add (i);
    TimeOff("Bowier-Watson algorithm", 3);

    long dummy, min_cs, max_cs, min_ni, max_ni;
    double avg_cs;
    pt_my_mesh->GetStats(dummy, min_cs, max_cs, avg_cs, min_ni, max_ni);
    printf ("\n   Cluster size:"
         "\n      Min: %ld"
         "\n      Max: %ld"
         "\n      Avg: %lf"
         "\n   nel increment:"
         "\n      Min: %ld"
         "\n      Max: %ld",
     min_cs, max_cs, avg_cs, min_ni, max_ni);
	 	this->UpdateProgress(.58);	
    TimeOn();
    pt_my_mesh->renumber(vper);
    mFree (vper);
    TimeOff("Nodes reordering", 3);

    printf ("\n   Testing lost nodes...");
    TimeOn();
    long lost_nodes = pt_my_mesh->Count_Lost_Nodes();
    int maxiters = 5;
    	while (lost_nodes && maxiters)
        {
        printf ("\n   Reinserting %ld nodes.", lost_nodes);
        lost_nodes = pt_my_mesh->Reinsert_Nodes();
        maxiters--;
        }
    	TimeOff("Lost Nodes reinserting", 3);
    	if (lost_nodes)
        Error(WARNING, 10, "%ld lost nodes remain");

    	printf ("\n   Testing mesh...");
    	// Test de la malla
    	TimeOn();
 
	  	if(pt_my_mesh->is_ok(1,1,1) == -1)
			{ 
			ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
	  		return 1;    
		  	}
    	TimeOff("Mesh testing", 3);

    	if (FindKeyWord("OUTPUT_CUBE_MESH", fcfg))
        	fscanf(fcfg,"%s",filename);
    	else
      	strcpy(filename, "cubemesh.vwm");

    	TimeOn();
    	pt_my_mesh->print(filename);
    	TimeOff("Mesh writing", 3);
    	printf ("\n...ready Delaunay mesh.");
    	printf ("\n\nSaved mesh in %s.", filename);
		}
	this->UpdateProgress(.65);	
  	fprintf (stdout, "\n\nRecovering boundary triangles...");
  	long FnotR, CwIter, C_E_F, C_E_E;
  	printf ("\n   %ld edges, %ld faces to recover.",
  	surf->GetNumElems()*3/2, surf->GetNumElems());
  	printf ("\n   First sweep...");
  	TimeOn();
  	FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
  	// Test removed in 29/04/2008  
  	// if(FnotR == -1)
	//  	return 5;  
  TimeOff("Boundary recovering (first sweep)\n", 3);
  printf ("   Cases with more than 1 iteration: %ld.\n", CwIter);
  printf ("   Cases Edge-Face: %ld, Cases Edge-Edge: %ld.\n", C_E_F, C_E_E);
	this->UpdateProgress(.70);  
  if (FnotR > 0)
  {
    printf ("   Can't recover %ld faces.", FnotR);
    printf ("\n   Second sweep...");
    TimeOn();
    FnotR = pt_my_mesh->B_Recovery(surf, CwIter, C_E_F, C_E_E);
//	  if(FnotR == -1)
//  		return 5;     
    TimeOff("Boundary recovering (second sweep)", 3);
    printf ("\n   Cases with more than 1 iteration: %ld.",
        CwIter);
    printf (
        "\n   Cases Edge-Face: %ld, Cases Edge-Edge: %ld.",
        C_E_F, C_E_E);
  }
  if (FnotR > 0)
  {
    printf ("\nCan't recover %ld faces\n", FnotR);
    printf ("\n   Testing mesh...");
    TimeOn();
  
	  if(pt_my_mesh->is_ok(1,1,1) == -1)
		  { 
			ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
	  	return 1;    
		  }

    TimeOff("Mesh testing", 3);

    if (FindKeyWord("OUTPUT_CUBE_MESH", fcfg))
        fscanf(fcfg,"%s",filename);
    else
        strcpy(filename, "cubemesh.vwm");
    TimeOn();
    pt_my_mesh->print(filename);
    TimeOff("Mesh writing", 3);
    printf ("\n...ready boundary recovering.");
    printf ("\n\nSaved mesh in %s.", filename);
    Error(FATAL_ERROR, 10, "%ld faces not recovered", FnotR);
  	}
  	printf ("\n   Testing mesh...");
  	TimeOn();
  
	if(pt_my_mesh->is_ok(1,1,1) == -1)
	{ 
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
  		return 1;    
  	}

  	TimeOff("Mesh testing", 3);
  	printf ("\n...ready boundary recovering.");
	
	printf ("\n\nDeleting external tetrahedra...");
  	long rtet = pt_my_mesh->Delext(surf);

	if(rtet == -1)
	{
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  		return 7;    
	}
  
  	pt_my_mesh->DelFreeNodes();

  	printf ("\n...Ready.");
  	printf ("\n   %ld remaining tetrahedra.", rtet);
  	printf ("\n   Testing mesh...");

	// Esse teste dá erro se a superfície vier com as normais para fora
	// ao invés de para dentro (esperado)
	if(pt_my_mesh->is_ok(1,0,1) == -1)
	{
	ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  	return 1;    
	}

  	if (FindKeyWord("OUTPUT_MESH", fcfg))
  	fscanf(fcfg,"%s",filename);
  	else
  	strcpy(filename, "d3dmesh.vwm");

  	printf ("\n...ready deleting external tetrahedra.");
  	if(!this->output) // print result in file 
  	{
		pt_my_mesh->print(filename);
		printf ("\n\nSaved mesh in %s.\n\n", filename);	  
  	}
	else				// Fill output vtkUnstructuredGrid
	{
		printf("\nUpdating \'output\' MeshData...");
		pt_my_mesh->print(output);
		printf("done\n");		
	}
 	this->UpdateProgress(.90);	

  	if( !pt_my_mesh->CheckVolumeConsistency() )
		{
		ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);		
  		return 8; // Inconsistent mesh    
		}	

	ReleaseMemory(x,y,z, surf, pt_my_mesh, fcfg);
  	TraceOff("main");    
  	return 0;
}
