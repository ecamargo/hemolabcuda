/*
 * $Id$
 */
 
#ifndef _vtkDela3d_h_
#define _vtkDela3d_h_

#ifndef REAL
#define REAL double
#endif 

#define ID_FILE_OPEN 5000

#include <vtkObjectFactory.h>
#include <vtkAlgorithm.h>
#include <vtkMeshData.h>
#include "dmesh3d.h"
#include "clus3d.h"
#include "tsurface.h"
#include "acqueue.h"

class vtkKWProgressGauge;	

class VTK_EXPORT vtkDela3d: public vtkAlgorithm
{
  //Metodos.
  public:
  vtkTypeRevisionMacro(vtkDela3d,vtkAlgorithm);
  static vtkDela3d *New();
  void PrintSelf(ostream& os, vtkIndent indent){};

  vtkDela3d();
	~vtkDela3d();
  
	int o_order (long , REAL  *, REAL  *, REAL  *, long  *vper);
	int o_orderl(long , REAL  *, REAL  *, REAL  *, long  *vper, acQueue *lper);
	int r_order (long , REAL  *, REAL  *, REAL  *, long  *vper);

	void SetInput (vtkMeshData *in);
	void SetOutput(vtkMeshData *ou);

	// Description:
	// Process Filter using configuration File
	int ProcessFilter(char *);

	// Description:
	// Process Filter using manual settings
	int ProcessFilter(double*, double, double, double);

//
//  // Description:
//  // Set the Pointer to the main Progress Bar.
//	void SetProgressBar(vtkKWProgressGauge *p);

	protected:
		vtkMeshData *input;
		vtkMeshData *output;

	  // Description:
	  // Pointer to the main Progress Bar is passed to allow this filter to update it.
	  vtkKWProgressGauge* progress;  

  //Atributos.
  private:
//	long nod, *vper;
//	char *filename, *buf;
//	FILE *fcfg, *fp;
//	REAL  *x,  *y,  *z;
//	acQueue *lper;
//	Dmesh3D *pt_my_mesh;
//	ParamMesh *pm;
//	T_Surface *surf;
};
#endif /*_vtkDela3d_h_*/


