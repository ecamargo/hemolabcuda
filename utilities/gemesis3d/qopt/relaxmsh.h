#include "vscoo3d.h"
#include "vtkMesh3d.h"
#include "vtkUnstructuredGrid.h"
#include "vtkObjectFactory.h"
/*
Convencion:	n4
    		  /|\
	       / | \
	      /  |<-\---v2
	     /   |   \
	  n1/....|....\n3
	    \ v3 | v1 /
	     \   |   /
	      \  |<-/---v4
	       \ | /
	    	  \|/
		       n2
*/

class VTK_EXPORT TetElem
    {
    public:
       long n1,n2,n3,n4;
       TetElem *v1,*v2,*v3,*v4;
       int fgc,fgm;
       int edges;
       TetElem *prev,*next;
    public:
        TetElem()
            {
            fgc=0; fgm=0; edges=0;
            }
    };

class VTK_EXPORT FacElem
    {
    public:
        TetElem *el;
        long n1,n2,n3;
        FacElem *next;

    public:
        FacElem()
            {
            next= NULL;
            }

        FacElem(long N1, long N2, long N3)
            {
            n1 = N1;
            n2 = N2;
            n3 = N3;
            next= NULL;
            el = NULL;
            }
    };

class VTK_EXPORT NodElem
    {
    public:
        long n;
        NodElem *next;

        NodElem(){next = NULL;}
    };

class VTK_EXPORT Edge
    {
    public:
        long n1,n2;
    };

class VTK_EXPORT MQuality
    {
    public:
        double avol,vol,qlty;
        long num0;
    };

class VTK_EXPORT RelaxMesh
    {
    protected:
        TetElem     *firste,*curre;
        FacElem     *firstf,*currf;
        FacElem     *nfirst;
        acVSCoord3D *DCoord;
        long NumMaxCluster;
        int AddNodesFlag;
        int AntiElemFlag;
        int QualityFlag;
        int SeeAllFlag;

        long node,node1,node2;
        long NumFaces;
        int edges,faces;
        MQuality Qmin;

    public:
        RelaxMesh()
            {
            firste=curre=NULL;
            firstf=currf=NULL;
            DCoord=NULL;
            NumMaxCluster=200;
            AddNodesFlag = 1;
            AntiElemFlag = 0;
			SeeAllFlag   = 1;
			QualityFlag  = 1;
			}
        RelaxMesh(vtkMesh3d *mesh);
        void RelaxMsh( vtkMesh3d *mesh);
        ~RelaxMesh();
//	void Read(FILE *fp);
        void Print(FILE *fp);
        
        // Description:
        // Print RelaxMesh into a vtkUnstructuredGrid object
        void Print(vtkUnstructuredGrid *output);
        
        long AnalizeAllNodes();
        long AnalizeAllEdges();
        long AnalizeAllFaces();
        void SetNumMaxCluster(long num) {NumMaxCluster=num;}
        void SetAddNodesFlag (int flag) {AddNodesFlag=flag;}
        void SetAntiElemFlag (int flag) {AntiElemFlag=flag;}
		void SetQualityFlag  (int flag) {QualityFlag=flag;}
		void SetSeeAllFlag   (int flag) {SeeAllFlag=flag;}
		void TestStruct();
        long RemoveNullElements();

    protected:
        int AnalizeNode();
        int AnalizeEdge();
        int AnalizeFace(TetElem * ev);
        int AnalizeConfig();
        acPoint2 Quality(acPoint3 &p1,acPoint3 &p2,acPoint3 &p3,acPoint3 &p4);
        void CalcNoEl(TetElem *el);
        void CalcEdEl(TetElem *el);
        void RemoveFreeNodes();
        void GenerateNewElements(long nodom);
        int  TestSoft(long nodom);
        int  TestHard(long nodom);
        int  TestLHard(long nodom);
        int  TestSHard(long nodom);
        void FindVecs(TetElem *el, TetElem *elf);
        NodElem *MakeNodeList();
        void DeleteElem(TetElem *e1);
        void DeleteList(FacElem *first);
        void DeleteAntiElems(TetElem *e1,TetElem *e2,TetElem *elf);
        MQuality CalcMinQuality();
        FacElem *FindFace(FacElem *fa,FacElem *fi);
        void Marca(TetElem *el);
    };


