#----------------------------------------------------------------
SET(APP_NAME1 testQopt)

ADD_EXECUTABLE(${APP_NAME1} qopt.cpp) 

TARGET_LINK_LIBRARIES(${APP_NAME1} 
	vtkCommon
	gemesis3dcommon	
	baseoop
	qopt	
)

