/*
 * $Id$
 */
#include <string.h>
#include <vtkObjectFactory.h>

#include "vtkAlgorithm.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMeshData.h"

#include "vtkQopt.h"
#include "vtkMesh3d.h"
#include "acdp.h"
#include "relaxmsh.h"

vtkCxxRevisionMacro(vtkQopt, "$Revision:$");
vtkStandardNewMacro(vtkQopt);

vtkQopt::vtkQopt()
{
	this->input  = NULL;
	this->output = NULL;
}

vtkQopt::~vtkQopt()
{
}

void vtkQopt::SetInput(vtkMeshData *in)
{ 
	this->input = in;
}

void vtkQopt::SetOutput(vtkMeshData *ou)
{ 
	this->output = ou; 
}

void vtkQopt::ProcessFilter(char *filename)
{
    StartACDP(0, "Qopt.msg", "trace.txt");

    RelaxMesh *rmesh;
    FILE *fcfg, *fp;

    TraceOn("main");

    printf("                       QOPT - GEMESIS3D - HEMOLAB\n");
    printf("            Optimize element's quality on a tetraedral mesh\n\n");
    
    fcfg = fopen(filename, "r");
    if (!fcfg) Error (FATAL_ERROR, 1, "Can't open configuration file");

// LECTURA DE LA MALLA
		vtkMesh3d *mesh = vtkMesh3d::New();
		this->UpdateProgress(0.20);
		if(this->input)
			{
			cout << " Atualizando mesh a partir do input (vtkMeshData)" << endl;
			mesh->UpdateVolume((vtkUnstructuredGrid*) this->input);
			}
		else
			{
		    TimeOn();
		    char *kwd = "INPUT_MESH";
		    if (FindKeyWord(kwd, fcfg))
		        {
		        fscanf(fcfg,"%s",filename);
		        printf("\n\nInput mesh from file: %s\n", filename);
		        fp = fopen(filename, "r");
		        if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
		        }
		    else
		        Error(FATAL_ERROR, 2, "Can't find INPUT_MESH keyword");
				printf ("\n   Reading surface from input File...");				
		    mesh->Read(fp);				
	  	  fclose(fp);  
			}
    
//  CALCULO DE ESTRUCTURAS AUXILIARES
    printf ("\nComputing auxiliary structures...\n");
    TimeOn();
    rmesh = new RelaxMesh(mesh);
		this->UpdateProgress(0.30);

    printf ("\nDeleting auxiliary Mesh...\n");
    //delete mesh;
    mesh->Delete();
    
    printf ("\nTesting input mesh...\n");
    rmesh->TestStruct();
    TimeOff("Auxiliary structures", 3);
		this->UpdateProgress(0.35);
		
//  LECTURA DE PARAMETROS DEL ARCHIVO DE CONFIGURACION
    long nczmax;
    char *kwd3 = "MAX_CLUSTER_SIZE";
    if (FindKeyWord(kwd3, fcfg))
        fscanf(fcfg,"%ld",&nczmax);
    else
        nczmax = 200;
    rmesh->SetNumMaxCluster(nczmax);

    int flag;
    char *kwd4 = "ADD_NODES_FLAG";
    if (FindKeyWord(kwd4, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 1;
    rmesh->SetAddNodesFlag (flag);

    char *kwd7 = "QUALITY_FLAG";
    if (FindKeyWord(kwd7, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 0;
    rmesh->SetQualityFlag (flag);

	char *kwd8 = "SEE_ALL_FLAG";
	if (FindKeyWord(kwd8, fcfg))
		fscanf(fcfg,"%d",&flag);
	else
		flag = 1;
	rmesh->SetSeeAllFlag (flag);

	this->UpdateProgress(0.40);
	char *kwd5 = "ANTI_ELEM_FLAG";
    if (FindKeyWord(kwd5, fcfg))
        fscanf(fcfg,"%d",&flag);
    else
        flag = 0;
    rmesh->SetAntiElemFlag (flag);

    long ntloop,nitnod,nitedg,nitfac;
    char *kwd2 = "MAX_ITER";
    if (FindKeyWord(kwd2, fcfg))
        fscanf(fcfg,"%ld %ld %ld %ld",&ntloop,&nitnod,&nitedg,&nitfac);
    else
        {nitnod = nitedg = nitfac = 1;}

    long nloop=1,ntch=1;

		this->UpdateProgress(0.45);
		double progress = this->GetProgress();
    while(ntch && nloop<ntloop+1)
        {
        ntch=0;
    //  LOOP SOBRE LOS NODOS
		    progress+=0.03;
				this->UpdateProgress(progress);    
        printf ("\nAnalyzing node_clusters...\n");
        TimeOn();
        long nch,iter;
        nch = iter = 1;
        while (nch && iter<nitnod+1)
            {
            nch = rmesh->AnalizeAllNodes();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }
        TimeOff("Node_clusters analysis", 3);

    //  LOOP SOBRE LOS EDGES
		    progress+=0.03;
				this->UpdateProgress(progress);           
        printf ("\nAnalyzing edge_clusters...\n");
        TimeOn();
        nch = iter = 1;
        while (nch && iter<nitedg+1)
            {
            nch = rmesh->AnalizeAllEdges();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }

        TimeOff("Edgde_clusters analysis", 3);

    //  LOOP SOBRE LAS CARAS
		    progress+=0.03;
				this->UpdateProgress(progress); 
        printf ("\nAnalyzing face_clusters...\n");
        TimeOn();
        nch = iter = 1;
        while (nch && iter<nitfac+1)
            {
            nch = rmesh->AnalizeAllFaces();
            ntch+=nch;
            iter++;
            rmesh->TestStruct();
            }
        TimeOff("Face_clusters analysA compilação foi is", 3);

//    printf ("\nDeleting Null Elements...\n");
//    nch = rmesh->RemoveNullElements();
//    printf (" %ld elements removed",nch);

        nloop++;
    }

	//  IMPRESION DE LA MALLA    
		// Se há um output, ele será atualizado
	  if(!this->output) // print result in file 
			{
	    printf("Writing output mesh...\n");
	    char *kwd6 = "OUTPUT_MESH";
	    TimeOn();
	    if (FindKeyWord(kwd6, fcfg))
	        {
	        fscanf(fcfg,"%s",filename);
	        printf("\n\nOutput mesh to file: %s", filename);
	        fp = fopen(filename, "w");
	        if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
	        }
	    else
	        {
	        printf("Can't find OUTPUT_MESH keyword");
	        printf("\n\nOutput mesh to file: relax3d.vwm");
	        fp = fopen("relax3d.vwm", "w");
	        }
	    if (!fp) Error(FATAL_ERROR, 2, "Can't open input mesh file");
	    rmesh->Print(fp);
	    TimeOff("Mesh writing", 3);
	    fclose(fp);
			}
		else				// Fill output vtkUnstructuredGrid		
		  {
			printf("\nUpdating \'output\' vtkMeshData Volume...");
			rmesh->Print( this->output->GetVolume() );
			printf("done\n");		
		  }			
    delete rmesh;
}


void vtkQopt::ProcessFilter(int MaxIter[4],   int MaxCluster,  int AddNodeFlag,
										 				int AntiElemFlag, int QualityFlag, int SeeAllFlag)
{
	printf("                       QOPT - GEMESIS3D - HEMOLAB\n");
	printf("            Optimize element's quality on a tetraedral mesh\n\n");
	printf("Parameters: MAX_ITER:     %d %d %d %d\n",MaxIter[0],MaxIter[1],MaxIter[2],MaxIter[3]);
	printf("            MAX_CLUSTER:  %d\n", MaxCluster);
	printf("            NODES_FLAG:   %d\n", AddNodeFlag);
	printf("            ELEM_FLAG:    %d\n", AntiElemFlag);
	printf("            QUALITY_FLAG: %d\n", QualityFlag);
	printf("            SEE_ALL_FLAG: %d\n\n", SeeAllFlag);	
    
	// Atribuição do input
	vtkMesh3d *mesh = vtkMesh3d::New();
	this->UpdateProgress(0.20);

	cout << " Atualizando mesh a partir do input (vtkMeshData)" << endl;
	mesh->UpdateVolume((vtkUnstructuredGrid*) this->input);

	RelaxMesh *rmesh;
	// Calculo das estruturas auxiliares
	printf ("\nComputing auxiliary structures...\n");
	rmesh = new RelaxMesh(mesh);
	this->UpdateProgress(0.30);
	
	printf ("\nDeleting auxiliary Mesh...\n");
	mesh->Delete();
	
	printf ("\nTesting input mesh...\n");
	rmesh->TestStruct();
	this->UpdateProgress(0.35);
	
	// Max Cluster Parameter		
	long nczmax = MaxCluster;
	rmesh->SetNumMaxCluster(nczmax);
	
	this->UpdateProgress(0.40);
	// Setting Flags
	rmesh->SetAddNodesFlag 	(AddNodeFlag);
	rmesh->SetQualityFlag  	(QualityFlag);
	rmesh->SetSeeAllFlag 		(SeeAllFlag);
	rmesh->SetAntiElemFlag 	(AntiElemFlag);
	
	// Setting Max Iter
	long ntloop = MaxIter[0];
	long nitnod = MaxIter[1];
	long nitedg = MaxIter[2];
	long nitfac = MaxIter[3];
	
	long nloop=1,ntch=1;
	this->UpdateProgress(0.45);
	double progress = this->GetProgress();
	while(ntch && nloop<ntloop+1)
		{
		ntch=0;
		//  LOOP SOBRE LOS NODOS
		progress+=0.03;
		this->UpdateProgress(progress);    
		printf ("\nAnalyzing node_clusters...\n");
		long nch,iter;
		nch = iter = 1;
		while (nch && iter<nitnod+1)
			{
			nch = rmesh->AnalizeAllNodes();
			ntch+=nch;
			iter++;
			rmesh->TestStruct();
			}
		
		//  LOOP SOBRE LOS EDGES
		progress+=0.03;
		this->UpdateProgress(progress);           
		printf ("\nAnalyzing edge_clusters...\n");
		nch = iter = 1;
		while (nch && iter<nitedg+1)
			{
			nch = rmesh->AnalizeAllEdges();
			ntch+=nch;
			iter++;
			rmesh->TestStruct();
			}
		
		//  LOOP SOBRE LAS CARAS
		progress+=0.03;
		this->UpdateProgress(progress); 
		printf ("\nAnalyzing face_clusters...\n");
    nch = iter = 1;
		while (nch && iter<nitfac+1)
			{
			nch = rmesh->AnalizeAllFaces();
			ntch+=nch;
			iter++;
			rmesh->TestStruct();
			}
    nloop++;
		}

	// Fill output vtkUnstructuredGrid		
	printf("\nUpdating \'output\' vtkMeshData Volume...");
	rmesh->Print( this->output->GetVolume() );
	printf("done\n");		
		
  delete rmesh;
}

