/*
 * $Id$
 */
 
#ifndef _vtkQopt_h_
#define _vtkQopt_h_

#include <vtkObjectFactory.h>
#include <vtkAlgorithm.h>

class vtkMeshData;

class VTK_EXPORT vtkQopt: public vtkAlgorithm
{
  //Metodos
  public:
  vtkTypeRevisionMacro(vtkQopt,vtkAlgorithm);
  static vtkQopt *New();
  void PrintSelf(ostream& os, vtkIndent indent){};

  vtkQopt();
	~vtkQopt();
  
	void SetInput (vtkMeshData *in);
	void SetOutput(vtkMeshData *ou);

	// Processing with input configuration file
	void ProcessFilter(char *);
	
	// Processing with manual parameters
	void ProcessFilter(int MaxIter[4],   int MaxCluster, 	int AddNodeFlag,
										 int AntiElemFlag, int QualityFlag, int SeeAllFlag);	

	protected:
		vtkMeshData *input;
		vtkMeshData *output; 
};
#endif /*_vtkQopt_h_*/


