# $Id: CMakeLists.txt 779 2006-07-19 17:52:33Z rodrigo $

#===============================================================================
#=================== Configurando algumas variaveis para o projeto local =======
PROJECT(trisurf)

SET ( LIB_NAME		${PROJECT_NAME} )

SET(PKG_TRISURF_SRC	
	vtkTrisurf.cxx	
)

#==== Librarias utilizadas ============
SET(EXT_LIBS
	vtkCommon
	vtkGraphics	
	vtkFiltering	
	acdpoop
	baseoop	
	gemesis3dcommon
)

ADD_LIBRARY(${PROJECT_NAME} ${PKG_TRISURF_SRC})

FOREACH(c ${EXT_LIBS})
  TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${c})
ENDFOREACH(c)
