#include "vtkCallbackCommand.h"
#include "vtkCommand.h"

#include "vtkSurfaceGen.h"
#include "vtkTrisurf.h"

static void TrisurfUpdateProgress(vtkObject *_surface, unsigned long,
                             void *_trisurf, void *)
{  
  vtkSurfaceGen *surface = reinterpret_cast<vtkSurfaceGen *>(_surface);
	vtkTrisurf *trisurf = reinterpret_cast<vtkTrisurf *>(_trisurf);
  trisurf->SetProgress(surface->GetProgress());
  trisurf->UpdateProgress(surface->GetProgress());  
}

vtkCxxRevisionMacro(vtkTrisurf, "$Rev: 635 $");
vtkStandardNewMacro(vtkTrisurf);

vtkTrisurf::vtkTrisurf()
{
	this->surf = NULL;
	this->cbc = vtkCallbackCommand::New();
}    

vtkTrisurf::~vtkTrisurf()
{
	if(this->cbc)
		{
		cbc->Delete();  
	  if(this->surf) 	surf->RemoveObserver(cbc);		
		}
}

vtkSurfaceGen* vtkTrisurf::GetSurfaceGen()
{ 
	if(surf) return surf;
	else		 return NULL;
}


void vtkTrisurf::SetSurfaceGen(vtkSurfaceGen* surfOut)
{
	this->surf = surfOut;	
	
	if(cbc)
	{
	  cbc->SetClientData(this);
  	cbc->SetCallback(TrisurfUpdateProgress);
  	surf->AddObserver(vtkCommand::ProgressEvent, cbc);	
	}
	
}    

void vtkTrisurf::Smooth(int iter, double fac, int shr, int Gr, int needle)
{

	this->surf->Smooth( iter, fac, shr, Gr, needle);

}

void vtkTrisurf::SetCotaColap(double cota)
{
	this->surf->SetCotaCopla(cota);
}

void  vtkTrisurf::SetCotaCopla(double cota)
{
	this->surf->SetCotaCopla(cota);		
}

void  vtkTrisurf::SetFactorH(double fact)
{
	this->surf->SetFactorH(fact);		
}

long vtkTrisurf::SwapAll()
{
	long result;
	result = this->surf->SwapAll();
  return result;
}

long vtkTrisurf::DivideObtusos(double cota)
{
	long result;
	result =  this->surf->DivideObtusos(cota);
	return result;	
}

long vtkTrisurf::ColapsaTodos(double value) 
{
	long result = 0;
	if(this->surf)
	{
    this->surf->SetCotaColap(value);
	  result = this->surf->ColapsaTodos();
	}
	return result;
}


void vtkTrisurf::RemoveTresTri()
{
	this->surf->RemoveTresTri();
}



long vtkTrisurf::RemoveFreeNodes()
{
	long result;
	result = this->surf->RemoveFreeNodes();
	return result;
}

void vtkTrisurf::TestMesh(long &nod, long &nel, int &null)
{
	this->surf->TestMesh(nod, nel, null);
}

void vtkTrisurf::Transform(double tx, double ty, double tz,
                           double sx, double sy, double sz,
                           double rx, double ry, double rz)
{
	this->surf->Transform(tx, ty, tz, sx, sy, sz, rx, ry, rz);
}

void vtkTrisurf::GroupInformation(long *numElemGroup, long *numNodesGroup)
{
	this->surf->GroupInformation(numElemGroup, numNodesGroup);
}

void  vtkTrisurf::Information(long *nod, long *nel, long *grp)
{
	this->surf->Information(nod, nel, grp);
}

int vtkTrisurf::PoneTapas()
{
	return this->surf->PoneTapas();
}

long vtkTrisurf::DivideTodos()
{
	long result;
	result = this->surf->DivideTodos();
	return result;
}

void vtkTrisurf::SwapGroup(long gr)
{
	surf->SwapGroup(gr);
}


long vtkTrisurf::RemoveGroup(long gr)
{
	long result;
	result = surf->RemoveGroup(gr);
  return result;
}

void vtkTrisurf::MergeGroups(int id1, int id2)
{
	surf->MergeGroups(id1, id2);
}

void vtkTrisurf::RemoveNeedles(double angle, int *ret)
{
	surf->RemoveNeedles(angle, ret);
}

long vtkTrisurf::ViewNeedles(double angle, char *ids) 
{
	return surf->ViewNeedles(angle, ids);
}

long vtkTrisurf::ViewSmallTri(double area) 
{
	return surf->ViewSmallTri(area);
}

long vtkTrisurf::RemoveSmallTri(double area)
{
	return surf->RemoveSmallTri(area);	
}
