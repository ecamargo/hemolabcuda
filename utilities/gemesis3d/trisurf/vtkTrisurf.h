#ifndef __vtkTrisurf_h
#define __vtkTrisurf_h

#include <vtkObjectFactory.h>
#include "vtkSurfaceGen.h"

class vtkSurfaceGen;
class vtkCallbackCommand;

class VTK_EXPORT vtkTrisurf: public vtkAlgorithm //vtkObject
{
  public:
	vtkTypeRevisionMacro(vtkTrisurf,vtkObject);
	void PrintSelf(ostream& os, vtkIndent indent){};
	static vtkTrisurf *New();       
	vtkTrisurf();
 	~vtkTrisurf();

	void SetSurfaceGen(vtkSurfaceGen*);
	vtkSurfaceGen* GetSurfaceGen();	

  // Retorna informações referentes ao número elementos e nós por grupo
	void  GroupInformation(long *numElemGroup, long *numNodesGroup);
	void  Smooth(int iter, double fac, int shr, int Gr, int needle);
	void MergeGroups(int, int);
	long  DivideObtusos(double cota);
	long  DivideTodos();
	void  SetCotaColap(double cota);
	void  SetCotaCopla(double cota);
	void  RemoveTresTri();
	long  SwapAll();
	void  SwapGroup(long );    
	void  SetFactorH  (double fact);
	long  ColapsaTodos(double);
	long  RemoveFreeNodes();
	void  Transform(double tx, double ty, double tz,
                  double sx, double sy, double sz,
                  double rx, double ry, double rz);
	void  TestMesh(long &nod, long &nel, int &null);
	void  Information(long *nod, long *nel, long *grp);    
	int   PoneTapas();    
	long  RemoveGroup(long gr);
	void  RemoveNeedles(double angle, int *);  
	long  ViewNeedles(double angle, char *ids);
	long  ViewSmallTri(double area);     
	long  RemoveSmallTri(double area);  

	protected:
  	vtkSurfaceGen *surf;
	vtkCallbackCommand *cbc;
};

#endif

