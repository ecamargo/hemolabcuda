/* 
 * Primeiro teste geral da classe incorReader
 * 
 * Teste gera volume (.vtk) com os dados de derivadas (em x, y e z separados), 
 * campos escalares etc.
 * 
 */

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "../incorReader.h"
#include "../vectorField.h"
#include "../vectorFieldDerivative.h"
#include "../derivative.h"
#include "../vtkBridge.h"

using namespace std;

// Read the "config.txt" file
int readConfigFile(string input, string &scalarName, string &vectorPrefix, 
						 string &vectorPosfix, int &n, string &output)
{
	ifstream inputFile(input.c_str());
	
	if(inputFile.is_open())
	{
		string dummy, emptyLine;
		getline(inputFile, dummy);
		getline(inputFile, scalarName);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, vectorPrefix);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, vectorPosfix);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, dummy);		
		n = atoi(dummy.c_str() );
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, output);
		
		inputFile.close();
	}
	else
	{
		cout << "Error: Unable to open config file." << endl;
		return 0;
	}
	return 1;
}

// Main function
int main( int argc, char *argv[])
{
	// 
	// MODIFICAR AQUI PARA INDICAR QUAIS COMPONENTES VÃO ESTAR NO ARQUIVO VTK DE SAÍDA
	//
	bool exportVectorsToVTK				 = true;
	bool exportDerivativeVectorsToVTK = false;
	bool exportVolumeScalarsToVTK		 = true;
	bool exportScalarProductsToVTK	 = true;	
	
	string scalarName, vectorPrefix, vectorPosfix, output;
	string input = "incorTest01Config.txt";
	int numberOfVectorFields;
	if(!readConfigFile(input, scalarName, vectorPrefix, vectorPosfix, numberOfVectorFields, output) )
	{
		cout << "Try \"incor2hemolab config.txt\" with a valid config file." << endl;		
		return 0;
	}
	cout << "\nInitial Configure Settings" << endl;
	cout << "  Scalar: " << scalarName << endl;
	cout << "  Vector Prefix: " << vectorPrefix << endl;
	cout << "  Vector Posfix: " << vectorPosfix << endl;
	cout << "  Example of the first Vector X prediction: " << vectorPrefix << "1" << vectorPosfix << "X" << endl;  	
	cout << "  Number Of Vector Fields: " << numberOfVectorFields <<endl;
	cout << "  Output: " << output << endl;
	
	vtkBridge *exporter = new vtkBridge();
	
	IncorReader *incor = new IncorReader();
		incor->readTimScalars(scalarName);	// Reading Scalar Fields and get sizex, sizey, sizez and ntimes
		
	if(exportVolumeScalarsToVTK)
	{
		exporter->SetMultipleScalarsToVTK("Volume", incor->GetNumberOfVolumes(), incor->GetDimensions(), incor->GetScalars());
	}  		

	VectorFieldDerivative *vfield = new VectorFieldDerivative();
		vfield->SetNumberOfVolumes(numberOfVectorFields);
		vfield->SetVolumeRange( incor->GetDimensions() );
		vfield->Allocate();	
		
	// Reading Velocity Fields
	string outputName;	
	for(int i = 1; i <= numberOfVectorFields; i++ )
	{
		incor->SetFullName(outputName, i, vectorPrefix, vectorPosfix);
		incor->readTimVelocity(outputName);

		if(exportVectorsToVTK)
		{
			exporter->SetVectorsToVTK("Velocity", i, incor->GetDimensions(),incor->GetVector('x'),
											  incor->GetVector('y'), incor->GetVector('z'));
		}
		
		vfield->GetVector(i-1)->Allocate( incor->GetDimensions() );
		vfield->GetVector(i-1)->CopyValues('x', incor->GetVector('x') );
		vfield->GetVector(i-1)->CopyValues('y', incor->GetVector('y') );
		vfield->GetVector(i-1)->CopyValues('z', incor->GetVector('z') );		
		
		incor->ReleaseAllocatedVectors(); // Release allocated velocity vectors	
	}	
	
	cout << "Computing ALL matrices for this derivative vectors." << endl;
	vfield->ComputeAllDerivativeMatrices();
	vfield->ComputeAllSymmetricMatrixScalarProducts();
	
	cout << "Computing scalar products." << endl;
	if(exportScalarProductsToVTK)
	{
		
		for(int i = 1; i <= numberOfVectorFields; i++ )
		{
			exporter->SetScalarsToVTK("ScalarProduct", i-1, vfield->GetVolumeRange(),	
												vfield->GetScalarProductVector(i-1));			
		}	
	}	

	for(int i = 1; i <= numberOfVectorFields; i++ )
	{
		vfield->ComputeSymmetricMatrixLineValues(0, i-1);
		if(exportDerivativeVectorsToVTK)
		{
			exporter->SetVectorsToVTK("Derivative_Volume_X_", i, vfield->GetVolumeRange(),
		   	                       vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
		      	                    vfield->GetMatrixLineVector('z'));
		}
		vfield->ReleaseAllocatedVectors();
				                          
		vfield->ComputeSymmetricMatrixLineValues(1, i-1);
		if(exportDerivativeVectorsToVTK)
		{
			exporter->SetVectorsToVTK("Derivative_Volume_Y_", i, vfield->GetVolumeRange(),
		   	                       vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
		      	                    vfield->GetMatrixLineVector('z'));
		}
		vfield->ReleaseAllocatedVectors();	
			                          
		vfield->ComputeSymmetricMatrixLineValues(2, i-1);
		if(exportDerivativeVectorsToVTK)
		{
			exporter->SetVectorsToVTK("Derivative_Volume_Z_", i, vfield->GetVolumeRange(),
		   	                       vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
		      	                    vfield->GetMatrixLineVector('z'));
		}
		vfield->ReleaseAllocatedVectors();						
		//	vfield->SetDerivativeToVTK(grid, i);
	}
	
	exporter->Write(output);
	cout << "Done!" << endl;
	delete incor;
	delete exporter;
	delete vfield;
	
	return 0;
}
