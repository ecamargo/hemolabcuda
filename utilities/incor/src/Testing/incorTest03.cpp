/* 
 * Segundo teste da classe incorReader
 * 
 * Gera gráfico com os valores dos produtos escalares em cada instante de tempo
 * 
 */

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "../incorReader.h"
#include "../vectorField.h"
#include "../vectorFieldDerivative.h"
#include "../derivative.h"
#include "../vtkBridge.h"

using namespace std;

// Read the "config.txt" file
int readConfigFile(string input, string &scalarName, string &vectorPrefix, 
						 string &vectorPosfix, int &n, string &output)
{
	ifstream inputFile(input.c_str());
	
	if(inputFile.is_open())
	{
		string dummy, emptyLine;
		getline(inputFile, dummy);
		getline(inputFile, scalarName);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, vectorPrefix);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, vectorPosfix);
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, dummy);		
		n = atoi(dummy.c_str() );
		getline(inputFile, emptyLine);

		getline(inputFile, dummy);
		getline(inputFile, output);
		
		inputFile.close();
	}
	else
	{
		cout << "Error: Unable to open config file." << endl;
		return 0;
	}
	return 1;
}

// Main function
int main( int argc, char *argv[])
{
	string scalarName, vectorPrefix, vectorPosfix, output;
	string input = "incorTest01Config.txt";
	int numberOfVectorFields;
	if(!readConfigFile(input, scalarName, vectorPrefix, vectorPosfix, numberOfVectorFields, output) )
	{
		cout << "Try \"incor2hemolab config.txt\" with a valid config file." << endl;		
		return 0;
	}
	cout << "\nInitial Configure Settings" << endl;
	cout << "  Scalar: " << scalarName << endl;
	cout << "  Vector Prefix: " << vectorPrefix << endl;
	cout << "  Vector Posfix: " << vectorPosfix << endl;
	cout << "  Example of the first Vector X prediction: " << vectorPrefix << "1" << vectorPosfix << "X" << endl;  	
	cout << "  Number Of Vector Fields: " << numberOfVectorFields <<endl;
	cout << "  Output: " << output << endl;
	
	vtkBridge *exporter = new vtkBridge();
	
	IncorReader *incor = new IncorReader();
		incor->readTimScalars(scalarName);	// Reading Scalar Fields and get sizex, sizey, sizez and ntimes
		
	exporter->SetMultipleScalarsToVTK("Volume", incor->GetNumberOfVolumes(), incor->GetDimensions(), incor->GetScalars());

	VectorFieldDerivative *vfield = new VectorFieldDerivative();
		vfield->SetNumberOfVolumes(numberOfVectorFields);
		vfield->SetVolumeRange( incor->GetDimensions() );
		vfield->Allocate();	
		vfield->SetScalars( incor->GetScalars() );
	
	// Reading Velocity Fields
	string outputName;	
	for(int i = 1; i <= numberOfVectorFields; i++ )
	{
		incor->SetFullName(outputName, i, vectorPrefix, vectorPosfix);
		incor->readTimVelocity(outputName);
		vfield->GetVector(i-1)->Allocate( incor->GetDimensions() );
		vfield->GetVector(i-1)->CopyValues('x', incor->GetVector('x') );
		vfield->GetVector(i-1)->CopyValues('y', incor->GetVector('y') );
		vfield->GetVector(i-1)->CopyValues('z', incor->GetVector('z') );		
		incor->ReleaseAllocatedVectors(); // Release allocated velocity vectors	
	}	
	
	cout << "Computing ALL matrices for this derivative vectors." << endl;
	vfield->ComputeAllDerivativeMatrices();
		vfield->SetRho(1.0f);
		vfield->SetVoxelSpacing(1.0f, 1.0f, 1.0f);
		vfield->ComputeAllSymmetricMatrixScalarProducts();	

	float *sum = new float[numberOfVectorFields];

	cout << "Preliminary product scalar sum results: " << endl; 
	
	vfield->ComputeSymmetricMatrixScalarProductsSum( sum );	
	
	for(int i = 1; i <= numberOfVectorFields; i++ )
	{
	//	sum[i-1] =	vfield->ComputeSymmetricMatrixScalarProductsSum(i-1);
		cout << "Volume[" << i-1 << "]: " << sum[i-1] << endl; 
	}
	
	
	exporter->GenerateXYPlot(numberOfVectorFields, sum);
	
	cout << "Done!" << endl;
	delete incor;
	delete exporter;
	delete vfield;
	
	return 0;
}
