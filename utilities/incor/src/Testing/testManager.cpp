/*
 * Manage all the tests by reading a "testManager.txt" file
 * 
 * This file has the following structure
 * 
 * Vector Test    		<- Name of the first test
 * g++ bla.cpp -o app	<- Line to executed for the first test
 * 							<- Empty line (mandatory)
 * Matrix Test				<- Name of the second test
 * g++ mat.cpp -o app   <- Line to executed for the second test
 * 							<- Empty line (mandatory) 
 * ...
 * 
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
using namespace std;

//------------------------------------------------------------------------------
int countNumberOfTests()
{
	string line;
	ifstream myfile ("testManager.txt");
	int countLines = 0;
	if (myfile.is_open())
	{
   	while (! myfile.eof() )
   	{
      	getline (myfile,line);
      	if(line.length() > 0) // Count only lines that has text
      		countLines++;
    	}
    	myfile.close();
	}
	else
	{ 
		cout << "Error: Unable to open file" << endl;
		exit(1);
	}
	return countLines/2; // Each Test has two lines
   
}

//------------------------------------------------------------------------------
void setNamesAndCommands(string *name, string *command)
{
	string line;
	ifstream myfile ("testManager.txt");
	int i = 0;
	if (myfile.is_open())
	{
   	while (! myfile.eof())
   	{
			getline (myfile,line); 
			// If one 'valid' was found, its a test name and next is the command
			if(line.length() > 0)
			{
   		name[i] = line;
			getline (myfile,line); 
			command[i] = line;
			i++;   						
			}   		
    	}
    	myfile.close();
	}
	else
	{ 
		cout << "Error: Unable to open file" << endl;
		exit(1);
	} 
}

void cleaning()
{
	cout << "\nCleaning 'app' and '*.o'..." << endl;
	system("rm *.o");
	system("rm ../*.o");
	system("rm app");	
	cout << "Done!" << endl;
	
}

//------------------------------------------------------------------------------
void leave()
{
	cleaning();
	exit(1);
}										

//------------------------------------------------------------------------------
int showMainMenu(string *names, int numTests)
{
	int selected;
	cout << "\n\nTEST MANAGER" << endl;
	cout << "------------\n" << endl;
	cout << "Which test would you like to compile: " << endl;	
	for(int i = 0; i < numTests; i++)
	{
		cout.fill('0');
		cout << setw(2) << i+1 << " - " << names[i] << endl;		
	}
	cout << " 0 - Exit" << endl;
	cout << "Option: " ;
	cin >> selected;
	cin.ignore();
	if(!selected) leave();	
	return selected-1; // get the index
}

//------------------------------------------------------------------------------
char compileSelected(string name, string command)
{
	char op;
	cout << "Compiling " << name << endl;
	system(command.c_str());
	cout << "\nWould you like to execute the test now ([y]es or [n]o) or [c]ompile again? [y][n][c]: ";
	cin >> op;
	return op;
}

//-----------------------------------------------------------------------------
void executeSelected()
{
	cout << "\nExecuting selected test...\n\n" << endl;
	system("./app");
}

//------------------------------------------------------------------------------
void managerMainLoop(string *names, string *command, int numTests)
{
	int selected;
	char op;
	do
	{
		selected = showMainMenu(names, numTests);
		if(selected >= 0 && selected < numTests)
		{
			op = compileSelected(names[selected], command[selected]);
			switch(op)
			{
				case 'y':
					executeSelected();
				break;
				case 'n':
					cleaning();				
				break;
				case 'c':
					do
					{
						op = compileSelected(names[selected], command[selected]);
						if(op == 'y') executeSelected();							
					}while(op=='c');
				break;
			}
			
			cout << "\nGet back to menu? [y][n]: ";
			cin >> op;
			if(op == 'y') continue; 
			else			  leave();
		}
	}while(1);
}

//------------------------------------------------------------------------------
int main () 
{
	int num = countNumberOfTests();
	cout << "Number of tests: " << num << endl;
	
	string *testNames 	= new string[num];
	string *testCommands = new string[num];
	
	setNamesAndCommands(testNames, testCommands);	
	managerMainLoop(testNames, testCommands, num);
	
	return 0;
}


