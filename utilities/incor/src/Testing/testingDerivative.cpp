
/*
 * Test of the Derivative class
 */
 
#include <iostream>
using namespace std;
#include "../vector.h"
#include "../derivative.h"

void getValue(Vector *v, int i, int j, int k, float *values)
{
	values[0] = v->GetValue('x', i, j, k);
	values[1] = v->GetValue('y', i, j, k);
	values[2] = v->GetValue('z', i, j, k);
}
	
void print(float *values)
{
	cout << "{ " << values[0] << ", " << values[1] << ", " << values[2] << " }" << endl;
}


int main()
{
	cout << "Testing \"Derivative\" Class...\n\n" << endl;
	Derivative *deriv = new Derivative();
		
	cout << "Creating the input arrays for a [3,4,5] volume" << endl;
	Vector *v = new Vector();
	int size = 3 * 4 * 5;
	float vx[size];
	float vy[size];
	float vz[size];	
	for(int i = 0; i < size; i++)
	{
		vx[i] = i+1;
		vy[i] = vx[i] * 2;
		vz[i] = vy[i] * 2;		
	}
	
	v->Allocate(3, 4, 5);
	cout << "Copying inpuy arrays to vector" << endl;
	v->CopyValues('x', vx);
	v->CopyValues('y', vy);
	v->CopyValues('z', vz);
	deriv->SetVector(v);
		
	cout << "Computing Dx, Dy e Dz da posição 1,1,1." << endl;
	cout << "  Valores esperados: { 1, 3, 12 } {2, 6, 24} {4, 12, 48} )" << endl;
	deriv->SetVoxelPosition(1,1,1);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;
	
	cout << "Computing Dx, Dy e Dz da posição 0,1,2." << endl;
	cout << "  Valores esperados: { 0, 3, 12 } {0, 6, 24} {0, 12, 48} )" << endl;
	deriv->SetVoxelPosition(0,1,2);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;

	cout << "Computing Dx, Dy e Dz da posição 1,3,1." << endl;
	cout << "  Valores esperados: { 1, 0, 12 } {2, 0, 24} {4, 0, 48} )" << endl;
	deriv->SetVoxelPosition(1,3,1);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;

	cout << "Computing Symmetric of the previuos matrix." << endl;
	cout << "  Valores esperados: { 1, 1, 8 } {1, 0, 12} {8, 12, 48} )" << endl;
	deriv->ComputeSymmetricMatrix();
	cout << *deriv->GetSymmetricMatrix() << endl;

	cout << "Computing Dx, Dy e Dz da posição 2,3,0;  0,0,4 e 2,3,4." << endl;
	cout << "  Valores esperados para as três posições acima: { 0, 0, 0 } {0, 0, 0} {0, 0, 0} )" << endl;
	deriv->SetVoxelPosition(2,3,0);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;

	deriv->SetVoxelPosition(0,0,4);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;

	deriv->SetVoxelPosition(2,3,4);
	deriv->ComputeMatrix();
	cout << *deriv->GetMatrix() << endl;

	
	delete v;
	delete deriv;

	return 0;
}


