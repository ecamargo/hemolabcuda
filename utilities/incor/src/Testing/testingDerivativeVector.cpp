
/*
 * Test of the DerivativeVector class
 */
 
#include <iostream>
using namespace std;
#include "../vector.h"
#include "../derivative.h"
#include "../derivativeVector.h"

int main()
{
	cout << "Testing \"DerivativeVector\" Class...\n" << endl;
	cout << "Creating the input arrays for a [3,4,5] volume" << endl;
	Vector *v = new Vector();
	int size = 3 * 4 * 5;
	float vx[size];
	float vy[size];
	float vz[size];	
	for(int i = 0; i < size; i++)
	{
		vx[i] = i+1;
		vy[i] = vx[i] * 2;
		vz[i] = vy[i] * 2;		
	}
	
	v->Allocate(3, 4, 5);
	cout << "Copying inpuy arrays to vector" << endl;
	v->CopyValues('x', vx);
	v->CopyValues('y', vy);
	v->CopyValues('z', vz);
	
	cout << "Creating \"DerivativeVector\" Class...\n" << endl; 
	DerivativeVector *derivVec = new DerivativeVector();
		derivVec->Allocate(3, 4, 5);
		derivVec->SetVector(v);
	
	cout << "Computing Dx, Dy e Dz da posição 1,1,1." << endl;
	cout << "  Valores esperados: { 1, 3, 12 } {2, 6, 24} {4, 12, 48} )" << endl;
	derivVec->GetDerivative(1,1,1)->ComputeMatrix();
	cout << *derivVec->GetDerivative(1,1,1)->GetMatrix() << endl;

	cout << "Computing Dx, Dy e Dz da posição 1,3,1." << endl;
	cout << "  Valores esperados: { 1, 0, 12 } {2, 0, 24} {4, 0, 48} )" << endl;
	derivVec->GetDerivative(1,3,1)->ComputeMatrix();
	cout << *derivVec->GetDerivative(1,3,1)->GetMatrix() << endl;


	cout << "Computing Symmetric of the previuos matrix." << endl;
	cout << "  Valores esperados: { 1, 1, 8 } {1, 0, 12} {8, 12, 48} )" << endl;
	derivVec->GetDerivative(1,3,1)->ComputeSymmetricMatrix();
	cout << *derivVec->GetDerivative(1,3,1)->GetSymmetricMatrix() << endl;
	
	delete v;
	delete derivVec;

	return 0;
}



