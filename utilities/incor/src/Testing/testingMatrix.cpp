
/*
 * Test of the Matrix class
 */
 
#include <iostream>
using namespace std;
#include "../matrix.h"

int main()
{
	cout << "Testing \"Matrix\" Class...\n\n" << endl;	
	Matrix *m1 = new Matrix();
	Matrix *mt = new Matrix();
	Matrix *ms = new Matrix();	
	
	// Inserindo elementos
	for(int i = 0; i < 9; i++)
		m1->SetElement(i, (i+1) * 10);
		
	cout << "Matriz de Entrada" << endl;
	cout << *m1 << endl;
	
	cout << "Matriz Transposta" << endl;
	mt->ComputeTransposeMatrix(m1);
	cout << *mt << endl;

	cout << "Matriz Simétrica" << endl;
	ms->ComputeSymmetricMatrix(m1);
	cout << *ms << endl;

	Matrix *msoma = new Matrix();
	msoma->Sum(m1);
	msoma->Sum(mt);
	cout << "Matriz Soma da entrada + transposta" << endl;
	cout << *msoma << endl;
					
	cout << "Matriz de Entrada * 1/2" << endl;
	m1->Multiply(0.5f);
	cout << *m1 << endl; 

	cout << "Matriz anterior / 100" << endl;
	m1->Divide(100.0f);
	cout << *m1 << endl; 
	
	cout << "Matriz após método clear" << endl;
	m1->Clear();
	cout << *m1 << endl;	
	
	// Testing error 1 (descomentar para testar)
	//m1->SetElement(15, 5.0f);

	// Testing error 2 (descomentar para testar)
	//m1->Divide(0.0f);
	
	delete m1;
	delete mt;
	delete msoma;
	
	return 0;
}


