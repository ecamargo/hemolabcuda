
/*
 * Test of the Vector class
 */
 
#include <iostream>
using namespace std;
#include "../vector.h"

void getValue(Vector *v, int i, int j, int k, float *values)
{
	values[0] = v->GetValue('x', i, j, k);
	values[1] = v->GetValue('y', i, j, k);
	values[2] = v->GetValue('z', i, j, k);
}
	
void print(float *values)
{
	cout << "{ " << values[0] << ", " << values[1] << ", " << values[2] << " }" << endl;
}


int main()
{
	cout << "Testing \"Vector\" Class...\n\n" << endl;	
	Vector *v = new Vector();
	
	cout << "Creating the input arrays for a [3,4,5] volume" << endl;
	int size = 3 * 4 * 5;
	float vx[size];
	float vy[size];
	float vz[size];	
	for(int i = 0; i < size; i++)
	{
		vx[i] = i+1;
		vy[i] = i+2;
		vz[i] = i+3;		
	}
	
	v->Allocate(3, 4, 5);
	cout << "Copying inpuy arrays to vector" << endl;
	v->CopyValues('x', vx);
	v->CopyValues('y', vy);
	v->CopyValues('z', vz);
	
	float values[3];
	cout << "Accessing position [0,0,0]: " << endl;
	getValue(v, 0, 0, 0, values);
	print(values);
	cout << "Accessing position [1,1,1]: " << endl;	
	getValue(v, 1, 1, 1, values);
	print(values);
	cout << "Accessing position [2,3,2]: " << endl;	
	getValue(v, 2, 3, 2, values);
	print(values);
	cout << "Accessing position [2,3,4]: " << endl;	
	getValue(v, 2, 3, 4, values);
	print(values);

	//----------------------------------------------------------------------------
	// Error Tests

	// ** Error test 1 **		
	//cout << "\nTrying to Access position [1,-1,1]: " << endl;		// Uncomment this line to enable the test 1	
	//getValue(v, 1, -1, 1, values);											// Uncomment this line to enable the test 1

	// ** Error test 2 **
	//cout << "\nTrying to Access position [7,5,9]: " << endl;		// Uncomment this line to enable the test 2
	//getValue(v, 7, 5, 9, values);											// Uncomment this line to enable the test 2

	// ** Error test 3 **
	//	Vector *v2 = new Vector();												// Uncomment this line to enable the test 3
	//	cout << "\n-> Accessing a not initialized vector" << endl;	// Uncomment this line to enable the test 3
	//	v2->CopyValues('x', vx);												// Uncomment this line to enable the test 3
	//delete v2;																	// Uncomment this line to enable the test 3
		
	delete v;


	return 0;
}


