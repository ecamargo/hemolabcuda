
/*
 * Test of the VectorField class 
 */
 
#include <iostream>
using namespace std;
#include "../vectorField.h"

void getValue(Vector *v, int i, int j, int k, float *values)
{
	values[0] = v->GetValue('x', i, j, k);
	values[1] = v->GetValue('y', i, j, k);
	values[2] = v->GetValue('z', i, j, k);
}
	
void print(float *values)
{
	cout << "{ " << values[0] << ", " << values[1] << ", " << values[2] << " }" << endl;
}


int main()
{
	cout << "Testing \"VectorField\" Class...\n\n" << endl;
	cout << "Setting 3 time steps (Volumes)" << endl;
	VectorField *vfield = new VectorField();
		vfield->SetNumberOfVolumes(3);
		vfield->SetVolumeRange(3, 4, 5);
		
	cout << "Creating the input arrays for a [3,4,5] volume and 3 time steps" << endl;
	for(int t = 0; t < 3; t++)
	{
		int size = 3 * 4 * 5;
		float vx[size];
		float vy[size];
		float vz[size];	
		for(int i = 0; i < size; i++)
		{
			vx[i] = i+1+t;
			vy[i] = i+2+t;
			vz[i] = i+3+t;		
		}
		
		cout << "Copying inpuy arrays to vector" << endl;
		vfield->GetVector(t)->CopyValues('x', vx);
		vfield->GetVector(t)->CopyValues('y', vy);
		vfield->GetVector(t)->CopyValues('z', vz);
	}
		
	float values[3];
	cout << "Accessing position [0,0,0] in time 0: " << endl;
	getValue(vfield->GetVector(0), 0, 0, 0, values);
	print(values);

	cout << "Accessing position [0,0,0] in time 2: " << endl;
	getValue(vfield->GetVector(2), 0, 0, 0, values);
	print(values);
	
	cout << "Accessing position [2,3,4] in time 0: " << endl;
	getValue(vfield->GetVector(0), 2, 3, 4, values);
	print(values);

	cout << "Accessing position [2,3,4] in time 2: " << endl;
	getValue(vfield->GetVector(2), 2, 3, 4, values);
	print(values);

	//----------------------------------------------------------------------------
	// Error Tests (uncomment both lines to test)

	// ** Error Test 1
	//cout << "Trying to access invalid time { [2,3,4] in time 3 }: " << endl;	// Uncomment this line to enable the test 1
	//getValue(vfield->GetVector(3), 2, 3, 4, values);									// Uncomment this line to enable the test 1
	//print(values);																					// Uncomment this line to enable the test 1
	
	// ** Error Test 2	
	//VectorField *vfield2 = new VectorField();											// Uncomment this line to enable the test 2
	//cout << "\n-> Accessing a not initialized vectorField" << endl;				// Uncomment this line to enable the test 2
	//Vector *aux = vfield2->GetVector(0);													// Uncomment this line to enable the test 2
	//delete vfield2;																				// Uncomment this line to enable the test 2

	delete vfield;
	
	cout << "\nTests were OK!\n" << endl; 
	return 0;
}


