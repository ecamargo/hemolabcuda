
/*
 * Test of the VectorFieldDerivative class 
 */
 
#include <iostream>
using namespace std;
#include "../vectorFieldDerivative.h"

void getValue(Vector *v, int i, int j, int k, float *values)
{
	values[0] = v->GetValue('x', i, j, k);
	values[1] = v->GetValue('y', i, j, k);
	values[2] = v->GetValue('z', i, j, k);
}
	
void print(float *values)
{
	cout << "{ " << values[0] << ", " << values[1] << ", " << values[2] << " }" << endl;
}


int main()
{
	cout << "Testing \"VectorFieldDerivative\" Class..." << endl;
	cout << "Setting 3 time steps" << endl;
	VectorFieldDerivative *vfield = new VectorFieldDerivative();
		vfield->SetNumberOfVolumes(3);
		vfield->SetVolumeRange(3, 4, 5);
		vfield->Allocate();
		
	cout << "Creating the input arrays for a [3,4,5] volume and 3 time steps" << endl;
	for(int t = 0; t < 3; t++)
	{
		int size = 3 * 4 * 5;
		float vx[size];
		float vy[size];
		float vz[size];	
		for(int i = 0; i < size; i++)
		{
			vx[i] = i+1 + t;
			vy[i] = vx[i] * 2 + t;
			vz[i] = vy[i] * 2 + t;		
		}
		
		cout << "Copying inpuy arrays to vector" << endl;
		vfield->GetVector(t)->Allocate(3,4,5);
		vfield->GetVector(t)->CopyValues('x', vx);
		vfield->GetVector(t)->CopyValues('y', vy);
		vfield->GetVector(t)->CopyValues('z', vz);
	}
		
	float values[3];
	cout << "Accessing position [0,0,0] in time 0: " << endl;
	getValue(vfield->GetVector(0), 0, 0, 0, values);
	print(values);

	cout << "Accessing position [1,1,1] in time 0: " << endl;
	getValue(vfield->GetVector(0), 1, 1, 1, values);
	print(values);

	cout << "Accessing position [1,1,1] in time 0 for the Derivative to check: " << endl;
	getValue(vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetVector(), 1, 1, 1, values);
	print(values);

	cout << "Accessing position [0,0,0] in time 2: " << endl;
	getValue(vfield->GetVector(2), 0, 0, 0, values);
	print(values);
	
	cout << "Accessing position [2,3,4] in time 0: " << endl;
	getValue(vfield->GetVector(0), 2, 3, 4, values);
	print(values);

	cout << "Accessing position [2,3,4] in time 2: " << endl;
	getValue(vfield->GetVector(2), 2, 3, 4, values);
	print(values);

	cout << "Computing ALL matrices for this derivative vectors." << endl;
	vfield->ComputeAllDerivativeMatrices();
	
	cout << "Accessing Dx, Dy e Dz of volume 0 in position 1,1,1." << endl;
	cout << "  Excepted: { 1, 3, 12 } {2, 6, 24} {4, 12, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,1,1)->GetMatrix() << endl;
	
	cout << "Accessing Dx, Dy e Dz of volume 0 in position 1,3,1." << endl;
	cout << "  Excepted: { 1, 0, 12 } {2, 0, 24} {4, 0, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetMatrix() << endl;
	
	cout << "Accessing symmetric matrix of the previous voxel." << endl;
	cout << "  Excepted: { 1, 1, 8 } {1, 0, 12} {8, 12, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetSymmetricMatrix() << endl;

	cout << "Accessing Dx, Dy e Dz of volume 2 in position 1,3,1." << endl;
	cout << *vfield->GetDerivativeVector(2)->GetDerivative(1,3,1)->GetMatrix() << endl;
	
	cout << "Accessing symmetric matrix of the previous voxel." << endl;
	cout << *vfield->GetDerivativeVector(2)->GetDerivative(1,3,1)->GetSymmetricMatrix() << endl;

	delete vfield;
	
	cout << "\nTests were OK!\n" << endl; 
	return 0;
}


