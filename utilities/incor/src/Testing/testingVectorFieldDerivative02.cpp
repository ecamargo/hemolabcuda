
/*
 * Test 02 of the VectorFieldDerivative class
 * 
 * Using constant data base of x = 1 and y and z = 0; 1 volume
 *  
 */
 
#include <iostream>
using namespace std;
#include "../vectorFieldDerivative.h"
#include "../vtkBridge.h"

#include "vtkStructuredGrid.h"
#include "vtkFloatArray.h"
#include "vtkPointData.h"
#include "vtkDataSetWriter.h"
#include "vtkDataSetMapper.h"

void getValue(Vector *v, int i, int j, int k, float *values)
{
	values[0] = v->GetValue('x', i, j, k);
	values[1] = v->GetValue('y', i, j, k);
	values[2] = v->GetValue('z', i, j, k);
}
	
void print(float *values)
{
	cout << "{ " << values[0] << ", " << values[1] << ", " << values[2] << " }" << endl;
}


int main()
{
	const char outputFile[256] = {"../../output/outputTest02.vtk"};
	cout << "Testing \"VectorFieldDerivative\" Class - Second Test" << endl;
	VectorFieldDerivative *vfield = new VectorFieldDerivative();
		vfield->SetNumberOfVolumes(1);
		vfield->SetVolumeRange(3, 4, 5);
		vfield->Allocate();
	
	//----------------------------------------------------------------------------------	
	cout << "Creating the input arrays for a [3,4,5] volume in 1 time steps" << endl;
	int size = 3 * 4 * 5;
	float *vx = new float[size];
	float *vy = new float[size];
	float *vz = new float[size];	
	for(int i = 0; i < size; i++)
	{
		vx[i] = 1.0f;
		vy[i] = 0.0f;
		vz[i] = 0.0f;		
	}	
	cout << "Copying inpuy arrays to vector" << endl;
	vfield->GetVector(0)->Allocate(3,4,5);
	vfield->GetVector(0)->CopyValues('x', vx);
	vfield->GetVector(0)->CopyValues('y', vy);
	vfield->GetVector(0)->CopyValues('z', vz);

	//----------------------------------------------------------------------------------
	cout << "Computing derivatives and scalars products" << endl;	
	vfield->ComputeAllDerivativeMatrices();
	vfield->ComputeAllSymmetricMatrixScalarProducts();

	cout << "Creating vtkBridge object" << endl;	
	vtkBridge *exporter = new vtkBridge();

	cout << "--> Setting Derivative_Volume_X_1 to VTK" << endl;	
	vfield->ComputeSymmetricMatrixLineValues(0, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_X_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();

	cout << "--> Setting Derivative_Volume_Y_1 to VTK" << endl;			                          
	vfield->ComputeSymmetricMatrixLineValues(1, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_Y_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();	

	cout << "--> Setting Derivative_Volume_Z_1 to VTK" << endl;		                          
	vfield->ComputeSymmetricMatrixLineValues(2, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_Z_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();						
	// End of Setting derivatives
	cout << "--> Setting ScalarProduct to VTK" << endl;	
	exporter->SetScalarsToVTK("ScalarProduct", 1, vfield->GetVolumeRange(),	
										vfield->GetScalarProductVector(0));
	
	cout << "--> Setting Components to VTK" << endl;
	exporter->SetVectorsToVTK("Components_", 1, vfield->GetVolumeRange(),
	                          vfield->GetVectorComponents('x'), 
	                          vfield->GetVectorComponents('y'), 
	                          vfield->GetVectorComponents('z'));
										
	exporter->Write(outputFile);
	
	delete exporter;
	delete vfield;	
	delete []vx;
	delete []vy;
	delete []vz;
	/*	
	// Setting vtk output
	vtkStructuredGrid *grid = vtkStructuredGrid::New();
	
	vfield->SetVTKStructuredDimensions(grid);
	vfield->SetDerivativeToVTK(grid);
	vfield->SetVectorComponentsToVTK(grid);
	vfield->SetScalarProductToVTK(grid);
	
	vtkDataSetMapper *camMapper = vtkDataSetMapper::New();
		camMapper->SetInput(grid);
		
	cout << "Generating test volume named " << outputFile << endl;
	vtkDataSetWriter *writer = vtkDataSetWriter::New();
		writer->SetInput(grid);
		writer->SetFileName(outputFile);
		writer->Write();	
	cout << "File \"" << outputFile << "\" successfully generated!" << endl << endl;
	
	writer->Delete();
	camMapper->Delete();
	grid->Delete();
	delete vfield;
		*/
		/*
	float values[3];
	cout << "Accessing position [0,0,0] in time 0: " << endl;
	getValue(vfield->GetVector(0), 0, 0, 0, values);
	print(values);

	cout << "Accessing position [1,1,1] in time 0: " << endl;
	getValue(vfield->GetVector(0), 1, 1, 1, values);
	print(values);

	cout << "Accessing position [1,1,1] in time 0 for the Derivative to check: " << endl;
	getValue(vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetVector(), 1, 1, 1, values);
	print(values);

	cout << "Accessing position [0,0,0] in time 2: " << endl;
	getValue(vfield->GetVector(2), 0, 0, 0, values);
	print(values);
	
	cout << "Accessing position [2,3,4] in time 0: " << endl;
	getValue(vfield->GetVector(0), 2, 3, 4, values);
	print(values);

	cout << "Accessing position [2,3,4] in time 2: " << endl;
	getValue(vfield->GetVector(2), 2, 3, 4, values);
	print(values);

	cout << "Computing ALL matrices for this derivative vectors." << endl;
	vfield->ComputeAllDerivativeMatrices();
	
	cout << "Accessing Dx, Dy e Dz of volume 0 in position 1,1,1." << endl;
	cout << "  Excepted: { 1, 3, 12 } {2, 6, 24} {4, 12, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,1,1)->GetMatrix() << endl;
	
	cout << "Accessing Dx, Dy e Dz of volume 0 in position 1,3,1." << endl;
	cout << "  Excepted: { 1, 0, 12 } {2, 0, 24} {4, 0, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetMatrix() << endl;
	
	cout << "Accessing symmetric matrix of the previous voxel." << endl;
	cout << "  Excepted: { 1, 1, 8 } {1, 0, 12} {8, 12, 48} )" << endl;
	cout << *vfield->GetDerivativeVector(0)->GetDerivative(1,3,1)->GetSymmetricMatrix() << endl;

	cout << "Accessing Dx, Dy e Dz of volume 2 in position 1,3,1." << endl;
	cout << *vfield->GetDerivativeVector(2)->GetDerivative(1,3,1)->GetMatrix() << endl;
	
	cout << "Accessing symmetric matrix of the previous voxel." << endl;
	cout << *vfield->GetDerivativeVector(2)->GetDerivative(1,3,1)->GetSymmetricMatrix() << endl;

	delete vfield;
	*/
	
	cout << "\nTests were OK!\n" << endl; 
	return 0;
}


