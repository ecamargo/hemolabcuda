/*
 * Test 04 of the VectorFieldDerivative class
 * 
 * Using constant data base x = 0 and z = 0 and y is linear (increasing by one) 
 * 1 volume
 *  
 */
 
#include <iostream>
using namespace std;
#include "../vectorFieldDerivative.h"
#include "../vtkBridge.h"

int main()
{
	int sizex = 6;
	int sizey = 7;
	int sizez = 5;
	const char outputFile[256] = {"../../output/outputTest04.vtk"};
	cout << "Testing \"VectorFieldDerivative\" Class - Fourth Test  (Linear in Y)" << endl;
	VectorFieldDerivative *vfield = new VectorFieldDerivative();
		vfield->SetNumberOfVolumes(1);
		vfield->SetVolumeRange(sizex, sizey, sizez);
		vfield->Allocate();
	
	//----------------------------------------------------------------------------------	
	cout << "Creating the input arrays for a [6,7,5] volume in 1 time steps" << endl;
	int size = sizex * sizey * sizez;
	float *vx = new float[size];
	float *vy = new float[size];
	float *vz = new float[size];
	int i = 0;	
	for(int z = 0; z < sizez; z++)
		for(int y = 0; y < sizey; y++)
			for(int x = 0; x < sizex; x++)	
			{
				vx[i] = 0.0f;
				vy[i] = (float) y;
				vz[i] = 0.0f;
				i++;		
			}	
	cout << "Copying inpuy arrays to vector" << endl;
	vfield->GetVector(0)->Allocate(sizex, sizey, sizez);
	vfield->GetVector(0)->CopyValues('x', vx);
	vfield->GetVector(0)->CopyValues('y', vy);
	vfield->GetVector(0)->CopyValues('z', vz);

	//----------------------------------------------------------------------------------
	cout << "Computing derivatives and scalars products" << endl;	
	vfield->ComputeAllDerivativeMatrices();
	vfield->ComputeAllSymmetricMatrixScalarProducts();

	cout << "Creating vtkBridge object" << endl;	
	vtkBridge *exporter = new vtkBridge();

	cout << "--> Setting Derivative_Volume_X_1 to VTK" << endl;	
	vfield->ComputeSymmetricMatrixLineValues(0, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_X_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();

	cout << "--> Setting Derivative_Volume_Y_1 to VTK" << endl;			                          
	vfield->ComputeSymmetricMatrixLineValues(1, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_Y_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();	

	cout << "--> Setting Derivative_Volume_Z_1 to VTK" << endl;		                          
	vfield->ComputeSymmetricMatrixLineValues(2, 0);
	exporter->SetVectorsToVTK("Derivative_Volume_Z_", 1, vfield->GetVolumeRange(),
	                          vfield->GetMatrixLineVector('x'), vfield->GetMatrixLineVector('y'), 
	                          vfield->GetMatrixLineVector('z'));
	vfield->ReleaseAllocatedVectors();						
	// End of Setting derivatives
	cout << "--> Setting ScalarProduct to VTK" << endl;	
	exporter->SetScalarsToVTK("ScalarProduct", 1, vfield->GetVolumeRange(),	
										vfield->GetScalarProductVector(0));
	
	cout << "--> Setting Components to VTK" << endl;
	exporter->SetVectorsToVTK("Components_", 1, vfield->GetVolumeRange(),
	                          vfield->GetVectorComponents('x'), 
	                          vfield->GetVectorComponents('y'), 
	                          vfield->GetVectorComponents('z'));
										
	exporter->Write(outputFile);
	
	delete exporter;
	delete vfield;	
	delete []vx;
	delete []vy;
	delete []vz;

	cout << "\nTests were OK!\n" << endl; 
	return 0;
}


