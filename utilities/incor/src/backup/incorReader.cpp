#include <iostream>
#include <string>

#include "vtkDataSetMapper.h"
#include "vtkStructuredGrid.h"
#include "vtkPointData.h"
#include "vtkIntArray.h"
#include "vtkFloatArray.h"

#include "incorReader.h"
using namespace std;

IncorReader::IncorReader()
{
	this->grid = NULL;
}

void IncorReader::SetOutput(vtkStructuredGrid *output)
{
	this->grid = output;
}

/*----------------- lerlinha () ---------------------*/
int IncorReader::lerlinha(FILE *arq)
{
	char line[200];
	char dummy[200];
	int llength=200;
	int i,k;

	fgets(line,llength,arq);
	i=0; k=0;
	while (line[i]!='=') i++;
	i++;
	while (line[i]!='\0')
	{
		dummy[k]=line[i];
		k++; i++;
	}
	dummy[k]='\0';

	return(atoi(dummy));
}

short int IncorReader::readHeader(char *input_file, int *type, int *dim, int *nx, int *ny, int *nz, int *nt)
{
	char       filein[300];
	FILE       *pfile;

	strcpy(filein, input_file);
	strcat(filein,".thd");
	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);

	/* read a blank line */
	lerlinha(pfile);

	/* read file name */
	lerlinha(pfile);

	/* read data type */
	*type=lerlinha(pfile);

	/* read image dimension */
	*dim=lerlinha(pfile);

	/* read x dims */
	*nx=lerlinha(pfile);

	/* read y dims */
	*ny=lerlinha(pfile);

	/* read z dims */
	*nz=lerlinha(pfile);

	/* read t dims */
	*nt=lerlinha(pfile);

	fclose(pfile);
	return(0);
}

// Rotina para ler  um arquivo binario formato raw data   
short int IncorReader::readRaw (void *p, char *input_file, int type, int npixels)
{
	char       				filein[512];
	unsigned short int  	*pshort;
	int        				*pint;
	float						*pfloat;
	int        				numwriten;
	FILE       				*pfile;

	strcpy(filein, input_file);
	strcat(filein,".tim");

	printf("\n FILEIN = %s\n", filein);


	if (type==1) pint		= (int* ) p;
	if (type==2) pfloat	= (float *) p;
	if (type==3) pshort	= (unsigned short int*) p;
	
	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);
	
	printf("  * type = %d, npixels=%d\n",type,npixels);

	if (type==1)
	{
		if ((numwriten = fread(pint,sizeof(int),npixels,pfile)) != npixels)
			return(-2);

	}
	if (type==2)
	{
		if ((numwriten = fread(pfloat,sizeof(float),npixels,pfile)) != npixels)
			return(-2);
	}
	if (type==3)
	{
		if ((numwriten = fread(pshort,sizeof(unsigned short int),npixels,pfile)) != npixels)
			return(-2);
	}

	printf("  * File was successfully read\n");
	fclose(pfile);
	return(0);
}

int IncorReader::func_convert_endian(int number)
{
	int byte0, byte1, byte2, byte3;
	
	byte0 = (number & 0x000000FF) >> 0 ;
	byte1 = (number & 0x0000FF00) >> 8 ;
	byte2 = (number & 0x00FF0000) >> 16 ;
	byte3 = (number & 0xFF000000) >> 24 ;	

	return((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
}

float IncorReader::func_convert_endian( float bEnum )
{

	float lEnum;
	char *lE = (char*) &lEnum;
	char *bE = (char*) &bEnum;
	lE[0] = bE[3];
	lE[1] = bE[2];
	lE[2] = bE[1];
	lE[3] = bE[0];
	return lEnum;

}

int IncorReader::readTimScalars(string s)
{
	/* ----> START OF USER-SUPPLIED CODE SECTION #3 (COMPUTE ROUTINE BODY)   */
	const char *file_name = s.c_str();
	char filein[512];
	short int     codErro=0;
	unsigned short int  *pshort;
	int 	      type;
	int 	      imgdim,nlin,ncol,nslices=1,nvolumes=1,npixels;
	int	      *pint,dims[3],i,j,k;
	float	      *pfloat;
	
	if(!this->grid)
	{
		cout << "Error: No vtkStructuredGrid was set!" << endl;
		return 0;
	}
	strcpy(filein, file_name);

	readHeader(filein, &type, &imgdim, &ncol, &nlin, &nslices, &nvolumes);
	/* type=1 -> integer; type=2 -> float */
	cout << "\n\n----------------------------------------------------------------------" << endl;
	printf(" Main Dimensions:\n  imgdim = %d \n  ncol = %d \n  nlin = %d  \n  nslices = %d \n  nvolumes = %d\n", imgdim,ncol,nlin,nslices,nvolumes);
     
	//reading image data on 'tim' file
	npixels = nlin*ncol*nslices;
	if (type==1) // integer type
	{
		if ((pint = (int *) calloc(npixels,sizeof (int))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pint,filein,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
		   return(codErro);
		}
	}
		
	if (type==2) // float type 
	{
		if ((pfloat = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pfloat,filein,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
	}

	if (type==3) // short integer type
	{
		if ((pshort = (unsigned short int *) calloc(npixels,sizeof (short int))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pshort,filein,type,npixels)) != 0)
		{
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
		if ((pint = (int *) calloc(npixels,sizeof (int))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		for (i=0; i < npixels; i++)
			pint[i] = (int) pshort[i];
		free(pshort);
	}

	dims[0] = ncol;
	dims[1] = nlin;
	dims[2] = nslices/nvolumes;
			
	int p = 0;
	vtkPoints *xyzpoints = vtkPoints::New();
	for (k = 0; k < dims[2]; k++)
	{
		for (j = 0; j < dims[1]; j++)
		{
			for (i = 0; i < dims[0]; i++)
			{
				xyzpoints->InsertNextPoint(i,j,k);
				pint += 3;
			}
		}
	}
	this->grid->SetDimensions(dims[0],dims[1], dims[2]);
	this->grid->SetPoints(xyzpoints); 
	xyzpoints->Delete();
	
	if(type == 1 || type == 3)
	{
		vtkIntArray *array;
		char scalarName[256];
		p = 0;
		for (int n = 0; n < nvolumes; n++)
		{
			array = vtkIntArray::New();
			array->SetNumberOfComponents(1);
			sprintf(scalarName, "Volume%d", n);
			array->SetName(scalarName);
			for (k = 0; k < dims[2]; k++)
			{
				for (j = 0; j < dims[1]; j++)
				{
					for (i = 0; i < dims[0]; i++)
					{
						array->InsertNextValue( func_convert_endian( pint[p] )  ); 
						p++;
					}
				}
			}
			this->grid->GetPointData()->AddArray(array);
			array->Delete();			
		}
	}
	else // float
	{
		vtkFloatArray *array;
		char scalarName[256];
		p = 0;
		for (int n = 0; n < nvolumes; n++)
		{
			array = vtkFloatArray::New();
			array->SetNumberOfComponents(1);
			sprintf(scalarName, "Volume%d", n);
			array->SetName(scalarName);
			for (k = 0; k < dims[2]; k++)
			{
				for (j = 0; j < dims[1]; j++)
				{
					for (i = 0; i < dims[0]; i++)
					{
						array->InsertNextValue( func_convert_endian( pfloat[p] )  ); 
						p++;
					}
				}
			}
			this->grid->GetPointData()->AddArray(array);
			array->Delete();			
		}
	}

	// <---- END OF USER-SUPPLIED CODE SECTION #3
	return(1);
}

// Read float (and only float) vector fields
// TODO: Inserir leitura de inteiros se necessário
int IncorReader::readTimVelocity(string s, int n)
{
	if(!this->grid)
	{
		cout << "Error: No vtkStructuredGrid was set!" << endl;
		return 0;
	}
	
	string fileX = s;
	fileX.append("X");
	string fileY = s;
	fileY.append("Y");
	string fileZ = s;
	fileZ.append("Z");
	cout << "\n\n----------------------------------------------------------------------" << endl;
	cout << "Reading Files: " << endl;
	cout << fileX << ".*" << endl;
	cout << fileY << ".*" << endl;
	cout << fileZ << ".*" << endl;		
			
	// START OF USER-SUPPLIED CODE SECTION #3 (COMPUTE ROUTINE BODY)
	//const char *file_name = fileX.c_str(); // Leia apenas um, pois headers são iguais (ao menos para esse teste)
	char fileinX[512], fileinY[512], fileinZ[512];
	short int     codErro=0;
	//unsigned short int  *pshort;
	int 	      type, imgdim,nlin,ncol,nslices=1,nvolumes=1,npixels,dims[3],i,j,k;//,count=0;
	float	      *pfloat1, *pfloat2, *pfloat3;
	//strcpy(filein, file_name);

	strcpy(fileinX, (char *) fileX.c_str());
	strcpy(fileinY, (char *) fileY.c_str());
	strcpy(fileinZ, (char *) fileZ.c_str());
	
	// Leio apenas uma vez pois os dados são os mesmos (ao menos nesse teste) para todos 
	// os arquivos de velocidade (X, Y, Z)
	readHeader(fileinX, &type, &imgdim, &ncol, &nlin, &nslices, &nvolumes);
	// type=1 -> integer; type=2 -> float 
	printf("\n type = %d \n imgdim = %d \n ncol = %d \n nlin = %d  \n nslices = %d \n nvolumes = %d\n", type, imgdim,ncol,nlin,nslices,nvolumes);
	npixels = nlin*ncol*nslices;
	
	if(type == 1)
	{
		cout << "Error: This version is only capable of read FLOAT vector fields" << endl;
		return 0;
	}
	
	if ((pfloat1 = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate X vector coords!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
			return(-5);
		}
	if ((codErro =  readRaw (pfloat1,fileinX,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
			return(codErro);
		}
	
	if ((pfloat2 = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate Y vector coords!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
			return(-5);
		}
	if ((codErro =  readRaw (pfloat2,fileinY,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
			return(codErro);
		}
		
	if ((pfloat3 = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate Z vector coords!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
			return(-5);
		}
	if ((codErro =  readRaw (pfloat3,fileinZ,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
			return(codErro);
		}
		
	vtkFloatArray *array;
	char scalarName[256];
	int p = 0;
	float value[3];
		
	dims[0] = ncol;
	dims[1] = nlin;
	dims[2] = nslices;
	
	// CRIA AQUI OS VETORES DE VELOCIDADE QUE SERÃO ADICIONADOS AO 
	// GRID NÃO ESTRUTURADO
	array = vtkFloatArray::New();
	array->SetNumberOfComponents(3);
	printf("Setting Velocity%d vector field.\n", n);
	sprintf(scalarName, "Velocity%d", n);
	array->SetName(scalarName);
	for (k = 0; k < dims[2]; k++)
	{
		for (j = 0; j < dims[1]; j++)
		{
			for (i = 0; i < dims[0]; i++)
			{
				value[0] = func_convert_endian(pfloat1[p]);
				value[1] = func_convert_endian(pfloat2[p]);
				value[2] = func_convert_endian(pfloat3[p]);
				array->InsertNextTupleValue(value); 
				p++;
			}
		}
	}
	this->grid->GetPointData()->AddArray(array);
	array->Delete();			
	return(1);
}
