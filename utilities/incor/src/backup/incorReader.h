/***************************************************                                          
* RODRIGO LUIS DE SOUZA DA SILVA          25/08/2008                          
*	   			incorReader.h                  
***************************************************/
/*
Read incor raw header and data files
*/

#include <string>
using namespace std;

class vtkStructuredGrid;

#ifndef INCORREADER_H_
#define INCORREADER_H_

class IncorReader
{
	public:	
		// public Attributes
		vtkStructuredGrid *grid;

	public:	
		// public methods
		void SetOutput(vtkStructuredGrid *);
		int readTimScalars(string s);
		int readTimVelocity(string s, int n);     
		
		// Constructor
		IncorReader();
	
   protected:
		int lerlinha(FILE *arq);
		short int readHeader(char *input_file, int *type, int *dim, int *nx, int *ny, int *nz, int *nt);
		short int readRaw (void *p, char *input_file, int type, int npixels);
		int   func_convert_endian( int number);
		float func_convert_endian( float bEnum );
};


#endif /*INCORREADER_H_*/
