Ola Prof. Raul e Pablo,

Prazer, eu sou a Marina, que participa no projeto de Modelagem computacional do sistema cardiovascular
humano aqui pelo InCor.

Estou enviando para voces um conjunto de imagens, que contem as velocidades estimadas a partir das
imagens de SPECT de um sujeito normal (parte de um protocolo de pesquisa do InCor).

O arquivo em anexo "vel.ziz' (é um arquivo zip, bastarenomear) contem os seguintes arquivos:

    . eduardo.thd e eduardo.tim - contem as informacoes originais de intensidade do miocardio. E´ um
                                                  estudo com 15 volumes temporais. Cada volume possui dimensoes
                                                  28x18x25 (x,y,z). O arquivo ".thd" é um header simples** e o arquivo
                                                  ".tim" contem os dados de imagem.
    . eduardoVel-n_VelX.thd e eduardoVel-n_VelX.tim (n = 1... 13) - contem as informacoes da
                                                  componente X da velocidade estimada para um determinado frame.
                                                  Foram calculadas as velocidades dos frames 1 a 13.  O numero no nome
                                                  do arquivo indica o frame. Os arquivos com sufixo
                                                  VelY contem a componente Y e os com sufixo VelZ, a componente Z.
    .  readtimGeral.c -  codigo  para leitura dos arquivos de velocidade.

**O header é bastante simples, aqui vai um exemplo (o arquivo eduardo.thd):

       title = /dados1/pesqdese/granada/ADAC/gated/normais/male/tim/eduardo   
       type = 1              // define o tipo de dado: 1 long int; 2 float; 3 short int
      dim = 3                // dimensao da imagem
       xdim = 28           // nx
       ydim = 18           // ny
       zdim = 375         // nz*nt
       tdim = 15           //nt
       xsigma = 0.000000
       ysigma = 0.000000
       zsigma = 0.000000
       tsigma = 0.000000
       attributes =

 No arquivo readtimGeral.c os trechos de codigo mais importante sao o readheader  e o readraw.
Importante: os codigos foram implementados e testados em Solaris (big endian). Caso voces estejam
trabalhando em maquinas little endian, e´ necessario inverter a ordem dos bytes. A seguir, o
trecho de codigo para isso:

       int func_convert_endian(int number)
      {
             int byte0, byte1, byte2, byte3;

             byte0 = (number & x000000FF) >> 0 ;
             byte1 = (number & x0000FF00) >> 8 ;
             byte2 = (number & x00FF0000) >> 16 ;
             byte3 = (number & xFF000000) >> 24 ;

            return((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
      }
   
Os dados de eduardo.tim sao inteiros. Os dados de velocidade sao float, o codigo e´ semelhante.

Caso tenham alguma dificuldade em acessar os dados, e´so´ entrar em contato comigo.

Abracos
Marina




-- 
-------------------------------------------------------
Marina de Sá Rebelo
Pesquisadora - Unidade de P&D
Servico de Informatica - Instituto do Coracao - HCFMUSP
Av.Dr.Enéas de Carvalho Aguiar, 44
CEP 05403-000 Sao Paulo - SP
Fone: (11) 3069-5547
Fax: (11) 3069-5311
E-mail: marina.rebelo@incor.usp.br
------------------------------------------------------- 
