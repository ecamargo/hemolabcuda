#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCubeSource.h"
#include "vtkDataSetMapper.h"
#include "vtkLODActor.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkProperty.h"
#include "vtkStructuredGrid.h"
#include "vtkPoints.h"
#include "vtkPointData.h"
#include "vtkDataSetWriter.h"
#include "vtkIntArray.h"
#include "vtkByteSwap.h"

#include <iostream>
#include <string>
using namespace std;

string teste;

/* definicao de estruturas */

/* Field 3D integer */
typedef struct {
	int dimensions [3];
	int *data;
} field3Dint;

/* Field 3D float */
typedef struct {
	int dimensions [3];
	float *data;
} field3Dfloat;

/* Field 3D vector */
typedef struct {
	int dimensions [3];
	float *data;
} field3Dvector;


/*----------------- lerlinha () ---------------------*/
int lerlinha(FILE *arq)
{
	char line[200];
	char dummy[200];
	int llength=200;
	//int value;
	int i,k;//,j,l,pto;
	//float a;

	fgets(line,llength,arq);
	i=0; k=0;
	while (line[i]!='=') i++;
	i++;
	while (line[i]!='\0')
	{
		dummy[k]=line[i];
		k++; i++;
	}
	dummy[k]='\0';

	return(atoi(dummy));
}

short int readHeader(char *input_file, int *type, int *dim, int *nx, int *ny, int *nz, int *nt)
{
	char       filein[300];
	//char       line[80];
	//int         i, length=80;
	FILE       *pfile;


	strcpy(filein, input_file);
	strcat(filein,".thd");
	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);

	/* read a blank line */
	lerlinha(pfile);


	/* read file name */
	lerlinha(pfile);

	/* read data type */
	*type=lerlinha(pfile);

	/* read image dimension */
	*dim=lerlinha(pfile);

	/* read x dims */
	*nx=lerlinha(pfile);

	/* read y dims */
	*ny=lerlinha(pfile);

	/* read z dims */
	*nz=lerlinha(pfile);

	/* read t dims */
	*nt=lerlinha(pfile);

	fclose(pfile);
	printf("\n file: %s successful read", filein);
	return(0);
}

/*--------------- readraw () ---------------------*/
/* Rotina para ler  um arquivo binario formato raw data   */
/* Input: pointer p/ imagem            */
/*            number of pixels               */
/*            filename without extension (default will be *.tim      */

short int readRaw (void *p, char *input_file, int type, int npixels)
{
	char       				filein[300];
	unsigned short int  	*pshort;
	int        				*pint;
	float						*pfloat;
	int        				numwriten;
	FILE       				*pfile;

	strcpy(filein, input_file);
	strcat(filein,".tim");

	printf("\n FILEIN = %s", filein);


	if (type==1) pint		= (int* ) p;
	if (type==2) pfloat	= (float *) p;
	if (type==3) pshort	= (unsigned short int*) p;

	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);

	printf("\n type = %d, npixels=%d",type,npixels);

	if (type==1)
	{
		if ((numwriten = fread(pint,sizeof(int),npixels,pfile)) != npixels)
			return(-2);

	}
	if (type==2)
	{
		if ((numwriten = fread(pfloat,sizeof(float),npixels,pfile)) != npixels)
			return(-2);
	}
	if (type==3)
	{
		if ((numwriten = fread(pshort,sizeof(unsigned short int),npixels,pfile)) != npixels)
			return(-2);

	}

	printf("\n file: %s successful read", filein);
	fclose(pfile);
	return(0);
}

int func_convert_endian(int number)
{
	int byte0, byte1, byte2, byte3;
	
	byte0 = (number & 0x000000FF) >> 0 ;
	byte1 = (number & 0x0000FF00) >> 8 ;
	byte2 = (number & 0x00FF0000) >> 16 ;
	byte3 = (number & 0xFF000000) >> 24 ;	

	return((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
}

int readTimCompute(string s, vtkStructuredGrid *grid)
{

	/* ----> START OF USER-SUPPLIED CODE SECTION #3 (COMPUTE ROUTINE BODY)   */

	//char 	      cbuf[1000],aux[512];
	const char *file_name = s.c_str();
	char filein[512];
	short int     codErro=0;
	unsigned short int  *pshort;
	int 	      type;
	int 	      imgdim,nlin,ncol,nslices=1,nvolumes=1,npixels;//,offset;
	int	      *pint,dims[3],i,j,k;//,count=0;
	//int	       ndim, veclen, uniform, ncoord;
	float	      *pfloat;
//	AVSfield_int  	      *field_int;
//	AVSfield_float     *field_float;	 if (file_name)
	//{

		//if (*output_field) free(*output_field);

     /* ----------------------------------------------------------------
		reading image information on 'thd' file
		---------------------------------------------------------------- */

		
		//offset = strlen(file_name);
		//strcpy(aux,file_name);
		//aux[offset-4] = '\0';

		//strcpy(filein,aux);
		strcpy(filein, file_name);

		readHeader(filein, &type, &imgdim, &ncol, &nlin, &nslices, &nvolumes);
		/* type=1 -> integer; type=2 -> float */

		printf("\n imgdim = %d \n ncol = %d \n nlin = %d  \n nslices = %d \n nvolumes = %d\n", imgdim,ncol,nlin,nslices,nvolumes);

		
      //	displaying information in the AVS window
		/*
		offset = strlen(filein);
		while (filein[offset] != '/')
		{ offset--;   count++;}
		offset++; count--;
		for (i=0; i < count; i++)
			aux[i] = filein[offset+i];
		aux[i] = '\0';

		strcpy (cbuf,"\n ---------------------\n");
		strcat (cbuf,"\n");
		strcat (cbuf,aux);
		strcat (cbuf,"\n");
		strcat (cbuf,"\nlinhas: ");
		intoa(nlin,aux);
		strcat (cbuf,aux);
		strcat (cbuf, "\ncolunas: ");
		intoa(ncol,aux);
		strcat (cbuf,aux);
		strcat (cbuf, "\nslices: ");
		intoa((nslices/nvolumes),aux);
		strcat (cbuf,aux);
		strcat (cbuf, "\nvolumes: ");
		intoa(nvolumes,aux);
		strcat (cbuf,aux);
		strcat(cbuf,"\n\n ---------------------\n");
		AVSmodify_parameter ("information", AVS_VALUE, cbuf, 0, 0);
		*/

     
		//reading image data on 'tim' file
		npixels = nlin*ncol*nslices;
		if (type==1) // integer type
		{
			if ((pint = (int *) calloc(npixels,sizeof (int))) == NULL)
			{  
				printf("Error!! Not Enough memory!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
				return(-5);
			}
			if ((codErro =  readRaw (pint,filein,type,npixels)) != 0)
			{  
				printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
			   return(codErro);
			}
//			type = AVS_TYPE_INTEGER;
		}
		
		if (type==2) // float type 
		{
			if ((pfloat = (float *) calloc(npixels,sizeof (float))) == NULL)
			{  
				printf("Error!! Not Enough memory!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
				return(-5);
			}
			if ((codErro =  readRaw (pfloat,filein,type,npixels)) != 0)
			{  
				printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
				return(codErro);
			}
		//	type = AVS_TYPE_REAL;

		}

		if (type==3) // short integer type
		{
			if ((pshort = (unsigned short int *) calloc(npixels,sizeof (short int))) == NULL)
			{  
				printf("Error!! Not Enough memory!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
				return(-5);
			}
			if ((codErro =  readRaw (pshort,filein,type,npixels)) != 0)
			{
				printf("Error!! Error on routine ReadRaw!\n");				
				//AVSmessage(file_version, AVS_Warning, NULL,"read_tim_compute","exit","Error on routine Readraw");
				return(codErro);
			}
			if ((pint = (int *) calloc(npixels,sizeof (int))) == NULL)
			{  
				printf("Error!! Not Enough memory!\n");
				//AVSmessage(file_version, AVS_Warning, NULL, "read_tim_compute","exit","Not enough memory for intermediate buffer");
				return(-5);
			}
			for (i=0; i < npixels; i++)
				pint[i] = (int) pshort[i];
			free(pshort);
			//type = AVS_TYPE_INTEGER;
		}

		//allocating memory for AVS field
		dims[0] = ncol;
		dims[1] = nlin;
		dims[2] = nslices/nvolumes;
				
		int p = 0;
		vtkPoints *xyzpoints = vtkPoints::New();
		for (k = 0; k < dims[2]; k++)
		{
			for (j = 0; j < dims[1]; j++)
			{
				for (i = 0; i < dims[0]; i++)
				{
					xyzpoints->InsertNextPoint(i,j,k);
					pint += 3;
				}
			}
		}
		grid->SetDimensions(dims[0],dims[1], dims[2]);
		grid->SetPoints(xyzpoints); 
		xyzpoints->Delete();
		
		vtkIntArray *array;
		char scalarName[256];
		p = 0;
		for (int n = 0; n < nvolumes; n++)
		{
			array = vtkIntArray::New();
			array->SetNumberOfComponents(1);
			sprintf(scalarName, "Volume %d", n);
			array->SetName(scalarName);
			for (k = 0; k < dims[2]; k++)
			{
				for (j = 0; j < dims[1]; j++)
				{
					for (i = 0; i < dims[0]; i++)
					{
						array->InsertNextValue( func_convert_endian( pint[p] )  ); 
						p++;
					}
				}
			}
			grid->GetPointData()->AddArray(array);
			array->Delete();			
		}

		
		/*
		switch(nslices)
		{ 
			case 1: ndim=2; // 2D 
			veclen=1; // length of data; for scalar field veclen=1 
			uniform=UNIFORM; // field is uniform 
			ncoord=ndim; // #dimensions; rectilinear field is == ndim 

		if ((type==AVS_TYPE_INTEGER))
			*output_field=AVSbuild_field(ndim,veclen,uniform,ncoord,type,ncol,nlin,pint);

		if (type==AVS_TYPE_REAL)
			*output_field=AVSbuild_field(ndim,veclen,uniform,ncoord,type,ncol,nlin,pfloat);
		break;

		default: ndim=3; // 3D 
		veclen=1; // length of data; for scalar field veclen=1
		uniform=UNIFORM; // field is uniform 
		ncoord=ndim; // #dimensions; rectilinear field is == ndim 

		if ((type==AVS_TYPE_INTEGER))
			*output_field=AVSbuild_field(ndim,veclen,uniform,ncoord,type,ncol,nlin,nslices,pint);

		if (type==AVS_TYPE_REAL)
			*output_field=AVSbuild_field(ndim,veclen,uniform,ncoord,type,ncol,nlin,nslices,pfloat);
		break;
		}
		*/

	// <---- END OF USER-SUPPLIED CODE SECTION #3
	return(1);
}
int main( int argc, char *argv[])
{

	vtkStructuredGrid *grid = vtkStructuredGrid::New();
	readTimCompute("/home/rodrigo/workspace/incor/data/eduardo", grid);
	
	vtkRenderer *ren1 = vtkRenderer::New();
		ren1->SetBackground(0.1, 0.2, 0.4);
	vtkRenderWindow *renWin = vtkRenderWindow::New();
		renWin->AddRenderer(ren1);
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
		iren->SetRenderWindow(renWin);

//	vtkUnstructuredGridReader *reader = vtkUnstructuredGridReader::New();
//		reader->SetFileName("unstructuredGrid.vtk");
//		reader->Update();

	vtkDataSetMapper *camMapper = vtkDataSetMapper::New();
		camMapper->SetInput(grid);//(vtkDataSet *) reader->GetOutput());
		
	vtkDataSetWriter *writer = vtkDataSetWriter::New();
		writer->SetInput(grid);
		writer->SetFileName("miocardio.vtk");
		writer->Write();

	vtkLODActor *camActor = vtkLODActor::New();
		camActor->SetMapper(camMapper);
		camActor->GetProperty()->SetDiffuseColor( 1, .4, .3);
		camActor->GetProperty()->SetSpecular( .4);
		camActor->GetProperty()->SetSpecularPower( 80);		
		camActor->GetProperty()->SetDiffuse( .8);
		camActor->GetProperty()->SetOpacity (91.0);
  
	ren1->AddActor(camActor);

	renWin->Render();

	iren->Start();
	
	return 0;
}
