
#include "derivative.h"
#include <iostream>
using namespace std;

//------------------------------------------------------------------------------------
Derivative::Derivative()
{
	this->debugMode = false;	
	this->DerivMatrix = new Matrix();
	this->DerivSymmetricMatrix = new Matrix();
	this->vector = NULL;	
	
	this->SymmetricMatrixScalarProduct = 0;
	
	this->VoxelPosition[0] = -1;
	this->VoxelPosition[1] = -1;
	this->VoxelPosition[2] = -1;
	
	this->DeltaX = 1;
	this->DeltaY = 1;
	this->DeltaZ = 1;
}

//------------------------------------------------------------------------------------
void Derivative::SetVoxelSpacing(float x, float y, float z)
{
	this->DeltaX = x;
	this->DeltaY = y;
	this->DeltaZ = z;	
	
	if(this->vector)
		this->vector->SetVoxelSpacing(x, y, z);
}

//------------------------------------------------------------------------------------
void Derivative::SetVector(Vector *v)
{
	this->vector = v;
}

//------------------------------------------------------------------------------------
Vector * Derivative::GetVector()
{
	return this->vector;
}

//------------------------------------------------------------------------------------
Matrix* Derivative::GetMatrix() 
{
	return this->DerivMatrix;
}

//------------------------------------------------------------------------------------
Matrix* Derivative::GetSymmetricMatrix() 
{
	return this->DerivSymmetricMatrix;
}

//------------------------------------------------------------------------------------
void Derivative::ComputeSymmetricMatrix( )
{
	if(this->DerivMatrix)
		this->DerivSymmetricMatrix->ComputeSymmetricMatrix(this->DerivMatrix);	
}

//------------------------------------------------------------------------------------
void Derivative::ComputeMatrixInternal(char whichVector)
{
	if(debugMode) cout << " >> Derivative::ComputeMatrixInternal(char)" << endl;
		
	// Setting Derivative values
	float viA;  // v(i+1,j,k)
	float viB;  // v(i-1,j,k)
	float vjA;  // v(i,j+1,k)
	float vjB;  // v(i,j-1,k)
	float vkA;  // v(i,j,k+1)
	float vkB;  // v(i,j,k-1)
	
	int *vp = this->VoxelPosition;
	
	// Computing derivative matrix through the 'x' vector
	viA = this->vector->GetValue(whichVector, vp[0]+1, vp[1]  ,   vp[2]  , true);
	viB = this->vector->GetValue(whichVector, vp[0]-1, vp[1]  ,   vp[2]  , true);
	vjA = this->vector->GetValue(whichVector, vp[0]  , vp[1]+1,   vp[2]  , true);
	vjB = this->vector->GetValue(whichVector, vp[0]  , vp[1]-1,   vp[2]  , true);	
	vkA = this->vector->GetValue(whichVector, vp[0]  , vp[1]  ,   vp[2]+1, true);
	vkB = this->vector->GetValue(whichVector, vp[0]  , vp[1]  ,   vp[2]-1, true);	
		
	// Select the line of the matrix that will be altered
	int line;
	if(whichVector == 'x') line = 0; // D_Vx - First line
	if(whichVector == 'y') line = 1; // D_Vy - Second line
	if(whichVector == 'z') line = 2; // D_Vz - Third line				
		
	this->DerivMatrix->SetElement(line, 0, (viA - viB) / 2 * this->DeltaX);
	this->DerivMatrix->SetElement(line, 1, (vjA - vjB) / 2 * this->DeltaY);
	this->DerivMatrix->SetElement(line, 2, (vkA - vkB) / 2 * this->DeltaZ);	
}
//------------------------------------------------------------------------------------
void Derivative::ComputeMatrix()
{
	if(debugMode) cout << " >> Derivative::ComputeMatrix()" << endl;	
	if(!this->vector)
	{
		cout << "\nError in \"Derivative::ComputeMatrix() : Vector are not initiazed!" << endl;
		exit(1);
	}
	
	// Check just the first voxel
	if(this->VoxelPosition[0] == -1)
	{
		cout << "\nError in \"Derivative::ComputeMatrix() : Invalid Voxel Position!" << endl;
		exit(1);
	}
	
	// Setting Deltas
	this->vector->GetVoxelSpacing(this->DeltaX, this->DeltaY, this->DeltaZ);
	
	// Computing Matrix lines
	this->ComputeMatrixInternal('x');
	this->ComputeMatrixInternal('y');
	this->ComputeMatrixInternal('z');		
}

//------------------------------------------------------------------------------------
void Derivative::SetVoxelPosition(int i, int j, int k)
{
	this->VoxelPosition[0] = i;
	this->VoxelPosition[1] = j;
	this->VoxelPosition[2] = k;
}

//------------------------------------------------------------------------------------
void Derivative::SetVoxelPosition(int *pos)
{
	this->VoxelPosition[0] = pos[0];
	this->VoxelPosition[1] = pos[1];
	this->VoxelPosition[2] = pos[2];
}

//------------------------------------------------------------------------------------
void  Derivative::GetVoxelPosition(int *pos)
{
	pos[0] = this->VoxelPosition[0];
	pos[1] = this->VoxelPosition[1];
	pos[2] = this->VoxelPosition[2];
}

//------------------------------------------------------------------------------------
void Derivative::GetMatrixValues(float *output)
{
	this->DerivMatrix->GetAllElements(output);
}

//------------------------------------------------------------------------------------
void Derivative::GetSymmetricMatrixValues(float *output)
{
	this->DerivSymmetricMatrix->GetAllElements(output);
}

//------------------------------------------------------------------------------------
float Derivative::ComputeSymmetricScalarProduct()
{
	this->SymmetricMatrixScalarProduct = 0;
	
	for(int i = 0; i < 9; i++)
		this->SymmetricMatrixScalarProduct += this->DerivSymmetricMatrix->GetElement(i) * this->DerivSymmetricMatrix->GetElement(i);
	
	return this->SymmetricMatrixScalarProduct;	
}

//------------------------------------------------------------------------------------
float Derivative::ComputeSymmetricScalarProduct(float scalar)
{
	this->SymmetricMatrixScalarProduct = 0;
	
	for(int i = 0; i < 9; i++)
		this->SymmetricMatrixScalarProduct += this->DerivSymmetricMatrix->GetElement(i) * this->DerivSymmetricMatrix->GetElement(i);
	
	this->SymmetricMatrixScalarProduct *= scalar;
	
	return this->SymmetricMatrixScalarProduct;	
}

//------------------------------------------------------------------------------------
float Derivative::GetSymmetricScalarProduct()
{
	return this->SymmetricMatrixScalarProduct;	
}
