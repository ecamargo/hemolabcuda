/*
  Name:        derivative.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        23/10/2008
  Description: Gerencia as matrizes de derivada
*/

#include "matrix.h"
#include "vector.h"

#ifndef _Derivative_H_
#define _Derivative_H_

class Derivative
{
	public:	
		// Constructor
		Derivative();
		
		// Set Vector
		void SetVector(Vector *);
		Vector* GetVector();	
		
		// Set main Matrix
		void ComputeMatrix();
		
		// Copy Matrix Values
		void GetMatrixValues(float *);

		// Copy Matrix Values
		void GetSymmetricMatrixValues(float *);
		
		// Set/Get Voxel Number
		void SetVoxelPosition(int *);
		void SetVoxelPosition(int, int, int);		
		void GetVoxelPosition(int *);
		
		// Retrieve Matrices
		Matrix* GetMatrix();
		Matrix* GetSymmetricMatrix(); 
		
		// Compute Symmetric Matrix
		void ComputeSymmetricMatrix( );
		
		// Compute/Get Symmetric Scalar Product
		float ComputeSymmetricScalarProduct();
		float ComputeSymmetricScalarProduct(float scalar); // Multiply the result by float scalar		
		float GetSymmetricScalarProduct();
		
		// Set Deltas
		void SetVoxelSpacing(float x, float y, float z);
				
	protected:
		// Computes each line of the matrix
		// 'whichVector' can be 'x', 'y' or 'z' representing line 0, 1 and 2, respectively
		void ComputeMatrixInternal(char whichVector);			
				
   protected:
   	int VoxelPosition[3];
   	
   	// Voxel Spacing
   	int DeltaX, DeltaY, DeltaZ;
   	
   	Matrix *DerivMatrix;
   	Matrix *DerivSymmetricMatrix;   
   	Vector *vector;	
   	
   	float SymmetricMatrixScalarProduct;
   	
   	bool debugMode;
};


#endif /*_Derivative_H_*/
