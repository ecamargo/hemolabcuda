
#include "derivative.h"
#include "derivativeVector.h"
#include <iostream>
using namespace std;

//------------------------------------------------------------------------------------
DerivativeVector::DerivativeVector()
{
	this->derivVector = NULL;
	this->NumberOfVoxels = 0;
}

//------------------------------------------------------------------------------------				
void DerivativeVector::Allocate(int sizex, int sizey, int sizez)		
{
	this->Nx = sizex;
	this->Ny = sizey;
	this->Nz = sizez;
	this->NumberOfVoxels = sizex * sizey * sizez;	
	this->derivVector = new Derivative[this->NumberOfVoxels];
	
	int index;
	// Setting Voxel position for each allocated derivative
	for(int i = 0; i < this->Nx; i++)
		for(int j = 0; j < this->Ny; j++)
			for(int k = 0; k < this->Nz; k++)
			{
				index = k * (this->Nx)*(this->Ny) + j * (this->Nx) + i;				
				this->derivVector[index].SetVoxelPosition(i,j,k);
			}
}

//------------------------------------------------------------------------------------
void DerivativeVector::SetVoxelSpacing(float x, float y, float z)
{
	if(!this->NumberOfVoxels)
	{
		cout << "Warning in \"DerivativeVector::SetVoxelSpacing(float x, float y, float z)\": " << endl;
		cout << "Derivatives not allocated yet!" << endl;
		return;
	}
		
	// Setting derivative vector for each derivative
	for(int i = 0; i < this->NumberOfVoxels; i++)
		this->derivVector[i].SetVoxelSpacing(x, y, z);		
}

//------------------------------------------------------------------------------------
void DerivativeVector::SetVector(Vector *v)
{
	//this->vector = v;
	if(!this->NumberOfVoxels)
	{
		cout << "Warning in \"DerivativeVector::SetVector(Vector *v)\": " << endl;
		cout << "Derivatives not allocated yet!" << endl;
		return;
	}
	
	// Setting derivative vector for each derivative
	for(int i = 0; i < this->NumberOfVoxels; i++)
		this->derivVector[i].SetVector(v);	
}


//------------------------------------------------------------------------------------
void DerivativeVector::SetNumberOfVoxels(int num)
{
	this->NumberOfVoxels = num;
}

//------------------------------------------------------------------------------------		
int  DerivativeVector::GetNumberOfVoxels()		
{
	return this->NumberOfVoxels;	
}

//------------------------------------------------------------------------------------
Derivative* DerivativeVector::GetDerivative(int voxel)
{
	if(voxel < this->NumberOfVoxels)
		return &this->derivVector[voxel];
	return NULL;
}

//------------------------------------------------------------------------------------
Derivative* DerivativeVector::GetDerivative(int i, int j, int k)
{
	int index = k * (this->Nx)*(this->Ny) + j * (this->Nx) + i;
	return this->GetDerivative(index);
}
