/*
  Name:        derivativevector.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        10/11/2008
  Description: Gerencia vetor de derivatives. Classe gerencia apenas um volume
               e por isso o método 'SetVector' é utilizado para replicar vetor
               para todas as derivadas.
*/

#include "derivative.h"
#include "vector.h"

#ifndef _Derivative_Vector_H_
#define _Derivative_Vector_H_

class DerivativeVector
{
	public:	
		// Constructor
		DerivativeVector();
				
		// Allocate space for the derivative vector
		void Allocate(int sizex, int sizey, int sizez);		
		
		// Set/Get Number Of Voxels
		void SetNumberOfVoxels(int);
		int  GetNumberOfVoxels();	
		
		// Get derivative by voxel number or i,j,k coordinates
		Derivative* GetDerivative(int voxel);
		Derivative* GetDerivative(int i, int j, int k);

		// Set main vector for all derivatives
		void SetVector(Vector *v);
		
		// Spread voxel spacing to vectors and derivatives
		void SetVoxelSpacing(float x, float y, float z);
							
   protected:
		// Number Of Voxels
		int   NumberOfVoxels;
		
		// Vectors size
		int   Nx, Ny, Nz;			

   	// Derivative Vector
   	Derivative *derivVector;
};


#endif 
