
#include "incorReader.h"
#include <iostream>
#include <string>
using namespace std;

//-------------------------------------------------------------------------------------------
IncorReader::IncorReader()
{
	this->floatVecX = NULL;
	this->floatVecY = NULL;
	this->floatVecZ = NULL;
	
	this->pint   = NULL;
	this->pfloat = NULL;	
}

//-------------------------------------------------------------------------------------------
IncorReader::~IncorReader()
{
}

//-------------------------------------------------------------------------------------------
float* IncorReader::GetVector(char which)
{
	switch(which)
	{
		case 'x':
			return this->floatVecX;
		break;
		case 'y':
			return this->floatVecY;
		break;
		case 'z':
			return this->floatVecZ;
		break;		
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------
float* IncorReader::GetScalars()
{
	return this->pfloat;
}

//-------------------------------------------------------------------------------------------
void IncorReader::SetFullName(string &outputName, int i, string vectorPrefix, string vectorPosfix)
{
	char num[3];
	sprintf(num, "%d", i);
	outputName = vectorPrefix;
	outputName.append(num);
	outputName.append(vectorPosfix.c_str() );

}

//-------------------------------------------------------------------------------------------
void IncorReader::GetDimensions(int &sizex, int &sizey, int &sizez)
{
	sizex = this->sizex;
	sizey = this->sizey;
	sizez = this->sizez;
}

//-------------------------------------------------------------------------------------------
int* IncorReader::GetDimensions()
{
	int *dims = new int[3]; 
	dims[0] = this->sizex;
	dims[1] = this->sizey;
	dims[2] = this->sizez;
	return dims;
}

//-------------------------------------------------------------------------------------------		
void IncorReader::SetDimensions(int  sizex, int  sizey, int  sizez)	
{
	this->sizex = sizex;
	this->sizey = sizey;
	this->sizez = sizez;
}

//-------------------------------------------------------------------------------------------
int  IncorReader::GetNumberOfVolumes()
{
	return this->numberOfVolumes;
}

//-------------------------------------------------------------------------------------------		
void IncorReader::SetNumberOfVolumes(int v)	
{
	this->numberOfVolumes = v;
}

//-------------------------------------------------------------------------------------------
void IncorReader::ReleaseAllocatedVectors()
{
	if(this->floatVecX) delete []this->floatVecX;
	if(this->floatVecY) delete []this->floatVecY;
	if(this->floatVecZ) delete []this->floatVecZ;
	this->floatVecX = NULL;
	this->floatVecY = NULL;
	this->floatVecZ = NULL;	
}

//-------------------------------------------------------------------------------------------
int IncorReader::lerlinha(FILE *arq)
{
	char line[200];
	char dummy[200];
	int llength=200;
	int i,k;

	fgets(line,llength,arq);
	i=0; k=0;
	while (line[i]!='=') i++;
	i++;
	while (line[i]!='\0')
	{
		dummy[k]=line[i];
		k++; i++;
	}
	dummy[k]='\0';

	return(atoi(dummy));
}

//-------------------------------------------------------------------------------------------
short int IncorReader::readHeader(char *input_file, int *type, int *dim, 
											 int *nx, int *ny, int *nz, int *nt)
{
	char       filein[300];
	FILE       *pfile;

	strcpy(filein, input_file);
	strcat(filein,".thd");
	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);

	// read a blank line 
	lerlinha(pfile);

	// read file name 
	lerlinha(pfile);

	// read data type 
	*type=lerlinha(pfile);

	// read image dimension 
	*dim=lerlinha(pfile);

	// read x dims 
	*nx=lerlinha(pfile);

	// read y dims 
	*ny=lerlinha(pfile);

	// read z dims 
	*nz=lerlinha(pfile);

	// read t dims 
	*nt=lerlinha(pfile);

	fclose(pfile);
	return(0);
}

// Rotina para ler  um arquivo binario formato raw data   
short int IncorReader::readRaw (void *p, char *input_file, int type, int npixels)
{
	char       				filein[512];
	unsigned short int  	*pshort;
	int        				*pint;
	float						*pfloat;
	int        				numwriten;
	FILE       				*pfile;

	strcpy(filein, input_file);
	strcat(filein,".tim");

	printf("\n FILEIN = %s\n", filein);

	if (type==1) pint		= (int* ) p;
	if (type==2) pfloat	= (float *) p;
	if (type==3) pshort	= (unsigned short int*) p;
	
	if ((pfile = fopen (filein,"r")) == NULL)
		return(-1);
	
	printf("  * type = %d, npixels=%d\n",type,npixels);

	if (type==1)
	{
		if ((numwriten = fread(pint,sizeof(int),npixels,pfile)) != npixels)
			return(-2);

	}
	if (type==2)
	{
		if ((numwriten = fread(pfloat,sizeof(float),npixels,pfile)) != npixels)
			return(-2);
	}
	if (type==3)
	{
		if ((numwriten = fread(pshort,sizeof(unsigned short int),npixels,pfile)) != npixels)
			return(-2);
	}

	printf("  * File was successfully read\n");
	fclose(pfile);
	return(0);
}

int IncorReader::func_convert_endian(int number)
{
	int byte0, byte1, byte2, byte3;
	
	byte0 = (number & 0x000000FF) >> 0 ;
	byte1 = (number & 0x0000FF00) >> 8 ;
	byte2 = (number & 0x00FF0000) >> 16 ;
	byte3 = (number & 0xFF000000) >> 24 ;	

	return((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
}

float IncorReader::func_convert_endian( float bEnum )
{

	float lEnum;
	char *lE = (char*) &lEnum;
	char *bE = (char*) &bEnum;
	lE[0] = bE[3];
	lE[1] = bE[2];
	lE[2] = bE[1];
	lE[3] = bE[0];
	return lEnum;

}

int IncorReader::readTimScalars(string s)
{
	// ----> START OF USER-SUPPLIED CODE SECTION #3 (COMPUTE ROUTINE BODY)   
	const char *file_name = s.c_str();
	char filein[512];
	short int     codErro=0;	
	int 	      type, dims[3];
	int 	      imgdim,nlin,ncol,nslices=1,nvolumes=1,npixels;
	
	strcpy(filein, file_name);

	readHeader(filein, &type, &imgdim, &ncol, &nlin, &nslices, &nvolumes);

	cout << "\n\n-- Reading Tim Scalars ----------------------------------------------" << endl;
	printf(" Main Dimensions:\n  imgdim = %d \n  ncol = %d \n  nlin = %d  \n  nslices = %d \n  nvolumes = %d\n", imgdim,ncol,nlin,nslices,nvolumes);
	
	// type=1 -> integer; type=2 -> float 
	this->scalarType = type;     
     
	//reading image data on 'tim' file
	npixels = nlin*ncol*nslices;
	if (type==1) // integer type
	{
		if ((pint = (int *) calloc(npixels,sizeof (int))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pint,filein,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
		   return(codErro);
		}			
	}
		
	if (type==2) // float type 
	{
		if ((pfloat = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pfloat,filein,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
	}

	if (type==3) // short integer type
	{
		if ((pshort = (unsigned short int *) calloc(npixels,sizeof (short int))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		if ((codErro =  readRaw (pshort,filein,type,npixels)) != 0)
		{
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
		if ((pfloat = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		for (int i=0; i < npixels; i++)
			pfloat[i] = (float) pshort[i];
		free(pshort);
	}

	dims[0] = ncol;
	dims[1] = nlin;
	dims[2] = nslices/nvolumes;
	
	// Setting inner grid dimensions
	this->SetDimensions(dims[0], dims[1], dims[2]);
	
	// Setting number of Volumes
	this->SetNumberOfVolumes(nvolumes);
	
	// Converting int to float
	if(type != 2)	
	{
		// Fixing byte order for int
		for (int i=0; i < npixels; i++)
			pint[i] = func_convert_endian(pint[i]);				
		
		if ((pfloat = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory!\n");
			return(-5);
		}
		// Convert to float
		for (int i=0; i < npixels; i++)
			pfloat[i] = (float) pint[i];
		free(pint);
	}
	else
	{	
		// Fixing byte order for float
		for (int i=0; i < npixels; i++)
			pfloat[i] = func_convert_endian(pfloat[i]);
	}
		
	return(1);
}

// Read float (and only float) vector fields
// TODO: Inserir leitura de inteiros se necessário
int IncorReader::readTimVelocity(string s)
{
	string fileX = s;
	fileX.append("X");
	string fileY = s;
	fileY.append("Y");
	string fileZ = s;
	fileZ.append("Z");
	cout << "\n\n----------------------------------------------------------------------" << endl;
	cout << "Reading Files: " << endl;
	cout << fileX << ".*" << endl;
	cout << fileY << ".*" << endl;
	cout << fileZ << ".*" << endl;		
			
	// START OF USER-SUPPLIED CODE SECTION #3 (COMPUTE ROUTINE BODY)
	//const char *file_name = fileX.c_str(); // Leia apenas um, pois headers são iguais (ao menos para esse teste)
	char fileinX[512], fileinY[512], fileinZ[512];
	short int     codErro=0;
	//unsigned short int  *pshort;
	int 	      type, imgdim,nlin,ncol,nslices=1,nvolumes=1,npixels;//,dims[3],i,j,k,count=0;
	//strcpy(filein, file_name);

	strcpy(fileinX, (char *) fileX.c_str());
	strcpy(fileinY, (char *) fileY.c_str());
	strcpy(fileinZ, (char *) fileZ.c_str());
	
	// Leio apenas uma vez pois os dados são os mesmos (ao menos nesse teste) para todos 
	// os arquivos de velocidade (X, Y, Z)
	readHeader(fileinX, &type, &imgdim, &ncol, &nlin, &nslices, &nvolumes);
		
	// type=1 -> integer; type=2 -> float 
	printf("\n type = %d \n imgdim = %d \n ncol = %d \n nlin = %d  \n nslices = %d \n nvolumes = %d\n", type, imgdim,ncol,nlin,nslices,nvolumes);
	npixels = nlin*ncol*nslices;
	
	if(type == 1)
	{
		cout << "Error: This version is only capable of read FLOAT vector fields" << endl;
		return 0;
	}
	
	if ((this->floatVecX = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate X vector coords!\n");
			return(-5);
		}
	if ((codErro =  readRaw (this->floatVecX,fileinX,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
	
	if ((this->floatVecY = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate Y vector coords!\n");
			return(-5);
		}
	if ((codErro =  readRaw (this->floatVecY,fileinY,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
		
	if ((this->floatVecZ = (float *) calloc(npixels,sizeof (float))) == NULL)
		{  
			printf("Error!! Not Enough memory to allocate Z vector coords!\n");
			return(-5);
		}
	if ((codErro =  readRaw (this->floatVecZ,fileinZ,type,npixels)) != 0)
		{  
			printf("Error!! Error on routine ReadRaw!\n");				
			return(codErro);
		}
	
	// Convert to endian
	int p = 0;	
	for (int k = 0; k < this->sizez; k++)
	{
		for (int j = 0; j < this->sizey; j++)
		{
			for (int i = 0; i < this->sizex; i++)
			{
				this->floatVecX[p] = func_convert_endian(this->floatVecX[p]);
				this->floatVecY[p] = func_convert_endian(this->floatVecY[p]);
				this->floatVecZ[p] = func_convert_endian(this->floatVecZ[p]);
				p++;
			}
		}
	}				
	return 1;
}
