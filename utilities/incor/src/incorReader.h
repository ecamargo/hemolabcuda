/*
  Name:        incorReader.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        25/08/2008
  Description: Read incor raw header and data files
*/

#include <string>

using namespace std;

#ifndef INCORREADER_H_
#define INCORREADER_H_

class IncorReader
{
	public:	
		// Constructor/Destructor
		IncorReader();
		~IncorReader();
		
		// Read Scalars
		int readTimScalars(string s);
		
		// Read Tim Velocity data		
		int readTimVelocity(string s); 
				
		// Set/Get Grid Dimensions
		void GetDimensions(int &sizex, int &sizey, int &sizez);
		int* GetDimensions();
		void SetDimensions(int  sizex, int  sizey, int  sizez);	
		
		// Set/Get Number Of instances of the simulation (number of volumes)
		int  GetNumberOfVolumes();
		void SetNumberOfVolumes(int v);	
		
		// Set the name of the vector to be read based on an index, prefix and posfix
		// For example: Passing vectorPrefix = "eduardoVel-", i = 10 and vectorPosfix = _vel 
		// will result in "eduardoVel-10_vel" outputName		
		void SetFullName(string &outputName, int i, string vectorPrefix, string vectorPosfix);		
	 	
	 	// Release allocated velocity vectors
		void ReleaseAllocatedVectors();

		// Get Vector 'x', 'y' and 'z'		
		float* GetVector(char which);
		
		// Get pfloat vectors
		float* GetScalars();
		
   protected:
		int lerlinha(FILE *arq);
		short int readHeader(char *input_file, int *type, int *dim, int *nx, int *ny, int *nz, int *nt);
		short int readRaw (void *p, char *input_file, int type, int npixels);
		int   func_convert_endian( int number);
		float func_convert_endian( float bEnum );
		
	private:
		int sizex, sizey, sizez; 	// Data base dimensions
		int numberOfVolumes;			// Numbe Of instances of simulation (number of volumes)
		
		// Velocity Vectors (used in 'readTimVelocity)
		float *floatVecX;
		float *floatVecY;
		float *floatVecZ;
		
		// Scalars (used in 'readTimScalars)
		int	      *pint;
		float	      *pfloat;	
		unsigned short int  *pshort;		
		int 			scalarType;	
};


#endif /*INCORREADER_H_*/
