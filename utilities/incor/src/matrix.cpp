
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "matrix.h"



//------------------------------------------------------------------------------------------
Matrix::Matrix()
{
	this->Clear();
}

//------------------------------------------------------------------------------------------
void Matrix::Clear()
{
	for(int i = 0; i < 9; i++)
		this->values[i] = 0;		
}

//------------------------------------------------------------------------------------------
void Matrix::SetElement(int position, float value)
{
	if( position >= 0 && position < 9)
		this->values[position] = value;	
	else
		this->Error("Matrix::SetElement", 1);
}

//------------------------------------------------------------------------------------------
void Matrix::SetElement(int line, int column, float value)
{
	int position = line * 3 + column; // works only with 3 x 3 matrices
	this->SetElement(position, value);
}		

//------------------------------------------------------------------------------------------
float Matrix::GetElement(int position)
{
	if( position < 0 && position >=9)
		this->Error("Matrix::SetElement", 1);
	return this->values[position];		
}

//------------------------------------------------------------------------------------------
float Matrix::GetElement(int line, int column)
{
	int position = line * 3 + column; // works only with 3 x 3 matrices
	return this->GetElement(position);
}		

//------------------------------------------------------------------------------------------
void  Matrix::GetAllElements(float *output)
{
	for(int i = 0; i < 9; i++)
		output[i] = this->values[i];
}	

//------------------------------------------------------------------------------------------
void Matrix::ComputeTransposeMatrix( Matrix *input )
{
	this->SetElement(0, 0, input->GetElement(0, 0) );
	this->SetElement(0, 1, input->GetElement(1, 0) );
	this->SetElement(0, 2, input->GetElement(2, 0) );

	this->SetElement(1, 0, input->GetElement(0, 1) );
	this->SetElement(1, 1, input->GetElement(1, 1) );
	this->SetElement(1, 2, input->GetElement(2, 1) );

	this->SetElement(2, 0, input->GetElement(0, 2) );
	this->SetElement(2, 1, input->GetElement(1, 2) );
	this->SetElement(2, 2, input->GetElement(2, 2) );
}

//------------------------------------------------------------------------------------------
void Matrix::ComputeSymmetricMatrix( Matrix *input )
{
	Matrix *mt = new Matrix();
	
	// Clear Matrix
	this->Clear();
	
	// Copy input
	this->Sum(input);
	
	// Compute Transpose Matrix
	mt->ComputeTransposeMatrix(input);
	
	// Sum input + transpose
	this->Sum(mt);
	
	// Divide by 2
	this->Divide(2);		
	
	delete mt;
}

//------------------------------------------------------------------------------------------
void Matrix::ComputeInverseMatrix( Matrix *input )
{
	// NOT AVAILABLE YET
	// TODO: Implement inverse Matrix
}

//------------------------------------------------------------------------------------------
void Matrix::Multiply(float scalar)
{
	for(int i = 0; i < 9; i++)
		this->values[i] *= scalar;			
}

//------------------------------------------------------------------------------------------
void Matrix::Divide(float scalar)
{
	if(!scalar)
		Error("Matrix::Divide(float scalar)", 2);
	
	for(int i = 0; i < 9; i++)
		this->values[i] /= scalar;
}

//------------------------------------------------------------------------------------------
void Matrix::Sum( Matrix *m)
{
	for(int i = 0; i < 9; i++)
		this->values[i] += m->GetElement(i);				
}

//------------------------------------------------------------------------------------------
void Matrix::Error(string method, int error)
{
	cout << "Error in \"" << method << "\": ";
	switch(error)
	{
		case 1: cout << "Matrix out of range!" << endl;
		break;
		case 2: cout << "Division by zero!" << endl;
		break;		
	}
	exit(1);
}

//------------------------------------------------------------------------------------------
void Matrix::PrintSelf(ostream& os)
{
	os << fixed << setprecision(5);
	os << " _                                  _" << endl;
	os << "| " << setw(10) << this->values[0] << "  " 
				  << setw(10) << this->values[1] << "  " 
				  << setw(10) << this->values[2] << " |" << endl;
	os << "| " << setw(10) << this->values[3] << "  " 
				  << setw(10) << this->values[4] << "  " 
				  << setw(10) << this->values[5] << " |" << endl;
	os << "|_" << setw(10) << this->values[6] << "  " 
				  << setw(10) << this->values[7] << "  " 
				  << setw(10) << this->values[8] << "_|" << endl;	
}

//------------------------------------------------------------------------------------------
ostream& operator<<(ostream& os, Matrix& obj)
{
	obj.PrintSelf(os);
	return os;
}

