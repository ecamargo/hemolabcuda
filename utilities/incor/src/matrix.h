/*
  Name:        matrix.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        29/10/2008
  Description: Classe para gerenciar matrizes 3 x 3. Calcula Matriz transposta,
	matriz simétrica, limpa matriz etc. Tem funções para somar a 
	matriz corrente por outra, somar, multiplicar ou dividir por um 
	escalar.
*/

#include <string> 
using namespace std;

#ifndef _MATRIX_H_
#define _MATRIX_H_

class Matrix
{	
	public:	
		// Constructor
		Matrix();
		
		// Clear values
		void Clear();

		// Set/Get Elements
		void  SetElement(int position, float value);
		void  SetElement(int line, int column, float value);		
		float GetElement(int position);
		float GetElement(int line, int column);
		void  GetAllElements(float *);		

		// Compute the transpose matrix using the "input" matrix values  
		void ComputeTransposeMatrix( Matrix *input );

		// Compute the symmetric matrix using the "input" matrix values  
		void ComputeSymmetricMatrix( Matrix *input );
		
		// Compute the inverse Matrix using the "input" matrix values  
		void ComputeInverseMatrix( Matrix *input );	
		
		// Multiply by scalar
		void Multiply(float scalar);

		// Divide by scalar
		void Divide(float scalar);
		
		// Sum current matrix by 'm' element by element
		void Sum( Matrix *m);

		// Error table 		
		void Error(string method, int error);
		
		// Print Matrix (Use for example cout << m << endl;  // Where 'm' is Matrix *m = new Matrix(); 
		void PrintSelf(ostream& os);
		friend ostream& operator<<(ostream& os, Matrix& obj); 
		 			
   protected:
   	float values[9];
};

#endif /*_MATRIZ_H_*/
