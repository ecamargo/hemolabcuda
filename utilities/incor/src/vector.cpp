
#include "vector.h"
#include <iostream>
using namespace std;

//----------------------------------------------------------------------------------------
Vector::Vector()
{
	this->vx = NULL;
	this->vy = NULL;
	this->vz = NULL;
	
	this->DeltaX = this->DeltaY = this->DeltaZ = 1;
}

//----------------------------------------------------------------------------------------
Vector::~Vector()
{
	if(this->vx) delete []this->vx;
	if(this->vy) delete []this->vy;
	if(this->vz) delete []this->vz;
	
	this->vx = NULL;
	this->vy = NULL;
	this->vz = NULL;
}

//----------------------------------------------------------------------------------------
Vector::Vector(int sizex, int sizey, int sizez)
{
	this->DeltaX = this->DeltaY = this->DeltaZ = 1;
	
	this->Allocate(sizex, sizey, sizez);
}

//----------------------------------------------------------------------------------------
void Vector::Allocate(int *size)
{
	this->Allocate(size[0], size[1], size[2]);
}

//----------------------------------------------------------------------------------------
void Vector::Allocate(int sizex, int sizey, int sizez)
{
	this->Nx = sizex;
	this->Ny = sizey;
	this->Nz = sizez;

	// Cada vetor (vx, vy e vz) vai ter o tamanho de sizex * sizey * sizez, pois 
	// os vetores armazenam os valores de x, y e z em cada voxel.	
	this->vx = new float[this->Nx * this->Ny * this->Nz];
	this->vy = new float[this->Nx * this->Ny * this->Nz];
	this->vz = new float[this->Nx * this->Ny * this->Nz];
}

//----------------------------------------------------------------------------------------
void Vector::IsInitialized()
{
	if(!this->vx || !this->vy || !this->vz)
	{
		cout << "\nError in \"Vector::CopyValues(...) : Vector are not initiazed!" << endl;
		exit(1);
	}	
}

//----------------------------------------------------------------------------------------
void Vector::SetVoxelSpacing(int dx, int dy, int dz)
{
	this->DeltaX = dx;
	this->DeltaY = dy;
	this->DeltaZ = dz;
}

//----------------------------------------------------------------------------------------
void Vector::GetVoxelSpacing(int &dx, int &dy, int &dz)
{
	dx = this->DeltaX;
	dy = this->DeltaY;
	dz = this->DeltaZ;
}

//----------------------------------------------------------------------------------------
void Vector::CopyValues(char whichVector, float *input)
{
	this->IsInitialized();
	
	int size = this->Nx * this->Ny * this->Nz;
	switch(whichVector)
	{
		case 'x':
			for(int i = 0; i < size; i++)
				this->vx[i] = input[i];
		break;
		case 'y':
			for(int j = 0; j < size; j++)
				this->vy[j] = input[j];
		break;
		case 'z':
			for(int k = 0; k < size; k++)
				this->vz[k] = input[k];
		break;			
	}
}

//----------------------------------------------------------------------------------------
float Vector::GetValue(char vector, int i, int j, int k, bool enableOutOfRangeException)
{
	this->IsInitialized();	
	
	if(!enableOutOfRangeException) // Default
	{		
		// Only positive index are allowed
		if( i < 0 || j < 0 || k < 0 )
		{
			cout << "\nError in \"Vector::GetValue(...) : Trying to access a negative index!" << endl;
			exit(1);
		}	
		
		// Checking range
		if( !(i < this->Nx && j < this->Ny && k < this->Nz) )
		{
			cout << "\nError in \"Vector::GetValue(...) : Acessing Vector index Out of Range!" << endl;
			exit(1);
		}
	}
	else
	{
		// Get near values when on the border
		if(i < 0) i = 1;
		if(j < 0) j = 1;
		if(k < 0) k = 1;
		if(i > Nx-1) i = Nx - 2;
		if(j > Ny-1) j = Ny - 2;
		if(k > Nz-1) k = Nz - 2;				
	}
	int index = k * (this->Nx)*(this->Ny) + j * (this->Nx) + i;
	float value = 0.0f;
	switch(vector)
	{
		case 'x':
			value = this->vx[ index ];
		break;
		case 'y':
			value = this->vy[ index ];
		break;
		case 'z':
			value = this->vz[ index ];
		break;			
	}
	return value;
}

//----------------------------------------------------------------------------------------
float* Vector::GetComponentVector(char vector)
{
	this->IsInitialized();	
	
	switch(vector)
	{
		case 'x': return this->vx;
		break;
		case 'y': return this->vy;
		break;
		case 'z': return this->vz;
		break;			
	}
	return NULL;
}
