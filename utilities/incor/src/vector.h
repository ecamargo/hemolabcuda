/*
  Name:        vector.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        03/11/2008
  Description: Classe para gerenciar vetores
*/

#ifndef _VECTOR_H_
#define _VECTOR_H_

class Vector
{
	public:
	
		// Constructor and Destructors
		Vector();
		~Vector();
		
		// Constructor that allocates the vector with the parameters
		Vector(int sizex, int sizey, int sizez);
		
		// Check if the vectors are initialized or not
		void IsInitialized();		
		
		// Allocate space for all the vector vx, vy and vz
		void Allocate(int sizex, int sizey, int sizez);
		void Allocate(int *size);		
		
		// Get value stored on the 'whichvector' ('x', 'y' or 'z') on a i, j and k of the plain vector
		// The 'enableOutOfRangeException' is used when the border treatment must be on for derivative calculation  
		float GetValue(char whichVector, int i, int j, int k, bool enableOutOfRangeException = false);
		
		// Copy values on the 'whickvector' ('x', 'y' or 'z') of the input array		
		void CopyValues(char whichVector, float *input);
		
		// Return the pointer to a vector (vx, vy and vz)
		float* GetComponentVector(char vector);
		
		// Set/Get Voxel Spacing
		void SetVoxelSpacing(int dx, int dy, int dz);
		void GetVoxelSpacing(int  &, int  &, int  &);
		
	public:
		// Main vectors
		float *vx, *vy, *vz;
		
		// Voxel Spacing
		int DeltaX, DeltaY, DeltaZ;
		
		// Vectors size
		int   Nx, Ny, Nz;	
};

#endif
