
#include "vectorField.h"
#include <iostream>
using namespace std;

//----------------------------------------------------------------------------------------
VectorField::VectorField()
{
	this->vector = NULL;
}

//----------------------------------------------------------------------------------------
VectorField::VectorField(int t)		
{
	this->vector = NULL;
	this->SetNumberOfVolumes(t);
}

//----------------------------------------------------------------------------------------
VectorField::~VectorField()
{
	if(this->vector)
		delete []this->vector;
	this->numberOfVolumes = 0;	
}

//----------------------------------------------------------------------------------------
void VectorField::Allocate()
{
	if(!this->numberOfVolumes)
	{
		cout << "\nError in \"VectorField::Allocate() : You must set the number of Volumes first!" << endl;
		exit(1);
	}	
	
	if(this->vector) delete []this->vector;
	
	// Allocating vectors
	this->vector = new Vector[ this->numberOfVolumes ];	
}

//----------------------------------------------------------------------------------------
void VectorField::SetNumberOfVolumes(int t)
{
	this->numberOfVolumes = t;
	this->Allocate();
}

//----------------------------------------------------------------------------------------
int  VectorField::GetNumberOfVolumes()
{
	return this->numberOfVolumes;
}

//----------------------------------------------------------------------------------------
void VectorField::SetVolumeRange(int sizex, int sizey, int sizez)
{
	this->sizex = sizex;
	this->sizey = sizey;
	this->sizez = sizez;
	
	// Allocate range for vectors in each time step
	for(int t = 0; t < this->numberOfVolumes; t++)
	{
		this->GetVector(t)->Allocate(sizex, sizey, sizez);
	}
}

//----------------------------------------------------------------------------------------
void VectorField::GetVolumeRange(int &sizex, int &sizey, int &sizez)
{
	sizex = this->sizex; 
	sizey = this->sizey;
	sizez	= this->sizez;
}

//-------------------------------------------------------------------------------------------
int* VectorField::GetVolumeRange()
{
	int *dims = new int[3]; 
	dims[0] = this->sizex;
	dims[1] = this->sizey;
	dims[2] = this->sizez;
	return dims;
}

//----------------------------------------------------------------------------------------		
Vector* VectorField::GetVector(int t)
{
	if(!this->vector)
	{
		cout << "\nError in \"VectorField::GetVector(int t) : Vectors are not initiazed!" << endl;
		exit(1);		
	}
	
	if(this->numberOfVolumes < t)
	{
		cout << "\nError in \"VectorField::GetVector(int t) : Trying to access out of range time step " << t << " !" << endl;
		exit(1);
	}	
	
	return &this->vector[t];
}
