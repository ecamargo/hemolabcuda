/*
  Name:        vectorfields.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        31/10/2008
  Description: Classe para gerenciar campos de vetores. Cada vetor possui um subvetor
  x, y e z.  A idéia da classe é armazenar um vetor (com 3 componentes) para
  cada instante de tempo.  Portanto, se a simulação contiver 10 instantes de tempo,
  teremos 10 vetores alocados nesta classe.
*/

#include "vector.h"

#ifndef _VECTORFIELD_H_
#define _VECTORFIELD_H_

class VectorField
{
	public:	
		// Constructors and Destructors
		VectorField();
		VectorField(int );		
		~VectorField();
			
		// Set/Get number of volumes
		void SetNumberOfVolumes(int t);
		int  GetNumberOfVolumes();
		
		// Set the volume range to compute each vector size
		void SetVolumeRange(int  sizex, int  sizey, int  sizez);
		void GetVolumeRange(int &sizex, int &sizey, int &sizez);	
		int* GetVolumeRange();

		// Get Vector in time t
		Vector* GetVector(int t);
	
		// Allocates space for vectors
		void Allocate();
	
   protected:
   	// Number of Volumes
   	int numberOfVolumes;
   	
   	// Vectors representing each instante of the simulation
   	Vector *vector;
   	
   	// Volume Range
   	int sizex, sizey, sizez;
};


#endif
