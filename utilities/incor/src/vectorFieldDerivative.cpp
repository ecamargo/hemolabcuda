#include <iostream>
using namespace std;

#include "vectorFieldDerivative.h"

//----------------------------------------------------------------------------------------
VectorFieldDerivative::VectorFieldDerivative()
	: VectorField()
{
	this->debugMode = false;
	this->vector = NULL;
	this->derivVec = NULL;
	this->floatVecX = NULL;
	this->floatVecY = NULL;
	this->floatVecZ = NULL;
	this->scalarProductVec = NULL;
	this->scalarVec = NULL;	
	this->rho  		= 1;
	this->DeltaX	= 1;
	this->DeltaY	= 1;
	this->DeltaZ	= 1;	
}

//----------------------------------------------------------------------------------------
VectorFieldDerivative::VectorFieldDerivative(int t)	
{
	this->debugMode = false;		
	this->vector = NULL;
	this->derivVec = NULL;
	this->floatVecX = NULL;
	this->floatVecY = NULL;
	this->floatVecZ = NULL;
	this->scalarVec = NULL;
	this->rho  		= 1;
	this->DeltaX	= 1;
	this->DeltaY	= 1;
	this->DeltaZ	= 1;
		
	this->SetNumberOfVolumes(t);	
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetRho(float r)
{
	this->rho  		= r;
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetVoxelSpacing(float x, float y, float z)
{
	this->DeltaX	= x;
	this->DeltaY	= y;
	this->DeltaZ	= z;		
}

//----------------------------------------------------------------------------------------
VectorFieldDerivative::~VectorFieldDerivative()
{
	if(this->derivVec)  delete [] this->derivVec;
	this->ReleaseAllocatedVectors();
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ReleaseAllocatedVectors()
{
	if(this->floatVecX) delete [] this->floatVecX;
	if(this->floatVecY) delete [] this->floatVecY;
	if(this->floatVecZ) delete [] this->floatVecZ;
	this->floatVecX = NULL;
	this->floatVecY = NULL;
	this->floatVecZ = NULL;
	
	if(this->scalarProductVec) delete [] this->scalarProductVec;
	this->scalarProductVec = NULL;
	
	if(this->scalarVec) delete [] this->scalarVec;
	this->scalarVec = NULL;	
}

//-------------------------------------------------------------------------------------------
float* VectorFieldDerivative::GetMatrixLineVector(char which)
{
	switch(which)
	{
		case 'x': return this->floatVecX;
		break;
		case 'y': return this->floatVecY;
		break;
		case 'z': return this->floatVecZ;
		break;		
	}
	return NULL;
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetVolumeRange(int sizex, int sizey, int sizez)
{
	if(this->debugMode) cout << " --> VectorFieldDerivative::SetVolumeRange(int sizex, int sizey, int sizez)" << endl;	
	VectorField::SetVolumeRange(sizex, sizey, sizez);
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetVolumeRange(int *size)
{
	if(this->debugMode) cout << " --> VectorFieldDerivative::SetVolumeRange(int *sizex)" << endl;	
	VectorField::SetVolumeRange(size[0], size[1], size[2]);
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetNumberOfVolumes(int v)
{
	if(this->debugMode) cout << " --> VectorFieldDerivative::SetNumberOfVolumes(int v)" << endl;	
	VectorField::SetNumberOfVolumes(v);	
	//this->Allocate();	
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::Allocate()
{
	if(this->debugMode) cout << " --> void VectorFieldDerivative::Allocate()" << endl; 
	VectorField::Allocate();
	
	// Allocating Derivative
	this->derivVec = new DerivativeVector [ this->numberOfVolumes ];		
		
	// Setting the vectorFields vectors references the inner derivative vectors pointers 
	for(int v = 0; v < this->GetNumberOfVolumes(); v++)
	{
		this->derivVec[v].Allocate(sizex, sizey, sizez);		
		this->derivVec[v].SetVector( this->GetVector(v) );
		
		this->derivVec[v].SetVoxelSpacing(this->DeltaX, this->DeltaY, this->DeltaZ);
	}
	
	// Allocating scalarValues
	this->scalarProductVec = new float[sizex*sizey*sizez];
	
	// Allocating scalarValues	
	this->scalarVec = new float[this->sizex * this->sizey * 
										 this->sizez * this->numberOfVolumes ];			
}

//----------------------------------------------------------------------------------------
DerivativeVector* VectorFieldDerivative::GetDerivativeVector(int volume)
{
	if(volume >= this->GetNumberOfVolumes())
	{
		cout << "Error in \"VectorFieldDerivative::GetVectorFieldDerivative()\": ";
		cout << "Trying to Access an invalid volume." << endl;
		exit(1);
	}
	return &this->derivVec[volume];
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeAllSymmetricMatrixScalarProducts()
{
	if(this->debugMode) cout << " >> VectorFieldDerivative::ComputeAllSymmetricMatrixScalarProducts()" << endl;
		
	for(int v = 0; v < this->GetNumberOfVolumes(); v++)
		for(int z = 0; z < this->sizez; z++)
			for(int y = 0; y < this->sizey; y++)
				for(int x = 0; x < this->sizex; x++)
				{
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->SetVoxelPosition(x, y, z);
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeSymmetricScalarProduct();			
				}				
}

/*
//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeAllSymmetricMatrixScalarProductsAndMultiplyScalars()
{
	if(this->debugMode) cout << " >> VectorFieldDerivative::ComputeAllSymmetricMatrixScalarProductsAndMultiScalars()" << endl;
		
	int p = 0;
	for(int v = 0; v < this->GetNumberOfVolumes(); v++)
		for(int z = 0; z < this->sizez; z++)
			for(int y = 0; y < this->sizey; y++)
				for(int x = 0; x < this->sizex; x++)
				{
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->SetVoxelPosition(x, y, z);
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeSymmetricScalarProduct(this->scalarVec[p]);
					p++;			
				}				
}
*/

//----------------------------------------------------------------------------------------
float VectorFieldDerivative::ComputeSymmetricMatrixScalarProductsSum(int v)
{
	float sum = 0;
	if(this->debugMode) cout << " >> VectorFieldDerivative::ComputeAllSymmetricMatrixScalarProductsSum()" << endl;
		
	for(int z = 0; z < this->sizez; z++)
		for(int y = 0; y < this->sizey; y++)
			for(int x = 0; x < this->sizex; x++)
			{
				this->GetDerivativeVector(v)->GetDerivative(x,y,z)->SetVoxelPosition(x, y, z);
				sum+=this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeSymmetricScalarProduct();			
			}
	sum *= this->rho* this->DeltaX * this->DeltaY * this->DeltaZ;			
	return sum;		
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeSymmetricMatrixScalarProductsSum( float *result )
{
	float sum = 0;
	if(this->debugMode) cout << " >> VectorFieldDerivative::ComputeSymmetricMatrixScalarProductsSum( float *result )" << endl;

	int p = 0;
	for(int v = 0; v < this->GetNumberOfVolumes(); v++)
	{
		for(int z = 0; z < this->sizez; z++)
			for(int y = 0; y < this->sizey; y++)
				for(int x = 0; x < this->sizex; x++)
				{
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->SetVoxelPosition(x, y, z);
					sum+=this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeSymmetricScalarProduct(this->scalarVec[p]);
					p++;		
				}
		sum *= this->rho* this->DeltaX * this->DeltaY * this->DeltaZ;				
		result[v] = sum;
		sum = 0;
	}
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::SetScalars(float *inputScalars)
{
	if(this->debugMode) cout << " >> VectorFieldDerivative::SetScalars(float *inputScalars)" << endl;
		
	int size = this->sizex*this->sizey*this->sizez*this->numberOfVolumes;
		
	if(!this->scalarVec)
		this->scalarVec = new float[size];
	
	for (int i = 0; i < size; i++)
		this->scalarVec[i] = inputScalars[i]; 
}

//----------------------------------------------------------------------------------------
float* VectorFieldDerivative::GetScalarProductVector(int volume)
{
	if(this->debugMode) cout << " >> VectorFieldDerivative::GetScalarProductVector()" << endl;
		
	if(!this->scalarProductVec)
		this->scalarProductVec = new float[this->sizex*this->sizey*this->sizez];
	
	int p = 0;
	for (int k = 0; k < this->sizez; k++)
		for (int j = 0; j < this->sizey; j++)
			for (int i = 0; i < this->sizex; i++)
			{
				this->scalarProductVec[p] = this->GetDerivativeVector(volume)->GetDerivative(i,j,k)->GetSymmetricScalarProduct();
				p++;
			}
	return this->scalarProductVec;
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeAllDerivativeMatrices()
{
	if(this->debugMode) cout << " >> VectorFieldDerivative::ComputeAllDerivativeMatrices()" << endl;
		
	for(int v = 0; v < this->GetNumberOfVolumes(); v++)
	{
		for(int z = 0; z < this->sizez; z++)
		{
			for(int y = 0; y < this->sizey; y++)
			{
				for(int x = 0; x < this->sizex; x++)
				{
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->SetVoxelPosition(x, y, z);
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeMatrix();			
					this->GetDerivativeVector(v)->GetDerivative(x,y,z)->ComputeSymmetricMatrix();				
				}				
			}			 
		}
	}
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeMatrixLineValues(int whichLine, int volume)
{
	// Setting first matrix index
	int index;
	if(whichLine == 0) index = 0; // int matrix[9] = { 1, 5, 7, 9, 4, 7, 1, 2, 0}  
	if(whichLine == 1) index = 3; //                   ^        ^        ^
	if(whichLine == 2) index = 6; //     índex    ->   0        3        6
	 
	this->floatVecX = new float[sizex*sizey*sizez];
	this->floatVecY = new float[sizex*sizey*sizez];
	this->floatVecZ = new float[sizex*sizey*sizez];		
	 
	int pos = 0;
	float matrixValues[9];
	for (int k = 0; k < this->sizez; k++)
		for (int j = 0; j < this->sizey; j++)
			for (int i = 0; i < this->sizex; i++)
			{
				this->GetDerivativeVector(volume)->GetDerivative(i,j,k)->GetMatrix()->GetAllElements(matrixValues);
				// First line of the matrix (DvX)
				this->floatVecX[pos] = matrixValues[index  ];
				this->floatVecY[pos] = matrixValues[index+1];
				this->floatVecZ[pos] = matrixValues[index+2];		
				pos++;				
			}
}

//----------------------------------------------------------------------------------------
void VectorFieldDerivative::ComputeSymmetricMatrixLineValues(int whichLine, int volume)
{
	// Setting first matrix index
	int index;
	if(whichLine == 0) index = 0; // int matrix[9] = { 1, 5, 7, 9, 4, 7, 1, 2, 0}  
	if(whichLine == 1) index = 3; //                   ^        ^        ^
	if(whichLine == 2) index = 6; //     índex    ->   0        3        6
	 
	this->floatVecX = new float[sizex*sizey*sizez];
	this->floatVecY = new float[sizex*sizey*sizez];
	this->floatVecZ = new float[sizex*sizey*sizez];		
	 
	int pos = 0;
	float matrixValues[9];
	for (int k = 0; k < this->sizez; k++)
		for (int j = 0; j < this->sizey; j++)
			for (int i = 0; i < this->sizex; i++)
			{
				this->GetDerivativeVector(volume)->GetDerivative(i,j,k)->GetSymmetricMatrix()->GetAllElements(matrixValues);
				// First line of the matrix (DvX)
				this->floatVecX[pos] = matrixValues[index  ];
				this->floatVecY[pos] = matrixValues[index+1];
				this->floatVecZ[pos] = matrixValues[index+2];		
				pos++;				
			}
}

//-------------------------------------------------------------------------------------------
float* VectorFieldDerivative::GetVectorComponents(char which, int volume)
{
	
	if(!this->floatVecX && which == 'x') this->floatVecX = new float[sizex*sizey*sizez];
	if(!this->floatVecY && which == 'y') this->floatVecY = new float[sizex*sizey*sizez];
	if(!this->floatVecZ && which == 'z') this->floatVecZ = new float[sizex*sizey*sizez];
			
	float value;		
	int pos = 0;
	for (int k = 0; k < this->sizez; k++)
		for (int j = 0; j < this->sizey; j++)
			for (int i = 0; i < this->sizex; i++)
			{
				value = this->GetDerivativeVector(volume)->GetDerivative(i,j,k)->GetVector()->GetValue(which, i,j,k);
				if(which == 'x') this->floatVecX[pos] = value;
				if(which == 'y') this->floatVecY[pos] = value;
				if(which == 'z') this->floatVecZ[pos] = value;				 				
				pos++;
			}
	if(which == 'x') return this->floatVecX;
	if(which == 'y') return this->floatVecY;
	if(which == 'z') return this->floatVecZ;
	
	// Just in case...
	return NULL;
}
