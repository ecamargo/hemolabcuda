/*
  Name:        vectorFieldsDerivative.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        10/11/2008
  Description: Classe herdando de 'vectorField' que além de geren gerenciar campos de vetores, também 
  gerencia objetos 'derivatives'. Cada vetor possui um subvetor
  x, y e z.  A idéia da classe é armazenar um vetor (com 3 componentes) para
  cada instante de tempo.  Portanto, se a simulação contiver 10 instantes de tempo,
  teremos 10 vetores alocados nesta classe.
*/

#include "vectorField.h"
#include "derivativeVector.h"

#ifndef _VECTORFIELD_DERIVATIVE_H_
#define _VECTORFIELD_DERIVATIVE_H_

class VectorFieldDerivative : public VectorField
{
	
	//--- Methods ----------------------------------------	
	public:	
		// Constructors and Destructors
		VectorFieldDerivative();
		VectorFieldDerivative(int );		
		~VectorFieldDerivative();
			
		// Get DerivativeVector of volume 'v'
		DerivativeVector* GetDerivativeVector(int v);			
			
		// Set/Get number of volumes
		void SetNumberOfVolumes(int v);
		
		// Set the volume range to compute each vector size
		void SetVolumeRange(int sizex, int sizey, int sizez);	
		void SetVolumeRange(int *size);		

		// Compute all derivative matrices (normal and symmetric) for all volumes		
		void ComputeAllDerivativeMatrices();

		// Compute all scalar Products based on the symmetric matrix results		
		void ComputeAllSymmetricMatrixScalarProducts();

		// Compute all scalar Products OF ONE VOLUME based on the symmetric matrix 
		// results and returns the sum of all of it.  		
		float ComputeSymmetricMatrixScalarProductsSum(int volume = 0);

		// Compute all scalar Products based on the symmetric matrix 
		// results and store the sum of all of it in the "result" vec (each index is a volume).  			
		void ComputeSymmetricMatrixScalarProductsSum( float *result );

		// Allocates space for vectors
		void Allocate();
		
		// Release allocated vectors
		void ReleaseAllocatedVectors();

		// Compute regular or symmetric values for each lines. 'whichline'
		// can be 0, 1 or 2.  The values are stored in the floatVecX, floatVecY
		// and floatVecZ arrays.
		void ComputeMatrixLineValues(int whichLine, int volume);
		void ComputeSymmetricMatrixLineValues(int whichLine, int volume);	

		// Get Vector 'x', 'y' and 'z'		
		float* GetMatrixLineVector(char which);		

		// Populates and retrieve pre calculated scalars products
		float* GetScalarProductVector(int volume = 0);
		
		// Vector component (x, y and z). For all vectors, "floatVec" is used;
		float* GetVectorComponents(char which, int volume = 0);
								
		// Set Rho and Deltas
		void SetRho(float r);
		void SetVoxelSpacing(float x, float y, float z);	
		
		// Set scalars values of all volumes ( !!! must check this description !!! )
		void SetScalars(float *inputScalars);							
								
   protected:
		// Derivative Vector
   	DerivativeVector *derivVec; 
   	
   	// Debug mode
   	bool debugMode;
   	
   	// Used to stores values from matrix, line by line
		float *floatVecX;
		float *floatVecY;
		float *floatVecZ;

		// Scalars (falta completar descrição)		
		float *scalarVec;	
		
		// Symmetric Matrix scalar product vector (A.A sendo A a matriz)
		float *scalarProductVec;
		
		// Complementary Data passed by the user. Default Values are 1.
		float rho;
		float DeltaX;
		float DeltaY;
		float DeltaZ;
};

#endif
