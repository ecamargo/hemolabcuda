
#include "vtkBridge.h"
#include "vtkDataSetMapper.h"
#include "vtkDataSetWriter.h"
#include "vtkStructuredGrid.h"
#include "vtkPointData.h"
#include "vtkIntArray.h"
#include "vtkFloatArray.h"
#include "vtkPolyData.h"

#include "vtkDataObject.h"
#include "vtkFieldData.h"
#include "vtkXYPlotActor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkTextProperty.h"
#include "vtkProperty2D.h"

#include <iostream>
#include <string>
using namespace std;

//-------------------------------------------------------------------------------------------
vtkBridge::vtkBridge()
{
	this->grid = vtkStructuredGrid::New();
	this->IsVTKReady = false;
}

//-------------------------------------------------------------------------------------------
vtkBridge::~vtkBridge()
{
	if(grid) grid->Delete();
}

//-------------------------------------------------------------------------------------------
vtkStructuredGrid* vtkBridge::GetGrid()
{
	return this->grid;
}

//-------------------------------------------------------------------------------------------
void vtkBridge::Write(string FileName)
{
	vtkDataSetWriter *writer = vtkDataSetWriter::New();
		writer->SetInput(this->grid);
		writer->SetFileName(FileName.c_str());
		writer->Write();
		
	cout << "\n\nFile \"" << FileName << "\" successfully generated!\n\n";
}

//-------------------------------------------------------------------------------------------
void vtkBridge::SetDimensions(int sizex, int sizey, int sizez)
{	
	int i,j,k;
	vtkPoints *xyzpoints = vtkPoints::New();
	
	for (k = 0; k < sizez; k++)
		for (j = 0; j < sizey; j++)
			for (i = 0; i < sizex; i++)
				xyzpoints->InsertNextPoint(i,j,k);
				
	grid->SetDimensions(sizex, sizey, sizez);
	grid->SetPoints(xyzpoints); 
	xyzpoints->Delete();
	
	// Class is ready to receive vectors and scalars	
	this->IsVTKReady = true;
}

//-------------------------------------------------------------------------------------------
void vtkBridge::SetMultipleScalarsToVTK(string prefix, int nvolumes, int *dims, float *pfloat)
{		
	if(!this->IsVTKReady) this->SetDimensions(dims[0], dims[1], dims[2]);	

	vtkFloatArray *array;
	char scalarName[256];
	int p = 0;
	for (int n = 0; n < nvolumes; n++)
	{
		array = vtkFloatArray::New();
		array->SetNumberOfComponents(1);
		sprintf(scalarName, "%s%d", prefix.c_str(), n);	
		array->SetName(scalarName);
		for (int k = 0; k < dims[2]; k++)
			for (int j = 0; j < dims[1]; j++)
				for (int i = 0; i < dims[0]; i++)
				{
					array->InsertNextValue( pfloat[p]  ); 
					p++;
				}
		grid->GetPointData()->AddArray(array);
		array->Delete();			
	}
}


//-------------------------------------------------------------------------------------------
void vtkBridge::SetScalarsToVTK(string prefix, int volume, int *dims, float *pfloat)
{		
	if(!this->IsVTKReady) this->SetDimensions(dims[0], dims[1], dims[2]);	

	vtkFloatArray *array;
	char scalarName[256];
	int p = 0;
	
	array = vtkFloatArray::New();
	array->SetNumberOfComponents(1);
	sprintf(scalarName, "%s%d", prefix.c_str(), volume);	
	array->SetName(scalarName);
	for (int k = 0; k < dims[2]; k++)
		for (int j = 0; j < dims[1]; j++)
			for (int i = 0; i < dims[0]; i++)
			{
				array->InsertNextValue( pfloat[p]  ); 
				p++;
			}
	grid->GetPointData()->AddArray(array);
	array->Delete();			

}

//-------------------------------------------------------------------------------------------
void vtkBridge::SetVectorsToVTK(string prefix, int volume, int* dims, float* floatVecX, float* floatVecY, float* floatVecZ)
{
	if(!this->IsVTKReady) this->SetDimensions(dims[0], dims[1], dims[2]);
		
	vtkFloatArray *array;
	char scalarName[256];
	int p = 0;
	float value[3];

	array = vtkFloatArray::New();
	array->SetNumberOfComponents(3);
	sprintf(scalarName, "%s%d", prefix.c_str(), volume);	
	array->SetName(scalarName);
	for (int k = 0; k < dims[2]; k++)
		for (int j = 0; j < dims[1]; j++)
			for (int i = 0; i < dims[0]; i++)
			{
				value[0] = floatVecX[p];
				value[1] = floatVecY[p];
				value[2] = floatVecZ[p];				
				array->InsertNextTupleValue(value); 
				p++;
			}

	grid->GetPointData()->AddArray(array);
	array->Delete();			
}

//-------------------------------------------------------------------------------------------
void vtkBridge::GenerateXYPlot(int numberOfXLabels, float *values)
{
	cout << "Numero de Labels: " << numberOfXLabels << endl;
	// Preparing input
	vtkFloatArray *array = vtkFloatArray::New();
		array->SetNumberOfComponents(1);			
		array->SetName("scalar");
	for (int n = 0; n < numberOfXLabels; n++)
		array->InsertNextValue( values[n]  );		
	
	vtkDataObject *DataObject = vtkDataObject::New();
	vtkFieldData *FieldData = vtkFieldData::New();
	FieldData->AddArray(array);
	DataObject->SetFieldData(FieldData);
  	 	
	cout << "Setting up the plot..." << endl;
	vtkXYPlotActor *plot = vtkXYPlotActor::New();
  		plot->AddDataObjectInput(DataObject);	
  		plot->SetPlotColor(0, 1.0, 0.0, 0.0);
    	plot->GetPositionCoordinate()->SetValue( 0.05, 0.05, 0); //
    	plot->GetPosition2Coordinate()->SetValue( 0.95, 0.95, 0);   //
  		plot->SetTitle("Incor Scalar Product 01");
  		plot->SetXTitle("Time");
  		plot->SetYTitle("Normalized Value");
  		plot->SetXRange(0, numberOfXLabels);
		plot->GetProperty()->SetColor( 0.0, 0.0, 0.0); // Axis Color
		plot->GetProperty()->SetLineWidth( 2);
		plot->SetLabelFormat("%g");
		vtkTextProperty* tprop = plot->GetTitleTextProperty();
			tprop->SetColor(0.02,0.02,0.02);
			tprop->SetFontFamilyToArial();
		plot->SetAxisTitleTextProperty(tprop);
		plot->SetAxisLabelTextProperty(tprop);
		plot->SetTitleTextProperty(tprop);  		

	vtkRenderer *ren = vtkRenderer::New();
		ren->SetBackground(1.0, 1.0, 1.0);

	vtkRenderWindow *renWin = vtkRenderWindow::New();
		renWin->AddRenderer(ren);
		renWin->SetSize(800, 400);
		
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
		iren->SetRenderWindow(renWin);
	
	// Rendering Plot	
	ren->AddActor2D(plot);
	renWin->Render();
	iren->Start();

	iren->Delete();
	renWin->Delete();
	ren->Delete();			
  	FieldData->Delete();
  	DataObject->Delete();
  	array->Delete();
	plot->Delete();
	tprop->Delete();
}
