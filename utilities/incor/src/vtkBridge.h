/*
  Name:        vtkBridge.h
  Version:   	0.1
  Author:      Rodrigo Luis de Souza da Silva
  Date:        17/11/2008
  Description: Receive several types of vectors and arrays and convert
               them to VTK types.
*/

#ifndef _VTKBRIDGE_H_
#define _VTKBRIDGE_H_

#include <string>
using namespace std;

class vtkStructuredGrid;

class vtkBridge
{
	public:	
		// Constructor/Destructor
		vtkBridge();
		~vtkBridge();
		
		// Return internal vtkStructuredGrid
		vtkStructuredGrid* GetGrid();
		
		// Set Scalars in multiple volumes
		void SetMultipleScalarsToVTK(string prefix, int nvolumes, int *dims, float *pfloat);

		// Set Scalars of one single volume
		void SetScalarsToVTK(string prefix, int volume, int *dims, float *pfloat);
		
		// Set Vectors to VTK
		void SetVectorsToVTK(string prefix, int volume, int* dims, float* floatVecX, float* floatVecY, float* floatVecZ);		
		
		// Write vtk to file named "fileName"
		void Write(string fileName);
		
		// Set dimensions and initialize internal vtkStructuredGrid
		void SetDimensions(int sizex, int sizey, int sizez);
		
		// Create a XY Plot with 'numberOfXLabels' and "values" in Y
		void GenerateXYPlot(int numberOfXLabels, float *values);
		
   protected:
   	vtkStructuredGrid *grid;
   	
   	// Is if a grid was set. Necessary to setup scalars and vectors
   	bool IsVTKReady;   	
};
#endif
