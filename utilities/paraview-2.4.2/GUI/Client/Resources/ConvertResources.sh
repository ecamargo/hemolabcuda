KWConvertImageToHeader --zlib --base64 \
    vtkPV3DCursorButton.h \
    PV3DCursorButton.png \
    PV3DCursorButtonActive.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVCalculatorButton.h \
    PVCalculatorButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVClipButton.h \
    PVClipButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVContourButton.h \
    PVContourButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVCutButton.h \
    PVCutButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVEditCenterButtonClose.h \
    PVEditCenterButtonClose.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVEditCenterButtonOpen.h \
    PVEditCenterButtonOpen.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVExtractGridButton.h \
    PVExtractGridButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVFlyButton.h \
    PVFlyButton.png \
    PVFlyButtonActive.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVGlyphButton.h \
    PVGlyphButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVHideCenterButton.h \
    PVHideCenterButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVLogoSmall.h \
    PVLogoSmall.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVPickCenterButton.h \
    PVPickCenterButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVPickButton.h \
    PVPickButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVProbeButton.h \
    PVProbeButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVPullDownArrow.h \
    PVPullDownArrow.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVToolbarPullDownArrow.h \
    PVToolbarPullDownArrow.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVIntegrateFlowButton.h \
    PVIntegrateFlowButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSurfaceVectorsButton.h \
    PVSurfaceVectorsButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVResetCenterButton.h \
    PVResetCenterButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVResetViewButton.h \
    PVResetViewButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVRulerButton.h \
    PVRulerButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVRotateViewButton.h \
    PVRotateViewButton.png \
    PVRotateViewButtonActive.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVShowCenterButton.h \
    PVShowCenterButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSplashScreen.h \
    PVSplashScreen.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVStreamTracerButton.h \
    PVStreamTracerButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVThresholdButton.h \
    PVThresholdButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVTranslateViewButton.h \
    PVTranslateViewButton.png \
    PVTranslateViewButtonActive.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVVectorDisplacementButton.h \
    PVVectorDisplacementButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSelectionWindowButton.h \
    PVSelectionWindowButton.png \
    PVNavigationWindowButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVMandelbrotButton.h \
    PVMandelbrotButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVWaveletButton.h \
    PVWaveletButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSphereSourceButton.h \
    PVSphereSourceButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSuperquadricButton.h \
    PVSuperquadricButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVGroupButton.h \
    PVGroupButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVUngroupButton.h \
    PVUngroupButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVAMRPartButton.h \
    PVAMRPartButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVAMROutlineButton.h \
    PVAMROutlineButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVAMRSurfaceButton.h \
    PVAMRSurfaceButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVLockButton.h \
    PVUnlockedButton.png \
    PVLockedButton.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVRamp.h \
    PVRamp.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVStep.h \
    PVStep.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVExponential.h \
    PVExponential.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVSinusoid.h \
    PVSinusoid.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVKeyFrameChanges.h \
    PVKeyFrameChanges.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVInitState.h \
    PVInitState.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVRecord.h \
    PVRecord.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVRecordState.h \
    PVRecordState.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVMovie.h \
    PVMovie.png

KWConvertImageToHeader --zlib --base64 \
    vtkPVLookmarkButton.h \
    PVLookmarkButton.png


