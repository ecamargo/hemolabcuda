/* 
 * Resource generated for file:
 *    PV3DCursorButton.png (zlib, base64)
 */
static const unsigned int  image_PV3DCursorButton_width          = 21;
static const unsigned int  image_PV3DCursorButton_height         = 21;
static const unsigned int  image_PV3DCursorButton_pixel_size     = 3;
static const unsigned long image_PV3DCursorButton_length         = 100;
static const unsigned long image_PV3DCursorButton_decoded_length = 1323;

static const unsigned char image_PV3DCursorButton[] = 
  "eNo7e5Zs8Pfs37+4ZRnBkDwA1AkBxJgAV0wMwDQQ0524bMfqoxErSHwoYVPJSB6Am0CJ7Y"
  "yjsUlxmqckx1GY36le2hALAD3OKSk=";

/* 
 * Resource generated for file:
 *    PV3DCursorButtonActive.png (zlib, base64)
 */
static const unsigned int  image_PV3DCursorButtonActive_width          = 21;
static const unsigned int  image_PV3DCursorButtonActive_height         = 21;
static const unsigned int  image_PV3DCursorButtonActive_pixel_size     = 3;
static const unsigned long image_PV3DCursorButtonActive_length         = 100;
static const unsigned long image_PV3DCursorButtonActive_decoded_length = 1323;

static const unsigned char image_PV3DCursorButtonActive[] = 
  "eNr7/59s8Pf/37+4ZRnBkDwA1AkBxJgAV0wMwDQQ0524bMfqoxErSHwoYVPJSB6Am0CJ7Y"
  "yjsUlxmqckx1GY36le2hALAPMJ7XM=";

