/* 
 * Resource generated for file:
 *    PVAMROutlineButton.png (zlib, base64)
 */
static const unsigned int  image_PVAMROutlineButton_width          = 32;
static const unsigned int  image_PVAMROutlineButton_height         = 32;
static const unsigned int  image_PVAMROutlineButton_pixel_size     = 3;
static const unsigned long image_PVAMROutlineButton_length         = 288;
static const unsigned long image_PVAMROutlineButton_decoded_length = 3072;

static const unsigned char image_PVAMROutlineButton[] = 
  "eNqzSMq1GBYotr4js3MiVRDQKEzDP339+p9KAGgUmhVAS4HiyAwgAjKAXCDj7PVbszdsQ2"
  "ZgakRGcI1oyoBk/7I1EAZEBMgFMm4+fLzlyAkIgxLziQGUmE/r8KG1+VQEJMUvHMEjGo6Q"
  "1cMRqeEDRxSGD1A70IUQBhBR3XygXqD3IQwgorr59A8fIBfChiBgwfLs9RtkEWT1cIQnfJ"
  "ARUAQYXMgiEMPRlGEiPOGD35uY4YMVjZpPnvmY2RyzfKDEfDLK/9H4HTWfDu0rWrcPhy4C"
  "AA80YOE=";

