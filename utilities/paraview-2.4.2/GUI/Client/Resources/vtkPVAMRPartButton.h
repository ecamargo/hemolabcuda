/* 
 * Resource generated for file:
 *    PVAMRPartButton.png (zlib, base64)
 */
static const unsigned int  image_PVAMRPartButton_width          = 32;
static const unsigned int  image_PVAMRPartButton_height         = 32;
static const unsigned int  image_PVAMRPartButton_pixel_size     = 3;
static const unsigned long image_PVAMRPartButton_length         = 1804;
static const unsigned long image_PVAMRPartButton_decoded_length = 3072;

static const unsigned char image_PVAMRPartButton[] = 
  "eNrllmtQlGUUx/+TGY5Truk4qaPhJZVQJCfUNZU1RRkHcSG8Ky43gXVZVth9H9d9WFBJtA"
  "TJS2qag4OGkmtIomh2wjQaZUKj1MamifLCh4osZ/RDX+y8zyJD2AVNZ5pp5swz7+688zvn"
  "/M/leY1JduP/z+Lz1llf2/goyKvf3uNdV2E27zZN2u7Iriwoq1717sGdh46KzTtitLwHxk"
  "bYxPp9BwuqjwWmFiDOiUgN/QUeEwgQGOrE5KxQe6HFu3vDiTfrv/p63wc1xWW+DpI5qi0H"
  "qh37Kw3OXKRryNSQpcHJ5kKyBqMLw1zoKfCcwEw3hMa6sYs7d+5wpn9P5pSLSo9ZZDlMAm"
  "kCswXM2bAkwxH1pCukr9Z/sKtfuKvncNtoTHQPmTEdC2SwNeNkfQPb5e+v/lWBWArOrmDT"
  "8dA5xZgmYBQYKTBJID3jCTE8yB6QMA+xts5jY1/qHT2lc+xSOOwYL7sk2cwb3vnh+nWOii"
  "Gcwr18lmL7gaPO/ErDyFwMEhglECEwQqCbwBgBLXqQBtcrsIQH9JgeBncyBu5FRjmiNUN6"
  "/rnqQxV0Kcjp9aNYn7Zk9rVxT7WFX+4h0Fugj8BAgTCB8YrMKQQLjBNInN89awyS53aePR"
  "+ebfB+hDlrElevb2xuNifsxVTZz+q6l8/lbrzWbHGVGwbn6vxnBHope1ZVjeFRAkkCmU5k"
  "xyMtBSkeJOTAuNYQsaOi9vOKLy8a3PkIkrDLQKvTHy3r08rfdeScOaehpLLpxq+/1Zz+xp"
  "FTGTh+LYYJXaIBAkECo5ULLnGWGzaJtBxE5ppfLW1sajZv2wOXBxaJCIkUGZy67F4+x4/J"
  "hJGEgWRaVF9cdqXx+u3Gq83FpadNi7djiNCtjxsxHmRLLJOBnsKK85cqzlw0eFbD4YFL4r"
  "kcJEssksGLdD63BzNb+Tx9iCIYCc8TggnhhBcpcEGtY+vlmi9+vnHzdsmRzyx2nyEun/mO"
  "yqrz3zWZV+5FtAcLFDxaYpTEUolIOSoh0w9kazvyocvPYgrhBcU3ESYSphIiCTFkSKmybP"
  "CV+Opv/HL7/LWm4lO1Bm8+lrBKUleGHwxS14fjny39QO5/f5e2No+psB7RhHGEEYQJpPua"
  "QZhJmEtY6EOMxFCJvpK7QpeIY9Y8cHqQLjFWYoReWSRKQ3Y+K8Orr13z8z/mtxpgJkQQwg"
  "gvK3IcYRYhnpDkQ7xEiEQ3xbfpJdCBHPMEiaclFktYZacUz7QVq1h2ntymH3/iIW3b/3kf"
  "fovFhFjSCz1ZPcwhWAg2gtWn5x4m8ZTij5GIkjBJvSFZmWkSDtkpzROS6PDPPgfPAbeb3O"
  "ILV5BPSCS90CzOPPVsJWgEl48JCJcYoviPS3SX6CXRW8WfIbva3GF3F8ufwtmKTtZhGyFP"
  "MeMU305YQfDy6UOmapLhit9F8QdJzJCsVVfrP8N1/uk6lBI2KmYKa85hE9YQXiesqtL5XM"
  "pQxR+gunGhXoJeKVpH4C0jVkHYRSgi5BAchOWkK8YeC4lF0F0sUfxZuuA6PFUzdgzeMmKH"
  "CQeVi02ElYQCwhuErernUsXPVHwFD0x1dhzeMmKfnsX7hEOEMsJOwmZ1lihL16XmLHS+VQ"
  "5Ozr4vuP9N76la3cVRQhXhPcI+wn5CuTK/PnadPzQ5637hbbMoqq0z1dWjmlpy4fOwip8n"
  "KN3TutUfAN52XWw5cdLScFEns6Pj3KsywLYiJMnh5/8b+B/uyiMnnA0XDDUf93WuDLt7Kz"
  "0UeLu7nvdJ1SdnmMn8hwhvVxpeXDdv3XoU8Lbp/Ne+hH8HHlaoig==";

