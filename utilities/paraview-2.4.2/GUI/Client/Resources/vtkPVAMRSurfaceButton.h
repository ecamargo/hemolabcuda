/* 
 * Resource generated for file:
 *    PVAMRSurfaceButton.png (zlib, base64)
 */
static const unsigned int  image_PVAMRSurfaceButton_width          = 32;
static const unsigned int  image_PVAMRSurfaceButton_height         = 32;
static const unsigned int  image_PVAMRSurfaceButton_pixel_size     = 3;
static const unsigned long image_PVAMRSurfaceButton_length         = 1628;
static const unsigned long image_PVAMRSurfaceButton_decoded_length = 3072;

static const unsigned char image_PVAMRSurfaceButton[] = 
  "eNrtlmtMU2ccxj+jZUyyQTDRLXHJNo0ZMxhIFJeJOiYqJqJbp4DSjlEmliLdhlyLlGtbYO"
  "VeLiuIpVUot1FYATuQIpVCKUKhFtCBiHEs2Yd93zPf7c3Z6ZZtuiV+WPLk5O0h/J7//3n/"
  "7zknKCYh6H899woVplyqqBPKy7H4F7EnPpeUX+8wT8+OzC1Ju4ZUow7z0ppr9dHQpF3d/U"
  "1yafWxi+lPgRUUlOLfZ+8t90464lWtRR03fPmZfheKtmY3Q4Jrw8HKjpgWU9nwndUff3Ku"
  "fW91OK/2DWbWqLlp0j9jhsSLUY/BbHEuPqhW9Z/iFhMsxOKLO2+RBV37y66fuTJocq12z9"
  "x3PvrB7lpEecGxIhqCadxuGb8rU3Tt2ZfBeTGaSFDSQvg5rUZ0kaIzZvdZAWydWqR89ELX"
  "+CvxOl7XhxhpL0g4I0sXEJgC7XxbTPlxck2k8qroq/bSr4e2nc99RaQgHLVlHi6pPRYIEW"
  "3P19FeQiq6wcfauvyY1o/BkObpCdPbh0eMIK1x3F8sR/38Si2u+7JrCEdhstOatfcdcoel"
  "w7VQb5vVTLiwL8QOKTF3E4HTsqny6w3B6WVQRovhA5k64bImqWZQqB9B/ZR/e/1h09IMLC"
  "KG2z8e65VMj2hcc5KRsYWVVcpHUG36MXf+2dQGxJKo0ufK2vcfzA4ISg/cW+q/WxHN04jz"
  "+zJ1owgEQJBxhcuX89aqu7b428aQgRZMFOUjqEnbEguOfNp6xiP5FUjst5s8r5fTtr2RC5"
  "cD71e/+VZh8P5yZCIbnSidsVY6bcB+NmlKmxqCS79lgjmcjvkVSo6MLos/Xxcalkc3hckn"
  "ggW50vnB8KCL7gcL4CMuzCeTj7j8tnwCLPT69kQC5MVW/SUfo0g3PdE6UL9gD+xrQv2YSS"
  "YfcYkuqhlR/NrI7/nn3Pl4YmAXcJaTem8iH/2yM2/mlvnxCmaSyUdcCJy1BTSfjV5Rm17i"
  "enDe8+Ac3uh1xtP7AuGHhddi4FF8YLG+e/aexDx24+F3cMHQYiaZfMSFBwKLj0OH64YXTn"
  "pwQj04h5jatVsAI35c496SduaDAsOPXqbX1lkPIlV7j1sa0e8eSE0UVW559SQL/gs/gB9x"
  "OoubLvc+xvM5l7o5WYmzTA/F9Oo6PbzuR/hJIB96eB4Bx8s77HTkZaFIeTQ8hcJ37IxWNn"
  "ZKGnRcSbFveMymw1GxRVWHxFJixDq8RHhxKMsMT8gf+W4+5V4wmHCBBMJibqYiKO4LYHfx"
  "kuGiHxqDEX4SHU2VNfcOsPiIy9g/tcEz3J3MtEjKU53NVSaU1F6qaUbl7ySko3IwsUguVx"
  "Nd6/+WFQ6RtKHZYp2Li5cjExYZd4TJZUfEUlrkibRC7cDN8jYDaYRIoe1kjb37K6ZE02q1"
  "O5GDj99xAg/aI0gqqD2YJEFtKH5rROxr3E+BwoIYoRfc1w2O4PX0d16O6K6wSWtzuHKkjQ"
  "JRCbNsMBt7TRCw9CbsTBPTUVkF//QtjKGyORdUXcYdUUJwEDjSJoGgZrQDF/wctt15lo8K"
  "nESzfRYu+VfayDRCxAXDgw+JP9zNp3DBMwSjSMi4ogXWQ/LZhTGGS/+4fcBqx7z9R99y+K"
  "bi58ifkw/LnwH6lsWD";

