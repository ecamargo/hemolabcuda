/* 
 * Resource generated for file:
 *    PVAttributeEditor.PNG (zlib, base64) (image file)
 */
static const unsigned int  image_PVAttributeEditor_width          = 34;
static const unsigned int  image_PVAttributeEditor_height         = 32;
static const unsigned int  image_PVAttributeEditor_pixel_size     = 3;
static const unsigned long image_PVAttributeEditor_length         = 2240;
static const unsigned long image_PVAttributeEditor_decoded_length = 3264;

static const unsigned char image_PVAttributeEditor[] = 
  "eNrdVmlQU1cUvm6tlIo4SK2OMi1gFRWpuKF1gRYJiwgq4DoCyg4CigoiOwLBgMSFRRQbBB"
  "FZFMKmgLKFEIgpILLJvpmQkAQIW8hyewMzHad1rBX80ztv8k6+d9/93r3nfOccCL/QGIdQ"
  "gG5CPk8kFHwJgqchplIAbJoLLL4GaNwJt5/d9T10dJQACDy+DdmD7F4Wg7JbauFjnPcsUp"
  "zF7LbcruJrqjH9NyYKhzYS4GYN4dhsUQQc3kgI1C3LjMuLj3LcCkqfPfXCrIJw2EwBDLLo"
  "M18/6PguT4ONvsYbkP0o2NLbUC7ATF53DsBbHUWIsTyYGBlCRnVRZkc9VSCY/AyK1LDA0+"
  "uBh640hCIIn0PYMwW/UwGgorx8UMyNvWw2PXM9AFYbFjWQiU3UF9OIWCz+FIqXSfgdAKBj"
  "EUz2Q9gIYQpaH+GBR9cYAunSssIr1gY1RRkIScJin4QZeBppmqstvoiRoRCTy1Lj6J2vRU"
  "Lhx7mG2ANHVkhV5blPSaNXwM+DkF2ZF50Z45rotzcvbNtdXJjBctBVX4UmK8+dj34jnZ2e"
  "pRKxp7darZvrvFMu6sLhnJgAbn/3MIeFqP4pK2ZPPcHX2UgWUJ/7vaVG17zEkYkBIlgdaL"
  "ZFVbK7iiy8vo2Gsr+pIqOraSrClTuqErPuB9V3tpksAdOL3HA9hXfQfeB/4jHWNj3sIqO7"
  "9X0KDpN7dtfCCJvtKaHmaTesyzM9ChKcLmnL3nDaGXNuN/aoItJKnIfaIRX1EU7zxBgPvZ"
  "J7PzpgP2iuiI08dyIFi2Ib0un9yREuKBRyfw/OT8QayYEbdvqt1cVtryuGuSw0IcrN2nId"
  "yIh0nOJsh5AmElWVpmNxlruqsu1HuTfR0V01WuiOMfjrwyb5k8sBuO5y5ZrDHnbnmz4ms7"
  "620kYVJIS48vkSNVX/QQmz1fM/pHpZd0XG7fMIcdwh+7okvLM+DcJaCPOnrhIUMkXJ/m8K"
  "bHhdfshNF7aDFPz1908g4ozx+T1q1pvmDY1wswm4EiJhZHj80r6VEfaaN520R8VwTAiL0+"
  "LjfGzveh0ItdA0V5KWpEFhL4SlEKKwRH4v5vEa0sNOMFpCIXwm5BNcts0f4vxdGlW5T/YA"
  "UNvS7ab1rfcBBYT0dbXccjXEntycFO6ScSeYzZTol9Xf5n14i42qfF2pdx0pXAw7IKycYi"
  "FxWDUP/bWZ7QTkFGK48j2vcx+MTH+jvZeNd+RnJyfd9g8y+QkhbDb7HYNFykty11t10xFD"
  "zU9jMyV+SQqxd9X6yuewAi0/FsI3aCPINfT2gvRQvREOYmk2/xGw6NwPsjB7mtw0pFMJ+C"
  "eJkREuxolBdtM4OrFaasktJz3XnbLBp35prKEhkFaalREbZCoP6kqQr7tRDNA7iA0lvu1k"
  "h7pcTNBxnY8IbWyQcVFLJvDIhoLCl3gXw+y4q6ScBCQ5IYQtjXW5CdcjHHRwFtooJRanxu"
  "QnRVipLrt2cvV0zepqfthN9U4PVLdSBjXkyo8njVZqwascgoXqHCqNFmarabUWFCZHIpw7"
  "NsFgc9pbm7q6+46vBF76CvH+Z+hMzpUDyl31acyeYnpnOTFYLcph8zEl8Ik5MNpZK8bHvo"
  "JCptKqnNQBMcYP7WhwUswYGJbUkd1SpZlxdVVFyCbn57hrL8bb7UCnGGAAiFHe5NyMT63Z"
  "PO418+0PbvqRyKRKCsVyNUjGnZ1+hDKv1bo5KMGxBljcET5CnDSWuOsqhp5aGmquwqb3/K"
  "eU3lFHdtOWPSgH2hkTZaRiT4MffPevrSYVTEDora8siWout7MNCR8WP40n3sUdWw6639I+"
  "o3ZQsu4VxPlYqwH0xeXFhe66ay7+ujTkhHp9DQXpbWCI19bSPDQ6PiYSd7Q2WiiBMR7ns0"
  "thuMVmN0NF+ih8Nw6LnmdhAKCV5kiaBx6vpbkR0b1telVZlK0FQAM5YyY1l+BpduZnpfzn"
  "RSikIy6dKku/j8ABLrf3naRmeWB+9DFeZ/YdCLfSE4nEMyGKdTP5DYAHeK/OYRhg/D1C+t"
  "ksRv8AMpDSHTcvctwGrNXm0zsaZsLC7H776lmCneo3IS4Hg44dkuSE0RHh1CPr9fNcd8p5"
  "6MicVASvCpMn+SMzbFfEwgl3jBTKqwLI6+tp7u1pwTtqOWuAC5rAZBlAHcWLpFjecNustF"
  "6PAi/IoAYVABQJ6gDsWwAOLl3A7KGQsuPpnc2T/IlZYRGJRAKBQPhv7cf/dfwJjFtYnw==";

