/* 
 * Resource generated for file:
 *    PVCalculatorButton.png (zlib, base64)
 */
static const unsigned int  image_PVCalculatorButton_width          = 32;
static const unsigned int  image_PVCalculatorButton_height         = 32;
static const unsigned int  image_PVCalculatorButton_pixel_size     = 4;
static const unsigned long image_PVCalculatorButton_length         = 3692;
static const unsigned long image_PVCalculatorButton_decoded_length = 4096;

static const unsigned char image_PVCalculatorButton[] = 
  "eNqdVwtYzmcbf3rr1flM57zlFW8pHRRTRKMaNoca5RSlRCWHKMo5Q2oVTdIVciEMyyy0RH"
  "LJoRI2M75hss/MzHwOm9jm9933U/narn27vm//63qut973+f9/9/O7f/fvvv97P74q9tLa"
  "X3FNzF/5qZgy+4BITK8QOw9cEQNGFojQ8SUiPG6nsFaN8/b0TYo3MR/UoNR1/V5b6fRUoe"
  "PwMy9tHaen/J2xeWCjl29Sgo0qos+4+DIxfNIWETh6o9ix/3MxI+2QCI/dJZbn1op9h76U"
  "mB3Xn+HzvY7qiWMNTQdUKbTtXmppO0LfIgQ23aOh6ZuGXv6L5dL0XQhrdTQMLEKhpaOCQt"
  "v2pYFpwEmVy6R3B4dt+lv4Q0JXmBiZBVYpdBxbdIz6w/WNxYhNKUdm/inkl9Rj0/YmbCpt"
  "QmHpBWwpu4ySXZeQVVgHwkEPv3QojQLA9xqaBh4bGrrC7H/Fj5lXLtw8ZwZ20ne/rzTyp2"
  "ctROqqY4R3QWIWbK2XfxftaELRziasp1iSlxzBgFEF6Dc8Dxu21MvflmafRHffNPAzlHpu"
  "DzQe8UPGTi/7S3zCEWpNzGjK7TN9q1Gg/SjY1kiYDRJnM+HxytpYh+SMIxgyrgh2HvNh4B"
  "ANQ9U0GDvHYc7So9i47QI20D1FFG/EzN0wsBoD1olKPSl6Zf6p/4rfy3PmO4xtZBeJpPTD"
  "xOslFO64ID/5XPOWVyIovBCO3qnQtZsMI1WcPHMa8ZNbdJb+j8WQsUWSp5yic8grPo8tuy"
  "9hFvFjaDeedOHw3MsvOeTP8EOGZVoodTU/GtqEY86yo9h14Ao2Um7nr6hEcEQx1H2XwLz7"
  "TBg7xUITsBKJ6Z/QORsxj/YODitE9qaz8B+5Hla95iBncyt2Ln3y4vhnZVQQD2FQ6vZ4RF"
  "hW7bjpa4+LNQV1wqxz8BEdw37E+S7sKv8C42fuRfd+y2CqngGLHgnwDl6H8Ql7CecMlq47"
  "Af931sPCJVFyru8QhXeiSpG+thp69lGYNu8jbCINtOPnF9cTD5cRSbnQIT2YWg6tXkuYjJ"
  "22ulpoPKYPovp6ofZZgCI68yZaboGr6LlTETi6gLg9h2XZNaB6hmm3eCitJ0Dtt5hi3C1z"
  "zPw4+2UQ1nnY954P76HrUFx2kXJwVn7Hi2tm47YGuJCetbRtX/R0nx6StqpaePRJ1qGzf8"
  "pnX7SmGsU7L4Jik88zcJyK6QvKsZ5yb0CxWPZIkvzsq7iK5juP0X4x10akv9UbTiMkYjOs"
  "3eZiHfG0gWolt0Mu+H/ChI4hcxB0grA7DQxK70HxtHBcG7c2UpznpdbcB6+WelL3W4q8ov"
  "Po4pos66H9unv/GVbm1cI39H1YamaR/mNIZxVUq1WUk1hEJe+X2mvPQftivlz7LyYOrF8Q"
  "truXX1Is+1pU0ocoJD2xbktILyMmb4MZ5d7YKU7G1HdYruQkp+gMhk0okbrgvPcMWIHRMa"
  "Wk1UbUnr2DQ5VfEU+J6P/2ehTvuog84j7vNQfnpV9NnXOA8FXw8ElIJB7O6Jq/ieU5tVTr"
  "DTJGrvnlOSdh4z6XMKZgTMx2ZGTVQGkVCR2r8bBym4MJiXuwfd9nqDlzR/KxIrcWnWwnyd"
  "rpPyJf3sv88Jk64rOX8Pf65iEwsRhcr2vg1Wypmojc4tbfeA/rpoRi93xzrcyrg2cqjh6/"
  "SXrbQ7VxBbeaH+MmLa51v7fex/0HP2Hh6mPQ7jyOfKMCyyh29ocpc/ZLr+xYi5xbrocu3a"
  "LQSd/jrrZS/cjZI1nywhrlPVy/XNvjE/bAhOqLdcD+Ulf/T9J2NmmoCt9S/q1IZ6zRmjPN"
  "ePL0BZo+/w4PHv5Mn/dl7nxDc+RzOH+va7Gtd6h9UqCt7PaY/O5JN8/ZdPZG+RvHyov/zs"
  "w7BRuPFMKPkV6bTbkXBiFSm0+fvUTZwas4XteMV69aNXn05E30Dlor65/rwIJ0wLXE583b"
  "/J8aKCxtQHef+dDWUT1jfLXX3LY463+nVc6Hb8j70vNsyec/rvpK8neaeOCrpeVX8qM66Q"
  "t37z2Tv2mZjyJ/zMSagtPQt5+CqFn7Zd23PrPdBxrRo88Cic9zg4MmXt67/g/4nJOJiR9K"
  "z+E6WEIexFdG1nFQH5Ncd39jKRQWYbInPfzxOfKI62/uPsHZxm9h5Tqb+Fgje5bEJh64L7"
  "Gf2PaMg46u5gc9I59bJrZ0f2GbXxT9Hn8pYdpRDoyp14ycWgqaJaC0mSB9/snTl7TvLPXf"
  "wxKbrxu3HyE+9SBmLz0ieWHP4rmAz8d55TOuo//NHCKga+Dd7OA0bru2nverBZlVsoZzO9"
  "QKf7J+ufZZg137LCTvu4aJSXtI4yekBvg603CX6u8k7tC5ufcLk+FwHZhJfnpc+kQk1Q17"
  "AXsLn5/7pba+L2y7hu329ksOEoouv42YuEXqslV/5+ReXuxhExL3Sj0bdo2Wdfbyl99Qf/"
  "Eeysqv4pdfX8E9aDW0KAdcdz8//wUxpL9jp26jsuYWrHvNhdeQrLZnt3I6lOqWMb38ZgU7"
  "a6KtlXquP1iqJr/u2bwvt02vzMmydTWw96Q5g/BDIjfj4NF/yL7n4JWK5m8ey74xkOaf8i"
  "PXyQue0e/XiaMP4eSbLs/v4JUm5zb2N9afpfMU6sOaR900MfaVJ78WKvWEfK1OGoybUdbm"
  "ma0e8NqLyI99gnOkF7GmDlffxKCwjTTblOE29aFbzf9COWFOS/kIXb0XEU8xkiveb6qOB8"
  "3PknfubdwXFJ3c4OAcUUj8iANHrougIUscdWguMLaPxMrcUzLn7TMEc8Jz3lTyMn4W+w37"
  "541bj+T3kRQD1wBj6VPvZ51w/+miSYYHaZ97vpwVaTEHxnYRdHbXh8FvrbSvPnVb7D98TV"
  "TVfi1cXKMXKZTq524ByyRP7V7crsdsqg+e9Uy6TScdLkKvQatg7pJA8bSfc4asCZ4bxsaX"
  "0YxSI8/M+eYewM/0CMyEQunyvGevaYu/uP5Q1DXcFRXVN0QV5YCwhKXNsE8U+t40420irl"
  "pnXeafvauY4udZrx2LPYn5sHGfB/dB72HEpG2kzWqJx/xxjbf6XaPkPSjsAyj0vOm9YMAn"
  "S7JqxMUr30t88mrRdPk78fmXD8S1Gw8F9cN6Lf2+eGNYzutZmmc7fi7P/zwTdZbcvif9IL"
  "WtbvkdoLCt13DMvDbvaJK+7v92HhQG/WBsFnA1ZXmlII0J8lBBvVM0XLr3enEs4e/mKMy7"
  "DD2n0O3d4uSVghSaP0tolmIM9nKuRfYKximic3F8fNacNt9irjjurRRPauYxONNMp9Dzaj"
  "GzfLNx1JgsvfNN9wi3WVSevCVXR3yqaeLgR+Huk6gwNg/aTfNyiy71ab/gLJr/aiUHW3d/"
  "RjVySdYz55cX+7lcpa15Zm/uG5pN72qhPHO/MDIfvM/NK0FxmTjmemO9ta8/4rMuEhYdEl"
  "RvwrV3/NhOBt6XqE++5PcplcdsDIssRjK9Y2RknSC/qZXvOTTH0txRieE0F/Ee3ivv0e/9"
  "Bc22Y2lGE3ELDsqc/z/4SRkVIii8SFg7hseadR56jN9NtBSWvyktR1JPioM5zeU8a5mSFy"
  "ktR4F/oz0/0d7jVg5j4vjdc+bCQ+Lv4s9aclgMHPWB8B+5QWTm1YrODuFqlcvkAcYmvj8J"
  "oUTHZWTs/dxZM3WApX2Yy6oNp4X/2/kigN7f+Vl/hf9vL2P51A==";

