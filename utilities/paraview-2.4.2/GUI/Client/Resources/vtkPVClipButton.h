/* 
 * Resource generated for file:
 *    PVClipButton.png (zlib, base64)
 */
static const unsigned int  image_PVClipButton_width          = 32;
static const unsigned int  image_PVClipButton_height         = 32;
static const unsigned int  image_PVClipButton_pixel_size     = 3;
static const unsigned long image_PVClipButton_length         = 604;
static const unsigned long image_PVClipButton_decoded_length = 3072;

static const unsigned char image_PVClipButton[] = 
  "eNrFlj9LA0EQxa+66ppU1ws2cpBGEIKNEUM67awE7VNYiNjli1gd+F0sLe4D+eILwzA7+y"
  "dZxTAcm73c783Om9vN9PU55eJjfH99eUasbq77vscV46ngwSyw//nMTs664RLRXtw2V0+4"
  "Yoa3CoViQNAYYHJm/4Nh0S0fcM2uKAZEcIapEohoNqMEvhLL9EQIXzV/djoHBBkySVk7ge"
  "39VjNdvqkDF0UVDJq7t3b5CBXmmQCGgUc0X/cAb5GP6M7XgDPKJWixGKeLQz6EkDwldlqb"
  "EfCsCm7RX+EbW2USV2Su+UIIVYRJu6nido7LhwUmZ6oAIo6bHxzEx8B0oEDceSaAChfyd1"
  "00LA7ihy2U4BsLavhYFBupno8l6xdWN6eMhe9akOa7Fv8X3y1Rmu9a8Kd82KGb1vDDtyzL"
  "NxabHcPwQwuyfGOB4estzn3LKvnmFQgtSO8P3PGQpNTc8GmHXkIJX85fqTwG3K71pK4SnH"
  "Ut0HzuyeYoMRx3x+C6KOHyWYrEXwVm7iava0UJXSKpcOxBno+xFYUSPE2kFOmcy/9lmTIK"
  "NnbCHkfWEi6/nmwsI59t/Fvk0I50Y1RKHE3+BomgyO8=";

