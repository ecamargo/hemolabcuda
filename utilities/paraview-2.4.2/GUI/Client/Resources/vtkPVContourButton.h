/* 
 * Resource generated for file:
 *    PVContourButton.png (zlib, base64)
 */
static const unsigned int  image_PVContourButton_width          = 32;
static const unsigned int  image_PVContourButton_height         = 32;
static const unsigned int  image_PVContourButton_pixel_size     = 3;
static const unsigned long image_PVContourButton_length         = 2364;
static const unsigned long image_PVContourButton_decoded_length = 3072;

static const unsigned char image_PVContourButton[] = 
  "eNqtVnlUVHUU7t95+E/n1zGt92yxvs4pAVGgXBinOp2yVQcDUTNFjEUwQBm2YQYVTVwGEo"
  "QMIUQFR0RETXNjRk3EXBEEcUFNxC203Dd43csAgSVlNef+8c7Mm++797tr1aHyqkeYKWWe"
  "KTMdPiMxdjz8J7KNmwDfUaZFGaaUudb83KpH/7cLsxZb9QYDvghGVCxMMzAzGbMtSLZgbi"
  "rbrLkwJyE6HoEh+uho0+ykx0AuWkF/QVg4TNMZauG3WJyHJQVYVoj8VSgowvJC5BUgeyky"
  "smBJw7RZiJiqNxqtq61dI+dmZ3p69H+i7SM++EiON2PVOpT8gA3b8IMNm7ezbbJjQynWbu"
  "Kf8ouQnYfUDA4nJEw/KaQLfM3TT8vePuzPgm+QWyB8R0nOrhpXNzltEcoOYE8F9lbhp8Ns"
  "uw9i137YdjNXyUYOKmsJqzc1Wh8V9Zfg5Lns48fCfpPDXpF7W3di514lM0dy99R4vC6TRL"
  "Vn2I6eQfUpVJ7AgWqUV2D7HmYpWo/cfJYrxkjy/lkZzXPP46t5+DYXK9dgYym2l7Ofh2oZ"
  "ala64hsmPdlLXlmCc1dQfwU/X8apCzhej+o6HKjB7gPYtotlXGpF6kIY4vSRER3xJwVPFO"
  "+8y+CFJezMrn3s25E6zMmGLgTvz8Qn6fDOlrq/JqKNuHIHv9zCxWvMdfoSjp3F4ePsjK2M"
  "o85bgfkLED7FmDyzE/6Hn2BFMSeRhD1YI3m+8Uei+4yQ30nGODv8fxT9Jmq8dDIFeO0BE1"
  "26gfpGnGzAkZPYW4nSXVizETlLWQr/AOuKvHYKwsH6rdjxE4GL0Ajx4gB8nAJfK8ZuFv0D"
  "pWc8hHsIQk+SKcOXS8++Lm+x4bbKLI230XAVdedRdQLlh7BlB0jGjMWIM+knh3XCLy3Dvi"
  "rUnObnj7+G32qM34nASoTW4csGMSCavpd91iHyCiIuSfJANKm4q+J6E1Ocu4oT51BxDD/u"
  "w/dbWaU5KdR9nSQKCOKqqDvP+CNXkhoIrsbkekReRdQtRN9HTJPUSyeP2obou5h6TVIGy6"
  "W2VorLN3H2F64u8pD8pNagEGKMxlSLA/+7xRkad0/U/kzlIQzxws0fQUfIbUy9jpgHiGtG"
  "0FF4b4b3NsnpGXmMDbFNRNpKcUfFb/dw4RqrVHmcM7h+C75bjqRkeH/aHoKHez+5YDW9Jm"
  "ISRP8ghJ3BlN8IXHglUkQap1fkF2bDuRTOxZLTqy0UzYi6ycE+UHGzGY23OISa01xLm3fw"
  "MKFC6iARhzBgENfezWb+V/glGO4iTuVnZ7vSe6XUTavpNhCu5+BSLjk5M0XMA8VnvWaIjk"
  "O4eo8TTU1BHeGQKH0RIqKMSdM6hUCh3W4W5kQxMI4ER0ILvksD+t6Hm6q8bBc9zeh7GS7b"
  "JaeX5dE2CkG8ESVoUlEtXWyRqKKW67B4A4+CWKPRMq9TCF5D2JnmFtg4FSZVel4nv7SFwN"
  "GPTfRMFD3i4VqjvBDJ7xjuIPy89KynTEOP2oH6mlKwcy+3c1Yu4hONaakde5lDoNpWVZGY"
  "KLRmmFXF3844bfhkUjed3HsZ+mQpn9k1ihbhFxFUye9QFZ2+yI1AE2DdJh56CdOM6QseGk"
  "f8pqqSSTqd/LkN01Vlgl3TTdeOD7d7osdkvGZBvCr10sreRQg6LPqOE1NiWv2nLmD/l8D4"
  "sP+tKul0DgrmmqZiRhsFReHWRPrz91oLCxh1g5+/2I/Ra6SnXpJp9lKXkf40KBblcAukzP"
  "vzuO5IIXSJHSi0cu9CuJaJ7sOElxkx9xHZyONiaBp88xWP0RrPAVw/trb6iTQYH7E9qaNl"
  "GydCsdvFODMSVWJRxtsp3dwOvYa01P91fFkvPEKFy0joMxEwg2Nx1D8tnflptMG72GiUa2"
  "E2Oyg0z+moVlstXm3p3xsIv4CQGvjvYNih01FiEyGTBR0Y7f2rH9H1UuZyaouC59uoUtbE"
  "cJtbO7wBk2oRUE7KM37AdF43eyr4mcTJdMyf+X97UbRTOCpKDIylgkfYKQRXYUIZxqzF8I"
  "WMWbgW+6tp8Ul9XOTYBL49AgL/4dHCFDTHmrjvRIKZl457sPyeBX6rMCxNcfcTk8K5bWmy"
  "bSwVPn7ivfdp/g8PDf3ndxFRaLQ63GrmUXa9iRalNMjLseBo++PwMT4qKK2Fa8UwbzHkTb"
  "r0Hveo46IdrJVLNvEcOP8rzjbytKGBefAoHzC0ueiKWJwn3h0q+rl33L+PRcFFFWngbUXI"
  "tNlp7dK0oauAcpq9DHNSxWCt9u23/t1d6rDgwAmcgqBQnjN0mVA1Wot5miVbFL8x7v3d/g"
  "v4wyx+Y8QIXz5Tk2aLQV5Sj545WQv/F/x2FgcRfejhL8F/B5dqchI=";

