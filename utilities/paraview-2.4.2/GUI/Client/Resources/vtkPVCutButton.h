/* 
 * Resource generated for file:
 *    PVCutButton.png (zlib, base64)
 */
static const unsigned int  image_PVCutButton_width          = 32;
static const unsigned int  image_PVCutButton_height         = 32;
static const unsigned int  image_PVCutButton_pixel_size     = 3;
static const unsigned long image_PVCutButton_length         = 540;
static const unsigned long image_PVCutButton_decoded_length = 3072;

static const unsigned char image_PVCutButton[] = 
  "eNrFljFOBDEMRbeiotlqLsEJKFnBFbbiBhQUK1ouQrUSd6GkmAPxtV/6smzHyRDQjqxRJh"
  "M/J7bjZP3+Wnvyef54O71Cnh4Py7LgjfY6oNgFLpeHQAj68Yn3VkMtYGvA/u7+9uEZ766h"
  "FhBifUIgZPdyluCTKrQuDj4tH50QdHIYgXgIvDm+W2bKd8skgVbQYL/mWQCjUN0uWR7jLw"
  "3AL8Ap4yagbuNinUMyPaMeqADetYJfjK/4LqzqtH/JFyFaEZPhppU0c1I+1uLmTCuAKOJu"
  "wCY+Gi4DBUn7OQHMapCPQGC2m/gxhQq+C8EMX2kzz8eS7Ya1yRnbaQhqfhria/FTF9X8NA"
  "T/ymc1a/HjLuvyXYhjzjh+rPM134XA8ZmrKq0spH/IZ3V1pXW8PrDi8ZBK+a5KD/J1/kqR"
  "pz/9EHecPaPdLrB81mS3XsdJK4ZOtxafriiuClRPJx9T17pIHm4pxgjWdyGeJnJFPWeSt1"
  "7huNK0hk+SY8Rb+TxzBbUh0+VTG3CeHMNRJ8akiV+TfwCj6lHO";

