/* 
 * Resource generated for file:
 *    PVEditCenterButtonClose.png (zlib, base64)
 */
static const unsigned int  image_PVEditCenterButtonClose_width          = 32;
static const unsigned int  image_PVEditCenterButtonClose_height         = 32;
static const unsigned int  image_PVEditCenterButtonClose_pixel_size     = 3;
static const unsigned long image_PVEditCenterButtonClose_length         = 1088;
static const unsigned long image_PVEditCenterButtonClose_decoded_length = 3072;

static const unsigned char image_PVEditCenterButtonClose[] = 
  "eNq1lt1uElEQxzcxPgMXJiaitW2wrSlFPqqpBgpIuxpZ1gJFwJiQYGMhKF8LXrTatNS0TT"
  "9SSgslMalaXoEX8IILL7hqwiv4CvqPk55s+OqC4WSyGfac+c3snHNmqP/6We9Fzr4WIfUe"
  "rZRL4kNUpVINzoXNajk/D9vtkwNygeAvLj5DBuSC8f/fBVJN2aYBGp7p9DPG79sFTJDnVI"
  "qHyGltpaftZmRsImwRrd2ulX2CtlIJMzJ0vNHphvGZCuH4XkYG0O02SRJfLAZOTvx4er3T"
  "LD9QQI5GrU6nXgmf4CwqQdAfHi6CeXTkK5WCkNPTIONjgdP5YGvrJUQJHwsYnKICdn/fAx"
  "eZDA+sxzNNKcICPLGA4NGozTprvjL/yUSA4FNTwxsbAgy3txdE0QAUzOPvI0TAT3gnMgQ6"
  "ZpVcycvDppUkx+amC1G1PRV4ycJWfmzoyiCxy8uz6bQjErF2ior4CsNmmactg+3a2gue13"
  "ax7aO4UfDI/NiYOpdzdToJS8nM0qcvD3mx19vKkmMyjWi1d9uueZtd5R4HuKfv5nsfjA+l"
  "U/DcfRvHx657VrH+Ty+D8XFn1eobnb6du/eEm4teEz8Sv1KpWCyWUCj0+9+Agp942TTVxI"
  "fSKYflb2ecQeDMb7C+0WiAAPNarSaKIlBEho438iniy89P950Kp7Ly/BQKhWQySTqYIMN1"
  "0xTxWWW4sowwfrVapQhpAI6YW6eIPzR0k+4vynL348f4iJABMdiHNE1hPayMxhFUBiX9qD"
  "8+yhfPT/p8j6h3wAUqUlsvfZxPqvwzMxpB0FH5peP047tEN0LuqI/7RYaoOeDH43aN5hbL"
  "Fe0ItXU25E2ZegE1mnL5NZQuJwQuXC5dJGJGFSUUzq282zKhpkydN5/37ewsdOpiHMc1uS"
  "iXg9nsXC4noOGiZ01M3FG1DLQzSZpPJBy7u27AYzE7HGETW+FNfNqL8fHbaB/r686Vleel"
  "UoBaMJovBDrk+NhfKLwqFv17ex606bb9kbscbRs9TIzGUZdLn88vHhx44YWE/kjAEeKnjt"
  "wadvf45V5gC0csLQbDKCnUkbuQ5S7+Aud7lMw=";

