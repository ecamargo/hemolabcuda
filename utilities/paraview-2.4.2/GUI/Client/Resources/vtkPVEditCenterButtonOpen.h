/* 
 * Resource generated for file:
 *    PVEditCenterButtonOpen.png (zlib, base64)
 */
static const unsigned int  image_PVEditCenterButtonOpen_width          = 32;
static const unsigned int  image_PVEditCenterButtonOpen_height         = 32;
static const unsigned int  image_PVEditCenterButtonOpen_pixel_size     = 3;
static const unsigned long image_PVEditCenterButtonOpen_length         = 1092;
static const unsigned long image_PVEditCenterButtonOpen_decoded_length = 3072;

static const unsigned char image_PVEditCenterButtonOpen[] = 
  "eNq1lt1OGlEQxzdp+gxcNGlSW6vGqo1IAW1jGxC36LYpy1ZAKjRNSKipEFq+VnqhrVFs1P"
  "gREQVJmthWXoEX6IUXveDKxFfoK7T/OPFkuyyw0HAy2Qx7zvxmds45M1R//ay2IidfC5Bq"
  "i1b6JfEhajAYOudi0mE/PQ3z/HCHXCD48/PPkA65YPz/d4FUU7ZpgIZnOv2M8dt2ARPkOZ"
  "USIEqaprS03YyMTYQtouV5o+ITjOVymJGh443J1IPP1AnH9zIygB7PqCwLhULg6GgOT59v"
  "jOUHCsjRqMPlMuvhE5xFJYrm/f1ZMA8O/MViEHJ8HGR8LHC5HmxsvITo4WMBg1NUwO7ueu"
  "FicVEA1usdoxRhAZ5YQPBodNIxYWua/2QiQPCRkZ61NRGGm5szkmQBCubx9xEi4Ce8ExkC"
  "HbN6ruTVYTPKsnN93Y2oNE8FXrKw9R8bujJI7MLCRDrtjEQc9aIivs6wWeZpy2C7svJCEI"
  "wNbNsobhQ8Mj8w0JXNuuudhPnk4vynLw8FqdXbypIzOtprNN7VXPM2s8w9DnBP3023Phgf"
  "Sr3gufuTnBC77l3G+j+tDMbHne3qulHv27l7T7ip6DXpI/HL5bLdbg+FQr8vBxT8xEvVlI"
  "oPpV4OS99OOIvI2d5g/cXFBQgwPzs7kyQJKCJDxxvlFPGV56fxToVTGWV+8vl8MpkkHUyQ"
  "4Vo1RXxWGZqWEcavVCoUIQ3AEXPtFPG7u2/S/UVZbnz8GB8RMiAG+xDVFNbDymrtRWXQ04"
  "/a46N8CcKw3/+IegdcoCJpemnjfFLlHx/vF0UTlV86Tj++y3QjlI7auF9kiJoDfjzO9/ff"
  "YrmiHaG2zoayKVMvoEZTKr2G0uCEwIXbbYpEbKiihMK5VXZbJtSUqfPmcv6trZnaLsZxnK"
  "aLUimYyUxlsyIaLnrW0NAdQ81AO5Pl6UTCub3tATwW4+EIm/jPlb8cml14cPA22sfqqmtp"
  "6XmxGKAWjOYLgQ45PJzL518VCnM7O160ac3+yF0NTRcwsVr73G5zLje7t+eDFxL6IwFHiJ"
  "86sirspvErvcAWjlhaLJY+Uqgj1yOr8v8XrVuUzA==";

