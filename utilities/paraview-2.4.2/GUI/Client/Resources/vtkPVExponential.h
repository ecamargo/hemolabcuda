/* 
 * Resource generated for file:
 *    PVExponential.png (zlib, base64)
 */
static const unsigned int  image_PVExponential_width          = 32;
static const unsigned int  image_PVExponential_height         = 32;
static const unsigned int  image_PVExponential_pixel_size     = 4;
static const unsigned long image_PVExponential_length         = 720;
static const unsigned long image_PVExponential_decoded_length = 4096;

static const unsigned char image_PVExponential[] = 
  "eNq91z9oU0EcB3BRCUUXCXFtF0UKRSHBrpKxQ2gx3DtM33tEhNg4lNgk757SkqfBQaRZnN"
  "ouGQRBxUXFiGZwEqFCdRBMKbg4JJQ2DVr/gL3+LibmeOQ909j3G76QKZ/c/e53+R3n/AD3"
  "OIXSex8kD1mF1CEvIUGv3UTEOnKRXDt+58nyc/C4Lb8gof2yYtHsSZWwpE7Nuzphr3SFfd"
  "EUxpPT87yL3U75f0yVGKfAmIWsCKtb0vmim1/vx9WoSWB9H5xMKT9TswsNF7/Sxx6/cLC+"
  "qQp7qBMzC58jKjFPEEIOgRGA1Bz8XC9uPG4NaJTdgO/9YTO3IY/AVTQtc9Tl/IchVZt9D3"
  "L4X7Z+IXMa7DWb+1ucs/hE6tgeetAPiUKmej33k9Q8C9aGzX6r0qznvasTYxSshuRu6pRd"
  "tizroNe22Fc4458lu6rR6yMc4a5s9pfCHkh2TaPGMJYNd1dMsndgz8ewbFFbMD9JfgHL/r"
  "N2k0r2ultPe1T3d21fVYwcph07nx6S1v79Ernqx/TFf2fHN55h2s3aK+zp372H34Ltg7vV"
  "9qEHBzHt5n3Xqf1X7LWrlJ2R/I/otadsXPJL+L5xRfIX0fcfznsieZun5pb4zM1iScwKWL"
  "aYgW7df/3GNhuJWSmM5OccZkMxMwYQ/IrLfKwj+HUX30Dwyy7+OQQ/1HoL2u3HiD0QbL2J"
  "6603sngr+zDsXWJiTig=";

