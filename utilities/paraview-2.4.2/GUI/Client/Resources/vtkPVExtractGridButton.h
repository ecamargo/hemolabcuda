/* 
 * Resource generated for file:
 *    PVExtractGridButton.png (zlib, base64)
 */
static const unsigned int  image_PVExtractGridButton_width          = 32;
static const unsigned int  image_PVExtractGridButton_height         = 32;
static const unsigned int  image_PVExtractGridButton_pixel_size     = 3;
static const unsigned long image_PVExtractGridButton_length         = 796;
static const unsigned long image_PVExtractGridButton_decoded_length = 3072;

static const unsigned char image_PVExtractGridButton[] = 
  "eNqtlj9LA0EQxVOlSpPqeiHNIaSR2IQDFcUuphDRysBVQUFQxEYULMTWwkJSCdZWgh/B0i"
  "IfyOe9+Bhm9y6X6DEcw+7tb/7u7k2/Pqfz5OP9bfL8BLm9uc7zHG/o0xoL5wLz34dMjEPn"
  "7EKGHBALKZzlFHXMSqljyIZsP7PMMr5muTBqSJ9VMKUTQt3yrXUlAdFV8DELZ5gBxqi1jh"
  "CNTnrID1vFRm3zwxqFzJBvi0udrTI3//yMq6LlU8hkqmeiGX59mWRZFjVEvkxYvq14NNij"
  "w4M0TZMkaa+krdU+lJ3trZqNVMbH4HA4TIoHTGKpwArHYQURRdMY5TOZLBkG6TOAzfUB+X"
  "zAb2yMIByEGyHK5kdtwDe7DoK1jb2rdqdrmRDoDKGd9lr9fbgRbi72tvhi2vyT31rbhcAK"
  "mAiEWJIbo0cIRuxR4HZfWc9A4Jj4VIAlE/CQr0Zix5b1jDYX+D9ud7riiyk+LKK7KnrPnY"
  "TkM11wrLl5TBMhf1brtHd6MtahUcZXaaL5n5UYhoq0k9wcnNPQ5cWZO/oqcm57zOVffOZc"
  "gZBvD093Sju+2vXh/q4s/+LDIj5zHMcPz2G1Kw+cMP/MPN5wXsdO9L6zqSOWu1jmoAAyC6"
  "RoG2Bh1O4abfwoX1eJLmKd7foGSciKR9nmKuuGdUymoxdx9DxUDu3Gp1iau3HcX4R8tjvC"
  "LrcXikK2JYg670zYm0INwCldRjZ7NsY6/0JlPoeDdlstJEq4HLNt4DbschJWSj3zR7Lbzq"
  "40/0KOVrxO+ZYzsTT5G+M5MTU=";

