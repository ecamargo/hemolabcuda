/* 
 * Resource generated for file:
 *    PVFlyButton.png (zlib, base64)
 */
static const unsigned int  image_PVFlyButton_width          = 32;
static const unsigned int  image_PVFlyButton_height         = 32;
static const unsigned int  image_PVFlyButton_pixel_size     = 3;
static const unsigned long image_PVFlyButton_length         = 580;
static const unsigned long image_PVFlyButton_decoded_length = 3072;

static const unsigned char image_PVFlyButton[] = 
  "eNq1ljFLw1AUhX9Gtg4dusibDCVTEaIVB4uCgwGLkyIuFbQOce6/cBLc6v9wdMjsb/GjF8"
  "LjNn25ealwyZLLOfedd85Nqp/vqne9vjxRnx/v1T7QKKAEM89LarH4pZw76z+kABbFWjD9"
  "SpIkAnB+s/SHDFSev9Hfemo7oCoOdTo9Dg/MGRmjK7JRoo28K+dm0SxF8RWQCH2y7BGKaB"
  "bLFVxe3AqFsHSlsLho+bzYIK+Gw8OuEpECY9CmJ3mjz/tcgS9U7BWvcXgrPj3/5FIxajR4"
  "6xXwigGQsdPAfn/YpbwiBTiHMSws9IzH96o/vEvFnMISvmVwnLuaTMq6X1gar0CWG08iZg"
  "kyOIPBQZo+zM6vZSSfRUmE7OJ2MZiFBRABV6mUfoWvDNnKIspQyirSr/B3uT3AMhodMXyj"
  "T2SJ1a9qZcKRpA1hhQUK8GtlwnuAk9p2b8lIIrIcwfiRNfrczyYsxiXZNU1pOresR/VNVO"
  "nYlVNxeyd85dsAC7ZsNKSxfAdus2AhDBMN7vvWZ8myO8aGBdn9qPb8t9xOE+DMv6/fV8UC"
  "fk9lwizYJhr8D28MTOY=";

/* 
 * Resource generated for file:
 *    PVFlyButtonActive.png (zlib, base64)
 */
static const unsigned int  image_PVFlyButtonActive_width          = 32;
static const unsigned int  image_PVFlyButtonActive_height         = 32;
static const unsigned int  image_PVFlyButtonActive_pixel_size     = 3;
static const unsigned long image_PVFlyButtonActive_length         = 592;
static const unsigned long image_PVFlyButtonActive_decoded_length = 3072;

static const unsigned char image_PVFlyButtonActive[] = 
  "eNq1lr1qw0AQhFWpkRqr8UO4VO1gSOKgRpWrBBcJ2Bg3CeQXqfZbpAqks98jZQrVfpZ8sC"
  "COs3xanRRYhEHrmb25mTtVvz9V73p7eaK+vz6rIdAooASzKGLqeAyoLEv6DymA+30omGaN"
  "x2MPwI/3C3NIR5VlTH/rqvWAVrGom/mVe2DWWJZRV2SlRODvdkGex94sh0PokAh9ttsQCm"
  "8WzRY83E+FQlj4y4ASSb0+P+Z5BH6ajrpKRAqUQZtfXzb6vM8WmEJ1VaZ2KQ5vxafnn1wq"
  "RvUGb90CXjEAMnYa2Ox3u5RXpCBNkywbaVjoWa/tfn479MH2mFNY3BEDZ7EIiyKo+4WlcQ"
  "vkcONJxDRBBmcySTab4O52KnkxWSyJkF3cLgbTsLCPAm6lUvotfMuQrSyiDGVZRfot/HNu"
  "d7DMZjHDN/pEDrH6Va2MO5K0cRwJCxTg18q4zwFk1JicG42RRGRZgvKSVfrczCYsykOya5"
  "qWy1hzPFp3opWOczkVt3fCt3zrYMGWjYZUlunAUxYshGG8wU3fmiyrFfdvBAuym1Ht+W15"
  "mibAmX+oz1eLBfyeyrhZsI03+B8th0zZ";

