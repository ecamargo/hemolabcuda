/* 
 * Resource generated for file:
 *    PVGlyphButton.png (zlib, base64)
 */
static const unsigned int  image_PVGlyphButton_width          = 32;
static const unsigned int  image_PVGlyphButton_height         = 32;
static const unsigned int  image_PVGlyphButton_pixel_size     = 3;
static const unsigned long image_PVGlyphButton_length         = 440;
static const unsigned long image_PVGlyphButton_decoded_length = 3072;

static const unsigned char image_PVGlyphButton[] = 
  "eNrtVrsKwkAQtEqVJlVaOxEkhZWCjUjEH7CKlZ1WImJn7U8IgmDnh1ha5IOcMLBEvdwjnq"
  "CgLBK93Ex2djO3+e2af32cT4eP4g9niziOP8ECTCAH2R6Bi+1m5RcfmI31heGdAlBRpw/M"
  "MJ0DXCiMQlkqCfxwmDWWR3yDKEoGIpRNvcbpSE8k+IxgumM63Fu1C0t4BqQcNVsIjZ7UBx"
  "QSgl+lEsFZL+QbtnuaJwECVuPHD/4BrxKczUYZSVH8bLb0KmEVgDZtQz0pY7lYmhRc+03q"
  "JSwohLEfnPpZWS9fbw2VfK2X9xf/H79yrBhtql7Y21RtcEubqncmutoUn0rjfm/aFFZpNU"
  "jWaPuuNiV64jbgI+odK8pdxf3JQMaAIuXuRO+ZTjZVHgM4A0AfX578NAZwWEIKHlu6rCdY"
  "WC/vk5JS0u/3wDt19hWe";

