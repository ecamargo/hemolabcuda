/* 
 * Resource generated for file:
 *    PVGroupButton.png (zlib, base64)
 */
static const unsigned int  image_PVGroupButton_width          = 32;
static const unsigned int  image_PVGroupButton_height         = 32;
static const unsigned int  image_PVGroupButton_pixel_size     = 3;
static const unsigned long image_PVGroupButton_length         = 1448;
static const unsigned long image_PVGroupButton_decoded_length = 3072;

static const unsigned char image_PVGroupButton[] = 
  "eNrVVltTUlEY7SekcjEzrwmo5Tg1QkDTRSu1RFMwUUFESm0EsUCxjEZD0dIZKVHBC1p2kc"
  "DUmXqpZvprreOHxyMcCaeX2rMfOIez13dd69u/fv3TKxr9Pjm5Szv5lwsrIUO/yz4yljpy"
  "ff2GSLQgFi/SLi5e8np5rCyursubTMKq1ixtf6amO01ZZ+gf+iO4TLbEBcfGI3aiCYW287"
  "TemWvxFFhn83tf5XS4RbWdnunZpOBBQBUXL7NpwUuzeRMvhcJ5rgmkRXijLdcyLnWFzo9u"
  "lrrfF9rnsvWD8rvGo/A1mncAQXIS/wKySDQvFPphjt4Y7cNZOnuB1Vc2uql6/aPi1VeZay"
  "2364Xg+r3PW18SEYLBbwKBH8mhx8jWNqoGZzwzPnrjcERhHT7Qo/3Zi8z6nvze6VL3B/n0"
  "1wvebYlz6UyHu+h2O6/zSuUq3IMV/K5oMAiut6BqiFd8u6uoRu+ZeY33anUIPrAhpKvrc0"
  "zPC+1+2fAawPMfzmRpbbwljkS+CwRz5Bs6LVPzAJEW9M3ibF73VHabS9FsYWM0mT6yKYL1"
  "7NYhfAzP4U/65YbPW9uJ+I8fR4BPzit05uz24bOPFlC1ck9U9mQ99/6E6JZxcWUN/6pUqx"
  "JJgD2I7KFFBZUtaco78DzCB45VV/dWKg0yXbG8KqjUo+UQMrKqfvOzbCxcYPNlNQ/UGHuo"
  "ChkZc4j3EMWWQ8nbPiPjDVu4s9X6M8aRIkcQzl+c2i15tpHX81J8x0IlQIzAn5jYOZYaAN"
  "/rjR0ZeD5xqrEPmEiRxLkM7jDmals5H8+xJUhlBQLfCP/qVQYE3XtSXgO+owo5JjeaHApg"
  "sDrY76XSAPKZOj6ChUsI/MSJ8pKSa35/kNpb0dyVrqxDk4CqcTRMjg+cpPscAgmHvySh+d"
  "/hl+p01r/B580P8r+XnyvhcJRYptCa0y6hq11xXX1cfDRzfH0rqkFh0JZhpa4f9GnvO6gv"
  "+HUsfOpPhyOy35/jp+72QRb2+nMJKsH0Z82h/gTfucd5BZO7JJJFDr9a9vgVYPg1uVMyAn"
  "5N7fHLl8gvz7QPGgtNhmwabENHGUK8EK6YPlQl04fOzk/Ap1MGu0tca4bGxvStyZaubuA1"
  "AX+gbzSeFLquA30bJ33ziqpj+gaZgpLH9Fmliemza1+fm2wG2yBvCMBXqUL7+tzN6LPVVz"
  "gwD6FAoQurtCRuCJOSz50vGF4H86W2jRcfgWO+UAhMSqv0UAZoOzKP4pK4YTqz8+VgPo6F"
  "U5mPOEXzkY5DcoEgb+ywPx2lDzCXMR/hRuJ8LxsNIwokipnvjUfOd5qwrAnuInCaEZz7if"
  "k0imsZRxRIFGqBcqOjkjQq3aywcSehy5vTuYXrCt1P4uziqgMTwpvtSBRqgXKD7H/kWtzl"
  "je5XCIr3CkeJQiZR7tTpjEThTigWL9BGLIkZ+9/Xb4nq3Zk=";

