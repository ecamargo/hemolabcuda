/* 
 * Resource generated for file:
 *    PVHideCenterButton.png (zlib, base64)
 */
static const unsigned int  image_PVHideCenterButton_width          = 32;
static const unsigned int  image_PVHideCenterButton_height         = 32;
static const unsigned int  image_PVHideCenterButton_pixel_size     = 3;
static const unsigned long image_PVHideCenterButton_length         = 956;
static const unsigned long image_PVHideCenterButton_decoded_length = 3072;

static const unsigned char image_PVHideCenterButton[] = 
  "eNq1Vl1PGkEU/Rk8NGlSKlVD/AgrhcU0prHiit1+7C4ISAEbX3yRjS1bWH0xwSgaNX5E/A"
  "BJmjSt/pI+9oEnf01PctPJBLY6LLK52YzLzDln7ty5x9af361u4sf3OqLV5SrxsL6aHo+n"
  "fxSz0Tc3NyuKEugTBcTf3VUQfaJg+L1TINWUbXqAhne5/I7hu6bAEuS5VFIRPJpjdHXcDB"
  "mHiLVQqygStwXp9naFIWOML8HgILYpCI79MmQAJpMR21br9dzVVRbvdHqS5QcDIJtmVNNC"
  "IvgEzlTpeujsbBGY5+eZRiOPuL7OM3xM0LSX+/sJhAg+JjBwUgXYk5MUKNbXVcCmUpOUIk"
  "zAGxMI3DRnozPTD+b/m5Uj8ImJwZ0dHQsPDhbi8TCgsLz4pUAI+BPshIzAGL+KXMl/xSbZ"
  "dmx314Aqx6rARyZbvGzoyiCxq6sz5XKsUIj+TxXhC8pmmacjw9qtrY+qKt2z1kVzI/HI/M"
  "iIt1o1BCu5W3xsIRIZkqQXfepXdJt6FM+aFY9D+LizXu+THvstL7UTn//oTjzVSRs+Xz+P"
  "4guUIsfO4Dr/vPhEIhQI+PiffL6nRI227M4sSCFifHwgnZbbbpAsD6EzuPMjfvu4m5XKh+"
  "Xl120IaF+qGshkXpF3YD460oMszIkIHDlHe7Es5fOS1jlzasqv60Fqv1ROv37aVMltRMyU"
  "eSdCR9rbi5dKc2i/jsKQMeAXi4rf/4zlik6EbJ09vCmTbDKaZnMJg3sqBBSGESwUprFNgo"
  "Iw3m1ZkCmT89ZqmcPDBUEXA0Wzmd/YmK9WdRguPGtsbMDT8cDObPutZcWOjpIAX1tTQIRD"
  "FCy20dHnsI/tbW1z832jkSMLhvkiMEZcXmYvLj7V69nj4xRsWsQf2yiwRJaHDSNUqy2enq"
  "bBQkH/SIAI+smRBWU7smAtiFhawuFhGpAjCyL/BQvTvYA=";

