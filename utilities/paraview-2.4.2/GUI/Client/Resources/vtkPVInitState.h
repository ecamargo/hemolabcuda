/* 
 * Resource generated for file:
 *    PVInitState.png (zlib, base64)
 */
static const unsigned int  image_PVInitState_width          = 32;
static const unsigned int  image_PVInitState_height         = 32;
static const unsigned int  image_PVInitState_pixel_size     = 4;
static const unsigned long image_PVInitState_length         = 808;
static const unsigned long image_PVInitState_decoded_length = 4096;

static const unsigned char image_PVInitState[] = 
  "eNr7//8/w/9RPIopxKFZ+1VAdHTuCb7Q3EOipOpnKChbxlBQPgFIVzLkl/qQZHfegdjQ3I"
  "N/gnMP5AHpo0B8NbTwmBBR9jIwsAExB9Du/wz5ZU8Y8kqPMWQXtgPFeICYF4j5gJgfigWA"
  "WBCIhaBYGIhF7P37HUKy978F2vsfhAPSt63h5BSWAspJAjGIloZiGSCWBWI5KJYHYl0gNg"
  "fbD8I5xc8YEtPXAMWMgdgIiA2B2ACK9YFYD6oHhHWAWJtfRMUoMGPHRZj9HlGLuoDi6kCs"
  "BsSqQKwCxcpArATEilCsgAh/qP0F5b+BeC0p4R+Wd7ABZG9Y7qHdQPpxaO6BK8Dw56RX2k"
  "tI2M8BtLMutOEqW0TBfoXI/D3iQy39U2Q3BemfGjg897A+0M7XsPQXkndgcUPDfyYi858F"
  "EDtQgrn4JN3907Zcg9nvGDR5BrF6qeH/0fQ/sBjkf1AcQMLhkH1Y7gFfetofknOwHpz28g"
  "6dAtKfgfhLZPF+EXrZ75m7jT0k98AhsBtyDv4D+j+S6HqXCvlP0zgmOThrzyeg/f9AbvBL"
  "2XSRi0vUjQi9NtQJ/wOJQHt/h2UfjA/LObAcyJ48UOU/qNxjYPjPOJr/qGM2Me1CWuY/aJ"
  "voMRAfBrqhEZhmgfHLwALErEDMDmo7yun6CAZn7z0KyX8H/vkkrk6GthPB7UMgFgNicaT2"
  "IKwdCGr/meDNI/B2YdFzhvjUDUAxWyC2BmIraN4117VIjQLa/xGW//zTtp3h45M1hbUPgV"
  "gTiDWQ2oOwdqACkf7H2y4czX9DFwMAuSXQig==";

