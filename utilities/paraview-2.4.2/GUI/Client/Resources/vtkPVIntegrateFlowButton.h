/* 
 * Resource generated for file:
 *    PVIntegrateFlowButton.png (zlib, base64)
 */
static const unsigned int  image_PVIntegrateFlowButton_width          = 32;
static const unsigned int  image_PVIntegrateFlowButton_height         = 32;
static const unsigned int  image_PVIntegrateFlowButton_pixel_size     = 3;
static const unsigned long image_PVIntegrateFlowButton_length         = 1640;
static const unsigned long image_PVIntegrateFlowButton_decoded_length = 3072;

static const unsigned char image_PVIntegrateFlowButton[] = 
  "eNq1lulTWlcYxm/apMmkcQsFZJFVdq6SUGWRisQ9iogIGhXZkdVdo0mUpNqkJjFUcAGXKE"
  "6djDOZfuh0ph/91/reC6JzsZ2Y6pkzDPdy5jfPed7nvIeTk0uPTCZzcj0jsZkenZ6Px+Nl"
  "FOrB57+vFp6OpxPr2/Al6HEjyM3I809XCD/6uKHp8ucfj4+Pr1Z8p065e/zXybWN29ya64"
  "OHHQ6D1Xd9fIOq2u15lYvQ1o5Sq79LF9YZB9Z3ryaotxBkKfkZyDqTvVSsKRGpSQ9aKZoe"
  "WsPQZub/FvpoI4kgd3Xdw+QaI00/SFF3U7W9dIOd2TrCNo5xe2ZSR398/YFK7XTUNdz4hk"
  "1rGGS2+gFIf+SoaAuwu8a5lqf8vgXBwJJw+E3605+XJ+/+ZHZSartK2Sinqg2XOsuzvWCb"
  "JniWOX7/omBwWehYEbtXxb4108L+pckqE5gAUito7FJpI69voXLgZ6H9TeWTl4KhX0TOt2"
  "LPB4k/IQ1syIIpWXjbE4x6w+O+6JR/Yv4eS1oqVOmMAwRyEsg9Loqqm24YrmgPglSuZa4c"
  "Qe6jduHwryLne7Enjk3vb5KRpDSwKQul5eEdeXSv8smrYt7DEmEtSV5PkuvLJFpvZLKQTF"
  "WbMXsfhzimSW7vPL8/BibcRJDq/hXASnxJ6cgGYKXBLRAsj+yi0Y/o6D7P+ryidYSE6n+o"
  "MgD5HhtNbKbOk+stbqqmh/HIyXoc5nRP8Xqf8bMmOFZqzWMIcks3e4ipxX2Q4YLRUSAf0B"
  "vsZGV7MV9JrukEQomgRt1sInhC1VgYjS5WR4RjngYlsFOh/TVmr/sDFA5VtiMITRZKZQXL"
  "ccHo2AEILuJUFXEVrPYg6IE38tF9cLWwmqzOKMc8A8HIlk/keodbkcCsCG6BMLawGgTnyV"
  "XjGVZ7CN4LhpbhEV7CT/LIHuwLZKQLeiAWjMElrHyu91jh/Mm8FSCYJkApqj6Mg5OxOXEI"
  "n7jgPDYtDWxJRtZhv1NrxEsBTBZBhr1rsACWwWLM4UhOMIIgVPV0jjx+mJ3oWAZ2JAttww"
  "ZxbAK2DPJA5OTqIYEvzlmRgvUyzOE9Oe5DVjDwUVs6j8Un7klkV3KGXYEwQOF4tgXIM4Gf"
  "t+K8w9mpcMQQ5DvcjRwWYoMJgMXhHbFr9TwWgsfqHIVTQ+ATBJ86jE2quhspkkEOMfIpFo"
  "toKAU1AmfOYQOMJje9Yai8zkbgF2LzgqFFsNp8hVhoC2JvHKoGkWM0uej6wXKtFToV6UFL"
  "YDZG4J8jE31gCuXMxtgZ1odjXatCx1s4I9A5K/tjbOM4SdFUKtXBKfu+QprY3C7gHxLtza"
  "lN30Dua5/+flpHwEIdX4Nmft8inCmueQbgYDhVawHyt3eKFMofC88XTs5VLYvNpk6qNVLV"
  "zrN4YNgFnmUeNzwKlwt0KmqdlaRoDs4tJ1N7/9aNCdh86spE9TLfOsSDb3sBVwl0POhOjG"
  "YvXGFklane4gnMLH5Jt89j+XUdZL4Kzgj0TFajS26eggsl6wA0qHKdjaRoCc7/l9QLh9gd"
  "FznfQeeBo3SnmMSzPmPpBzitHmaLH9oj3AVfLvXCkTUWSzKDfJvEZFY+ZNa0fZ3UCwfmQF"
  "uAbnBQNWardywwE7va/07/ACTmWsM=";

