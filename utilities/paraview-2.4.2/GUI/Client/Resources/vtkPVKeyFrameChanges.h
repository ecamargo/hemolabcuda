/* 
 * Resource generated for file:
 *    PVKeyFrameChanges.png (zlib, base64)
 */
static const unsigned int  image_PVKeyFrameChanges_width          = 32;
static const unsigned int  image_PVKeyFrameChanges_height         = 32;
static const unsigned int  image_PVKeyFrameChanges_pixel_size     = 4;
static const unsigned long image_PVKeyFrameChanges_length         = 608;
static const unsigned long image_PVKeyFrameChanges_decoded_length = 4096;

static const unsigned char image_PVKeyFrameChanges[] = 
  "eNrt1ksvA1EYBuATcYv7/RaXdi51a92qnTEdiR/gP4iFWBCKIBIbKwsLXVtYWggiERFBXF"
  "OatCimC5GIEDb+gcV4pzmoBBFpsTiLJyf50plv5vR8k1fXdaIzzB8j7pE54h71YB0j/cPt"
  "0binJrh2NUENYX2Eow/7EpIIyeitk/6RO9I37CU9A5OopUE6ZEAmlQXZkEPlQh7kUwVQCE"
  "UGPy9vBq3Oy6DY8hDg5EPUSqEMyqkKsIEU7m/oHbonnd0LqNmhCRqhgaqHOnqNwQq1UENV"
  "QxVUGrxm55pfdAZOePnKx0m7qPHAgZkyve0/7e8efYLF6Oy/Moh9n9B4xRMS1XF2zn+XJr"
  "qmQoI6g/9g/kJwTf9B/1WtUjnBeoOzsPPJ/MnQFgtbFfZtn9V+6bdIt3smh++j38T0/QV1"
  "NiQoK8a3B3uwxM4kmz82f++obP6YWOXCaMwfzUS3sI9nmMCZjYN4SICkcHYkJAVSI/JiOC"
  "ce8y0bQYt8fiYod8ec7EWtGEoicqCR/5q/nKPXXDj4QDq6llFrBRcodHYlcND7vOTFcE48"
  "MDvWA7Zm7dQiXx9x0h5qFhAjcqDpm+//o1zI5u//ewbWivA0";

