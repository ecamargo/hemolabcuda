/* 
 * Resource generated for file:
 *    PVUnlockedButton.png (zlib, base64)
 */
static const unsigned int  image_PVUnlockedButton_width          = 15;
static const unsigned int  image_PVUnlockedButton_height         = 15;
static const unsigned int  image_PVUnlockedButton_pixel_size     = 3;
static const unsigned long image_PVUnlockedButton_length         = 260;
static const unsigned long image_PVUnlockedButton_decoded_length = 675;

static const unsigned char image_PVUnlockedButton[] = 
  "eNrbvx8F9M+cm1DXEVbZltbYNX/p8v04wO7de0JKGvSLuw3L+w2Ku40qJujmtuQ0dWJVnF"
  "rfYVQx0TKrqm3KjHlLllV0TTDKqtMv6mqbMhNN5eatW3Uza4Gy6zZuggtOm79Ir6DdNqsK"
  "TTHQeQalvZFVrWjilplV2hl127ZvJ0axV3ET0OUbNm3eTwSgUPGGzVvUkip0iroNqyZbt8"
  "xz6VvhPnG1c88yy6Y5BhWTdAo79ZOKR47iXbt3E6kYAACAh28=";

/* 
 * Resource generated for file:
 *    PVLockedButton.png (zlib, base64)
 */
static const unsigned int  image_PVLockedButton_width          = 15;
static const unsigned int  image_PVLockedButton_height         = 15;
static const unsigned int  image_PVLockedButton_pixel_size     = 3;
static const unsigned long image_PVLockedButton_length         = 268;
static const unsigned long image_PVLockedButton_decoded_length = 675;

static const unsigned char image_PVLockedButton[] = 
  "eNrbv59OYN3GTbW9k/NaulsnTd+2fQcelYVtvXpZ9UYVE4DIsLzfKKO6d+Y8rCoruybqF3"
  "cb5jSkNXRWdU+MKG/Sy2/Ty2masWAxmspdu3YZplfp5zTNXrwULljbO8mwtNc9vxZN8cwF"
  "iw1KugNKGtHETTOqtNOqd+zciSw4df4ioCGJdR1oit0LG3RzWzZs2gQX2bB5i1pShU5Rt2"
  "HVZOuWeS59K9wnrnbuWWbZNMegYpJOYad+UvHIUbxr924iFQMAMZOMlg==";

