/* 
 * Resource generated for file:
 *    PVLogoSmall.png (zlib, base64)
 */
static const unsigned int  image_PVLogoSmall_width          = 95;
static const unsigned int  image_PVLogoSmall_height         = 16;
static const unsigned int  image_PVLogoSmall_pixel_size     = 3;
static const unsigned long image_PVLogoSmall_length         = 1304;
static const unsigned long image_PVLogoSmall_decoded_length = 4560;

static const unsigned char image_PVLogoSmall[] = 
  "eNrtV0FLVFEUvm2jzQj9ARct2oli7cyYWkU7cVkM4SIIoRZDRdYmMtFFbkRbaGlpRlIEQQ"
  "1YEBXWjKJSi8ASETTScSZmSodpXp/vcw6n++5oyLipzmI4977zzj3nO985706hsCEeJJe7"
  "Z4zny55Pu3Nra1Dq69+lv30taLN/RuysjTnv42OgjfrKuna6UPj5Hxwvn39mzEo6uw5Lxi"
  "TmX/r4pAYHHwbBaQ1ILBYre4Td3d3wjF9rf3h4mIcmk0kqOw6O583uMvd7+9dzL1w79uQo"
  "lNsDXk1NRxAc45JwOFzeCKPRKNyGQiG9CUCwg32AhooEDXYInPGqqo9vPkAZ8UZuTF6Gkk"
  "gUIpE7llk8HicaQpvKykrulJc/SJ9uAYgmLXZwIiNxUmsnwOkozuSKiYql5CKU2trpsbFx"
  "ywysZsy6mhY4MzMzbAoIUtDAwgb2fAoz7mOTxtgXKEgM7VZogxjEmzgRuKxDY75ot5ssne"
  "CsDg21HDoMJetlzRVTbJ8TQQxZO91EkgVDkoqLSHHJMbwrWSOq6upqbYylhCoUdR5NP+K8"
  "qalJ+0FX6kOJsAU4gGVjbg7OpSITzFPzPZOCsnfvi9HR10FwGBJ+iTliYzWRlDQddlBcPG"
  "1oaJB0hGCIFmkyeBogLxgLJ4UqxI2WTEQ/1UsOKNiThHxEUjFgkk0AJKRcBke6zvr9wQMP"
  "bvZCmfKm9rfvg5JK4ejjzu6TCaMFm5rJjAq/zJ3ZWQSzjHXDSh01tkxEaCNYaV1cEVWNG7"
  "uYhbOWFm1+yzqdPiW0GZSGuprP/3CCI+QP+4Kj9aBgW2GfMegysaAkmEYGWevO4rDVfQRX"
  "kr4ASyRpLFQJF4WnExw+5Syiwt9StNFZ9xmTzeWxc3b5XPuj61AuXEw3n+lydp9UP4i21X"
  "ScijpIVhAhWcMKNnjE24s1zYRL0n3O0UfPPFQLg+QpwB8HAUwu4SrkizORjZwXF1uL9wQz"
  "ILRpwXMnOFJKJzLylbeQ1LjpSjFl2QkaCGGsrrHetWDnp1BGk0QlHJZlqQskU8739bU1Nm"
  "CZ8TLmrvH/ZuHdI6XmttTI6VPQYOGks/hUs0ijgZoiZiYbDFgS0bSRqcIxK9/HqC/BSxd3"
  "hCfWsiRzpqf7ZeDMm4WVz/67z+fmEk5wgsV1tpV8koIzU8eD1ARA5O50zs1gItYIEkx4tH"
  "Ud5UdEPMtM3voSCJCNWZ5d7vJumawZTz9ubsTeZGfnkHMgbylxX/7QGCkH72DbFutOWJYb"
  "8qu6uoWFL1B6Vnsm5t9CORlZikTatgfOXyC/ACqTUDc=";

