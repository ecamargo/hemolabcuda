/* 
 * Resource generated for file:
 *    PVLookmarkButton.png (zlib, base64)
 */
static const unsigned int  image_PVLookmarkButton_width          = 35;
static const unsigned int  image_PVLookmarkButton_height         = 31;
static const unsigned int  image_PVLookmarkButton_pixel_size     = 3;
static const unsigned long image_PVLookmarkButton_length         = 2252;
static const unsigned long image_PVLookmarkButton_decoded_length = 3255;

static const unsigned char image_PVLookmarkButton[] = 
  "eNqdlllMlGcYhecCbS+0KcYGYw3GrUGS0kpLSY1om0JVrMENEBFlURAKCrLbABIKKAjIwI"
  "DDjsq+iaCUfd8JhC0gxAgKFRQhLnhhUft0/nRChqXS92LCDP//nnc553xfVdU8USkL+dea"
  "mpo6WdTW1vL7gwcPBgcH09LSXF1d16xZIxKJIiIi6uvrq5YYAgTJebehoYHP27dvZ2ZmRk"
  "ZGamhoiP4NNTU1IyOj8+fPnzt3LiwsrLOzs6Ki4kOSC/kpm+RdXV3l5eUxMTESieTSpUub"
  "Nm0i8/Lly3V0dOhCLBbHxcXR0a1bt6ytrU+dOqWnpzc1NfWfQGRubW1ta2sD5caNG4GBgc"
  "HBwZaWlsrKykpKSqtWrTpx4kRCQkJeXl5BQUFTU1N/f39PT09+fr6np6eVlRVYPGxhYUES"
  "ylsIpbm5OSgoiOft7Ozs7e2Zhqam5vr163ft2uXn51dYWHj37l1eZztFRUXp6ekBAQGkNT"
  "AwMDQ0PHny5OnTpwHi08TEhGKGhoYWwqJ4FRUVLS2tLVu2MJ/t27dT55UrV1jKnTt3MjIy"
  "eJ1fyHnw4EFjY2MzMzOArOeEhSzu3btXLou5QCtXruR1dnr8+HFQaM3Ly4uvvr6+Dg4ONE"
  "ipoLAIGxsb64WDpnjMx8enr69vYGBgLpa2tjYZyJ+bm8suGKBUKj1y5Ii5ufm8lS8SvMLw"
  "79+/zwBLSkoUgHR1ddlRmyymp6chlZOTk7u7+1JRhKYYS3Jy8vv37wXezgbKysqC2I8ePU"
  "IsgkBoirnxivXSg+k5OjpCobmbQpgAsXQgbG1tPTw82JS3t/f/aIqO2MLhw4cZPk319vbO"
  "xgKIArZt23b27Fk0+Pr1a7DoCCBm/iH54QlPCoQBi3UzDUxJ8Cs5UHV1NQKBXSkpKePj45"
  "QBBN2hfbxloabIRmYzWfDV2dkZrtIIYwcOovr7+6Nf7Gs2FoHMaZMNHjt2TFVVFc+hNhyM"
  "UgVJCpPh63FZMGQy407YyM2bN7u7uxE+3AZXeP7AgQNsv6OjQ6Gp4uJinOry5csrVqxAPm"
  "5ubsuWLUtMTISuVrIwNTUFheRXr16NiooiOQRmOHxitiiRzHKjEEwJ0OzsbIEDs7HKyspI"
  "C9uhN/vixQ0bNpSWlmIIfGUsElnEx8fjh1gH9eBCKI4aBBoojJfhuLi4IF68VI7FH/SOBH"
  "gecw4PD8fTNm/eDFepn1JDQkKwPv5L8WRgetQsr3+hOHToEEeAwjEBLp6ck5MDUIIsGCCs"
  "ePjwob6+PqYBnODVS4r9+/djBQxQjsWOIEljY2NSUhJj55OmmBtn6Lt373bu3Lm4180rKx"
  "jIBKCxwlFFX+yXFTC62NhYtMC+OJKYKuRnax+IJfATCCbM3xSJ8yhYHyQfHh7mjAgNDU1N"
  "TeUtQVYoHXUsKuF/pIpwjx49ijlYW9tIJFEtLS04AGelgprkWOyLjmAacJwy+/bto7C3b9"
  "/u3buXacyunOB0FcRrbm7h6uoWExP/+PGTkZE/KypLa2qq5TeFeU9D2I7HwgqaEg7f1atX"
  "v3r1ihqoVvAZChCcx8bGNjAwKD4+sauza3JybGCgTip1MjRax0Fa31A0/mRSQUqzA01NTE"
  "zgtNHR0TQlWI26ujpnjaenBzPE1Vxc3CUSaVp6Vl1d9eBAe3VNWnS04xm7b37W+1TjS5H2"
  "V5/8pLkuKy0lXCzmMkDlC90lkCo3KHwAbqAm1rpjxw4aiYq6Fi6W5ORmlZQWVlfm1pddz4"
  "5zv+ht4OdnGi31iAgNNjE4+OP3Ohec3Aqy8mJiYrEplLIIkLw1GEhfbCQkJDQ3l3tQTqTY"
  "KzLsQlZG8LXwX92iXH9vLPROSZ0cf2b6y3f6P+i6OjuXFf/R3NqWnZvHqQ1dKXVxIMiPmX"
  "N7wRySkpKfPn3a2lbo4Wq8UVmkvVElNdLzt/7ePZUNqicsRWvXtTx/Hu0f0NfXPzw62tLa"
  "yoq5nsE6tDlbrYvfWlHus2cTSclee/Q++1h2Cn+99vP8GLFWVaVo9x7RVjWRmVn/6CgZa+"
  "vqEB33k/b29rlX60WCTc3M/BUWJp6ZmVbfKvr2i48cTM2TExMzMzKbSsrsBweVdu8WpaeL"
  "LC0H6uoqZZmZA+T8kOuxQjDnysrqtrYy34uWLS3t3T09ff39VN5aVXWmo0MUEnL95UuRj0"
  "/+yEhDcTFLrfpfARnGxsawvjdv3kil8c9fvKBaYSAtFRUn6+tzpqbqy8vxDYeenpTR0dKh"
  "oYWw/gZ/pC7V";

