/* 
 * Resource generated for file:
 *    PVMandelbrotButton.png (zlib, base64)
 */
static const unsigned int  image_PVMandelbrotButton_width          = 32;
static const unsigned int  image_PVMandelbrotButton_height         = 32;
static const unsigned int  image_PVMandelbrotButton_pixel_size     = 3;
static const unsigned long image_PVMandelbrotButton_length         = 1392;
static const unsigned long image_PVMandelbrotButton_decoded_length = 3072;

static const unsigned char image_PVMandelbrotButton[] = 
  "eNqllstvW0UUh9dNyYKWRStVwK4qEmx4LKBiiQRSgUIDpFEqCAgoilSgXSCgD0pJ6pgEHJ"
  "pHC3GSxk6dOLZzfV+e+xj/a3wzE19fv6Q0lo6s6zm/882cM8+42YzTJpVFshnGTRHLRiT9"
  "SHqhrPghtr7fwMw3jbgQIEMcyYPYuDnYZJqvYoF4YWwHcdWPcjvuuzfXMD74SyMuBMgQt/"
  "nyMPCmH0oniOtC2b6I9rzw3pZ17OMZjA/+0mi8yBB3pCAH8iNtQSzdIC45wVZdMNRCXfzy"
  "X/X9O/kzUwsYH/wtaBcCZIgDncLAKsnuypN4XajwO/nar2vVZ8ZmzeCN8ZdGXAiQIe6eBd"
  "kfbgavpjWUVLhgi4u/b5y68mcaboxGXAhsXR9CulOQfSrTXjZq/PGuG1KKXrgxXAiQGX6S"
  "QkeVOuFBil/xwvmS89aN1UF8XAiQJXzCu7towZPBi9bKZGBPnOCzmceD+LgQIPNa/HQKkR"
  "w8+Ei6rWV/O197djzTC6cRl9kIiJO91pVCAtf89oY1i3+/Ee244ZVssZdPIy4EZgsk2znQ"
  "KaSz6Bo8StYzo7JEVPEVf8MS15fL564ujn56H+ODvzTiQoBMpRC0qxR08g08SMFN5dlBE5"
  "nC2z89Oj3ZZ33SiAsBsm09C0kXQaqLVmWaIjI1UeMB/tG9zYu/rQ+a2bQhQ0yIpU6MCIiI"
  "DvhteKyqV/bCxV03V3IeVrxz3+QOAzeGmBACCQfip6qUlN0N5cM9b3pp98Uv/3rjx5XDw4"
  "0RQiDhQNzURFArYRZkoFYLYzj79d9PCzdGIOFAQKkU9EQka4a6cepmivaZz+ePxieQcCBO"
  "ai0lcGZnY7/x5vWVkSPBMQIJB2LpWTZdcAexB+mUeXlsiZenHxw7Kh8jHAgogGCBs24fVf"
  "wflssXbudvrVWfm5gbhk84EFAAwQJnD84/sU9NZo1g9JP7w/CTcIBggT8oey9MLRy/NDMM"
  "ttcAggVOofK1xqvXlo6Pzb5+benE5aHqc3JiDgi352vfL4MFztHEdKxWPOZ9pexydg3DP/"
  "ttDshazV/d88oark/j2BZqffIS+PBwZ87As+jueslVB5GtLp3Yb20B89qB/8HdjdHOp8JT"
  "TO7YLOE7TuAEcfrGTF+IJPVFtjhypOn+an6b8PR12Tri9BGkU+B5yfPp5OW5vl2MaOsLPz"
  "GeYdkziWbwonWRpd8kti7Re7fyr0z/887P//Z2Mf7HJtbd6aUZxC9dXeRpuuMGVL7rrXLQ"
  "RSTZ0ZtWgzO8aAdsjZv52vNTCzxCTk9mz99Y/S5XshoRxsd53YgLATLERVsQyOEARESy7x"
  "UvdArmMuV3yxLZbZuneKZQr/rqVOSdgDnqXaEacSFAloTY+vJKw/8HquJr3Q==";

