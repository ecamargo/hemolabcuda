/* 
 * Resource generated for file:
 *    PVMovie.png (zlib, base64)
 */
static const unsigned int  image_PVMovie_width          = 16;
static const unsigned int  image_PVMovie_height         = 16;
static const unsigned int  image_PVMovie_pixel_size     = 4;
static const unsigned long image_PVMovie_length         = 356;
static const unsigned long image_PVMovie_decoded_length = 1024;

static const unsigned char image_PVMovie[] = 
  "eNrFkzFrAkEQRgdBQoIgSkAIEgQVBLFJF7CzkCiawlps7LSIiIXYWNgrBAzpEhJFSWHATq"
  "P+tPUtbmBZRNHG4jF7s983t7Nzp5QSdUFWItE/kRlxsBTpEK9PrYG/gS9IfCPOT3x/GjbQ"
  "w98kdiF5Rh9F/J+mn48z72Jm+hmx/oU6ZHVNCMAthOAO7sHj+N+N7gWu4JFaNfhmPYRXfc"
  "9mrc/qc/wteND+/9xGJLwWKbkzI1fcdwdQdvwRtM+2biESR1PY47+BqeOP4c/bOnIJN2fV"
  "aMPE0WZtDc9JyB2YQwV+oIquABl7n+80xd7TkVn6IQd97n9OHK92c/jS5zN9etWF/78t8c"
  "BXKA==";

