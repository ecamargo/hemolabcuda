/* 
 * Resource generated for file:
 *    PVPickButton.png (zlib, base64)
 */
static const unsigned int  image_PVPickButton_width          = 32;
static const unsigned int  image_PVPickButton_height         = 32;
static const unsigned int  image_PVPickButton_pixel_size     = 3;
static const unsigned long image_PVPickButton_length         = 2124;
static const unsigned long image_PVPickButton_decoded_length = 3072;

static const unsigned char image_PVPickButton[] = 
  "eNrFlmtMU2cYx3XJsiyZc1MR6P3OdYgTrKjjJjeROy0X21OoFyq3trSVtlAqlcLoRHDKRR"
  "EV8YKOKDh0osa4JVuyBN2yZGHxy+aGZlMXzBI/LNk+7P+eU7AoTDeX7OT5cM573ud3nvPc"
  "3ufGjedfPUf63fu6KIMtXVsNwQ0esXjj5a6h8yMak0O6cfOyeKVfkmp5aol/WikEN37JKi"
  "ziFTZg278hmxuWxuYBFZCpC8ytYimMbKWJXWAmojThEYsBmWXYsDQ2H5tf/Cvuji7ZJgqK"
  "gdnl4HCKa7nqOq7GwStx8kp20eLEIxbxinwouxyboQLF58IpgxU/HpCxnZUPspVLNfC0Lv"
  "5Wt2B7i6CslRYPT6FnJyqwyNe6sEFe/b629XBIsRGKCM1z4ImFAVk74ASuuh7qwAp1HmF5"
  "m7CyXVTVIaraB+Fl61irYsmizqPpGPzp7r1Ln1y+/f2Py1M1fomFgMztln1dxHLACy1cys"
  "Hf0iQAuWIvgGL9AbGxS2LsltQQ4edVsaPisYhXe05fTFGq3ylvmZqaWrIuG+qAAPVsQJes"
  "zfTP2M4uMMG3xCE7PhBVdgBCsKaDEnOv1HJYupOIQGlgRyeSRWM3PvGupevziR+MO210Ap"
  "gAAeqpcGssTsQIwSJugeWAV3WIDZ2A0Ng+qfWo1HZMZuuHCApNbHkSWbQczu04992du+qy"
  "SuhCEUFnKQxAAfjE+OER5BjSAAFFNBFEYjng5oMMmWDtA7K6E0H1J4PqTwiLLRx5MhaVXa"
  "O///Hn+Pj48Bdf57l6SOi1LkCAAhBYr/EmB2M8j2rgb2sRlu8VwS2mHhp+TGY/DmaQ41Rw"
  "w+lg5yBEpLJGhMZcWRR3S5x5SFmmdXi09a3hBZVQhDogLEUN+QWTg+GjDFFEnKJakjA6Dw"
  "koomnpJZYT+EmQQ3adCWk8G+r6KNQ1JKbs+YsFNxesYkRmPYrNYjoWUOeTX6gFEFimtyyL"
  "L0AxwnUIK7JOpN9P3F57BB6A5Qwc2HD3+YjmkYiWEXmufhbf1i+t7YMKFKEOCFAAAks3rm"
  "70FhJZjYNke0W72AjP98KqIPtAsONUiHMwzDUU0Ty80nMxqu1y1N4xp/8aXz62kV8w4xc6"
  "RRXtgAAFoF+yGnDKaIevkFeoehJZpE1NNxJDZjsWVEcb33g2vOncSs/o6vYrMfuv70kwdr"
  "wWAv5ZbpaXX3cCm6ECRagDAhSAwAKOfouWiIJFY2GcL6Tq2Ks3cORJnDXJnJgU7to03rqN"
  "gvc2ieKyxAnZIMe9zQ0UByfyInAPIdvkSVBhRyewVsWxVq4PXBETEB7tn6Zl+jluCL8U/F"
  "bSAXStqCBhoUlUbEGqSCi7rNQRus0VUd4CyyG6N8ScyJiNG7TMY0qsCpuhws+v4uXouBlb"
  "OKnqwAQFzCaHxYv4x30ezvfE6RmHrC+08NenbylqZh41uQ1/4x8S32T1THxFs+MbROJ7Jm"
  "z3ULx14NbCKEhrrD5kayM8pi10MyuEPx1foW98k1SAz5+ffTP5Gdk0dFnnmYjcfG1xAlJU"
  "WupAUEqVuxk+leNQHbxUffyqxHDg2fx8Ul/FvvXVjZKR0fUV5jz1zeTDCyMjj6am1A2HUG"
  "KoL05Maomi0cvPrhu8/uXk5GRARpm3voqe1JdPf6iZsz+U9l0dGxs7viD40YVPCyxNTH/g"
  "rEnR5Dm9/Cx79cC10dFRrsY53R+Mvv3hmf7m8e1vw70fd7tbr7wZ92DywYriarQLpr9RuQ"
  "6Gr86o7bl2q7mNjuxc/c2nPxtIFHz6szrD+mv/aCdVWVnk+vmX+6zc6pn+DLcwfFW6+bOJ"
  "OyqDjUf6c73XeJ/+/PT5QnnPl+iS5quL4u65++5/e/vxzQm3vf76wsivXon+8PUwxVtC3E"
  "AIP9X48LfHkdkapM1858vs89E8cz6qUg3jr8qH0rRN8vSBhSEM05e/OUW/mmrUOtv8Uykc"
  "rPOdjzPnu9/s8z0ly9wpzumU5HZK8ohIiVQIYuPZoWRRnINsJ1OEuh4qBD7/+T79Cdv0fG"
  "Jg5hP+rPnEQ4Ko0HPo+YRHzyfYhmEGKvR8Yv1/56vZ82E+PR+WzT8f6uj5MO8fzYdzz7fJ"
  "s+fbpJeab//D+fwvjB9Spw==";

