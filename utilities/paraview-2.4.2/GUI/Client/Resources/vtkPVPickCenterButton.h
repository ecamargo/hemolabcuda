/* 
 * Resource generated for file:
 *    PVPickCenterButton.png (zlib, base64)
 */
static const unsigned int  image_PVPickCenterButton_width          = 32;
static const unsigned int  image_PVPickCenterButton_height         = 32;
static const unsigned int  image_PVPickCenterButton_pixel_size     = 3;
static const unsigned long image_PVPickCenterButton_length         = 1232;
static const unsigned long image_PVPickCenterButton_decoded_length = 3072;

static const unsigned char image_PVPickCenterButton[] = 
  "eNq1ltlPE1EUxseIT/hso0QMBWSTJZTSFjRI2MoyLF2gLZUWJEYk0MpSSqkPbGETkCWUpa"
  "WRBBFMNJJIwgMPJJpgogkPPPHX+MUTbybThQHD5GRymd77O+eee+75ODs9ObuMbW/5YWeX"
  "XCXdXH1OmUx2fS4qykt3dzu02txrcoHgz8/HYNfkgvH/3wVSTdmmBzS8BwdrGf/KLrAEeX"
  "a7eZiQFtYuddyMjEPEWkSr1SoEW1Ds7XUwMsb4olSmYJsS4dgvIwNoMhV4PLzfb9vYaMHb"
  "Yilk+cEAZKezXKdTReJ/2/8kgrOo9HrVykozmKur1kDADtvctDM+Juh0+bOzjTARf3TY21"
  "BXk56eGhMTgzc7SganqIBdWjLDxdAQD6zZXEgpwgS8MYHgTmdFeVmJMP/gc3+f27Gxhwdf"
  "6OOAy0bwvLyUyUk9Fs7NNRmNaqCwvL/XQQT8Ce9EhmGMX4UJWVmeexB/H2Tw599OsCv5r9"
  "gUHk/V9LQBUYWtCnxkYYsmHB3ul5UWx927u/hu+uvnHWtzk+jKILHd3WWDg1UOR7koKhFf"
  "FPbpr+99r7sQ88sXbb9/HodeIjoyrB0fb+B5RSR42Oa2FVxLlCcUaFTCUgm978h8ZqZ8as"
  "oQqdI6B4Y6R2ce80YGPz464GsqZbI7LM9R+NhCQUGqQvEw7JxX3hHuqY2r7OJK2vN1NvqI"
  "bNttzSc/jqT0K7pNkYLncio4vueWeeRGvYtTG6JnT8QhPu6sXB4XqY1wj4q5audN4xuO7+"
  "WUtVJCDeULP4os+GGbU+u5kudcoSm4uxspeKoTEV9YP9Ez2eH2Bt8HLtQFSpGQzzqDxDYY"
  "PfjGRlVubrLwp+TkeHKNtnw1saAIYTk5SRaLRnSDNJpUdIar6ZFw+7ibY2P17e3FIgLaF8"
  "/nWq1PSDswHx3pQi9MiQiOnKO9uFzatlZd6Myiogy9Xkntl8rp446HKlnkiImyUInQkWZm"
  "jG53Jdpv2MCQMfD7+7UZGQksV3QiJOvsEYoyhU1CEwy2YhClQuDCYFA6HCXYJqEQmFBtmZ"
  "Eok/L6fNb5+aZQFYvkIhi0e73VU1N6CC40Kzs7SRbyQM48nhqXq2phwQR4T48WjnCIEost"
  "KysR8jExoRsergsEbCTBEF8YxrD19Za1tWd+f8viohkyLdJHKS6wRKNJMxhUPl/z8rIFXs"
  "joHwk4QvykyBLDDusFa+GIpUWtTqMBKbJE8h/562Z9";

