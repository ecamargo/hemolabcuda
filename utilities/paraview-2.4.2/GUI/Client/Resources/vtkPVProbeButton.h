/* 
 * Resource generated for file:
 *    PVProbeButton.png (zlib, base64)
 */
static const unsigned int  image_PVProbeButton_width          = 32;
static const unsigned int  image_PVProbeButton_height         = 32;
static const unsigned int  image_PVProbeButton_pixel_size     = 3;
static const unsigned long image_PVProbeButton_length         = 432;
static const unsigned long image_PVProbeButton_decoded_length = 3072;

static const unsigned char image_PVProbeButton[] = 
  "eNq7cuHEFWyorKQAiK7gkKUcjZo/mM1fumgO7cwHGu7m6kwj86liOC7zqWU4VvOpaDim+d"
  "Q1HM186hq+YNlSs8RSHl07Hk2L7LaJFBoOdKS4uDjctQuWLmJwy2BI6GfImMOQPoshvJnN"
  "yJNyN5cW5wMR0CIePXuG+D6GotWMlVsZyzYyZC9kCChfsHwZtQKHwTaGIW0WY8UWro7jLA"
  "37GApXMkS1ZTV2Ust808RShpRpDKUbWBv2MlZvZ8hbymMV6O7mAgw9yssEIJq/arWgmgFD"
  "5lyGguUMuYsZEiYw2EQBTQaGHjCWkWOKPAR0an5dM4NnHkNoA0NQlWl6w/zFC7HGFBmeAm"
  "qBqxeUksevmFRPIRsOREBdZCQ/XJ5CM5xU8/F7CquN5JmP6SlcspSbjx+Nmj9q/mA2H0/W"
  "IAkBAIjN7IA=";

