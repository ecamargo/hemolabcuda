/* 
 * Resource generated for file:
 *    PVPullDownArrow.png (zlib, base64)
 */
static const unsigned int  image_PVPullDownArrow_width          = 9;
static const unsigned int  image_PVPullDownArrow_height         = 6;
static const unsigned int  image_PVPullDownArrow_pixel_size     = 3;
static const unsigned long image_PVPullDownArrow_length         = 32;
static const unsigned long image_PVPullDownArrow_decoded_length = 162;

static const unsigned char image_PVPullDownArrow[] = 
  "eNo7cAAfYMAGcMni0ovL5APEAQDaylWB";

