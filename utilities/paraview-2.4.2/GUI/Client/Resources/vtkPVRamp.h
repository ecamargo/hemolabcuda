/* 
 * Resource generated for file:
 *    PVRamp.png (zlib, base64)
 */
static const unsigned int  image_PVRamp_width          = 32;
static const unsigned int  image_PVRamp_height         = 32;
static const unsigned int  image_PVRamp_pixel_size     = 4;
static const unsigned long image_PVRamp_length         = 608;
static const unsigned long image_PVRamp_decoded_length = 4096;

static const unsigned char image_PVRamp[] = 
  "eNq91ztLglEcx3Ehsot2mVoquuE7aOwl2BQIQUFDkhFhSiFhGF3ULibUELS1RGGZXTSlsB"
  "aXCKRahIZegZWlhFlP9jvwCIeDtp1/8Flavs8vD09HjUb+jz/20AJeSEEa4tAHWoK2HrZA"
  "gSLniT0DQb8fckK7JE7Qt1doM2nZ/bXQrWUjel+pn5LZHjY5Gsat/mtvIFGuzc6DV2LbAJ"
  "dQnLBvFvEMCncGc+qZbJHU1kMMvlkffq3OHRc+hxH1PLAzqZfU7oGo2mVewErwmmFtHVxw"
  "uxkb+z1BuxsiXPcV7ES76yEMBa4/zc4BQbsLzrnuG8wQ7a6DM2G3g2h3J5xy3QzMEu2uhR"
  "Nht5O97wjaHRASds8R7a6BY/ji+i5oJGi3Q5DrvsM84e4jYfcC0e42OOS6H7BEtFsLAWG3"
  "G5oI2q1wIOz2EO2uhn3Ic/0VaJZ0R6yCXjB6AgnjqNnNf95ZWJV4P9XBFDzDpy+SzC7uXv"
  "2Yxzyl/rqs3WrfBnnxfri8d6NYJn3baMu+n9+Vuxvjrqb4wslBgu8HmX++H0j/P46NjxXa"
  "BTDJ7uPvPKB+JxX7QVn3c/H84xmGIM3tjoCB4l3zB2n1JwE=";

