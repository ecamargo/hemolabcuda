/* 
 * Resource generated for file:
 *    PVRecord.png (zlib, base64)
 */
static const unsigned int  image_PVRecord_width          = 16;
static const unsigned int  image_PVRecord_height         = 16;
static const unsigned int  image_PVRecord_pixel_size     = 4;
static const unsigned long image_PVRecord_length         = 204;
static const unsigned long image_PVRecord_decoded_length = 1024;

static const unsigned char image_PVRecord[] = 
  "eNr7//8/w//BhhkYQoB4AxA/BuL3QLwXiMuAmJWAPk4gXg7E/6H4HxIbhM8CsTIe/ZPR1G"
  "PDZ7G6g4HBAmrfPyLMKMOiv5YIfTC8F4v+9STof49F/yIi9f4Dxwum/gIS7N+ARb8CEH8l"
  "MvxCcMRfOo54RxZbTiANpUPdgU3/ZHAaI5x+FaDhsQgaL7Xg9DEI8xoA5Pht/g==";

