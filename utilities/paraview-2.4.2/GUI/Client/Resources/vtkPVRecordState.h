/* 
 * Resource generated for file:
 *    PVRecordState.png (zlib, base64)
 */
static const unsigned int  image_PVRecordState_width          = 16;
static const unsigned int  image_PVRecordState_height         = 16;
static const unsigned int  image_PVRecordState_pixel_size     = 4;
static const unsigned long image_PVRecordState_length         = 312;
static const unsigned long image_PVRecordState_decoded_length = 1024;

static const unsigned char image_PVRecordState[] = 
  "eNr7//8/w/9BgJ0DTwrjVcPAMAGID8AxVBwIRK2dlyW5+x354+KzezmQLwzEQkAsCMQCQM"
  "wPxHy/GBgOA1X/h2GgGCcQcwCxiq3L8kke/kf+u/rsuQrk6wCxLhDrAbE+EBsAseF7BoYz"
  "aPrNgNgc5AZPzxN8ngFHCrwDj2vicf8BZP3/B0m4Uwu7+x4ycnDYz0Gq/0HxZ++2pgIU/m"
  "4+ew8C+VJQLA3FMiD8lYHhOFr4y0Oxio3TknaQfhfvXaeAfFUoVoFiZRD+wsBwAk2/AgiD"
  "3BAauorZ3e+wh3voMSFS099QxwClHlkF";

