/* 
 * Resource generated for file:
 *    PVResetCenterButton.png (zlib, base64)
 */
static const unsigned int  image_PVResetCenterButton_width          = 32;
static const unsigned int  image_PVResetCenterButton_height         = 32;
static const unsigned int  image_PVResetCenterButton_pixel_size     = 3;
static const unsigned long image_PVResetCenterButton_length         = 1216;
static const unsigned long image_PVResetCenterButton_decoded_length = 3072;

static const unsigned char image_PVResetCenterButton[] = 
  "eNq1lslPU1EUxq8xrjDBlV0YMSLIPKSldECDpAwFfIgdoC2VFmVTCbQylLbUGKZAQSAMoQ"
  "ytVRLQ4o7EBQsXJCw00YQFK/4av+SEm5fXgYLh5eTl8nrv75x77rnn4/Tk1+llbHcnAju9"
  "5KrMzTvskclk1+eiqbE+Hnfp9fJrcoHgz86mYNfkgvP/3wVSTdmmBzS8/f42zr+yCyxBnn"
  "0+ASamJbVLHTcn4xCxFtHq9QrRFhT7+y5OxhhflMoCbDNDOPbLyQBaLNpAQIhEHNvb3Xjb"
  "bDU8PxiA7PE0GgyqTPgE51EZjar19S4wNzbs0agT9umTk/MxwWCoXljogKXib4WXG+rr+F"
  "FyOEUF7OqqFS7GxgRgrdYaShEm4I0JBPd4mhobdJL8/zj4rtWoHuTcX/o4Q19GvQ6CV1UV"
  "zM4asXBxsdNsVgOF5SNDbiLgT3gnMgxj/ComHx8dWjtNd7Kzh9/1n/w55lfyvNgUgUDL3J"
  "wJUSWtCnzkYUsm/P195O53gQw+vCReGSR2YKDB729xuxslUUn4PGxEiAz/PDyYHA/KZHcx"
  "RmYSLxEdGdZOT78UBEUqeGJzczq6GGO3s7IqyktTXQEKHpkvK8sNhUypKq1vdKxvcv6JYO"
  "acmakP7PzZ3/ucvp9gC1ptoULxOOmct8EJ9szBmvuZrrfa4KBiRqqLiwuRE7OxfSe2eSEf"
  "g1TBs8omJgzesk7caPcytSl99iQc4uPO5ubeS5VDVlrHWj03ze+ZMMSUbZmEmsgXf5RYbG"
  "+XqY1M94bVWGLxeKrgqU4kfHH9pG8gLl8w9iV6oS5QisR83hkybIPpg+/oUMnl+eKf8vNz"
  "yDXa8tXEgiKEVVbm2WwayQ3SaArRGa6mR+Lt425OTbX39tZJCGhfgiC325+SdmA+OtKFXr"
  "gSERw5R3vxevWvewyJM2trS4xGJbVfKqdvXwNUyRJHXJTFSoSOND9v9vma0X6TBoaMgT8y"
  "oi8pechzRSdCss4fsShT2CQ0sVgPBmkqBC5MJqXbrcM2CYXAxGrLjUSZlDccti8tdaZRMY"
  "mLWMwZDLaGQkYILjSroiJPlvBAzgKB515vy/KyBfDBQT0c4RAzLLby8keQj5kZw/j4i2jU"
  "QRIM8YVhDNva6t7cfBWJdK+sWCHTifp4oQss0WiKTCZVONy1tmaDFzL6RwKOED8pcoZhJ/"
  "WCtXDE06JWF9GAFDlD8j/BZmYn";

