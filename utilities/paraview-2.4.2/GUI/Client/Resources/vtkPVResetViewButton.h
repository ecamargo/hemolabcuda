/* 
 * Resource generated for file:
 *    PVResetViewButton.png (zlib, base64)
 */
static const unsigned int  image_PVResetViewButton_width          = 32;
static const unsigned int  image_PVResetViewButton_height         = 32;
static const unsigned int  image_PVResetViewButton_pixel_size     = 3;
static const unsigned long image_PVResetViewButton_length         = 1028;
static const unsigned long image_PVResetViewButton_decoded_length = 3072;

static const unsigned char image_PVResetViewButton[] = 
  "eNq7cuHEFUKIgYHB1MRo/pxpV4hQTCrKzkwFmu/sLKejLUd1WxbMnW5ibAhkSEgwrF9b0d"
  "wUBbQFaCO1zDczNZ43eyrcfAgKD7MB+ohyW4AmZKYnw9n5eTZwKyC2UBJc8JCBIKA5Ls5y"
  "yObDg4s8W+AhA0fIQYRpC0nBhRwycOToYNTfF4XVCnikEGk+VpWYUQBHLU1RQC1AX2C6ip"
  "iQwR9EOjpy2tpykIACxgVJ0YrffKCzgYY3NyICjWAQ4VeAHEQR4TZAZyMbDvKLthyetIQn"
  "ZNpb51hYFNrYVNjYlCkoOAKdDYxQzLACWoorCnCFzLLFS9JSJ/n4zIyI2BgXt0dXN0pERN"
  "PQMMndLRhrXOMKAVziKckTAgMXJSYezs6+Ji1tamaWnZx8NCJig6trR39vIZHmY03wQFRb"
  "3eXlNTk+fn9Q0BKgxsjIDaWlLwoLH6SlnQGKBARkEBNEeNJMR9u84OBlkpJGsrKWtbUf6u"
  "u/1dX9LC19mZl5MSxsrZVVKTHm44nWqopmoLP9/ecUFz9tbPzW3Py7puZrScnzzMxLkZGb"
  "rK1LCUYBrpCB2MvDIwEMh9TUk0VFj6ur39TWfiwvf11U9Cgj43x4+Fo9PezFBXIqxRoywB"
  "CDeGrF0uUuLp0xMTsyMi4UFt4tKXkCNDw393ps7C5X166c7FSs5iMHEaRuRYtroKXwEAsL"
  "LfHxmRYdvTUl5TjQI8DEExW1GRjpmppBmCU2vESFmwnM8s3g0gloLMTZmLEvJqZjb1/n5z"
  "c7IGC+n98cYMpsa5kHqRSA2gvybfBEAbzUhRSwyBENtBFoHdwvNVUdDXUTqyvb0QIT6B6g"
  "LWhFNzwKkIsUuKWQNgNQI650hYmA7gQGF9wW5CiAl4pAQQYwIKYYx0RABwNtgQcX3LWQMA"
  "wIkAM2cggW4AQRPLiQQ0NFhQESSsTXcfiLcR4eBiCChwPca/gLcKBGNARMAAwYAGgsBKFF"
  "H1AEaCkwiAhqxGUCkQFInkZIbkUGtGgP09RwzKoKAOPrhh4=";

