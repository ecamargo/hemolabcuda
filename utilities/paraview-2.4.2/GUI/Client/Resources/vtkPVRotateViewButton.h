/* 
 * Resource generated for file:
 *    PVRotateViewButton.png (zlib, base64)
 */
static const unsigned int  image_PVRotateViewButton_width          = 32;
static const unsigned int  image_PVRotateViewButton_height         = 32;
static const unsigned int  image_PVRotateViewButton_pixel_size     = 3;
static const unsigned long image_PVRotateViewButton_length         = 1004;
static const unsigned long image_PVRotateViewButton_decoded_length = 3072;

static const unsigned char image_PVRotateViewButton[] = 
  "eNq1lm1v0lAUxz8G75YOki0bK0gpW2c26C4PpTy08GKGCWoiyaJLFnXDDZyowS1hSuZzfC"
  "JuM1mi38DEL+ALX/iCN34b/+uVm1oQKZ3NCYHbw++c+++557Tz41tnCNuq3HS5XEoi1hnO"
  "366BT0i1UPiEKPj+n/jr6z9hhGxjIycf22fIB43nk5QPw0Z4XnG+EWCp8rhY/sychAAZTB"
  "CghgXb3cJnt1uAzwjk1qMjkLtq16CMEStpgRPy0C7f0FlhEPxdFIuE3NP1Q3xn6zMzWcBh"
  "iDL8g4ZnqVhh5MXFSir1Mpc71PUPudyRKJaMEv2d+QhwlrnbHcxk3qjqc017p2nvZfmOIK"
  "xMTJyHRCPDN26dMGEV5SAWa6rq00ikho0k4tHbmzfwyeCSdA0rto4PShp8ow5PCcnkgSSV"
  "wTS70buAW9aHVAbyer0pg7C223jR6wm+XTgMh50lL8sP5uZW/0bA+uP9439KATM/Glp4UJ"
  "7jfLOzq3bT6xFwG2qYzwXlG+KoF1e2nDQusxR9xXeSvEUK1ppY/s57O0NNTkYZqlTcZHwn"
  "4rAZgWM4NZWw8GlLd5L81y/fqTjj4+emp1WWKouLRz9y/ubkCbmbTj/p5WMLo01wc+9aWm"
  "qo6rP79aalrmh7pyFs7QL+3fROexf4oVDZUiflq82xMS9Gia3ZCh9W8IDzvGb0rkZvkcPT"
  "51tGCDak6EsCPex9OwA9qhSOxDwekZDdAX27vrMnCCXUlfGAaiwKzPXnRRfN05Pn9Wh0D8"
  "NocPfjOH5+fkMQLiOKwan1nexUDUoOBC7Ick3T2un0K0m6PngoYF+6VhSEKwsLVUVp+Xx5"
  "ljPdFPvp8YQwlykZYy6RaPn9+SF7C9w4zh8O18PhHVQaBiUsEqkGg0VMYVg+fwzDoq63M5"
  "nXglDA6LQ1zujLDwY62nU8vk9DZLNvKR+GtDGURfES3EAe7VTSmQsCSgJJIlwgsIxPKhFu"
  "ndUrKDiIxczWf38BWc1rdw==";

/* 
 * Resource generated for file:
 *    PVRotateViewButtonActive.png (zlib, base64)
 */
static const unsigned int  image_PVRotateViewButtonActive_width          = 32;
static const unsigned int  image_PVRotateViewButtonActive_height         = 32;
static const unsigned int  image_PVRotateViewButtonActive_pixel_size     = 3;
static const unsigned long image_PVRotateViewButtonActive_length         = 976;
static const unsigned long image_PVRotateViewButtonActive_decoded_length = 3072;

static const unsigned char image_PVRotateViewButtonActive[] = 
  "eNq1ls9L40AUx99pLplLeul/UdSgFXvcbmoFFT2pqBEF0Uq7wrYNWlBQVPBU8SdaUeu2B/"
  "+SPe6h/X/2O306G7LdmqTd8CghnXzem29m3ndav362AkS5uBuPx/HbCjY+bIBcLstSSf6n"
  "LMxvtwmBLBk73ajXBsgHLZMxmY9oNIRtx5ClfywqB0fJ8lG/DjyJnIKxKBJq+LAczaawrB"
  "jyRpHCTgMLEcBxXQPKxDuXD352RmH5gKNmhoCM15eW5NGRaDYplYoBy/DZWQk4AjfBl5OG"
  "g4CaV1dlrUavr/T0hCfkOBKC68oROzvC/volOJylZoLripsbAv/5mV5e6OBApFImkkaG68"
  "o3NuTFBV1e0v097e0Jx1Eqlb5/Aw03Gh5ql6n90vmaTKhWFX9uToLp3Uf8b6jKvcVD3s1N"
  "wRV2XRh4GBbOb7Eyw8Ox01Pa2vonASM/hatOUtz1zZqLn5yU+KaWZfaYae+2AzJvST193b"
  "iwNtbWxMiI2U/j0jTdZr1P3t4orLY91omPj3lVq6LPrqt7CKRgvpK0kxFZIjSrrusQUjvO"
  "n0XCD7mlD6R46Oy6pDegV6LI+mjlUfzEhMxmpZfP3oQBkf1C966ZGXl4SENDpndRJZMycg"
  "q83m5ZujFih25vi+aPnHdMOi0Tiff2jhQBDwl8aNGVsymAjx3q0xnp8nlCCm3ivAf5nOAd"
  "zNufHVlbJ/ftqyvVuHK57q21kB8vFgnfBYPhXN5zAiL+cYGprVN7XKUibm/p/LxXa0WRbj"
  "m5v0/r62Jq6j0LK/a3s2tTXlyU19fKhh4eqFD4pLXCRBIJs1KBW1E+L+CPfELwXTBi/MWm"
  "zGSYxeioGaS3YBYgZLMGJotlBklRXqOhLPLuTrmwjsdHFfW68rixsXBdERNBlulp4+RElQ"
  "cOgOzyHMAeH4uVFYMdIdqu5CwdQcz5eWN52VhYMNiOGYsBgzp8AqUj1Lu/AYrIVkY=";

