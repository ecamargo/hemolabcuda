/* 
 * Resource generated for file:
 *    PVRulerButton.png (zlib, base64)
 */
static const unsigned int  image_PVRulerButton_width          = 32;
static const unsigned int  image_PVRulerButton_height         = 32;
static const unsigned int  image_PVRulerButton_pixel_size     = 3;
static const unsigned long image_PVRulerButton_length         = 936;
static const unsigned long image_PVRulerButton_decoded_length = 3072;

static const unsigned char image_PVRulerButton[] = 
  "eNq9lkFrE0EUx3vpwRDEaLVGKZRCwVNF4kEv3kIoNSlFUg0SQVKNoB6igSq1lK0itKWWgi"
  "i69aBBhEJPZa3mkpP2GIoH/Qr5Fv7wD8O4m03W1vbxCLOb3d+8939vZrbR6G4bG5sLi+/w"
  "K/kZXGNuNvZmEG7dfn723NTAYKGt8xcP7GIikQXJ5GYrTz4sv62vrW8b55Kb/KVn/mkWcl"
  "fMpXuvfNig80C+uKBceDEKnIcvXqp0Jftm4RVe7DyF4GQdnWy75AqbYo9w/E5l7VAsGz88"
  "HpyC6iAgOe4Oe/5Cpacnk83OZrP3+QXlK7e6Jbrmi6+/TN54MTRcEtZ1N1t/zHFWU6mSOs"
  "oOXt0SHZtMFsrlFc/71vrbPO8rfHWUSaFr8EGs637CWwFz3RrpgLJTQK62Ze0QbbP5oy0f"
  "6++fUC+BNeKwGA328bP19Nhc4mg+TAQTahifwABKIrWlxCFasKnUXcd532z+bIUbwZfLM7"
  "6b9fr20tJH+HSUJAI+/WiVgdoMsrBhuVv6NHd2fgHUZa32GR8ZuYkPDl1HAYDis9+KT1II"
  "oh4LxhY0gMXiU81CzNXqy76+8dMDBeKUziqx4eM0s0m/FcHAMks6/YCw4/HLmZxDkKaO4h"
  "t9cMSXPqTQmUzk4jNIJHLDZ6akie3Sx64vy1wr0XGWPc/rTKaUCJI8dY2uaLujim/3p+G7"
  "ruvDmmpCluAIQhvbgthu+tNeX5SG1WfHGawm5DBBfHu11pe9P9h8OBqgg4kcQU6cvNpWkK"
  "A4Zn8w+5tpUcVPzBrgdEgsNoaAYYLY7tvf7BRMi4pP5Oq9Y8cnOwsSFrzvfDEtiiw0NlL3"
  "9o52FcR2IMHzxZyPHHBqoa2t79EFiX4Ec3qyubFtkkhEQSLCZfPzbyjBkcTEfnyfyB5WV/"
  "bv++oAvg8P4Pv2P36f/wb6l3F0";

