/* 
 * Resource generated for file:
 *    PVSelectionWindowButton.png (zlib, base64)
 */
static const unsigned int  image_PVSelectionWindowButton_width          = 13;
static const unsigned int  image_PVSelectionWindowButton_height         = 13;
static const unsigned int  image_PVSelectionWindowButton_pixel_size     = 4;
static const unsigned long image_PVSelectionWindowButton_length         = 44;
static const unsigned long image_PVSelectionWindowButton_decoded_length = 676;

static const unsigned char image_PVSelectionWindowButton[] = 
  "eNr7//8/w38yMRD8J1U9DKPz0TEl9oz6Z3j5BwAWBrFd";

/* 
 * Resource generated for file:
 *    PVNavigationWindowButton.png (zlib, base64)
 */
static const unsigned int  image_PVNavigationWindowButton_width          = 13;
static const unsigned int  image_PVNavigationWindowButton_height         = 13;
static const unsigned int  image_PVNavigationWindowButton_pixel_size     = 4;
static const unsigned long image_PVNavigationWindowButton_length         = 68;
static const unsigned long image_PVNavigationWindowButton_decoded_length = 676;

static const unsigned char image_PVNavigationWindowButton[] = 
  "eNr7//8/w38aYCD4T44edH0wMWIwse7Apx6fHlr4lxr2EOMfUsON2PihRTpAxwBPsLFd";

