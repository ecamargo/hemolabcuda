/* 
 * Resource generated for file:
 *    PVShowCenterButton.png (zlib, base64)
 */
static const unsigned int  image_PVShowCenterButton_width          = 32;
static const unsigned int  image_PVShowCenterButton_height         = 32;
static const unsigned int  image_PVShowCenterButton_pixel_size     = 3;
static const unsigned long image_PVShowCenterButton_length         = 1060;
static const unsigned long image_PVShowCenterButton_decoded_length = 3072;

static const unsigned char image_PVShowCenterButton[] = 
  "eNq1Vl1PE0EUHWP8DX0wMRFBIMhHWkpb0CCplAVcxN0ttKVSUF6QSBuQpV3qAwiBQoDwEc"
  "pHy0YT1PJLfPSBJ36NJ7lxslnaMq1hc7MZtjPnnLlz5x6u/vy+qiQuvucQVxWuEg/9c8Lh"
  "cNwdRV/gVaEwLUnOO6KA+OvrVcQdUXD8/6dAqinb9AAN71RqiONXTYElyHMyKSOsaEWjou"
  "PmyDhErIVaSXJZtuC6vJzmyBjji9tdj20KgmO/HBmAoVCnYci5XOzsbBzvSKSL5wcDICcS"
  "AUXxiOATOFelqp6jozFgHh9H8/kJxPn5BMfHBEXp2N4eQYjgYwIHJ1WAPTgIg2JpSQZsON"
  "xFKcIEvDGBwBOJvkCv/9b8L+oxAm9vr9/YULFwZ2c0GPQCCssX5uOEgD/BTsgIjPGryJX8"
  "V2wuwxjY3NSgqmhV4COXLV42dGWQ2NnZ3lRqIB4PlFJF+IKyeebpyLB2be2tLLvKrK2iuZ"
  "F4ZL65uSaT0UpVwszi0szXredysNLbypPT2dngcj0tOudjeoW9jLH+T8w/1aHEqsPHoJR4"
  "1tbH5LkH4ZV7wzrzauWzZ8MhfNzZmpqHpfbOnvWwwcT94BcmzzP3kIjUm/jWj7Ywf1wwr8"
  "r8H1hXyCwUyteJDd9aP+UzOZ1Mm9/yt/oCpahoZxBsg+XFj4x4nM466091dY+IGm25OrMg"
  "hYi2ttpIxGe7QT5fAzpDdX5k3T7u5urq8NRUjw0B7UuWndHoC/IOzEdHupWFOxGBI+doL7"
  "ouvZ9Ubs7s7m5SVTe1XyqnXz8NqmQbETdlqxOhI21tBZPJfrTfosKQMeAvLEhNTY95ruhE"
  "yNb5YzVlkk1GY5qTGJSpEFBomjse92ObBAVhVrflQaZMzpvNRnd3RwVdDBSmOZFOD2YyKg"
  "wXntXaWuu48cDODOO1rg/s7YUAPjcngQiHKFhsLS1PYB/r68ry8pt8PkYWDPNFYIw4PR0/"
  "OXmXy43v74dh0yL+aKPAEp+vUdM82ezY4WEELBT0jwSIoJ8cWVB2URasBRFPi9fbSANyZE"
  "Hkv/AZl+w=";

