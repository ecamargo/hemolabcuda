/* 
 * Resource generated for file:
 *    PVSinusoid.png (zlib, base64)
 */
static const unsigned int  image_PVSinusoid_width          = 32;
static const unsigned int  image_PVSinusoid_height         = 32;
static const unsigned int  image_PVSinusoid_pixel_size     = 4;
static const unsigned long image_PVSinusoid_length         = 992;
static const unsigned long image_PVSinusoid_decoded_length = 4096;

static const unsigned char image_PVSinusoid[] = 
  "eNr7//8/w/9BjHM9c9mjQyu1GBgYGOlpb2x4lU5sWMXE2LDyt0D8H4ifxYWVzY6NKDOhtd"
  "3x4RVuQPv+QO1Fxx/jIkv0aGV3XHilGtCO92h2/kTmx4WVP02MqJaltt2hoQ08QPNvItn1"
  "LCa0zNXBoYElLqzCBWjvByS5U1T3e1hFEpL53+NCy8xQ0kRohRNQ/BdMTUx4uT5V01xYxQ"
  "F4GIeWV2BXU74GYX9ZF7XsToioUACa+Q9q9p+YoCpJ7Omj1AcpjB5TK18C81Ytkrk7cKkD"
  "pQWg/AuY2viwSmtq2B8TVr4abn9oRQL+sqFiESIvVCRRanffjosSNVPWP29etPd//axt/+"
  "vnbGsgEFYdCLeWVRNpRzQQbwbiE0A8GWQnVFwBiN8A8X803Ivb/2UFSHE1hQi7e7GY/wZq"
  "9zosciD8D4gNsfo/tCIc4f/ytQTsNoSahc0OkN2vcciBcCFW/4eW2iP8X3aMgP2FeMwH2f"
  "0Ij3w61rQaWqaOFP73Cdifjsd8kN0zcch9B2JFbGZGRzfwIZeTBOxXhJqFzQ6Q3fxAfB5N"
  "/DcQp+AvK8u/wtwQGlrOT8ANKb3bL6CnAZCd/FB5ViBOBuIZQNwMxAaEy+ryO/ByGBgfhN"
  "Rn5PQcK+tY+r9qwpr/dTO2TATZSVldUX4YkQdK7QmrL7sMU58QWmpAeV1VvgqpHkrEW7ck"
  "NHAA1X2BqY+MrBKnQnldgpQGd+EtL1HLiytUqS9DSyWQ2mh/o4KK5fGE/VZ4WIVVlFOtvR"
  "BasRmpHsJaZ0QHlckA5X/D3AniU6+NXBaEFAffYiMqzZHl03wbuIDhfRqpTbOHyu1FNuR8"
  "CMSv4sPK7UBtkqjgCiVge3ELktwfUBuS2m3GqOBSVaDZr9Hay++wtNuzadZmD620BIc/9v"
  "7C/7jw8gk07y8B+0VAe6aB2uxQe3/HhJZvA5YN0Q0NDUz06reB4j46vMI0LrBS+P8g79sO"
  "BgwA1aLw+Q==";

