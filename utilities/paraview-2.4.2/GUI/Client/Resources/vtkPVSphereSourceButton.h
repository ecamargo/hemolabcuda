/* 
 * Resource generated for file:
 *    PVSphereSourceButton.png (zlib, base64)
 */
static const unsigned int  image_PVSphereSourceButton_width          = 32;
static const unsigned int  image_PVSphereSourceButton_height         = 32;
static const unsigned int  image_PVSphereSourceButton_pixel_size     = 3;
static const unsigned long image_PVSphereSourceButton_length         = 928;
static const unsigned long image_PVSphereSourceButton_decoded_length = 3072;

static const unsigned char image_PVSphereSourceButton[] = 
  "eNrl1mlL1VEQBvBvYCUm7rtFiFqoReQtkqIkXIhybfGWlWlpipqYpWViGSolZaAYtIAoWU"
  "EEQSB9s36CUZZeb7kQdJkXM+c8z3D+c56Zc+fn/6PfzNs5thGZn01OHau5uGX3YcYRrlfm"
  "5u7egpKqbXuPbz9SE1PWEFPawBFatPX3B56Yqm1qi8g5GBkojy4Oxp68Fl/RllDdyTjC6O"
  "K6yMLyiJwAGHD4mWfn3uWfqNiafzSqqCqm5HLcqZb4yvaE2q7Ecz1JwbuMI7RoCyCqqBIY"
  "BTGc/JAqEFveGHe6NaG6I/FMd9L5O8kX+lMuDaZefcQ4Qou2AMCAURDDKYvDwCfUdCae7U"
  "mq60uuv59y5WFq43Da9cfpN54yjtCiLQAwYBTEVQulmL7XkbCS6wdSG4bSmkbTm8cyWscz"
  "2yd23JxiHKHFtKYRADBgFPVED53ffSmpr3Yw3PSWsYy255kdkzu7XuzqfpnV85pxhBZtAY"
  "ABoyDSQ2gpkoRbU1jf7ngyLGS+9Srr9pvs3uncezOMI7RoC8AHAqMgElsI0ZI01RGGu1Ne"
  "FXBIebL7pnP7Z/cMzOUNvmccoUVbAGDAKIiULMlKHaprCJv2yMMNKrITOqpseQ8+FAx93D"
  "f8iXGEFm0tfELrODAKIroky3a3rqcxvUPeFEgk7lGpVcOB5dw/+vnAky+MI7RoCwAMGAUR"
  "XRKpfh9cBove1546iMjpkFTcpoKriWPLfGj8K+MILdoCAANGQUQ3Q6T6ZQwu5i9bj/yly+"
  "Tf6Ppswv1+12dwbfqsW0mfi/0V+Lm/RsPrr5Ef/VVYHvpR0OBrmw+BMOZb1dL5NrLyfBtd"
  "Ot8qV51vGz2fN/p9Wfo+Vob3Plb90fu49H0PkATVLfe+B4mNHv70fQ/1/6S0wQxZl/8nm/"
  "b/anP+H/6zv2/dKI6v";

