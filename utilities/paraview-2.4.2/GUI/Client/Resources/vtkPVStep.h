/* 
 * Resource generated for file:
 *    PVStep.png (zlib, base64)
 */
static const unsigned int  image_PVStep_width          = 32;
static const unsigned int  image_PVStep_height         = 32;
static const unsigned int  image_PVStep_pixel_size     = 4;
static const unsigned long image_PVStep_length         = 576;
static const unsigned long image_PVStep_decoded_length = 4096;

static const unsigned char image_PVStep[] = 
  "eNr7//8/w38a474dF9mAuBmIbwPxByDeDcRG/+lgN9T+NUD8Hw3/oocbgHbYYLEbhnfTwf"
  "4yPPZ/oIP98Xjsv00H+8WA+C0O+5vplP7c0d3Qs+XcRlC+oGMeECuomfm1uHnB/4zcnv8J"
  "AQUC9LIbhmPDyt8B8X8QjoqqEBy1n+72v4XZnxxaKDRqP93tfwOzPy6wUnjUfrrb/xpmf2"
  "Rkscio/XS3/xXM/sTQStFR++lu/0uY/bGBJWJo7QMhIHYDYn0gZqan/UD7aoD4J1L76AIQ"
  "a9LDfqA9qbjapkDMRchMIGB0cGhgyfXMZU/zbeBK8ivjBdXtoPwVGVklHhFRJpUYUS2bEF"
  "GhgFz+Idl/CVf7OK9iGqi+eg7Nt6C2w0cg/gLE34H4FxD/g5tHDA4t+4/I/1XiUPs/47K/"
  "pGXRf5LMJ8/+07jszy6eRIr5v4H4BzR8PkLrelB+ewbEj2PDKx7EhILlv4LYsPIPaE8ANr"
  "t7tpy9HB/bqBoXWi4XF1otDXIvSA+o3QyK49DQQs7Q0Aa2hoYGJiq0zWOA+BXU7n9AvBmI"
  "JehRJgAAov0wbg==";

