/* 
 * Resource generated for file:
 *    PVStreamTracerButton.png (zlib, base64)
 */
static const unsigned int  image_PVStreamTracerButton_width          = 32;
static const unsigned int  image_PVStreamTracerButton_height         = 32;
static const unsigned int  image_PVStreamTracerButton_pixel_size     = 3;
static const unsigned long image_PVStreamTracerButton_length         = 1168;
static const unsigned long image_PVStreamTracerButton_decoded_length = 3072;

static const unsigned char image_PVStreamTracerButton[] = 
  "eNq1ls9TUlEUx4+u27SyVf9B0zTTezXW2CXxR9MPTU2yqRSV4IIpIIKSwEPkKQHyI9PRtI"
  "siGA9Qmxb1J7Rs4b51/0Xn6oypvadjPWfuCu773HvO93zPubs/vu+ez1LGJx2Lir7MXLmi"
  "hGbIjQ7SaIGHHprN64OtbnNswwswRQRnnCaW9cmAy8mxd17Ck5Bgj9BoWiesa6yzT7pihM"
  "4AfVeg4bfacW2dAesZE21h6UojtE/QTJ5VtjV3OkcUaZaQPjBN8VM2i45oKldQ0SKnlH9R"
  "izgSJ4Z+eOShmTVWrmpjnTyuq83QLdH54n5c9mgankagLw2P/bnqDmdWqkpIFl1J6WoT7u"
  "R6za2yzaJmEipbioQq9KIKmC5WPRIXPJ4AVwm8OzC0Dk1W0mInTTbBlaTpNRqUT6qZfA5D"
  "IzefgCks2KfY1mfVbTXd4dqx7do3X/kpbT726dPpQox6MDpyd0Bwp+jMvGbFhmPQFQSDGa"
  "xLMFyAgXlh0H/ShffyxvVt89EUY6p67WEJ6UVlBRqm4TjbyAuWADTT+hfD6rd1u5VQlNzu"
  "gc5J/ITtaXQcWyrxgml4ybE2iUqxYxu8HieuP0yfH68KRismATr8gi1Ep1OqpzvmVsnNLo"
  "4djtFIQit2hBtMZtGdkq61wjOZX9UeUSantMoGcyXaJGK0QOvQCd3AIWcO+MRkxnZEp+Kn"
  "iOvzE6GNNwRrSMsLjsSy6EwQ4yvoCuz/UlhfaWk2niRuIa8Ep6Hdx8VNrqgfHYyKo1noiZ"
  "JbPdyJpdLhf+vq6tS/8k6Iw7PE0MeNphYdYr2DE4gVRtNKYFrrhof5DmkGd4ojCV5pD9y8"
  "Gv8SwpFY8vaPo/cRS5MfTnUN8sUhmdv8aZTrOySjcKxYVFEWt90dIPXdWolSXc1NjfWDPi"
  "pn2DpTVy2+iKWIjY5m1lnpzIPvmAWOZDgQweYJ917T2MI/T42/+dicRWuANDwXRmI/Vxb+"
  "cyod8LnZAxHuyvZxdC5TyrpMPbTA/UuXicGMS3DIdPa97i+WjosXmVLZPbcXkZbF9HrDXL"
  "jeKlqD+EbSH66U0eB8RNKP2GFyxQ19+aJ5rIau1r75Br4vYM7aZ+f15dvfLoBlscb3pcaz"
  "Bb1Ju5zVl8/yOZwU0J+F3jl8RZyTxDQyd+p8Oev6Db7iXrw=";

