/* 
 * Resource generated for file:
 *    PVSuperquadricButton.png (zlib, base64)
 */
static const unsigned int  image_PVSuperquadricButton_width          = 32;
static const unsigned int  image_PVSuperquadricButton_height         = 32;
static const unsigned int  image_PVSuperquadricButton_pixel_size     = 3;
static const unsigned long image_PVSuperquadricButton_length         = 1656;
static const unsigned long image_PVSuperquadricButton_decoded_length = 3072;

static const unsigned char image_PVSuperquadricButton[] = 
  "eNrtltlTmmcUxq+SxsmYurDvsijgFmO0YEzVRBRBRRQXFndQBJVFFJSoiOBWl7jHDa111I"
  "Jb0k5v86/1QTqdVJPGppfJN+84qO93luf5naPv3397vq5ncn5Zrjcq2nuVXQPRY3IqjQ6H"
  "f25zd/9/Rjb0uhLyypMKqpKeqZIL1ck/1pHKW8iVJmqNlaaxU2sspDKDVGcJLG98UfDB5O"
  "dqwoum5Oe1SRJlYr4cifAtpbqH3uhid/g5pilO1wwO0zCCvG2eQOTy7R2DR67eIRS5ooMk"
  "byMUNyQ8lcWLJfHpBYkSJUneTq93sDsDXOsS37mZOhQSuLZ5tlVO1zRN3Yss4fOLz8aXGv"
  "piOlBVPURZc6JE8ZCX9YDGixdJCEUa/JDVOsa1LCCy0PuLaPRYPHaCD3z7GvoiKzpanN5/"
  "Cb4ZOkTxVHUvQztEbxigqiyEksZ4Uf69BFIcMy0ht5RYqqPV2VltPm7va6E7lOUP58799n"
  "jqSjx6jIwp5p8goECujVxcfRyYxRWiTM9oGkIE1MnQeVAwoagOwe8TGXEccXy6NOlZNdSj"
  "N7k4xiDfvi4aPhCPHolfHaUO7fFsaxzTNL1pUFjXHVhcvR3fPzsPv1gto2xjkNM9g5aZza"
  "/QDpSRaztw4ddweGJ61jYynlIoJ75oitrRMcm1LiIyjMAHeIFXaHU2/DawtHY7BbFUjwso"
  "I6VnHj4CEvQCJiHU0wpN+Oz875sbW9vAGABT1VaAdN3yOFM/TK93oiSywggIJxdWbsQvaL"
  "GhQfTO7V3i9a/y7OvIwjZOMfTDlKousKrr7vvw/vqbrdyyGoQiKzop1WZ8JZW3osjoeam9"
  "7UWra4xS1Y1KYhAKXDuCwR3oDGZAJuwjyVu5xdV9g55/CrsAa5Ad5/snL3ESf1AkFyJvbX"
  "617sObYFhQoaM3ODnds1HI3aFUzz6OYHD3ryztfrrGDgz4Mg302djejb3oCwQf0PlxLOF3"
  "FE4cW/woozBJqoxNaOTi8obLhJIG8MPrX04b2gPeOGkjhziABFlgDWxl6NxUlRk3uSWqcl"
  "2H3txfqtLcSyTfJ7MfCnIePS6OTn1JI6Go3jExe8MFWE+uNHJMQYFtVeQJZYyfZE6E032n"
  "IkwTUHSH0Bqv73VK9yyrbRyTAmvI8jZMB0iD44gJ/UllzTEvsBVvg9Rs82CP4XV+37LIvZ"
  "c1cZozfZUz/TY7eJnhj2By04Z/ju0HiAYe2O0+gMfQugEtrbYfiNJq+ygqC1nezi2p+ei4"
  "IQVRZoAOKea5NNd25tjxk5l3eQt/5M79jpnN8kfS0U5MNMcGr3+Fa1lER8AAVUVz6TwAm6"
  "w0NttHPrkx9g6kWjMqYXdOQnmx9zDDd5o1GckOnmcHLzKRYvxE5D1K9RxEAXC+AdIAD/Ug"
  "C9PgRRcY2M9uV4dvhiQz4D6chSCCga3Uod204X2h91D06giORLVCFwNbGGQMDhZRFACtG6"
  "hLGrvutLcvriRNXRSlkdHownRge2DFYfpQM1xARr79WiIrJJrjGKcwyJRKU6rCcPc/DddZ"
  "LgGbVG8FLbR6B7PZyzYGoAbWCLsTFk+wWsaY+hGMIVjKU+lvkP/fEvlmpLqeGIdEHJnhei"
  "fohGqTM7j4qRX9xU/47GxianZja+fb/0Vf5/MnLVzCgw==";

