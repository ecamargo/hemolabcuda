/* 
 * Resource generated for file:
 *    PVSurfaceVectorsButton.png (zlib, base64)
 */
static const unsigned int  image_PVSurfaceVectorsButton_width          = 32;
static const unsigned int  image_PVSurfaceVectorsButton_height         = 32;
static const unsigned int  image_PVSurfaceVectorsButton_pixel_size     = 3;
static const unsigned long image_PVSurfaceVectorsButton_length         = 1128;
static const unsigned long image_PVSurfaceVectorsButton_decoded_length = 3072;

static const unsigned char image_PVSurfaceVectorsButton[] = 
  "eNql1UtLW0EUB/D5Lv0ECt124Sbgpgp1kSCWkoKVGMFFogg1UfT6iDeEy01cKCKoiCtRJE"
  "oRFAy4qSAqPlAQQRAUURIEF0F7zjzuzJ2Mj9Thv5hNfnfOmeNIciHifjUlSBOmaSdunLhJ"
  "4gzTODy2jRmwSY9NOm0StEiDRQIW+WyRT9YGXST3U1Bawjrujr7uLywsmPwOhdLSLqL6ju"
  "ZfXFy8ev4OgbwkCxx9Z2hoSPNPT09Vv1wug393dyf8bkXT4uHSPzo6kp8QPpYg/IeHB/A3"
  "xCK5fuFUJunDXZexcDbNf35+Vv3NzU3Pn5+fX1tbU6ikJy8vL8diMYFLH0qQ/gBmZ2eHtE"
  "ofWuT54+PjflwmnU7DSfC3DBe+r//U397e9nx2v0p/xpQT8oyMjExOTsIGPtHc3FxXV6f5"
  "+XweNwNiOCGKv7Kyovp/6GpsbARzcHCwvr4ev5JN0WSZPDU1ZTj/C743PLD+0pWny1cFw5"
  "VkMhkw8UMU393dZf7T0xNcLrYxSIczYF1fX3t+bW3tb7p4B7z4cR4qx+Nxef4e7uMVBOlw"
  "Qv+/qP3P6fJbPk4jbIa5XyqVCoUC6w8bzo/7WILiYwngi+F8v18sFit9aO/+/j76PdLHEo"
  "QPJXgj9Lq/tLSE9Vb4iAsfmr+6ukpa8Pzs/YGfKO/PG/2BEsLhsOpD8LSKjy+G8HFfTf9v"
  "b2+184N8f39PEmYfq6vyfmdnZ6VPO3N2dsb9TszV1dVH/L29Pb4fdmTA7+Q+Tqzw/3/+U1"
  "kVh0uZm5tDv9U+Pz/HK24wvj/oF8Qy+6ns8fEx9xM8cAXYFuHjX0SD4f1ZX19nYCgU2tra"
  "amtrc6DJzE9VJCF9uAL0W7l/cHDA8JmZGdUnY6I/o/5och+WAAfgfsy5ubkx+jBv6vuG/q"
  "hriB/HJJzLy0uGez5eQQv6/NEIWLCBEqrw+3x+NBplPok4i4uL5FcaJ+ebjf9J6fl1v9/k"
  "J13JGv2I8L+nMU3pw8NDz4cWyfcH/KRrSKUfc05OTmpqal70A+g/Pj765v89fhcN9T1c+k"
  "0+H4dc9bvf8ruk39vbK/0fZh9w3Y+7hnRlp6enJQ6JUD/i95u4j0+E8CcmJqr2o1kpU/yd"
  "/j+UkH6X";

