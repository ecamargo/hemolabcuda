/* 
 * Resource generated for file:
 *    PVThresholdButton.png (zlib, base64)
 */
static const unsigned int  image_PVThresholdButton_width          = 32;
static const unsigned int  image_PVThresholdButton_height         = 32;
static const unsigned int  image_PVThresholdButton_pixel_size     = 3;
static const unsigned long image_PVThresholdButton_length         = 688;
static const unsigned long image_PVThresholdButton_decoded_length = 3072;

static const unsigned char image_PVThresholdButton[] = 
  "eNqllj1OxEAMhVOlSkOVHmmbFdI2ETTbBARKtxQgUVCwnAAJhDgHNRUSd6GkyIF4yYOH8c"
  "xOfnZlrfJjf/bYHk/ar892SD7e356fHiAX52dlWeIf1+0Iw0Fg2f8IhOA5bvl2kiOaMDwL"
  "DDXx1lmlHRF4cLgsjtYU3DLUXfrR8Gxg1raDL1bg5yebrN5S4AVPoBx6sXybQ8YGCCNkkN"
  "TPLl/y0zt5oY7UQr5jKioF1sW8PKYt+RR64dtifZ1tXy0/wVQwsMo3jzCULZIArFwUVQM4"
  "BZriI2a6ZhK4RoYKJoOxgifMDw0tn8qAc40sAdS6V1Xz0w/9AhVqKJbPtuQqxJfav4RXDd"
  "ScziCfF/DSlXixoi1XzfyQD6xb4yQ+21hbzD53fMSQyEyCLy+uJ5lDCF10LddD4CVaXAgK"
  "pMINzhDp/BWiT53NpxPX2CP5dn8lamEbewbf7krLZ7o4hWbwNS05zUI+LkAOmWm+mkqzUY"
  "V2fNWUOoN8tY0b79zLId8OE6vv+NrRmuROfyqfcz7ERuvL5ddXt5wndi/UN/dRfVYtcWZF"
  "9yDnSTe4fiehch7qR4+qBN8ei7LlLfMQ0hJn9Jg9bjnphpnNV78lUuFadyR/sIJhVq1mgi"
  "/y1E84u8wofzY5rPiuft7nE9SWTPNNG3B/cvTbb0z55rmYTf4GHG57SQ==";

