/* 
 * Resource generated for file:
 *    PVToolbarPullDownArrow.png (zlib, base64)
 */
static const unsigned int  image_PVToolbarPullDownArrow_width          = 9;
static const unsigned int  image_PVToolbarPullDownArrow_height         = 32;
static const unsigned int  image_PVToolbarPullDownArrow_pixel_size     = 3;
static const unsigned long image_PVToolbarPullDownArrow_length         = 40;
static const unsigned long image_PVToolbarPullDownArrow_decoded_length = 864;

static const unsigned char image_PVToolbarPullDownArrow[] = 
  "eNo7cGAUjILhCRiwAVyyuPTiMplINwAAMTdkHw==";

