/* 
 * Resource generated for file:
 *    PVTranslateViewButton.png (zlib, base64)
 */
static const unsigned int  image_PVTranslateViewButton_width          = 32;
static const unsigned int  image_PVTranslateViewButton_height         = 32;
static const unsigned int  image_PVTranslateViewButton_pixel_size     = 3;
static const unsigned long image_PVTranslateViewButton_length         = 996;
static const unsigned long image_PVTranslateViewButton_decoded_length = 3072;

static const unsigned char image_PVTranslateViewButton[] = 
  "eNq9ll1rE0EUhn9Grko/EkrbpKHN7lqCEBqaZIlJk00jkVKtUC8jKGgT0N55IX7ciRU0Yq"
  "ukv8VLL+L/8XEPHcaZSWxxMRzC7GTmPe855z0nO/nxfZKojb+OJkljig2ePg5rlVQqlawL"
  "0EAGtlw+7vXGLHhMChbCuVwN5H7/J1YuD1ZWdv4RX5AFFsKCrOFXatUddVgypj5cVDYtjZ"
  "KNXu9CR77EH3reoc7fOAklZTDEuxM/m60vLm7YLkBbXa3q+PPz604mbC4teZgzBAnWvgKl"
  "hYV142ShsG+74GQ+38acxYqjHqucSxJYZLO33rw6M4rV2t3nJ1cyj51iI2kKXMIMw7ekK0"
  "5OaJ9nx/fvSQgxjaEW8tiogg1eqbysVl+XSs9xQXJsfEKYm1vN5cI4LeHGxm2+9YyprnGC"
  "Y1tbD6g4xrFpkqMvgC0W+43Ge98/gox41NPlBE+nPRgeP3kEuK58IwSExEnON5unBIvhJe"
  "76oVNaOvhVhpsQgD8cMIIVL2trdcOFFAVpGeB/7Xo9QNyRoiA4wtE0dVEOA3/25DEO84g7"
  "qOrtadRdVxcLnF5rjunglHt7+5nU3VaXSAtzjpSrgFOaZvNDqXQi6jKKoo3oqSHILJXJaY"
  "OjlmLxId2h6u6cojNCYP9yhA70kStSFGmJzJS64kAGdultXcWcQ2vKKeZ9vXdEXXjRw5wd"
  "AmkxmIg8BN9uTOFDdMqMohtatee5GtTOVwLpl83NHsaE55umTv35mY0PJTaXl2+m04GzT/"
  "k1ir60Wp8ajdNO54zBxSKKfi92dz9yS4WgC0ZGNHd9/2Bv71uncx4E96f9y+TzEb+Wyyf1"
  "+rt2+3O3e9FqjbrdMY5QmrolyZc0QrhQuMNhkKHXbo8I34kvY1PMyAxXwFG32KFYnncQBI"
  "cQgDYWRefsZDJbFPe6r2ScF5NHEMgGVEkmtFkQNU4hltSro+/fhXOl8iKTuSF9lOw7pOQt"
  "Wdj/8F79C15n3pY=";

/* 
 * Resource generated for file:
 *    PVTranslateViewButtonActive.png (zlib, base64)
 */
static const unsigned int  image_PVTranslateViewButtonActive_width          = 32;
static const unsigned int  image_PVTranslateViewButtonActive_height         = 32;
static const unsigned int  image_PVTranslateViewButtonActive_pixel_size     = 3;
static const unsigned long image_PVTranslateViewButtonActive_length         = 980;
static const unsigned long image_PVTranslateViewButtonActive_decoded_length = 3072;

static const unsigned char image_PVTranslateViewButtonActive[] = 
  "eNq9ls9KI0EQxvs0l+nLzCXP4CEQCOpB2Ev+ES/xEJQQTUBvq+CCmwiamwdBvIkKGlHXTZ"
  "5lj3uI77O/2cq2nZ5OVtlhQzHMTKq/+qrq65qe/PwxydRG34aTrDHF+l+/1KrlXC6XbQjQ"
  "QAa219OjUcANj1nBQrhajUF+fVVYv69LJf2P+IIssBAWZINfLutqpWScpWLmx0Jj88oo1R"
  "iPZ5DFjo/DTiew+TueUDIGQ6J78et1XSjE6RCgVSoz9cnnYy8TXhaLMeZNQZJNL4FSPh85"
  "nq2WTofAs9EIGw1/sxIR/qk8NZcicLO+rsffPzvN2m5/4q90CpTIKzaKZsAlzctLRblwrt"
  "U8fHizszNNgSs9MiHAcbqQBj8/VxcX6vQ0CUFx0viksLQU12oRS7g2m5qrXTGza7zg2N6e"
  "rtdDmo7bPMmxL4Dd3w+ur9XubgAZiWiXywteLEYw7B0dAm4r30kBIeGJ/81NkixGFJbY5Z"
  "oH/p7hJgTgDweMfCUKWTshpCmoywH/6663EyRcoRCRAvjz1JW0YxZ/8eRxnHlMcqnG9vZ0"
  "+m6rixuCfmiO2eC0++Rk2ve0ukRamHekvAec1tzeqsFgqi6nKWZEL0hBZqlMzjQ4ajk4CJ"
  "rNt757p+iCFHgvBOyGwlOkKNISmRl1kUi6+8lOSemKN86GspkLuC1m1EUUO83FKSTflFkm"
  "Ig/BT29M4UN2xpymO1pNz3MzqL1HAtkvm5saY8JzZVPnZn+L8aHEy7W1eHk59u5T/n18VP"
  "f3yUh5elIMLnNzd6dYZVKwBSMjmrXttn55Uc/Pqtud+5XZ2Ai73XAwCK6u1MMDa9VwSHGS"
  "QMxhs0qKL2WE8NaWxhlk6OFP+l58GZtiTmVaLXDevgW/Pz0RhDsdDSC0hXm7Ha6uRjT3o0"
  "cy/MXkEQTAQaaYwHJD1smR7Ogwq6Pj9nZS7bOzYGUlkn2U7RlS6pYt7H84V/8Ce8/dlg==";

