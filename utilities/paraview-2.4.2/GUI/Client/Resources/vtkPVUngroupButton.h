/* 
 * Resource generated for file:
 *    PVUngroupButton.png (zlib, base64)
 */
static const unsigned int  image_PVUngroupButton_width          = 32;
static const unsigned int  image_PVUngroupButton_height         = 32;
static const unsigned int  image_PVUngroupButton_pixel_size     = 3;
static const unsigned long image_PVUngroupButton_length         = 1392;
static const unsigned long image_PVUngroupButton_decoded_length = 3072;

static const unsigned char image_PVUngroupButton[] = 
  "eNrVVmtTUlEU5S8YXDA0XyhIljmVFNjTaiYVRFHTVAQsNTW5NqJiPlBRLLG0RPHJlGlWaO"
  "WUfahp/GstOHhAvFelmmm6cz7APeeus/faj7V3dv7BMza2RdbGxve/i9zRsaFULkgkXrLE"
  "4tni4jdHvOXD5ke232lk7bNLPs4DgAKgRDJL8fEbbzIzFw69wtjec+ySPl7fLC1vZ25Wqw"
  "wm/8fPkQfM5nWGmQGaxfKOooEipXIxeMX8AeBs76BEez/J7Ehtdqc9nEq+N5JQ2aEqMdID"
  "8/PbDOMRi2dcri1Ov3A1DODDv1BmTqzuTm+fyepfyx58r+heTq4fFuVX+jc/kQMazTIQKD"
  "jbO6QqNdVau+gBUCQSeWDGfnCwDUJgc2bPK5V7W/Pix+nBd2mtz6WGNiAQ4/EtjAwg9w1n"
  "FNbEaxsS73ZJy6yi63dy9TXUQT4XjuUVJ5kG5J2LZ12fcse/ZvWtpjQ9BWPgDbta7QrwCe"
  "cZt6sSa3pSmsbhbFrrZLJlSFJocU5MBX30CYXT3MFl7QhrasszuW1BYffJ2OkTdX0ZBTVk"
  "F1/pdCv4ARxJYX1K4xPl49c5IxunHevprCexqlNVatx1c3p09DMnRcIrBvh7oq4fzCdU2c"
  "S3TbVtndiam9sGvssV+KrA3Iq4y6wvzwx/uDT9M3f8CyKVZBkSXi0jOHBTq33N6QIiVWvt"
  "jlMXgVJVSa3TPUnewx5YRcgB+fHFTakP3EiD80+2cpz+9Ede0CW7EcKHm3K599Aqi/wLe4"
  "AfjpQGkepPa3uBYCEEKY1juBH37la3Xyh8GVNDAD4hnzzIKCSbtKId+YCURg6k36qgu6Ax"
  "Et/hcMeKT4IVKHaNDlVDEiwKn4aYYXKwWlpsMeHvfzyeFQHvc1YgyGYYNZ8vR8Nf5sfPEQ"
  "hOnTxZ4PEsxcAPa49TawP8PObgBykdwU+2wzF6MD9oPrzxLfrT+JpMb/fmpy6Qnw+n5LZg"
  "fjbsyU/0n1jzE8mA+t1XX6vnwvVll90w0DarVi/FVF9+/3fgQxYP7A/ltD/AX/7+0BV3sU"
  "h0rQL9wbv8im7J5XMkBBH9bWVvf6sj0gwmaXCj+9tl7v62G4KQdnD153rSnyGRCsUcf39m"
  "U5snAv252yezkv5cTXZBPvDR3kkI4GO8jupLJdEXGA99Qf/h1pdAVlB9+RLSl6L7NL2JuE"
  "PZw/poCOsjDMCuQjHPOUXMLob0UQl9nNjOC+njpLQspI8EAZ9HXhEh/d8gvtgiOcCj75aQ"
  "vg+sZQ9B333J9U5RfhWVb1I7ZD7BQGKzbZL5Ta9fxRssne7N0eaTifB8UmqMOob5ASNKcM"
  "TyRo1wh9ZRcL4qOY75qoLMV+ao+YoSBbTgCBdYmBU5J6Lfmw//x+cXLi3cgw==";

