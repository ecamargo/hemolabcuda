/* 
 * Resource generated for file:
 *    PVVectorDisplacementButton.png (zlib, base64)
 */
static const unsigned int  image_PVVectorDisplacementButton_width          = 32;
static const unsigned int  image_PVVectorDisplacementButton_height         = 32;
static const unsigned int  image_PVVectorDisplacementButton_pixel_size     = 3;
static const unsigned long image_PVVectorDisplacementButton_length         = 1104;
static const unsigned long image_PVVectorDisplacementButton_decoded_length = 3072;

static const unsigned char image_PVVectorDisplacementButton[] = 
  "eNqllttOE1EUhh9DQENa40xPUxPBgES9UGxNjCXxhkQgJoCGEi/U0BaxrUApgj6AiaG2Ug"
  "6KbcXH8E7UPpD/3mu6utnTaUds/kxW90y/tda/D53myY+mNx1US+ccn3hsHONNzxBGOQdB"
  "C2988yfm+wxr4MFTW+OTfaaVST3vDsQD0N34HS5MS4FbRnIbfMhMvsUD/bcnOUv/1VtoxA"
  "1OQPCJiSC99AxArkqFsy4M3dQacVbFfIyzk8SnAIOA+xJzGtytEXjlbATPkOdA4cp8SoG7"
  "zuK1RuBPl0aIz1bgK/HxDH5LBArc5E/MnfJKNsIpVD7XTO1oc+reSAPryrl63fioH4Oi5u"
  "KxkGLF6RQNuprJ7cFrsaHST9bgWEybTdUiDALln5gnW1AbhJiqaqVosHArkt1luDGVUmdQ"
  "41PlkeL3SPHYXBS2EJ+wCHyJWXf4iZWtqkuoI1/CBR/m0LZVplLhFwheBRYa/nhi5ara+t"
  "T4MMdcfMd81K+tnDa/wLbbcGhwLK5tMZVPCzKyacMh/8RjbXOJKV7YYjhh3eAaXzgD+KYN"
  "F3xRrcIvNIRdhQYFLfIvqCNc5dvOKPzwxml+C0u2Yx6JDFm5PbfzjfhtZxS45Lf8kXDie4"
  "erp6VdfMsZWbNSv8KXtkt4WcjNGfXwxAHCcxrhDVsQu953f7YFr5vJLWF7mci/oe5w4nc8"
  "3jvw1+u0hRlu5fe7ONPerVduhNdr7IAq9CX5dYLDasn3Cie+796j8NrXkFBNomqgCRXqYr"
  "WIrzVz4Q2sABPX4cofqKczzA9m90L5w2D+UKRYPaJcYOJKfArANGYyOLso0E6VLvzAy0pg"
  "5VMwdxDMH4Refw6tfkGW0NoRAtxFDIsYi8CjM6SB6Kh/KmNmSuZyOZCtBnP76AVZRDuSbz"
  "wpsifE9+gMnzlYP/7pZTO9I1K82oVdQnnRjpz96yiY+YB7L149NgcujxpLH5ACCmarspc9"
  "+kMhOPHdXkK8NIIUdiPpncBKBfMi/JHOq/wzwDWvRCPIkimpxf/TsunhVXT00ov3F2dW2n"
  "y5p4zp1P/zba+iI+ejI2yO8XAJOrP5bl7xmwy9APd8T3bqL7fuZMo=";

