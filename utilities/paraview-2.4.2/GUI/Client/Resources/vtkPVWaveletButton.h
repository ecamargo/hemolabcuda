/* 
 * Resource generated for file:
 *    PVWaveletButton.png (zlib, base64)
 */
static const unsigned int  image_PVWaveletButton_width          = 32;
static const unsigned int  image_PVWaveletButton_height         = 32;
static const unsigned int  image_PVWaveletButton_pixel_size     = 3;
static const unsigned long image_PVWaveletButton_length         = 2360;
static const unsigned long image_PVWaveletButton_decoded_length = 3072;

static const unsigned char image_PVWaveletButton[] = 
  "eNrdVvtTE1ka/Q9mhaTJ6+bpTfNYyDTMWlsyo/PYkVkVRwGBGZU3IQlpBYGAgjiD0EnoTn"
  "tLUENApQCBCU0nm+75ClSm5l/b2z21q5Q7W1uz88PWpr5K3U6qzvc4557+fv75//+T21SS"
  "jzcu9Mz/+fJMWWUYf3L7Yu/8gryxX4LfBph8ujkurV5KpCr7JpjrPNMTYwZiTIRG1DY0xP"
  "TTx2GmN850DFd2jV8aTyUfbeRe7v8amqLAQmpjdCJ7/sZ88NwI82WUaY8zfTwTjdr4iJ3v"
  "cfDXEd+B+FbEt5nfHS7+mp3vs/FDRtJBnrnGW8/Fyhsj57vnRxPZ7IryLv63Fx/bysf/UB"
  "krPxW2/oWC80w/z8SGbfGwg+9zGeDtJuxlM1rNx28cfFcFP8jwMSbMM908czluaYycqAt/"
  "4A+Xoxm3dSmXe/0L/pXWFbtDsHiEMm7S8ukIcynOdPJML89EhplYzBYbsvFhOz/gMBqhNQ"
  "8YZfMRZjjKDMYN5A6eaebtF+64mhdsZ3OOL/L2+rzbn/9n/f0z646v086WRftf09bTQsXZ"
  "WUtjwvrVMHOFZ1qGre0x2r7BQreZlB7aYkwbT/+t+Pq2s3POO5jFYwU8WsTjJX9MDSSKgZ"
  "miP6y+ZfOJ4uqR0BxBAkG3ZVe3yJwRLPVzZd7Jsj9GyxvCJz4Mn6iNlDWEP+CMM/PJrL1J"
  "9HSuYV7FMzoWAT8FvA1Y1bAKWNHxLuC5t9ISFhXmdBKNE7ckISKijIgmZUe7aAkKVqcQKJ"
  "t2lPNW+4SFmbd5l+xn8s7P84Goiu+V8CoEFa1Kz9fAVh1shGAdQyG4VcIPdU/H2/koxUM6"
  "GTSQQdMEZWQfeWBkWZRd/VLFx0k3I9RYhGp73o7ziMv7etXAVIlWGywWa2E7BFkOljh4Ug"
  "svWFCMymWdpm5NlN6VkIGcktEDgpJGChpusmicx+SKM0mbR/BX5X3XVd9IEW8B3tOrYJcW"
  "zBngq/RAHzEUMejGcLZ1vKN1vjx8F98Vz/jk+QCZCZBZryyiFEGLBN0jRsa+jONC0nNdxc"
  "uAc4ABgppWBUrQiD0M+SDkq2GzFtZDsFwH2RpYpxPrLB3DbxnLoXsZis+SbpYMBuTvkZhB"
  "YwRFiatlEbWlffEtOm0smzxuAC7RKJld0BYEDsY4iHEQ5uA7miu4oXWuHcNvTa+jBEFSBp"
  "MJlnzDkhuYjLsFEfGk4qxg+Uhwnn+BJzR8UzWksgZVulJrIM9y0NMAl+qhmYMBDu7Xwgbe"
  "0fFDTdp9/S7+4LOX7ozoIwsBMofJFG2BJV0nScItis5ropUTKhqeUzUGZkq4QAWza9Yc4e"
  "Bqg4F8jQOeAzkEa7SjoF4IFkrS62P4qZV99zQtPsGSMEsSmNw1z1OYzLiFjKtXQu3rxlj2"
  "NJNTkYNRE3OEgyk6Ew4emBlnOJijjLNFRTp4c8wwlxXnlbQnLbKklyVtLOkPkPtukvZQFW"
  "UylGV/6ocgqFSBVIp1sMYBMUM2MWP10FEPV+uhi+ZitQKeL0m7x/D3f3yFYjK6T7yZBZYM"
  "sOQcJdov30dpgu4SNEr88g9Y0/EeBOkN0oshQy20i/ifoLkRPmuEz+thIARpVlfwc8DTpf"
  "dd2tEpUX6RvOgmKUwiLOmkvfjIPJogqC3l7dvETwATqk+tGrZo2fVw4xR8eRq+aIAOqp8Q"
  "5KoMrarBYommeB+/qk5yDYt+eS5Apk+SOT+ZZclNlox5pSQakl0XnlLZ0LsTgifmtG/UG7"
  "RSiu+YU1rm4Cn9i1KMi5p/vPA+fnPnCnMq6RoRT5JpltzykiTtxSunvESgU/IkVllNrYZf"
  "lEMBkyHDFnImC/SXSTr5Wsix2wVfvGgP5d/HF4hib0q6LqfQXTkg32XJpDEcaqrfETRLPB"
  "mqvT1aWzXsBLUSa9ys5/8Q0i0qm2p9B2d1/3jJ16V8OqD+yxdlKrsfurmMhglKyD4pxZI7"
  "bkmkvuq8kvLEVgziUrrhwEWq0jVzSoY4a2CNCt643dSoJf1U6sd09uDX3sVUSKHZxwantw"
  "hVPjK8SHK1LzIfP6KqoP7A6mqt4WyL1DZr9E3qojhP74UZGxDdPFTh6N8vD/vwqnVpHU0R"
  "FCEoLKEF2g5xXX1sOMMeVME2vaFsQTFg1+nbhBqRjnU9qr1S3/z0n68orQ+NFI4LKefFlP"
  "Nb0TOyhndM/1zX8fcafgj4BRizglL08JV69NNv2ILSu0UUkW3nkhavYD/zyD9SdDfv4XgR"
  "jxYMryjoTfrBszdH/83mll5VQ9Ela41QYRXsp/OOz/J4QcOPoCl/8Ozw6HdZDve1w8qvJC"
  "sW3C0KjilN2YNnfzv6ffdPpXDY3LXyYaxwZxn+13bjvwN3KE6L";

