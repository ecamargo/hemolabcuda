/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVBooleanKeyFrame.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkPVBooleanKeyFrame.h"
#include "vtkObjectFactory.h"
#include "vtkKWApplication.h"
#include "vtkKWLabel.h"

vtkStandardNewMacro(vtkPVBooleanKeyFrame);
vtkCxxRevisionMacro(vtkPVBooleanKeyFrame, "$Revision: 1.2 $");

//-----------------------------------------------------------------------------
vtkPVBooleanKeyFrame::vtkPVBooleanKeyFrame()
{
  this->SetKeyFrameProxyXMLName("BooleanKeyFrame");
  this->DetermineKeyFrameProxyName();
}

//-----------------------------------------------------------------------------
vtkPVBooleanKeyFrame::~vtkPVBooleanKeyFrame()
{
}

//-----------------------------------------------------------------------------
void vtkPVBooleanKeyFrame::ChildCreate(vtkKWApplication* app)
{
  this->Superclass::ChildCreate(app);
}

//-----------------------------------------------------------------------------
void vtkPVBooleanKeyFrame::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

