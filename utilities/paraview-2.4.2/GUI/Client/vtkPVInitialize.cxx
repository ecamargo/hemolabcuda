/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVInitialize.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVInitialize.h"

#include "vtkObjectFactory.h"
#include "vtkPVWindow.h"

#include "vtkToolkits.h" // Needed for vtkPVGeneratedModules
#include "vtkPVConfig.h" // Needed for vtkPVGeneratedModules
#include "vtkPVGeneratedModules.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPVInitialize);
vtkCxxRevisionMacro(vtkPVInitialize, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkPVInitialize::vtkPVInitialize()
{
}

//----------------------------------------------------------------------------
vtkPVInitialize::~vtkPVInitialize()
{
}

//----------------------------------------------------------------------------
void vtkPVInitialize::Initialize(vtkPVWindow* win)
{
  // Include generated modules
#include "vtkParaViewIncludeModulesToPVInitialize.h"
}

//----------------------------------------------------------------------------
void vtkPVInitialize::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
