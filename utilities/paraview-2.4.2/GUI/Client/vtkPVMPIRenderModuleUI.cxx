/*=========================================================================

  Program:   ParaView
  Module:    $RCSfile: vtkPVMPIRenderModuleUI.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVMPIRenderModuleUI.h"
#include "vtkObjectFactory.h"
#include "vtkPVApplication.h"



//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPVMPIRenderModuleUI);
vtkCxxRevisionMacro(vtkPVMPIRenderModuleUI, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
vtkPVMPIRenderModuleUI::vtkPVMPIRenderModuleUI()
{
}


//----------------------------------------------------------------------------
vtkPVMPIRenderModuleUI::~vtkPVMPIRenderModuleUI()
{
}



//----------------------------------------------------------------------------
void vtkPVMPIRenderModuleUI::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

