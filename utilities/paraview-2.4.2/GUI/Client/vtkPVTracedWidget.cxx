/*=========================================================================

  Module:    $RCSfile: vtkPVTracedWidget.cxx,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkPVTracedWidget.h"

#include "vtkPVTraceHelper.h"
#include "vtkObjectFactory.h"

#include <vtksys/SystemTools.hxx>

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkPVTracedWidget );
vtkCxxRevisionMacro(vtkPVTracedWidget, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
vtkPVTracedWidget::vtkPVTracedWidget()
{
  this->TraceHelper = NULL;
}

//----------------------------------------------------------------------------
vtkPVTracedWidget::~vtkPVTracedWidget()
{
  if (this->TraceHelper)
    {
    this->TraceHelper->Delete();
    this->TraceHelper = NULL;
    }
}

//----------------------------------------------------------------------------
int vtkPVTracedWidget::HasTraceHelper()
{
  return this->TraceHelper ? 1 : 0;
}

//----------------------------------------------------------------------------
vtkPVTraceHelper* vtkPVTracedWidget::GetTraceHelper()
{
  // Lazy allocation. Create the helper only when it is needed

  if (!this->TraceHelper)
    {
    this->TraceHelper = vtkPVTraceHelper::New();
    this->TraceHelper->SetTraceObject(this);
    }

  return this->TraceHelper;
}

//----------------------------------------------------------------------------
void vtkPVTracedWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);

  os << indent << "TraceHelper: ";
  if (this->TraceHelper)
    {
    os << this->TraceHelper << endl;
    }
  else
    {
    os << "None" << endl;
    }
}

