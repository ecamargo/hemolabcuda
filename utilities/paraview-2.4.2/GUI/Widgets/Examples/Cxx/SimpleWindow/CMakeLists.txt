GET_FILENAME_COMPONENT(EXAMPLE_DIR_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
SET(EXAMPLE_TARGET_NAME "KW${EXAMPLE_DIR_NAME}Example")
SET(EXAMPLE_SRCS "${EXAMPLE_TARGET_NAME}.cxx")

IF(WIN32)
  SET(RES_FILE "${CMAKE_CURRENT_BINARY_DIR}/${EXAMPLE_TARGET_NAME}.rc")
  SET(KWWIDGETS_RES_APP_NAME "${EXAMPLE_TARGET_NAME}")
  SET(KWWIDGETS_RES_FILE_NAME "${EXAMPLE_TARGET_NAME}")
  CONFIGURE_FILE(${KWWidgets_SOURCE_DIR}/Resources/KWWidgets.rc.in ${RES_FILE})
ENDIF(WIN32)

ADD_EXECUTABLE(${EXAMPLE_TARGET_NAME} WIN32 ${EXAMPLE_SRCS} ${RES_FILE})
TARGET_LINK_LIBRARIES(${EXAMPLE_TARGET_NAME} ${KWWidgets_LIBRARIES})

IF(BUILD_EXAMPLES)
  INSTALL_TARGETS(${KW_INSTALL_BIN_DIR} ${EXAMPLE_TARGET_NAME})
  INSTALL_FILES("${KW_INSTALL_SHARE_DIR}/Examples/Cxx/${EXAMPLE_DIR_NAME}" .cxx ${EXAMPLE_SRCS})
ENDIF(BUILD_EXAMPLES)

IF(BUILD_TESTING)
  ADD_TEST(Test${EXAMPLE_TARGET_NAME} ${EXECUTABLE_OUTPUT_PATH}/${EXAMPLE_TARGET_NAME} --test)
ENDIF(BUILD_TESTING)
