/* 
 * Resource generated for file:
 *    bounding_box.png (zlib, base64) (image file)
 */
static const unsigned int  image_bounding_box_width          = 16;
static const unsigned int  image_bounding_box_height         = 16;
static const unsigned int  image_bounding_box_pixel_size     = 4;
static const unsigned long image_bounding_box_length         = 96;
static const unsigned long image_bounding_box_decoded_length = 1024;

static const unsigned char image_bounding_box[] = 
  "eNr7//8/w380DAT/icW49P7HYi4hdTA+If3Y1OFi49KLzz5c+rGpo0VYkSI2XPWTG66kxB"
  "2l6YbSNEtpfqE0r4IwAEZDQds=";

/* 
 * Resource generated for file:
 *    color_bar_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_color_bar_annotation_width          = 16;
static const unsigned int  image_color_bar_annotation_height         = 16;
static const unsigned int  image_color_bar_annotation_pixel_size     = 3;
static const unsigned long image_color_bar_annotation_length         = 124;
static const unsigned long image_color_bar_annotation_decoded_length = 768;

static const unsigned char image_color_bar_annotation[] = 
  "eNpjYCAZPH1wg0hEUD2ascSo//8HhEhQfw2ESFA/D4RIUB/P8D+OFPVqIESCejAiXv0tBm"
  "kgIl79YgZnICJefRpDPhARr16fYboBw3Ti1ZOUHvBoJxIAAF0xm54=";

/* 
 * Resource generated for file:
 *    connection.png (zlib, base64) (image file)
 */
static const unsigned int  image_connection_width          = 16;
static const unsigned int  image_connection_height         = 16;
static const unsigned int  image_connection_pixel_size     = 4;
static const unsigned long image_connection_length         = 160;
static const unsigned long image_connection_decoded_length = 1024;

static const unsigned char image_connection[] = 
  "eNq9kgEOgDAIA3mgj+Jp/Axlc0nDcAxjjLlE2brUtapKzKwRqgfldH17uWnfRL/qPahf/Z"
  "PNRGQCz8A9/qzu/XqYHj2g3mYGro9ZhF9H3/t3NPOl/k1/vL6af0Ufecr8e32U/8ix0h+f"
  "5U5/ovyx/1l/VpyvmjtU";

/* 
 * Resource generated for file:
 *    corner_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_corner_annotation_width          = 16;
static const unsigned int  image_corner_annotation_height         = 16;
static const unsigned int  image_corner_annotation_pixel_size     = 3;
static const unsigned long image_corner_annotation_length         = 52;
static const unsigned long image_corner_annotation_decoded_length = 768;

static const unsigned char image_corner_annotation[] = 
  "eNpjYCAZPH1wg0gEV49LO5o42epJdc+oeuqqH1TxSxIAAOVLrKQ=";

/* 
 * Resource generated for file:
 *    document.png (zlib, base64) (image file)
 */
static const unsigned int  image_document_width          = 12;
static const unsigned int  image_document_height         = 14;
static const unsigned int  image_document_pixel_size     = 4;
static const unsigned long image_document_length         = 80;
static const unsigned long image_document_decoded_length = 672;

static const unsigned char image_document[] = 
  "eNpjYGD4z4ADAwEDOoaKYwBcevCpR6KJVo+uh5B70P2DSz02/Q0NDf9H1RNWTywGqQdpBd"
  "HEYgDlAeTG";

/* 
 * Resource generated for file:
 *    empty_16x16.png (zlib, base64) (image file)
 */
static const unsigned int  image_empty_16x16_width          = 16;
static const unsigned int  image_empty_16x16_height         = 16;
static const unsigned int  image_empty_16x16_pixel_size     = 4;
static const unsigned long image_empty_16x16_length         = 28;
static const unsigned long image_empty_16x16_decoded_length = 1024;

static const unsigned char image_empty_16x16[] = 
  "eNr7//8/w/9RPIpH8YjEAFrT/R8=";

/* 
 * Resource generated for file:
 *    empty_1x1.png (zlib, base64) (image file)
 */
static const unsigned int  image_empty_1x1_width          = 16;
static const unsigned int  image_empty_1x1_height         = 16;
static const unsigned int  image_empty_1x1_pixel_size     = 4;
static const unsigned long image_empty_1x1_length         = 28;
static const unsigned long image_empty_1x1_decoded_length = 1024;

static const unsigned char image_empty_1x1[] = 
  "eNr7//8/w/9RPIpH8YjEAFrT/R8=";

/* 
 * Resource generated for file:
 *    error.png (zlib, base64) (image file)
 */
static const unsigned int  image_error_width          = 32;
static const unsigned int  image_error_height         = 32;
static const unsigned int  image_error_pixel_size     = 4;
static const unsigned long image_error_length         = 900;
static const unsigned long image_error_decoded_length = 4096;

static const unsigned char image_error[] = 
  "eNr7//8/w38icFTpWlUgTgDiKUB8AIgfAvEPKH4IFZsCVaP6n0hzibDXAYgnAfEDIP5PJH"
  "4A1eNAgb1KQNwGxM9JsBcdP4eaoUSi3c5AvIUCe9ExyCxnIu32A+LTVLQbhkFm+hHhb1rY"
  "jewGZzzxjRHmqXWb/u89ce//iYtPwGxi7AGpA6kH6cOiZwu29ABNJxhmLd96+T8M3Hv8nq"
  "AbQPIgdTAA0o9FXRuWPIY1nc9Zc+4/Mrh04xle+5HtBoEJUxb8D81bhC1fOCDZPwmfmXsP"
  "nkYxc/fRW9jVAcMbGWxYt+6/IA/PfwuXJGzqJyGVa3jLFq/w8v/nz6GGA3q4IscTCIDUK8"
  "rI4LP/AVKZSjBNmdv6/3//HjVsQXGDLY5A6mB2a+o74TMXVp4TtB8Uh76+YVjdgG63o60t"
  "2G4FRR1scY+MYXXJf2LdkJKU8h8fSIiNBdstJixOyO7/SPUY0WWId0Lv/+LCEqx2F+Tmwu"
  "0GqSPCPFgdSlI55h/XgBEPID7IbhB2Cion1qwf5NiPns9gYMG8ef9tvXNJMesHqeG/af9N"
  "vPEPkifBvIekpD/0tH7//v3/hjo6YBpbviQCHyA2/7XNOowzn4Fo9PQAUk+EuVOIKX+qJ+"
  "79/+XbLxTzA3x9wXZLSyn+D0if8T8iPAZF/tPnr2B9BMxOIFT+guqzl2+/EMxn2PLls5fv"
  "8NWXD2DtVHz1z+VbL1Hrs74+eD5zj2xCUQvig+RR6oGrD3HZP4mY+hc9f8HsxpXPQOIgdc"
  "iAUP1LTPvjwL59cLuNbcPxxqujVxJYPSyuVDTM8bY/8LW/QBhmLwjrmfkQlbdA6pD1EWp/"
  "4Wt/gupvkBlY/IG3rgKpR4ur04Ta4QPZ/h4M/Y/B0P8aDP1PevW/AR1plE8=";

/* 
 * Resource generated for file:
 *    error_mini.png (zlib, base64) (image file)
 */
static const unsigned int  image_error_mini_width          = 16;
static const unsigned int  image_error_mini_height         = 16;
static const unsigned int  image_error_mini_pixel_size     = 4;
static const unsigned long image_error_mini_length         = 64;
static const unsigned long image_error_mini_decoded_length = 1024;

static const unsigned char image_error_mini[] = 
  "eNr7//8/w38aYCD4j4wHWv/MmTP/j+qnn34QBulBxv9plNaGY/qhV/gBAHZtokc=";

/* 
 * Resource generated for file:
 *    error_red_mini.png (zlib, base64) (image file)
 */
static const unsigned int  image_error_red_mini_width          = 16;
static const unsigned int  image_error_red_mini_height         = 16;
static const unsigned int  image_error_red_mini_pixel_size     = 4;
static const unsigned long image_error_red_mini_length         = 64;
static const unsigned long image_error_red_mini_decoded_length = 1024;

static const unsigned char image_error_red_mini[] = 
  "eNr7//8/w39aYAYghYwHWP/MmTP/j+qno34gBulBxv9pldaGY/qhU/gBAPFTzhs=";

/* 
 * Resource generated for file:
 *    expand.png (zlib, base64) (image file)
 */
static const unsigned int  image_expand_width          = 10;
static const unsigned int  image_expand_height         = 10;
static const unsigned int  image_expand_pixel_size     = 4;
static const unsigned long image_expand_length         = 80;
static const unsigned long image_expand_decoded_length = 400;

static const unsigned char image_expand[] = 
  "eNr7//8/w/8BwkCQDcTbgfg4GgaJZaOpnQ3Er4H4NxSD2LNxmDsfiN9D8XwCbpgOwv8HMB"
  "yQMQCeof29";

/* 
 * Resource generated for file:
 *    file_open.png (zlib, base64) (image file)
 */
static const unsigned int  image_file_open_width          = 16;
static const unsigned int  image_file_open_height         = 13;
static const unsigned int  image_file_open_pixel_size     = 4;
static const unsigned long image_file_open_length         = 116;
static const unsigned long image_file_open_decoded_length = 832;

static const unsigned char image_file_open[] = 
  "eNqdUoEJACAM6vj9bVEEJrGWgbgo3dYC0FDAWNhAUcPa297x0nqc+vTu4hlP5nNF0t/hce"
  "Nc//b4yZ/1UOk/4g2Zo6Xl/I6W5+lo9T9U3ovRAZBf4XI=";

/* 
 * Resource generated for file:
 *    floppy.png (zlib, base64) (image file)
 */
static const unsigned int  image_floppy_width          = 16;
static const unsigned int  image_floppy_height         = 16;
static const unsigned int  image_floppy_pixel_size     = 4;
static const unsigned long image_floppy_length         = 92;
static const unsigned long image_floppy_decoded_length = 1024;

static const unsigned char image_floppy[] = 
  "eNpjZGRkYKQQA8F/cjCy/oYGiNiBAwfwYpgaSvRTaj9M7XDTDxPHh5HVYdNPLCbXfnLdT6"
  "n/aamf3DzESIW8CwAU7WTE";

/* 
 * Resource generated for file:
 *    folder.png (zlib, base64) (image file)
 */
static const unsigned int  image_folder_width          = 15;
static const unsigned int  image_folder_height         = 13;
static const unsigned int  image_folder_pixel_size     = 4;
static const unsigned long image_folder_length         = 104;
static const unsigned long image_folder_decoded_length = 780;

static const unsigned char image_folder[] = 
  "eNrtj9sJACAMAzN6R+tm0YhiEXx+KxxB9BolCWbMjCOsZyt0z93LNubOn3nRn72p5tWSA4"
  "DdxbI/5uieeu0fv/e+9wW5GqR8IQFckCHj";

/* 
 * Resource generated for file:
 *    folder_open.png (zlib, base64) (image file)
 */
static const unsigned int  image_folder_open_width          = 16;
static const unsigned int  image_folder_open_height         = 13;
static const unsigned int  image_folder_open_pixel_size     = 4;
static const unsigned long image_folder_open_length         = 140;
static const unsigned long image_folder_open_decoded_length = 832;

static const unsigned char image_folder_open[] = 
  "eNqdkAEOwCAIA3k6T+NnXVwkYx0qjKQhQa9UAQimVBUshPOd5t1PVTycNbN79O5Aliv6Pr"
  "tl6ZHlEhGc9+fd+VW2nYZH5KvlDO/vZI+Z/7w/7mW++nfMdz2cZb4j5od81tEF64k9/Q==";

/* 
 * Resource generated for file:
 *    grid_linear.png (zlib, base64) (image file)
 */
static const unsigned int  image_grid_linear_width          = 16;
static const unsigned int  image_grid_linear_height         = 16;
static const unsigned int  image_grid_linear_pixel_size     = 4;
static const unsigned long image_grid_linear_length         = 44;
static const unsigned long image_grid_linear_decoded_length = 1024;

static const unsigned char image_grid_linear[] = 
  "eNpjYGD4z0AhBgIQwUAOmxr6R90/6v5R95OHATizxkg=";

/* 
 * Resource generated for file:
 *    grid_log.png (zlib, base64) (image file)
 */
static const unsigned int  image_grid_log_width          = 16;
static const unsigned int  image_grid_log_height         = 16;
static const unsigned int  image_grid_log_pixel_size     = 4;
static const unsigned long image_grid_log_length         = 52;
static const unsigned long image_grid_log_decoded_length = 1024;

static const unsigned char image_grid_log[] = 
  "eNpjYGD4z0AhBgIGdIwujsyHsRmoYDc17B/p7h/1/8gNfwDY0fAe";

/* 
 * Resource generated for file:
 *    header_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_header_annotation_width          = 16;
static const unsigned int  image_header_annotation_height         = 16;
static const unsigned int  image_header_annotation_pixel_size     = 3;
static const unsigned long image_header_annotation_length         = 44;
static const unsigned long image_header_annotation_decoded_length = 768;

static const unsigned char image_header_annotation[] = 
  "eNpjYCAZPH1wg0iEqR6/aZSrJ9U9o+qHn3qSAAADH+Do";

/* 
 * Resource generated for file:
 *    helpbubble.png (zlib, base64) (image file)
 */
static const unsigned int  image_helpbubble_width          = 16;
static const unsigned int  image_helpbubble_height         = 16;
static const unsigned int  image_helpbubble_pixel_size     = 4;
static const unsigned long image_helpbubble_length         = 180;
static const unsigned long image_helpbubble_decoded_length = 1024;

static const unsigned char image_helpbubble[] = 
  "eNqVk10OwCAIgz06R/NmzAbJkDktJn1Q+drgj6q23nsbQ1mhHtLBQlhbx3nuHl+e80G9iO"
  "hfvvUjGjXzLrz7G4N59GLzUTv3kx+XH/tF3evH5Xuds7H3Sn7uo5p/un82P55bPb9R+bf3"
  "Hzln4/v1/4D1ncxjZXf8SfnvVXn3yPwDgkCsMw==";

/* 
 * Resource generated for file:
 *    info_mini.png (zlib, base64) (image file)
 */
static const unsigned int  image_info_mini_width          = 16;
static const unsigned int  image_info_mini_height         = 16;
static const unsigned int  image_info_mini_pixel_size     = 4;
static const unsigned long image_info_mini_length         = 264;
static const unsigned long image_info_mini_decoded_length = 1024;

static const unsigned char image_info_mini[] = 
  "eNr7//8/w38qY0F+/v8MDAxwDOITo8/IwBis3iYq6r951kI4BvFB4iB5fHql9Az+u7ef/m"
  "/XcvZ/9YaXILVgGsQHiYPkcZkBMh+kxnfCDbD6xJlX/k/b/RhMg/ggcZA8SB26Xnl5ebAb"
  "YWpANAzA9MPEQepA6tHtBvkTph+kHmQ3CBQuuY2iH6QO3Q3Y9IP00Us/uv9hYQcCsDDE53"
  "/k8AfZhQ3A3IAt/NHjH+RmULzDMIhPKP4pTX/40j8uNw8GDADD16Au";

/* 
 * Resource generated for file:
 *    lock.png (zlib, base64) (image file)
 */
static const unsigned int  image_lock_width          = 14;
static const unsigned int  image_lock_height         = 14;
static const unsigned int  image_lock_pixel_size     = 4;
static const unsigned long image_lock_length         = 140;
static const unsigned long image_lock_decoded_length = 784;

static const unsigned char image_lock[] = 
  "eNrVkjEOgDAIRZm9dK/GWbyFE8rwmyeJlsTJJm8o8EpLGhHm7pMxRpjZJPfMJ3E5AU91XD"
  "rnycu8HPaTy770VKOegvGOx7vX+FeP71lRvWPflmDeN4/zqLx5f+jXhR7/WYd0TrUDvkA=";

/* 
 * Resource generated for file:
 *    mag_glass.png (zlib, base64) (image file)
 */
static const unsigned int  image_mag_glass_width          = 16;
static const unsigned int  image_mag_glass_height         = 16;
static const unsigned int  image_mag_glass_pixel_size     = 4;
static const unsigned long image_mag_glass_length         = 152;
static const unsigned long image_mag_glass_decoded_length = 1024;

static const unsigned char image_mag_glass[] = 
  "eNrFk9EJADEIQ2//DdwmA7hT7zwIhFJU/KkQKNXXWGkBPEj0xVKhqKfM7K9396URe5FDw5"
  "cR9aHuGeqrHCNy2V3ofWI7PWjvU36f26n/agaVd+ZPaR/q22H5Bva9Lss6ridsR7dY/ScT"
  "Vs+Ysi/t7mza";

/* 
 * Resource generated for file:
 *    minus.png (zlib, base64) (image file)
 */
static const unsigned int  image_minus_width          = 12;
static const unsigned int  image_minus_height         = 12;
static const unsigned int  image_minus_pixel_size     = 4;
static const unsigned long image_minus_length         = 152;
static const unsigned long image_minus_decoded_length = 576;

static const unsigned char image_minus[] = 
  "eNr7//8/w38gBgJZIC4A4u1A/AqKt0PFZP9D1UHVWgLxBiDuB2IPIBaFYg+oGEjOEslcEN"
  "8f2Qw08/yhamBu6MelFklPP5J7PYhQ74HkN1Go2H9sGConihQOpKgn1T2k+pek8CQ1vv6T"
  "mB4APezfsQ==";

/* 
 * Resource generated for file:
 *    move.png (zlib, base64) (image file)
 */
static const unsigned int  image_move_width          = 9;
static const unsigned int  image_move_height         = 9;
static const unsigned int  image_move_pixel_size     = 4;
static const unsigned long image_move_length         = 56;
static const unsigned long image_move_decoded_length = 324;

static const unsigned char image_move[] = 
  "eNr7//8/w38kDAT//6OJocvDMCF1hMSxsZHNx4WJMec/Hf0FAD65wEA=";

/* 
 * Resource generated for file:
 *    move_h.png (zlib, base64) (image file)
 */
static const unsigned int  image_move_h_width          = 9;
static const unsigned int  image_move_h_height         = 9;
static const unsigned int  image_move_h_pixel_size     = 4;
static const unsigned long image_move_h_length         = 40;
static const unsigned long image_move_h_decoded_length = 324;

static const unsigned char image_move_h[] = 
  "eNr7//8/w386YiD4j08cRBPCxJhDTwwAkDrYKA==";

/* 
 * Resource generated for file:
 *    move_v.png (zlib, base64) (image file)
 */
static const unsigned int  image_move_v_width          = 9;
static const unsigned int  image_move_v_height         = 9;
static const unsigned int  image_move_v_pixel_size     = 4;
static const unsigned long image_move_v_length         = 48;
static const unsigned long image_move_v_decoded_length = 324;

static const unsigned char image_move_v[] = 
  "eNr7//8/w38kDAT//6OJocvDMCF1+OQHmxpK/AUAkDrYKA==";

/* 
 * Resource generated for file:
 *    orientation_cube_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_orientation_cube_annotation_width          = 16;
static const unsigned int  image_orientation_cube_annotation_height         = 16;
static const unsigned int  image_orientation_cube_annotation_pixel_size     = 3;
static const unsigned long image_orientation_cube_annotation_length         = 76;
static const unsigned long image_orientation_cube_annotation_decoded_length = 768;

static const unsigned char image_orientation_cube_annotation[] = 
  "eNpjYCAZPH1wg0g0qh5TFjkY8agHiv9/dxKCCKpHMxmiBb96ZMNprR6uhkj3Uxj+JAEAGi"
  "K4jw==";

/* 
 * Resource generated for file:
 *    pan_hand.png (zlib, base64) (image file)
 */
static const unsigned int  image_pan_hand_width          = 16;
static const unsigned int  image_pan_hand_height         = 16;
static const unsigned int  image_pan_hand_pixel_size     = 4;
static const unsigned long image_pan_hand_length         = 128;
static const unsigned long image_pan_hand_decoded_length = 1024;

static const unsigned char image_pan_hand[] = 
  "eNqtk1cKACAMQ73/paOCHxI7orXgKCRP6wDQ4LQRswMCjadfI1aCjGXp95zmZb/FSTwHmx"
  "mq39oLnZEbWZ2Zn1msV/177Tdrevd4y6j4lTf04lcYyn+KGL/9HQMYBiU=";

/* 
 * Resource generated for file:
 *    plus.png (zlib, base64) (image file)
 */
static const unsigned int  image_plus_width          = 12;
static const unsigned int  image_plus_height         = 12;
static const unsigned int  image_plus_pixel_size     = 4;
static const unsigned long image_plus_length         = 152;
static const unsigned long image_plus_decoded_length = 576;

static const unsigned char image_plus[] = 
  "eNqVktEJwDAIRLtEPjNINvAvq3QTV+lfduoS1wtcQPyqgRdQj0NNAFwgPJ3cZJFXLOU6pJ"
  "N2kIc4MdKEKbdrI/jueEaP5DelOT14qu8LKeehX/uhtzBbi7qMai3soaKv9lOdt7TP6nuh"
  "+B8+Lv7PwQ==";

/* 
 * Resource generated for file:
 *    point_finger.png (zlib, base64) (image file)
 */
static const unsigned int  image_point_finger_width          = 14;
static const unsigned int  image_point_finger_height         = 8;
static const unsigned int  image_point_finger_pixel_size     = 4;
static const unsigned long image_point_finger_length         = 96;
static const unsigned long image_point_finger_decoded_length = 448;

static const unsigned char image_point_finger[] = 
  "eNp90FEKACAIA1CP7s1XEcFaW8J+tBcigIJkFlz2rLHiTKrtX3fM395OTbLsklHLe8ob+4"
  "e7C/fCzubebftkK2UAQlwdeA==";

/* 
 * Resource generated for file:
 *    preset_add.png (zlib, base64) (image file)
 */
static const unsigned int  image_preset_add_width          = 16;
static const unsigned int  image_preset_add_height         = 16;
static const unsigned int  image_preset_add_pixel_size     = 4;
static const unsigned long image_preset_add_length         = 144;
static const unsigned long image_preset_add_decoded_length = 1024;

static const unsigned char image_preset_add[] = 
  "eNq1ktkNgDAMQxk967EMK5hDIJngHEUQyepH8lwrLYAJpK0QCW5W6Zx7VNcj4+l8zVceVf"
  "5qHxGv/MwMX/Ei2zCPZT40wt/uJL7KwfzFeVHGX/ksv5/xvWh/e6+j6P3U3+hIeWdaAbSb"
  "xmo=";

/* 
 * Resource generated for file:
 *    preset_apply.png (zlib, base64) (image file)
 */
static const unsigned int  image_preset_apply_width          = 16;
static const unsigned int  image_preset_apply_height         = 16;
static const unsigned int  image_preset_apply_pixel_size     = 4;
static const unsigned long image_preset_apply_length         = 140;
static const unsigned long image_preset_apply_decoded_length = 1024;

static const unsigned char image_preset_apply[] = 
  "eNq1k1EKwDAIQ3d0r7fL9ArZGBMk05oWJgR/8mwUCuBA0F2oBPJmen2fUmfM+NC3+W5Gl7"
  "+7R8Vn88wMf/Ih5zL/9HFGn8w7q/B8y8iqvPtUNuNZnC+T71+9q4j3ru6q/r1V9gI8q8Sz";

/* 
 * Resource generated for file:
 *    preset_delete.png (zlib, base64) (image file)
 */
static const unsigned int  image_preset_delete_width          = 16;
static const unsigned int  image_preset_delete_height         = 16;
static const unsigned int  image_preset_delete_pixel_size     = 4;
static const unsigned long image_preset_delete_length         = 144;
static const unsigned long image_preset_delete_decoded_length = 1024;

static const unsigned char image_preset_delete[] = 
  "eNqlk9ENACEIQx2d9VzmVqiamAvhLCUeScMPryAqgAanGWBCqD1p132i6pHxLl/zykPNr/"
  "bB+JOfmYHtvspn95V5eR5Pp3JzXvfP+Hdnu1fMq+Yvr/aveHGu0vxM6s/FN8KkejANjY6W"
  "aQ==";

/* 
 * Resource generated for file:
 *    preset_update.png (zlib, base64) (image file)
 */
static const unsigned int  image_preset_update_width          = 16;
static const unsigned int  image_preset_update_height         = 16;
static const unsigned int  image_preset_update_pixel_size     = 4;
static const unsigned long image_preset_update_length         = 144;
static const unsigned long image_preset_update_decoded_length = 1024;

static const unsigned char image_preset_update[] = 
  "eNqtUoEJwCAM8/S+5zN7IRuiUDRNRVcIIiRNrAVQ4PAVImDiMnTeUrs9lN6dx/qsR5Y/m0"
  "ekZ/3MDLd69V8up/RvnKdSOJ7MH/XY0d/4K+2f/myu6v3jPngRlr8nnmxP1P7OuZT+BdoR"
  "yLk=";

/* 
 * Resource generated for file:
 *    question.png (zlib, base64) (image file)
 */
static const unsigned int  image_question_width          = 32;
static const unsigned int  image_question_height         = 32;
static const unsigned int  image_question_pixel_size     = 4;
static const unsigned long image_question_length         = 828;
static const unsigned long image_question_decoded_length = 4096;

static const unsigned char image_question[] = 
  "eNrNl88rrFEYx+8fYHNTFDYmU1eJpLCZGndu3YVLNhKrScnGyOZ2b4p0y2uhK03vjZAfY2"
  "GDEGWDKRullGblLgylZKHp3ibJ5nGe03umN87znnNmhubUNzW9ns/58X2e8xwA+AAa6vm+"
  "4WcKM9lMcaZrpkdH185vtvONHzTjanCDTFGmKybQ1JXzP8EcuD4mi+nWgPtSt04MnyE7xL"
  "SbA/elMFZIk93OdJpHthDGbNdYtyd7aGIf1vYScHByCYm/dxntHF3AwvoZ9I3uqOYQ8jhv"
  "cs8xLnJU49//NMwubUPnYMzrLHwSvuXFTj88gcmIrcSg+UsvNQdLkmOkz3XWLRtjIyNQ2/"
  "iNyougix+l2NbcMWQ7UqkUfCwqovYh6qprZG1Bn8lGMpmEjrY2Hh//UqMlEICS4lKqRoma"
  "Snr27j4tjTsUiXB2eVklfO3+RZ6RmGNr+LcsvqjnJB9zCvPtjz0D8cNDLhyVFRU8bqA1wn"
  "OS8qf4DucoiS/uEmX9QA6eY0OgC6o+NXFh3B+TeyR7a3OTf4Mi8lHcY1nVNNwbio3eE2uv"
  "rvtMxRB3aFZsaqA30XfIRu951KLHbPheOYn+EOtGbxK+c/ON9//k/EbKXl5czJw3+sNj3e"
  "79j5vyqT0XbKLmUf6z88GfnprK7LlBLFtVf6jzR2FtEaqvqeF8zE+DWGFV/ZXdhYL/c3w1"
  "o4HheV5jOvpnTXpEv+r+0fX+WeKS8xV+f3X/6Ny/OnzMO+F7zb40qNt/vAHfMu2/8sjfpf"
  "pxnf4zR/6pqg/X7b/Ra6LeuOXBV/bfhfD+KIT3VyG8P9/r/f0MsXi9Bw==";

/* 
 * Resource generated for file:
 *    reload.png (zlib, base64) (image file)
 */
static const unsigned int  image_reload_width          = 14;
static const unsigned int  image_reload_height         = 14;
static const unsigned int  image_reload_pixel_size     = 4;
static const unsigned long image_reload_length         = 72;
static const unsigned long image_reload_decoded_length = 784;

static const unsigned char image_reload[] = 
  "eNr7//8/w38aYCD4j4zJ0YdPnlT78InjkifW7ejqhoo+YvQSE97kxiEuTKu0RQwGAB5r2j"
  "Q=";

/* 
 * Resource generated for file:
 *    rotate.png (zlib, base64) (image file)
 */
static const unsigned int  image_rotate_width          = 14;
static const unsigned int  image_rotate_height         = 14;
static const unsigned int  image_rotate_pixel_size     = 4;
static const unsigned long image_rotate_length         = 100;
static const unsigned long image_rotate_decoded_length = 784;

static const unsigned char image_rotate[] = 
  "eNr7//8/w38KMBD8R8bk6qWWfYTEZ86cSVAenxg+N5GrD6aO3vrwhRc2Pdj04QtLXG5Cj0"
  "N0jG4XMobFHzb1hPwP049PPQCDS9XS";

/* 
 * Resource generated for file:
 *    scale_bar_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_scale_bar_annotation_width          = 16;
static const unsigned int  image_scale_bar_annotation_height         = 16;
static const unsigned int  image_scale_bar_annotation_pixel_size     = 3;
static const unsigned long image_scale_bar_annotation_length         = 52;
static const unsigned long image_scale_bar_annotation_decoded_length = 768;

static const unsigned char image_scale_bar_annotation[] = 
  "eNpjYCAZPH1wg0g0ktUja8TDRlOPi8SqnmAEjczwJwkAAKI609c=";

/* 
 * Resource generated for file:
 *    shrink.png (zlib, base64) (image file)
 */
static const unsigned int  image_shrink_width          = 10;
static const unsigned int  image_shrink_height         = 10;
static const unsigned int  image_shrink_pixel_size     = 4;
static const unsigned long image_shrink_length         = 56;
static const unsigned long image_shrink_decoded_length = 400;

static const unsigned char image_shrink[] = 
  "eNr7//8/w38iMRCAiP/EiKOL4dKLLIdPDTHmkGIeMe4jxb/UwgDCL/cJ";

/* 
 * Resource generated for file:
 *    side_annotation.png (zlib, base64) (image file)
 */
static const unsigned int  image_side_annotation_width          = 16;
static const unsigned int  image_side_annotation_height         = 16;
static const unsigned int  image_side_annotation_pixel_size     = 3;
static const unsigned long image_side_annotation_length         = 60;
static const unsigned long image_side_annotation_decoded_length = 768;

static const unsigned char image_side_annotation[] = 
  "eNpjYCAZPH1wg0iEVT0eo6iinlT30EI9LjejiZOtfrD5d1DFL0kAAOVLrKQ=";

/* 
 * Resource generated for file:
 *    stopwatch.png (zlib, base64) (image file)
 */
static const unsigned int  image_stopwatch_width          = 16;
static const unsigned int  image_stopwatch_height         = 16;
static const unsigned int  image_stopwatch_pixel_size     = 4;
static const unsigned long image_stopwatch_length         = 220;
static const unsigned long image_stopwatch_decoded_length = 1024;

static const unsigned char image_stopwatch[] = 
  "eNqlk90NgCAMhFnIhHEYwmmcgFe36GbVEk/PCip6SaPRfsfxJyJBXK1SXzFGlUpvjYOMgZ"
  "58wBVmGI6y7xsHH+/BY+o4alXUY/2eL/7M2vs2fkpJOR9n2MeepmoGZs9xguac9cKTT4sF"
  "j/8l+zxf+BbLc9h7wOP5oODWpVeY/ylDB8tn4AvP+9+ToXWG4XG3X2/uEfv4+2fr7fvN+k"
  "8tCF7Dnw==";

/* 
 * Resource generated for file:
 *    transport_beginning.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_beginning_width          = 16;
static const unsigned int  image_transport_beginning_height         = 16;
static const unsigned int  image_transport_beginning_pixel_size     = 4;
static const unsigned long image_transport_beginning_length         = 188;
static const unsigned long image_transport_beginning_decoded_length = 1024;

static const unsigned char image_transport_beginning[] = 
  "eNrN00ENgDAMBdBZmAUsYAELWJiFWcBCPaFlFkq30KQpLesNlpTDz94nwEDEhB8PrX7Z0n"
  "MVJ19p6sQXJ++2TTxbnbPFFy+tzKX1vLaca2v5w7B4dzYj136nOZ37Q8D3fdno4OeHgE9G"
  "h3z/EPC6Q39/CHjZYZ0/UL7SLMa+7OTjPOAP/r0LX11aGw==";

/* 
 * Resource generated for file:
 *    transport_end.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_end_width          = 16;
static const unsigned int  image_transport_end_height         = 16;
static const unsigned int  image_transport_end_pixel_size     = 4;
static const unsigned long image_transport_end_length         = 184;
static const unsigned long image_transport_end_decoded_length = 1024;

static const unsigned char image_transport_end[] = 
  "eNrNk1sNgDAMRWcBC1jAAhawMAuzgIV6QgsWykbSpCn3bvxBk/vT7Jy9VTXpx6lVapb0rL"
  "Umk74G/gSOexxwIF6Bw/joYHx0eN47erx3RN4cI94cGfRb9he8kPmPmm3AC9l/Y6fB+oWc"
  "v7G98xNy/55lvJB3FlnEo7fbagas9Yv+4O9dh3laGw==";

/* 
 * Resource generated for file:
 *    transport_fast_forward.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_fast_forward_width          = 16;
static const unsigned int  image_transport_fast_forward_height         = 16;
static const unsigned int  image_transport_fast_forward_pixel_size     = 4;
static const unsigned long image_transport_fast_forward_length         = 168;
static const unsigned long image_transport_fast_forward_decoded_length = 1024;

static const unsigned char image_transport_fast_forward[] = 
  "eNrNk9ENwCAIBV2hs7gTs3SFzsIKztIVXkmjhhIofkrCz0vuoogACjZoqav4dUofNnR4BA"
  "6WbtYR8J6De/5x/PDWwSqfjoTXDjb561jg0WfHTt4W+Fu6Bjwl/GC981Nyf81anpL5W1bz"
  "lLy/xw6eFvavBvvr5tjk3z2gxJNI";

/* 
 * Resource generated for file:
 *    transport_fast_forward_to_key.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_fast_forward_to_key_width          = 16;
static const unsigned int  image_transport_fast_forward_to_key_height         = 16;
static const unsigned int  image_transport_fast_forward_to_key_pixel_size     = 4;
static const unsigned long image_transport_fast_forward_to_key_length         = 204;
static const unsigned long image_transport_fast_forward_to_key_decoded_length = 1024;

static const unsigned char image_transport_fast_forward_to_key[] = 
  "eNrFk9ENgCAMRF3BFVyBFZiFFVjBFZylKzCLK9RqijmbIho/JLmEXvIuhQIzD/yzZC2Dv2"
  "bRaE2H50YGiYrNaPBeBql/ybjhbQaBf2Z0eMwg4x8ZD3jWuyPHLw/4VRQafOrwlfX6T53z"
  "I2v5nZ1EERSAtyzySets+iHlPbbyCeoMDO5D4/1aP+sMo86YXv4Tt/8v2gD0ZHI2";

/* 
 * Resource generated for file:
 *    transport_loop.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_loop_width          = 16;
static const unsigned int  image_transport_loop_height         = 16;
static const unsigned int  image_transport_loop_pixel_size     = 4;
static const unsigned long image_transport_loop_length         = 212;
static const unsigned long image_transport_loop_decoded_length = 1024;

static const unsigned char image_transport_loop[] = 
  "eNrNk1ENwCAMRLGAhVnAAhZmYRZqYRawMAtYwMIszEJHk9tCSMuWfe2S+6H3CLTAzI5/4K"
  "pQTY2jG0jhRUlKMH3g5wd+QUbjffUBLhk8IeMVXmp7kw3G0SVDCp+rV/csyWSDpxc8Gbzc"
  "eXvBSyYp/NX7acBOyMzG/OQOBbPo5VHLg/lfmQN9ivCKtXtvg297VJq3VPre8k/+3QkrPX"
  "VE";

/* 
 * Resource generated for file:
 *    transport_pause.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_pause_width          = 16;
static const unsigned int  image_transport_pause_height         = 16;
static const unsigned int  image_transport_pause_pixel_size     = 4;
static const unsigned long image_transport_pause_length         = 80;
static const unsigned long image_transport_pause_decoded_length = 1024;

static const unsigned char image_transport_pause[] = 
  "eNr7//8/w/9BhIFAEIhdoBgGjKF8JSL0g9T9h2IY2A3ll4/qH9VPY/1KIHVQDANpUL7L/0"
  "GW3wCFUQ4g";

/* 
 * Resource generated for file:
 *    transport_play.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_play_width          = 16;
static const unsigned int  image_transport_play_height         = 16;
static const unsigned int  image_transport_play_pixel_size     = 4;
static const unsigned long image_transport_play_length         = 108;
static const unsigned long image_transport_play_decoded_length = 1024;

static const unsigned char image_transport_play[] = 
  "eNr7//8/w/9BhoFACYgFGbAAIvW7APEZbGaQoP8/NjNI1I9hBhn6UcwgUz/cDAr0g3AoBf"
  "rTKHB/GgXhl0ZB/KVRkH7SyE2/9MQARXskxQ==";

/* 
 * Resource generated for file:
 *    transport_play_to_key.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_play_to_key_width          = 16;
static const unsigned int  image_transport_play_to_key_height         = 16;
static const unsigned int  image_transport_play_to_key_pixel_size     = 4;
static const unsigned long image_transport_play_to_key_length         = 152;
static const unsigned long image_transport_play_to_key_decoded_length = 1024;

static const unsigned char image_transport_play_to_key[] = 
  "eNr7//8/w/9BhIFACYgFGbAAIvW7APEZbGaQoP8/NjNI1I9hBhn6UcwgUz/cDAr0g3AoBf"
  "rTKHB/GjT4lKByMGxMhP40pOgrRzN3NwH9aWjJpxymB5mNQ38aluRbDo0HkPxMXPrxhAtR"
  "7icVAwABaPew";

/* 
 * Resource generated for file:
 *    transport_rewind.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_rewind_width          = 16;
static const unsigned int  image_transport_rewind_height         = 16;
static const unsigned int  image_transport_rewind_pixel_size     = 4;
static const unsigned long image_transport_rewind_length         = 172;
static const unsigned long image_transport_rewind_decoded_length = 1024;

static const unsigned char image_transport_rewind[] = 
  "eNrN09ENgCAMBFBWcBZ36iys0FlcwVlcoVZjTa1X4BOS+7nwGgMoIkUmCFiLpha8uOMvu2"
  "s2ZDXS8GYFeH76zHsbPbse+Wi959AjH635CnrkKfGr5hjwaIZ9/29G4/wpOb/PjM79U3J/"
  "74yB90fJ+7lnDHjbC3uZ5L87AbKWk0g=";

/* 
 * Resource generated for file:
 *    transport_rewind_to_key.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_rewind_to_key_width          = 16;
static const unsigned int  image_transport_rewind_to_key_height         = 16;
static const unsigned int  image_transport_rewind_to_key_pixel_size     = 4;
static const unsigned long image_transport_rewind_to_key_length         = 196;
static const unsigned long image_transport_rewind_to_key_decoded_length = 1024;

static const unsigned char image_transport_rewind_to_key[] = 
  "eNrN010NgDAMBGAsYAELs4CWWcACFqoFC2jBQilkI0e5Ak+EJpeQhq9r+FHVRn8SUq1lbH"
  "jJg9/sbJmYteiNr1aJl9KPPFrvBfrMe4teXJ95b6sfSZ/5HPhkWV54NqPuf5lRTGfpIcnN"
  "wOd3mlH8EJyXg/d3zAA/4TXcm4PvZ58BZi67S3AeqxTt/+U/twKGyXI2";

/* 
 * Resource generated for file:
 *    transport_stop.png (zlib, base64) (image file)
 */
static const unsigned int  image_transport_stop_width          = 16;
static const unsigned int  image_transport_stop_height         = 16;
static const unsigned int  image_transport_stop_pixel_size     = 4;
static const unsigned long image_transport_stop_length         = 64;
static const unsigned long image_transport_stop_decoded_length = 1024;

static const unsigned char image_transport_stop[] = 
  "eNr7//8/w/9hgoFACYhdCGAlPPrLgfg/AVw+qn/Y6k8D4t0EcNr/YZRnAMXDIoM=";

/* 
 * Resource generated for file:
 *    trashcan.png (zlib, base64) (image file)
 */
static const unsigned int  image_trashcan_width          = 9;
static const unsigned int  image_trashcan_height         = 10;
static const unsigned int  image_trashcan_pixel_size     = 4;
static const unsigned long image_trashcan_length         = 52;
static const unsigned long image_trashcan_decoded_length = 360;

static const unsigned char image_trashcan[] = 
  "eNr7//8/w38oBoL/MPwfhzg+jA+QYAYDzCwcNMNgVYPPTwBvR79B";

/* 
 * Resource generated for file:
 *    tree_close.png (zlib, base64) (image file)
 */
static const unsigned int  image_tree_close_width          = 16;
static const unsigned int  image_tree_close_height         = 16;
static const unsigned int  image_tree_close_pixel_size     = 4;
static const unsigned long image_tree_close_length         = 84;
static const unsigned long image_tree_close_decoded_length = 1024;

static const unsigned char image_tree_close[] = 
  "eNr7//8/g7u7+39cGAgYCGGoOgxAjn4GBgay9YP0wjBMP8wMXDQ17afE/5SEPxa3MNATj6"
  "afoZt+AD+9zuo=";

/* 
 * Resource generated for file:
 *    tree_open.png (zlib, base64) (image file)
 */
static const unsigned int  image_tree_open_width          = 16;
static const unsigned int  image_tree_open_height         = 16;
static const unsigned int  image_tree_open_pixel_size     = 4;
static const unsigned long image_tree_open_length         = 80;
static const unsigned long image_tree_open_decoded_length = 1024;

static const unsigned char image_tree_open[] = 
  "eNr7//8/g7u7+39cGAgYCGGoOgxAb/0MDAxwDNMPMwMXTW/34xKjJPwJmU+KPKV41P6hZT"
  "8Asfff0A==";

/* 
 * Resource generated for file:
 *    warning.png (zlib, base64) (image file)
 */
static const unsigned int  image_warning_width          = 32;
static const unsigned int  image_warning_height         = 32;
static const unsigned int  image_warning_pixel_size     = 4;
static const unsigned long image_warning_length         = 628;
static const unsigned long image_warning_decoded_length = 4096;

static const unsigned char image_warning[] = 
  "eNrNl79Lw0AUgP0Durh3sMGCg5uDLkIhglNLFxGdiiBOioviUpGCcZGioYI42sHJpVQQhB"
  "pwEQp16KRTOoUObg7dnnchF0J+vncJ2oNvadL35S55794BwAwg2D56LDJqjBbDYIwYE4eR"
  "81vLuacIyLgIb4mhM0wGIDGd/5RSeBWGxrAIXj+WE0MhulVGN4XXD4+lIt0VRj9Dt4DHrC"
  "DmHes+vHiGh6dhKPwa4hnUmPeduOba3RtEDX4N+S6UEL+GWcc4//7xJays7UB17zYpjhaS"
  "Y1Zaf7VchtlcDta3Gpi8KHn8OvY7ysjP0T11zfwHv+mpqeg84t941Cjk8xQ/eOo5KZejBn"
  "cT/WIvIfnH3z8B98dg4Po3Du6xscQ+RvIPv8YBv9HruX5CLLGHkvyd18+A/6rZtN1zhUVK"
  "rImMn9da/zir123//MIy1U9e/+v2e2TuLa1uUtef/P2F1QDh5/WXEMuQyb/d004WuQeefg"
  "1k1uDkvG3PW8xdwl+j1l/h5jRuXly/eAZij1ik7j9x9Y84d11m/03yI5/B8vfG2P4DU/8R"
  "MTTZ/isDfzeqH8f0nyn9/aQ+nNJ/C58f2f6bev4g+NHnj2k4f03D+fOvzt+/1KxzrQ==";

/* 
 * Resource generated for file:
 *    warning_mini.png (zlib, base64) (image file)
 */
static const unsigned int  image_warning_mini_width          = 16;
static const unsigned int  image_warning_mini_height         = 16;
static const unsigned int  image_warning_mini_pixel_size     = 4;
static const unsigned long image_warning_mini_length         = 264;
static const unsigned long image_warning_mini_decoded_length = 1024;

static const unsigned char image_warning_mini[] = 
  "eNrFkrENhDAMRV3R0FBdiTIB9a3CVCzDEgzAGvRUPj0s31lWQBRIV1iWf/6P/Z2oqujD0b"
  "YvFZFvUN/RDcP74E+T6LKIzrNlanDOr7TjKLptBqEtxTI1OOdnd3D/vv+4TWM9yX4n52BZ"
  "2/flmDHqu8645KiHBz/3xqdD8OP+XE/AyzNQu0/vg1dwss/le6npY39iXW1/5IjX+mf/7o"
  "FecfYz/3n/cc7sq7b/2vu7hzjP1fvX/h/6u//vif//j/gAd0GI4Q==";

/* 
 * Resource generated for file:
 *    window_level.png (zlib, base64) (image file)
 */
static const unsigned int  image_window_level_width          = 16;
static const unsigned int  image_window_level_height         = 16;
static const unsigned int  image_window_level_pixel_size     = 4;
static const unsigned long image_window_level_length         = 172;
static const unsigned long image_window_level_decoded_length = 1024;

static const unsigned char image_window_level[] = 
  "eNrl0cENhCAQQFFLwCakDxqRTrANTlSCdWATSwlfcT0YJcZZ9ybJv/GSmQzQcLPlsY8Htu"
  "S9R2LL/2MSK5n3LfbJXWr+l5mlN/1ntf3vVvOqVWitMcZgbY8bHCEEYhxJUyJ/Mhtcdz15"
  "1aK7r++txblh85GUFp+vvaTiZ84zZrU=";

