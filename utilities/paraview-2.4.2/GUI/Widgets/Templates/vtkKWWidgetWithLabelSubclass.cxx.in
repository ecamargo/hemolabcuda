/*=========================================================================

  Module:    $RCSfile: vtkKWWidgetWithLabelSubclass.cxx.in,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "@WidgetType@WithLabel.h"

#include "vtkKWLabel.h"
#include "vtkObjectFactory.h"
#include "@WidgetType@.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(@WidgetType@WithLabel);
vtkCxxRevisionMacro(@WidgetType@WithLabel, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
@WidgetType@WithLabel::@WidgetType@WithLabel()
{
  this->ExpandWidget    = 1;
  this->Widget          = @WidgetType@::New();
}

//----------------------------------------------------------------------------
@WidgetType@WithLabel::~@WidgetType@WithLabel()
{
  if (this->Widget)
    {
    this->Widget->Delete();
    this->Widget = NULL;
    }
}

//----------------------------------------------------------------------------
@WidgetType@* @WidgetType@WithLabel::GetWidget()
{
  return this->Widget;
}

//----------------------------------------------------------------------------
void @WidgetType@WithLabel::Create(vtkKWApplication *app)
{
  // Check if already created

  if (this->IsCreated())
    {
    vtkErrorMacro("@WidgetType@WithLabel already created");
    return;
    }

  // Call the superclass

  this->Superclass::Create(app);

  // Create the widget

  if (!this->Widget->GetParent())
    {
    this->Widget->SetParent(this);
    }
  this->Widget->Create(app);
  if (!this->Widget->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " failed creating @WidgetType@");
    return;
    }

  // Pack

  this->Pack();

  // Update enable state
  
  this->UpdateEnableState();
}

//----------------------------------------------------------------------------
void @WidgetType@WithLabel::Pack()
{
  if (!this->IsCreated())
    {
    return;
    }

  // Unpack everything

  if (this->Widget)
    {
    this->Widget->UnpackSiblings();
    }

  // Repack everything

  ostrstream tk_cmd;

  const char *side;
  switch (this->LabelPosition)
    {
    case vtkKWWidgetWithLabel::LabelPositionTop:
      side = "top";
      break;
    case vtkKWWidgetWithLabel::LabelPositionBottom:
      side = "bottom";
      break;
    case vtkKWWidgetWithLabel::LabelPositionRight:
      side = "right";
      break;
    case vtkKWWidgetWithLabel::LabelPositionDefault:
    case vtkKWWidgetWithLabel::LabelPositionLeft:
    default:
      side = "left";
      break;
    }

  if (this->LabelVisibility && this->HasLabel() && this->GetLabel()->IsCreated())
    {
    tk_cmd << "pack " << this->GetLabel()->GetWidgetName() 
           << " -anchor nw -side " << side << endl;
    }

  if (this->Widget && this->Widget->IsCreated())
    {
    tk_cmd << "pack " << this->Widget->GetWidgetName() 
           << " -anchor nw -fill x -side " << side << " -expand "
           << (this->ExpandWidget ? "y" : "n") << endl;
    }
  
  tk_cmd << ends;
  this->Script(tk_cmd.str());
  tk_cmd.rdbuf()->freeze(0);
}

// ----------------------------------------------------------------------------
void @WidgetType@WithLabel::SetExpandWidget(int _arg)
{
  if (this->ExpandWidget == _arg)
    {
    return;
    }

  this->ExpandWidget = _arg;

  this->Modified();

  this->Pack();
}

//----------------------------------------------------------------------------
void @WidgetType@WithLabel::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();
  
  this->PropagateEnableState(this->Widget);
}

// ---------------------------------------------------------------------------
void @WidgetType@WithLabel::SetBalloonHelpString(const char *string)
{
  this->Superclass::SetBalloonHelpString(string);

  if (this->Widget)
    {
    this->Widget->SetBalloonHelpString(string);
    }
}

//----------------------------------------------------------------------------
void @WidgetType@WithLabel::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "ExpandWidget: " 
     << (this->ExpandWidget ? "On" : "Off") << endl;

  os << indent << "Widget: ";
  if (this->Widget)
    {
    os << endl;
    this->Widget->PrintSelf(os, indent.GetNextIndent());
    }
  else
    {
    os << "None" << endl;
    }
}
