/*=========================================================================

  Module:    $RCSfile: vtkKWWidgetWithSpinButtonsSubclass.cxx.in,v $

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "@WidgetType@WithSpinButtons.h"

#include "vtkKWSpinButtons.h"
#include "vtkObjectFactory.h"
#include "@WidgetType@.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(@WidgetType@WithSpinButtons);
vtkCxxRevisionMacro(@WidgetType@WithSpinButtons, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
@WidgetType@WithSpinButtons::@WidgetType@WithSpinButtons()
{
  this->Widget = @WidgetType@::New();
}

//----------------------------------------------------------------------------
@WidgetType@WithSpinButtons::~@WidgetType@WithSpinButtons()
{
  if (this->Widget)
    {
    this->Widget->Delete();
    this->Widget = NULL;
    }
}

//----------------------------------------------------------------------------
@WidgetType@* @WidgetType@WithSpinButtons::GetWidget()
{
  return this->Widget;
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::Create(vtkKWApplication *app)
{
  // Check if already created

  if (this->IsCreated())
    {
    vtkErrorMacro("@WidgetType@WithSpinButtons already created");
    return;
    }

  // Call the superclass

  this->Superclass::Create(app);

  // Create the widget

  if (!this->Widget->GetParent())
    {
    this->Widget->SetParent(this);
    }
  this->Widget->Create(app);
  if (!this->Widget->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " failed creating @WidgetType@");
    return;
    }

  // Pack

  this->Pack();

  // Update enable state
  
  this->UpdateEnableState();
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::Pack()
{
  if (!this->IsCreated())
    {
    return;
    }

  // Unpack everything

  if (this->Widget)
    {
    this->Widget->UnpackSiblings();
    }

  // Repack everything

  ostrstream tk_cmd;

  if (this->Widget && this->Widget->IsCreated())
    {
    tk_cmd << "pack " << this->Widget->GetWidgetName() 
           << " -anchor nw -side left -fill both -expand y" << endl;
    }

  if (this->SpinButtons && this->SpinButtons->IsCreated())
    {
    tk_cmd << "pack " << this->SpinButtons->GetWidgetName() 
           << " -anchor ne -side right -fill y -expand n" << endl;
    }
  
  tk_cmd << ends;
  this->Script(tk_cmd.str());
  tk_cmd.rdbuf()->freeze(0);
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::NextValueCallback()
{
  if (this->Widget)
    {
    this->Widget->NextValue();
    }
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::PreviousValueCallback()
{
  if (this->Widget)
    {
    this->Widget->PreviousValue();
    }
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();
  
  this->PropagateEnableState(this->Widget);
}

// ---------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::SetBalloonHelpString(const char *string)
{
  this->Superclass::SetBalloonHelpString(string);

  if (this->Widget)
    {
    this->Widget->SetBalloonHelpString(string);
    }
}

//----------------------------------------------------------------------------
void @WidgetType@WithSpinButtons::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "Widget: ";
  if (this->Widget)
    {
    os << endl;
    this->Widget->PrintSelf(os, indent.GetNextIndent());
    }
  else
    {
    os << "None" << endl;
    }
}
