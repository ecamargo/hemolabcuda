KWConvertImageToHeader --zlib --base64 \
    vtkKWTablelistTclLibrary.h \
    tablelistChecked.png \
    tablelistUnchecked.png \
    mwutil.tcl \
    tablelist.tcl \
    tablelistBind.tcl \
    tablelistConfig.tcl \
    tablelistEdit.tcl \
    tablelistMove.tcl \
    tablelistPublic.tcl \
    tablelistSort.tcl \
    tablelistUtil.tcl \
    tablelistUtil2.tcl \
    tablelistWidget.tcl