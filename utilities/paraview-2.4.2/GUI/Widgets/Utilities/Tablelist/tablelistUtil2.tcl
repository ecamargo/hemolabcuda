#------------------------------------------------------------------------------
# tablelist::emptyStr
#
# Return an empty string. Useful for -formatcommand
#------------------------------------------------------------------------------
proc tablelist::emptyStr {arg} {
  return ""
}
