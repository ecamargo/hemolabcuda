/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkUpdateSuppressorPipeline.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkUpdateSuppressorPipeline.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkUpdateSuppressorPipeline, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkUpdateSuppressorPipeline);


//----------------------------------------------------------------------------
vtkUpdateSuppressorPipeline::vtkUpdateSuppressorPipeline()
{
}

//----------------------------------------------------------------------------
vtkUpdateSuppressorPipeline::~vtkUpdateSuppressorPipeline()
{
}

//----------------------------------------------------------------------------
void vtkUpdateSuppressorPipeline::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
int vtkUpdateSuppressorPipeline::ProcessRequest(vtkInformation* request,
                                                int forward,
                                                vtkInformationVector** inInfo,
                                                vtkInformationVector* outInfo)
{
  if(this->Algorithm && request->Has(REQUEST_DATA()))
    {
    return 1;
    }
  if(request->Has(REQUEST_UPDATE_EXTENT()))
    {
    vtkInformation* info = outInfo->GetInformationObject(0);
    if(!info->Has(MAXIMUM_NUMBER_OF_PIECES()))
      {
      info->Set(MAXIMUM_NUMBER_OF_PIECES(), -1);
      }
    return 1;
    }
  return this->Superclass::ProcessRequest(request,forward,
                                          inInfo,outInfo);
}



