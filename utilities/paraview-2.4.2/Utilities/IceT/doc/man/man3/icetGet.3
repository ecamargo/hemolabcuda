.\" -*- nroff -*-
.ig
Documentation for the Image Composition Engine for Tiles (ICE-T).

Copyright (C) 2000-2002 Sandia National Laboratories

$Id: icetGet.3,v 1.5 2003/08/12 20:35:26 kmorel Exp $
..
.TH icetGet 3 "August 12, 2003" "Sandia National Labs" "ICE-T Reference"
.SH NAME
.B icetGet
\- get an
.B ICE-T
state parameter
.SH SYNOPSIS
.nf
.B #include <GL/ice-t.h>
.sp
.BI "void icetGetDoublev(GLenum " pname ", GLdouble *" params ");"
.BI "void icetGetFloatv(GLenum " pname ", GLfloat *" params ");"
.BI "void icetGetIntegerv(GLenum " pname ", GLint *" params ");"
.BI "void icetGetBooleanv(GLenum " pname ", GLboolean *" params ");"
.BI "void icetGetPointerv(GLenum " pname ", GLvoid **" params ");"
.fi
.SH DESCRIPTION
Like OpenGL, the operation of
.B ICE-T
is defined by a large state machine.  Also like OpenGL, the state parameters
can be retrieved through the
.B icetGet*v
functions.  Each function takes a symbolic constant,
.IR pname ,
which identifies the state parameter to retrieve.  They also each take an
array,
.IR params ,
which will be filled with the values in
.IR pname .
It is the calling application's responsibility to ensure that
.I params
is big enough to hold all the data.
.SH STATE PARAMETERS
The following list identifies valid values for
.I pname
and a description of the associated state parameter.
.TP 10
.B ICET_ABSOLUTE_FAR_DEPTH
The maximum possible value in the depth buffer (i.e. the value in a cleared
depth buffer), as stored as an unsigned 32 bit integer.  Usually, this is
the expected
.BR 0xFFFFFFFF .
However, some systems that use buffer values with
24 bits or less cast the maximum value to something smaller.
.TP
.B ICET_BACKGROUND_COLOR
The color that ICE-T is currently assuming is the background color.  It is
an RGBA value that is stored as four floating point values.
.TP
.B ICET_BACKGROUND_COLOR_WORD
The same as
.B ICET_BACKGROUND_COLOR
except that each component is stored as 8-bit values and packed in a 4-byte
integer as specified by
.BR ICET_COLOR_FORMAT .
The idea is to rapidly fill the background of color buffers.
.TP
.B ICET_BLEND_TIME
The total time, in seconds, spent in performing color blending of images
during the last call to
.BR icetDrawFrame .
Stored as a double.  An alias for this value is
.BR ICET_COMPARE_TIME .
.TP
.B ICET_BUFFER_READ_TIME
The total time, in seconds, spent reading from OpenGL buffers during the
last call to
.BR icetDrawFrame .
Stored as a double.
.TP
.B ICET_BUFFER_WRITE_TIME
The total time, in seconds, spent writing to OpenGL buffers during the last
call to
.BR icetDrawFrame .
Stored as a double.
.TP
.B ICET_BYTES_SENT
The total number of bytes sent by the calling process for transferring
image data during the last call to
.BR icetDrawFrame .
Stored as an integer.
.TP
.B ICET_COLOR_BUFFER_VALID
True if a color buffer was computed during the last call to
.B icetDrawFrame
and is available with a call to
.BR icetGetColorBuffer .
.TP
.B ICET_COLOR_FORMAT
The OpenGL symbolic constant describing the format in which
.B ICE-T
reads and stores color buffers.  Currently always set to
.BR GL_RGBA ", " GL_BGRA ", or " GL_BGRA_EXT .
.TP
.B ICET_COMPARE_TIME
The total time, in seconds, spent in performing Z comparisons of images
during the last call to
.BR icetDrawFrame .
Stored as a double.  An alias for this value is
.BR ICET_BLEND_TIME .
.TP
.B ICET_COMPOSITE_ORDER
The order in which images are to be composited if
.B ICET_ORDERED_COMPOSITE
is enabled and the current startegy supports ordered compositing.  The
parameter contains
.B ICET_NUM_PROCESSES
entries.  The value of this parameter is set with
.BR icetCompositeOrder .
If the element of index
.I i
in the array is set to
.IR j ,
then there are
.I i
images ``on top'' of the image generated by process
.IR j .
.TP
.B ICET_COMPOSITE_TIME
The total time, in seconds, spent in compositing during the last call to
.BR icetDrawFrame .
Equal to
.BR ICET_TOTAL_DRAW_TIME " - " ICET_RENDER_TIME " - "
.BR ICET_BUFFER_READ_TIME " - " ICET_BUFFER_WRITE_TIME .
Stored as a double.
.TP
.B ICET_COMPRESS_TIME
The total time, in seconds, spent in compressing image data using active
pixel encoding during the last call to
.BR icetDrawFrame .
Stored as a double.
.TP
.B ICET_DATA_REPLICATION_GROUP
An array of process ids.  There are
.B ICET_DATA_REPLICATION_GROUP_SIZE
entries in the array.
.B ICE-T
assumes that all processes in the list will create the exact same image
with their draw functions (set with
.BR icetDrawFunc ).
The local process id
.RB ( ICET_RANK )
will be part of this list.
.TP
.B ICET_DATA_REPLICATION_GROUP_SIZE
The length of the
.B ICET_DATA_REPLICATION_GROUP
array.
.TP
.B ICET_DEPTH_BUFFER_VALID
True if a depth buffer was computed during the last call to
.B icetDrawFrame
and is available with a call to
.BR icetGetDepthBuffer .
.TP
.B ICET_DIAGNOSTIC_LEVEL
The diagnostics flags set with
.BR icetDiagnostics .
.TP
.B ICET_DISPLAY_NODES
An array of process ranks.  The size of the array is equal to the number
of tiles
.RB ( ICET_NUM_TILES ).
The
.IR i -th
entry is the rank of the process that is displaying the tile described by
the
.IR i -th
entry in
.BR ICET_TILE_VIEWPORTS .
.TP
.B ICET_DRAW_FUNCTION
A pointer to the drawing callback function, as set by
.BR icetDrawFunc .
.TP
.B ICET_INPUT_BUFFERS
A bitmask specifying the the buffers which
.B ICE-T
will read from OpenGL and perform composition.  The value is set with
.BR icetInputOutputBuffers .
See the documentation of that function for valid bit flags.
.TP
.B ICET_FRAME_COUNT
The number of times
.B icetDrawFrame
has been called for the current context.
.TP
.B ICET_GEOMETRY_BOUNDS
An array of vertices whose convex hull bounds the drawn geometry.  Set with
.BR icetBoundingVerticies " or " icetBoundingBox* .
Each vertex has three coordinates and are tightly packed in the array.  The
size of the array is
.BR 3 * ICET_NUM_BOUNDING_VERTS .
.TP
.B ICET_GLOBAL_VIEWPORT
Defines a viewport in an infinite logical display that covers all tile
viewports (listed in
.BR ICET_TILE_VIEWPORTS ).
The viewport, like an OpenGL viewport, is given as the integer four-tuple
.RI ( x ", " y ", " width ", " height ).
.IR x " and " y
are placed at the leftmost and lowest position of all the tiles, and
.IR width " and " height
are just big enough for the viewport to cover all tiles.  The viewports are
listed in the same order as the tiles were defined with
.BR icetAddTile .
.TP
.B ICET_NUM_BOUNDING_VERTS
The number of bounding vertices listed in the
.B ICET_GEOMETRY_BOUNDS
parameter.
.TP
.B ICET_NUM_TILES
The number of tiles in the defined display.  Basically equal to the number
of times
.B icetAddTiles
was called after the last
.BR icetResetTiles .
.TP
.B ICET_NUM_PROCESSES
The number of processes in the parallel job as given by the
.B IceTCommunicator
object associated with the current context.
.TP
.B ICET_OUTPUT_BUFFERS
A bitmask specifying the the buffers which
.B ICE-T
will generate from composition.  The value is set with
.BR icetInputOutputBuffers .
See the documentation of that function for valid bit flags.
.TP
.B ICET_PROCESS_ORDERS
Basically, the inverse of
.BR ICET_COMPOSITE_ORDER .
The parameter contains
.B ICET_NUM_PROCESSES
entries.  If the element of index
.I i
in the array is set to
.IR j ,
then there are
.I j
images ``on top'' of the image generated by process
.IR i .
.TP
.B ICET_RANK
The rank of the process as given by the
.B IceTCommunicator
object associated with the current context.
.TP
.B ICET_READ_BUFFER
Set to the OpenGL symbolic constant that
.B ICE-T
will use to read back buffers.  Currently always set to
.BR GL_BACK .
.TP
.B ICET_RENDER_TIME
The total time, in seconds, spent in the drawing callback during the last
call to
.BR icetDrawFrame .
Stored as a double.
.TP
.B ICET_STRATEGY_SUPPORTS_ORDERING
Is true if and only if the current strategy supports ordered compositing.
.TP
.B ICET_TILE_DISPLAYED
The index of the tile the local process is displaying.  The index will
correspond to the tile entry in the
.BR ICET_DISPLAY_NODES " and " ICET_TILE_VIEWPORT
arrays.  If set to
.B 0
<=
.I i
<
.BR ICET_NUM_PROCESSES ,
then the
.IR i -th
entry of
.B ICET_DISPLAY_NODES
is equal to
.BR ICET_RANK .
If the local process is not displaying any tile, then
.B ICET_TILE_DISPLAYED
is set to
.BR -1 .
.TP
.B ICET_TILE_MAX_HEIGHT
The maximum
.I height
of any tile.
.TP
.B ICET_TILE_MAX_PIXELS
The maximum number of pixels in any tile.  This number is actually set to
.RB ( ICET_TILE_MAX_WIDTH " * " ICET_TILE_MAX_HEIGHT ") + " ICET_NUM_PROCESSES .
The number of processes is added to provide sufficient padding such that
the max tile image may be divided evenly amongst any group of processes
without dropping any real pixels.
.TP
.B ICET_TILE_MAX_WIDTH
The maximum
.I width
of any tile.
.TP
.B ICET_TILE_VIEWPORTS
A list of viewports in the logical global display defining the tiles.  Each
viewport is the four-tuple
.RI ( x ", " y ",  " width ", " height )
defining the position and dimensions of a tile in pixels, much like a
viewport is defined in OpenGL.  The size of the array is
.BR 4 * ICET_NUM_TILES .
.TP
.B ICET_TOTAL_DRAW_TIME
Time spent in the last call to
.BR icetDrawFrame .
Stored as a double.
.SH ERRORS
.TP 20
.B ICET_BAD_CAST
The state parameter requested is of a type that cannot be cast to the output
type.
.TP
.B ICET_INVALID_ENUM
.I pname
is not a valid state parameter.
.SH WARNINGS
None.
.SH BUGS
None known.
.SH NOTES
Not every state variable is documented here.  There is a set of parameters
used internally by
.B ICE-T
or are more appropriately
retrieved with other functions such as
.BR icetIsEnabled .
.SH COPYRIGHT
Copyright \(co 2003 Sandia Corporation
.br
Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
license for use of this work by or on behalf of the U.S. Government.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that this Notice and any statement of
authorship are reproduced on all copies.
.SH SEE ALSO
.BR icetIsEnabled ", " icetGetStrategyName


\" These are emacs settings that go at the end of the file.
\" Local Variables:
\" writestamp-format:"%B %e, %Y"
\" writestamp-prefix:"3 \""
\" writestamp-suffix:"\" \"Sandia National Labs\""
\" End:
