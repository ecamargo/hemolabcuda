.\" -*- nroff -*-
.ig
Documentation for the Image Composition Engine for Tiles (ICE-T).

Copyright (C) 2000-2002 Sandia National Laboratories

$Id: ICE-T.7,v 1.2 2003/07/24 17:46:19 kmorel Exp $
..
.TH ICE-T 7 "July 23, 2003" "Sandia National Labs" "ICE-T User's Guide"
.SH NAME
.B ICE-T
\- Image Composition Engine for Tiles
.SH SYNOPSIS
.nf
.B #include <GL/ice-t.h>
.fi
.SH DESCRIPTION
The Image Composition Engine for Tiles
.RB ( ICE-T )
is an API designed to enable OpenGL applications to perform Sort-Last
parallel rendering on very large displays.  The displays are assumed to be
tile displays.  The overall resolution of the display may be several times
larger than any viewport that may be rendered by a single machine.  It is
also assumed that several processes in the parallel application are
.I display
processes.  That is, their entire display window makes up part of the
display.
.PP
The design philosophy behind
.B ICE-T
was to allow very large sets of polygons to be displayed on very high
resolution displays.  As such, fast frame rates have been sacrificed in
lieu of very scalable and very high polygon/second rendering rates.  In
applications that require only moderate size data to be rendered on
high-resolution tile displays, the user may best be suited with API's with
approaches offering potentially higher frame rates such as Wire-GL or
Chromium.
.PP
.B ICE-T
is designed to take advantage of
.I spatial decomposition
of the geometry being rendered.  That is, it works best if all the geometry
on each process is located in as small a region of space as possible.  When
this is true, each process usually projects geometry on only a small
section of the screen.  This results in less work for the compositing
engine.
.PP
.B ICE-T
can also be used to perform sort-last parallel rendering to a single
display.  Such
.I single-tile
rendering is simply a special case of the multi-tile display
.B ICE-T
was designed for.  Many of the optimizations done by
.B ICE-T
apply to the single-tile mode.  Using
.B ICE-T
for this purpose is quite worthwhile.
.BR ICE-T 's
performance should rival that of other such software image compositors.
.PP
The rest of this document describes the use of the
.B ICE-T
API.  There are also separate manual pages for each of the functions
described here.  For more details on
.BR ICE-T 's
algorithms, see:
.HP 4
Moreland, Wylie, and Pavlakos.  ``Sort-last parallel rendering for viewing
extremely large data sets on tile displays,'' In
.IR "Proceedings of IEEE Symposium on Parallel and Large-Data Visualization and Graphics" ,
October 2001, pp. 85-154.
.SH COMMUNICATORS
Before
.B ICE-T
can function, it must have a communicator.  A communicator is described by
an ``object'' of type
.BR IceTCommunicator .
(Here we are using the term object loosely since it is actually implemented
as a C struct.)
.B IceTCommunicator
is an abstract object that can potentially be implemented with a wide
variety of communication APIs.  An implementation of
.B IceTCommunicator
that uses MPI is provided with
.BR ICE-T .
To use it, include
.BR icet-t_mpi.h .
An
.B ICE-T
communicator can be built from an MPI communicator with
.B icetCreateMPICommunicator
and a later deleted with
.BR icetDestroyMPICommunicator .
.SH INITIALIZATION
Before an application can use
.BR ICE-T ,
it must create a context.  This can be done with a call to
.BR icetCreateContext .
A context may be deleted with a call to
.BR icetDestroyContext .
Failing to create a context will make the behaviors of all other
.B ICE-T
functions undefined (and will probably result in a segmentation fault).
.PP
The typical application will probably need only one context, so calling
.B icetCreateContext
at program startup and calling
.B icetDestroyContext
at program exit is usually sufficient.  See the section
.B CONTEXTS
below for more information on using contexts.
.PP
A context also requires a bit of setup before rendering and image
composition can begin.  A strategy, tile layout, and drawing function must
all be set.  See the following sections for more details.
.SH STRATEGIES
.B ICE-T
provides several different
.I strategies
for composing multiple tile images in an efficient manner.  The strategy
used must be set with
.B icetStrategy
before any rendering and image composition may be performed with
.BR ICE-T .
If no particular strategy is clearly the best choice,
.B ICET_STRATEGY_REDUCE
is a good generic strategy to use.  A textual description of the current
strategy can be retrieved with
.BR icetGetStrategyName .
.SH TILE DISPLAY LAYOUT
Before
.B ICE-T
can do its compositing, it must know the topology of the tile display.  The
display is defined as a set of tiles.  Each tile is defined as a
rectangular region of pixels in a large, logical window, much like a
viewport in OpenGL is a region of pixels in a physical window.  Each tile
is also associated with a unique process.  The pixels in the physical
window are assumed to be projected as the described tile in the overall
display.
.PP
The set of tiles used in the display are described through the use of two
functions.  First,
.B icetResetTiles
is called to clear out any previous tile definitions.  Then,
.B icetAddTile
is called multiple times, each time adding a new tile to the display.
.B icetAddTile
specifies the layout of an additional tile in the overall display and the
rank of the processor whose output is plugged into that part of the display.
.PP
The current
.B ICE-T
algorithms were designed around the assumption that all of the tiles are of
the same size, so for the moment it is best to follow this convention.
Otherwise, there are no restrictions on how tiles are lain out.  Mullions
and overlaps can be implemented simply by laying tiles out properly.  It is
also usually assumed that each processes' rendering viewport is the same
size as the tiles.  However, there may be a significant advantage to having
a rendering viewport that is larger then the tiles.  So if the size of a
rendering window does not match the size of a tile, use
.B icetRenderableSize
to inform
.B ICE-T
on the actual size of the rendering viewport.
.SH RENDERING
Because
.B ICE-T
does image compositions on large tile displays, it is a requirement that
.B ICE-T
be able to render the same frame with multiple projections (for the various
tiles).  It does this through the use of callbacks.  The application sets
the drawing callback function with
.BR icetDrawFunc .
When
.B ICE-T
needs a tile rendered, it will set the appropriate projection matrix and
call the drawing callback.
.PP
In order for
.B ICE-T
to efficiently perform large screen compositions, it must know the bounds
of the geometry on each process.  The geometry bounds are set with
.B icetBoundingVertices
(or with one of the
.B icetBoundingBox
convenience functions).  The geometry drawn by the drawing callback must
fit within the hull of the bounding vertices.  Otherwise the geometry may
be culled in unexpected ways.  If the bounds are not specified,
.B ICE-T
will assume that the geometry spans the entire display.  For single-tile
applications, this may be perfectly acceptable, but for large, multi-tile
displays, this will probably result in abysmal performance.
.PP
After the tiles, rendering callback, and geometry bounds are set up,
.B ICE-T
is ready to render a frame.  To render a frame, set up the OpenGL
projection and modelview matrices and then call
.BR icetDrawFrame .
.B icetDrawFrame
will transform the geometry bounds and determine into which tiles the
geometry may project.  It then calls the rendering callback
multiple times with different projections to get the tile images, and
composes these images together.
.SH STATE
Like OpenGL,
.B ICE-T
operates as one big state machine.  Also like OpenGL,
.B ICE-T
provides a means to retrieve information about the current state.  The
.B icetGet
functions
.RB ( icetGetDoublev ", " icetGetFloatv ", " icetGetIntegerv ", "
.BR icetGetBooleanv ", and " icetGetPointerv )
will retrieve data pertaining to a given state.
.PP
In addition, the
.BR icetEnable " and " icetDisable
functions can be used to change the part of the state that controls how
.B ICE-T
behaves.  The
.B icetIsEnabled
function will return the current state of one of these flags.
.SH IMAGE COMPOSITION
.B ICE-T
supports two image composition operations.  The first is the
.I Z-compare
operation.  In this operation, the OpenGL depth buffer is read as part of
the pixels.  When two pixels are compared, the one with that is closest
(that is, has the minimum depth value) is selected for the output.
.PP
The second operation is the
.I color blend
operation.  In this operation, the pixel colors are blended with the Porter
and Duff OVER operator (or equivocally the UNDER operator depending on
which is on top).  See Porter and Duff, SIGGRAPH 1984 for more information
on how this color blending operation works.  This composition operation is
not commutative; that is, order matters.  Therefore, when using the
.I color blend
operation, it is wise to enable
.B ICET_ORDERED_COMPOSITE
and specify a composite order.  See
.B icetCompositeOrder
for more details.
.PP
The image composition method is chosen implicitly by which buffers
.B ICE-T
is asked to compose.  If a depth buffer is selected as an input buffer,
then Z-compare is used.  If a depth buffer is not selected as an input
buffer, then color blending is used.  This scheme was chosen because color
blending makes no sense on a depth buffer, and Z comparison makes no sense
without a depth buffer.  The input (and output) buffer may be selected with
.BR icetInputOutputBuffers .
.SH DATA REPLICATION
.B ICE-T
supports K-way data replication.  That is, if two or more processors draw
exactly the same geometry,
.B ICE-T
can use this to its advantage to reduce the amount of image composition
work that is required.  Groups of processors that share data can be
specified with the
.BR icetDataReplicationGroup " and " icetDataReplicationGroupColor
functions.
.SH TIMING
.B ICE-T
keeps several timing statistics for testing purposes and for monitoring by
user applications.  The timing statistics are stored in a set of state
parameters which can be retrieved with the
.B icetGet
functions.  The following statistics are available:
.TP 23
.B ICET_RENDER_TIME
Time spent in the rendering callback.
.TP
.B ICET_BUFFER_READ_TIME
Time spent while reading OpenGL buffers.
.TP
.B ICET_BUFFER_WRITE_TIME
Time spent while writing OpenGL buffers.
.TP
.B ICET_COMPRESS_TIME
Time spent compressing image data with active pixel encoding.
.TP
.B ICET_COMPARE_TIME
Time spent performing Z comparisons.
.TP
.B ICET_COMPOSITE_TIME
Total time spent compositing images.
.TP
.B ICET_TOTAL_DRAW_TIME
Total time spent in the last call to
.BR icetDrawFrame .
.TP
.B ICET_BYTES_SENT
Total number of bytes sent while transferring images.
.PP
All of these statistics are reset at the begining of a call to
.BR icetDrawFrame .
They are also all local to each processor.  So, for example,
.B ICET_TOTAL_DRAW_TIME
may give a smaller than expected time if the calling processor had less
work to do than others.  Except for
.BR ICET_BYTES_SENT ,
all these statistics are given in seconds and stored as doubles.
.SH DIAGNOSTICS AND ERRORS
.B ICE-T
has two mechanisms to deal with anomalous conditions: diagnostics and
errors.  During its operation,
.B ICE-T
has the ability to print diagnostic messages to standard output.  Use
.B icetDiagnostics
to set which diagnostics are printed.  Also, if an anomalous condition
occurs,
.B ICE-T
will set an error flag.  This flag can be retrieved with
.BR icetGetError .
.SH CONTEXTS
It is conceivable that
.B ICE-T
may be used in several configurations at one time.  A user may, for example,
wish to swap between separate tile layouts.  This functionality is
facilitated by contexts.  Each context has its own state.  At any given
point in time, a single context is in use.  Contexts may be changed to swap
states quickly.
.PP
When
.B ICE-T
is initialized, a default context is created.  Additional contexts may be
created with
.B icetCreateContext
and destroyed with
.BR icetDestroyContext .
A handle for the current context can be retrieved with
.BR icetGetContext ,
and the context may be changed with
.BR icetSetContext .
Also, an entire state may be copied from one context to another with
.BR icetCopyState .
.SH COPYRIGHT
Copyright \(co 2003 Sandia Corporation
.br
Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
license for use of this work by or on behalf of the U.S. Government.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that this Notice and any statement of
authorship are reproduced on all copies.
.SH SEE ALSO
.BR ICE-T_buckets


\" These are emacs settings that go at the end of the file.
\" Local Variables:
\" writestamp-format:"%B %e, %Y"
\" writestamp-prefix:"7 \""
\" writestamp-suffix:"\" \"Sandia National Labs\""
\" End:
