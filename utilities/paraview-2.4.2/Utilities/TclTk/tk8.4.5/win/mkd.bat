@echo off
rem RCS: @(#) $Id: mkd.bat,v 1.1.1.1 2003/12/19 15:38:16 king Exp $

if exist %1\nul goto end

md %1
if errorlevel 1 goto end

echo Created directory %1

:end



