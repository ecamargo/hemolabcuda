PROJECT(Xdmf)

SET(CMAKE_C_FLAGS "${CMAKE_ANSI_CFLAGS} ${CMAKE_C_FLAGS}")
SET(XDMF_BUILD_VTK 1)

SET(HDF5_LIBRARY ${PARAVIEW_HDF5_LIBRARIES})
IF(VTK_USE_SYSTEM_EXPAT)
  SET(XDMF_EXPAT_LIBRARIES ${EXPAT_LIBRARY})
ELSE(VTK_USE_SYSTEM_EXPAT)
  SET(XDMF_EXPAT_LIBRARIES vtkexpat)
ENDIF(VTK_USE_SYSTEM_EXPAT)
SET(XDMF_USE_ANSI_STDLIB ${PARAVIEW_USE_ANSI_STDLIB})

IF(NOT XDMF_INSTALL_LIB_DIR)
  SET(XDMF_INSTALL_LIB_DIR /lib)
ENDIF(NOT XDMF_INSTALL_LIB_DIR)
IF(NOT XDMF_INSTALL_INCLUDE_DIR)
  SET(XDMF_INSTALL_INCLUDE_DIR /include/Xdmf)
ENDIF(NOT XDMF_INSTALL_INCLUDE_DIR)
IF(NOT XDMF_INSTALL_INCLUDE_VTK_DIR)
  SET(XDMF_INSTALL_INCLUDE_VTK_DIR /include/XdmfVtk)
ENDIF(NOT XDMF_INSTALL_INCLUDE_VTK_DIR)

# Let parent project set XDMF_INSTALL_NO_DEVELOPMENT or
# XDMF_INSTALL_NO_RUNTIME to remove components from the installation.
SET(XDMF_INSTALL_NO_LIBRARIES)
IF(BUILD_SHARED_LIBS)
  IF(XDMF_INSTALL_NO_RUNTIME AND XDMF_INSTALL_NO_DEVELOPMENT)
    SET(XDMF_INSTALL_NO_LIBRARIES 1)
  ENDIF(XDMF_INSTALL_NO_RUNTIME AND XDMF_INSTALL_NO_DEVELOPMENT)
ELSE(BUILD_SHARED_LIBS)
  IF(XDMF_INSTALL_NO_DEVELOPMENT)
    SET(XDMF_INSTALL_NO_LIBRARIES 1)
  ENDIF(XDMF_INSTALL_NO_DEVELOPMENT)
ENDIF(BUILD_SHARED_LIBS)

IF(EXISTS "${HDF5_CONFIG}")
  INCLUDE("${HDF5_CONFIG}")
  ADD_DEFINITIONS(${HDF5_EXTRA_FLAGS})
ENDIF(EXISTS "${HDF5_CONFIG}")

OPTION(XDMF_HAS_NDGM "XDMF has Network Distributed Global Memory (NDGM)" OFF)
MARK_AS_ADVANCED(XDMF_HAS_NDGM)
IF(XDMF_HAS_NDGM)
  OPTION(XDMF_HAS_NDGM_SOURCE "XDMF has Network Distributed Global Memory (NDGM) Source" OFF)
  IF(XDMF_HAS_NDGM_SOURCE)
    FIND_PATH(NDGM_DIR BuildNDGM.cmake ${Xdmf_SOURCE_DIR}/../Ndgm "Root of Source for NDGM")
    IF(NDGM_DIR)
      MESSAGE(STATUS "......Process NDGM Source")
      INCLUDE(${NDGM_DIR}/BuildNDGM.cmake)
      #SUBDIRS(NDGM)
      INCLUDE_DIRECTORIES(${NDGM_DIR}/libsrc)
      SET(NDGM_LIBRARY ndgm) 
    ENDIF(NDGM_DIR)
  ELSE(XDMF_HAS_NDGM_SOURCE)
    MESSAGE(STATUS "......Looking for NDGM_INCLUDE_DIR")
    FIND_PATH(NDGM_INCLUDE_DIR Ndgm ${Xdmf_SOURCE_DIR}/../Ndgm/libsrc "Include Directory for NDGM; Resolves #include <Ndgm/ndgm.h>")
    MESSAGE(STATUS "......Looking for NDGM_LIBRARY")
    FIND_LIBRARY(NDGM_LIBRARY ndgm ${LIBRARY_OUTPUT_PATH})
    INCLUDE_DIRECTORIES(${NDGM_INCLUDE_DIR})
    SET(NDGM_LIBRARY ${NDGM_LIBRARY})
  ENDIF(XDMF_HAS_NDGM_SOURCE)
  SET(HAVE_NDGM 1)
ENDIF(XDMF_HAS_NDGM)

INCLUDE_DIRECTORIES(
  ${Xdmf_SOURCE_DIR}/Ice
  ${Xdmf_BINARY_DIR}/Ice
  ${Xdmf_SOURCE_DIR}/libsrc
  ${Xdmf_BINARY_DIR}/libsrc
  ${Xdmf_SOURCE_DIR}/vtk
  ${Xdmf_BINARY_DIR}/vtk
  ${VTK_INCLUDE_DIR} 
  ${VTKEXPAT_INCLUDE_DIR}
  ${ParaView_SOURCE_DIR}/VTK/Utilities
  ${ParaView_BINARY_DIR}/VTK/Utilities
  ${HDF5_INCLUDE_DIR}
  )
SUBDIRS(Ice)
SUBDIRS(libsrc)
SUBDIRS(vtk)
