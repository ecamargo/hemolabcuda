/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkHierarchicalDataSet.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkHierarchicalDataSet.h"

#include "vtkDataSet.h"
#include "vtkHierarchicalDataInformation.h"
#include "vtkInformation.h"
#include "vtkInformationIntegerKey.h"

#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkHierarchicalDataSet, "$Revision: 1.8 $");
vtkStandardNewMacro(vtkHierarchicalDataSet);

vtkInformationKeyMacro(vtkHierarchicalDataSet,LEVEL,Integer);

//----------------------------------------------------------------------------
vtkHierarchicalDataSet::vtkHierarchicalDataSet()
{
}

//----------------------------------------------------------------------------
vtkHierarchicalDataSet::~vtkHierarchicalDataSet()
{
}

//----------------------------------------------------------------------------
vtkHierarchicalDataInformation* 
vtkHierarchicalDataSet::GetHierarchicalDataInformation()
{
  return vtkHierarchicalDataInformation::SafeDownCast(
    this->GetMultiGroupDataInformation());
}

//----------------------------------------------------------------------------
void vtkHierarchicalDataSet::SetHierarchicalDataInformation(
  vtkHierarchicalDataInformation* info)
{
  this->Superclass::SetMultiGroupDataInformation(info);
}

//----------------------------------------------------------------------------
void vtkHierarchicalDataSet::AddDataSet(vtkInformation* index, vtkDataObject* dobj)
{
  if (index->Has(INDEX()) && index->Has(LEVEL()))
    {
    this->SetDataSet(index->Get(LEVEL()), index->Get(INDEX()), dobj);
    }
  else
    {
    this->Superclass::AddDataSet(index, dobj);
    }
}

//----------------------------------------------------------------------------
vtkDataObject* vtkHierarchicalDataSet::GetDataSet(vtkInformation* index)
{
  if (index->Has(INDEX()) && index->Has(LEVEL()))
    {
    return this->GetDataSet(index->Get(LEVEL()), index->Get(INDEX()));
    }
  return this->Superclass::GetDataSet(index);
}

//----------------------------------------------------------------------------
void vtkHierarchicalDataSet::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

