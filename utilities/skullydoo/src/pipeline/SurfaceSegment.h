/*
# $Id: SurfaceSegment.h,v 1.3 2003/05/23 19:18:59 sebasfiorent Exp $
# SkullyDoo - Segmentador y visualizador de imagenes tridimensionales  
# (C) 2002 Sebasti n Fiorentini / Ignacio Larrabide
# Contact Info: sebasfiorent@yahoo.com.ar / nacholarrabide@yahoo.com
# Argentina
############################# GPL LICENSE ####################################
#   This program is free software; you can redistribute it and/or modify      
#   it under the terms of the GNU General Public License as published by      
#   the Free Software Foundation; either version 2 of the License, or         
#   (at your option) any later version.                                       
#                                                                             
#   This program is distributed in the hope that it will be useful,           
#   but WITHOUT ANY WARRANTY; without even the implied warranty of            
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             
#   GNU General Public License for more details.                              
#                                                                             
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##############################################################################
*/

#ifndef _SurfaceSegment_h_
#define _SurfaceSegment_h_
#ifdef _MSC_VER
	#pragma warning  ( disable : 4786 )
#endif

#include "PipelineSegment.h"
#include "common/SurfaceModel.h"

#include <vtkOutlineFilter.h>
#include <vtkActor.h>
#include <vtkImageData.h>

class SurfaceSegment:public PipelineSegment{
protected:
	vtkOutlineFilter* outline;
	vtkActor* aoutline;
	vtkActor* surfaceActor;
	SurfaceModel::Pointer input;

	virtual void initializeSegment();
public:
	NewMacro(SurfaceSegment);
	virtual ~SurfaceSegment();

	void highlightSurface(bool highlight);
	void setInput(SurfaceModel::Pointer input);
	void setTexture(vtkImageData* texture);
	void clearTexture();
	vtkActor* getSurfaceActor();
};

#endif
